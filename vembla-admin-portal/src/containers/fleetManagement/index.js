import React, {Component} from "react";
import {connect} from "react-redux";
import {Row, Col} from "reactstrap";
import {withRouter} from "react-router-dom";

import {joinRoom} from "../../socket-io/socket-io-utils";
import {getPendingOrders} from "../../store/order/order-actions";
import {assignOrderManually} from "../../store/fleet/fleet-actions";

import Loader from "../../components/Loader";
import MapComponent from "../../components/MapComponent";
import TabComponent from "../../components/TabComponent";

let interval;

class FleetManagement extends Component {
    state = {
        setInterval: true,
        loadPage: true
    };

    componentWillUnmount() {
        clearInterval(interval);
    }

    componentDidMount() {
        joinRoom(this.props.token);
        this.props.getPendingOrders();
        interval = setInterval(() => {
            this.props.getPendingOrders();
            this.setState({loadPage: false})
        }, 1000 * 5);
    }

    render() {
        const {
            mapData,
            click,
            isfetchingPendingOrders,
            isProcessingFleet
        } = this.props;
        const currentLocation = mapData;

        const onDashboardClick = () => {
            clearInterval(interval);
            this.props.history.push("/admin/dashboard");
        };

        // if (isfetchingPendingOrders || isProcessingFleet) {
        //   return <Loader />;
        // }

        let markers = currentLocation ? [currentLocation] : this.props.activeFleet;
        return (
            <div className="content" style={{padding: "15px"}}>
                <Row>
                    <i className="tim-icons icon-minimal-left pointer"
                       onClick={() => onDashboardClick()}
                       style={{color: "white", padding: "5px 0px 16px 13px"}}>
            <span style={{
                paddingLeft: "8px",
                cursor: "pointer",
                fontSize: "18px",
                fontFamily: "Poppins"
            }}>
              Dashboard
            </span>
                    </i>
                </Row>
                <Row>
                    <Col md="3">
                        <TabComponent
                            title={this.props.orders}
                            firstNavItem={"Pending"}
                            pendingOrdersArray={this.props.pendingOrders}
                            secondNavItem={"Assigned"}
                            assignedOrdersArray={this.props.assignedOrders}
                            orderFirstNavContent={true}
                            orderSecondNavContent={true}
                            busyFleet={this.props.busyFleet}
                            freeRiders={this.props.freeRiders}
                            assignOrderManually={this.props.assignOrderManually}
                            getPendingOrders={this.props.getPendingOrders}
                        />
                    </Col>
                    <Col md="6" style={{height: "90vh"}}>
                        <MapComponent click={click}
                                      markerArray={markers}
                                      loadPage={this.state.loadPage}
                        />
                    </Col>
                    <Col md="3">
                        <TabComponent
                            title={this.props.riders}
                            firstNavItem={"Free"}
                            secondNavItem={"Busy"}
                            thirdNavItem={"Inactive"}
                            assignedOrdersArray={this.props.assignedOrders}
                            busyFleet={this.props.busyFleet}
                            offlineFleet={this.props.offlineFleet}
                            activeFleet={this.props.activeFleet}
                            pendingOrdersArray={this.props.pendingOrders}
                            freeRiders={this.props.freeRiders}
                            inactiveRiders={this.props.inactiveRiders}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        assignedOrders: state.fleet.assignedOrders,
        activeFleet: state.fleet.activeFleet,
        busyFleet: state.fleet.busyFleet,
        click: state.fleet.click,
        freeRiders: state.fleet.freeRiders,
        inactiveRiders: state.fleet.inactiveRiders,
        isProcessingFleet: state.fleet.processing,
        mapData: state.fleet.mapData,
        offlineFleet: state.fleet.offlineFleet,

        orders: state.multilingual.activeLanguageData.fleetManagement.orders,
        riders: state.multilingual.activeLanguageData.fleetManagement.riders,

        isfetchingPendingOrders: state.order.processing,
        pendingOrders: state.order.pendingOrders,

        token: state.user.token
    };
};

const connectedComponent = connect(mapStateToProps, {
    assignOrderManually,
    getPendingOrders
})(FleetManagement);
export default withRouter(connectedComponent);
