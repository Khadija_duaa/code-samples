import React, {Component} from 'react';
import {
    Row,
    Col
} from 'reactstrap';
import SimpleTable from '../../components/SimpleTable';
import {getAllConsumer, resetRedux} from '../../store/consumer/consumer-actions';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Loader from '../../components/Loader/index';
import ReactPaginate from 'react-paginate';

class Consumer extends Component {

    state = {
        pageSize: 10,
        totalPages: 0,
        currentPage: 0
    };

    componentDidMount() {
        this.props.resetRedux();
        this.props.getAllConsumer(this.state.currentPage, this.state.pageSize, "asc", "firstName");
    }

    getSortedConsumer = (sortVal, sortField) => {
        let sortValue = "asc";
        if(sortVal === "Descending") {
            sortValue = "desc"
        }
        this.props.getAllConsumer(this.state.currentPage, this.state.pageSize, sortValue, sortField);
    };

    getSimpleTable = () => {
        if (this.props.processing) {
            return (
                <div style={{marginBottom: '200px'}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <SimpleTable title={this.props.consumerTitle}
                                 sortFunction={this.getSortedConsumer}
                                 sortingDropdown={true}
                                 onClick={false}
                                 noDataTitle={this.props.noDataConsumerTitle}
                                 dataArray={this.props.consumer}/>
                </>
            )
    };

    getMoreConsumer = (e) => {
        this.setState({currentPage: e.selected}, () => {
                this.props.getAllConsumer(this.state.currentPage, this.state.pageSize, "asc", "firstName");
            }
        );
    };

    render() {
        let consumerData = this.props.consumerData;
        if (consumerData) {
            if (this.state.totalPages !== consumerData.totalPages) {
                this.setState({totalPages: consumerData.totalPages});
            }
        }
        return (
            <>
                <div className="content">
                    <Row>
                        <Col md="12">
                            {this.getSimpleTable()}
                            {
                                this.state.totalPages > 1 ? (
                                    <ReactPaginate
                                        previousLabel={<i className="fa fa-angle-left"/>}
                                        nextLabel={<i className="fa fa-angle-right"/>}
                                        breakLabel={'...'}
                                        breakClassName={'break-me'}
                                        pageCount={this.state.totalPages}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        onPageChange={(e) => this.getMoreConsumer(e)}
                                        containerClassName={'list-inline mx-auto justify-content-center pagination'}
                                        subContainerClassName={'list-inline-item pages pagination'}
                                        activeClassName={'active'}
                                    />) : null
                            }
                        </Col>
                    </Row>
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.consumer.processing,
        consumer: state.consumer.consumer,
        totalPages: state.consumer.totalPages,
        consumerData: state.consumer.consumerData,
        consumerTitle: state.multilingual.activeLanguageData.consumer,
        noDataConsumerTitle: state.multilingual.activeLanguageData.noDataTitle.consumers
    }
};

const connectedComponent = connect(mapStateToProps, {getAllConsumer, resetRedux})(Consumer);
export default withRouter(connectedComponent);
