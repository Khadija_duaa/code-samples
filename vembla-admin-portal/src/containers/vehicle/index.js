import React, {Component} from 'react';
import {
    Row,
    Col,
    Button,
    Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';
import ReactPaginate from 'react-paginate';
import Loader from '../../components/Loader/index';
import TableWithAddButton from "../../components/TableWithAddButton";
import {connect} from "react-redux";
import {getAllVehicles, setVehicleData, deleteVehicle, resetRedux} from "../../store/shopper/shopper-actions";
import {withRouter} from "react-router-dom";
import VehicleForm from "../../components/VehicleForm";

class Vehicle extends Component {

    state = {
        pageSize: 10,
        totalPages: 0,
        currentPage: 0,
        showVehicleForms: false,
        deleteModalOpen: false,
        vehicleData: null
    };

    componentDidMount() {
        this.props.resetRedux();
        this.props.getAllVehicles(this.state.currentPage, this.state.pageSize);
    }

    onDeleteBtnClick = (vehicleData, index) => {
        this.openDeleteModal({id: vehicleData.id, index});
    };

    openDeleteModal = (vehicleData) => this.setState({deleteModalOpen: !this.state.deleteModalOpen, vehicleData: vehicleData});

    getModalContent = () => {
        return (
            <Modal isOpen={this.state.deleteModalOpen} toggle={this.openDeleteModal}
                       className={'modal-danger ' + this.props.className}>

                <ModalHeader toggle={this.openDeleteModal}>
                    Are you sure ?
                </ModalHeader>

                <ModalBody>
                    Are you sure you want to delete this Vehicle ?
                </ModalBody>

                <ModalFooter>
                    <Button color="secondary"
                            onClick={() => {
                                let {vehicleData} = this.state;
                                this.props.deleteVehicle(vehicleData.id, vehicleData.index);
                                this.openDeleteModal(null);
                            }}
                    >
                        Delete
                    </Button>
                </ModalFooter>
            </Modal>
        );
    };

    vehicleEditBtn = (value, vehicleData) => {
        this.setState({showVehicleForms: value});
        this.props.setVehicleData(vehicleData)
    };

    showVehicleForm = (value) => {
        this.setState({showVehicleForms: value})
    };

    getSimpleTable = () => {
        const headers = ["Title", "Load Capacity", "Vehicle Type", "Weight Limit", "Volume Limit", "Action"];
        if (this.props.processing) {
            return (
                <div style={{marginBottom: '200px'}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <TableWithAddButton title={"Vehicle"}
                                        vehicleForm={true}
                                        noDataTitle={"No Vehicle Available"}
                                        topBtnTitle={"+ Vehicle"}
                                        headers={headers}
                                        dataArray={this.props.allVehicles}
                                        btnTitle={"Delete"}
                                        onDeleteBtnClick={this.onDeleteBtnClick}
                                        vehicleEditBtn={this.vehicleEditBtn}
                                        showForm={this.showVehicleForm}
                    />
                </>
            )
    };

    getMoreVehicles = (e) => {
        this.setState({currentPage: e.selected + 1}, () => {
                this.props.getAllStores(this.state.currentPage, this.state.pageSize);
            }
        );
    };

    render() {
        return (
            <>
                {this.getModalContent()}
                {
                    this.state.showVehicleForms === true ?
                        <div className="content">
                            <Row>
                                <Col md="12">
                                    <VehicleForm showForm={this.showVehicleForm}/>
                                </Col>
                            </Row>
                        </div>
                        :
                        <div className="content">
                            <Row>
                                <Col md="12">
                                    {this.getSimpleTable()}
                                    {
                                        this.state.totalPages > 1 ? (
                                            <ReactPaginate
                                                previousLabel={<i className="fa fa-angle-left"/>}
                                                nextLabel={<i className="fa fa-angle-right"/>}
                                                breakLabel={'...'}
                                                breakClassName={'break-me'}
                                                pageCount={this.state.totalPages}
                                                marginPagesDisplayed={2}
                                                pageRangeDisplayed={5}
                                                onPageChange={(e) => this.getMoreVehicles(e)}
                                                containerClassName={'list-inline mx-auto justify-content-center pagination'}
                                                subContainerClassName={'list-inline-item pages pagination'}
                                                activeClassName={'active'}
                                            />) : null
                                    }
                                </Col>
                            </Row>
                        </div>
                }
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.shopper.processing,
        allVehicles: state.shopper.allVehicles
    }
};

const connectedComponent = connect(mapStateToProps, {getAllVehicles, setVehicleData, deleteVehicle, resetRedux})(Vehicle);
export default withRouter(connectedComponent);
