import React, {Component} from 'react';
import {
    Row,
    Col
} from 'reactstrap';
import Loader from '../../components/Loader/index';
import TableWithAddButton from "../../components/TableWithAddButton";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import AdminForm from "../../components/AdminForm";
import {getAllRoles, getAllAdmins} from "../../store/admin/admin-actions";
import ReactPaginate from 'react-paginate';

class Admin extends Component {

    state = {
        showAdminForm: false,
        pageSize: 10,
        totalPages: 0,
        currentPage: 0
    };

    componentDidMount() {
        this.props.getAllRoles();
        this.props.getAllAdmins('', this.state.currentPage, this.state.pageSize)
    }

    // showAdminForm = (value) => {
    //     this.setState({showAdminForm: value})
    // };

    getSimpleTable = () => {
        const headers = [this.props.name, this.props.role, this.props.phone, this.props.email];
        if (this.props.processing) {
            return (
                <div style={{marginBottom: '200px'}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <TableWithAddButton title={this.props.admins}
                                        adminForm={true}
                                        route={'/admin/admin/form'}
                                        noDataTitle={this.props.noAdmins}
                                        topBtnTitle={this.props.createAdmin}
                                        headers={headers}
                                        roles={this.props.roles}
                                        dataArray={this.props.adminList}
                    />
                </>
            )
    };

    getMoreAdmins = (e) => {
        this.setState({currentPage: e.selected + 1}, () => {
            this.props.getAllAdmins('admin', this.state.currentPage, this.state.pageSize)
            }
        );
    };

    render() {
        let allAdminsData = this.props.allAdminsData;
        if (allAdminsData) {
            if (this.state.totalPages !== allAdminsData.totalPages) {
                this.setState({totalPages: allAdminsData.totalPages});
            }
        }
        return (
            <div className="content">
                {
                    this.state.showAdminForm ?
                        <Row>
                            <Col md={"12"}>
                                <AdminForm
                                    showAdminForm={this.showAdminForm}
                                />
                            </Col>
                        </Row> :
                        <Row>
                            <Col md="12">
                                {this.getSimpleTable()}
                                {
                                    this.state.totalPages > 1 ? (
                                        <ReactPaginate
                                            previousLabel={<i className="fa fa-angle-left"/>}
                                            nextLabel={<i className="fa fa-angle-right"/>}
                                            breakLabel={'...'}
                                            breakClassName={'break-me'}
                                            pageCount={this.state.totalPages}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={(e) => this.getMoreAdmins(e)}
                                            containerClassName={'list-inline mx-auto justify-content-center pagination'}
                                            subContainerClassName={'list-inline-item pages pagination'}
                                            activeClassName={'active'}
                                        />) : null
                                }
                            </Col>
                        </Row>

                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.admin.processing,
        roles: state.admin.roles,
        adminList: state.admin.admins,
        name: state.multilingual.activeLanguageData.name,
        email: state.multilingual.activeLanguageData.email,
        phone: state.multilingual.activeLanguageData.phone,
        role: state.multilingual.activeLanguageData.role,
        noAdmins: state.multilingual.activeLanguageData.noDataTitle.noAdmins,
        admins: state.multilingual.activeLanguageData.admins,
        createAdmin: state.multilingual.activeLanguageData.createAdmin,
        totalPages: state.admin.totalPages,
        allAdminsData: state.admin.allAdminsData,
    }
};

const connectedComponent = connect(mapStateToProps, {getAllRoles, getAllAdmins})(Admin);
export default withRouter(connectedComponent);
