import React, {Component} from "react";
import {Row, Col} from "reactstrap";
import ReactPaginate from "react-paginate";
import Loader from "../../components/Loader/index";
import TableWithAddButton from "../../components/TableWithAddButton";
import {connect} from "react-redux";
import {
    getAllStores,
    setEditItemBtn,
    resetRedux
} from "../../store/store/store-actions";
import {withRouter} from "react-router-dom";
import StoreForm from "../../components/StoreForm";
import StoreDetails from "../../components/StoreDetails";
import StorePackage from "../../components/StorePackage";
import StoreDeals from "../../components/StoreDeals";

let pageTitle;

class Store extends Component {
    state = {
        showStoreForm: false,
        showStoreDetails: false,
        showStoresByItemId: false,
        showStorePackage: false,
        cancelVal: false,
        showForm: false,
        allStores: "",
        pageSize: 10,
        totalPages: 0,
        currentPage: 0
    };

    componentDidMount() {
        this.props.resetRedux();
        if (this.props.showStoresByItemId === false) {
            this.props.getAllStores(this.state.currentPage, this.state.pageSize);
        } else {
            this.setState({
                allStores: this.props.allStores,
                showStoresByItemId: this.props.showStoresByItemId
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.allStores && nextProps.allStores.length > 0) {
            return true;
        } else if (nextState.showStoreForm === false && nextState.cancelVal === true) {
            this.props.getAllStores(this.state.currentPage, this.state.pageSize);
            return true;
        } else {
            return false;
        }
    }

    showStoreForm = (value, cancelVal) => {
        this.setState({
            showStoreForm: value,
            showStoreDetails: false,
            showForm: value
        });
        if (cancelVal) {
            this.setState({cancelVal: cancelVal});
        }
    };

    showStoreDetails = value => {
        this.setState({showStoreDetails: value, showStoreForm: value});
    };

    setEditItemButton = value => {
        this.props.setEditItemBtn(value);
    };

    showStorePackage = value => {
        this.setState({showStorePackage: value, showStoreForm: value});
    };

    showStoreDeal = value => {
        this.setState({showStoreDeal: value, showStoreForm: value});
    };

    getSimpleTable = () => {
        pageTitle = this.props.itemName
            ? `${this.props.storesTitle} for Item : ${this.props.itemName}`
            : this.props.storesTitle;
        const headers = [
            this.props.name,
            this.props.address,
            "Store ID",
            this.props.geofence,
            this.props.action
        ];
        if (this.props.processing) {
            return (
                <div style={{marginBottom: "200px"}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <TableWithAddButton
                        title={pageTitle}
                        setEditItemButton={this.setEditItemButton}
                        storeForm={true}
                        route={'/admin/store/form'}
                        noDataTitle={this.props.noDataStoresTitle}
                        topBtnTitle={this.props.newStoreTitle}
                        topRightButtonTitle={this.props.importCSVTitle}
                        btnTitle={this.props.itemsTitle}
                        headers={headers}
                        showStoreDetails={this.showStoreDetails}
                        showForm={this.showStoreForm}
                        currentPage={this.state.currentPage}
                        pageSize={this.state.pageSize}
                        activeLanguageLabel={this.props.activeLanguageLabel}
                        showStorePackage={this.showStorePackage}
                        showStoreDeal={this.showStoreDeal}
                        dataArray={this.state.showStoresByItemId === false ? this.props.allStores : this.state.allStores}
                    />
                </>
            );
    };

    getMoreStores = e => {
        this.setState({currentPage: e.selected + 1}, () => {
            this.props.getAllStores(this.state.currentPage, this.state.pageSize);
        });
    };

    render() {
        let allStoresData = this.props.allStoresData;
        if (allStoresData) {
            if (this.state.totalPages !== allStoresData.totalPages) {
                this.setState({totalPages: allStoresData.totalPages});
            }
        }
        return (
            <>
                {this.state.showStoreForm === true ? (
                    <div className="content">
                        <Row>
                            <Col md="12">
                                {this.state.showStoreDetails === true ? (
                                    <StoreDetails
                                        showStoreDetails={this.showStoreDetails}
                                        showStoreForm={this.showStoreForm}
                                    />
                                ) : null}
                                {this.state.showForm === true ? (
                                    <StoreForm
                                        showStoreForm={this.showStoreForm}
                                        currentPage={this.state.currentPage}
                                        pageSize={this.state.pageSize}
                                    />
                                ) : null}
                            </Col>
                        </Row>
                    </div>
                ) : (
                    <div className="content">
                        <Row>
                            <Col md="12">
                                {this.getSimpleTable()}
                                {this.state.totalPages > 1 ? (
                                    <ReactPaginate
                                        previousLabel={<i className="fa fa-angle-left"/>}
                                        nextLabel={<i className="fa fa-angle-right"/>}
                                        breakLabel={"..."}
                                        breakClassName={"break-me"}
                                        pageCount={this.state.totalPages}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        onPageChange={e => this.getMoreStores(e)}
                                        containerClassName={
                                            "list-inline mx-auto justify-content-center pagination"
                                        }
                                        subContainerClassName={"list-inline-item pages pagination"}
                                        activeClassName={"active"}
                                    />
                                ) : null}
                            </Col>
                        </Row>
                    </div>
                )}
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        processing: state.storeReducer.processing,
        allStores: state.storeReducer.allStores,
        itemName: state.storeReducer && state.storeReducer.itemName,
        totalPages: state.storeReducer.totalPages,
        allStoresData: state.storeReducer.allStoresData,
        storesTitle: state.multilingual.activeLanguageData.storesTitle,
        showStoresByItemId: state.storeReducer.showStoresByItemId,
        name: state.multilingual.activeLanguageData.name,
        address: state.multilingual.activeLanguageData.address,
        geofence: state.multilingual.activeLanguageData.geofence,
        action: state.multilingual.activeLanguageData.action,
        noDataStoresTitle: state.multilingual.activeLanguageData.noDataTitle.stores,
        itemsTitle: state.multilingual.activeLanguageData.items,
        newStoreTitle: state.multilingual.activeLanguageData.newStoreTitle,
        importCSVTitle: state.multilingual.activeLanguageData.csvTitle,
        activeLanguageLabel: state.multilingual.activeLanguageLabel,
        id: state.multilingual.activeLanguageData.id
    };
};

const connectedComponent = connect(mapStateToProps, {getAllStores, setEditItemBtn, resetRedux})(Store);
export default withRouter(connectedComponent);
