import React, {Component} from "react";
import {connect} from "react-redux";
import {NotificationManager} from "react-notifications";
import ReactPaginate from "react-paginate";
import {Row, Col} from "reactstrap";
import {withRouter} from "react-router-dom";

import {
    getPendingShopper,
    getShopperVehicleInfo,
    getAllPrograms,
    getVehicleTypes,
    getRevolutAccounts,
    resetRedux
} from "../../store/shopper/shopper-actions";

import Loader from "../../components/Loader/index";
import PendingShoppers from "../../components/PendingShoppers";
import ShowDetail from "../../components/ShowDetail";
import SimpleTable from "../../components/SimpleTable";

class ShopperPending extends Component {
    state = {
        pageSize: 10,
        totalPages: 0,
        currentPage: 0,
        showDetails: false
    };

    componentDidMount() {
        this.props.resetRedux();
        this.props.getPendingShopper(
            this.state.currentPage,
            this.state.pageSize,
            "asc",
            "firstName"
        );
        this.props.getAllPrograms();
        this.props.getVehicleTypes();
        this.props.getRevolutAccounts();
    }

    componentDidUpdate(prevProps) {
        const {pendingShopperData} = this.props;
        const {totalPages} = this.state;
        if (this.props !== prevProps) {
            if (pendingShopperData && totalPages !== pendingShopperData.totalPages)
                this.setState({totalPages: pendingShopperData.totalPages});
        }
    }

    showDetails = (value, data) => {
        this.setState({showDetails: value});
        if (data) {
            this.props.getShopperVehicleInfo(data.uuid);
        }
    };

    getSortedPendingShopper = (sortVal, sortField) => {
        let sortValue = "asc";
        if (sortVal === "Descending") {
            sortValue = "desc";
        }
        this.props.getPendingShopper(
            this.state.currentPage,
            this.state.pageSize,
            sortValue,
            sortField
        );
    };

    getSimpleTable = () => {
        const {
            processing,
            pendingShopperTitle,
            noDataPendingShopperTitle,
            pendingShopper,
            allPrograms,
            vehicleTypes,
            revolutAccounts
        } = this.props;

        if (processing) {
            return (
                <div style={{marginBottom: "200px"}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <SimpleTable
                        title={pendingShopperTitle}
                        onClick={false}
                        sortFunction={this.getSortedPendingShopper}
                        sortingDropdown={true}
                        noDataTitle={noDataPendingShopperTitle}
                        actionHeader={true}
                        showDetails={this.showDetails}
                        dataArray={pendingShopper}
                        state={this.state}
                        showAction={true}
                        revolutAccounts={revolutAccounts}
                        showApproveButton={true}
                        // programsArray={allPrograms} // MS: Redundant - Remove
                        // vehicleTypeArray={vehicleTypes} // MS: Redundant - Remove
                    />
                </>
            );
    };

    getMoreShopper = e => {
        this.setState({currentPage: e.selected}, () => {
            this.props.getPendingShopper(
                this.state.currentPage,
                this.state.pageSize,
                "asc",
                "firstName"
            );
        });
    };

    render() {
        const {shopperVehicleInfo, processing, approveFleetDetail} = this.props;
        const {showDetails, totalPages} = this.state;

        if (processing) {
            return <Loader/>;
        }
        return (
            <>
                <div className="content">
                    {/* <PendingShoppers /> */}
                    <Row>
                        {showDetails === true ? (
                            <Col md="12">
                                {/* Fetching User Vehicle Information */}
                                <ShowDetail
                                    title={this.props.shopperVehicleTitle}
                                    showDetails={this.showDetails}
                                    shopperVehicleInfo={shopperVehicleInfo}
                                    noDataTitle={this.props.noDetailsTitle}
                                />
                            </Col>
                        ) : (
                            <Col md="12">
                                {this.getSimpleTable()}
                                {totalPages > 1 ? (
                                    <ReactPaginate
                                        previousLabel={<i className="fa fa-angle-left"/>}
                                        nextLabel={<i className="fa fa-angle-right"/>}
                                        breakLabel={"..."}
                                        breakClassName={"break-me"}
                                        pageCount={totalPages}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        onPageChange={e => this.getMoreShopper(e)}
                                        containerClassName={"list-inline mx-auto justify-content-center pagination"}
                                        subContainerClassName={"list-inline-item pages pagination"}
                                        activeClassName={"active"}
                                    />
                                ) : null}
                            </Col>
                        )}
                    </Row>
                </div>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        processing: state.shopper.processing,
        pendingShopper: state.shopper.pendingShopper,
        pendingShopperData: state.shopper.pendingShopperData,
        totalPages: state.shopper.totalPages,
        allPrograms: state.shopper.allPrograms,
        shopperVehicleInfo: state.shopper.shopperVehicleInfo,
        vehicleTypes: state.shopper.vehicleTypes,
        message: state.shopper.message,
        noDetailsTitle: state.multilingual.activeLanguageData.noDataTitle.noDetailsTitle,
        noDataPendingShopperTitle: state.multilingual.activeLanguageData.noDataTitle.pendingShopper,
        pendingShopperTitle: state.multilingual.activeLanguageData.pendingShopper,
        shopperVehicleTitle: state.multilingual.activeLanguageData.shopperVehicleTitle,
        revolutAccounts: state.shopper.revolutAccounts,
        approveFleetDetail: state.shopper.approveFleetDetail
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getPendingShopper: (currentPage, pageSize, sort, field) => dispatch(getPendingShopper(currentPage, pageSize, sort, field)),
        getShopperVehicleInfo: uuid => dispatch(getShopperVehicleInfo(uuid)),
        getAllPrograms: () => dispatch(getAllPrograms()),
        getVehicleTypes: () => dispatch(getVehicleTypes()),
        getRevolutAccounts: () => dispatch(getRevolutAccounts()),
        resetRedux: () => dispatch(resetRedux())
    };
};

const connectedComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)(ShopperPending);
export default withRouter(connectedComponent);
