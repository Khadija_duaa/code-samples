import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Row, Col} from 'reactstrap';
import {withRouter} from 'react-router-dom';
import ReactPaginate from 'react-paginate';
import SimpleTable from '../../components/SimpleTable';

import {
    getApprovedShopper,
    getShopperVehicleInfo,
    resetRedux
} from '../../store/shopper/shopper-actions';
import {getStaffedStores} from '../../store/store/store-actions';

import Loader from '../../components/Loader/index';
import ShowDetail from '../../components/ShowDetail';
import storeReducer from '../../store/store/store-reducer';

class ShopperApproved extends Component {
    state = {
        pageSize: 10,
        totalPages: 0,
        currentPage: 0,
        showDetails: false
    };

    componentDidMount() {
        this.props.resetRedux();
        this.props.getApprovedShopper(
            this.state.currentPage,
            this.state.pageSize,
            'asc',
            'firstName'
        );
        this.props.getStaffedStores();
    }

    showDetails = (value, data) => {
        this.setState({showDetails: value});
        if (data) {
            this.props.getShopperVehicleInfo(data.uuid);
        }
    };

    getSortedApprovedShopper = (sortVal, sortField) => {
        let sortValue = 'asc';
        if (sortVal === 'Descending') {
            sortValue = 'desc';
        }
        this.props.getApprovedShopper(
            this.state.currentPage,
            this.state.pageSize,
            sortValue,
            sortField
        );
    };

    getSimpleTable = () => {
        if (this.props.processing) {
            return (
                <div style={{marginBottom: '200px'}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <SimpleTable
                        key={12}
                        title={this.props.approvedShopperTitle}
                        staffedStoresArray={this.props.staffedStores}
                        actionDropdown={true}
                        sortFunction={this.getSortedApprovedShopper}
                        sortingDropdown={true}
                        noDataTitle={this.props.noDataApprovedShopperTitle}
                        actionHeader={true}
                        showDetails={this.showDetails}
                        dataArray={this.props.approvedShopper}
                        showAction={true}
                        currentPage={this.state.currentPage}
                        pageSize={this.state.pageSize}
                        activeTypo={true}
                        showToggleButton={true}
                    />
                </>
            );
    };

    getMoreShopper = e => {
        this.setState({currentPage: e.selected}, () => {
            this.props.getApprovedShopper(
                this.state.currentPage,
                this.state.pageSize,
                'asc',
                'firstName'
            );
        });
    };

    render() {
        const {shopperVehicleInfo} = this.props;
        let approvedShopperData = this.props.approvedShopperData;
        if (approvedShopperData) {
            if (this.state.totalPages !== approvedShopperData.totalPages) {
                this.setState({totalPages: approvedShopperData.totalPages});
            }
        }
        return (
            <>
                <div className="content">
                    <Row>
                        {this.state.showDetails === true ? (
                            <Col md="12">
                                <ShowDetail
                                    title={'Details for Fleet '}
                                    showDetails={this.showDetails}
                                    shopperVehicleInfo={shopperVehicleInfo}
                                    noDataTitle={this.props.noDetailsTitle}
                                />
                            </Col>
                        ) : (
                            <Col md="12">
                                {this.getSimpleTable()}
                                {this.state.totalPages > 1 ? (
                                    <ReactPaginate
                                        previousLabel={<i className="fa fa-angle-left"/>}
                                        nextLabel={<i className="fa fa-angle-right"/>}
                                        breakLabel={'...'}
                                        breakClassName={'break-me'}
                                        pageCount={this.state.totalPages}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        onPageChange={e => this.getMoreShopper(e)}
                                        containerClassName={
                                            'list-inline mx-auto justify-content-center pagination'
                                        }
                                        subContainerClassName={'list-inline-item pages pagination'}
                                        activeClassName={'active'}
                                    />
                                ) : null}
                            </Col>
                        )}
                    </Row>
                </div>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        processing: state.shopper.processing,
        approvedShopper: state.shopper.approvedShopper,
        totalPages: state.shopper.totalPages,
        approvedShopperData: state.shopper.approvedShopperData,
        approvedShopperTitle: state.multilingual.activeLanguageData.approvedShopper,
        noDataApprovedShopperTitle:
        state.multilingual.activeLanguageData.noDataTitle.approvedShopper,
        shopperVehicleInfo: state.shopper.shopperVehicleInfo,
        shopperVehicleTitle:
        state.multilingual.activeLanguageData.shopperVehicleTitle,
        noDetailsTitle:
        state.multilingual.activeLanguageData.noDataTitle.noDetailsTitle,
        staffedStores: state.storeReducer.staffedStores
    };
};

const connectedComponent = connect(mapStateToProps, {
    getApprovedShopper,
    getShopperVehicleInfo,
    getStaffedStores,
    resetRedux
})(ShopperApproved);
export default withRouter(connectedComponent);
