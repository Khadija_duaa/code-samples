import React, {Component} from 'react';
import {
    Row,
    Col
} from 'reactstrap';
import DataListing from '../../components/DataListing';
import {getAllBrands, getItemsByBrandId, resetRedux, setAllItems} from '../../store/store/store-actions';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Loader from '../../components/Loader/index';
import ReactPaginate from 'react-paginate';

class Brands extends Component {

    state = {
        pageSize: 10,
        totalPages: 0,
        currentPage: 0,
        allBrands: []
    };

    componentDidMount() {
        this.props.resetRedux();
        this.props.getAllBrands(this.state.currentPage, this.state.pageSize);
    }

    onBrandDetailClick = (data) => {
        this.props.setAllItems(null);
        this.props.history.push('/admin/items/brand/' + data.uuid);
        // this.props.getItemsByBrandId(data.uuid, true, () => this.props.history.push('/admin/items'), data.name, 1, 10)
    };

    getSimpleTable = () => {
        const headers = [this.props.name, this.props.createdAt, this.props.action];

        if (this.props.processing) {
            return (
                <div style={{marginBottom: '200px'}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <DataListing title={this.props.brand}
                                 btnTitle={this.props.items}
                                 noDataTitle={this.props.noDataBrandsTitle}
                                 onBrandDetailClick={this.onBrandDetailClick}
                                 brands={true}
                                 headers={headers}
                                 dataArray={this.props.allBrands}
                    />
                </>
            )
    };

    getMoreBrands = (e) => {
        this.setState({currentPage: e.selected + 1}, () => {
                this.props.getAllBrands(this.state.currentPage, this.state.pageSize);
            }
        );
    };

    render() {
        let allBrandsData = this.props.allBrandsData;
        if (allBrandsData) {
            if (this.state.totalPages !== allBrandsData.totalPages) {
                this.setState({totalPages: allBrandsData.totalPages});
            }
        }
        return (
            <>
                <div className="content">
                    <Row>
                        <Col md="12">
                            {this.getSimpleTable()}
                            {
                                this.state.totalPages > 1 ? (
                                    <ReactPaginate
                                        previousLabel={<i className="fa fa-angle-left"/>}
                                        nextLabel={<i className="fa fa-angle-right"/>}
                                        breakLabel={'...'}
                                        breakClassName={'break-me'}
                                        pageCount={this.state.totalPages}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        onPageChange={(e) => this.getMoreBrands(e)}
                                        containerClassName={'list-inline mx-auto justify-content-center pagination'}
                                        subContainerClassName={'list-inline-item pages pagination'}
                                        activeClassName={'active'}
                                    />) : null
                            }
                        </Col>
                    </Row>
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.storeReducer.processing,
        allBrands: state.storeReducer.allBrands,
        totalPages: state.storeReducer.totalPages,
        allBrandsData: state.storeReducer.allBrandsData,
        name: state.multilingual.activeLanguageData.name,
        createdAt: state.multilingual.activeLanguageData.createdAt,
        action: state.multilingual.activeLanguageData.action,
        noDataBrandsTitle: state.multilingual.activeLanguageData.noDataTitle.brands,
        items: state.multilingual.activeLanguageData.items,
        brand: state.multilingual.activeLanguageData.brand
    }
};

const connectedComponent = connect(mapStateToProps, {getAllBrands, getItemsByBrandId, resetRedux, setAllItems})(Brands);
export default withRouter(connectedComponent);
