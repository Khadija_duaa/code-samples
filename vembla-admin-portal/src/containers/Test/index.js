import React, {Component} from 'react';


class Test extends Component {

    componentDidMount() {
        console.log(this.props.match.params);
    }

    render() {
        console.log("THIS");
        return (
            <h1>
                {this.props.match.params.id}
            </h1>
        )
    }
}

export default Test;