import React, { Component } from "react";
import { connect } from "react-redux";
import { NotificationManager } from "react-notifications";
import { Row, Col, Button } from "reactstrap";

import { fetchCoupons } from "../../store/coupon/coupon-actions";

import CouponForm from "../../components/CouponForm";
import CouponsListing from "../../components/CouponListing";
import CustomFormModal from "../../components/CustomComponents/CustomFormModal";
import Loader from "../../components/Loader";

class Coupon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showFormModal: false
    };
  }
  componentDidMount() {
    this.props.fetchCoupons();
  }

  componentDidUpdate(prevProps) {
    const { isSuccess, resMsg, fetchCoupons } = this.props;
    if (prevProps.isSuccess !== isSuccess) {
      if (isSuccess) {
        NotificationManager.success("", resMsg);
        this.setState({ showFormModal: false });
        fetchCoupons();
      } else if (isSuccess !== null && !isSuccess) {
        // Do not use simple else that would run for null too
        NotificationManager.error("", resMsg);
      }
    }
  }

  render() {
    const { showFormModal } = this.state;
    const { isProcessing } = this.props;

    if (isProcessing) {
      return <Loader />;
    }
    return (
      <div className="content">
        {showFormModal && (
          <CustomFormModal
            showModal={showFormModal}
            element={<CouponForm />}
            onCancel={() => this.setState({ showFormModal: false })}
            label={"Coupon Form"}
          />
        )}
        <Row>
          <Col md={{ size: 2, offset: 10 }}>
            <Button onClick={() => this.setState({ showFormModal: true })}>
              Add Coupon
            </Button>
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <CouponsListing />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isProcessing: state.coupon.processing,
  isSuccess: state.coupon.isSuccess,
  resMsg: state.coupon.responseMsg
});

const mapDispatchToProps = dispatch => {
  return {
    fetchCoupons: () => dispatch(fetchCoupons())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Coupon);
