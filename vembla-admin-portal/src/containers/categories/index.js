import React, {Component} from 'react';
import {
    Row,
    Col, Modal, ModalHeader, ModalBody, ModalFooter, Button
} from 'reactstrap';
import DataListing from '../../components/DataListing';
import {
    getAllCategories,
    getItemsByCategoryId,
    setPagination,
    resetRedux,
    setAllItems,
    deleteCategory, setCategoryData
} from '../../store/store/store-actions';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Loader from '../../components/Loader/index';
import ReactPaginate from 'react-paginate';
import CategoryForm from '../../components/CategoryForm';

class Categories extends Component {

    state = {
        pageSize: 10,
        totalPages: 0,
        currentPage: 0,
        allCategories: [],
        showCategoryForm: false,
        categoryData: null
    };

    componentDidMount() {
        this.props.resetRedux();
        this.props.getAllCategories(this.state.currentPage, this.state.pageSize);
    }

    onDeleteIconClick = (categoryData, index) => {
        this.openDeleteModal({id: categoryData.uuid, index});
    };
    openDeleteModal = (categoryData) => this.setState({
        deleteModalOpen: !this.state.deleteModalOpen,
        categoryData: categoryData
    });

    getModalContent = () => {
        return (
            <Modal isOpen={this.state.deleteModalOpen} toggle={this.openDeleteModal}
                   className={'modal-danger ' + this.props.className}>
                <ModalHeader toggle={this.openDeleteModal}>
                    Are you sure ?
                </ModalHeader>
                <ModalBody>
                    Are you sure you want to delete this Category ?
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary"
                            onClick={() => {
                                let {categoryData} = this.state;
                                this.props.deleteCategory(categoryData.id, categoryData.index);
                                this.openDeleteModal(null);
                            }}
                    >
                        Delete
                    </Button>
                </ModalFooter>
            </Modal>
        );
    };


    onCategoryDetailClick = (check, data) => {
        console.log("DAAT^A: ", data);
        this.props.setAllItems(null);
        this.props.history.push('/admin/items/category/' + data.uuid);
        //this.props.getItemsByCategoryId(data.uuid, true, () => this.props.history.push('/admin/items'), data.name, 1, 10)
    };

    onCategoryEditClick = (value, categoryData) => {
        this.setState({showCategoryForms: value});
        this.props.setCategoryData(categoryData);
    };

    getSimpleTable = () => {
        const headers = [this.props.name, this.props.colorCode, this.props.action];

        if (this.props.processing) {
            return (
                <div style={{marginBottom: '200px'}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <DataListing title={this.props.categories}
                                 btnTitle={this.props.items}
                                 noDataTitle={this.props.noDataCategoriesTitle}
                                 onCategoryDetailClick={this.onCategoryDetailClick}
                                 categories={true}
                                 headers={headers}
                                 button={true}
                                 topBtnTitle={"Create"}
                                 showCategoryForm={this.showCategoryForm}
                                 deleteBtnTitle={"Delete"}
                                 onCategoryEditClick={this.onCategoryEditClick}
                                 onDeleteBtnClick={this.onDeleteIconClick}
                                 dataArray={this.props.activeCategories}/>
                </>
            )
    };

    getMoreItems = (e) => {
        this.setState({currentPage: e.selected + 1}, () => {
                this.props.setPagination(this.state.currentPage);
            }
        );
    };

    showCategoryForm = (value, categoryData) => {
        this.setState({showCategoryForm: value});
        this.props.setCategoryData(categoryData);
    };

    render() {
        return (
            <>
                {this.getModalContent()}
                <div className="content">
                    {
                        this.state.showCategoryForm ?
                            <CategoryForm showCategoryForm={this.showCategoryForm}
                                          allCategories={this.props.allCategories}/> :
                            <Row>
                                <Col md="12">
                                    {this.getSimpleTable()}
                                    {
                                        this.props.totalPages > 1 ? (
                                            <ReactPaginate
                                                previousLabel={<i className="fa fa-angle-left"/>}
                                                nextLabel={<i className="fa fa-angle-right"/>}
                                                breakLabel={'...'}
                                                breakClassName={'break-me'}
                                                pageCount={this.props.totalPages}
                                                marginPagesDisplayed={2}
                                                pageRangeDisplayed={5}
                                                onPageChange={(e) => this.getMoreItems(e)}
                                                containerClassName={'list-inline mx-auto justify-content-center pagination'}
                                                subContainerClassName={'list-inline-item pages pagination'}
                                                activeClassName={'active'}
                                            />) : null
                                    }
                                </Col>
                            </Row>
                    }
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.storeReducer.processing,
        activeCategories: state.storeReducer.activeCategories,
        allCategories: state.storeReducer.allCategories,
        totalPages: state.storeReducer.totalPages,
        name: state.multilingual.activeLanguageData.name,
        colorCode: state.multilingual.activeLanguageData.colorCode,
        categories: state.multilingual.activeLanguageData.categories,
        action: state.multilingual.activeLanguageData.action,
        noDataCategoriesTitle: state.multilingual.activeLanguageData.noDataTitle.categories,
        items: state.multilingual.activeLanguageData.items
    }
};

const connectedComponent = connect(mapStateToProps, {
    getAllCategories,
    getItemsByCategoryId,
    setPagination,
    resetRedux,
    setCategoryData,
    setAllItems,
    deleteCategory
})(Categories);
export default withRouter(connectedComponent);
