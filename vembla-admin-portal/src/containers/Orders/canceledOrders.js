import React, {Component} from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {
    Row,
    Col
} from 'reactstrap';
import {getCancelOrders, resetRedux} from "../../store/order/order-actions";

import OrdersListing from "../../components/OrdersListing";
import Loader from "../../components/Loader/index";

class CanceledOrders extends Component {
    state = {
        pageSize: 10,
        totalPages: 0,
        currentPage: 0
    };

    componentDidMount() {
        const {resetRedux, getCancelOrders} = this.props;
        const {currentPage, pageSize} = this.state;
        resetRedux();
        getCancelOrders(currentPage, pageSize);
    }

    componentDidUpdate(prevProps) {
        const {cancelOrderData} = this.props;
        const {totalPages} = this.state;
        if (cancelOrderData !== prevProps.cancelOrderData) {
            if (cancelOrderData && totalPages !== cancelOrderData.pageCount) {
                this.setState({totalPages: cancelOrderData.pageCount});
            }
        }
    }

    getSimpleTable = () => {
        const {
            orderNo,
            deliveredToTitle,
            amountReceivedTitle,
            phone,
            storeTitle,
            deliveredAddress,
            processing,
            cancelOrder,
            cancelOrderTitle,
            noCancelOrderTitle
        } = this.props;

        const {totalPages, currentPage} = this.state;
        const deliveredOrdersheaders = [orderNo, deliveredToTitle, amountReceivedTitle, phone, storeTitle, deliveredAddress];

        if (processing) {
            return (
                <div style={{marginBottom: "200px"}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <OrdersListing
                        title={cancelOrderTitle}
                        noOrdersFoundMsg={noCancelOrderTitle}
                        headers={deliveredOrdersheaders}
                        ordersList={cancelOrder}
                        orderDetail={false}
                        totalPages={totalPages}
                        currentPage={currentPage}
                        getMoreOrders={this.getMoreOrders}
                        isDeliveredOrder={true}
                    />
                </>
            );
    };

    getMoreOrders = e => {
        this.setState({currentPage: e.selected}, () => {
            this.props.getCancelOrders(this.state.currentPage, this.state.pageSize);
        });
    };

    render() {
        return (
            <>
                <div className="content">
                    <Row>
                        <Col md={"12"}>
                            {this.getSimpleTable()}
                        </Col>
                    </Row>
                </div>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        processing: state.order.processing,
        cancelOrder: state.order.cancelOrder,
        cancelOrderData: state.order.cancelOrderData,
        totalPages: state.order.totalPages,
        cancelOrderTitle: state.multilingual.activeLanguageData.cancelOrderTitle,
        noCancelOrderTitle: state.multilingual.activeLanguageData.noDataTitle.noCancelOrderTitle,
        phone: state.multilingual.activeLanguageData.phone,
        orderNo: state.multilingual.activeLanguageData.orderNo,
        deliveredToTitle: state.multilingual.activeLanguageData.deliveredToTitle,
        amountReceivedTitle: state.multilingual.activeLanguageData.amountReceivedTitle,
        deliveredAddress: state.multilingual.activeLanguageData.deliveredAddress,
        storeTitle: state.multilingual.activeLanguageData.storeTitle
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getCancelOrders: (currentPage, pageSize) => dispatch(getCancelOrders(currentPage, pageSize)),
        resetRedux: () => dispatch(resetRedux())
    };
};

const connectedComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)(CanceledOrders);
export default withRouter(connectedComponent);
