import React, {Component} from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {
    Row,
    Col
} from 'reactstrap';
import {getOrdersHistory, resetRedux} from "../../store/order/order-actions";

import OrdersListing from "../../components/OrdersListing";
import Loader from "../../components/Loader/index";

class Orders extends Component {
    state = {
        pageSize: 10,
        totalPages: 0,
        currentPage: 0
    };

    componentDidMount() {
        const {resetRedux, getOrdersHistory} = this.props;
        const {currentPage, pageSize} = this.state;
        resetRedux();
        getOrdersHistory(currentPage, pageSize);
    }

    componentDidUpdate(prevProps) {
        const {orderHistoryData} = this.props;
        const {totalPages} = this.state;
        if (orderHistoryData !== prevProps.orderHistoryData) {
            if (orderHistoryData && totalPages !== orderHistoryData.pageCount) {
                this.setState({totalPages: orderHistoryData.pageCount});
            }
        }
    }

    getSimpleTable = () => {
        const {
            orderNo,
            deliveredToTitle,
            amountReceivedTitle,
            phone,
            storeTitle,
            deliveredAddress,
            processing,
            deliveredOrderTitle,
            noDeliveredOrderTitle,
            orderHistory
        } = this.props;

        const {totalPages, currentPage} = this.state;

        const deliveredOrdersheaders = [
            orderNo,
            deliveredToTitle,
            amountReceivedTitle,
            phone,
            storeTitle,
            deliveredAddress
        ];

        if (processing) {
            return (
                <div style={{marginBottom: "200px"}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <OrdersListing
                        title={deliveredOrderTitle}
                        noOrdersFoundMsg={noDeliveredOrderTitle}
                        headers={deliveredOrdersheaders}
                        ordersList={orderHistory}
                        orderDetail={true}
                        totalPages={totalPages}
                        currentPage={currentPage}
                        getMoreOrders={this.getMoreOrders}
                        isDeliveredOrder={true}
                    />
                </>
            );
    };

    getMoreOrders = e => {
        this.setState({currentPage: e.selected}, () => {
            this.props.getOrdersHistory(this.state.currentPage, this.state.pageSize);
        });
    };

    render() {
        return (
            <>
                <div className="content">
                    <Row>
                        <Col md={"12"}>
                            {this.getSimpleTable()}
                        </Col>
                    </Row>
                </div>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        processing: state.order.processing,
        orderHistory: state.order.orderHistory,
        totalPages: state.order.totalPages,
        orderHistoryData: state.order.orderHistoryData,

        deliveredOrderTitle: state.multilingual.activeLanguageData.deliveredOrderTitle,
        noDeliveredOrderTitle: state.multilingual.activeLanguageData.noDataTitle.noDeliveredOrderTitle,
        phone: state.multilingual.activeLanguageData.phone,
        orderNo: state.multilingual.activeLanguageData.orderNo,
        deliveredToTitle: state.multilingual.activeLanguageData.deliveredToTitle,
        amountReceivedTitle: state.multilingual.activeLanguageData.amountReceivedTitle,
        deliveredAddress: state.multilingual.activeLanguageData.deliveredAddress,
        storeTitle: state.multilingual.activeLanguageData.storeTitle
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getOrdersHistory: (currentPage, pageSize) => dispatch(getOrdersHistory(currentPage, pageSize)),
        resetRedux: () => dispatch(resetRedux())
    };
};

const connectedComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)(Orders);
export default withRouter(connectedComponent);
