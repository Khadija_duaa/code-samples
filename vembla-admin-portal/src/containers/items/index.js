import React, {Component} from "react";
import {connect} from "react-redux";
import ReactPaginate from "react-paginate";
import {Row, Col} from "reactstrap";
import {withRouter} from "react-router-dom";

import {
    getAllItems,
    getStoresByItemId,
    getSingleItemDetail,
    searchItems,
    importItems,
    getStoreData,
    getItemsByCategoryId,
    getItemsByBrandId,
    resetRedux, getStoreByUUID,
    setStoreData,
    getBrandById,
    getCategoryById,
} from "../../store/store/store-actions";

import DataListing from "../../components/DataListing";
import ItemEditForm from "../../components/ItemEditForm";
import ItemDetail from "../../components/ItemDetail";
import ItemsListing from "../../components/ItemsListing";
import Loader from "../../components/Loader/index";

let pageTitle;
let storeUUID;
let categoryUUID;
let brandUUID;
let showSearch = true;

class Items extends Component {
    state = {
        showItemsByCategory: false,
        showItemsByBrand: false,
        showUniqueStoreItems: false,
        showItemForm: false,
        showItemDetail: false,
        pageSize: 10,
        totalPages: 0,
        currentPage: 0,
        allItems: [],
        itemDetailData: ""
    };

    componentDidMount() {
        const params = this.props.match.params;
        switch (params.type) {
            case "store" :
                this.props.getStoreByUUID(params.uuid, (err) => {
                    if (!err) {
                        this.props.getStoreData(params.uuid, true, () => {
                            this.setState({
                                allItems: this.props.allItems,
                                showUniqueStoreItems: true
                            });
                        }, 1, 10)
                    }
                });
                break;
            case 'brand' :
                this.props.getBrandById(params.uuid, (err) => {
                    if (!err) {
                        this.props.getItemsByBrandId(params.uuid, true, () => {
                            this.setState({
                                allItems: this.props.allItems,
                                showItemsByBrand: true
                            });
                        }, this.props.singleBrand.name, 1, 10)
                    }
                });
                break;
            case "category" :
                this.props.getCategoryById(params.uuid, (err) => {
                    if (!err) {
                        this.props.getItemsByCategoryId(params.uuid, true, () => {
                            this.setState({
                                allItems: this.props.allItems,
                                showItemsByCategory: true
                            });
                        }, this.props.singleCategory.name, 1, 10)
                    }
                });
                break;
            default:
                this.props.getAllItems(this.state.currentPage, this.state.pageSize);
                break;
        }


        // this.props.resetRedux();
        // if (this.props.showUniqueStoreItems === true) {
        //     this.setState({
        //         allItems: this.props.allItems,
        //         showUniqueStoreItems: true
        //     });
        // } else if (this.props.showItemsByCategory === true) {
        //     this.setState({
        //         allItems: this.props.allItems,
        //         showItemsByCategory: true
        //     });
        // } else if (this.props.showItemsByBrand === true) {
        //     this.setState({
        //         allItems: this.props.allItems,
        //         showItemsByBrand: true
        //     });
        // } else {
        //     this.props.getAllItems(this.state.currentPage, this.state.pageSize);
        // }
    }

    // shouldComponentUpdate(nextProps) {
    //     if (nextProps.allItems && nextProps.allItems.length > 0) {
    //         return true;
    //     } else if (nextProps.storeData && nextProps.storeData !== null) {
    //         pageTitle = `${nextProps.items} for Store : ${nextProps.storeData.name}`;
    //         return true;
    //     } else return false;
    // }

    onItemDetailClick = data => {
        this.props.getStoresByItemId(
            data.uuid,
            true,
            () => this.props.history.push("/admin/store"),
            data.name
        );
    };

    onItemDetail = data => {
        this.setState({
            showItemForm: true,
            showItemDetail: true,
            itemDetailData: data
        });
    };

    onEditItemClick = (itemData, value) => {
        if (itemData) {
            this.props.getSingleItemDetail(itemData.uuid, () =>
                this.showItemEditForm(value)
            );
        }
    };

    getSimpleTable = () => {
        let test = this.props.allItems;
        const headers = [
            "Icon",
            this.props.name,
            this.props.category,
            this.props.itemID,
            this.props.brand,
            "GTIN",
            this.props.action
        ];
        if (this.props.processing) {
            return (
                <div style={{marginBottom: "200px"}}>
                    <Loader/>
                </div>
            );
        } else
            return (
                <>
                    <DataListing
                        title={pageTitle}
                        button={true}
                        topBtnTitle={this.props.csvTitle}
                        onEditItemClick={this.onEditItemClick}
                        itemEditBtn={this.props.itemEditBtn}
                        btnTitle={this.props.storesTitle}
                        noDataTitle={this.props.noDataItemsTitle}
                        onItemDetailClick={this.onItemDetailClick}
                        actionTitle={this.props.action}
                        priceTitle={this.props.price}
                        items={true}
                        headers={headers}
                        search={showSearch}
                        searchAPI={this.props.searchItems}
                        onItemDetail={this.onItemDetail}
                        importItems={this.props.importItems}
                        showItemEditForm={this.showItemEditForm}
                        dataArray={
                            !(
                                this.state.showUniqueStoreItems ||
                                this.state.showItemsByCategory ||
                                this.state.showItemsByBrand
                            )
                                ? this.props.allItems
                                : this.state.allItems
                        }
                    />
                </>
            );
    };

    getMoreItems = e => {
        if (this.state.showUniqueStoreItems) {
            this.setState({currentPage: e.selected + 1}, () => {
                this.props.getStoreData(
                    storeUUID,
                    "",
                    () => {
                        this.setState({showUniqueStoreItems: false});
                    },
                    this.state.currentPage,
                    10
                );
            });
        } else if (this.state.showItemsByCategory) {
            this.setState({currentPage: e.selected + 1}, () => {
                this.props.getItemsByCategoryId(
                    categoryUUID,
                    "",
                    () => this.setState({showItemsByCategory: false}),
                    pageTitle,
                    this.state.currentPage,
                    10
                );
            });
        } else if (this.state.showItemsByBrand) {
            this.setState({currentPage: e.selected + 1}, () => {
                this.props.getItemsByBrandId(
                    brandUUID,
                    "",
                    () => this.setState({showItemsByBrand: false}),
                    pageTitle,
                    this.state.currentPage,
                    10
                );
            });
        } else {
            this.setState({currentPage: e.selected + 1}, () => {
                this.props.getAllItems(this.state.currentPage, this.state.pageSize);
            });
        }
    };

    showItemEditForm = value => {
        this.setState({showItemForm: value});
    };

    showItemDetailData = () => {
        this.setState({showItemForm: false, showItemDetail: false});
    };

    render() {
        let allItemsData = this.props.allItemsData;
        if (allItemsData) {
            if (this.state.totalPages !== allItemsData.totalPages) {
                this.setState({totalPages: allItemsData.totalPages});
            }
        }
        pageTitle = this.props.items;
        if (this.props.storeData) {
            pageTitle = `${this.props.items} for Store : ${this.props.storeData.name}`;
            storeUUID = this.props.storeData.uuid;
            showSearch = false;
        }
        if (this.props.categoryName) {
            pageTitle = `${this.props.items} for Category : ${this.props.categoryName}`;
            categoryUUID = this.props.categoryUUID;
            showSearch = false;
        }
        if (this.props.brandName) {
            pageTitle = `${this.props.items} for Brand : ${this.props.brandName}`;
            brandUUID = this.props.brandUUID;
            showSearch = false;
        }

        return (
            <>
                <div className="content">
                    {this.state.showItemForm ? (
                        <Row>
                            <Col md={12}>
                                {this.state.showItemDetail ? (
                                    <ItemDetail
                                        showItemDetailData={this.showItemDetailData}
                                        data={this.state.itemDetailData}
                                    />
                                ) : (
                                    <ItemEditForm showItemEditForm={this.showItemEditForm}/>
                                )}
                            </Col>
                        </Row>
                    ) : (
                        <Row>
                            <Col md="12">
                                {this.getSimpleTable()}
                                {this.state.totalPages > 1 ? (
                                    <ReactPaginate
                                        previousLabel={<i className="fa fa-angle-left"/>}
                                        nextLabel={<i className="fa fa-angle-right"/>}
                                        breakLabel={"..."}
                                        breakClassName={"break-me"}
                                        pageCount={this.state.totalPages}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        onPageChange={e => this.getMoreItems(e)}
                                        containerClassName={"list-inline mx-auto justify-content-center pagination"}
                                        subContainerClassName={"list-inline-item pages pagination"}
                                        activeClassName={"active"}
                                    />
                                ) : null}
                            </Col>
                        </Row>
                    )}
                </div>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        processing: state.storeReducer.processing,
        allItems: state.storeReducer.allItems,
        totalPages: state.storeReducer.totalPages,
        allItemsData: state.storeReducer.allItemsData,
        name: state.multilingual.activeLanguageData.name,
        category: state.multilingual.activeLanguageData.category,
        itemID: state.multilingual.activeLanguageData.itemID,
        brand: state.multilingual.activeLanguageData.brand,
        showUniqueStoreItems: state.storeReducer.showUniqueStoreItems,
        createdAt: state.multilingual.activeLanguageData.createdAt,
        items: state.multilingual.activeLanguageData.items,
        action: state.multilingual.activeLanguageData.action,
        showItemsByCategory: state.storeReducer.showItemsByCategory,
        showItemsByBrand: state.storeReducer.showItemsByBrand,
        noDataItemsTitle: state.multilingual.activeLanguageData.noDataTitle.items,
        storesTitle: state.multilingual.activeLanguageData.storesTitle,
        price: state.multilingual.activeLanguageData.price,
        itemEditBtn: state.storeReducer.itemEditBtn,
        storeData: state.storeReducer.storeData,
        categoryName: state.storeReducer.categoryName,
        brandName: state.storeReducer.brandName,
        csvTitle: state.multilingual.activeLanguageData.csvTitle,
        categoryUUID: state.storeReducer.categoryUUID,
        brandUUID: state.storeReducer.brandUUID,
        singleCategory: state.storeReducer.singleCategory,
        singleBrand: state.storeReducer.singleBrand
    };
};

const connectedComponent = connect(
    mapStateToProps,
    {
        getAllItems,
        getStoresByItemId,
        getSingleItemDetail,
        searchItems,
        importItems,
        getStoreData,
        getItemsByCategoryId,
        getItemsByBrandId,
        resetRedux,
        getStoreByUUID,
        setStoreData,
        getBrandById,
        getCategoryById
    }
)(Items);
export default withRouter(connectedComponent);
