import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Container,
  CardBody,
  Input,
  CardGroup,
  Card,
  Form,
  Row,
  Col,
  Button
} from "reactstrap";

import { login } from "../../store/user/user-actions";

import Loader from "../../components/Loader/index";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.userAuthenticated) {
      this.props.history.push("/admin/dashboard");
    }
  }

  onInputChange = e => {
    let state = { ...this.state };
    state[e.target.name] = e.target.value;
    this.setState(state);
  };

  onFormSubmit = e => {
    e.preventDefault();
    this.props.login(this.state.username, this.state.password);
  };

  usernameField = () => {
    return (
      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fas fa-user" />
          </InputGroupText>
        </InputGroupAddon>
        <Input
          type="email"
          placeholder="Username"
          name="username"
          onChange={this.onInputChange}
          autoComplete="username"
        />
      </InputGroup>
    );
  };

  passwordField = () => {
    return (
      <InputGroup className="mb-4">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fas fa-unlock-alt" />
          </InputGroupText>
        </InputGroupAddon>
        <Input
          type="password"
          placeholder="Password"
          autoComplete="current-password"
          onChange={this.onInputChange}
          name="password"
        />
      </InputGroup>
    );
  };

  form = () => {
    return (
      <Form>
        <h1 style={{ textAlign: "center" }}>{this.props.loginTitle}</h1>

        <p
          className="text-muted"
          style={{
            textAlign: "center",
            marginBottom: "14px",
            fontWeight: "600"
          }}
        >
          {this.props.vemblaAdmin}
        </p>

        {this.props.processing ? (
          <div style={{ marginBottom: "5%" }}>
            <Loader style={{ marginTop: "5%" }} />
          </div>
        ) : null}

        {this.props.processing ? null : this.props.error ? (
          <div className={"error-style"}>{this.props.error}</div>
        ) : null}

        {this.usernameField()}
        {this.passwordField()}

        <Row>
          <Col xs="6" md="12" style={{ textAlign: "center" }}>
            <Button
              id="Popover1"
              type="submit"
              disabled={!this.state.username || !this.state.password}
              onClick={this.onFormSubmit}
              color="primary"
              className="px-4"
            >
              {this.props.loginTitle}
            </Button>
          </Col>
        </Row>
      </Form>
    );
  };

  render() {
    return (
      <div
        className="app flex-row align-items-center"
        style={{ height: "-webkit-fill-available" }}
      >
        <Container>
          <Row className="justify-content-center">
            <Col xs="4">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>{this.form()}</CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    userAuthenticated: state.user.authenticated,
    processing: state.user.processing,
    error: state.user.error,
    loginTitle: state.multilingual.activeLanguageData.loginTitle,
    vemblaAdmin: state.multilingual.activeLanguageData.vemblaAdmin
  };
};

const connectedComponent = connect(mapStateToProps, { login })(Login);
export default withRouter(connectedComponent);
