import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import store from './store/store';
import { Provider } from 'react-redux';
import connectSocket from './socket-io/socket-io';

import './assets/scss/black-dashboard-react.scss';
import './assets/demo/demo.css';
import './assets/css/nucleo-icons.css';
import 'react-notifications/lib/notifications.css';

export const socket = connectSocket();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
