import React, {Component} from "react";
import {Router, Route, Switch, Redirect} from "react-router-dom";
import shortid from "shortid";
import AdminLayout from "./layouts/Admin/Admin.jsx";
import RTLLayout from "./layouts/RTL/RTL.jsx";
import {createBrowserHistory} from "history";
import Login from "./containers/login";
import FleetManagement from "./containers/fleetManagement";
import {NotificationContainer} from "react-notifications";
import {connect} from "react-redux";
import StoreForm from "./components/StoreForm";
import Dashboard from "./layouts/views/Dashboard.jsx";

import "./app.css";
import "react-flags-select/css/react-flags-select.css";
import "react-tagsinput/react-tagsinput.css";
import "react-datepicker/dist/react-datepicker.css";

const hist = createBrowserHistory();

class App extends Component {
    // getAuthRedirect = () => {
    //   return this.props.authenticated ? (
    //     <Redirect to={"/admin/dashboard"} />
    //   ) : (
    //     <Redirect to={"/login"} />
    //   );
    // };

    getDashboard = () => {
        let jsx = [];
        if (this.props.authenticated) {
            jsx.push(
                <Route
                    key={shortid.generate()}
                    path="/admin/fleet-management"
                    render={props => <FleetManagement {...props} />}
                />,
                <Route
                    key={shortid.generate()}
                    path="/admin"
                    render={props => <AdminLayout {...props} />}
                />,
                <Route
                    key={shortid.generate()}
                    path="/rtl"
                    render={props => <RTLLayout {...props} />}
                />
            );
        }
        return jsx;
    };

    render() {
        const {authenticated} = this.props;
        return (
            <div>
                <Router history={hist}>
                    <>
                        <Switch>
                            {this.getDashboard()}
                            {authenticated && <Redirect to={"/admin/dashboard"}/>}
                            <Route
                                key={shortid.generate()}
                                path="/login"
                                name="Login"
                                component={Login}
                            />
                            {!authenticated && <Redirect to={"/login"}/>}
                            {/* {this.getAuthRedirect()} */}
                            {/*<Route*/}
                                {/*key={shortid.generate()}*/}
                                {/*path="/admin/store/:id"*/}
                                {/*name="Store"*/}
                                {/*component={StoreForm}*/}
                            {/*/>*/}
                        </Switch>
                        <NotificationContainer/>
                    </>
                </Router>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        authenticated: state.user.authenticated
    };
};

export default connect(mapStateToProps)(App);
