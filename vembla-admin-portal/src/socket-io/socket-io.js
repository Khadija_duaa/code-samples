import io from 'socket.io-client';
import * as actions from './socket-actions';
import {setOnlineFleet, setBusyFleet, setOfflineFleet} from '../store/fleet/fleet-actions';
import store from '../store/store';

const connectSocket = () => {

    const socket = io(actions.SOCKET_URL, {
        transports: ['websocket']
    });

    socket.on(actions.NEW_CONNECTION, () => {
        console.log("Socket Established!!!");
    });

    socket.on(actions.DISCONNECTION, () => {
        console.log('Socket Disconnected!!!');
    });

    socket.on(actions.SOCKET_AVAILABLE_ONLINE_FLEET, (data) =>{
        console.log("Online Fleet Data:", data);
        const {dispatch} = store;
        dispatch(setOnlineFleet(data))
    });

    socket.on(actions.SOCKET_AVAILABLE_BUSY_FLEET, (data) =>{
        console.log("Busy Fleet Data:", data);
        const {dispatch} = store;
        dispatch(setBusyFleet(data))
    });

    socket.on(actions.SOCKET_AVAILABLE_OFFLINE_FLEET, (data) =>{
        console.log("Offline Fleet Data:", data);
        const {dispatch} = store;
        dispatch(setOfflineFleet(data))
    });

    return socket;
};

export default connectSocket;