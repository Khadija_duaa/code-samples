// export const SOCKET_URL = 'http://192.168.1.69:7001';
// export const SOCKET_URL = 'http://58.27.238.210:8004';
// export const SOCKET_URL = 'http://ec2-13-53-40-162.eu-north-1.compute.amazonaws.com:7001/';
// export const SOCKET_URL = "https://prod.vembla.se:5968";
// export const SOCKET_URL = "https://demo.vembla.se:7001";
export const SOCKET_URL = 'http://192.168.1.66:7001';

export const NEW_CONNECTION = "connect";
export const DISCONNECTION = "disconnect";
export const SOCKET_ACTION_JOIN_ROOM = "JOIN_MY_ROOM";
export const SOCKET_AVAILABLE_ONLINE_FLEET = "AVAILABLE_ONLINE_FLEET";
export const SOCKET_AVAILABLE_BUSY_FLEET = "AVAILABLE_BUSY_FLEET";
export const SOCKET_AVAILABLE_OFFLINE_FLEET = "AVAILABLE_OFFLINE_FLEET";
