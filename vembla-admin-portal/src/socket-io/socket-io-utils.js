import {socket} from "../index";
import * as actions from './socket-actions';

export const joinRoom = (jwt) => {
    socket.emit(actions.SOCKET_ACTION_JOIN_ROOM, {jwt}, (status) => {
        if (status === 401) {
            console.log("UNABLE TO JOIN ROOM");
        } else {
            console.log("Successfully joined room");
        }
    });
};