import {NotificationManager} from "react-notifications";
import * as actions from "./fleet-action-types";
import axios from "../../utils/axios";
import {ASSIGN_ORDER_MANUALLY} from "../../utils/config";

const setProcessing = processing => {
    return {
        type: actions.PROCESSING,
        payload: processing
    };
};

export const setMapData = (key, uuid, click) => {
    return {
        type: actions.SET_MAP_DATA,
        payload: {
            key: key,
            uuid: uuid,
            click: click
        }
    };
};

const mapFleetData = user => {
    return {
        ...user,
        location: {
            lng: user.currentLocation.coordinates[0],
            lat: user.currentLocation.coordinates[1]
        }
    };
};

const setActiveFleet = payload => {
    return {
        type: actions.SET_ACTIVE_FLEET,
        payload
    };
};

const _setBusyFleet = payload => {
    return {
        type: actions.SET_BUSY_FLEET,
        payload
    };
};

const _setOfflineFleet = payload => {
    return {
        type: actions.SET_OFFLINE_FLEET,
        payload
    };
};

const setFreeRiders = payload => {
    return {
        type: actions.SET_FREE_RIDERS,
        payload
    };
};

export const setOnlineFleet = data => {
    return dispatch => {
        let preparedData = data.map(mapFleetData);
        let freeRiders = data.filter(
            rider => rider.idle && rider.storeId === null
        );


        dispatch(setActiveFleet(preparedData));
        dispatch(setFreeRiders(freeRiders));
    };
};

export const setBusyFleet = data => {
    return dispatch => {
        let preparedData = data.map(mapFleetData);
        dispatch(_setBusyFleet(preparedData));
    };
};

export const setOfflineFleet = data => {
    return dispatch => {
        let preparedData = data.map(mapFleetData);
        dispatch(_setOfflineFleet(preparedData));
    };
};

export const assignOrderManually = (riderUUID, orderUUID) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios.post(ASSIGN_ORDER_MANUALLY + riderUUID, {orderId: orderUUID})
            .then(res => {
                NotificationManager.success("Order Assigned Successfully", "", 3000);
                dispatch(setProcessing(false));
            })
            .catch(err => {
                NotificationManager.error("Some Error Occured", "", 3000);
                dispatch(setProcessing(false));
            });
    };
};
