import {
  PROCESSING,
  SET_MAP_DATA,
  SET_ACTIVE_FLEET,
  SET_FREE_RIDERS,
  SET_BUSY_FLEET,
  SET_OFFLINE_FLEET
} from "./fleet-action-types";

const initState = {
  processing: false,
  mapData: "",
  click: false,
  activeFleet: [],
  busyFleet: [],
  offlineFleet: [],
  selectedArray: null,
  selectedId: null,
  freeRiders: [],

  inactiveRiders: [
    {
      order: null || undefined,
      rider: {
        riderName: "Neon",
        riderPhone: "03236792673",
        riderEmail: "neon@synavos.com",
        location: {
          lat: "15.785",
          lng: "50.902"
        }
      }
    },
    {
      order: null || undefined,
      rider: {
        riderName: "Zinha",
        riderPhone: "03436792673",
        riderEmail: "zinha@synavos.com",
        location: {
          lat: "15.797",
          lng: "50.923"
        }
      }
    }
  ]
};

const reducer = (state = initState, action) => {
  let newState = { ...state };
  switch (action.type) {
    case PROCESSING:
      setProcessing(state, action.payload);
      break;
    case SET_MAP_DATA:
      setMapData(newState, action.payload);
      break;
    case SET_ACTIVE_FLEET:
      setActiveFleet(newState, action.payload);
      break;
    case SET_FREE_RIDERS:
      setFreeRiders(newState, action.payload);
      break;
    case SET_BUSY_FLEET:
      setBusyFleet(newState, action.payload);
      break;
    case SET_OFFLINE_FLEET:
      setOfflineFleet(newState, action.payload);
      break;
    default: {
      // do nothing
    }
  }
  return newState;
};

const setMapData = (state, data) => {
  state.selectedArray = data.key;
  state.selectedId = data.uuid;
  state.click = data.click;
};

const setActiveFleet = (state, data) => {
  state.activeFleet = data;
};

const setFreeRiders = (state, data) => {
  state.freeRiders = data;
};

const setBusyFleet = (state, data) => {
  state.busyFleet = data;
};

const setOfflineFleet = (state, data) => {
  state.offlineFleet = data;
};

const setProcessing = (state, data) => {
  state.processing = data;
};

export default reducer;
