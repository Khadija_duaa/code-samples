export const PROCESSING = 'ACTION_ADMIN_PROCESSING';
export const RESET_REDUX = 'ACTION_ADMIN_RESET_REDUX';
export const SET_ROLES = 'ACTION_ADMIN_SET_ROLES';
export const SET_ADMINS = 'ACTION_ADMIN_SET_ADMINS';