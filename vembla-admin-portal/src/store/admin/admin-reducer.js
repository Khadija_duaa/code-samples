import * as actions from './admin-action-types';

const initState = {
    processing: false,
    roles: null,
    admins: null,
    totalPages: '',
    allAdminsData: null
};

const reducer = (state = initState, action) => {
    let newState = {...state};

    switch (action.type) {
        case actions.SET_ROLES:
            setRoles(newState, action.payload);
            break;

        case actions.SET_ADMINS:
            setAdmins(newState, action.payload);
            break;

        case actions.PROCESSING:
            setProcessing(newState, action.payload);
            break;

        case actions.RESET_REDUX:
            newState = {...initState};
            break;

        default : {
            // do nothing
        }
    }
    return newState;
};

const setRoles = (state, data) => {
    state.roles = data.content;
};

const setAdmins = (state, data) => {
    state.admins = data.content;
    state.totalPages = data.totalPages;
    state.allAdminsData = data
};

const setProcessing = (state, processing) => {
    state.processing = processing;
};

export default reducer;