import * as actions from "./admin-action-types";
import axios from '../../utils/axios';
import {
    CREATE_NEW_ADMIN,
    FETCH_ROLES,
    FETCH_ADMINS
} from "../../utils/config";
import {NotificationManager} from "react-notifications";

const setProcessing = (processing) => {
    return {
        type: actions.PROCESSING,
        payload: processing
    };
};

const setRoles = (payload) => {
    return {
        type: actions.SET_ROLES,
        payload
    }
};

const setAdmins = (payload) => {
    return {
        type: actions.SET_ADMINS,
        payload
    }
};

export const getAllRoles = () => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(FETCH_ROLES)
            .then((response) => {
                const content = response.data;
                dispatch(setRoles(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message = err.response && err.response.data && err.response.data.message;
                dispatch(setRoles({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getAllAdmins = (adminValue, number, size) => {
    return (dispatch) => {
        let url;
        dispatch(setProcessing(true));
        if(adminValue){
            url = FETCH_ADMINS + adminValue + `?number=${number}&size=${size}`
        }else {
            url = FETCH_ADMINS + `?number=${number}&size=${size}`
        }
        axios.get(url)
            .then((response) => {
                const content = response.data;
                dispatch(setAdmins(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message = err.response && err.response.data && err.response.data.message;
                dispatch(setAdmins({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const saveAdmin = (adminData, role, cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.post(CREATE_NEW_ADMIN + role, adminData)
            .then((response) => {
                const {message} = response.data;
                dispatch(setProcessing(false));
                NotificationManager.success(message, '', 3000);
                dispatch(getAllAdmins(role, 0, 10));
                cb() && cb();
            })
            .catch(err => {
                console.log(err);
                let message = err.response && err.response.data && err.response.data.error_description;
                NotificationManager.error(message === "" ? 'Some Error Occured' : message, '', 3000);
                dispatch(setProcessing(false));
            });
    };
};

export const resetRedux = () => {
    return {
        type: actions.RESET_REDUX
    };
};