// Redux
import thunk from "redux-thunk";
import { createStore, applyMiddleware, combineReducers, compose } from "redux";
// Stores
import admin from "./admin/admin-reducer";
import consumer from "./consumer/consumer-reducer";
import coupon from "./coupon/coupon-reducer";
import fleet from "./fleet/fleet-reducer";
import multilingual from "./multilingual/multilingual-reducer";
import order from "./order/order-reducer";
import shopper from "./shopper/shopper-reducer";
import storeReducer from "./store/store-reducer";
import user from "./user/user-reducer";

const reducer = combineReducers({
  admin,
  consumer,
  coupon,
  fleet,
  multilingual,
  order,
  shopper,
  storeReducer,
  user
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));
export default store;
