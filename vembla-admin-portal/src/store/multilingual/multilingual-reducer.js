import {LANGUAGE} from "./multilingual-action-types";

const ACTIVE_LANGUAGE_KEY = "ACTIVE_LANGUAGE";

const getActiveLanguage = () =>
    localStorage.getItem(ACTIVE_LANGUAGE_KEY) || "SE";

const initState = () => {
    let initState = {
        activeLanguageLabel: "",
        activeLanguageData: {},

        US: {
            consumer: "Consumers",
            approvedShopper: "Approved Agents",
            pendingShopper: "Pending Agents",
            noDataTitle: {
                brands: "No Brands Available!",
                items: "No Items Available!",
                stores: "No Stores Available!",
                consumers: "No Consumers Available!",
                categories: "No Categories Available!",
                pendingShopper: "No Pending Shopper Available!",
                approvedShopper: "No Approved Shopper Available!",
                noDetailsTitle: "No Details Found!",
                noDeliveredOrderTitle: "No Delivered Orders Found!",
                noAdmins: "No Admins Found!",
                noCancelOrderTitle: "No Canceled Orders Found!",
            },
            name: "Name",
            address: "Address",
            geofence: "Geofence",
            email: "Email",
            phone: "Phone",
            role: "Role",
            action: "Actions",
            id: "ID",
            active: "Active",
            category: "Category",
            categories: "Categories",
            itemID: "Item ID",
            brand: "Brand",
            items: "Items",
            admins: "Admins",
            createAdmin: "Create Admin",
            orderNo: "Order #",
            deliveredToTitle: "Delivered To",
            price: "Price",
            amountReceivedTitle: "Amount",
            deliveredAddress: "Delivered Address",
            storeTitle: "Store",
            createdAt: "Created At",
            colorCode: "Color Code",
            loginTitle: "Login",
            vemblaAdmin: "Vembla Admin Panel",
            fleetManagement: {
                orders: "Orders",
                riders: "Riders"
            },
            storesTitle: "Stores",
            shopperVehicleTitle: "Shopper Vehicle Information",
            deliveredOrderTitle: "Delivered Orders",
            cancelOrderTitle: "Canceled Orders",
            mapCheckTitle:
                "Kindly select area from the given list and set the marker at desired location!",
            newStoreTitle: "+ New Store",
            csvTitle: "Import CSV File"
        },

        SE: {
            consumer: "Konsument",
            approvedShopper: "Godkänd Agenter",
            pendingShopper: "Väntar Agenter",
            noDataTitle: {
                brands: "Inga märken tillgängliga!",
                items: "Inga artiklar tillgängliga!",
                stores: "Inga butiker tillgängliga!",
                consumers: "Inga konsumenter tillgängliga!",
                categories: "Inga kategorier tillgängliga!",
                pendingShopper: "Ingen väntande shopper tillgänglig!",
                approvedShopper: "Ingen godkänd shopper tillgänglig!",
                noDetailsTitle: "Inga detaljer hittade!",
                noDeliveredOrderTitle: "Inga levererade order hittade!",
                noAdmins: "Inga administratörer hittades!",
                noCancelOrderTitle: "Inga avbrutna order hittades!",
            },
            name: "Namn",
            address: "Adress",
            geofence: "Geofence",
            email: "E-post",
            phone: "Telefon",
            role: "Roll",
            action: "Åtgärder",
            id: "ID",
            active: "Aktiva",
            loginTitle: "Logga in",
            category: "Kategori",
            categories: "Kategorier",
            itemID: "Artikelnummer",
            brand: "Varumärke",
            items: "Artiklar",
            admins: "Admins",
            createAdmin: "Skapa Administratör",
            orderNo: "Beställningsnr",
            deliveredToTitle: "Levererad till",
            price: "Pris",
            amountReceivedTitle: "Mottaget",
            deliveredAddress: "Levererad adress",
            storeTitle: "Lagra",
            createdAt: "Skapad vid",
            colorCode: "Färg kod",
            vemblaAdmin: "Vembla Admin Panel",
            fleetManagement: {
                orders: "Order",
                riders: "Riders"
            },
            storesTitle: "Butiker",
            shopperVehicleTitle: "Information om kundens fordon",
            deliveredOrderTitle: "Levererade Beställningar",
            cancelOrderTitle: "Avbrutna beställningar",
            mapCheckTitle:
                "Välj område från den givna listan och ställ in markören på önskad plats!",
            newStoreTitle: "+ Ny butik",
            csvTitle: "Importera CSV Fil"
        }
    };
    let activeLanguageLabel = getActiveLanguage();
    initState.activeLanguageData = initState[activeLanguageLabel];
    initState.activeLanguageLabel = activeLanguageLabel;

    return initState;
};

export default function (state = initState(), action) {
    let newState = {...state};

    switch (action.type) {
        case LANGUAGE:
            changeLanguage(newState, action.payload);
            break;
        default:
            break;
    }
    return newState;
}

const changeLanguage = (state, language) => {
    state.activeLanguageData = {...state[language]};
    state.activeLanguageLabel = language;

    if (state.activeLanguageLabel !== "undefined") {
        localStorage.setItem(ACTIVE_LANGUAGE_KEY, state.activeLanguageLabel);
    }
};
