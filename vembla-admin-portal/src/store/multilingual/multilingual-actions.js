import {LANGUAGE} from "./multilingual-action-types";

export const setLanguage = (props)=>{
    return{
        type: LANGUAGE,
        payload:props
    }
};
