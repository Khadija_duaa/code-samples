import * as actions from './shopper-action-types';
import {saveRevolutInfo} from "./shopper-actions";

const initState = {
    approvedShopper: null,
    pendingShopper: null,
    approvedShopperData: null,
    pendingShopperData: null,
    shopperVehicleInfo: '',
    allVehicles: '',
    vehicleData: null,
    successMsg: null,
    isApproveUserSuccess: false,
    isSetUserVehNProgSuccess: false,
    message: null,
    error: null,
    fleetErrorMsg: null,
    revolutAccounts: [],
    approveFleetDetail: null,
    commissions: [],
    commissionsProcessing: false,
    isSetUserRevolutInfo: false,
    revolutInfo: null,
    fleetInfo: null,
    gigaPayInfo: null
};

const reducer = (state = initState, action) => {
    let newState = {...state};

    switch (action.type) {
        case actions.SET_APPROVED_SHOPPER:
            setApprovedShopper(newState, action.payload);
            break;

        case actions.SET_PENDING_SHOPPER:
            setPendingShopper(newState, action.payload);
            break;

        case actions.SET_SHOPPER_VEHICLE_INFO:
            setShopperVehicleInfo(newState, action.payload);
            break;

        case actions.SET_ALL_VEHICLES:
            setAllVehicles(newState, action.payload);
            break;

        case actions.SET_VEHICLE_DATA:
            setVehicleData(newState, action.payload);
            break;

        case actions.DELETE_VEHICLE:
            deleteVehicle(newState, action.payload);
            break;

        case actions.SET_ALL_PROGRAMS:
            setAllPrograms(newState, action.payload);
            break;

        case actions.FLEET_DATA_ERROR_MESSAGE:
            setFleetDataErrorMsg(newState, action.payload);
            break;

        case actions.SET_VEHICLE_TYPES:
            setVehicleType(newState, action.payload);
            break;

        case actions.PROCESSING:
            setProcessing(newState, action.payload);
            break;

        case actions.SET_REVOLUT_ACCOUNTS:
            setRevolutAccounts(newState, action.payload);
            break;

        case actions.SET_SUCCESS_MESSAGE:
            setMessage(newState, action.payload);
            break;

        case actions.SET_APPROVE_FLEET_DETAIL:
            setApproveFleetDetail(newState, action.payload);
            break;

        case actions.SET_EMPLOYEE_COMMISSSIONS:
            setCommissions(newState, action.payload);
            break;

        case actions.SET_EMPLOYEE_COMMISSSIONS_ACTION:
            setCommissionsProcessing(newState, action.payload);
            break;

        case actions.SAVE_REVOLUT_INFO:
            setRevolutInfo(newState, action.payload);
            break;

        case actions.SAVE_FLEET_INFO:
            setFleetInfo(newState, action.payload);
            break;

        case actions.SAVE_GIGAPAY_INFO:
            setGigapayInfo(newState, action.payload);
            break;

        case actions.RESET_REDUX:
            newState = {...initState};
            break;

        default: {
            // do nothing
        }
    }
    return newState;
};

const setProcessing = (state, processing) => {
    state.processing = processing;
};

const setCommissions = (state, data) => {
    if(data && data.commissionData){
        state.commissions = data.commissionData;
    }
    if(data && data.error){
        state.error = data.error;
    }
};

const setCommissionsProcessing = (state, val) => {
    state.commissionsProcessing = val;
};

const setMessage = (state, payload) => {
    state.successMsg = payload.successMsg;
    state.isApproveUserSuccess = payload.isApproveUserSuccess && payload.isApproveUserSuccess;
    state.isSetUserVehNProgSuccess = payload.isSetUserVehNProgSuccess && payload.isSetUserVehNProgSuccess;
    state.isSetUserRevolutInfo = payload.isSetUserRevolutInfo && payload.isSetUserRevolutInfo;
};

const setApprovedShopper = (state, data) => {
    state.approvedShopper = data.content;
    state.totalPages = data.totalPages;
    state.approvedShopperData = data;
};

const setPendingShopper = (state, data) => {
    state.pendingShopper = data.content;
    state.totalPages = data.totalPages;
    state.pendingShopperData = data;
};

const setShopperVehicleInfo = (state, data) => {
    state.shopperVehicleInfo = data.content;
};

const setAllVehicles = (state, data) => {
    state.allVehicles = data.content;
};

const setAllPrograms = (state, data) => {
    state.allPrograms = data.content;
};

const setVehicleData = (state, data) => {
    state.vehicleData = data;
};

const deleteVehicle = (state, index) => {
    let newVehicles = [...state.allVehicles];
    newVehicles.splice(index, 1);
    state.allVehicles = newVehicles;
};

const setFleetDataErrorMsg = (state, errorMsg) => {
    state.fleetErrorMsg = errorMsg;
};

const setVehicleType = (state, data) => {
    state.vehicleTypes = data.content;
};

const setRevolutAccounts = (state, data) => {
    state.revolutAccounts = data.content;
};

const setApproveFleetDetail = (state, data) => {
    state.approveFleetDetail = data.content;
};

const setRevolutInfo = (state, data) => {
    state.revolutInfo = data;
};

const setFleetInfo = (state, data) => {
    state.fleetInfo = data;
};

const setGigapayInfo = (state, data) => {
    state.gigaPayInfo = data;
};

export default reducer;
