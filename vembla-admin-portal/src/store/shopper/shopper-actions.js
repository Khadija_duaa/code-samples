import * as actions from './shopper-action-types';
import axios from '../../utils/axios';
import {
    FETCH_APPROVED_SHOPPER,
    FETCH_PENDING_SHOPPER,
    APPROVE_PENDING_SHOPPER,
    ACTIVATE_APPROVED_SHOPPER,
    DEACTIVATE_APPROVED_SHOPPER,
    GET_SHOPPER_VEHICLE_INFO,
    GET_ALL_VEHICLES,
    CREATE_NEW_STORE,
    CREATE_NEW_VEHICLE,
    UPDATE_VEHICLE,
    DELETE_VEHICLE,
    GET_ALL_PROGRAMS,
    SAVE_SHOPPER_PROGRAM,
    SET_FLEET_A_STAFF_STORE_SHOPPER,
    ADD_FLEET_DATA,
    SAVE_REVOLUT_INFO,
    SAVE_GIGAPAY_INFO,
    GET_VEHICLE_TYPES,
    SAVE_USER_VEHICLE,
    GET_REVOLUT_ACCOUNTS,
    FETCH_APPROVED_FLEET_DETAIL,
    EMPLOYEE_COMMISSIONS,
    CREATE_EMPLOYEE_COMMISSIONS, FETCH_SEARCH_ITEMS
} from '../../utils/config';
import {NotificationManager} from 'react-notifications';

const setProcessing = processing => {
    return {
        type: actions.PROCESSING,
        payload: processing
    };
};

const setApprovedShopper = payload => {
    return {
        type: actions.SET_APPROVED_SHOPPER,
        payload
    };
};

const setPendingShopper = payload => {
    return {
        type: actions.SET_PENDING_SHOPPER,
        payload
    };
};

const setShopperVehicleInfo = payload => {
    return {
        type: actions.SET_SHOPPER_VEHICLE_INFO,
        payload
    };
};

const setAllVehicles = payload => {
    return {
        type: actions.SET_ALL_VEHICLES,
        payload
    };
};

const _deleteVehicle = index => {
    return {
        type: actions.DELETE_VEHICLE,
        payload: index
    };
};

const setAllPrograms = payload => {
    return {
        type: actions.SET_ALL_PROGRAMS,
        payload
    };
};

const setVehicleTypes = payload => {
    return {
        type: actions.SET_VEHICLE_TYPES,
        payload
    };
};

export const setFleetErrorMsg = payload => {
    return {
        type: actions.FLEET_DATA_ERROR_MESSAGE,
        payload
    };
};

const setSuccessMsg = payload => {
    return {
        type: actions.SET_SUCCESS_MESSAGE,
        payload
    };
};

const setRevolutAccounts = payload => {
    return {
        type: actions.SET_REVOLUT_ACCOUNTS,
        payload
    };
};

const setApprovedFleetDetail = payload => {
    return {
        type: actions.SET_APPROVE_FLEET_DETAIL,
        payload
    };
};

const _saveFleetInfo = payload => {
    return {
        type: actions.SAVE_FLEET_INFO,
        payload
    };
};

const _saveRevolutInfo = payload => {
    return {
        type: actions.SAVE_REVOLUT_INFO,
        payload
    };
};

const _saveGigapayInfo = payload => {
    return {
        type: actions.SAVE_GIGAPAY_INFO,
        payload
    };
};

export const getApprovedShopper = (number, size, sortVal, sortField) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios.get(FETCH_APPROVED_SHOPPER + `?number=${number}&size=${size}&sortField=${sortField}&sortDirection=${sortVal}`)
            .then(response => {
                const content = response.data;
                dispatch(setApprovedShopper(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setApprovedShopper({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getPendingShopper = (number, size, sortVal, sortField) => {
    return dispatch => {
        axios.get(
            FETCH_PENDING_SHOPPER +
            `?number=${number}&size=${size}&sortField=${sortField}&sortDirection=${sortVal}`
        )
            .then(response => {
                const content = response.data;
                dispatch(setPendingShopper(content));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setPendingShopper({error: message}));
            });
    };
};

export const approvePendingUser = (uuid, number, size) => {
    return (dispatch, getState) => {
        let fleetInfo = getState().shopper.fleetInfo;
        let revolutInfo = getState().shopper.revolutInfo ? getState().shopper.revolutInfo.revolutInfo : null;
        let gigaPayInfo = getState().shopper.gigaPayInfo ? getState().shopper.gigaPayInfo.gigaPayInfo : null;

        if(fleetInfo !== 'undefined'){
            fleetInfo.revolutInfo = revolutInfo;
            fleetInfo.gigaPayInfo = gigaPayInfo;
        }

        let fleetData = fleetInfo;
        dispatch(setProcessing(true));
        axios.patch(APPROVE_PENDING_SHOPPER + uuid, fleetInfo)
            .then(response => {
                let message = response.data && response.data.message;
                dispatch(
                    setSuccessMsg({
                        successMsg: message,
                        isApproveUserSuccess: true
                    })
                );
                dispatch(addFleetData(fleetData));
                dispatch(getPendingShopper(number, size, 'asc', 'firstName'));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let messageArray = err.response && err.response.data && err.response.data.fieldErrors;
                let message = err.response && err.response.data && err.response.data.message;
                if (messageArray) {
                    for (let i = 0; i < messageArray.length; i++) {
                        NotificationManager.error(messageArray[i].error, '', 5000);
                    }
                } else {
                    NotificationManager.error(message, '', 5000);
                }
                dispatch(setFleetErrorMsg(message));
            });
    };
};

export const saveShopperProgram = (programUUID, shopperUUID) => {
    return dispatch => {
        axios
            .post(SAVE_SHOPPER_PROGRAM + programUUID + '/' + shopperUUID)
            .then(response => {
                const {message} = response.data;
                NotificationManager.success(message, '', 3000);
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                NotificationManager.error(message, '', 3000);
            });
    };
};

export const saveRevolutInfo = (uuidFleet, accountId) => {
    let revolutInfoObj = {
        uuidFleet: uuidFleet,
        revolutInfo: {
            accountId: accountId
        }
    };
    return dispatch => {
        dispatch(setSuccessMsg({isSetUserRevolutInfo: true}));
        dispatch(_saveRevolutInfo(revolutInfoObj))
    }
};

export const saveGigaPayInfo = (uuidFleet, gigaPayInfo) => {
    let gigaPayInfoObj = {
        uuidFleet: uuidFleet,
        gigaPayInfo: gigaPayInfo
    };
    return dispatch => {
        dispatch(_saveGigapayInfo(gigaPayInfoObj));
        dispatch(approvePendingUser(uuidFleet, 0, 10))
    }
};

export const addFleetData = fleetData => {
    fleetData['uuidFleet'] = fleetData['uuid'];
    return dispatch => {
        axios.post(ADD_FLEET_DATA, fleetData)
            .then(response => {
                const {message} = response.data;
                // Setting Fleet Data Success::
                // dispatch(
                //     setSuccessMsg({
                //         successMsg: message,
                //         isSetUserVehNProgSuccess: true
                //     })
                // );
            })
            .catch(err => {
                let message =
                    err.response && err.response.data
                        ? err.response.data.message
                        : err.response.message;
                dispatch(setFleetErrorMsg(message));
                NotificationManager.error(message, '', 3000);
            });
    };
};

export const activateApprovedShopper = (uuid, currentPage, pageSize) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.patch(ACTIVATE_APPROVED_SHOPPER + uuid)
            .then((response) => {
                dispatch(setProcessing(false));
                dispatch(getApprovedShopper(currentPage, pageSize, "asc", "firstName"));
            })
            .catch(err => {
                dispatch(setProcessing(false));
                console.log(err);
            });
    };
};

export const deactivateApprovedShopper = (uuid, currentPage, pageSize) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .patch(DEACTIVATE_APPROVED_SHOPPER + uuid)
            .then(response => {
                dispatch(setProcessing(false));
                dispatch(getApprovedShopper(currentPage, pageSize, 'asc', 'firstName'));
            })
            .catch(err => {
                dispatch(setProcessing(false));
                console.log(err);
            });
    };
};

export const getShopperVehicleInfo = uuid => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios.get(GET_SHOPPER_VEHICLE_INFO + uuid)
            .then(response => {
                const content = response.data;
                dispatch(setShopperVehicleInfo(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setShopperVehicleInfo({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getAllVehicles = (number, size) => {
    return dispatch => {
        let URL;
        dispatch(setProcessing(true));
        if (number && size) {
            URL =
                GET_ALL_VEHICLES +
                `?number=${number}&size=${size}&sortField=firstName&sortDirection=asc`;
        } else {
            URL = GET_ALL_VEHICLES;
        }
        axios
            .get(URL)
            .then(response => {
                const content = response.data;
                dispatch(setAllVehicles(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setAllVehicles({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const saveVehicle = (vehicleData, fleetData, cb) => {
    return dispatch => {
        axios
            .post(SAVE_USER_VEHICLE, vehicleData)
            .then(response => {
                const {message} = response.data;
                const {content} = response.data;
                fleetData.vehicleInfo = content;
                if (cb) {
                    cb();
                }
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                NotificationManager.error(message, '', 3000);
                if (cb) {
                    cb();
                }
            });
    };
};

// Save Shopper's Vehicle and Program Information
export const saveShopperVehicleNProgramInfo = (vehicleData, programInfo, fleetData, cb) => {
    return dispatch => {
        axios.post(SAVE_USER_VEHICLE, vehicleData)
            .then(response => {
                const {message} = response.data;
                const {content} = response.data;
                fleetData.vehicleInfo = content;
                fleetData.programInfo = programInfo;
                dispatch(_saveFleetInfo(fleetData));
                dispatch(
                    setSuccessMsg({
                        successMsg: message,
                        isSetUserVehNProgSuccess: true
                    })
                );
                // dispatch(addFleetData(fleetData));
                if (cb) {
                    cb();
                }
            })
            .catch(err => {
                let message = err.response && err.response.data && err.response.data.message;
                NotificationManager.error(message, '', 3000);
                if (cb) {
                    cb();
                }
            });
    };
};

export const deleteVehicle = (id, index) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .delete(DELETE_VEHICLE + id)
            .then(response => {
                const {message} = response.data;
                NotificationManager.success(message, '', 3000);
                dispatch(_deleteVehicle(index));
                dispatch(setProcessing(false));
                dispatch(getAllVehicles());
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                NotificationManager.error(message, '', 3000);
                dispatch(setProcessing(false));
            });
    };
};

export const setFleetAStaffStoreShopper = data => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));
        axios
            .post(SET_FLEET_A_STAFF_STORE_SHOPPER, data)
            .then(response => {
                const {message} = response.data;
                NotificationManager.success(message, '', 3000);
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                NotificationManager.error(message, '', 3000);
                dispatch(setProcessing(false));
            });
    };
};

export const getAllPrograms = () => {
    return dispatch => {
        axios
            .get(GET_ALL_PROGRAMS)
            .then(response => {
                const content = response.data;
                dispatch(setAllPrograms(content));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setAllPrograms({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getVehicleTypes = () => {
    return dispatch => {
        // dispatch(setProcessing(true));
        axios
            .get(GET_VEHICLE_TYPES)
            .then(response => {
                const content = response.data;
                dispatch(setVehicleTypes(content));
                // dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setVehicleTypes({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getRevolutAccounts = () => {
    return dispatch => {
        axios
            .get(GET_REVOLUT_ACCOUNTS)
            .then(response => {
                const content = response.data;
                dispatch(setRevolutAccounts(content));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setRevolutAccounts({content: []}));
                dispatch(setProcessing(false));
            });
    };
};

export const getApprovedFleetDetail = (uuid, cb) => {
    return dispatch => {
        axios
            .get(FETCH_APPROVED_FLEET_DETAIL + uuid)
            .then(response => {
                const content = response.data;
                dispatch(setApprovedFleetDetail(content));
                cb && cb();
            })
            .catch(err => {
                console.log(err);
                let message = err.response && err.response.data && err.response.data.message;
                dispatch(setApprovedFleetDetail({error: message}));
                dispatch(setProcessing(false));
                cb && cb();
            });
    };
};

export const setVehicleData = vehicleData => {
    return {
        type: actions.SET_VEHICLE_DATA,
        payload: vehicleData
    };
};

export const resetRedux = () => {
    return {
        type: actions.RESET_REDUX
    };
};

export const setCommissions = commissions => {
    return {
        type: actions.SET_EMPLOYEE_COMMISSSIONS,
        payload: commissions
    };
};

export const setCommssionsProcessing = val => {
    return {
        type: actions.SET_EMPLOYEE_COMMISSSIONS_ACTION,
        payload: val
    };
};
export const fetchCommissions = uuidFleet => {
    return dispatch => {
        dispatch(setCommssionsProcessing(true));
        axios
            .post(EMPLOYEE_COMMISSIONS, {uuidFleet})
            .then(res => {
                const {data} = res;
                dispatch(setCommissions({commissionData: data && data.content}));
                dispatch(setCommssionsProcessing(false));
            })
            .catch(err => {
                let message = err.response && err.response.data && err.response.data.message;
                NotificationManager.error(message, '', 3000);
                dispatch(setCommissions({error: message}));
                dispatch(setCommssionsProcessing(false));
            });
    };
};

export const createCommission = obj => {
    return dispatch => {
        dispatch(setCommssionsProcessing(true));
        axios
            .post(CREATE_EMPLOYEE_COMMISSIONS, obj)
            .then(res => {
                const {data} = res;
                dispatch(setCommissions(data.content));
                NotificationManager.success('', 'Commission Saved Successfully');
                dispatch(setCommssionsProcessing(false));
            })
            .catch(err => {
                NotificationManager.error('', 'Commission Request Failed');
                dispatch(setCommssionsProcessing(false));
            });
    };
};
