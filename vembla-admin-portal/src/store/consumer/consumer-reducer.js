import * as actions from './consumer-action-types';

const initState = {
    consumer: null,
    message: null,
    error: null,
    totalPages: null,
    consumerData: null
};

const reducer = (state = initState, action) => {
    let newState = {...state};

    switch (action.type) {
        case actions.SET_CONSUMER:
            setConsumer(newState, action.payload);
            break;

        case actions.PROCESSING:
            setProcessing(newState, action.payload);
            break;

        case actions.RESET_REDUX:
            newState = {...initState};
            break;

        default : {
            // do nothing
        }
    }
    return newState;
};

const setProcessing = (state, processing) => {
    state.processing = processing;
};

const setConsumer = (state, data) => {
    state.totalPages = data.totalPages;
    state.consumer = data.content;
    state.consumerData = data
};

export default reducer;