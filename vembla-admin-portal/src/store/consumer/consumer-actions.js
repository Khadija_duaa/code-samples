import * as actions from "./consumer-action-types";
import axios from '../../utils/axios';
import {
    FETCH_CONSUMER
} from "../../utils/config";

const setProcessing = (processing) => {
    return {
        type: actions.PROCESSING,
        payload: processing
    };
};

const setConsumer = (payload) => {
    return {
        type: actions.SET_CONSUMER,
        payload
    }
};

export const getAllConsumer = (number, size, sortValue, sortField) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(FETCH_CONSUMER + `?number=${number}&size=${size}&sortField=${sortField}&sortDirection=${sortValue}`)
            .then((response) => {
                const content = response.data;
                dispatch(setConsumer(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message = err.response && err.response.data && err.response.data.message;
                dispatch(setConsumer({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const resetRedux = () => {
    return {
        type: actions.RESET_REDUX
    };
};