import * as actions from "./store-action-types";

const initState = {
    allStores: "",
    storeData: null,
    totalPages: "",
    allStoresData: "",
    singleStoreData: "",
    showUniqueStoreItems: false,
    allCategories: "",
    allCategoriesData: "",
    allBrands: "",
    allBrandsData: "",
    allItems: "",
    allItemsData: "",
    storesByItem: "",
    itemsByCategory: "",
    itemsByBrand: "",
    showStoresByItemId: false,
    showItemsByCategory: false,
    showItemsByBrand: false,
    message: null,
    error: null,
    processing: false,
    itemEditBtn: false,
    storeUUID: "",
    itemData: null,
    itemName: null,
    categoryName: null,
    staffedStores: null,
    categoryData: null,
    pageSize: 10,
    currentPage: 1,
    allCategoriesBySearch: [],
    activeCategories: [],
    allItemsBySearch: [],
    storeDeals: [],
    storeName: null,
    singleBrand: null,
    singleCategory: null
};

const reducer = (state = initState, action) => {
    let newState = {...state};
    switch (action.type) {
        case actions.GET_BRAND_BY_ID :
            newState.singleBrand = action.payload;
            break;
        case actions.GET_CATEGORY_BY_ID :
            newState.singleCategory = action.payload;
            break;
        case actions.SET_STORE_NAME:
            newState.storeName = action.payload;
            break;
        case actions.SET_ALL_STORES:
            setAllStores(newState, action.payload);
            break;

        case actions.SET_STORE_DATA:
            setStoreData(newState, action.payload);
            break;

        case actions.SET_SINGLE_STORE_DATA:
            setSingleStoreData(newState, action.payload);
            break;

        case actions.SET_ALL_ITEMS:
            setAllItems(newState, action.payload);
            break;

        case actions.SET_ALL_CATEGORIES:
            setAllCategories(newState, action.payload);
            break;

        case actions.SET_ALL_BRANDS:
            setAllBrands(newState, action.payload);
            break;

        case actions.SET_STORE_BY_ITEM:
            setStoreByItem(newState, action.payload);
            break;

        case actions.SET_ITEM_BY_CATEGORY:
            setItemByCategory(newState, action.payload);
            break;

        case actions.SET_ITEM_BY_BRAND:
            setItemByBrand(newState, action.payload);
            break;

        case actions.SET_EDIT_ITEM_BTN:
            setEditItemButton(newState, action.payload);
            break;

        case actions.SET_SINGLE_ITEM_DETAIL:
            setSingleItemDetail(newState, action.payload);
            break;

        case actions.SET_STAFFED_STORES:
            setStaffedStores(newState, action.payload);
            break;

        case actions.PROCESSING:
            setProcessing(newState, action.payload);
            break;

        case actions.SET_IMPORTED_ITEMS:
            setImportedItems(newState, action.payload);
            break;

        case actions.SET_PAGINATION:
            setPagination(newState, action.payload);
            break;

        case actions.SET_STORE_PACKAGES:
            setStorePackages(newState, action.payload);
            break;

        case actions.RESET_REDUX:
            newState = {...initState};
            break;

        case actions.SET_CATEGORY_DATA:
            setCategoryData(newState, action.payload);
            break;

        case actions.DELETE_CATEGORY:
            deleteCategory(newState, action.payload);
            break;

        case actions.DELETE_STORE_PACKAGE:
            deleteStorePackage(newState, action.payload);
            break;

        case actions.SET_ALL_CATEGORIES_BY_SEARCH:
            setAllCategoriesBySearch(newState, action.payload);
            break;

        case actions.SET_ALL_ITEMS_BY_SEARCH:
            setAllItemsBySearch(newState, action.payload);
            break;

        case actions.SET_STORE_DEALS:
            setStoreDeals(newState, action.payload);
            break;

        case actions.DELETE_STORE_DEAL:
            deleteStoreDeal(newState, action.payload);
            break;

        default: {
            // do nothing
        }
    }
    return newState;
};

const setAllCategoriesBySearch = (state, data) => {
    state.allCategoriesBySearch = data && data.content;
};

const setAllItemsBySearch = (state, data) => {
    state.allItemsBySearch = data && data;
};

const setProcessing = (state, processing) => {
    state.processing = processing;
};

const setAllStores = (state, data) => {
    if(data){
        state.allStores = data.content;
        state.totalPages = data.totalPages;
        state.allStoresData = data;
    }
};

const setStoreData = (state, data) => {
    state.storeData = data;
};

const setCategoryData = (state, data) => {
    state.categoryData = data && data;
};

const setImportedItems = (state, data) => {
};

const setSingleStoreData = (state, data) => {
    state.singleStoreData = data && data;
    if (data && data.content) {
        state.allItems = data.content.content;
        state.totalPages = data.content.totalPages;
        state.allItemsData = data.content;
    }
    state.showUniqueStoreItems = data.showUniqueStoreItems;
};

const setAllItems = (state, data) => {
    if (data) {
        state.allItems = data ? data.content : null;
        state.totalPages = data.totalPages;
        state.allItemsData = data;
    }
};

const setAllCategories = (state, data) => {
    state.allCategories = data && data.content;
    state.activeCategories = [];
    if (state.allCategories && state.allCategories.length) {
        state.totalPages = Math.ceil(state.allCategories.length / state.pageSize);
        setPagination(state, 1);
    }
};

const deleteCategory = (state, index) => {
    let newCategories = [...state.allCategories];
    newCategories.splice(index, 1);
    state.allCategories = newCategories;
};

const setAllBrands = (state, data) => {
    if(data){
        state.allBrands = data.content;
        state.totalPages = data.totalPages;
        state.allBrandsData = data;
    }
};

const setStoreByItem = (state, data) => {
    state.storesByItem = data;
    state.itemName = data.itemName;
    if (data.content) {
        state.allStores = data.content.content;
        state.totalPages = data.content.totalPages;
        state.allStoresData = data.content;
    }
    state.showStoresByItemId = data.showStoresByItemId;
};

const setItemByCategory = (state, data) => {
    state.itemsByCategory = data;
    state.categoryName = data.categoryName;
    if (data.content) {
        state.allItems = data.content.content;
        state.totalPages = data.content.totalPages;
        state.allItemsData = data.content;
    }
    state.showItemsByCategory = data.showItemsByCategory;
    state.categoryUUID = data.uuid;
};

const setItemByBrand = (state, data) => {
    state.itemsByBrand = data;
    state.brandName = data.brandName;
    if (data.content) {
        state.allItems = data.content.content;
        state.totalPages = data.content.totalPages;
        state.allItemsData = data.content;
    }
    state.showItemsByBrand = data.showItemsByBrand;
    state.brandUUID = data.uuid;
};

const setEditItemButton = (state, data) => {
    state.itemEditBtn = data && data;
};

const setSingleItemDetail = (state, data) => {
    state.itemData = data && data;
};

const setStaffedStores = (state, data) => {
    state.staffedStores = data && data.content;
};

const setPagination = (state, currentPage) => {
    let allCategories = [...state.allCategories];
    let firstIndex = (currentPage - 1) * state.pageSize;
    let lastIndex = firstIndex + state.pageSize;
    if (lastIndex > allCategories.length) {
        lastIndex = allCategories.length;
    }
    state.activeCategories = allCategories.slice(firstIndex, lastIndex);
    state.currentPage = currentPage;
};

const setStorePackages = (state, data) => {
    if (data) {
        state.storePackages = data.content;
        state.storeName = data.storeName;
        state.storeUUID = data.storeUUID;
    }
};

const deleteStorePackage = (state, index) => {
    let newStorePackages = {...state.storePackages};
    newStorePackages && newStorePackages.content && newStorePackages.content.splice(index, 1);
    state.storePackages = newStorePackages;
};

const setStoreDeals = (state, data) => {
    if (data.content) {
        state.storeDeals = data.content && data.content.content;
    }
    state.storeUUID = data.storeUUID;
    state.storeName = data.storeName;
};

const deleteStoreDeal = (state, data) => {
    let newStoreDeals = [...state.storeDeals];
    if(data){
        newStoreDeals[data.itemIndex] && newStoreDeals[data.itemIndex].items.splice(data.index, 1);
        state.storeDeals = newStoreDeals;
    }
};

export default reducer;
