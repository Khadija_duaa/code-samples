import {
    FETCH_ALL_STORES,
    CREATE_NEW_STORE,
    ENABLE_STORE,
    DISABLE_STORE,
    FETCH_STORE_ITEMS,
    FETCH_ALL_ITEMS,
    FETCH_ALL_CATEGORIES,
    FETCH_ALL_BRANDS,
    FETCH_STORE_BY_ITEM_ID,
    FETCH_ITEMS_BY_CATEGORY_ID,
    FETCH_ITEMS_BY_BRAND_ID,
    UPDATE_ITEM,
    FETCH_SINGLE_ITEM_DETAIL,
    FETCH_STAFFES_STORES,
    SEARCH_ITEMS,
    SAVE_CATEGORY,
    IMPORT_ITEMS,
    IMPORT_STORE_ITEMS,
    DELETE_VEHICLE,
    DELETE_CATEGORY,
    FETCH_SEARCH_CATEGORIES,
    GET_STORE_PACKAGES,
    SAVE_STORE_PACKAGE,
    DELETE_PACKAGE,
    FETCH_SEARCH_ITEMS,
    SAVE_ITEMS_FOR_STORE,
    SAVE_ITEMS_FOR_PACKAGE,
    GET_STORE_DEALS,
    SAVE_STORE_DEAL,
    DELETE_DEAL,
    UPDATE_ITEMS_FOR_PACKAGE,
    FETCH_STORE_BY_UUID,
    GET_BRAND_BY_ID,
    FETCH_CATEGORY_BY_ID
} from "../../utils/config";
import axios from "../../utils/axios";
import * as actions from "../store/store-action-types";
import {NotificationManager} from "react-notifications";
import storeReducer from "./store-reducer";
import {getAllVehicles} from "../shopper/shopper-actions";
import _ from 'lodash';

const setProcessing = processing => {
    return {
        type: actions.PROCESSING,
        payload: processing
    };
};

const setAllStores = payload => {
    return {
        type: actions.SET_ALL_STORES,
        payload
    };
};

const setSingleStoreData = payload => {
    return {
        type: actions.SET_SINGLE_STORE_DATA,
        payload
    };
};

const setStoreByItem = payload => {
    return {
        type: actions.SET_STORE_BY_ITEM,
        payload
    };
};

export const setAllItems = payload => {
    return {
        type: actions.SET_ALL_ITEMS,
        payload
    };
};

const setAllCategories = payload => {
    return {
        type: actions.SET_ALL_CATEGORIES,
        payload
    };
};

const setAllBrands = payload => {
    return {
        type: actions.SET_ALL_BRANDS,
        payload
    };
};

const setItemByCategory = payload => {
    return {
        type: actions.SET_ITEM_BY_CATEGORY,
        payload
    };
};

const _deleteCategory = index => {
    return {
        type: actions.DELETE_CATEGORY,
        payload: index
    };
};

const setItemByBrand = payload => {
    return {
        type: actions.SET_ITEM_BY_BRAND,
        payload
    };
};

const setAllCategoriesBySearch = payload => {
    return {
        type: actions.SET_ALL_CATEGORIES_BY_SEARCH,
        payload
    };
};

const setAllItemsBySearch = payload => {
    return {
        type: actions.SET_ALL_ITEMS_BY_SEARCH,
        payload
    };
};

export const setCategoryData = categoryData => {
    return {
        type: actions.SET_CATEGORY_DATA,
        payload: categoryData
    };
};

const setImportedItems = payload => {
    return {
        type: actions.SET_IMPORTED_ITEMS,
        payload
    };
};

const setSingleItemDetail = payload => {
    return {
        type: actions.SET_SINGLE_ITEM_DETAIL,
        payload
    };
};

const setStaffedStores = payload => {
    return {
        type: actions.SET_STAFFED_STORES,
        payload
    };
};

const setStorePackages = payload => {
    return {
        type: actions.SET_STORE_PACKAGES,
        payload
    };
};

const setStoreDeals = payload => {
    return {
        type: actions.SET_STORE_DEALS,
        payload
    };
};


const _deleteStorePackage = index => {
    return {
        type: actions.DELETE_STORE_PACKAGE,
        payload: index
    };
};

const _deleteStoreDeal = index => {
    return {
        type: actions.DELETE_STORE_DEAL,
        payload: index
    };
};

export const getStoreByUUID = (uuid, cb) => {
    return (dispatch => {
        dispatch(setProcessing(true));
        axios.get(FETCH_STORE_BY_UUID + uuid)
            .then(res => {
                const data = res.data;
                dispatch(setStoreData(data.content));
                dispatch(setProcessing(false));
                dispatch(setStoreName(data.content && data.content.name));
                cb && cb();
            })
            .catch(err => {

                console.log(err);
                dispatch(setProcessing(false));
                let message = err.response && err.response.data && err.response.data.message;

                cb && cb(err);
            })
    })
};

export const getAllStores = (number, size, singleStore) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios.get(FETCH_ALL_STORES + `?number=${number}&size=${size}&sortField=name&sortDirection=asc`)
            .then(response => {
                const content = response.data;
                dispatch(setAllStores(content));
                dispatch(setProcessing(false));
                if (singleStore) {
                    const single = _.find(content, p => p.uuid === singleStore);
                    console.log("Single Store==> ",)
                }
            })
            .catch(err => {
                console.log(err);
                let message = err.response && err.response.data && err.response.data.message;
                dispatch(setAllStores({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const saveStore = (storeData, currentPage, pageSize) => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));
        let url = CREATE_NEW_STORE;
        let storeAxios = null;
        let storeUUId = getState().storeReducer.storeData
            ? getState().storeReducer.storeData.uuid
            : null;
        if (storeUUId) {
            url += `/${storeUUId}`;
            storeAxios = axios.put(url, storeData);
        } else {
            storeAxios = axios.post(url, storeData);
        }
        storeAxios
            .then(response => {
                const {message} = response.data;
                NotificationManager.success(message, "", 3000);
                dispatch(setProcessing(false));
                dispatch(getAllStores(1, 10));
            })
            .catch(err => {
                let messageArray =
                    err.response && err.response.data && err.response.data.errors;
                let message =
                    err.response && err.response.data && err.response.data.message;
                if (messageArray) {
                    for (let i = 0; i < messageArray.length; i++) {
                        NotificationManager.error(messageArray[i], "", 5000);
                    }
                } else {
                    NotificationManager.error(message, "", 5000);
                }

                dispatch(setProcessing(false));
            });
    };
};

export const enableStore = (uuid, currentPage, pageSize) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .put(ENABLE_STORE + uuid)
            .then(response => {
                dispatch(getAllStores(currentPage, pageSize));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                dispatch(setProcessing(false));
            });
    };
};

export const disableStore = (uuid, currentPage, pageSize) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .put(DISABLE_STORE + uuid)
            .then(response => {
                dispatch(getAllStores(currentPage, pageSize));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                dispatch(setProcessing(false));
            });
    };
};

export const getStoreData = (uuid, showUniqueStoreItems, cb, number, size) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios.get(FETCH_STORE_ITEMS + uuid + `?number=${number}&size=${size}`)
            .then(response => {
                const content = response.data;
                dispatch(setProcessing(false));
                dispatch(setSingleStoreData({content, showUniqueStoreItems}));
                if (cb()) {
                    cb();
                }
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setSingleStoreData({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getAllItems = (number, size) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios.get(FETCH_ALL_ITEMS + `?number=${number}&size=${size}&sortField=name&sortDirection=asc`)
            .then(response => {
                const content = response.data;
                dispatch(setAllItems(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message = err.response && err.response.data && err.response.data.message;
                dispatch(setAllItems({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getAllCategories = (number, size) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(FETCH_ALL_CATEGORIES)
            .then(response => {
                const content = response.data;
                dispatch(setAllCategories(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setAllCategories({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const deleteCategory = (id, index) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .delete(DELETE_CATEGORY + id)
            .then(response => {
                NotificationManager.success("Category Deleted Successfully", "", 3000);
                dispatch(_deleteCategory(index));
                dispatch(setProcessing(false));
                dispatch(getAllCategories());
            })
            .catch(err => {
                let message =
                    err.response &&
                    err.response.data &&
                    err.response.data.error &&
                    err.response.data.error;
                NotificationManager.error(message, "", 3000);
                dispatch(setProcessing(false));
            });
    };
};

export const getAllBrands = (number, size) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(
                FETCH_ALL_BRANDS +
                `?number=${number}&size=${size}&sortField=name&sortDirection=asc`
            )
            .then(response => {
                const content = response.data;
                dispatch(setAllBrands(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setAllBrands({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getStoresByItemId = (uuid, showStoresByItemId, cb, itemName) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(FETCH_STORE_BY_ITEM_ID + uuid)
            .then(response => {
                const content = response.data;
                dispatch(setProcessing(false));
                dispatch(setStoreByItem({content, showStoresByItemId, itemName}));
                cb() && cb();
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setStoreByItem({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getItemsByCategoryId = (
    uuid,
    showItemsByCategory,
    cb,
    categoryName,
    number,
    size
) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(FETCH_ITEMS_BY_CATEGORY_ID + uuid + `?number=${number}&size=${size}`)
            .then(response => {
                const content = response.data;
                dispatch(setProcessing(false));
                dispatch(
                    setItemByCategory({
                        content,
                        showItemsByCategory,
                        categoryName,
                        uuid
                    })
                );
                cb() && cb();
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setItemByCategory({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const importItems = data => {
    let obj = {items: data};
    return (dispatch, getState) => {
        dispatch(setProcessing(true));
        axios
            .post(IMPORT_ITEMS, obj)
            .then(response => {
                const content = response.data;
                alert(
                    `Your Badge Id is ${content.batchId}. An email has been successfully sent to your mail.`
                );
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setProcessing(false));
            });
    };
};

export const getAllCategoriesBySearch = data => {
    let obj = {search: data};
    return (dispatch, getState) => {
        dispatch(setProcessing(true));
        axios
            .post(FETCH_SEARCH_CATEGORIES, obj)
            .then(response => {
                const content = response.data;
                dispatch(setAllCategoriesBySearch(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                console.log("its error", message);
                dispatch(setProcessing(false));
            });
    };
};

export const attachItemsWithPackage = (packageId, items, updateItem) => {
    let obj = {storeItems: items};
    let itemAxios;

    if (updateItem === "true") {
        itemAxios = axios.put(UPDATE_ITEMS_FOR_PACKAGE + packageId, obj);
    } else {
        itemAxios = axios.post(SAVE_ITEMS_FOR_PACKAGE + packageId, obj);
    }
    return dispatch => {
        dispatch(setProcessing(true));
        itemAxios.then(response => {
            const {message} = response.data;
            NotificationManager.success(message, "", 3000);
            dispatch(setProcessing(false));
        })
            .catch(err => {
                let message = err.response && err.response.data && err.response.data.message;
                console.log(message);
                NotificationManager.error(message, "", 3000);
                dispatch(setProcessing(false));
            });
    };
};

export const getAllItemsOfStoresBySearch = data => {
    let obj = {expression: data};
    return (dispatch, getState) => {
        dispatch(setProcessing(true));
        let storeUUId = getState().storeReducer.storeData ? getState().storeReducer.storeData.uuid : null;
        axios.post(FETCH_SEARCH_ITEMS + storeUUId, obj)
            .then(response => {
                const {content} = response.data;
                dispatch(setAllItemsBySearch(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                console.log(message);
                dispatch(setProcessing(false));
            });
    };
};

export const importItemsForStores = (data, uuid) => {
    let obj = {storeItems: data};
    return dispatch => {
        dispatch(setProcessing(true));

        axios
            .post(IMPORT_STORE_ITEMS + uuid + "/import", obj)
            .then(response => {
                const content = response.data;
                alert(`File is imported successfully.`);
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message =
                    err.response &&
                    err.response.data &&
                    err.response.data.error_description;
                console.log("its error", message);
                alert(message);
                dispatch(setProcessing(false));
            });
    };
};

export const getItemsByBrandId = (
    uuid,
    showItemsByBrand,
    cb,
    brandName,
    number,
    size
) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(FETCH_ITEMS_BY_BRAND_ID + uuid + `?number=${number}&size=${size}`)
            .then(response => {
                const content = response.data;
                dispatch(setProcessing(false));
                dispatch(
                    setItemByBrand({content, showItemsByBrand, brandName, uuid})
                );
                cb() && cb();
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setItemByBrand({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getSingleItemDetail = (itemUUID, cb) => {
    return (dispatch, getState) => {
        let storeUUID =
            getState().storeReducer.storeData &&
            getState().storeReducer.storeData.uuid;
        dispatch(setProcessing(true));
        axios
            .get(FETCH_SINGLE_ITEM_DETAIL + storeUUID + "/" + itemUUID)
            .then(response => {
                const {content} = response.data;
                dispatch(setSingleItemDetail(content));
                dispatch(setProcessing(false));
                cb() && cb();
            })
            .catch(err => {
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setSingleItemDetail({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const updateItem = (itemData, uuid) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .put(UPDATE_ITEM + uuid, itemData)
            .then(response => {
                let {message} = response.data;
                NotificationManager.success(message, "", 3000);
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                NotificationManager.error(message, "", 3000);
                dispatch(setProcessing(false));
            });
    };
};

export const getStaffedStores = () => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(FETCH_STAFFES_STORES)
            .then(response => {
                const content = response.data;
                dispatch(setStaffedStores(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setStaffedStores({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const searchItems = searchValue => {
    let searchObj;
    if (searchValue) {
        searchObj = {
            search: searchValue
        };
    }
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .post(SEARCH_ITEMS, searchObj)
            .then(response => {
                const content = response.data;
                dispatch(setAllItems(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let messageArray =
                    err.response && err.response.data && err.response.data.errors;
                let message =
                    err.response && err.response.data && err.response.data.message;
                if (messageArray) {
                    for (let i = 0; i < messageArray.length; i++) {
                        NotificationManager.error(messageArray[i], "", 5000);
                    }
                } else {
                    NotificationManager.error(message, "", 5000);
                }
                dispatch(setProcessing(false));
            });
    };
};

export const saveCategory = (categoryData, cb, callBack) => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));
        let url = SAVE_CATEGORY;
        let categoryAxios = null;
        let categoryUUID = getState().storeReducer.categoryData
            ? getState().storeReducer.categoryData.uuid
            : null;
        if (categoryUUID) {
            url += `/${categoryUUID}`;
            categoryAxios = axios.put(url, categoryData);
        } else {
            categoryAxios = axios.post(url, categoryData);
        }
        categoryAxios.then(response => {
            NotificationManager.success("Category Saved Successfully", "", 3000);
            dispatch(setProcessing(false));
            cb() && cb();
            callBack() && callBack();
        })
            .catch(err => {
                let messageArray = err.response && err.response.data && err.response.data.fieldErrors ? err.response.data.fieldErrors : [];
                let message = err.response && err.response.data && err.response.data.error_description;
                if (messageArray.length > 0) {
                    for (let i = 0; i < messageArray.length; i++) {
                        NotificationManager.error(messageArray[i], "", 5000);
                    }
                } else {
                    NotificationManager.error(message, "", 5000);
                }
                cb() && cb();
                dispatch(setProcessing(false));
            });
    };
};

export const getStorePackages = (storeUUID, storeName) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(GET_STORE_PACKAGES + storeUUID)
            .then(response => {
                const content = response.data;
                dispatch(setStorePackages({content, storeName, storeUUID}));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setStorePackages({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const saveStorePackage = (storePackageData, storeName, storePackageUUID, cb) => {
    let storeUUID = storePackageData && storePackageData.storeId;
    return dispatch => {
        dispatch(setProcessing(true));
        let url = SAVE_STORE_PACKAGE;
        let packageAxios = null;
        if (storePackageUUID) {
            url += `/${storePackageUUID}`;
            packageAxios = axios.put(url, storePackageData);
        } else {
            packageAxios = axios.post(url, storePackageData);
        }
        packageAxios
            .then(response => {
                const {message} = response.data;
                NotificationManager.success(message, "", 3000);
                dispatch(getStorePackages(storeUUID, storeName));
                dispatch(setProcessing(false));
                cb && cb();
            })
            .catch(err => {
                let messageArray = err.response && err.response.data && err.response.data.errors;
                let message = err.response && err.response.data && err.response.data.message;
                if (messageArray) {
                    for (let i = 0; i < messageArray.length; i++) {
                        NotificationManager.error(messageArray[i], "", 5000);
                    }
                } else {
                    NotificationManager.error(message, "", 5000);
                }

                dispatch(setProcessing(false));
            });
    };
};

export const deleteStorePackage = (id, index, storeUUID, storeName) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios.delete(DELETE_PACKAGE + id)
            .then(response => {
                const {message} = response.data;
                NotificationManager.success(message, "", 3000);
                dispatch(_deleteStorePackage(index));
                dispatch(setProcessing(false));
                dispatch(getStorePackages(storeUUID, storeName));
            })
            .catch(err => {
                let message = err.response && err.response.data && err.response.data.message;
                NotificationManager.error(message, "", 3000);
                dispatch(setProcessing(false));
            });
    };
};

export const setStoreName = (payload) => {
    return {
        type: actions.SET_STORE_NAME,
        payload
    }
};
export const setStoreData = storeData => {
    return {
        type: actions.SET_STORE_DATA,
        payload: storeData
    };
};

export const setItemData = payload => {
    return {
        type: actions.SET_ITEM_DATA,
        payload
    };
};

export const setEditItemBtn = payload => {
    return {
        type: actions.SET_EDIT_ITEM_BTN,
        payload
    };
};

export const setPagination = payload => {
    return {
        type: actions.SET_PAGINATION,
        payload
    };
};

export const getStoreDeals = (storeUUID, storeName) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(GET_STORE_DEALS + storeUUID)
            .then(response => {
                const content = response.data;
                dispatch(setStoreDeals({content, storeName, storeUUID}));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message = err.response && err.response.data && err.response.data.message;
                dispatch(setStoreDeals({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const deleteStoreDeal = (id, index, storeUUID, storeName, itemIndex) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios.delete(DELETE_DEAL + id)
            .then(response => {
                const {message} = response.data;
                NotificationManager.success(message, "", 3000);
                dispatch(_deleteStoreDeal({index, itemIndex}));
                dispatch(getStoreDeals(storeUUID, storeName));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message = err.response && err.response.data && err.response.data.message;
                NotificationManager.error(message, "", 3000);
                dispatch(setProcessing(false));
            });
    };
};

export const saveStoreDeal = (storeDealData, storeName, storeDealUUID) => {
    let storeUUID = storeDealData && storeDealData.storeId;
    return dispatch => {
        dispatch(setProcessing(true));
        let url = SAVE_STORE_DEAL;
        let dealAxios = null;
        if (storeDealUUID) {
            // url += `/${storeDealUUID}`;
            dealAxios = axios.put(url, storeDealData);
        } else {
            dealAxios = axios.post(url, storeDealData);
        }
        dealAxios.then(response => {
            const {message} = response.data;
            NotificationManager.success(message, "", 3000);
            dispatch(getStoreDeals(storeUUID, storeName));
            dispatch(setProcessing(false));
        })
            .catch(err => {
                let messageArray = err.response && err.response.data && err.response.data.errors;
                let message = err.response && err.response.data && err.response.data.message;
                if (messageArray) {
                    for (let i = 0; i < messageArray.length; i++) {
                        NotificationManager.error(messageArray[i], "", 5000);
                    }
                } else {
                    NotificationManager.error(message, "", 5000);
                }
                dispatch(setProcessing(false));
            });
    };
};

export const getCategoryById = (uuid, cb) => {

    return (dispatch => {
        dispatch(setProcessing(true));
        axios.get(FETCH_CATEGORY_BY_ID + uuid)
            .then(res => {
                dispatch(setProcessing(false));
                dispatch(setSingleCategory(res.data.content));
                cb && cb();

            })
            .catch(err => {
                console.error(err);
                dispatch(setProcessing(false));
                cb && cb(err);
            });
    })
};

export const getBrandById = (uuid, cb) => {
    return (dispatch => {
        dispatch(setProcessing(true));
        axios.get(GET_BRAND_BY_ID + uuid)
            .then(res => {
                dispatch(setProcessing(false));
                dispatch(setSingleBrand(res.data.content));
                cb && cb();

            })
            .catch(err => {
                console.error(err);
                dispatch(setProcessing(false));
                cb && cb(err);
            });
    });
};

export const resetRedux = () => {
    return {
        type: actions.RESET_REDUX
    };
};

export const setSingleBrand = (payload) => {
    return {
        type: actions.GET_BRAND_BY_ID,
        payload
    }
};
export const setSingleCategory = (payload) => {
    return {
        type: actions.GET_CATEGORY_BY_ID,
        payload
    }
};