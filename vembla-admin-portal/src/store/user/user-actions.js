import AxiosLogin from "axios";
import * as actions from "./user-action-types";
import {SERVER_URL, USER_LOGIN} from "../../utils/config";
import {joinRoom} from "../../socket-io/socket-io-utils";
import {NotificationManager} from "react-notifications";

const setProcessing = processing => {
    return {
        type: actions.PROCESSING,
        payload: processing
    };
};

const loginUser = args => {
    return {
        type: actions.LOGIN,
        payload: args
    };
};

export function login(email, password) {
    return dispatch => {
        dispatch(setProcessing(true));
        AxiosLogin.post(SERVER_URL + USER_LOGIN, {email, password})
            .then(response => {
                console.log("login resp:", response.data.principal)
                console.log("TOKEN:", response.data.token)
                const {data} = response;
                joinRoom(data.token);
                dispatch(loginUser(data));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message = err.response && err.response.data && err.response.data.message;
                dispatch(loginUser({error: message}));
                dispatch(setProcessing(false));
            });
    };
}

export const logout = () => {
    return dispatch => {
        dispatch({
            type: actions.LOGOUT,
            payload: null
        });
    };
};

export const resetRedux = () => {
    return {
        type: actions.RESET_REDUX
    };
};
