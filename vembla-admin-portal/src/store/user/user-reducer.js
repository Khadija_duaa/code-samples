import * as actions from "./user-action-types";

const getUserAuth = () => {
  const userKey = "user-auth";
  let userState = sessionStorage.getItem(userKey);
  if (userState && userState.length) {
    return JSON.parse(userState);
  }
  return null;
};

const _initState = {
  processing: false,

  authenticated: false,
  loginData: null,
  error: null,

  isAdmin: false,
  message: null,
  showBrands: false,
  showCategories: false,
  showConsumers: false,
  showDeliveredOrders: false,
  showFleet: false,
  showItems: false,
  showShoppers: false,
  showStores: false,
  token: null
};

const initState = () => {
  const prevState = getUserAuth();
  if (!prevState) {
    return { ..._initState };
  }
  return prevState;
};

const reducer = (state = initState(), action) => {
  let newState = { ...state };

  switch (action.type) {
    case actions.LOGIN:
      login(newState, action.payload);
      break;

    case actions.PROCESSING:
      setProcessing(newState, action.payload);
      break;

    case actions.LOGOUT:
      logout(newState);
      break;

    case actions.RESET_REDUX:
      newState = { ...initState };
      break;

    default: {
      // do nothing
    }
  }
  return newState;
};

const setProcessing = (state, processing) => {
  state.processing = processing;
};

const login = (state, data) => {
  state.token = data.token;
  state.error = data.error;
  state.loginData = data.principal;
  if (!state.error && state.token) {
    state.authenticated = true;
    sessionStorage.setItem("user-auth", JSON.stringify(state));
  }
};

const logout = state => {
  sessionStorage.removeItem("user-auth");
  state.processing = false;
  state.authenticated = false;
  state.loginData = null;
  state.error = null;
  state.isAdmin = false;
  state.message = null;
  state.showBrands = false;
  state.showCategories = false;
  state.showConsumers = false;
  state.showDeliveredOrders = false;
  state.showFleet = false;
  state.showItems = false;
  state.showShoppers = false;
  state.showStores = false;
  state.token = null;
};

export default reducer;
