import * as actions from "./order-action-types";
import axios from "../../utils/axios";
import {
    GET_PENDING_ORDERS,
    GET_ORDER_HISTORY,
    GET_CANCEL_ORDER
} from "../../utils/config";

const setProcessing = processing => {
    return {
        type: actions.PROCESSING,
        payload: processing
    };
};

const setPendingOrders = payload => {
    return {
        type: actions.SET_PENDING_ORDERS,
        payload
    };
};

const setOrderHistory = payload => {
    return {
        type: actions.SET_ORDER_HISTORY,
        payload
    };
};

const setCancelOrder = payload => {
    return {
        type: actions.SET_CANCEL_ORDER,
        payload
    };
};

export const getPendingOrders = () => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(GET_PENDING_ORDERS)
            .then(response => {
                const content = response.data;
                dispatch(setPendingOrders(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setPendingOrders({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getOrdersHistory = (page, limit) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(GET_ORDER_HISTORY + `&page=${page}&limit=${limit}`)
            .then(response => {
                const content = response.data;
                dispatch(setOrderHistory(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                let message =
                    err.response && err.response.data && err.response.data.message;
                dispatch(setOrderHistory({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const getCancelOrders = (page, limit) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .get(GET_CANCEL_ORDER + `&page=${page}&limit=${limit}`)
            .then(response => {
                const content = response.data;
                dispatch(setCancelOrder(content));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                let message = err.response && err.response.data && err.response.data.message;
                dispatch(setCancelOrder({error: message}));
                dispatch(setProcessing(false));
            });
    };
};

export const resetRedux = () => {
    return {
        type: actions.RESET_REDUX
    };
};
