import * as actions from "./order-action-types";

const initState = {
    pendingOrders: null,
    orderHistory: null,
    totalPages: "",
    orderHistoryData: "",
    processing: false
};

const reducer = (state = initState, action) => {
    let newState = {...state};

    switch (action.type) {
        case actions.SET_PENDING_ORDERS:
            setPendingOrders(newState, action.payload);
            break;

        case actions.SET_ORDER_HISTORY:
            setOrderHistory(newState, action.payload);
            break;

        case actions.SET_CANCEL_ORDER:
            setCancelOrder(newState, action.payload);
            break;

        case actions.PROCESSING:
            setProcessing(newState, action.payload);
            break;

        case actions.RESET_REDUX:
            newState = {...initState};
            break;

        default: {
            // do nothing
        }
    }
    return newState;
};

const setProcessing = (state, processing) => {
    state.processing = processing;
};

const setPendingOrders = (state, data) => {
    state.pendingOrders = data.content && data.content.data;
};

const setOrderHistory = (state, data) => {
    if (data && data.content) {
        state.orderHistory = data.content.data;
        state.totalPages = data.content.pageCount;
        state.orderHistoryData = data.content;
    }
};

const setCancelOrder = (state, data) => {
    if (data && data.content) {
        state.cancelOrder = data.content.data;
        state.totalPages = data.content.pageCount;
        state.cancelOrderData = data.content;
    }
};
export default reducer;
