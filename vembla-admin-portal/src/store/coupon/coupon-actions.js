import axios from "../../utils/axios";
import * as actions from "./coupon-action-types";
import {
  CREATE_COUPON,
  FETCH_COUPONS,
  DELETE_COUPONS
} from "../../utils/config";

export const createCoupon = couponInfo => {
  return dispatch => {
    dispatch(setProcessing(true));
    axios
      .post(CREATE_COUPON, couponInfo)
      .then(res => {
        dispatch(onSuccess("Coupon Created Successfully"));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        dispatch(onFail(err.message));
        dispatch(setProcessing(false));
      });
  };
};

export const fetchCoupons = () => {
  return dispatch => {
    dispatch(setProcessing(true));
    axios
      .get(FETCH_COUPONS)
      .then(res => {
        const { data } = res;
        dispatch(setCoupons(data.content));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        dispatch(onFail(err.message));
        dispatch(setProcessing(false));
      });
  };
};

export const deleteCoupon = uuid => {
  return dispatch => {
    dispatch(setProcessing(true));
    axios
      .delete(`${DELETE_COUPONS}/${uuid}`)
      .then(res => {
        const { status } = res;
        if (status === 200) dispatch(onSuccess("Coupon Deleted Successfully"));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        dispatch(onFail(err.message));
        dispatch(setProcessing(false));
      });
  };
};

const setCoupons = payload => {
  return {
    type: actions.FETCH_COUPONS,
    payload: {
      coupons: payload
    }
  };
};

const setProcessing = payload => {
  return {
    type: actions.SET_PROCESSING,
    payload
  };
};

const onSuccess = msg => {
  return {
    type: actions.COUPON_ACTION_SUCCESS,
    payload: { msg, isSuccess: true }
  };
};

const onFail = msg => {
  return {
    type: actions.COUPON_ACTION_FAIL,
    payload: { msg, isSuccess: false }
  };
};
