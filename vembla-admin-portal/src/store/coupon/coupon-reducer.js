import {
  COUPON_ACTION_SUCCESS,
  COUPON_ACTION_FAIL,
  FETCH_COUPONS,
  SET_PROCESSING
} from "./coupon-action-types";

const initialState = {
  processing: false,
  coupons: [],
  isSuccess: null,
  responseMsg: ""
};

const reducer = (state = initialState, action) => {
  let newState = { ...state };
  switch (action.type) {
    case SET_PROCESSING:
      setProcessing(newState, action.payload);
      break;
    case FETCH_COUPONS:
      setCoupons(newState, action.payload);
      break;
    case COUPON_ACTION_SUCCESS:
      onCouponActionRes(newState, action.payload);
      break;
    case COUPON_ACTION_FAIL:
      onCouponActionRes(newState, action.payload);
      break;
    default:
      break;
  }
  return newState;
};

const setProcessing = (state, data) => {
  state.processing = data;
  state.isSuccess = null;
  state.responseMsg = "";
};

const setCoupons = (state, data) => {
  state.coupons = data.coupons;
};

const onCouponActionRes = (state, payload) => {
  state.isSuccess = payload.isSuccess;
  state.responseMsg = payload.msg;
};

export default reducer;
