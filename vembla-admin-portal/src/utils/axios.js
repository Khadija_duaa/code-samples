import axios from "axios";
import store from "../store/store";
import { SERVER_URL } from "./config.js";

const getOptions = () => {
  return {
    headers: {
      Authorization: `Bearer ${store.getState().user.token}`
    }
  };
};

const prepareUrl = api => SERVER_URL + api;

const wrapper = {
  get: api => axios.get(prepareUrl(api), getOptions()),
  post: (api, formData = {}) =>
    axios.post(prepareUrl(api), formData, getOptions()),
  put: (api, formData = {}) =>
    axios.put(prepareUrl(api), formData, getOptions()),
  patch: (api, formData = {}) =>
    axios.patch(prepareUrl(api), formData, getOptions()),
  delete: api => axios.delete(prepareUrl(api), getOptions())
};

export default wrapper;
