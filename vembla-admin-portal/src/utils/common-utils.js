import moment from 'moment';

export const getTdWidth = items => {
  return `${100 / items.length}%`;
};

export const getDateFromISO = iso => {
  return moment(iso).format('LL');
};

export const capitalize = (name = '') => {
  return name.charAt(0).toUpperCase() + name.slice(1);
};

export const timeSplitter = date => {
  let newTime = moment(date).format('LT');
  return newTime;
};

export const defaultBlankImg =
  'https://services.validoo.se/images/no-image-icon.png';

export const getDateFromPST = (date) => {
    if(date){
        let newDate = new Date(date);
        let month = ("0" + (newDate.getMonth() + 1)).slice(-2);
        let day = ("0" + newDate.getDate()).slice(-2);
        return [newDate.getFullYear(), month, day].join("-");
    }
};

export const parseIntDate = (date) => {
  if(date){
      // let year = parseInt(date);
      // let month = parseInt(date.charAt(5) + date.charAt(6));
      // let day = parseInt(date.charAt(8) + date.charAt(9));
      // return year + "-" + month + "-" + day;

      // let t=date.length;
      // if (date.charAt(0)=== '"'){
      //     date=date.substring(1,t--);
      // }
      // if (date.charAt(--t)=== '"'){
      //     date=date.substring(0,t);
      // }
      // return date;
  }
};