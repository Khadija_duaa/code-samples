// Server
// export const SERVER_URL = "https://prod.vembla.se:6881";
// export const SERVER_URL = 'http://192.168.1.69:8080';
export const SERVER_URL = 'http://192.168.1.66:8080';
// export const SERVER_URL = 'http://58.27.238.210:8005';
// export const SERVER_URL = 'https://demo.vembla.se:8080';

//User
export const USER_LOGIN = '/vembla-admins/login/basic';

//Consumer
export const FETCH_CONSUMER = '/vembla-consumers/users/consumer';

//Shopper
export const FETCH_APPROVED_SHOPPER = '/vembla-shoppers/users/shopper';
export const FETCH_PENDING_SHOPPER = '/vembla-shoppers/users/pending';
export const APPROVE_PENDING_SHOPPER = '/vembla-shoppers/users/pending/shopper/';
export const ACTIVATE_APPROVED_SHOPPER = '/vembla-shoppers/users/shopper/activate/';
export const DEACTIVATE_APPROVED_SHOPPER = '/vembla-shoppers/users/shopper/deactivate/';

//Shopper - Vehicle
export const GET_SHOPPER_VEHICLE_INFO = '/vehicle-management/vehicle/user/get/';
export const GET_ALL_VEHICLES = '/vehicle-management/vehicle/get';
export const CREATE_NEW_VEHICLE = '/vehicle-management/vehicle/save';
export const UPDATE_VEHICLE = '/vehicle-management/vehicle/update';
export const DELETE_VEHICLE = '/vehicle-management/vehicle/delete/';
export const DELETE_CATEGORY = '/validoo-service/categories/';
export const GET_VEHICLE_TYPES = '/vehicle-management/vehicle-types';
export const SAVE_USER_VEHICLE = '/vehicle-management/vehicles';

//Shopper - Programs
export const GET_ALL_PROGRAMS = '/shopper-commission-management/programs';
export const SAVE_SHOPPER_PROGRAM = '/shopper-commission-management/programs/users/attach/';

//Fleet
export const SET_FLEET_A_STAFF_STORE_SHOPPER = '/fleet-management/fleet/staffed-store-fleet';
export const ADD_FLEET_DATA = '/fleet-management/fleet/register-agent';

export const SAVE_REVOLUT_INFO = '/fleet-management/fleet/revolut-info';
export const SAVE_GIGAPAY_INFO = '/fleet-management/fleet/giga-pay-info';

export const ASSIGN_ORDER_MANUALLY = '/fleet-management/fleet/order-manual-assignment/';
export const FETCH_APPROVED_FLEET_DETAIL = '/fleet-management/fleet/fleet-information/';
export const EMPLOYEE_COMMISSIONS = '/fleet-management/fleet/employee-commissions';
export const CREATE_EMPLOYEE_COMMISSIONS = '/fleet-management/fleet/hourly-based-commission';

//Revolut Management
export const GET_REVOLUT_ACCOUNTS = '/revolut-management/revolut/accounts';

// Coupon
export const CREATE_COUPON = '/inventory-management/coupons';
export const FETCH_COUPONS = '/inventory-management/coupons';
export const DELETE_COUPONS = '/inventory-management/coupons';

//Store
export const FETCH_ALL_STORES = '/inventory-management/stores';
export const CREATE_NEW_STORE = '/inventory-management/stores';
export const ENABLE_STORE = '/inventory-management/stores/activate/';
export const DISABLE_STORE = '/inventory-management/stores/de-activate/';
export const FETCH_STORE_ITEMS = '/inventory-management/stores/items/';
export const FETCH_STORE_BY_ITEM_ID = '/inventory-management/items/stores/';
export const FETCH_STAFFES_STORES = '/inventory-management/stores/staffed/stores';
export const FETCH_STORE_BY_UUID  = "/inventory-management/stores/";
//Store - Packages - Deals
export const GET_STORE_PACKAGES = '/inventory-management/item-packages/store/packages/';
export const SAVE_STORE_PACKAGE = '/inventory-management/item-packages';
export const DELETE_PACKAGE = '/inventory-management/item-packages/';
export const GET_STORE_DEALS = '/inventory-management/deals/store/';
export const SAVE_STORE_DEAL = '/inventory-management/deals';
export const DELETE_DEAL = '/inventory-management/deals/';

//Items
export const FETCH_ALL_ITEMS = '/validoo-service/items';
export const SEARCH_ITEMS = '/validoo-service/items/search';
export const FETCH_ITEMS_BY_CATEGORY_ID = '/inventory-management/items/categories/';
export const IMPORT_ITEMS = '/validoo-service/items/import';
export const FETCH_SEARCH_CATEGORIES = '/validoo-service/categories/search';
export const FETCH_SEARCH_ITEMS = '/inventory-management/items/search/';
export const SAVE_ITEMS_FOR_PACKAGE = '/inventory-management/item-packages/add-item/';
export const UPDATE_ITEMS_FOR_PACKAGE = '/inventory-management/item-packages/update-item/';
export const IMPORT_STORE_ITEMS = '/validoo-service/stores/';
export const FETCH_ITEMS_BY_BRAND_ID = '/inventory-management/items/brands/';
export const UPDATE_ITEM = '/inventory-management/stores/items/';
export const FETCH_SINGLE_ITEM_DETAIL = '/inventory-management/stores/items/';

//Categories
export const FETCH_ALL_CATEGORIES = '/validoo-service/categories';
export const FETCH_CATEGORY_BY_ID = '/inventory-management/categories/';
export const SAVE_CATEGORY = '/validoo-service/categories';

//Brands
export const FETCH_ALL_BRANDS = '/inventory-management/brands';
export const GET_BRAND_BY_ID = '/inventory-management/brands/';

//Orders
export const GET_PENDING_ORDERS = '/order-management/orders?pending=true';
export const GET_ORDER_HISTORY = '/order-management/orders?delivered=true';
export const GET_CANCEL_ORDER = '/order-management/orders?canceled=true';

//Roles
export const FETCH_ROLES = '/vembla-admins/roles';

//Admin
export const CREATE_NEW_ADMIN = '/vembla-admins/users/create/';
export const FETCH_ADMINS = '/vembla-admins/users/';

export const csvToJson = csvData => {
  let lines = csvData.split('\n');
  let result = [];
  let headers = lines[0].split(',').map(data => data.trimEnd());
  for (let i = 1; i < lines.length; i++) {
    let obj = {};
    let currentline = lines[i].split(',').map(info => info.trimEnd());
    for (let j = 0; j < headers.length; j++) {
      let index = headers[j];

      obj[index] = currentline[j];
    }
    result.push(obj);
  }
  return JSON.stringify(result);
};
