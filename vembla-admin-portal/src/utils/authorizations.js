import store from '../store/store';
import _ from 'lodash';

let authorities = [];

export const filterAuthorizedRoutes = (routes = []) => {
    let authorizedRoutes = [];
    routes.forEach(route => {
        const routeAuthorizations = route.authorities;

        if (!routeAuthorizations || _.isEmpty(routeAuthorizations) || hasAnyAuthority(routeAuthorizations)) {
            authorizedRoutes.push(route);
        }
    });
    return authorizedRoutes;
};

export const hasAnyAuthority = (requiredAuthorities = []) => {
    authorities = store.getState().user && store.getState().user.loginData ? store.getState().user.loginData.authorities : [];

    if (authorities.indexOf("SUPER_ADMIN") > 0) {
        return true;
    }
    for (let i = 0; i < requiredAuthorities.length; i++) {
        let auth = requiredAuthorities[i];
        if (authorities.indexOf(auth) > 0) {
            return true;
        }
    }
    return false;
};

export const hasAuthority = (authority) => {
    authorities = store.getState().user && store.getState().user.loginData ? store.getState().user.loginData.authorities : [];
    return authorities.indexOf("SUPER_ADMIN") > 0 || authorities.indexOf(authority) > 0;
};
