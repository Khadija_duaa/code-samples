import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import shortid from "shortid";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";

// core components
import AdminNavbar from "../../components/Navbars/AdminNavbar.jsx";
import Footer from "../../components/Footer/Footer.jsx";
import Sidebar from "../../components/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../components/FixedPlugin/FixedPlugin.jsx";

import routes from "../../routes.js";

import logo from "../../assets/img/vembla-logo.png";
import { connect } from "react-redux";
import { filterAuthorizedRoutes } from "../../utils/authorizations";

var ps;
let authorizedRoutes = [];

export const getRoutes = routes => {
  return routes.map(prop => {
    if (prop.layout === "/admin") {
      if (prop.submenu && prop.submenu === true) {
        prop.subOptions.map((optionProp, i) => {
          if (optionProp.layout === "/admin") {
            return (
              <Route
                key={i}
                path={optionProp.layout + optionProp.path}
                component={optionProp.component}
              />
            );
          } else {
            return null;
          }
        });
      } else {
        return (
          <Route
            key={shortid.generate()}
            path={prop.layout + prop.path}
            component={prop.component}
          />
        );
      }
    } else {
      return null;
    }
  });
};

class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: "blue",
      sidebarOpened:
        document.documentElement.className.indexOf("nav-open") !== -1
    };
  }

  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      document.documentElement.className += " perfect-scrollbar-on";
      document.documentElement.classList.remove("perfect-scrollbar-off");
      ps = new PerfectScrollbar(this.refs.mainPanel, { suppressScrollX: true });
      let tables = document.querySelectorAll(".table-responsive");
      for (let i = 0; i < tables.length; i++) {
        ps = new PerfectScrollbar(tables[i]);
      }
    }
  }

  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
      document.documentElement.className += " perfect-scrollbar-off";
      document.documentElement.classList.remove("perfect-scrollbar-on");
    }
  }

  componentDidUpdate(e) {
    if (e.history.action === "PUSH") {
      if (navigator.platform.indexOf("Win") > -1) {
        let tables = document.querySelectorAll(".table-responsive");
        for (let i = 0; i < tables.length; i++) {
          ps = new PerfectScrollbar(tables[i]);
        }
      }
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.refs.mainPanel.scrollTop = 0;
    }
  }

  // this function opens and closes the sidebar on small devices
  toggleSidebar = url => {
    document.documentElement.classList.toggle("nav-open");
    this.setState({ sidebarOpened: !this.state.sidebarOpened });
    let string = document.URL;
    // string = string.split("/admin").pop();
    string = string.substring(string.indexOf("/admin") + 1);
    if (url === `/${string}`) {
      window.location.reload();
    }
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  getBrandText = path => {
    for (let i = 0; i < authorizedRoutes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          authorizedRoutes[i].layout + authorizedRoutes[i].path
        ) !== -1
      ) {
        if (this.props.activeLanguageLabel === "US") {
          return authorizedRoutes[i].name.US;
        } else {
          return authorizedRoutes[i].name.SE;
        }
      }
    }
    return "Brand";
  };

  render() {
    authorizedRoutes = filterAuthorizedRoutes(routes);

    return (
      <>
        <div className="wrapper">
          <Sidebar
            {...this.props}
            routes={authorizedRoutes}
            bgColor={this.state.backgroundColor}
            logo={{
              outterLink: "https://www.creative-tim.com/",
              text: "Vembla",
              imgSrc: logo
            }}
            toggleSidebar={this.toggleSidebar}
          />
          <div
            className="main-panel"
            ref="mainPanel"
            data={this.state.backgroundColor}
          >
            <AdminNavbar
              {...this.props}
              brandText={this.getBrandText(this.props.location.pathname)}
              toggleSidebar={this.toggleSidebar}
              sidebarOpened={this.state.sidebarOpened}
            />
            <Switch>{getRoutes(authorizedRoutes)}</Switch>
            {// we don't want the Footer to be rendered on map page
            this.props.location.pathname.indexOf("maps") !== -1 ? null : (
              <Footer fluid />
            )}
          </div>
        </div>
        {/*<FixedPlugin
          bgColor={this.state.backgroundColor}
          handleBgClick={this.handleBgClick}
        />*/}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    activeLanguageLabel: state.multilingual.activeLanguageLabel,
    isAdmin: state.user.isAdmin,
    showFleet: state.user.showFleet,
    showShoppers: state.user.showShoppers,
    showStores: state.user.showStores,
    showItems: state.user.showItems,
    showCategories: state.user.showCategories,
    showBrands: state.user.showBrands,
    showConsumers: state.user.showConsumers,
    showDeliveredOrders: state.user.showDeliveredOrders
  };
};

const connectedComponent = connect(mapStateToProps)(Admin);
export default withRouter(connectedComponent);
