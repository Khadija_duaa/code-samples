// Imports Listed Alphabetically
import Admins from "./containers/admins";
import Brands from "./containers/brands";
import Categories from "./containers/categories";
import Consumer from "./containers/consumer/index";
import Coupon from "./containers/Coupon";
import Dashboard from "./layouts/views/Dashboard.jsx";
import Orders from "./containers/Orders/index";
import FleetManagement from "./containers/fleetManagement";
import Icons from "./layouts/views/Icons.jsx";
import Items from "./containers/items";
import Map from "./layouts/views/Map.jsx";
import Notifications from "./layouts/views/Notifications.jsx";
import Rtl from "./layouts/views/Rtl.jsx";
import ShopperApproved from "./containers/shopper/shopperApproved";
import ShopperPending from "./containers/shopper/shopperPending";
import Store from "./containers/store";
import StoreDeals from './components/StoreDeals';
import StorePackage from './components/StorePackage';
import StoreDetails from './components/StoreDetails';
import StoreForm from "./components/StoreForm";
import AdminForm from "./components/AdminForm";
import CanceledOrders from "./containers/Orders/canceledOrders";
import TableList from "./layouts/views/TableList.jsx";
import Typography from "./layouts/views/Typography.jsx";
import UserProfile from "./layouts/views/UserProfile.jsx";
import Vehicles from "./containers/vehicle";

var routes = [
    {
        path: "/dashboard",
        name: {US: "Dashboard", SE: "Instrumentbräda"},
        rtlName: "لوحة القيادة",
        icon: "tim-icons icon-chart-pie-36",
        component: Dashboard,
        layout: "/admin",
        show: true
    },
    {
        path: "/admin/form",
        name: {US: "Admins", SE: "Admins"},
        rtlName: "الرموز",
        icon: "tim-icons icon-image-02",
        component: AdminForm,
        layout: "/admin",
        authorities: ["CAN_MANAGE_AUTHORIZATIONS", "CAN_CREATE_USERS"],
        show: false
    },
    {
        path: "/admins",
        name: {US: "Admins", SE: "Admins"},
        rtlName: "الرموز",
        icon: "tim-icons icon-image-02",
        component: Admins,
        layout: "/admin",
        authorities: ["CAN_MANAGE_AUTHORIZATIONS", "CAN_CREATE_USERS"],
        show: true
    },
    {
        path: "/consumers",
        name: {US: "Consumers", SE: "Konsumenter"},
        rtlName: "خرائط",
        icon: "tim-icons icon-single-02",
        component: Consumer,
        layout: "/admin",
        authorities: ["CAN_VIEW_USERS"],
        show: true
    },
    {
        path: "/shopper-pending",
        name: {US: "Pending Agents", SE: "Vänta Agenter"},
        rtlName: "الرموز",
        icon: "tim-icons icon-alert-circle-exc",
        component: ShopperPending,
        layout: "/admin",
        authorities: [
            "CAN_VIEW_USERS",
            "CAN_UPDATE_USER",
            "CAN_ACTIVATE_USER",
            "CAN_DEACTIVATE_USER",
            "CAN_APPROVE_PENDING_USER",
            "CAN_VIEW_PENDING_USERS"
        ],
        show: true
    },
    {
        path: "/shopper-approved",
        name: {US: "Approved Agents", SE: "Godkända Agenter"},
        rtlName: "الرموز",
        icon: "tim-icons icon-bulb-63",
        component: ShopperApproved,
        layout: "/admin",
        authorities: [
            "CAN_VIEW_USERS",
            "CAN_UPDATE_USER",
            "CAN_ACTIVATE_USER",
            "CAN_DEACTIVATE_USER",
            "CAN_APPROVE_PENDING_USER"
        ],
        show: true
    },
    // {
    //     path: "/shopper-vehicles",
    //     name: {'US': "Shopper Vehicles", 'SE' : 'Shoppare'},
    //     rtlName: "الرموز",
    //     icon: "tim-icons icon-bus-front-12",
    //     component: Vehicles,
    //     layout: "/admin",
    //     authorities : ["CAN_UPDATE_USER"]
    // },
    {
        path: "/fleet-management",
        name: {US: "Fleet Management", SE: "Fleet Management"},
        rtlName: "الرموز",
        icon: "tim-icons icon-user-run",
        component: FleetManagement,
        layout: "/admin",
        authorities: ["CAN_MANAGE_FLEET"],
        show: true
    },
    {
        path: "/coupons",
        name: {US: "Coupons", SE: "Kuponger"},
        rtlName: "الرموز",
        icon: "tim-icons icon-money-coins",
        component: Coupon,
        layout: "/admin",
        authorities: ["CAN_MANAGE_COUPONS", "CAN_MANAGE_STORE"],
        show: true
    },
    {
        path: "/store/details/:uuid",
        name: {US: "Stores", SE: "Butiker"},
        rtlName: "الرموز",
        icon: "tim-icons icon-bag-16",
        component: StoreDetails,
        layout: "/admin",
        authorities: ["CAN_MANAGE_STORE", "CAN_VIEW_STORE_DETAILS", "CAN_MANAGE_STORE"],
        show: false
    },
    {
        path: "/store/deals/:uuid",
        name: {US: "Stores", SE: "Butiker"},
        rtlName: "الرموز",
        icon: "tim-icons icon-bag-16",
        component: StoreDeals,
        layout: "/admin",
        authorities: ["CAN_MANAGE_STORE", "CAN_VIEW_STORE_DETAILS"],
        show: false
    },
    {
        path: "/store/package/:uuid",
        name: {US: "Stores", SE: "Butiker"},
        rtlName: "الرموز",
        icon: "tim-icons icon-bag-16",
        component: StorePackage,
        layout: "/admin",
        authorities: ["CAN_MANAGE_STORE", "CAN_VIEW_STORE_DETAILS"],
        show: false
    },
    {
        path: "/store/form",
        name: {US: "Stores", SE: "Butiker"},
        rtlName: "الرموز",
        icon: "tim-icons icon-bag-16",
        component: StoreForm,
        layout: "/admin",
        authorities: ["CAN_MANAGE_STORE", "CAN_VIEW_STORE_DETAILS"],
        show: false
    },
    {
        path: "/store/:id",
        name: {US: "Stores", SE: "Butiker"},
        rtlName: "الرموز",
        icon: "tim-icons icon-bag-16",
        component: StoreForm,
        layout: "/admin",
        authorities: ["CAN_MANAGE_STORE", "CAN_VIEW_STORE_DETAILS"],
        show: false
    },
    {
        path: "/store",
        name: {US: "Stores", SE: "Butiker"},
        rtlName: "الرموز",
        icon: "tim-icons icon-bag-16",
        component: Store,
        layout: "/admin",
        authorities: ["CAN_MANAGE_STORE", "CAN_VIEW_STORE_DETAILS"],
        show: true
    },
    {
        path: "/items/:type/:uuid",
        name: {US: "Items", SE: "Artiklar"},
        rtlName: "الرموز",
        icon: "tim-icons icon-bullet-list-67",
        component: Items,
        layout: "/admin",
        authorities: ["CAN_MANAGE_STORE", "CAN_VIEW_STORE_DETAILS"],
        show: false
    },
    {
        path: "/items",
        name: {US: "Items", SE: "Artiklar"},
        rtlName: "الرموز",
        icon: "tim-icons icon-bullet-list-67",
        component: Items,
        layout: "/admin",
        authorities: ["CAN_MANAGE_STORE", "CAN_VIEW_STORE_DETAILS"],
        show: true
    },

    {
        path: "/categories",
        name: {US: "Categories", SE: "Kategorier"},
        rtlName: "الرموز",
        icon: "tim-icons icon-basket-simple",
        component: Categories,
        layout: "/admin",
        authorities: ["CAN_VIEW_STORE_DETAILS"],
        show: true
    },
    {
        path: "/brands",
        name: {US: "Brands", SE: "Märken"},
        rtlName: "الرموز",
        icon: "tim-icons icon-molecule-40",
        component: Brands,
        layout: "/admin",
        authorities: ["CAN_VIEW_STORE_DETAILS"],
        show: true
    },
    {
        path: "/delivered-orders",
        name: {US: "Delivered Orders", SE: "Levererade Beställningar"},
        rtlName: "الرموز",
        icon: "tim-icons icon-check-2",
        component: Orders,
        layout: "/admin",
        authorities: ["CAN_MANAGE_FLEET"],
        show: true
    },
    {
        path: "/canceled-orders",
        name: {US: "Canceled Orders", SE: "Inställt Beställningar"},
        rtlName: "الرموز",
        icon: "tim-icons icon-simple-remove",
        component: CanceledOrders,
        layout: "/admin",
        authorities: ["CAN_MANAGE_FLEET"],
        show: true
    }

    /*{
        path: "/icons",
        name: {'US': "Icons", 'SE': 'Icons SV'},
        rtlName: "الرموز",
        icon: "tim-icons icon-atom",
        component: Icons,
        layout: "/admin",
        authorities: ["CAN_MANAGE_FLEET"],
        show: true
    },*/
    /*{
          name: "Shopper",
          rtlName: "الرموز",
          icon: "tim-icons icon-image-02",
          layout: "/admin",
          submenu: true,
          subOptions: [
              {
                  path: "/shopper-approved",
                  name: "Approved",
                  rtlName: "الرموز",
                  icon: "tim-icons icon-bulb-63",
                  component: ShopperApproved,
                  layout: "/admin"
              },
              {
                  path: "/shopper-pending",
                  name: "Pending",
                  rtlName: "الرموز",
                  icon: "tim-icons icon-alert-circle-exc",
                  component: ShopperPending,
                  layout: "/admin"
              }
          ]
      },
      {
          path: "/icons",
          name: "Icons",
          rtlName: "الرموز",
          icon: "tim-icons icon-atom",
          component: Icons,
          layout: "/admin"
      },
      {
          path: "/map",
          name: "Map",
          rtlName: "خرائط",
          icon: "tim-icons icon-pin",
          component: Map,
          layout: "/admin"
      },
      {
          path: "/notifications",
          name: "Notifications",
          rtlName: "إخطارات",
          icon: "tim-icons icon-bell-55",
          component: Notifications,
          layout: "/admin"
      },
      {
          path: "/user-profile",
          name: "User Profile",
          rtlName: "ملف تعريفي للمستخدم",
          icon: "tim-icons icon-single-02",
          component: UserProfile,
          layout: "/admin"
      },
      {
          path: "/tables",
          name: "Table List",
          rtlName: "قائمة الجدول",
          icon: "tim-icons icon-puzzle-10",
          component: TableList,
          layout: "/admin"
      },
      {
          path: "/typography",
          name: "Typography",
          rtlName: "طباعة",
          icon: "tim-icons icon-align-center",
          component: Typography,
          layout: "/admin"
      },
      {
          path: "/rtl-support",
          name: "RTL Support",
          rtlName: "ار تي ال",
          icon: "tim-icons icon-world",
          component: Rtl,
          layout: "/rtl"
      }*/
];

export default routes;
