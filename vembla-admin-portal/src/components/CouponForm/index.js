import React, { useState } from "react";
import { connect } from "react-redux";
import { Row, Col, FormGroup, Button, Alert, Label } from "reactstrap";

import { createCoupon } from "../../store/coupon/coupon-actions";

import CustomCheckboxFormGroup from "../CustomComponents/CustomCheckboxFormGroup";
import CustomDateinputFormGroup from "../CustomComponents/CustomDateInputFormGroup";
import CustomInputFormGroup from "../CustomComponents/CustomInputFormGroup";
import CustomPercentageInputFormGroup from "../CustomComponents/CustomPercentageInputFormGroup";
import CustomRadioInputFormGroup from "../CustomComponents/CustomRadioInputFormGroup";

const discountTypes = ["fixed", "percentage"];
const couponTypes = ["generic", "user"];
/*
 *   Validating all field values
 * Capturing default starting date.
 *
 */

const CouponForm = props => {
  const [amount, setAmount] = useState();
  const [couponName, setCouponName] = useState();
  const [deliveryFeeExemption, setDeliveryFeeExemption] = useState(false);
  const [discountType, setDiscountType] = useState(discountTypes[0]);
  const [endDate, setEndDate] = useState();
  const [errMsg, setErrMsg] = useState();
  const [firstOrderOnly, setFirstOrderOnly] = useState(false);
  const [onDiscountedItems, setOnDiscountedItems] = useState(false);
  const [startDate, setStartDate] = useState();
  const [type, setCouponType] = useState(couponTypes[0]);
  const [usageCountLimit, setUsageCountLimit] = useState();

  const onChange = (name, val) => {
    switch (name) {
      case "couponType":
        setCouponType(val);
        break;
      case "couponName":
        setCouponName(val);
        break;
      case "discountType":
        setDiscountType(val);
        break;
      case "deliveryFee":
        setDeliveryFeeExemption(val);
        break;
      case "firstOrder":
        setFirstOrderOnly(val);
        break;
      case "onDiscountedItems":
        setOnDiscountedItems(val);
        break;
      case "startDate":
        setStartDate(val);
        break;
      case "endDate":
        setEndDate(val);
        break;
      case "amount":
        setAmount(val);
        break;
      case "usageCountLimit":
        setUsageCountLimit(val);
        break;
      default:
        break;
    }
  };

  const isReq = () =>
    couponName &&
    discountType &&
    endDate &&
    startDate &&
    type &&
    usageCountLimit &&
    amount > -1
      ? true
      : false;

  const validateNCreateCoupon = () => {
    //   validations remaining
    if (!isReq()) {
      setErrMsg("All Fields Are Required");
      return;
    }
    const couponInfo = {
      name: couponName,
      startDate,
      endDate,
      amount,
      discountType,
      usageCountLimit,
      deliveryFeeExemption,
      affectsDiscountedItems: onDiscountedItems
      // firstOrderOnly: firstOrderOnly
    };
    props.createCoupon(couponInfo);
  };

  return (
    <div>
      {errMsg && <Alert color="danger">{errMsg}</Alert>}
      <Row>
        <Col md="12">
          <CustomInputFormGroup
            label="Coupon Name:"
            type="text"
            name="couponName"
            onChange={onChange}
          />
        </Col>
      </Row>
      <Row>
        <Col md="6">
          <CustomDateinputFormGroup
            label="Start Date:"
            name="startDate"
            onChange={onChange}
          />
        </Col>
        <Col md="6">
          <CustomDateinputFormGroup
            label="End Date:"
            name="endDate"
            onChange={onChange}
          />
        </Col>
      </Row>
      <Row>
        <Col md="6">
          <CustomRadioInputFormGroup
            label="Discount Type:"
            data={discountTypes}
            name="discountType"
            defaultValue={discountType}
            onChange={onChange}
          />
        </Col>
        <Col md="6">
          {discountType === "percentage" ? (
            <CustomPercentageInputFormGroup
              type="number"
              label="Percentage:"
              name="amount"
              onChange={onChange}
            />
          ) : (
            <CustomInputFormGroup
              label="Amount:"
              type="number"
              name="amount"
              value={amount}
              onChange={onChange}
            />
          )}
        </Col>
      </Row>
      <Row>
        <Col md="6">
          <Label>Options</Label>
          <Col md={{ size: 10, offset: 1 }}>
            <CustomCheckboxFormGroup
              label="Applicable On Discounted Items"
              name="onDiscountedItems"
              value={onDiscountedItems}
              onChange={onChange}
            />
            <CustomCheckboxFormGroup
              label="Exempt Delivery Fee"
              name="deliveryFee"
              value={deliveryFeeExemption}
              onChange={onChange}
            />
            {/* <CustomCheckboxFormGroup
              label="First Order Only"
              name="firstOrder"
              value={firstOrderOnly}
              onChange={onChange}
            /> */}
          </Col>
        </Col>
      </Row>
      <Row>
        <Col md="12">
          <CustomInputFormGroup
            label="Total Coupons"
            type="number"
            name="usageCountLimit"
            value={usageCountLimit}
            onChange={onChange}
          />
        </Col>
      </Row>
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <FormGroup>
            <Button onClick={() => validateNCreateCoupon()}>Save</Button>
          </FormGroup>
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => {
  return {
    createCoupon: couponInfo => dispatch(createCoupon(couponInfo))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CouponForm);
