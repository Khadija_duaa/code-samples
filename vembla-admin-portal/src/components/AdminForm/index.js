import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardTitle,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
} from 'reactstrap';
import {getAllRoles, saveAdmin} from "../../store/admin/admin-actions";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import Loader from '../../components/Loader/index';

class AdminForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            countryCode: '',
            password: '',
            role: '',
            roleDropdownOpen: false,
            regularTags: [],
            countryDropdownOpen: false
        };
    }

    componentDidMount() {
        this.props.getAllRoles()
    }

    prepareFields = () => {
        const inputFields = [];
        inputFields.push({
            name: 'firstName',
            placeholder: 'First Name',
            label: 'First Name',
            value: this.state.firstName
        });

        inputFields.push({
            name: 'lastName',
            placeholder: 'Last Name',
            label: 'Last Name',
            value: this.state.lastName
        });

        inputFields.push({
            name: 'phone',
            placeholder: 'Phone Number',
            label: 'Phone Number',
            value: this.state.phone
        });

        inputFields.push({
            name: 'email',
            placeholder: 'Email',
            label: 'Email',
            value: this.state.email
        });

        inputFields.push({
            name: 'password',
            placeholder: 'Password',
            label: 'Password',
            value: this.state.password
        });
        return inputFields;
    };

    onCancelBtnClick = (parentFun) => {
        // parentFun(false);
        this.props.history.push('/admin/admins')
    };


    roleToggle = () => {
        this.setState(prevState => ({
            roleDropdownOpen: !prevState.roleDropdownOpen
        }));
    };

    countryToggle = () => {
        this.setState(prevState => ({
            countryDropdownOpen: !prevState.countryDropdownOpen
        }));
    };

    onInputChange = (e) => {
        let state = {...this.state};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    countryChangeValue = (e) => {
        this.setState({countryCode: e.currentTarget.textContent})
    };

    roleChangeValue = (e) => {
        this.setState({role: e.currentTarget.textContent})
    };

    onAdminSave = (parentFun) => {
        const {firstName, lastName, phone, email, password, countryCode} = this.state;
        this.props.saveAdmin({firstName, lastName, phone, email, password, countryCode}, this.state.role,
            () => this.props.history.push('/admin/admins'));
    };

    render() {
        const fields = this.prepareFields();
        return (
            <div className="content">
                <Row>
                    <Col md="12">
                        {
                            this.props.processing ?
                                <div style={{marginBottom: '200px'}}>
                                    <Loader/>
                                </div> :
                                <Card>
                                    <CardHeader>
                                        <CardTitle tag="h4">Create New Admin</CardTitle>
                                    </CardHeader>

                                    <CardBody>
                                        <Form>
                                            <Row>
                                                {
                                                    fields.map(field => {
                                                        return (
                                                            <Col className="pr-md-1" md="6">
                                                                <FormGroup>
                                                                    <label>{field.label}</label>
                                                                    <Input
                                                                        value={field.value}
                                                                        onChange={(e) => this.onInputChange(e)}
                                                                        name={field.name}
                                                                        placeholder={field.placeholder}
                                                                        type="text"
                                                                    />
                                                                </FormGroup>
                                                            </Col>
                                                        )
                                                    })
                                                }
                                            </Row>
                                            <Row>
                                                <Col md={6}>
                                                    <Row>
                                                        <Col md={4}>
                                                            <label>Select Country Code</label>
                                                            <Dropdown isOpen={this.state.countryDropdownOpen}
                                                                      toggle={() => this.countryToggle()}>
                                                                <DropdownToggle caret>
                                                                    {this.state.countryCode === '' ? "Select Code" : this.state.countryCode}
                                                                    <span className={"dropdown-icon"}/>
                                                                </DropdownToggle>
                                                                <DropdownMenu>
                                                                    <DropdownItem>
                                                                        <div onClick={(e) => this.countryChangeValue(e)}>PK
                                                                        </div>
                                                                    </DropdownItem>
                                                                    <DropdownItem>
                                                                        <div onClick={(e) => this.countryChangeValue(e)}>SV
                                                                        </div>
                                                                    </DropdownItem>
                                                                </DropdownMenu>
                                                            </Dropdown>
                                                        </Col>
                                                        <Col md={6}>
                                                            <label>Select Role</label>
                                                            <Dropdown isOpen={this.state.roleDropdownOpen}
                                                                      toggle={() => this.roleToggle()}>
                                                                <DropdownToggle caret>
                                                                    {this.state.role === '' ? "Select Role" : this.state.role}
                                                                    <span className={"dropdown-icon"}/>
                                                                </DropdownToggle>
                                                                <DropdownMenu>
                                                                    {
                                                                        Array.isArray(this.props.roles) && this.props.roles.map(data => {
                                                                            return (
                                                                                <DropdownItem>
                                                                                    <div
                                                                                        onClick={(e) => this.roleChangeValue(e)}>{data.role}</div>
                                                                                </DropdownItem>
                                                                            )
                                                                        })
                                                                    }
                                                                </DropdownMenu>
                                                            </Dropdown>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Form>
                                    </CardBody>

                                    <CardFooter style={{textAlign: 'center'}}>
                                        <Button className="btn-fill" color="secondary"
                                                type="submit"
                                                onClick={() => this.onCancelBtnClick(this.props.showAdminForm)}
                                        >
                                            Cancel
                                        </Button>
                                        <Button className="btn-fill" color="primary"
                                                onClick={() => this.onAdminSave(this.props.showAdminForm)}
                                                type="submit">
                                            Save
                                        </Button>
                                    </CardFooter>

                                </Card>
                        }
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.admin.processing,
        roles: state.admin.roles
    }
};

const connectedComponent = connect(mapStateToProps, {getAllRoles, saveAdmin})(AdminForm);
export default withRouter(connectedComponent);

