import React, {Component} from "react";
import {
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    FormGroup,
    Form,
    Input,
    Col,
    Row,
    Badge
} from "reactstrap";
import Select from "react-select";
import MultiTabComponent from "../../components/MultiTabComponent";
import {connect} from "react-redux";
import {getAllItemsOfStoresBySearch} from "../../store/store/store-actions";
import {withRouter} from "react-router-dom";
import {NotificationManager} from "react-notifications";
import SubCategoryField from "../CategoryForm/SubCategoryField";
import DatePicker from "react-datepicker";
import {parseIntDate} from "../../utils/common-utils";
import * as actions from "../../store/admin/admin-action-types";

let timeoutRef = null;
let searchValue;
let type = "percentage";
let selectedItemUUID;
let stateValue = false;
let packageStoreItems;

class GetModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuIsOpen: false,
            selectedOption: null,
            revolutAccID: "",
            personalNumber: "",
            cellNumber: "",
            heading: "",
            subHeading: "",
            image: "",
            startDate: "",
            endDate: "",
            amount: "",
            filteredList: [],
            searchValue: "",
            selectedItemUUID: '',
        };
    }

    componentDidMount() {

        let editItemList = [];
        let itemDataForPackage = this.props && this.props.content && this.props.content.itemDataForPackage;
        packageStoreItems = itemDataForPackage && itemDataForPackage.packageStoreItems;
        if(packageStoreItems){
            let length = packageStoreItems ? packageStoreItems.length : 0;
            for (let i = 0; i < length; i++) {
                editItemList.push({
                    name: packageStoreItems[i].storeItem && packageStoreItems[i].storeItem.item && packageStoreItems[i].storeItem.item.name,
                    uuid: packageStoreItems[i].storeItem ? packageStoreItems[i].storeItem.uuid : ''
                });
            }
            this.setState({filteredList: editItemList});
        }
        if (this.props.content && this.props.content.storePackageData) {
            let data = this.props.content.storePackageData;
            let {heading, subHeading, image} = data;
            this.setState({heading, subHeading, image});
        }
        if (this.props.content && this.props.content.storeDealData) {
            let data = this.props.content.storeDealData;
            let {startDate, endDate, amount} = data;
            this.setState({startDate, endDate, amount});
        }
    }

    componentWillUpdate(nextProps) {
        if (nextProps.content) {
            selectedItemUUID = nextProps.content.itemId
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({searchValue: "", allItemsBySearch: nextProps.getAllItemsOfStores});
    }

    openMenu = () => {
        this.setState({menuIsOpen: !this.state.menuIsOpen});
    };

    handleChange = selectedOption => {
        if (selectedOption) {
            this.setState({
                selectedOption: selectedOption,
                menuIsOpen: !this.state.menuIsOpen
            });
        }
    };

    onInputChange = (e) => {
        let state = {...this.state};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    prepareGigapayFields = () => {
        const inputFields = [];
        inputFields.push({
            name: "personalNumber",
            placeholder: "Personal Number",
            label: "Enter Personal Number:"
        });
        inputFields.push({
            name: "cellNumber",
            placeholder: "Cell Number",
            label: "Enter Cell Number:"
        });
        return inputFields;
    };

    prepareStorePackageFields = () => {
        const inputFields = [];
        inputFields.push({
            name: "heading",
            placeholder: "Heading",
            label: "Heading",
            value: this.state.heading
        });
        inputFields.push({
            name: "subHeading",
            placeholder: "Sub Heading",
            label: "Sub Heading",
            value: this.state.subHeading
        });
        inputFields.push({
            name: "image",
            placeholder: "Url",
            label: "Image Url",
            value: this.state.image
        });
        return inputFields;
    };

    prepareStoreDealFields = () => {
        const inputFields = [];
        inputFields.push({
            name: "startDate",
            placeholder: "2019-24-12",
            label: "Start Date",
            value: this.state.startDate
        });
        inputFields.push({
            name: "endDate",
            placeholder: "2019-28-12",
            label: "End Date",
            value: this.state.endDate
        });
        inputFields.push({
            name: "amount",
            placeholder: "5",
            label: "Amount / percent",
            value: this.state.amount
        });
        return inputFields;
    };

    onSelectItemClick = item => {
        this.setState({searchValue: item.name, selectedItemUUID: item.uuid, allItemsBySearch: []});
    };

    handleStartDateChange = date => {
        this.setState({
            startDate: date
        });
    };

    handleEndDateChange = date => {
        this.setState({
            endDate: date
        });
    };

    getForm = (fields, showSelect) => {

        // if(startDate){
        //     let sDate = parseIntDate(startDate);
        // }

        return (
            <Form>
                {
                    showSelect ?
                        <Row>
                            <Col className="pr-md-1" md="12">
                                <FormGroup>
                                    <label className={"text-color-gray"}>Select Item</label>
                                    <Input
                                        placeholder={"Search and Select Item"}
                                        type="text"
                                        onChange={e => this.onInputChangeBySearch(e, this.props.getAllItemsOfStoresBySearch)}
                                        value={this.state.searchValue}
                                        name={"searchValue"}
                                        style={{marginBottom: "3px"}}
                                    />
                                    <div id={"myHeader"} style={{
                                        border: "1px solid #272A3D",
                                        backgroundColor: "#272A3D",
                                        maxHeight: "150px",
                                        overflowY: "auto",
                                        overflowX: "hidden",
                                        borderRadius: '3px',
                                        textAlign: 'center'
                                    }}>
                                        {this.state.allItemsBySearch && this.state.allItemsBySearch.length ? (
                                            <ul style={{
                                                cursor: "pointer",
                                                listStyleType: "none",
                                                marginLeft: "-41px"
                                            }}>
                                                {this.state.allItemsBySearch.map((item) => {
                                                        return (
                                                            <li style={{
                                                                backgroundColor: "white",
                                                                margin: "-1px",
                                                                borderRadius: "0.4285rem"
                                                            }}
                                                                key={item.name}
                                                                onClick={() => {
                                                                    this.onSelectItemClick(item)
                                                                }}>
                                                                <p style={{color: "black", marginLeft: "9px"}}>
                                                                    {item.name}
                                                                </p>
                                                            </li>
                                                        );
                                                    }
                                                )}
                                            </ul>
                                        ) : null}
                                        {
                                            (this.state.allItemsBySearch && this.state.allItemsBySearch.length === 0) && (this.state.searchValue === '') ?
                                                <p id={"myP"} style={{fontFamily: "Poppins"}}>
                                                    No suggestions!
                                                </p>
                                                : null
                                        }
                                    </div>
                                </FormGroup>
                            </Col>
                        </Row> : null
                }
                {/*<Row style={{marginTop: "1%"}}>*/}
                {/*<Col className="pr-md-1" md={6}>*/}
                {/*<FormGroup>*/}
                {/*<label className={"text-color-gray"}>Start Date</label>*/}
                {/*<DatePicker*/}
                {/*selected={this.state.startDate ? this.state.startDate : startDate}*/}
                {/*onChange={this.handleStartDateChange}*/}
                {/*/>*/}
                {/*</FormGroup>*/}
                {/*</Col>*/}
                {/*<Col className="pr-md-1" md={6}>*/}
                {/*<FormGroup>*/}
                {/*<label className={"text-color-gray"}>End Date</label>*/}
                {/*<DatePicker*/}
                {/*selected={this.state.endDate ? this.state.endDate : endDate}*/}
                {/*onChange={this.handleEndDateChange}*/}
                {/*/>*/}
                {/*</FormGroup>*/}
                {/*</Col>*/}
                {/*</Row>*/}
                <Row>
                    {fields && fields.map(field => {
                        return (
                            <Col
                                className="pr-md-1"
                                md={field.name === "image" ? "12" : "6"}
                            >
                                <FormGroup>
                                    <label className={"text-color-gray"}>{field.label}</label>
                                    <Input
                                        style={{backgroundColor: "#27293d"}}
                                        onChange={e => this.onInputChange(e)}
                                        name={field.name}
                                        value={field.value}
                                        placeholder={field.placeholder}
                                        type="text"
                                    />
                                </FormGroup>
                            </Col>
                        );
                    })}
                </Row>
            </Form>
        );
    };

    onInputChangeBySearch = (e, searchApi) => {
        let text = e.target.value;
        let state = {...this.state};
        state[e.target.name] = text;
        this.setState(state);
        searchValue = text;
        if (timeoutRef) {
            clearTimeout(timeoutRef);
        }
        if (text.length > 2) {
            timeoutRef = setTimeout(() => {
                searchApi(text);
            }, 1000);
        }
    };

    onKeyPress = (e) => {
        if(e.which === 13){
            e.preventDefault();
        }
    };

    onClickItemClose = (name) => {
        let deleted_list = this.state.filteredList.filter(item => item.name !== name);
        this.setState({filteredList: deleted_list});
    };

    getFilteredListOnUpdate = filteredList => {
        return (
            <SubCategoryField
                list={filteredList}
                onItemClose={this.onClickItemClose}
                label={"Items"}
            />
        );
    };

    isItemExists = suggestion => {
        let found = false;
        for (let i = 0; i < this.state.filteredList.length; i++) {
            if (
                this.state.filteredList &&
                this.state.filteredList[i] &&
                this.state.filteredList[i].name === suggestion.name
            ) {
                found = true;
                NotificationManager.error("You have already selected this item!");
                break;
            }
        }

        return found;
    };

    onClickItem = suggestion => {
        this.props.getAllItemsOfStores.length = 0;
        let _filteredList = [...this.state.filteredList];
        if (!this.isItemExists(suggestion)) {
            _filteredList.push({
                name: suggestion.name,
                uuid: suggestion.stores[0].storeItem.uuid
            });
            this.setState(
                {
                    searchValue: suggestion.name,
                    filteredList: _filteredList
                },
                () => {
                    console.log("its final state", this.state);
                }
            );
        }
    };

    getAddItemMenu = () => {
        return (
            <div>
                <Form>
                    <Row>
                        {
                            <Col className="pr-md-1" md="12">
                                <FormGroup>
                                    {this.getFilteredListOnUpdate(this.state.filteredList)}
                                </FormGroup>
                            </Col>
                        }
                    </Row>
                    <Row>
                        <Col className="pr-md-1" md="12">
                            <FormGroup>
                                <label className="text-color-gray">{"Add Item"}</label>
                                <Input
                                    placeholder={"Search and Add item..."}
                                    type="text"
                                    onChange={e => this.onInputChangeBySearch(e, this.props.getAllItemsOfStoresBySearch)}
                                    onKeyPress={e => this.onKeyPress(e)}
                                    value={this.state.searchValue}
                                    name={"searchValue"}
                                    style={{marginBottom: "3px"}}
                                />
                                <div
                                    id={"myHeader"}
                                    style={{
                                        backgroundColor: "rgb(30, 30, 46)",
                                        minHeight: "120px",
                                        height: "120px",
                                        overflowY: "auto",
                                        overflowX: "hidden"
                                    }}
                                >
                                    {this.props.getAllItemsOfStores &&
                                    this.props.getAllItemsOfStores.length ? (
                                        <ul
                                            style={{
                                                cursor: "pointer",
                                                listStyleType: "none",
                                                marginLeft: "-41px"
                                            }}
                                        >
                                            {this.props.getAllItemsOfStores.map(
                                                (suggestion, index) => {
                                                    return (
                                                        <li
                                                            style={{
                                                                backgroundColor: "white",
                                                                margin: "-1px",
                                                                borderRadius: "0.4285rem"
                                                            }}
                                                            key={suggestion.name}
                                                            onClick={() => {this.onClickItem(suggestion);}}
                                                        >
                                                            <p
                                                                style={{
                                                                    color: "black",
                                                                    marginLeft: "9px"
                                                                }}
                                                            >
                                                                {suggestion.name}
                                                            </p>
                                                        </li>
                                                    );
                                                }
                                            )}
                                        </ul>
                                    ) : (
                                        <div>
                                            <p id={"myP"} style={{fontFamily: "Poppins"}}>
                                                No suggestions!
                                            </p>
                                        </div>
                                    )}
                                </div>
                            </FormGroup>
                        </Col>
                    </Row>
                </Form>
            </div>
        );
    };

    storePackageButtonClick = (buttonClick, heading, subHeading, image) => {
        buttonClick(heading, subHeading, image);
        stateValue = false;
    };

    storeDealButtonClick = (buttonClick, selectedItemUUID, startDate, endDate, amount, type) => {
        buttonClick(selectedItemUUID, startDate, endDate, amount, type);
        stateValue = false;
    };

    getGigapayInfoChange = gigapayFields => {
        return (
            <Form>
                <Row>
                    {gigapayFields.map(field => {
                        return (
                            <Col className="pr-md-1" md="6">
                                <FormGroup>
                                    <label>{field.label}</label>
                                    <Input
                                        style={{backgroundColor: "#27293d"}}
                                        onChange={e => this.onInputChange(e)}
                                        name={field.name}
                                        placeholder={field.placeholder}
                                        type="text"
                                    />
                                </FormGroup>
                            </Col>
                        );
                    })}
                </Row>
            </Form>
        );
    };

    getRevolutInfoChange = () => {
        return (
            <Form>
                <Row>
                    <Col className="pr-md-1" md="12">
                        <FormGroup>
                            <Input
                                style={{backgroundColor: "#27293d"}}
                                onChange={e => this.onInputChange(e)}
                                name={"revolutAccID"}
                                placeholder={"Account ID"}
                                type="text"
                            />
                        </FormGroup>
                    </Col>
                </Row>
            </Form>
        );
    };

    toggleClick = toggle => {
        if (toggle) {
            toggle();
        }
        this.setState({menuIsOpen: false});
    };

    onDivClick = () => {
        this.setState({menuIsOpen: !this.state.menuIsOpen});
    };

    addItemBtnClick = (onClick, list, value, storeUUID) => {
        onClick(list, value);
        this.props.history.push('/admin/store/package/' + storeUUID);
    };

    render() {
        const {
            isOpen,
            toggle,
            content,
            headerStyle,
            modalBodyStyle,
            modalFooterStyle,
            itemDataForPackage,
            bolClear
        } = this.props;

        const gigapayFields = this.prepareGigapayFields();
        const storePackageFields = this.prepareStorePackageFields();
        const storeDealFields = this.prepareStoreDealFields();
        return (
            <Modal
                isOpen={isOpen}
                toggle={() => this.toggleClick(toggle)}
                className={"modal-danger " + this.props.className}
            >
                <ModalHeader
                    toggle={() => this.toggleClick(toggle)}
                    style={headerStyle ? headerStyle : null}
                >
                    {content && content.headingTextStyle !== true ? (
                        content.headerText
                    ) : content && content.headingTextStyle ? (
                        <h4 style={{color: "white"}}>{content.headerText}</h4>
                    ) : null}
                </ModalHeader>

                <ModalBody style={modalBodyStyle ? modalBodyStyle : null}>
                    {content ? content.bodyText : "Body Text Here"}

                    {content && content.dropdown ? (
                        <div
                            onClick={this.state.selectedOption ? () => this.onDivClick() : null}>
                            <Select
                                options={content && content.contentArray}
                                onFocus={this.openMenu}
                                onChange={this.handleChange}
                                menuIsOpen={this.state.menuIsOpen}
                            />
                        </div>
                    ) : null}

                    {content && content.multiTabs ? (
                        <MultiTabComponent
                            data={content && content}
                            toggle={() => this.toggleClick(toggle)}
                        />
                    ) : null}

                    {content && content.revolutInfoChange ? this.getRevolutInfoChange() : null}

                    {content && content.gigapayInfoChange ? this.getGigapayInfoChange(gigapayFields) : null}

                    {content && content.storePackage ? this.getForm(storePackageFields) : null}

                    {content && content.addItem ? this.getAddItemMenu() : null}

                    {content && content.storeDeals ? this.getForm(storeDealFields, !content.showItemSelect) : null}

                </ModalBody>
                {content && content.footerShow === false ? null : (
                    <ModalFooter>
                        <Button
                            color="secondary"
                            onClick={() => content.buttonClick(this.state.selectedOption)}
                        >
                            {content ? content.footerBtnText : "Button"}
                        </Button>
                    </ModalFooter>
                )}

                {content && content.revolutInfoChange ? (
                    <ModalFooter>
                        <Button
                            color="secondary"
                            onClick={() => content.buttonClick(this.state.revolutAccID)}
                        >
                            {content ? content.footerBtnText : "Button"}
                        </Button>
                    </ModalFooter>
                ) : null}

                {content && content.gigapayInfoChange ? (
                    <ModalFooter>
                        <Button
                            color="secondary"
                            onClick={() => content.buttonClick(this.state.personalNumber, this.state.cellNumber)}
                        >
                            {content ? content.footerBtnText : "Button"}
                        </Button>
                    </ModalFooter>
                ) : null}

                {content && content.storePackage ? (
                    <ModalFooter style={modalFooterStyle ? modalFooterStyle : null}>
                        <Button
                            color="secondary"
                            onClick={() =>
                                this.storePackageButtonClick(
                                    content.buttonClick,
                                    this.state.heading,
                                    this.state.subHeading,
                                    this.state.image
                                )
                            }
                        >
                            {content ? content.footerBtnText : "Button"}
                        </Button>
                    </ModalFooter>
                ) : null}

                {content && content.deleteModalBody ? (
                    <ModalFooter style={modalFooterStyle ? modalFooterStyle : null}>
                        <Button
                            color="secondary"
                            onClick={() =>
                                content.buttonClick(content.storePackageData, content.index)
                            }
                        >
                            {content ? content.footerBtnText : "Button"}
                        </Button>
                    </ModalFooter>
                ) : null}

                {content && content.addItem ? (
                    <ModalFooter style={modalFooterStyle ? modalFooterStyle : null}>
                        <Button
                            color="secondary"
                            onClick={() => this.addItemBtnClick(content.buttonClick, this.state.filteredList,
                                packageStoreItems && packageStoreItems.length > 0 ? "true" : "false",
                                content && content.storeUUID
                            )}
                        >
                            {content ? content.footerBtnText : "Button"}
                        </Button>
                    </ModalFooter>
                ) : null}

                {content && content.storeDeals ? (
                    <ModalFooter style={modalFooterStyle ? modalFooterStyle : null}>
                        <Button
                            color="secondary"
                            onClick={() =>
                                this.storeDealButtonClick(
                                    content.buttonClick,
                                    this.state.selectedItemUUID ? this.state.selectedItemUUID : selectedItemUUID,
                                    this.state.startDate,
                                    this.state.endDate,
                                    this.state.amount,
                                    type
                                )
                            }
                        >
                            {content ? content.footerBtnText : "Button"}
                        </Button>
                    </ModalFooter>
                ) : null}

                {content && content.deleteDealModal ? (
                    <ModalFooter style={modalFooterStyle ? modalFooterStyle : null}>
                        <Button
                            color="secondary"
                            onClick={() => content.buttonClick(content.storeDealData, content.index, content.itemIndex)}
                        >
                            {content ? content.footerBtnText : "Button"}
                        </Button>
                    </ModalFooter>
                ) : null}

            </Modal>
        );
    }
}

const mapStateToProps = state => {
    return {
        processing: state.storeReducer.processing,
        getAllItemsOfStores: state.storeReducer.allItemsBySearch
    };
};

const connectedComponent = connect(mapStateToProps, {getAllItemsOfStoresBySearch})(GetModal);
export default withRouter(connectedComponent);
