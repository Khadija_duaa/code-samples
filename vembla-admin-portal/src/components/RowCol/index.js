import React from 'react';
import { Row, Col} from 'reactstrap';

const rowCol = (props) => {
    return(
        <Row style={props.style}>
            <Col md={5} className={"small-font text-color-gray"}>
                {props.title}
            </Col>
            <Col md={7} className={"small-font text-color-gray"}>
                {props.value}
            </Col>
        </Row>
    )
};

export default rowCol;