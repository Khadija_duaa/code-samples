import React, {useState} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Row, Col, Form, Label, Button} from 'reactstrap';

import {saveGigaPayInfo} from '../../store/shopper/shopper-actions';

import CustomInputFormGroup from '../CustomComponents/CustomInputFormGroup';

const GigaPayForm = ({uuidFleet, saveGigaPayInfo}) => {
    const [personalNumber, setPersonalNumber] = useState('');
    const [cellNumber, setCellNumber] = useState('');

    const onChange = (name, val) => {
        switch (name) {
            case 'personalNum':
                setPersonalNumber(val);
                break;
            case 'cellNum':
                setCellNumber(val);
                break;
            default:
                break;
        }
    };

    const onSave = () => {
        if (personalNumber && cellNumber) {
            saveGigaPayInfo(uuidFleet, {
                personalNumber,
                cellNumber
            });
        }
    };

    return (
        <Form>
            <Row>
                <Col md="6">
                    <CustomInputFormGroup
                        type="text"
                        label="Personal Number"
                        name="personalNum"
                        onChange={onChange}
                    />
                </Col>
                <Col md="6">
                    <CustomInputFormGroup
                        type="text"
                        label="Cell Number"
                        name="cellNum"
                        onChange={onChange}
                    />
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <Button
                        disabled={!personalNumber || !cellNumber}
                        onClick={onSave}
                    >
                        Approve
                    </Button>
                </Col>
            </Row>
        </Form>
    );
};

GigaPayForm.propTypes = {
    uuidFleet: PropTypes.string.isRequired
};

export default connect(null, {saveGigaPayInfo})(GigaPayForm);
