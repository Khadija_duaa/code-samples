import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Row, Col, FormGroup, Button, Alert } from 'reactstrap';

import { createCommission } from '../../store/shopper/shopper-actions';

import CustomInputFormGroup from '../CustomComponents/CustomInputFormGroup';

/*
 *   Validating all field values
 * Capturing default starting date.
 *
 */

const ShopperCommissionForm = props => {
  const [description, setDescription] = useState();
  const [commission, setCommission] = useState();
  const [errMsg, setErrMsg] = useState();

  const onChange = (name, val) => {
    switch (name) {
      case 'description':
        setDescription(val);
        break;
      case 'commission':
        setCommission(val);
        break;
      default:
        break;
    }
  };

  const isReq = () => (description && commission > -1 ? true : false);

  const payCommission = () => {
    console.log();
    const { uuidFleet } = props;
    //   validations remaining
    if (!isReq()) {
      setErrMsg('All Fields Are Required');
      return;
    }
    const commissionInfo = {
      uuidFleet,
      type: 'commission',
      description,
      commission
    };
    props.createCommission(commissionInfo);
  };

  return (
    <div>
      {errMsg && <Alert color="danger">{errMsg}</Alert>}
      <Row>
        <Col md="12">
          <CustomInputFormGroup
            label="Description:"
            type="text"
            name="description"
            onChange={onChange}
          />
        </Col>
      </Row>
      <Row>
        <Col md="12">
          <CustomInputFormGroup
            label="Commsission:"
            type="number"
            name="commission"
            onChange={onChange}
          />
        </Col>
      </Row>
      <Row>
        <Col md={{ size: 4, offset: 4 }}>
          <FormGroup>
            <Button onClick={() => payCommission()}>Save</Button>
          </FormGroup>
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => {
  return {
    createCommission: obj => dispatch(createCommission(obj))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShopperCommissionForm);
