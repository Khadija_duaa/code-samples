import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  Alert,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Button,
  Form
} from "reactstrap";

import {
  getAllPrograms,
  getVehicleTypes,
  saveShopperVehicleNProgramInfo as updateData
} from "../../store/shopper/shopper-actions";

import CustomInputFormGroup from "../CustomComponents/CustomInputFormGroup";
import CustomDropDownFormGroup from "../CustomComponents/CustomDropDownFormGroup";
import CustomCheckboxFormGroup from "../CustomComponents/CustomCheckboxFormGroup";

const VehicleNProgramForm = props => {
  const { titles, headers, vehicleTypes, allPrograms, shopperData } = props;

  const [showErr, setShowErr] = useState(false);
  const [program, setProgram] = useState(null);
  const [vehicleName, setVehicleName] = useState("");
  const [vehicleColor, setVehicleColor] = useState("");
  const [vehicleReg, setVehicleReg] = useState("");
  const [vehicleTypeId, setVehicleTypeId] = useState("");
  const [vehicleHasCoolingBox, setVehicleHasCoolingBox] = useState(false);
  const [vehicleHasHeatingBox, setVehicleHasHeatingBox] = useState(false);

  useEffect(() => {
    props.getVehicleTypes();
    props.getAllPrograms();
  }, []);

  const onChange = (name, val) => {
    // switch-case and update state
    switch (name) {
      case "vehicleName":
        setVehicleName(val);
        break;
      case "vehicleColor":
        setVehicleColor(val);
        break;
      case "vehicleReg":
        setVehicleReg(val);
        break;
      case "vehicleType":
        setVehicleTypeId(val);
        break;
      case "vehicleHasCoolingBox":
        setVehicleHasCoolingBox(val);
        break;
      case "vehicleHasHeatingBox":
        setVehicleHasHeatingBox(val);
        break;
      case "package":
        const selectedProg = allPrograms.filter(prog => prog.uuid === val);
        setProgram({
          active: selectedProg[0].active,
          uuid: selectedProg[0].uuid,
          name: selectedProg[0].name
        });
        break;
      default:
        break;
    }
  };

  const isReqFields = () =>
    vehicleName &&
    vehicleColor &&
    vehicleReg &&
    vehicleTypeId &&
    program &&
    shopperData;

  const onSubmit = () => {
    // all true - Fields not Empty
    if (isReqFields()) {
      const vehicleData = {
        userId: shopperData.uuid,
        name: vehicleName,
        color: vehicleColor,
        registrationNumber: vehicleReg,
        vehicleTypeId: vehicleTypeId,
        hasCoolingBox: vehicleHasCoolingBox,
        hasHeatingBox: vehicleHasHeatingBox
      };
      props.updateData(vehicleData, program, shopperData);
    } else {
      setShowErr(true);
    }
  };

  return (
    <Card>
      <Col md="12">
        <CardHeader>
          <Col md="12">
            <h4 className="title">{titles.titleShopperVehicleNPackage}</h4>
            {showErr && <Alert color="danger">All Fields are Required!</Alert>}
          </Col>
        </CardHeader>
        <CardBody>
          <Col md="12">
            <Form>
              <Row>
                <Col className="pr-md-1" md="6">
                  <CustomInputFormGroup
                    type="text"
                    label={headers.vehicleName}
                    name="vehicleName"
                    val={vehicleName}
                    onChange={onChange}
                  />
                </Col>
                <Col className="pr-md-1" md="6">
                  <CustomInputFormGroup
                    type="text"
                    label={headers.color}
                    name="vehicleColor"
                    val={vehicleColor}
                    onChange={onChange}
                  />
                </Col>
              </Row>
              <Row>
                <Col className="pr-md-1" md="6">
                  <CustomInputFormGroup
                    type="text"
                    label={headers.regNumber}
                    name="vehicleReg"
                    val={vehicleReg}
                    onChange={onChange}
                  />
                </Col>
                <Col className="pr-md-1" md="6">
                  <CustomDropDownFormGroup
                    label={headers.selectVehicleType}
                    name="vehicleType"
                    dataField="uuid"
                    dispField="name"
                    btnText={titles.titleVehicles}
                    data={vehicleTypes}
                    onChange={onChange}
                  />
                </Col>
              </Row>
              <Row>
                <Col md="12">
                  <h5>{titles.titleOptions}</h5>
                  <Row>
                    <Col md={{ size: 10, offset: 1 }}>
                      <CustomCheckboxFormGroup
                        label={headers.coolingBoxText}
                        name="vehicleHasCoolingBox"
                        value={vehicleHasCoolingBox}
                        onChange={onChange}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col md={{ size: 10, offset: 1 }}>
                      <CustomCheckboxFormGroup
                        label={headers.heatingBoxText}
                        name="vehicleHasHeatingBox"
                        value={vehicleHasHeatingBox}
                        onChange={onChange}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
              <hr style={{ color: "white" }} />
              <Row>
                <Col md="12">
                  <h5>{titles.titleProgramInfo}</h5>
                  <Row>
                    <Col md={{ size: 10, offset: 1 }}>
                      <CustomDropDownFormGroup
                        label={headers.selectProgram}
                        name="package"
                        dataField="uuid"
                        dispField="name"
                        btnText={titles.titleProgram}
                        data={allPrograms}
                        onChange={onChange}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Form>
          </Col>
        </CardBody>
        <CardFooter style={{ textAlign: "center" }}>
          <Col md="12">
            <Button
              className="btn-fill"
              color="primary"
              type="submit"
              onClick={onSubmit}
            >
              {titles.btnTextNext}
            </Button>
          </Col>
        </CardFooter>
      </Col>
    </Card>
  );
};

const mapStateToProps = state => ({
  titles: {
    titleShopperVehicleNPackage: "Vehicle and Package Information",
    titleVehicles: "Vehicles",
    titleProgram: "Programs",
    titleOptions: "Options",
    titleVehicleInfo: "Vehicle Information",
    titleProgramInfo: "Program Information",
    btnTextNext: "Next"
  },
  headers: {
    vehicleName: "Vehicle Name",
    color: "Color",
    regNumber: "Registration Number",
    selectVehicleType: "Select Vehicle Type",
    selectProgram: "Select Program",
    coolingBoxText: "Cooling Box Available",
    heatingBoxText: "Heating Box Available",
    available: "Available"
  },

  vehicleTypes: state.shopper.vehicleTypes,
  allPrograms: state.shopper.allPrograms
});

const mapDispatchToProps = dispatch => {
  return {
    updateData: (vehicleData, programInfo, userObj, cb) => dispatch(updateData(vehicleData, programInfo, userObj, cb)),
    getAllPrograms: () => dispatch(getAllPrograms()),
    getVehicleTypes: () => dispatch(getVehicleTypes())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VehicleNProgramForm);
