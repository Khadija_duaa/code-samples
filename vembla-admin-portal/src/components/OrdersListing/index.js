import React, {Component} from "react";
import {connect} from "react-redux";
import ReactPaginate from "react-paginate";
import {
    Col,
    Row,
    Table,
    Card,
    CardHeader,
    CardBody,
    CardTitle
} from "reactstrap";

import {capitalize} from "../../utils/common-utils";

import ItemsListing from "../ItemsListing";
import OrderMetaInfo from "../OrderMetaInfo";

class OrdersListing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showOrderDetails: false,
            order: {},
            items: [],
            currentOrderNo: ""
        };
    }

    toggleOrderDetails = (order = {}, items = [], orderNo = "") => {
        const {showOrderDetails} = this.state;
        this.setState({
            showOrderDetails: !showOrderDetails,
            order: order,
            items: items,
            currentOrderNo: orderNo
        });
    };

    getOrdersTable = () => {
        const {ordersList, headers, orderDetail} = this.props;

        return (
            <Table className="tablesorter" responsive>
                <thead className="text-primary">
                <tr>
                    {headers && headers.map((headerTitle, i) => <th key={i}>{headerTitle}</th>)}
                </tr>
                </thead>
                <tbody>
                {ordersList && ordersList.map((data, index) => (
                    <tr key={index}
                        onClick={orderDetail ? () => this.toggleOrderDetails(data, data.store && data.store.items, data.orderNo) : null}
                        style={orderDetail ? {cursor: "pointer"} : null}>
                        <td>{data.orderNo}</td>
                        <td>{capitalize(data.firstName) + " " + capitalize(data.lastName)}</td>
                        <td>{data.total}</td>
                        <td>{data.phone}</td>
                        <td>{data.store && data.store.name}</td>
                        <td>{data.deliveryAddress}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
        );
    };

    getNoOrdersMsg = () => {
        const {noOrdersFoundMsg} = this.props;

        return (
            <Row>
                <Col md={{size: '6', offset: '5'}}>{noOrdersFoundMsg}</Col>
            </Row>
        );
    };

    getOrderDetails = () => {
        const {
            titleOrderDetails,
            itemIcon,
            itemName,
            itemQuantity,
            itemPrice,
            itemTotalPrice,
            isDeliveredOrder
        } = this.props;
        const {order, items, currentOrderNo} = this.state;

        const itemHeaders = [
            itemIcon,
            itemName,
            itemQuantity,
            itemPrice,
            itemTotalPrice
        ];

        const deliveryStatus = order.status.filter(
            obj => obj.status === "Delivered"
        );

        return (
            <div>
                <Card>
                    <CardHeader>
                        <CardTitle
                            className="h5"
                            style={{cursor: "pointer"}}
                            onClick={() => this.toggleOrderDetails()}
                        >
                            <i className="tim-icons icon-minimal-left" style={{color: "white", padding: "9px 12px 11px 0px"}}/>
                            Back
                        </CardTitle>
                    </CardHeader>
                    <CardBody>
                        <OrderMetaInfo
                            orderPlacedAt={order.placedAt}
                            consumerF_Name={order.firstName}
                            consumerL_Name={order.lastName}
                            deliveryFee={order.recalculatedDeliveryFee}
                            orderAmount={order.recalculatedOrderAmount}
                            deliveryAddress={order.deliveryAddress}
                            deliveryTime={deliveryStatus[0].time}
                            store={order.store}
                        />
                        <ItemsListing
                            onToggleOrderDetails={this.toggleOrderDetails}
                            headers={itemHeaders}
                            title={titleOrderDetails}
                            data={items}
                            orderNo={currentOrderNo}
                            isDeliveredOrder={isDeliveredOrder}
                        />
                    </CardBody>
                </Card>
            </div>
        );
    };

    render() {
        const {
            ordersList,
            title,
            totalPages,
            currentPage,
            getMoreOrders
        } = this.props;
        const {showOrderDetails} = this.state;

        // short circuit for overlaying order details
        if (showOrderDetails) {
            return this.getOrderDetails();
        }

        // Orders Listed
        return (
            <Row>
                <Col md="12">
                    <Card>
                        <CardHeader>
                            <CardTitle tag="h4">{title ? title : ""}</CardTitle>
                        </CardHeader>
                        <CardBody>
                            {ordersList && ordersList.length === 0
                                ? this.getNoOrdersMsg()
                                : this.getOrdersTable()}
                        </CardBody>
                    </Card>
                    {totalPages > 1 ? (
                        <ReactPaginate
                            pageCount={totalPages}
                            pageRangeDisplayed={5}
                            marginPagesDisplayed={2}
                            onPageChange={e => getMoreOrders(e)}
                            forcePage={currentPage}
                            previousLabel={<i className="fa fa-angle-left"/>}
                            nextLabel={<i className="fa fa-angle-right"/>}
                            breakLabel={"..."}
                            breakClassName={"break-me"}
                            containerClassName={"list-inline mx-auto justify-content-center pagination"}
                            subContainerClassName={"list-inline-item pages pagination"}
                            activeClassName={"active"}
                        />
                    ) : null}
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    titleOrderDetails: "Order Details",
    itemIcon: "ICON",
    itemName: state.multilingual.activeLanguageData.name,
    itemQuantity: "Quantity",
    itemPrice: state.multilingual.activeLanguageData.price,
    itemTotalPrice: "Total Price"
});

export default connect(
    mapStateToProps,
    null
)(OrdersListing);
