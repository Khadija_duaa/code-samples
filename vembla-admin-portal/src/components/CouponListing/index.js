import React, { useState } from "react";
import { connect } from "react-redux";
import { Card, CardHeader, CardBody, Table } from "reactstrap";

import { deleteCoupon } from "../../store/coupon/coupon-actions";

import CustomConfirmationModal from "../CustomComponents/CustomConfirmationModal";

const statusBulb = active => (
  <span style={{ color: `${active ? "green" : "red"}` }}>&#11044;</span>
);

const okSignDisp = ok => {
  if (ok) {
    return <i style={{ color: "green" }} className="tim-icons icon-check-2" />;
  }
  return <i className="tim-icons icon-simple-remove" />;
};

const CouponListing = props => {
  const [showModal, setShowModal] = useState(false);
  const [deleteCouponId, setDeleteCouponId] = useState();

  const { coupons, titles, messages } = props;
  const deleteCoupon = uuid => {
    props.deleteCoupon(uuid);
    setShowModal(false);
  };

  const headers = [
    titles.status,
    titles.code,
    titles.name,
    titles.startDate,
    titles.endDate,
    titles.discountType,
    titles.amount,
    titles.total,
    titles.used,
    titles.deliveryFee,
    titles.onDiscounts,
    titles.action
  ];

  return (
    <Card>
      {showModal && (
        <CustomConfirmationModal
          showModal={showModal}
          message={messages.deleteCoupon}
          onYes={() => deleteCoupon(deleteCouponId)}
          onCancel={() => {
            setDeleteCouponId("");
            setShowModal(false);
          }}
        />
      )}
      <CardHeader>{titles.coupons}</CardHeader>
      <CardBody>
        <Table>
          <thead>
            <tr>
              {headers.map((heading, i) => (
                <th key={i}>{heading}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {coupons.map(coupon => (
              <tr key={coupon.uuid}>
                <td>{statusBulb(coupon.active)}</td>
                <td style={{ fontWeight: "bold" }}>{coupon.discountCode}</td>
                <td>{coupon.name}</td>
                <td>{coupon.startDate}</td>
                <td>{coupon.endDate}</td>
                <td>{coupon.discountType}</td>
                <td>{coupon.amount}</td>
                <td>{coupon.usageCountLimit}</td>
                <td>{coupon.currentUsageCount}</td>
                <td>{okSignDisp(coupon.deliveryFeeExemption)}</td>
                <td>{okSignDisp(coupon.affectsDiscountedItems)}</td>
                <td>
                  <div
                    style={{
                      cursor: "pointer",
                      fontSize: "1.25rem",
                      padding: "5px"
                    }}
                    onClick={() => {
                      setDeleteCouponId(coupon.uuid);
                      setShowModal(true);
                    }}
                  >
                    <i className="tim-icons icon-trash-simple" />
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </CardBody>
    </Card>
  );
};

const mapStateToProps = state => ({
  coupons: state.coupon.coupons,
  titles: {
    deliveryFee: "Delivery Fee",
    // firstOrderOnly: "First Order",
    onDiscounts: "On Discounts",
    coupons: "Coupons",
    status: "Status",
    name: "Name",
    code: "Code",
    discountType: "Discount Type",
    startDate: "Start Date",
    endDate: "End Date",
    amount: "Amount",
    total: "Total",
    used: "Used",
    action: "Action"
  },
  messages: {
    deleteCoupon: "Are You Sure You Want to Delete This Coupon?"
  }
});

const mapDispatchToProps = dispatch => {
  return {
    deleteCoupon: uuid => dispatch(deleteCoupon(uuid))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CouponListing);
