import React, {Component} from 'react';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    Col,
    Row,
    Table,
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
} from "reactstrap";
import {capitalize, getTdWidth, getDateFromPST} from "../../utils/common-utils";
import Loader from '../../components/Loader/index';
import GetModal from "../../components/Modal";
import {
    saveStoreDeal,
    deleteStoreDeal,
    getStoreByUUID,
    attachItemsWithPackage,
    getStoreDeals
} from '../../store/store/store-actions';
import AutoSuggest from "../Autosuggest/AutoSuggest";

const headers = ["Category Name", "Items Detail"];

class StoreDeals extends Component {

    state = {
        modalVisible: false,
        modalContent: {},
        itemDataForPackage: null,
        bolClear: false,
    };

    openModal = () => {
        this.setState({modalVisible: !this.state.modalVisible});
    };


    componentDidMount() {
        const uuid = this.props.match.params.uuid;
        this.props.getStoreByUUID(uuid, (err) => {
            if (!err) {
                this.props.getStoreDeals(uuid, this.props.storeName);
            }
        })
    }

    modalBtnClick = (itemId, sDate, eDate, value, type) => {
        let startDate = getDateFromPST(sDate);
        let endDate = getDateFromPST(eDate);
        let storeId = this.props.storeUUID;
        let storeName = this.props.storeName;
        let amount = parseInt(value);
        let storeDealUUID = this.state.storeDealUUID;
        if (storeDealUUID) {
            this.props.saveStoreDeal({storeId, itemId, startDate, endDate, amount, type}, storeName, storeDealUUID);
        } else {
            this.props.saveStoreDeal({storeId, itemId, startDate, endDate, amount, type}, storeName);
        }
        this.setState({modalVisible: !this.state.modalVisible});
    };

    onDeleteDealClick = (data, index, itemIndex) => {

        let storeUUID = this.props.storeUUID;
        let storeName = this.props.storeName;
        if (data) {
            this.props.deleteStoreDeal(data.uuid, index, storeUUID, storeName, itemIndex);
        }
        this.setState({modalVisible: !this.state.modalVisible});
    };

    onCreateDealClick = () => {
        this.setState({
            modalVisible: true,
            modalContent: {
                headerText: "Create New Deal",
                storeDeals: true,
                footerShow: false,
                headingTextStyle: true,
                footerBtnText: 'Save',
                buttonClick: this.modalBtnClick,
                storeDealsData: null
            },
            modalToggle: this.openModal,
            headerStyle: {backgroundColor: '#1e1e2e'},
            modalBodyStyle: {backgroundColor: '#1e1e2e'},
            modalFooterStyle: {backgroundColor: '#1e1e2e'}
        });
    };

    onDealEditClick = (data, itemId, itemName) => {
        this.setState({
            modalVisible: true,
            modalContent: {
                headerText: "Update Deal for item : " + `${itemName}`,
                storeDeals: true,
                showItemSelect: true,
                footerShow: false,
                headingTextStyle: true,
                footerBtnText: 'Save',
                buttonClick: this.modalBtnClick,
                storeDealData: data,
                itemId: itemId
            },
            modalToggle: this.openModal,
            headerStyle: {backgroundColor: '#1e1e2e'},
            modalBodyStyle: {backgroundColor: '#1e1e2e'},
            modalFooterStyle: {backgroundColor: '#1e1e2e'},
            storeDealUUID: data.uuid
        });
    };

    onDealDeleteClick = (data, index, itemName, itemIndex) => {
        this.setState({
            modalVisible: true,
            modalContent: {
                headerText: "Are you sure?",
                bodyText: "Are you sure, you want to delete this deal on item : " + `${itemName}`,
                footerShow: false,
                deleteDealModal: true,
                headingTextStyle: true,
                footerBtnText: 'Yes',
                storeDealData: data,
                index: index,
                itemIndex: itemIndex,
                buttonClick: this.onDeleteDealClick,
            },
            modalToggle: this.openModal,
            headerStyle: {backgroundColor: '#1e1e2e'},
            modalBodyStyle: {backgroundColor: '#1e1e2e'},
            modalFooterStyle: {backgroundColor: '#1e1e2e'}
        });
    };

    render() {
        const {storeDeals} = this.props;
        return (
            <div className="content">
                <Row>
                    <Col md="12">
                        <Card>
                            {this.state.modalVisible ?
                                <GetModal isOpen={this.state.modalVisible}
                                          toggle={this.state.modalToggle}
                                          content={this.state.modalContent}
                                          modalBodyStyle={this.state.modalBodyStyle}
                                          headerStyle={this.state.headerStyle}
                                          modalFooterStyle={this.state.modalFooterStyle}
                                /> : null}
                            <CardHeader>
                                <Row>
                                    <Col md={6}>
                                        <CardTitle tag="h4">Deals for Store : {this.props.storeName}</CardTitle>
                                    </Col>
                                    <Col md={{size: '2', offset: '4'}}>
                                        <Button type="submit" size="sm" color="primary"
                                                style={{padding: "10px 19px"}}
                                                onClick={() => {
                                                    this.onCreateDealClick()
                                                }}
                                        >
                                            Create Deal
                                        </Button>
                                    </Col>
                                </Row>
                            </CardHeader>
                            {
                                this.props.processing ?
                                    <div style={{marginBottom: '200px'}}>
                                        <Loader/>
                                    </div> :
                                    <CardBody>
                                        <Table className="tablesorter" responsive>
                                            <thead className="text-primary">
                                            <tr>
                                                {
                                                    headers.map(header => {
                                                        return (
                                                            <th>{header}</th>
                                                        )
                                                    })
                                                }
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                storeDeals && storeDeals.map((data, index) => {
                                                    return (
                                                        <tr>
                                                            <td style={{width: getTdWidth(headers[0])}}>{data.name}</td>
                                                            <td style={{width: getTdWidth(headers[1])}}>
                                                                {
                                                                    data.items && data.items.map((item, i) => {
                                                                        let deal = item.storeItems && item.storeItems[0].deals[0] && item.storeItems[0].deals[0];
                                                                        return (
                                                                            <div style={{marginBottom: '2%'}}>
                                                                                <Row>
                                                                                    <Col md={3}>
                                                                                        Item Name :
                                                                                    </Col>
                                                                                    <Col md={9}>
                                                                                        {item.name}
                                                                                    </Col>
                                                                                </Row>
                                                                                <Row style={{marginTop: '1%'}}>
                                                                                    <Col md={3}>
                                                                                        Deal Applied :
                                                                                    </Col>
                                                                                    <Col md={3}>
                                                                                        {deal.amount + " " + "%"}
                                                                                    </Col>
                                                                                    <Col md={6}
                                                                                         style={{marginTop: '2px'}}>
                                                                                        <Row>
                                                                                            <Col md={3}>
                                                                                                <div
                                                                                                    style={{fontSize: '13px'}}>
                                                                                                    <i className="fas fa-edit fa-lg pointer"
                                                                                                       onClick={() => this.onDealEditClick(deal, item.uuid, item.name)}/>
                                                                                                </div>
                                                                                            </Col>
                                                                                            <Col md={3}>
                                                                                                <div
                                                                                                    style={{fontSize: '13px'}}>
                                                                                                    <i className="fas fa-trash-alt fa-lg pointer"
                                                                                                       onClick={() => this.onDealDeleteClick(deal, i, item.name, index)}/>
                                                                                                </div>
                                                                                            </Col>
                                                                                        </Row>
                                                                                    </Col>
                                                                                </Row>
                                                                            </div>
                                                                        )
                                                                    })
                                                                }
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </Table>
                                    </CardBody>
                            }
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.storeReducer.processing,
        storeDeals: state.storeReducer.storeDeals,
        storeName: state.storeReducer.storeName,
        storeUUID: state.storeReducer.storeUUID
    }
};

const connectedComponent = connect(mapStateToProps, {
    saveStoreDeal,
    deleteStoreDeal,
    getStoreByUUID,
    attachItemsWithPackage,
    getStoreDeals
})(StoreDeals);
export default withRouter(connectedComponent)

