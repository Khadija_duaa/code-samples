import React, {Component} from 'react';
import {connect} from "react-redux";
import shortId from "shortid"
import {withRouter} from "react-router-dom";
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    Col,
    Row,
    Table,
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
} from "reactstrap";
import {capitalize, getTdWidth} from "../../utils/common-utils";
import {
    setStoreData,
    enableStore,
    disableStore,
    getStoreData,
    importItemsForStores,
    getStorePackages,
    getStoreDeals,
    setAllItems
} from "../../store/store/store-actions";
import {getAllAdmins} from "../../store/admin/admin-actions";
import Loader from '../../components/Loader/index';
import {csvToJson} from "../../utils/config";

let roleVal = 'Select Admin';

let jsonResultData = [];

class TableWithAddButton extends Component {



    constructor(props) {
        super(props);
        this.onDropFile = this.onDropFile.bind(this);
        this.state = {
            csvUpload: '',
            settingsDropdown: false,
            settingsToggleClick: false
        };
    }

    onDropFile = (e, uuid) => {
        if (e.target.files.length) {
            let regex = new RegExp("(.*?)\.(csv)$");
            const file = e.target.files[0];
            let reader = new FileReader();
            if (!(regex.test(e.target.value.toLowerCase()))) {
                e.target.value = '';
                alert('Please select CSV file format!');
            } else {
                reader.readAsText(file);
                reader.onload = (state, callback) => {

                    this.setState({csvUpload: reader.result},
                        () => {
                            jsonResultData = csvToJson(this.state.csvUpload)
                        });
                    this.props.importItemsForStores(JSON.parse(jsonResultData), uuid)
                };
            }
        } else {
            return null
        }
    };


    onEditClick = (storeData) => {
        // this.props.showForm(true);
        this.props.setStoreData(storeData);
        this.props.history.push('/admin/store/' + storeData.uuid);
    };

    showDetails = (data) => {
        // this.props.showForm(true);
        // this.props.showStoreDetails(true);
        // this.props.getStoreData(data.uuid);
        this.props.history.push('/admin/store/details/' + data.uuid);
    };

    showButtonClickDetails = (data) => {
        this.props.setAllItems(null);
        // this.props.getStoreData(data.uuid, true, () => {
        this.props.history.push('/admin/items/store/' + data.uuid);
        this.props.setEditItemButton(true);
        // this.props.setStoreData(data);
        // }, 1, 10)
    };

    onToggleClick = (uuid, data, currentPage, pageSize) => {
        if (data.active) {
            this.openDeactivateModal(uuid, data);
            this.setState({toggleClick: false, currentPage: currentPage, pageSize: pageSize});
        } else {
            this.openActivateModal(uuid, data);
            this.setState({toggleClick: true, currentPage: currentPage, pageSize: pageSize});
        }
    };

    openActivateModal = (uuid, data) => {
        this.setState({activateModalOpen: !this.state.activateModalOpen, storeId: uuid, currentStoreData: data});
    };

    openDeactivateModal = (uuid, data) => {
        this.setState({deactivateModalOpen: !this.state.deactivateModalOpen, storeId: uuid, currentStoreData: data});
    };

    onToggle = (state, activate) => {
        if (activate) {
            if (state.currentStoreData) {
                state.currentStoreData.active = true;
            }
            this.setState({activateModalOpen: !this.state.activateModalOpen});
            this.props.enableStore(state.storeId, state.currentPage, state.pageSize)
        } else {
            if (state.currentStoreData) {
                state.currentStoreData.active = false;
            }
            this.setState({deactivateModalOpen: !this.state.deactivateModalOpen});
            this.props.disableStore(state.storeId, state.currentPage, state.pageSize)
        }
    };

    getModalContent = (activate) => {
        return (
            <>
                {
                    this.props.processing ?
                        <div style={{marginBottom: '200px'}}>
                            <Loader/>
                        </div> :
                        <Modal isOpen={activate ? this.state.activateModalOpen : this.state.deactivateModalOpen}
                               toggle={activate ? this.openActivateModal : this.openDeactivateModal}
                               className={'modal-danger ' + this.props.className}>

                            <ModalHeader toggle={activate ? this.openActivateModal : this.openDeactivateModal}>
                                Are you sure ?
                            </ModalHeader>

                            <ModalBody>
                                {
                                    activate ?
                                        <>Are you sure you want to enable this store ?</> :
                                        <>Are you sure you want to disable this store ?</>
                                }
                            </ModalBody>

                            <ModalFooter>
                                <Button color="secondary" onClick={() => this.onToggle(this.state, activate)}>
                                    {
                                        activate ? <>Enable</> : <>Disable</>
                                    }
                                </Button>
                            </ModalFooter>
                        </Modal>
                }
            </>
        );
    };

    roleChangeValue = (e) => {
        roleVal = e.currentTarget.textContent;
        this.setState({role: e.currentTarget.textContent},
            () => {
                this.props.getAllAdmins(this.state.role, 0, 10);
            }
        );
    };

    roleToggle = () => {
        this.setState(prevState => ({
            roleDropdownOpen: !prevState.roleDropdownOpen
        }));
    };

    settingsToggle = () => {
        this.setState(prevState => ({settingsDropdown: !prevState.settingsDropdown, settingsToggleClick : !this.state.settingsToggleClick}));
    };

    emptyFunction = () => {
        console.log("STATE" , this.state);
        // if(this.state.settingsToggleClick){
        //     this.setState(prevState => ({settingsDropdown: !prevState.settingsDropdown, settingsToggleClick : !this.state.settingsToggleClick}));
        // }
        if(this.state.storeUUID) {
            this.setState({storeUUID : null});
        }
    };

    settingsClick = (storeID, storeUUID) => {
        this.setState({storeID: storeID, storeUUID: storeUUID});
    };

    onStorePackageDropdownClick = (props, data) => {
        // props.showStorePackage(true);
        // this.props.setStoreData(data);
        // this.props.getStorePackages(data.uuid, data.name)
        this.props.history.push('store/package/' + data.uuid);
    };

    onStoreDealsDropdownClick = (props, data) => {
        // props.showStoreDeal(true);
        // this.props.setStoreData(data);
        // this.props.getStoreDeals(data.uuid, data.name)
        this.props.history.push('store/deals/' + data.uuid);
    };

    getSettingsDropDown = (data, stateID, btnTitle, topRightButtonTitle, props) => {
        return (
            <Dropdown isOpen={this.state.storeUUID === data.uuid}
                      style={{marginLeft: '15px'}}
                      toggle={() => this.emptyFunction()}
            >
                <DropdownToggle
                    onClick={() => this.settingsToggle()}
                    caret
                    className={"button"}
                    style={{background: 'none', padding: '3px 5px 2px 2px'}}>
                    <span className={"dropdown-icon"}/>
                </DropdownToggle>

                {
                    data && data.id === stateID ?
                        <div tabIndex='-1' role='menu' aria-hidden='false'
                             className="dropdown-menu dropdown-menu-right show"
                             data-placement='bottom-end'
                             style={{
                                 position: 'absolute',
                                 willChange: 'transform',
                                 top: '0px',
                                 left: '0px',
                                 transform: 'translate3d(-145px, 23px, 0px)',
                                 cursor: 'pointer'
                             }}>
                            <DropdownItem className={"pointer"}>
                                <div onClick={() => this.showButtonClickDetails(data)}>{btnTitle}</div>
                            </DropdownItem>
                            <input type="file" name="csvUpload" accept='.csv'
                                   ref={uploadElement => this.uploadElement = uploadElement}
                                   onChange={(e) => this.onDropFile(e, data.uuid)}
                                   hidden/>
                            <DropdownItem className={"pointer"}>
                                <div onClick={e => {
                                    e.preventDefault();
                                    this.uploadElement.click()
                                }}>{topRightButtonTitle}</div>
                            </DropdownItem>
                            <DropdownItem className={"pointer"}>
                                <div onClick={() => this.onStorePackageDropdownClick(props, data)}>Store Package</div>
                            </DropdownItem>
                            <DropdownItem className={"pointer"}>
                                <div onClick={() => this.onStoreDealsDropdownClick(props, data)}>Show Deals</div>
                            </DropdownItem>
                        </div> : null
                }
            </Dropdown>
        )
    };

    onTopBtnClick = (route) => {
        this.props.history.push(route);
    };

    render() {
        console.log("STATE> " , this.state);
        const {
            dataArray, title, headers, noDataTitle, btnTitle, topBtnTitle, storeForm, adminForm, vehicleForm,
            onDeleteBtnClick, vehicleEditBtn, topRightButtonTitle, activeLanguageLabel, route} = this.props;
        return (
            <Card>
                <CardHeader>
                    <Row>
                        <Col md={6}>
                            <CardTitle tag="h4">{title}</CardTitle>
                        </Col>
                        {
                            this.state.toggleClick === true ? this.getModalContent(true) :
                                this.getModalContent(false)
                        }
                        <Col md={{size: "2", offset: "2"}}>
                            {
                                adminForm ?
                                    <div>
                                        <Dropdown isOpen={this.state.roleDropdownOpen} toggle={() => this.roleToggle()}>
                                            <DropdownToggle caret>
                                                {roleVal}
                                                <span className={"dropdown-icon"}/>
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                {
                                                    Array.isArray(this.props.roles) && this.props.roles.map((data, i) => {
                                                        return (
                                                            <DropdownItem key={i}>
                                                                <div
                                                                    onClick={(e) => this.roleChangeValue(e)}>{data.role}</div>
                                                            </DropdownItem>
                                                        )
                                                    })
                                                }
                                            </DropdownMenu>
                                        </Dropdown>
                                    </div> : null
                            }
                        </Col>
                        <Col md={1} style={{paddingLeft: 'unset', marginLeft: '1%'}}>
                            <Button type="submit" size="sm" color="primary"
                                    onClick={() => this.onTopBtnClick(route)}
                                    style={{padding: "10px 19px"}}>
                                {topBtnTitle}
                            </Button>
                        </Col>
                    </Row>
                </CardHeader>
                <CardBody>
                    {
                        dataArray && dataArray.length > 0 ?
                            <Table className="tablesorter" responsive>
                                <thead className="text-primary">
                                <tr>
                                    {
                                        headers && headers.map((header, i) => {
                                            return (
                                                <th key={i}>{header}</th>
                                            )
                                        })
                                    }
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    storeForm === true ? dataArray && dataArray.map((data) => {
                                        return (
                                            <tr key={shortId.generate()}>
                                                <td style={{width: getTdWidth(headers[0]), cursor: "pointer"}}
                                                    onClick={() => this.showDetails(data)}
                                                >{data.name}</td>
                                                <td style={{width: getTdWidth(headers[1])}}>{data.address}</td>
                                                <td style={{width: getTdWidth(headers[2])}}>{data.id}</td>
                                                <td style={{width: getTdWidth(headers[3])}}>{data.geofence}</td>
                                                <td style={{width: getTdWidth(headers[4])}}>
                                                    <Row>
                                                        <Col md={4}>
                                                            <Row>
                                                                <Col md={1} style={{marginTop: "4px"}}>
                                                                    <i className="fas fa-edit fa-lg pointer"
                                                                       onClick={() => this.onEditClick(data)}/>
                                                                </Col>
                                                                <Col md={3} style={{paddingLeft: '5px'}}>
                                                                    <label className="switch"
                                                                           style={{marginLeft: "12px"}}>
                                                                        <input type="checkbox"
                                                                               checked={data.active}
                                                                               onChange={() => {
                                                                                   this.onToggleClick(data.uuid, data, this.props.currentPage, this.props.pageSize)
                                                                               }}
                                                                        />
                                                                        <span className="slider round"/>
                                                                    </label>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                        <Col md={4}
                                                             onClick={() => this.settingsClick(data.id, data.uuid)}>
                                                            {this.getSettingsDropDown(data, this.state.storeID, btnTitle, topRightButtonTitle, this.props)}
                                                        </Col>
                                                    </Row>
                                                </td>
                                            </tr>
                                        )
                                    }) : null
                                }
                                {
                                    adminForm ?
                                        <>
                                            {
                                                dataArray && dataArray.map(data => {
                                                    return (
                                                        <tr key={shortId.generate()}>
                                                            <td style={{
                                                                width: getTdWidth(headers[0]),
                                                                cursor: "pointer"
                                                            }}>
                                                                {capitalize(data.firstName) + " " + capitalize(data.lastName)}
                                                            </td>
                                                            <td style={{width: getTdWidth(headers[1])}}>{data.role ? data.role.role : ''}</td>
                                                            <td style={{width: getTdWidth(headers[2])}}>{data.phone}</td>
                                                            <td style={{width: getTdWidth(headers[3])}}>{data.email}</td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </> : null
                                }

                                {
                                    vehicleForm ?
                                        <>
                                            {
                                                dataArray && dataArray.map((data, index) => {
                                                    return (
                                                        <tr key={shortId.generate()}>
                                                            <td style={{width: getTdWidth(headers[0])}}>{data.title}</td>
                                                            <td style={{width: getTdWidth(headers[1])}}>{data.loadCapacity}</td>
                                                            <td style={{width: getTdWidth(headers[2])}}>{data.type}</td>
                                                            <td style={{width: getTdWidth(headers[3])}}>{data.weightLimit}/{data.weightUnit} </td>
                                                            <td style={{width: getTdWidth(headers[4])}}>{data.volumeLimit}/ {data.volumeUnit} </td>
                                                            <td style={{width: getTdWidth(headers[5])}}>
                                                                <Row>
                                                                    <Col md={1} style={{marginTop: "4px"}}>
                                                                        <i className="fas fa-edit fa-lg"
                                                                           onClick={() => vehicleEditBtn(true, data)}
                                                                        />
                                                                    </Col>
                                                                    <Col md={{size: '4', offset: '1'}}>
                                                                        <Button type="submit" size="sm" color="primary"
                                                                                onClick={() => onDeleteBtnClick(data, index)}
                                                                        >
                                                                            {btnTitle}
                                                                        </Button>
                                                                    </Col>
                                                                </Row>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </> : null
                                }

                                </tbody>
                            </Table> : <Row><Col md={{size: '6', offset: '5'}}>{noDataTitle}</Col></Row>
                    }
                </CardBody>
            </Card>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.storeReducer.processing,
    }
};

const connectedComponent = connect(mapStateToProps, {
    setStoreData,
    enableStore,
    disableStore,
    getStoreData,
    getAllAdmins,
    importItemsForStores,
    getStorePackages,
    getStoreDeals,
    setAllItems
})(TableWithAddButton);
export default withRouter(connectedComponent)

