import React from 'react';
import {Row, Col} from 'reactstrap';

const labelValue = (props) => {
    return (
        <Row>
            <Col md={12} className={"text-color-gray"} style={{fontWeight: '600'}}>
                {props.label}
            </Col>
            <Col md={12} className={"text-color-gray"}>
                {props.value}
            </Col>
        </Row>
    )
};

export default labelValue;