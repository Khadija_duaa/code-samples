import React, {Component} from 'react';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    Col,
    Row,
    Table,
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
} from "reactstrap";
import {capitalize, getTdWidth} from "../../utils/common-utils";
import Loader from '../../components/Loader/index';
import GetModal from "../../components/Modal";
import {
    saveStorePackage,
    deleteStorePackage,
    attachItemsWithStore,
    getStoreByUUID,
    getStorePackages,
    attachItemsWithPackage
} from '../../store/store/store-actions';
import AutoSuggest from "../Autosuggest/AutoSuggest";

const headers = ["Package Id", "Image", "Title", "Sub-Title", "Actions"];

class StorePackage extends Component {

    state = {
        modalVisible: false,
        modalContent: {},
        itemDataForPackage: null,
        bolClear: false,
    };

    openModal = () => {
        this.setState({modalVisible: !this.state.modalVisible});
    };


    componentDidMount() {
        const uuid = this.props.match.params.uuid;
        this.props.getStoreByUUID(uuid, (err) => {
            if (!err) {
                this.props.getStorePackages(uuid, this.props.storeName);
            }
        })
    }

    modalBtnClick = (heading, subHeading, image) => {
        let storeId = this.props.storeUUID;
        let storeName = this.props.storeName;
        let storePackageUUID = this.state.storePackageUUID;
        if (storePackageUUID) {
            this.props.saveStorePackage({storeId, heading, subHeading, image}, storeName, storePackageUUID,
                () => this.setState({storePackageUUID : null}))
        } else {
            this.props.saveStorePackage({storeId, heading, subHeading, image}, storeName);
        }
        this.setState({modalVisible: !this.state.modalVisible});
    };

    addItemsBtnClick = (filteredList, updateItem) => {
        let items = [];
        for (let i = 0; i < filteredList.length; i++) {
            items.push(filteredList[i].uuid);
        }
        this.props.attachItemsWithPackage(this.state.storePackageUUID, items, updateItem);
        this.setState({modalVisible: !this.state.modalVisible});
    };

    onDeletePackageClick = (data, index) => {
        let storeUUID = this.props.storeUUID;
        let storeName = this.props.storeName;
        if (data) {
            this.props.deleteStorePackage(data.uuid, index, storeUUID, storeName);
        }
        this.setState({modalVisible: !this.state.modalVisible});
    };

    onCreatePackageClick = () => {
        this.setState({
            modalVisible: true,
            modalContent: {
                headerText: "Create New Package",
                storePackage: true,
                footerShow: false,
                headingTextStyle: true,
                footerBtnText: 'Save',
                buttonClick: this.modalBtnClick,
                storePackageData: null
            },
            modalToggle: this.openModal,
            headerStyle: {backgroundColor: '#1e1e2e'},
            modalBodyStyle: {backgroundColor: '#1e1e2e'},
            modalFooterStyle: {backgroundColor: '#1e1e2e'}
        });
    };

    onPackageEditClick = (data) => {
        this.setState({
            modalVisible: true,
            modalContent: {
                headerText: "Update Package" + ' : ' + `${data.heading}`,
                storePackage: true,
                footerShow: false,
                headingTextStyle: true,
                footerBtnText: 'Save',
                buttonClick: this.modalBtnClick,
                storePackageData: data,
                itemDataForPackage: null
            },
            modalToggle: this.openModal,
            headerStyle: {backgroundColor: '#1e1e2e'},
            modalBodyStyle: {backgroundColor: '#1e1e2e'},
            modalFooterStyle: {backgroundColor: '#1e1e2e'},
            storePackageUUID: data.uuid
        });
    };

    onItemAddClick = (data) => {
        let storeUUID = this.props.storeUUID;
        this.setState({
            modalVisible: true,
            bolClear: false,
            modalContent: {
                headerText: "Add Item in Package" + ' : ' + `${data.heading}`,
                footerShow: false,
                headingTextStyle: true,
                footerBtnText: 'Save',
                addItem: true,
                buttonClick: this.addItemsBtnClick,
                itemDataForPackage: data,
                storeUUID: this.props.storeUUID
            },
            modalToggle: this.openModal,
            headerStyle: {backgroundColor: '#1e1e2e'},
            modalBodyStyle: {backgroundColor: '#1e1e2e'},
            modalFooterStyle: {backgroundColor: '#1e1e2e'},
            storePackageUUID: data.uuid,
        });
    };

    onPackageDeleteClick = (data, index) => {
        this.setState({
            modalVisible: true,
            modalContent: {
                headerText: "Are you sure?",
                bodyText: "Are you sure, you want to delete package : " + `${data.heading}`,
                footerShow: false,
                deleteModalBody: true,
                headingTextStyle: true,
                footerBtnText: 'Yes',
                storePackageData: data,
                index: index,
                buttonClick: this.onDeletePackageClick,
            },
            modalToggle: this.openModal,
            headerStyle: {backgroundColor: '#1e1e2e'},
            modalBodyStyle: {backgroundColor: '#1e1e2e'},
            modalFooterStyle: {backgroundColor: '#1e1e2e'}
        });
    };

    render() {
        const {storePackages} = this.props;
        return (
            <div className="content">
                <Row>
                    <Col md="12">
                        <Card>
                            {this.state.modalVisible ?
                                <GetModal isOpen={this.state.modalVisible}
                                          toggle={this.state.modalToggle}
                                          content={this.state.modalContent}
                                          modalBodyStyle={this.state.modalBodyStyle}
                                          headerStyle={this.state.headerStyle}
                                          modalFooterStyle={this.state.modalFooterStyle}
                                /> : null}
                            <CardHeader>
                                <Row>
                                    <Col md={6}>
                                        <CardTitle tag="h4">Packages for Store : {this.props.storeName}</CardTitle>
                                    </Col>
                                    <Col md={{size: '2', offset: '4'}}>
                                        <Button type="submit" size="sm" color="primary"
                                                style={{padding: "10px 19px"}}
                                                onClick={() => {
                                                    this.onCreatePackageClick()
                                                }}
                                        >
                                            Create Package
                                        </Button>
                                    </Col>
                                </Row>
                            </CardHeader>
                            {
                                this.props.processing ?
                                    <div style={{marginBottom: '200px'}}>
                                        <Loader/>
                                    </div> :
                                    <CardBody>
                                        <Table className="tablesorter" responsive>
                                            <thead className="text-primary">
                                            <tr>
                                                {
                                                    headers.map((header, i) => {
                                                        return (
                                                            <th key={i}>{header}</th>
                                                        )
                                                    })
                                                }
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                storePackages && storePackages.content.map((data, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td style={{width: getTdWidth(headers[0])}}>{data.id}</td>
                                                            <td style={{width: getTdWidth(headers[1])}}>
                                                                <img
                                                                    src={data.image ? data.image : "https://services.validoo.se/images/no-image-icon.png"}
                                                                    alt="Item Image"
                                                                    height={"30px"}
                                                                    width={"30px"}
                                                                />
                                                            </td>
                                                            <td style={{width: getTdWidth(headers[2])}}>{data.heading}</td>
                                                            <td style={{width: getTdWidth(headers[3])}}>{data.subHeading}</td>
                                                            <td style={{width: getTdWidth(headers[4])}}
                                                                className={"pointer"}>
                                                                <Row>
                                                                    <Col md={2} style={{marginTop: '2%'}}>
                                                                        <i className="fas fa-edit fa-lg"
                                                                           onClick={() => this.onPackageEditClick(data)}/>
                                                                    </Col>
                                                                    <Col md={5}>
                                                                        <Button className="btn-fill" color="primary"
                                                                                size={"sm"}
                                                                                onClick={() => this.onItemAddClick(data)}
                                                                                type="submit">
                                                                            + Item
                                                                        </Button>
                                                                    </Col>
                                                                    <Col md={2} style={{marginTop: '2%'}}>
                                                                        <i className="fas fa-trash-alt fa-lg"
                                                                           onClick={() => this.onPackageDeleteClick(data, index)}/>
                                                                    </Col>
                                                                </Row>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </Table>
                                    </CardBody>
                            }

                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.storeReducer.processing,
        storePackages: state.storeReducer.storePackages,
        storeName: state.storeReducer.storeName,
        storeUUID: state.storeReducer.storeUUID,
    }
};

const connectedComponent = connect(mapStateToProps, {
    saveStorePackage,
    deleteStorePackage,
    attachItemsWithPackage,
    getStorePackages,
    getStoreByUUID
})(StorePackage);
export default withRouter(connectedComponent)

