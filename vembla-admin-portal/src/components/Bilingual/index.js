import React from 'react'
import ReactFlagsSelect from 'react-flags-select';
import connect from 'react-redux/es/connect/connect';
import {setLanguage} from "../../store/multilingual/multilingual-actions";

const countries = ["US", "SE"];

const Bilingual = (props) => {

    const onSelectFlag = (countryCode) =>{
        props.setLanguage(countryCode)
    };

    return (
        <div style={{margin: '9px 9px 0px 0px'}}>
            <ReactFlagsSelect
                showSelectedLabel={false}
                showOptionLabel={false}
                countries={countries}
                onSelect={onSelectFlag}
                defaultCountry={props.activeLanguageLabel}
            />
        </div>
    );
};

const mapStateToProps = state => {
    return {
        activeLanguageLabel: state.multilingual.activeLanguageLabel
    }
};

export default connect(mapStateToProps, {setLanguage})(Bilingual);
