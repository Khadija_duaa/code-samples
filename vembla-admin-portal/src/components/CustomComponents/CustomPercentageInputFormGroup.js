import React, { useState } from "react";
import PropTypes from "prop-types";
import { FormGroup, Input, Label } from "reactstrap";

const errMsg = "Valid '%' Values: 0-100";
const validatePercentage = val => (val > -1 && val < 101 ? true : false);

const CustomPercentageInputFormGroup = ({
  label,
  name,
  onChange,
  type,
  defaultValue
}) => {
  const [value, setValue] = useState(() =>
    validatePercentage(defaultValue) ? defaultValue : ""
  );
  const [err, isErr] = useState(false);

  if (type === "number") {
    return (
      <FormGroup>
        <Label>
          {label}
          {err && <span style={{ color: "red" }}>{" " + errMsg}</span>}
        </Label>
        <Input
          type="number"
          name={name}
          value={value}
          placeholder={label}
          onChange={e => {
            const val = e.target.value;
            if (!validatePercentage(val)) {
              onChange(name, -1);
              isErr(true);
              setValue(val);
            } else {
              onChange(name, val);
              isErr(false);
              setValue(val);
            }
          }}
          onFocus={e => (e.target.value = "")}
        />
      </FormGroup>
    );
  }

  if (type === "range") {
    return (
      <FormGroup>
        <Label>{`${label} ${value}%`}</Label>
        <Input
          type="range"
          min="0"
          max="100"
          value={value}
          name={name}
          onChange={e => {
            setValue(e.target.value);
            onChange(name, e.target.value);
          }}
        />
      </FormGroup>
    );
  }
};

CustomPercentageInputFormGroup.propTypes = {
  type: PropTypes.oneOf(["number", "range"]).isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  defaultValue: (props, propName, componentName) => {
    if (props[propName] && !validatePercentage(props[propName])) {
      // invalid % out of Range
      return new Error(
        `Invalid Prop value '${propName}' supplied to '${componentName}' Validation failed. Value must be in range 0-100. Current Value: ${props[propName]}`
      );
    }
  }
};

export default CustomPercentageInputFormGroup;
