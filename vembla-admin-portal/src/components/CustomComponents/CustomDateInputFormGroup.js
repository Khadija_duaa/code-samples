import React from "react";
import PropTypes from "prop-types";
import { FormGroup, Label, Input } from "reactstrap";

// Incomplete yet without formatting and validation

const CustomDateInputFormGroup = ({ label, name, onChange }) => {
  return (
    <FormGroup>
      <Label>{label}</Label>
      <Input
        type="text"
        name={name}
        placeholder="YYYY-MM-DD"
        onChange={e => onChange(name, e.target.value)}
      />
    </FormGroup>
  );
};

CustomDateInputFormGroup.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default CustomDateInputFormGroup;
