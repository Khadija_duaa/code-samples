import React from 'react';
import shortid from 'shortid';
import { Table } from 'reactstrap';

import { defaultBlankImg } from '../../utils/common-utils';

// Review for dynamic fields based on headers

const getDeliveredOrderItems = data => {
  return data.map(rowData => (
    <tr key={rowData.uuid}>
      <td>
        <img
          alt="Item Logo"
          src={rowData.defaultImage || defaultBlankImg}
          style={{ height: '30px', width: '30px' }}
        />
      </td>
      <td>{rowData.name}</td>
      <td>{rowData.purchasedQuantity}</td>
      <td>{rowData.recalculatedDiscountedPricePerUnit}</td>
      <td>{rowData.recalculatedTotalPrice}</td>
    </tr>
  ));
};

const getPendingOrderItems = data => {
  return data.map(rowData => (
    <tr key={rowData.uuid}>
      <td>
        <img
          alt="Item Logo"
          src={rowData.defaultImage || defaultBlankImg}
          style={{ height: '30px', width: '30px' }}
        />
      </td>
      <td>{rowData.name}</td>
      <td>{rowData.quantity}</td>
      <td>{rowData.pricePerUnit}</td>
      <td>{rowData.totalPrice}</td>
    </tr>
  ));
};

const titles = {
  itemIcon: 'ICON',
  itemName: 'Name',
  itemQuantity: 'Quantity',
  itemPrice: 'Price',
  itemTotalPrice: 'Total Price'
};

const itemHeaders = [
  titles.itemIcon,
  titles.itemName,
  titles.itemQuantity,
  titles.itemPrice,
  titles.itemTotalPrice
];

const CustomItemsTable = ({ data, isPendingOrder, isDeliveredOrder }) => {
  return (
    <Table className="tablesorter" responsive>
      <thead className="text-primary">
        <tr>
          {itemHeaders &&
            itemHeaders.map((title, i) => <th key={i}>{title}</th>)}
        </tr>
      </thead>
      {isDeliveredOrder && <tbody>{getDeliveredOrderItems(data)}</tbody>}
      {isPendingOrder && <tbody>{getPendingOrderItems(data)}</tbody>}
    </Table>
  );
};

export default CustomItemsTable;
