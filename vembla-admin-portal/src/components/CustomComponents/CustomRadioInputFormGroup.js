import React from "react";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";

import { capitalize } from "../../utils/common-utils";

const CustomRadioInputFormGroup = ({
  data,
  label,
  defaultValue,
  name,
  onChange
}) => {
  return (
    <FormGroup>
      <Label>{label}</Label>
      {data.map((val, i) => (
        <Row key={i}>
          <Col md={{ size: 10, offset: 2 }}>
            <Label>
              <Input
                type="radio"
                id={i}
                name={name}
                onChange={() => onChange(name, val)}
                checked={val === defaultValue}
              />
              {capitalize(val)}
            </Label>
          </Col>
        </Row>
      ))}
    </FormGroup>
  );
};

export default CustomRadioInputFormGroup;
