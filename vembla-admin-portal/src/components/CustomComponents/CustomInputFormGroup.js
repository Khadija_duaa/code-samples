import React from "react";
import PropTypes from "prop-types";
import { FormGroup, Input, Label } from "reactstrap";

const CustomInputFormGroup = ({ type, label, name, val, onChange }) => {
  return (
    <FormGroup>
      <Label>{label}</Label>
      <Input
        type={type}
        value={val}
        name={name}
        placeholder={label}
        onChange={e => onChange(e.target.name, e.target.value)}
      />
    </FormGroup>
  );
};

CustomInputFormGroup.propTypes = {
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  val: PropTypes.any
};

export default CustomInputFormGroup;
