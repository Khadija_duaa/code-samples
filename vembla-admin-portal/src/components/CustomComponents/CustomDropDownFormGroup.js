import React, {useState} from 'react';
import {
    FormGroup,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';

const CustomDropDownFormGroup = ({
                                     label,
                                     btnText,
                                     name,
                                     dataField,
                                     dispField,
                                     data,
                                     onChange,
                                     isCaretOnly
                                 }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedValue, setSelectedValue] = useState(btnText);

    const toggle = () => setIsOpen(prevVal => !prevVal);

    return (

        <FormGroup>
            {!isCaretOnly && <label>{label}</label>}
            <Dropdown isOpen={isOpen} toggle={toggle}>
                <DropdownToggle caret>
                    {(!isCaretOnly && selectedValue) || (
                        <span className={'dropdown-icon'}/>
                    )}
                </DropdownToggle>
                <DropdownMenu>
                    {data && data.length ? (

                        <>
                            {data.map((item, i) => (
                                <DropdownItem
                                    key={i}
                                    onClick={e => {
                                        setSelectedValue(item[dispField]);
                                        onChange(name, item[dataField]);
                                    }}
                                >
                                    {item[dispField]}
                                </DropdownItem>
                            ))}
                        </>
                    ) :null}

                </DropdownMenu>
            </Dropdown>
        </FormGroup>
    );
};

export default CustomDropDownFormGroup;
