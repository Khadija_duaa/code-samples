import React from "react";
import PropTypes from "prop-types";
import {
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

const CustomConfirmationModal = ({ showModal, message, onYes, onCancel }) => {
  return (
    <div>
      <Modal isOpen={showModal} toggle={onCancel}>
        <ModalHeader toggle={onCancel}>Confirmaton!</ModalHeader>
        <ModalBody>{message}</ModalBody>
        <ModalFooter>
          <Row>
            <Col md="6">
              <Button onClick={() => onCancel()}>Cancel</Button>
            </Col>
            <Col md="6">
              <Button onClick={() => onYes()}>Yes</Button>
            </Col>
          </Row>
        </ModalFooter>
      </Modal>
    </div>
  );
};

CustomConfirmationModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  onYes: PropTypes.func,
  onCancel: PropTypes.func
};

export default CustomConfirmationModal;
