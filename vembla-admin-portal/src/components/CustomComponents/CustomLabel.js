import React from "react";
import { Label } from "reactstrap";

const CustomLabel = ({ label }) => {
  return <Label>{label}</Label>;
};

export default CustomLabel;
