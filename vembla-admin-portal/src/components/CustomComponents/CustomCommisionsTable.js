import React from 'react';
import { Table, Button } from 'reactstrap';

import { getDateFromISO, timeSplitter } from '../../utils/common-utils';

// Review for dynamic fields based on headers

const titles = {
  status: 'Status',
  commission: 'Commission',
  date: 'Date'
};

const itemHeaders = [titles.status, titles.commission, titles.date];

const CustomItemsTable = ({ data }) => {
  return (
    <Table responsive>
      <thead className="text-primary">
      {
          data && data.length > 0 ?
              <tr>
                  {itemHeaders && itemHeaders.map((title, i) => <th key={i}>{title}</th>)}
              </tr> : null
      }
      </thead>
      <tbody>
        {data.map(rowData => {
          const datetime =
            getDateFromISO(rowData.createdAt) +
            ', ' +
            timeSplitter(rowData.createdAt);
          return (
            <tr key={rowData.uuid}>
              <td>{rowData.paid ? 'Settled' : 'Unsettled'}</td>
              <td>{rowData.commission}</td>
              <td>{datetime}</td>
              <td>
                <Button>Order</Button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

export default CustomItemsTable;
