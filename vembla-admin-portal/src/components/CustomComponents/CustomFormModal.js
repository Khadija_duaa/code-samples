import React from "react";
import PropTypes from "prop-types";
import {Card, Modal, ModalBody, ModalHeader} from "reactstrap";

const test = (onCancel) => {
    return onCancel
};

const CustomFormModal = ({showModal, element, onCancel, label}) => {
    return (
        <Modal isOpen={showModal} toggle={onCancel}>
            <Card style={{margin: "0px"}}>
                {
                    label && label ?
                        <ModalHeader toggle={test(onCancel)}>
                            <label style={{color: "white"}}>{label}</label>
                        </ModalHeader> : null
                }
                <ModalBody>{element}</ModalBody>
            </Card>
        </Modal>
    );
};

CustomFormModal.propTypes = {
    showModal: PropTypes.bool.isRequired,
    element: PropTypes.element.isRequired,
    onCancel: PropTypes.func
};

export default CustomFormModal;
