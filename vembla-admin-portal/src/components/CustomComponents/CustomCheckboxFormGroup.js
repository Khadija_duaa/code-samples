import React from "react";
import PropTypes from "prop-types";
import { FormGroup, Input, Label } from "reactstrap";

const CustomCheckboxFormGroup = ({ label, value, name, onChange }) => {
  return (
    <FormGroup>
      <Label>
        <Input
          type="checkbox"
          checked={value}
          onChange={() => onChange(name, !value)}
        />
        {label}
      </Label>
    </FormGroup>
  );
};

CustomCheckboxFormGroup.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default CustomCheckboxFormGroup;
