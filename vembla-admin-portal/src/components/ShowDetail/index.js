import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardTitle,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
} from 'reactstrap';
import Loader from '../../components/Loader/index';
import {
    getAllPrograms,
    getPendingShopper,
    getRevolutAccounts,
    getShopperVehicleInfo, getVehicleTypes, resetRedux
} from "../../store/shopper/shopper-actions";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {capitalize} from "../../utils/common-utils";

class ShowDetail extends Component {

    prepareBasicDataFields = (data) => {
        const rowData = [];
        rowData.push({
            label: 'Name',
            value: capitalize(data.firstName) + " " + capitalize(data.lastName)
        });
        rowData.push({
            label: 'Phone',
            value: data.phone
        });
        rowData.push({
            label: 'Fleet ID',
            value: data.uuidFleet
        });
        rowData.push({
            label: 'Email',
            value: data.email
        });
        return rowData;
    };

    prepareVehicleDataFields = (data) => {
        const rowData = [];
        if (data.vehicleInfo) {
            let vehicle = data.vehicleInfo;
            rowData.push({
                label: 'Vehicle Name',
                value: vehicle.name
            });
            rowData.push({
                label: 'Color',
                value: vehicle.color
            });
            rowData.push({
                label: 'Registration Number',
                value: vehicle.registrationNumber
            });
        }
        return rowData;
    };

    prepareVehicleTypeDataFields = (data) => {
        const rowData = [];
        if (data.vehicleType) {
            let vehicleType = data.vehicleType;
            rowData.push({
                label: 'Vehicle Type',
                value: vehicleType.name
            });
            rowData.push({
                label: 'Volume Limit',
                value: vehicleType.volumeLimit + " " + vehicleType.volumeUnit
            });
            rowData.push({
                label: 'Weight Limit',
                value: vehicleType.weightLimit + " " + vehicleType.weightUnit
            });
        }
        return rowData;
    };

    render() {
        const {title, shopperVehicleInfo, approveFleetDetail, noDataTitle} = this.props;
        let basicDataFields = this.prepareBasicDataFields(approveFleetDetail ? approveFleetDetail : '');
        let vehicleDataFields = this.prepareVehicleDataFields(approveFleetDetail ? approveFleetDetail : '');
        let vehicleTypeDataFields = this.prepareVehicleTypeDataFields(approveFleetDetail ? approveFleetDetail.vehicleInfo : '');
        return (
            <div>
                <Card>
                    <CardHeader>
                        <CardTitle tag="h4">
                            <i className="tim-icons icon-minimal-left pointer"
                               onClick={() => this.props.showDetails(false)}
                               style={{color: 'white', padding: "9px 12px 11px 0px"}}>
                            </i>
                            {title}  {approveFleetDetail ? ":" : null} {capitalize(approveFleetDetail && approveFleetDetail.firstName) + " " +
                        capitalize(approveFleetDetail && approveFleetDetail.lastName)}
                        </CardTitle>
                    </CardHeader>
                    {
                        this.props.processing ?
                            <div style={{marginBottom: '200px'}}>
                                <Loader/>
                            </div> :

                            <CardBody>
                                {
                                    approveFleetDetail && approveFleetDetail !== '' ?
                                        <>
                                            <Row>
                                                <Col md={12}>
                                                    {
                                                        basicDataFields && basicDataFields.map(data => {
                                                            return (
                                                                <Col md={12}>
                                                                    <Row>
                                                                        <Col md={{size: '4', offset: '1'}}
                                                                             className={"text-color-gray"}
                                                                             style={{fontWeight: "600", marginBottom: '12px'}}>{data.label}</Col>
                                                                        <Col md={6}
                                                                             className={"text-color-gray"}>{data.value}</Col>
                                                                    </Row>
                                                                </Col>
                                                            )
                                                        })
                                                    }
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={7} style={{textAlign: 'center'}}>
                                                    <h4>Fleet Vehicle Information</h4>
                                                </Col>
                                                <Col md={12}>
                                                    {
                                                        vehicleDataFields && vehicleDataFields.map(data => {
                                                            return (
                                                                <Col md={12}>
                                                                    <Row>
                                                                        <Col md={{size: '4', offset: '1'}}
                                                                             className={"text-color-gray"}
                                                                             style={{
                                                                                 fontWeight: "600",
                                                                                 marginBottom: '12px'
                                                                             }}
                                                                        >{data.label}</Col>
                                                                        <Col md={6}
                                                                             className={"text-color-gray"}>{data.value}</Col>
                                                                    </Row>
                                                                </Col>
                                                            )
                                                        })
                                                    }
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={6} style={{margin: '2% 0% 1% 114px'}}>
                                                    <h5 style={{fontWeight: '100'}}>Vehicle Type Information</h5>
                                                </Col>
                                                <Col md={12}>
                                                    {
                                                        vehicleTypeDataFields && vehicleTypeDataFields.map(data => {
                                                            return (
                                                                <Col md={12}>
                                                                    <Row>
                                                                        <Col md={{size: '4', offset: '1'}}
                                                                             className={"text-color-gray"}
                                                                             style={{
                                                                                 fontWeight: "600",
                                                                                 marginBottom: '12px'
                                                                             }}
                                                                        >{data.label}</Col>
                                                                        <Col md={6}
                                                                             className={"text-color-gray"}>{data.value}</Col>
                                                                    </Row>
                                                                </Col>
                                                            )
                                                        })
                                                    }
                                                </Col>
                                            </Row>
                                        </>
                                        : <Row><Col md={{size: '6', offset: '5'}}>{noDataTitle}</Col></Row>
                                }
                            </CardBody>
                    }
                </Card>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        approveFleetDetail: state.shopper.approveFleetDetail
    };
};

const connectedComponent = connect(mapStateToProps)(ShowDetail);
export default withRouter(connectedComponent);

