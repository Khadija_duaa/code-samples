import React from 'react';
import ReactTransition from "../ReactTransition"
import {Row, Col} from 'reactstrap';
import {capitalize} from "../../utils/common-utils";
import RowCol from '../../components/RowCol';
import RowColHeading from '../../components/RowColHeading';

const riderDetail = (props) => {
    const riderData = props.orderDetail;
    let payload = null;
    let itemsArray = null;
    let prepareItemArray = [];

    if (props.orderDetail && props.orderDetail.assignment) {
        if(props.orderDetail.assignment.payload){
            payload = props.orderDetail.assignment.payload;
            if(payload !== null){
                itemsArray = payload.store && payload.store.items;
            }
        }
    }

    if (itemsArray) {
        itemsArray.forEach(item => {
            prepareItemArray.push({
                title: 'Item Name',
                value: item.name
            });
            prepareItemArray.push({
                title: 'Price/per unit',
                value: item.pricePerUnit
            });
            prepareItemArray.push({
                title: 'Item Brand',
                value: item.brand && item.brand.name
            });
            prepareItemArray.push({
                title: 'Item Category',
                value: item.category && item.category.name
            });
            prepareItemArray.push({
                title: 'Quantity',
                value: item.quantity
            });
            prepareItemArray.push({
                title: 'Total Price',
                value: item.totalPrice
            });
        });
    }

    const prepareRiderData = (riderData) => {
        const data = [];
        if (riderData) {
            data.push({
                title: 'Name',
                value: capitalize(riderData.firstName) + " " + capitalize(riderData.lastName)
            });
            data.push({
                title: 'Phone',
                value: riderData.phone
            });
        }
        return data;
    };

    const prepareOrderData = (payload) => {
        const data = [];
        if (payload) {
            data.push({
                title: 'Total Order Amount',
                value: payload.total
            });
        }
        return data;
    };

    const prepareStoreData = (payload) => {
        const data = [];
        if (payload) {
            data.push({
                title: 'Store Name',
                value: payload.store && payload.store.name
            });
            data.push({
                title: 'Store Address',
                value: payload.store && payload.store.address
            });
        }
        return data;
    };

    const prepareConsumerData = (payload) => {
        const data = [];
        if (payload) {
            data.push({
                title: 'Consumer Name',
                value: capitalize(payload.firstName) + " " + capitalize(payload.lastName)
            });
            data.push({
                title: 'Phone',
                value: payload.phone
            });
            data.push({
                title: 'Address',
                value: payload.deliveryAddress
            });
        }
        return data;
    };

    const riderDataArr = prepareRiderData(riderData);
    const orderDataArr = prepareOrderData(payload && payload);
    const storeDataArr = prepareStoreData(payload && payload);
    const consumerDataArr = prepareConsumerData(payload && payload);

    return (
        <ReactTransition>
            <div style={{height: '83vh'}}>
                <Row onClick={() => props.setRiderDetailVisibility(false)} style={{cursor: 'pointer'}}>
                    <Col md={1} style={{padding: "9px 0px 0px 21px"}}>
                        <i className="tim-icons icon-minimal-left"
                           style={{color: 'white'}}/>
                    </Col>
                    <Col md={{size: "1"}} className="heading">
                        Details
                    </Col>
                </Row>

                <div className={"innerDiv scrollbar"} id="style-1">
                    <RowColHeading style={{marginBottom: '6px', marginTop: '15px'}}
                                   spanStyle={{fontWeight: '600'}}
                                   heading={"Rider Details"}/>
                    {
                        riderDataArr && riderDataArr.map(riderData => {
                            return (
                                <RowCol style={{margin: '7px 3px 15px 7px'}}
                                        title={riderData.title}
                                        value={riderData.value}/>
                            )
                        })
                    }

                    {
                        props.orderDetail && props.orderDetail.assignment && props.orderDetail.assignment.payload ?
                            <>
                                <RowColHeading style={{marginBottom: '6px', marginTop: '15px'}}
                                               spanStyle={{fontWeight: '600'}}
                                               heading={"Order Details"}/>
                                {
                                    prepareItemArray && prepareItemArray.map(item => {
                                        return (
                                            <div style={{marginTop: '15px'}}>
                                                <RowCol style={{margin: '0px 3px 0px 7px'}}
                                                        title={item.title}
                                                        value={item.value}/>
                                            </div>

                                        )
                                    })
                                }
                                {
                                    orderDataArr && orderDataArr.map(orderData => {
                                        return (
                                            <RowCol style={{margin: '15px 3px 15px 7px'}}
                                                    title={orderData.title}
                                                    value={orderData.value}/>
                                        )
                                    })
                                }
                                <RowColHeading style={{marginBottom: '6px', marginTop: '15px'}}
                                               spanStyle={{fontWeight: '600'}}
                                               heading={"Store Details"}/>
                                {
                                    storeDataArr && storeDataArr.map(storeData => {
                                        return (
                                            <RowCol style={{margin: '7px 3px 15px 7px'}}
                                                    title={storeData.title}
                                                    value={storeData.value}/>
                                        )
                                    })
                                }
                                <RowColHeading style={{marginBottom: '6px', marginTop: '15px'}}
                                               spanStyle={{fontWeight: '600'}}
                                               heading={"Consumer Details"}/>
                                {
                                    consumerDataArr && consumerDataArr.map(consumerData => {
                                        return (
                                            <RowCol style={{margin: '7px 3px 15px 7px'}}
                                                    title={consumerData.title}
                                                    value={consumerData.value}/>
                                        )
                                    })
                                }
                            </> : null
                    }
                </div>
            </div>
        </ReactTransition>
    );
};

export default riderDetail;