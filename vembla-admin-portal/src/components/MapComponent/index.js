import React, {Component} from 'react';
import {GoogleApiWrapper, Map, Marker} from 'google-maps-react';
import carIcon from '../../assets/img/icons8-car-40.png';
import houseIcon from '../../assets/img/icons8-dog-house-30.png';
import _ from 'lodash';
import {connect} from 'react-redux';

export class MapContainer extends Component {

    shouldComponentUpdate(nextProps) {
        if (nextProps.loadPage === true) {
            return true;
        } else if (nextProps.loadPage === false) {
            return false;
        }
    }

    getMarkers = () => {
        let {fleet} = this.props;
        let markers = fleet.activeFleet;

        if (fleet.selectedArray && fleet.selectedId && fleet[fleet.selectedArray] && fleet[fleet.selectedArray].length) {
            markers = fleet[fleet.selectedArray].filter(marker => marker.uuidFleet === fleet.selectedId);
        }

        return markers;
    };

    render() {
        let markerArray = this.getMarkers();
        const {click} = this.props;
        let points = [];
        let currentLocation = _.isEmpty(markerArray) ? {lat: "31.4697", lng: "74.2728"} : markerArray[0];

        if (markerArray.length > 0) {
            markerArray.forEach(point => {
                points.push(point.location)
            });
        }

        let bounds = new this.props.google.maps.LatLngBounds();
        for (let i = 0; i < points.length; i++) {
            bounds.extend(points[i]);
        }
        return (
            <Map
                google={this.props.google}
                zoom={currentLocation && click === true ? 15 : 10}
                style={{width: '96%', height: '100%', position: 'relative'}}
                initialCenter={currentLocation.location}
                bounds={bounds}
            >
                {
                    markerArray && markerArray.map(marker => {
                        const {location} = marker;
                        let lat, lng;
                        if (marker.assignment && marker.assignment.payload) {
                            lat = marker.assignment.payload.deliveryLatitude;
                            lng = marker.assignment.payload.deliveryLongitude;
                        }
                        return ([
                                <Marker
                                    title={'Riders Current Location'}
                                    position={{lat: location ? location.lat : "", lng: location ? location.lng : ""}}
                                    icon={{
                                        url: carIcon,
                                    }}
                                />,
                                click === true ?
                                    <Marker
                                        title={'Riders Destination Location'}
                                        position={{
                                            lat: lat ? lat : "",
                                            lng: lng ? lng : ""
                                        }}
                                        icon={{
                                            url: houseIcon,
                                        }}
                                    /> : null
                            ]
                        )
                    })
                }
            </Map>
        );
    }
}


const googleMapComponent = GoogleApiWrapper({
    apiKey: 'AIzaSyAmrkCGdtCCWHfzueCST6TLSllPA2zY7iM'
})(MapContainer);

const mapStateToProps = (state) => {
    return {
        fleet: state.fleet
    };
};

export default connect(mapStateToProps)(googleMapComponent);
