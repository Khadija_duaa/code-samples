import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardTitle,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
} from 'reactstrap';
import {connect} from "react-redux";
import {saveStore, setStoreData, getStoreByUUID, resetRedux} from "../../store/store/store-actions";
import {withRouter} from "react-router-dom";
import Loader from '../../components/Loader/index';
import MapWithAutoCompleteSearch from '../MapWithAutoCompleteSearch/index';
import TimeInput from 'react-time-input';
import TagsInput from "react-tagsinput";

class StoreForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            formName: "Add New Store",
            name: '',
            geofence: null,
            icon: '',
            address: '',
            latitude: '',
            longitude: '',
            storeTimings: '',
            storePostalCodes: []
        };
        this.handleRegularTags = this.handleRegularTags.bind(this);
    }

    componentDidMount() {
        const uuid = this.props.match.params.id;
        if(uuid){
            this.props.getStoreByUUID(uuid, () => {
                if (this.props.storeData) {
                    let {name, storeTimings, icon, geofence, address, latitude, longitude, storePostalCodes} = this.props.storeData;
                    this.setState({
                        name,
                        storeTimings,
                        icon,
                        geofence,
                        address,
                        latitude,
                        longitude,
                        storePostalCodes,
                        formName: `Update Store ${name}`
                    });
                }
            })
        }
    }

    componentWillMount() {
        this.props.resetRedux();
        let storeId = this.props.match.params.id;
    }

    handleRegularTags(regularTags) {
        this.setState({storePostalCodes : regularTags});
    };

    getStoreTime = (storeTimings, day, startTime) => {
        for (let i = 0; i < storeTimings.length; i++) {
            if (startTime && storeTimings[i].day === day) {
                return storeTimings[i].startTime
            } else if (storeTimings[i].day === day) {
                return storeTimings[i].endTime
            }
        }
    };

    storeTimings = [
        {
            day: '1',
            dayName: 'Monday',
            startTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 1, true) : '00:00',
            endTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 1, false) : '23:59'
        },
        {
            day: '2',
            dayName: 'Tuesday',
            startTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 2, true) : '00:00',
            endTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 2, false) : '23:59'
        },
        {
            day: '3',
            dayName: 'Wednesday',
            startTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 3, true) : '00:00',
            endTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 3, false) : '23:59'
        },
        {
            day: '4',
            dayName: 'Thursday',
            startTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 4, true) : '00:00',
            endTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 4, false) : '23:59'
        },
        {
            day: '5',
            dayName: 'Friday',
            startTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 5, true) : '00:00',
            endTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 5, false) : '23:59'
        },
        {
            day: '6',
            dayName: 'Saturday',
            startTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 6, true) : '00:00',
            endTime: this.props.storeData ?
                this.getStoreTime(this.props.storeData.storeTimings, 6, false) : '23:59'
        }
    ];

    onInputChange = (e, parseIntValue) => {
        let state = {...this.state};
        let newValue = e.target.value;
        if (parseIntValue) {
            if(e.target.value === ""){
                newValue = ""
            }else {
                if(newValue.match(/[a-z]/i)){
                    alert("Only numbers are acceptable for Radius!")
                    return newValue
                }else {
                    newValue = parseInt(e.target.value);
                }
            }
            state[e.target.name] = newValue;
            this.setState(state);
        } else {
            state[e.target.name] = e.target.value;
            this.setState(state);
        }
    };

    prepareFields = () => {
        const inputFields = [];
        inputFields.push({
            name: 'name',
            placeholder: 'Store Name',
            label: 'Store Name',
            value: this.state.name
        });
        inputFields.push({
            name: 'icon',
            placeholder: 'Icon URL - https://icon.png',
            label: 'Icon',
            value: this.state.icon
        });
        inputFields.push({
            name: 'geofence',
            placeholder: '10',
            label: 'Radius',
            value: this.state.geofence
        });
        inputFields.push({
            name: 'address',
            placeholder: 'Address',
            label: 'Address',
            value: this.state.address
        });

        return inputFields;
    };

    onStoreSave = (currentPage, pageSize, parentFun) => {
        const {name, storeTimings, icon, geofence, address, latitude, longitude, storePostalCodes} = this.state;
        this.props.saveStore({name, storeTimings, icon, geofence, address, latitude, longitude, storePostalCodes}, currentPage, pageSize);
        if (this.props.storeData) {
            // parentFun(false);
            this.props.history.push('/admin/store')
        }
    };

    setLocation = (location) => {
        this.setState({latitude: location.lat, longitude: location.lng})
    };

    onCancelBtnClick = (parentFun) => {
        // parentFun(false, true);
        this.props.setStoreData(null);
        this.props.history.push('/admin/store')
    };

    onTimeChangeHandler = (time, index, startTime) => {
        for (let i = 0; i < this.storeTimings.length; i++) {
            if (index === i) {
                if (startTime) {
                    this.storeTimings[i].startTime = time
                } else {
                    this.storeTimings[i].endTime = time
                }
            }
        }
        this.setState({storeTimings: this.storeTimings})
    };

    inputProps = () => {
        return {
            placeholder: 'Type Codes',
        }
    };

    render() {
        const fields = this.prepareFields();
        this.props.setStoreData(this.props.storeData);

        console.log("StoreTiming -", this.storeTimings)
        return (
            <div className="content">
                <Row>
                    <Col md="12">
                        {
                            this.props.processing ?
                                <div style={{marginBottom: '200px'}}>
                                    <Loader/>
                                </div> :
                                <Card>
                                    <CardHeader>
                                        <CardTitle tag="h4">{this.state.formName}</CardTitle>
                                    </CardHeader>
                                    <CardBody>
                                        <Form>
                                            <Row>
                                                {
                                                    fields.map(field => {
                                                        let parseIntValue;
                                                        if (field.label === "Radius") {
                                                            parseIntValue = true
                                                        } else {
                                                            parseIntValue = false
                                                        }
                                                        return (
                                                            <Col className="pr-md-1" md="6">
                                                                <FormGroup>
                                                                    <label>{field.label}</label>
                                                                    <Input
                                                                        value={field.value}
                                                                        onChange={(e) => this.onInputChange(e, parseIntValue)}
                                                                        name={field.name}
                                                                        placeholder={field.placeholder}
                                                                        type="text"
                                                                    />
                                                                </FormGroup>
                                                            </Col>
                                                        )
                                                    })
                                                }
                                            </Row>

                                            <Row>
                                                <Col className="pr-md-1" md="6">
                                                    <label>Postal Codes</label>
                                                    <TagsInput
                                                        inputProps={this.inputProps()}
                                                        value={this.state.storePostalCodes && this.state.storePostalCodes.length > 0 ? this.state.storePostalCodes : [] }
                                                        onChange={this.handleRegularTags}
                                                        tagProps={{className: "react-tagsinput-tag bg-info text-white rounded"}}
                                                    />
                                                </Col>
                                            </Row>

                                            <Row style={{marginTop: "20px"}}>
                                                <Col md={12}>
                                                    <label>Week Timings</label>
                                                </Col>
                                            </Row>
                                            {
                                                this.storeTimings && this.storeTimings.map((timing, index) => {
                                                    return (
                                                        <Row style={{paddingLeft: "25%"}}>
                                                            <Col md={2} className={"text-color-gray"}>{timing.dayName}</Col>
                                                            <Col md={6}>
                                                                <Row>
                                                                    <Col md={6}>
                                                                        <label>Start Time</label>
                                                                        <TimeInput
                                                                            initTime={(timing.startTime).slice(0,5)}
                                                                            ref="TimeInputWrapper"
                                                                            className='form-control'
                                                                            onTimeChange={(e) => this.onTimeChangeHandler(e, index, true)}
                                                                        />
                                                                    </Col>
                                                                    <Col md={6}>
                                                                        <label>End Time</label>
                                                                        <TimeInput
                                                                            initTime={(timing.endTime).slice(0,5)}
                                                                            ref="TimeInputWrapper"
                                                                            className='form-control'
                                                                            onTimeChange={(e) => this.onTimeChangeHandler(e, index, false)}
                                                                        />
                                                                    </Col>
                                                                </Row>
                                                            </Col>
                                                        </Row>
                                                    )
                                                })
                                            }

                                            <Row style={{marginBottom: '40px', padding: '16px'}}>
                                                <MapWithAutoCompleteSearch
                                                    setLocation={this.setLocation}
                                                    google={this.props.google}
                                                    center={{
                                                        lat: this.props.storeData && this.props.storeData.latitude ? this.props.storeData.latitude : 30.5204,
                                                        lng: this.props.storeData && this.props.storeData.longitude ? this.props.storeData.longitude : 73.8567
                                                    }}
                                                    height='300px'
                                                    zoom={15}
                                                />
                                            </Row>
                                        </Form>
                                    </CardBody>

                                    <CardFooter style={{textAlign: 'center'}}>
                                        <Button className="btn-fill" color="secondary"
                                                type="submit"
                                                onClick={() => this.onCancelBtnClick(this.props.showStoreForm)}>
                                            Cancel
                                        </Button>
                                        <Button className="btn-fill" color="primary"
                                                onClick={() => this.onStoreSave(this.props.currentPage, this.props.pageSize, this.props.showStoreForm)}
                                                type="submit">
                                            {this.props.storeData ? "Update" : "Save"}
                                        </Button>
                                    </CardFooter>
                                </Card>
                        }
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        storeData: state.storeReducer.storeData,
        processing: state.storeReducer.processing
    }
};

const connectedComponent = connect(mapStateToProps, {saveStore, setStoreData, getStoreByUUID, resetRedux})(StoreForm);
export default withRouter(connectedComponent);

