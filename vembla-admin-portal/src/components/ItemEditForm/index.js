import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardTitle,
    Row,
    Col,
    Form,
    FormGroup,
    Input
} from 'reactstrap';
import {connect} from "react-redux";
import {setItemData, updateItem} from "../../store/store/store-actions";
import {withRouter} from "react-router-dom";
import Loader from '../../components/Loader/index';

class ItemEditForm extends Component {

    state = {
        quantity: '',
        price: '',
        uuid: ''
    };

    componentDidMount() {
        if (this.props.itemData) {
            let {quantity, price, uuid} = this.props.itemData;
            this.setState({quantity, price, uuid});
        }
    }

    onInputChange = (e) => {
        let state = {...this.state};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    prepareFields = () => {
        const inputFields = [];
        inputFields.push({
            name: 'quantity',
            label: 'Quantity',
            value: this.state.quantity
        });
        inputFields.push({
            name: 'price',
            label: 'Price',
            value: this.state.price
        });

        return inputFields;
    };

    onStoreSave = () => {
        const uuid = this.state.uuid;
        const quantity = parseInt(this.state.quantity);
        const price = parseInt(this.state.price);
        this.props.updateItem({quantity, price}, uuid);
    };

    onCancelBtnClick = (parentFun) => {
        parentFun(false);
        this.props.setItemData(null)
    };

    render() {
        const fields = this.prepareFields();
        return (
            <div>
                {
                    this.props.processing ?
                        <div style={{marginBottom: '200px'}}>
                            <Loader/>
                        </div> :
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h4">Update Store Item</CardTitle>
                            </CardHeader>

                            <CardBody>
                                <Form>
                                    <Row>
                                        {
                                            fields.map(field => {
                                                return (
                                                    <Col className="pr-md-1" md="6">
                                                        <FormGroup>
                                                            <label>{field.label}</label>
                                                            <Input
                                                                value={field.value}
                                                                onChange={(e) => this.onInputChange(e)}
                                                                name={field.name}
                                                                type="text"
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                )
                                            })
                                        }
                                    </Row>
                                </Form>
                            </CardBody>

                            <CardFooter style={{textAlign: 'center'}}>
                                <Button className="btn-fill" color="secondary"
                                        type="submit"
                                        onClick={() => this.onCancelBtnClick(this.props.showItemEditForm)}>
                                    Cancel
                                </Button>
                                <Button className="btn-fill" color="primary"
                                        onClick={() => this.onStoreSave()}
                                        type="submit">
                                    Update
                                </Button>
                            </CardFooter>
                        </Card>
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        itemData: state.storeReducer.itemData,
        processing: state.storeReducer.processing
    }
};

const connectedComponent = connect(mapStateToProps, {setItemData, updateItem})(ItemEditForm);
export default withRouter(connectedComponent);

