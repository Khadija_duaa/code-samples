import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    CardTitle,
    Row,
    Col,
    Table
} from 'reactstrap';
import LabelValue from '../../components/LabelValue';
import './item.css';
import {capitalize, getTdWidth} from "../../utils/common-utils";

class ItemDetail extends Component {

    valueAlreadyExists = (list, value) => {
        let found = false;
        if (typeof list !== 'undefined') {
            for (let i = 0; list && i < list.length; i++) {
                if (list[i] === value.nutrientTypeCode) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    };

    render() {
        const {showItemDetailData, data} = this.props;
        let nutritionDataList = [];
        let nutritionData;

        if (data && data.nutritionalInformation) {
            console.log("Nutritional Information : ", data.nutritionalInformation)
            let nutrientDetailArray = [];
            let uniqueTypeCodes = [];

            nutritionData = JSON.parse(unescape(data.nutritionalInformation));
            if (nutritionData && nutritionData.nutrientHeader && nutritionData.nutrientHeader.nutrientDetail) {
                nutrientDetailArray = nutritionData.nutrientHeader.nutrientDetail
                console.log("Nutritional Detail : ", nutrientDetailArray)
            }

            if (typeof nutrientDetailArray !== 'undefined') {
                for (let i = 0; i < nutrientDetailArray.length; i++) {
                    if (!this.valueAlreadyExists(uniqueTypeCodes, nutrientDetailArray[i])) {
                        uniqueTypeCodes.push(nutrientDetailArray[i].nutrientTypeCode);
                    }
                }
            }
            for (let i = 0; i < uniqueTypeCodes.length; i++) {
                let dataList = [];
                let typeCodeLabel = '';
                if (typeof nutrientDetailArray !== 'undefined') {
                    for (let j = 0; j < nutrientDetailArray.length; j++) {
                        if (uniqueTypeCodes[i] === nutrientDetailArray[j].nutrientTypeCode) {
                            if (typeCodeLabel === '') {
                                typeCodeLabel = nutrientDetailArray[j].nutrientTypeCode
                            }
                            dataList.push(nutrientDetailArray[j])
                        }
                    }
                }
                nutritionDataList.push({
                    typeCodeLabel: typeCodeLabel,
                    data: dataList
                });
            }
        }


        return (
            <div>
                <Card>
                    <CardHeader>
                        <CardTitle tag="h4" style={{cursor: "pointer"}} onClick={showItemDetailData}>
                            <i className="tim-icons icon-minimal-left"
                               style={{color: 'white', padding: "9px 12px 11px 0px"}}>
                            </i>
                            Details for item : {data.name}
                        </CardTitle>
                    </CardHeader>

                    <CardBody>
                        <Row>
                            <Col md={4}>
                                <LabelValue label={"Brand"} value={data.brand ? data.brand.name : ''}/>
                            </Col>
                            <Col md={4}>
                                <LabelValue label={"Category"} value={data.category ? data.category.name : ''}/>
                            </Col>
                            <Col md={4}>
                                <LabelValue label={"GTIN"} value={data.gtin}/>
                            </Col>
                        </Row>
                        <Row style={{marginTop: '3%'}}>
                            <Col md={4}>
                                <LabelValue label={"Supplier Name"} value={data.supplierName ? data.supplierName : ''}/>
                            </Col>
                            <Col md={4}>
                                <LabelValue label={"Supplier gln"} value={data.supplierGln ? data.supplierGln : ''}/>
                            </Col>
                        </Row>

                        <div id={"myHeader"} style={{marginTop: '4%', display:'flex'}}>
                            {
                                data && data.images.length > 0 ?
                                    <>
                                        {
                                            data.images.map(image => {
                                                return (
                                                    <Col md={2} style={{textAlign: 'center'}}
                                                    >

                                                        <a href={image.url.replace("view", "download")} style={{color: '#bebec4',display:'block'}} download>
                                                            <img
                                                                src={image.url ? image.url : null}
                                                                alt="Item"
                                                                height={"150px"}
                                                                width={"150px"}
                                                                style={{ objectFit: 'cover',marginBottom:'4px'}}
                                                            />
                                                            <div style={{ textColor:'white',}}>{'Click to download'}</div>
                                                        </a>
                                                    </Col>
                                                )
                                            })
                                        }
                                    </> : null
                            }
                        </div>

                        <Row style={{marginTop: '3%'}}>
                            <Col md={6}>
                                {
                                    nutritionData && nutritionData.nutrientHeader && nutritionData.nutrientHeader.nutrientDetail ?
                                        <Row>
                                            <Col md={12} className={"text-color-gray"} style={{fontWeight: '600'}}>Nutrition
                                                Information</Col>
                                            <Col md={12} style={{marginTop: '3%'}}>
                                                <Card style={{backgroundColor: '#33384d'}}>
                                                    <CardBody className={"scrollbar"} id="style-1" style={{marginBottom: 'unset'}}>
                                                        <Table className="tablesorter" responsive>
                                                            <thead className="text-primary">
                                                            <tr>
                                                                <th>Nutrition Information</th>
                                                                <th>Quantity</th>
                                                                <th>Unit</th>
                                                            </tr>
                                                            </thead>

                                                            <tbody>
                                                            {
                                                                nutritionDataList && nutritionDataList.length > 0 ?
                                                                    <>
                                                                        {
                                                                            nutritionDataList.map(data => {
                                                                                return (
                                                                                    <>
                                                                                        <div style={{marginTop: '3%'}}>
                                                                                            <tr className={"text-color-gray"} style={{fontWeight: '600'}}>
                                                                                                {data.typeCodeLabel}
                                                                                            </tr>
                                                                                        </div>
                                                                                        {
                                                                                            data.data.length > 0 ?
                                                                                                <>
                                                                                                    {
                                                                                                        data.data.map(innerData => {
                                                                                                            return(
                                                                                                                <tr>
                                                                                                                    <td>{innerData.measurementPrecisionCode}</td>
                                                                                                                    <td>{innerData.quantityContained ?
                                                                                                                        innerData.quantityContained.quantity : '' }</td>
                                                                                                                    <td>{innerData.quantityContained ?
                                                                                                                        innerData.quantityContained.measurementUnitCode : '' }
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            )
                                                                                                        } )
                                                                                                    }
                                                                                                </> : null
                                                                                        }

                                                                                    </>
                                                                                )
                                                                            })
                                                                        }
                                                                    </> : null
                                                            }
                                                            </tbody>
                                                        </Table>
                                                    </CardBody>
                                                </Card>
                                            </Col>
                                        </Row> :
                                        <Row>
                                            <Col md={12} className={"text-color-gray"} style={{fontWeight: '600'}}>No Nutrition Information Found!</Col>
                                        </Row>
                                }
                            </Col>
                            <Col md={6}>
                                {
                                    data.marketingMessage ?
                                        <Row>
                                            <Col md={12} className={"text-color-gray"} style={{fontWeight: '600'}}>Product
                                                Description</Col>
                                            <Col md={12} className={"text-color-gray"}
                                                 style={{marginTop: '3%'}}>{data.marketingMessage}</Col>
                                        </Row> : null
                                }
                                {
                                    data.ingredients ?
                                        <Row style={{marginTop: '4%'}}>
                                            <Col md={12} className={"text-color-gray"}
                                                 style={{fontWeight: '600'}}>Ingredients</Col>
                                            <Col md={12} className={"text-color-gray"}
                                                 style={{marginTop: '3%'}}>{data.ingredients}</Col>
                                        </Row> : null
                                }

                                <Row style={{marginTop: '4%'}}>
                                    <Col md={12} className={"text-color-gray"} style={{fontWeight: '600'}}>Storage</Col>
                                    <Col md={12} style={{marginTop: '3%'}}>
                                        <Row>
                                            <Col md={6}>
                                                <LabelValue label={"Maximum Storage Temperature"}
                                                            value={data.maxStorageTemp ? data.maxStorageTemp : ''}/>
                                            </Col>
                                            <Col md={6}>
                                                <LabelValue label={"Maximum Storage Temperature"}
                                                            value={data.maxTransportTemp ? data.maxTransportTemp : ''}/>
                                            </Col>
                                        </Row>
                                        <Row style={{marginTop: '3%'}}>
                                            {
                                                data.minStorageTemp ?
                                                    <Col md={6}>
                                                        <LabelValue label={"Minimum Storage Temperature"}
                                                                    value={data.minStorageTemp}/>
                                                    </Col> : null
                                            }
                                            {
                                                data.minTransportTemp ?
                                                    <Col md={6}>
                                                        <LabelValue label={"Minimum Storage Temperature"}
                                                                    value={data.minTransportTemp}/>
                                                    </Col> : null
                                            }

                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default ItemDetail;

