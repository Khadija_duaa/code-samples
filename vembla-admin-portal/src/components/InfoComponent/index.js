import React, { Component } from "react";
import { Row, Col, Button } from "reactstrap";

class InfoComponent extends Component {
  render() {
    const {
      riderName,
      riderPhone,
      riderEmail,
      onClick,
      riderFirstName
    } = this.props;
    return (
      <div
        className="main-container"
        onClick={onClick}
        style={{ cursor: onClick !== "undefined" ? "pointer"  : null}}
      >
        <Row>
          <Col md="3" style={{ textAlign: "center" }}>
            <div className={"name-div"}>
              <div className={"text-div"}>{riderName.charAt(0)}</div>
            </div>
            <span className={"text-color-gray"}>{riderName}</span>
          </Col>
          <Col md="9">
            <Row>
              <Col md="10">
                <Row style={{ marginTop: "4%" }}>
                  <Col md={12}>
                    <span className={"text-color-gray"}>{riderPhone}</span>
                  </Col>
                  <Col md={12}>
                    <span className={"text-color-gray"}>{riderEmail}</span>
                  </Col>
                </Row>
              </Col>
              <Col md="2" style={{ marginTop: "18px" }}>
                <i
                  className="tim-icons icon-minimal-right"
                  style={{ color: "white" }}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export default InfoComponent;
