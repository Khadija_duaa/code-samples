import React, {Component} from "react";
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardTitle,
    Row,
    Col,
    Form,
    FormGroup,
    Input
} from "reactstrap";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import Loader from "../../components/Loader/index";
import {
    getAllCategories,
    getAllCategoriesBySearch,
    saveCategory,
    setCategoryData
} from "../../store/store/store-actions";
import "./index.css";
import {NotificationManager} from "react-notifications";
import SubCategoryField from "./SubCategoryField";

let timeoutRef = null;
let searchValue;

let example = [{a: "1"}, {a: "2"}, {a: "3"}];

class CategoryForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            childCategories: [],
            categoryName: "Create Category",
            searchValue: "",
            filteredList: []
        };
    }

    componentDidMount() {
        console.log("~~~~~~~~~CategoryForm componentDidMount");
        if (this.props.categoryData && this.props.categoryData) {
            let {name} = this.props.categoryData;
            let _childCategories = [];
            let childCategories =
                this.props.categoryData &&
                this.props.categoryData.childCategories &&
                this.props.categoryData.childCategories;
            for (let i = 0; i < childCategories.length; i++) {
                _childCategories.push({
                    name: childCategories[i].name,
                    uuid: childCategories[i].uuid
                });
            }
            this.setState({
                categoryName: "Update Category",
                name: name,
                filteredList: _childCategories
            });
        }
    }

    prepareFields = () => {
        const inputFields = [];
        inputFields.push({
            name: "name",
            placeholder: "Category Name",
            label: " Name",
            childCategories: [],
            value: this.state.name
        });

        return inputFields;
    };

    onCancelBtnClick = parentFun => {
        parentFun(false);
    };

    onInputChange = e => {
        let state = {...this.state};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    onInputChangeBySearch = (e, searchApi) => {
        let text = e.target.value;
        let state = {...this.state};
        state[e.target.name] = text;
        this.setState(state);
        searchValue = text;
        if (timeoutRef) {
            clearTimeout(timeoutRef);
        }
        if (text.length > 2) {
            timeoutRef = setTimeout(() => {
                searchApi(text);
            }, 1000);
        }
    };

    onCategorySave = parentFun => {
        const {name} = this.state;
        let childCategories = [];
        for (let i = 0; i < this.state.filteredList.length; i++) {
            childCategories.push(
                this.state.filteredList &&
                this.state.filteredList[i] &&
                this.state.filteredList[i].uuid
            );
        }
        this.props.saveCategory(
            {name, childCategories},
            () => parentFun(false),
            () => {
                this.props.getAllCategories(0, 10);
            }
        );
    };

    isCategoryExists = suggestion => {
        let found = false;
        for (let i = 0; i < this.state.filteredList.length; i++) {
            if (
                this.state.filteredList &&
                this.state.filteredList[i] &&
                this.state.filteredList[i].name === suggestion.name
            ) {
                found = true;
                NotificationManager.error("You have already selected this category!");
                break;
            }
        }
        return found;
    };

    onClickCategory = suggestion => {
        this.props.allCategoriesBySearch.length = 0;

        let _filteredList = [...this.state.filteredList];
        if (!this.isCategoryExists(suggestion)) {
            _filteredList.push({name: suggestion.name, uuid: suggestion.uuid});
            this.setState(
                {
                    searchValue: suggestion.name,
                    filteredList: _filteredList
                },
                () => {
                    console.log("its final state", this.state);
                }
            );
        }
    };

    onClickSubCategoryClose = name => {
        const filteredList = this.state.filteredList.filter(
            item => item.name !== name
        );
        this.setState({filteredList});
    };

    render() {
        const {showCategoryForm} = this.props;
        const fields = this.prepareFields();
        return (
            <div>
                {this.props.processing ? (
                    <div style={{marginBottom: "200px"}}>
                        <Loader/>
                    </div>
                ) : (
                    <Card>
                        <CardHeader>
                            <CardTitle tag="h4">{this.state.categoryName}</CardTitle>
                        </CardHeader>

                        <CardBody>
                            <Form>
                                <Row>
                                    {fields.map(field => {
                                        return (
                                            <Col className="pr-md-1" md="6" key={field.label}>
                                                <FormGroup>
                                                    <label>{field.label}</label>
                                                    <Input
                                                        value={field.value}
                                                        onChange={e => this.onInputChange(e)}
                                                        name={field.name}
                                                        placeholder={field.placeholder}
                                                        type="text"
                                                    />
                                                </FormGroup>
                                            </Col>
                                        );
                                    })}
                                </Row>
                                <Row>
                                    {
                                        <Col className="pr-md-1" md="6">
                                            <SubCategoryField
                                                list={this.state.filteredList}
                                                onItemClose={this.onClickSubCategoryClose}
                                                label={"Sub Categories"}
                                            />
                                        </Col>
                                    }
                                </Row>
                                <Row>
                                    <Col className="pr-md-1" md="6">
                                        <FormGroup>
                                            <label>{"Add Sub Category"}</label>
                                            <Input
                                                placeholder={"Search and Add Category..."}
                                                type="text"
                                                onChange={e => this.onInputChangeBySearch(e, this.props.getAllCategoriesBySearch)}
                                                value={this.state.searchValue}
                                                name={"searchValue"}
                                                style={{marginBottom: "3px"}}
                                            />
                                            <div id={"myHeader"} style={{
                                                border: "1px solid #272A3D",
                                                backgroundColor: "#272A3D",
                                                minHeight: "150px",
                                                overflowY: "auto",
                                                overflowX: "hidden"
                                            }}>
                                                {this.props.allCategoriesBySearch && this.props.allCategoriesBySearch.length ? (
                                                    <ul style={{
                                                        cursor: "pointer",
                                                        listStyleType: "none",
                                                        marginLeft: "-41px"
                                                    }}>
                                                        {this.props.allCategoriesBySearch.map((suggestion, index) => {
                                                                return (
                                                                    <li style={{
                                                                        backgroundColor: "white",
                                                                        margin: "-1px",
                                                                        borderRadius: "0.4285rem"
                                                                    }}
                                                                        key={suggestion.name}
                                                                        onClick={() => {
                                                                            this.onClickCategory(suggestion)
                                                                        }}>
                                                                        <p style={{color: "black", marginLeft: "9px"}}>
                                                                            {suggestion.name}
                                                                        </p>
                                                                    </li>
                                                                );
                                                            }
                                                        )}
                                                    </ul>
                                                ) : (
                                                    <div>
                                                        <p id={"myP"} style={{fontFamily: "Poppins"}}>
                                                            No suggestions!
                                                        </p>
                                                    </div>
                                                )}
                                            </div>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </Form>
                        </CardBody>

                        <CardFooter style={{textAlign: "center"}}>
                            <Button
                                className="btn-fill"
                                color="secondary"
                                type="submit"
                                onClick={() => this.onCancelBtnClick(showCategoryForm)}
                            >
                                Cancel
                            </Button>
                            <Button
                                className="btn-fill"
                                color="primary"
                                onClick={() => this.onCategorySave(showCategoryForm)}
                                type="submit"
                            >
                                Save
                            </Button>
                        </CardFooter>
                    </Card>
                )}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        processing: state.storeReducer.processing,
        categoryData: state.storeReducer.categoryData,
        allCategoriesBySearch: state.storeReducer.allCategoriesBySearch
    };
};

const connectedComponent = connect(mapStateToProps, {
    saveCategory,
    setCategoryData,
    getAllCategoriesBySearch,
    getAllCategories
})(CategoryForm);
export default withRouter(connectedComponent);
