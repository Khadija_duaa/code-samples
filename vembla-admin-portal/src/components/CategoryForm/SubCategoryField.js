import React from "react";
import { FormGroup, Badge } from "reactstrap";
import "./index.css";

const SubcategoryField = ({ list, onItemClose, label }) => {
  return (
    <FormGroup>
      <label className={"text-color-gray"}>{label ? label : ''}</label>
      <div
        id={"myHeader"}
        style={{
          border: "1px solid #525f7f",
          borderRadius: "0.4285rem",
          display: "flex",
          maxWidth: "auto",
          padding: "11px",
          overflow: "auto"
        }}
      >
        {list.map((field, index) => {
          return (
            <Badge
              key={index}
              color="secondary"
              style={{
                height: "30px",
                width: "auto",
                display: "flex"
              }}
            >
              <h5
                style={{
                  color: "black",
                  marginTop: "2px"
                }}
              >
                {field.name}
              </h5>{" "}
              <span
                className="fa fa-window-close fa-2x"
                onClick={() => onItemClose(field.name)}
                style={{
                  backgroundColor: "#f4f5f7",
                  marginTop: "1px",
                  marginLeft: "5px",
                  color: "red"
                }}
              ></span>
            </Badge>
          );
        })}
      </div>
    </FormGroup>
  );
};

export default SubcategoryField;
