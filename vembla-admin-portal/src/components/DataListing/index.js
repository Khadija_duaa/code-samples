import React, {Component} from "react";
import csv from "csv";
import * as regex from "react-notifications";
import {
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardTitle,
    Col,
    Row,
    Table,
    Button,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Input
} from "reactstrap";

import {csvToJson} from "../../utils/config";
import {
    getTdWidth,
    getDateFromISO,
    capitalize
} from "../../utils/common-utils";

let jsonData = [];
let timeoutRef = null;
let searchValue;

class DataListing extends Component {
    state = {
        searchValue: ""
    };

    onInputChange = (e, searchAPI) => {
        let text = e.target.value;
        let state = {...this.state};
        state[e.target.name] = text;
        this.setState(state);
        searchValue = text;
        if (timeoutRef) {
            clearTimeout(timeoutRef);
        }
        if (text.length > 2) {
            timeoutRef = setTimeout(() => {
                searchAPI(text);
            }, 1000);
        }
    };

    onEditClick = (showCategoryForm, onCategoryEditClick, data) => {
        showCategoryForm(true);
        onCategoryEditClick(true, data);
    };

    constructor(props) {
        super(props);
        this.onDrop = this.onDrop.bind(this);
        this.state = {
            csvUpload: ""
        };
    }

    onDrop = e => {
        if (e.target.files.length) {
            let regex = new RegExp("(.*?).(csv)$");
            const file = e.target.files[0];
            let reader = new FileReader();
            if (!regex.test(e.target.value.toLowerCase())) {
                e.target.value = "";
                alert("Please select CSV file format!");
            } else {
                reader.readAsText(file);
                reader.onload = (state, callback) => {
                    this.setState({csvUpload: reader.result}, () => {
                        jsonData = csvToJson(this.state.csvUpload);
                    });
                    this.props.importItems(JSON.parse(jsonData));
                };
            }
        } else {
            return null;
        }
    };

    render() {
        const {
            title,
            headers,
            dataArray,
            categories,
            items,
            brands,
            noDataTitle,
            orders,
            btnTitle,
            itemEditBtn,
            priceTitle,
            actionTitle,
            button,
            topBtnTitle,
            searchAPI,
            search,
            deleteBtnTitle,
            onItemDetail,
            onEditItemClick,
            onItemDetailClick,
            onCategoryDetailClick,
            onBrandDetailClick,
            onDeleteBtnClick,
            showCategoryForm,
            onCategoryEditClick
        } = this.props;
        return (
            <Card>
                <CardHeader>
                    {button ? (
                        <Row>
                            <Col md={6}>
                                <CardTitle tag="h4">{title ? title : ""}</CardTitle>
                            </Col>
                            {search ? (
                                <Col md={{size: "5", offset: "1"}}>
                                    <Row>
                                        <Col md={8}>
                                            <InputGroup style={{marginTop: "1%"}}>
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="fas fa-search"/>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input
                                                    placeholder="Search"
                                                    value={searchValue ? searchValue : this.state.searchValue}
                                                    name={"searchValue"}
                                                    onChange={e => this.onInputChange(e, searchAPI)}
                                                />
                                            </InputGroup>
                                        </Col>
                                        <Col md={4}>
                                            <input
                                                type="file"
                                                name="csvUpload"
                                                accept=".csv"
                                                ref={uploadElement =>
                                                    (this.uploadElement = uploadElement)
                                                }
                                                onChange={this.onDrop}
                                                hidden
                                            />
                                            <Button
                                                type="submit"
                                                size="sm"
                                                color="primary"
                                                onClick={e => {
                                                    e.preventDefault();
                                                    this.uploadElement.click();
                                                }}
                                                style={{padding: "10px 19px"}}
                                            >
                                                {topBtnTitle}
                                            </Button>
                                        </Col>
                                    </Row>
                                </Col>
                            ) : (
                                <Col md={{size: "2", offset: "4"}}>
                                    <Button
                                        type="submit"
                                        size="sm"
                                        color="primary"
                                        onClick={() => showCategoryForm(true)}
                                        style={{padding: "10px 19px"}}
                                    >
                                        {topBtnTitle}
                                    </Button>
                                </Col>
                            )}
                        </Row>
                    ) : (
                        <CardTitle tag="h4">{title ? title : ""}</CardTitle>
                    )}
                </CardHeader>
                <CardBody>
                    {dataArray && dataArray.length > 0 ? (
                        <Table className="tablesorter" responsive>
                            <thead className="text-primary">
                            <tr>
                                {headers &&
                                headers.map((header, i) => {
                                    return (
                                        <th key={i}>{itemEditBtn && i === 6 ? priceTitle : header}</th>
                                    );
                                })}
                                {itemEditBtn ? <th>{actionTitle}</th> : null}
                            </tr>
                            </thead>
                            <tbody>
                            {dataArray && dataArray.map((data, index) => {
                                return (
                                    <tr key={index}>
                                        {categories ? (
                                            <>
                                                <td style={{width: getTdWidth(headers[0])}}>
                                                    {data.name}
                                                </td>
                                                <td style={{width: getTdWidth(headers[1])}}>
                                                    {data.colorCode && data.colorCode}
                                                </td>
                                                <td style={{width: getTdWidth(headers[2])}}>
                                                    <Row>
                                                        <Col md={2} style={{marginTop: "1%"}}>
                                                            <i className="fas fa-edit fa-lg pointer"
                                                               onClick={() => this.onEditClick(showCategoryForm, onCategoryEditClick, data)}
                                                            />
                                                        </Col>
                                                        <Col md={4}>
                                                            <Button
                                                                type="submit"
                                                                size="sm"
                                                                color="primary"
                                                                onClick={() =>
                                                                    onCategoryDetailClick(true, data)
                                                                }
                                                            >
                                                                {btnTitle}
                                                            </Button>
                                                        </Col>
                                                        <Col md={2}
                                                             style={{marginTop: "1%", cursor: "pointer"}}
                                                        >
                                                            <i className="fas fa-trash-alt fa-lg"
                                                               onClick={() =>
                                                                    onDeleteBtnClick(data, index)
                                                                }
                                                            />
                                                        </Col>
                                                    </Row>
                                                </td>
                                            </>
                                        ) : null}
                                        {items ? (
                                            <>
                                                <td>
                                                    <img
                                                        src={
                                                            data.defaultImage ||
                                                            "https://services.validoo.se/images/no-image-icon.png"
                                                        }
                                                        alt="Item"
                                                        height={"30px"}
                                                        width={"30px"}
                                                    />
                                                </td>
                                                <td
                                                    style={{
                                                        width: getTdWidth(headers[0]),
                                                        cursor: "pointer"
                                                    }}
                                                    onClick={() => onItemDetail(data)}
                                                >
                                                    {data.name}
                                                </td>
                                                <td style={{width: getTdWidth(headers[1])}}>
                                                    {data.category && data.category.name}
                                                </td>
                                                <td style={{width: getTdWidth(headers[2])}}>
                                                    {data.id}
                                                </td>
                                                <td style={{width: getTdWidth(headers[3])}}>
                                                    {data.brand && data.brand.name}
                                                </td>
                                                {/*<td style={{width: getTdWidth(headers[4])}}>{getDateFromISO(data.createdAt)}</td>*/}
                                                <td style={{width: getTdWidth(headers[5])}}>
                                                    {data.gtin}
                                                </td>
                                                {data.stores ? (
                                                    <td style={{width: getTdWidth("Price")}}>
                                                        {data.stores[0] &&
                                                        data.stores[0].storeItem &&
                                                        data.stores[0].storeItem.price}
                                                    </td>
                                                ) : null}
                                                {itemEditBtn ? (
                                                    <td>
                                                        <i
                                                            className="fas fa-edit fa-lg"
                                                            onClick={() => onEditItemClick(data, true)}
                                                        />
                                                    </td>
                                                ) : (
                                                    <td>
                                                        <Button
                                                            type="submit"
                                                            size="sm"
                                                            color="primary"
                                                            onClick={() => onItemDetailClick(data)}
                                                        >
                                                            {btnTitle}
                                                        </Button>
                                                    </td>
                                                )}
                                            </>
                                        ) : null}
                                        {brands ? (
                                            <>
                                                <td style={{width: getTdWidth(headers[0])}}>
                                                    {data.name}
                                                </td>
                                                <td style={{width: getTdWidth(headers[1])}}>
                                                    {getDateFromISO(data.createdAt)}
                                                </td>
                                                <td style={{width: getTdWidth(headers[2])}}>
                                                    <Button
                                                        type="submit"
                                                        size="sm"
                                                        color="primary"
                                                        onClick={() => onBrandDetailClick(data)}
                                                    >
                                                        {btnTitle}
                                                    </Button>
                                                </td>
                                            </>
                                        ) : null}
                                        {orders ? (
                                            <>
                                                <td style={{width: getTdWidth(headers[0])}}>
                                                    {capitalize(data.firstName) +
                                                    " " +
                                                    capitalize(data.lastName)}
                                                </td>
                                                <td style={{width: getTdWidth(headers[2])}}>
                                                    {data.total}
                                                </td>
                                                <td style={{width: getTdWidth(headers[1])}}>
                                                    {data.phone}
                                                </td>
                                                <td style={{width: getTdWidth(headers[4])}}>
                                                    {data.store && data.store.name}
                                                </td>
                                                <td style={{width: getTdWidth(headers[3])}}>
                                                    {data.deliveryAddress}
                                                </td>
                                            </>
                                        ) : null}
                                    </tr>
                                );
                            })}
                            </tbody>
                        </Table>
                    ) : (
                        <Row>
                            <Col md={{size: "6", offset: "5"}}>{noDataTitle}</Col>
                        </Row>
                    )}
                </CardBody>
            </Card>
        );
    }
}

export default DataListing;
