import React from "react";
import shortid from "shortid";
import {
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Row,
    Col
} from "reactstrap";
import classnames from "classnames";
import DetailInfoComponent from "../DetailInfoComponent";
import OrderDetailComponent from "../OrderDetailComponent";
import InfoComponent from "../InfoComponent";
import {setMapData} from "../../store/fleet/fleet-actions";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {capitalize} from "../../utils/common-utils";
import RiderDetail from "../RiderDetail";

class TabComponent extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: "1",
            showOrderDetail: false,
            currentOrderDetail: {},
            showRiderDetail: false,
            currentRiderDetail: {}
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    onRiderClick = (key, uuid, data) => {
        this.props.setMapData(key, uuid, true);
        this.setState({showRiderDetail: true, currentRiderDetail: data});
    };

    setRiderDetailVisibility = value => {
        this.setState({showOrderDetail: value, showRiderDetail: value});
        this.props.setMapData(null, null, false);
    };

    render() {
        const {
            title,
            firstNavItem,
            secondNavItem,
            thirdNavItem,
            orderFirstNavContent,
            orderSecondNavContent,
            assignedOrdersArray,
            pendingOrdersArray,
            freeRiders,
            inactiveRiders,
            activeFleet,
            busyFleet,
            offlineFleet,
            assignOrderManually,
            getPendingOrders
        } = this.props;

        return (
            <div>
                <div className={"tab-heading-div"}>{title}</div>
                {this.state.showOrderDetail ? (
                    <RiderDetail
                        orderDetail={this.state.currentRiderDetail}
                        setRiderDetailVisibility={this.setRiderDetailVisibility}
                    />
                ) : this.state.showRiderDetail === true ? (
                    <RiderDetail
                        orderDetail={this.state.currentRiderDetail}
                        setRiderDetailVisibility={this.setRiderDetailVisibility}
                    />
                ) : (
                    <>
                        <Nav tabs>
                            <NavItem
                                className={"col-md-4"}
                                style={{
                                    borderBottom:
                                        this.state.activeTab === "1" ? "0.0625rem solid" : ""
                                }}
                            >
                                <NavLink
                                    className={classnames({
                                        active: this.state.activeTab === "1"
                                    })}
                                    onClick={() => {
                                        this.toggle("1");
                                    }}
                                >
                                    {firstNavItem}
                                </NavLink>
                            </NavItem>
                            <NavItem
                                className={"col-md-4"}
                                style={{
                                    borderBottom:
                                        this.state.activeTab === "2" ? "0.0625rem solid" : ""
                                }}
                            >
                                <NavLink
                                    className={classnames({
                                        active: this.state.activeTab === "2"
                                    })}
                                    onClick={() => {
                                        this.toggle("2");
                                    }}
                                >
                                    {secondNavItem}
                                </NavLink>
                            </NavItem>
                            {thirdNavItem ? (
                                <NavItem
                                    className={"col-md-4"}
                                    style={{
                                        borderBottom:
                                            this.state.activeTab === "3" ? "0.0625rem solid" : ""
                                    }}
                                >
                                    <NavLink
                                        className={classnames({
                                            active: this.state.activeTab === "3"
                                        })}
                                        onClick={() => {
                                            this.toggle("3");
                                        }}
                                    >
                                        {thirdNavItem}
                                    </NavLink>
                                </NavItem>
                            ) : null}
                        </Nav>

                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1" style={{height: "75vh"}}>
                                <Row>
                                    <Col md="12">
                                        <div className="scrollbar" id="style-1">
                                            {orderFirstNavContent === true ? (
                                                pendingOrdersArray && pendingOrdersArray.length > 0 ? (
                                                    <div className="force-overflow">
                                                        {pendingOrdersArray.map((data, index) => {
                                                            return (
                                                                <DetailInfoComponent
                                                                    key={index}
                                                                    store={data.store}
                                                                    orderPlacedAt={data.placedAt}
                                                                    orderNo={data.orderNo}
                                                                    orderUUID={data.uuid}
                                                                    consumerF_Name={data.firstName}
                                                                    consumerL_Name={data.lastName}
                                                                    deliveryFee={data.deliveryFee}
                                                                    storeName={data.store ? data.store.name : ""}
                                                                    storeAddress={
                                                                        data.store ? data.store.address : ""
                                                                    }
                                                                    orderAmount={data.total}
                                                                    deliveryAddress={data.deliveryAddress}
                                                                    freeRiders={freeRiders}
                                                                    assignOrderManually={assignOrderManually}
                                                                    getPendingOrders={getPendingOrders}
                                                                />
                                                            );
                                                        })}
                                                    </div>
                                                ) : null
                                            ) : (
                                                <div className="force-overflow">
                                                    {activeFleet && activeFleet.length > 0 ? (
                                                        <div>
                                                            {activeFleet.map((data, i) => {
                                                                return (
                                                                    <InfoComponent
                                                                        key={i}
                                                                        onClick={() =>
                                                                            this.onRiderClick(
                                                                                "activeFleet",
                                                                                data.uuidFleet,
                                                                                data
                                                                            )
                                                                        }
                                                                        riderEmail={data.email}
                                                                        riderFirstName={capitalize(data.firstName)}
                                                                        riderName={
                                                                            capitalize(data.firstName) +
                                                                            " " +
                                                                            capitalize(data.lastName)
                                                                        }
                                                                        riderPhone={data.phone}
                                                                    />
                                                                );
                                                            })}
                                                        </div>
                                                    ) : null}
                                                </div>
                                            )}
                                        </div>
                                    </Col>
                                </Row>
                            </TabPane>

                            <TabPane tabId="2" style={{height: "75vh"}}>
                                <Row>
                                    <Col sm={"12"}>
                                        <div className="scrollbar" id="style-1">
                                            {orderSecondNavContent === true ? (
                                                busyFleet && busyFleet.length > 0 ? (
                                                    <div className="force-overflow">
                                                        {busyFleet.map((data, i) => {
                                                            return (
                                                                <DetailInfoComponent
                                                                    key={i}
                                                                    showCircle={true}
                                                                    onClick={() =>
                                                                        this.onRiderClick(
                                                                            "busyFleet",
                                                                            data.uuidFleet,
                                                                            data
                                                                        )
                                                                    }
                                                                    simpleData={true}
                                                                    riderName={capitalize(data.firstName)}
                                                                    startTime={
                                                                        data.assignment &&
                                                                        data.assignment.payload &&
                                                                        data.assignment.payload.placedAt
                                                                    }
                                                                    riderPhone={
                                                                        data.assignment &&
                                                                        data.assignment.payload &&
                                                                        data.assignment.payload.phone
                                                                    }
                                                                    address={
                                                                        data.assignment &&
                                                                        data.assignment.payload &&
                                                                        data.assignment.payload.deliveryAddress
                                                                    }
                                                                />
                                                            );
                                                        })}
                                                    </div>
                                                ) : null
                                            ) : (
                                                <div className="force-overflow">
                                                    {busyFleet && busyFleet.length > 0 ? (
                                                        <div>
                                                            {busyFleet.map((data, i) => {
                                                                return (
                                                                    <InfoComponent
                                                                        key={i}
                                                                        onClick={() =>
                                                                            this.onRiderClick(
                                                                                "busyFleet",
                                                                                data.uuidFleet,
                                                                                data
                                                                            )
                                                                        }
                                                                        riderEmail={data.email}
                                                                        riderFirstName={capitalize(data.firstName)}
                                                                        riderName={
                                                                            capitalize(data.firstName) +
                                                                            " " +
                                                                            capitalize(data.lastName)
                                                                        }
                                                                        riderPhone={data.phone}
                                                                    />
                                                                );
                                                            })}
                                                        </div>
                                                    ) : null}
                                                </div>
                                            )}
                                        </div>
                                    </Col>
                                </Row>
                            </TabPane>
                            {thirdNavItem ? (
                                <TabPane tabId="3" style={{height: "75vh"}}>
                                    <Row>
                                        <Col md={"12"}>
                                            {offlineFleet && offlineFleet.length > 0 ? (
                                                <div>
                                                    {offlineFleet.map((data, i) => {
                                                        return (
                                                            <InfoComponent
                                                                key={i}
                                                                riderEmail={data.email}
                                                                riderFirstName={capitalize(data.firstName)}
                                                                riderName={
                                                                    capitalize(data.firstName) +
                                                                    " " +
                                                                    capitalize(data.lastName)
                                                                }
                                                                riderPhone={data.phone}
                                                            />
                                                        );
                                                    })}
                                                </div>
                                            ) : null}
                                        </Col>
                                    </Row>
                                </TabPane>
                            ) : null}
                        </TabContent>
                    </>
                )}
            </div>
        );
    }
}

const connectedComponent = connect(null, {setMapData})(TabComponent);
export default withRouter(connectedComponent);
