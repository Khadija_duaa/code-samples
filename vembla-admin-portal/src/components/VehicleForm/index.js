import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardTitle,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
} from 'reactstrap';
import {connect} from "react-redux";
import {saveVehicle, setVehicleData, resetRedux} from "../../store/shopper/shopper-actions";
import {withRouter} from "react-router-dom";
import Loader from '../../components/Loader/index';

class VehicleForm extends Component {

    state = {
        formName: "Add New Vehicle",
        title: '',
        type: null,
        loadCapacity: '',
        weightLimit: '',
        weightUnit: '',
        volumeLimit: '',
        hasCoolingBox: false,
        hasHeatingBox: false
    };

    componentDidMount() {
        if (this.props.vehicleData) {
            let {title, type, loadCapacity, weightLimit, weightUnit, volumeLimit, hasCoolingBox, hasHeatingBox} = this.props.vehicleData;
            this.setState({
                title,
                type,
                loadCapacity,
                weightLimit,
                weightUnit,
                volumeLimit,
                hasCoolingBox,
                hasHeatingBox,
                formName: `Update Vehicle ${title}`
            });
        }
    }

    prepareFields = () => {
        const inputFields = [];
        inputFields.push({
            name: 'title',
            placeholder: 'Vehicle Title',
            label: 'Vehicle Title',
            value: this.state.title
        });
        inputFields.push({
            name: 'type',
            placeholder: 'Type',
            label: 'Car Type',
            value: this.state.type
        });
        inputFields.push({
            name: 'loadCapacity',
            placeholder: 'Capacity',
            label: 'Load Capacity',
            value: this.state.loadCapacity
        });
        inputFields.push({
            name: 'weightLimit',
            placeholder: 'Weight Limit',
            label: 'Weight Limit',
            value: this.state.weightLimit
        });
        inputFields.push({
            name: 'weightUnit',
            placeholder: 'Weight Unit',
            label: 'Weight Unit',
            value: this.state.weightUnit
        });
        inputFields.push({
            name: 'volumeLimit',
            placeholder: 'Volume Limit',
            label: 'Volume Limit',
            value: this.state.volumeLimit
        });
        return inputFields;
    };

    onCancelBtnClick = (parentFun) => {
        parentFun(false);
        this.props.setVehicleData(null);
    };

    onInputChange = (e, parseIntValue) => {
        let state = {...this.state};
        let newValue;
        if (parseIntValue) {
            newValue = parseInt(e.target.value);
            if (e.target.value !== "") {
                state[e.target.name] = newValue;
            } else {
                state[e.target.name] = '';
            }
            this.setState(state);
        } else {
            state[e.target.name] = e.target.value;
            this.setState(state);
        }
    };

    onVehicleSave = (parentFun) => {
        const {title, type, loadCapacity, weightLimit, weightUnit, volumeLimit, hasCoolingBox, hasHeatingBox} = this.state;
        this.props.saveVehicle({
            title,
            type,
            loadCapacity,
            weightLimit,
            weightUnit,
            volumeLimit,
            hasCoolingBox,
            hasHeatingBox
        });
        parentFun(false);
        this.props.setVehicleData(null);
    };

    handleCoolingChecked = () => {
        this.setState({hasCoolingBox: !this.state.hasCoolingBox});
    };

    handleHeatingChecked = () => {
        this.setState({hasHeatingBox: !this.state.hasHeatingBox});
    };

    render() {
        const fields = this.prepareFields();
        return (
            <div>
                {
                    this.props.processing ?
                        <div style={{marginBottom: '200px'}}>
                            <Loader/>
                        </div> :
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h4">{this.state.formName}</CardTitle>
                            </CardHeader>

                            <CardBody>
                                <Form>
                                    <Row>
                                        {
                                            fields.map(field => {
                                                let parseIntValue;
                                                if (field.label === "Load Capacity" || field.label === "Weight Limit" || field.label === "Volume Limit") {
                                                    parseIntValue = true
                                                }
                                                else {
                                                    parseIntValue = false
                                                }
                                                return (
                                                    <Col className="pr-md-1" md="6">
                                                        <FormGroup>
                                                            <label>{field.label}</label>
                                                            <Input
                                                                value={field.value}
                                                                onChange={(e) => this.onInputChange(e, parseIntValue)}
                                                                name={field.name}
                                                                placeholder={field.placeholder}
                                                                type="text"
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                )
                                            })
                                        }
                                    </Row>
                                    <Row>
                                        <Col md={6}>
                                            <Row>
                                                <Col md={6}>
                                                    <Row>
                                                        <Col md={7}>
                                                            <p>Cooling Box Available</p>
                                                        </Col>
                                                        <Col md={4} style={{marginTop: '1%'}}>
                                                            <input type="checkbox"
                                                                   checked={this.state.hasCoolingBox ? this.state.hasCoolingBox : ''}
                                                                   value={this.state.hasCoolingBox}
                                                                   onChange={this.handleCoolingChecked}/>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                                <Col md={6}>
                                                    <Row>
                                                        <Col md={7}>
                                                            <p>Heating Box Available</p>
                                                        </Col>
                                                        <Col md={4} style={{marginTop: '1%'}}>
                                                            <input type="checkbox"
                                                                   checked={this.state.hasHeatingBox ? this.state.hasHeatingBox : ''}
                                                                   value={this.state.hasHeatingBox}
                                                                   onChange={this.handleHeatingChecked}/>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>

                                </Form>
                            </CardBody>

                            <CardFooter style={{textAlign: 'center'}}>
                                <Button className="btn-fill" color="secondary"
                                        type="submit"
                                        onClick={() => this.onCancelBtnClick(this.props.showForm)}
                                >
                                    Cancel
                                </Button>
                                <Button className="btn-fill" color="primary"
                                        onClick={() => this.onVehicleSave(this.props.showForm)}
                                        type="submit">
                                    Save
                                </Button>
                            </CardFooter>
                        </Card>
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.shopper.processing,
        vehicleData: state.shopper.vehicleData
    }
};

const connectedComponent = connect(mapStateToProps, {saveVehicle, setVehicleData, resetRedux})(VehicleForm);
export default withRouter(connectedComponent);

