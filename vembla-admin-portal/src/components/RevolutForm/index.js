import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Form, Button, Label } from 'reactstrap';

import {
  getRevolutAccounts,
  saveRevolutInfo
} from '../../store/shopper/shopper-actions';

import CustomDropDownFormGroup from '../CustomComponents/CustomDropDownFormGroup';

const RevolutForm = ({
  accounts,
  uuidFleet,
  getRevolutAccounts,
  saveRevolutInfo
}) => {
  const [selectedAccount, setSelectedAccount] = useState('');

  useEffect(() => {
    getRevolutAccounts();
  }, []);

  const onChange = (name = 'accountId', val) => {
    setSelectedAccount(val);
  };

  const onSave = () => {
    if (uuidFleet && selectedAccount) {
      saveRevolutInfo(uuidFleet, selectedAccount);
    }
  };

  return (
    <>
      <Row style={{ textAlign: 'center' }}>
        <Col className="pr-md-1" md="6">
          <CustomDropDownFormGroup
            name="accountId"
            dataField="id"
            dispField="name"
            btnText={'Select Revolut Account Name'}
            data={accounts}
            onChange={onChange}
          />
        </Col>
      </Row>
      <Row>
        <Col md={{ size: 4, offset: 8 }}>
          <Button
              disabled={!selectedAccount}
              onClick={onSave}>Next</Button>
        </Col>
      </Row>
    </>
  );
};

RevolutForm.propTypes = {
  uuidFleet: PropTypes.string.isRequired,
  accounts: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  accounts: state.shopper.revolutAccounts
});

export default connect(mapStateToProps, {
  getRevolutAccounts,
  saveRevolutInfo
})(RevolutForm);
