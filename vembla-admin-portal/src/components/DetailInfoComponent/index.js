import React, {Component} from "react";
import {Row, Col, Button} from "reactstrap";

import {
    timeSplitter,
    capitalize,
    getDateFromISO
} from "../../utils/common-utils";

import PendingOrderDetails from "../PendingOrderDetails";

class DetailInfoComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openModal: false
        };
    }

    toggle = () => this.setState({openModal: !this.state.openModal});

    render() {
        const {
            store,
            orderPlacedAt,
            orderNo,
            orderUUID,
            consumerF_Name,
            consumerL_Name,
            deliveryFee,
            storeName,
            storeAddress,
            orderAmount,
            deliveryAddress,
            address,
            showCircle,
            riderName,
            riderPhone,
            onClick,
            simpleData,
            startTime,
            freeRiders,
            assignOrderManually,
            getPendingOrders
        } = this.props;

        const {openModal} = this.state;

        const prepareData = () => {
            const data = [];
            data.push({
                name: "Order No#",
                value: orderNo
            });
            data.push({
                name: "Date:",
                value: getDateFromISO(orderPlacedAt)
            });
            data.push({
                name: "Time:",
                value: timeSplitter(orderPlacedAt)
            });
            data.push({
                name: "Consumer Name",
                value: capitalize(consumerF_Name) + " " + capitalize(consumerL_Name)
            });
            data.push({
                name: "Order Amount",
                value: orderAmount
            });
            data.push({
                name: "Delivery Fee",
                value: deliveryFee
            });
            data.push({
                name: "Delivery Address",
                value: deliveryAddress
            });
            data.push({
                name: "Store Name",
                value: storeName
            });
            data.push({
                name: "Store Address",
                value: storeAddress
            });

            if (riderName) {
                data.push({
                    name: "Rider Name",
                    value: riderName
                });
                data.push({
                    name: "Rider Phone",
                    value: riderPhone
                });
            }
            return data;
        };

        const rowData = prepareData();
        return (
            <div
                className="main-container"
                onClick={onClick}
                style={{cursor: "pointer"}}
            >
                <Row>
                    {showCircle ? (
                        <Col md="3" style={{textAlign: "center", top: "17px"}}>
                            <div className={"name-div"}>
                                <div className={"text-div"}>
                                    {riderName && riderName.charAt(0)}
                                </div>
                            </div>
                            <span className={"text-color-gray"}>{riderName}</span>
                        </Col>
                    ) : null}

                    <Col
                        md={{
                            size: showCircle ? "9" : "12",
                            offset: showCircle ? "0" : "1"
                        }}
                    >
                        <Row>
                            {showCircle ? (
                                <Col md={2} style={{margin: "5px 0px 30px 0px"}}>
                                    <div className={"small-circle-blue"}>
                                        <div className={"small-circle-text"}>P</div>
                                    </div>
                                    <div className={"border"}></div>
                                    <div className={"small-circle-transparent"}>
                                        <div className={"small-circle-text-trans"}>D</div>
                                    </div>
                                </Col>
                            ) : null}
                            {simpleData === true ? (
                                <Col md={10}>
                                    <Row>
                                        <Col md={10}>
                                            <Row className={"text-color-gray"}>
                                                {timeSplitter(startTime)}
                                            </Row>
                                            <Row className={"text-color-gray"}>{riderPhone}</Row>
                                            <Row
                                                className={"text-color-gray"}
                                                style={{marginTop: "24px"}}
                                            >
                                                {address}
                                            </Row>
                                        </Col>
                                        <Col md={2} style={{marginTop: "20px"}}>
                                            <i
                                                className="tim-icons icon-minimal-right"
                                                style={{color: "white"}}
                                            />
                                        </Col>
                                    </Row>
                                </Col>
                            ) : (
                                <Col md={10} onClick={() => this.setState({openModal: true})}>
                                    <PendingOrderDetails
                                        openModal={openModal}
                                        toggle={this.toggle}
                                        store={store}
                                        orderPlacedAt={orderPlacedAt}
                                        orderNo={orderNo}
                                        orderUUID={orderUUID}
                                        consumerF_Name={consumerF_Name}
                                        consumerL_Name={consumerL_Name}
                                        deliveryFee={deliveryFee}
                                        orderAmount={orderAmount}
                                        deliveryAddress={deliveryAddress}
                                        freeRiders={freeRiders}
                                        assignOrderManually={assignOrderManually}
                                        getPendingOrders={getPendingOrders}
                                    />

                                    {rowData.map((rowData, i) => {
                                        return (
                                            <Row key={i}>
                                                <Col md="6">
                          <span style={{color: "aliceblue"}}>
                            {rowData.name}
                          </span>
                                                </Col>
                                                <Col md="6">
                          <span className={"text-color-gray"}>
                            {rowData.value}
                          </span>
                                                </Col>
                                            </Row>
                                        );
                                    })}
                                </Col>
                            )}
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default DetailInfoComponent;
