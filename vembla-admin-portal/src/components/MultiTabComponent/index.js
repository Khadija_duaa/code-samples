import React from 'react';
import {connect} from 'react-redux';
import classnames from 'classnames';
import {withRouter} from 'react-router-dom';
import {
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Row,
    Col
} from 'reactstrap';

import {
    setFleetErrorMsg
    // saveRevolutInfo,
    // saveGigaPayInfo
    // approvePendingUser,
    // saveShopperProgram,
    // saveVehicle
} from '../../store/shopper/shopper-actions';

// import ApprovedShopper from '../../components/ApproveShopper/index_copy';
import ApproveShopper from '../../components/ApproveShopper/index';
import GigaPayForm from '../GigaPayForm';
import RevolutForm from '../RevolutForm';
import VehicleNProgramForm from '../../components/VehicleNProgramForm';

class MultiTabComponent extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1',

            showActivateTab: true,
            fleetErrorMsg: null,
            showSecondTab: false,
            showThirdTab: false,
            showFourthTab: false
        };
    }

    componentDidUpdate(prevProps) {
        const {isUserApproved, isUserVehNProgSet, isUserRevolutInfo} = this.props;

        // if (isUserApproved !== prevProps.isUserApproved && isUserApproved) {
        //     this.setState({activeTab: '2', showSecondTab: true, showActivateTab: false});
        // }

        if (isUserVehNProgSet !== prevProps.isUserVehNProgSet && isUserVehNProgSet) {
            this.setState({activeTab: '2', showSecondTab: true, showActivateTab: false});
        }

        if (isUserRevolutInfo !== prevProps.isUserRevolutInfo && isUserRevolutInfo) {
            this.setState({activeTab: '3', showThirdTab: true, showSecondTab: false, showActivateTab: false});
        }

        // if (isUserRevolutInfo !== prevProps.isUserRevolutInfo && isUserRevolutInfo) {
        //     this.setState({activeTab: '4', showFourthTab: true, showSecondTab: false, showThirdTab: false, showActivateTab: false});
        // }
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    // onRevolutAssignIdClick = accountId => {
    //   if (this.props.data && this.props.data.shopperData && accountId) {
    //     this.props.saveRevolutInfo(
    //       this.props.data.shopperData.userUUID,
    //       accountId
    //     );
    //   }
    // };

    // onGigapayAssignInfoClick = (personalNumber, cellNumber) => {
    //   let gigaPayInfo = {};
    //   if (personalNumber && cellNumber) {
    //     gigaPayInfo = {
    //       personalNumber: personalNumber,
    //       cellNumber: cellNumber
    //     };
    //   }
    //   if (this.props.data && this.props.data.shopperData) {
    //     this.props.saveGigaPayInfo(
    //       this.props.data.shopperData.userUUID,
    //       gigaPayInfo
    //     );
    //   }
    // };

    // Separate Component for Approve Shopper: ApproveShopper

    // onParentApproveBtnClick = (uuid, number, size, formData, value) => {
    //   console.log(
    //     "ON APPROVING PENDING SHOPPER FROM MULTI TAB::::",
    //     uuid,
    //     number,
    //     size,
    //     formData,
    //     value
    //   );
    //   // this.props.approvePendingUser(uuid, number, size, formData, () => {
    //   //   this.setState({ fleetErrorMsg: this.props.fleetErrorMsg });
    //   //   if (this.state.fleetErrorMsg === null) {
    //   //     this.setState({ showActivateTab: value });
    //   //   } else {
    //   //     this.props.setFleetErrorMsg(null);
    //   //   }
    //   // });
    //   // this.setState({ activeTab: "2" });
    // };

    // Separate Component For Vehicle and Program Info VehicleNProgramForm

    // onVehicleAssignBtnClick = (
    //   name,
    //   color,
    //   registrationNumber,
    //   vehicleTypeId,
    //   hasCoolingBox,
    //   hasHeatingBox,
    //   shopperDetailDataObj
    // ) => {
    //   if (this.props.data && this.props.data.shopperData) {
    //     let userId = this.props.data.shopperData.userUUID;
    //     this.props.saveVehicle(
    //       {
    //         userId,
    //         name,
    //         color,
    //         registrationNumber,
    //         vehicleTypeId,
    //         hasCoolingBox,
    //         hasHeatingBox
    //       },
    //       shopperDetailDataObj,
    //       () => {
    //         this.setState({ fleetErrorMsg: this.props.fleetErrorMsg });
    //         if (this.state.fleetErrorMsg === null) {
    //           this.setState({ showMultiTabs: true });
    //         } else {
    //           this.props.setFleetErrorMsg(null);
    //         }
    //       }
    //     );
    //   }
    // };

    // Separate Component Incorporated with Vehicle Info in: VehicleNProgramForm

    // onProgramAssignBtnClick = programUUID => {
    //   if (this.props.data && this.props.data.shopperData && programUUID) {
    //     this.props.saveShopperProgram(
    //       programUUID,
    //       this.props.data.shopperData.userUUID
    //     );
    //   }
    // };

    render() {
        const {
            shopperData,
            shopperDetailDataObj
            // parentState,
            // dispatchFun,
            // programsArray,
            // vehicleTypeArray,
            // revolutAccounts
        } = this.props.data;
        const {userUUID} = shopperData;
        return (
            <div>
                <Nav tabs
                     style={{cursor: 'auto'}}
                >
                    {
                        this.state.showActivateTab === true ? (
                            <NavItem
                                className={'col-md-4'}
                                style={{
                                    borderBottom: this.state.activeTab === '1' ? '0.0625rem solid' : '',
                                    padding: 'none',
                                    cursor: this.state.activeTab === '1' ? 'pointer' : 'auto'
                                }}
                            >
                                <NavLink
                                    className={classnames({active: this.state.activeTab === '1'})}
                                    onClick={() => {
                                        this.toggle('1');
                                    }}
                                >
                                    Step 1 of 3
                                </NavLink>
                            </NavItem>
                        ) : null
                    }
                    {
                        this.state.showSecondTab === true ?
                            <NavItem
                                className={'col-md-3'}
                                style={{
                                    borderBottom: this.state.activeTab === '2' ? '0.0625rem solid' : '',
                                    padding: 'none',
                                    cursor: this.state.activeTab === '2' ? 'pointer' : 'auto'
                                }}
                            >
                                <NavLink
                                    className={classnames({active: this.state.activeTab === '2'})}
                                    onClick={() => {
                                        this.toggle('2');
                                    }}
                                >
                                    Step 2 of 3
                                </NavLink>
                            </NavItem> : null
                    }

                    {
                        this.state.showThirdTab === true ?
                            <NavItem
                                className={'col-md-3'}
                                style={{
                                    borderBottom: this.state.activeTab === '3' ? '0.0625rem solid' : '',
                                    padding: 'none',
                                    cursor: this.state.activeTab === '3' ? 'pointer' : 'auto'
                                }}
                            >
                                <NavLink
                                    className={classnames({
                                        active: this.state.activeTab === '3'
                                    })}
                                    onClick={() => {
                                        this.toggle('3');
                                    }}
                                >
                                    Step 3 of 3
                                </NavLink>
                            </NavItem> : null
                    }

                   {/* {
                        this.state.showFourthTab === true ?
                            <NavItem
                                className={'col-md-3'}
                                style={{
                                    borderBottom: this.state.activeTab === '4' ? '0.0625rem solid' : '',
                                    padding: 'none',
                                    cursor: this.state.activeTab === '4' ? 'pointer' : 'auto'
                                }}
                            >
                                <NavLink
                                    className={classnames({
                                        active: this.state.activeTab === '4'
                                    })}
                                    onClick={() => {
                                        this.toggle('4');
                                    }}
                                >
                                    Step 4 of 4
                                </NavLink>
                            </NavItem> : null
                    }*/}
                </Nav>

                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <Row>
                            <Col md="12">

                                <VehicleNProgramForm shopperData={shopperDetailDataObj}/>

                                {/*<ApproveShopper shopperData={shopperData}/>*/}
                                {/*         <ApprovedShopper
                                    shopperData={shopperData}
                                    parentState={parentState}
                                    onParentApproveBtnClick={this.onParentApproveBtnClick}
                                    title={"Approve the Pending Shopper"}
                                    approveShopperDialogue={true}
                                /> */}
                            </Col>
                        </Row>
                    </TabPane>

                    <TabPane tabId="2">
                        <Row>
                            <Col sm={'12'}>

                                <RevolutForm uuidFleet={userUUID}/>

                                {/*      <ApprovedShopper
                                    vehicleDialogue={true}
                                    vehicleTypeArray={vehicleTypeArray}
                                    shopperDetailDataObj={shopperDetailDataObj}
                                    onVehicleAssignBtnClick={this.onVehicleAssignBtnClick}
                                    title={"Vehicle Information"}
                                /> */}
                            </Col>
                        </Row>
                    </TabPane>

                    <TabPane tabId="3">
                        <Row>
                            <Col md={'12'}>

                                <GigaPayForm uuidFleet={userUUID}/>

                                {/*       <ApprovedShopper
                                    revolutDialogue={true}
                                    onRevolutAssignIdClick={this.onRevolutAssignIdClick}
                                    revolutAccounts={revolutAccounts}
                                    title={'Revolut Information'}
                                /> */}
                            </Col>
                        </Row>
                    </TabPane>

                  {/*  <TabPane tabId="4">
                        <Row>
                            <Col md={'12'}>
                                     <ApprovedShopper
                                    gigapayDialogue={true}
                                    onGigapayAssignInfoClick={this.onGigapayAssignInfoClick}
                                    title={'GigaPay Information'}
                                />
                            </Col>
                        </Row>
                    </TabPane>*/}

                </TabContent>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        fleetErrorMsg: state.shopper.fleetErrorMsg,
        isUserApproved: state.shopper.isApproveUserSuccess,
        isUserVehNProgSet: state.shopper.isSetUserVehNProgSuccess,
        isUserRevolutInfo: state.shopper.isSetUserRevolutInfo
    };
};

const mapDispatchToProps = dispatch => {
    return {
        // saveGigaPayInfo: (uuid, gigaPayInfo) =>
        //   dispatch(saveGigaPayInfo(uuid, gigaPayInfo)),
        // saveRevolutInfo: (uuid, accountInfo) =>
        //   dispatch(saveRevolutInfo(uuid, accountInfo)),
        // saveShopperProgram: (progUUID, userUUID) =>
        //   dispatch(saveShopperProgram(progUUID, userUUID)),
        // approvePendingUser: (uuid, number, size, formData, cb) =>
        //   dispatch(approvePendingUser(uuid, number, size, formData, cb)),
        // saveVehicle: (vehicleNprogObj, userObj, cb) =>
        //   dispatch(saveVehicle(vehicleNprogObj, userObj, cb)),
        setFleetErrorMsg: msg => dispatch(setFleetErrorMsg(msg))
    };
};

const connectedComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)(MultiTabComponent);
export default withRouter(connectedComponent);
