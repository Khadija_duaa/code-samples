import React from 'react';
import './style.css';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

const reactTransition = (props) => {
    return (
        <ReactCSSTransitionGroup
            transitionName="smoothTransition"
            transitionAppear={true}
            transitionAppearTimeout={10000}
            transitionEnter={false}
            transitionLeave={false}
        >
            {props.children}
        </ReactCSSTransitionGroup>
    );
};

export default reactTransition;