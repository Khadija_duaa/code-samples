import React, {Component} from 'react'
import {withGoogleMap, GoogleMap, withScriptjs, InfoWindow, Marker} from "react-google-maps";
import Autocomplete from 'react-google-autocomplete';
import Geocode from "react-geocode";
import {Alert} from "reactstrap";
import {connect} from "react-redux";

Geocode.setApiKey("AIzaSyAmrkCGdtCCWHfzueCST6TLSllPA2zY7iM");
Geocode.enableDebug();

class MapWithAutoCompleteSearch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            address: '',
            alertVisible: false,
            mapPosition: {
                lat: this.props.center.lat,
                lng: this.props.center.lng
            },
            markerPosition: {
                lat: this.props.center.lat,
                lng: this.props.center.lng
            }
        };
        this.onDismiss = this.onDismiss.bind(this);
    }

    componentDidMount() {
        Geocode.fromLatLng(this.state.mapPosition.lat, this.state.mapPosition.lng).then(
            response => {
                const address = response.results[0].formatted_address;
                this.setState({
                    address: (address) ? address : '',
                })
            },
            error => {
                console.error(error);
            }
        );
    };

    shouldComponentUpdate(nextProps, nextState) {
        if (
            this.state.markerPosition.lat !== this.props.center.lat ||
            this.state.address !== nextState.address ||
            this.state.state !== nextState.state
        ) {
            return true
        } else if (this.props.center.lat === nextProps.center.lat) {
            return false
        }
    }

    onDismiss () {
        this.setState({alertVisible: false});
        this.forceUpdate();
    };

    onPlaceSelected = (place, state) => {

        let address, latValue, lngValue;

        if (place && place.formatted_address) {
            address = place.formatted_address;
        } else {
            address = "";
            this.setState({alertVisible: true});
            this.forceUpdate();
        }

        if (place && place.geometry) {
            latValue = place.geometry.location.lat();
            lngValue = place.geometry.location.lng();
        } else {
            latValue = state.mapPosition.lat;
            lngValue = state.mapPosition.lng;
        }
        this.setState({
            address: (address) ? address : '',
            markerPosition: {
                lat: latValue,
                lng: lngValue
            },
            mapPosition: {
                lat: latValue,
                lng: lngValue
            },
        });
        this.props.setLocation(this.state.markerPosition)
    };

    onMarkerDragEnd = (event) => {
        let newLat = event.latLng.lat(), newLng = event.latLng.lng();
        this.setState({
            markerPosition: {
                lat: newLat,
                lng: newLng
            },
            mapPosition: {
                lat: newLat,
                lng: newLng
            },
        });
        this.props.setLocation(this.state.markerPosition);
        Geocode.fromLatLng(newLat, newLng).then(
            response => {
                const address = response.results[0].formatted_address;
                this.setState({
                    address: (address) ? address : ''
                })
            },
            error => {
                console.error(error);
            }
        );

    };

    render() {

        if(this.state.alertVisible === true) {
            return(
                <Alert color="info" isOpen={this.state.alertVisible} toggle={this.onDismiss}>
                    {this.props.mapCheckTitle}
                </Alert>
            )
        }

        const AsyncMap = withScriptjs(
            withGoogleMap(
                props => (
                    <GoogleMap google={this.props.google}
                               defaultZoom={this.props.zoom}
                               defaultCenter={{lat: this.state.mapPosition.lat, lng: this.state.mapPosition.lng}}
                    >
                        {/* For Auto complete Search Box */}
                        <Autocomplete
                            style={{
                                width: '100%',
                                height: '40px',
                                paddingLeft: '16px',
                                marginTop: '2px',
                                marginBottom: '100px'
                            }}
                            onPlaceSelected={(e) => this.onPlaceSelected(e, this.state)}

                            types={['(regions)']}
                        />
                        {/*Marker*/}
                        <Marker google={this.props.google}
                                name={'Dolores park'}
                                draggable={true}
                                onDragEnd={this.onMarkerDragEnd}
                                position={{lat: this.state.markerPosition.lat, lng: this.state.markerPosition.lng}}
                        />
                        <Marker/>
                        {/* InfoWindow on top of marker */}
                        {
                            this.state.address === "" ? null :
                                <InfoWindow
                                    onClose={this.onInfoWindowClose}
                                    position={{
                                        lat: (this.state.markerPosition.lat + 0.0018),
                                        lng: this.state.markerPosition.lng
                                    }}
                                >
                                    <div>
                                        <span style={{padding: 0, margin: 0}}>{this.state.address}</span>
                                    </div>
                                </InfoWindow>
                        }

                    </GoogleMap>
                )
            )
        );

        let map;
        if (this.props.center.lat !== undefined) {
            map = <div style={{width: '100%'}}>

                <AsyncMap
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmrkCGdtCCWHfzueCST6TLSllPA2zY7iM&libraries=places"
                    loadingElement={
                        <div style={{height: `100%`}}/>
                    }
                    containerElement={
                        <div style={{height: this.props.height}}/>
                    }
                    mapElement={
                        <div style={{height: `100%`}}/>
                    }
                />
            </div>
        } else {
            map = <div style={{height: this.props.height}}/>
        }
        return (map)
    }
}

const mapStateToProps = (state) => {
    return {
        mapCheckTitle: state.multilingual.activeLanguageData.mapCheckTitle
    };
};

export default connect(mapStateToProps)(MapWithAutoCompleteSearch);
