import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NotificationManager} from 'react-notifications';
import {withRouter} from 'react-router-dom';
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    Col,
    Row,
    Table,
    Button,
    Container,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';

import {getTdWidth, capitalize} from '../../utils/common-utils';
import {
    // approvePendingUser,
    activateApprovedShopper,
    deactivateApprovedShopper,
    setFleetAStaffStoreShopper,
    saveRevolutInfo,
    saveGigaPayInfo,
    getApprovedFleetDetail
    // getAllVehicles
} from '../../store/shopper/shopper-actions';

import CustomDropDownFormGroup from '../CustomComponents/CustomDropDownFormGroup';
import CustomFormModal from '../CustomComponents/CustomFormModal';
import GetModal from '../../components/Modal';
import GigaPayForm from '../GigaPayForm';
import RevolutForm from '../RevolutForm';
import ShopperCommissions from '../ShopperCommissions';

let options = [];

// const getTableRow = (title, dropdownFun, sortingDropdown) => {
//   return (
//     <Row>
//       <Col md={12} style={{ display: 'flex' }}>
//         <span style={{ marginTop: '2px' }}>{title}</span>
//         {sortingDropdown ? dropdownFun : null}
//       </Col>
//     </Row>
//   );
// };

class SimpleTable extends Component {
    state = {
        modalOpen: false,
        selectedShopperUUID: '',
        selectedShopperName: '',
        showGigaPayModal: false,
        showShopperCommission: false,
        showRevolutModal: false,
        deactivateModalOpen: false,
        toggleClick: false,
        approveBtnClick: false,
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        countryCode: '',
        userInfo: null,
        currentUserData: null,
        active: false,
        checkedOn: false,
        program: '',
        programDropdownOpen: false,
        programUUID: '',
        sortVal: '',
        id: '',
        modalVisible: false,
        modalContent: {},
        headerStyle: '',
        settingsToggleClick: false
    };

    getActions = (props, uuid, index, data) => {
        return (
            <Container>
                <Row>
                    {props.showApproveButton === true ? (
                        <Col xs={12} xl={'auto'} style={{marginLeft: 'inherit'}}>
                            <Button
                                type="submit"
                                size="sm"
                                color="primary"
                                onClick={() => {
                                    this.onApproveBtnClick(uuid, data, props);
                                }}
                            >
                                Approve
                            </Button>
                        </Col>
                    ) : null}
                    {props.showToggleButton === true ? (
                        <Col xs={12} xl={'auto'} style={{marginLeft: 'inherit'}}>
                            <label className="switch">
                                <input
                                    type="checkbox"
                                    checked={data.active}
                                    onChange={() => {
                                        this.onToggleClick(uuid, data);
                                    }}
                                />
                                <span className="slider round"/>
                            </label>
                        </Col>
                    ) : null}
                </Row>
            </Container>
        );
    };

    onToggleClick = (uuid, data) => {
        if (data.active) {
            this.openDeactivateModal(uuid, data);
            this.setState({toggleClick: false});
        } else {
            this.openToggleModal(uuid, data);
            this.setState({toggleClick: true});
        }
    };

    getModalDeactivate = () => {
        return (
            <Modal
                isOpen={this.state.deactivateModalOpen}
                toggle={this.openDeactivateModal}
                className={'modal-danger ' + this.props.className}
            >
                <ModalHeader toggle={this.openDeactivateModal}>
                    Are you sure ?
                </ModalHeader>

                <ModalBody>Are you sure you want to de-activate this user ?</ModalBody>

                <ModalFooter>
                    <Button
                        color="secondary"
                        onClick={() => this.onDeactivate(this.state)}
                    >
                        Deactivate
                    </Button>
                </ModalFooter>
            </Modal>
        );
    };

    openDeactivateModal = (uuid, data) => {
        this.setState({
            deactivateModalOpen: !this.state.deactivateModalOpen,
            userInfo: uuid,
            currentUserData: data
        });
    };

    onDeactivate = state => {
        if (state.currentUserData) {
            state.currentUserData.active = false;
        }
        this.setState({deactivateModalOpen: !this.state.deactivateModalOpen});
        this.props.deactivateApprovedShopper(
            state.userInfo,
            this.props.currentPage,
            this.props.pageSize
        );
    };

    getModalContent = () => {
        return (
            <Modal
                isOpen={this.state.modalOpen}
                toggle={this.openToggleModal}
                className={'modal-danger ' + this.props.className}
            >
                <ModalHeader toggle={this.openToggleModal}>Are you sure ?</ModalHeader>

                <ModalBody>Are you sure you want to activate this user ?</ModalBody>

                <ModalFooter>
                    <Button color="secondary" onClick={() => this.onToggle(this.state)}>
                        Activate
                    </Button>
                </ModalFooter>
            </Modal>
        );
    };

    openToggleModal = (uuid, data) => {
        this.setState({
            modalOpen: !this.state.modalOpen,
            userInfo: uuid,
            currentUserData: data
        });
    };

    onToggle = state => {
        if (state.currentUserData) {
            state.currentUserData.active = true;
        }
        this.setState({modalOpen: !this.state.modalOpen});
        this.props.activateApprovedShopper(
            state.userInfo,
            this.props.currentPage,
            this.props.pageSize
        );
    };

    onApproveBtnClick = (uuid, data, props) => {
        const dataObj = {
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            phone: data.phone,
            countryCode: data.countryCode,
            userUUID: uuid
        };

        if (data) {
            this.setState({
                modalVisible: true,
                modalContent: {
                    headerText: `Approve Shopper : ${capitalize(
                        data.firstName
                    )} ${capitalize(data.lastName)}`,
                    footerShow: false,
                    headingTextStyle: true,
                    multiTabs: true,
                    shopperData: dataObj,
                    parentState: props.state,
                    // dispatchFun: this.props.approvePendingUser,
                    shopperDetailDataObj: data,
                    // programsArray: props.programsArray, // Redundant - Remove
                    // vehicleTypeArray: props.vehicleTypeArray, // Redundant - Remove
                    revolutAccounts: props.revolutAccounts
                },
                modalToggle: this.openSettingsModal,
                headerStyle: {backgroundColor: '#1e1e2e'},
                modalBodyStyle: {backgroundColor: '#1e1e2e'},
                userInfo: uuid
            });
        }
    };

    dropdownChangeValue = (e, sortFun, sortField) => {
        this.setState({sortVal: e.currentTarget.textContent}, () =>
            sortFun(this.state.sortVal, sortField)
        );
    };

    openSettingsModal = () => {
        this.setState({modalVisible: !this.state.modalVisible});
    };

    modalBtnClick = storeData => {
        let shopperUUID = this.state.shopperUUID;
        let idObj = {
            uuidFleet: shopperUUID,
            storeId: storeData && storeData.value,
            storeName: storeData && storeData.label
        };
        if (storeData !== null) {
            this.props.setFleetAStaffStoreShopper(idObj);
        } else {
            NotificationManager.error('Kindly Select A Store to Save!', '', 3000);
        }
    };

    getContentArray = staffStoreArr => {
        if (staffStoreArr) {
            for (let i = 0; i < staffStoreArr.length; i++) {
                let obj = {};
                obj.value = staffStoreArr[i].uuid;
                obj.label = staffStoreArr[i].name;
                options.push(obj);
            }
        }
    };

    settingsDropdownChangeValue = () => {
        if (this.props.staffedStoresArray) {
            this.getContentArray(this.props.staffedStoresArray);
        }
        this.setState({
            modalVisible: true,
            modalContent: {
                headerText: 'Assign Runner to Specific Staff Store!',
                bodyText: 'Kindly select the store',
                dropdown: true,
                footerBtnText: 'Save',
                buttonClick: this.modalBtnClick,
                contentArray: options
            },
            modalToggle: this.openSettingsModal,
            modalBodyStyle: {padding: '30px 24px 50px 24px'}
        });
    };

    // onRevolutBtnClick = accountId => {
    //   let shopperUUID = this.state.shopperUUID;
    //   if (shopperUUID && accountId) {
    //     this.props.saveRevolutInfo(shopperUUID, accountId);
    //   }
    // };

    // onGigapayBtnClick = (personalNumber, cellNumber) => {
    //   let gigaPayInfo = {};
    //   let shopperUUID = this.state.shopperUUID;
    //   if (personalNumber && cellNumber) {
    //     gigaPayInfo = {
    //       personalNumber: personalNumber,
    //       cellNumber: cellNumber
    //     };
    //   }
    //   if (shopperUUID) {
    //     this.props.saveGigaPayInfo(shopperUUID, gigaPayInfo);
    //   }
    // };

    // onRevolutChangeInfo = () => {
    //   this.setState({
    //     modalVisible: true,
    //     modalContent: {
    //       headerText: 'Update Revolut Information!',
    //       bodyText: 'Enter Revolut Account ID:',
    //       revolutInfoChange: true,
    //       footerShow: false,
    //       footerBtnText: 'Update',
    //       buttonClick: this.onRevolutBtnClick
    //     },
    //     modalToggle: this.openSettingsModal
    //   });
    // };

    // onGigapayChangeInfo = () => {
    //   this.setState({
    //     modalVisible: true,
    //     modalContent: {
    //       headerText: 'Update GigaPay Information!',
    //       gigapayInfoChange: true,
    //       footerShow: false,
    //       footerBtnText: 'Update',
    //       buttonClick: this.onGigapayBtnClick
    //     },
    //     modalToggle: this.openSettingsModal
    //   });
    // };

    toggle = dropDownStateVal => {
        switch (dropDownStateVal) {
            case 'nameDropdown':
                this.setState(prevState => ({nameDropdown: !prevState.nameDropdown}));
                break;
            case 'idDropdown':
                this.setState(prevState => ({idDropdown: !prevState.idDropdown}));
                break;
            case 'roleDropdown':
                this.setState(prevState => ({roleDropdown: !prevState.roleDropdown}));
                break;
            case 'phoneDropdown':
                this.setState(prevState => ({
                    phoneDropdown: !prevState.phoneDropdown
                }));
                break;
            case 'emailDropdown':
                this.setState(prevState => ({
                    emailDropdown: !prevState.emailDropdown
                }));
                break;
            default: {
                // do nothing
            }
        }
    };

    getDropDown = (dropdownOpen, stateValue, sortField, menuRight) => {
        return (
            <Dropdown
                isOpen={dropdownOpen}
                style={{marginLeft: '15px'}}
                toggle={() => this.toggle(stateValue)}
            >
                <DropdownToggle
                    caret
                    className={'button'}
                    style={{background: 'none', padding: '3px 5px 2px 2px'}}
                >
                    <span className={'dropdown-icon'}/>
                </DropdownToggle>
                <DropdownMenu right={!!menuRight}>
                    <DropdownItem onClick={e => this.dropdownChangeValue(e, this.props.sortFunction, sortField)}>
                        <div>
                            Ascending
                        </div>
                    </DropdownItem>
                    <DropdownItem onClick={e => this.dropdownChangeValue(e, this.props.sortFunction, sortField)}>
                        <div>
                            Descending
                        </div>
                    </DropdownItem>
                </DropdownMenu>
            </Dropdown>
        );
    };

    settingsClick = (shopperID, shopperUUID) => {
        this.setState({id: shopperID, shopperUUID: shopperUUID});
    };

    handleCommissions = (uuidFleet = '', nameFleet = '') => {
        const {showShopperCommission} = this.state;
        this.setState({
            showShopperCommission: !showShopperCommission,
            selectedShopperUUID: uuidFleet,
            selectedShopperName: nameFleet
        });
    };

    handleGigaPay = (uuidFleet = '') => {
        const {showGigaPayModal} = this.state;
        this.setState({
            showGigaPayModal: !showGigaPayModal,
            selectedShopperUUID: uuidFleet
        });
    };

    handleRevolut = (uuidFleet = '') => {
        const {showRevolutModal} = this.state;
        this.setState({
            showRevolutModal: !showRevolutModal,
            selectedShopperUUID: uuidFleet
        });
    };

    handleDropDownAction = (name = 'dropDownActions', val) => {
        const {
            selectedShopperUUID: uuidFleet,
            selectedShopperName: nameFleet
        } = this.state;
        switch (val) {
            case 'assignStore':
                this.settingsDropdownChangeValue();
                break;
            case 'revolutModal':
                this.handleRevolut(uuidFleet);
                break;
            case 'gigapayModal':
                this.handleGigaPay(uuidFleet);
                break;
            case 'commissionModal':
                this.handleCommissions(uuidFleet, nameFleet);
                break;
            default:
                break;
        }
    };

    settingsToggle = () => {
        this.setState(prevState => ({settingsDropdown: !prevState.settingsDropdown, settingsToggleClick : !this.state.settingsToggleClick}));
    };

    toggleFunction = () => {
        if(this.state.settingsToggleClick){
            this.setState(prevState => ({settingsDropdown: !prevState.settingsDropdown, settingsToggleClick : !this.state.settingsToggleClick}));
        }
    };

    getSettingsDropDown = (dataID, stateID, uuidFleet, nameFleet) => {
        const dropDownMenu = [
            {fieldName: 'Assign To Store', fieldData: 'assignStore'},
            {fieldName: 'Update Revolut Information', fieldData: 'revolutModal'},
            {fieldName: 'Update GigaPay Information', fieldData: 'gigapayModal'},
            {fieldName: 'Commissions', fieldData: 'commissionModal'}
        ];
        return (
            <div
                onClick={() =>
                    this.setState({
                        selectedShopperUUID: uuidFleet,
                        selectedShopperName: nameFleet
                    })
                }
            >
                {/*<CustomDropDownFormGroup*/}
                    {/*isCaretOnly={true}*/}
                    {/*label=""*/}
                    {/*name="dropDownActions"*/}
                    {/*data={dropDownMenu}*/}
                    {/*dispField="fieldName"*/}
                    {/*dataField="fieldData"*/}
                    {/*onChange={this.handleDropDownAction}*/}
                {/*/>*/}

                <Dropdown isOpen={this.state.settingsDropdown}
                          style={{marginLeft: '15px'}}
                          toggle={() => this.toggleFunction()}>
                    <DropdownToggle caret
                                    onClick={() => this.settingsToggle()}
                                    className={"button"}
                                    style={{background: 'none', padding: '3px 5px 2px 2px'}}
                    >
                        <span className={'dropdown-icon'}/>
                    </DropdownToggle>

                    {
                        dataID && dataID === stateID ?
                            <div tabIndex='-1' role='menu' aria-hidden='false'
                                 className="dropdown-menu dropdown-menu-right show"
                                 data-placement='bottom-end'
                                 style={{
                                     position: 'absolute',
                                     willChange: 'transform',
                                     top: '0px',
                                     left: '0px',
                                     transform: 'translate3d(-145px, 23px, 0px)',
                                     cursor: 'pointer'
                                 }}>
                                {dropDownMenu.map((item, i) => (
                                    <DropdownItem
                                        className={"pointer"}
                                        key={i}
                                        onClick={e => {
                                            this.handleDropDownAction('dropDownActions', item.fieldData)
                                        }}
                                    >
                                        {item.fieldName}
                                    </DropdownItem>
                                ))}
                            </div> : null
                    }
                </Dropdown>

            </div>
        );
        // return (
        //   <Dropdown
        //     isOpen={this.state.settingsDropdown}
        //     style={{ marginLeft: '15px' }}
        //   >
        //     <DropdownToggle
        //       onClick={() => this.settingsToggle()}
        //       className={'button'}
        //       style={{ background: 'none', padding: '3px 5px 2px 2px' }}
        //     >
        //       <span className={'dropdown-icon'} />
        //     </DropdownToggle>
        //     {dataID === stateID ? (
        //       <div
        //         tabIndex="-1"
        //         role="menu"
        //         aria-hidden="false"
        //         className="dropdown-menu dropdown-menu-right show"
        //         data-placement="bottom-end"
        //         style={{
        //           position: 'absolute',
        //           willChange: 'transform',
        //           top: '0px',
        //           left: '0px',
        //           transform: 'translate3d(-145px, 23px, 0px)',
        //           cursor: 'pointer'
        //         }}
        //       >
        //         <DropdownItem className={'pointer'}>
        //           <div onClick={() => this.settingsDropdownChangeValue()}>
        //             Assign to Store
        //           </div>
        //         </DropdownItem>
        //         <DropdownItem className={'pointer'}>
        //           <div onClick={() => this.handleRevolut(uuidFleet)}>
        //             Update Revolut Information
        //           </div>
        //         </DropdownItem>
        //         <DropdownItem className={'pointer'}>
        //           <div onClick={() => this.handleGigaPay(uuidFleet)}>
        //             Update GigaPay Information
        //           </div>
        //         </DropdownItem>
        //         <DropdownItem className={'pointer'}>
        //           <div onClick={() => this.handleCommissions(uuidFleet, nameFleet)}>
        //             Commissions
        //           </div>
        //         </DropdownItem>
        //       </div>
        //     ) : null}
        //   </Dropdown>
        // );
    };

    onTableNameClick = (showDetailsFun, data) => {
        if (data) {
            this.props.getApprovedFleetDetail(data.uuid, () =>
                showDetailsFun(true, data)
            );
        }
    };

    render() {
        const {
            selectedShopperUUID,
            selectedShopperName,
            showShopperCommission,
            showRevolutModal,
            showGigaPayModal
        } = this.state;
        const {dataArray, title, noDataTitle, revolutAccounts} = this.props;
        const getTableRow = (title, dropdownFun) => {
            return (
                <Row>
                    <Col md={12} style={{display: 'flex'}}>
                        <span style={{marginTop: '2px'}}>{title}</span>
                        {this.props.sortingDropdown ? dropdownFun : null}
                    </Col>
                </Row>
            );
        };
        if (!dataArray) {
            return (
                <Card>
                    <CardBody>
                        <Row
                            style={{
                                textAlign: 'center',
                                paddingTop: '35px',
                                paddingBottom: '35px'
                            }}
                        >
                            <Col md={{size: '6', offset: '3'}}>{noDataTitle}</Col>
                        </Row>
                    </CardBody>
                </Card>
            );
        }

        return (
            <div>
                {showShopperCommission && (
                    <CustomFormModal
                        showModal={showShopperCommission}
                        element={
                            <ShopperCommissions
                                uuidFleet={selectedShopperUUID}
                                nameFleet={selectedShopperName}
                            />
                        }
                        onCancel={() => this.handleCommissions()}
                        label={"Shopper Commission"}
                    />
                )}
                {showRevolutModal && (
                    <CustomFormModal
                        showModal={showRevolutModal}
                        element={<RevolutForm uuidFleet={selectedShopperUUID}/>}
                        onCancel={() => this.handleRevolut()}
                        label={"Revolut Information"}
                    />
                )}
                {showGigaPayModal && (
                    <CustomFormModal
                        showModal={showGigaPayModal}
                        element={<GigaPayForm uuidFleet={selectedShopperUUID}/>}
                        onCancel={() => this.handleGigaPay()}
                        label={"GigaPay Information"}
                    />
                )}
                <Card>
                    {this.state.toggleClick === true
                        ? this.getModalContent()
                        : this.getModalDeactivate()}

                    <GetModal
                        isOpen={this.state.modalVisible}
                        toggle={this.state.modalToggle}
                        content={this.state.modalContent}
                        modalBodyStyle={this.state.modalBodyStyle}
                        headerStyle={this.state.headerStyle}
                    />
                    <CardHeader>
                        <CardTitle tag="h4">{title ? title : ''}</CardTitle>
                    </CardHeader>
                    <CardBody>
                        {
                            <Table className="tablesorter" responsive>
                                <thead className="text-primary">
                                <tr>
                                    <th>
                                        {getTableRow(
                                            this.props.name,
                                            this.getDropDown(
                                                this.state.nameDropdown,
                                                'nameDropdown',
                                                'firstName'
                                            )
                                        )}
                                    </th>
                                    <th>
                                        {getTableRow(
                                            this.props.id,
                                            this.getDropDown(
                                                this.state.idDropdown,
                                                'idDropdown',
                                                'id'
                                            )
                                        )}
                                    </th>
                                    <th>
                                        {getTableRow(
                                            this.props.phone,
                                            this.getDropDown(
                                                this.state.phoneDropdown,
                                                'phoneDropdown',
                                                'phone'
                                            )
                                        )}
                                    </th>
                                    <th>
                                        {getTableRow(
                                            this.props.email,
                                            this.getDropDown(
                                                this.state.emailDropdown,
                                                'emailDropdown',
                                                'email',
                                                true
                                            )
                                        )}
                                    </th>
                                    {this.props.actionHeader === true ? (
                                        <th>
                                            {this.props.activeTypo
                                                ? this.props.active
                                                : this.props.action}
                                        </th>
                                    ) : null}
                                    {this.props.actionDropdown ? (
                                        <th>{this.props.action}</th>
                                    ) : null}
                                </tr>
                                </thead>
                                <tbody>
                                {dataArray && dataArray.map((data, index) => {
                                    const fleetName =
                                        capitalize(data.firstName) +
                                        ' ' +
                                        capitalize(data.lastName);
                                    return (
                                        <tr key={data.uuid}>
                                            {this.props.onClick === false ? (
                                                <td style={{width: getTdWidth(this.props.name)}}>
                                                    {capitalize(data.firstName)}{' '}
                                                    {capitalize(data.lastName)}
                                                </td>
                                            ) : (
                                                <td
                                                    style={{
                                                        width: getTdWidth(this.props.name),
                                                        cursor: 'pointer'
                                                    }}
                                                    onClick={() =>
                                                        this.onTableNameClick(
                                                            this.props.showDetails,
                                                            data
                                                        )
                                                    }
                                                >
                                                    {fleetName}
                                                </td>
                                            )}
                                            <td style={{width: getTdWidth(this.props.id)}}>
                                                {data.id}
                                            </td>
                                            <td style={{width: getTdWidth(this.props.phone)}}>
                                                {data.phone}
                                            </td>
                                            <td style={{width: getTdWidth(this.props.email)}}>
                                                {data.email}
                                            </td>
                                            {this.props.showAction === true ? (
                                                <td
                                                    style={{width: getTdWidth(this.props.action)}}
                                                >
                                                    {this.getActions(
                                                        this.props,
                                                        data.uuid,
                                                        index,
                                                        data
                                                    )}
                                                </td>
                                            ) : null}
                                            {this.props.actionDropdown ? (
                                                <td
                                                    style={{width: getTdWidth(this.props.action)}}
                                                    onClick={() =>
                                                        this.settingsClick(data.id, data.uuid)
                                                    }
                                                >
                                                    {this.getSettingsDropDown(
                                                        data.id,
                                                        this.state.id,
                                                        data.uuid,
                                                        fleetName
                                                    )}
                                                </td>
                                            ) : null}
                                        </tr>
                                    );
                                })}
                                </tbody>
                            </Table>
                        }
                    </CardBody>
                </Card>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        action: state.multilingual.activeLanguageData.action,
        active: state.multilingual.activeLanguageData.active,
        email: state.multilingual.activeLanguageData.email,
        id: state.multilingual.activeLanguageData.id,
        name: state.multilingual.activeLanguageData.name,
        phone: state.multilingual.activeLanguageData.phone,
        role: state.multilingual.activeLanguageData.role
    };
};

const connectedComponent = connect(mapStateToProps, {
    activateApprovedShopper,
    deactivateApprovedShopper,
    setFleetAStaffStoreShopper,
    saveRevolutInfo,
    saveGigaPayInfo,
    getApprovedFleetDetail
})(SimpleTable);
export default withRouter(connectedComponent);
