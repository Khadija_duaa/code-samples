import React, { Component } from "react";
import { Card, CardHeader, CardBody, CardTitle } from "reactstrap";

import CustomItemsTable from "../CustomComponents/CustomItemsTable";

class ItemsListing extends Component {
  getItemsForOrderDetails = () => {
    const {
      headers,
      data,
      title,
      orderNo,
      // onToggleOrderDetails,
      isDeliveredOrder
    } = this.props;
    return (
      <Card>
        <CardHeader>
          {/* <CardTitle
            className="h5"
            style={{ cursor: "pointer" }}
            onClick={() => onToggleOrderDetails()}
          >
            <i
              className="tim-icons icon-minimal-left"
              style={{ color: "white", padding: "9px 12px 11px 0px" }}
            ></i>
            Back
          </CardTitle> */}

          <CardTitle className="h3">{`${title}: #${orderNo}`}</CardTitle>
        </CardHeader>
        <CardBody>
          <CustomItemsTable data={data} isDeliveredOrder={isDeliveredOrder} />
        </CardBody>
      </Card>
    );
  };

  render() {
    const {
      title,
      button,
      topBtnTitle,
      onEditItemClick,
      itemEditBtn,
      btnTitle,
      noDataTitle,
      onItemDetailClick,
      actionTitle,
      priceTitle,
      items,
      headers,
      search,
      searchAPI,
      onItemDetail,
      importItems,
      showItemEditForm,
      dataArray
    } = this.props;

    return this.getItemsForOrderDetails();
  }
}
export default ItemsListing;
