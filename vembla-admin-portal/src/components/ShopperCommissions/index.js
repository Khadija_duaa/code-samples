import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardBody } from 'reactstrap';

import { fetchCommissions } from '../../store/shopper/shopper-actions';

import CustomCommissionsTable from '../CustomComponents/CustomCommisionsTable';
import ShopperCommissionForm from '../ShopperCommissionForm';
import Loader from '../Loader';

class ShopperCommissions extends Component {
  componentDidMount() {
    const { uuidFleet, fetchCommissions } = this.props;
    fetchCommissions(uuidFleet);
  }

  render() {
    const { uuidFleet, nameFleet, isProcessing, commissions } = this.props;

    return (
      <Card style={{ margin: '0px' }}>
        {isProcessing ? (
          <Loader />
        ) : (
          <CardBody>
            <h4>{nameFleet}</h4>
            <ShopperCommissionForm uuidFleet={uuidFleet} />
            <hr />
            <CustomCommissionsTable data={commissions} />
          </CardBody>
        )}
      </Card>
    );
  }
}
const mapStateToProps = state => ({
  isProcessing: state.shopper.commissionsProcessing,
  commissions: state.shopper.commissions
});
export default connect(mapStateToProps, { fetchCommissions })(
  ShopperCommissions
);
