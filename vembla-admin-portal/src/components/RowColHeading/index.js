import React from 'react';
import { Row, Col} from 'reactstrap';

const rowColHeading = (props) => {
    return(
        <Row style={props.style}>
            <Col md={12} className={"small-font text-color-gray"}>
                <span style={props.spanStyle} className={"text-color-gray"}>{props.heading}</span>
            </Col>
        </Row>
    )
};

export default rowColHeading;