import React, {Component} from "react";
// import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
    Row,
    Col,
    Label,
    Modal,
    ModalHeader,
    Card,
    CardBody,
    Button,
    FormGroup
} from "reactstrap";

import {
    timeSplitter,
    capitalize,
    getDateFromISO,
    defaultBlankImg
} from "../../utils/common-utils";

import CustomDropDownFormGroup from "../CustomComponents/CustomDropDownFormGroup";
import CustomItemsTable from "../CustomComponents/CustomItemsTable";
import OrderMetaInfo from "../OrderMetaInfo";

const DispText = ({label, text}) => {
    return (
        <div>
            <Label>{label}</Label>
            <p style={{fontSize: "1.2rem"}}>{text}</p>
        </div>
    );
};

class PendingOrderDetials extends Component {
    constructor(props) {
        super(props);
        this.state = {
            freeRiderUUID: null
        };
    }

    selectedRider = (name, val) => {
        switch (name) {
            case "riderUUID":
                this.setState({freeRiderUUID: val});
                break;
            default:
                break;
        }
    };

    assignOrder = () => {
        const {assignOrderManually, orderUUID, getPendingOrders} = this.props;
        const {freeRiderUUID} = this.state;
        assignOrderManually(freeRiderUUID, orderUUID);
        getPendingOrders(); // For refreshing pendingOrders in Fleet Management
    };

    render() {
        const {
            store,
            orderPlacedAt,
            orderNo,
            consumerF_Name,
            consumerL_Name,
            deliveryFee,
            orderAmount,
            deliveryAddress,
            openModal,
            toggle,
            freeRiders
        } = this.props;

        return (
            <Modal isOpen={openModal} toggle={toggle}>
                <Card style={{marginBottom: "0px"}}>
                    <ModalHeader toggle={toggle}>
                        <p style={{fontSize: "1.5rem"}}>{`Order #${orderNo}`}</p>
                    </ModalHeader>
                    <CardBody>
                        <Row>
                            <Col md="6">
                                <CustomDropDownFormGroup
                                    label="Select Rider For Assignment"
                                    btnText={freeRiders && freeRiders.length ? "Free Riders" : "Not Available"}
                                    name="riderUUID"
                                    dataField="uuidFleet"
                                    dispField="firstName"
                                    data={freeRiders}
                                    onChange={this.selectedRider}
                                />
                            </Col>
                            <Col md="3">
                                <FormGroup>
                                    <Label></Label>
                                    {freeRiders && freeRiders.length ? (
                                        <Button onClick={this.assignOrder}
                                                disabled={this.state.freeRiderUUID === null}>
                                            Assign
                                        </Button>) : (
                                        <Button onClick={() => console.log("N/A")} disabled>Assign</Button>)}
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="12">
                                <Row>
                                    <Col md="12">
                                        <OrderMetaInfo
                                            orderPlacedAt={orderPlacedAt}
                                            consumerF_Name={consumerF_Name}
                                            consumerL_Name={consumerL_Name}
                                            deliveryFee={deliveryFee}
                                            orderAmount={orderAmount}
                                            deliveryAddress={deliveryAddress}
                                            store={store}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Card>
                                        <CardBody>
                                            <CustomItemsTable
                                                data={store.items}
                                                isPendingOrder={true}
                                            />
                                        </CardBody>
                                    </Card>
                                </Row>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </Modal>
        );
    }
}

PendingOrderDetials.propTypes = {
    openModal: PropTypes.bool.isRequired,
    store: PropTypes.object.isRequired,
    orderPlacedAt: PropTypes.string.isRequired,
    orderNo: PropTypes.string.isRequired,
    consumerF_Name: PropTypes.string.isRequired,
    consumerL_Name: PropTypes.string.isRequired,
    deliveryFee: PropTypes.number.isRequired,
    orderAmount: PropTypes.number.isRequired,
    deliveryAddress: PropTypes.string.isRequired,
    freeRiders: PropTypes.array,
    assignOrderManually: PropTypes.func,
    getPendingOrders: PropTypes.func
};

// const mapStateToProps = state => {
//   return {
//     freeRiders: state.fleet.freeRiders
//   };
// };

// export default connect(mapStateToProps)(PendingOrderDetials);

export default PendingOrderDetials;
