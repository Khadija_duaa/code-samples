import React, { Component } from 'react';
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Col,
  Row,
  Button,
  FormGroup,
  Form,
  Input
} from 'reactstrap';
import CustomDropDownFormGroup from '../CustomComponents/CustomDropDownFormGroup';

// const prepareGigapayFields = () => {
//   const inputFields = [];
//   inputFields.push({
//     name: 'personalNumber',
//     placeholder: 'Personal Number',
//     label: 'Enter Personal Number'
//   });
//   inputFields.push({
//     name: 'cellNumber',
//     placeholder: 'Cell Number',
//     label: 'Enter Cell Number'
//   });
//   return inputFields;
// };

class ApprovedShopper extends Component {
  state = {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    programDropdownOpen: false,
    program: '',
    programUUID: '',
    revolutAccID: '',
    personalNumber: '',
    cellNumber: '',
    vehicleTypeId: '',
    name: '',
    color: '',
    registrationNumber: '',
    hasCoolingBox: false,
    hasHeatingBox: false
  };

  componentDidMount() {
    if (this.props.shopperData) {
      let { firstName, lastName, email, phone } = this.props.shopperData;
      this.setState({ firstName, lastName, email, phone });
    }
  }

  // Refactored Into Separate Component : ApproveShopper

  // onApprove = (
  //   dispatchFun,
  //   parentState,
  //   shopperData,
  //   onParentApproveBtnClick,
  //   state
  // ) => {
  //   const { countryCode } = shopperData;

  //   let obj = {
  //     firstName: state.firstName,
  //     lastName: state.lastName,
  //     email: state.email,
  //     phone: state.phone,
  //     countryCode: countryCode
  //   };

  //   if (shopperData) {
  //     onParentApproveBtnClick(
  //       shopperData.userUUID,
  //       parentState.currentPage,
  //       parentState.pageSize,
  //       obj,
  //       true
  //     );
  //   }
  // };

  // onInputChange = e => {
  //   let state = { ...this.state };
  //   state[e.target.name] = e.target.value;
  //   this.setState(state);
  // };

  // prepareFields = () => {
  //   const inputFields = [];
  //   inputFields.push({
  //     name: 'firstName',
  //     placeholder: 'First Name',
  //     label: 'First Name',
  //     value: this.state.firstName
  //   });
  //   inputFields.push({
  //     name: 'lastName',
  //     placeholder: 'Last Name',
  //     label: 'Last Name',
  //     value: this.state.lastName
  //   });
  //   inputFields.push({
  //     name: 'email',
  //     placeholder: 'Email',
  //     label: 'Email',
  //     value: this.state.email
  //   });
  //   inputFields.push({
  //     name: 'phone',
  //     placeholder: 'Phone Number',
  //     label: 'Phone Number',
  //     value: this.state.phone
  //   });
  //   return inputFields;
  // };

  // prepareVehicleFields = () => {
  //   const inputFields = [];
  //   inputFields.push({
  //     name: 'name',
  //     placeholder: 'Vehicle Name',
  //     label: 'Vehicle Name',
  //     value: this.state.name
  //   });
  //   inputFields.push({
  //     name: 'color',
  //     placeholder: 'Color',
  //     label: 'Color',
  //     value: this.state.color
  //   });
  //   inputFields.push({
  //     name: 'registrationNumber',
  //     placeholder: 'Registration Number',
  //     label: 'Registration Number',
  //     value: this.state.registrationNumber
  //   });
  //   return inputFields;
  // };

  // programToggle = () => {
  //   this.setState(prevState => ({
  //     programDropdownOpen: !prevState.programDropdownOpen
  //   }));
  // };

  // programChangeValue = (e, data) => {
  //   this.setState({
  //     program: e.currentTarget.textContent,
  //     programUUID: data.uuid
  //   });
  // };

  // vehicleTypeToggle = () => {
  //   this.setState(prevState => ({
  //     vehicleTypeDropdownOpen: !prevState.vehicleTypeDropdownOpen
  //   }));
  // };

  // vehicleTypeChangeValue = (e, data) => {
  //   this.setState({
  //     vehicleType: e.currentTarget.textContent,
  //     vehicleTypeId: data.uuid
  //   });
  // };

  // handleCoolingChecked = () => {
  //   this.setState({ hasCoolingBox: !this.state.hasCoolingBox });
  // };

  // handleHeatingChecked = () => {
  //   this.setState({ hasHeatingBox: !this.state.hasHeatingBox });
  // };

  // Refactored Into Separate Component: ApproveShopper
  // getApproveShopperDialogue = fields => {
  //   return (
  //     <Form>
  //       <Row>
  //         {fields.map(field => {
  //           return (
  //             <Col className="pr-md-1" md="6">
  //               <FormGroup>
  //                 <label>{field.label}</label>
  //                 <Input
  //                   value={field.value}
  //                   onChange={e => this.onInputChange(e)}
  //                   name={field.name}
  //                   placeholder={field.placeholder}
  //                   type="text"
  //                 />
  //               </FormGroup>
  //             </Col>
  //           );
  //         })}
  //       </Row>
  //     </Form>
  //   );
  // };

  // Refactored Into Separate Component: VehicleNProgramForm

  // getVehicleDialogue = (vehicleFields, vehicleTypeArray) => {
  //   return (
  //     <Form>
  //       <Row>
  //         {vehicleFields.map(field => {
  //           return (
  //             <Col className="pr-md-1" md="6">
  //               <FormGroup>
  //                 <label>{field.label}</label>
  //                 <Input
  //                   value={field.value}
  //                   onChange={e => this.onInputChange(e)}
  //                   name={field.name}
  //                   placeholder={field.placeholder}
  //                   type="text"
  //                 />
  //               </FormGroup>
  //             </Col>
  //           );
  //         })}
  //         <Col className="pr-md-1" md={"6"}>
  //           <FormGroup>
  //             <label>Select Vehicle Type</label>
  //             <Dropdown
  //               isOpen={this.state.vehicleTypeDropdownOpen}
  //               toggle={() => this.vehicleTypeToggle()}
  //             >
  //               <DropdownToggle caret>
  //                 {this.state.vehicleType
  //                   ? this.state.vehicleType
  //                   : "Vehicle Type"}
  //                 <span className={"dropdown-icon"} />
  //               </DropdownToggle>
  //               <DropdownMenu>
  //                 {vehicleTypeArray &&
  //                   vehicleTypeArray.map(data => {
  //                     return (
  //                       <DropdownItem>
  //                         <div
  //                           onClick={e => this.vehicleTypeChangeValue(e, data)}
  //                         >
  //                           {data.name}
  //                         </div>
  //                       </DropdownItem>
  //                     );
  //                   })}
  //               </DropdownMenu>
  //             </Dropdown>
  //           </FormGroup>
  //         </Col>
  //       </Row>
  //       <Row>
  //         <Col md={12}>
  //           <Row>
  //             <Col md={6}>
  //               <Row>
  //                 <Col md={10}>
  //                   <div style={{ color: "rgba(255, 255, 255, 0.8)" }}>
  //                     Cooling Box Available
  //                   </div>
  //                 </Col>
  //                 <Col md={2} style={{ marginTop: "1%" }}>
  //                   <input
  //                     type="checkbox"
  //                     checked={
  //                       this.state.hasCoolingBox ? this.state.hasCoolingBox : ""
  //                     }
  //                     value={this.state.hasCoolingBox}
  //                     onChange={this.handleCoolingChecked}
  //                   />
  //                 </Col>
  //               </Row>
  //             </Col>
  //             <Col md={6}>
  //               <Row>
  //                 <Col md={10}>
  //                   <div style={{ color: "rgba(255, 255, 255, 0.8)" }}>
  //                     Heating Box Available
  //                   </div>
  //                 </Col>
  //                 <Col md={2} style={{ marginTop: "1%" }}>
  //                   <input
  //                     type="checkbox"
  //                     checked={
  //                       this.state.hasHeatingBox ? this.state.hasHeatingBox : ""
  //                     }
  //                     value={this.state.hasHeatingBox}
  //                     onChange={this.handleHeatingChecked}
  //                     />
  //                  </Col>
  //               </Row>
  //             </Col>
  //           </Row>
  //         </Col>
  //       </Row>
  //     </Form>
  //   );
  // };

  // Refactored Into Separate Component: VehicleNProgramForm

  // getProgramDialogue = programsArray => {
  //   return (
  //     <Row>
  //       <Col className="pr-md-1" md={{ size: "6", offset: "3" }}>
  //         <FormGroup>
  //           <div style={{ textAlign: "center" }}>
  //             <label>Kindly Select Any Program !</label>
  //           </div>
  //           <Dropdown
  //             isOpen={this.state.programDropdownOpen}
  //             toggle={() => this.programToggle()}
  //           >
  //             <DropdownToggle caret>
  //               {this.state.program === ""
  //                 ? "Select Program"
  //                 : this.state.program}
  //               <span className={"dropdown-icon"} />
  //             </DropdownToggle>
  //             <DropdownMenu>
  //               {programsArray &&
  //                 programsArray.map(data => {
  //                   return (
  //                     <DropdownItem>
  //                       <div onClick={e => this.programChangeValue(e, data)}>
  //                         {data.name}
  //                       </div>
  //                     </DropdownItem>
  //                   );
  //                 })}
  //             </DropdownMenu>
  //           </Dropdown>
  //         </FormGroup>
  //       </Col>
  //     </Row>
  //   );
  // };

  // onChange = (name, val) => {
  //   this.setState({ revolutAccID: val });
  // };

  // getRevolutDialogue = revolutAccounts => {
  //   return (
  //     <Form>
  //       <Row>
  //         <Col className="pr-md-1" md="12">
  //           <FormGroup>
  //             <CustomDropDownFormGroup
  //               label={'Select Revolut Account:'}
  //               name="revolutAccID"
  //               dataField="id"
  //               dispField="name"
  //               btnText={'Select Revolut Account Name'}
  //               data={revolutAccounts}
  //               onChange={this.onChange}
  //             />
  //           </FormGroup>
  //         </Col>
  //       </Row>
  //     </Form>
  //   );
  // };

  // getGigapayDialogue = gigapayFields => {
  //   return (
  //     <Form>
  //       <Row>
  //         {gigapayFields.map(field => {
  //           return (
  //             <Col className="pr-md-1" md="6">
  //               <FormGroup>
  //                 <label>{field.label}</label>
  //                 <Input
  //                   onChange={this.onInputChange}
  //                   name={field.name}
  //                   placeholder={field.placeholder}
  //                   type="text"
  //                 />
  //               </FormGroup>
  //             </Col>
  //           );
  //         })}
  //       </Row>
  //     </Form>
  //   );
  // };

  render() {
    const {
      title,
      shopperData,
      approveShopperDialogue,
      programDialogue,
      revolutDialogue,
      gigapayDialogue,
      toggle,
      programsArray,
      parentState,
      dispatchFun,
      vehicleDialogue,
      vehicleTypeArray,
      onVehicleAssignBtnClick,
      shopperDetailDataObj,
      onParentApproveBtnClick,
      onProgramAssignBtnClick,
      onRevolutAssignIdClick,
      onGigapayAssignInfoClick,
      revolutAccounts
    } = this.props;
    // const fields = this.prepareFields();
    // const gigapayFields = prepareGigapayFields();
    // const vehicleFields = this.prepareVehicleFields();

    return (
      <div className="content">
        <Row>
          <Col md="12">
            <Card
              style={{
                marginBottom: 'unset',
                marginTop: '3%',
                paddingRight: '2%'
              }}
            >
              <CardHeader>
                <h4 className="title">{title}</h4>
              </CardHeader>
              <CardBody>
                {/* Refactored Into Separate Components: ApproveShopper & VehicleNProgramForm */}
                {/* {approveShopperDialogue
                  ? this.getApproveShopperDialogue(fields)
                  : null}

                {vehicleDialogue
                  ? this.getVehicleDialogue(vehicleFields, vehicleTypeArray)
                  : null}

                {programDialogue
                  ? this.getProgramDialogue(programsArray)
                  : null} */}

                {/* {revolutDialogue
                  ? this.getRevolutDialogue(revolutAccounts)
                  : null} */}

                {/* {gigapayDialogue
                  ? this.getGigapayDialogue(gigapayFields)
                  : null} */}
              </CardBody>
              <CardFooter style={{ textAlign: 'center' }}>
                {/* Refactored Into Separate Components: ApproveShopper & VehicleNProgramForm */}
                {/* {approveShopperDialogue ? (
                  <Button
                    className="btn-fill"
                    color="primary"
                    type="submit"
                    onClick={() =>
                      this.onApprove(
                        dispatchFun,
                        parentState,
                        shopperData,
                        onParentApproveBtnClick,
                        this.state
                      )
                    }
                  >
                    Approve
                  </Button>
                ) : null}

                {vehicleDialogue ? (
                  <Button
                    className="btn-fill"
                    color="primary"
                    type="submit"
                    onClick={() =>
                      onVehicleAssignBtnClick(
                        this.state.name,
                        this.state.color,
                        this.state.registrationNumber,
                        this.state.vehicleTypeId,
                        this.state.hasCoolingBox,
                        this.state.hasHeatingBox,
                        shopperDetailDataObj
                      )
                    }
                  >
                    Save
                  </Button>
                ) : null}

                {programDialogue ? (
                  <Button
                    className="btn-fill"
                    color="primary"
                    type="submit"
                    onClick={() =>
                      onProgramAssignBtnClick(this.state.programUUID)
                    }
                  >
                    Assign
                  </Button>
                ) : null} */}

                {/* {revolutDialogue ? (
                  <Button
                    className="btn-fill"
                    color="primary"
                    type="submit"
                    onClick={() =>
                      onRevolutAssignIdClick(this.state.revolutAccID)
                    }
                  >
                    Submit
                  </Button>
                ) : null} */}

                {/* {gigapayDialogue ? (
                  <Button
                    className="btn-fill"
                    color="primary"
                    type="submit"
                    onClick={() =>
                      onGigapayAssignInfoClick(
                        this.state.personalNumber,
                        this.state.cellNumber
                      )
                    }
                  >
                    Submit
                  </Button>
                ) : null} */}
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default ApprovedShopper;
