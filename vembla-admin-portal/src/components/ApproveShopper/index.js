import React from "react";
import { connect } from "react-redux";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button
} from "reactstrap";

import { approvePendingUser } from "../../store/shopper/shopper-actions";

const ApproveShopper = props => {
  const {
    titles,
    headers,
    shopperData: { firstName, lastName, email, phone, countryCode, userUUID },
    approvePendingUser
  } = props;
  //   const [firstName, setFirstName] = useState(shopperData.firstName);
  //   const [lastName, setLastName] = useState(shopperData.lastName);
  //   const [email, setEmail] = useState(shopperData.email);
  //   const [phone, setPhone] = useState(shopperData.phone);

  const approveUser = () => {
    // Takes 0 and 10 for reRendering pending users
    approvePendingUser(userUUID, 0, 10, {
      firstName,
      lastName,
      email,
      phone,
      countryCode
    });
  };

  return (
    <Card>
      <CardHeader>
        <h4 className="title">{titles.titleApprovePendingShopper}</h4>
      </CardHeader>
      <CardBody>
        <Form>
          <Row>
            <Col className="pr-md-1" md="6">
              <FormGroup>
                <label>{headers.firstName}</label>
                <Input value={firstName} type="text" readOnly />
              </FormGroup>
            </Col>
            <Col className="pr-md-1" md="6">
              <FormGroup>
                <label>{headers.lastName}</label>
                <Input value={lastName} type="text" readOnly />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col className="pr-md-1" md="6">
              <FormGroup>
                <label>{headers.email}</label>
                <Input value={email} type="text" readOnly />
              </FormGroup>
            </Col>
            <Col className="pr-md-1" md="6">
              <FormGroup>
                <label>{headers.phone}</label>
                <Input value={phone} type="text" readOnly />
              </FormGroup>
            </Col>
          </Row>
        </Form>
      </CardBody>
      <CardFooter>
        <Button
          className="btn-fill"
          color="primary"
          type="submit"
          onClick={() => approveUser()}
        >
          {titles.btnTextApprove}
        </Button>
      </CardFooter>
    </Card>
  );
};

const mapStateToProps = state => ({
  titles: {
    titleApprovePendingShopper: "Approve The Pending Shopper",
    btnTextApprove: "Approve"
  },
  headers: {
    firstName: "First Name",
    lastName: "Last Name",
    email: state.multilingual.activeLanguageData.email,
    phone: state.multilingual.activeLanguageData.phone
  }
});

const mapDispatchToProps = dispatch => {
  return {
    approvePendingUser: (uuid, number, size, formData, cb) => dispatch(approvePendingUser(uuid, number, size, formData, cb))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApproveShopper);
