import React from 'react';
import ReactTransition from "../ReactTransition"
import {Row, Col} from 'reactstrap';

const orderDetailComponent = (props) => {
    const {orderDetail} = props;

    const prepareOrderData = (orderDetail) => {
        const data = [];
        const {itemDetails} = orderDetail.order;
        const {deliveryAddress} = orderDetail.order;

        data.push({
            title: 'Name',
            value: itemDetails.itemName
        });
        data.push({
            title: 'Price',
            value: itemDetails.itemPrice
        });
        data.push({
            title: 'Quantity',
            value: itemDetails.itemQuantity
        });
        data.push({
            title: 'Delivery Address',
            value: deliveryAddress.address
        });
        return data;
    };

    const prepareRiderData = (orderDetail) => {
        const data = [];
        const {riderName, riderPhone, riderEmail} = orderDetail.rider;
        data.push({
            title: 'Name',
            value: riderName
        });
        data.push({
            title: 'Phone',
            value: riderPhone
        });
        data.push({
            title: 'Email',
            value: riderEmail
        });

        return data;
    };

    const orderData = prepareOrderData(orderDetail);
    const riderData = prepareRiderData(orderDetail);

    return (
        <ReactTransition>
            <div style={{height: '83vh'}}>
                <Row>
                    <Col md={1} style={{padding: "9px 0px 0px 21px"}}>
                        <i className="tim-icons icon-minimal-left"
                           onClick={() => props.setRiderDetailVisibility(false)}
                           style={{color: 'white'}}/>
                    </Col>
                    <Col md={{size: "1"}} className="heading">
                        Details
                    </Col>
                </Row>

                <div className={"innerDiv"}>
                    <Row style={{marginBottom: '6px', marginTop: "20px"}}>
                        <Col md={12}>
                            <span style={{fontWeight: '600'}} className={"text-color-gray"}> Order Details</span>
                        </Col>
                    </Row>
                    {
                        orderData && orderData.map( orderData => {
                            return(
                                <Row style={{margin: '7px 3px 7px 3px'}}>
                                    <Col md={5} className={"small-font text-color-gray"}>
                                        {orderData.title}
                                    </Col>
                                    <Col md={7} className={"small-font text-color-gray"}>
                                        {orderData.value}
                                    </Col>
                                </Row>
                            )
                        })
                    }
                    <Row style={{marginBottom: '6px', marginTop: '30px'}}>
                        <Col md={12}>
                            <span style={{fontWeight: '600'}} className={"text-color-gray"}> Rider Details</span>
                        </Col>
                    </Row>
                    {
                        riderData && riderData.map( riderData => {
                            return(
                                <Row style={{margin: '7px 3px 5px 7px'}}>
                                    <Col md={5} className={"small-font text-color-gray"}>
                                        {riderData.title}
                                    </Col>
                                    <Col md={7} className={"small-font text-color-gray"}>
                                        {riderData.value}
                                    </Col>
                                </Row>
                            )
                        })
                    }
                </div>
            </div>
        </ReactTransition>
    );
};

export default orderDetailComponent;