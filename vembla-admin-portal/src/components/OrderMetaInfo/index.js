import React from "react";
import { Row, Col, Label } from "reactstrap";

import {
  timeSplitter,
  capitalize,
  getDateFromISO,
  defaultBlankImg
} from "../../utils/common-utils";

const DispText = ({ label, text }) => {
  return (
    <div>
      <Label>{label}</Label>
      <p style={{ fontSize: "1.2rem" }}>{text}</p>
    </div>
  );
};

const OrderMetaInfo = ({
  consumerF_Name,
  consumerL_Name,
  orderPlacedAt,
  orderAmount,
  deliveryFee,
  deliveryAddress,
  deliveryTime,
  store
}) => {
  return (
    <Row>
      <Col md="12">
        <Row>
          <Col md="6">
            <DispText
              label="Consumer Name:"
              text={` ${capitalize(consumerF_Name)} ${capitalize(
                consumerL_Name
              )}`}
            />
          </Col>
          <Col md="3">
            <DispText
              label="Placed At: "
              text={`${getDateFromISO(orderPlacedAt)}, ${timeSplitter(
                orderPlacedAt
              )}`}
            />
          </Col>
          {deliveryTime && (
            <Col md="3">
              <DispText
                label="Delivered At: "
                text={`${getDateFromISO(deliveryTime)}, ${timeSplitter(
                  deliveryTime
                )}`}
              />
            </Col>
          )}
        </Row>
        <hr />
        <Row>
          <Col md="6">
            <DispText label="Order Amount: " text={`${orderAmount}`} />
          </Col>
          <Col md="6">
            <DispText label="Delivery Fee: " text={`${deliveryFee}`} />
          </Col>
        </Row>
        <hr />

        <Row>
          <Col md="12">
            <DispText label="Delivery Address: " text={`${deliveryAddress}`} />
          </Col>
        </Row>
        <hr />

        <Row>
          <Col md="3">
            <img alt="Store Icon" src={store.icon || defaultBlankImg} />
          </Col>
          <Col md="3">
            <DispText label="Store Name: " text={`${store.name}`} />
          </Col>
          <Col md="6">
            <DispText label="Store Address: " text={`${store.address}`} />
          </Col>
        </Row>
        <hr />
      </Col>
    </Row>
  );
};

export default OrderMetaInfo;
