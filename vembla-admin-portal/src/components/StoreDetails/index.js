import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardTitle,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
} from 'reactstrap';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import Loader from '../../components/Loader/index';
import {getDateFromISO} from "../../utils/common-utils";
import {
    getStoreByUUID
} from '../../store/store/store-actions';
import * as actions from "../../store/consumer/consumer-action-types";

class StoreDetails extends Component {

    componentDidMount() {
        const uuid = this.props.match.params.uuid;
        this.props.getStoreByUUID(uuid)

    }

    closeDetails = () => {
        // this.props.showStoreForm(false);
        // this.props.showStoreDetails(false);
        this.props.history.push('/admin/store')
    };

    prepareFields = (store) => {
        const inputFields = [];
        store.name && inputFields.push({label: 'Store Name', value: store.name});
        store.address && inputFields.push({label: 'Address:', value: store.address});
        store.uuid && inputFields.push({label: 'Store ID', value: store.uuid});
        store.geofence && inputFields.push({label: 'Store Radius', value: store.geofence});
        store.phone && inputFields.push({label: 'Contact Number', value: store.phone});
        store.staffed && inputFields.push({label: 'Staffed Store', value: store.staffed === true ? "Yes" : "No"});
        store.createdAt && inputFields.push({label: 'Date of Creation', value: getDateFromISO(store.createdAt)});
        store.instructions && inputFields.push({label: 'Store Instructions', value: store.instructions});
        return inputFields;
    };

    prepareStoreTimingsFields = (timingsArr) => {
        const inputFields = [];

        for (let i = 0; i < timingsArr.length; i++) {
            switch (timingsArr[i].day) {
                case 1:
                    inputFields.push({label: 'Monday', startTime: timingsArr[i].startTime, endTime: timingsArr[i].endTime});
                    break;
                case 2:
                    inputFields.push({label: 'Tuesday', startTime: timingsArr[i].startTime, endTime: timingsArr[i].endTime});
                    break;
                case 3:
                    inputFields.push({label: 'Wednesday', startTime: timingsArr[i].startTime, endTime: timingsArr[i].endTime});
                    break;
                case 4:
                    inputFields.push({label: 'Thursday', startTime: timingsArr[i].startTime, endTime: timingsArr[i].endTime});
                    break;
                case 5:
                    inputFields.push({label: 'Friday', startTime: timingsArr[i].startTime, endTime: timingsArr[i].endTime});
                    break;
                case 6:
                    inputFields.push({label: 'Saturday', startTime: timingsArr[i].startTime, endTime: timingsArr[i].endTime});
                    break;
                default : {
                    // do nothing
                }
            }
        }
        return inputFields;
    };

    render() {
        const {storeData} = this.props;

        let fields, storeTimingsFields;
        if (storeData) {
            fields = this.prepareFields(storeData);
        }
        if (storeData && storeData.storeTimings) {
            storeTimingsFields = this.prepareStoreTimingsFields(storeData.storeTimings);
        }
        return (
            <div className="content">
                <Row>
                    <Col md={"12"}>
                        <Card>
                            <CardHeader>
                                <CardTitle tag="h4">
                                    <i className="tim-icons icon-minimal-left pointer"
                                       onClick={() => this.closeDetails()}
                                       style={{color: 'white', padding: "9px 12px 11px 0px"}}>
                                    </i>
                                    Store Details
                                </CardTitle>
                            </CardHeader>

                            {
                                this.props.processing ?
                                    <div style={{marginBottom: '200px'}}>
                                        <Loader/>
                                    </div> :

                                    <CardBody>
                                        {
                                            storeData && storeData.icon ?
                                                <Row style={{marginBottom: '2%'}}>
                                                    <Col md={{size: '4', offset: '1'}}>
                                                        <img
                                                            src={storeData.icon}
                                                            alt="Item"
                                                            height={"150px"}
                                                            width={"150px"}
                                                            style={{objectFit: 'cover', marginBottom: '4px'}}
                                                        />
                                                    </Col>
                                                </Row> : null
                                        }
                                        <Row>
                                            {
                                                fields && fields.map(fields => {
                                                    return (
                                                        <Col md={12}>
                                                            <Row>
                                                                <Col md={{size: '2', offset: '1'}}
                                                                     className={"text-color-gray"}
                                                                     style={{fontWeight: "600", marginBottom: '7px'}}
                                                                >{fields.label}</Col>
                                                                <Col md={9}
                                                                     className={"text-color-gray"}>{fields.value}</Col>
                                                            </Row>
                                                        </Col>
                                                    )
                                                })
                                            }
                                        </Row>

                                        <Row>
                                            <Col md={"12"} style={{marginTop: '2%'}}>
                                                <Row>
                                                    <Col md={{size: '2', offset: '1'}} className={"text-color-gray"}
                                                         style={{fontSize: 'initial', marginBottom: '7px'}}>Days</Col>
                                                    <Col md={4} className={"text-color-gray"}
                                                         style={{fontSize: 'initial', marginBottom: '7px'}}>Start Time</Col>
                                                    <Col md={4} className={"text-color-gray"}
                                                         style={{fontSize: 'initial', marginBottom: '7px'}}>End Time</Col>
                                                </Row>
                                            </Col>
                                            {
                                                storeTimingsFields && storeTimingsFields.map(fields => {
                                                    return (
                                                        <Col md={12}>
                                                            <Row>
                                                                <Col md={{size: '2', offset: '1'}} className={"text-color-gray"}
                                                                     style={{fontWeight: "600", marginBottom: '7px'}}>{fields.label}</Col>
                                                                <Col md={4} className={"text-color-gray"}>{fields.startTime}</Col>
                                                                <Col md={4} className={"text-color-gray"}>{fields.endTime}</Col>
                                                            </Row>
                                                        </Col>
                                                    )
                                                })
                                            }
                                        </Row>
                                    </CardBody>
                            }
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.storeReducer.processing,
        storeData: state.storeReducer.storeData
    }
};

const connectedComponent = connect(mapStateToProps, {getStoreByUUID})(StoreDetails);
export default withRouter(connectedComponent)

