import React, { Component } from "react";
import { connect } from "react-redux";
import shortid from "shortid";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Button
} from "reactstrap";

import { getPendingShopper } from "../../store/shopper/shopper-actions";
import { capitalize } from "../../utils/common-utils";

class PendingShoppers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0,
      pageSize: 10
      //   totalPages: 0 in Props
    };
  }

  componentDidMount() {
    const { currentPage, pageSize } = this.state;
    this.props.getPendingShopper(currentPage, pageSize);
  }

  render() {
    const { titlePendingShoppers, pendingShoppers, header } = this.props;
    return (
      <Card>
        <CardHeader>
          <CardTitle className="h3">{titlePendingShoppers}</CardTitle>
        </CardHeader>
        <CardBody>
          <Table className="tablesorter" responsive>
            <thead className="text-primary">
              <tr>
                <th>{header.name}</th>
                <th>{header.id}</th>
                <th>{header.phone}</th>
                <th>{header.email}</th>
                <th>{header.action}</th>
              </tr>
            </thead>
            <tbody>
              {pendingShoppers &&
                pendingShoppers.map(row => (
                  <tr key={shortid.generate()}>
                    <td>
                      {capitalize(row.firstName) +
                        " " +
                        capitalize(row.lastName)}
                    </td>
                    <td>{row.id}</td>
                    <td>{row.phone}</td>
                    <td>{row.email}</td>
                    <td>
                      <Button>{header.btnTextApprove}</Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
          {pendingShoppers && pendingShoppers.map}
        </CardBody>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  titlePendingShoppers: "Pending Shoppers",
  titleNoDataFound: state.multilingual.activeLanguageData.noDataTitle,
  header: {
    action: state.multilingual.activeLanguageData.action,
    email: state.multilingual.activeLanguageData.email,
    id: state.multilingual.activeLanguageData.id,
    name: state.multilingual.activeLanguageData.name,
    phone: state.multilingual.activeLanguageData.phone,
    btnTextApprove: "Approve"
  },
  role: state.multilingual.activeLanguageData.role,

  pendingShoppers: state.shopper.pendingShopper,
  pendingShoppersData: state.shopper.pendingShopperData,
  totalPages: state.shopper.totalPages
});

const mapDispatchToProps = dispatch => {
  return {
    getPendingShopper: (
      currentPage,
      pageSize,
      sort = "asc",
      field = "firstName"
    ) => dispatch(getPendingShopper(currentPage, pageSize, sort, field))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PendingShoppers);
