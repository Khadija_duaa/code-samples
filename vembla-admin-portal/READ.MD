## Vembla Admin Portal

## Installation

``` bash
# clone the repo
$ git ssh://git@bitbucket.synavos.com:7999/vem/admin-web.git

# go into app's directory
$ cd admin-web

# install app's dependencies
$ npm install
```

``` bash
# dev server  with hot reload at http://localhost:3000
$ npm start
```

Navigate to [http://localhost:3000](http://localhost:3000). The app will automatically reload if you change any of the source files.

### Build

Run `build` to build the project. The build artifacts will be stored in the `build/` directory.

```bash
# build for production with minification
$ npm run build
```