const fs = require('fs');
const https = require('https');
const express = require('express');
​
const app = express();
​
app.use(express.static(__dirname + '/build'));
​
app.get('*', (req, resp)=>{
   resp.sendFile(__dirname + '/build/index.html');
});
​
const PORT = process.env.PORT || 3000;
​
    app.listen(PORT, () => {
        console.log(`App started on port ${PORT}`);
    });
