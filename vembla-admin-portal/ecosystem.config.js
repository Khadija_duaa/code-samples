module.exports = {
    apps : [{
        name      : 'vembla-admin-panel',
        script    : 'npm',
        args      : 'start',
        env: {
            NODE_ENV: 'production',
            PORT : 3006
        },
        env_production : {
            NODE_ENV: 'production',

        }
    }],

  
};
