import abbrevDays from 'abbrev-weekday-range';

const toNumericWeekDay = (day) => {
    let weekday = null;
    switch (day) {
        case 'Sun' :
            weekday = 0;
            break;
        case 'Mon' :
            weekday = 1;
            break;
        case 'Tue' :
            weekday = 2;
            break;
        case 'Wed' :
            weekday = 3;
            break;
        case 'Thu' :
            weekday = 4;
            break;
        case 'Fri' :
            weekday = 5;
            break;
        case 'Sat' :
            weekday = 6;
            break;
        default :
            break;
    }

    return weekday;
};

const mapToNumericDays = day => toNumericWeekDay(day);

export const toAbbrevDays = (days) => {
    let numericDays = days.map(mapToNumericDays);
    numericDays.sort();
    return abbrevDays(numericDays);
};