import validator from 'validator';
import PhoneNumber from "awesome-phonenumber";
//import ssnValidator from 'swedish-ssn';
import validate from 'swe-validation';
import postcodeValidator from 'postcode-validator';
//const isSwedish = () => localStorage.getItem('ACTIVE_LANGUAGE') === 'SV';
const isEnglish = () => localStorage.getItem('ACTIVE_LANGUAGE') !== 'SV';


const t_required = () => isEnglish() ? 'Required' : 'Nödvändig';
const t_fixChars = (fix) => isEnglish() ? `Must be ${fix} characters` : `Måste vara ${fix} tecken`;
const t_minChars = (min) => isEnglish() ? `Must be ${min} characters or more` : `Måste vara ${min} tecken eller mer`;

const t_minmaxChars = (min,max)=>isEnglish()?`Must be ${min} to ${max} digits`:`Måste vara ${min} till ${max} siffror`;

const t_maxChars = (max) => isEnglish() ? `Must be ${max} characters or less` : `Måste vara ${max} tecken eller mindre`;
const t_number = () => isEnglish() ? 'Must be a number' : 'Måste vara en siffra';
const t_invalidEmail = () => isEnglish() ? 'Invalid email address' : 'Ogiltig e-postadress';
const t_invalidPhone = () => isEnglish() ? 'Invalid phone number' : 'Ogiltigt telefonnummer';

const t_invalidSSN = ()=> isEnglish()   ?   'Invalid Security Number'   : 'Ogiltigt Säkerhetsnummer';
const t_invalidPostalCode= () => isEnglish() ? 'Invalid postal code'  : 'Ogiltig postnummer';
const t_alphanumeric = ()=>isEnglish()?'Only alphanumeric characters' : 'Endast alfanumeriska tecken';

const t_mustNumber = ()=>isEnglish()?"Must be an Integer value":"Måste vara ett heltal värde";
export const required = value => (value ? undefined : t_required());

export const requiredPassword = edit => edit ? () => undefined : t_required();

export const fixLength = max => value =>
    value && (value.length > max || value.length < max) ? t_fixChars(max) : undefined;

export const fixLength3 = fixLength(3);

export const minLength = min => value =>
    value && value.length < min ? t_minChars(min) : undefined;

export const maxLength = max => value =>
    value && value.length > max ? t_maxChars(max) : undefined;

export const minmaxLength = (min,max) => value =>
    value && (value.length < min || value.length > max)? t_minmaxChars(min,max) : undefined;


export const minLength4 = minLength(4);
export const maxLength5 = maxLength(5);
export const ssnLength = minmaxLength(10,12);
export const ssnPakistanLength = minmaxLength(1,30);


export const number = value =>
    value && isNaN(Number(value)) ? t_number() : undefined;

export const minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined;


export const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? t_invalidEmail()
        : undefined;

export const alphaNumeric = value =>
    value && /[^a-zA-Z0-9 ]/i.test(value)
        ? t_alphanumeric()
        : undefined;

export const alpha = value => /^[a-z ,.'-]+$/i.test(value) ? undefined : 'Only alphabetical characters are allowed';

export const phoneNumber = value =>
    value && !/^(0|[1-9][0-9]{9})$/i.test(value)
        ? 'Invalid phone number, must be 10 digits'
        : undefined;

export const swedishPhone = phone=>{


    let pn = new PhoneNumber(phone, 'SE');
    return pn.isValid() && pn.isMobile() ? undefined : t_invalidPhone();
};

export const pakistanPhone = phone=>{

    let pn = new PhoneNumber(phone, 'PK');
    return pn.isValid() && pn.isMobile() ? undefined : t_invalidPhone();
};

// export const validSwedishSsn = (ssn) => ssnValidator.validateSwedishSsn(ssn)?undefined :t_invalidSSN();

export const validSwedishSsn = (SSN) => validate.ssn(SSN).isValid?undefined :t_invalidSSN();


export const validPakistanPostalCode = postal => postcodeValidator.validate(postal,'PK') ? undefined: t_invalidPostalCode();
export const validSwedishPostalCode = postal => postcodeValidator.validate(postal,'SE') ? undefined: t_invalidPostalCode();

export const isChecked = (val) => val ? undefined : t_required();

export const pkPhone = value => value && !/^[+]\d{2}?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$/i.test(value) ? t_invalidPhone() : undefined;

export const pakistanCnic = value => value && !/^[1-7]{1}[0-9]{4}(-)?[0-9]{7}(-)?[0-9]{1}$/i.test(value) ? "Invalid CNIC" : undefined;

export const mapPoint = value => !/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/i.test(value) ? "Must be a valid value" : undefined;

export const latitude = value => (value <= 90 && value >= -90) ? undefined : "Must be a valid latitude";

export const longitude = value => (value <= 180 && value >= -180) ? undefined : "Must be a valid longitude";

export const website = value => !value || validator.isURL(value + "") ? undefined : "Must be a URL";


export const integer = value => !value || validator.isInt(value + "") ? undefined : t_mustNumber();

