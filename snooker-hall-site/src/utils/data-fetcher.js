import axios from '../utils/axios';
import {NotificationManager} from 'react-notifications';

export const fetchItems = () => {
    return new Promise((resolve, reject) => {
        axios.get('/item-category')
                .then(resp => {
                const {itemCategories} = resp.data;
                resolve(itemCategories);
            })
            .catch(error => {
                let message = 'Error occurred while fetching cart items';
                console.error(message, error);
                NotificationManager.error(message, '', 3000);
            });
    });
};

export const fetchCart = (sessionID) => {
    return new Promise((resolve, reject) => {
        let url = '/cart';


        if (sessionID) url += `/${sessionID}`;

        axios.get(url)
            .then(resp => {
                const {cart} = resp.data;
                resolve(cart);
            })
            .catch(error => {
                let message = 'Error occurred while fetching cart';
                console.error(message, error);
                reject({});
                NotificationManager.error(message, '', 3000);
            });
    });
};


export const saveCart = (items) => {
    return new Promise((resolve, reject) => {
        axios.post('/cart', {items})
            .then((resp) => {
                console.log(resp);
                resolve();
            })
            .catch(error => {
                let message = 'Error occurred while saving cart items';
                console.error(message, error);
                reject({});
                NotificationManager.error(message, '', 3000);
            });
    });
};