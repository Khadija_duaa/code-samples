import {store} from "../index";
import moment from 'moment';

export const getObjectValue = (obj, path) => {
    let val = null;

    if (path.indexOf('.') > -1) {
        let paths = path.split('.');
        val = obj;
        paths.forEach(_path => {
            val = val[_path];
        });
    }
    else {
        val = obj[path];
    }

    return val;
};


export const formatAmount = (amount) => {
    if (isNaN(amount)) {
        return amount;
    }

    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'SEK',
        minimumFractionDigits: 2
    });

    return formatter.format(amount);
};

export const formatDate = (date, showTime = true) => {
    if (!date) {
        return "-";
    }
    return showTime ? moment(date).format("DD MMM,YY hh:mm A") : moment(date).format("DD MMM,YY");
};

export const capitalize = (str) => {
    if (str) {
        str = str.toLowerCase();
        return `${str.charAt(0).toUpperCase()}${str.length > 1 ? str.substr(1) : ''}`;
    }
    return str;
};

export const isInWaitingList = (waitingList, activeUser) => {

    if (activeUser && waitingList && waitingList.length) {
        let activeUsers = waitingList.filter(entry => entry.user._id === activeUser._id);
        return activeUsers.length > 0;
    }

    return false;
};

export const formatName = (user) => {
    if (user && user.firstName && user.lastName) {
        return `${capitalize(user.firstName.substring(0,10))} ${capitalize(user.lastName.charAt(0))}.`;
    }
    return user;
};


export const isInTable = (user_id, table_id = null) => {
    let tables = store.getState().server_reducer.hallTables;
    let resultUser = null;


    if (tables && tables.length) {
        if (table_id) {
            tables = tables.filter(table => table._id === table_id);
        }

        tables.forEach(table => {
            table.users.forEach(user => {
                if (user.user._id === user_id) {
                    resultUser = user;
                }
            });
        });

        if (resultUser) return true;
    }

    return false;
};

export const getTdWidth = (items) => {
    return `${100 / items.length}%`;
};