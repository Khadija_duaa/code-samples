import axios from "./axios";
import {store} from "../index";
import {setProcessing} from "../store/server/server-actions";
import {NotificationManager} from 'react-notifications';
import {handleError} from '../store/store-utils';



export const removeFromQueue = () => {
    let {dispatch} = store;

    dispatch(setProcessing(true));

    axios.delete('/waiting-list')
        .then(() => {
            dispatch(setProcessing(false));
        })
        .catch(error => {
            dispatch(setProcessing(false));
            let message = handleError(error);
            console.error(message, error);
            NotificationManager.error(message, '', 3000);
            console.error('removeFromWaitingList', error);
        });
};