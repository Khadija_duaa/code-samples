export const GREEN_COLOR = '#18d26e';
export const GRAY_COLOR = '#212529';
export const HEADING_FONT = 'Montserrat';
export const BODY_FONT = 'Muli';

