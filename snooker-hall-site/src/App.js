import React, {Component} from 'react';
import './App.css'
import LandingPage from "./containers/LandingPage/LandingPage";
import HallView from './containers/GameView/Hall';
import {Redirect, HashRouter, Route, Switch} from 'react-router-dom';

class App extends Component {

    state = {
        mounted: false
    };

    componentDidMount() {
        this.setState({mounted: true});
    }

    render() {
        return (
            <div>
                <HashRouter>
                    <Switch>
                        <Route path="/hall" component={HallView}/>
                        <Route path="/" exact name={"Home"} component={LandingPage}/>
                        <Redirect to="/"/>
                    </Switch>
                </HashRouter>
            </div>
        );
    }
}


export default App;