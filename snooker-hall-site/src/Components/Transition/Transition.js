import React from 'react';
import './styles.css';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


const transition = (props) => {
    return (
        <ReactCSSTransitionGroup
            transitionName="floorView"
            transitionAppear={true}
            transitionAppearTimeout={1000}
            transitionEnter={false}
            transitionLeave={false}
        >
            {props.children}
        </ReactCSSTransitionGroup>
    );
};

export default transition;