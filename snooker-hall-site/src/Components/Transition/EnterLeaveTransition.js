import React from 'react';
import './styles.css';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


const transition = (props) => {
    return (
        <ReactCSSTransitionGroup
            transitionName="itemEnterLeave"
            transitionEnterTimeout={500}
            transitionLeaveTimeout={300}>
            {props.children}
        </ReactCSSTransitionGroup>
    );
};

export default transition;