import React from 'react';
import {HEADING_FONT} from "../../utils/css-constants";
import './Title.css';

class Title extends React.Component {

    headingStyle = {
        letterSpacing: "1px",
        fontFamily: HEADING_FONT,
        fontSize: "20px",
        WebkitTextStroke: "1px",
    };

    render() {
        const {divStyle, afterClass} = this.props;
        const _style = {
            ...this.headingStyle,
            ...this.props.style
            };
        return (
            <div className={`section-header ${afterClass && afterClass}`} style={divStyle && divStyle}>
                <h4 style={_style}>
                    {this.props.children && this.props.children}
                </h4>
            </div>

        )
    }
}

export default Title