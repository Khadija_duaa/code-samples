import React from 'react';
import EnterTransition from '../../Transition/EnterLeaveTransition';
import {formatName} from "../../../utils/common-utils";
import {connect} from 'react-redux';
import Time from "../../../containers/GameView/HallView/WaitingList/Time";

const userDataStyle = {
    color: "white",
    marginBottom: "5px",
    fontWeight: "bold",
    fontSize: "12.5px",
    letterSpacing: "1.5px",

};

const leftTextStyle = {
    ...userDataStyle,

    width:"50%",
    float:"left"
};

const rightTextStyle = {
    ...userDataStyle,

    width:"50%",
    float: "right",
    textAlign:"right"
};


const renderUsers = (tablePlayers,activeUser) => tablePlayers.map((player, index) => {

    let isCurrentUser = Boolean(activeUser && activeUser._id === player.user._id);
    let divStyle = {display:"inline-block",width:"100%"};
    let textStyle = {color: "#006002", fontSize: "12px", WebkitTextStroke: "0.3px"};

    if(isCurrentUser){
        var user = {
            firstName:activeUser.firstName,
            lastName:activeUser.lastName
        }

    }else{
        user = player.user
    }


    return (
        <div key={index} style={isCurrentUser?{...divStyle,borderBottom:"2px solid white"}:{...divStyle}}>
            <p style={leftTextStyle}>
                {formatName(user)}
            </p>
            <p style={rightTextStyle}>
                <Time style={isCurrentUser?{...textStyle,color:"#fff"}:{...textStyle}} time={player.joinedTime}
                      minuteText={"min"}/>
            </p>

        </div>
    )
});

const TableUsers = (props) => {

    const {tablePlayers, activeUser} = props;

    if (!(tablePlayers || !tablePlayers.length)) return null;

    return (
        <EnterTransition>
            {renderUsers(tablePlayers, activeUser)}
        </EnterTransition>
    )
};

const mapStateToProps = state=>{
  return{
      activeUser:state.user_reducer.activeUser
  }
};

export default connect(mapStateToProps)(TableUsers);