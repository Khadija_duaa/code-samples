import React from 'react';

const defaultStyle = {
    background: "rgba(31, 31, 31, 0.9)",
    border: "1px solid  rgba(206, 210, 219,0.7)",
    borderRadius: "7px",
    width: "100%",
    height: '100%',
    margin: "unset"
};

const container = ({style, children, className}) => {
    const componentStyle = {...defaultStyle, ...style};

    return (
        <div style={componentStyle}>
            {children}
        </div>
    );
};

export default container;