import React from 'react';
import NavItem from '../Dashboard/Navbar/NavItems/NavItem';
import {withRouter} from 'react-router-dom';

const navBar = ({navItems, history}) => {
    const navBar = [];

    const navItem_style = {
        margin: '0 1%',
        display: "inline-block",
        cursor: "pointer",
    };

    for (let index = 0; index < navItems.length - 1; index++) {
        const item = navItems[index];
        navBar.push(
            <li style={navItem_style} key={item.key}
                onClick={() => item.to && history.push(item.to)}>
                <NavItem {...item}/>
            </li>
        );
    }

    return navBar;
};

export default withRouter(navBar);