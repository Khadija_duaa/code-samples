import React from 'react'
import connect from "react-redux/es/connect/connect";
import {withRouter} from 'react-router-dom';
import ColouredLine from '../../Components/ColouredLine/ColouredLine';
import {GREEN_COLOR} from "../../utils/css-constants";
import {formatAmount} from "../../utils/common-utils";
import {ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import Modal from '../../Components/Modal/Modal';
import './membershipsDetail.css';
import {changeMembership} from "../../store/User/user-actions";
import Loader from 'react-loader-spinner';
import Title from "../Heading/Title";
import CircularButton from "../CircularButton/CircularButton";
import {toAbbrevDays} from "../../utils/date-utils";

const badgeArray = ["junior", "bronze", "silver", "gold"];


class MembershipsDetail extends React.Component {


    state = {
        showModal: false,
        membershipInfo: {}
    };


    toggleModal = (membership) => {
        this.setState({showModal: !this.state.showModal,membershipInfo: membership});
    };

    UpgradeMembershipModal = () => {
        let {membershipInfo} = this.state;

        const buttonStyle = {
            fontWeight: "normal",
            width: "100%",
            color: '#fff',
            padding: "10px",
            borderRadius: "5px",
            border: "unset"
        };

        return (
            <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
                <ModalHeader style={{border: "unset"}}>
                    <Title afterClass={"blackDash"}>
                        {this.props.modalHeading}
                    </Title>
                </ModalHeader>

                <ModalBody>
                    <p style={{fontSize: 18}}>
                        {`${this.props.modalBody}  ${membershipInfo.price} SEK`}
                    </p>
                </ModalBody>

                <ModalFooter style={{border: "unset"}}>
                    <CircularButton style={buttonStyle} backColor="#1C74BF" margin="0px 10px 0px 0px"
                                    onClick={this.toggleModal}>
                        {this.props.modalNoText}
                    </CircularButton>

                    <CircularButton style={buttonStyle} backColor="#13C95F" margin="0px 0px 0px 10px"
                                    onClick={() => {
                                        let {membershipInfo} = this.state;
                                        this.props.changeMembership(membershipInfo._id);
                                        this.toggleModal(false);
                                    }}>
                        {this.props.modalYesText}
                    </CircularButton>
                </ModalFooter>
            </Modal>
        )
    };

    openDetails = (index) => {
    };

    getPackageBadge = (membership) => {
        const headingStyle = {
            fontWeight: "600",
            marginTop: "3vw",
            color: 'black'
        };
        let style = {...headingStyle, ...this.props.style};

        return (
            <div>
                <img alt={membership.name} style={{width: "4vw"}} src={membership.imageUrl}/>
                <h6 style={style}>
                    {membership.name}
                </h6>
            </div>
        );
    };


    getPackageDetails = (column) => {
        let columnJSX = column.map((item, index) => {
            return (
                <p style={{height: "30px", fontSize: "14px"}} key={index}>
                    {typeof (item) === 'number' ? formatAmount(item) : item.length > 0 ? item : 'X'}
                </p>
            )
        });
        return (
            <div style={{paddingTop: "10px", fontSize: "14px"}}>
                {columnJSX}
            </div>
        );
    };


    getPackageDetailsButton = (membership) => {
        if (!membership._id) return null;
        if (membership._id && this.props.activeUser && membership._id === this.props.activeUser.membership._id) return null;

        const btnStyle = {
            backgroundColor: `${GREEN_COLOR}`,
            color: "#fff",
            padding: "5px",
            border: `1px solid ${GREEN_COLOR}`,
            borderRadius: "5px",
            width: "70%",
            marginBottom: "2vh",
            cursor: "pointer",

            transition: "0.5s",
            boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.3)"
        };


        let getAvailButton = () => {
            if (this.props.processing) {
                return (
                    <div style={{textAlign: 'center'}}>
                        <Loader
                            type="Triangle"
                            color={GREEN_COLOR}
                            height="40px"
                            width="40px"/>
                    </div>
                );
            }
            else {


                return (
                    <button className={"lp-packages_btnStyle"} style={btnStyle}
                            onClick={() => {
                                this.toggleModal(membership);
                            }}>
                        {this.props.pack_avail}
                    </button>
                );
            }
        };

        let getDetailButton = () => {
            return (
                <button className={"lp-packages_btnStyle"} style={btnStyle}
                        onClick={() => this.openDetails(membership._id)}>
                    {this.props.pack_details}
                </button>
            );
        };

        return (
            (this.props.activeUser) ? getAvailButton() : getDetailButton()
        )
    };


    renderPackages = (_memberships) => {
        const columnStyle = {
            border: "1px solid rgba(235, 237, 242, 0.2)",
            float: "left",
            width: "500px",
            padding:"unset",
            marginBottom:"-1vw",
            paddingBottom:"1vw"

        };
        let style = {...columnStyle, ...this.props.style};

        let ratesArray = {}, columns = [];
        let generateSpecialRates = () => _memberships.map((membership, index) => {

            return membership.specialRates.map((item) => {

                let abbrevDays = toAbbrevDays(item.days);


                if (!ratesArray[`${abbrevDays} (${item.startTime}-${item.endTime})`]) {
                    ratesArray[`${abbrevDays} (${item.startTime}-${item.endTime})`] = ['', '', '', ''];
                    return ratesArray[`${abbrevDays} (${item.startTime}-${item.endTime})`][index] = item.rate;
                } else {
                    return ratesArray[`${abbrevDays} (${item.startTime}-${item.endTime})`][index] = item.rate;
                }
            })
        });


        generateSpecialRates();
        let days = Object.keys(ratesArray);
        let prices = Object.values(ratesArray);


        const getColumnValues = (index) => {
            for (let j = 0; j < days.length; j++) {
                columns[index][Object.values(columns[index]).length] = prices[j][index];
            }

        };
        const getColumnKeys = () => {
            for (let j = 0; j < days.length; j++) {
                columns[0][Object.values(columns[0]).length] = days[j];
            }

        };


        let memberships = _memberships.map((_membership, index) => {


            let image_name = badgeArray.includes(_membership.name.toLowerCase())?_membership.name.toLowerCase()+".svg":"empty.svg";


            columns.push([]);

            columns[index][0] = _membership.price;


            if (days.length > 0) {
                getColumnValues(index);
            }
            columns[index][Object.values(columns[index]).length] = _membership.rate;

            return {
                imageUrl: `/img/badges/${image_name}`,
                name: _membership.name,
                _id: _membership._id,
                specialRates: _membership.specialRates,
                price: _membership.price

            };
        });

        memberships.unshift(
            {
                imageUrl: `/img/badges/empty.svg`,
                name: this.props.pack_title
            }
        );


        columns.unshift([]);
        columns[0][0] = this.props.pack_lowerText;


        if (days.length > 0) {
            getColumnKeys();
        }
        columns[0][Object.keys(columns[0]).length] = this.props.otherRateText;


        return memberships.map((membership, index) => {

            return (
                <div className={"col"} style={style} key={membership.name}>

                    {this.getPackageBadge(membership)}

                    <ColouredLine style={{opacity: "0.5"}}/>

                    {this.getPackageDetails(columns[index])}

                    {this.getPackageDetailsButton(membership)}
                </div>
            );
        });
    };

    getBody = (memberships) => {

        const _style = {
            backgroundColor: "white",
            borderRadius: "5px",
            paddingBottom:"1vw",
            textAlign: "center",
            boxShadow: "0px 2px 20px 12px rgba(0,0,0, 0.04)",
        };

        let style = {..._style, ...this.props.style};
        const divStyle = {
            padding: '0px 10vw 3%',
            ...this.props.style
        };

        return (
            <div style={divStyle} className="wow fadeInUp">
                <div className={"row"} style={style}>
                    {this.renderPackages(memberships)}
                </div>
            </div>
        );
    };

    render() {
        let {memberships} = this.props;

        if (!memberships || !memberships.length) return null;

        return (
            <div>
                {this.UpgradeMembershipModal()}
                {this.getBody(memberships)}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        pack_title: state.lp_reducer.activeLanguage.packages.pack_title,
        activeUser: state.user_reducer.activeUser,
        pack_description: state.lp_reducer.activeLanguage.packages.pack_description,
        memberships: state.lp_reducer.memberships,
        pack_details: state.lp_reducer.activeLanguage.packages.details,
        pack_avail: state.lp_reducer.activeLanguage.packages.purchase,
        pack_upperText: state.lp_reducer.activeLanguage.packages.pack_upperText,
        pack_lowerText: state.lp_reducer.activeLanguage.packages.pack_lowerText,
        processing: state.user_reducer.processing,
        modalHeading: state.gv_reducer.activeLanguage.hallView.UpgradeMembershipHead,
        modalBody: state.gv_reducer.activeLanguage.hallView.UpgradeMembershipBody,
        modalYesText: state.gv_reducer.activeLanguage.hallView.joinModalYesText,
        modalNoText: state.gv_reducer.activeLanguage.hallView.joinModalNoText,
        otherRateText:state.lp_reducer.activeLanguage.package_details.otherRateText,
    }
};

const mapDispatchToProps = (dispatch) => {

    return {
        changeMembership: (membershipID) => dispatch(changeMembership(membershipID))
    };
};

const connected = connect(mapStateToProps, mapDispatchToProps)(MembershipsDetail);
export default withRouter(connected);