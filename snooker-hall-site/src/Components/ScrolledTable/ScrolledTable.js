import React from 'react';
import ReactTableContainer from "react-table-container";


class ScrolledTable extends React.Component {


    render() {
        const props = this.props
        return (
            <ReactTableContainer width={props.width} height={props.height} style={props.style}>
                {props.children && props.children}
            </ReactTableContainer>
        )
    }
}

export default ScrolledTable