import React from 'react';
import {VerticalTimeline, VerticalTimelineElement} from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import './TimeLine.css';
import connect from 'react-redux/es/connect/connect';

const timeLineData = props => {
    return {
        timeLine: [
            {
                icon: "member",
                title: props.timeline1_title,
                content: props.timeline1_content
            },
            {
                icon: "play",
                title: props.timeline2_title,
                content: props.timeline2_content
            },
            {
                icon: "food",
                title: props.timeline3_title,
                content: props.timeline3_content
            },
            {
                icon: "pay",
                title: props.timeline4_title,
                content: props.timeline4_content
            }
        ]
    }
};

class TimeLine extends React.Component {

    renderTimeLine = () => timeLineData(this.props).timeLine.map(data => {
        return (
            <VerticalTimelineElement
                icon={<img src={"/img/timeline/" + data.icon + ".svg"} alt={'img'}/>}
                key={data.title}
            >
                <p>
                    <span>{data.title}</span>
                    <br/>
                    {data.content}
                </p>
            </VerticalTimelineElement>
        )
    });

    render() {
        return (
            <div className={"col-md-12"}>
                <VerticalTimeline layout={"1-column"}>
                    {this.renderTimeLine()}
                </VerticalTimeline>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        timeline1_title: state.lp_reducer.activeLanguage.howWork.timeline1_title,
        timeline1_content: state.lp_reducer.activeLanguage.howWork.timeline1_content,

        timeline2_title: state.lp_reducer.activeLanguage.howWork.timeline2_title,
        timeline2_content: state.lp_reducer.activeLanguage.howWork.timeline2_content,

        timeline3_title: state.lp_reducer.activeLanguage.howWork.timeline3_title,
        timeline3_content: state.lp_reducer.activeLanguage.howWork.timeline3_content,

        timeline4_title: state.lp_reducer.activeLanguage.howWork.timeline4_title,
        timeline4_content: state.lp_reducer.activeLanguage.howWork.timeline4_content,

    }
};

export default connect(mapStateToProps)(TimeLine);