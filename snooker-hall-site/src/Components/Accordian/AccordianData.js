import React from 'react';

const AccordianData = (props)=>{

    return(
        <div style={props.style}>
            {props.children && props.children}
        </div>
    )
};

export default AccordianData