import React from 'react';
import {GREEN_COLOR, GRAY_COLOR} from "../../utils/css-constants";
import {connect} from 'react-redux';
import {capitalize, formatAmount, formatDate} from "../../utils/common-utils";
import Para from "../Para/Para";

const Wrapper = (props) => {
    let {requiredelement} = props;
    if (requiredelement === 'tr') {
        return <tr {...props}/>;
    } else {
        return <div {...props}/>;
    }
};


class Row extends React.Component {

    headingStyle = {
        textDecoration: "underline",
        marginBottom: "10px",
        fontWeight: "bold"
    };

    leftTextStyle = {
        width: "50%",
        display: "inline-block",
        textAlign: "left"
    };

    rightTextStyle = {
        width: "50%",
        display: "inline-block",
        textAlign: "right"
    };

    leftTaxStyle = {
        width: "50%",
        fontSize: "13px",
        display: "inline-block",
        textAlign: "left",
        fontFamily: "initial"
    };

    rightTaxStyle = {
        width: "50%",
        fontSize: "13px",
        display: "inline-block",
        textAlign: "right",
        fontFamily: "initial"
    };


    getTax = (amount, tax) => {
        let taxCal = 0;
        let taxInclusiveAmount = 0;
        let totalTax = 0;

        taxCal = 1 + tax / 100;
        taxInclusiveAmount = amount / taxCal;
        totalTax = amount - taxInclusiveAmount;
        return totalTax;
    };

    getGameDetails = () => {
        let props = this.props;
        let gameArray = [
            {
                title: props.minutesText,
                data: `${props.minutesPlayed} Mins`
            },

            {
                title: props.freeMinuteText,
                data: `${props.freeMinutes} Mins`
            },

            {
                title: props.billText,
                data: formatAmount(props.billableAmount)
            },

            {
                title: props.gameVatText,
                data: formatAmount(this.getTax(props.billableAmount, props.gameTax))
            },
        ];

        if (props.sessions && props.sessions.length) {
            let gameJSX = gameArray.map((game, index) => {
                return (
                    ((game.title !== "VAT Inclusive") && (game.title !== "Moms ingår")) ?
                        <div key={index}>
                            <Para style={this.leftTextStyle}>
                                {game.title}
                            </Para>

                            <Para style={this.rightTextStyle}>
                                {game.data}
                            </Para>
                        </div> :
                        <div key={index}>
                            <Para style={this.leftTaxStyle}>
                                <span style={{marginRight: '6px'}}>{game.title}</span>
                                <span>{game.data}</span>
                            </Para>
                        </div>
                )
            });
            return (
                <div style={{marginBottom: "20px"}}>
                    <h5 style={this.headingStyle}>
                        {this.props.gameHeading}
                    </h5>
                    {gameJSX}
                </div>
            )
        }

        return null;

    };

    getCartTax = (cart) => {
        let totalTax = 0;
        let itemTax = 0;
        cart.map(cart => {
            itemTax = this.getTax(cart.quantity * cart.price, cart.tax);
            totalTax += itemTax;
        });
        return totalTax;
    };

    getCartDetails = () => {

        const props = this.props;

        let cartFooter = [
            {
                title: props.cartBillText,
                value: formatAmount(props.cartBill)

            },

            {
                title: props.cartVatText,
                value: formatAmount(this.getCartTax(props.cart))
            }
        ];

        if (props.cart && props.cart.length) {

            let cartJSX = props.cart.map((item, index) => {
                return (
                    <div key={index}>
                        <Para style={this.leftTextStyle}>
                            {capitalize(item.name)}
                            <span style={{float: "right"}}>x {item.quantity}</span>
                        </Para>

                        <Para style={this.rightTextStyle}>
                            {formatAmount(item.quantity * item.price)}
                        </Para>
                    </div>
                )
            });

            cartJSX.push(
                <br key={cartJSX.length}/>,

                cartFooter.map((data, index) => {
                    index = cartJSX.length + index + 1;
                    return (
                        ((data.title !== "VAT Inclusive") && (data.title !== "Moms ingår")) ?
                            <div key={index}>
                                <Para style={this.leftTextStyle}>
                                    {data.title}
                                </Para>

                                <Para style={this.rightTextStyle}>
                                    {data.value}
                                </Para>
                            </div> :
                            <div key={index}>
                                <Para style={this.leftTaxStyle}>
                                    <span style={{marginRight: '6px'}}>{data.title}</span>
                                    <span>{data.value}</span>
                                </Para>
                            </div>
                    )
                })
            );


            return (
                <div style={{marginBottom: "20px"}}>
                    <h5 style={this.headingStyle}>
                        {this.props.cartHeading}
                    </h5>
                    {cartJSX}

                </div>
            )
        }

        return null;
    };


    getAccordianData = () => {
        console.log("All Data: ", this.props);
        return (
            <td
                style={{
                    cursor: "default",
                    padding: "5%",
                    backgroundColor: GRAY_COLOR,
                    borderBottom: "7px solid black",
                    width: '100%'
                }}>

                <div className={"row"}>

                    <div className={"col-md-4 offset-md-1"}>
                        <h5 style={{letterSpacing: "1px", WebkitTextStroke: "1px", textTransform: "uppercase"}}>
                            {this.props.billHeading}
                        </h5>


                        <hr style={{borderTop: "dotted 1px #707070"}}/>

                        <div style={{paddingTop: "10%"}}>
                            {this.getGameDetails()}
                            {this.getCartDetails()}
                        </div>


                        {/*Total Bill */}

                        <hr style={{borderTop: "dotted 1px #fff"}}/>

                        <div>
                            <Para style={{fontWeight: "bold", ...this.leftTextStyle}}>
                                Subtotal
                            </Para>

                            <Para style={{color: GREEN_COLOR, ...this.rightTextStyle}}>
                                {formatAmount(this.props.totalBill)}
                            </Para>
                        </div>

                        <hr style={{borderTop: "dotted 1px #fff"}}/>

                    </div>

                    <div className={"col-md-5 offset-md-2"} style={{paddingTop: "10%"}}>
                        <div style={{borderLeft: "1px dotted #707070", paddingLeft: "10px", height: "95%"}}>
                            <p style={{marginBottom: '6px'}}>
                                {this.props.checkInText} : {formatDate(this.props.startDate)}
                            </p>
                            {
                                (!this.props.endDate) ? null :
                                    <span>
                                        <p style={{marginBottom: '6px'}}>
                                        {this.props.checkOutText} : {formatDate(this.props.endDate)}
                                        </p>
                                        <p style={{marginBottom: '6px', marginTop: '20px'}}>
                                            {this.props.previousWalletText} : {formatAmount(this.props.availableWalletBalance)}
                                        </p>

                                        <p style={{marginBottom: '6px'}}>
                                            {this.props.currentWalletText} : {formatAmount(this.props.availableWalletBalance - this.props.totalBill)}
                                        </p>

                                        <p style={{marginBottom: '6px', marginTop: '20px'}}>
                                            {this.props.beforeFreeMinText} : {this.props.availableFreeMinutes}
                                        </p>

                                        <p style={{marginBottom: '6px'}}>
                                            {this.props.afterFreeMinText} : {this.props.availableFreeMinutes - this.props.freeMinutes}
                                        </p>
                                    </span>


                            }

                        </div>
                    </div>
                </div>

            </td>
        )
    };


    // Rendering Method
    render() {
        let {   style, children, onClick, isAccordian} = this.props;
        let _style = {...style};

        if (isAccordian) {
            _style.backgroundColor = GREEN_COLOR;
        }

        return (
            <Wrapper style={_style} onClick={onClick} requiredelement={'tr'}>
                {children && children}
                {
                    isAccordian ?
                        this.getAccordianData()
                        :
                        null
                }
            </Wrapper>
        )
    }
}

const mapStateToProps = (state) => {
    const {sessionHistory} = state.gv_reducer.activeLanguage;
    return {

        billHeading: sessionHistory.detailHeading,

        gameHeading: sessionHistory.gameTitle,
        minutesText: sessionHistory.minutesText,
        freeMinuteText: sessionHistory.freeMinuteText,
        gameVatText: sessionHistory.gameVatText,
        billText: sessionHistory.gameBillText,
        cartVatText: sessionHistory.cartVatText,
        totalCartText: sessionHistory.totalCartText,
        cartBillText: sessionHistory.cartBillText,
        cartHeading: sessionHistory.cartTitle,
        checkInText: sessionHistory.checkInText,
        checkOutText: sessionHistory.checkOutText,
        previousWalletText: sessionHistory.previousWalletBalance,
        currentWalletText: sessionHistory.currentWalletBalance,
        totalGameText: sessionHistory.totalGameText,
        walletText: sessionHistory.walletText,
        beforeFreeMinText: sessionHistory.beforeFreeMinText,
        afterFreeMinText: sessionHistory.afterFreeMinText
    }
};

const connected = connect(mapStateToProps)(Row);
export default connected;