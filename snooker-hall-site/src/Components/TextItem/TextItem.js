import React from 'react';
import {BODY_FONT} from "../../utils/css-constants";

const _style = {
    color: "#fff",
    margin: "unset",
    textTransform: "uppercase",
    fontFamily: BODY_FONT
};

const textItem = (props) => {

    let style = {..._style, ...props.style};

    return (
        <div>
            <span style={style}>
            {props.children && props.children}
            </span>
        </div>

    )
};

export default textItem;