import React from 'react';

const style = {
    background: "rgba(31, 31, 31, 0.9)",
    border: "1px solid  rgba(206, 210, 219,0.7)",
    borderRadius: "7px",
    width: "100%",
    display: "inline-block",
    height: '100%',
    margin: "unset",
    marginTop: "4%"
};

const BlackBG = (props)=>{
    let _style = {
        ...style,
        ...props.style
    };

    return(
        <div style={_style}>
            {props.children && props.children}
        </div>
    )
};

export default BlackBG