import React from 'react';
import './categoryStyle.css';
import {withRouter} from "react-router-dom";
import {logoutUser} from "../../../store/User/user-actions";
import {connect} from 'react-redux';
import {NotificationManager} from "react-notifications";
import axios from "../../../utils/axios";
import {handleError} from "../../../store/store-utils";
import {isInWaitingList} from "../../../utils/common-utils";
import {GREEN_COLOR} from "../../../utils/css-constants";
import Title from "../../../Components/Heading/Title";
import Modal from "../../../Components/Modal/Modal";
import CircularButton from "../../../Components/CircularButton/CircularButton";
import Loader from "react-loader-spinner";
import {ModalBody, ModalFooter, ModalHeader} from 'reactstrap';


class categories extends React.Component{

    state = {
        showLogoutModal: false,
        processing: false,
        isWaitingList: false,
        isActiveSession: false,
        logoutModalHeader: '',
        logoutModalBody: '',
        logoutType: '',
    };


    logoutOpen = (isinWaitingList = false, hasActiveSession = false) => {
        let _state = {...this.state};

        _state.logoutModalHeader = '';
        _state.logoutModalBody = '';
        _state.logoutType = '';


        if (hasActiveSession) {
            _state.logoutModalHeader = this.props.checkoutModalHeader;
            _state.logoutModalBody = this.props.checkoutModalBody;
            _state.logoutType = 'checkout';

        } else if (isinWaitingList) {
            _state.logoutModalHeader = this.props.waitingModalHeader;
            _state.logoutModalBody = this.props.waitingModalBody;
            _state.logoutType = 'waitingList'
        }

        _state.isWaitingList = isinWaitingList;
        _state.isActiveSession = hasActiveSession;

        _state.showLogoutModal = !_state.showLogoutModal;

        this.setState(_state);
    };

    handleLogoutClick = () => {

        axios.get('/session-bill')
            .then(response => {
                const {game} = response.data.session;

                if (game.cart.length || game.sessions.length) {
                    this.logoutOpen(false, true);
                } else if (isInWaitingList(this.props.waitingList, this.props.activeUser)) {
                    this.logoutOpen(true, false);
                } else {
                    this.logoutUser();
                }
            })
            .catch((err) => {
                // let message = handleError(err);
                // NotificationManager.error(message, '', 3000);
                if (isInWaitingList(this.props.waitingList, this.props.activeUser)) {
                    this.logoutOpen(true, false);
                } else {
                    this.logoutUser();
                }
            });


    };

    payBill = () => {
        axios.post('/pay-bill')
            .then(() => {
                this.logoutUser();
            }).catch((err) => {
            let message = handleError(err);
            NotificationManager.error(message, '', 3000);
        });
    };

    removeFromWaitingList = () => {
        axios.delete('/waiting-list')
            .then(() => {
                this.logoutUser();
            }).catch((err) => {
            let message = handleError(err);
            NotificationManager.error(message, '', 3000);
        })
    };

    handleLogoutSubmit = () => {
        if (this.state.isActiveSession) {
            this.payBill();
        } else if (this.state.isWaitingList) {
            this.removeFromWaitingList();
        }
    };

    logoutUser = () => {
        this.props.logoutUser(this.props.history.push('/hall'));
    };

    logoutDialog = () => {
        const buttonStyle = {
            fontWeight: "normal",
            width: "100%",
            color: '#fff',
            padding: "1vh 2vw",
            borderRadius: "5px",
            border: "unset"
        };

        return (
            <Modal isOpen={this.state.showLogoutModal} toggle={() => this.logoutOpen(false, false)}>
                <ModalHeader style={{border: "unset"}} toggle={() => this.logoutOpen(false, false)}>
                    <Title afterClass={"blackDash"}>
                        {this.state.logoutModalHeader}
                    </Title>
                </ModalHeader>

                {this.state.processing ?
                    <div style={{textAlign: 'center'}}>
                        <Loader type="Triangle" color={GREEN_COLOR} height="40px" width="40px"/>
                    </div> :
                    <div>
                        <ModalBody>
                            {this.state.logoutModalBody}
                        </ModalBody>

                        <ModalFooter style={{border: "unset"}}>
                            <CircularButton style={buttonStyle} backColor="#BF1C1C" margin="0px 10px 0px 0px"
                                            onClick={this.logoutUser}>
                                {this.props.logoutCancelBtnText}
                            </CircularButton>

                            <CircularButton style={buttonStyle} backColor="#13C95F" margin="0px 0px 0px 10px"
                                            onClick={this.handleLogoutSubmit}>
                                {this.props.logoutSubmitBtnText}
                            </CircularButton>
                        </ModalFooter>
                    </div>
                }
            </Modal>
        )
    };


     handleClick = (props)=>{

        if(props.link){
            props.history.push(props.link);
        }else{
            this.handleLogoutClick();
        }
    };
     renderCategories = (src, text, link, props) => {

        return (
            <div className={`services-col ${props.hoverClass}`} onClick={() => this.handleClick(props)}>
                <div className="img">
                    <img alt={""} className={"svg"} src={src}/>
                </div>
                <h2 className="title">
                    <strong>{text}</strong>
                </h2>
            </div>
        )
    };

    render() {
        const {style, src, text, link} = this.props;
        return (
            <div id="categories" style={style && style}>
                {this.logoutDialog()}
                {this.renderCategories(src, text, link, this.props)}
            </div>

        );
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        logoutUser: (logoutCallback) => dispatch(logoutUser(logoutCallback))
    }
};

const mapStateToProps = (state) => {
    const nav = state.gv_reducer.activeLanguage.navbar;
    return {

        activeUser: state.user_reducer.activeUser,
        waitingList: state.server_reducer.waitingList,
        playerSession: state.server_reducer.player_session,

        checkoutModalHeader: nav.checkoutModalHeader,
        checkoutModalBody: nav.checkoutModalBody,

        waitingModalHeader: nav.waitingModalHeader,
        waitingModalBody: nav.waitingModalBody,

        logoutSubmitBtnText: nav.logoutSubmitBtnText,
        logoutCancelBtnText: nav.logoutCancelBtnText,
    }
};
const connected = connect(mapStateToProps,mapDispatchToProps)(categories);
export default withRouter(connected);