import React from 'react';
import {BODY_FONT} from "../../../utils/css-constants";

const getContent = () =>{

    let textStyle = {
        color: "#F2F3F8",
        fontFamily: BODY_FONT,
        textAlign: 'left',
        lineHeight:"normal",
        fontSize:"14px",

    };

    return(
        <div style={textStyle}>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Cras eu augue a nibh interdum interdum. Vestibulum vitae tempor nisl, sed ornare massa.
            </p>
            <p>
                Lorem ipsum dolor sit amet, consectetur elit. Cras eu augue a nibh interdum interdum.
                Vestib ulum vitae tempor nisl, sed ornare massa.
                Cras eu augue a nibh interdum interdum.Cras eu augue a nibh interdum interdum.
            </p>
        </div>
    )
};

const Text = () => {


    return (
        <div>
            {getContent()}
        </div>
    )
};

export default Text;