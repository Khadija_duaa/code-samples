import React from 'react';
import {BODY_FONT} from "../../../utils/css-constants";

const setHeading = (props)=> {
    return  <h2 style={{fontWeight:"1000",...props.fontStyle}}>
                {props.title}
            </h2>

};
const Heading = (props) => {

    let headingStyle = {
        color: '#F2F3F8',
        fontFamily: BODY_FONT,
        marginBottom:"10%",
        ...props.style
    };

    return (
        <div style={headingStyle}>
            {setHeading(props)}
        </div>
    )
};

export default Heading;