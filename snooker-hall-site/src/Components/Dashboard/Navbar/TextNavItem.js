import React from 'react';
import {BODY_FONT, GREEN_COLOR} from "../../../utils/css-constants";
import {NavLink} from 'react-router-dom';

const _style = {
    color: "#fff",
    margin: "unset",
    textTransform: "uppercase",
    fontFamily: BODY_FONT,

};

const textNavItem = (props) => {
    let {to, children, active, activeColor} = props;

    let style = {..._style, ...props.style};
    if (active) style.color = activeColor || GREEN_COLOR;

    return (
        <NavLink to={to} style={style}>
            {children && children}
        </NavLink>
    );
};

export default textNavItem;