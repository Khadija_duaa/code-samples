import React from 'react'
import ReactFlagsSelect from 'react-flags-select';
import 'react-flags-select/css/react-flags-select.css';
import connect from 'react-redux/es/connect/connect';
import {setLanguage} from '../../../../store/GameView/gv-actions';

import './Bilingual.css';

const countries = ["US", "SE"];

const Bilingual = (props) => {
    return (
        <div>
            <ReactFlagsSelect
                defaultCountry={mapLanguageToCountry(props.activeLang)}
                countries={countries}
                showSelectedLabel={false}
                showOptionLabel={false}
                onSelect={props.selectLanguage}/>
        </div>
    );
};

const mapCountrytoLang = (country) => {
    let lang = null;

    switch (country) {
        case 'US' :
            lang = 'EN';
            break;
        case 'SE' :
            lang = 'SV';
            break;
        default :
            lang = 'SV';
            break;
    }

    return lang;
};


const mapLanguageToCountry = (lang) => {
    let country = null;

    switch (lang) {
        case 'EN' :
            country = 'US';
            break;
        case 'SV' :
            country = 'SE';
            break;
        default :
            country = 'SE';
            break;
    }

    return country;
};


const mapStateToProps = state => {
    return {
        activeLang: state.gv_reducer.activeLang
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectLanguage: (language) => dispatch(setLanguage(mapCountrytoLang(language)))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Bilingual);
