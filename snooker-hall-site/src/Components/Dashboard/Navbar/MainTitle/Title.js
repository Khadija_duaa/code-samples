import React from 'react';
import connect from 'react-redux/es/connect/connect';
import {HEADING_FONT} from "../../../../utils/css-constants";

const style = {
    textTransform: "uppercase",
    color: "white",
    letterSpacing: "3px",
    fontFamily: HEADING_FONT,
    fontWeight: "bold",
    fontSize: "20px",
    cursor: "pointer",
    margin: "unset"
};

const title = ({title}) => <a onClick={() => window.location.href="/"} style={style}>{title}</a>;

const mapStateToProps = state => {
    return {
        title: state.gv_reducer.activeLanguage.navbar.main_title
    }
};

export default connect(mapStateToProps)(title)