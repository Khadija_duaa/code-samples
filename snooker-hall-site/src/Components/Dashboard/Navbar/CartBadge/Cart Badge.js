import React from 'react';
import Badge from 'material-ui/Badge/Badge';
import {MuiThemeProvider} from 'material-ui';
import {GREEN_COLOR} from "../../../../utils/css-constants";
import {NavLink} from 'react-router-dom';

const style = {
    top: -5,
    right: "unset",
    left: "60%",
    color: "#000",
    backgroundColor: GREEN_COLOR,
    width: "80%",
    height: "70%",
    fontSize: "11px"
};

const cartBadge = (props) => {
    const {active, img, activeImg, to} = props;
    return (
        <NavLink to={to}>
            <MuiThemeProvider>
                <Badge badgeContent={"?"} style={{padding: "unset"}} badgeStyle={style}>
                    <img alt={""} src={active ? activeImg : img} style={{cursor: "pointer"}} width={"20px"}/>
                </Badge>
            </MuiThemeProvider>
        </NavLink>
    )
};

export default cartBadge;