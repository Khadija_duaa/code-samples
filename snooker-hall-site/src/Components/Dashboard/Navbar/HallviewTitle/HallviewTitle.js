import React from 'react';
import {GREEN_COLOR,BODY_FONT} from "../../../../utils/css-constants";
import connect from 'react-redux/es/connect/connect';
import {withRouter} from "react-router-dom";

const textStyle = (isActive)=>{
    return ({
        color:isActive?GREEN_COLOR:"#fff",
        margin:"unset",
        textTransform:"uppercase",
        fontFamily:BODY_FONT
    })
};

const setText = (props) =>  <a href={props.link} style={textStyle(props.isActive)}>{props.hallview_title}</a>;

class HallviewTitle extends React.Component{


    render(){
        return(
            <div>
                {setText(this.props)}

            </div>
        )
    }
}

const mapStateToProps = state =>{
    return{
        hallview_title:state.gv_reducer.activeLanguage.navbar.hallview_title
    }
};

const connected = connect(mapStateToProps) (HallviewTitle);
export default withRouter(connected)