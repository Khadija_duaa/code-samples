import React from 'react';
import TextNavItem from '../TextNavItem';
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";
import UserDropdown from "./UserDropdown";
import Loader from "react-loader-spinner";
import {GREEN_COLOR} from "../../../../utils/css-constants";
import '../../../../utils/cb_Styles.css';

class NavUser extends React.Component{

    getJSX = () => {
        const {activeUser} = this.props;

        let jsx = null;

        if(activeUser){
            jsx = <UserDropdown {...this.props}/>
        }
        else{
            jsx = (
                <TextNavItem to={'/hall/login'}>
                    <span>{this.props.loginTitle}</span>
                </TextNavItem>
            );
        }
        return jsx;
    };

    render(){

        return (
            this.props.isProcessing?
                <div style={{textAlign: 'left'}}>
                    <Loader id={"loader"}
                            color={GREEN_COLOR}
                            width={"20px"}
                            height={"20px"}
                        type="Triangle"/>
                </div>
                :
            <div>
                {this.getJSX()}
            </div>
        )
    }
}


const mapStateToProps = state =>{
    return{
        activeUser:state.user_reducer.activeUser,
        loginTitle:state.lp_reducer.activeLanguage.header.h_signIn,
        isProcessing: state.user_reducer.processing,
    }
};



const connected = connect(mapStateToProps)(NavUser);
export default withRouter(connected);