import React from 'react';
import {connect} from 'react-redux';
import './UserDropdown.css';
import {logoutUser} from "../../../../store/User/user-actions";
import {withRouter} from "react-router-dom";
import Title from "../../../Heading/Title";
import Loader from "react-loader-spinner";
import {GREEN_COLOR} from "../../../../utils/css-constants";
import ErrorMessage from "../../../FormComponents/ErrorMessage/ErrorMessage";
import {Input, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import Modal from "../../../Modal/Modal";
import CircularButton from "../../../CircularButton/CircularButton";
import axios from '../../../../utils/axios';
import {handleError} from "../../../../store/store-utils";
import {SERVER_URL} from "../../../../server";
import {formatName, isInWaitingList} from "../../../../utils/common-utils";
import {NotificationManager} from 'react-notifications';

const imgStyle = {
    width: "35px",
    height: "35px",
    marginRight: "10px",
    borderRadius: "50%",
};


class UserDropdown extends React.Component {

    state = {
        showModal: false,
        showLogoutModal: false,
        oldPassword: '',
        newPassword: '',
        confirmPassword: '',
        processing: false,
        isWaitingList: false,
        isActiveSession: false,
        logoutModalHeader: '',
        logoutModalBody: '',
        logoutType: '',
    };

    open = () => {
        let _state = {...this.state};

        _state.mismatchError = '';
        _state.required = '';
        _state.validationErr = '';
        _state.oldPassword = '';
        _state.newPassword = '';
        _state.confirmPassword = '';

        _state.showModal = !_state.showModal;

        this.setState(_state);
    };

    changePasswordClick = () => {
        this.setState({processing: true, mismatchError: ''});
        let {oldPassword, newPassword, confirmPassword} = this.state;


        const isEmpty = (val) => !val || !val.trim().length;

        if (isEmpty(oldPassword) || isEmpty(newPassword) || isEmpty(confirmPassword)) {
            this.setState({processing: false, required: '*'});
        }
        else {
            axios.put('/change-password', {existingPassword: oldPassword, newPassword: newPassword})
                .then(() => {
                    this.open();
                    this.setState({processing: false, validationErr: ''});
                })
                .catch(error => {
                    const message = handleError(error);
                    this.setState({validationErr: message, processing: false});
                });
        }
    };

    setPassword = (e) => {
        this.setState({[e.target.name]: e.target.value}, () => {
            if (this.state.confirmPassword && this.state.confirmPassword !== this.state.newPassword) {
                this.setState({mismatchError: 'Passwords mismatched'})
            }
            else {
                this.setState({mismatchError: ''})
            }
        });
    };

    changePasswordDialog = () => {
        const buttonStyle = {
            fontWeight: "normal",
            width: "100%",
            color: '#fff',
            padding: "1vh 2vw",
            borderRadius: "5px",
            border: "unset"
        };

        let {oldPassword, newPassword, confirmPassword} = this.state;
        const isEmpty = (val) => !val || !val.trim().length;

        return (
            <Modal isOpen={this.state.showModal} toggle={this.open}>
                <ModalHeader style={{border: "unset"}} toggle={this.open}>
                    <Title afterClass={"blackDash"}>
                        {this.props.passwordDialogTitle}
                    </Title>
                </ModalHeader>

                {this.state.processing ?
                    <div style={{textAlign: 'center'}}>
                        <Loader type="Triangle" color={GREEN_COLOR} height="40px" width="40px"/>
                    </div> :
                    <div>
                        <ModalBody>

                            {this.state.validationErr && !this.state.mismatchError ?
                                <ErrorMessage large
                                              style={{marginBottom: '12px'}}>{this.state.validationErr}</ErrorMessage> : null}

                            <div style={{display: "flex"}}>
                                {isEmpty(oldPassword) ?
                                    <div>{this.state.required &&
                                    <ErrorMessage large style={{
                                        marginBottom: '12px',
                                        marginTop: '20px',
                                        marginRight: '5px'
                                    }}>*</ErrorMessage>}
                                    </div> : null}

                                <Input
                                    style={{border: `${this.state.validationErr ? '1px solid red' : '1px solid #ced4da'}`}}
                                    type={"password"} placeholder={this.props.oldPasswordText}
                                    value={this.state.oldPassword}
                                    autoFocus
                                    onChange={(e) => this.setState({oldPassword: e.target.value})}/>
                            </div>


                            <div style={{display: "flex"}}>
                                {isEmpty(newPassword) ?
                                    <div>{this.state.required &&
                                    <ErrorMessage large
                                                  style={{
                                                      marginBottom: '12px',
                                                      marginTop: '20px',
                                                      marginRight: '5px'
                                                  }}>*</ErrorMessage>}</div> : null}

                                <Input style={{
                                    marginTop: '2%',
                                    border: `${this.state.mismatchError ? '1px solid red' : '1px solid #ced4da'}`
                                }} type={"password"}
                                       placeholder={this.props.newPasswordText}
                                       value={this.state.newPassword}
                                       name={'newPassword'}
                                       onChange={this.setPassword}/>
                            </div>


                            <div style={{display: "flex"}}>
                                {isEmpty(confirmPassword) ?
                                    <div>{this.state.required &&
                                    <ErrorMessage large
                                                  style={{
                                                      marginBottom: '12px',
                                                      marginTop: '20px',
                                                      marginRight: '5px'
                                                  }}>*</ErrorMessage>}</div> : null}
                                <Input style={{
                                    marginTop: '2%',
                                    border: `${this.state.mismatchError ? '1px solid red' : '1px solid #ced4da'}`
                                }} type={"password"}
                                       name={'confirmPassword'}
                                       placeholder={this.props.confirmPasswordText}
                                       value={this.state.confirmPassword}
                                       onChange={this.setPassword}/>
                            </div>

                            {this.state.mismatchError ?
                                <ErrorMessage large
                                              style={{marginBottom: '12px'}}>{this.state.mismatchError}</ErrorMessage> : null}

                        </ModalBody>

                        <ModalFooter style={{border: "unset"}}>
                            <CircularButton style={buttonStyle} backColor="#BF1C1C" margin="0px 10px 0px 0px"
                                            onClick={this.open}>
                                {this.props.cancelBtnText}
                            </CircularButton>

                            <CircularButton style={buttonStyle} backColor="#13C95F" margin="0px 0px 0px 10px"
                                            onClick={this.changePasswordClick}
                                            disabled={
                                                (
                                                    ((this.state.newPassword.length) < 4 && (this.state.newPassword.length > 0)) &&
                                                    ((this.state.confirmPassword.length) < 4 && (this.state.confirmPassword.length > 0))
                                                ) || this.state.mismatchError
                                            }>
                                {this.props.submitBtnText}
                            </CircularButton>
                        </ModalFooter>
                    </div>
                }
            </Modal>
        )
    };


    logoutOpen = (isinWaitingList = false, hasActiveSession = false) => {
        let _state = {...this.state};

        _state.logoutModalHeader = '';
        _state.logoutModalBody = '';
        _state.logoutType = '';


        if (hasActiveSession) {
            _state.logoutModalHeader = this.props.checkoutModalHeader;
            _state.logoutModalBody = this.props.checkoutModalBody;
            _state.logoutType = 'checkout';

        } else if (isinWaitingList) {
            _state.logoutModalHeader = this.props.waitingModalHeader;
            _state.logoutModalBody = this.props.waitingModalBody;
            _state.logoutType = 'waitingList'
        }

        _state.isWaitingList = isinWaitingList;
        _state.isActiveSession = hasActiveSession;

        _state.showLogoutModal = !_state.showLogoutModal;

        this.setState(_state);
    };


    handleLogoutClick = () => {

        axios.get('/session-bill')
            .then(response => {
                const {game} = response.data.session;

                if (game.cart.length || game.sessions.length) {
                    this.logoutOpen(false, true);
                } else if (isInWaitingList(this.props.waitingList, this.props.activeUser)) {
                    this.logoutOpen(true, false);
                } else {
                    this.logoutUser();
                }
            })
            .catch((err) => {
                // let message = handleError(err);
                // NotificationManager.error(message, '', 3000);
                if (isInWaitingList(this.props.waitingList, this.props.activeUser)) {
                    this.logoutOpen(true, false);
                } else {
                    this.logoutUser();
                }
            });


    };

    payBill = () => {
        axios.post('/pay-bill')
            .then(() => {
                this.logoutUser();
            }).catch((err) => {
            let message = handleError(err);
            NotificationManager.error(message, '', 3000);
        });
    };

    removeFromWaitingList = () => {
        axios.delete('/waiting-list')
            .then(() => {
                this.logoutUser();
            }).catch((err) => {
            let message = handleError(err);
            NotificationManager.error(message, '', 3000);
        })
    };

    handleLogoutSubmit = () => {
        if (this.state.isActiveSession) {
            this.payBill();
        } else if (this.state.isWaitingList) {
            this.removeFromWaitingList();
        }
    };

    logoutUser = () => {
        this.props.logoutUser(this.props.history.push('/hall'));
    };

    logoutDialog = () => {
        const buttonStyle = {
            fontWeight: "normal",
            width: "100%",
            color: '#fff',
            padding: "1vh 2vw",
            borderRadius: "5px",
            border: "unset"
        };

        return (
            <Modal isOpen={this.state.showLogoutModal} toggle={() => this.logoutOpen(false, false)}>
                <ModalHeader style={{border: "unset"}} toggle={() => this.logoutOpen(false, false)}>
                    <Title afterClass={"blackDash"}>
                        {this.state.logoutModalHeader}
                    </Title>
                </ModalHeader>

                {this.state.processing ?
                    <div style={{textAlign: 'center'}}>
                        <Loader type="Triangle" color={GREEN_COLOR} height="40px" width="40px"/>
                    </div> :
                    <div>
                        <ModalBody>
                            {this.state.logoutModalBody}
                        </ModalBody>

                        <ModalFooter style={{border: "unset"}}>
                            <CircularButton style={buttonStyle} backColor="#BF1C1C" margin="0px 10px 0px 0px"
                                            onClick={this.logoutUser}>
                                {this.props.logoutCancelBtnText}
                            </CircularButton>

                            <CircularButton style={buttonStyle} backColor="#13C95F" margin="0px 0px 0px 10px"
                                            onClick={this.handleLogoutSubmit}>
                                {this.props.logoutSubmitBtnText}
                            </CircularButton>
                        </ModalFooter>
                    </div>
                }
            </Modal>
        )
    };


    render() {
        let {activeUser, settingsText, changePasswordText, logoutText} = this.props;
        return (
            <div className="user-dropdown">

                {this.changePasswordDialog()}
                {this.logoutDialog()}
                <a href={""} className="dropdown-toggle" data-toggle="dropdown">
                        <span>
                            <img alt={""} src={`${SERVER_URL}/photos/${activeUser.picture}`} style={imgStyle}/>
                            {formatName(activeUser)}
                        </span>
                </a>

                <ul className="dropdown-menu">
                    <li onClick={() => this.props.history.push('/hall/profile')}>
                        <a>
                            <i className="fa fa-gear"/>
                            {settingsText}
                        </a>
                    </li>

                    <li onClick={this.open}>
                        <a>
                            <i className="fa fa-key"/>
                            {changePasswordText}
                        </a>
                    </li>

                    <li onClick={this.handleLogoutClick}>
                        <a>
                            <i className="fa fa-power-off"/>
                            {logoutText}
                        </a>
                    </li>
                </ul>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        logoutUser: (cb) => dispatch(logoutUser(cb))
    }
};

const mapStateToProps = (state) => {
    const nav = state.gv_reducer.activeLanguage.navbar;
    return {
        switchText: nav.switchText,
        settingsText: nav.settingsText,
        changePasswordText: nav.changePasswordText,
        leaveText: nav.leaveText,
        logoutText: nav.logoutText,

        passwordDialogTitle: nav.passwordDialogTitle,
        oldPasswordText: nav.oldPasswordPlaceholder,
        newPasswordText: nav.newPasswordPlaceholder,
        confirmPasswordText: nav.confirmPasswordPlaceholder,

        submitBtnText: nav.submitBtnText,
        cancelBtnText: nav.cancelBtnText,

        activeUser: state.user_reducer.activeUser,
        waitingList: state.server_reducer.waitingList,
        playerSession: state.server_reducer.player_session,

        checkoutModalHeader: nav.checkoutModalHeader,
        checkoutModalBody: nav.checkoutModalBody,

        waitingModalHeader: nav.waitingModalHeader,
        waitingModalBody: nav.waitingModalBody,

        logoutSubmitBtnText: nav.logoutSubmitBtnText,
        logoutCancelBtnText: nav.logoutCancelBtnText,
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(UserDropdown);
export default withRouter(connected);