import React from 'react';
import {GREEN_COLOR,BODY_FONT} from "../../../../utils/css-constants";
import connect from 'react-redux/es/connect/connect';

const textStyle = (isActive)=> {
    return ({
        color:isActive?GREEN_COLOR:"#fff",
        margin:"unset",
        textTransform:"uppercase",
        fontFamily:BODY_FONT
    })
};

const setText = (props) => <a href={props.link} style={textStyle(props.isActive)}>{props.dashboard_title}</a>;

class DashboardTitle extends React.Component{
    render(){
        return(
            <div>
                {setText(this.props)}
            </div>
        )
    }
}

const mapStateToProps = state =>{
    return{
        dashboard_title:state.gv_reducer.activeLanguage.navbar.dashboard_title
    }
};

export default connect(mapStateToProps)(DashboardTitle)