import React from 'react';
import {connect} from 'react-redux';
import NavItem from './NavItem';
import {withRouter} from 'react-router-dom';

class NavMenu extends React.Component {

    state = {
        width:''
    };

    componentDidMount() {
        window.addEventListener('resize',this.resize.bind(this));
        this.resize();
    }

    componentWillUnmount() {
        window.removeEventListener('resize',this.resize);
    }

    resize = ()=>{
        this.setState({width:window.screen.width})
    };

    renderNavBar = () => {
        const navItems = this.getNavItems();
        const navBar = [];

        const navItem_style = {
            margin: this.state.width > 768 ?'0 0.7%':'0 0.4%',
            display: "inline-block",
            cursor: "pointer",
        };

        for (let index = 0; index < navItems.length - 1; index++) {
            const item = navItems[index];
            navBar.push(
                <li style={navItem_style} key={item.key}
                    onClick={() => item.to && this.setState({activeKey: item.key})}>
                    <NavItem {...item}/>
                </li>
            );
        }

        return navBar;
    };

    addInNavItem = (jsx, item) => {
        item.active = item.to === this.props.location.pathname;

        jsx.push(item);

        jsx.push(<span key={`sep-${item.key}`} style={{color: "#707070"}}>|</span>);
    };

    getNavItems = () => {
        let jsx = [];
        const {activeUser} = this.props;

        this.addInNavItem(jsx, {
            key: jsx.length,
            type: 'text',
            children: <span>{this.props.hallview_title}</span>,
            to: '/hall',
        });

        if (activeUser) {
            this.addInNavItem(jsx, {
                key: jsx.length,
                type: 'text',
                children: <span>{this.props.dashboard_title}</span>,
                to: '/hall/dashboard',
            });
        }

        this.addInNavItem(jsx, {
            key: jsx.length,
            type: 'text',
            children: <span>{this.props.onlineMembers_title}</span>,
            to: '/hall/onlineMembers',
            active: false
        });

       /* if (activeUser) {
            this.addInNavItem(jsx, {
                key: jsx.length,
                type: 'badge',
                img: '/FloorView_images/Dashboard/cart.svg',
                activeImg: '/FloorView_images/Dashboard/green_cart.svg',
                to: '/hall/cart',
            });
        }*/

        this.addInNavItem(jsx, {
            key: jsx.length,
            type: 'bilingual',
        });

        this.addInNavItem(jsx, {
            key: jsx.length,
            type: 'user',
        });

        return jsx;
    };

    render() {
        return (
            <ul style={{margin: "unset", textAlign: "right",...this.props.style}}>
                {this.renderNavBar()}
            </ul>
        )
    }
}

const mapStateToProps = (_state) => {
    const {navbar} = _state.gv_reducer.activeLanguage;

    return {
        dashboard_title: navbar.dashboard_title,
        hallview_title: navbar.hallview_title,
        onlineMembers_title: navbar.onlineMembers_title,
        activeUser: _state.user_reducer.activeUser
    };
};

const connectedComponent = connect(mapStateToProps)(NavMenu);

export default withRouter(connectedComponent);