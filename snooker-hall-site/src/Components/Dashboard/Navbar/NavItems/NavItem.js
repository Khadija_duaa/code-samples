import React from 'react';
import TextNavItem from '../TextNavItem';
import CartBadge from '../CartBadge/Cart Badge';
import Bilingual from '../Bilingual/Bilingual';
import NavUser from '../NavUser/NavUser';


const navItem = (props) => {
    let component = null;

    switch (props.type) {
        case  'text' :
            component = <TextNavItem {...props}/>;
            break;
        case  'badge' :
            component = <CartBadge {...props}/>;
            break;
        case  'bilingual' :
            component = <Bilingual {...props}/>
            break;
        case 'user':
            component = <NavUser {...props}/>
            break;
        default :
            component = {...props};
            break;
    }

    return component;
};

export default navItem;