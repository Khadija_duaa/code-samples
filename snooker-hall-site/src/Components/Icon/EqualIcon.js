import React from 'react';
import './style.css'

const EqualIcon = (props)=> <i style={props.style} className={"fa fa-equal"}/>;

export default EqualIcon;