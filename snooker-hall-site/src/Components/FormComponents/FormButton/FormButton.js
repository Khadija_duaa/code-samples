import React from 'react';

const btnStyle = {
    backgroundColor: "#fff",
    color: "#000",
    textAlign: "center",
    padding: "5px",
    width: "50%",
    border: "1px solid #fff",
    borderRadius: "2px",
    margin: "5vh auto",
    marginBottom: "2vh",
    cursor: "pointer",
    fontSize: "12px",
    fontWeight: "700",
    display: "block",
};

const formButton = (props) => {
    return (
        <button style={btnStyle} {...props}>
            {props.children}
        </button>
    );
};

export default formButton;