import React from 'react';

const errorMessage = (props) => {
    let style = {
        fontSize: props.large ? '18px' : "10px",
        marginBottom: "-5px",
        color: "rgb(244, 67, 54)",
        textAlign: props.large ? 'center' : undefined,
        ...props.style
    };

    return (
        <p style={style}>
            {props.children}
        </p>
    );
};

export default errorMessage;