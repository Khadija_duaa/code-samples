import React from 'react';
import './ModalStyle.css';
import {Modal} from 'reactstrap'


class myModal extends React.Component{

    modalStyle = {
        position: 'auto',
        zIndex: 1040,
        top: 0 , bottom: 0, left: 0, right: 0
    };

    dialogStyle = {
        position: 'absolute',
        width: 450,
        border: '1px solid #e5e5e5',
        backgroundColor: 'white',
        boxShadow: '0 5px 15px rgba(0,0,0,.5)',
        padding: 35,
        borderRadius: '5px',
        marginTop:"50%",
        left:50
    };

    render(){
        const {isOpen,toggle,autoFocus} = this.props;
        return(
                <Modal aria-labelledby='modal-label' style={this.modalStyle} isOpen={isOpen} toggle={toggle} autoFocus={autoFocus}>
                    <div style={this.dialogStyle}>
                        {this.props.children && this.props.children}
                    </div>
                </Modal>
            )
    }
}

export default myModal