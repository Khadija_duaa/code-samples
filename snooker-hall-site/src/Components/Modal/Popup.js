import React from 'react';
import {Button, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import Modal from './Modal';
import Title from '../../Components/Heading/Title';
import CircularButton from "../CircularButton/CircularButton";


class ModalExample extends React.Component {

    state = {
        showModal: false
    };

    buttonStyle = {
        fontWeight: "normal",
        width: "100%",
        color: '#fff',
        padding: "1vh 2vw",
        borderRadius: "5px",
        border: "unset"
    };


    open = () => this.setState({showModal: !this.state.showModal});


    orderPopup = () => {
        return (
            <Modal isOpen={this.state.showModal} toggle={this.open}>
                <ModalHeader style={{border: "unset", textAlign: "center", display: "block"}}>
                    <Title>
                        Order Placed
                    </Title>
                </ModalHeader>

                <ModalBody>
                    <p style={{fontSize: 18, textAlign: "center"}}>
                        Your order is placed, you can receive it from Counter.
                    </p>
                </ModalBody>

                <ModalFooter style={{border: "unset"}}>
                    <CircularButton style={this.buttonStyle} backColor="#1C74BF" margin="0px 10px 0px 0px"
                                    onClick={this.open}>
                        Go to Cart
                    </CircularButton>

                    <CircularButton style={this.buttonStyle} backColor="#13C95F" margin="0px 0px 0px 10px"
                                    onClick={this.open}>
                        Dashboard
                    </CircularButton>
                </ModalFooter>
            </Modal>
        )
    };


    paymentDialog = () => {
        return (
            <Modal isOpen={this.state.showModal}>

                <ModalHeader style={{border: "unset"}} toggle={this.open}>
                    <Title afterClass={"blackDash"}>
                        Payment Method
                    </Title>
                </ModalHeader>

                <ModalBody>
                    <p style={{fontSize: 18}}>
                        You currently have 59 kr in wallet. You can pay using your balance form the wallet, or select a
                        different option.
                    </p>
                </ModalBody>

                <ModalFooter style={{display: 'block', border: "unset", textAlign: "center"}}>

                    <CircularButton style={this.buttonStyle} backColor="#13C95F" margin="0px 0px 5% 0px"
                                    onClick={this.open}>
                        Pay via Wallet
                    </CircularButton>

                    <CircularButton style={this.buttonStyle} backColor="#1C74BF" margin="0px 0px 5% 0px"
                                    onClick={this.open}>
                        Pay via Swish
                    </CircularButton>

                    <CircularButton style={this.buttonStyle} backColor="#D96916" margin="0px 0px 5% 0px"
                                    onClick={this.open}>
                        Pay via Credit Card
                    </CircularButton>

                </ModalFooter>
            </Modal>
        )
    };

    paymentConfirmDialog = () => {
        return (
            <Modal isOpen={this.state.showModal}>

                <ModalHeader style={{border: "unset"}} toggle={this.open}>
                    <Title afterClass={"blackDash"}>
                        Thanks for Playing!
                    </Title>
                </ModalHeader>

                <ModalBody>
                    <p style={{fontSize: 18}}>
                        Payment done! You have paid 41 kr, you have 18 kr available in wallet. Stay around!
                    </p>
                </ModalBody>

                <ModalFooter style={{display: 'block', border: "unset", textAlign: "center"}}>

                    <CircularButton style={this.buttonStyle} backColor="#13C95F" margin="0px 0px 5% 0px"
                                    onClick={this.open}>
                        Back To Dashboard
                    </CircularButton>

                    <CircularButton style={this.buttonStyle} backColor="#1C74BF" margin="0px 0px 5% 0px"
                                    onClick={this.open}>
                        Sign Out
                    </CircularButton>

                </ModalFooter>
            </Modal>
        )
    };


    render() {
        return (
            <div>
                {this.reActiveUserDialog()}
                <Button onClick={() => {
                    this.open()
                }}>
                    Open Modal
                </Button>
            </div>
        );
    }
}

export default ModalExample;

