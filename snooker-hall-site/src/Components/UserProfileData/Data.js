import React from 'react';
import {BODY_FONT} from '../../utils/css-constants'
import {withRouter} from "react-router-dom";
import {InputGroup, Button, InputGroupAddon, Input} from 'reactstrap';
import ErrorMessage from '../../Components/FormComponents/ErrorMessage/ErrorMessage';

class TextInput extends React.Component {

    constructor(props){
        super(props);
        this.state = {disable: true, error: null};
    }


    handleClick = () => {
        this.setState({disable: !this.state.disable});
    };

    inputLabel = () => {
        return (
            <div className={"col-md-3"} style={{padding: '1% 0 0 0'}}>
                <p style={{fontFamily: BODY_FONT, color: '#D3D3D3'}}>
                    {this.props.label}
                </p>
            </div>
        );
    };

    onInputChange = (e) => {
        const {value} = e.target;
        let {validate} = this.props;
        if (validate && validate.length) {
            let error = null;

            for (let index = 0; index < validate.length; index++) {
                let validation = validate[index];

                error = validation(value);

                if (error) {
                    break;
                }
            }
            this.setState({error});

            // this.props.onError({[e.target.name]: error});
        }

        this.props.onChange(e);
    };


    renderInputs = () => {
        return (
            <div className={"row"}>
                {this.inputLabel()}

                <div className={"col-md-9"}>
                    <div className={'row'}>
                        <div className={'col-md-12'}>
                            <InputGroup>
                                <Input disabled={this.state.disable}
                                       onChange={this.onInputChange}
                                       value={this.props.value}
                                       name={this.props.name}
                                       style={{
                                           color: `${this.state.disable ? '#fff' : 'black'}`,
                                           backgroundColor: `${this.state.disable ? 'rgba(31, 31, 31, 0.9)' : 'rgb(255,255,255)'}`,
                                           border: '1px solid  rgba(206, 210, 219,0.7)'
                                       }}/>
                                <InputGroupAddon addonType="append">
                                    <Button color="secondary"
                                            style={{height: "100%", border: '1px solid  rgba(206, 210, 219,0.7)'}}
                                            onClick={this.handleClick}>
                                        <i className="fa fa-pencil fa-lg"/>
                                    </Button>
                                </InputGroupAddon>

                            </InputGroup>
                        </div>
                        <div className={'col-md-12'}>
                            <ErrorMessage>
                                {this.state.error || (this.props.error && this.props.error[this.props.name])}
                            </ErrorMessage>
                        </div>
                    </div>
                </div>
            </div>
        )
    };

    render() {
        const style = {...this.props.style};


        return (
            <div style={style}>
                {this.renderInputs()}
            </div>
        );
    }
}

export default withRouter(TextInput);