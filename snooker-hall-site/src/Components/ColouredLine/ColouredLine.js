import React from 'react'

const lineStyle = {
    backgroundColor:"#EFEFEF",
    height:"1",
    width:"100%",
    opacity:"1",

};

const ColoredLine = (props) => (

    <hr style={{...lineStyle,...props.style}}/>
);

export default ColoredLine;