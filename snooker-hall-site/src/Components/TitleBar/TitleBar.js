import React from 'react';
import Title from '../Title/Title';
import TextItem from '../../Components/TextItem/TextItem';

const containerStyle = {
    background: "rgba(31, 31, 31, 0.9)",
    border: "1px solid  rgba(206, 210, 219,0.7)",
    borderRadius: "7px",
    padding: "2% 0 0 2% ",
    width: "100%",
    display: "flex",
    margin: "unset",
    marginTop: "3%",
};

const getWalletBalance = (balance, balanceTitle) => {

    if (balanceTitle) {
        return (
            <TextItem style={{
                borderLeft: "1px solid white",
                marginLeft:"10px",
                paddingLeft:"10px",
                marginRight: '5px',
                fontWeight: 'bold',
                fontStyle: 'italic',
                textTransform: 'none'
            }}>{balanceTitle} : {balance}</TextItem>

        )
    }
    return null;
};

const getWalletFreeMinutes = (freeMin, freeMinTitle) => {

    if (freeMinTitle) {
        return (
            <TextItem style={{
                borderLeft: "1px solid white",
                marginLeft:"10px",
                paddingLeft:"10px",
                marginRight: '5px',
                fontWeight: 'bold',
                fontStyle: 'italic',
                textTransform: 'none'
            }}>{freeMinTitle} : {freeMin}</TextItem>

        )
    }
    return null;
};
const titleBar = ({leftTitle, balance, balanceTitle, leftIcon, rightTitle, rightIcon,freeMinTitle,freeMin}) => {

    return (
        <div className="row" style={containerStyle}>
            <div className={"col-md-8"} style={{display: 'flex'}}>
                <img className={"wallet"} src={leftIcon} alt="userDP"/>
                <div style={{margin: '0 0 0 4%', display: 'flex'}}>
                    <Title>{leftTitle}</Title>
                    {getWalletBalance(balance,balanceTitle)}
                    {getWalletFreeMinutes(freeMin,freeMinTitle)}

                </div>
            </div>

            <div className={"col-md-3 offset-md-1"} style={{display: 'flex'}}>
                {
                    rightIcon ? <img className={"wallet"} src={rightIcon} alt="userDP"/> : null

                }
                <div style={{margin: '0 0 0 6%'}}>
                    <Title>
                        {rightTitle}
                    </Title>
                </div>
            </div>
        </div>
    );
};

export default titleBar;