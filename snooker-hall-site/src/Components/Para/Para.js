import React from 'react';

const Para = (props)=>{

    let _style= {
      marginBottom:"unset",
        ...props.style
    };

    return(
        <p style={_style}>{props.children && props.children}</p>
    )
};

export default Para;