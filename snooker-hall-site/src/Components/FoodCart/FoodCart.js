import React from 'react';
import OnlineMembersComponent from '../../../../Components/OnlineMembers/OnlineMembers';
import connect from "react-redux/es/connect/connect";
import Transition from '../../../../Components/Transition/Transition';

const getOnlineMembers = (members, activeMembers) => {
    /*  return(
          members.map( (data, index) => {
              return (
                  <div className="col-md-3">
                      <OnlineMembersComponent key={index} image={`/FloorView_images/Categories/${data.image}`} name={data.name} age={data.age} />
                  </div>
              )
          })
      )*/

    return (
        members.map((data, index) => {
            let firstName = data.user.firstName.toLowerCase();
            let lastName = data.user.lastName.toLowerCase();

            firstName = firstName.charAt(0).toUpperCase() + firstName.substr(1);
            lastName = lastName.charAt(0).toUpperCase() + lastName.substr(1);

            return (
                <Transition>
                    <OnlineMembersComponent key={index} image={data.profilePicture}
                                            name={`${firstName} ${lastName}`}
                                            age={data.user.phoneNumber} user={data.user} activeUser={activeMembers}/>
                </Transition>
            )
        })
    );
};

class OnlineMembers extends React.Component {

    render() {

        const {membersList, activeMembers} = this.props;

        return (
            <div className="row">
                {getOnlineMembers(membersList, activeMembers)}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        onlineMembersList: state.gv_reducer.activeLanguage.onlineMembers.onlineMembers_List,
        membersList: state.server_reducer.waitingList,
        activeMembers: state.user_reducer.activeUser
    }
};

export default connect(mapStateToProps)(OnlineMembers)