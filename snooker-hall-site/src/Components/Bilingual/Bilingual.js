import React from 'react'
import {connect} from 'react-redux';
import {selectLanguage} from "../../store/LandingPage/lp-actions";
import {GREEN_COLOR} from "../../utils/css-constants";

const activeStyle = {color: GREEN_COLOR};
const inactiveStyle = {fontSize: '12px', fontWeight: 'bold', color: '#fff', marginLeft: '10px', marginRight: '10px'};

const langData = [
    {lang: "EN", img: "/img/eng.png"},
    {lang: "SV", img: "/img/sweden.png"}
];

const bilingual = (props) => {
    return (
        <span style={inactiveStyle}>
            {
                langData && langData.map((lang, index) => {
                    return (
                        <span key={index}>
                            <a className='hover'
                               onClick={() => props.setActiveLang(lang.lang)}
                               style={props.activeLang === lang.lang ? activeStyle : null}>

                                <img src={lang.img}
                                     alt={'lang'}
                                     style={{width: '20px', height: '20px', marginRight : '5px'}}/>
                                {lang.lang}
                            </a>
                            {(index + 1) < langData.length ?
                                <span>/</span> : null}
                        </span>
                    );
                })
            }
        </span>
    );
};

const mapStateToProps = (state) => {
    return {activeLang: state.lp_reducer.activeLang}
};

const mapDispatchToProps = (dispatch) => {
    return {
        setActiveLang: (lang) => dispatch(selectLanguage(lang))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(bilingual);
