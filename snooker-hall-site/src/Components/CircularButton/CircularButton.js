import React from 'react';
import './ButtonStyle.css'

const _buttonStyle= {
    cursor:"pointer",
    transition:"0.5s",
    fontSize:"inherit",
    borderStyle:"none"
};


const CircularButton = (props) => {



    _buttonStyle.margin = props.margin && props.margin;
    if (props.disabled) {
        _buttonStyle.backgroundColor = "grey"
    }
    else {
        _buttonStyle.backgroundColor = props.backColor && props.backColor;
    }

    let style = {
        ..._buttonStyle,
        ...props.style,
    };


    return (
        <button className={"circularButton"} style={style}   onClick={props.onClick} disabled={props.disabled }>
            {props.children && props.children}
        </button>
    );
};

export default CircularButton;