import React from 'react';
import '../../containers/GameView/HallView/WaitingList/WaitingList.css'

class OnlineMembersComponent extends React.Component {

    imageStyle = {
        border: '2px solid white',
        borderRadius: '50%',
        opacity: '0.6',
        width: '50px',
        height: '50px',
        zIndex: '100',
    };

    textStyle = {
        color: 'white',
        fontFamily: 'Montserrat',
        margin: '0px',
        cursor: 'pointer'
    };

    outStyle = {
      margin: '0px 0px 0px 5px'
    };

    isActiveUser = () => this.props.activeUser && this.props.activeUser._id === this.props.user._id;

    getWaitingListClass = () => this.isActiveUser() ? 'WaitingList-active' : 'WaitingList';

    render() {
        const {image, name, price, style, onClick, outerStyle, paddingStyle, paraStyle} = this.props;
        return (
            <div className={"waitListItems"} style={{display: 'flex', marginTop: "2%", paddingBottom: '2%'}}>
                <div className={"col-md-4 "} style={outerStyle || this.outStyle }>
                    <img style={style || this.imageStyle} src={image} alt={''}/>
                </div>

                <div className={`col-md-8 ${this.getWaitingListClass()}`} onClick={onClick} style={paddingStyle? paddingStyle: null}>
                    <p style={this.textStyle}>
                        {name}
                    </p>
                    <p style={paraStyle || this.textStyle}>
                        {price}
                    </p>
                </div>
            </div>
        )
    }
}

export default OnlineMembersComponent;
