import React from 'react';
import {HEADING_FONT} from "../../utils/css-constants";
import './title.css';

const headingStyle = {
    color: "white",
    letterSpacing: "1px",
    fontFamily: HEADING_FONT,
    fontSize: "20px",
    WebkitTextStroke: "1px"
};

const Title = (props) => {
    let style = {
        ...headingStyle,
        ...props.style
    };

    return (
        <div className={"section-header"} >
            <h4 style={style}>
                {props.children}
            </h4>
        </div>

    )
};

export default Title