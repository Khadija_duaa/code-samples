const GET_URL = () => {
    const {REACT_APP_ENV} = process.env;
    let url = '';

    console.log(`NODE_ENV [${REACT_APP_ENV}]`);

    switch (REACT_APP_ENV.trim()) {
        case 'production' :
            url = '/';
            break;
        case 'heroku' :
            url = 'https://snooker247-backend.herokuapp.com';
            break;
        case 'qa' :
            url = 'http://192.168.1.134:8080';
            break;
        case 'dev' :
            url = 'http://localhost:5000';
            break;
        default :
            break;
    }



    return url;
};

export const SERVER_URL = GET_URL();