import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import lp_reducer from './store/LandingPage/lp-reducer';
import user_reducer from './store/User/user-reducer';
import server_reducer from './store/server/server-reducer';
import gv_reducer from './store/GameView/gv-reducer';
import {reducer as formReducer} from 'redux-form'
import connectSocket from './socket-io/socket-io';

export const socket = connectSocket();

const reducer = combineReducers({
    lp_reducer,
    user_reducer,
    server_reducer,
    gv_reducer,
    form: formReducer
});

export const store = createStore(reducer, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>, document.getElementById('root')
);
