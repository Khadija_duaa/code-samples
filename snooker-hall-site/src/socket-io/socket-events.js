export const NEW_CONNECTION = 'connect';
export const DISCONNECTION = 'disconnect';

export const NEW_MESSAGE = 'newMessage';
export const SYNC_MEMBERSHIPS = 'syncMemberships';
export const SYNC_WAITING_LIST = 'syncWaitingList';

export const REQUEST_IN_HALL_ACCESS = 'requestInHallAccess';
export const GRANTED_IN_HALL_ACCESS = 'grantedInHallAccess';

export const SYNC_TABLES = 'syncTables';
export const SYNC_HALL_USERS = 'syncHallUsers';

export const SYNC_SERVER_CONFIGURATIONS = 'syncServerConfigurations';