import io from 'socket.io-client';
import * as events from './socket-events';
import {store} from '../index';
import {setMemberships} from '../store/LandingPage/lp-actions';
import {
    setWaitingList,
    setHallAccessToken,
    setHallTables,
    setServerConfigurations
} from '../store/server/server-actions';
import {setUsers, logoutUser} from "../store/User/user-actions";
import {SERVER_URL} from "../server";
import moment from 'moment';

const setup = () => {
    const socket = io(SERVER_URL, {
        transports: ['websocket']
    });

    socket.on(events.NEW_CONNECTION, () => {
        console.log('##Socket## Established with server');
    });

    socket.on(events.DISCONNECTION, () => {
        console.log('##Socket## disconnected from server');
    });

    socket.on(events.NEW_MESSAGE, (message) => {
        console.log('##Socket## New Message', message);
    });

    socket.on(events.SYNC_MEMBERSHIPS, (data) => {
        if (data && data.memberships) {
            let {dispatch} = store;
            dispatch(setMemberships(data));
        }
    });

    socket.on(events.SYNC_WAITING_LIST, (data) => {
        if (data && data.waitingList) {
            let {dispatch} = store;
            const waitingList = data.waitingList.sort((a, b) => moment(a.joinedTime).isBefore(moment(b.joinedTime)) ? -1 : 1);
            dispatch(setWaitingList(waitingList));
        }

    });

    socket.on(events.SYNC_TABLES, (data) => {
        if (data && data.tables) {
            let {dispatch} = store;
            dispatch(setHallTables(data));

        }

    });

    socket.on(events.SYNC_HALL_USERS, (data) => {
        if (data && data.hallUsers) {
            let {dispatch} = store;
            dispatch(setUsers(data.hallUsers));
        }

    });


    socket.on(events.GRANTED_IN_HALL_ACCESS, (data) => {
        if (data && data.hallAccessToken) {
            const {dispatch} = store;
            dispatch(setHallAccessToken(data.hallAccessToken));
            dispatch(logoutUser(null,true));
        }

    });

    socket.on(events.SYNC_SERVER_CONFIGURATIONS, (data) => {


        if (data && data.configurations) {

            const {dispatch} = store;
            dispatch(setServerConfigurations(data.configurations));
        }
    });


    return socket;
};

export default setup;