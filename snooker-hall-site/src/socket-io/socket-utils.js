import {socket} from '../index';
import * as events from './socket-events';

export const requestInHallAccess = (username, password, cb) => {
    socket.emit(events.REQUEST_IN_HALL_ACCESS, {username, password}, (errorCode) => {
        if (!errorCode) {
            cb && cb();
        }
        else {
            cb && cb(errorCode);
            console.error('Request rejected with errorCode', errorCode);
        }
    });
};