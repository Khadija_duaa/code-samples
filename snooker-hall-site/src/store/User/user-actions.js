import axios from '../../utils/axios';
import * as actions from './user-action-types';
import {NotificationManager} from 'react-notifications';

import {handleError} from "../store-utils";


export const resetRedux = () => {
    return {
        type: actions.RESET_USER_STATE,
    }
};

const setProcessing = (processing) => {
    return {
        type: actions.PROCESSING,
        payload: processing
    };
};

export const saveData = (data) => {
    return {
        type: actions.SAVE_FORM_DATA,
        payload: data
    }
};

export const changeMembership = (membershipID) => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));
        axios.put(`/change-membership/${membershipID}`)
            .then(response => {
                const activeUser = {...getState().user_reducer.activeUser, ...response.data.user};
                dispatch(setActiveUser(activeUser));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setProcessing(false));
                let message = handleError(err);
                console.error(message, err);
                NotificationManager.error(message, '', 3000);
            });
    };
};

export const forgotPassword = (formdata,cb,errorCallback) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(`/forgot-password?email=${formdata.email}`)
            .then(response => {

                dispatch(setProcessing(false));
                cb && cb();
            })
            .catch(err => {
                errorCallback && errorCallback({_error: handleError(err)});
                dispatch(setProcessing(false));
            });
    };
};

export const setProfilePic = (picture) => {
    return (dispatch, getState) => {
        const activeUser = {...getState().user_reducer.activeUser};
        activeUser.picture = picture;

        dispatch(setActiveUser(activeUser));
    };
};


export const setProfile = (user) => {

    return (dispatch, getState) => {
        const activeUser = {...getState().user_reducer.activeUser, ...user};


        dispatch(setActiveUser(activeUser));
    };
};

export const updateProfile = () => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));

        const activeUser = getState().user_reducer.activeUser;
        const jwt = activeUser && activeUser.jwt;

        if (jwt) {
            axios.get('/me')
                .then(response => {
                    const {user} = response.data;
                    dispatch(loginUser(user, jwt));
                    dispatch(setProcessing(false));
                })
                .catch(err => {
                    const message = handleError(err);
                    NotificationManager.error(message, '', 3000);
                    dispatch(setProcessing(false));
                });
        }
    };
};

const updateUser = (verificationCode, cb, errorCallback) => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));

        const data = {...getState().user_reducer.formData};
        if (verificationCode !== 'NOT_REQUIRED') {
            data.verificationCode = verificationCode;
        }

        axios.put('/update-profile', data)
            .then(response => {


                dispatch(setProcessing(false));
                dispatch(setProfile(response.data.user));

                cb && cb();
            })
            .catch(err => {
                errorCallback && errorCallback({_error: handleError(err)});
                dispatch(setProcessing(false));
            });
    };
};

export const sendUpdateCodes = (callback, cb, errorCallback) => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));

        const {activeUser, formData} = getState().user_reducer;
        const {email, phoneNumber} = formData;

        const isVerificationRequired = (user) => user.email !== email || user.phoneNumber !== phoneNumber;

        const axiosPromise = isVerificationRequired(activeUser) ? axios.post('/verify-update', {
            email,
            phoneNumber
        }) : Promise.resolve({data: {verificationCode: "NOT_REQUIRED"}});

        axiosPromise.then((response) => {
            const {verificationCode, _id, emailVerificationRequired, smsVerificationRequired} = response.data;

            // Call Register
            if (verificationCode) {
                dispatch(updateUser(verificationCode, cb, errorCallback));
            }

            // OTP for Verification
            else {
                callback && callback({
                    emailVerificationRequired,
                    smsVerificationRequired,
                    _id,
                    callback: cb,
                    next: updateUser
                });
                dispatch(setProcessing(false));
            }
        }).catch((err) => {
            const message = handleError(err);

            if (err.response && err.response.data && err.response.data.error && err.response.data.error.param) {
                const {param} = err.response.data.error;
                const errors = {};
                errors[param] = message;
                errorCallback && errorCallback(errors);
            }
            else {
                errorCallback && errorCallback({_error: message});
            }

            dispatch(setProcessing(false));
        });
    }
};


export const sendCodes = (callback, cb, errorCallback) => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));

        let {email, phoneNumber} = getState().user_reducer.formData;

        axios.post('/verify', {email, phoneNumber})
            .then((response) => {
                let {verificationCode, _id, emailVerificationRequired, smsVerificationRequired} = response.data;

                // Call Register
                if (verificationCode) {
                    dispatch(register(verificationCode, cb, errorCallback));
                }

                // OTP for Verification
                else {
                    callback && callback({
                        emailVerificationRequired,
                        smsVerificationRequired,
                        _id,
                        callback: cb,
                        next: register
                    });
                    dispatch(setProcessing(false));
                }
            })
            .catch((err) => {
                const message = handleError(err);

                if (err.response && err.response.data && err.response.data.error && err.response.data.error.param) {
                    const {param} = err.response.data.error;
                    const errors = {};
                    errors[param] = message;
                    errorCallback && errorCallback(errors);
                }
                else {
                    errorCallback && errorCallback({_error: message});
                }

                dispatch(setProcessing(false));
            });
    }
};

const register = (verificationCode, callback, errorCallback) => {
    return (dispatch, getState) => {
        const {formData} = getState().user_reducer;

        axios.post(`/register/${verificationCode}`, formData)
            .then((response) => {
                const {user} = response.data;
                const jwt = response.headers['x-auth'];



                if (user && jwt) {
                    dispatch(loginUser(user, jwt));
                }

                callback && callback();

                dispatch(setProcessing(false));
            })
            .catch((err) => {
                errorCallback && errorCallback({_error: handleError(err)});
                dispatch(setProcessing(false));
            })
    }
};

export const verifyCodes = (codes, _id, next, callback, errCallback) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.post(`/check-otp/${_id}`, codes)
            .then((response) => {
                let {verificationCode} = response.data;

                dispatch(next(verificationCode, callback, errCallback));
            })
            .catch((err) => {
                let message = handleError(err);
                errCallback && errCallback({_error: message});
                dispatch(setProcessing(false));

            })
    }
};


const handleLoginResp = (dispatch, response, cb) => {
    const {user} = response.data;
    const jwt = response.headers['x-auth'];

    dispatch(loginUser(user, jwt));

    cb && cb();

    dispatch(setProcessing(false));
};

export const login = (formData, cb, callbackError) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.post('/login', formData)
            .then((response) => handleLoginResp(dispatch, response, cb))
            .catch((err) => {
                callbackError && callbackError({_error: handleError(err)});
                dispatch(setProcessing(false));
            });
    };
};

export const reActiveUser = (_id, password, cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.post(`/login/${_id}`, {password})
            .then((resp) => handleLoginResp(dispatch, resp, cb))
            .catch(() => {
                cb && cb("Invalid Password");
                dispatch(setProcessing(false));
            });
    };
};

export const setUsers = (users) => {
    return (dispatch, getState) => {
        let {activeUser} = getState().user_reducer;
        let {hallAccessToken} = getState().server_reducer;

        if (activeUser && hallAccessToken) {
            let hallActiveUsers = users.filter(hallUser => hallUser.user._id === activeUser._id);
            if (!hallActiveUsers || !hallActiveUsers.length) {

                dispatch(setActiveUser(null));
            }
        }

        dispatch({
            type: actions.SET_USERS,
            payload: users
        });
    };


};

const setActiveUser = (payload) => {
    return {
        type: actions.SET_ACTIVE_USER,
        payload
    };
};

const loginUser = (user, jwt) => {
    return (dispatch) => {
        const preparedUser = {...user, jwt};
        dispatch(setActiveUser(preparedUser));
    };
};


export const logoutUser = (callback , adminAccessLogout = false, leaveDevice=false) => {

    return (dispatch, getState) => {

        // let {activeUser} = getState().user_reducer;
        // let {hallAccessToken,waitingList} = getState().server_reducer;
        dispatch(setProcessing(true));

        setTimeout(() => {
            dispatch(setActiveUser(null));
            callback && callback();
            dispatch(setProcessing(false));
        }, 500);

        // if (activeUser) {
        //
        //     dispatch(setProcessing(true));
        //
        //     if(hallAccessToken && !adminAccessLogout && leaveDevice){
        //         let url = '/session-bill';
        //
        //         axios.get(url)
        //             .then(response => {
        //
        //                 const {game} = response.data.session;
        //
        //                 if(!game.cart.length && !game.sessions.length && !isInWaitingList(waitingList,activeUser)){
        //
        //
        //                     axios.post('/pay-bill').then(()=>{
        //
        //                         setTimeout(() => {
        //                             dispatch(setActiveUser(null));
        //                             callback && callback();
        //                             dispatch(setProcessing(false));
        //                         }, 500);
        //
        //                     }).catch((err) => {
        //                         let message =  handleError(err);
        //                         NotificationManager.error(message, '', 3000);
        //                     });
        //                 }
        //                 else{
        //                     setTimeout(() => {
        //                         dispatch(setActiveUser(null));
        //                         callback && callback();
        //                         dispatch(setProcessing(false));
        //                     });
        //                 }
        //             }).catch((err) => {
        //             let message =  handleError(err);
        //             NotificationManager.error(message, '', 3000);
        //
        //         });
        //     }else{
        //         setTimeout(() => {
        //             dispatch(setActiveUser(null));
        //             callback && callback();
        //             dispatch(setProcessing(false));
        //         }, 500);
        //     }
        //
        // }
    };
};


