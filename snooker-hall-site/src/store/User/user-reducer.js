import * as actions from './user-action-types';

const getActiveUser = () => {
    let userStr = sessionStorage.getItem('activeUser');
    if (userStr) return JSON.parse(userStr);
    return null;
};


const initState = () => {
    return {
        processing: false,
        message: null,
        error: null,
        formData: null,
        users: [],
        activeUser: getActiveUser(),

    };
};

const user_reducer = (state = initState(), action) => {
    let newState = {...state};

    switch (action.type) {
        case actions.SET_USERS:
            setUsers(newState, action.payload);
            break;

        case actions.SET_ACTIVE_USER:
            setActiveUser(newState, action.payload);
            break;

        case actions.PROCESSING:
            setProcessing(newState, action.payload);
            break;

        case actions.SAVE_FORM_DATA:
            newState.formData = action.payload;
            break;

        case actions.RESET_USER_STATE:
            resetRedux(newState);
            break;
        default :
            break;
    }

    return newState;
};

const setUsers = (state, users) => {
    users.forEach(item => {
        item.user.picture += `?time=${Date.now()}`;
    });
    state.users = users;
};

const setActiveUser = (state, user) => {
    if (user) {
        state.activeUser = {...user};
        state.activeUser.picture += `?time=${Date.now()}`;
    }
    else {
        state.activeUser = null;
    }

    sessionStorage.setItem('activeUser', JSON.stringify(user));
};

const resetRedux = (state) => {
    state.formData = null;
    state.activeUser = null;
};

const setProcessing = (state, processing) => {
    state.processing = processing;
};

export default user_reducer;

