import * as types from './gv-types';

const LS_KEY_ACTIVE_LANG = 'ACTIVE_LANGUAGE';
const getActiveLanguage = () => localStorage.getItem(LS_KEY_ACTIVE_LANG) || "SV";

const initState = () => {
    let initState = {
        activeLang: '',
        activeLanguage: {},
        SV: {
            upgradeMembership: {
                title: "+ Uppgradera medlemskap, se paket",
                label: "Uppgradera ditt medlemskap",
            },
            navbar: {

                main_title: "Snooker247",
                dashboard_title: "Instrumentbräda",
                hallview_title: "HallView",
                onlineMembers_title: "Hall Medlemmar",
                basicInfo_title: "Grundläggande information",
                membership_title: "Medlemskap",
                wallet: "Plånbok Balanace",

                switchText: "Ändra användare",
                settingsText: "Kontoinställningar",
                changePasswordText: "Ändra Lösenord",
                leaveText:"Lämna enheten",
                logoutText: "Logga ut",


                passwordDialogTitle: "Ändra Lösenord",
                oldPasswordPlaceholder: "Gammalt Lösenord",
                newPasswordPlaceholder: "Nytt Lösenord",
                confirmPasswordPlaceholder: "Bekräfta Lösenord",

                submitBtnText: "Lämna",
                cancelBtnText: "Annullera",
                freeMin: "Gratis minuter",

                checkoutModalHeader:"Checkout Bekräftelse",
                checkoutModalBody:"Vill du fortsätta till kassan?",

                waitingModalHeader:"Väntelista bekräftelse",
                waitingModalBody:"Vill du ta bort dig från väntelistan efter att du loggat ut?",

                logoutSubmitBtnText: "Ja visst.",
                logoutCancelBtnText: "Nej tack.",
            },

            wallet: {
                title: 'Min plånbok',
                freeMinuteText: "Gratis minuter",
                transactions: 'Transaktioner',
                headers: ["Datum", "Transaktions Av", "Syfte", "Källa", "Belopp"],
                top_up: 'Top Up',
                membership: 'Medlemskapsändring',
                payment: 'Betalning',
                topUp: 'TopUp Balance',
                wallet: 'Plånbok',
                freeMinutes: 'Gratis minuter'
            },

            dashboard: {
                categoryDashboard_Array: [
                    {
                        src: 'play.svg',
                        text: 'Spela spel',
                        link: "/hall",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'wallet.svg',
                        text: 'Visa plånboken',
                        link: "/hall/wallet",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'history.svg',
                        text: 'Visa historia',
                        link: "/hall/history",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'profile.svg',
                        text: 'Visa profil',
                        link: "/hall/profile",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'food.svg',
                        text: 'Beställa mat',
                        link: "/hall/cart/items",
                        hoverClass: "green-service"
                    },
                ],
                orderFoodObject: {
                    src: 'food.svg',
                    text: 'Beställa mat',
                    link: '/hall/cart/items',
                    hoverClass: "green-service"
                },
                logoutObject: {
                    src: 'logout.svg',
                    text: "Loga ut",
                    link: '',
                    hoverClass: "red-service"
                }
            },
            hallView: {

                categoryHallView_Array: [
                    {
                        src: 'food.svg',
                        text: 'Beställa mat',
                        link: '/hall/cart/items',
                        hoverClass: "green-service"
                    },
                    {
                        src: 'profile.svg',
                        text: 'Visa profil',
                        link: "/hall/profile",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'history.svg',
                        text: 'Visa historia',
                        link: "/hall/history",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'checkout.svg',
                        text: 'Checka ut',
                        link: '/hall/cart/checkout',
                        hoverClass: "red-service"
                    },
                ],
                orderFoodObject: {
                    src: 'food.svg',
                    text: 'Beställa mat',
                    link: '/hall/cart/items',
                    hoverClass: "green-service"
                },
                profileObject: {
                    src: 'profile.svg',
                    text: 'Visa profil',
                    link: "/hall/profile",
                    hoverClass: "green-service"
                },
                hallMemberObject: {
                    src: 'profile.svg',
                    text: 'Hall Members',
                    link: "/hall/onlineMembers",
                    hoverClass: "green-service"
                },
                waitingTitle: "Väntelista",
                waitingItems: [
                    {
                        image: 'user1.jpg',
                        text: 'Henry O.',
                        time: '42 MINS'
                    },
                    {
                        image: 'user2.jpg',
                        text: 'Benjamin F.',
                        time: '62 MINS'
                    },
                    {
                        image: 'user3.jpg',
                        text: 'Schneider M.',
                        time: '38 MINS'
                    }
                ],
                addText: "Lägg till mig i kö",
                removeText: "Ta bort från kö",

                UpgradeMembershipHead: "Är du säker, vill du uppgradera ditt medlemskap?",
                UpgradeMembershipBody: "Du kommer att få betala",

                joinModalHead: "Vill du stanna i kö?",
                joinModalBody: "Om du vill byta bord eller spela med någon annan, kan du stanna i listan.",
                joinModalYesText: "Ja visst.",
                joinModalNoText: "Nej tack.",

                inHallText: "In-hall användare",
                outHallText: "Out-hall användare"

            },

            onlineMembers: {
                onlineMembers_title: "Hall Medlemmar",
                reActiveDialogHeading: "Skriv in lösenord",
                activeText: "Aktivera",
                cancelText: "Annullera"
            },
            userProfile: {
                userProfile_title: "Min profil",
                confirmText: "Bekräfta",
                cancelText: "Annullera"
            },
            foodCart: {
                foodItem_title: "Food Varor",
                foodCart_title: "Vagn",
            },

            billSummary: {
                title: "Bill Sammanfattning / Varukorg",
                foodItembtnText: "Lägg till produkt (er)",
                tableHeaders: ["Namn", "Kategori", "Kvantitet", "Belopp"],
                payButtonText: "Betala Nu",
                foodTotalText: "Varukorg Totalt",
                gameTotalText: "Spel Totalt",
                subTotalText: "Delsumma",

            },

            checkoutSummary: {
                title: "Kassa detaljer",
                foodItembtnText: "Lägg till produkt (er)",
                historyBtnText: "Tillbaka till historia",
                tableHeaders: ["Typ", "Mängd ", "Detaljer"],
                detailText: "Detalj",

                checkoutButtonText: "Checka ut",
                payBtnText: "Betala nu",
                subTotalText: "Total belopp",

                prevBillText: "Tidigare Betald Bill",
                prevFreeMinutesText: "Tidigare Gratis Minuter Belopp",
                prevWalletBillText: "Föregående Wallet Bill",

                dialogTitle: "Checkout Bekräftelse",
                dialogBody: "Är du säker på att du vill kolla??",
                dialogCheckoutBtnText: "Ja",
                dialogHalviewBtnText: "Nej",

            },

            currentMembership: {
                changePicture:'Byt',
                currentTitle: 'Nuvarande medlemskap\n',
                name: 'Medlemsnamn',
                negativeBalanceLimit: 'Negativ balansgräns',
                price: 'Prenumeration Pris (en gång)',
                rate: 'Timpriser'
            },

            membershipHistory: {
                historyTitle: 'Medlemskapshistoria',
                name: 'Medlemsnamn',
                subscriptionDate: 'Prenumerationsdatum',
                expirationDate: 'Utgångsdatum',
                price: 'Prenumeration Pris',
                rate: 'Betygsätta'
            },

            sessionHistory: {
                heading: "Historia",
                tableHeads: ["Checka in", "Checka ut", "Belopp", "Protokoll spelas", "Detalj"],
                detailHeading: "Bill Sammanfattning",

                paidText: "Detalj",
                payBtnText: "Betala Nu",

                checkInText: "Checka in",
                checkOutText: "Checka ut",
                transactionText: "Transaktions ID",
                previousWalletBalance: "Tillgänglig plånbokssaldo",
                currentWalletBalance: "Resterande plånbokssaldo",

                gameTitle: "Spel",
                minutesText: "Protokoll spelas",
                freeMinuteText: "Gratis minuter",
                gameVatText: "Moms ingår",
                gameBillText: "Räkningen",

                cartVatText: "Moms ingår",
                cartBillText: "Varukorg ",
                cartTitle: "Varukorg",
                totalCartText: "Total kundvagn Bill",

                totalGameText: "Totala spelräkningen",
                walletText: "Plånbok Detaljer",
                beforeFreeMinText: "Tillgängliga gratis minuter",
                afterFreeMinText: "Återstående fria protokoll"
            },
        },

        EN: {
            upgradeMembership: {
                title: "+ Upgrade Membership, see Packages",
                label: "Upgrade your Membership",
            },
            navbar: {
                main_title: "Snooker247",
                dashboard_title: "Dashboard",
                hallview_title: "HallView",
                onlineMembers_title: "Hall Members",
                basicInfo_title: "Basic Info",
                membership_title: "Membership",

                switchText: "Switch User",
                settingsText: "Account Settings",
                changePasswordText: "Change Password",
                leaveText:"Leave the device",
                logoutText: "Sign out",

                passwordDialogTitle: "Change Password",
                oldPasswordPlaceholder: "Old Password",
                newPasswordPlaceholder: "New Password",
                confirmPasswordPlaceholder: "Confirm Password",
                submitBtnText: "Submit",
                cancelBtnText: "Cancel",
                wallet: "Wallet Balance",
                freeMin: "Free Minutes",

                checkoutModalBody:"Do you want to proceed to checkout?",
                checkoutModalHeader:"Checkout Confirmation",

                waitingModalBody:"Do you want to remove yourself from waiting list after logging out?",
                waitingModalHeader:"Waiting List Confirmation",

                logoutSubmitBtnText:"Yes, Sure",
                logoutCancelBtnText:"No, thanks",
            },

            wallet: {
                freeMinuteText: "Free Minutes",
                title: 'My Wallet',
                transactions: 'Transactions',
                headers: ["Date", "Transaction By", "Purpose", "Source", "Amount"],
                top_up: 'Top Up',
                membership: 'Membership Change',
                payment: 'Payment',
                topUp: 'TopUp Balance',
                wallet: 'Wallet',
                freeMinutes: 'Free Minutes'
            },

            dashboard: {
                categoryDashboard_Array: [
                    {
                        src: 'play.svg',
                        text: 'Play Games',
                        link: "/hall",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'wallet.svg',
                        text: 'Check Wallet',
                        link: "/hall/wallet",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'history.svg',
                        text: 'Show History',
                        link: "/hall/history",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'profile.svg',
                        text: 'View Profile',
                        link: "/hall/profile",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'food.svg',
                        text: 'Order Food',
                        link: "/hall/cart/items",
                        hoverClass: "green-service"
                    },
                ],
                orderFoodObject: {
                    src: 'food.svg',
                    text: 'Order Food',
                    link: '/hall/cart/items',
                    hoverClass: "green-service"
                },
                logoutObject: {
                    src: 'logout.svg',
                    text: "Logout",
                    link: '',
                    hoverClass: "red-service"
                }
            },

            hallView: {

                categoryHallView_Array: [
                    {
                        src: 'food.svg',
                        text: 'Order Food',
                        link: '/hall/cart/items',
                        hoverClass: "green-service"
                    },
                    {
                        src: 'profile.svg',
                        text: 'View Profile',
                        link: "/hall/profile",
                        hoverClass: "green-service"
                    },
                    {
                        src: 'history.svg',
                        text: 'Show History',
                        link: "/hall/history",
                        hoverClass: "green-service"

                    },
                    {
                        src: 'checkout.svg',
                        text: 'Check out',
                        link: '/hall/cart/checkout',
                        hoverClass: "red-service"
                    },
                ],
                orderFoodObject: {
                    src: 'food.svg',
                    text: 'Order Food',
                    link: '/hall/cart/items',
                    hoverClass: "green-service"
                },
                profileObject: {
                    src: 'profile.svg',
                    text: 'View Profile',
                    link: "/hall/profile",
                    hoverClass: "green-service"
                },
                hallMemberObject: {
                    src: 'profile.svg',
                    text: 'Hall Members',
                    link: "/hall/onlineMembers",
                    hoverClass: "green-service"
                },
                waitingTitle: "Waiting List",
                waitingItems: [
                    {
                        image: 'user1.jpg',
                        text: 'Henry O.',
                        time: '42 MINS'
                    },
                    {
                        image: 'user2.jpg',
                        text: 'Benjamin F.',
                        time: '62 MINS'
                    },
                    {
                        image: 'user3.jpg',
                        text: 'Schneider M.',
                        time: '38 MINS'
                    }
                ],
                addText: "Add me in Queue",
                removeText: "Remove from Queue",
                UpgradeMembershipHead: "Are you sure, you want to upgrade membership?",
                UpgradeMembershipBody: "You will be charged",
                joinText: "Joined",
                joinModalHead: "Do you want to stay in Queue?",
                joinModalBody: "If you want to switch your table, or play with someone else, you can stay in list.",
                joinModalYesText: "Yes, sure.",
                joinModalNoText: "No, Thanks.",


                inHallText: "In-hall Users",

                outHallText: "Out-hall Users"
            },

            onlineMembers: {
                onlineMembers_title: "Hall Members",
                reActiveDialogHeading: "Enter Password",
                activeText: "Set Active",
                cancelText: "Cancel"
            },


            userProfile: {
                userProfile_title: "My Profile",
                confirmText: "Confirm",
                cancelText: "Cancel"
            },
            foodCart: {
                foodItem_title: "Food Items",
                foodCart_title: "Cart",
            },

            billSummary: {
                title: "Bill Summary / Cart",
                foodItembtnText: "Add Item(s)",
                tableHeaders: ["Name", "Category", "Quantity", "Amount"],
                payButtonText: "Pay Now",
                foodTotalText: "Cart Total",
                gameTotalText: "Game Total",
                subTotalText: "Subtotal"
            },

            checkoutSummary: {
                title: "Checkout Details",
                foodItembtnText: "Back to Cart",
                historyBtnText: "Back to History",
                tableHeaders: ["Type", "Amount", "Detail"],
                checkoutButtonText: "Check Out",
                payBtnText: "Pay Now",
                subTotalText: "Total Amount",
                detailText: "Detail",

                prevBillText: "Previous Paid Bill",
                prevFreeMinutesText: "Previous Free Minutes Amount",
                prevWalletBillText: "Previous Wallet Bill",


                dialogTitle: "Checkout Confirmation",
                dialogBody: "Are you sure you want to Check out?",
                dialogCheckoutBtnText: "Yes",
                dialogHalviewBtnText: "No",
            },

            currentMembership: {
                changePicture:"Change",
                currentTitle: 'Current Membership',
                name: 'Name',
                negativeBalanceLimit: 'Negative Balance Limit',
                price: 'Subscription Price (one time)',
                rate: 'Hourly Rates'
            },

            membershipHistory: {
                historyTitle: 'Membership History',
                name: 'Name',
                subscriptionDate: 'Subscription Date',
                expirationDate: 'Expiration Date',
                price: 'Subscription Price',
                rate: 'Rate'
            },

            sessionHistory: {
                heading: "History",
                tableHeads: ["Check In", "Check out", "Total Amount", "Minutes Played", "Detail"],
                detailHeading: "Bill Summary",

                paidText: "Detail",
                payBtnText: "Pay Now",

                checkInText: "Check In",
                checkOutText: "Check Out",
                transactionText: "Transaction Id",
                previousWalletBalance: "Available Wallet Balance",
                currentWalletBalance: "Remaining Wallet Balance",

                gameTitle: "Game",
                minutesText: "Minutes Played",
                freeMinuteText: "Free Minutes",
                gameVatText: "VAT Inclusive",
                gameBillText: "Game Bill",

                cartVatText: "VAT Inclusive",
                cartBillText: "Cart Bill",
                cartTitle: "Cart Items",
                totalCartText: "Total Cart Bill",

                totalGameText: "Total Game Bill",
                walletText: "Wallet Details",
                beforeFreeMinText: "Available Free Minutes",
                afterFreeMinText: "Remaining Free Minutes"
            },
        }
    };

    let activeLang = getActiveLanguage();
    initState.activeLanguage = initState[activeLang];
    initState.activeLang = activeLang;

    return initState;
};

export default function (state = initState(), action) {
    let newState = {...state};

    switch (action.type) {
        case types.CHANGE_LANGUAGE:
            updateLanguage(newState, action.payload);
            break;
        default :
            break;
    }
    return newState;
}

const updateLanguage = (state, language) => {

    state.activeLanguage = {...state[language]};
    state.activeLang = language;
    localStorage.setItem(LS_KEY_ACTIVE_LANG, state.activeLang)
};