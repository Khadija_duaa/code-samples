import axios from '../../utils/axios';
import * as actions from './server-action-types';
import {handleError} from '../store-utils';

const _setGallery = (payload) => {
    return {
        type: actions.SET_GALLERY,
        payload
    }
};

const _gameCount = (payload) => {
    return {
      type: actions.SET_COUNT,
      payload
  }
};

const _setAlbum = (payload) => {
    return {
        type: actions.SET_ALBUM,
        payload
    }
};

export const setPlayerSession = (payload) => {
    return {
        type: actions.SET_PLAYER_SESSION,
        payload
    }
};

const setSessionHistory = (payload) => {
    return {
        type: actions.SET_SESSION_HISTORY,
        payload
    }
};

const setTotalSessions = (payload) => {
    return {
        type: actions.SET_TOTAL_SESSIONS,
        payload
    }
};


export const setProcessing = (payload) => {
    return {
        type: actions.SET_PROCESSING,
        payload
    }
};

export const setWaitingList = (payload) => {
    return {
        type: actions.SET_WAITING_LIST,
        payload
    }
};

export const setHallTables = (payload) => {
    return {
        type: actions.SET_HALL_TABLES,
        payload
    }
};

export const setHallAccessToken = (payload) => {
    return {
        type: actions.SET_HALL_ACCESS_TOKEN,
        payload
    }
};

export const setAlbum = (_id) => {
    return (dispatch, getState) => {

        if (!_id) {
            dispatch(_setAlbum(null));
        }
        else {
            let albums = getState().server_reducer.gallery.filter(album => album._id === _id);

            if (albums && albums.length) {
                dispatch(_setAlbum(albums[0]));
            }
        }
    };
};

export const fetchGallery = () => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.get('/gallery')
            .then(response => {
                let {galleries} = response.data;
                dispatch(_setGallery({gallery: galleries}));
                dispatch(setProcessing(false));
            })
            .catch((err) => {
                handleError(err);
                dispatch(setProcessing(false));
            });
    };
};

export const gameCount = () => {
  return (dispatch) => {
      dispatch(setProcessing(true));

      axios.get('/game-count')
          .then(resp => {
              let {count} = resp.data;
              dispatch(_gameCount({gameCount: count}));
              dispatch(setProcessing(false));
          })
          .catch(err => {
              handleError(err);
              dispatch(setProcessing(false));
          })
  }
};


export const fetchAllSession = (showLoader, _id, cb) => {
    return (dispatch) => {
        if (showLoader) dispatch(setProcessing(true));

        let url = '/session-bill';
        if (_id) {
            url += `/${_id}`
        }

        axios.get(url)
            .then(response => {

                const {session} = response.data;

                dispatch(setPlayerSession(session.game));
                dispatch(setProcessing(false));

                cb && cb();
            }).catch((err) => {
            handleError(err);

            if (err.response && err.response.status === 403) {
                dispatch(setPlayerSession(null));
            }
            dispatch(setProcessing(false));
        });
    };
};

export const fetchSessionHistory = (cb) => {
    return (dispatch, getState) => {
        const serverReducer = getState().server_reducer;

        const sessionsLength = serverReducer.sessionHistory.length;

        const totalSessions = serverReducer.totalSessions;
        const currentSessions = !sessionsLength ? -1 : sessionsLength;

        if (currentSessions < totalSessions) {
            let url = `/session-history?limit=${30}`;
            if (sessionsLength) {
                url += `&skip=${sessionsLength}`;
            }

            dispatch(setProcessing(true));

            axios.get(url)
                .then(response => {
                    const {sessions, totalSessions} = response.data;

                    dispatch(setSessionHistory({sessionHistory: sessions, append: currentSessions !== -1}));
                    dispatch(setTotalSessions(totalSessions));

                    dispatch(setProcessing(false));

                    cb && cb();
                }).catch((err) => {
                handleError(err);
                if (err.response && err.response.status === 403) {
                    dispatch(setSessionHistory({sessionHistory: []}));
                }
                dispatch(setProcessing(false));
            });
        }
    };
};

export const fetchTransactionHistory = (cb) => {
    return (dispatch, getState) => {
        const serverReducer = getState().server_reducer;
        const transactionsLength = serverReducer.transactionHistory.length;

        console.log("Total Transactions", serverReducer.totalTransactions);

        const totalSessions = serverReducer.totalTransactions;

        const currentTransactions = !transactionsLength ? -1 : transactionsLength;

        if (currentTransactions < totalSessions) {
            let url = `/transaction-history?limit=${30}`;
            if (transactionsLength) {
                url += `&skip=${transactionsLength}`;
            }

            dispatch(setProcessing(true));

            axios.get(url)
                .then(response => {

                    console.log("dataaaaa", response.data);

                    const {transactions, totalTransactions} = response.data;

                    dispatch(setTransactioHistory({transactionHistory: transactions, append: currentTransactions !== -1}));
                    dispatch(setTotalTransactions(totalTransactions));

                    dispatch(setProcessing(false));

                    cb && cb();
                }).catch((err) => {
                handleError(err);
                if (err.response && err.response.status === 403) {
                    dispatch(setTransactioHistory({transactionHistory: []}));
                }
                dispatch(setProcessing(false));
            });
        }
    };
};

const setTransactioHistory = (payload) => {
    return {
        type: actions.SET_TRANSACTION_HISTORY,
        payload
    }
};

const setTotalTransactions = (payload) => {
    return {
        type: actions.SET_TOTAL_TRANSACTIONS,
        payload
    }
};

export const setServerConfigurations = (payload)=>{
  return{
      type:actions.SET_CONFIGURATIONS,
      payload
  }
};

export const resetRedux = () => {
    return {
        type: actions.RESET_REDUX
    }
};
