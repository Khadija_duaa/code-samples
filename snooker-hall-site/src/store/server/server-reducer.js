import * as actions from './server-action-types';

const initState = () => {
    return {
        gallery: [],
        gameCount: 0,
        album: [],
        waitingList: [],
        hallTables: [],
        playerSession: null,
        sessionHistory: [],
        totalSessions: 0,
        error: null,
        processing: false,
        transactionHistory: [],
        totalTransactions: 0,
        hallAccessToken: localStorage.getItem('hallAccessToken') || '',
        serverConfigurations:null
    };
};

const reducer = (state = initState(), action) => {
    let newState = {...state};

    const {type, payload} = action;

    switch (type) {
        case actions.SET_GALLERY: {
            setGallery(newState, payload);
            break;
        }
        case actions.SET_ALBUM: {
            setAlbum(newState, payload);
            break;
        }
        case actions.SET_PROCESSING : {
            setProcessing(newState, payload);
            break;
        }
        case actions.SET_WAITING_LIST : {
            setWaitingList(newState, payload);
            break;
        }
        case actions.SET_HALL_ACCESS_TOKEN : {
            setHallAccessToken(newState, payload);
            break;
        }
        case actions.SET_HALL_TABLES: {
            setHallTables(newState, payload);
            break;
        }
        case actions.SET_PLAYER_SESSION: {
            setPlayerSession(newState, payload);
            break;
        }
        case actions.SET_SESSION_HISTORY: {
            setSessionHistory(newState, payload);
            break;
        }
        case actions.SET_TOTAL_SESSIONS: {
            setTotalSessions(newState, payload);
            break;
        }
        case actions.SET_TRANSACTION_HISTORY: {
            setTransactionHistory(newState, payload);
            break;
        }
        case actions.SET_TOTAL_TRANSACTIONS: {
            setTotalTransactions(newState, payload);
            break;
        }
        case actions.SET_CONFIGURATIONS:{

            configureServer(newState,payload);
            break;
        }
        case actions.SET_COUNT: {
            setGameCount(newState, payload);
            break;
        }
        case actions.RESET_REDUX : {
            resetRedux(newState);
            break;
        }
        default : {
            // do nothing
        }
    }

    return newState;
};

const resetRedux = (state) => {
    state.sessionHistory = [];
    state.totalSessions = 0;

    state.transactionHistory = [];
    state.totalTransactions = 0;
};

const setHallAccessToken = (state, hallAccessToken) => {
    state.hallAccessToken = hallAccessToken;
    localStorage.setItem('hallAccessToken', hallAccessToken);
};

const setWaitingList = (state, waitingList) => {
    waitingList.forEach(item => {
        item.user.picture += `?time=${Date.now()}`;
    });

    state.waitingList = waitingList;
};

const setHallTables = (state, tableArray) => {
    state.hallTables = tableArray.tables.sort((a, b) => a.name.localeCompare(b.name));
};

const setProcessing = (state, processing) => {
    state.processing = processing;
};

const setGallery = (state, data) => {
    state.gallery = data.gallery;
    state.album = null;
};

const setAlbum = (state, album) => {
    state.album = album;
};

const setPlayerSession = (state, session) => {
    state.playerSession = session;
};

const setGameCount = (state, data) => {
    state.gameCount = data;
};

const setSessionHistory = (state, data) => {
    let sessionHistory = null;
    if (data.append) {
        sessionHistory = [...state.sessionHistory];
        data.sessionHistory.forEach(item => {
            sessionHistory.push(item);
        });
    } else {
        sessionHistory = data.sessionHistory;
    }

    state.sessionHistory = sessionHistory;
};

const setTotalSessions = (state, totalSessions) => {
    state.totalSessions = totalSessions;
};

const setTransactionHistory = (state, data) => {
    let transactionHistory = null;
    if (data.append) {
        transactionHistory = [...state.transactionHistory];
        data.transactionHistory.forEach(item => {
            transactionHistory.push(item);
        });
    } else {
        transactionHistory = data.transactionHistory;
    }

    state.transactionHistory = transactionHistory;
};

const setTotalTransactions = (state, totalTransactions) => {

    state.totalTransactions = totalTransactions;
};

const configureServer = (state,configurations)=>{
    state.serverConfigurations = configurations;
};

export default reducer;
