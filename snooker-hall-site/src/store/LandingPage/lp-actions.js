import {CHANGE_LANGUAGE, ALL_MEMBERSHIP} from "./lp-types";

export const selectLanguage = (props) => {

    return {
        type: CHANGE_LANGUAGE,
        payload: props
    }
};

export const setMemberships = (props) => {
    return {
        type: ALL_MEMBERSHIP,
        payload: props
    }
};
