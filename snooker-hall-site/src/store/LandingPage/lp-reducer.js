import * as types from './lp-types';

const LS_KEY_ACTIVE_LANG = 'ACTIVE_LANGUAGE';


const getActiveLanguage = () => localStorage.getItem(LS_KEY_ACTIVE_LANG) || "SV";

const initState = () => {
    let initState = {
        memberships: [],
        gallery: [],
        activeLanguage: {},
        activeLang: '',
        "SV": {
            "header": {
                leftnavArray: ["Hem","Om Oss", "Tjänster", "Medlemsskap"],
                rightnavArray: [{name:"Bilder",key:4}, {name:"Kontakta Oss",key:5}],
                h_floorView: "Se hallen",
                h_signIn: "Logga in",
                h_logout: "Logga ut",
                h_signup: "Registrera",

            },

            "slider": {
                s_title1: "Snooker247",
                s_title2: "Stället som Aldrig Stänger!",
                s_content: "Medlemsbaserad klubb som erbjuder snooker 24h om dygnet  365 dagar om året! En helt " +
                "automatiserad snookerhall som tack vare uttnyttjandet av den senaste tekniken i kombination " +
                "med självbetjäning håller kostnaderna på minimum, utan att kompromissa på spelkvaliten!",
                s_memberText: "Bli medlem!", //need to change
                s_hallText: "Gå till HallView"
            },
            "howWork": {
                title: "Så fungerar det",
                timeline1_title: "Bli medlem",
                timeline1_content: "Få tillgång till klubben hemifrån. Visa golvyta och lägg till dig själv i Queue.",

                timeline2_title: "Spela",
                timeline2_content: "När dina varv kommer, kan du spela på valfritt bord.",

                timeline3_title: "Beställ mat",
                timeline3_content: "Det är inte bara om spelet, du kan beställa mat lika bra.",

                timeline4_title: "Betala",
                timeline4_content: "Betala för den tid du spelar, mat din beställning genom Swish.",

            },

            "services": {
                ab_title: "Våra tjänster",
                ab_description: "Bli medlem och få en personlig kod för att komma in i hallen, ställ dig i kö redan hemifrån, " +
                "checka in och ut vid början/slut och betala genom att antingen ladda på pengar på ditt " +
                "spelarkonto, eller Swisha vid slutet av varje gång. Det är kostnadsfritt att bli medlem och " +
                "man betalar endast för spelad tid! ",
                ab_service1: "Ställ dig i kö hemifrån",
                ab_service2: "Realtids-statistik",
                ab_service3: "Online betalning",
                ab_service4: "Personlig historik"
            },

            "miniature": {
                min_description: "Det är är inte min hall, det är våran hall och det blir precis det vi " +
                "tillsammans gör det till! Visa respekt till andra besökare så väl som utrustningen. " +
                "Alla som älskar snooker är välkomna och jag är övertygad om ifall ni gillar snooker" +
                "och kommer en gång kommer vi definitivt att komma igen!",
                min_founderName: "Suleman Kukka-Salam",
                min_founderTitle: "Grundare Snooker 247",
                min_founderContact: "+46 076 20 78 739"
            },

            "packages": {
                pack_title: "Medlemsskap",
                pack_description: "Välj det medlemskap som passar dig bäst.",
                details: "Detaljer",
                purchase:'Inköp',

                pack_lowerText: "Årligt Teckningspris",
            },

            package_details:{
                membershipText:"Medlemskap",
                basicRateText:"Grundränta",
                otherRateText:"Annan gång",
                subscriptionText:"Prenumeration",
                upgradeBtnText:"Inköp",
                backBtnText:"Tillbaka",
                detailHeads:["Dagar","Tid","Pris"],
                anydayText:"Några Dag"


            },

            "gallery": {
                gal_title: "Bildgalleri",
                gal_description: "För att en bild säger mer än 1000 ord."
            },

            "contact": {
                main_title: "Kontaka Oss",
                contact_title1: "Kontaktperson",
                contact_tag1: "Suleman kukka-Salam",
                contact_tag3: "info@snooker247.se",
                contact_title2: "Address",
                address_description: "Turebergs alle 4, Sollentuna,Sweden"
            },
            "footer": {
                foot_title: "Snooker 247",
                navArray: ["Hem", "Hur det fungerar", "Tjänster", "Medlemsskap", "Bildgalleri", "Kontakta Oss"],
                footerArray: {
                    address: [
                        {
                            "tag": "Företag",
                            "detail": "Sulemans Snooker AB"
                        },
                        {
                            "tag": "E-post",
                            "detail": "info@snooker247.se"
                        },
                        {
                            "tag": "Kontakt",
                            "detail": "076 20 78 739"
                        },
                        {
                            "tag": "BG",
                            "detail": "5304-4160"
                        },
                        {
                            "tag": "Address",
                            "detail": "Turebergs alle 4, Sollentuna, Sweden"
                        }
                    ]
                },

                foot_head1: "MENY",
                foot_head2: "SOCIALA MEDIER",
                foot_head3: "Öppettider",

                foot_linkTitle_1: "Facebook",


                foot_timing_1: "Måndag - Söndag",

                foot_reclaim_title: "REKLAMATIONO",
                foot_reclaim_description: "Vid reklamation eller despyt vänligen skicka oss ett meil med detaljer kring " +
                "ärendet samt kontaktuppgifter så kontaktar vi er snarast",

                foot_copyright_content: "Copyright © 2018 Suleman Snooker AB | Powered by Synavos Solutions | All Rights Reserved."

            },
            "signup_form": {
                "firstName": "Förnamn",
                "lastName": "Efternamn",
                "email": "E-post",
                "submit":'Lämna',
                "enterEmail":'Ange E-post',
                "forgotPassword":"Glömt lösenord?",
                "successLabel":'Nytt lösenord har skickats till din email.Kontrollera det',
                "login_label":'logga in',
                "password": "Lösenord",
                "confirmPassword": "Bekräfta Lösenordet",
                "streetName": "Gatuadress",
                "securityNumber": "SSN",
                "phoneNumber": "Mobiltelefon",
                "city": "Stad",
                "postalCode": "Postkod",
                "highestBrake": "Högsta pausen",
                "termsCheck": "Jag godkänner medlemsvillkoren",

                highestBreakAccountTitle:"Högsta bryt",
                ssnAccountTitle:"Stad",

                "required": "Nödvändig!",
                "invalidEmail": "Ogiltig e-postadress !",
                "passwordLength": "Minsta lösenordslängd är 6",
                "passwordMismatch": "Lösenorden matchar inte !",
                "invalidPhone": "Ogiltigt telefonnummer !"
            },

            verification_form: {
                title: 'OTP-verifiering',
            },

            admin_verification_form: {
                title: 'HallView Authorization',
                username: 'Användarnamn',
                password: 'Lösenord',
                already_auth : 'Den här enheten är redan behörig',
                authorizeText:"Godkänna"
            }

        },

        "EN": {
            "header": {
                leftnavArray: ["Home","About", "Services", "Membership"],
                rightnavArray: [{name:"Gallery",key:4}, {name:"Contact",key:5}],
                h_floorView: "Hall View",
                h_signIn: "Sign In",
                h_logout: "Logout",
                h_signup: "Sign Up",

            },

            "slider": {
                s_title1: "Snooker247",
                s_title2: " The place that Never Closes!",
                s_content: "Membership club that offer snooker 24 hours a day 365 days a year. " +
                "A fully automated snooker club that thanks to its utilization of the latest " +
                "technique in combination with self service keeps the prices at a minimum, without " +
                "compromising the playing quality!",
                s_memberText: "Become a member!",
                s_hallText: "Go To HallView"
            },
            "howWork": {
                title: "How it Works",
                timeline1_title: "Become a Member",
                timeline1_content: "Get access to the club from home. View floor view and add yourself in Queue",

                timeline2_title: "Play",
                timeline2_content: "Once your turns come, you can play at any table of your choice.",

                timeline3_title: "Order Food",
                timeline3_content: "Its not about the game only, you can order food as well.",

                timeline4_title: "Pay",
                timeline4_content: "Pay only for the time you play, food your order through Swish.",

            },

            "services": {
                ab_title: "Our Services",
                ab_description: "Become a member and get a personal code to enter the club, you can get in cue " +
                "already from home, check-in and out at arrival/exit and pay by either topping up your " +
                "account before hand or by Swish at the end of each session. Becoming a memeber is " +
                "free of cost and you only pay for the time you play! ",
                ab_service1: "Get in Queue from Home",
                ab_service2: "Real-time Statistics",
                ab_service3: "Online Payment",
                ab_service4: "Personal Game Summary"
            },

            "miniature": {
                min_description: "This is not my club, it is our club and it becomes what " +
                "we make out of it! Everyone who loves snooker is welcome and " +
                "I am confident if you do like snooker and come once you will come again! ",
                min_founderName: "Suleman Kukka-Salam",
                min_founderTitle: "Founder Snooker 247",
                min_founderContact: "+46 076 20 78 739"
            },

            "packages": {
                pack_title: "Memberships",
                pack_description: "Choose the membership that suits you best",
                details: "Details",
                purchase:"Purchase",

                pack_lowerText: "Annual Subscription Price",
            },
            package_details:{
                basicRateText:"Basic Rate",
                otherRateText:"Other Time",
                membershipText:"Membership",
                subscriptionText:"Subscription",
                upgradeBtnText:"Purchase",
                backBtnText:"Back",
                detailHeads:["Days","Time","Rate"],
                anydayText:"Any Day"
            },

            "gallery": {
                gal_title: "Picture Gallery",
                gal_description: "Because a picture says more than a 1000 words."
            },
            "contact": {
                main_title: "Contact",
                contact_title1: "Contact Person",
                contact_tag1: "Suleman kukka-Salam",
                contact_tag3: "info@snooker247.se",
                contact_title2: "Address",
                address_description: "Turebergs alle 4, Sollentuna,Sweden"
            },
            "footer": {
                foot_title: "Snooker 247",
                navArray: ["Home", "About", "Services", "Packages", "Gallery", "Contact"],
                footerArray: {
                    address: [
                        {
                            "tag": "Company",
                            "detail": "Sulemans Snooker AB"
                        },
                        {
                            "tag": "Email",
                            "detail": "info@snooker247.se"
                        },
                        {
                            "tag": "Contact",
                            "detail": "076 20 78 739"
                        },
                        {
                            "tag": "BG",
                            "detail": "5304-4160"
                        },
                        {
                            "tag": "Address",
                            "detail": "Turebergs alle 4, Sollentuna, Sweden"
                        }
                    ]
                },

                foot_head1: "MENU",
                foot_head2: "SOCIAL",
                foot_head3: "TIMINGS",

                foot_linkTitle_1: "Facebook",
                // foot_linkTitle_2:"Twitter",
                // foot_linkTitle_3:"Google +",

                foot_timing_1: "Monday - Sunday",

                foot_reclaim_title: "RECLAIMATION",
                foot_reclaim_description: "In case of reclaimation or dispute please send us an email " +
                "with details and contact information and we will get back to you.",

                foot_copyright_content: "Copyright © 2018 Suleman Snooker AB | Powered by Synavos Solutions | All " +
                "Rights Reserved."

            },
            signup_form: {
                "firstName": "First Name",
                "lastName": "Last Name",
                "email": "Email",
                "enterEmail":'Enter Email',
                "submit":'Submit',
                "password": "Password",
                "forgotPassword":"Forgot Password?",
                "successLabel":'New password has been sent to your email.Kindly check it',
                "login_label":'Login',
                "confirmPassword": "Confirm Password",
                "streetName": "Street Name",
                "securityNumber": "SSN",
                "phoneNumber": "Phone Number",
                "city": "City",
                "postalCode": "Postal Code",
                "highestBrake": "Highest break",
                "termsCheck": "I Agree to the terms and conditions",

                highestBreakAccountTitle:"Highest Break",
                ssnAccountTitle:"SSN",

                "required": "Required!",
                "invalidEmail": "Invalid Email Address !",
                "passwordLength": "Minimum password length is 6",
                "passwordMismatch": "Passwords do not match !",
                "invalidPhone": "Invalid Phone Number !"
            },
            verification_form: {
                title: 'OTP Verification'
            },
            admin_verification_form: {
                title: 'HallView Authorization',
                username: 'username',
                password: 'password',
                already_auth : 'This device is already authorized',
                authorizeText:"Authorize",
            }
        }
    };


    let activeLang = getActiveLanguage();

    initState.activeLanguage = initState[activeLang];
    initState.activeLang = activeLang;

    return initState;
};

export default function (state = initState(), action) {
    let newState = {...state};

    switch (action.type) {
        case types.ALL_MEMBERSHIP:
            setMemberships(newState, action.payload);
            break;
        case types.CHANGE_LANGUAGE:
            updateLanguage(newState, action.payload);
            break;
        default :
            break;
    }
    return newState;
}

const setMemberships = (state, data) => {
    state.memberships = data.memberships;
};

const updateLanguage = (state, language) => {
    state.activeLanguage = {...state[language]};
    state.activeLang = language;
    localStorage.setItem(LS_KEY_ACTIVE_LANG, state.activeLang);
};