export const handleError = (error) => {
    let message = 'Some error occurred, Unable to connect to server';

    console.error('error', error);

    if (error.response) {
        console.error('error.response', error.response);
    }

    if (error.response && error.response.data) {
        console.error('error.response.data', error.response.data);
        message = error.response.data._message;
    }

    return message;
};