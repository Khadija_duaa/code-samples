import React from 'react';
import Input from '../../../../Components/UserProfileData/Data';
import connect from 'react-redux/es/connect/connect';
import {Button} from 'reactstrap';
import {withRouter} from 'react-router-dom';
import {
    required,
    email,
    swedishPhone,
    pakistanPhone,
    ssnLength,
    ssnPakistanLength,
    integer,
    validSwedishPostalCode,
    validPakistanPostalCode

} from '../../../../utils/validations';

class UserProfilePanel extends React.Component {

    state = {
        errors: {}
    };

    componentDidMount() {
        const {user} = this.props;
        if (user) {
            let state = {...user};
            this.setState(state);
        }
    }

    onInputChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };

    prepareInputFields = () => {
        let {serverConfigurations} = this.props;

        let postalCode = '',ssnValidate = '',phoneValidate ='';

        if(serverConfigurations && serverConfigurations.countryCode &&  serverConfigurations.countryCode=== 'PK'){
            postalCode = validPakistanPostalCode;
            phoneValidate = pakistanPhone;
            ssnValidate = ssnPakistanLength;
        }else {
            postalCode = validSwedishPostalCode;
            phoneValidate = swedishPhone;
            ssnValidate = ssnLength;
        }


        const inputFields = [];
        inputFields.push({
            name: 'firstName',
            label: this.props.firstName,
            validate: [required]
        });

        inputFields.push({
            name: 'lastName',
            label: this.props.lastName,
            validate: [required]

        });

        inputFields.push({
            name: 'email',
            label: this.props.email,
            validate: [required, email]
        });

        inputFields.push({
            name: 'phoneNumber',
            label: this.props.phoneNumber,
            validate: [required, phoneValidate]
        });

        inputFields.push({
            name: 'ssn',
            label: this.props.securityNumber,
            validate: [required,integer, ssnValidate]
        });

        inputFields.push({
            name: 'street',
            label: this.props.streetName,
            validate: [required]
        });

        inputFields.push({
            name: 'city',
            label: this.props.city,
            validate: [required]
        });

        inputFields.push({
            name: 'postalCode',
            label: this.props.postalCode,
            validate: [required, postalCode]
        });

        inputFields.push({
            name: 'highestBreak',
            label: this.props.highestBrake,
        });

        return inputFields;
    };

    footer = () => {
        return (
            <div key={2} className={"col-md-8 "} style={{margin: '1% 0 4% 20%', display: 'flex'}}>
                <Button onClick={() => {
                    this.props.onSubmit({...this.state})
                }}
                        disabled={this.hasError()}
                        style={{
                            color: 'white',
                            borderRadius: '10%',
                            backgroundColor: '#13C95F',
                            width: '20%',
                            height: '100%',
                            padding: '1%',
                            textAlign:"center"
                        }}>
                    {this.props.confirmText}
                </Button>

                <Button onClick={() => this.props.history.push('/hall')}
                        style={{
                            color: 'black',
                            borderRadius: '10%',
                            backgroundColor: 'white',
                            width: '20%',
                            height: '100%',
                            padding:"1%",
                            marginLeft: '5%',
                            textAlign:"center"
                        }}>
                    {this.props.cancelText}
                </Button>
            </div>
        )
    };

    onFieldError = (error) => {

        if (error) {
            const errors = {
                ...this.state.errors,
                ...error
            };

            this.setState({errors});
        }
    };

    hasError = () => {
        let hasError = false;
        const {errors} = this.state;

        for (let key in errors) {
            if (errors.hasOwnProperty(key)) {
                hasError = Boolean(errors[key]);
                if (hasError) break;
            }
        }

        return hasError;
    };

    render() {
        const inputFields = this.prepareInputFields();
        const {error} = this.props;
        return [
            <div key={1} className={"col-md-10"} style={{margin: '4% 0 0 6%'}}>
                <div className={'row'}>
                    {inputFields.map(field => {
                        return (
                            <div key={field.name} className={"col-md-6"}>
                                <Input style={{marginBottom: '1%'}}
                                       {...field}
                                       error={error}
                                       value={this.state[field.name]}
                                       onChange={this.onInputChange}
                                />
                            </div>
                        );
                    })}
                </div>
            </div>,
            this.footer()
        ];
    }
}

const mapStateToProps = state => {

    let activeLanguage = state.lp_reducer.activeLanguage;
    return {
        categoriesArray: state.gv_reducer.activeLanguage.userInfo,

        lastName: activeLanguage.signup_form.lastName,
        firstName: activeLanguage.signup_form.firstName,
        email: activeLanguage.signup_form.email,
        streetName: activeLanguage.signup_form.streetName,
        phoneNumber: activeLanguage.signup_form.phoneNumber,
        securityNumber: activeLanguage.signup_form.ssnAccountTitle,
        city: activeLanguage.signup_form.city,
        postalCode: activeLanguage.signup_form.postalCode,
        highestBrake: activeLanguage.signup_form.highestBreakAccountTitle,

        confirmText:state.gv_reducer.activeLanguage.userProfile.confirmText,
        cancelText:state.gv_reducer.activeLanguage.userProfile.cancelText,

        serverConfigurations:state.server_reducer.serverConfigurations
    }
};

const connected = connect(mapStateToProps)(UserProfilePanel);
export default withRouter(connected);

