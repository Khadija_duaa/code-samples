import React from 'react';
import Transition from '../../../Components/Transition/Transition';
import TitleBar from '../../../Components/TitleBar/TitleBar';
import {connect} from "react-redux";
import {withRouter, Switch, Route} from "react-router-dom";
import NavMenu from './Navbar/NavMenu';
import Profile from './Profile/Profile';
import Membership from './Membership/UserMembership';
import {updateProfile} from "../../../store/User/user-actions";
import './userProfile.css';
import {formatAmount} from "../../../utils/common-utils";

const NavBar = () => {
    return (
        <div className={"col-md-12"}
             style={{borderBottom: "1px solid  #777777", marginBottom: '3%', width: '100%', display: 'inline-block'}}>
            <NavMenu/>
        </div>
    )
};

const style = {
    background: "rgba(31, 31, 31, 0.9)",
    border: "1px solid  rgba(206, 210, 219,0.7)",
    borderRadius: "6px",
    padding: "2% 0 0 0",
    width: "100%",
    height: '100%',
    maxWidth: "100",
    maxHeight: '100%',
    margin: "unset",
    marginTop: "1%",
};

class UserProfile extends React.Component {

    componentDidMount() {
        this.props.updateProfile();
    }

    getProfileComponent = () => {
        const profile = () => <Profile {...this.props.activeUser}/>;
        const membership = () => <Membership {...this.props.activeUser}/>;
        return (
            <Switch>
                <Route path={'/hall/profile/membership'} component={membership}/>
                <Route path={'/hall/profile'} component={profile}/>
            </Switch>
        );
    };

    render() {
        return (
            <Transition>
                <TitleBar
                    leftTitle={this.props.profileTitle} balanceTitle={this.props.wallet} freeMinTitle={this.props.freeMin} freeMin={(this.props.activeUser.profile.freeMinutes)}
                    balance={formatAmount(this.props.activeUser.profile.wallet)} leftIcon='/img/registrationIcons/user.svg'
                    rightTitle={this.props.activeUser.membership.name} rightIcon='/img/memberships.svg'/>
                <div className="row" style={style}>
                    <NavBar/>
                    {this.getProfileComponent()}
                </div>
            </Transition>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        activeUser: state.user_reducer.activeUser,
        wallet: state.gv_reducer.activeLanguage.navbar.wallet,
        freeMin: state.gv_reducer.activeLanguage.navbar.freeMin,
        profileTitle: state.gv_reducer.activeLanguage.userProfile.userProfile_title

    };

};

const mapDispatchToProps = (dispatch) => {
    return {
        updateProfile: () => dispatch(updateProfile())
    };
};

const connected = connect(mapStateToProps, mapDispatchToProps)(UserProfile);

export default withRouter(connected);