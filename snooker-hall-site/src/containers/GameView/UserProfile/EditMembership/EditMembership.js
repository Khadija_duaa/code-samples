import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Transition from "../../../../Components/Transition/Transition";
import TextItem from '../../../../Components/TextItem/TextItem';
import MembershipsDetail from "../../../../Components/MembershipsDetail/MembershipsDetail";

class EditMembership extends React.Component {

    render() {

        return (
            <Transition>
                <div className="row" style={{margin:"unset"}}>
                    <div className="col-md-12" style={{marginBottom:'30px'}}>
                        <TextItem style={{paddingInlineStart:"40px",marginLeft:"3%", textTransform: 'none'}}> {this.props.label}</TextItem>
                    </div>
                    <div className="col-md-12"  >
                        <MembershipsDetail style={{backgroundColor:'transparent', color:'white'}} />
                    </div>
                </div>
            </Transition>
        );
    };
}

const mapStateToProps = (state) => {
    return {
        processing: state.user_reducer.processing,
        label:state.gv_reducer.activeLanguage.upgradeMembership.label,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};
const connected = connect(mapStateToProps, mapDispatchToProps)(EditMembership);
export default withRouter(connected);