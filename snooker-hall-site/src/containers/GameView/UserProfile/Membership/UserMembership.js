import React from 'react';
import {Switch, Route, Link} from "react-router-dom";
import Title from "../../../../Components/Heading/Title";
import {VerticalTimeline, VerticalTimelineElement} from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import '../../../../Components/Vertical TimeLine/TimeLine.css';
import connect from "react-redux/es/connect/connect";
import moment from 'moment';
import ScrollBar from "../../HallView/WaitingList/ScrollBar";
import EditMembership from '../EditMembership/EditMembership';
import {toAbbrevDays} from "../../../../utils/date-utils";
import {formatAmount} from "../../../../utils/common-utils";

const _style = {
    background: "rgba(31, 31, 31, 0.9)",
    border: "1px solid  rgba(206, 210, 219,0.7)",
    borderRadius: "7px",
    padding: "3%",
    margin: '2% 0 10% 5%',
};

const textStyle = {
    color: 'white'
};

const textStyleValue = {
    color: 'white',
    fontSize: '16px'
};

const prepareCurrentMembershipFields = (props) => {

    const inputFields = [];

    inputFields.push({
        value: props.membership.name,
        label: props.name,
    });

    inputFields.push({
        value: props.membership.price,
        label: props.price
    });

    return inputFields;
};

const prepareMembershipHistoryFields = (membershipHistory, historyName, subscriptionDate, expirationDate, historyPrice) => {

    const inputFields = [];

    inputFields.push({
        value: membershipHistory.name,
        label: historyName
    });

    inputFields.push({
        value: moment(membershipHistory.subscriptionDate).format("DD MMM, YY"),
        label: subscriptionDate
    });

    inputFields.push({
        value: moment(membershipHistory.expirationDate).format("DD MMM, YY"),
        label: expirationDate
    });

    inputFields.push({
        value: membershipHistory.price === 0 ? "Free" : `${membershipHistory.price}/year`,
        label: historyPrice
    });

    return inputFields;
};

const getData = (activeUser) => {
    return (
        activeUser.membership.specialRates.map((data, index) => {
            return (
                <div className={"row"} key={index} style={{display: 'flex', marginTop: '4px'}}>
                    <div className={"col-md-3"}>{toAbbrevDays(data.days)}</div>
                    <div className={"col-md-4"}>{data.startTime} - {data.endTime}</div>
                    <div className={"col-md-5"}>{`${formatAmount(data.rate)}/hr`}</div>
                </div>
            )
        })
    )
};

const getHourlyRates = (activeUser) => {
    return (
        <div className={"col-md-12"} style={{color: 'white'}}>
            <ScrollBar style={{maxHeight: "230px", marginBottom: "unset", marginTop: '15px'}} scrollType={"tableScroll"}>
                {getData(activeUser)}
            </ScrollBar>
        </div>
    )
};


const CurrentMembership = (props) => {


    let {activeUser} = props;
    const currentMembershipFields = prepareCurrentMembershipFields(props);

    return (
        <div className={"col-md-5"} style={_style}>
            <div className={"row"}>
                {
                    currentMembershipFields.map((field) => {
                        return (
                            <div key={field.label} className={"col-md-12"}>
                                <div className={"row"} style={{paddingBottom: '2%'}}>
                                    <div className={"col-md-6"}>
                                        <strong style={{color: 'white'}}>{field.label}</strong>
                                    </div>
                                    <div className={"col-md-4"} style={{color: 'white'}}>
                                        {field.value}
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
            <div className={"row"}>
                <UpgradeMembershipTitle key={3} {...props} />
            </div>
            {
                activeUser.membership.specialRates && activeUser.membership.specialRates.length ?
                    <div className={"row"}>
                        <div className={"col-md-12"}>
                            <div className={"row"}>
                                <div className={"col-md-12"}>
                                    <strong style={{color: 'white'}}>{props.rate}</strong>
                                </div>
                                {getHourlyRates(activeUser)}
                            </div>
                        </div>
                    </div> : null
            }
        </div>
    )
};

const MembershipHistory = (props) => {

    const {historyName, subscriptionDate, expirationDate, historyPrice} = props;

    return props.membershipHistory.map(membership => {

        const membershipHistoryFields = prepareMembershipHistoryFields(membership, historyName, subscriptionDate, expirationDate, historyPrice);

        return (
            <div className="row" key={membership._id}>
                <div className={"col-md-8 offset-md-1"} style={{
                    marginLeft: '67px', maxWidth: 'unset' +
                    ''
                }}>
                    <VerticalTimeline layout={"1-column"}>
                        <VerticalTimelineElement
                            icon={<img style={{filter: 'invert(34%)'}} src={"/img/memberships.svg"} alt={'img'}/>}
                            key={membership.name}>
                            {
                                membershipHistoryFields.map((field) => {
                                    return (
                                        <div key={field.label} className={"row"}>
                                            <div className={"col-md-6"}>
                                                <strong style={textStyle}>{field.label}</strong>
                                            </div>
                                            <div className={"col-md-6"}>
                                                <span style={textStyleValue}>{field.value}</span>
                                            </div>
                                        </div>
                                    )
                                })
                            }

                        </VerticalTimelineElement>
                    </VerticalTimeline>
                </div>
            </div>
        )
    });
};

const MembershipTitle = (props) => {

    return (
        <div className={"col-md-12"}>
            <div className={"row"}>
                <div className={"col-md-3 offset-md-2"}>
                    <Title afterClass="whiteDash">{props.currentTitle}</Title>
                </div>
                <div className={"col-md-3 offset-md-2"}>
                    <Title afterClass="whiteDash">{props.historyTitle}</Title>
                </div>
            </div>
        </div>
    )
};

const UpgradeMembershipTitle = (props) => {
    return (
        <div>
            <Link to={'/hall/profile/membership/upgrade'}>
                <p style={{color: 'GREEN_COLOR', margin: '22px 0 37px 15px'}}><u>{props.title}</u></p>
            </Link>
        </div>
    )
};

const membershipInfo = (props) => () => [
    <MembershipTitle key={0} {...props}/>,
    <CurrentMembership key={1} {...props}/>,
    <ScrollBar key={2}
               style={{maxHeight: "100%", height: "300px", maxWidth: '50%', marginBottom: 'unset', marginTop: '24px'}}
               scrollType="waitingScroll">
        <MembershipHistory {...props} />
    </ScrollBar>,
];

const userMembership = (props) => {
    return (
        <Switch>
            <Route path={'/hall/profile/membership/upgrade'} component={EditMembership}/>
            <Route path={'/hall/profile/membership'} component={membershipInfo(props)}/>
        </Switch>
    );
};


const mapStateToProps = state => {

    const currentMembership = state.gv_reducer.activeLanguage.currentMembership;
    const membershipHistory = state.gv_reducer.activeLanguage.membershipHistory;

    return {
        currentTitle: currentMembership.currentTitle,
        name: currentMembership.name,
        negativeBalanceLimit: currentMembership.negativeBalanceLimit,
        price: currentMembership.price,
        rate: currentMembership.rate,

        historyTitle: membershipHistory.historyTitle,
        historyName: membershipHistory.name,
        subscriptionDate: membershipHistory.subscriptionDate,
        expirationDate: membershipHistory.expirationDate,
        historyPrice: membershipHistory.price,
        historyRate: membershipHistory.rate,

        title: state.gv_reducer.activeLanguage.upgradeMembership.title,

        memberships: state.lp_reducer.memberships,
        activeUser: state.user_reducer.activeUser
    }
};

export default connect(mapStateToProps)(userMembership);