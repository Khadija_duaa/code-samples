import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import NavBar from '../../../../Components/NavBar/Navbar';
import {HEADING_FONT} from "../../../../utils/css-constants";

class NavMenu extends React.Component {

    addInNavItem = (jsx, item) => {
        let {pathname} = this.props.location;
        item.active = item.exact ? (pathname === item.to) : pathname.startsWith(item.to);

        jsx.push(item);

        jsx.push(<span key={`sep-${item.key}`} style={{color: "#707070"}}>|</span>);
    };

    getNavItems = () => {
        let jsx = [];

        this.addInNavItem(jsx, {
            key: jsx.length,
            type: 'text',
            children: <b>{this.props.basicInfo_title}</b>,
            to: '/hall/profile',
            exact: true,
            style: {color: '#777777', fontFamily: HEADING_FONT, textTransform: 'unset'},
            activeColor: '#fff'
        });

        this.addInNavItem(jsx, {
            key: jsx.length,
            type: 'text',
            children: <b>{this.props.membership_title}</b>,
            to: '/hall/profile/membership',
            style: {color: '#777777', fontFamily: HEADING_FONT, textTransform: 'unset'},
            activeColor: '#fff'
        });

        return jsx;
    };

    render() {
        return (
            <ul style={{textAlign: "left", marginLeft:"2%"}}>
                <div style={{width: '100%', display: 'inline-block'}}>
                    <NavBar navItems={this.getNavItems()}/>
                </div>
            </ul>
        )
    }
}

const mapStateToProps = (_state) => {
    const {navbar} = _state.gv_reducer.activeLanguage;

    return {
        basicInfo_title: navbar.basicInfo_title,
        membership_title: navbar.membership_title,
    };
};

const connectedComponent = connect(mapStateToProps)(NavMenu);

export default withRouter(connectedComponent);