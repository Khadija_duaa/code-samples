import React from 'react';
import UserProfileTitle from '../../../../Components/Title/Title';
import connect from "react-redux/es/connect/connect";


class Title extends React.Component{
    render() {

        return(
            <UserProfileTitle>
                {this.props.userProfileTitle}
            </UserProfileTitle>
        )
    }
}

const mapStateToProps = state => {
    return {
        userProfileTitle : state.gv_reducer.activeLanguage.userProfile.userProfile_title
    }
};

export default connect(mapStateToProps)(Title)