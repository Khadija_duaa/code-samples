import React from 'react';
import axios from '../../../../utils/axios';
import {SERVER_URL} from "../../../../server";
import Loader from 'react-loader-spinner';
import {setProfilePic} from "../../../../store/User/user-actions";
import {connect} from 'react-redux';
import './style.css';
import {GREEN_COLOR} from "../../../../utils/css-constants";
import {NotificationManager} from 'react-notifications';
import {Link} from "react-router-dom";

class Picture extends React.Component {

    state = {
        processing: false
    };

    setProcessing = (processing, cb) => {
        if (processing) {
            this.setState({processing}, cb);
        }
        else {
            setTimeout(() => {
                this.setState({processing}, cb);
            }, 500);
        }
    };

    onDrop = (e) => {

        if (e.target.files.length) {
            let file = e.target.files[0];

            const MAX_FILE_SIZE = 50 * 1024 * 1024; // MB

            if (file.size > MAX_FILE_SIZE) {
                let message = 'Error occurred! File size exceeds the limit';
                NotificationManager.error(message, '', 3000);
            }
            else {
                const formData = new FormData();
                formData.append('file', file, file.name);

                this.setProcessing(true);

                axios.post('/profile-pic', formData)
                    .then(response => {

                        this.setProcessing(false, () => {
                            this.props.setProfilePic(response.data.picture);
                        });
                    })
                    .catch(err => {
                        this.setProcessing(false);
                        console.error('err', err);
                    });
            }
        }
    };


    getPicture = () => {
        if (this.state.processing) {
            return (
                <div style={{textAlign: 'center'}}>
                    <Loader
                        type="Triangle"
                        color={GREEN_COLOR}
                        height="40px"
                        width="40px"/>
                </div>
            );
        }

        const picUrl = `${SERVER_URL}/photos/${this.props.src}?${Date.now()}`;

        return [
            <img key={'1'} className={"profile"} src={picUrl || '/img/dp.jpg'} alt="userDP"/>,
            <div key={'2'} className="edit">
                <input type={'file'}
                       ref={uploadElement => this.uploadElement = uploadElement}
                       accept={'.jpg,.jpeg,.png'}
                       hidden
                       onChange={this.onDrop}/>
                <a className='upload-link' onClick={() => this.uploadElement.click()}>
                    <i className="fa fa-pencil fa-lg"/>
                </a>
            </div>
        ];
    };


    render() {
        return (
            <div className={"row-md-12"} style={{display:'flex'}}>
                <div className={"col-md-6 offset-md-2"}>
                    <div style={{ marginLeft:'100px', marginTop:'44px'}} className={"profile"}>
                        {this.getPicture()}
                    </div>
                </div>
                <div className={"col-md-2 "} style={{marginLeft: '48px', marginTop: '60px'}}>
                    <input type={'file'}
                           ref={uploadElement => this.uploadElement = uploadElement}
                           accept={'.jpg,.jpeg,.png'}
                           hidden
                           onChange={this.onDrop}/>
                    <div style={{display:'flex'}}>
                    <div style={{color:GREEN_COLOR, marginRight:'5px', marginTop:'6px'}} className="fa fa-pencil fa-sm"/>
                    <p style={{color:GREEN_COLOR, display: 'flex', cursor:'pointer'}}><a  style={{display:'flex'}} onClick={() => this.uploadElement.click()}>{this.props.changePicture}</a></p>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        changePicture: state.gv_reducer.activeLanguage.currentMembership.changePicture
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setProfilePic: (picture) => dispatch(setProfilePic(picture))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Picture);