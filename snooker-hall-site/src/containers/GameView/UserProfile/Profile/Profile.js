import React from 'react';
import UserProfilePanel from '../UserProfileInputs/Inputs';
import Picture from './Picture';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Loader from 'react-loader-spinner';
import {GREEN_COLOR} from "../../../../utils/css-constants";
import {sendUpdateCodes, saveData} from "../../../../store/User/user-actions";

class Profile extends React.Component {

    state = {
        error:{},
    };

    onFormSubmit = (user) => {
        this.props.saveData(user);

        this.props.updateUser(
            (state) => {
                this.props.history.push({pathname: '/hall/login/verification', state});
            },
            () => {

                this.props.history.push('/hall/profile');
                window.location.reload();

            },
            (errors) => {

                console.error('errors', errors);
                this.setState({error:errors})
                // TODO handle Errors
            }
        );
    };

    getProfilePanel = () => {
        if (this.props.processing) {
            return (
                <div key={1} style={{textAlign: 'center'}}>
                    <Loader
                        type="Triangle"
                        color={GREEN_COLOR}
                        height="40px"
                        width="40px"/>
                </div>
            );
        }

        return <UserProfilePanel key={1} error={this.state.error} user={this.props} onSubmit={this.onFormSubmit}/>;
    };


        render()
        {
        return [
            <Picture key={0} src={this.props.picture}/>
            ,
            this.getProfilePanel()
        ];
    };
}

const mapStateToProps = (state) => {
    return {
        processing: state.user_reducer.processing
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateUser: (callback, cb, errorCallback) => dispatch(sendUpdateCodes(callback, cb, errorCallback)),
        saveData: (data) => dispatch(saveData(data))
    };
};
const connected = connect(mapStateToProps, mapDispatchToProps)(Profile);
export default withRouter(connected);