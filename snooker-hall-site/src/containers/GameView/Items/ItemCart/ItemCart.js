import React from 'react';
import EnterLeaveTransition from '../../../../Components/Transition/EnterLeaveTransition';
import Title from '../../../../Components/Heading/Title';
import connect from "react-redux/es/connect/connect";
import {BODY_FONT} from "../../../../utils/css-constants";
import {formatAmount} from "../../../../utils/common-utils";
import '../../HallView/WaitingList/WaitingList.css'
import ScrollBar from "../../HallView/WaitingList/ScrollBar";
import {Badge} from 'reactstrap';

const btnStyle = {
    color: 'black',
    letterSpacing: '1px',
    fontFamily: BODY_FONT,
    fontSize: '14px',
};

const itemCart = ({cart, onAddItem, onRemoveItem, foodCartTitle, saveCart}) => {

/*
    if (!cart || !cart.length) return null;
*/

    return (
        <div className="row" style={{marginRight:"unset"}}>
            <div className={"col-md-12"} style={{padding: '11% 0% 2% 10%'}}>
                <Title afterClass={"whiteDash"}>
                    {foodCartTitle}
                </Title>
            </div>

            <ScrollBar style={{height:"35vh",maxHeight: '35vh',paddingRight:"2%",marginRight:"1%"}} scrollType="waitingScroll">
            <div className="row" style={{width: '100%', margin: 'unset'}}>
                <div className={"col-md-12"} style={{padding: '1% 0 2% 10%'}}>
                    <EnterLeaveTransition>
                        {
                            cart.map(item => (
                            <div className="row" key={item.item_id} style={{color: 'white'}}>
                                    <div className="col-md-3" style={{textAlign: 'left'}}>
                                            {item.name}
                                        <p style={{color:'#BEBEBE', fontFamily: 'serif'}}>SEK {item.price}</p>
                                    </div>
                                    <div className="col-md-4" style={{paddingLeft:'0px'}}>
                                        <Badge style={{padding: '6% 20%', border: '1.5px solid #6c757d',borderRadius: '10px', background: 'transparent', marginTop:'7%'}}>
                                            <i className={"fa fa-minus"} style={{paddingRight:"15px", cursor: 'pointer'}}
                                               onClick={() => {onRemoveItem(item.item_id)}}/>
                                                {item.quantity}
                                            <i className={"fa fa-plus"} style={{paddingLeft:"15px", cursor: 'pointer'}}
                                               onClick={() => {onAddItem(item)}}/>
                                        </Badge>
                                    </div>

                                    <div className="col-md-5" style={{paddingLeft:'3px'}}>
                                        <span style={{display:'block', paddingTop: '7%'}}>
                                            {
                                                formatAmount(item.price * item.quantity)
                                            }
                                            <i className={"fa fa-trash"} style={{float:'right', marginRight: '2%', marginTop :' 3%', cursor: 'pointer'}}
                                               onClick={() => {onRemoveItem(item.item_id, true)}}/>
                                        </span>
                                    </div>
                            </div>
                        ))}
                    </EnterLeaveTransition>
                </div>
            </div>

            </ScrollBar>



            <div className={"col-md-12"}>
                <div className={"row"}>
                    <div className={"col-md-12"} style={{padding: '0 12% 0 12%'}}>
                        <hr style={{borderStyle: 'none none dotted', border: '1px dotted white'}}/>
                        <div className={"row"}>
                            <div className={"col-md-5 offset-md-1"}>
                                <p style={{color: 'white'}}>Subtotal</p>
                            </div>
                            <div className={"col-md-5"}>
                                <div style={{color: 'white'}}>{
                                    formatAmount(cart.reduce((accumulator, item) => {

                                        return accumulator + (item.price * item.quantity)
                                    }, 0))
                                }
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className={"col-md-12"} style={{padding: '0% 25% 5% 25%'}}>
                        <a className="btn" style={{padding: '3%', height: '100%'}} onClick={saveCart}>
                            <strong style={btnStyle}>Add to Cart</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        foodCartTitle: state.gv_reducer.activeLanguage.foodCart.foodCart_title
    }
};

export default connect(mapStateToProps)(itemCart);