import React from 'react';
import ScrollBar from "../../HallView/WaitingList/ScrollBar";
import CategoryDetails from '../CategoryDetails/CategoryDetails';
import {HEADING_FONT} from "../../../../utils/css-constants";

const navItem_style = {
    color: "#777777",
    margin: '0 1% 0 1%',
    display: "inline-block",
    cursor: "pointer",
    fontFamily: HEADING_FONT,
    lineHeight: "1px",
    borderRight: '2px solid #777777',
    padding: '1%',
    fontSize: 'large'
};


const NavBar = ({categories, activeCategory, onSetActive}) => {

    const navBar = categories.map(category => {

        const style = {...navItem_style};

        if (activeCategory && (category._id === activeCategory._id)) {
            style.color = 'white';
            style.fontWeight = 'bold';
        }

        return (
            <li style={style} key={category._id}
                onClick={() => onSetActive(category)}>
                {category.categoryName}
            </li>
        );
    });

    return (
        <ul style={{width: "100%", display: "inline-block", textAlign: "left"}}>
            {navBar}
        </ul>
    );
};

const itemCategories = (props) => {
    const {categories, activeCategory, onSetActive, onAddItem} = props;

    if (!categories || !categories.length || !onSetActive) return null;

    return (
        <div className="row">
            <div className={"col-md-12"} style={{paddingTop: '3%'}}>
                <NavBar {...props}/>
            </div>

            <ScrollBar style={{maxHeight: '30%'}} scrollType="waitingScroll">
                <div className={"col-md-12"} style={{height:"420px", paddingTop: '3%', paddingLeft: '4%'}}>
                    <CategoryDetails items={activeCategory ? activeCategory.items : []}
                                     categoryIcon={activeCategory ? activeCategory.icon : null} onClick={onAddItem}/>
                </div>
            </ScrollBar>
        </div>
    );
};

export default itemCategories;