import React from 'react';
import Transition from '../../../Components/Transition/Transition';
import ItemCart from './ItemCart/ItemCart';
import ItemCategories from './ItemCategories/ItemCategories'
import Container from '../../../Components/HallView/Container/ContentContainer';
import Loader from 'react-loader-spinner';
import {fetchItems, saveCart, fetchCart} from "../../../utils/data-fetcher";
import {GREEN_COLOR} from "../../../utils/css-constants";
import {connect} from 'react-redux';

class Items extends React.Component {

    componentDidMount() {
        fetchItems()
            .then((categories) => {
                this.setData(true, categories); // With True
                if (categories && categories.length) {
                    this.setState({activeCategory: categories[0]});
                }
                return fetchCart();
            })
            .then((cart) => {
                this.setState({processing: false, cart})
            })
            .catch(() => {
                this.setData(false)
            });
    }

    state = {
        categories: [],
        processing: true,
        activeCategory: null,
        cart: []
    };

    setCategory = (activeCategory = null) => {
        this.setState({activeCategory});
    };

    setData = (processing, categories = this.state.categories) => {
        this.setState({processing, categories});
    };

    getItemIndexInCart = (item_id, cart = []) => {
        let index = -1;

        for (let i = 0; i < cart.length; i++) {
            let cartItem = cart[i];
            if (cartItem.item_id === item_id) {
                index = i;
                break;
            }
        }

        return index;
    };

    onAddItem = (item, quantity = 1) => {
        const item_id = item.item_id || item._id;



        const cart = [...this.state.cart];

        const itemIndex = this.getItemIndexInCart(item_id, cart);
        if (itemIndex < 0) {
            cart.push({item_id, quantity, name: item.name, price: item.price});
        }
        else {
            let cartItem = cart[itemIndex];
            cartItem.quantity += quantity;
            cart[itemIndex] = cartItem;
        }

        this.setState({cart});
    };

    onRemoveItem = (item_id, removeAll = false) => {

        const cart = [...this.state.cart];
        const itemIndex = this.getItemIndexInCart(item_id, cart);

        if (itemIndex > -1) {
            const item = cart[itemIndex];
            if (removeAll || item.quantity < 2) {
                cart.splice(itemIndex, 1);
            }
            else {
                item.quantity -= 1;
                cart[itemIndex] = item;
            }
        }

        this.setState({cart});
    };

    saveCart = () => {
        let cart = [...this.state.cart];

        if (this.props.session || cart.length) {
            this.setData(true);

            saveCart(cart)
                .then(() => {
                    this.setData(false);
                    this.props.history.push('/hall')
                })
                .catch(() => {
                    // do nothing
                });
        }

    };

    render() {
        const {processing, categories, activeCategory, cart} = this.state;

        if (processing) return (
            <div style={{textAlign: 'center'}}>
                <Loader
                    type="Triangle"
                    color={GREEN_COLOR}
                    height="40px"
                    width="40px"/>
            </div>
        );

        return (
            <Transition>
                <div className={'container-fluid'}>
                    <div className="row" style={{paddingTop: "5%"}}>
                        <div className="col-md-7">
                            <Container>
                                <ItemCategories activeCategory={activeCategory} categories={categories}
                                                onSetActive={this.setCategory} onAddItem={this.onAddItem}/>
                            </Container>
                        </div>

                        <div className="col-md-5" style={{paddingRight: 0}}>
                            <Container>
                                <ItemCart cart={cart} onRemoveItem={this.onRemoveItem} onAddItem={this.onAddItem}
                                          saveCart={this.saveCart}/>
                            </Container>
                        </div>
                    </div>
                </div>
            </Transition>
        );
    }
}

const mapStateToProps = state => {
    return {
        session: state.server_reducer.playerSession
    }
};

const connected = connect(mapStateToProps)(Items);
export default connected;