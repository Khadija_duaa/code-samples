import React from 'react';
import OnlineMembersComponent from '../../../../Components/OnlineMembers/OnlineMembers';
import {SERVER_URL} from "../../../../server";


class CategoryDetails extends React.Component {

    render() {
        const {items, categoryIcon, onClick} = this.props;

        if (!items || !items.length || !onClick) return null;

        return (
            <div className="row">
                {items.map((data, index) => {
                    return (
                        <div className="col-md-4" key={index} style={{paddingLeft: '20px'}} onClick={() => onClick(data)}>
                            <OnlineMembersComponent
                                image={`${SERVER_URL}/photos/${categoryIcon}`} name={data.name} price= {`SEK ${data.price}`}
                                style={{width: '35px', height: '35px'}} outerStyle={{marginTop: '4px',marginRight: '5px', padding: "unset", float: "right"}} paddingStyle={{padding:'unset'}}
                                paraStyle={{color: '#BEBEBE', fontFamily: 'serif', fontSize: '16px'}}/>
                        </div>
                    )
                })}
            </div>
        );
    }
}


export default CategoryDetails;
