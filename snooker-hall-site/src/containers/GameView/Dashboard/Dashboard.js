import React from 'react';
import ContentPanel from "./ContentPanel/ContentPanel";
import DashboardCategories from "./DashboardCategoryPanel/CategoryPanel";
import Transition from '../../../Components/Transition/Transition';
import {connect} from 'react-redux';

const renderAllPanels = ()=>{
    return(
        <div className={"row"} style={{paddingTop:"15%"}}>
            <ContentPanel divClass={"col-md-4 offset-md-0"}/>
            <DashboardCategories divClass={"col-md-7 offset-md-1"}/>
        </div>
    )
};

class Dashboard extends React.Component{
    render(){

        if(!this.props.activeUser){
            return null;
        }
        return(
            <div>
                <Transition>
                    {renderAllPanels()}
                </Transition>
            </div>
        )
    }
}

const mapStateToProps = state =>{
    return{
        activeUser:state.user_reducer.activeUser
    }
};

export default connect(mapStateToProps)(Dashboard)