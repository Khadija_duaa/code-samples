import React from 'react';
import Heading from '../../../../Components/Dashboard/Contents/Heading';

import {connect} from 'react-redux';
import {formatName} from "../../../../utils/common-utils";

class ContentPanel extends React.Component {

    getHeading = () => <Heading title={`Hi ${formatName(this.props.activeUser)}`}/>;


    render() {
        const {divClass} = this.props;
        return (
            <div className={divClass && divClass}>
                {this.getHeading()}

            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        activeUser: state.user_reducer.activeUser
    };
};

export default connect(mapStateToProps)(ContentPanel);