import React from 'react';
import NavTitle from '../../../../Components/Dashboard/Navbar/MainTitle/Title';
import NavItems from '../../../../Components/Dashboard/Navbar/NavItems/NavMenu';

class Navbar extends React.Component {

    state = {
        width:''
    };

    componentDidMount() {
        window.addEventListener('resize',this.resize.bind(this));
        this.resize();
    }

    componentWillUnmount() {
        window.removeEventListener('resize',this.resize);
    }

    resize = ()=>{
        this.setState({width:window.screen.width})
    };

    // Navbar Styling
    navContainerStyle = {
        background: "rgba(31, 31, 31,0.9)",
        border: "1px solid  rgba(206, 210, 219,0.7)",
        borderRadius: "5px",
        padding: this.state.width > 768 ?"1% 3%":'1% 2.5%',
        width: "100%",
        display: "inline-block"
    };



    render() {

        let ipadWidth = Boolean(this.state.width < 1024);

        return (

            <div style={this.navContainerStyle}>
                <div style={ipadWidth?{textAlign:"center"}:{width: "15%", float: "left"}}>
                    <NavTitle/>
                </div>

                <div style={!ipadWidth?{width: "85%", float: "right"}:null}>
                    <NavItems style={ipadWidth?{textAlign:"center",paddingInlineStart:"unset",marginTop:"10px"}:null}/>
                </div>
            </div>
        )
    }
}

export default Navbar;