import React from 'react';
import Categories from '../../../../Components/Dashboard/Categories/Categories';
import connect from 'react-redux/es/connect/connect';
import './CategoryPanel.css';


const getCategories =(props)=> {
    let {categoriesArray} = props;

    categoriesArray.pop();

    if(!props.hallAccess){
        categoriesArray.push(props.logoutObject);
    }else{
        categoriesArray.push(props.orderFoodObject);
    }

    return categoriesArray.map((data,index) => {
        return (
            <div key={index} className="col-md-4" >
                <Categories style={{cursor:"pointer", marginBottom: '20%'}}
                            src={`/FloorView_images/Categories/${data.src}`}
                            text={data.text} link={data.link} hoverClass={data.hoverClass}/>
            </div>
        )
    });
};


class CategoryPanel extends React.Component {
    render(){
        const {divClass} = this.props;

        return(
            <div className={divClass && divClass}>
                <div className={"row categoriesRow"}>
                    {getCategories(this.props)}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state =>{
    return{
        categoriesArray : state.gv_reducer.activeLanguage.dashboard.categoryDashboard_Array,
        hallAccess: Boolean(state.server_reducer.hallAccessToken),
        orderFoodObject:state.gv_reducer.activeLanguage.dashboard.orderFoodObject,
        logoutObject:state.gv_reducer.activeLanguage.dashboard.logoutObject
    }
};

export default connect(mapStateToProps)(CategoryPanel)