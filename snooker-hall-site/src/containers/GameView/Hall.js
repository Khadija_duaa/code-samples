import React from 'react';
import {withRouter, Switch, Route, Redirect} from 'react-router-dom';
import Verification from "./Login_Register/Verification/VerificationDialog";
import Login from "./Login_Register/Member";
import Recovery from "./Login_Register/Recover/Recovery"
import GameView from './GameView';
import {NotificationContainer} from 'react-notifications';


const hall = () => {
    return (
        <div>
            <Switch>
                <Route path="/hall/login/verification" component={Verification}/>
                <Route path="/hall/login/recovery" component={Recovery}/>
                <Route path="/hall/login" component={Login}/>
                <Route path='/hall' component={GameView}/>
                <Redirect to='/hall'/>
            </Switch>
            <NotificationContainer/>
        </div>
    );
};

export default withRouter(hall);