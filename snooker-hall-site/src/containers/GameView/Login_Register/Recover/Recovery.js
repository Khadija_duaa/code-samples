import React from 'react'
import {Field, reduxForm, SubmissionError} from 'redux-form'
import {MuiThemeProvider} from 'material-ui'
import connect from 'react-redux/es/connect/connect'
import {forgotPassword} from "../../../../store/User/user-actions";
import TextField from '../Sign Up/FormFields/TextField';
import FormButton from '../../../../Components/FormComponents/FormButton/FormButton';
import ErrorMessage from '../../../../Components/FormComponents/ErrorMessage/ErrorMessage';
import Loader from 'react-loader-spinner';
import {NavLink, withRouter} from 'react-router-dom';

import './Recovery.css';

import {required, email} from "../../../../utils/validations";
import Transition from "../../../GameView/Login_Register/Transition/Transition";

class Recovery extends React.Component {

    state={
       success:'',
    };

    submitValues = formData => {
        return new Promise((resolve, reject) => {
            this.setState({ success: ''});
            this.props.forgotPassword(formData, () => {
                    resolve(this.setState({success:this.props.forgotPass_success_Label}))
                },
                (errorCallback) => {
                    reject(errorCallback);
                });
        }).catch(errors => {
            throw new SubmissionError(errors);
        });
    };

    getForm = () => {
        const {handleSubmit, submitting} = this.props;

        return (
            <MuiThemeProvider>
                {!this.state.success ?
                    <form id={"signin_form"} onSubmit={handleSubmit(this.submitValues)}>
                        <div>
                            <Field
                                icon={"/img/registrationIcons/email.svg"}
                                name="email"
                                component={TextField}
                                label={this.props.enterEmail_label}
                                validate={[required, email]}
                            />
                        </div>
                        <div style={{cursor: "pointer", margin: "20px auto"}}>
                            <FormButton disabled={submitting}>
                                {this.props.submit_label}
                            </FormButton>
                        </div>
                    </form> :
                    <div style={{cursor: "pointer", margin: "20px 100px 40px 100px"}}>
                        <FormButton style={{width:'100px', cursor:'pointer',fontSize:'12px', fontWeight:'700'}} disabled={submitting} onClick={() =>{this.props.history.push('/hall/login')}}>
                            {this.props.login_label}
                        </FormButton>
                    </div>}

            </MuiThemeProvider>
        );
    };

    getLogo = () => {
        return (
            <div className="logoStyle">
                <NavLink to={"/hall"}>
                    <img style={{width: "5vw",cursor:"pointer"}} src={"/img/logo/logo.png"} alt={'logo'}/>
                </NavLink>
            </div>
        );
    };
    getLoader = () => {
        if (!this.props.processing) return null;
        return (
            <div style={{textAlign: 'center'}}>
                <Loader
                    type="Triangle"
                    color="#fff"
                    height="40px"
                    width="40px"/>
            </div>
        );
    };


    render() {
        return (
            <div className="loginMain">
                <Transition>
                    <div className="forgotPassContainer">
                        {this.getLogo()}
                        {this.getLoader()}

                        <ErrorMessage large>
                            {this.props.error}
                        </ErrorMessage>
                        <p style={{color: 'green', textAlign:'center'}}>
                            {this.state.success}
                        </p>

                        {this.getForm()}
                    </div>
                </Transition>
            </div>


        )
    }
}

const recovery = reduxForm({form: 'recovery'})(Recovery);

const mapStateToProps = (state) => {
    return {
        processing: state.user_reducer.processing,
        submit_label: state.lp_reducer.activeLanguage.signup_form.submit,
        enterEmail_label: state.lp_reducer.activeLanguage.signup_form.enterEmail,
        forgotPassword_label: state.lp_reducer.activeLanguage.signup_form.forgotPassword,
        forgotPass_success_Label: state.lp_reducer.activeLanguage.signup_form.successLabel,
        login_label: state.lp_reducer.activeLanguage.signup_form.login_label,

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        forgotPassword: (formData, cb, errorCallback) => dispatch(forgotPassword(formData, cb, errorCallback))
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(recovery);
export default withRouter(connected);
