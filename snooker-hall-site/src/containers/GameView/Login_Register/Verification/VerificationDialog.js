import React from 'react'
import {Field, reduxForm, SubmissionError} from 'redux-form'
import {MuiThemeProvider} from 'material-ui'
import connect from 'react-redux/es/connect/connect'
import Loader from 'react-loader-spinner'
import {withRouter} from 'react-router-dom';
import {verifyCodes} from "../../../../store/User/user-actions";
import Transition from '../../../GameView/Login_Register/Transition/Transition';
import ErrorMessage from '../../../../Components/FormComponents/ErrorMessage/ErrorMessage';
import TextField from '../Sign Up/FormFields/TextField';
import {required} from "../../../../utils/validations";

import './VerficationDialog.css'

const btnStyle = {
    backgroundColor: "#fff",
    color: "#000",
    textAlign: "center",
    padding: "5px",
    width: "50%",
    border: "1px solid #fff",
    borderRadius: "2px",
    margin: "5vh auto",
    marginBottom: "2vh",
    cursor: "pointer",
    fontSize: "12px",
    fontWeight: "700",
    display: "block",
};

class VerificationDialog extends React.Component {

    componentWillMount() {
        let {formData} = this.props;
        if (!formData || !formData.email) {
            this.props.history.push('/hall/login');
        }
    }

    submitValues = codes => {
        return new Promise((resolve, reject) => {
            const {_id, callback, next} = this.props.location.state;
            console.log("Location" , this.props.location.state);
            this.props.verifyCodes(codes, _id, next, (...params) => {
                callback(...params);
                resolve()
            }, (errors) => {
                reject(errors);
            });
        }).catch(errors => {
            throw new SubmissionError(errors);
        });
    };

    getHeading = () => {
        return (
            <h5 style={{
                marginTop: "5vh",
                textAlign: "center",
                marginBottom: "unset",
                fontWeight: "bold",
                color: "#fff"
            }}>
                {this.props.title}
            </h5>
        );
    };

    getLoader = () => {
        if (this.props.processing) return <Loader type="Triangle" color="#fff" height="40px" width="40px"/>;

        if (this.props.verficationError) return <ErrorMessage>{this.props.verificationError}</ErrorMessage>;

        return null;
    };


    getEmailOtpField = () => {
        if (this.props.location && this.props.location.state && this.props.location.state.emailVerificationRequired) {
            return (
                <div>
                    <Field name="emailOtp" component={TextField} label={this.props.email + ' OTP'}
                           validate={[required]} disabled={this.props.processing}/>
                </div>
            );
        }

        return null;
    };

    getPhoneOtpField = () => {
        if (this.props.location && this.props.location.state && this.props.location.state.smsVerificationRequired) {
            return (
                <div>
                    <Field name="phoneOtp" component={TextField} label={this.props.phone + ' OTP'}
                           validate={[required]} disabled={this.props.processing}/>
                </div>
            );
        }

        return null;
    };

    getForm = () => {
        const {handleSubmit} = this.props;

        return (
            <MuiThemeProvider>
                <form id={"verify_form"} onSubmit={handleSubmit(this.submitValues)}>

                    {this.getEmailOtpField()}

                    {this.getPhoneOtpField()}

                    <div style={{cursor: "pointer", margin: "20px auto"}}>
                        <button style={btnStyle} disabled={this.props.processing}>
                            Verify
                        </button>
                    </div>

                </form>

            </MuiThemeProvider>
        );
    };

    render() {
        return (
            <div className={"otpMain"}>
                <Transition>
                    <div className={"otpContainer"} style={{textAlign: 'center'}}>
                        {this.getHeading()}
                        <ErrorMessage large>
                            {this.props.error}
                        </ErrorMessage>
                        {this.getLoader()}
                        {this.getForm()}
                    </div>
                </Transition>
            </div>
        );
    }
}

const verificationDialog = reduxForm({form: 'VerificationDialog'})(VerificationDialog);

const mapStateToProps = (state) => {
    return {
        title: state.lp_reducer.activeLanguage.verification_form.title,
        email: state.lp_reducer.activeLanguage.signup_form.email,
        phone: state.lp_reducer.activeLanguage.signup_form.phoneNumber,

        required: state.lp_reducer.activeLanguage.signup_form.required,

        verificationError: state.user_reducer.error,
        formData: state.user_reducer.formData,
        phoneID: state.user_reducer.verifyID,
        processing: state.user_reducer.processing,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        verifyCodes: (codes, _id, next, callback, errCallback) => dispatch(verifyCodes(codes, _id, next, callback, errCallback)),
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(verificationDialog);
export default withRouter(connected);
