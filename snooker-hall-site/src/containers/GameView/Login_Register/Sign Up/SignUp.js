import React from 'react'
import {resetRedux, sendCodes, saveData} from "../../../../store/User/user-actions";
import Loader from 'react-loader-spinner'
import connect from 'react-redux/es/connect/connect';
import {withRouter} from 'react-router-dom';
import RegistrationForm from './SignUpForm';
import {SubmissionError} from 'redux-form';

import './SignUp.css';

class Register extends React.Component {

    componentDidMount() {
        this.props.resetRedux();
    };

    submitValues = (formData) => {
        this.props.resetRedux();
        this.props.saveFormData(formData);

        let {push} = this.props.history;

        return new Promise((resolve, reject) => {
            this.props.verifyUser(
                (state) => {
                    push({pathname: '/hall/login/verification', state});
                    resolve();
                },
                () => {
                    push('/hall/profile');
                    resolve();
                },
                (errors) => {
                    reject(errors);
                }
            )
        }).catch(errors => {
            throw new SubmissionError(errors);
        });
    };

    getForm = () => {
        let initialValues = {};
        if (this.props.formData) {
            initialValues = {...this.props.formData};
        }

        return (
            <RegistrationForm {...this.props}
                              initialValues={initialValues}
                              enableReinitialize={true}
                              onSubmit={this.submitValues}/>
        );
    };

    getLoader = () => {
        if (this.props.processing)
            return (
                <div style={{textAlign: 'center'}}>
                    <Loader type="Triangle" color="#18d26e" height="40px" width="40px"/>
                </div>
            );

        return null;
    };

    render() {
        return (
            <div>
                {this.getLoader()}
                {this.getForm()}
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        processing: state.user_reducer.processing,
        registerTitle: state.lp_reducer.activeLanguage.header.h_signup,

        lastName: state.lp_reducer.activeLanguage.signup_form.lastName,
        firstName: state.lp_reducer.activeLanguage.signup_form.firstName,
        email: state.lp_reducer.activeLanguage.signup_form.email,
        password: state.lp_reducer.activeLanguage.signup_form.password,
        confirmPassword: state.lp_reducer.activeLanguage.signup_form.confirmPassword,
        streetName: state.lp_reducer.activeLanguage.signup_form.streetName,
        securityNumber: state.lp_reducer.activeLanguage.signup_form.securityNumber,
        phoneNumber: state.lp_reducer.activeLanguage.signup_form.phoneNumber,
        city: state.lp_reducer.activeLanguage.signup_form.city,
        postalCode: state.lp_reducer.activeLanguage.signup_form.postalCode,
        highestBrake: state.lp_reducer.activeLanguage.signup_form.highestBrake,
        termsCheck: state.lp_reducer.activeLanguage.signup_form.termsCheck,

        mismatchError: state.lp_reducer.activeLanguage.signup_form.passwordMismatch,

        formData: state.user_reducer.formData,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        saveFormData: (data) => dispatch(saveData(data)),
        verifyUser: (callback, cb, errorCallback) => dispatch(sendCodes(callback, cb, errorCallback)),
        resetRedux: () => dispatch(resetRedux())
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Register);
export default withRouter(connected);
