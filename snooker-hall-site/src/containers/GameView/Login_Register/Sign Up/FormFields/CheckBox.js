import React from 'react';
import ErrorMessage from '../../../../../Components/FormComponents/ErrorMessage/ErrorMessage';
import Checkbox from 'material-ui/Checkbox'

const renderCheckbox = ({input, label, ...custom, meta: {touched, error},}) => (
    <span>
        <Checkbox
            iconStyle={{fill: "white"}}
            label={label}
            checked={!!input.value}
            onCheck={input.onChange}

        />
        {touched && (error && <ErrorMessage>{error}</ErrorMessage>)}
    </span>
);

export default renderCheckbox;