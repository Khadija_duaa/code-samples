import React from 'react';
import TextField from 'redux-form-material-ui/es/TextField'

const renderTextField = ({input, icon, label, meta: {touched, error}, ...custom}) => (

    <div style={{width: "100%", position: "relative", display: "inline-block"}}>

        <img src={icon} style={{position: 'absolute', left: 0, top: "60%", width: 15}} alt={''}/>

        <TextField
            style={{textAlign: "left", textIndent: 30, width: "100%"}}
            floatingLabelText={label}
            floatingLabelStyle={{left: "0px"}}
            errorText={touched && error}
            errorStyle={{position: "absolute", bottom: "-1vh", fontSize: "10px"}}
            {...input}
            {...custom}
        />
    </div>

);

export default renderTextField;