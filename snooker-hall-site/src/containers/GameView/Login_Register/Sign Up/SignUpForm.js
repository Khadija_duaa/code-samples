import React from 'react';
import TextField from "./FormFields/TextField";
import CheckBox from "./FormFields/CheckBox";
import {Field, reduxForm} from 'redux-form'
import {MuiThemeProvider} from 'material-ui'
import ErrorMessage from '../../../../Components/FormComponents/ErrorMessage/ErrorMessage';
import {connect} from 'react-redux';
import {
    required,
    email,
    swedishPhone,
    pakistanPhone,
    isChecked,
    minLength4,
    ssnLength,
    ssnPakistanLength,
    integer,
    validSwedishPostalCode,
    validPakistanPostalCode

} from "../../../../utils/validations";

const btnStyle = {
    backgroundColor: "#fff",
    color: "#000",
    textAlign: "center",
    padding: "5px",
    width: "100px",
    border: "1px solid #fff",
    borderRadius: "2px",
    margin: "5px auto",
    marginBottom: "2vh",
    cursor: "pointer",
    fontSize: "12px",
    fontWeight: "700",
    display: "block",
};


const prepareFields = (props) => {
    const fields = prepareFieldsJson(props);

    const jsx = [];
    let fieldsJsx = [];


    const isOdd = (fields.length % 2) !== 0;

    for (let index = 0; index < fields.length; index++) {
        const isOddLast = isOdd && (index + 1) === fields.length;
        const field = fields[index];

        let fieldJsx = (
            <div key={index} className={`col-md-${isOddLast ? 12 : 6}`}>
                <Field
                    icon={`/img/registrationIcons/${field.icon}`}
                    name={field.name}
                    component={field.component}
                    floatingLabelText={field.label}
                    type={field.type || 'text'}
                    validate={field.validate || []}
                    disabled={props.processing}
                />
            </div>
        );

        fieldsJsx.push(fieldJsx);

        if (fieldsJsx.length === 2 || isOddLast) {
            jsx.push((
                <div key={jsx.length} className={"row"}>
                    {fieldsJsx}
                </div>
            ));
            fieldsJsx = [];
        }
    }

    return jsx;
};


const prepareFieldsJson = (props) => {
    let fields = [];

    let {serverConfigurations} = props;

    let postalCode = '',ssnValidate = '',phoneValidate ='';

    if(serverConfigurations && serverConfigurations.countryCode &&  serverConfigurations.countryCode=== 'PK'){
        postalCode = validPakistanPostalCode;
        phoneValidate = pakistanPhone;
        ssnValidate = ssnPakistanLength;
    }else {
        postalCode = validSwedishPostalCode;
        phoneValidate = swedishPhone;
        ssnValidate = ssnLength;
    }

    fields.push({
        icon: 'user.svg',
        name: 'firstName',
        component: TextField,
        label: props.firstName,
        validate: [required]
    });
    fields.push({
        icon: 'user.svg',
        name: 'lastName',
        component: TextField,
        label: props.lastName,
        validate: [required]
    });

    fields.push({
        icon: 'email.svg',
        name: 'email',
        component: TextField,
        label: props.email,
        validate: [required, email]
    });


    fields.push({
        icon: 'call.svg',
        name: 'phoneNumber',
        component: TextField,
        label: props.phoneNumber,
        validate: [required, phoneValidate]
    });

    fields.push({
        icon: 'passpin.svg',
        name: 'password',
        component: TextField,
        type: 'password',
        label: props.password,
        validate: [required, minLength4]
    });
    fields.push({
        icon: 'passpin.svg',
        name: 'confirmPassword',
        component: TextField,
        type: 'password',
        label: props.confirmPassword,
        validate: [required, minLength4]
    });


    fields.push({
        icon: 'ssn.svg',
        name: 'ssn',
        component: TextField,
        label: props.securityNumber,
        validate: [required,integer, ssnValidate]
    });

    fields.push({icon: 'address.svg', name: 'city', component: TextField, label: props.city, validate: [required]});

    fields.push({
        icon: 'address.svg',
        name: 'street',
        component: TextField,
        label: props.streetName,
        validate: [required]
    });

    fields.push({
        icon: 'address.svg',
        name: 'postalCode',
        component: TextField,
        label: props.postalCode,
        validate: [required, postalCode]
    });

    fields.push({
        icon: 'ssn.svg',
        name: 'highestBrake',
        component: TextField,
        label: props.highestBrake,
        validate: [integer]
    });

    return fields;
};


const form = (props) => {
    const {handleSubmit, submitting, processing, error} = props;

    return (
        <MuiThemeProvider>
            <form id={"register_form"} onSubmit={handleSubmit}>

                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <ErrorMessage large>
                            {error}
                        </ErrorMessage>
                    </div>
                </div>

                {prepareFields(props)}


                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <Field name="termsChecked" component={CheckBox}
                               label={props.termsCheck} validate={isChecked} disabled={processing}/>
                    </div>
                </div>

                <div style={{margin: "0 auto"}}>
                    <button style={btnStyle} disabled={submitting || processing}>
                        {props.registerTitle}
                    </button>
                </div>

            </form>
        </MuiThemeProvider>
    );

};

const validate = (values, props) => {
    const errors = {};

    if (values.password !== values.confirmPassword) {
        errors.confirmPassword = props.mismatchError;
        errors.password = props.mismatchError
    }

    return errors
};

const mapStateToProps = (state)=>{
    return{
        serverConfigurations:state.server_reducer.serverConfigurations
    }
};

const redux = reduxForm({form: 'register', validate})(form);
const connected = connect(mapStateToProps)(redux);
export default connected