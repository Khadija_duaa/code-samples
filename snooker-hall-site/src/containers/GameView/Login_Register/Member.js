import React from 'react'
import Login from './Sign In/SignIn'
import Register from './Sign Up/SignUp'
import 'react-notifications/lib/notifications.css';
import connect from 'react-redux/es/connect/connect';
import './Member.css';
import Transition from '../../GameView/Login_Register/Transition/Transition';
import {NavLink} from 'react-router-dom';
import {resetRedux} from "../../../store/User/user-actions";

const li_style = {
    display: "inline-block",
    marginRight: "20px",
    cursor: "pointer",
    fontWeight: "bold",
    listStyle: "none",
    color: "#787878"
};

const activeStyles = {
    ...li_style,
    borderBottom: '1px solid #fff',
    color: '#fff'
};

const inactiveStyles = {
    ...li_style,
    borderBottom: 'unset'
};

class LoginForm extends React.Component {

    state = {
        login: true
    };

    showSignIn = () => {
        this.setState({login: true});
    };

    showRegister = () => {
        this.setState({login: false});
    };


    getLogo = () => {
        return (
            <div className="logoStyle">
                <NavLink to={"/hall"}>
                    <img style={{width: "5vw",cursor:"pointer"}} src={"/img/logo/logo.png"} alt={'logo'}/>
                </NavLink>
            </div>
        );
    };

    getForm = () => this.state.login ? <Login/> : <Register/>;

    getFormNav = () => {
        let {login} = this.state;
        return (
            <ul style={{padding: "unset", marginBottom: "unset"}}>

                <li style={login ? activeStyles : inactiveStyles}
                    onClick={this.showSignIn}>
                    {this.props.loginTitle}
                </li>

                <li style={inactiveStyles}> |</li>

                <li style={login ? inactiveStyles : activeStyles}
                    onClick={this.showRegister}>
                    {this.props.regirsterTitle}
                </li>
            </ul>
        );
    };

    render() {
        return (
            <div className="loginMain">
                <Transition>
                    <div className={`container ${this.state.login ? 'signInContainer' : 'registerContainer'}`}>
                        {this.getLogo()}
                        {this.getFormNav()}
                        {this.getForm()}
                    </div>
                </Transition>
            </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        loginTitle: state.lp_reducer.activeLanguage.header.h_signIn,
        regirsterTitle: state.lp_reducer.activeLanguage.header.h_signup
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetRedux: () => dispatch(resetRedux())
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);