import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {MuiThemeProvider} from 'material-ui'
import connect from 'react-redux/es/connect/connect'
import Loader from 'react-loader-spinner';
import {withRouter} from 'react-router-dom';
import Transition from '../../../GameView/Login_Register/Transition/Transition';
import ErrorMessage from '../../../../Components/FormComponents/ErrorMessage/ErrorMessage';
import TextField from '../Sign Up/FormFields/TextField';
import {required} from "../../../../utils/validations";
import {requestInHallAccess} from "../../../../socket-io/socket-utils"
import './adminVerification.css'

const btnStyle = {
    backgroundColor: "#fff",
    color: "#000",
    textAlign: "center",
    padding: "5px",
    width: "50%",
    border: "1px solid #fff",
    borderRadius: "2px",
    margin: "5vh auto",
    marginBottom: "2vh",
    cursor: "pointer",
    fontSize: "12px",
    fontWeight: "700",
    display: "block",
};

class AdminVerification extends React.Component {

    constructor(props) {
        super(props);


        this.state = {
            errorMsg: null,
            processing: false,
            username: '',
            password: '',
        };
    }

    startProcessing = () => {
        this.setState({processing: true, errorMsg: null});
    };

    stopProcessing = (errorMsg = null) => {
        this.setState({processing: false, errorMsg});
    };

    submitValues = (formData) => {
        this.startProcessing();
        requestInHallAccess(formData.username, formData.password, (error) => {

            if (!error) {
                this.props.history.push('/hall');
            }
            else {
                this.stopProcessing('Invalid Credentials');
            }
        });
    };

    getHeading = () => {
        return (
            <h5 style={{
                marginTop: "5vh",
                textAlign: "center",
                marginBottom: "unset",
                fontWeight: "bold",
                color: "#fff"
            }}>
                {this.props.title}
                {this.props.hallAccessToken ?
                    <ErrorMessage>{this.props.alreadyAuthMsg}</ErrorMessage>
                    : null}
            </h5>
        );
    };

    getLoader = () => {
        if (this.state.processing) return <Loader type="Triangle" color="#fff" height="40px" width="40px"/>;
        return null;
    };


    getUsernameField = () => {
        return (
            <div>
                <Field name="username" component={TextField} label={this.props.email}
                       validate={[required]} disabled={this.state.processing}/>
            </div>
        );

    };

    getPasswordField = () => {
        return (
            <div>
                <Field name="password" component={TextField} type={'password'} label={this.props.password}
                       validate={[required]} disabled={this.state.processing}/>
            </div>
        );

    };

    getForm = () => {
        const {handleSubmit} = this.props;

        return (
            <MuiThemeProvider>
                <form id={"verify_form"} onSubmit={handleSubmit(this.submitValues)}>

                    {this.getUsernameField()}

                    {this.getPasswordField()}

                    <div style={{cursor: "pointer", margin: "20px auto"}}>
                        <button style={btnStyle} disabled={this.state.processing}>
                            {this.props.authorizeText}
                        </button>
                    </div>

                </form>

            </MuiThemeProvider>
        );
    };

    render() {
        return (
            <Transition>
                <div className={"otpContainer"} style={{textAlign: 'center'}}>
                    {this.getHeading()}
                    {this.state.errorMsg ? <ErrorMessage large>{this.state.errorMsg}</ErrorMessage> : null}
                    {this.getLoader()}
                    {this.getForm()}
                </div>
            </Transition>
        );
    }
}

const adminVerification = reduxForm({form: 'adminVerification'})(AdminVerification);


const mapStateToProps = (state) => {
    return {
        title: state.lp_reducer.activeLanguage.admin_verification_form.title,
        email: state.lp_reducer.activeLanguage.admin_verification_form.username,
        password: state.lp_reducer.activeLanguage.admin_verification_form.password,
        required: state.lp_reducer.activeLanguage.signup_form.required,
        hallAccessToken: state.server_reducer.hallAccessToken,
        alreadyAuthMsg: state.lp_reducer.activeLanguage.admin_verification_form.already_auth,
        authorizeText: state.lp_reducer.activeLanguage.admin_verification_form.authorizeText,
    }
};

const connected = connect(mapStateToProps)(adminVerification);
export default withRouter(connected);
