import React from 'react'
import {Field, reduxForm, SubmissionError} from 'redux-form'
import {MuiThemeProvider} from 'material-ui'
import connect from 'react-redux/es/connect/connect'
import {login} from "../../../../store/User/user-actions";
import TextField from '../Sign Up/FormFields/TextField';
import FormButton from '../../../../Components/FormComponents/FormButton/FormButton';
import ErrorMessage from '../../../../Components/FormComponents/ErrorMessage/ErrorMessage';
import Loader from 'react-loader-spinner';
import {Link} from "react-router-dom";
import {withRouter} from 'react-router-dom';
import './SignIn.css';

import {required, email} from "../../../../utils/validations";

class SignIn extends React.Component {

    submitValues = formData => {
        return new Promise((resolve, reject) => {
            this.props.login(formData, () => {
                    this.props.history.push('/hall');
                    resolve()
                },
                (errors) => {
                    reject(errors);
                });
        }).catch(errors => {
            throw new SubmissionError(errors);
        });
    };

    getForm = () => {
        const {handleSubmit, submitting} = this.props;

        return (
            <MuiThemeProvider>
                <form id={"signin_form"} onSubmit={handleSubmit(this.submitValues)}>
                    <div>
                        <Field
                            icon={"/img/registrationIcons/email.svg"}
                            name="email"
                            component={TextField}
                            label={this.props.email_label}
                            validate={[required, email]}
                        />
                    </div>

                    <div style={{marginBottom:'20px'}}>
                        <Field
                            icon={"/img/registrationIcons/passpin.svg"}
                            name="password"
                            component={TextField}
                            label={this.props.password_label}
                            type={"password"}
                            validate={[required]}
                        />
                    </div>

                    <div className="forgotPassword">
                        <Link to={'/hall/login/recovery'}>
                            <p>{this.props.forgotPassword_label}</p>
                        </Link>
                    </div>

                    <div style={{cursor: "pointer", margin: "20px auto"}}>
                        <FormButton disabled={submitting}>
                            {this.props.loginTitle}
                        </FormButton>
                    </div>
                </form>
            </MuiThemeProvider>
        );
    };

    getLoader = () => {
        if (!this.props.processing) return null;
        return (
            <div style={{textAlign: 'center'}}>
                <Loader
                    type="Triangle"
                    color="#fff"
                    height="40px"
                    width="40px"/>
            </div>
        );
    };



    render() {
        return (
            <div>
                {this.getLoader()}

                <ErrorMessage style={{marginTop:"20px"}} large>
                    {this.props.error}
                </ErrorMessage>

                {this.getForm()}
            </div>

        )
    }
}

const signIn = reduxForm({form: 'signin'})(SignIn);

const mapStateToProps = (state) => {
    return {
        processing: state.user_reducer.processing,
        loginTitle: state.lp_reducer.activeLanguage.header.h_signIn,
        email_label: state.lp_reducer.activeLanguage.signup_form.email,
        enterEmail_label: state.lp_reducer.activeLanguage.signup_form.enterEmail,
        submit_label: state.lp_reducer.activeLanguage.signup_form.submit,
        forgotPassword_label: state.lp_reducer.activeLanguage.signup_form.forgotPassword,
        password_label: state.lp_reducer.activeLanguage.signup_form.password
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (formData, cb, errorCallback) => dispatch(login(formData, cb, errorCallback))
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(signIn);
export default withRouter(connected);
