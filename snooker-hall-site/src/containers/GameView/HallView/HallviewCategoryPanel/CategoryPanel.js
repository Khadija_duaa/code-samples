import React from 'react';
import Categories from '../../../../Components/Dashboard/Categories/Categories';
import connect from 'react-redux/es/connect/connect';


const getCategories =(props)=> {
    let {categories} = props;

    categories.shift();

    if(!props.hallAccess){
        categories.unshift(props.hallMemberObject);
    }else{
        categories.unshift(props.orderFoodObject);
    }

    return categories.map((data,index) => {
        return (
            <div key={index} className="col-md-3" style={{float: 'center'}}>
                <Categories style={{cursor:"pointer"}} src={`/FloorView_images/Categories/${data.src}`}
                            text={data.text} link={data.link} hoverClass={data.hoverClass}/>
            </div>
        );
    });
};


class HallviewCategoryPanel extends React.Component{
    render(){
        const {divClass,divStyle} = this.props;
        return(
            <div className={divClass && divClass} style={divStyle&&divStyle}>
                <div className={"row"}>
                    {getCategories(this.props)}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state =>{
    return{
        categories : state.gv_reducer.activeLanguage.hallView.categoryHallView_Array,
        hallAccess: Boolean(state.server_reducer.hallAccessToken),
        profileObject : state.gv_reducer.activeLanguage.hallView.profileObject,
        orderFoodObject : state.gv_reducer.activeLanguage.hallView.orderFoodObject,
        hallMemberObject : state.gv_reducer.activeLanguage.hallView.hallMemberObject,
    }
};

export default connect(mapStateToProps)(HallviewCategoryPanel);