import React from 'react';
import TableView from "./TableView/TableView";
import HallviewCategories from './HallviewCategoryPanel/CategoryPanel';
import WaitingList from "./WaitingList/WaitingList";
import {withRouter} from 'react-router-dom';
import Transition from '../../../Components/Transition/Transition';
import {connect} from 'react-redux';

class HallView extends React.Component {

    getCategories = () => {
        if (!this.props.activeUser) return <div className={"col-md-12"} style={{height : '14vh'}}/>;
        return <HallviewCategories divClass="col-md-12" divStyle={{padding: "unset"}}/>;
    };

    render() {
        return (
            <div>
                <Transition>
                    <div className={"row"} style={{paddingTop: "5%"}}>
                        <div className={"col-md-8"}>
                            <TableView divClass="col-md-12"/>
                            {this.getCategories()}
                        </div>
                        <div className={"col-md-4"}>
                            <WaitingList/>
                        </div>
                    </div>
                </Transition>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        activeUser: state.user_reducer.activeUser
    };
};

const connected = connect(mapStateToProps)(HallView);
export default withRouter(connected);