import React from 'react';
import connect from 'react-redux/es/connect/connect';
import TableUsers from "../../../../Components/HallView/Table/TableUsers";
import ScrollBar from "../WaitingList/ScrollBar";
import JoinButton from './JoinButton';
import {isInWaitingList, isInTable} from "../../../../utils/common-utils";
import Loader from 'react-loader-spinner';
import axios from "../../../../utils/axios";
import {ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import Title from '../../../../Components/Heading/Title';
import CircularButton from "../../../../Components/CircularButton/CircularButton";
import {removeFromQueue} from "../../../../utils/business-utils";
import './TableView.css';
import {NotificationManager} from 'react-notifications';
import {handleError} from '../../../../store/store-utils';


const tableStyle = {
    borderRadius: "5px",
    height: "440px",
    backgroundSize:"100% 95%",
    backgroundRepeat:"no-repeat",
    backgroundImage: "url(/FloorView_images/table.png)",
};


const titleStyle = {
    color: "white",
    fontWeight: "900",
    letterSpacing: "2px",
    marginBottom: 5,

};

const playerCountStyle = {
    color: "#006002",
    fontWeight: "normal",
    WebkitTextStroke: "0.8px",
    fontSize: "14px",
    letterSpacing: "1px",
    marginBottom: "10%",
};


class TableView extends React.Component {


    state = {
        processing: false,
        showModal: false,
    };

    toggleModal = () => {
        this.setState({showModal: !this.state.showModal});
    };

    removeFromQueue = (beinQueue) => {
        // close modal
        this.toggleModal();

        if (!beinQueue) {
            removeFromQueue();
        }
    };

    joinTableModal = () => {
        const buttonStyle = {
            fontWeight: "normal",
            width: "100%",
            color: '#fff',
            padding: "10px",
            borderRadius: "5px",
            border: "unset"
        };

        return (
            <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
                <ModalHeader style={{border: "unset"}}>
                    <Title afterClass={"blackDash"}>
                        {this.props.modalHeading}
                    </Title>
                </ModalHeader>

                <ModalBody>
                    <p style={{fontSize: 18}}>
                        {this.props.modalBody}
                    </p>
                </ModalBody>

                <ModalFooter style={{border: "unset"}}>
                    <CircularButton style={buttonStyle} backColor="#1C74BF" margin="0px 10px 0px 0px"
                                    onClick={() => this.removeFromQueue(false)}>
                        {this.props.modalNoText}
                    </CircularButton>

                    <CircularButton style={buttonStyle} backColor="#13C95F" margin="0px 0px 0px 10px"
                                    onClick={() => this.removeFromQueue(true)}>
                        {this.props.modalYesText}
                    </CircularButton>
                </ModalFooter>
            </Modal>
        )
    };

    setProcessing = (processing) => {
        if (processing) {
            this.setState({processing});
        }
        else {
            setTimeout(() => {
                this.setState({processing});
            }, 400);
        }
    };

    onJoinTable = (tableID, _switch = false, cb) => {
        this.setProcessing(true);

        axios.post(`/${_switch ? 'switch' : 'join'}-table/${tableID}`)
            .then(() => {
                this.setProcessing(false);
                if (!_switch && cb) {
                    cb();
                }
            })
            .catch(error => {
                this.setProcessing(false);
                let message = handleError(error);
                console.error(message, error);
                NotificationManager.error(message, '', 3000);
                console.error('Join Table', error);
            });
    };

    onLeaveTable = () => {
        this.setProcessing(true);
        axios.delete('/leave-table')
            .then(() => {
                // do nothing
                this.setProcessing(false);
            })
            .catch(error => {
                this.setProcessing(false);
                let message = handleError();
                console.error(message, error);
                NotificationManager.error(message, '', 3000);
                console.error('Leave Table', error);
            });
    };


    checkWaitingList = () => {


        if (this.props.waitingList.length === 1) {
            removeFromQueue();
        }
        else {
            this.toggleModal();
        }
    };

    getButton = (props, table_id) => {
        if (!props.activeUser) return null;


        if (this.state.processing) {
            return (
                <div style={{textAlign: 'center'}}>
                    <Loader
                        type="Triangle"
                        color={"white"}
                        height="40px"
                        width="40px"/>
                </div>
            );
        }
        else {
            if (isInTable(props.activeUser._id, table_id)) {
                return <JoinButton onClick={this.onLeaveTable}>Leave</JoinButton>
            }

            if (isInTable(props.activeUser._id)) {
                return <JoinButton onClick={() => this.onJoinTable(table_id, true)}>Switch</JoinButton>
            }

            if (isInWaitingList(props.waitingList, props.activeUser)) {
                return <JoinButton onClick={() => {
                    this.onJoinTable(table_id, false, () => {
                        this.checkWaitingList();
                    });

                }}>Join</JoinButton>
            }
            return null;
        }

    };

    renderTableData = (props) => props.tableData.map((table, index) => {
        let paddingArray = [
            {
                tablePadding:"25% 0px 0px 10%",
                scrollPadding:{paddingLeft:"inherit"}
            }
            ,
            {
                tablePadding:"25% 8% 0px 13%",
                scrollPadding:{padding:"unset"}
            }
            ,
            {
                tablePadding:"25% 16% 0px 10%",
                scrollPadding:{padding:"unset"}
            }
        ];
        return (
            <div key={table._id} className="col-md-4" style={{padding: "unset", textAlign: "center"}}>
                <div style={{
                    padding: paddingArray[index].tablePadding,
                    display: "inline-block",
                    width: "inherit",
                    textAlign: "left"
                }}>
                    <div style={{margin: "0px auto", display:"table"}}>
                        <h4 style={titleStyle}>
                            {table.name}
                        </h4>

                        <p className={"playerCounts"} style={playerCountStyle}>
                            {table.users.length} Players
                        </p>
                    </div>

                    <ScrollBar scrollType="tableScroll"
                               style={{...paddingArray[index].scrollPadding,paddingRight: "5%", maxHeight: "16vh", marginBottom: "8%"}}>
                        <TableUsers tablePlayers={table.users} {...this.props}/>
                    </ScrollBar>

                    {this.getButton(props, table._id)}

                </div>
            </div>
        )
    });

    render() {
        const {divClass} = this.props;

        return (
            <div className={divClass && divClass} style={tableStyle}>
                {this.joinTableModal()}
                <div className={"row"}>
                    {this.renderTableData(this.props)}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        waitingList: state.server_reducer.waitingList,
        activeUser: state.user_reducer.activeUser,
        tableData: state.server_reducer.hallTables,
        hallAccess: Boolean(state.server_reducer.hallAccessToken),

        modalHeading: state.gv_reducer.activeLanguage.hallView.joinModalHead,
        modalBody: state.gv_reducer.activeLanguage.hallView.joinModalBody,
        modalYesText: state.gv_reducer.activeLanguage.hallView.joinModalYesText,
        modalNoText: state.gv_reducer.activeLanguage.hallView.joinModalNoText

    }
};

export default connect(mapStateToProps)(TableView);