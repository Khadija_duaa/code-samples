import React from 'react';
import Transition from '../../../../Components/Transition/Transition';
import CircularButton from "../../../../Components/CircularButton/CircularButton";

class JoinButton extends React.Component {

    buttonStyle = {
        width:"100px",
        color : '#000',
        padding : "5px 1vw",
        borderRadius : "50px",
        border:"unset",
        fontWeight:"bold",
    };

    render() {
        return (
            <div style={{textAlign:"center"}}>
                <Transition>
                    <CircularButton style={this.buttonStyle} backColor={"#fff"}
                                    onClick={this.props.onClick}>
                        {this.props.children}
                    </CircularButton>
                </Transition>
            </div>
        )
    }
}

export default JoinButton;