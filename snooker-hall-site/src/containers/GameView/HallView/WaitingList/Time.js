import React from 'react';
import moment from 'moment';

class Time extends React.Component {

    intervalRef = null;

    state = {
        timeElapsed: 0,
    };

    updateTime = () => {
        const {time} = this.props;

        const durationMoment = moment().diff(moment(time));
        const duration = moment.duration(durationMoment);

        this.setState({timeElapsed: Math.round(duration.asMinutes())});
    };

    componentDidMount() {
        this.updateTime();
        this.intervalRef = setInterval(() => {
            this.updateTime();
        }, 5000);
    }

    componentWillUnmount() {
        clearInterval(this.intervalRef);
    }

    render() {
        return (
            <span style={this.props.style}>{this.state.timeElapsed} {this.props.minuteText}</span>
        );
    };
}

export default Time;