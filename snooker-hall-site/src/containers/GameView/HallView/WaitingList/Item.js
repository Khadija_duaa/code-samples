import React from 'react';
import './WaitingList.css';

import Time from './Time';
import {connect} from 'react-redux';

const isActiveUser = ({activeUser, user}) => activeUser && activeUser._id === user._id;

const getWaitingListClass = (props) => {

    if(isActiveUser(props)){
        return 'WaitingList-active';
    }
    else if(isHallUser(props.user,props.hallUsers)){
        return 'WaitingList-hall';
    }
    else {
        return 'WaitingList';
    }
};

const isHallUser = (user, hallUsers)=>{

    if(hallUsers && hallUsers.length){
        let hallUser = hallUsers.filter(entry => entry.user._id === user._id);
        return hallUser.length > 0;
    }
    return false;
};

const item = (props) => {
    const {text, time, image} = props;

    return (
        <div className={"row"} style={{cursor:"pointer",marginTop:"3%"}}>
            <div className={"col-md-2"} style={{padding:"unset"}}>
                <img className={"image"} src={image} alt="userDP"/>
            </div>

            <div className={`col-md-6 ${getWaitingListClass(props)}`} style={{paddingLeft:"5px"}}>
                <p className={"user"} style={{marginTop: '4px'}}>
                    {text}
                </p>
            </div>

            <div className={`col-md-4 ${getWaitingListClass(props)}`}>
                <p className={"time"} style={{float: "right",marginTop:"4px",width:"max-content"}}>
                    <Time time={time} minuteText={"M"}/>
                </p>
            </div>
        </div>
    )
};

const mapStateToProps = state =>{
    return{
     hallUsers : state.user_reducer.users
    }
};

export default connect(mapStateToProps)(item);
