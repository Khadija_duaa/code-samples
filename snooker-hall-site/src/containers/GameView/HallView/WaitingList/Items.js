import React from 'react';
import './WaitingList.css';
import './digital.css';
import Time from './Time';

class Items extends React.Component {

    isActiveUser = () => this.props.activeUser && this.props.activeUser._id === this.props.user._id;

    getWaitingListClass = () => this.isActiveUser() ? 'WaitingList-active' : 'WaitingList';

    render() {
        const {text, time, image} = this.props;
        return (
            <div className={"waitListItems"} style={{marginTop: "3%"}}>
                <div className={"row"}>
                    <div style={{display: 'flex'}}>
                        <div className={"col-md-2 "}>
                            <img className={"image"} src={image} alt="userDP"/>
                        </div>

                        <div className={`col-md-4 ${this.getWaitingListClass()}`}>
                            <p className={"user"} style={{marginTop: '4px'}}>
                                {text}
                            </p>
                        </div>

                        <div className={`col-md-4 ${this.getWaitingListClass()}`}>
                            <p className={"time"} style={{float: "right", marginTop: '4px'}}>
                                <i><Time time={time}/></i>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default Items;
