import React from 'react';
import Title from '../../../../Components/Heading/Title';
import WaitListItems from "./Item";
import ScrollBar from "./ScrollBar";
import connect from 'react-redux/es/connect/connect';
import {formatName} from "../../../../utils/common-utils";
import EnterLeaveTransition from '../../../../Components/Transition/EnterLeaveTransition';
import axios from "../../../../utils/axios";
import Loader from 'react-loader-spinner';
import {GREEN_COLOR} from "../../../../utils/css-constants";
import {removeFromQueue} from "../../../../utils/business-utils";
import {NotificationManager} from 'react-notifications';
import {handleError} from '../../../../store/store-utils';
import ColouredLine from "../../../../Components/ColouredLine/ColouredLine";
import {SERVER_URL} from "../../../../server";
import CircularButton from "../../../../Components/CircularButton/CircularButton";
import {withRouter} from 'react-router-dom';

const getWaitListItems = (waitingUsers, activeUser, hallAccess) => {

    return (
        <EnterLeaveTransition>
            {
                waitingUsers.map((data, index) => {

                    if(activeUser && activeUser._id === data.user._id){
                        var user = {
                            firstName:activeUser.firstName,
                            lastName:activeUser.lastName
                        }
                    }else{
                        user = data.user;
                    }

                    const _props = {
                        key: index,
                        image: `${SERVER_URL}/photos/${data.user.picture}`,
                        text: `${formatName(user)}`,
                        time: data.joinedTime,
                        user: data.user,
                        activeUser,
                        hallAccess

                    };
                    return <WaitListItems {..._props}/>;
                })
            }
        </EnterLeaveTransition>
    );
};

const waitContainerStyle = {
    background: "rgba(31, 31, 31, 0.9)",
    border: "1px solid  rgba(206, 210, 219,0.7)",
    borderRadius: "7px",
    padding: "16% 9% 35% 9%",
    width: "91%",
    display: "inline-block",
    height: '100%',
    margin: "unset",
    maxHeight: '100%',
    maxWidth: '91%',
    position: 'absolute',
};

class WaitingList extends React.Component {

    state = {
        processing: false
    };

    setProcessing = (processing) => {
        if (processing) {
            this.setState({processing});
        }
        else {

            setTimeout(() => {
                this.setState({processing});
            }, 400);
        }
    };

    onAddInQueue = () => {
        this.setProcessing(true);

        axios.post('/waiting-list')
            .then(() => {
                this.setProcessing(false);
            })
            .catch(error => {
                this.setProcessing(false);
                let message = handleError(error);
                NotificationManager.error(message, '', 3000);
                console.error('addInWaitingList', error);
            });
    };


    getWaitListButton = () => {

        const {activeUser, removeText, addText} = this.props;
        if (!activeUser) return null;

        if (this.state.processing || this.props.isProcessing) {
            return (
                <div style={{textAlign: 'center'}}>
                    <Loader
                        type="Triangle"
                        color={GREEN_COLOR}
                        height="40px"
                        width="40px"/>
                </div>
            );
        }
        else {
            const inWaitingList = this.isInWaitingList();

            const buttonStyle = {
                fontWeight: "bold",
                width: "80%",
                margin: "15% auto 5px auto",
                display: "table",
                color: inWaitingList ? 'red' : '#000',
                padding: "10px",
                borderRadius: "40px",
                border: "unset",
                fontSize: "12px",
                backgroundColor: "#fff"
            };

            return (
                <div>

                    <CircularButton style={{...buttonStyle}}
                                    onClick={inWaitingList ? removeFromQueue : this.onAddInQueue}>
                        {inWaitingList ? removeText : addText}
                    </CircularButton>
                </div>
            );
        }
    };

    isInWaitingList = () => {
        const {waitingList, activeUser} = this.props;

        if (activeUser && waitingList && waitingList.length) {
            let activeUsers = waitingList.filter(entry => entry.user._id === activeUser._id);
            return activeUsers.length > 0;
        }

        return false;
    };

    leftTextStyle = {
        width: "50%",
        display: "unset",
        textAlign: "left",
        fontWeight: "bold",
        color: "burlyWood",
        float: "left",
        fontSize: "14px",

    };

    rightTextStyle = {
        width: "50%",
        display: "unset",
        textAlign: "right",
        fontWeight: "bold",
        color: "aliceBlue",
        float: "right",
        fontSize: "14px",
    };

    renderUserTypes = () => {
        return (
            <div style={{width: "100%", display: "inline-block"}}>
                <li className={"outHall"} style={{...this.leftTextStyle}}>
                    {this.props.outHallText}
                </li>

                <li className={"inHall"} style={{...this.rightTextStyle}}>
                    {this.props.inHallText}
                </li>
            </div>
        )
    };

    render() {
        const {waitingTitle, waitingList, activeUser, hallAccess} = this.props;

        return (
            <div className={"row"} style={waitContainerStyle}>
                <Title afterClass={"whiteDash"} divStyle={{marginBottom: "15%"}}>
                    {waitingTitle}
                </Title>

                {this.renderUserTypes()}

                <ColouredLine style={{color: "#EFEFEF", height: "0.1", opacity: "0.1"}}/>

                <ScrollBar style={{maxHeight: '70%'}} scrollType="waitingScroll">
                    <div className={"col-md-12 "}>
                        {getWaitListItems(waitingList, activeUser, hallAccess)}
                    </div>
                </ScrollBar>

                <div className={"col-md-12"} style={{padding: "unset"}}>
                    {this.getWaitListButton()}
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        waitingTitle: state.gv_reducer.activeLanguage.hallView.waitingTitle,
        waitingList: state.server_reducer.waitingList,
        activeUser: state.user_reducer.activeUser,
        addText: state.gv_reducer.activeLanguage.hallView.addText,
        removeText: state.gv_reducer.activeLanguage.hallView.removeText,

        isProcessing: state.server_reducer.processing,
        inHallText: state.gv_reducer.activeLanguage.hallView.inHallText,
        outHallText: state.gv_reducer.activeLanguage.hallView.outHallText
    }
};

const connected = connect(mapStateToProps)(WaitingList);
export default withRouter(connected);