import React from 'react';
import ScrollArea from 'react-scrollbar';
import './scrollStyle.css';

const _style = {
    maxHeight: "9vh",
    maxWidth: '100%',
    width: '100%',
    height: '100%',
    marginBottom: '10%',
    position:"sticky"
};

const ScrollBar = (props) => {
    let style = {
        ..._style,
        ...props.style
    };

    return (
        <ScrollArea
            speed={0.5}
            style={style}
            className={`area scroll ${props.scrollType || 'tableScroll'}`}
            contentClassName="content"
            horizontal={false}
            onScroll={props.onScroll}
        >
            {props.children && props.children}
        </ScrollArea>

    )
};

export default ScrollBar;