import React from 'react';
import {BODY_FONT} from "../../../../utils/css-constants";
import './WaitingList.css';

const _style = {
    color: 'black',
    letterSpacing: '1px',
    fontFamily: BODY_FONT,
    fontSize: '12px',
};

const button = (props) => {
    const style = {..._style};
    style.color = props.textColor || "black";

    return (
        <a className="btn" onClick={props.onClick}>
            <strong style={style}>{props.children}</strong>
        </a>
    );
};


export default button;


