import React from 'react';
import Title from "../../../Components/Heading/Title";
import Transition from '../../../Components/Transition/Transition';
import ColouredLine from "../../../Components/ColouredLine/ColouredLine";
import {formatAmount} from '../../../utils/common-utils';
import {GREEN_COLOR} from "../../../utils/css-constants";
import CircularButton from "../../../Components/CircularButton/CircularButton";
import BlackBG from "../../../Components/BlackBackground/BlackBG";
import {connect} from 'react-redux';
import {fetchAllSession, resetRedux, setProcessing} from "../../../store/server/server-actions";
import Loader from 'react-loader-spinner';
import {ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import Modal from '../../../Components/Modal/Modal';
import '../../../Components/FixHeaderTable/style.css';
import axios from '../../../utils/axios';
import {getTdWidth} from "../../../utils/common-utils";
import {NotificationManager} from 'react-notifications';
import {handleError} from '../../../store/store-utils';
import ScrolledTable from '../../../Components/ScrolledTable/ScrolledTable';
import AccordianRow from "../../../Components/Accordian/Row";
import {logoutUser} from "../../../store/User/user-actions";

let intervalID = null;

const buttonStyle = {
    fontWeight: "normal",
    width: "100%",
    color: '#fff',
    padding: "10px",
    borderRadius: "5px",
    border: "unset"
};

class CheckoutSummary extends React.Component {

    state = {
        showModal: false,
        processing: false,
        activeKey: null
    };

    getLocationState = () => {
        let state = {};
        let history = this.props.history;
        if (history && history.location && history.location.state) {
            state = history.location.state;
        }
        return state;
    };

    fetchCurrentSession = () => {
        intervalID = setInterval(() => {
            this.props.fetchPlayerSession(false);
        }, 10000);
    };

    fetchSpecificSession = (id) => {
        const specificSession = (processing, id) => this.props.fetchPlayerSession(processing, id);

        specificSession(true, id);

        intervalID = setInterval(() => {
            specificSession(false, id);
        }, 5000);
    };

    componentDidMount() {
        this.props.resetRedux();
        this.props.fetchPlayerSession();
    }

    componentWillMount() {

        this.fetchCurrentSession();

        /*if(this.props.session){
            this.fetchCurrentSession();
        }

        const {id} = this.getLocationState();
        if (!this.props.session && !id) {
            this.props.history.push('/hall/cart');
        }
        else {
            if (id) {
                this.fetchSpecificSession(id);
            } else {
                this.fetchCurrentSession()
            }
        }*/
    }

    componentWillUnmount() {
        clearInterval(intervalID);
    }

    toggleModal = () => {
        this.setState({showModal: !this.state.showModal});
    };

    checkout = (id) => {
        if (id) {
            this.props.setProcessing(true);
        }
        else {
            this.setState({processing: true});
        }

        let url = '/pay-bill';
        if (id) url += `/${id}`;

        
        axios.post(url)
            .then(() => {
                if (id) {
                    this.props.history.push('/hall/history');
                }
                else {
                    this.props.logoutUser(()=>this.props.history.push('/hall'));
                }
            })
            .catch((err) => {
                let message = handleError(err);
                NotificationManager.error(message, '', 3000);
            });
    };

    modal = () => {
        const props = this.props;
        return (
            <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
                <ModalHeader style={{border: "unset"}} toggle={this.toggleModal}>
                    <Title afterClass={"blackDash"}>
                        {props.dialogTitle}
                    </Title>
                </ModalHeader>

                {this.state.processing ?
                    <div style={{textAlign: 'center'}}>
                        <Loader type="Triangle" color={GREEN_COLOR} height="40px" width="40px"/>
                    </div>
                    :
                    <div>
                        <ModalBody>
                            {props.dialogBody}
                        </ModalBody>

                        <ModalFooter style={{border: "unset", textAlign: "center"}}>
                            <CircularButton style={buttonStyle} backColor="#BF1C1C" margin="0px 10px 0px 0px"
                                            onClick={() => this.setState({showModal: !this.state.showModal})}>
                                {props.dialogHalviewBtnText}
                            </CircularButton>

                            <CircularButton style={buttonStyle} backColor="#13C95F" margin="0px 0px 0px 10px"
                                            onClick={() => this.checkout()}>
                                {props.dialogCheckoutBtnText}
                            </CircularButton>
                        </ModalFooter>
                    </div>
                }
            </Modal>
        );
    };

    buttonStyle = {
        color: '#fff',
        padding: "1vh 2vw",
        borderRadius: "50px",
        border: "1px solid #fff",
        fontWeight: "bold"
    };

    getHeaderButton = () => {
        let _buttonStyle = {...this.buttonStyle};
        _buttonStyle.backgroundColor = "transparent";

        let history = this.props.history;

        if (history && history.location && history.location.state) {
            return (
                <CircularButton style={_buttonStyle} onClick={() => {
                    this.props.history.push('/hall/history')
                }}>
                    {this.props.historyBtnText}
                </CircularButton>
            )
        }

        return (
            <CircularButton style={_buttonStyle} onClick={() => {
                this.props.history.push('/hall/cart')
            }}>
                {this.props.foodBtnText}
            </CircularButton>
        )
    };

    getPageHeader = (session) => {

        return (
            <div className={"row"}>
                <div className={"col-md-6"} style={{paddingTop: "10px", paddingLeft: "5%"}}>
                    <Title afterClass={"whiteDash"}>
                        {this.props.cartHeading}
                    </Title>
                </div>

                 {/*<div className={"col-md-6"} style={{textAlign: "right", paddingRight: "5%"}}>
                    {session && this.getHeaderButton()}
                </div>*/}
            </div>
        )
    };

    getPageBody = () => {

        let tableHeaderJSX = this.props.tableHeads.map((head, index) => {
            return (
                <th style={{width: getTdWidth(this.props.tableHeads)}} key={index}>{head}</th>
            )
        });

        return [
            <div className={"col-md-12"} key={1}>
                <ScrolledTable width={"100%"} style={{maxHeight: "400px", marginBottom: "unset"}}>
                    <table className="table table-dark table-hover fixHead">

                        <thead>
                        <tr>
                            {tableHeaderJSX}
                        </tr>
                        </thead>

                        {this.getTableData()}
                    </table>
                </ScrolledTable>
            </div>,
            <div key={2} className={"col-md-12"}>
                {this.props.processing ?
                    <div style={{textAlign: 'center'}}>
                        <Loader
                            type="Triangle"
                            color={GREEN_COLOR}
                            height="40px"
                            width="40px"/>
                    </div> : null
                }
            </div>
        ]
    };

    getSubmitButton = () => {
        let _buttonStyle = {...this.buttonStyle};
        _buttonStyle.color = "#000";
        _buttonStyle.backgroundColor = "#fff";

        const id = this.getLocationState().id;
        if (id) {
            return (
                <CircularButton style={_buttonStyle} onClick={() => this.checkout(id)}>
                    {this.props.payBtnText}
                </CircularButton>
            );
        }
        else {
            return (
                <CircularButton style={_buttonStyle} onClick={this.toggleModal}>
                    {this.props.checkoutBtnText}
                </CircularButton>
            );
        }
    };

    getPageFooter = () => {

        let textStyle = {
            marginRight: "5vw",
            color: "#fff",
            fontWeight: "bold",
            fontFamily: "Helvetica"
        };


        return (
            <div className={"col-md-12"} style={{marginTop: "5%", paddingRight: "5%"}}>
                <div style={{textAlign: "left", float: "right"}}>
                    {this.getSubmitButton()}
                </div>

                <div style={{textAlign: "left", float: "right"}}>
                    <p style={textStyle}>

                        {this.props.subtotalText}

                        <br/>
                        <span
                            style={{fontSize: "25px", fontWeight: "normal", fontFamily: "inherit", color: 'white'}}>
                            {formatAmount(this.props.session.totalBill)}
                        </span>
                    </p>

                </div>

                {this.getLocationState().prevPaidBill >= 0 &&
                <div style={{textAlign: "left", float: "left"}}>

                    <p style={textStyle}>
                        {this.props.prevBillText}
                        <br/>
                        <span
                            style={{fontSize: "25px", fontWeight: "normal", fontFamily: "inherit", color: GREEN_COLOR}}>
                            {formatAmount(this.getLocationState().prevPaidBill)}
                        </span>
                    </p>

                    <p style={textStyle}>
                        {this.props.prevFreeMinutesText}
                        <br/>
                        <span
                            style={{fontSize: "25px", fontWeight: "normal", fontFamily: "inherit", color: GREEN_COLOR}}>
                            {this.getLocationState().prevFreeMinutes} Mins
                        </span>
                    </p>

                    <p style={textStyle}>
                        {this.props.prevWalletText}
                        <br/>
                        <span
                            style={{fontSize: "25px", fontWeight: "normal", fontFamily: "inherit", color: GREEN_COLOR}}>
                            {formatAmount(this.getLocationState().prevWalletBill)}
                        </span>
                    </p>

                </div>
                }
            </div>

        )
    };


    sessionStatus = (row, index) => {
        /*const buttonStyle = {
            color: '#000',
            padding: "0.05vw 0.8vw",
            borderRadius: "50px",
            border: "1px solid #fff",
            fontWeight: "bold",
            backgroundColor: "#fff"
        };*/

        return (
            <span style={{cursor: "pointer"}} onClick={(e) => {
                if (this.state.activeKey === index) {
                    this.setState({activeKey: -1});

                } else {
                    this.setState({activeKey: index});
                }

            }}>
                    {this.props.detailText}
                <i style={{marginLeft: "5%", color: "#fff"}}
                   className={index === this.state.activeKey ? 'fa fa-sort-up' : 'fa fa-sort-down'}/>
                </span>
        );

        /* return (
             <CircularButton style={buttonStyle}
                             onClick={() => {
                                 this.props.history.push({
                                     pathname: '/hall/cart/checkout',
                                     state: {
                                         id: row._id,
                                         prevPaidBill: ((row.totalBill + row.taxAmount) - row.netTotalBill),
                                         prevFreeMinutes: row.freeMinutesAmount,
                                         prevWalletBill: row.totalBillFromWallet
                                     }
                                 })
                             }}>
                 {this.props.payBtnText}
             </CircularButton>
         );*/
    };

    getTableData = () => {
        const {session} = this.props;

        const tableData = [
            /*{name: 'Game Played', value: `${session.billableMinutes} Mins`},

            {name: 'Game Bill', value: formatAmount(session.billableAmount)},
            {name: 'VAT (Game)', value: formatAmount(session.sessionTaxAmount)},

            {name: 'Cart Bill', value: formatAmount(session.cartBill)},
            {name: 'VAT (Cart)', value: formatAmount(session.cartTaxAmount)},

            {name: 'Total Bill (Cart + Game)',
                value: formatAmount(session.billableAmount + session.cartBill + session.sessionTaxAmount + session.cartTaxAmount)}

            {name: 'Total Payable Bill', value: formatAmount(session.netTotalBill)},

            {name: 'Total Bill from Wallet (VAT inclusive)', value: formatAmount(session.totalBillFromWallet)},*/

            {name: 'Total Bill (Tax Inclusive)', value: formatAmount(session.totalBill)},


        ];

        /*if (session.freeMinutes) {
            tableData.unshift({name: 'Free Minutes', value: `${session.freeMinutes} Mins`});
        }*/

        return (
            <tbody style={{marginBottom: "unset"}}>
            {
                tableData.map((row, index) => {
                    return (
                        <AccordianRow {...this.props.session} key={index}
                                      isAccordian={index === this.state.activeKey}>
                            <td style={{width: getTdWidth(this.props.tableHeads)}}>{row.name}</td>
                            <td style={{width: getTdWidth(this.props.tableHeads)}}>{row.value}</td>
                            <td style={{width: getTdWidth(this.props.tableHeads)}}>
                                {this.sessionStatus(row, index)}
                            </td>
                        </AccordianRow>
                    );
                })
            }
            </tbody>
        )
    };


    render() {
        const session = this.props.session || {};
        const {cart, sessions} = session;
        let isCart = cart && cart.length;
        let isSession = sessions && sessions.length;

        return (
            <Transition>
                <BlackBG>
                    <div className={"col-md-12"} style={{padding: "3% 3% 0px 4%"}}>
                        {this.getPageHeader(session._id)}
                    </div>

                    <ColouredLine style={{marginBottom: "unset", color: "#EFEFEF", height: "0.1", opacity: "0.1"}}/>

                    {this.modal()}

                    {
                        this.props.processing ?
                            <div style={{textAlign: 'center'}}>
                                <Loader
                                    type="Triangle"
                                    color={GREEN_COLOR}
                                    height="45px"
                                    width="45px"/>
                            </div> :
                            (isCart || isSession) ?
                            <div className={"col-md-12"} style={{padding: "unset"}}>
                                <div className={"col-md-12"} style={{backgroundColor: "#212529", padding: "2% 5%"}}>
                                    {this.getPageBody()}
                                </div>

                                <div className={"col-md-12"}>
                                    {this.getPageFooter()}
                                </div>

                            </div>: null
                    }
                </BlackBG>
            </Transition>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        resetRedux: () => dispatch(resetRedux()),
        fetchPlayerSession: (showLoader = true, _id = null, cb) => dispatch(fetchAllSession(showLoader, _id, cb)),
        setProcessing: (processing) => dispatch(setProcessing(processing)),
        logoutUser:(cb)=>dispatch(logoutUser(cb))
    }
};

const mapStateToProps = (state) => {
    const {checkoutSummary} = state.gv_reducer.activeLanguage;
    return {
        activeUser: state.user_reducer.activeUser,

        session: state.server_reducer.playerSession,
        processing: state.server_reducer.processing,

        cartHeading: checkoutSummary.title,
        foodBtnText: checkoutSummary.foodItembtnText,
        historyBtnText: checkoutSummary.historyBtnText,
        tableHeads: checkoutSummary.tableHeaders,
        checkoutBtnText: checkoutSummary.checkoutButtonText,
        payBtnText: checkoutSummary.payBtnText,
        subtotalText: checkoutSummary.subTotalText,

        prevBillText: checkoutSummary.prevBillText,
        prevFreeMinutesText: checkoutSummary.prevFreeMinutesText,
        prevWalletText: checkoutSummary.prevWalletBillText,

        dialogTitle: checkoutSummary.dialogTitle,
        dialogBody: checkoutSummary.dialogBody,
        dialogCheckoutBtnText: checkoutSummary.dialogCheckoutBtnText,
        dialogHalviewBtnText: checkoutSummary.dialogHalviewBtnText,

        detailText: checkoutSummary.detailText
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(CheckoutSummary);
