import React, {Component} from 'react';
import {Redirect, Route, Switch, withRouter} from 'react-router-dom';
import HallView from "./HallView/HallView";
import Dashboard from "./Dashboard/Dashboard";
import Navbar from "./Dashboard/Navbar/Navbar";
import Transition from '../../Components/Transition/Transition';
import OnlineMembers from './OnlineMembers/OnlineMembers';
import Items from './Items/Items';
import UserProfile from './UserProfile/UserProfile';
import AdminVerification from './Login_Register/AdminVerification/AdminVerification';
import BillSummary from "./Bill Summary/BillSummary";
import CheckoutSummary from "./Checkout Summary/CheckoutSummary";
import History from '../GameView/MemberHistory/History';
import './GameView.css';
import {connect} from 'react-redux';
import Wallet from './Wallet/Wallet';
import ReactCountdownClock from 'react-countdown-clock';
import {logoutUser} from "../../store/User/user-actions";
import IdleTimer from 'react-idle-timer';

const timeInterval = 1000 * 300000;


class GameView extends Component {

    constructor(props){
        super(props);
        this.state = {activeTimer:false}
    }

    getHallRoutes = () => {

        const {hasActiveUser, hallAccess} = this.props;

        if (hasActiveUser) {
            let routes = [];

            routes.push(<Route key={routes.length} path='/hall/dashboard' component={Dashboard}/>);
            routes.push(<Route key={routes.length} path='/hall/profile' component={UserProfile}/>);
            routes.push(<Route key={routes.length} path='/hall/history' component={History}/>);
            routes.push(<Route key={routes.length} path='/hall/wallet' component={Wallet}/>);
            if (hallAccess) {
                routes.push(<Route key={routes.length} path='/hall/cart/items' component={Items}/>);
            }
            routes.push(<Route key={routes.length} path='/hall/cart/checkout' component={CheckoutSummary}/>);
            routes.push(<Route key={routes.length} path='/hall/cart' component={BillSummary}/>);
            return routes;
        }

        return null;
    };

    onActive = (props) => {
        if (props.hasActiveUser && props.hallAccess) {
            this.setState({activeTimer:false})
        }
    };

    onIdle = (props) => {
        if (props.hasActiveUser && props.hallAccess) {
            this.setState({activeTimer:true})
        }
    };

    deActiveUser = ()=>{
        this.props.logoutUser(this.props.history.push('/hall'), true);
        this.setState({activeTimer:false});
    };

    getTimer = () => {
        const {hasActiveUser,hallAccess} = this.props;

        if (hasActiveUser && hallAccess && this.state.activeTimer) {
            return (
                <ReactCountdownClock seconds={16}
                                     color="#fff"
                                     alpha={0.9}
                                     size={60}
                                     onComplete ={this.deActiveUser}
                />

            )
        }
    };


    render() {
        return (
            <div>
                <IdleTimer
                    element={document}
                    onActive={() => this.onActive(this.props)}
                    onIdle={() => this.onIdle(this.props)}
                    debounce={250}
                    timeout={timeInterval}/>

                <Transition>
                    <div className={"gv-Main"}>

                        {this.getTimer()}

                        <div className={"container"}>
                            <Navbar/>
                            <Switch>
                                <Route path='/hall/admin' component={AdminVerification}/>
                                {this.getHallRoutes()}
                                <Route path='/hall/onlineMembers' component={OnlineMembers}/>
                                <Route path='/hall' component={HallView}/>
                                <Redirect to={'/hall'}/>
                            </Switch>
                        </div>
                    </div>
                </Transition>
            </div>

        );
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        logoutUser: (logoutCallback, leaveDevice) => dispatch(logoutUser(logoutCallback, leaveDevice)),
    };
};

const mapStateToProps = (state) => {
    return {
        hasActiveUser: Boolean(state.user_reducer.activeUser),
        hallAccess: Boolean(state.server_reducer.hallAccessToken)
    }
};

const connected = connect(mapStateToProps,mapDispatchToProps)(GameView);
export default withRouter(connected);