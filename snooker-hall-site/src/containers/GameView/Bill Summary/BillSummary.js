import React from 'react';
import Title from "../../../Components/Heading/Title";
import Transition from '../../../Components/Transition/Transition';
import ColouredLine from "../../../Components/ColouredLine/ColouredLine";
import {formatAmount, getTdWidth} from '../../../utils/common-utils';
import {GREEN_COLOR} from "../../../utils/css-constants";
import CircularButton from "../../../Components/CircularButton/CircularButton";
import BlackBG from "../../../Components/BlackBackground/BlackBG";
import {connect} from 'react-redux';
import {fetchAllSession, resetRedux} from "../../../store/server/server-actions";
import {capitalize} from "../../../utils/common-utils";
import Loader from 'react-loader-spinner';
import '../../../Components/FixHeaderTable/style.css';
import ScrolledTable from '../../../Components/ScrolledTable/ScrolledTable';

let intervalID = null;

class BillSummary extends React.Component {

    componentDidMount() {
        this.props.resetRedux();
        this.props.fetchPlayerSession(true, () => {
            intervalID = setInterval(() => {
                this.props.fetchPlayerSession(false);
            }, 10000);
        });
    }

    componentWillUnmount() {
        clearInterval(intervalID);
    }

    buttonStyle = {
        color: '#fff',
        padding: "1vh 2vw",
        borderRadius: "50px",
        border: "1px solid #fff",
        fontWeight: "bold",

    };

    getAddItemButton = (session) => {
        let _buttonStyle = {...this.buttonStyle};
        _buttonStyle.backgroundColor = "transparent";

        if (!this.props.hallAccess) {
            return null;
        }
        return (
            <div className={"col-md-6"} style={{textAlign: "right", paddingRight: "5%"}}>
                {session &&
                <CircularButton style={_buttonStyle} onClick={() => {
                    this.props.history.push('/hall/cart/items')
                }}>
                    <i className={"fa fa-plus"} style={{marginRight: "10px"}}/>
                    {this.props.foodBtnText}
                </CircularButton>
                }
            </div>
        )

    };

    getPageHeader = (session) => {
        return (
            <div className={"row"}>
                <div className={"col-md-6"} style={{paddingTop: "10px", paddingLeft: "5%"}}>
                    <Title afterClass={"whiteDash"}>
                        {this.props.cartHeading}
                    </Title>
                </div>

                {this.getAddItemButton(session)}

            </div>
        )
    };

    getPageBody = () => {
        let tableHeaderJSX = this.props.tableHeads.map((head, index) => {
            return (
                <th style={{width: getTdWidth(this.props.tableHeads)}} key={index}>{head}</th>
            )
        });

        return (
            <div className={"col-md-12"}>
                <ScrolledTable width={"100%"} style={{maxHeight: "250px", marginBottom: "unset"}}>
                    <table className="table table-dark table-hover fixHead">
                        <thead>
                        <tr>
                            {tableHeaderJSX}
                        </tr>
                        </thead>
                        {this.getTableData()}
                    </table>
                </ScrolledTable>
            </div>
        )
    };


    getPageFooter = () => {

        const {cartBill, sessionBill} = this.props.session;

        let _buttonStyle = {...this.buttonStyle};
        _buttonStyle.color = "#000";
        _buttonStyle.backgroundColor = "#fff";

        let footerDivStyle = {
            margin: "0px 1.5%",
            textAlign: "left",
            display: "inline-block"
        };

        let amountStyle = {
            fontSize: "20px",
            fontWeight: "normal",
            fontFamily: "inherit",
            "WebkitTextStroke": "0.5px"
        };

        let totalTextStyle = {
            color: "#fff",
            fontWeight: "bold",
            fontFamily: "Helvetica"
        };

        let footerJSON = [

            {
                divStyle: {...footerDivStyle},
                isPara: true,
                upperText: this.props.foodTotalText,
                amountStyle: {...amountStyle, color: "#fff"},
                lowerText: formatAmount(cartBill)
            },
            {
                divStyle: {...footerDivStyle, top: "-10px", position: "inherit"},
                isPara: false,
                component: <img alt="add" src={"/FloorView_images/add.svg"} width={"15px"}/>
            },
            {
                divStyle: {...footerDivStyle},
                isPara: true,
                upperText: this.props.gameTotalText,
                amountStyle: {...amountStyle, color: "#fff"},
                lowerText: formatAmount(sessionBill)
            },
            {
                divStyle: {...footerDivStyle, top: "-10px", position: "inherit"},
                isPara: false,
                component: <img alt="equal" src={"/FloorView_images/equal.svg"} width={"15px"}/>
            },
            {
                divStyle: {...footerDivStyle},
                isPara: true,
                upperText: this.props.subtotalText,
                amountStyle: {...amountStyle, color: GREEN_COLOR},
                lowerText: formatAmount(this.props.session.totalBill)
            },

            {
                divStyle: {...footerDivStyle, top: "-10px", position: "inherit"},
                isPara: false,
                component: <CircularButton style={_buttonStyle} onClick={() => {
                    this.props.history.push('/hall/cart/checkout')
                }}>
                    {this.props.payButtonText}
                </CircularButton>
            }
        ];

        let footerJSX = footerJSON.map((data, index) => {

            let getPara = () => {
                return (
                    <p style={totalTextStyle}>
                        {data.upperText}
                        <br/>
                        <span style={data.amountStyle}>
                            {data.lowerText}
                        </span>
                    </p>
                )
            };

            let getComponent = () => data.component;

            return (
                <div key={index} style={data.divStyle}>
                    {
                        data.isPara ? getPara() : getComponent()
                    }
                </div>
            )
        });

        return (
            <div className={"col-md-12"} style={{marginTop: "2%", textAlign: "center"}}>
                {footerJSX}
            </div>

        )
    };


    getTableData = () => {
        const {cart, sessions} = this.props.session;

        let cartJsx = cart.map((item, index) => {
            return (
                <tr key={index}>
                    <td style={{width: getTdWidth(this.props.tableHeads)}}>
                        {capitalize(item.name)}
                    </td>

                    <td style={{width: getTdWidth(this.props.tableHeads)}}>
                        {capitalize(item.category)}
                    </td>

                    <td style={{width: getTdWidth(this.props.tableHeads)}}>
                        {item.quantity}
                    </td>
                    <td style={{width: getTdWidth(this.props.tableHeads)}}>
                        {formatAmount((item.quantity * item.price).toFixed(2))}
                    </td>
                </tr>
            )
        });

        let gameJsx = sessions.map((session, index) => {
            return (
                <tr key={index}>
                    <td style={{width: getTdWidth(this.props.tableHeads)}}>
                        {session.tableName}
                    </td>

                    <td style={{width: getTdWidth(this.props.tableHeads)}}>
                        Game
                    </td>

                    <td style={{width: getTdWidth(this.props.tableHeads)}}>
                        {session.minutesPlayed} Min
                    </td>
                    <td style={{width: getTdWidth(this.props.tableHeads)}}>
                        {formatAmount(session.amount)}
                    </td>
                </tr>
            )
        });

        return (
            <tbody style={{marginBottom:"unset"}}>
            {cartJsx}
            {gameJsx}
            </tbody>
        )
    };

    render() {
        const session = this.props.session || {};

        const {cart, sessions} = session;
        let isCart = cart && cart.length;
        let isSession = sessions && sessions.length;

        return (
            <Transition>
                <BlackBG>
                    <div className={"col-md-12"} style={{padding: "3% 3% 0px 4%"}}>
                        {this.getPageHeader(session._id)}
                    </div>

                    <ColouredLine style={{marginBottom: "unset", color: "#EFEFEF", height: "0.1", opacity: "0.1"}}/>

                    {
                        this.props.processing ?
                            <div style={{textAlign: 'center'}}>
                                <Loader
                                    type="Triangle"
                                    color={GREEN_COLOR}
                                    height="45px"
                                    width="45px"/>
                            </div>
                            : isCart || isSession ?
                            <div className={"col-md-12"} style={{padding: "unset"}}>
                                <div className={"col-md-12"} style={{backgroundColor: "#212529", padding: "2% 5%"}}>
                                    {this.getPageBody()}
                                </div>

                                <div className={"col-md-12"} style={{textAlign: "center"}}>
                                    {this.getPageFooter()}
                                </div>

                            </div> : null
                    }
                </BlackBG>
            </Transition>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        resetRedux: () => dispatch(resetRedux()),
        fetchPlayerSession: (showLoader = true, cb) => dispatch(fetchAllSession(showLoader, null, cb))
    }
};

const mapStateToProps = (state) => {
    return {
        session: state.server_reducer.playerSession,
        processing: state.server_reducer.processing,
        cartHeading: state.gv_reducer.activeLanguage.billSummary.title,
        foodBtnText: state.gv_reducer.activeLanguage.billSummary.foodItembtnText,
        tableHeads: state.gv_reducer.activeLanguage.billSummary.tableHeaders,
        payButtonText: state.gv_reducer.activeLanguage.billSummary.payButtonText,
        gameTotalText: state.gv_reducer.activeLanguage.billSummary.gameTotalText,
        foodTotalText: state.gv_reducer.activeLanguage.billSummary.foodTotalText,
        subtotalText: state.gv_reducer.activeLanguage.billSummary.subTotalText,
        hallAccess: Boolean(state.server_reducer.hallAccessToken)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BillSummary);
