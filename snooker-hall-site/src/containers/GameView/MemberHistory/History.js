import React from 'react';
import Transition from '../../../Components/Transition/Transition';
import BlackBG from "../../../Components/BlackBackground/BlackBG";
import AccordianRow from "../../../Components/Accordian/Row";
import {connect} from 'react-redux';
import {fetchSessionHistory, gameCount} from "../../../store/server/server-actions";
import Loader from 'react-loader-spinner';
import {GREEN_COLOR} from "../../../utils/css-constants";
import {formatAmount, formatDate, getTdWidth} from "../../../utils/common-utils";
import {resetRedux} from "../../../store/server/server-actions";
import TitleBar from '../../../Components/TitleBar/TitleBar';
import '../../../Components/FixHeaderTable/style.css';
import ScrolledTable from '../../../Components/ScrolledTable/ScrolledTable';

let timeOutRef = null;

class History extends React.Component {

    componentDidMount() {
        this.props.getGameCount();
        this.props.resetRedux();
        this.fetchSessionHistory();
    }

    componentWillUnmount() {
        timeOutRef = null;
    }

    fetchSessionHistory = () => {
        if (null === timeOutRef) {
            timeOutRef = setTimeout(() => {
                this.props.getSessionHistory(() => {
                    setTimeout(() => {
                        timeOutRef = null;
                    }, 100);
                });
            }, 200);
        }
    };

    handleScroll = (e) => {
        const {scrollHeight, scrollTop, clientHeight} = e.target;
        if ((clientHeight / (scrollHeight - scrollTop)) > 0.8) {
            this.fetchSessionHistory();
        }
    };

    state = {
        activeKey: null
    };

    getTableData = () => {
        const {sessionHistory} = this.props;
        console.log("SessionHistory:",sessionHistory);
        let rowsJSX = sessionHistory.map((row, index) => {
            if (!row.checkout) {
                return null;
            }
            return (
                row.totalBill !==0 ?
                <AccordianRow {...this.props.sessionHistory[index]} key={index}
                              isAccordian={index === this.state.activeKey}>
                    <td style={{width: getTdWidth(this.props.tableHeads)}}>{formatDate(row.startDate)}</td>
                    <td style={{width: getTdWidth(this.props.tableHeads)}}>{formatDate(row.endDate)}</td>
                    <td style={{width: getTdWidth(this.props.tableHeads)}}>{formatAmount(row.totalBill)}</td>
                    <td style={{width: getTdWidth(this.props.tableHeads)}}>
                        {
                            row.minutesPlayed > 1 ?  <span>{row.minutesPlayed} mins</span> : <span>{row.minutesPlayed} min</span>
                        }
                    </td>
                    <td style={{width: getTdWidth(this.props.tableHeads)}}>
                        {this.sessionStatus(row, index)}
                    </td>
                </AccordianRow> :  null
            )
        });

        return (

            <tbody onScroll={this.handleScroll}>
            {rowsJSX}
            </tbody>
        )

    };

    sessionStatus = (row, index) => {


        if (row.checkout) {
            return (
                <span style={{cursor: "pointer"}} onClick={(e) => {
                    if((row.totalBill !== 0)) {
                        if (this.state.activeKey === index) {
                            this.setState({activeKey: -1});

                        } else {
                            this.setState({activeKey: index});
                        }
                    }
                    else{
                        return null
                    }

                }}>
                    {this.props.paidText}
                    {(row.totalBill !== 0) ?
                        <i style={{marginLeft: "5%", color: "#fff"}}
                           className={index === this.state.activeKey ? 'fa fa-sort-up' : 'fa fa-sort-down'}/> : null
                    }
                </span>
            );
        }

    };

    getPageData = () => {
        let tableHeaderJSX = this.props.tableHeads.map((head, index) => <th
            style={{width: getTdWidth(this.props.tableHeads)}} key={index}>{head}</th>);

        return [
            <div key={1} className={"col-md-12"}>
                <ScrolledTable width={"100%"} style={{maxHeight: "55vh", marginBottom: "unset"}}>
                    <table className="table table-dark fixHead" style={{marginBottom: "unset"}}>
                        <thead>
                            <tr>
                                {tableHeaderJSX}
                            </tr>
                        </thead>

                        {this.getTableData()}

                    </table>
                </ScrolledTable>
            </div>,
            <div key={2} className={"col-md-12"}>
                {this.props.processing ?
                    <div style={{textAlign: 'center'}}>
                        <Loader
                            type="Triangle"
                            color={GREEN_COLOR}
                            height="40px"
                            width="40px"/>
                    </div> : null
                }
            </div>
        ]
    };

    getTotalSessions = () => {
        /*let {sessionHistory} = this.props;
        console.log("Session History:", this.props);
        let count = 0;

        sessionHistory.map( data => {
            if (data.endDate !== null)
                return (data.totalBill !== 0) ? count++ : null;
        });

        if (totalSessions > 0) {
            return (totalSessions-1) - count;
        }
        return count;*/
    };

    render() {
        console.log("Game Count:", this.props.gameCount);
        return (
            <Transition>
                <TitleBar leftTitle={this.props.historyTitle} leftIcon='/FloorView_images/Categories/history.svg'
                          balanceTitle={this.props.wallet} freeMinTitle={this.props.freeMin} freeMin={(this.props.activeUser.profile.freeMinutes)}
                          balance={formatAmount(this.props.activeUser.profile.wallet)}
                          rightTitle={`${this.props.gameCount.gameCount} Total Games`}
                />

                <BlackBG style={{marginTop: "2%"}}>
                    <div className={"row"}>
                        {this.getPageData()}
                    </div>
                </BlackBG>
            </Transition>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getSessionHistory: (cb) => dispatch(fetchSessionHistory(cb)),
        getGameCount: () => dispatch(gameCount()),
        resetRedux: () => dispatch(resetRedux())
    }
};

const mapStateToProps = (state) => {
    return {
        activeUser: state.user_reducer.activeUser,
        historyTitle: state.gv_reducer.activeLanguage.sessionHistory.heading,
        tableHeads: state.gv_reducer.activeLanguage.sessionHistory.tableHeads,
        payBtnText: state.gv_reducer.activeLanguage.sessionHistory.payBtnText,
        paidText: state.gv_reducer.activeLanguage.sessionHistory.paidText,
        sessionHistory: state.server_reducer.sessionHistory,
        processing: state.server_reducer.processing,
        wallet: state.gv_reducer.activeLanguage.navbar.wallet,
        freeMin: state.gv_reducer.activeLanguage.navbar.freeMin,
        totalSessions: state.server_reducer.totalSessions,
        gameCount: state.server_reducer.gameCount
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(History)
