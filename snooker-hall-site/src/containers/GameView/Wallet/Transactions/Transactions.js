import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Loader from 'react-loader-spinner';
import {GREEN_COLOR} from '../../../../utils/css-constants';
import {getTdWidth, formatDate, formatAmount, formatName} from '../../../../utils/common-utils';
import '../../../../Components/FixHeaderTable/style.css';
import {fetchTransactionHistory, resetRedux} from "../../../../store/server/server-actions";
import moment from 'moment';
import ScrolledTable from "../../../../Components/ScrolledTable/ScrolledTable";

let timeOutRef = null;

class Transactions extends React.Component {

    componentDidMount() {
        this.props.resetRedux();
        this.getTransactionHistory();
    }

    componentWillUnmount() {
        timeOutRef = null;
    }

    getTransactionHistory = () => {
        if (null === timeOutRef) {
            timeOutRef = setTimeout(() => {
                this.props.fetchTransactionHistory(() => {
                    setTimeout(() => {
                        timeOutRef = null;
                    }, 100);
                });
            }, 200);
        }
    };

    handleScroll = (e) => {
        const {scrollHeight, scrollTop, clientHeight} = e.target;
        if ((clientHeight / (scrollHeight - scrollTop)) > 0.8) {
            this.getTransactionHistory();
        }
    };

    getTransactionBy = (by) => {
        switch (by) {
            case "User" : {
                return formatName(this.props.activeUser);
            }
            case "Admin" : {
                return 'Admin';
            }
            default : {
                // do nothing
            }
        }
    };

    getPurpose = (purpose) => {
        switch (purpose) {
            case "Membership" : {
                return `${this.props.membership}`;
            }
            case "Payment" : {
                return `${this.props.payment}`;
            }
            case "TopUp" : {
                return `${this.props.topUp}`;
            }
            default : {
                // do nothing
            }
        }
    };

    getType = (type) => {
        switch (type) {
            case "Wallet" : {
                return `${this.props.wallet}`;
            }
            case "FreeMinutes" : {
                return `${this.props.freeMinutes}`;
            }
            default : {
                // do nothing
            }
        }
    };

    getData = () => {
        const {transactionHistory} = this.props;


        let rowsJSX = transactionHistory.map((row, index) => {

            return (
                <tr key={index}>
                    <td style={{width: getTdWidth(this.props.headers)}}>{moment(formatDate(row.createdOn)).format("MMM Do YY")}</td>
                    <td style={{width: getTdWidth(this.props.headers)}}>{this.getTransactionBy(row.by)}</td>
                    <td style={{width: getTdWidth(this.props.headers)}}>{this.getPurpose(row.purpose)}</td>
                    <td style={{width: getTdWidth(this.props.headers)}}>{this.getType(row.type)}</td>
                    <td style={{width: getTdWidth(this.props.headers)}}>{formatAmount(row.amount)}</td>
                </tr>
            )
        });

        return (
            <tbody onScroll={this.handleScroll}>
            {rowsJSX}
            </tbody>
        )
    };

    getTransactionData = () => {
        let headers = this.props.headers.map((headers, index) => <th
            style={{width: getTdWidth(this.props.headers)}} key={index}>{headers}</th>);

        return [
            <div key={1} className={"col-md-12"}>
                <ScrolledTable width={"100%"}
                               style={{maxHeight: "55vh", marginBottom: "unset", backgroundColor: 'transparent'}}>
                    <table className="table table-dark fixHead"
                           style={{marginBottom: "unset"}}>
                        <thead>
                        <tr>
                            {headers}
                        </tr>
                        </thead>
                        {this.getData()}
                    </table>
                </ScrolledTable>
            </div>,

            <div key={2} className={"col-md-12"}>
                {this.props.processing ?
                    <div style={{textAlign: 'center'}}>
                        <Loader
                            type="Triangle"
                            color={GREEN_COLOR}
                            height="40px"
                            width="40px"/>
                    </div> : null
                }
            </div>
        ]
    };

    render() {
        return (
            <div className={"row"}>
                {this.getTransactionData()}
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchTransactionHistory: (cb) => dispatch(fetchTransactionHistory(cb)),
        resetRedux: () => dispatch(resetRedux())
    }
};

const mapStateToProps = (state) => {
    return {
        activeUser: state.user_reducer.activeUser,
        headers: state.gv_reducer.activeLanguage.wallet.headers,
        membership: state.gv_reducer.activeLanguage.wallet.membership,
        payment: state.gv_reducer.activeLanguage.wallet.payment,
        topUp: state.gv_reducer.activeLanguage.wallet.topUp,
        wallet: state.gv_reducer.activeLanguage.wallet.wallet,
        freeMinutes: state.gv_reducer.activeLanguage.wallet.freeMinutes,
        transactionHistory: state.server_reducer.transactionHistory
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Transactions);
export default withRouter(connected);