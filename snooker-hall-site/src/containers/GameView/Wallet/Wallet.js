import React from 'react';
import Transition from '../../../Components/Transition/Transition';
import TitleBar from '../../../Components/TitleBar/TitleBar';
import NavMenu from './Navbar/NavMenu';
import {withRouter} from "react-router-dom";
import Transactions from "./Transactions/Transactions";
import {connect} from "react-redux";
import {formatAmount} from "../../../utils/common-utils";
import {updateProfile} from "../../../store/User/user-actions";

const NavBar = () => {
    return (
        <div className={"col-md-12"}
             style={{borderBottom: "1px solid  #777777", marginBottom: '3%', width: '100%', display: 'inline-block'}}>
            <NavMenu/>
        </div>
    )
};

const style = {
    background: "rgba(31, 31, 31, 0.9)",
    border: "1px solid  rgba(206, 210, 219,0.7)",
    borderRadius: "6px",
    padding: "2% 0 0 0",
    width: "100%",
    height: '100%',
    maxWidth: "100",
    maxHeight: '100%',
    margin: "unset",
    marginTop: "1%",
};

class Wallet extends React.Component{

    componentDidMount() {
        this.props.updateProfile();
    }

    render () {
        const {freeMinuteText,walletTitle,userProfile} = this.props;

        return (
            <Transition>
                <TitleBar
                    leftTitle={walletTitle} leftIcon='/FloorView_images/Categories/wallet.svg'
                    balanceTitle={freeMinuteText}
                    balance={`${userProfile.freeMinutes} Mins`}
                    rightTitle={`${formatAmount(userProfile.wallet)}`}/>
                <div className={"row"} style={style}>
                    <NavBar/>
                    <div className={"col-md-12"}>
                        <Transactions/>
                    </div>
                </div>
            </Transition>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userProfile:state.user_reducer.activeUser.profile,
        walletTitle: state.gv_reducer.activeLanguage.wallet.title,
        freeMinuteText:state.gv_reducer.activeLanguage.wallet.freeMinuteText,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateProfile: () => dispatch(updateProfile())
    };
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Wallet);

export default withRouter(connected);