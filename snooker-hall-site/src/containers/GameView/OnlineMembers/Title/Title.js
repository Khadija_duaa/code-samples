import React from 'react';
import HeadingTitle from '../../../../Components/Heading/Title';
import connect from "react-redux/es/connect/connect";

class Title extends React.Component{
    render() {

        return(
            <HeadingTitle afterClass = {"whiteDash"}>
                {this.props.onlineMembersTitle}
            </HeadingTitle>
        )
    }
}

const mapStateToProps = state => {
    return {
        onlineMembersTitle : state.gv_reducer.activeLanguage.onlineMembers.onlineMembers_title
    }
};

export default connect(mapStateToProps)(Title)