import React from 'react';
import OnlineMembersComponent from '../../../../Components/OnlineMembers/OnlineMembers';
import connect from "react-redux/es/connect/connect";
import {Input, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import Title from '../../../../Components/Heading/Title';
import CircularButton from "../../../../Components/CircularButton/CircularButton";
import Loader from 'react-loader-spinner';
import {GREEN_COLOR} from "../../../../utils/css-constants";
import {reActiveUser} from "../../../../store/User/user-actions";
import ErrorMessage from '../../../../Components/FormComponents/ErrorMessage/ErrorMessage';
import {formatName} from "../../../../utils/common-utils";
import {SERVER_URL} from "../../../../server";
import {formatDate} from "../../../../utils/common-utils";

class ShowMembers extends React.Component {

    state = {
        showModal: false,
        password: '',
        processing: false
    };


    open = (user) => {
        let _state = {...this.state};
        _state.user = user;
        _state.error = '';
        _state.password = '';

        _state.showModal = !_state.showModal;

        this.setState(_state);
    };

    setActiveUser = () => {
        let {_id} = this.state.user;
        let {password} = this.state;

        this.setState({processing: true});
        this.props.login(_id, password, (error) => {
            if (error) {
                this.setState({processing: false});
                this.setState({error: error});
            } else {
                this.setState({processing: false, showModal: false});
            }
        });
    };

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.setActiveUser();
        }
    };

    reActiveUserDialog = () => {
        const buttonStyle = {
            fontWeight: "normal",
            width: "100%",
            color: '#fff',
            padding: "10px",
            borderRadius: "5px",
            border: "unset"
        };

        return (
            <Modal isOpen={this.state.showModal} toggle={this.open} autoFocus={false}>
                <ModalHeader style={{border: "unset"}} toggle={this.open}>
                    <Title afterClass={"blackDash"}>
                        {this.props.headingText}
                    </Title>
                    <span>{this.state.user && this.state.user.firstName && formatName(this.state.user)}</span>
                </ModalHeader>

                {this.state.processing ?
                    <div style={{textAlign: 'center'}}><Loader type="Triangle" color={GREEN_COLOR} height="40px"
                                                               width="40px"/></div> :
                    <div>
                        <ModalBody>
                            {this.state.error &&
                            <ErrorMessage large style={{marginBottom: '12px'}}>{this.state.error}</ErrorMessage>}
                            <Input type={"password"} name={"pin"} placeholder={"Password"}
                                   value={this.state.password}
                                   onKeyPress={this.handleKeyPress}
                                   onChange={(e) => this.setState({password: e.target.value})} autoFocus/>
                        </ModalBody>

                        <ModalFooter style={{border: "unset"}}>
                            <CircularButton style={buttonStyle} backColor="#BF1C1C" margin="0px 10px 0px 0px"
                                            onClick={this.open}>
                                {this.props.cancelText}
                            </CircularButton>

                            <CircularButton style={buttonStyle} backColor="#13C95F" margin="0px 0px 0px 10px"
                                            onClick={this.setActiveUser}>
                                {this.props.activeText}
                            </CircularButton>
                        </ModalFooter>
                    </div>
                }
            </Modal>
        )
    };


    getOnlineMembers = (members, activeUser) => {
        return (
            members.map((data, index) => {
                if(activeUser && activeUser._id === data.user._id){
                    var user = {
                        firstName:activeUser.firstName,
                        lastName:activeUser.lastName
                    }
                }else{
                    user = data.user;
                }

                return (
                    <OnlineMembersComponent key={index}
                                            image={`${SERVER_URL}/photos/${data.user.picture}`}
                                            name={formatName(user)}
                                            price={formatDate(data.joinedTime, false)}
                                            user={data.user}
                                            activeUser={activeUser}
                                            onClick={() => {
                                                if (this.props.hallAccess && (!activeUser || (activeUser._id !== data.user._id))) {
                                                    this.open(data.user);
                                                }
                                            }}
                    />
                )
            })
        );
    };

    render() {
        const {users, activeMembers} = this.props;

        return (
            <div className="row">
                {this.reActiveUserDialog()}
                {this.getOnlineMembers(users, activeMembers)}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        activeMembers: state.user_reducer.activeUser,
        hallAccess: Boolean(state.server_reducer.hallAccessToken),

        headingText: state.gv_reducer.activeLanguage.onlineMembers.reActiveDialogHeading,
        activeText: state.gv_reducer.activeLanguage.onlineMembers.activeText,
        cancelText: state.gv_reducer.activeLanguage.onlineMembers.cancelText
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (_id, password, cb) => dispatch(reActiveUser(_id, password, cb))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowMembers)