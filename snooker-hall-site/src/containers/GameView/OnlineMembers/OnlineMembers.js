import React from 'react';
import Transition from '../../../Components/Transition/Transition';
import ShowMembers from './ShowMembers/ShowMembers';
import Title from './Title/Title';
import ScrollBar from "../HallView/WaitingList/ScrollBar";
import {connect} from 'react-redux';
import {getObjectValue} from "../../../utils/common-utils";

const searchFields = ['firstName', 'lastName', 'phoneNumber'];

class HallView extends React.Component {

    state = {
        users: null
    };

    searchUsers = (searchStr, searchParams = []) => {
        const users = [...this.props.hallUsers];
        console.log("All Hall Users", users)
        if (searchStr) {
            let filteredUsers = [];
            users.forEach(hallUser => {
                let {user} = hallUser;
                console.log("Singlr User In HallUsers", user)

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                        const _val = getObjectValue(user, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredUsers.push(hallUser);
                        break;
                    }
                }
            });
            this.setState({users: filteredUsers});
        }
        else {
            this.setState({users: null});
        }
    };

    containerStyle = {
        background: "rgba(31, 31, 31, 0.9)",
        border: "1px solid  rgba(206, 210, 219,0.7)",
        borderRadius: "7px",
        padding: "5%",
        width: "100%",
        display: "inline-block",
        height: '100%',
        margin: "unset",
        marginTop: "4%"
    };

    style = {
        backgroundColor: 'rgba(31, 31, 31, 0.9)',
        border: '1px solid  rgba(206, 210, 219,0.7)',
        borderRadius: '4px',
        color: 'white',
        padding: '1% 5% 1% 3%',
    };

    render() {
        const users = this.state.users ? this.state.users : this.props.hallUsers;

        return (
            <div className="row" style={this.containerStyle}>
                <Transition>
                    <div className={"col-md-12"} style={{paddingTop: '3%'}}>
                        <div className="row">
                            <div className={"col-md-6"}>
                                <Title/>
                            </div>
                            <div className={"col-md-6"} style={{textAlign: 'right'}}>
                                <input placeholder=" Search..." style={this.style}
                                       onChange={(e) => this.searchUsers(e.target.value, searchFields)}/>
                            </div>
                        </div>
                    </div>

                    <ScrollBar style={{maxHeight: '40vh'}} scrollType="waitingScroll">
                        <div className={"col-md-12"} style={{height: '100%', paddingTop: '3%'}}>
                            <ShowMembers users={users}/>
                        </div>
                    </ScrollBar>
                </Transition>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        hallUsers: state.user_reducer.users
    };
};

export default connect(mapStateToProps)(HallView);
