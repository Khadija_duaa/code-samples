import React from 'react'
import Header from './Header/Header';
import Slider from './MainSlider/Slider';
import About from './About/About';
import Services from './Services/Services';
import Miniature from './Miniature/Miniature';
import Packages from './Packages/Packages';
import Gallery from './Gallery/Gallery';
import Contact from './Contact/Contact';
import Footer from './Footer/Footer';
import Transition from '../../Components/Transition/Transition'

import './style.css';
import './landing-page.css';

import {fetchGallery} from "../../store/server/server-actions";

import {connect} from 'react-redux';
import ScrollButton from "../../Components/ScrollTop/ScrollButton";

class LandingPage extends React.Component {

    componentDidMount() {
        this.props.fetchGallery();
    }

    render() {

        return (
            <Transition>
                <div className={'landing-page-container'}>
                    <Header/>

                    <Slider/>

                    <main id="main">
                        <About/>
                        <Services/>

                        <Miniature/>

                        <Packages/>

                        <Gallery/>

                        <Contact isMarkerShown lat="59.4306784" lng="17.9428211"
                                 api_key="AIzaSyBr2f9VD_nze2ZJsyzYVbiJlroIf3aWk1I"/>
                    </main>

                    <Footer/>
                    <ScrollButton scrollStepInPx="50" delayInMs="10"/>
                </div>
            </Transition>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchGallery: () => dispatch(fetchGallery()),
    };
};


export default connect(null, mapDispatchToProps)(LandingPage);