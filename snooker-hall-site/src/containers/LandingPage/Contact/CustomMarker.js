import React, { Component } from 'react';
import { Marker, InfoWindow } from 'react-google-maps';
import './Marker.css'
import connect from 'react-redux/es/connect/connect';



class CustomMarker extends Component {
   state = {
        showInfoWindow: false
    };

    contactArray = {
        "contact":{
            "personal":[
                {
                    "tag":this.props.tag1,
                    "icon":"fa fa-user"
                },
                {
                    "tag":"076 20 78 739",
                    "icon":"fa fa-phone"
                },
                {
                    "tag":this.props.tag3,
                    "icon":"fa fa-envelope"
                }

            ]
        }
    }


    contactData =()=> this.contactArray.contact.personal.map((contact_obj)=>{
        return(
            <p key={contact_obj.tag}>
                <i className={contact_obj.icon}/>
                {contact_obj.tag}</p>
        )
    })


    onMarkerClick = e => {
        this.setState({
            showInfoWindow: true
        });
    };

    onCloseClick = e => {
        this.setState({
            showInfoWindow: false
        });
    };

    render() {
        const { showInfoWindow } = this.state;

        return (
            <Marker position={ {lat: 59.4306784, lng:17.9428211} } onClick={this.onMarkerClick} >
                {showInfoWindow && (
                    <InfoWindow style={{width:"20px !important",height:"100%"}} onCloseClick={this.onCloseClick}>
                        <div className={"container"} style={{padding:"20px"}}>
                            <header className={"section-header"} style={{padding:"0px 10vw 0px 0px ",textAlign:"left"}}>
                                <h3>{this.props.main_title}</h3>
                            </header>

                            <div id={"marker_contact"}>
                                <h6>{this.props.contact_title1}</h6>
                                {this.contactData()}
                            </div>

                            <div id={"marker_contact"}>
                                <h6>{this.props.contact_title2}</h6>

                                <p>
                                    <i className={"fa fa-map-marker"}/>
                                    {this.props.address}
                                </p>
                            </div>


                        </div>

                    </InfoWindow>
                )}
            </Marker>
        );
    }
}


const mapStateToProps = (state)=>{
    return{
        tag1:state.lp_reducer.activeLanguage.contact.contact_tag1,
        tag3:state.lp_reducer.activeLanguage.contact.contact_tag3,
        main_title:state.lp_reducer.activeLanguage.contact.main_title,
        contact_title1:state.lp_reducer.activeLanguage.contact.contact_title1,
        contact_title2:state.lp_reducer.activeLanguage.contact.contact_title2,
        address:state.lp_reducer.activeLanguage.contact.address_description
    }
}

export default connect(mapStateToProps)(CustomMarker);