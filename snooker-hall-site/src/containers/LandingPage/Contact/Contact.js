import React from 'react'
import {compose, withProps} from "recompose";

import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
} from "react-google-maps";

import CustomMarker from './CustomMarker';

const Contact = compose(
    withProps({
        googleMapURL:
            "https://maps.googleapis.com/maps/api/js?key=AIzaSyBr2f9VD_nze2ZJsyzYVbiJlroIf3aWk1I",
        loadingElement: <div style={{height: `100%`}}/>,
        containerElement: <div style={{height: "600px"}}/>,
        mapElement: <section id={"contact"}
                             style={{height: "100%"}}>
        </section>
    }),
    withScriptjs,
    withGoogleMap
)(props => (

    <div>
        <GoogleMap defaultZoom={15} defaultCenter={{lat: 59.4306784, lng: 17.9428211}}>
            {props.isMarkerShown && (
                <CustomMarker lats="59.4306784" long="17.9428211"/>
            )}
        </GoogleMap>
    </div>

));

export default Contact;
