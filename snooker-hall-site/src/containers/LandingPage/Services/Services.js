import React from 'react'
import connect from 'react-redux/es/connect/connect';


const Services = (props) => {

    const serviceArray = {
        "abouts": [
            {
                title: props.ab_service1,
                svg: "queue",
                delay: "0s"
            },
            {
                title: props.ab_service2,
                svg: "stats",
                delay: "0.1s"
            },
            {
                title: props.ab_service3,
                svg: "online_payment",
                delay: "0.2s"
            },
            {
                title: props.ab_service4,
                svg: "summary",
                delay: "0.3s"
            }
        ]
    };

    const serviceData = serviceArray.abouts.map((item) => {
        return (
            <div key={item.title} className="col-md-3 wow fadeInUp" data-wow-delay={item.delay}>
                <div className="services-col">
                    <h2 className="title">{item.title}</h2>
                    <div className="img">
                        <img alt={""} className={"svg"} src={`/img/services/${item.svg}.svg`}/>

                    </div>
                </div>
            </div>
        )
    });


    return (
        <div>
            <section id="services">
                <div className="container">
                    <header className="section-header">
                        <h3>{props.ab_title}</h3>
                        <p>{props.ab_description}</p>
                    </header>

                    <div className="row services-cols">
                        {serviceData}
                    </div>
                </div>
            </section>
        </div>
    );
};


const mapStateToProps = (state) => {
    return {
        ab_title: state.lp_reducer.activeLanguage.services.ab_title,
        ab_description: state.lp_reducer.activeLanguage.services.ab_description,
        ab_service1: state.lp_reducer.activeLanguage.services.ab_service1,
        ab_service2: state.lp_reducer.activeLanguage.services.ab_service2,
        ab_service3: state.lp_reducer.activeLanguage.services.ab_service3,
        ab_service4: state.lp_reducer.activeLanguage.services.ab_service4
    }
};


export default connect(mapStateToProps)(Services);