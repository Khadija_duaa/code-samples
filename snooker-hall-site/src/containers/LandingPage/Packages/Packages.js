import React from 'react'
import connect from "react-redux/es/connect/connect";
import ColouredLine from '../../../Components/ColouredLine/ColouredLine';
import {GREEN_COLOR} from "../../../utils/css-constants";
import {formatAmount} from "../../../utils/common-utils";
import {withRouter} from 'react-router-dom';
// import PackageDetails from "./PackageDetails/PackageDetails";
import {toAbbrevDays} from "../../../utils/date-utils";

const badgeArray = ["junior", "bronze", "silver", "gold"];

class Packages extends React.Component {

    state = {
        isDetails: true,
        currentMembership: null,
    };
    openDetails = (membership) => {
        this.setState({isDetails: !this.state.isDetails, currentMembership: membership});
    };
    
    getPackageBadge = (membership) => {
        const headingStyle = {
            marginTop: "3vw",
            WebkitTextStroke: "1px"
        };


        return (
            <div>
                <img alt={membership.name} style={{width: "4vw"}} src={membership.imageUrl}/>
                <h6 style={headingStyle}>
                    {membership.name}
                </h6>
            </div>
        );
    };


    getPackageDetails = (column) => {
        let columnJSX = column.map((item, index) => {
            return (
                <p style={{height: "30px", fontSize: "14px"}} key={index}>
                    {typeof (item) === 'number' ? formatAmount(item) : item.length > 0 ? item : 'X'}
                </p>
            )
        });
        return (
            <div style={{paddingTop: "10px", fontSize: "14px"}}>
                {columnJSX}
            </div>
        );
    };

    getPackageDetailsButton = (membership) => {
        if (!membership._id) return null;
        let isActive = this.props.isActiveUser;
        let propsHistory = this.props.history;
        const btnStyle = {
            backgroundColor: `${GREEN_COLOR}`,
            color: "#fff",
            padding: "5px",
            border: `1px solid ${GREEN_COLOR}`,
            borderRadius: "5px",
            width: "70%",
            marginTop: "20px",
            fontSize: "14px",
            cursor: "pointer",
            transition: "0.5s",
            boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.3)"
        };

        return (
            <div>
                <button className={"lp-packages_btnStyle"} style={btnStyle}
                        onClick={() => {
                            isActive ? propsHistory.push('/hall/profile/membership/upgrade') : propsHistory.push('/hall/login')
                        }}>
                    {this.props.upgradeBtnText}
                </button>

            </div>

        );
    };


    renderPackages = (_memberships) => {
        const columnStyle = {
            borderLeft: "1px solid rgba(239, 239, 239,0.5)",
            textAlign: "center",
            width: "20%",
            padding: "unset",
            paddingBottom: "5vw",
            marginBottom: "-5vw"
        };

        let ratesArray = {}, columns = [];
        let generateSpecialRates = () => _memberships.map((membership, index) => {

            return membership.specialRates.map((item) => {

                let abbrevDays = toAbbrevDays(item.days);

                if (!ratesArray[`${abbrevDays} (${item.startTime}-${item.endTime})`]) {
                    ratesArray[`${abbrevDays} (${item.startTime}-${item.endTime})`] = ['', '', '', ''];
                    return ratesArray[`${abbrevDays} (${item.startTime}-${item.endTime})`][index] = item.rate;
                } else {
                    return ratesArray[`${abbrevDays} (${item.startTime}-${item.endTime})`][index] = item.rate;
                }
            })
        });


        generateSpecialRates();
        let days = Object.keys(ratesArray);
        let prices = Object.values(ratesArray);


        const getColumnValues = (index) => {
            for (let j = 0; j < days.length; j++) {
                columns[index][Object.values(columns[index]).length] = prices[j][index];
            }

        };
        const getColumnKeys = () => {
            for (let j = 0; j < days.length; j++) {
                columns[0][Object.values(columns[0]).length] = days[j];
            }

        };


        let memberships = _memberships.map((_membership, index) => {


            let image_name = badgeArray.includes(_membership.name.toLowerCase())?_membership.name.toLowerCase()+".svg":"empty.svg";


            columns.push([]);

            columns[index][0] = _membership.price;


            if (days.length > 0) {
                getColumnValues(index);
            }
            columns[index][Object.values(columns[index]).length] = _membership.rate;

            return {
                imageUrl: `/img/badges/${image_name}`,
                name: _membership.name,
                _id: _membership._id,
                specialRates: _membership.specialRates
            };
        });

        memberships.unshift(
            {
                imageUrl: `/img/badges/empty.svg`,
                name: this.props.pack_title
            }
        );


        columns.unshift([]);
        columns[0][0] = this.props.pack_lowerText;


        if (days.length > 0) {
            getColumnKeys();
        }
        columns[0][Object.keys(columns[0]).length] = this.props.otherRateText;

        return memberships.map((membership, index) => {
            return (
                <div className={"col"} style={columnStyle} key={membership.name}>

                    {this.getPackageBadge(membership)}

                    <ColouredLine style={{opacity: "0.5"}}/>

                    {this.getPackageDetails(columns[index])}

                    {this.getPackageDetailsButton(membership)}
                </div>
            );
        });
    };

    getHeader = () => {
        return (
            <header className="section-header">
                <h3>{this.props.pack_title}</h3>
                <p>{this.props.pack_description}</p>
            </header>
        );
    };

    getPackageData = (membership) => {
        // if (this.state.isDetails) {
        //
        // }

        return this.renderPackages(membership);

        // return <PackageDetails onBack={() => {
        //     this.setState({isDetails: true})
        // }} membership={this.state.currentMembership} style={{marginBottom: "-5vw"}}/>
    };

    getBody = (memberships) => {
        const mainDiv = {
            backgroundColor: "white",
            borderRadius: "5px",
            textAlign: "center",
            paddingBottom: "5vw",
            boxShadow: "0px 2px 20px 12px rgba(0,0,0, 0.04)"
        };

        return (
            <div style={{padding: "0px 10vw"}} className="wow fadeInUp">
                <div className={"row"} style={mainDiv}>
                    {this.getPackageData(memberships)}
                </div>
            </div>
        );
    };

    render() {
        let {memberships} = this.props;
        if (!memberships || !memberships.length) return null;

        return (
            <div>
                <section id="packages">
                    <div className="container">
                        {this.getHeader()}
                        {this.getBody(memberships)}
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        pack_title: state.lp_reducer.activeLanguage.packages.pack_title,
        pack_description: state.lp_reducer.activeLanguage.packages.pack_description,
        memberships: state.lp_reducer.memberships,
        pack_details: state.lp_reducer.activeLanguage.packages.details,
        upgradeBtnText: state.lp_reducer.activeLanguage.package_details.upgradeBtnText,
        otherRateText:state.lp_reducer.activeLanguage.package_details.otherRateText,
        pack_upperText: state.lp_reducer.activeLanguage.packages.pack_upperText,
        pack_lowerText: state.lp_reducer.activeLanguage.packages.pack_lowerText,
        isActiveUser: Boolean(state.user_reducer.activeUser)
    }
};

const connected = connect(mapStateToProps)(Packages);
export default withRouter(connected)