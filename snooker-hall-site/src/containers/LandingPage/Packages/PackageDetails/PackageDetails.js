import React from 'react';
import Heading from "../../../../Components/Dashboard/Contents/Heading";
import Para from "../../../../Components/Para/Para";
import {GREEN_COLOR} from "../../../../utils/css-constants";
import CircularButton from "../../../../Components/CircularButton/CircularButton";
import {formatAmount, getTdWidth} from "../../../../utils/common-utils";
import '../../../../Components/FixHeaderTable/style.css';
import {toAbbrevDays} from "../../../../utils/date-utils";
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import ScrolledTable from '../../../../Components/ScrolledTable/ScrolledTable';

class PackageDetails extends React.Component {


    /******************************** Headings Panel Data ************************************/

    getBadge = (imageUrl) => <img alt={imageUrl} src={imageUrl} style={{width: "4vw"}}/>;

    getTitle = (name) => {
        const fontStyle = {
            fontSize: "25px",
            color: "black",
            fontWeigth: "normal",
            "WebkitTextStroke": "0.3px"
        };
        return (
            <Heading style={{margin: "5% auto", width: "50%"}}
                     fontStyle={fontStyle}
                     title={`${name} ${this.props.membershipText}`}/>
        )
    };

    getPrices = (membership) => {
        const paraStyle = {
            fontSize: "14px"
            , fontWeight: "bold",
            "WebkitTextStroke": "0.1px",
            letterSpacing: "0.5px"
        };
        return (
            <div>
                <Para style={paraStyle}>
                    {membership.specialRates.length ? this.props.otherRateText : this.props.basicRateText}:
                    <span style={{color: GREEN_COLOR}}>{` ${formatAmount(membership.upperText)} /hr`}</span>
                </Para>

                <Para style={paraStyle}>
                    {this.props.subscriptionText}:
                    <span style={{color: GREEN_COLOR}}>{` ${formatAmount(membership.lowerText)} /Yr`}</span>
                </Para>
            </div>
        )
    };

    getButtons = () => {

        const buttonStyle = {
            fontSize: "14px",
            padding: "10px",
            color: "#fff",
            width: "100%",
            borderRadius: "7px"
        };

        return (
            <div style={{margin: "20% auto", width: "55%"}}>

                {
                    this.props.isActiveUser ?
                        <CircularButton style={{...buttonStyle, backgroundColor: GREEN_COLOR}} onClick={() => {
                            this.props.history.push('/hall/profile/membership/upgrade')
                        }}>
                            {this.props.upgradeBtnText}
                        </CircularButton>
                        : null
                }


                <CircularButton style={{...buttonStyle, marginTop: "15px", backgroundColor: "#000"}}
                                onClick={this.props.onBack}>
                    {this.props.backBtnText}
                </CircularButton>
            </div>
        )
    };

    getHeadingsPanel = (membership) => {

        return (
            <div className={"col-md-5"}>
                {this.getBadge(membership.imageUrl)}
                {this.getTitle(membership.name)}
                {this.getPrices(membership)}
                {this.getButtons()}
            </div>
        )

    };


    /******************************** Detail Panel Data ************************************/

    getDetailsPanel = (membership) => {


        let detailHeads = this.props.detailHeads;
        let tableHeaderJSX = detailHeads.map((head, index) => {
            return (
                <th style={{backgroundColor: "#FAFAFA", width: getTdWidth(detailHeads)}} key={index}>
                    {head}
                </th>
            )
        });


        let tableBodyJSX = membership.specialRates.map((data, index) => {

            return (
                <tr key={index}>
                    <td style={{width: getTdWidth(detailHeads)}}>{data.days.length ? toAbbrevDays(data.days) : this.props.anydayText}</td>
                    <td style={{width: getTdWidth(detailHeads)}}>{data.startTime} - {data.endTime}</td>
                    <td style={{width: getTdWidth(detailHeads)}}>{data.rate}</td>
                </tr>
            )
        });

        return (

            <div className={"col-md-7"} style={{padding: "unset"}}>
                <ScrolledTable width={"100%"} height={"300px"}
                               style={{maxHeight: "300px", marginTop: "30%", marginBottom: "30%"}}>
                    <table className="table table-hover borderTable fixHead">
                        <thead>
                        <tr>
                            {tableHeaderJSX}
                        </tr>
                        </thead>

                        <tbody>
                        {tableBodyJSX}
                        </tbody>
                    </table>
                </ScrolledTable>
            </div>
        )
    };


    render() {
        const {membership} = this.props;

        return (
            <div className={"col-md-12"} style={this.props.style}>
                <div className={"row"}>
                    {this.getHeadingsPanel(membership)}
                    {this.getDetailsPanel(membership)}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const packageDetails = state.lp_reducer.activeLanguage.package_details;

    return {
        membershipText: packageDetails.membershipText,
        basicRateText: packageDetails.basicRateText,
        otherRateText: packageDetails.otherRateText,
        subscriptionText: packageDetails.subscriptionText,
        upgradeBtnText: packageDetails.upgradeBtnText,
        backBtnText: packageDetails.backBtnText,
        detailHeads: packageDetails.detailHeads,
        anydayText: packageDetails.anydayText,
        isActiveUser: Boolean(state.user_reducer.activeUser)
    }
};

const connected = connect(mapStateToProps)(PackageDetails);
export default withRouter(connected);