import React from 'react'
import connect from 'react-redux/es/connect/connect'
import {GREEN_COLOR} from "../../../utils/css-constants";

import Scrollchor from 'react-scrollchor';

const navArray = ["Home", "About", "Services", "Packages", "Gallery", "Contact"];

class Footer extends React.Component {


    navLinks = ()=> this.props.navArray.map((item, index) => {
        return (
            <li key={index}>

                <Scrollchor to={`#${navArray[index].toLowerCase()}`} animate={{offset: -120, duration: 1000}}>
                    {item}
                </Scrollchor>

            </li>
        )
    });

    addressData = ()=> this.props.footerArray.address.map((item) => {
        return <p key={item.tag}>{item.tag} : <span style={{color: GREEN_COLOR}}>{item.detail}</span></p>

    });

    render() {

        return (
            <div>
                <footer id="footer">
                    <div className="footer-top">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6 col-md-12 col-sm-6 footer-info">
                                    <h3>{this.props.main_title}</h3>
                                    {this.addressData()}
                                </div>

                                <div className="col-lg-2 col-md-4 col-sm-4 footer-links">
                                    <h4>{this.props.head1}</h4>
                                    <ul className="footer-nav">
                                        {this.navLinks()}
                                    </ul>
                                </div>

                                <div className="col-lg-2 col-md-4 col-sm-4 footer-links">
                                    <h4>{this.props.head2}</h4>
                                    <ul>
                                        <li>
                                            <a target="_blank"
                                               rel="noopener noreferrer"
                                               href="https://www.facebook.com/Snooker-247-235371880410642">
                                                {this.props.linkTitle_1}
                                            </a>
                                        </li>
                                        {/*<li><a href="#">{this.props.linkTitle_2}</a></li>*/}
                                        {/*<li><a href="#">{this.props.linkTitle_3}</a></li>*/}
                                    </ul>

                                </div>

                                <div className="col-lg-2 col-md-4 col-sm-4 footer-newsletter">
                                    <h4>{this.props.head3}</h4>
                                    <p>
                                        {this.props.timing_1}
                                        <br/>
                                        <span style={{color: "#1A1A1A"}}>00:00 - 24:00</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="container" id="reclaimation">
                        <h4>{this.props.reclaim_title}</h4>
                        <div className="description">
                            <p>{this.props.reclaim_description}</p>
                        </div>
                    </div>

                    <div className="container" id="copyright">
                        <div className="description">
                            {this.props.copyright_content}
                        </div>
                    </div>

                </footer>
            </div>
        );
    }
}

const mapStateToProps = (state)=>{
    return {
        main_title:state.lp_reducer.activeLanguage.footer.foot_title,
        navArray: state.lp_reducer.activeLanguage.footer.navArray,
        footerArray:state.lp_reducer.activeLanguage.footer.footerArray,

        head1:state.lp_reducer.activeLanguage.footer.foot_head1,
        head2:state.lp_reducer.activeLanguage.footer.foot_head2,
        head3:state.lp_reducer.activeLanguage.footer.foot_head3,

        linkTitle_1:state.lp_reducer.activeLanguage.footer.foot_linkTitle_1,
        //linkTitle_2:state.lp_reducer.activeLanguage.footer.foot_linkTitle_2,
        //linkTitle_3:state.lp_reducer.activeLanguage.footer.foot_linkTitle_3,

        timing_1:state.lp_reducer.activeLanguage.footer.foot_timing_1,

        reclaim_title:state.lp_reducer.activeLanguage.footer.foot_reclaim_title,
        reclaim_description:  state.lp_reducer.activeLanguage.footer.foot_reclaim_description,

        copyright_content:state.lp_reducer.activeLanguage.footer.foot_copyright_content
    }
};

export default connect(mapStateToProps)(Footer);