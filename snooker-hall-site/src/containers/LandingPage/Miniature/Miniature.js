import React from 'react'
import connect from 'react-redux/es/connect/connect';

const Miniature = (props) => {
    return (
        <div>
            <section id="miniature">
                <div className="container">
                    <header className="section-header wow fadeInUp">
                        <h1>“</h1>
                        <div className="miniature_text">
                            <blockquote id="description">
                                {props.min_description}
                            </blockquote>

                            <div className="founder_info">
                                <h5 id="founder_name">{props.min_founderName}</h5>
                                <p id="founder_title">
                                    {props.min_founderTitle}
                                    <br/>
                                    {props.min_founderContact}
                                </p>
                            </div>
                        </div>
                    </header>
                </div>
            </section>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        min_description: state.lp_reducer.activeLanguage.miniature.min_description,
        min_founderName: state.lp_reducer.activeLanguage.miniature.min_founderName,
        min_founderTitle: state.lp_reducer.activeLanguage.miniature.min_founderTitle,
        min_founderContact: state.lp_reducer.activeLanguage.miniature.min_founderContact
    }
};

export default connect(mapStateToProps)(Miniature);