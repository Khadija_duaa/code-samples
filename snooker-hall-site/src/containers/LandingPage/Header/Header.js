import React from 'react'
import Bilingual from '../../../Components/Bilingual/Bilingual';
import 'react-flags-select/css/react-flags-select.css';
import {logoutUser} from '../../../store/User/user-actions';
import connect from 'react-redux/es/connect/connect'
import {withRouter} from 'react-router-dom';
import Scrollchor from 'react-scrollchor';
import {withGetScreen} from 'react-getscreen'
import {isChrome} from 'react-device-detect'
const navArray = ["Home", "About", "Services", "Packages", "Gallery", "Contact"];


class Header extends React.Component {


    state = {
        width:'',
    };

    componentDidMount() {
        document.addEventListener('scroll', this.handleScroll);
        window.addEventListener('resize',this.resize.bind(this));
        this.resize();
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.handleScroll);
        window.removeEventListener('resize',this.resize);
    }

    resize = ()=>{
      this.setState({width:window.screen.width})
    };

    handleScroll = () => {

        const headerNav = document.getElementById('header');

        if (window.scrollY > 100) {
            headerNav.classList.add('header-scrolled')
        } else {
            headerNav.classList.remove('header-scrolled')
        }

    };


    getLogo = (img_style) => {
        let logo_style = {
            width:"100%",
            transition:"1s"
        };

        if(!isChrome){

            logo_style.minWidth = "155px"

        }

        return (
            <div id="logo" style={img_style && img_style}>
                <span>
                    <a style={{cursor: "pointer"}}>
                        <img src={"/img/logo/logo.svg"} onClick={() => window.location.href = "/"} alt="" title=""
                             style={logo_style}/>
                    </a>
                </span>
            </div>
        );
    };



    getNavMenu = () => {

        let leftNavJSX = this.props.leftnavArray.map((item, index) => {
            //let name = this.state.index === index?'menu-active':'';
            return (
                <li key={index}>
                    <Scrollchor to={`#${navArray[index].toLowerCase()}`} animate={{offset: -120, duration: 1000}}>
                        {item}
                    </Scrollchor>
                </li>
            )
        });

        let rightNavJSX = this.props.rightnavArray.map((item) => {

            //let name = this.state.index === item.key?'menu-active':'';

            return (
                <li key={item.key}>
                    <Scrollchor to={`#${navArray[item.key].toLowerCase()}`} animate={{offset: -120, duration: 800}}>
                        {item.name}
                    </Scrollchor>
                </li>
            )
        });

        let  navMenuContainerStyle = {
            float: "unset",
            paddingTop: "50px",
            margin: "auto"
        };


        let menu_style = {
            marginTop: "-50px",
            marginRight:"-10px",
            marginLeft:"-10px"
        };

        return (
            <nav id="nav-menu-container" style={navMenuContainerStyle}>
                <ul className="nav-menu" style={{display: "inline"}}>
                    {leftNavJSX}
                </ul>

                {this.getLogo(menu_style)}

                <ul className={"nav-menu"} style={{display: "inline", width: "inherit"}}>
                    {rightNavJSX}
                    <li>
                        <a id={"floor_view"} className={'hover lp-nav-menu-a'}
                           onClick={() => this.props.history.push('/hall')}>
                            {this.props.floorView}
                        </a>
                    </li>
                    <Bilingual/>
                </ul>
            </nav>
        );
    };

    getMobileNavMenu = ()=>{
        return(
            <nav id="nav-menu-container" style={{width:"inherit",paddingTop:"30px"}}>
                {this.getLogo({marginTop:"-30px"})}
                <ul className={"nav-menu"} style={{float: "right"}}>
                    <li>
                        <a id={"floor_view"} className={'hover lp-nav-menu-a'}
                           onClick={() => this.props.history.push('/hall')}>
                            {this.props.floorView}
                        </a>
                    </li>
                    <li style={{color:"rgb(112, 112, 112)"}}>
                        |
                    </li>
                    <Bilingual/>
                </ul>
            </nav>
        );
    };

    render() {

        return (
            <div>
                <header id="header">
                    <div className="container-fluid" style={{display: "flex"}}>
                        {this.state.width <= 768?this.getMobileNavMenu() : this.getNavMenu()}
                    </div>
                </header>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        leftnavArray: state.lp_reducer.activeLanguage.header.leftnavArray,
        rightnavArray: state.lp_reducer.activeLanguage.header.rightnavArray,
        floorView: state.lp_reducer.activeLanguage.header.h_floorView,
        signIn: state.lp_reducer.activeLanguage.header.h_signIn,
        activeUser: state.user_reducer.activeUser,
        logoutTitle: state.lp_reducer.activeLanguage.header.h_logout,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logoutUser: () => dispatch(logoutUser())
    }
};

//or you may set your own breakpoints by providing an options object

const options = {tabletLimit: 769};
const getScreen = withGetScreen(Header, options);

const connected = connect(mapStateToProps, mapDispatchToProps)(getScreen);

export default withRouter(connected)