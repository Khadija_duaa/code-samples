import React from 'react'
import connect from 'react-redux/es/connect/connect';
import TimeLine from '../../../Components/Vertical TimeLine/TimeLine';


class About extends React.Component {
    render() {
        const props = this.props;
        return (
            <div>
                <section id={"about"}>
                    <div className={"container"}>
                        <header className="section-header">
                            <h3>{props.ab_title}</h3>
                        </header>
                    </div>

                    <div className={"lp-about_mainDiv"}>
                        <div className={"lp-timeline_div"}>
                            <TimeLine/>
                        </div>
                        <div className={"lp-img_div"}>
                            <img src={"/img/ipad.png"} alt={'img'}/>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

};


const mapStateToProps = (state) => {
    return {
        ab_title: state.lp_reducer.activeLanguage.howWork.title,
    }
};


export default connect(mapStateToProps)(About);