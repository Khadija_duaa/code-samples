import React from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick-theme.css'
import 'slick-carousel/slick/slick.css'
import connect from 'react-redux/es/connect/connect'
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css'
import {setAlbum} from "../../../store/server/server-actions";
import {GREEN_COLOR} from "../../../utils/css-constants";
import {SERVER_URL} from "../../../server";
import './gallery.css';

const img_style = {
    minWidth: '160px',
    maxHeight: '200px',
    cursor: "pointer"
};

const li_style = {
    display: "inline-block",
    border: "0.5px solid black",
    margin: "5px 15px 0px 0px",
    padding: "1vh 2vw",
    borderRadius: "50px",
    cursor: "pointer",
    listStyle: "none",
    color: "black",
    textAlign: "center",
    transition: "0.5s"

};

const li_active_style = {
    ...li_style,
    backgroundColor: GREEN_COLOR,
    color: '#fff',
    border: `0.5px solid ${GREEN_COLOR}`,
};

const settings = {
    className: "center",
    centerMode: true,
    overflow: "visible",
    dots: true,
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                initialSlide: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]

};


class Gallery extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            photoIndex: 0,
            isOpen: false,
        }
    }

    addInImages = (album, images) => {
        album.photos && album.photos.forEach(photo => {
            images.push(`${SERVER_URL}/photos/${album._id}/${photo}`);
        });
    };

    prepareImages = () => {
        let images = [];
        let {gallery, album} = this.props;

        if (album) {
            this.addInImages(album, images);
        }
        else if (gallery && gallery.length) {
            gallery.forEach(_album => this.addInImages(_album, images));
        }

        return images;
    };


    getAlbumNames = () => {
        let jsx = null;
        let {gallery} = this.props;

        if (gallery && gallery.length) {
            jsx = gallery.filter(album => album.photos && album.photos.length)
                .map(album => {
                    return (
                        <li className={'albumCategory'} onClick={() => this.props.setAlbum(album._id)}
                            style={this.props.album && (album._id === this.props.album._id) ? li_active_style : li_style}
                            key={album._id}>
                            {album.name}
                        </li>
                    );
                });

            if (jsx && jsx.length) {
                jsx.unshift(<li className={'albumCategory'}
                                onClick={() => this.props.setAlbum(null)}
                                style={!this.props.album ? li_active_style : li_style}
                                key={'All_Album'}>All</li>);
            }
        }


        return jsx ? <ul style={{paddingInlineStart:"unset"}}>{jsx}</ul> : null;
    };


    getSlider = (images) => {
        let _settings = {...settings};
        _settings.infinite = images.length > 3;

        return (
            <Slider key={"gallery-slider"} {..._settings} className="wow fadeInUp">
                {images.map((image, index) => {
                    return (
                        <div key={image}>
                            <img
                                style={img_style}
                                src={image}
                                onClick={() => this.setState({isOpen: true, photoIndex: index})}
                                alt={""}
                            />
                        </div>
                    );
                })}
            </Slider>
        );
    };

    getLightBox = (images) => {
        let {isOpen, photoIndex} = this.state;
        if (!isOpen) return null;

        return (
            <Lightbox
                mainSrc={images[photoIndex]}
                nextSrc={images[(photoIndex + 1) % images.length]}
                prevSrc={images[(photoIndex + images.length - 1) % images.length]}
                onCloseRequest={() => this.setState({isOpen: false})}
                onMovePrevRequest={() =>
                    this.setState({
                        photoIndex: (photoIndex + images.length - 1) % images.length
                    })}
                onMoveNextRequest={() =>
                    this.setState({
                        photoIndex: (photoIndex + 1) % images.length
                    })}
            />
        );
    };

    render() {
        const images = this.prepareImages();
        // If no images, do not show Gallery Component
        if (!images || !images.length) {
            return null;
        }

        return (
            <div>
                <section id="gallery">
                    <div className="container">
                        <header className="section-header">
                            <h3>{this.props.gal_title}</h3>
                            <p>{this.props.gal_description}</p>

                            {this.getAlbumNames()}
                        </header>
                        {this.getSlider(images)}
                        {this.getLightBox(images)}
                    </div>
                </section>
            </div>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        gal_title: state.lp_reducer.activeLanguage.gallery.gal_title,
        gal_description: state.lp_reducer.activeLanguage.gallery.gal_description,
        gallery: state.server_reducer.gallery,
        album: state.server_reducer.album
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setAlbum: (_id) => dispatch(setAlbum(_id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);