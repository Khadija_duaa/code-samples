import React from 'react'
import connect from 'react-redux/es/connect/connect'
import {NavLink} from 'react-router-dom';

const getButton = (props) => {
    if (props.activeUser) {
        return (
            <NavLink to={"/hall"} className="btn-get-started scrollto">
                {props.s_hallText}
                <img style={{marginLeft: "10px", width: "40px"}}
                     src={"/img/right-arrow.svg"} alt={'arrow'}/>
            </NavLink>
        )
    }
    else {
        return (
            <NavLink to={"/hall/login"} className="btn-get-started scrollto">
                {props.s_memberText}
                <img style={{marginLeft: "20px", width: "50px"}}
                     src={"/img/right-arrow.svg"} alt={'arrow'}/>
            </NavLink>
        )
    }
};

const Slider = (props) => {
    return (
        <div>
            <section id="home">
                <div className="intro-container">
                    <div id="introCarousel" className="carousel  slide carousel-fade" data-ride="carousel">
                        <div className="carousel-inner" role="listbox">

                            <div className="carousel-item active">
                                <div className="carousel-background">
                                    <img style={{width: "100vw",height:"100%",minHeight: "100vh"}}
                                         src={"/img/slider/1.jpeg"} alt=""/>
                                </div>

                                <div className="carousel-container">
                                    <div className="container">
                                        <h2>
                                            <span style={{color: "#13C95F"}}>
                                            {props.s_title1}
                                             </span>
                                            <br/>
                                            {props.s_title2}
                                        </h2>
                                        <p>{props.s_content}</p>

                                        {getButton(props)}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        s_title1: state.lp_reducer.activeLanguage.slider.s_title1,
        s_title2: state.lp_reducer.activeLanguage.slider.s_title2,
        s_content: state.lp_reducer.activeLanguage.slider.s_content,
        s_hallText: state.lp_reducer.activeLanguage.slider.s_hallText,
        s_memberText: state.lp_reducer.activeLanguage.slider.s_memberText,
        activeUser: Boolean(state.user_reducer.activeUser)
    }
};

export default connect(mapStateToProps)(Slider);