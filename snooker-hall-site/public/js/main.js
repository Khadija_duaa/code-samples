jQuery(document).ready(function( $ ) {
    var sections = $('section')
        , nav = $('header')
        , nav_height = nav.outerHeight();

  // Back to top button;
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });



  // Initiate superfish on nav menu
  $('.nav-menu').superfish({
    animation: {
      opacity: 'show'
    },
    speed: 400
  });



    // Initiate the wowjs animation library
    new WOW().init();

  // Smooth scroll for the menu and links with .scrollto classes

    // Add Scrolled Header on Page refresh
    var cur_pos = $(this).scrollTop();
    $('.back-to-top').fadeOut();

});
