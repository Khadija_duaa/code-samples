function ToggleMenu()
{
	var leftPanel = document.getElementById("leftPanel");
	if (leftPanel.style.width == "240px" || leftPanel.style.width == "")
	{
		ToggleControls("hide-from-menu", "none");
		leftPanel.style.width = "88px";
		$("#leftPanel").css("min-width", "88px");
		$(".left-menu-links ul li ul").addClass("sub-menu-links");
	}
	else
	{
		ToggleControls("hide-from-menu", "");
		leftPanel.style.width = "240px";
		$("#leftPanel").css("min-width", "240px");
		$(".left-menu-links ul li ul").removeClass("sub-menu-links");
	}
}

// toggle display of controls
function ToggleControls(className, displayState)
{
	var elements = document.getElementsByClassName(className)
	for (var i = 0; i < elements.length; i++)
	{
		elements[i].style.display = displayState;
	}
}