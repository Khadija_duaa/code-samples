import React from 'react';
import {connect} from 'react-redux';
import {formatName} from "../../../utils/common-utils";
import {logoutUser} from "../../../store/user/user-actions";
import 'antd/dist/antd.css';
import NewLoader from '../../Loader/NewLoader';

class AdminTopNav extends React.Component {

    renderLeftPanel = () => {
        return (
            <div className="col-md-5 align-middle right-panel-top-bar-left">
            </div>
        );
    };

    renderRightPanel = () => {
        const {activeUser} = this.props;

        return (
            <div className="col-md-7 align-middle text-right right-panel-top-bar-right">

                {
                    this.props.processing ?
                        <NewLoader/>
                        :
                        <div>
                            {/*<span className="notification-container">*/}
                            {/*<img alt={"bell"} src="./images/bell.svg" className="notification-bell"/>*/}
                            {/*<span className="notification-counter">10</span>*/}
                            {/*</span>*/}
                            <img alt={"member"} src="./images/member.svg"/>

                            <div role="button" id="memberLinks" className="login-options-container"
                                 data-toggle="dropdown"
                                 aria-haspopup="true" aria-expanded="false">
                                {formatName(activeUser.principal)}
                                <img src="./images/arrow-bottom.svg" className="login-options-arrow"/>
                            </div>

                            <div className="dropdown-menu login-options" aria-labelledby="memberLinks">
                                <a className="dropdown-item" onClick={() => this.props.logoutUser()}>Logout</a>
                            </div>
                        </div>
                }
            </div>
        )
    };


    render() {

        return (
            <div className="fixed-top-bar">
                <div className="row no-gutters right-panel-top-bar custom-top-bar right-panel-horizontal-spacing">
                    {this.renderLeftPanel()}
                    {this.renderRightPanel()}
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        activeUser: state.user_reducer.activeUser,
        processing: state.user_reducer.processing
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logoutUser: () => dispatch(logoutUser())
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(AdminTopNav);
export default connected;