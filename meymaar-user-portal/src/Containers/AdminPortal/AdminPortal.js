import React from 'react';
import { Route, Switch, withRouter} from 'react-router-dom';
import Admin from './Content/DashboardComponent/Admin';
import AdminForm from '../../Components/AdminPortal/Form/UserJSONForm';

import {createUserJSON} from "../../utils/utilsJSON";

import AdminLeftNav from './LeftMenu/AdminLeftNav';
import AdminTopNav from './TopNav/AdminTopNav';
import Users from "./Content/Users/Users";
import Offices from "./Content/Offices/Offices";
import Admins from "./Content/Admins/Admins";

import CreateAdmin from "./Content/Admins/CreateAdmin/CreateAdmin";
import CreateUser from "./Content/Users/CreateUser/CreateUser";
import CreateOffice from "./Content/Offices/CreateOffice/CreateOffice";
import OfficeDetails from "./Content/Offices/OfficeDetails/OfficeDetails";

import ManageRanks from "./Content/Offices/ManageRanks/ManageRanks";
import Stations from "./Content/Stations/Stations";
import Departments from './Content/Departments/Departments';
import Domains from './Content/Domains/Domains';
import ContractingPower from './Content/ContractingPower/ContractingPower';

import {hasAuthority} from "../../utils/common-utils";
class AdminPortal extends React.Component {

    render() {


        if (hasAuthority('IS_SUPER_ADMIN')) {
            let formProps = {
                formJSON:createUserJSON,
                submitURL:"/user-management/admins",
                headerText:"Create an Admin",
                bodyHeaderText:"You need to set-up an admin for the system portal and re-login",
                isSuperAdmin:true
            };

            return <AdminForm {...formProps}/>;
    }

        return (
            <div>
                <main>
                    <div className="container-fluid pl-0 pr-0">
                        <div className="panel-container">
                            <AdminLeftNav/>
                            <AdminTopNav/>

                            <div className="row no-gutters content-container right-panel-horizontal-spacing">
                                <Switch>

                                    <Route path={"/dashboard/admin/create"} name={"CreateAdmins"} component={CreateAdmin}/>
                                    <Route path={"/dashboard/admin"} name={"Admins"} component={Admins}/>

                                    <Route path={"/dashboard/stations"} name={"Stations"} component={Stations}/>
                                    <Route path={"/dashboard/departments"} name={"Departments"} component={Departments}/>
                                    <Route path={"/dashboard/domains"} name={"Domains"} component={Domains}/>
                                    <Route path={"/dashboard/contracting-powers"} name={"Contracting Power"} component={ContractingPower}/>

                                    <Route path={"/dashboard/offices/users"} name={"OfficeUsers"} component={OfficeDetails}/>
                                    <Route path={"/dashboard/offices/create"} name={"CreateOffice"} component={CreateOffice}/>


                                    <Route path={"/dashboard/offices/ranks"} name={"OfficeRanks"} component={ManageRanks}/>
                                    <Route path={"/dashboard/offices"} name={"Offices"} component={Offices}/>


                                    <Route path={"/dashboard/users/create"} name={"CreateUsers"} component={CreateUser}/>
                                    <Route path={"/dashboard/users"} name={"Users"} component={Users}/>




                                    <Route path={"/dashboard"}  component={Admin}/>

                                </Switch>
                            </div>

                        </div>
                    </div>
                </main>
            </div>
        )
    }
}

export default withRouter(AdminPortal)