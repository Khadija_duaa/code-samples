import React from 'react';
import {
    getAllContractingPowers,
    createContractingPower,
    updateContractingPower
} from "../../../../store/server/server-actions";
import {connect} from 'react-redux';
import {compareAsc, compareDesc, formatAmount, getObjectValue} from "../../../../utils/common-utils";
import NewLoader from '../../../Loader/NewLoader';
import {Icon} from 'antd';
import 'antd/dist/antd.css';
import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import HeaderTitle from "../../../../Components/HeaderTitle/HeaderTitle";
import TableContainer from "../../../../Components/Table/TableContainer/TableContainer";
import CPForm from './CPForm';
import SortableHeader from "../../../../Components/Table/SortableHeader/SortableHeader";
import RowButtonsList from "../../../../Components/Buttons/RowButtonList/RowButtonsList";
import ConfirmationModal from '../../../../Components/ModalFactory/MessageModal/ConfirmationModal';

const searchFields = ['name'];

class ContractingPower extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            departments: null,
            showModal: false,
            isAdd: false,
            formErrors: null,
            powerEditData: null,
            showConfirmModal: false
        }
    };

    componentDidMount() {
        this.fetchAllContractingPowers();
    }

    fetchAllContractingPowers = () => {
        this.props.getAllContractingPowers((powers) => {
            this.setState({allPowers: powers});
        });
    };


    /*************************************** MODAL ***********************************/

    handleConfirmClick = () => {
        let {isUpdate, powerEditData} = this.state;

        if (!isUpdate) {
            this.props.createContractingPower(powerEditData, () => {
                    this.setState({
                        showModal: false,
                        showConfirmModal: false,
                        isUpdate: false,
                        powerData:null,
                        powerEditData: null,
                        isAdd: false,
                        formErrors: null
                    }, () => {
                        this.fetchAllContractingPowers();
                    })
                }, (err) => this.setState({showConfirmModal: false, powerEditData: null, isUpdate: false, formErrors: err})
            )
        } else {
            this.props.updateContractingPower(powerEditData, this.state.powerId, () => {
                this.setState({
                    showModal: false,
                    showConfirmModal: false,
                    isUpdate: false,
                    powerData: null,
                    powerEditData:null,
                    isAdd: false,
                    formErrors: false,
                }, () => {
                    this.fetchAllContractingPowers();
                });
            }, (err) => {
                this.setState({showConfirmModal: false, powerEditData: null, isUpdate: false, formErrors: err})
            })
        }
    };

    handleCancelClick = () => {
        this.setState({showConfirmModal: false})
    };

    toggleConfirmationModal = (data, isUpdate = false) => {
        this.setState({showConfirmModal: !this.state.showConfirmModal, powerEditData: data, isUpdate})
    };

    renderConfirmationModal = () => {
        let {isAdd} = this.state;

        let message = `Are you sure, you want to ${isAdd ? 'create' : 'update'} contracting power?`;

        return (
            <ConfirmationModal isOpen={this.state.showConfirmModal}
                               okCB={this.handleConfirmClick} cancelCB={this.handleCancelClick}
                               message={message}/>
        );
    };


    // Field Modal
    toggleModal = () => {
        this.setState({showModal: !this.state.showModal})
    };

    renderFieldsModal = () => {
        let headerText = this.state.isAdd ? "Create New Contracting Power" : "Update Contracting Power";
        let formProps = {
            isAdd: this.state.isAdd,
            createCB: (data) => this.toggleConfirmationModal(data),
            updateCB: (data) => this.toggleConfirmationModal(data,true),
            cancelCB: this.toggleModal,
            powerData: this.state.powerData,
            formErrors: this.state.formErrors
        };

        return (
            <div className={"col-md-12"}>
                <Modal isOpen={this.state.showModal} toggle={this.toggleModal}
                       style={{minWidth: "600px", width: "100%"}}>
                    <ModalHeader style={{border: "unset", WebkitTextStroke: "0.5px"}}>
                        {headerText}
                    </ModalHeader>

                    <ModalBody>
                        <CPForm {...formProps}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    };

    /*************************************** END ***********************************/

    /********************************* SORTING LAYOUTS ****************************/

    sortedParams = ["Name"];

    ascendingSort = (headerKey) => {
        if (!this.state.allPowers) return null;
        let arrayCopy = [...this.state.allPowers];
        arrayCopy.sort(compareAsc(headerKey));
        this.setState({allPowers: arrayCopy});
    };

    descendingSort = (headerKey) => {
        if (!this.state.allPowers) return null;
        let arrayCopy = [...this.state.allPowers];
        arrayCopy.sort(compareDesc(headerKey));
        this.setState({allPowers: arrayCopy});
    };

    getSortedLayout = (headerKey) => {
        if (this.sortedParams.includes(headerKey)) {
            return (
                <div>
                    {headerKey}
                    <span style={{
                        float: "right",
                        display: "grid",
                        marginRight: "20px",
                        height: "-webkit-fill-available"
                    }}>
                        <Icon type="caret-up" onClick={() => this.ascendingSort(headerKey.toLowerCase())}/>
                        <Icon type="caret-down" onClick={() => this.descendingSort(headerKey.toLowerCase())}/>
                    </span>
                </div>
            )
        } else {
            return headerKey;
        }
    };

    /*************************************** END ***********************************/

    getTitle = () => {
        return (
            <HeaderTitle>
                <h3>Contracting Powers</h3>
            </HeaderTitle>
        )
    };


    searchPower = (searchStr, searchParams = []) => {
        if (!this.props.contractingPowers) return null;
        const contractingPowers = [...this.props.contractingPowers];

        if (searchStr) {
            let filteredPowers = [];
            contractingPowers.forEach(powers => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(powers, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredPowers.push(powers);
                        break;
                    }
                }
            });
            this.setState({powers: filteredPowers});
        }
        else {
            this.setState({powers: null});
        }
    };

    getButtonList = () => {

        let buttonListProps = {
            buttonText: "+ Create Power Level",
            onCreateClick: () => this.setState({showModal: true, isAdd: true, formErrors: null}),
            onSearchChange: (e) => this.searchPower(e.target.value, searchFields)
        };
        return (
            <RowButtonsList {...buttonListProps}/>
        )
    };


    tableHeader = () => {
        let tableHeaders = ["Name", "Power", "Power with concurrence", "Edit Contracting Power"];

        let headerProps = {
            tableHeaders,
            getSortedLayout: (head) => this.getSortedLayout(head)
        };

        return (
            <SortableHeader {...headerProps}/>
        );
    };

    tableBody = () => {
        if (!this.state.allPowers)
            return null;

        let contractingPowers = this.state.powers ? this.state.powers : this.state.allPowers;

        let bodyJSX = contractingPowers.map((power, index) => {
            return (
                <tr key={index}>
                    <td>{power.name}</td>
                    <td>{formatAmount(power.power)}</td>
                    <td>{power.powerWithConcurrence !== -1 ? formatAmount(power.powerWithConcurrence) : "Full Power"}</td>
                    <td colSpan={2}>
                        <a onClick={() => this.setState({
                            showModal: true,
                            powerId: power.uuid,
                            powerData: power,
                            isAdd: false,
                            formErrors: null
                        })}>
                            Edit
                        </a>
                    </td>
                </tr>
            )
        });


        return (
            <tbody>
            {bodyJSX}
            </tbody>
        )
    };

    getContractingPowersData = () => {
        return (
            <TableContainer>
                {this.tableHeader()}
                {this.tableBody()}
            </TableContainer>
        )
    };


    render() {
        return (
            <>
                {this.renderConfirmationModal()}
                {this.renderFieldsModal()}
                {this.getTitle()}
                {this.getButtonList()}
                {this.props.processing ?

                    <NewLoader/>
                    :
                    this.getContractingPowersData()
                }
            </>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getAllContractingPowers: (cb) => dispatch(getAllContractingPowers(cb)),
        createContractingPower: (data, cb, errorCB) => dispatch(createContractingPower(data, cb, errorCB)),
        updateContractingPower: (data, id, cb, errorCB) => dispatch(updateContractingPower(data, id, cb, errorCB))
    }
};

const mapStateToProps = state => {
    return {
        contractingPowers: state.server_reducer.contractingPowers,
        processing: state.server_reducer.processing
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(ContractingPower);
export default connected;