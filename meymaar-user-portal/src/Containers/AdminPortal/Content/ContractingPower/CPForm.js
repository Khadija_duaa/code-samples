import React from 'react';
import {Input, InputNumber, Form, Button} from 'antd';
import {BUTTON_COLOR, isNumber} from "../../../../utils/common-utils";
import {getPowerNextParent} from "../../../../store/server/server-actions";
import {connect} from 'react-redux';

class CPForm extends React.Component {

    state = {
        nextParent: null,
    };

    componentDidMount() {
        this.props.getNextParent((parent) => {
            this.setState({nextParent: parent})
        });

        if (!this.props.isAdd) {
            this.props.form.setFieldsValue(this.props.powerData);
        }
    }

    // Handeling Submit Power
    handlePowerSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldValues) => {
            if (err) {
                return;
            }

            let {isAdd, createCB, updateCB, powerData} = this.props;

            let {nextParent} = this.state;

            if ((!nextParent && !this.props.form.getFieldValue('powerWithConcurrence')) || (!isAdd && powerData['powerWithConcurrence'] === -1)) {
                fieldValues.powerWithConcurrence = -1;
            }


            if (isAdd) {
                createCB && createCB(fieldValues);
            } else {
                updateCB && updateCB(fieldValues);
            }
        })
    };

    formItemLayout = {
        labelCol: {span: 12},
        wrapperCol: {span: 12},
    };

    buttonItemLayout = {
        wrapperCol: {offset: 12},
    };

    renderFormButtons = () => {
        let buttonText = this.props.isAdd ? "Create" : "Update";
        return (
            <Form.Item {...this.buttonItemLayout} >

                <Button htmlType="submit" style={{
                    height: "40px",
                    width: "40%",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>{buttonText}</Button>

                <Button style={{height: "40px", width: "40%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.cancelCB}>Cancel</Button>

            </Form.Item>

        )
    };

    /*************** Validatiion Fields *********************/
    validateNumber = (rules, value, callback) => {

        if (value && (!isNumber(value) || (isNumber(value) && value < 0))) {
            callback('Invalid Number!');
        }
        else {
            callback();
        }
    };

    /*************** Validatiion Fields *********************/

        // Rendering Power with concurrence Fields
    renderPowerWithConcurrence = () => {
        let {getFieldDecorator} = this.props.form;

        let {nextParent} = this.state;
        let {isAdd, powerData} = this.props;

        if (!nextParent || (!isAdd && powerData && powerData['powerWithConcurrence'] === -1)) {
            return <h6>Your Power with concurrence is FULL</h6>
        }

        let nextParentJSX = isAdd ? nextParent.name : powerData && powerData.parentContractualPower.name;
        return (

            <>
                <Form.Item label={"Power with Concurrence"} {...this.formItemLayout}>
                    {getFieldDecorator('powerWithConcurrence', {
                        initialValue: !isAdd && powerData ? powerData['powerWithConcurrence'] : null,
                        rules: [{
                            required: true,
                            message: "Required!",
                        }, {validator: this.validateNumber}]
                    })(<InputNumber min={0} style={{width: "100%"}}/>)}
                </Form.Item>

                <h6 style={{color: "red"}}> * Your Power must be lower than {nextParentJSX}</h6>
            </>
        )
    };


    render() {
        let {getFieldDecorator} = this.props.form;
        let {formErrors} = this.props;

        return (
            <div>
                {formErrors ? <h6 style={{color: "red", textAlign: "center"}}>{formErrors}</h6> : null}
                <Form layout={"horizontal"} onSubmit={this.handlePowerSubmit}>
                    <Form.Item label={"Name"} {...this.formItemLayout}>
                        {getFieldDecorator('name', {
                            rules: [{
                                required: true,
                                message: "Name is required",
                                whitespace: true
                            }]
                        })(<Input placeholder={"Name"}/>)}
                    </Form.Item>

                    <Form.Item label={"Power"} {...this.formItemLayout}>
                        {getFieldDecorator('power', {
                            rules: [{
                                required: true,
                                message: "Required!",
                            }, {validator: this.validateNumber}]
                        })(<InputNumber min={0} style={{width: "100%"}}/>)}
                    </Form.Item>

                    {this.renderPowerWithConcurrence()}
                    {this.renderFormButtons()}
                </Form>
            </div>
        )

    }
}

const mapDispatchToProps = dispatch => {
    return {
        getNextParent: (cb) => dispatch(getPowerNextParent(cb))
    }
};

const connected = connect(null, mapDispatchToProps)(CPForm);

export default Form.create()(connected);