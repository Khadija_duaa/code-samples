import React from 'react';
import {Input,  Form, Button} from 'antd';
import {BUTTON_COLOR} from "../../../../utils/common-utils";

class DomainForms extends React.Component {

    componentDidMount() {
        if (!this.props.isAdd) {
            this.props.form.setFieldsValue(this.props.domain);
        }
    }

    // Handeling Submit Department
    handleDomainSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldValues) => {
            if (err) {
                return;
            }

            let {isAdd, createCB, updateCB} = this.props;

            if (isAdd) {
                createCB && createCB(fieldValues);
            } else {
                updateCB && updateCB(fieldValues);
            }
        })
    };

    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };

    buttonItemLayout = {
        wrapperCol: {offset: 8},
    };


    renderFormButtons = () => {
        let buttonText = this.props.isAdd ? "Create" : "Update";
        return (
            <Form.Item {...this.buttonItemLayout} >

                <Button htmlType="submit" style={{
                    height: "40px",
                    width: "40%",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>{buttonText}</Button>

                <Button style={{height: "40px", width: "40%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.cancelCB}>Cancel</Button>

            </Form.Item>

        )
    };

    validateName = (rules, value, callback) => {
        let aplhaNumeric = /^[a-zA-Z]+$/i;
        if (value && !aplhaNumeric.test(value)) {
            callback('Value can be only aplhabetical');
        } else {
            callback();
        }
    };

    render() {
        let {getFieldDecorator} = this.props.form;
        let {formErrors} = this.props;
        return (
            <div>
                {formErrors ? <h6 style={{color: "red", textAlign: "center"}}>{formErrors}</h6> : null}
                <Form layout={"horizontal"} onSubmit={this.handleDomainSubmit}>
                    <Form.Item label={"Name"} {...this.formItemLayout}>
                        {getFieldDecorator('name', {
                            rules: [{
                                required: true,
                                message: "Name is required",
                                whitespace: true
                            }]
                        })(<Input placeholder={"Domain Name"}/>)}
                    </Form.Item>

                    <Form.Item label={"Short Code"} {...this.formItemLayout}>
                        {getFieldDecorator('shortCode', {
                            rules: [{
                                required: true,
                                message: "Short code is required",
                                whitespace: true
                            }, {validator: this.validateName}]
                        })(<Input placeholder={"Short Code"}/>)}
                    </Form.Item>


                    {this.renderFormButtons()}
                </Form>
            </div>
        )

    }
}


export default Form.create()(DomainForms);