import React from 'react';
import {createDomain, updateDomain} from "../../../../store/server/server-actions";
import {connect} from 'react-redux';
import {compareAsc, compareDesc, getObjectValue} from "../../../../utils/common-utils";
import NewLoader from '../../../Loader/NewLoader';
import {Icon} from 'antd';
import 'antd/dist/antd.css';
import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import HeaderTitle from "../../../../Components/HeaderTitle/HeaderTitle";
import TableContainer from "../../../../Components/Table/TableContainer/TableContainer";
import DomainForm from './DomainForms';
import SortableHeader from "../../../../Components/Table/SortableHeader/SortableHeader";
import RowButtonsList from "../../../../Components/Buttons/RowButtonList/RowButtonsList";
import {fetchAllDomains} from "../../../../utils/server-utils";
import ConfirmationModal from '../../../../Components/ModalFactory/MessageModal/ConfirmationModal';

const searchFields = ['name', 'shortCode'];

class Domain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            domains: null,
            showModal: false,
            isAdd: false,
            formErrors: null,
            domainData: null
        }
    };

    componentDidMount() {
        this.fetchAllDomains();
    }

    fetchAllDomains = () => {
        fetchAllDomains((domains) => {
            this.setState({allDomains: domains});
        });
    };


    /*************************************** MODAL ***********************************/

    handleConfirmClick = () => {
        let {isUpdate, domainData} = this.state;

        if (!isUpdate) {
            this.props.createDomain(domainData, () => {
                    this.setState({
                        showModal: false,
                        showConfirmModal: false,
                        isUpdate: false,
                        domainData: null,
                        isAdd: false,
                        formErrors: null
                    }, () => {
                        this.fetchAllDomains();
                    })
                }, (err) => this.setState({showConfirmModal: false, domainData: null, isUpdate: false, formErrors: err})
            )
        } else {
            this.props.updateDomain(domainData, this.state.domainId, () => {
                this.setState({
                    showModal: false,
                    showConfirmModal: false,
                    isUpdate: false,
                    domainData: null,
                    isAdd: false,
                    formErrors: false,
                }, () => {
                    this.fetchAllDomains();
                });
            }, (err) => {
                this.setState({showConfirmModal: false, domainData: null, isUpdate: false, formErrors: err})
            })
        }
    };

    handleCancelClick = () => {
        this.setState({showConfirmModal: false})
    };

    toggleConfirmationModal = (data, isUpdate = false) => {
        this.setState({showConfirmModal: !this.state.showConfirmModal, domainData: data, isUpdate})
    };

    renderConfirmationModal = () => {
        let {isAdd} = this.state;

        let message = `Are you sure, you want to ${isAdd ? 'create' : 'update'} domain?`;

        return (
            <ConfirmationModal isOpen={this.state.showConfirmModal}
                               okCB={this.handleConfirmClick} cancelCB={this.handleCancelClick}
                               message={message}/>
        );
    };


    // Field Modal
    toggleModal = () => {
        this.setState({showModal: !this.state.showModal})
    };

    renderFieldsModal = () => {
        let headerText = this.state.isAdd ? "Create New Domain" : "Update Domain";
        let formProps = {
            isAdd: this.state.isAdd,
            createCB: (data) => this.toggleConfirmationModal(data),
            updateCB: (data) => this.toggleConfirmationModal(data,true),
            cancelCB: this.toggleModal,
            domain: this.state.domain,
            formErrors: this.state.formErrors
        };

        return (
            <div className={"col-md-12"}>
                <Modal isOpen={this.state.showModal} toggle={this.toggleModal} style={{width: "100%"}}>
                    <ModalHeader style={{border: "unset", WebkitTextStroke: "0.5px"}}>
                        {headerText}
                    </ModalHeader>

                    <ModalBody>
                        <DomainForm {...formProps}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    };

    /*************************************** END ***********************************/

    /********************************* SORTING LAYOUTS ****************************/

    sortedParams = ["Name"];

    ascendingSort = (headerKey) => {
        if (!this.state.allDomains) return null;
        let arrayCopy = [...this.state.allDomains];
        arrayCopy.sort(compareAsc(headerKey));
        this.setState({allDomains: arrayCopy});
    };

    descendingSort = (headerKey) => {
        if (!this.state.allDomains) return null;
        let arrayCopy = [...this.state.allDomains];
        arrayCopy.sort(compareDesc(headerKey));
        this.setState({allDomains: arrayCopy});
    };

    getSortedLayout = (headerKey) => {
        if (this.sortedParams.includes(headerKey)) {
            return (
                <div>
                    {headerKey}
                    <span style={{
                        float: "right",
                        display: "grid",
                        marginRight: "20px",
                        height: "-webkit-fill-available"
                    }}>
                        <Icon type="caret-up" onClick={() => this.ascendingSort(headerKey.toLowerCase())}/>
                        <Icon type="caret-down" onClick={() => this.descendingSort(headerKey.toLowerCase())}/>
                    </span>
                </div>
            )
        } else {
            return headerKey;
        }
    };

    /*************************************** END ***********************************/

    getTitle = () => {
        return (
            <HeaderTitle>
                <h3>Domains</h3>
            </HeaderTitle>
        )
    };


    searchDomains = (searchStr, searchParams = []) => {
        if (!this.props.domains) return null;
        const domains = [...this.props.domains];

        if (searchStr) {
            let filteredDomains = [];
            domains.forEach(domain => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(domain, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredDomains.push(domain);
                        break;
                    }
                }
            });
            this.setState({domains: filteredDomains});
        }
        else {
            this.setState({domains: null});
        }
    };

    getButtonList = () => {

        let buttonListProps = {
            buttonText: "+ Create Domain",
            onCreateClick: () => this.setState({showModal: true, isAdd: true, formErrors: null}),
            onSearchChange: (e) => this.searchDomains(e.target.value, searchFields)
        };
        return (
            <RowButtonsList {...buttonListProps}/>
        )
    };


    tableHeader = () => {
        let tableHeaders = ["Name", "Short Code", "Edit Domain"];

        let headerProps = {
            tableHeaders,
            getSortedLayout: (head) => this.getSortedLayout(head)
        };

        return (
            <SortableHeader {...headerProps}/>
        );
    };

    tableBody = () => {
        if (!this.state.allDomains)
            return null;

        let domains = this.state.domains ? this.state.domains : this.state.allDomains;

        let bodyJSX = domains.map((domain, index) => {
            return (
                <tr key={index}>
                    <td>{domain.name}</td>
                    <td>{domain.shortCode}</td>
                    <td colSpan={2}>
                        <a onClick={() => this.setState({
                            showModal: true,
                            domainId: domain.uuid,
                            domain,
                            isAdd: false,
                            formErrors: null,
                        })}>
                            Edit
                        </a>
                    </td>
                </tr>
            )
        });


        return (
            <tbody>
            {bodyJSX}
            </tbody>
        )
    };

    getDomainData = () => {
        return (
            <TableContainer>
                {this.tableHeader()}
                {this.tableBody()}
            </TableContainer>
        )
    };


    render() {
        return (
            <>
                {this.renderConfirmationModal()}
                {this.renderFieldsModal()}
                {this.getTitle()}
                {this.getButtonList()}
                {this.props.processing ?

                    <NewLoader/>
                    :
                    this.getDomainData()
                }
            </>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        createDomain: (data, cb, errorCB) => dispatch(createDomain(data, cb, errorCB)),
        updateDomain: (data, id, cb, errorCB) => dispatch(updateDomain(data, id, cb, errorCB))
    }
};

const mapStateToProps = state => {
    return {
        domains: state.server_reducer.projectDomains,
        processing: state.server_reducer.processing
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(Domain);
export default connected;