import React from 'react';
import {
    getAllOfficeAuthorities,
    createOfficeAuthority,
    updateOfficeAuthority,
    getAllDepartments
} from "../../../../store/server/server-actions";
import {connect} from 'react-redux';
import {compareAsc, compareDesc, formatAmount, getObjectValue} from "../../../../utils/common-utils";
import NewLoader from '../../../Loader/NewLoader';
import {Icon} from 'antd';
import 'antd/dist/antd.css';
import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import HeaderTitle from "../../../../Components/HeaderTitle/HeaderTitle";
import TableContainer from "../../../../Components/Table/TableContainer/TableContainer";
import AuthorityForm from './OfficeAuthorityForm';
import SortableHeader from "../../../../Components/Table/SortableHeader/SortableHeader";
import RowButtonsList from "../../../../Components/Buttons/RowButtonList/RowButtonsList";
import ConfirmationModal from '../../../../Components/ModalFactory/MessageModal/ConfirmationModal';

const searchFields = ['name'];

class OfficeAuthority extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allAuthorities: null,
            showModal: false,
            isAdd: true,
            formErrors: null,
            authorityEditData:null,
            showConfirmModal:false,
            authorityData : null,
        }
    };

    componentDidMount() {
        this.props.fetchAllDepartments();
        this.fetchAllOfficeAuthorities();
    }

    fetchAllOfficeAuthorities = () => {
        this.props.getAllOfficeAuthorities((authorities) => {
            this.setState({allAuthorities: authorities});
        });
    };



    /*************************************** MODAL ***********************************/

    handleConfirmClick = () => {
        let {isUpdate, authorityEditData} = this.state;

        if (!isUpdate) {
            this.props.createOfficeAuthority(authorityEditData, () => {
                    this.setState({
                        showModal: false,
                        showConfirmModal: false,
                        isUpdate: false,
                        authorityEditData: null,
                        isAdd: false,
                        formErrors: null
                    }, () => {
                        this.fetchAllOfficeAuthorities();
                    })
                }, (err) => this.setState({showConfirmModal: false, authorityEditData: null, isUpdate: false, formErrors: err})
            )
        } else {
            this.props.updateOfficeAuthority(authorityEditData, this.state.authorityId, () => {
                this.setState({
                    showModal: false,
                    showConfirmModal: false,
                    isUpdate: false,
                    authorityEditData:null,
                    isAdd: false,
                    formErrors: false,
                }, () => {
                    this.fetchAllOfficeAuthorities();
                });
            }, (err) => {
                this.setState({showConfirmModal: false, authorityEditData: null, isUpdate: false, formErrors: err})
            })
        }
    };

    handleCancelClick = () => {
        this.setState({showConfirmModal: false})
    };

    toggleConfirmationModal = (data, isUpdate = false) => {
        this.setState({showConfirmModal: !this.state.showConfirmModal, authorityEditData: data, isUpdate})
    };

    renderConfirmationModal = () => {
        let {isAdd} = this.state;

        let message = `Are you sure, you want to ${isAdd ? 'create' : 'update'} office authority?`;

        return (
            <ConfirmationModal isOpen={this.state.showConfirmModal}
                               okCB={this.handleConfirmClick} cancelCB={this.handleCancelClick}
                               message={message}/>
        );
    };


    // Field Modal
    toggleModal = () => {
        this.setState({showModal: !this.state.showModal})
    };

    renderFieldsModal = () => {
        let headerText = this.state.isAdd ? "Create New Office Authority" : "Update Office Authority";
        let formProps = {
            isAdd: this.state.isAdd,
            createCB: (data) => this.toggleConfirmationModal(data),
            updateCB: (data) => this.toggleConfirmationModal(data,true),
            cancelCB: this.toggleModal,
            authorityData: this.state.authorityData,
            formErrors: this.state.formErrors,
            departments: this.props.departments,
            domains: this.props.domains
        };

        return (
            <div className={"col-md-12"}>
                <Modal isOpen={this.state.showModal} toggle={this.toggleModal}
                       style={{minWidth: "600px", width: "100%"}}>
                    <ModalHeader style={{border: "unset", WebkitTextStroke: "0.5px"}}>
                        {headerText}
                    </ModalHeader>

                    <ModalBody>
                        <AuthorityForm {...formProps}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    };

    /*************************************** END ***********************************/

    /********************************* SORTING LAYOUTS ****************************/

    sortedParams = ["Name"];

    ascendingSort = (headerKey) => {
        if (!this.state.allAuthorites) return null;
        let arrayCopy = [...this.state.allAuthorites];
        arrayCopy.sort(compareAsc(headerKey));
        this.setState({allAuthorites: arrayCopy});
    };

    descendingSort = (headerKey) => {
        if (!this.state.allAuthorites) return null;
        let arrayCopy = [...this.state.allAuthorites];
        arrayCopy.sort(compareDesc(headerKey));
        this.setState({allAuthorites: arrayCopy});
    };

    getSortedLayout = (headerKey) => {
        if (this.sortedParams.includes(headerKey)) {
            return (
                <div>
                    {headerKey}
                    <span style={{
                        float: "right",
                        display: "grid",
                        marginRight: "20px",
                        height: "-webkit-fill-available"
                    }}>
                        <Icon type="caret-up" onClick={() => this.ascendingSort(headerKey.toLowerCase())}/>
                        <Icon type="caret-down" onClick={() => this.descendingSort(headerKey.toLowerCase())}/>
                    </span>
                </div>
            )
        } else {
            return headerKey;
        }
    };

    /*************************************** END ***********************************/

    getTitle = () => {
        return (
            <HeaderTitle>
                <h3>Office Authorities</h3>
            </HeaderTitle>
        )
    };


    searchAuthority = (searchStr, searchParams = []) => {
        if (!this.props.officeAuthorities) return null;
        const officeAuthorities = [...this.props.officeAuthorities];

        if (searchStr) {
            let filteredAuthorities = [];
            officeAuthorities.forEach(authority => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(authority, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredAuthorities.push(authority);
                        break;
                    }
                }
            });
            this.setState({authorities: filteredAuthorities});
        }
        else {
            this.setState({authorities: null});
        }
    };

    getButtonList = () => {

        let buttonListProps = {
            buttonText: "+ Create Office Authority",
            onCreateClick: () => this.setState({showModal: true, isAdd: true,formErrors:null}),
            onSearchChange: (e) => this.searchAuthority(e.target.value, searchFields)
        };
        return (
            <RowButtonsList {...buttonListProps}/>
        )
    };


    tableHeader = () => {
        let tableHeaders = ["Name","Department","Domain","Contracting Power","Authorization Power", "Edit Authority"];

        let headerProps = {
            tableHeaders,
            getSortedLayout: (head) => this.getSortedLayout(head)
        };

        return (
            <SortableHeader {...headerProps}/>
        );
    };

    tableBody = () => {
        if (!this.state.allAuthorities)
            return null;

        let officeAuthorities = this.state.authorities ? this.state.authorities : this.state.allAuthorities;

        let bodyJSX = officeAuthorities.map((authority, index) => {
            return (
                <tr key={index}>
                    <td>{authority.name}</td>
                    <td>{authority.department?authority.department.name:"-"}</td>
                    <td>{authority.domain.name}</td>
                    <td>{authority.contractualPower?authority.contractualPower.name:"-"}</td>
                    <td>{authority.authorizationPower === -1?"Full Power":formatAmount(authority.authorizationPower)}</td>
                    <td colSpan={2}>
                        <a onClick={() => this.setState({
                            showModal: true,
                            authorityId: authority.uuid,
                            authorityData: authority,
                            isAdd: false,
                            formErrors:null,
                        })}>
                            Edit
                        </a>
                    </td>
                </tr>
            )
        });


        return (
            <tbody>
            {bodyJSX}
            </tbody>
        )
    };

    getOfficeAuthoritiesData = () => {
        return (
            <TableContainer>
                {this.tableHeader()}
                {this.tableBody()}
            </TableContainer>
        )
    };


    render() {
        return (
            <>
                {this.renderConfirmationModal()}
                {this.renderFieldsModal()}
                {this.getTitle()}
                {this.getButtonList()}
                {this.props.processing ?

                    <NewLoader/>
                    :
                    this.getOfficeAuthoritiesData()
                }
            </>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getAllOfficeAuthorities: (cb) => dispatch(getAllOfficeAuthorities(cb)),
        createOfficeAuthority: (data, cb, errorCB) => dispatch(createOfficeAuthority(data, cb, errorCB)),
        updateOfficeAuthority: (data, id, cb, errorCB) => dispatch(updateOfficeAuthority(data, id, cb, errorCB)),
        fetchAllDepartments: () => dispatch(getAllDepartments())
    }
};

const mapStateToProps = state => {
    return {
        officeAuthorities: state.server_reducer.officeAuthorities,
        processing: state.server_reducer.processing,
        departments: state.server_reducer.departments,
        domains: state.server_reducer.projectDomains
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(OfficeAuthority);
export default connected;