import React from 'react';
import {Input, InputNumber, Form, Button} from 'antd';
import {BUTTON_COLOR, isNumber} from "../../../../utils/common-utils";
import {fetchAuthorityPowers, getNextParentInfo} from "../../../../store/server/server-actions";
import {Select} from 'antd';
import {connect} from 'react-redux';

const Option = Select.Option;

class OfficeAuthorityForm extends React.Component {

    state = {
        nextParentInfo: null,
        department: null,
        domainDisabled: !this.props.isAdd && this.props.authorityData.authorizationPower !== -1,
        departmentDisabled : !this.props.isAdd
    };


    formItemLayout = {
        labelCol: {span: 10},
        wrapperCol: {span: 14},
    };

    buttonItemLayout = {
        wrapperCol: {offset: 10},
    };


    componentDidMount() {

        if (!this.props.isAdd && this.props.authorityData) {

            let {authorityData} = this.props;
            // Fetching Authority Powers
            this.props.getAuthorityPowers(authorityData.uuid,(data)=>{
                this.setState({nextParentInfo:data})
            });

            this.props.form.setFieldsValue({name: authorityData['name']});

            this.props.form.setFieldsValue({departmentId: authorityData['department'].uuid});
            this.props.form.setFieldsValue({domainId: authorityData['domain'].uuid});
            if (authorityData['contractualPower']) {
                this.props.form.setFieldsValue({contractualPowerId: authorityData['contractualPower'].uuid})
            }

            if (authorityData['authorizationPower'] !== -1) {
                this.props.form.setFieldsValue({authorizationPower: authorityData['authorizationPower']})
            }
        }
    }


    /*************** Validatiion Fields *********************/
    validateNumber = (rules, value, callback) => {

        if (value && (!isNumber(value) || (isNumber(value) && value < 0))) {
            callback('Invalid Number!');
        }
        else {
            callback();
        }
    };

    /*************** End *********************/


    /***************************** Events *************************/

    fetchNextParentInfo = () => {
        let departmentId = this.props.form.getFieldValue('departmentId');
        let domainId = this.props.form.getFieldValue('domainId');
        if (domainId && departmentId) {
            this.props.getNextParentInfo(departmentId, domainId, (parentInfo) => {
                this.setState({nextParentInfo: parentInfo})
            });
        }
    };


    // Handeling Submit Office Authorities
    handleAuthoritySubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldValues) => {
            if (err) {
                return;
            }

            let {isAdd, createCB, updateCB} = this.props;


            if (!this.props.form.getFieldValue('authorizationPower')) {
                fieldValues.authorizationPower = -1;
            }


            if (isAdd) {
                createCB && createCB(fieldValues);
            } else {
                updateCB && updateCB(fieldValues);
            }
        })
    };


    handleDepartmentChange = (id) => {
        let department = this.props.departments.filter(dept => dept.uuid === id);

        this.setState({department}, this.fetchNextParentInfo);

    };

    //Rendering Departments
    renderDepartments = () => {
        let {departments, form} = this.props;

        let departmentsOptions = departments.map((dept, index) => {
            return (
                <Option key={index} value={dept.uuid}>{dept.name}</Option>
            )
        });

        return (
            <Form.Item label={"Departments"} {...this.formItemLayout}>
                {
                    form.getFieldDecorator('departmentId', {
                        rules: [{required: true, message: "Required!"}]
                    })(<Select disabled={this.state.departmentDisabled} placeholder={"Select Department"} onChange={this.handleDepartmentChange}>
                        {departmentsOptions}
                    </Select>)
                }
            </Form.Item>
        )
    };

    // Handeling Domain Change and fetch Next Parent Info
    handleDomainChange = (id) => {
        let domains = this.props.domains.filter(domain => domain.uuid === id);
        this.setState({domains}, this.fetchNextParentInfo);
    };

    //Rendering Departments
    renderDomains = () => {
        let {domains, form} = this.props;

        let domainsOptions = domains.map((domain, index) => {
            return (
                <Option key={index} value={domain.uuid}>{domain.name}</Option>
            )
        });

        return (
            <Form.Item label={"Domains"} {...this.formItemLayout}>
                {
                    form.getFieldDecorator('domainId', {
                        rules: [{required: true, message: "Required!"}]
                    })(<Select placeholder={"Select Domain"} disabled={this.state.domainDisabled}
                               onChange={this.handleDomainChange}>
                        {domainsOptions}
                    </Select>)
                }
            </Form.Item>
        )
    };


    // Rendering Form Buttons
    renderFormButtons = () => {
        let buttonText = this.props.isAdd ? "Create" : "Update";
        return (
            <Form.Item {...this.buttonItemLayout} >

                <Button htmlType="submit" style={{
                    height: "40px",
                    width: "40%",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>{buttonText}</Button>

                <Button style={{height: "40px", width: "40%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.cancelCB}>Cancel</Button>

            </Form.Item>

        )
    };

    renderContractualPowers = () => {
        let {nextParentInfo} = this.state;
        let {isAdd, authorityData} = this.props;


        if (!nextParentInfo && isAdd) return null;

        if ((isAdd && !nextParentInfo.contractualPowers.length) || (!isAdd && authorityData && !authorityData['contractualPower'])) return null;

        let powerOptionsJSX = nextParentInfo && nextParentInfo.contractualPowers.map((power, index) => {
            return (
                <Option key={index} value={power.uuid}>{power.name}</Option>
            )
        });

        return (
            <Form.Item label={"Contractual Powers"} {...this.formItemLayout}>
                {
                    this.props.form.getFieldDecorator('contractualPowerId', {
                        rules: [{required: true, message: "Required!"}]
                    })(<Select placeholder={"Select Contractual Power"}>
                        {powerOptionsJSX}
                    </Select>)
                }
            </Form.Item>
        )

    };

    // Rendering Power with concurrence Fields
    renderAuthorizationPower = () => {
        let {getFieldDecorator} = this.props.form;

        let {nextParentInfo} = this.state;
        let {isAdd, authorityData} = this.props;

        if (!nextParentInfo && isAdd) return null;


        if ((isAdd && !nextParentInfo.officeAuthority) || (!isAdd && authorityData && authorityData['authorizationPower'] === -1)) {
            return <h6>Your Authorization Power is FULL</h6>
        }

        let nextParentJSX = isAdd ? nextParentInfo.officeAuthority.name : authorityData && authorityData.parentAuthority.name;
        return (

            <>
                <Form.Item label={"Authorization Power"} {...this.formItemLayout}>
                    {getFieldDecorator('authorizationPower', {
                        initialValue: !isAdd ? authorityData && authorityData['authorizationPower'] : null,
                        rules: [{
                            required: true,
                            message: "Required!",
                        }, {validator: this.validateNumber}]
                    })(<InputNumber min={0} style={{width: "100%"}}/>)}
                </Form.Item>

                <h6 style={{color: "red"}}> * Your Power must be lower than {nextParentJSX}</h6>
            </>
        )
    };


    renderForm = () => {
        let {getFieldDecorator} = this.props.form;
        return (
            <Form layout={"horizontal"} onSubmit={this.handleAuthoritySubmit}>
                <Form.Item label={"Name"} {...this.formItemLayout}>
                    {getFieldDecorator('name', {
                        rules: [{
                            required: true,
                            message: "Name is required",
                            whitespace: true
                        }]
                    })(<Input placeholder={"Name"}/>)}
                </Form.Item>
                {this.renderDepartments()}
                {this.renderDomains()}
                {this.renderContractualPowers()}
                {this.renderAuthorizationPower()}
                {this.renderFormButtons()}
            </Form>
        )
    };


    /***************************** END *************************/

    render() {

        let {formErrors} = this.props;

        return (
            <div>
                {formErrors ? <h6 style={{color: "red", textAlign: "center"}}>{formErrors}</h6> : null}
                {this.renderForm()}
            </div>
        )

    }
}

const mapDispatchToProps = dispatch => {
    return {
        getNextParentInfo: (departmentId, domainId, cb) => dispatch(getNextParentInfo(departmentId, domainId, cb)),
        getAuthorityPowers : (authorityId,cb) => dispatch(fetchAuthorityPowers(authorityId,cb))
    }
};

const connected = connect(null, mapDispatchToProps)(OfficeAuthorityForm);

export default Form.create()(connected);