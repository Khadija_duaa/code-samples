import React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {fetchAllAdmins} from "../../../../store/server/server-actions";
import moment from "moment/moment";
import {formatName, formatPhoneNumber, getObjectValue} from "../../../../utils/common-utils";
import NewLoader from '../../../Loader/NewLoader';
import TableContainer from "../../../../Components/Table/TableContainer/TableContainer";
import HeaderTitle from "../../../../Components/HeaderTitle/HeaderTitle";
import RowButtonsList from "../../../../Components/Buttons/RowButtonList/RowButtonsList";


const searchFields = ['armyNumber', 'cnic', 'username', 'phoneNumber', 'firstName', 'dob'];

class Admins extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            admins: null
        }
    };


    componentDidMount() {
        this.props.getAllAdmins();
    }

    /************************* Events *************************/



    searchUsers = (searchStr, searchParams = []) => {
        if(!this.props.admins) return null;
        const admins = [...this.props.admins];

        if (searchStr) {
            let filteredUsers = [];
            admins.forEach(projectUser => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(projectUser, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredUsers.push(projectUser);
                        break;
                    }
                }
            });


            this.setState({admins: filteredUsers});
        }
        else {
            this.setState({admins: null});
        }
    };


    /**********************************************************/


    tableHeader = () => {
        let tableHeaders = ["Service #", "CNIC", "Name", "Username", "Contact #", "DOB"];

        let headerJSX = tableHeaders.map((head, index) => {
            return (
                index < tableHeaders.length - 1 ?
                    <th key={index} scope={"col"}>{head}</th>
                    :
                    <th key={index} scope={"col"} colSpan={"2"}>{head}</th>
            )
        });

        return (
            <thead className="thead-dark">
            <tr>
                {headerJSX}
            </tr>
            </thead>
        )
    };

    tableBody = () => {

        if (!this.props.admins)
            return null;

        let adminUsers = this.state.admins ? this.state.admins : this.props.admins;


        let bodyJSX = adminUsers.map((user, index) => {
            return (
                <tr key={index}>
                    <td>{user.armyNumber}</td>
                    <td>{user.cnic}</td>
                    <td>{formatName(user)}</td>
                    <td>{user.username}</td>
                    <td>{formatPhoneNumber(user.phoneNumber, 'PK')}</td>
                    <td colSpan="2">{moment(user.dob).format("DD-MM-YYYY")}</td>
                </tr>
            )
        });

        return (
            <tbody>
            {bodyJSX}
            </tbody>
        );
    };


    getTitle = () => {
        return (
            <HeaderTitle>
                <h3>Admins</h3>
            </HeaderTitle>
        )
    };

    getButtonList = () => {

        let buttonListProps = {
            buttonText: "+ Create an Admin",
            onCreateClick: () => this.props.history.push('/dashboard/admin/create'),
            onSearchChange : (e) => this.searchUsers(e.target.value, searchFields)
        };
        return (
            <RowButtonsList {...buttonListProps}/>
        )
    };

    getAdminData = () => {

        return (
            <TableContainer>
                {this.tableHeader()}
                {this.tableBody()}
            </TableContainer>
        )
    };

    render() {
        return (
            <>
                {this.getTitle()}
                {this.getButtonList()}
                {this.props.processing ?

                    <NewLoader/>
                    :
                    this.getAdminData()
                }
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAllAdmins: () => dispatch(fetchAllAdmins())
    }
};


const mapStateToProps = state => {
    return {
        processing: state.server_reducer.processing,
        projectDomains: state.server_reducer.projectDomains,
        admins: state.server_reducer.admins
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Admins);

export default withRouter(connected)