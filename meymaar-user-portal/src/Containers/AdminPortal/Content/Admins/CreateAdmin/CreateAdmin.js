import React from 'react';
import UserJSONForm from '../../../../../Components/AdminPortal/Form/UserJSONForm'
import {createUserJSON} from "../../../../../utils/utilsJSON";
import {withRouter} from 'react-router-dom';
import BreadCrumb from '../../../../../Components/BreadCrumb/BreadCrumb';

class CreateAdmin extends React.Component {

    getBreadCrumbs = ()=>{

        let breadCrumbsJSON = [
            {
                title:"Admins",
                returnPath:'/dashboard/admin'
            },
            {
                title:`Create Admin`
            }
        ];
        return (
            <BreadCrumb className={"form-crumb"} style={{fontSize:"20px"}} crumbsJSON={breadCrumbsJSON}/>
        )
    };


    render() {
        let formProps = {
            formJSON: createUserJSON,
            submitURL: "/user-management/admins",
            headerText: "Create Admin",
            bodyHeaderText: "",
            successText:"Admin has created successfully",
            isSuperAdmin: false
        };


        return (
            <div className={"container-fluid"}>
                {this.getBreadCrumbs()}
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <UserJSONForm successCB={()=>this.props.history.push('/dashboard/admin')} {...formProps}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(CreateAdmin);