import React from 'react';
import {withRouter} from 'react-router-dom';
import {Form, Button, Select} from 'antd';
import 'antd/dist/antd.css';
import {connect} from "react-redux";
import {assignRoleToUser} from "../../../../../store/server/server-actions";
import {BUTTON_COLOR} from "../../../../../utils/common-utils";
import {
    getAllSubOfficesOptions,
    getDepartmentOffices,
    getDepartmentOptions,
    getRolesByOfficeOptions
} from "../../../../../utils/server-utils";

import '../../../../../Components/AdminPortal/Form/formStyle.css';
import NewLoader from '../../../../Loader/NewLoader';


const Option = Select.Option;


class AssignOffice extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            departmentOptions: null,
            departmentId: null,
            headOffices: null,
            officeRoles: null,
            subOffices: null
        }
    }

    componentDidMount() {
        getDepartmentOptions((departments) => this.setState({departmentOptions: departments}));
    }

    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };

    /******************* Departments *****************/

    handleDepartmentChange = (id) => {
        getDepartmentOffices(id, (offices) => {
            this.setState({departmentId: id, headOffices: offices}, () => {
                this.props.form.resetFields(['headOffice', 'roleId']);
                this.setState({officeRoles: null})
            })
        });
    };

    renderDepartments = () => {
        let {departmentOptions} = this.state;

        let jsx = null;
        if (departmentOptions) {
            jsx = departmentOptions.map((department, index) => {
                return (
                    <Option key={index} value={department.uuid}>{department.name}</Option>
                )
            });

            return jsx;
        }
        return jsx;
    };

    getDepartments = () => {
        let {getFieldDecorator} = this.props.form;
        return (
            <Form.Item {...this.formItemLayout} label={"Department"}>

                {
                    getFieldDecorator(`departmentId`, {
                        rules: [{required: true, message: 'Department is required'}],
                    })(
                        <Select size={"default"} id={"department"} className={"department"}
                                placeholder={"Select Department..."} onChange={this.handleDepartmentChange}>
                            {this.renderDepartments()}
                        </Select>
                    )
                }

            </Form.Item>
        )
    };

    /******************* End *****************/


    /********************* Domains *******************/
    handleDomainChange = () => {
        this.props.form.resetFields(['headOffice', 'subOffice', 'roleId']);
    };


    renderDomains = () => this.props.domains.map((domain, index) => {
        return (
            <Option key={index} value={domain.uuid}>{domain.name}</Option>
        )
    });


    getDomains = () => {
        let {getFieldDecorator} = this.props.form;
        return (

            <Form.Item {...this.formItemLayout} label={"Domain"}>

                {
                    getFieldDecorator(`domainId`, {
                        rules: [{required: true, message: 'Domain is required'}],
                    })(
                        <Select size={"default"} id={"domain"} className={"domain"}
                                placeholder={"Select Domain..."} onChange={this.handleDomainChange}>
                            {this.renderDomains()}
                        </Select>
                    )
                }

            </Form.Item>

        )
    };

    /********************* Offices *******************/

    handleOfficeChange = (id) => {

        this.props.form.resetFields(['subOffice', 'roleId']);

        getAllSubOfficesOptions(id, (subOffices) => {
            this.setState({subOffices})
        });

        getRolesByOfficeOptions(id, (designations) => {
            this.setState({officeRoles: designations, officeId: id})
        });
    };

    renderOffices = () => {

        let {headOffices} = this.state;
        let {getFieldValue} = this.props.form;
        let jsx = null;

        if (headOffices && getFieldValue('domainId')) {
            headOffices = headOffices.filter(office => {
                console.log(office);
                let {officeAuthority} = office;
                return officeAuthority.domain && officeAuthority.domain.uuid === getFieldValue('domainId');
            });


            jsx = headOffices.map((office, index) => {
                return (
                    <Option key={index} value={office.uuid}>{office.name}</Option>
                )
            });
            return jsx;
        }


        return jsx;
    };

    getOffices = () => {
        let {getFieldDecorator} = this.props.form;
        return (
            <Form.Item {...this.formItemLayout} label={"Office"}>

                {
                    getFieldDecorator(`headOffice`, {
                        rules: [{required: true, message: 'Office is required'}],
                    })(
                        <Select size={"default"} id={"office"} className={"office"}
                                placeholder={"Select Office..."} onChange={this.handleOfficeChange}>
                            {this.renderOffices()}
                        </Select>
                    )
                }

            </Form.Item>
        )
    };

    /********************* End *******************/

    /********************* Offices *******************/

    handleSubOfficeChange = (id) => {
        getRolesByOfficeOptions(id, (designations) => {
            this.setState({officeRoles: designations, officeId: id}, () => {
                this.props.form.resetFields('roleId');
            })
        });
    };

    renderSubOffices = () => {

        let {subOffices} = this.state;
        let jsx = null;

        if (subOffices) {
            jsx = subOffices.map((office, index) => {
                return (
                    <Option key={index} value={office.uuid}>{office.name}</Option>
                )
            });
            return jsx;
        }


        return jsx;
    };

    getSubOffices = () => {
        let {getFieldDecorator, getFieldValue} = this.props.form;
        if (!getFieldValue('headOffice')) return null;

        return (
            <Form.Item {...this.formItemLayout} label={"Sub Office"}>

                {
                    getFieldDecorator(`subOffice`,)(
                        <Select size={"default"} id={"subOffice"} className={"subOffice"}
                                placeholder={"Select Sub Office..."} onChange={this.handleSubOfficeChange}>
                            {this.renderSubOffices()}
                        </Select>
                    )
                }
            </Form.Item>
        )
    };

    /********************* End *******************/


    /************************* Roles ************************/

    renderRoles = () => {

        let {officeRoles} = this.state;

        let jsx = null;
        if (officeRoles) {

            jsx = officeRoles.map((role, index) => {
                return (
                    <Option key={index} value={role.uuid}>{role.name}</Option>
                )
            });

            return jsx;
        }
        return jsx;
    };

    getUserRoles = () => {
        let {getFieldDecorator} = this.props.form;
        return (
            <Form.Item {...this.formItemLayout} label={"Appointment"}>

                {
                    getFieldDecorator(`roleId`, {
                        rules: [{required: true, message: 'Appointment is required'}],
                    })(
                        <Select size={"default"} id={"role"} className={"role"}
                                placeholder={"Select Appointment..."}>
                            {this.renderRoles()}
                        </Select>
                    )
                }

            </Form.Item>

        )
    };

    /************************* END ************************/

    /******************* Buttons ***********/
    renderButtons = () => {

        const buttonItemLayout = {
            wrapperCol: {offset: 8},
        };


        return (
            <Form.Item {...buttonItemLayout}>
                <Button htmlType="submit" style={{
                    height: "40px",
                    width: "45%",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>Assign</Button>
                <Button style={{height: "40px", width: "45%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.toggleCB}>Cancel</Button>
            </Form.Item>
        )
    };
    /******************* End ***********/

    /**************************Form Submissions ******************/
    handleAssignSubmit = (e) => {
        e.preventDefault();

        let successMessage = "User has appointed to office successfully";

        this.props.form.validateFields((err, fieldsValue) => {

            if (err) {
                return;
            }

            let {userId} = this.props;
            let roleId = this.props.form.getFieldValue('roleId');

            this.props.assignRole(userId, roleId,
                () => {
                    this.props.form.resetFields();
                    this.props.toggleCB() && this.props.toggleCB();
                },
                (errors) => {


                    errors.forEach(errorField => {
                        this.props.form.setFields({
                            [errorField.field]: {
                                value: fieldsValue[errorField.field],
                                errors: [new Error(`${errorField.error}`)],
                            }
                        });
                    });
                },
                successMessage
            );

        })
    };

    /************************** END ******************************/



    render() {
        if (this.props.processing) return <NewLoader/>;

        return (
            <div className={"col-md-12"} style={{padding: "unset"}}>
                <Form layout={"horizontal"} onSubmit={this.handleAssignSubmit}>

                    {this.getDepartments()}
                    {this.getDomains()}
                    {this.getOffices()}
                    {this.getSubOffices()}
                    {this.getUserRoles()}
                    {this.renderButtons()}
                </Form>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        formErrors: state.server_reducer.fieldErrors,
        processing: state.server_reducer.processing,
        domains: state.server_reducer.projectDomains
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        assignRole: (userId, roleId, successCB, errorCB, successText) => dispatch(assignRoleToUser(userId, roleId, successCB, errorCB, successText))
    }
};

const formCreated = Form.create()(AssignOffice);
const connected = connect(mapStateToProps, mapDispatchToProps)(formCreated);
export default withRouter(connected);