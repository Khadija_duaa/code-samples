import React from 'react';
import UserJSONForm from '../../../../../Components/AdminPortal/Form/UserJSONForm'
import BreadCrumb from '../../../../../Components/BreadCrumb/BreadCrumb';
import {createUserJSON} from "../../../../../utils/utilsJSON";
import {withRouter} from 'react-router-dom';

class CreateUser extends React.Component {

    state = {
        user:null
    };

    isEditState = () => {
        const {location} = this.props;
        let userState = null;
        if(location && location.state){
            userState =  location.state;
        }
        return userState;
    };

   componentWillMount(){
       let userState = this.isEditState();
       if(!userState){
           this.props.history.push('/dashboard/users');
       }
       else if(userState.user){
           let user = userState.user;
           this.setState({user});
       }
   }

    getBreadCrumbs = ()=>{

        let breadCrumbsJSON = [
            {
                title:"Users",
                returnPath:'/dashboard/users'
            },
            {
                title:`${this.state.user?'Update':'Create'} User`
            }
        ];


        return (
            <BreadCrumb className={"form-crumb"} style={{fontSize:"20px"}} crumbsJSON={breadCrumbsJSON}/>
        )
    };


    render() {
        let formProps = {
            formJSON: createUserJSON,
            submitURL: "/user-management/users",
            headerText: "Create User",
            bodyHeaderText: "",
            successText: "User has created successfully",
            isSuperAdmin: false
        };

        let dob = '';
        let userState = this.state.user;
        if(userState){
            dob = userState['dob'];
        }

        return (
            <div className={"container-fluid"}>
                {this.getBreadCrumbs()}
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <UserJSONForm dob={dob} user={userState}
                                   successCB={() => this.props.history.push('/dashboard/users')} {...formProps}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(CreateUser);