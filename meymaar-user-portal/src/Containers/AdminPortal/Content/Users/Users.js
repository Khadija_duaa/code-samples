import React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {fetchAllUsers} from "../../../../store/server/server-actions";
import {formatName, formatPhoneNumber, getObjectValue} from "../../../../utils/common-utils";
import moment from 'moment';

import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import AssignOffice from './AssignOffice/AssignOffice';
import NewLoader from '../../../Loader/NewLoader';

import {Select} from 'antd';
import HeaderTitle from "../../../../Components/HeaderTitle/HeaderTitle";
import TableContainer from "../../../../Components/Table/TableContainer/TableContainer";

const Option = Select.Option;

const searchFields = ['armyNumber', 'firstName', 'lastName', 'cnic', 'username', 'phoneNumber', 'dob'];


class Users extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            users: null,
            showModal: false,
            userId: null,
            visible: true
        }
    };

    componentDidMount() {
        this.props.getAllUsers(2000);
    }


    /************************* Events *************************/

    handleSelectChange = (values, event) => {
        // let value = e.target.value;
        if (values !== "All") {
            this.props.getAllUsers(10, values);
        } else {
            this.props.getAllUsers(2000);
        }
    };

    searchUsers = (searchStr, searchParams = []) => {
        if(!this.props.users) return null;
        const users = [...this.props.users];

        if (searchStr) {
            let filteredUsers = [];
            users.forEach(projectUser => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(projectUser, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredUsers.push(projectUser);
                        break;
                    }
                }
            });


            this.setState({users: filteredUsers});
        }
        else {
            this.setState({users: null});
        }
    };

    /**********************************************************/


    /************************* Assigning Office Form Modal *************************/

    toggleModal = () => {
        this.setState({showModal: !this.state.showModal});
    };

    assignOfficeModal = () => {

        return (
            <div className={"col-md-12"}>
                <Modal isOpen={this.state.showModal} toggle={this.toggleModal}
                       style={{minWidth: "550px", width: "100%"}}>
                    <ModalHeader style={{border: "unset", WebkitTextStroke: "0.5px"}}>
                        Assign Office to {this.state.name}
                    </ModalHeader>

                    <ModalBody>
                        <AssignOffice userId={this.state.userId} toggleCB={() => this.toggleModal()}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    };


    /************************* END *************************/

    getHeaderSection = () => {

        const {users, projectDomains} = this.props;
        if (!users)
            return null;

        let headerJSON = [
            {
                title: "Total Users",
                value: users.length
            },
            {
                title: "Domains",
                value: projectDomains.length
            },
            {
                title: "Offices",
                value: "-"
            }
        ];

        let headerDetailsJSX = headerJSON.map((item, index) => {

            return (
                <div key={index} className="row no-gutters" style={index < 1 ? {borderTop: "unset"} : null}>
                    <div className="col-md-8 dashboard-detail-box-title">{item.title}</div>
                    <div className="col-md-4 dashboard-detail-box-value">{item.value}</div>
                </div>
            )
        });

        return (
            <div className="container-fluid mb-5">
                <div className="row">
                    <div className="col-md-12">
                        <div className="dashboard-show-hide-section-container">
                            <div className={`padding-bottom-2x p-5 dashboard-show-hide-section`}
                                 style={this.state.visible ? {display: "block"} : {display: "none"}}>
                                <div className="row">
                                    <div className="col-md-12" style={{marginBottom: "30px"}}>
                                        <h3>Users Detail</h3>
                                    </div>
                                </div>
                                <div className="row align-items-center">

                                    <div className="col-md-5 dashboard-detail-box">
                                        {headerDetailsJSX}
                                    </div>


                                    <div className="col-md-7">
                                        <div id="donutchart" style={{height: "auto"}}/>
                                    </div>

                                </div>
                            </div>

                            {/*<div className="dashboard-show-hide-button" onClick={() => this.setState({visible: !this.state.visible})}>*/}
                            {/*<div className="button-section-close"*/}
                            {/*style={this.state.visible ? {display: "block"} : {display: "none"}}>*/}
                            {/*<span style={{fontSize:"14px"}}>/\</span>*/}
                            {/*</div>*/}

                            {/*<div className="button-section-open dashboard-show-hide-button-hidden "*/}
                            {/*style={this.state.visible ? {display: "none"} : {display: "block"}}>*/}
                            {/*<span style={{fontSize:"14px"}}>\/</span>*/}
                            {/*</div>*/}
                            {/*</div>*/}

                        </div>
                    </div>
                </div>
            </div>
        )
    };


    tableHeader = () => {
        let tableHeaders = ["Service #", "CNIC", "Name", "Username", "Contact #", "DOB", "Action"];

        let headerJSX = tableHeaders.map((head, index) => {
            return (
                index < tableHeaders.length - 1 ?
                    <th key={index} scope={"col"}>{head}</th>
                    :
                    <th key={index} scope={"col"} colSpan={"2"}>{head}</th>
            )
        });

        return (
            <thead className="thead-dark">
            <tr>
                {headerJSX}
            </tr>
            </thead>
        )
    };

    tableBody = () => {

        let users = this.state.users ? this.state.users : this.props.users;


        let bodyJSX = users.map((user, index) => {
            let {history} = this.props;
            let historyProps = {pathname: "/dashboard/users/create", state: {user}};

            return (
                <tr key={index} style={{cursor: "pointer"}}>
                    <td onClick={() => history.push(historyProps)}>{user.armyNumber}</td>
                    <td onClick={() => history.push(historyProps)}>{user.cnic}</td>
                    <td onClick={() => history.push(historyProps)}>{formatName(user)}</td>
                    <td onClick={() => history.push(historyProps)}>{user.username}</td>
                    <td onClick={() => history.push(historyProps)}>{formatPhoneNumber(user.phoneNumber, 'PK')}</td>
                    <td onClick={() => history.push(historyProps)}>{moment(user.dob).format("DD-MM-YYYY")}</td>
                    <td colSpan="2"><a href={"javascript:;"}
                                       onClick={() => this.setState({
                                           showModal: true,
                                           userId: user.uuid,
                                           name: formatName(user)
                                       })}>
                        Assign Office </a></td>
                </tr>
            )
        });

        return (
            <tbody>
            {bodyJSX}
            </tbody>
        )
    };


    getTitle = () => {
        return (
            <HeaderTitle>
                <h3>Users</h3>
            </HeaderTitle>
        )
    };

    getButtonList = () => {
        let {projectDomains} = this.props;
        let dropdownJSX = projectDomains && projectDomains.map((domain, index) => {

            return (
                <Option key={index + 1} value={domain.uuid}>{domain.name}</Option>
            )
        });

        dropdownJSX.unshift(<option key={0} value={"All"}>All Domains</option>);

        let historyProps = {pathname: "/dashboard/users/create", state: {add: true}};

        return (
            <div className="container-fluid">
                <div className="row button-list align-items-baseline">
                    <div className="col-md-4">
                        <button className="custom-button custom-button-dark create-button"
                                onClick={() => this.props.history.push(historyProps)}>
                            + Add a User
                        </button>
                    </div>


                    <div className="col-md-8 text-right  margin-top-56">

                        {/* <!--dropdown-button--> */}
                        <div className="dropdown-button">
                            <Select className="padding-top-22 new-add-assign-select"
                                    onChange={(values, event) => this.handleSelectChange(values, event)}
                                    placeholder="Projects"
                                    defaultValue={"All"}
                                    size="large">
                                {dropdownJSX}
                            </Select>
                        </div>


                        <div className="search-bar">
                            <form className=" new-btn-with-gradient-select search-container">
                                <input type="text" id="search-bar" placeholder="Search…"
                                       onChange={(e) => this.searchUsers(e.target.value, searchFields)}/>
                                <a><i className="fas fa-search"/></a>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        )
    };

    getUserData = () => {

        if (!this.props.users)
            return null;
        return (
            <TableContainer>
                {this.tableHeader()}
                {this.tableBody()}
            </TableContainer>
        )
    };

    render() {


        return (
            <>
                {this.assignOfficeModal()}
                {this.getHeaderSection()}
                {this.getTitle()}
                {this.getButtonList()}
                {this.props.processing ?

                    <NewLoader/>
                    :
                    this.getUserData()
                }
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAllUsers: (pageSize = 10, domainId = null) => dispatch(fetchAllUsers(pageSize, domainId))
    }
};

const mapStateToProps = state => {
    return {
        projectDomains: state.server_reducer.projectDomains,
        users: state.server_reducer.users,
        processing: state.server_reducer.processing
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Users);

export default withRouter(connected)