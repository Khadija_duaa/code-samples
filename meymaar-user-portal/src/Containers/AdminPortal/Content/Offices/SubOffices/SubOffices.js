import React from 'react';
import {Icon} from 'antd';
import {withRouter} from 'react-router-dom';


class SubOffices extends React.Component {

    sortedParams = [""];
    getSortedLayout = (headerKey) => {
        if (this.sortedParams.includes(headerKey)) {
            return (
                <div>
                    {headerKey}
                    <span style={{
                        float: "right",
                        display: "inline-grid",
                        marginRight: "20px",
                        height: "-webkit-fill-available"
                    }}>
                        <Icon type="caret-up" onClick={() => this.ascendingSort(headerKey.toLowerCase())}/>
                        <Icon type="caret-down" onClick={() => this.descendingSort(headerKey.toLowerCase())}/>
                    </span>
                </div>
            )
        } else {
            return headerKey;
        }
    };

    tableHeader = () => {
        let tableHeaders = ["Name", "Department", "Office Authority", "Reporting Office", "Domain", "Station", "Action"];

        let headerJSX = tableHeaders.map((head, index) => {
            return (
                index < tableHeaders.length - 1 ?
                    <th key={index} scope={"col"}>
                        {this.getSortedLayout(head)}
                    </th>
                    :
                    <th key={index} scope={"col"} colSpan={"2"}>
                        {this.getSortedLayout(head)}
                    </th>
            )
        });


        return (
            <thead className="thead-dark">
            <tr>
                {headerJSX}
            </tr>
            </thead>
        )
    };

    handleOfficeRowClick = (office) => {
        sessionStorage.setItem("admin-office", JSON.stringify(office));
        window.location.reload();
    };

    tableBody = () => {


        let bodyJSX = this.props.subOffices.map((office, index) => {

            let officeData = {
                name: office.name,
                department: office.officeAuthority.department? office.officeAuthority.department.name : "-",
                officeAuthority: office.officeAuthority ? office.officeAuthority.name : "-",
                'reporting office': office.reportingOffice?office.reportingOffice.name:"-",
                domain: office.officeAuthority.domain ? office.officeAuthority.domain.name:"-",
                station: office.geoLocation? office.geoLocation.name : "-"
            };


            return (
                <tr key={index} style={{cursor: "pointer"}}>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData.name}</td>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData.department}</td>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData.officeAuthority}</td>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData['reporting office']}</td>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData.domain}</td>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData.station}</td>
                    <td onClick={()=>this.props.onEditClick(office)}>
                        <a>
                            Edit
                        </a>
                    </td>
                </tr>
            )
        });

        return (
            <tbody>
            {bodyJSX}
            </tbody>
        )
    };


    render() {
        return (
            <>
                <div className=" custom-table table-responsive">
                    <table className="table border-0 ">
                        {this.tableHeader()}
                        {this.tableBody()}
                    </table>
                </div>
            </>
        )
    }
}


export default withRouter(SubOffices)