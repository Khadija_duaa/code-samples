import React from 'react';
import {Form, Button, Input} from 'antd';
import 'antd/dist/antd.css';
import {BUTTON_COLOR} from "../../../../../utils/common-utils";


class CreateSubOffices extends React.Component {


    componentDidMount() {
        if (this.props.data && Object.keys(this.props.data).length) {
            let {data} = this.props;
            this.props.form.setFieldsInitialValue({data})
        }
    }

    handleFormSubmit = (event) => {
        event.preventDefault();
        this.props.form.validateFields((err, fieldValues) => {
            if (err) {
                return;
            }

            let {onFormSubmit} = this.props;
            onFormSubmit && onFormSubmit(fieldValues);
        })
    };


    renderButtons = () => {

        const buttonItemLayout = {
            wrapperCol: {offset: 6},
        };

        let {data} = this.props;
        return (
            <Form.Item {...buttonItemLayout}>
                <Button htmlType="submit" onClick={this.handleFormSubmit} style={{
                    height: "40px",
                    width: "45%",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>{data ? "Update" : "Create"}</Button>
                <Button style={{height: "40px", width: "45%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.toggleCB}>Cancel</Button>
            </Form.Item>
        )
    };


    renderForm = () => {
        let {getFieldDecorator} = this.props.form;
        const formItemLayout = {
            labelCol: {span: 6},
            wrapperCol: {span: 18},
        };
        return (
            <Form layout={"horizontal"}>
                <Form.Item {...formItemLayout} label={"Name"}>
                    {getFieldDecorator('name', {
                        initialValue:this.props.data && this.props.data.name,
                        rules: [{
                            required: true,
                            whitespace: true,
                            message: "Required!"
                        }]
                    })(
                        <Input placeholder={"Name"}/>)}
                </Form.Item>

                {this.renderButtons()}
            </Form>
        )
    };

    render() {
        return (
            <div className={"col-md-12"} style={{padding: "unset"}}>
                {this.renderForm()}
            </div>
        )
    }
}

const createdForm = Form.create()(CreateSubOffices);
export default createdForm