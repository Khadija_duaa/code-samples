import React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {
    createLinkedOffice, createSubOffice, fetchOfficeDetails, fetchSubOfficeDetails, getAllDepartments, getLinkedOffices,
    revokeRoleFromUser, unlinkOffice, updateOffice
} from "../../../../../store/server/server-actions";
import {BUTTON_COLOR, formatName, getObjectValue} from "../../../../../utils/common-utils";
import {Button, Tabs, Col, Row} from 'antd';
import {ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import Modal from '../../../../../Components/Modal/Modal';

import NewLoader from '../../../../Loader/NewLoader';
import BackButton from "../../../../../Components/Buttons/BackButton";
import BreadCrumb from "../../../../../Components/BreadCrumb/BreadCrumb";
import CreateSubOffices from "../SubOffices/CreateSubOffices";
import OfficeUsers from "../OfficeUsers/OfficeUsers";
import SubOffices from "../SubOffices/SubOffices";

import CreateLinkedOffice from "../LinkedOffices/CreateLinkedOffice";
import LinkedOffices from '../LinkedOffices/LinkedOffices';

import HeaderTitle from "../../../../../Components/HeaderTitle/HeaderTitle";
import ConfirmationModal from '../../../../../Components/ModalFactory/MessageModal/ConfirmationModal';

const TabPane = Tabs.TabPane;


const userSearchFields = ['armyNumber', 'firstName', 'phoneNumber', 'username', 'rankName', 'phoneNumber'];
const officeSearchFields = ['name', "department", "powerLevel", "head", "domain", "station"];
const linkedOfficeSearchFields = ['name'];

class OfficeDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userRanks: null,
            visible: true,
            showModal: false,
            showSubOfficeModal: false,
            showConfirmModal: false,
            userId: null,
            rankId: null,
            searchParams: userSearchFields,
            subOfficeData: null,
            subOfficeEdit: null,
            tab: "user",

            showLinkedOfficeModal: false,
            linkedOfficeData: null,
            linkedOfficeEdit: null,
            showLinkedConfirmModal: false,

            isAdd:false
        }
    };

    getOffice = () => {
        const storageKey = "admin-office";
        let office = sessionStorage.getItem(storageKey);

        if (office) {
            office = JSON.parse(office);
        }

        const {location} = this.props;

        if (location && location.state && location.state.office) {
            office = location.state.office;
        }

        sessionStorage.setItem(storageKey, JSON.stringify(office));

        return office;
    };


    componentDidMount() {
        this.fetchOfficeDetails();
        this.fetchSubOffices();
        this.fetchLinkedOffices();
        this.props.getAllDepartments();
    }

    /************************* Events *************************/

    fetchOfficeDetails = () => {
        let office = this.getOffice();
        this.props.loadOfficeDetails(office.uuid);
    };

    fetchSubOffices = () => {
        let office = this.getOffice();
        this.props.loadSubOfficeDetails(office.uuid);
    };

    fetchLinkedOffices = () => {
        let office = this.getOffice();
        this.props.loadLinkedOffices(office.uuid);
    };


    searchUsers = (searchStr) => {
        let {searchParams, tab} = this.state;

        let {officeDetails, subOffices, linkedOffices} = this.props;
        if ((tab === "user" && !officeDetails) || (tab === 'subOffice' && !subOffices) || (tab === 'linkedOffice' && !linkedOffices)) {
            return null;
        }

        // Setting Data & Key to search for particular props data and set into particular value in state;
        // For Example: For user tab, search must be between office details user data, not in sub offices and linked offices.

        let data = null;
        let key = null;
        if (tab === 'user') {
            key = 'userRanks';
            data = [...this.props.officeDetails]
        } else if (tab === 'subOffice') {
            key = 'subOfficeData';
            data = [...this.props.subOffices];
        } else if (tab === 'linkedOffice') {
            key = 'linkedOfficeData';
            data = [...this.props.linkedOffices];
        }


        if (searchStr) {
            let filteredData = [];
            data.forEach(projectUser => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(projectUser, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredData.push(projectUser);
                        break;
                    }
                }
            });

            this.setState({[key]: filteredData});
        }
        else {
            this.setState({[key]: null});
        }
    };

    // Revoking Office Role
    revokeOfficeRole = () => {
        let data = {
            rankId: this.state.rankId,
            userId: this.state.userId,
            successMessage: "Office appointment has revoked from user successfully"
        };
        let office = this.getOffice();


        this.props.revokeOffice(data,
            () => {
                this.toggleModal();
                this.props.loadOfficeDetails(office.uuid);
            }
        )
    };

    // Handle Maanage Role Click
    handleManageRankClick = () => {
        this.props.history.push({
            pathname: '/dashboard/offices/ranks',
            state: {office: this.getOffice()}
        });
    };


    /**********************************************************/

    /************************* Modal Components *************************/

        //Revoking Office Confirmation Modal
    toggleModal = () => {
        this.setState({showModal: !this.state.showModal});
    };
    confirmationModal = () => {

        let buttonStyle = {
            height: "40px",
            width: "50%"
        };

        return (
            <div className={"col-md-12"}>
                <Modal isOpen={this.state.showModal} toggle={this.toggleModal} style={{width: "100%"}}>
                    <ModalHeader style={{border: "unset", WebkitTextStroke: "0.5px"}}>
                        Relinquish Role of {this.state.name}
                    </ModalHeader>


                    <ModalBody>
                        Are you sure, you want to relinquish role?
                    </ModalBody>


                    <ModalFooter style={{border: "unset"}}>
                        <Button style={{...buttonStyle, backgroundColor: BUTTON_COLOR, color: "#fff"}}
                                onClick={this.revokeOfficeRole}>
                            Yes
                        </Button>

                        <Button style={{...buttonStyle, marginLeft: "5%", borderColor: BUTTON_COLOR}}
                                onClick={this.toggleModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    };


    //Form Submission Confirmation Modal
    toggleConfirmationModal = (data) => {
        this.setState({showConfirmModal: !this.state.showConfirmModal, subOfficeData: data})
    };
    handleConfirmClick = (data) => {

        let office = this.getOffice();
        let formData = {
            name: data.name,
            parentOffice: office.uuid
        };

        if (this.state.subOfficeEdit) {
            let {subOfficeEdit} = this.state;
            let officeData = {};
            officeData.name = data.name;
            officeData.domainId = subOfficeEdit.officeAuthority.domain.uuid;
            officeData.departmentId = subOfficeEdit.officeAuthority.department.uuid;
            officeData.authorityId = subOfficeEdit.officeAuthority.uuid;
            officeData.geoLocationId = subOfficeEdit.geoLocation.uuid;
            if (subOfficeEdit.reportingOffice) {
                officeData.reportingOfficeId = subOfficeEdit.reportingOffice.uuid;
            }

            this.props.updateSubOffice(this.state.subOfficeEdit.uuid, officeData,
                () => this.setState({
                    showSubOfficeModal: false,
                    subOfficeEdit: null
                }, () => this.fetchSubOffices()), null, "Sub Office has updated successfully"
            )

        } else {
            this.props.createSubOffice(formData, () => {
                this.setState({showSubOfficeModal: false}, () => {
                    this.fetchSubOffices();
                })
            })
        }

    };


    // Sub Office Modal
    toggleSubOfficeModal = () => {
        this.setState({showSubOfficeModal: !this.state.showSubOfficeModal})
    };
    renderSubOfficeModal = () => {
        let data = null;
        let text = "Create";
        if (this.state.subOfficeEdit) {
            data = {
                name: this.state.subOfficeEdit.name
            };
            text = "Edit"
        }

        return (
            <div className={"col-md-12"}>
                <Modal isOpen={this.state.showSubOfficeModal} style={{width: "100%"}}>
                    <ModalHeader style={{border: "unset", WebkitTextStroke: "0.5px"}}>
                        {text} Sub Office
                    </ModalHeader>

                    <ModalBody>
                        <CreateSubOffices data={data} onFormSubmit={(formData) => this.handleConfirmClick(formData)}
                                          toggleCB={() => this.setState({showSubOfficeModal: false})}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    };


    toggleLinkedOfficeModal = () => {
        this.setState({showLinkedOfficeModal: !this.state.showLinkedOfficeModal,isAdd:true})
    };
    handleLinkOfficeClick = () => {
        let office = this.getOffice();
        let {linkedOfficeEditData,isAdd} = this.state;

        if (isAdd) {

            this.props.createLinkedOffices(office.uuid, linkedOfficeEditData.linkedOfficeId, () => {
                this.setState({
                    showLinkedOfficeModal: false,
                    showLinkedConfirmModal: false,
                    linkedOfficeData: null,
                    linkedOfficeEditData : null,
                    isAdd:false
                }, () => {
                    this.fetchLinkedOffices();
                })
            }, () => {
                this.setState({showLinkedConfirmModal: false, linkedOfficeEditData: null})
            })
        } else {
            this.props.unlinkOffice(office.uuid, linkedOfficeEditData.uuid, () => this.setState({
                showLinkedConfirmModal: false,
                linkedOfficeData: null,
                linkedOfficeEditData : null
            }, () => {
                this.fetchLinkedOffices();
            }))
        }
        // if (this.state.linkedOfficeEdit) {
        //     let {subOfficeEdit} = this.state;
        //     let officeData = {};
        //     officeData.name = data.name;
        //     officeData.domainId = subOfficeEdit.officeAuthority.domain.uuid;
        //     officeData.departmentId = subOfficeEdit.officeAuthority.department.uuid;
        //     officeData.authorityId = subOfficeEdit.officeAuthority.uuid;
        //     officeData.geoLocationId = subOfficeEdit.geoLocation.uuid;
        //     if (subOfficeEdit.reportingOffice) {
        //         officeData.reportingOfficeId = subOfficeEdit.reportingOffice.uuid;
        //     }
        //
        //     this.props.updateSubOffice(this.state.subOfficeEdit.uuid, officeData,
        //         () => this.setState({
        //             showSubOfficeModal: false,
        //             subOfficeEdit: null
        //         }, () => this.fetchSubOffices()), null, "Sub Office has updated successfully"
        //     )
        //
        // } else {
        //     this.props.createLinkedOffices(office.uuid,formData.linkedOfficeId, () => {
        //         this.setState({showLinkedOfficeModal: false}, () => {
        //             this.fetchLinkedOffices();
        //         })
        //     })
        // }
    };


    // Linked Office Modal
    handleLinkedCancelClick = () => {
        this.setState({showLinkedConfirmModal: false})
    };

    toggleLinkedConfirmationModal = (data, isUnlink = false) => {
        this.setState({
            showLinkedConfirmModal: !this.state.showLinkedConfirmModal,
            linkedOfficeEditData: data,
            isUnlink
        })
    };

    renderLinkedConfirmationModal = () => {

        let {linkedOfficeEditData,isAdd} = this.state;

        let message = '';

        if (linkedOfficeEditData) {
            if(isAdd){
                message = `Are you sure, you want to link ${this.getOffice().name} with ${linkedOfficeEditData.name}?`;
            }else{
                message = `Are you sure, you want to unlink ${this.getOffice().name} from ${linkedOfficeEditData.name}?`;
            }

        }

        return (
            <ConfirmationModal isOpen={this.state.showLinkedConfirmModal}
                               okCB={this.handleLinkOfficeClick} cancelCB={this.handleLinkedCancelClick}
                               message={message}/>
        );
    };

    renderLinkedOfficeModal = () => {
        let data = null;
        let text = "Create";
        if (this.state.linkedOfficeEdit) {
            data = this.state.linkedOfficeEdit;
            text = "Edit"
        }

        let dataProps = {
            departments: this.props.departments,
            office: this.getOffice(),
            onFormSubmit: (formData) => this.toggleLinkedConfirmationModal(formData)
        };
        return (
            <div className={"col-md-12"}>
                <Modal isOpen={this.state.showLinkedOfficeModal} style={{minWidth: "650px", width: "100%"}}>
                    <ModalHeader style={{border: "unset", WebkitTextStroke: "0.5px"}}>
                        {text} Linked Office
                    </ModalHeader>

                    <ModalBody>
                        <CreateLinkedOffice {...dataProps} departments={this.props.departments} data={data}
                                            toggleCB={() => this.setState({showLinkedOfficeModal: false,isAdd:false})}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    };


    /************************* END *************************/


    getBreadCrumbs = () => {

        let breadCrumbsJSON = [
            {
                title: "Offices",
                returnPath: '/dashboard/offices'
            },
            {
                title: "Office Users"
            }
        ];


        return (
            <BreadCrumb style={{fontSize: "20px"}} crumbsJSON={breadCrumbsJSON}/>
        )
    };

    getHeaderSection = () => {


        let {officeDetails} = this.props;

        if (!officeDetails)
            return null;

        let office = this.getOffice();

        return (
            <div className="container-fluid mb-5">
                <div className="row">
                    <div className="col-md-12">
                        <div className="dashboard-show-hide-section-container">
                            <div className={`padding-bottom-2x p-5 dashboard-show-hide-section`}
                                 style={this.state.visible ? {display: "block"} : {display: "none"}}>
                                <div className="row">
                                    <div className="col-md-12" style={{marginBottom: "30px"}}>
                                        <h3>{office.name}</h3>
                                    </div>
                                </div>
                                <div className="row office-detail-dashboard flex-view">
                                    <div className="col-md-custom margin0right-10p">
                                        <p>Station</p>
                                        <span>{office.geoLocation ? office.geoLocation.name : "N/A"}</span>
                                    </div>
                                    <div className="col-md-custom">
                                        <p>Department</p>
                                        <span>{office.officeAuthority.department ? office.officeAuthority.department.name : "N/A"}</span>
                                    </div>

                                    <div className="col-md-custom margin0right-10p">
                                        <p>Domain</p>
                                        <span>{office.officeAuthority.domain ? office.officeAuthority.domain.name : "N/A"}</span>
                                    </div>

                                    <div className="col-md-custom">
                                        <p>Reporting Office</p>
                                        <span>{office.reportingOffice ? office.reportingOffice.name : "N/A"}</span>
                                    </div>

                                    <div className="col-md-custom no-border-bottom margin0right-10p">
                                        <p>Users Count</p>
                                        <span>{officeDetails.length}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    };

    getTitle = () => {
        let office = this.getOffice();

        return (
            <HeaderTitle>
                <div className={"row"}>
                    <div className={"col-md-6"}>
                        <h3>{office.name} Office Details</h3>
                    </div>
                    <div className={"col-md-6"} style={{textAlign: "right"}}>
                        <BackButton returnCB={() => this.props.history.push('/dashboard/offices')}
                                    style={{backgroundColor: "white", width: "100px", height: "40px"}}/>
                    </div>
                </div>
            </HeaderTitle>
        )
    };


    getButtonList = () => {
        let {projectDomains} = this.props;
        let dropdownJSX = projectDomains && projectDomains.map((domain, index) => {

            return (
                <option key={index + 1} value={domain.name}>{domain.name}</option>
            )
        });

        dropdownJSX.unshift(<option key={0} defaultValue={"All"} value={"All"}>All Domains</option>);

        return (
            <div className="container-fluid">
                <div className="row button-list align-items-baseline mt-5">
                    <div className="col-md-8">
                        <button className="custom-button custom-button-dark create-button"
                                onClick={() => this.handleManageRankClick()}>
                            <i className="fas fa-asterisk"/> Manage Roles
                        </button>
                        <button className="custom-button custom-button-dark create-button" style={{marginLeft: "10px"}}
                                onClick={this.toggleSubOfficeModal}>
                            <i className="fas fa-plus"/> Create Sub Office
                        </button>

                        <button className="custom-button custom-button-dark create-button" style={{marginLeft: "10px"}}
                                onClick={this.toggleLinkedOfficeModal}>
                            <i className="fas fa-plus"/> Create Linked Office
                        </button>
                    </div>

                    <div className="col-md-4 text-right margin-top-56">
                        <div className="new-btn-with-gradient-select search-bar">
                            <form className="search-container">
                                <input type="text" id="search-bar" placeholder="Search…"
                                       onChange={(e) => this.searchUsers(e.target.value)}/>
                                <a><i className="fas fa-search"/></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    };


    getOfficeUsers = () => {

        if (!this.props.officeDetails)
            return null;

        let userRanks = this.state.userRanks ? this.state.userRanks : this.props.officeDetails;

        return (
            <div className="container-fluid">
                <div className="row">
                    <OfficeUsers onRelinquishClick={(user) => this.setState({
                        showModal: true,
                        userId: user.uuid,
                        name: formatName(user),
                        rankId: user.rankId
                    })} dataSource={userRanks}/>
                </div>
            </div>
        )
    };

    getSubOffices = () => {
        if (!this.props.subOffices)
            return null;

        let subOffices = this.state.subOfficeData ? this.state.subOfficeData : this.props.subOffices;

        return (
            <div className="container-fluid">
                <div className="row">
                    <SubOffices onEditClick={(officeData) => this.setState({
                        showSubOfficeModal: true,
                        subOfficeEdit: officeData
                    })}
                                subOffices={subOffices}/>
                </div>
            </div>
        )
    };

    getLinkedOffices = () => {
        if (!this.props.linkedOffices)
            return null;

        let linkedOffices = this.state.linkedOfficeData ? this.state.linkedOfficeData : this.props.linkedOffices;

        return (
            <div className="container-fluid">
                <div className="row">
                    <LinkedOffices onUnlinkClick={(formData) => this.toggleLinkedConfirmationModal(formData, true)}
                                   linkedOffices={linkedOffices}/>
                </div>
            </div>
        )
    };


    handleTabChange = (key) => {
        if (key === "users") {
            this.setState({tab: "user", searchParams: userSearchFields, userRanks: null}, () => {
                document.getElementById('search-bar').value = "";
            })
        } else if (key === "subOffices") {
            this.setState({tab: "subOffice", searchParams: officeSearchFields, subOfficeData: null}, () => {
                document.getElementById('search-bar').value = "";
            })
        } else if (key === 'linkedOffices') {
            this.setState({tab: "linkedOffice", searchParams: linkedOfficeSearchFields, linkedOfficeData: null}, () => {
                document.getElementById('search-bar').value = "";
            })
        }
    };

    getTabs = () => {
        let tabsJSON = [
            {
                tab: "Office Users",
                key: "users",
                navLink: () => this.getOfficeUsers()
            },
            {
                tab: "Sub Offices",
                key: "subOffices",
                navLink: () => this.getSubOffices()
            },
            {
                tab: "Linked Offices",
                key: "linkedOffices",
                navLink: () => this.getLinkedOffices()
            }
        ];

        let tabsJSX = tabsJSON.map((tabs) => {
            return (
                <TabPane tab={tabs.tab} key={tabs.key}>
                    <Col span={24}>
                        {tabs.navLink()}
                    </Col>
                </TabPane>
            )
        });

        return (
            <Col span={24} className="firms">
                <Row className="radio">
                    <Tabs type="card" onChange={this.handleTabChange} className="firm-tabs-container">
                        {tabsJSX}
                    </Tabs>
                </Row>
            </Col>
        )
    };

    render() {

        return (
            <>
                {this.renderLinkedConfirmationModal()}
                {this.confirmationModal()}
                {this.renderSubOfficeModal()}
                {this.renderLinkedOfficeModal()}

                {this.getBreadCrumbs()}
                {this.getHeaderSection()}
                {this.getTitle()}
                {this.getButtonList()}

                {this.props.processing ?
                    <NewLoader/>
                    :
                    this.getTabs()
                }
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadOfficeDetails: (officeId) => dispatch(fetchOfficeDetails(officeId)),
        loadSubOfficeDetails: (officeId) => dispatch(fetchSubOfficeDetails(officeId)),

        loadLinkedOffices: (officeId) => dispatch(getLinkedOffices(officeId)),
        createLinkedOffices: (officeId, linkedOfficeId, cb, errorCB) => dispatch(createLinkedOffice(officeId, linkedOfficeId, cb, errorCB)),
        unlinkOffice: (officeId, linkedOfficeId, cb) => dispatch(unlinkOffice(officeId, linkedOfficeId, cb)),


        revokeOffice: (data, successCB) => dispatch(revokeRoleFromUser(data, successCB)),
        createSubOffice: (data, successCB) => dispatch(createSubOffice(data, successCB)),
        updateSubOffice: (officeId, data, successCB, errorCB, successText) => dispatch(updateOffice(officeId, data, successCB, errorCB, successText)),

        getAllDepartments: () => dispatch(getAllDepartments())
    }
};
const mapStateToProps = state => {
    return {
        projectDomains: state.server_reducer.projectDomains,
        officeDetails: state.server_reducer.officeDetails,
        subOffices: state.server_reducer.subOffices,
        linkedOffices: state.server_reducer.linkedOffices,
        processing: state.server_reducer.processing,

        departments: state.server_reducer.departments
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(OfficeDetails);

export default withRouter(connected)