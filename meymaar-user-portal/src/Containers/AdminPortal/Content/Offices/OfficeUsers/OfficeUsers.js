import React from 'react'
import {formatName, formatPhoneNumber} from "../../../../../utils/common-utils";

class OfficeUsers extends React.Component {

    tableHeader = () => {
        let tableHeaders = ["Service #", "Name", "Username", "Appointment", "Contact #", "Action"];

        let headerJSX = tableHeaders.map((head, index) => {
            return (
                index < tableHeaders.length - 1 ?
                    <th key={index} scope={"col"}>{head}</th>
                    :
                    <th key={index} scope={"col"} colSpan={"2"}>{head}</th>
            )
        });

        return (
            <thead className="thead-dark">
            <tr>
                {headerJSX}
            </tr>
            </thead>
        )
    };

    tableBody = () => {



        let bodyJSX = this.props.dataSource.map((user) => {
            return (
                <tr key={user.uuid + user.rankName}>
                    <td>{user.armyNumber}</td>
                    <td>{formatName(user)}</td>
                    <td>{user.username}</td>
                    <td>{user.rankName}</td>
                    <td>{formatPhoneNumber(user.phoneNumber,'PK')}</td>
                    <td colSpan="2">
                        <a onClick={()=>this.props.onRelinquishClick(user)}>
                            Relinquish Role
                        </a>
                    </td>
                </tr>
            )
        });

        return (
            <tbody>
            {bodyJSX}
            </tbody>
        )
    };

    render() {
        return (
            <>
                <div className=" custom-table table-responsive">
                    <table className="table border-0 ">
                        {this.tableHeader()}
                        {this.tableBody()}
                    </table>
                </div>
            </>
        )
    }
}

export default OfficeUsers