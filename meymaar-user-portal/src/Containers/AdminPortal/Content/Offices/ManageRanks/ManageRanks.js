import React from 'react';
import {connect} from 'react-redux';
import {fetchDesignations, getRolePrivilleges, resetRedux} from "../../../../../store/server/server-actions";
import ManageRole from './Role';
import NewLoader from '../../../../Loader/NewLoader';

class ManageRanks extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            activeTabIndex: 0,
            isAdd: false,
            isEditing: false,
            activeDesignationId: null,
        }
    }

    getOffice = () => {
        const {location} = this.props;

        if (location && location.state && location.state.office) {
            return location.state.office;
        }
    };


    componentDidMount() {
        let office = this.getOffice();

        this.props.resetPriviliges();

        if (!office) {
            this.props.history.push("/dashboard/offices/users");
        } else {
            this.getDesignations(office);
        }
    }


    /************************* Events *************************/



    getDesignations = (office) => {
        this.props.getDesignations(office.uuid,
            (designationId) => {
                this.props.getPrivilleges(designationId);
                this.setState({activeTabIndex: 0, isAdd: false, activeDesignationId: designationId, isEditing: false})
            }
        );
    };

    handleSuccessCB = () => {
        //this.props.history.push('/dashboard/offices/users');
        let office = this.getOffice();
        this.props.getDesignations(office.uuid,(designationId)=>{
            let uuid = this.state.activeDesignationId || designationId;
            this.props.getPrivilleges(uuid);
            this.setState({isAdd: false, isEditing: false,activeDesignationId:uuid});
        });
    };
    renderRolePrivilieges = () => {

        if (!this.props.privilleges) return null;

        return this.props.privilleges.map((navigation) => {
            return (
                <li key={navigation.privilege}><span className="fa-li"><i
                    className="fas fa-check-square"/></span>{navigation.name}</li>
            )
        });
    };


    handleCancelCB = () => {

        this.setState({isAdd: false, isEditing: false}, () => {
            if (this.state.activeDesignationId) {
                this.props.getPrivilleges(this.state.activeDesignationId);
            }
        })
    };


    handleDesignationTabClick = (index, designationId) => {

        let {activeTabIndex} = this.state;

        if (activeTabIndex !== index) {
            this.props.resetPriviliges();
            this.props.getPrivilleges(designationId);

            this.setState({activeTabIndex: index, activeDesignationId: designationId});
        }
    };


    /**********************************************************/

    // Rendering Title of page
    getTitle = () => <h3 className="mb-5"> Manage Roles </h3>;

    // Rendering Top Designations Nav
    getDesignationNav = () => {

        let navJSX = this.props.designations.map((designation, index) => {
            let isActive = this.state.activeTabIndex === index;

            return (
                <a key={index} className={`nav-item nav-link ${isActive ? 'active' : ''}`} id="nav-tab"
                   data-toggle="tab" role="tab"
                   aria-controls="nav-home" aria-selected={isActive}
                   onClick={() => this.handleDesignationTabClick(index, designation.uuid)}>
                    {designation.name}
                </a>
            )
        });

        let addTabJSX = () => {
            return (
                <a key={navJSX.length} className=" link nav-item nav-link" id="nav-tab" data-toggle="tab"
                   onClick={() => this.setState({isAdd: true})}
                   role="tab" aria-controls="" aria-selected="false">+ Add</a>
            )
        };


        // Adding Add button
        navJSX.push(addTabJSX());

        return (
            <nav className="nav-tabs-responsive">
                <div className="nav nav-tabs" id="nav-tab" role="tablist">
                    {navJSX}
                </div>
            </nav>
        )
    };


    // Rendering Privileges of a Designation ( Default index = 0)
    getDesignationContent = () => {

        let btnStyle = {
            width:"150px",
            margin:"30px 0 136px 16px"
        };

        let paraStyle = {
            paddingTop:"40px"
        };

        let tabsJSX = this.props.designations.map((designations, index) => {

            let headDesignations = designations.headDesignation;

            return (
                <div className={`tab-pane fade ${this.state.activeTabIndex === index ? 'show active' : ''}`}
                     key={designations.uuid}
                     id={index} role="tabpanel">

                    <h3>{designations.name}</h3>


                    {
                        headDesignations ? <span style={{marginTop:"10px",paddingBottom:"unset"}}>REPORTS TO {headDesignations.name}</span> : null
                    }


                    <p style={paraStyle}>Privileges for {designations.name} are as follows, and can be updated as per requirement</p>


                    {
                        !this.props.processing ?
                            [
                                <ul key={0} className="fa-ul tab-list-options">
                                    {this.renderRolePrivilieges()}
                                </ul>,

                                <div key={1} className="row">
                                    <div className="col-md-12">
                                        <button className="custom-button custom-button-dark create-button" style={btnStyle}
                                                onClick={() => this.setState({
                                                    currentPrivillege: this.props.privilleges,
                                                    isEditing: true
                                                })}>
                                            Edit
                                        </button>

                                        <button className="custom-button custom-button-light create-button" style={btnStyle}
                                                onClick={() =>this.props.history.push("/dashboard/offices/users")}>
                                            Back
                                        </button>
                                    </div>
                                </div>

                            ] :  <NewLoader/>
                    }


                </div>
            )
        });

        return (
            <div className="tab-content" id="nav-tabContent">
                {tabsJSX}
            </div>
        )
    };


    // Main Render Method
    render() {
        let office = this.getOffice();


        if (!this.props.designations)
            return null;


        if (this.state.isAdd || this.state.isEditing) {

            let roleProps = {
                ...this.state,
                designations: this.props.designations,
                designationId: this.state.activeDesignationId,
                officeId: office.uuid,
                cancelCB: this.handleCancelCB,
                successCB: this.handleSuccessCB,
                activeIndex: this.state.activeTabIndex
            };

            return <ManageRole {...roleProps}/>
        }

        return (
            <div className="col-12">
                <div className="ranks-tabs">
                    <div className="title-row pt-3 pb-3 pl-5">
                        <div className={"col-md-12"}>
                            {this.getTitle()}
                            {this.getDesignationNav()}
                            {this.getDesignationContent()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        getDesignations: (officeId, cb) => dispatch(fetchDesignations(officeId, cb)),
        getPrivilleges: (roleId = null) => dispatch(getRolePrivilleges(roleId)),
        resetPriviliges: () => dispatch(resetRedux('privilleges'))
    }
};


const mapStateToProps = state => {
    return {
        activeUser: state.user_reducer.activeUser,
        designations: state.server_reducer.designations,
        privilleges: state.server_reducer.privilleges,
        processing: state.server_reducer.processing
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(ManageRanks);
export default connected