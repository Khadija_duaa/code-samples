import React from 'react';
import {Form, Input, Select, Checkbox} from 'antd';
import 'antd/dist/antd.css';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {
    assignPrivilegesToRole,
    updateDesignation,
    createDesignation, getAllPriviliges, resetRedux
} from "../../../../../store/server/server-actions";

import {capitilaze} from "../../../../../utils/common-utils";
import NewLoader from '../../../../Loader/NewLoader';

const Option = Select.Option;
const CheckboxGroup = Checkbox.Group;

class Role extends React.Component {


    componentDidMount() {
        this.props.resetPriviliges();
        this.props.getAllPrivilleges();
    }

    formItemLayout = {
        labelCol: {span: 4},
        wrapperCol: {span: 6},
    };

    /************************* Events *************************/

    assignPriviliges = (privileges, roleId) => {
        let {successCB} = this.props;

        let data = {
            ...privileges,
            roleId: roleId
        };


        this.props.assignPrivilegeToRoles(data, () => {
            successCB && successCB();
        })
    };


    handleRoleSubmit = (e) => {
        e.preventDefault();


        this.props.form.validateFields((err, fieldsValue) => {

            // Getting All selected privileges
            const privileges = this.props.form.getFieldsValue(["privileges"]);

            if (err) {
                return;
            }

            console.log("Role field values", fieldsValue);


            // Getting name and head designation & capitalizing designation name
            const designationData = this.props.form.getFieldsValue(["name", "headDesignationId", "color"]);
            designationData.officeId = this.props.officeId;
            designationData.name = capitilaze(designationData.name);


            // If new role has to add then create else update.
            if (this.props.isAdd) {
                this.props.createDesignation(designationData,
                    (roleId) => {
                        this.assignPriviliges(privileges, roleId);
                    });
            } else {
                let {designations, activeIndex} = this.props;
                let {uuid} = designations[activeIndex];


                this.props.updateDesignation(designationData, uuid, () => {
                    this.assignPriviliges(privileges, this.props.designationId);
                });
            }
        })
    };

    /**********************************************************/


    renderHeadDesignationsSelect = () => {

        let {designations, activeIndex, isEditing, form} = this.props;
        let designationJSX = null;

        if (designations.length) {

            let currentDesignation = designations[activeIndex];

            let designationOptions = designations.map((designations, index) => {
                return <Option key={index} value={designations.uuid}>{designations.name}</Option>;
            });

            designationOptions.unshift(<Option style={{padding: "15px 22px"}} key={designationOptions.length}
                                               value={null}/>);

            // Setting Initial Value
            let selectInitialValue = null;
            if (isEditing && currentDesignation.headDesignation) {
                selectInitialValue = designations[activeIndex].headDesignation.uuid;
            }


            designationJSX = <Form.Item  {...this.formItemLayout} label={"Reporting to"}>
                {
                    form.getFieldDecorator(`headDesignationId`, {
                        initialValue: selectInitialValue
                    })(<Select placeholder={"Select Head Designation..."}>
                        {designationOptions}
                    </Select>)
                }
            </Form.Item>
        }

        return designationJSX;
    };

    renderColorOptions = (inkColors) => {

        return inkColors.map((color, index) => {
            return (
                <Option key={index} value={color.value}>{color.name}</Option>
            )
        });
    };

    renderPrivilegeChecboxes = () => {
        let {isEditing, currentPrivillege, form} = this.props;

        let defaultValues = [];
        if (isEditing) {
            currentPrivillege && currentPrivillege.forEach(privillige => {
                defaultValues.push(privillige.id)
            })
        }


        const checkboxOptions = [];
        this.props.privilleges.filter(item => item.privilege !== 'SUPER_ADMIN').forEach(privillege => {
            let checkObject = {
                label: privillege.name,
                value: privillege.id
            };
            checkboxOptions.push(checkObject);
        });


        return (
            <Form.Item {...this.formItemLayout}>
                {
                    form.getFieldDecorator(`privileges`, {
                        initialValue: defaultValues
                    })(<CheckboxGroup options={checkboxOptions}/>)
                }
            </Form.Item>
        )
    };


    getTitle = () => {

        let titleText = "Add a Role";
        if (this.props.isEditing) {
            let {activeIndex, designations} = this.props;
            titleText = `Edit ${designations[activeIndex].name} Role`
        }

        return (
            <div className="title-row pt-3 pb-3 pl-5">
                <h3 className="mb-0"> {titleText} </h3>
            </div>
        )
    };

    getForm = () => {
        const {getFieldDecorator} = this.props.form;

        if (this.props.processing) return <NewLoader/>;

        if (!this.props.privilleges) return null;


        let btnStyle = {
            width: "150px",
            margin: "30px 0 136px 16px"
        };

        let buttonText = 'Add a Role';
        if (this.props.isEditing) buttonText = "Update Role";


        let inkColors = [
            {
                name: "Black",
                value: "#000000"
            },
            {
                name: "Green",
                value: "#02A75A"
            },
            {
                name: "Blue",
                value: "#044FD1",
            },
            {
                name: "Red",
                value: "#DB2929"
            },
            {
                name: "Orange",
                value: "#F5810B"
            },
            {
                name: "Brown",
                value: "#7B5E60"
            }
        ];


        return (
            <Form layout={"horizontal"}>

                <Form.Item {...this.formItemLayout} label={"Name"}>
                    {
                        getFieldDecorator(`name`, {
                            rules: [{required: true, message: 'Name is required', whitespace: true}]
                            ,
                            initialValue: this.props.isEditing ? this.props.designations[this.props.activeIndex].name : ""
                        })(<Input placeholder={"Name"}/>)
                    }
                </Form.Item>

                {this.renderHeadDesignationsSelect()}

                <Form.Item {...this.formItemLayout} label={"Ink Color"}>
                    {
                        getFieldDecorator(`color`, {
                            rules: [{required: true, message: "Ink color is required"}]
                            ,
                            initialValue: this.props.isEditing ? this.props.designations[this.props.activeIndex].color : "",
                        })(<Select placeholder={"Select Color..."}>
                            {this.renderColorOptions(inkColors)}
                        </Select>)
                    }
                </Form.Item>

                <div className="add-rol-Privileges">
                    <p>Assign Privileges</p>
                </div>

                {this.renderPrivilegeChecboxes()}

                {this.props.processing ? <NewLoader/> : null}


                <button type={"submit"} style={{...btnStyle}} onClick={this.handleRoleSubmit}
                        className="mt-5 mr-3 custom-button custom-button-dark create-button">{buttonText}</button>
                <button style={{...btnStyle}} className="custom-button custom-button-light create-button mt-5"
                        onClick={this.props.cancelCB}>Cancel
                </button>
            </Form>
        )
    };


    render() {

        return (
            <div className="col-12">
                <div className="form-container mb-5">
                    {this.getTitle()}
                    <div className="detail-row edit-mode pt-3 pb-3 pl-5">
                        {this.getForm()}
                    </div>

                </div>
            </div>

        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        createDesignation: (data, cb) => dispatch(createDesignation(data, cb)),
        resetPriviliges: () => dispatch(resetRedux('privilleges')),
        getAllPrivilleges: () => dispatch(getAllPriviliges()),
        assignPrivilegeToRoles: (data, cb) => dispatch(assignPrivilegesToRole(data, cb)),
        updateDesignation: (data, id, cb) => dispatch(updateDesignation(data, id, cb))
    }
};


const mapStateToProps = state => {
    return {
        designations: state.server_reducer.designations,
        privilleges: state.server_reducer.privilleges,
        processing: state.server_reducer.processing
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Role);


const formCreated = Form.create()(connected);
export default withRouter(formCreated);