import React from 'react';
import {Form, Button,  Select} from 'antd';
import 'antd/dist/antd.css';
import {BUTTON_COLOR} from "../../../../../utils/common-utils";
import {
    fetchAuthoritiesByDeptAndLocation,
    fetchGeoLocationsByDepartment,
    fetchOfficesByAuthAndLocation
} from "../../../../../utils/server-utils";

const Option = Select.Option;

class CreateLinkedOffice extends React.Component {

    state = {
        stationData: null,
        authorityData: null,
        officeAuthLocationData: null,
        linkedOfficesData: null
    };

    componentDidMount() {
        if (this.props.data && Object.keys(this.props.data).length) {
            let {data} = this.props;
            this.props.form.setFieldsInitialValue({data})
        }
    }

    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };

    handleFormSubmit = (event) => {
        event.preventDefault();
        this.props.form.validateFields((err, fieldValues) => {
            if (err) {
                return;
            }
            fieldValues.name = this.state.officeName;
            let {onFormSubmit} = this.props;
            onFormSubmit && onFormSubmit(fieldValues);
        })
    };


    renderButtons = () => {

        const buttonItemLayout = {
            wrapperCol: {offset: 8},
        };

        let {data} = this.props;
        return (
            <Form.Item {...buttonItemLayout}>
                <Button htmlType="submit" onClick={this.handleFormSubmit} style={{
                    height: "40px",
                    width: "45%",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>{data ? "Update" : "Create"}</Button>
                <Button style={{height: "40px", width: "45%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.toggleCB}>Cancel</Button>
            </Form.Item>
        )
    };

    /**************** Offices By  ********************/

    handleOfficeChange = (id, event) => {
        this.setState({officeName: event.props.children});
    };
    renderOffices = () => {
        let {form} = this.props;
        let {linkedOfficesData} = this.state;

        let officeOptions = linkedOfficesData && linkedOfficesData.map((office, index) => {
            return (
                <Option key={index} value={office.uuid}>{office.name}</Option>
            )
        });

        return (
            <Form.Item {...this.formItemLayout} label={"Offices"}>
                {form.getFieldDecorator('linkedOfficeId', {
                    rules: [{
                        required: true,
                        whitespace: true,
                        message: "Required!"
                    }]
                })(<Select placeholder={"Select Office"} onChange={this.handleOfficeChange}>{officeOptions}</Select>)}
            </Form.Item>

        )
    };

    /**************** End ********************/


    /**************** Office Authorities By Location And Department with same domain ********************/

    handleAuthorityChange = (authorityId) => {
        this.props.form.resetFields('linkedOfficeId');
        let {stationId} = this.state;

        fetchOfficesByAuthAndLocation(stationId, authorityId, (offices) => {
            this.setState({linkedOfficesData: offices})
        })
    };

    renderOfficeAuthorities = () => {
        let {form, office} = this.props;
        let {authorityData} = this.state;

        let {domain} = office.officeAuthority;
        if (domain && authorityData) {
            authorityData = authorityData.filter(auth => auth.domain.uuid === domain.uuid);
        }

        let authorityOptions = authorityData && authorityData.map((authData, index) => {
            return (
                <Option key={index} value={authData.uuid}>{authData.name}</Option>
            )
        });

        return (
            <Form.Item {...this.formItemLayout} label={"Office Authority"}>
                {form.getFieldDecorator('authorityId', {
                    rules: [{
                        required: true,
                        whitespace: true,
                        message: "Required!"
                    }]
                })(<Select placeholder={"Select Office Authority"}
                           onChange={this.handleAuthorityChange}>{authorityOptions}</Select>)}
            </Form.Item>

        )
    };

    /**************** End ********************/


    /**************** Stations By Departments ********************/

    handleStationChange = (stationId) => {
        this.props.form.resetFields(['authorityId', 'linkedOfficeId']);
        let {departmentId} = this.state;
        fetchAuthoritiesByDeptAndLocation(departmentId, stationId, (authorities) => {
            this.setState({stationId, authorityData: authorities});
        })
    };

    renderStations = () => {
        let {form} = this.props;
        let {stationData} = this.state;

        let locationOptions = stationData && stationData.map((station, index) => {
            return (
                <Option key={index} value={station.uuid}>{station.name}</Option>
            )
        });

        return (
            <Form.Item {...this.formItemLayout} label={"Station"}>
                {form.getFieldDecorator('stationId', {
                    rules: [{
                        required: true,
                        whitespace: true,
                        message: "Required!"
                    }]
                })(<Select placeholder={"Select Station"}
                           onChange={this.handleStationChange}>{locationOptions}</Select>)}
            </Form.Item>

        )
    };

    /**************** End ********************/


    /**************** Departments with not itself department ********************/

    handleDepartmentChange = (deptId) => {
        this.props.form.resetFields(['stationId', 'authorityId', 'linkedOfficeId']);
        fetchGeoLocationsByDepartment(deptId, (geoLocations) => {
            this.setState({departmentId: deptId, stationData: geoLocations})
        })
    };

    renderDepartments = () => {
        let {departments, office, form} = this.props;
        let {department} = office.officeAuthority;
        if (department) {
            departments = departments.filter(dept => dept.uuid !== department.uuid);
        }

        let deptOptions = departments.map((dept, index) => {
            return (
                <Option key={index} value={dept.uuid}>{dept.name}</Option>
            )
        });

        return (
            <Form.Item {...this.formItemLayout} label={"Department"}>
                {form.getFieldDecorator('departmentId', {
                    rules: [{
                        required: true,
                        whitespace: true,
                        message: "Required!"
                    }]
                })(<Select placeholder={"Select Department"}
                           onChange={this.handleDepartmentChange}>{deptOptions}</Select>)}
            </Form.Item>

        )
    };

    /**************** End ********************/




    renderForm = () => {

        return (
            <Form layout={"horizontal"}>
                {this.renderDepartments()}
                {this.renderStations()}
                {this.renderOfficeAuthorities()}
                {this.renderOffices()}

                {this.renderButtons()}
            </Form>
        )
    };

    render() {
        return (
            <div className={"col-md-12"} style={{padding: "unset"}}>
                {this.renderForm()}
            </div>
        )
    }
}

const createdForm = Form.create()(CreateLinkedOffice);
export default createdForm