import React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {fetchAllOffices, resetRedux} from "../../../../store/server/server-actions";
import {compareAsc, compareDesc, getObjectValue} from "../../../../utils/common-utils";
import NewLoader from '../../../Loader/NewLoader';
import {Select, Icon} from 'antd';
import TableContainer from "../../../../Components/Table/TableContainer/TableContainer";
import HeaderTitle from "../../../../Components/HeaderTitle/HeaderTitle";

const Option = Select.Option;

const searchFields = ['name', "department", "powerLevel", "head", "domain", "station"];

class Offices extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            offices: null,
            visible: true,
        }
    };

    componentDidMount() {
        this.props.getAllOffices(null, (offices) => {
            this.setState({allOffices: offices})
        });
    }

    /************************* Events *************************/

    setOffices = (offices) => {
        let officesData = [];
        offices.map((office, index) => {
            officesData[index] = {
                name: office.name,
                department: office.departmentEntity.name,
                "power level": office.powerLevel ? office.powerLevel : "-",
                "reporting office": office.headOffice ? office.headOffice.name : "-",
                domain: office.domainEntity ? office.domainEntity.name : "-",
                station: office.geoLocationEntity ? office.geoLocationEntity.name : "-"
            };
        });

        this.setState({allOffices: officesData});
    };


    handleSelectChange = (values, event) => {
        let domainId = values;

        this.props.resetUsers();

        if (domainId !== "All") {
            this.props.getAllOffices(domainId, (offices) => {
                this.setState({allOffices: offices});
            });
        } else {
            this.props.getAllOffices(null, (offices) => {
                this.setState({allOffices: offices});
            });
        }
    };


    searchOffice = (searchStr, searchParams = []) => {


        const offices = this.state.allOffices;

        if(!offices) return null;


        if (searchStr) {
            let filteredUsers = [];
            offices.forEach(office => {

                for (let index = 0; index < searchParams.length; index++) {

                    let param = searchParams[index];

                    const _val = getObjectValue(office, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredUsers.push(office);
                        break;
                    }
                }
            });
            this.setState({offices: filteredUsers});
        }
        else {
            this.setState({offices: null});
        }
    };

    /**********************************************************/


    sortedParams = ["Name"];

    ascendingSort = (headerKey) => {
        if(!this.state.allOffices) return null;

        let arrayCopy = [...this.state.allOffices];
        arrayCopy.sort(compareAsc(headerKey));
        this.setState({allOffices: arrayCopy});
    };

    descendingSort = (headerKey) => {
        if(!this.state.allOffices) return null;

        let arrayCopy = [...this.state.allOffices];
        arrayCopy.sort(compareDesc(headerKey));
        this.setState({allOffices: arrayCopy});
    };

    getSortedLayout = (headerKey) => {
        if (this.sortedParams.includes(headerKey)) {
            return (
                <div>
                    {headerKey}
                    <span style={{
                        float: "right",
                        display: "inline-grid",
                        marginRight: "20px",
                        height: "-webkit-fill-available"
                    }}>
                        <Icon type="caret-up" onClick={() => this.ascendingSort(headerKey.toLowerCase())}/>
                        <Icon type="caret-down" onClick={() => this.descendingSort(headerKey.toLowerCase())}/>
                    </span>
                </div>
            )
        } else {
            return headerKey;
        }
    };



    tableHeader = () => {
        let tableHeaders = ["Name", "Department", "Office Authority", "Reporting Office", "Domain", "Station", "Action"];

        let headerJSX = tableHeaders.map((head, index) => {
            return (
                index < tableHeaders.length - 1 ?
                    <th key={index} scope={"col"}>
                        {this.getSortedLayout(head)}
                    </th>
                    :
                    <th key={index} scope={"col"} colSpan={"2"}>
                        {this.getSortedLayout(head)}
                    </th>
            )
        });


        return (
            <thead className="thead-dark">
            <tr>
                {headerJSX}
            </tr>
            </thead>
        )
    };


    handleOfficeRowClick = (office) => {
        this.props.history.push({
            pathname: '/dashboard/offices/users',
            state: {office, returnCB: '/dashboard/offices'}
        })
    };
    tableBody = () => {

        let offices = this.state.offices ? this.state.offices : this.state.allOffices;

        let bodyJSX = offices.map((office, index) => {

            let historyProps = {pathname: "/dashboard/offices/create", state: {office}};

            let officeData = {
                name: office.name,
                department: office.officeAuthority.department.name,
                officeAuthority: office.officeAuthority ? office.officeAuthority.name : "-",
                'reporting office': office.reportingOffice?office.reportingOffice.name:"-",
                domain: office.officeAuthority.domain.name,
                station: office.geoLocation? office.geoLocation.name : "-"
            };


            return (
                <tr key={index} style={{cursor: "pointer"}}>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData.name}</td>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData.department}</td>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData.officeAuthority}</td>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData['reporting office']}</td>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData.domain}</td>
                    <td onClick={() => this.handleOfficeRowClick(office)}>{officeData.station}</td>
                    <td onClick={() => this.props.history.push(historyProps)}>
                        <a>
                            Edit
                        </a>
                    </td>
                </tr>
            )
        });

        return (
            <tbody>
            {bodyJSX}
            </tbody>
        )
    };


    getHeaderSection = () => {


        let {offices} = this.props;

        if (!offices)
            return null;

        let totalOffices = offices.length;
        let departmentDetails = {};

        offices.forEach(office => {
            if (Object.keys(departmentDetails).includes(office.officeAuthority.department.name)) {
                let value = departmentDetails[office.officeAuthority.department.name];
                departmentDetails[office.officeAuthority.department.name] = value + 1;
            } else {
                departmentDetails[office.officeAuthority.department.name] = 1;
            }
        });

        let departmentsJSX = Object.keys(departmentDetails).sort().map((key, index) => {
            return (
                <div className="row no-gutters" key={index}>
                    <div className="col-md-8 dashboard-detail-box-title">{key}</div>
                    <div className="col-md-4 dashboard-detail-box-value">{departmentDetails[key]}</div>
                </div>
            )
        });

        return (
            <div className="container-fluid mb-5">
                <div className="row">
                    <div className="col-md-12">
                        <div className="dashboard-show-hide-section-container">
                            <div className={`p-5 dashboard-show-hide-section`}
                                 style={this.state.visible ? {display: "block"} : {display: "none"}}>
                                <div className="row">
                                    <div className="col-md-12" style={{marginBottom: "30px"}}>
                                        <h3>Offices</h3>
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-md-5 dashboard-detail-box">

                                        <div className="row no-gutters" style={{borderTop: "unset"}}>
                                            <div className="col-md-8 dashboard-detail-box-title">Total Offices</div>
                                            <div className="col-md-4 dashboard-detail-box-value">{totalOffices}</div>
                                        </div>

                                        {departmentsJSX}

                                    </div>
                                    <div className="col-md-7">
                                        <div id="donutchart" style={{height: "auto"}}/>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    };

    getTitle = () => {
        return (
           <HeaderTitle>
               <h3>Offices</h3>
           </HeaderTitle>
        )
    };


    getButtonList = () => {
        let {projectDomains} = this.props;
        let dropdownJSX = projectDomains && projectDomains.map((domain, index) => {

            return (
                <Option key={index + 1} value={domain.uuid}>{domain.name}</Option>
            )
        });

        dropdownJSX.unshift(<Option key={0} defaultValue={"All"} value={"All"}>All Domains</Option>);

        let historyProps = {pathname: "/dashboard/offices/create", state: {add: true}};

        return (
            <div className="container-fluid">
                <div className="row button-list align-items-baseline">
                    <div className="col-md-4">
                        <button className="custom-button custom-button-dark create-button"
                                onClick={() => this.props.history.push(historyProps)}>
                            + Create an Office
                        </button>
                    </div>

                    <div className="col-md-8 text-right margin-top-56">
                        {/* <!--dropdown-button--> */}
                        <div className="dropdown-button">
                            <Select className="padding-top-22 new-add-assign-select"
                                    onChange={(values, event) => this.handleSelectChange(values, event)}
                                    defaultValue={"All"}
                                    size="large">
                                {dropdownJSX}
                            </Select>
                        </div>


                        <div className="search-bar">
                            <form className=" new-btn-with-gradient-select search-container">
                                <input type="text" id="search-bar" placeholder="Search…"
                                       onChange={(e) => this.searchOffice(e.target.value, searchFields)}/>
                                <a><i className="fas fa-search"/></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    };

    getOfficeData = () => {

        if (!this.state.allOffices) return null;

        return (
            <TableContainer>
                {this.tableHeader()}
                {this.tableBody()}
            </TableContainer>
        )
    };

    render() {

        return (
            <>
                {this.getHeaderSection()}
                {this.getTitle()}
                {this.getButtonList()}
                {this.props.processing ?
                    <NewLoader/>
                    :
                    this.getOfficeData()
                }
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAllOffices: (domainId = null, cb) => dispatch(fetchAllOffices(domainId, cb)),
        resetUsers: () => dispatch(resetRedux('users'))
    }
};
const mapStateToProps = state => {
    return {
        projectDomains: state.server_reducer.projectDomains,
        offices: state.server_reducer.offices,
        processing: state.server_reducer.processing
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Offices);

export default withRouter(connected)