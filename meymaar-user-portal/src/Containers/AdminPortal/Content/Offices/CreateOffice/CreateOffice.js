import React from 'react';
import {withRouter} from 'react-router-dom';
import {Form, Button, Input, Select} from 'antd';
import 'antd/dist/antd.css';
import {connect} from "react-redux";
import {
    createUser,
    createOffice,
    updateOffice,
    getDepartmentAuthorities, getPotentialParents
} from "../../../../../store/server/server-actions";
import {BUTTON_COLOR} from "../../../../../utils/common-utils";
import {getDepartmentOptions, getStationOptions} from "../../../../../utils/server-utils";
import BreadCrumb from '../../../../../Components/BreadCrumb/BreadCrumb';
import '../../../../../Components/AdminPortal/Form/formStyle.css';
import NewLoader from '../../../../Loader/NewLoader';


const Option = Select.Option;


class CreateOffice extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            departmentOptions: null,
            departmentId: null,
            geoLocations: null,
            headOffices: null,
            officeAuthorities: [],
            potentialParents: null
        }
    }

    isEditState = () => {
        const {location} = this.props;
        let office = null;
        if (location && location.state) {
            office = location.state;
        }
        return office;
    };

    componentWillMount() {
        let officeState = this.isEditState();
        if (!officeState) {
            this.props.history.push('/dashboard/offices');
        }
    }

    componentDidMount() {
        let officeState = this.isEditState();

        //getOfficeOptions((offices) => this.setState({headOffices: offices}));
        getDepartmentOptions((departments) => this.setState({departmentOptions: departments}));
        getStationOptions((locations) => this.setState({geoLocations: locations}));


        if (officeState && officeState.office) {
            let {name, officeAuthority, geoLocation, reportingOffice} = officeState.office;

            if (officeAuthority.department && officeAuthority.domain) {

                this.props.fetchAuthorities(officeAuthority.department.uuid, officeAuthority.domain.uuid, (authoritites) => {

                    this.setState({officeAuthorities: authoritites}, () => {
                        this.props.form.setFieldsValue({authorityId: officeAuthority.uuid});
                        this.props.fetchPotentialParents(officeAuthority.uuid, (potentialParents) => {
                            this.setState({potentialParents}, () => {
                                if (reportingOffice) {
                                    this.props.form.setFieldsValue({reportingOfficeId: reportingOffice.uuid});
                                }
                            });
                        });
                    })
                });
            }


            this.props.form.setFieldsValue({name: name});

            if (officeAuthority.department) {
                this.props.form.setFieldsValue({departmentId: officeAuthority.department.uuid});
            }
            if (officeAuthority.domain) {
                this.props.form.setFieldsValue({domainId: officeAuthority.domain.uuid});
            }
            this.props.form.setFieldsValue({geoLocationId: geoLocation.uuid});

        }
    }

    /*********************** EVENTS **************************/
    formItemLayout = {
        labelCol: {span: 4},
        wrapperCol: {span: 6},
    };

    fetchOfficeAuthorites = () => {
        let departmentId = this.props.form.getFieldValue('departmentId');
        let domainId = this.props.form.getFieldValue('domainId');

        if (departmentId && domainId) {
            this.props.fetchAuthorities(departmentId, domainId, (officeAuthorities) => {
                this.props.form.resetFields(['authorityId', 'reportingOfficeId']);
                this.setState({officeAuthorities, potentialParents: null})
            })
        }
    };

    /*********************** END **************************/

    getHeader = () => {


        let officeState = this.isEditState();

        let text = officeState && officeState.office ? "Update Office" : "Create an Office";

        return (
            <div className={"col-md-12 custom-form-header"}>
                <h4 style={{WebkitTextStroke: "0.5px"}}>{text}</h4>
            </div>
        )
    };

    renderDepartments = () => {
        let {departmentOptions} = this.state;

        let jsx = null;
        if (departmentOptions) {
            jsx = departmentOptions.map((department, index) => {
                return (
                    <Option key={index} value={department.uuid}>{department.name}</Option>
                )
            });
            return jsx;
        }
        return jsx;
    };

    handleDepartmentChange = (id) => {
        this.setState({departmentId: id}, () => {
            this.fetchOfficeAuthorites();
        })
    };

    renderDomains = () => this.props.domains.map((domain, index) => {
        return (
            <Option key={index} value={domain.uuid}>{domain.name}</Option>
        )
    });


    handleDomainChange = (id) => {
        this.setState({domainId: id}, () => {
            this.fetchOfficeAuthorites();
        })
    };

    renderStations = () => {
        let {geoLocations} = this.state;

        let jsx = null;
        if (geoLocations) {
            jsx = geoLocations.map((location, index) => {
                return (
                    <Option key={index} value={location.uuid}>{location.name}</Option>
                )
            });

            return jsx;
        }
        return jsx;
    };


    handleOfficeAuthorityChange = (authId) => {

        this.props.form.resetFields('reportingOfficeId');
        this.setState({potentialParents: null}, () => {
            this.props.fetchPotentialParents(authId, (potentialParents) => {
                this.setState({potentialParents})
            })
        })
    };

    getOfficeAuthorities = () => {
        let {getFieldDecorator} = this.props.form;
        let {officeAuthorities} = this.state;


        if (!officeAuthorities || !officeAuthorities.length) return null;

        let authoritiesJSX = officeAuthorities.map((authority, index) => {
            return (
                <Option key={index} value={authority.uuid}>{authority.name}</Option>
            )
        });

        return (
            <Form.Item {...this.formItemLayout} label={"Office Authority"}>
                {
                    getFieldDecorator(`authorityId`, {
                        rules: [{required: true, message: 'Office authority is required'}],
                    })(
                        <Select size={"default"} id={"office"} className={"office"}
                                placeholder={"Select Authority Office..."} onChange={this.handleOfficeAuthorityChange}>
                            {authoritiesJSX}
                        </Select>
                    )
                }
            </Form.Item>
        )
    };


    getPotentialParents = () => {
        let {getFieldDecorator} = this.props.form;
        let {potentialParents} = this.state;

        if (!potentialParents) return null;

        let potentialJSX = potentialParents.map((parent, index) => {
            return (
                <Option key={index} value={parent.uuid}>{parent.name}</Option>
            )
        });

        return (
            <Form.Item {...this.formItemLayout} label={"Reporting Office"}>
                {
                    getFieldDecorator(`reportingOfficeId`, {
                        rules: [{required: true, message: 'Reporting office is required'}],
                    })(
                        <Select size={"default"} id={"office"} className={"office"}
                                placeholder={"Select Reporting Office..."}>
                            {potentialJSX}
                        </Select>
                    )
                }
            </Form.Item>
        )
    };

    hasNotOfficeAuthority = ()=>{
        const {getFieldValue} = this.props.form;
        let {processing} = this.props;
        let {officeAuthorities} = this.state;
        let hasValues = getFieldValue('departmentId') && getFieldValue('domainId');

        return Boolean(hasValues && (!officeAuthorities || !officeAuthorities.length) && !processing)
    };


    renderButtons = () => {

        let officeState = this.isEditState();

        const buttonItemLayout = {
            wrapperCol: {offset: 4},
        };

        let cancelClick = (e) => {
            e.preventDefault();
            this.props.history.push('/dashboard/offices');
        };

        return (
            <Form.Item {...buttonItemLayout}>
                <Button disabled={this.hasNotOfficeAuthority()} htmlType="submit" style={{
                    height: "40px",
                    width: "12%",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>{officeState && officeState.office ? "Update" : "Create"}</Button>
                <Button style={{height: "40px", width: "12%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={cancelClick}>Cancel</Button>
            </Form.Item>
        )
    };

    getBody = () => {

        let officesState = this.isEditState();

        let {processing} = this.props;

        let offices = officesState && officesState.office;

        let handleSubmit = (e) => {
            e.preventDefault();

            let successMessage = "Office has created successfully";

            this.props.form.validateFields((err, fieldsValue) => {

                if (err) {
                    return;
                }

                const values = {...fieldsValue};

                if (offices) {
                    let message = "Office has updated successfully";
                    this.props.updateOffice(offices.uuid, values,
                        () => {
                            this.props.form.resetFields();
                            this.props.history.push('/dashboard/offices');
                        },
                        (errors) => {

                            errors.forEach(errorField => {
                                this.props.form.setFields({
                                    [errorField.field]: {
                                        value: fieldsValue[errorField.field],
                                        errors: [new Error(`${errorField.error}`)],
                                    }
                                });
                            });
                        },
                        message
                    );
                } else {
                    this.props.createOffice(values,
                        () => {
                            this.props.form.resetFields();
                            this.props.history.push('/dashboard/offices');
                        },
                        (errors) => {

                            errors.forEach(errorField => {
                                this.props.form.setFields({
                                    [errorField.field]: {
                                        value: fieldsValue[errorField.field],
                                        errors: [new Error(`${errorField.error}`)],
                                    }
                                });
                            });
                        },
                        successMessage
                    );
                }


            })
        };
        const {getFieldDecorator} = this.props.form;


        return (
            <div className={"col-md-12"} style={{paddingLeft: "5%", paddingTop: "3%"}}>
                <Form layout={"horizontal"} onSubmit={handleSubmit}>
                    <Form.Item {...this.formItemLayout} label={"Name"}>
                        {
                            getFieldDecorator(`name`, {
                                rules: [{required: true, message: 'Name is required', whitespace: true}],
                            })(<Input type={"text"} placeholder={"Name"}/>)
                        }
                    </Form.Item>

                    <Form.Item {...this.formItemLayout} label={"Department"}>

                        {
                            getFieldDecorator(`departmentId`, {
                                rules: [{required: true, message: 'Department is required'}],
                            })(
                                <Select size={"default"} id={"department"} className={"department"}
                                        placeholder={"Select Department..."} onChange={this.handleDepartmentChange}>
                                    {this.renderDepartments()}
                                </Select>
                            )
                        }

                    </Form.Item>

                    <Form.Item {...this.formItemLayout} label={"Domain"}>

                        {
                            getFieldDecorator(`domainId`, {rules: [{required: true, message: "Domain is required"}]})(
                                <Select disabled={Boolean(offices)} size={"default"} id={"domain"} className={"domain"}
                                        placeholder={"Select Domain..."} onChange={this.handleDomainChange}>
                                    {this.renderDomains()}
                                </Select>
                            )
                        }

                    </Form.Item>

                    {this.getOfficeAuthorities()}
                    {this.getPotentialParents()}

                    <Form.Item {...this.formItemLayout} label={"Station"}>

                        {
                            getFieldDecorator(`geoLocationId`, {
                                rules: [{required: true, message: 'Station is required'}],
                            })(
                                <Select size={"default"} id={"station"} className={"station"}
                                        placeholder={"Select Station..."}>
                                    {this.renderStations()}
                                </Select>
                            )
                        }

                    </Form.Item>


                    {
                        processing ?
                            <NewLoader/>
                            :
                            null
                    }
                    {this.hasNotOfficeAuthority()?
                        <h6 style={{color: "red"}}>No Office Authority Exists!</h6> : null
                    }

                    {this.renderButtons()}
                </Form>
            </div>
        )
    };


    getBreadCrumbs = () => {

        let officeState = this.isEditState();

        let text = officeState && officeState.office ? "Update Office" : "Create Office";

        let breadCrumbsJSON = [
            {
                title: "Offices",
                returnPath: '/dashboard/offices'
            },
            {
                title: text
            }
        ];
        return (
            <BreadCrumb className={"form-crumb"} style={{fontSize: "20px"}} crumbsJSON={breadCrumbsJSON}/>
        )
    };

    render() {

        let containerStyle = {
            border: "2px solid rgba(231, 232, 237,0.5)",
            padding: "unset",
            borderRadius: "4px",
            ...this.props.style
        };

        return (
            <div className={"container-fluid"}>
                {this.getBreadCrumbs()}
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <div style={containerStyle} className={"custom-form"}>
                            {this.getHeader()}
                            {this.getBody()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        formErrors: state.server_reducer.fieldErrors,
        processing: state.server_reducer.processing,
        activeUser: state.user_reducer.activeUser,
        domains: state.server_reducer.projectDomains
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        createUser: (url, data, superAdmin = false, successCB, errorCB, succesText) => dispatch(createUser(url, data, superAdmin, successCB, errorCB, succesText)),
        createOffice: (data, successCB, errorCB, successText) => dispatch(createOffice(data, successCB, errorCB, successText)),
        updateOffice: (officeId, data, successCB, errorCB, successText) => dispatch(updateOffice(officeId, data, successCB, errorCB, successText)),

        fetchAuthorities: (departmentId, domainId, cb) => dispatch(getDepartmentAuthorities(departmentId, domainId, cb)),
        fetchPotentialParents: (authId, cb) => dispatch(getPotentialParents(authId, cb))
    }
};

const formCreated = Form.create()(CreateOffice);
const connected = connect(mapStateToProps, mapDispatchToProps)(formCreated);
export default withRouter(connected);