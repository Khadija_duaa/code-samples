import React from 'react';
import {getAllDepartments, createDepartments, updateDepartment} from "../../../../store/server/server-actions";
import {connect} from 'react-redux';
import {compareAsc, compareDesc, getObjectValue} from "../../../../utils/common-utils";
import NewLoader from '../../../Loader/NewLoader';
import {Icon} from 'antd';
import 'antd/dist/antd.css';
import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import HeaderTitle from "../../../../Components/HeaderTitle/HeaderTitle";
import TableContainer from "../../../../Components/Table/TableContainer/TableContainer";
import DepartmentForm from './DepartmentForms';
import SortableHeader from "../../../../Components/Table/SortableHeader/SortableHeader";
import RowButtonsList from "../../../../Components/Buttons/RowButtonList/RowButtonsList";
import ConfirmationModal from '../../../../Components/ModalFactory/MessageModal/ConfirmationModal';


const searchFields = ['name'];

class Departments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            departments: null,
            showModal: false,
            isAdd: false,
            formErrors: null,
            showConfirmModal: false
        }
    };

    componentDidMount() {
        this.fetchAllDepartments();
    }

    fetchAllDepartments = () => {
        this.props.getAllDepartments((departments) => {
            this.setState({allDepartments: departments});
        });
    };


    /*************************************** MODAL ***********************************/

    handleConfirmClick = () => {
        let {isUpdate, departmentData} = this.state;

        if (!isUpdate) {
            this.props.createDepartments(departmentData, () => {
                this.setState({
                    showModal: false,
                    showConfirmModal: false,
                    isUpdate: false,
                    departmentData: null,
                    isAdd: false,
                    formErrors: null
                }, () => {
                    this.fetchAllDepartments();
                })}, (err) => this.setState({showConfirmModal: false, departmentData: null, isUpdate: false, formErrors: err})
            )
        } else {
            this.props.updateDepartment(departmentData, this.state.departmentId, () => {
                this.setState({
                    showModal: false,
                    showConfirmModal: false,
                    isUpdate: false,
                    departmentData: null,
                    isAdd: false,
                    formErrors:false,
                }, () => {
                    this.fetchAllDepartments();
                });
            }, (err) => {
                this.setState({showConfirmModal: false, departmentData: null, isUpdate: false, formErrors: err})
            })
        }
    };

    handleCancelClick = () => {
        this.setState({showConfirmModal: false})
    };

    toggleConfirmationModal = (data, isUpdate = false) => {
        this.setState({showConfirmModal: !this.state.showConfirmModal, departmentData: data, isUpdate})
    };

    renderConfirmationModal = () => {
        let {isAdd} = this.state;

        let message = `Are you sure, you want to ${isAdd ? 'create' : 'update'} department?`;

        return (
            <ConfirmationModal isOpen={this.state.showConfirmModal}
                               okCB={this.handleConfirmClick} cancelCB={this.handleCancelClick}
                               message={message}/>
        );
    };

    // Field Modal
    toggleModal = () => {
        this.setState({showModal: !this.state.showModal})
    };

    renderFieldsModal = () => {
        let headerText = this.state.isAdd ? "Create New Department" : "Update Department";
        let formProps = {
            isAdd: this.state.isAdd,
            createCB: (data) => this.toggleConfirmationModal(data),
            updateCB: (data) => this.toggleConfirmationModal(data, true),
            cancelCB: this.toggleModal,
            departmentName: this.state.name,
            formErrors: this.state.formErrors
        };

        return (
            <div className={"col-md-12"}>
                <Modal isOpen={this.state.showModal} toggle={this.toggleModal} style={{width: "100%"}}>
                    <ModalHeader style={{border: "unset", WebkitTextStroke: "0.5px"}}>
                        {headerText}
                    </ModalHeader>

                    <ModalBody>
                        <DepartmentForm {...formProps}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    };

    /*************************************** END ***********************************/

    /********************************* SORTING LAYOUTS ****************************/

    sortedParams = ["Name"];

    ascendingSort = (headerKey) => {
        if (!this.state.allDepartments) return null;
        let arrayCopy = [...this.state.allDepartments];
        arrayCopy.sort(compareAsc(headerKey));
        this.setState({allDepartments: arrayCopy});
    };

    descendingSort = (headerKey) => {
        if (!this.state.allDepartments) return null;
        let arrayCopy = [...this.state.allDepartments];
        arrayCopy.sort(compareDesc(headerKey));
        this.setState({allDepartments: arrayCopy});
    };

    getSortedLayout = (headerKey) => {
        if (this.sortedParams.includes(headerKey)) {
            return (
                <div>
                    {headerKey}
                    <span style={{
                        float: "right",
                        display: "grid",
                        marginRight: "20px",
                        height: "-webkit-fill-available"
                    }}>
                        <Icon type="caret-up" onClick={() => this.ascendingSort(headerKey.toLowerCase())}/>
                        <Icon type="caret-down" onClick={() => this.descendingSort(headerKey.toLowerCase())}/>
                    </span>
                </div>
            )
        } else {
            return headerKey;
        }
    };

    /*************************************** END ***********************************/

    getTitle = () => {
        return (
            <HeaderTitle>
                <h3>Departments</h3>
            </HeaderTitle>
        )
    };


    searchDepartments = (searchStr, searchParams = []) => {
        if (!this.props.departments) return null;
        const departments = [...this.props.departments];

        if (searchStr) {
            let filteredDepartments = [];
            departments.forEach(department => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(department, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredDepartments.push(department);
                        break;
                    }
                }
            });
            this.setState({departments: filteredDepartments});
        }
        else {
            this.setState({departments: null});
        }
    };

    getButtonList = () => {

        let buttonListProps = {
            buttonText: "+ Create Department",
            onCreateClick: () => this.setState({showModal: true, isAdd: true,formErrors:null}),
            onSearchChange: (e) => this.searchDepartments(e.target.value, searchFields)
        };
        return (
            <RowButtonsList {...buttonListProps}/>
        )
    };


    tableHeader = () => {
        let tableHeaders = ["Name", "Edit Department"];

        let headerProps = {
            tableHeaders,
            getSortedLayout: (head) => this.getSortedLayout(head)
        };

        return (
            <SortableHeader {...headerProps}/>
        );
    };

    tableBody = () => {
        if (!this.state.allDepartments)
            return null;

        let departments = this.state.departments ? this.state.departments : this.state.allDepartments;

        let bodyJSX = departments.map((dept, index) => {
            return (
                <tr key={index}>
                    <td>{dept.name}</td>
                    <td colSpan={2}>
                        <a onClick={() => this.setState({
                            showModal: true,
                            departmentId: dept.uuid,
                            name: dept.name,
                            isAdd: false,
                            formErrors:null,
                        })}>
                            Edit
                        </a>
                    </td>
                </tr>
            )
        });


        return (
            <tbody>
            {bodyJSX}
            </tbody>
        )
    };

    getDepartmentData = () => {
        return (
            <TableContainer>
                {this.tableHeader()}
                {this.tableBody()}
            </TableContainer>
        )
    };


    render() {
        return (
            <>
                {this.renderConfirmationModal()}
                {this.renderFieldsModal()}
                {this.getTitle()}
                {this.getButtonList()}
                {this.props.processing ?

                    <NewLoader/>
                    :
                    this.getDepartmentData()
                }
            </>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getAllDepartments: (cb) => dispatch(getAllDepartments(cb)),
        createDepartments: (data, cb, errorCB) => dispatch(createDepartments(data, cb, errorCB)),
        updateDepartment: (data, id, cb, errorCB) => dispatch(updateDepartment(data, id, cb, errorCB))
    }
};

const mapStateToProps = state => {
    return {
        departments: state.server_reducer.departments,
        processing: state.server_reducer.processing
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(Departments);
export default connected;