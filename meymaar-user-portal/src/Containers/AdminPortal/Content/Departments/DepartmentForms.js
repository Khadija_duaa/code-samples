import React from 'react';
import {Input, Checkbox, Form, Button} from 'antd';
import {BUTTON_COLOR} from "../../../../utils/common-utils";

class DepartmentForm extends React.Component {

    // Handeling Submit Department
    handleDepartmentSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldValues) => {
            if (err) {
                return;
            }

            let {isAdd, createCB, updateCB} = this.props;

            if (isAdd) {
                createCB && createCB(fieldValues);
            } else {
                updateCB && updateCB(fieldValues);
            }
        })
    };

    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };

    buttonItemLayout = {
        wrapperCol: {offset: 8},
    };


    renderFormButtons = () => {
        let buttonText = this.props.isAdd ? "Create" : "Update";
        return (
            <Form.Item {...this.buttonItemLayout} >

                <Button htmlType="submit" style={{
                    height: "40px",
                    width: "40%",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>{buttonText}</Button>

                <Button style={{height: "40px", width: "40%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.cancelCB}>Cancel</Button>

            </Form.Item>

        )
    };

    render() {
        let {getFieldDecorator, getFieldValue} = this.props.form;
        let nameLabel = this.props.isAdd ? "Name" : "New Name";
        let {formErrors} = this.props;
        let text = getFieldValue('canContract') ? "Department will contract" : "Department will not contract";
        return (
            <div>
                {formErrors ? <h6 style={{color: "red", textAlign: "center"}}>{formErrors}</h6> : null}
                <Form layout={"horizontal"} onSubmit={this.handleDepartmentSubmit}>

                    {
                        !this.props.isAdd ?
                            <Form.Item label={"Current Name"} {...this.formItemLayout}>
                                <Input defaultValue={this.props.departmentName} disabled={true}/>
                            </Form.Item>
                            :
                            null
                    }


                    <Form.Item label={nameLabel} {...this.formItemLayout}>
                        {getFieldDecorator('name', {
                            rules: [{
                                required: true,
                                message: "Name is required",
                                whitespace: true
                            }]
                        })(<Input
                            placeholder={"Department Name"}/>)}
                    </Form.Item>

                    {
                        this.props.isAdd && <Form.Item {...this.formItemLayout} label={"Can Contract ? "}>
                            {getFieldDecorator('canContract', {initialValue: false, valuePropName: 'checked'})(
                                <Checkbox/>)}
                        </Form.Item>
                    }

                    {this.props.isAdd ?
                        <p style={{color: "green", fontSize: "16px", fontWeight: "bold"}}>* {text}</p> : null}

                    {this.renderFormButtons()}
                </Form>
            </div>
        )

    }
}


export default Form.create()(DepartmentForm);