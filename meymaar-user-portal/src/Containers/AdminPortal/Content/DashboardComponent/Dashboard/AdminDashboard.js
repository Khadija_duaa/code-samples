import React from 'react';

class AdminDashboard extends React.Component {

    getOffices = () => {
        return (
            <div className="col-md-6 mb-4">
                <div className="dashboard-box p-3">
                    <span className="dashboard-box-title">Total Offices</span>
                    <span className="dashboard-box-number">190</span>
                    <span className="dashboard-box-sub-title">Current Office</span>
                    <span className="dashboard-box-sub-detail">E-in-C Branch</span>
                </div>
            </div>
        )
    };


    getUsers = () => {
        return (
            <div className="col-md-6 mb-4">
                <div className="dashboard-box p-3">
                    <span className="dashboard-box-title">Total Users</span>
                    <span className="dashboard-box-number">4,206</span>
                    <span className="dashboard-box-sub-title">Domains</span>
                    <span className="dashboard-box-sub-detail">Army, PAF, Navy, DP</span>
                </div>
            </div>
        )
    };
    getHorizontalLines = () => {
        return (
            <>

                {/* <div className="col-md-6 mb-4">
                    <div className="dashboard-box p-3 border-0">
                    </div>
                </div>
                <div className="col-md-6 mb-4">
                    <div className="dashboard-box p-3 border-0">
                    </div>
                </div>
                <div className="col-md-6 mb-4">
                    <div className="dashboard-box p-3 border-0">
                    </div>
                </div>
                <div className="col-md-6 mb-4">
                    <div className="dashboard-box p-3 border-0">
                    </div>
                </div> */}
            </>
        )
    };

    getCalendar = ()=>{
      return(
          <div className="col-md-12 mb-5">
              <div className="dashboard-calendar-box p-3">
                  <img alt={"calendar"} src="images/calendar.svg" className="img-fluid"/>
              </div>
          </div>
      )
    };

    getRecentActivites = ()=>{
        return(
            <div className="form-container mb-5 pl-5 pr-5">
                <div className="title-row pt-4 pb-4">
                    <h3 className="mb-0">Recent Activities</h3>
                </div>
                <div className="detail-row pt-3 pb-3">
                    <ul className="fa-ul ml-0 activity-list">
                        <li>
                            <span className="fa-li"><i className="fas fa-user"/></span>
                            You created an office “GE (A) - II Kharian Cantt
                            <a href="#">Preview</a>
                            <span className="activity-date">28/01/19</span>
                        </li>
                        <li>
                            <span className="fa-li"><i className="fas fa-user"/></span>
                            You assigned create, read and edit privileges to...
                            <a href="#">Preview</a>
                            <span className="activity-date">24/01/19</span>
                        </li>
                        <li>
                            <span className="fa-li"><i className="fas fa-user"/></span>
                            You added an officer “Muhammad Zia” in GE (A)...
                            <a href="#">Preview</a>
                            <span className="activity-date">18/01/19</span>
                        </li>
                        <li>
                            <span className="fa-li"><i className="fas fa-user"/></span>
                            You added an officer “Muhammad Zia” in GE (A)...
                            <a href="#">Preview</a>
                            <span className="activity-date">18/01/19</span>
                        </li>
                        <li>
                            <span className="fa-li"><i className="fas fa-user"/></span>
                            You added an officer “Muhammad Zia” in GE (A)...
                            <a href="#">Preview</a>
                            <span className="activity-date">16/01/19</span>
                        </li>
                        <li>
                            <span className="fa-li"><i className="fas fa-user"/></span>
                            You added an officer “Muhammad Zia” in GE (A)...
                            <a href="#">Preview</a>
                            <span className="activity-date">16/01/19</span>
                        </li>
                    </ul>
                </div>
            </div>
        )
    };

    render() {

        return (
            <div className="col-12">

                <div className="pl-3 mb-5"><h3>Overview</h3></div>

                <div className="row">
                    <div className="col-md-6">
                        <div className="row">
                            {this.getOffices()}
                            {this.getUsers()}
                            {this.getHorizontalLines()}
                        </div>


                        <div className="row">
                            {this.getCalendar()}
                        </div>

                    </div>

                    <div className="col-md-6 mb-5 recent-activity-container">
                        {this.getRecentActivites()}
                    </div>
                </div>
            </div>

        )
    }
}

export default AdminDashboard;