import React from 'react';
import {getAllStations, updateStation, createStation} from "../../../../store/server/server-actions";
import {connect} from 'react-redux';
import {compareAsc, compareDesc, getObjectValue} from "../../../../utils/common-utils";
import NewLoader from '../../../Loader/NewLoader';
import {Icon} from 'antd';
import 'antd/dist/antd.css';
import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import HeaderTitle from "../../../../Components/HeaderTitle/HeaderTitle";
import TableContainer from "../../../../Components/Table/TableContainer/TableContainer";
import StationForm from './StationForm';
import SortableHeader from "../../../../Components/Table/SortableHeader/SortableHeader";
import RowButtonsList from "../../../../Components/Buttons/RowButtonList/RowButtonsList";

const searchFields = ['name'];

class Stations extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stations: null,
            showModal: false,
            isAdd: false,
            formErrors: null
        }
    };

    componentDidMount() {
        this.fetchAllStations();
    }

    fetchAllStations = () => {
        this.props.getAllStations((stations) => {
            this.setState({allStations: stations});
        });
    };

    /********************************* Events ****************************************/
    handleCreateStationClick = (fieldValues) => {
        this.props.createStation(fieldValues, () => {
            this.setState({showModal: false, isAdd: false, formErrors: null}, () => {
                this.fetchAllStations();
            }, (err) => this.setState({formErrors: err}));
        })
    };

    handleUpdateStationClick = (fieldValues) => {
        this.props.updateStation(fieldValues, this.state.stationId, () => {
            this.setState({showModal: false, isAdd: false}, () => {
                this.fetchAllStations();
            });
        }, (err) => {
            this.setState({formErrors: err})
        })
    };
    /********************************* END ********************************************/

    /*************************************** MODAL ***********************************/

        // Field Modal
    toggleModal = () => {
        this.setState({showModal: !this.state.showModal})
    };

    renderFieldsModal = () => {
        let headerText = this.state.isAdd ? "Create New Station" : "Update Station";
        let formProps = {
            isAdd: this.state.isAdd,
            createCB: (data) => this.handleCreateStationClick(data),
            updateCB: (data) => this.handleUpdateStationClick(data),
            cancelCB: this.toggleModal,
            stationName: this.state.name,
            formErrors: this.state.formErrors
        };

        return (
            <div className={"col-md-12"}>
                <Modal isOpen={this.state.showModal} toggle={this.toggleModal} style={{width: "100%"}}>
                    <ModalHeader style={{border: "unset", WebkitTextStroke: "0.5px"}}>
                        {headerText}
                    </ModalHeader>

                    <ModalBody>
                        <StationForm {...formProps}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    };

    /*************************************** END ***********************************/

    /********************************* SORTING LAYOUTS ****************************/

    sortedParams = ["Name"];

    ascendingSort = (headerKey) => {
        if(!this.state.allStations) return null;

        let arrayCopy = [...this.state.allStations];
        arrayCopy.sort(compareAsc(headerKey));
        this.setState({allStations: arrayCopy});
    };

    descendingSort = (headerKey) => {
        if(!this.state.allStations) return null;

        let arrayCopy = [...this.state.allStations];
        arrayCopy.sort(compareDesc(headerKey));
        this.setState({allStations: arrayCopy});
    };

    getSortedLayout = (headerKey) => {
        if (this.sortedParams.includes(headerKey)) {
            return (
                <div>
                    {headerKey}
                    <span style={{
                        float: "right",
                        display: "grid",
                        marginRight: "20px",
                        height: "-webkit-fill-available"
                    }}>
                        <Icon type="caret-up" onClick={() => this.ascendingSort(headerKey.toLowerCase())}/>
                        <Icon type="caret-down" onClick={() => this.descendingSort(headerKey.toLowerCase())}/>
                    </span>
                </div>
            )
        } else {
            return headerKey;
        }
    };

    /*************************************** END ***********************************/

    getTitle = () => {
        return (
            <HeaderTitle>
                <h3>Stations</h3>
            </HeaderTitle>
        )
    };


    searchStations = (searchStr, searchParams = []) => {
        if(!this.props.stations) return null;
        const stations = [...this.props.stations];

        if (searchStr) {
            let filteredStations = [];
            stations.forEach(station => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(station, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredStations.push(station);
                        break;
                    }
                }
            });
            this.setState({stations: filteredStations});
        }
        else {
            this.setState({stations: null});
        }
    };

    getButtonList = () => {

        let buttonListProps = {
            buttonText: "+ Create Station",
            onCreateClick: () => this.setState({showModal: true, isAdd: true}),
            onSearchChange: (e) => this.searchStations(e.target.value, searchFields)
        };
        return (
            <RowButtonsList {...buttonListProps}/>
        )
    };


    tableHeader = () => {
        let tableHeaders = ["Name", "Edit Station"];

        let headerProps = {
            tableHeaders,
            getSortedLayout: (head) => this.getSortedLayout(head)
        };

        return (
            <SortableHeader {...headerProps}/>
        );
    };

    tableBody = () => {
        if (!this.state.allStations)
            return null;

        let stations = this.state.stations ? this.state.stations : this.state.allStations;

        let bodyJSX = stations.map((station, index) => {
            return (
                <tr key={index}>
                    <td>{station.name}</td>
                    <td colSpan={2}>
                        <a onClick={() => this.setState({
                            showModal: true,
                            stationId: station.uuid,
                            name: station.name,
                            isAdd: false
                        })}>
                            Edit
                        </a>
                    </td>
                </tr>
            )
        });


        return (
            <tbody>
            {bodyJSX}
            </tbody>
        )
    };

    getStationData = () => {
        return (
            <TableContainer>
                {this.tableHeader()}
                {this.tableBody()}
            </TableContainer>
        )
    };


    render() {
        return (
            <>
                {this.renderFieldsModal()}
                {this.getTitle()}
                {this.getButtonList()}
                {this.props.processing ?

                    <NewLoader/>
                    :
                    this.getStationData()
                }
            </>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getAllStations: (cb) => dispatch(getAllStations(cb)),
        createStation: (data, cb, errorCB) => dispatch(createStation(data, cb, errorCB)),
        updateStation: (data, id, cb, errorCB) => dispatch(updateStation(data, id, cb, errorCB))
    }
};

const mapStateToProps = state => {
    return {
        stations: state.server_reducer.stations,
        processing: state.server_reducer.processing
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(Stations);
export default connected;