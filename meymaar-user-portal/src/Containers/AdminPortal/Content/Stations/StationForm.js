import React from 'react';
import {Input, Form, Button} from 'antd';
import {BUTTON_COLOR} from "../../../../utils/common-utils";

class StationForm extends React.Component {

    // Handeling Submit Stations
    handleStationClick = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldValues) => {
            if (err) {
                return;
            }

            let {isAdd, createCB, updateCB} = this.props;

            if (isAdd) {
                createCB && createCB(fieldValues);
            } else {
                updateCB && updateCB(fieldValues);
            }
        })
    };

    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };

    buttonItemLayout = {
        wrapperCol: {offset: 8},
    };


    renderFormButtons = () => {
        let buttonText = this.props.isAdd ? "Create" : "Update";
        return (
            <Form.Item {...this.buttonItemLayout} >

                <Button htmlType="submit" style={{
                    height: "40px",
                    width: "40%",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>{buttonText}</Button>

                <Button style={{height: "40px", width: "40%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.cancelCB}>Cancel</Button>

            </Form.Item>

        )
    };

    render() {
        let {getFieldDecorator} = this.props.form;
        let nameLabel = this.props.isAdd ? "Name" : "New Name";

        return (
            <Form layout={"horizontal"} onSubmit={this.handleStationClick}>
                {
                    !this.props.isAdd ?
                        <Form.Item label={"Current Name"} {...this.formItemLayout}>
                            <Input defaultValue={this.props.stationName} disabled={true}/>
                        </Form.Item>
                        :
                        null
                }


                <Form.Item label={nameLabel} {...this.formItemLayout}>
                    {getFieldDecorator('name', {
                        rules: [{
                            required: true,
                            message: "Name is required",
                            whitespace: true
                        }]
                    })(<Input
                        placeholder={"Station Name"}/>)}
                </Form.Item>


                {this.renderFormButtons()}
            </Form>
        )

    }
}


export default Form.create()(StationForm);