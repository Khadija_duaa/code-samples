import React from 'react';
import NewLoader from '../Loader/NewLoader';
import {loginUser} from "../../store/user/user-actions";
import {connect} from 'react-redux';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            error: ''
        }
    }

    ShowHidePassword = () => {
        let password = document.getElementById("txtPassword");
        let passwordVisibility = document.getElementById("passwordVisibility");
        if (password.type.toLowerCase() === "password") {
            password.type = "text";
            passwordVisibility.className = "far fa-eye login-password-display";
        }
        else {
            password.type = "password";
            passwordVisibility.className = "far fa-eye-slash login-password-display";
        }
    };

    handleChange = (event, name) => {
        this.setState({[name]: event.target.value});
    };

    handleLoginSubmit = (e) => {
        e.preventDefault();
        this.props.setloginUser(this.state, (error) => {
            this.setState({error})
        });

    };

    render() {
        return (
            <div className={"form-login"}>
                <img src="/images/logo-meymaar.svg" className="img-fluid" alt=""/>
                <p className="login-introduction">Please enter credentials to access the Meymaar Portal</p>
                <form onSubmit={this.handleLoginSubmit}>

                    {
                        this.props.processing ?
                            <div style={{textAlign: 'center'}}>
                                <NewLoader/>
                            </div> :
                            null
                    }

                    {
                        this.state.error ?
                            <p className={"login-introduction"} style={{color: "#f5222d"}}>{this.state.error}</p>
                            : null
                    }
                    <div>
                        <input type="text" className="custom-input form-control" id="txtUsername"
                               name="txtUsername"
                               placeholder="Username" required autoFocus value={this.state.username}
                               onChange={(e) => this.handleChange(e, 'username')}/>
                    </div>


                    <div className="login-password-container">
                        <input type="password" className={"mt-3 custom-input form-control"}
                               id="txtPassword" name="txtPassword"
                               placeholder="Password" required value={this.state.password}
                               onChange={(e) => this.handleChange(e, 'password')}/>
                        <i id="passwordVisibility" className={"far fa-eye-slash login-password-display"}
                           onClick={this.ShowHidePassword}/>
                    </div>
                    {/*<p className="text-muted forget-title">*/}
                        {/*<a href="#" className="text-muted forget-pwd">Forgot Password?</a>*/}
                    {/*</p>*/}
                    <button className="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        activeUser: state.user_reducer.activeUser,
        userAuthenticated: state.user_reducer.authenticated,
        processing: state.user_reducer.processing
    }
};
const mapDispatchToProps = dispatch => {
    return {
        setloginUser: (data, cb) => dispatch(loginUser(data, cb)),
    }
};

const connected = connect(mapStateToProps,mapDispatchToProps)(LoginForm);
export default connected