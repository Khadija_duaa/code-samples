import React from 'react';
import {connect} from 'react-redux';
import UserPortal from './UserPortal/UserPortal';

class Dashboard extends React.Component {


    render() {
        return (
            <div>
                <UserPortal/>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        activeUser: state.user_reducer.activeUser
    }
};

const connected = connect(mapStateToProps)(Dashboard);
export default connected;