import React from 'react';
import { Row, Col, Layout} from 'antd';


class NewLoaderSmall extends React.Component {
    render() {
        return (
            <Layout>
                <Row type="flex" justify="space-around" align="middle" className="new-loader-small">
                    <Col span={24}><img alt={"small"} src="/images/spinner.gif" /></Col>
                </Row>
            </Layout>
        )
    }
}


export default NewLoaderSmall;