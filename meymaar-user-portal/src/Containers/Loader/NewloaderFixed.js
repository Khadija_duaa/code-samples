import React from 'react';
import { Row, Col, Layout} from 'antd';


class NewLoaderFixed extends React.Component {
    render() {
        return (
            <Layout>
                <Row type="flex" justify="space-around" align="middle" className="new-loader-fixed">
                    <Col span={24}><img alt={"spinner"} src="/images/spinner.gif" /></Col>
                </Row>
            </Layout>
        )
    }
}


export default NewLoaderFixed;