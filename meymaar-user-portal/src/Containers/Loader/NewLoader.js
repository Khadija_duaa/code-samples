import React from 'react';
import { Row, Col, Layout} from 'antd';


class NewLoader extends React.Component {
    render() {
        return (
            <Layout className="new-full-screen-loader">
                <Row>
                    <Col span={24}>Loading...</Col>
                </Row>
            </Layout>
        )
    }
}


export default NewLoader;