import React from 'react';
import { Row, Col, DatePicker,  Form, InputNumber, Input,  Modal, Button} from 'antd';
import axio from '../../../../utils/axios';

class AddContractorBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            showHide:true,
            radioValue:1,
            showDetailsBox: true,
            visible:false,
        }
    }
    showModal=()=>{
        this.setState({
            visible: true,
        });
    };
    handleOk = () => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            axio.post('/contractor-management/contractor/create',{
                firm_id: values.firm_id,
                first_name: values.first_name,
                last_name: values.last_name,
                email: values.email,
                phone: values.phone,
                thumb_print: values.thumb_print,
                username: values.username,
                password: values.password,
                cnic: values.cnic,
                address: values.address,
                gender: values.gender,
                dob: values.dob,
            })
            .catch(error=> {
                console.log('error', error);
            });
            console.log('new device add ', values);
          }
          this.handleCancel();
          this.props.getAllContractors();
        });
    };
        render(){
        console.log('tableClickData', this.props.tableClickData);
          const { getFieldDecorator } = this.props.form; 
            return(
                        <>
                           <button className="custom-button custom-button-dark create-button margin-60"  onClick={this.showModal}>+ Add User</button>
                            <Modal
                                title="Add User"
                                visible={this.state.visible}
                                className="custom-table-popup"
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                                destroyOnClose={true}
                                footer={false}
                            >
                            <Form onSubmit={this.handleSubmit}>
                                            <Row className="custom-popup-row">
                                                {/* <p className="center">Please fill the following the input fields</p> */}
                                                <Col span={24} offset={4} className="custom-form-wrap">
                                                    <Col span={16}>
                                                        <Form.Item >
                                                            {getFieldDecorator('firm_id', { 
                                                                initialValue: this.props.firmId
                                                                })(<InputNumber min={1} type="hidden"/>)}
                                                        </Form.Item>

                                                        <Form.Item label="First Name">
                                                            {getFieldDecorator('first_name', {
                                                                rules: [{ required: true, message: 'Please enter first name!' }]
                                                                })(<Input />)}
                                                        </Form.Item>

                                                        <Form.Item label="Last Name">
                                                            {getFieldDecorator('last_name', {
                                                                rules: [{ required: true, message: 'Please enter last nanme!' }]
                                                                })(<Input />)}
                                                        </Form.Item>

                                                        <Form.Item label="Email">
                                                            {getFieldDecorator('email', {
                                                                rules: [{ required: true, message: 'Please enter email!' }]
                                                                })(<Input type="email"/>)}
                                                        </Form.Item>

                                                        <Form.Item label="Phone">
                                                            {getFieldDecorator('phone', {
                                                                rules: [{ required: true, message: 'Please enter phone number!' }]
                                                                })(<Input />)}
                                                        </Form.Item>

                                                        <Form.Item label="Thumb Print">
                                                            {getFieldDecorator('thumb_print', {
                                                                rules: [{ required: true, message: 'Please enter thumbprint!' }]
                                                                })(<Input />)}
                                                        </Form.Item>

                                                        <Form.Item label="Username">
                                                            {getFieldDecorator('username', {
                                                                rules: [{ required: true, message: 'Please enter username!' }]
                                                                })(<Input />)}
                                                        </Form.Item>

                                                        <Form.Item label="Password">
                                                            {getFieldDecorator('password', {
                                                                rules: [{ required: true, message: 'Please enter password!' }]
                                                                })(<Input type="password"/>)}
                                                        </Form.Item>

                                                        <Form.Item label="CNIC" className="custom-ant-input-number">
                                                            {getFieldDecorator('cnic', { 
                                                                // initialValue: 3000/1024
                                                                rules: [{ required: true, message: 'Please enter CNIC' }]
                                                                })(<InputNumber min={1} />)}
                                                        </Form.Item>

                                                        <Form.Item label="Address">
                                                            {getFieldDecorator('address', {
                                                                rules: [{ required: true, message: 'Please enter address!' }]
                                                                })(<Input />)}
                                                        </Form.Item>

                                                        <Form.Item label="Gender">
                                                            {getFieldDecorator('gender', {
                                                                rules: [{ required: true, message: 'Please select date!' }]
                                                                })(<Input />)}
                                                        </Form.Item>

                                                        {/* <Form.Item label="DOB">
                                                            {getFieldDecorator('dob', {
                                                                })(<Input />)}
                                                        </Form.Item> */}
                                                        <Form.Item
                                                            label="DOB"
                                                            >
                                                            {getFieldDecorator('date-dob',{
                                                                rules: [{ required: true, message: 'Please select date!' }]
                                                            })(
                                                                <DatePicker />
                                                            )}
                                                        </Form.Item>

                                                        <Form.Item >
                                                            <Row type="flex" className="margin-top-40">
                                                                    <Col span={12} className="edit-black-buttons float-right">
                                                                        <Button type="primary" value="large" htmlType="submit">
                                                                        Add User
                                                                        </Button>
                                                                    </Col>
                                                                    <Col span={12} className="unfill-edit-black-buttons float-left">
                                                                        <Button type="primary" value="large" onClick={this.handleCancel}>Cancel</Button>
                                                                    </Col>
                                                            </Row>
                                                        </Form.Item>
                                                    </Col>
                                                </Col>
                                            </Row>
                                        </Form>
                            </Modal>
                        </>
            )
    }
}





const AddContractor = Form.create({})(AddContractorBtn);
export default AddContractor;