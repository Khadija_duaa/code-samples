import React from 'react';
import {Row, Col, Table, Form, DatePicker, message, Input, Select, Modal, Button} from 'antd';
import axio from '../../../../utils/axios';
import InputMask from 'react-input-mask';
import UpdateUser from './updateUser';
import moment from 'moment';
import {isMobile, isValidPhone} from "../../../../utils/validations";
import {handleError} from "../../../../store/store-utils";
import {NOTIFICATION_TIME} from "../../../../utils/common-utils";

const {Option} = Select;

class ContratorsView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showHide: true,
            radioValue: 1,
            visible: false,
            visible2: false,
            showDetailsBox: true,
            contratorsById: [],
            searchText: '',
            value: '',
            userClickData: [],
            modalOpen: false,
        }
    }


    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({searchText: selectedKeys[0]});
    };

    handleReset = (clearFilters) => {
        clearFilters();
        this.setState({searchText: ''});
    };

    componentDidMount() {
        console.log('firmId iaaaaaaaaaaaa', this.props.firmId);
        this.getAllContractors();
    }

    getAllContractors = () => {
        axio.get(`/contractor-management/contractor/firm/getContractorsByFirm/${this.props.firmId}`,)
            .then(res => {
                console.log('get all contarctor by clicking firms', res.data.firms.contractor);
                this.setState({
                    contratorsById: res.data.firms.contractor
                })
            })
            .catch(error => {
                console.log('error', error);
            });
    };
    showHide = () => {
        this.setState({
            showHide: !this.state.showHide,
        })
    };
    radioValue = (e) => {
        this.setState({
            radioValue: e.target.value,
        });
    };
    closePage = () => {
        this.setState({
            showDetailsBox: false,
        });
    }
    showModal = () => {
        this.setState({
            visible: true,
        });
    }
    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    }

    showModal2 = () => {
        this.setState({
            visible2: true,
        });
    }

    handleOk2 = () => {
        console.log('handleOk')
        this.setState({
            visible2: false,
        });
    }

    handleCancel2 = () => {
        console.log('handleCancel2');
        this.setState({
            visible2: false,
        });
    };
    success = (msg) => {
        message.success(msg);
    };
    errors = (msg) => {
        message.error(msg);
    };
    maskChange = (event) => {
        this.setState({
            value: event.target.value
        });
    };

    mobileCountryCodeValidator = (rule, value, callback) => {

        if (value && (!isValidPhone(value) || !isMobile(value))) {
            callback('Invalid Mobile Number')
        } else {
            callback()
        }

    };

    validateCNIC = (rules, value, callback) => {
        if (value) {
            let cnic_no_regex = /^[0-9+]{5}-[0-9+]{7}-[0-9]{1}/;

            if (!cnic_no_regex.test(value)) {
                callback('CNIC Format XXXXX-XXXXXXX-X');
            }
            else {
                callback();
            }
        } else {
            callback();
        }
    };

    ageValidator = (rule, value, callback) => {

        let startDate = moment(new Date(), "YYYY-MM-DD");
        let endDate = moment(value, "YYYY-MM-DD");
        let years = startDate.diff(endDate, 'year');

        if (years < 18) {
            callback("Minimum age limit is 18 years");
        } else {
            callback();
        }
    };


    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log('its values', values);
            if (!err) {
                const dates = {
                    'dob': values['dob'].format('YYYY-MM-DD'),
                };


                console.log('Received values of form: ', values, dates);
                axio.post('/contractor-management/contractor/create', {
                    firm_id: values.firm_id,
                    first_name: values.first_name,
                    last_name: values.last_name,
                    email: values.email && values.email ? values.email : null,
                    phone: values.phone,
                    thumb_print: values.thumb_print,
                    username: values.username,
                    password: values.password,
                    cnic: values.cnic,
                    address: values.address,
                    gender: values.gender,
                    dob: dates.dob,
                }).then(res => {
                    this.getAllContractors();
                    this.success('New Contractor Successfully Added!');
                    this.handleCancel();
                })
                    .catch((error) => {
                        let errorMessage = handleError(error);
                        message.error(errorMessage, NOTIFICATION_TIME);
                    });


            }

        });
    };


    firmUserClickData = (record) => {
        console.log('user click data is:', record);
        this.setState({
            modalOpen: true,
        })
        // console.log('this.state.userClickData.firmId', this.state.userClickData)
    }

    render() {
        // firms data  
        const contractorcolumns = [{
            title: 'Id',
            dataIndex: 'contractorId',
        }, {
            title: 'First Name',
            dataIndex: 'first_name',

        }, {
            title: 'Last Name',
            dataIndex: 'last_name',

        }, {
            title: 'Email',
            dataIndex: 'email',

        }, {
            title: 'Phone',
            dataIndex: 'phone',

        }, {
            title: 'Address',
            dataIndex: 'address',
        }, {
            title: 'Gender',
            dataIndex: 'gender',

        }
        ];

        const formItemLayout = {
            labelCol: {span: 24},
            wrapperCol: {span: 20},
        };


        const contractordata = this.state.contratorsById.map((data, index) => {
            return {
                id: data.id,
                contractorId: index + 1,
                first_name: data.first_name,
                last_name: data.last_name,
                email: data.email,
                phone: data.phone,
                gender: data.gender,
                address: data.address,
                dob: data.dob,
                cnic: data.cnic,
                created_at: data.created_at,
                updated_at: data.updated_at,
                username: data.username,
                password: data.password,

            }
        });
        const {getFieldDecorator} = this.props.form;
        return (
            <>

                <button className="custom-button custom-button-dark create-button margin-60" onClick={this.showModal}>+
                    Add User
                </button>
                <Modal
                    title="Add User"
                    visible={this.state.visible}
                    className="custom-table-popup"
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    destroyOnClose={true}
                    footer={false}
                >
                    <Form onSubmit={this.handleSubmit} layout={"horizontal"}>
                        <Row className="custom-popup-row">

                            {/* <p className="center">Please fill the following the input fields</p> */}
                            <Col span={24} offset={4} className="custom-form-wrap">
                                <Col span={16}>
                                    <Form.Item>
                                        {getFieldDecorator('firm_id', {
                                            initialValue: this.props.firmId
                                        })(<Input min={1} type="hidden"/>)}
                                    </Form.Item>

                                    <Form.Item label="First Name">
                                        {getFieldDecorator('first_name', {
                                            rules: [{
                                                required: true,
                                                message: 'Please enter first name!',
                                                whitespace: false
                                            }]
                                        })(<Input pattern="[A-Za-z0-9]+"/>)}
                                    </Form.Item>

                                    <Form.Item label="Last Name">
                                        {getFieldDecorator('last_name', {
                                            rules: [{required: true, message: 'Please enter last nanme!'}]
                                        })(<Input pattern="[A-Za-z0-9]+"/>)}
                                    </Form.Item>


                                    <Form.Item label="Email">
                                        {getFieldDecorator('email', {
                                            rules: [{
                                                type: 'email', message: 'Invalid email!',
                                            }, {
                                                required: false, message: 'Please enter email!'
                                            }]
                                        })(<Input/>)}
                                    </Form.Item>


                                    <Form.Item label="Phone">
                                        {getFieldDecorator('phone', {
                                            rules: [{required: true, message: 'Please enter phone number!'},
                                                {validator: this.mobileCountryCodeValidator},]
                                        })(<Input/>)}
                                    </Form.Item>

                                    <Form.Item label="Thumb Print">
                                        {getFieldDecorator('thumb_print', {
                                            rules: [{required: true, message: 'Please enter thumbprint!'}]
                                        })(<Input/>)}
                                    </Form.Item>

                                    <Form.Item label="Username">
                                        {getFieldDecorator('username', {
                                            rules: [{required: true, message: 'Please enter username!'}]
                                        })(<Input pattern="[A-Za-z0-9]+"/>)}
                                    </Form.Item>

                                    <Form.Item label="Password">
                                        {getFieldDecorator('password', {
                                            rules: [{required: true, message: 'Please enter password!'}]
                                        })(<Input type="password"/>)}
                                    </Form.Item>

                                    <Form.Item label="CNIC" {...formItemLayout}>
                                        {getFieldDecorator('cnic', {
                                            // initialValue: 3000/1024
                                            rules: [{required: true, message: 'Please enter CNIC'},
                                                {validator: this.validateCNIC},]
                                        })(<InputMask mask="99999-9999999-9" maskChar={' '}/>)}
                                    </Form.Item>

                                    <Form.Item label="Address">
                                        {getFieldDecorator('address', {
                                            rules: [{required: true, message: 'Please enter address!'}]
                                        })(<Input/>)}
                                    </Form.Item>

                                    {/* <Form.Item label="Gender">
                                                            {getFieldDecorator('gender', {
                                                                rules: [{ required: true, message: 'Please select date!' }]
                                                                })(<Input />)}
                                                        </Form.Item> */}


                                    <Form.Item label="Gender">
                                        {getFieldDecorator('gender', {
                                            rules: [{required: true, message: 'Please select gender'}],
                                        })(
                                            <Select className="custom-dropdown-menu" placeholder="Gender" size="large">
                                                <Option value={'Male'}>Male</Option>
                                                <Option value={'Female'}>Female</Option>
                                            </Select>
                                        )}
                                    </Form.Item>

                                    {/* <Form.Item label="DOB">
                                                            {getFieldDecorator('dob', {
                                                                })(<Input />)}
                                                        </Form.Item> */}
                                    <Form.Item label="DOB">
                                        {getFieldDecorator('dob', {
                                            rules: [{required: true, message: 'Please select date!'},
                                                {validator: this.ageValidator}]
                                        })(
                                            <DatePicker format="YYYY-MM-DD"/>
                                        )}
                                    </Form.Item>

                                    <Form.Item>
                                        <Row type="flex" className="margin-top-40">
                                            <Col span={12} className="edit-black-buttons float-right">
                                                <Button type="primary" value="large" htmlType="submit">
                                                    Add User
                                                </Button>
                                            </Col>
                                            <Col span={12} className="unfill-edit-black-buttons float-left">
                                                <Button type="primary" value="large"
                                                        onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                        </Row>
                                    </Form.Item>
                                </Col>
                            </Col>
                        </Row>
                    </Form>
                </Modal>

                <Col>
                    <Table
                        columns={contractorcolumns}
                        dataSource={contractordata}
                        // className="no-cursor"
                        pagination={false}
                        size="large"
                        onRow={(record) => ({
                            onClick: () => {
                                this.setState({
                                    userClickData: record,
                                    visible2: true,
                                }, () => {
                                    this.firmUserClickData(record)
                                });
                            }
                        })}
                    />
                    {this.state.modalOpen ?
                        <UpdateUser getAllUsers={this.getAllContractors} visible2={this.state.visible2}
                                    values={this.state.userClickData} handleOk2={this.handleOk2}
                                    handleCancel2={this.handleCancel2}/>
                        :
                        " "
                    }
                </Col>
            </>
        )
    }
}


const Contrators = Form.create({})(ContratorsView);
export default Contrators;