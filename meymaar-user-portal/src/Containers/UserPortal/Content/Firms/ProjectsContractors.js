import React from 'react';
import {Col, Table, Form, Popconfirm, message} from 'antd';
import axio from '../../../../utils/axios';
import ProjectsContractorAdd from './ProjectsContractorAdd';
import {handleError} from "../../../../store/store-utils";
import {NOTIFICATION_TIME} from "../../../../utils/common-utils";

class ProjectsContractorsView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showHide: true,
            radioValue: 1,
            showDetailsBox: true,
            getAllContractorAgianstPorjectClick: [],
        }
    }

    // success = () => {
    //     message.success('Contractor successfully added to project!');
    //   }

    // errors(){
    //     message.error('Contractor not add please try again!');
    // }
    componentDidMount() {
        // console.log('rizo', this.props.project_id)
        this.props.assignFirmsProjectApiData();
        this.projectsGetByFirmId()
    }


    getAllContractors = () => {
        axio.get(`/contractor-management/contractor/getByProject/${this.props.project_id}`,)
            .then(res => {
                console.log('contractor res', res.data.contractor)
                this.setState({
                    getAllContractorAgianstPorjectClick: res.data.contractor,
                })
            })
            .catch(error => {
                console.log('error', error);
            });
    };

    projectsGetByFirmId = () => {
        axio.get(`/contractor-management/contractor/getByProject/${this.props.project_id}`,)
            .then(res => {

                console.log('get all contarctor after click project', res.data.contractor);
                if (res.data.contractor === null) {
                    console.log('no data in object')
                } else {
                    this.setState({
                        getAllContractorAgianstPorjectClick: res.data.contractor,
                    })
                }
                this.props.assignFirmsProjectApiData()
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };

    deleteContractor = (record) => {
        console.log('deleted user', record);
        axio.delete(`/contractor-management/contractor/firm/remove-contractor-from-project/${record}`)
            .then(res => {
                console.log('es', res.data.status);
                this.getAllContractors();

                this.success('User successfully deleted!');
            })
            .catch(error => {
                console.log('error', error);
            })

    };

    render() {
        const onClickProjectscolumns = [{
            title: 'Contractor Id',
            dataIndex: 'contractorId',
        }, {
            title: 'First Name',
            dataIndex: 'first_name',

        }, {
            title: 'Last Name',
            dataIndex: 'last_name',

        }, {
            title: 'Email',
            dataIndex: 'email',
        }, {
            title: 'Phone',
            dataIndex: 'phone',
        }, {
            title: 'Address',
            dataIndex: 'address',
        }, {
            title: 'Gender',
            dataIndex: 'gender',
        },
            {
                title: 'Action',
                dataIndex: 'contractor_id',
                // key: 'x',
                render: (text, record) => <Popconfirm placement="rightTop" title="Sure to unassign?"
                                                      className="delete-confirmation-popup"
                                                      onConfirm={() => this.deleteContractor(record.contractor_id)}
                                                      okText="Yes"
                                                      cancelText="No">
                    {/* <a href="javascript:;" className="delete-text" onClick={()=>this.deleteLocation(record)}>Delete</a> */}
                    <a href="javascript:;" className="delete-text">Unassign</a>
                </Popconfirm>,
            },
        ];
        // if(this.state.getAllContractorAgianstPorjectClick===null){
        //     return
        // }else{
        const onClickProjectsData = this.state.getAllContractorAgianstPorjectClick.map((data, index) => {
            return {
                id: data.id,
                contractorId: index + 1,
                first_name: data.first_name,
                last_name: data.last_name,
                email: data.email,
                phone: data.phone,
                gender: data.gender,
                address: data.address,
                contractor_id: data.contractor_firm_project_id,
                dob: data.dob,
                created_at: data.created_at,
                updated_at: data.updated_at,
            }
        });
        // }


        // console.log('this.state.getAllContractorAgianstPorjectClick', this.state.getAllContractorAgianstPorjectClick===null)
        return (
            <>
                <Col>
                    <ProjectsContractorAdd projectsGetByFirmId={this.projectsGetByFirmId}
                                           assignFirmsProjectApiData={this.props.assignFirmsProjectApiData}
                                           project_id={this.props.project_id} projectId={this.props.projectId}
                                           id={this.props.id} first_name={this.state.first_name}
                                           firmId={this.props.firmId}/>
                    {this.state.getAllContractorAgianstPorjectClick === null ?
                        <div style={{textAlign: 'center'}}>
                            {/* <Loader/> */}
                            Still not add any user to this project...
                        </div>
                        :
                        <>
                            <Table
                                columns={onClickProjectscolumns}
                                dataSource={onClickProjectsData}
                                className="new-table-design"
                                pagination={false}
                                size="large"
                                onRow={(record) => ({
                                    onClick: () => {
                                        this.setState({
                                            first_name: record.first_name,
                                            last_name: record.last_name,
                                        }, () => {
                                            // this.firmsClick(record);
                                        });
                                    }
                                })}
                            />
                        </>

                    }
                </Col>
            </>
        )
    }
}

const ProjectsContractors = Form.create({})(ProjectsContractorsView);
export default ProjectsContractors;