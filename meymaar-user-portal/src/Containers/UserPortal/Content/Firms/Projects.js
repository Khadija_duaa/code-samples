import React from 'react';
import {Col, Table, Form, Modal} from 'antd';
import axio from '../../../../utils/axios';
import ProjectsContractors from './ProjectsContractors';

import Moment from 'react-moment';
import NewLoader from '../../../Loader/NewLoader';

class ProjectsView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showHide: true,
            radioValue: 1,
            showDetailsBox: true,
            getAllProject: [],
            visible: false,
            firmId: '',
            projectId: '',
            // project_id:'',
            searchText: '',
            isLoaded: false,
            visible2: false,
            getAllContractorAgianstPorjectClick: [],
            tableClickData: [],
        }
    }

    componentDidMount() {
        this.assignFirmsProjectApiData();
        // this.projectsGetByFirmId();
        this.setState(

        )
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    assignFirmsProjectApiData = () => {
        axio.get(`/contractor-management/contractor/project/getByFirm/${this.props.id}`,)
            .then(res => {
                if (res.data.project === null) {

                    this.setState({
                        isLoaded: true,
                    })
                } else {
                    this.setState({
                        isLoaded: true,
                        getAllProject: res.data.project,
                    })
                }
            })
            .catch(error => {
                console.log('error', error);
            });
    };
    hideModal = () => {
        this.setState({
            visible: false,
        });
    };
    projectContarctorsAssign = () => {
        this.setState({
            showHide: !this.state.showHide,
        })
    };
    radioValue = (e) => {
        this.setState({
            radioValue: e.target.value,
        });
    };
    closePage = () => {
        this.setState({
            showDetailsBox: false,
        });
    };

    tdClick = (tdClick) => {
        console.log('tdClick', tdClick);
        this.setState({
            showDetailsBox: true,
            tableClickData: tdClick,
            projectId: tdClick.project_id,
            project_id: tdClick.project_id,
            firmId: this.props.firmId,
        });
        this.showModal(tdClick.projectId, tdClick.project_id);
    };

    /*    handleEdit=(record)=>{
          console.log('handle project edit', record);
          this.setState({
            visible2: true,
            modalOpen:true,
            tableClickData:record,
          })
        };*/

    showModal2 = () => {
        this.setState({
            visible2: true,
        });
    };

    handleOk2 = () => {
        this.setState({
            visible2: false,
        });
    };

    handleCancel2 = () => {
        this.setState({
            visible2: false,
        });
    };


    render() {
        // firms table data  
        const projectcolumns = [

            {
                title: 'Name',
                dataIndex: 'name',
                width: 400,
                render: (text, record) => {
                    var name = record.name;
                    return <span className="table-data" onClick={() => this.tdClick(record)} key={name}>{name}</span>
                },
            }, {
                title: 'Reference Number',
                dataIndex: 'reference_number',
                width: 100,
                render: (text, record) => {
                    var reference_number = record.reference_number;
                    return <span className="table-data" onClick={() => this.tdClick(record)}
                                 key={reference_number}>{reference_number}</span>
                },
            }, {
                title: 'Created At',
                dataIndex: 'created_at',
                width: 100,
                render: (text, record) => {
                    var created_at = record.created_at;
                    return <span className="table-data" onClick={() => this.tdClick(record)}
                                 key={created_at}>{created_at}</span>
                },
            }
        ];
        const projectedata = this.state.getAllProject.map((data, index) => {
            var element = <Moment format="DD-MM-YYYY">{data.dateCreated}</Moment>;
            return {
                id: data.id,
                project_id: data.project_id,
                name: data.name,
                reference_number: data.reference_number,
                created_at: element,
            }
        });
        // firms table data


        //firm projects data
        const onClickProjectscolumns = [{
            title: 'Contractor Id',
            dataIndex: 'contractorId',
            width: 200,
        }, {
            title: 'First Name',
            dataIndex: 'first_name',
            width: 200,
        }, {
            title: 'Last Name',
            dataIndex: 'last_name',
            width: 200,
        }, {
            title: 'Email',
            dataIndex: 'email',
            width: 200,
        }, {
            title: 'Phone',
            dataIndex: 'phone',
            width: 200,
        }, {
            title: 'Address',
            dataIndex: 'address',
            width: 300,
        }, {
            title: 'Gender',
            dataIndex: 'gender',
            width: 100,
        }
        ];
        // if(this.state.getAllContractorAgianstPorjectClick===null){
        //     return
        // }else{
        const onClickProjectsData = this.state.getAllContractorAgianstPorjectClick.map((data) => {
            return {
                id: data.id,
                contractorId: data.id,
                first_name: data.first_name,
                last_name: data.last_name,
                email: data.email,
                phone: data.phone,
                gender: data.gender,
                address: data.address,
                dob: data.dob,
                created_at: data.created_at,
                updated_at: data.updated_at,
            }
        });
        // }
        //firm project data

        return (
            <>

                <Col span={24}>
                    {this.props.tableClickData === null ?
                        <NewLoader/>
                        :
                        <>

                            {this.state.isLoaded === true ?
                                <>
                                    <Table
                                        columns={projectcolumns}
                                        dataSource={projectedata}
                                        className="zero-padding"
                                        pagination={false}
                                        size="large"
                                    />
                                    {/*                                            {this.state.modalOpen?
                                                      <UpdateProject visible2={this.state.visible2} handleOk2={this.handleOk2} handleCancel2={this.handleCancel2} tableClickData={this.state.tableClickData} firmsApiData={this.firmsApiData}/>
                                                      : 
                                                      " "
                                                    }*/}
                                </>
                                :
                                ''
                            }

                            {this.state.projectId ?
                                <Modal
                                    title="Project Users"
                                    className="projects-contractor-popup"
                                    visible={this.state.visible}
                                    onOk={this.hideModal}
                                    onCancel={this.hideModal}
                                    okText="Ok"
                                    cancelText="Cancel"
                                    destroyOnClose={true}
                                    footer={false}
                                    centered
                                    maskClosable={false}
                                >
                                    <Col>
                                        <ProjectsContractors assignFirmsProjectApiData={this.assignFirmsProjectApiData}
                                                             project_id={this.state.project_id}
                                                             first_name={this.props.first_name}
                                                             projectId={this.state.projectId} id={this.props.id}
                                                             firmId={this.props.firmId}/>
                                    </Col>
                                </Modal>
                                :
                                ""
                            }
                        </>

                    }
                </Col>
            </>
        )
    }
}

const Projects = Form.create({})(ProjectsView);
export default Projects;