import React from 'react';
import { Row, Col,  Form, Input, message, Select, Modal, Button} from 'antd';
import {fetchAllExecutionProjects} from "../../../../store/project/project-actions";
import {connect} from 'react-redux';
import axio from '../../../../utils/axios';
import NewLoader from '../../../Loader/NewLoader';

const { Option } = Select;

class ProjectsBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            showHide:true,
            radioValue:1,
            showDetailsBox: true,
            visible:false,
        }
    }
    componentDidMount() {
        this.props.fetchAllExecutionProjects();
    }

    showModal=()=>{
        this.setState({
            visible: true,
        });
    };
    handleOk = () => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    handleOnChange=(values)=>{
        console.log('selected values of form: ', values);
        this.setState({
            selectedProject: values
        })
    };
    errors=(msg)=>{
        message.error(msg);
    };
    success=(msg)=>{
        message.success(msg);
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            axio.post('/contractor-management/contractor/project/create',{
                project_id: values.selectedProjectId,
                firm_id: values.firmId,
            }).then(res=>{
                this.success('project successfully added');
                this.handleCancel();
                this.projectsGetByFirmId();
                this.props.assignFirmsProjectApiData();
            })
            .catch(error=> {
                // this.errors('Project already assigned please select other!')
                 if(error.response){
                    this.errors(error.response.data.message);
                  }else if (error.request) {
                    console.log('error.request');
                  } else {
                    console.log('Error', error);
                  }
                // console.log('error', error.response.data.message);
            });
          }
        });
    };

    getSelectOptions = ()=> this.props.worksList.map((pro)=>{
        return(
             <Option key={pro.projectId} value={pro.projectId}>{pro.name}</Option>
        )
     });
        render(){
            console.log('tableClickData on project rown firmId this.props.worksList', this.props.firmId, this.props.worksList);
        // if(!this.props.worksList) return null;
          const { getFieldDecorator } = this.props.form; 
            return(
                        <>

                            {this.props.worksList===null ?
                                        <NewLoader />
                            :
                            <>
                           <Button value="large" className="new-add-assign-button add-new-btn " onClick={this.showModal}>+ Assign Project</Button>
                            <Modal
                                title="Assign Project"
                                visible={this.state.visible}
                                className="custom-table-popup"
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                                destroyOnClose={true}
                                footer={false}
                            >
                            <Form onSubmit={this.handleSubmit}>
                                            <Row className="custom-popup-row">
                                                <p className="center">Please fill the following the input fields</p>
                                                <Col span={24} offset={4} className="custom-form-wrap">
                                                    <Col span={16}>
                                                        <Form.Item label="Project Name">
                                                            {getFieldDecorator('selectedProjectId', {
                                                                rules: [{ required:true, message: 'Please select your project!' }],
                                                            })(
                                                                <Select value="No Selected Project" placeholder="Select a Project" onChange={(values, event) => this.handleOnChange(values, event)}>
                                                                   {this.getSelectOptions()}  
                                                                </Select>
                                                            )}
                                                        </Form.Item>

                                                        <Form.Item label="Firm Id">
                                                            {getFieldDecorator('firmId', {
                                                                initialValue: this.props.id
                                                                })(<Input disabled/>)}
                                                        </Form.Item>

                                                        <Form.Item >
                                                            <Row type="flex" className="margin-top-40">
                                                                    <Col span={12} className="edit-black-buttons float-right">
                                                                        <Button type="primary" value="large" htmlType="submit">
                                                                          Assign Project to Firm
                                                                        </Button>
                                                                    </Col>
                                                                    <Col span={12} className="unfill-edit-black-buttons float-left">
                                                                        <Button type="primary" value="large" onClick={this.handleCancel}>Cancel</Button>
                                                                    </Col>
                                                            </Row>
                                                        </Form.Item>
                                                    </Col>
                                                </Col>
                                            </Row>
                                        </Form>
                            </Modal>
                            </>
                            }
                        </>
            )
    }
}




const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllExecutionProjects: () => dispatch(fetchAllExecutionProjects())
    }
};

const mapStateToProps = state => {
    return {
        worksList: state.project_reducer.executionProjects,
        projectProcessing: state.project_reducer.processing
    }
};

const AddProjects = Form.create({})(ProjectsBtn);
const connected = connect(mapStateToProps, mapDispatchToProps)(AddProjects);
export default connected;