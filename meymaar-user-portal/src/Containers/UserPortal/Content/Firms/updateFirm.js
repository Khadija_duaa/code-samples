import React from 'react';
import {Row, Col, Form, message, Input, Modal, Button} from 'antd';
import axio from '../../../../utils/axios';

class UpdateFirmView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            //   visible2:this.props.visible2,
            // userClickData:[],
        }
    }

    success = (msg) => {
        message.success(msg);
    };
    errors = (msg) => {
        message.error(msg);
    };


    updateFirmInfo = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received edit firm values of form:', values, this.props.handleCancel2());

                axio.put(`/contractor-management/contractor/firm/update/${values.id}`, {
                    firm_id: values.firm_id,
                    firm_name: values.firm_name,
                    email: values.email,
                    phone: values.phone,
                    firms_id: values._id,
                    address: values.address,
                    landline: values.landline,
                    postal_address: values.postal_address,
                    telegraphic_address: values.telegraphic_address,
                }).then(res => {
                    this.success(res.data.message);
                    this.props.firmsApiData();
                })
                    .catch((error) => {
                        if (error.response) {
                            this.errors(error.response.data.message);
                        } else if (error.request) {
                            console.log('error.request');
                        } else {
                            console.log('Error', error);
                        }
                        console.log("rejected");
                    });

            }
        });
    };

    render() {
        console.log('this.props.tableClickData', this.props.tableClickData);
        const {tableClickData} = this.props;
        const {getFieldDecorator} = this.props.form;
        return (

            <Modal
                title={'Registration Number: ' + tableClickData.key}
                visible={this.props.visible2}
                className="custom-table-popup"
                onOk={this.props.handleOk2}
                onCancel={this.props.handleCancel2}
                destroyOnClose={true}
                footer={false}
                centered
            >
                <Form onSubmit={this.updateFirmInfo}>
                    <Row className="custom-popup-row">
                        <Col span={24} offset={4} className="custom-form-wrap">
                            <Col span={16}>

                                <Form.Item>
                                    {getFieldDecorator('id', {
                                        initialValue: tableClickData.id
                                    })(<Input disabled type='hidden'/>)}
                                </Form.Item>


                                <Form.Item label="Firm Name">
                                    {getFieldDecorator('firm_name', {
                                        initialValue: tableClickData.firm_name,
                                        rules: [{required: true, message: 'Please Enter firm name'}],
                                    })(<Input/>)}
                                </Form.Item>

                                <Form.Item label="Email">
                                    {getFieldDecorator('email', {
                                        initialValue: tableClickData.email,
                                        rules: [{
                                            type: 'email', message: 'Invalid email!',
                                        }, {required: true, message: 'Please enter firm email!'}],
                                    })(<Input/>)}
                                </Form.Item>

                                <Form.Item label="Postal Address">
                                    {getFieldDecorator('postal_address', {
                                        initialValue: tableClickData.postal_address,
                                    })(<Input/>)}
                                </Form.Item>

                                <Form.Item label="Address">
                                    {getFieldDecorator('address', {
                                        initialValue: tableClickData.address,
                                    })(<Input/>)}
                                </Form.Item>

                                <Form.Item label="Telegraphic Address">
                                    {getFieldDecorator('telegraphic_address', {
                                        initialValue: tableClickData.telegraphic_address,
                                    })(<Input/>)}
                                </Form.Item>

                                <Form.Item label="Phone">
                                    {getFieldDecorator('landline', {
                                        initialValue: tableClickData.landline,
                                        rules: [{
                                            required: true,
                                            message: 'Please enter landline!'
                                        }, {validator: this.validationPhone}],
                                    })(<Input/>)}
                                </Form.Item>

                                <Form.Item label="Cell">
                                    {getFieldDecorator('phone', {
                                        initialValue: tableClickData.phone,
                                        rules: [{
                                            required: true,
                                            message: 'Please enter cell number!'
                                        }, {validator: this.validationPhone}],
                                    })(<Input/>)}
                                </Form.Item>

                                <Form.Item>
                                    <Row type="flex" className="margin-top-40">
                                        <Col span={12} className="edit-black-buttons float-right">
                                            <Button type="primary" value="large" htmlType="submit">
                                                Update
                                            </Button>
                                        </Col>
                                        <Col span={12} className="unfill-edit-black-buttons float-left">
                                            <Button type="primary" value="large"
                                                    onClick={this.props.handleCancel2}>Cancel</Button>
                                        </Col>
                                    </Row>
                                </Form.Item>
                            </Col>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        )
    }
}


const UpdateFirm = Form.create({})(UpdateFirmView);
export default UpdateFirm;