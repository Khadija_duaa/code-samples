import React from 'react';
import {Row, Col, Form, Tabs} from 'antd';
import axio from '../../../../utils/axios';
import Contrators from './Contractors';

import Projects from './Projects';

const TabPane = Tabs.TabPane;

class FirmsDetailsView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showHide: true,
            radioValue: 1,
            showDetailsBox: true,
            firmsDetailsById: [],
        }
    }

    componentDidMount() {
        axio.get(`/contractor-management/contractor/firm/get/${this.props.tableClickData.id}`,)
            .then(res => {

                this.setState({
                    firmsDetailsById: res.data.firm
                })
            })
            .catch(error => {
                console.log('error', error);
            });
    }

    showHide = () => {
        this.setState({
            showHide: !this.state.showHide,
        })
    };
    radioValue = (e) => {
        this.setState({
            radioValue: e.target.value,
        });
    };
    handleOnChange = (values) => {

        this.setState({
            radioValue: values
        })
    };
    closePage = () => {
        this.setState({
            showDetailsBox: false,
        });
    };
    callback = (key) => {
        console.log(key);
    };

    renderFirmHeader = () => {
        let {tableClickData} = this.props;

        let dataJSON = [
            {
                title: "Landline # ",
                data: tableClickData.landline?tableClickData.landline:"",
                className: "detailz-box col-md-custom margin0right-10p",
            },
            {
                title: "Cell # ",
                data: tableClickData.contact,
                className: "detailz-box col-md-custom"
            },
            {
                title: "Address",
                data: tableClickData.address,
                className: "detailz-box col-md-custom margin0right-10p",
            },
            {
                title: "Email ",
                data: tableClickData.email,
                className: "detailz-box col-md-custom"
            }
        ];

        return dataJSON.map((data, index) => {
            return (
                <div key={index} className={` ${data.className}`}>
                    <p>{data.title}</p>
                    <span>{data.data}</span>
                </div>
            )
        })
    };

    render() {
        const {tableClickData} = this.props;

        return (
            <>
                <Col span={24}>
                    <div className={"firms-work-details dashboard-show-hide-section-container mb-5"}>
                        <div>
                            <h3>{tableClickData.firm_name}</h3>
                            <p style={{fontSize: "18px"}}><strong>Firm#:</strong> {tableClickData.firmId}</p>
                            <div className="row">
                                {this.renderFirmHeader()}
                            </div>
                        </div>
                    </div>
                </Col>
                <Col span={24} className="firms">
                    <Row className="radio">

                        <Tabs onChange={this.callback} type="card" className="firm-tabs-container">
                            <TabPane tab="Projects" key="1">
                                <Col span={24}>
                                    <h2 style={{marginBottom: "60px", marginTop: "60px"}}>Projects</h2>
                                    <Projects id={tableClickData.id} firstFirmUsers={tableClickData}
                                              firmId={tableClickData.firmId}/>
                                </Col>
                            </TabPane>
                            <TabPane tab="Users" key="2">
                                <Col span={24}>
                                    <h2 style={{marginBottom: "60px", marginTop: "60px"}}>Users</h2>
                                    <Contrators firmId={tableClickData.id} idNew={tableClickData.firmId}/>
                                </Col>
                            </TabPane>
                        </Tabs>
                    </Row>
                </Col>
            </>
        )
    }
}


const FirmsDetails = Form.create({})(FirmsDetailsView);
export default FirmsDetails;