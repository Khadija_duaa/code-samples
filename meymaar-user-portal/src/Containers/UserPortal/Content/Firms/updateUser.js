import React from 'react';
import {Row, Col, Form, message, Input, Select, Modal, DatePicker, Button} from 'antd';
import axio from '../../../../utils/axios';
import InputMask from 'react-input-mask';
import {isMobile, isValidPhone} from "../../../../utils/validations";
import moment from 'moment';


const {Option} = Select;

class UpdateUserView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            //   visible2:this.props.visible2,
            userClickData: [],
            value: '',
        }
    }

    success = (msg) => {
        message.success(msg);
    };
    errors = (msg) => {
        message.error(msg);
    };


    updateUserInfo = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                // const dates = {
                //   'dob': values['dob'].format('DD-MM-YYYY'),
                // };
                console.log('Received user values of form:', values, this.props.handleCancel2());


                axio.put(`/contractor-management/contractor/update/${values.firm_id}`, {
                    firm_id: values.firm_id,
                    first_name: values.first_name,
                    last_name: values.last_name,
                    email: values.email && values.email ? values.email : null,
                    phone: values.phone,
                    //  thumb_print: values.thumb_print,
                    //  username: values.username,
                    //  password: values.password,
                    cnic: values.cnic,
                    address: values.address,
                    gender: values.gender,
                    dob: values.dob,
                    username: values.username,
                    password: values.password,
                }).then(res => {
                    //  this.getAllContractors();
                    this.success(res.data.message);
                    this.props.getAllUsers();
                    console.log("update contarctor api", res.data.message);
                })
                    .catch((error) => {
                        if (error.response) {
                            this.errors(error.response.data.message);
                            this.setState({
                                visible2: false
                            })
                        } else if (error.request) {
                            console.log('error.request');
                            this.setState({
                                visible2: false
                            })
                        } else {
                            console.log('Error', error);
                            this.setState({
                                visible2: false
                            })
                        }
                        console.log("rejected");
                    });
            }

        });
    };

    mobileCountryCodeValidator = (rule, value, callback) => {

        if (value && (!isValidPhone(value) || !isMobile(value))) {
            callback('Invalid Mobile Number')
        } else {
            callback()
        }

    };

    ageValidator = (rule, value, callback) => {

        let startDate = moment(new Date(), "YYYY-MM-DD");
        let endDate = moment(value, "YYYY-MM-DD");
        let years = startDate.diff(endDate, 'year');

        if (years < 18) {
            callback("Minimum age limit is 18 years");
        } else {
            callback();
        }
    };

    validateCNIC = (rules, value, callback) => {
        if (value) {
            let cnic_no_regex = /^[0-9+]{5}-[0-9+]{7}-[0-9]{1}/;

            if (!cnic_no_regex.test(value)) {
                callback('CNIC Format XXXXX-XXXXXXX-X');
            }
            else {
                callback();
            }
        } else {
            callback();
        }
    };


    render() {
        console.log('this.props.showModal', this.props.visible2);
        console.log('this.props.values', this.props.values);

        const {getFieldDecorator} = this.props.form;
        return (

            <Modal
                title={this.props.values.first_name + ' ' + this.props.values.last_name}
                visible={this.props.visible2}
                className="custom-table-popup"
                onOk={this.props.handleOk2}
                onCancel={this.props.handleCancel2}
                destroyOnClose={true}
                footer={false}
                centered
            >
                <Form onSubmit={this.updateUserInfo}>
                    <Row className="custom-popup-row">
                        <Col span={24} offset={4} className="custom-form-wrap">
                            <Col span={16}>
                                <Form.Item>
                                    {getFieldDecorator('firm_id', {
                                        initialValue: this.props.values.id
                                    })(<Input disabled type="hidden"/>)}
                                </Form.Item>

                                <Form.Item label="First Name">
                                    {getFieldDecorator('first_name', {
                                        initialValue: this.props.values.first_name,
                                        rules: [{
                                            required: true,
                                            message: 'Please enter first name!',
                                            whitespace: false
                                        }]
                                    })(<Input pattern="[A-Za-z0-9]+"/>)}
                                </Form.Item>

                                <Form.Item label="Last Name">
                                    {getFieldDecorator('last_name', {
                                        initialValue: this.props.values.last_name,
                                        rules: [{required: true, message: 'Please enter last nanme!'}]
                                    })(<Input pattern="[A-Za-z0-9]+"/>)}
                                </Form.Item>

                                <Form.Item label="Email">
                                    {getFieldDecorator('email', {
                                        initialValue: this.props.values.email,
                                        rules: [{
                                            type: 'email', message: 'Invalid email!',
                                        }, {required: false, message: 'Please enter email!'}]
                                    })(<Input/>)}
                                </Form.Item>

                                <Form.Item label="Phone">
                                    {getFieldDecorator('phone', {
                                        initialValue: this.props.values.phone,
                                        rules: [{required: true, message: 'Please enter phone number!'},
                                            {validator: this.mobileCountryCodeValidator}]
                                    })(<Input/>)}
                                </Form.Item>

                                <Form.Item label="Username">
                                    {getFieldDecorator('username', {
                                        initialValue: this.props.values.username,
                                        rules: [{required: true, message: 'Please enter username!'}]
                                    })(<Input pattern="[A-Za-z0-9]+" disabled={true}/>)}
                                </Form.Item>

                                <Form.Item label="Password">
                                    {getFieldDecorator('password', {
                                        rules: [{required: false, message: 'Please enter password!'}]
                                    })(<Input type="password"/>)}
                                </Form.Item>


                                <Form.Item label="CNIC">
                                    {getFieldDecorator('cnic', {
                                        initialValue: this.props.values.cnic,
                                        rules: [{required: true, message: 'Please enter CNIC'},
                                            {validator: this.validateCNIC},]
                                    })(<InputMask mask="99999-9999999-9" maskChar={' '}/>)}
                                </Form.Item>

                                <Form.Item label="Address">
                                    {getFieldDecorator('address', {
                                        initialValue: this.props.values.address,
                                        rules: [{required: true, message: 'Please enter address!'}]
                                    })(<Input/>)}
                                </Form.Item>

                                <Form.Item label="Gender">
                                    {getFieldDecorator('gender', {
                                        initialValue: this.props.values.gender,
                                        rules: [{required: true, message: 'Please select gender'}],
                                    })(
                                        <Select className="custom-dropdown-menu" placeholder="Gneder" size="large">
                                            <Option value={'Male'}>Male</Option>
                                            <Option value={'Female'}>Female</Option>
                                        </Select>
                                    )}
                                </Form.Item>

                                <Form.Item label="DOB">
                                    {getFieldDecorator('dob', {
                                        initialValue: moment(this.props.values.dob, "YYYY-MM-DD"),
                                        rules: [{required: true, message: 'Please select date!'},
                                            {validator: this.ageValidator}]
                                    })(
                                        <DatePicker format="YYYY-MM-DD"/>
                                    )}
                                </Form.Item>


                                <Form.Item>
                                    <Row type="flex" className="margin-top-40">
                                        <Col span={12} className="edit-black-buttons float-right">
                                            <Button type="primary" value="large" htmlType="submit">
                                                Update
                                            </Button>
                                        </Col>
                                        <Col span={12} className="unfill-edit-black-buttons float-left">
                                            <Button type="primary" value="large"
                                                    onClick={this.props.handleCancel2}>Cancel</Button>
                                        </Col>
                                    </Row>
                                </Form.Item>
                            </Col>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        )
    }
}


const UpdateUser = Form.create({})(UpdateUserView);
export default UpdateUser;