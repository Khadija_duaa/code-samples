import React from 'react';
import { Row, Col,  Form, DatePicker,  message, Input,  Modal} from 'antd';



class UpdateProjectView extends React.Component {
    
    constructor(props) {
        super(props);
        this.state ={
          //   visible2:this.props.visible2,
            // userClickData:[],
        }
    }
     
  success=(msg)=>{
    message.success(msg);
  };
  errors=(msg)=>{
    message.error(msg);
  };
  maskChange = (event) => {
    this.setState({
      value: event.target.value
    });
  };
  

    updateProjectInfo = (e) =>{
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          console.log('THIS TIME WE DO NOT HAVE AUTHORITIES TO EDIT VALUES...!');
        }
      });
    };
    
    render(){
          console.log('this.props.tableClickData', this.props.tableClickData)
          const {tableClickData} = this.props
          const { getFieldDecorator } = this.props.form; 
            return(
                            
                                        <Modal
                                            title={'Update Project: ' + tableClickData.name}
                                            visible={this.props.visible2}
                                            className="custom-table-popup"
                                            onOk={this.props.handleOk2}
                                            onCancel={this.props.handleCancel2}
                                            destroyOnClose={true}
                                            footer={false}
                                            centered
                                        >
                                        <Form onSubmit={this.updateProjectInfo}>
                                                        <Row className="custom-popup-row">
                                                            <Col span={24} offset={4} className="custom-form-wrap">
                                                                <Col span={16}>

                                                                    <Form.Item >
                                                                        {getFieldDecorator('project_id', { 
                                                                            initialValue: tableClickData.id
                                                                            })(<Input disabled type='hidden' />)}
                                                                    </Form.Item>

                                                                    <Form.Item label="Project Name">
                                                                        {getFieldDecorator('project_name', {
                                                                            initialValue: tableClickData.name,
                                                                            rules: [{ required:true, message: 'Please Enter firm name' }],
                                                                            })(<Input disabled/>)}
                                                                    </Form.Item>

                                                                    <Form.Item label="Reference Number">
                                                                        {getFieldDecorator('reference_number', {
                                                                            initialValue: tableClickData.reference_number,
                                                                            rules: [{ required:true, message: 'Please enter firm email!' }],
                                                                            })(<Input disabled/>)}
                                                                    </Form.Item>

                                                                    {/* <Form.Item label="Created At">
                                                                        {getFieldDecorator('created_at', {
                                                                            initialValue: <Moment format="DD-MM-YYYY">{tableClickData.created_at}</Moment>,
                                                                            rules: [{ required:true, message: 'Please enter firm mobile number!' }],
                                                                            })(<Input />)}
                                                                    </Form.Item> */}

                                                                    <Form.Item label="Created At" >
                                                                        {getFieldDecorator('created_at',{
                                                                            rules: [{ required: true, message: 'Please select date!' }]
                                                                        })(
                                                                            <DatePicker disabled/>
                                                                        )}
                                                                    </Form.Item>


                                                                    <Form.Item >
                                                                        <Row type="flex" className="margin-top-40">
                                                                            {/* <Col span={12} className="edit-black-buttons float-right">
                                                                                <Button type="primary" value="large" htmlType="submit">
                                                                                Update
                                                                                </Button>
                                                                            </Col> */}
                                                                            {/* <Col span={12} className="unfill-edit-black-buttons float-left">
                                                                                <Button type="primary" value="large" onClick={this.props.handleCancel2}>Cancel</Button>
                                                                            </Col> */}
                                                                        </Row>
                                                                    </Form.Item>
                                                                </Col>
                                                            </Col>
                                                        </Row>
                                                    </Form>
                                        </Modal>
            )
    }
}



const UpdateProject = Form.create({})(UpdateProjectView);
export default UpdateProject;