import React from 'react';
import {Row, Col, Table, Form, Breadcrumb, Icon} from 'antd';
import {connect} from 'react-redux';
import FirmsDetails from './FirmsDetails';
import AddFirms from './AddFirms';
import NewLoader from '../../../Loader/NewLoader';
import {withRouter} from 'react-router-dom';
import {formatPhoneNumber} from '../../../../utils/common-utils';
import UpdateFirm from './updateFirm';
import {fetchFirmsApiData} from "../../../../utils/server-utils";

class Firms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDetailsBox: false,
            firmData: [],
            tableClickData: [],
            firmsDatas: [],
            filteredData: [],
            visible2: false,
            modalOpen: false,
        }
    }


    componentDidMount = () => {
        this.firmsApiData()
    };

    firmsApiData = () => {
        fetchFirmsApiData(data => {
            this.setState({firmData: data})
        })
    };

    filterTable = (e) => {
        let searcjQery = e.target.value.toLowerCase(),
            displayedContacts = this.state.firmData.filter((el) => {
                let searchValue = el.firm_name.toLowerCase();
                return searchValue.indexOf(searcjQery) !== -1;
            });
        this.setState({
            filteredData: displayedContacts
        })
    };


    closePage = () => {
        this.setState({
                showDetailsBox: !this.state.showDetailsBox,
            }, () => {
                this.props.history.push('/dashboard/firms')
            }
        )
    };


    handleEdit = (record) => {
        this.setState({
            visible2: true,
            modalOpen: true,
            tableClickData: record,
        })
    };


    handleOk2 = () => {
        console.log('handleOk');
        this.setState({
            visible2: false,
        });
    };

    handleCancel2 = () => {
        this.setState({
            visible2: false,
        });
    };


    tdClick = (tdClick) => {
        this.setState({
            showDetailsBox: true,
            tableClickData: tdClick,
        })
    };

    render() {
        if (this.props.processing) return <NewLoader/>;

        let {filteredData, firmData} = this.state;
        let firms_Details = filteredData.length ? filteredData : firmData;

        const firmscolumns = [
            {
                title: 'Registration Number',
                dataIndex: 'firmId',
                name: 'Firm Id',
                width: 200,
                render: (text, record) => {
                    let firmId = record.firmId;
                    return <span className="table-data" onClick={() => this.tdClick(record)}
                                 key={firmId}>{firmId}</span>
                },
            },
            {
                title: 'Firm Name',
                dataIndex: 'first_name',
                width: 200,
                render: (text, record) => {
                    let firm_name = record.firm_name;
                    return <span className="table-data" onClick={() => this.tdClick(record)}
                                 key={firm_name}>{firm_name}</span>
                },
            },
            {
                title: 'Email',
                dataIndex: 'email',
                width: 200,
                render: (text, record) => {
                    let email = record.email;
                    return <span className="table-data" onClick={() => this.tdClick(record)} key={email}>{email}</span>
                },
            },
            {
                title: 'Contact',
                dataIndex: 'contact',
                width: 200,
                render: (text, record) => {
                    let contact = record.contact;
                    return <span className="table-data" onClick={() => this.tdClick(record)}
                                 key={contact}>{contact}</span>
                },
            },
            {
                title: 'Action',
                dataIndex: 'action',
                width: 30,
                render: (text, record) => (
                    <span className="table-data edit-actions" onClick={() => this.handleEdit(record)}><Icon
                        type="form"/> <a
                        href="javascript:;">Edit</a></span>
                )
            }
        ];

        const firmsdata = firms_Details.map((data) => {
            return {
                id: data.id,
                key: data.firm_id,
                firmId: data.firm_id,
                firm_name: data.firm_name,
                email: data.email || 'hello@email.com',
                contact: formatPhoneNumber(data.phone, 'PK'),
                phone: data.phone,
                created_at: data.created_at,
                updated_at: data.updated_at,
                firms_id: data._id,
                address: data.address,
                landline: data.landline,
                postal_address: data.postal_address,
                telegraphic_address: data.telegraphic_address,
            }
        });


        const breadCrumbStyle = {cursor: "pointer"};
        return (
            <>
                {this.state.showDetailsBox ?
                    <Row span={24}>
                        <Col span={24} className="breadcrumbs-back">
                            <Breadcrumb separator=">" style={{marginBottom: '20px'}}>
                                <Breadcrumb.Item style={breadCrumbStyle}
                                                 onClick={() => this.props.history.push('/dashboard')}>Dashboard</Breadcrumb.Item>
                                <Breadcrumb.Item style={breadCrumbStyle}
                                                 onClick={this.closePage}>Firms</Breadcrumb.Item>
                                <Breadcrumb.Item>Firm Details</Breadcrumb.Item>
                            </Breadcrumb>
                            <p className="custom-button custom-button-light pointer" onClick={this.closePage}><Icon
                                type="caret-left"/> Back</p>
                        </Col>
                        <FirmsDetails firmsApiData={this.firmsApiData} tableClickData={this.state.tableClickData}/>
                    </Row>
                    :

                    <>
                        <Row>
                            <Col span={24} className="firms">
                                <h3 style={{marginBottom: "60px"}}>Firms</h3>
                                <Row type="flex" justify="space-between" className="firm-filters">
                                    <Col span={3}>
                                        <AddFirms firmsApiData={this.firmsApiData}/>
                                        {/* <Button size="large" className="new-add-assign-button add-new-btn" onClick={this.showModal}>+ Add a Device</Button>     */}
                                    </Col>
                                    <Col span={6}>
                                        <div className="new-btn-with-gradient-select search-bar-devices">
                                            <form className="search-container">
                                                <input type="text" id="search-bar-input" placeholder="Search…"
                                                       onChange={(e) => this.filterTable(e)}/>
                                                <span><i className="fas fa-search"/></span>
                                            </form>
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="">
                                    <Col span={24}>
                                        <Table
                                            columns={firmscolumns}
                                            dataSource={firmsdata}
                                            className="zero-padding"
                                            pagination={false}
                                            size="large"
                                        />
                                        {this.state.modalOpen ?
                                            <UpdateFirm visible2={this.state.visible2} handleOk2={this.handleOk2}
                                                        handleCancel2={this.handleCancel2}
                                                        tableClickData={this.state.tableClickData}
                                                        firmsApiData={this.firmsApiData}/>
                                            :
                                            " "
                                        }
                                    </Col>
                                </Row>
                            </Col>
                        </Row>

                    </>
                }
            </>
        )
    }
}


const mapStateToProps = state => {
    return {
        processing: state.project_reducer.processing
    }
};

const connected = connect(mapStateToProps)(Firms);
const DevicesComponent = Form.create({})(connected);
export default withRouter(DevicesComponent);