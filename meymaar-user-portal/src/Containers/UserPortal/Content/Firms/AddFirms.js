import React from 'react';
import {Row, Col, Form, Input, message, Modal, Button} from 'antd';
import axio from '../../../../utils/axios';
import {isValidPhone} from "../../../../utils/validations";
import {handleError} from "../../../../store/store-utils";
import {NOTIFICATION_TIME} from "../../../../utils/common-utils";


class AddFirmsBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showHide: true,
            radioValue: 1,
            showDetailsBox: true,
            visible: false,
        }
    }


    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = () => {
        this.setState({
            visible: false
        });
    };

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    success = (msg) => {
        message.success(msg, NOTIFICATION_TIME);
    };
    error = (msg) => {
        message.error(msg,NOTIFICATION_TIME);
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                axio.post('/contractor-management/contractor/firm/create', values).then(() => {

                    this.success('Firm Added Successfully!');
                    this.handleCancel();
                    // window.location.reload();
                    this.props.firmsApiData();
                })
                    .catch(error => {
                        let errorMessage = handleError(error);
                        this.error(errorMessage);
                    });
            }
        });
    };

    validationPhone = (rules, value, callback) => {
        if (value) {
            if (isValidPhone(value)) {
                callback();
            } else {
                callback('Invalid Phone Number');
            }
        } else {
            callback();
        }
    };

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <>
                <button className="custom-button custom-button-dark create-button" onClick={this.showModal}>+ Add Firm
                </button>
                <Modal
                    title="Add Firm"
                    visible={this.state.visible}
                    className="custom-table-popup"
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    destroyOnClose={true}
                    footer={false}
                    centered={true}
                >
                    <Form onSubmit={this.handleSubmit}>
                        <Row className="custom-popup-row">
                            {/* <p className="center">Please fill the following the input fields</p> */}
                            <Col span={24} offset={4} className="custom-form-wrap">
                                <Col span={16}>
                                    <Form.Item label="Registration Number">
                                        {getFieldDecorator('firm_id', {
                                            rules: [{
                                                required: true,
                                                message: 'Please enter firms registration number'
                                            }],
                                        })(<Input/>)}
                                    </Form.Item>

                                    <Form.Item label="Firm Name">
                                        {getFieldDecorator('firm_name', {
                                            rules: [{
                                                required: true,
                                                whitespace: true,
                                                message: 'Please Enter firm name'
                                            }],
                                        })(<Input/>)}
                                    </Form.Item>

                                    <Form.Item label="Email">
                                        {getFieldDecorator('email', {
                                            rules: [{
                                                type: 'email', message: 'Invalid email!',
                                            }, {required: true, message: 'Please enter firm email!'}],
                                        })(<Input/>)}
                                    </Form.Item>

                                    <Form.Item label="Postal Address">
                                        {getFieldDecorator('postal_address')(<Input/>)}
                                    </Form.Item>

                                    <Form.Item label="Address">
                                        {getFieldDecorator('address')(<Input/>)}
                                    </Form.Item>

                                    <Form.Item label="Telegraphic Address">
                                        {getFieldDecorator('telegraphic_address')(<Input/>)}
                                    </Form.Item>

                                    <Form.Item label="Phone">
                                        {getFieldDecorator('landline', {
                                            rules: [{
                                                required: true,
                                                message: 'Please enter landline!'
                                            }, {validator: this.validationPhone}],
                                        })(<Input/>)}
                                    </Form.Item>

                                    <Form.Item label="Cell">
                                        {getFieldDecorator('phone', {
                                            rules: [{
                                                required: true,
                                                message: 'Please enter cell number!'
                                            }, {validator: this.validationPhone}],
                                        })(<Input/>)}
                                    </Form.Item>

                                    <Form.Item>
                                        <Row type="flex" className="margin-top-40">
                                            <Col span={12} className="edit-black-buttons float-right">
                                                <Button type="primary" value="large" htmlType="submit">
                                                    Add Firm
                                                </Button>
                                            </Col>
                                            <Col span={12} className="unfill-edit-black-buttons float-left">
                                                <Button type="primary" value="large"
                                                        onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                        </Row>
                                    </Form.Item>
                                </Col>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            </>
        )
    }
}


const AddFirms = Form.create({})(AddFirmsBtn);
export default AddFirms;