import React from 'react';
import { Row, Col,   Form, Input, Select, Modal, message, Button} from 'antd';
import axio from '../../../../utils/axios';
import {handleError} from "../../../../store/store-utils";
import {NOTIFICATION_TIME} from "../../../../utils/common-utils";

const { Option } = Select;

class ProjectsContractorAddBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            showHide:true,
            radioValue:1,
            showDetailsBox: true,
            visible:false,
            projectsContratorsById: [],
        }
    }
    
    componentDidMount(){
        
        axio.get(`/contractor-management/contractor/firm/getContractorsByFirm/${this.props.id}`,)
          .then(res=> {
              this.setState({
                projectsContratorsById: res.data.firms.contractor
              })
          })
          .catch(error=> {
            console.log('error', error);
          });
        this.setState(
            
        )
    }
    showModal=()=>{
        this.setState({
            visible: true,
        });
    };
    errors=(msg)=>{
        message.error(msg);
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {

              console.log('its user role', values)
            
            axio.put('/contractor-management/contractor/firm/assignContractorToProject/',{
                project_id: values.projectId,
                contractor_id: this.state.selectedProject,
                role: values.userRole,
            }).then(res=>{
                this.props.projectsGetByFirmId();
                this.handleCancel();
            })
            .catch(error=> {
                let errorMessage = handleError(error);
                message.error(errorMessage,NOTIFICATION_TIME);
            });
        }
        });
    };
    handleOk = () => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    handleOnChange=(values)=>{
       
        this.setState({
            selectedProject: values
        })
    };
        render(){
          const { getFieldDecorator } = this.props.form; 
            return(
                        <>  
                           <button className="custom-button custom-button-dark create-button margin-60"  onClick={this.showModal}>+ Assign User to Project</button>
                            <Modal
                                title="Assign User to Project"
                                visible={this.state.visible}
                                className="custom-table-popup"
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                                destroyOnClose={true}
                                footer={false}
                                centered
                            >
                            <Form onSubmit={this.handleSubmit}>
                                            <Row className="custom-popup-row">
                                                <Col span={24} offset={4} className="custom-form-wrap">
                                                    <Col span={16}>
                                                        <Form.Item>
                                                                {getFieldDecorator('projectId', {
                                                                    initialValue: this.props.project_id
                                                                    // initialValue: this.props.firmId
                                                                    })(<Input type="hidden"/>)}
                                                        </Form.Item>
                                                        <Form.Item label="Select User">
                                                            {getFieldDecorator('selectedProjectId', {
                                                                rules: [{ required:true, message: 'Please select your project!' }],
                                                            })(
                                                                <Select values="No Selected user" placeholder="Select a User" onChange={(values, event) => this.handleOnChange(values, event)}>
                                                                    {this.state.projectsContratorsById.map((opt,i)=>{
                                                                    return(
                                                                        <Option value={opt.id}>{opt.first_name} {opt.last_name}</Option>     
                                                                    )
                                                                    })}
                                                                </Select>
                                                            )}
                                                        </Form.Item>
                                                        <Form.Item label="Select Role">
                                                            {getFieldDecorator('userRole', {
                                                                rules: [{ required:true, message: 'Please select your project!' }],
                                                            })(
                                                                <Select value="No Selected Role" placeholder="Select a role">
                                                                    <Option value="CONTRACTOR">Contractor</Option>
                                                                    <Option value="SUPERVISOR">Supervisor</Option> 
                                                                    <Option value="PROJECT_MANAGER">Project Manager</Option>
                                                                </Select>
                                                            )}
                                                        </Form.Item>

                                                        

                                                        <Form.Item >
                                                            <Row type="flex" className="margin-top-40">
                                                                    <Col span={12} className="edit-black-buttons float-right">
                                                                        <Button type="primary" value="large" htmlType="submit">
                                                                            <p style={{color:'white', marginTop:'10px'}}>Assign User to Project</p>
                                                                        </Button>
                                                                    </Col>
                                                                    <Col span={12} className="unfill-edit-black-buttons float-left">
                                                                        <Button type="primary" value="large" onClick={this.handleCancel}>Cancel</Button>
                                                                    </Col>
                                                            </Row>
                                                        </Form.Item>
                                                    </Col>
                                                </Col>
                                            </Row>
                                        </Form>
                            </Modal>
                        </>
            )
    }
}



const ProjectsContractorAdd = Form.create({})(ProjectsContractorAddBtn);
export default ProjectsContractorAdd;