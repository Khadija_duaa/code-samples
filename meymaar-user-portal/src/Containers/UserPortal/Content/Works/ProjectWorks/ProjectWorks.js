import React from 'react';
import {fetchAllWorks} from "../../../../../store/project/project-actions";
import {connect} from 'react-redux';
import {getObjectValue} from "../../../../../utils/common-utils";
import NewLoader from '../../../../Loader/NewLoader';
import {Select, Pagination} from 'antd';

const Option = Select.Option;


let searchFields = ['name','phaseName','referenceNumber','station','divArea','initOffice'];

class ProjectWorks extends React.Component {

    state = {
        works: null,
        pageNumber: 1,
        currentPhase: "all",
        sortOrder: {},
        ascending: false
    };

    getWorkCategory = () => {
        const category_key = 'workType-category';
        let workState = sessionStorage.getItem(category_key);

        if (workState) {
            workState = JSON.parse(workState);
        }

        const {location} = this.props;

        if (location && location.state) {
            workState = location.state;
        }

        sessionStorage.setItem(category_key, JSON.stringify(workState));
        return workState;
    };


    componentDidMount() {
        this.fetchAllPlanningWorks();
        this.forceUpdate();
    }

    componentDidUpdate() {
        const {location} = this.props;

        if (this.state.pathname && location && location.pathname !== this.state.pathname) {
            this.fetchAllPlanningWorks();
        }
    }


    fetchAllPlanningWorks = () => {
        const {location} = this.props;

        let workState = this.getWorkCategory();
        let isWork = workState.type === 'work';


        let sortOrder = {
            order: this.state.sortOrder
        };

        this.props.fetchAllWorks(sortOrder, 'Planing', this.state.currentPhase, this.state.pageNumber, !isWork, () => {
            this.setState({pathname: location.pathname});
        });
    };


    onPageChange = (page) => {
        this.setState({pageNumber: page}, () => {
            this.fetchAllPlanningWorks();
        })
    };


    /************************* Events *************************/

    searchWork = (searchStr, searchParams = []) => {
        if (!this.props.worksList) return null;
        const works = [...this.props.worksList];

        if (searchStr) {
            let filteredWorks = [];
            works.forEach(work => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(work, param);

                    let isNumber = typeof (_val) === 'number';
                    let isString = typeof (_val) === 'string';

                    let isNumberIndex = _val && isNumber && _val.toString().toLowerCase().indexOf(searchStr.toLowerCase()) > -1;
                    let isStringIndex = _val && isString && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1;

                    if (isNumberIndex || isStringIndex) {
                        filteredWorks.push(work);
                        break;
                    }
                }
            });


            this.setState({works: filteredWorks});
        }
        else {
            this.setState({works: null});
        }
    };

    handleWorkClick = (projectId) => {

        let workState = this.getWorkCategory();

        let backText = workState.type === "work"?"Office":"Associated";
        let backCB = workState.type === "work"?"/dashboard/planning-works/myProjects":"/dashboard/planning-associated/associated";

        this.props.history.push({
            pathname: '/dashboard/works/overview',
            state: {projectId, category: workState.category,backTitle:`${backText} Planning Projects`,backCB}
        });
    };

    handlePhaseChange = (phaseId) => {

        this.setState({currentPhase: phaseId}, () => {
            this.fetchAllPlanningWorks();
        })
    };


    /**********************************************************/


    /****************** Sorted Regions ************************/

    // sortedParams = ["name", "phaseId", "referenceNumber", "station", "divArea", "officeName"];
    //
    // handleSortClick = (key,order)=>{
    //     this.setState({sortOrder:[key,order]},()=>{
    //         this.fetchAllPlanningWorks();
    //     })
    // };
    //
    // getSortedLayout = (header) => {
    //     let {ascending} = this.state;
    //
    //
    //     if (this.sortedParams.includes(header.key)) {
    //         return (
    //             <div>
    //                 {header.title}
    //                 <span style={{
    //                     float: "right",
    //                     display: "inline-grid",
    //                     marginRight: "30px",
    //                     height: "-webkit-fill-available"
    //                 }}>
    //                     <Icon type="caret-up"
    //                           onClick={() => this.handleSortClick(header.key,'asc')}/>
    //                     <Icon type="caret-down"
    //                           onClick={() => this.handleSortClick(header.key,'desc')}/>
    //                 </span>
    //             </div>
    //         )
    //     } else {
    //         return header.title;
    //     }
    // };


    /**********************************************************/




    renderHeading = () => {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <h3>Projects in Planning</h3>
                    </div>
                </div>
            </div>
        )
    };



    renderActionButtons = () => {


        let createWorkJSX = '';

        let phasesJSX = [];

        if (this.props.phases && this.props.phases.length) {
            phasesJSX = this.props.phases.map((phase, index) => {
                return <Option key={index} value={phase.uuid}>{phase.name}</Option>
            });
        }
        phasesJSX.unshift(<Option key={0} value={"all"}>All</Option>);
        return (
            <div className="container-fluid">
                <div className="row button-list">
                    <div className="col-md-4">
                        {createWorkJSX}
                    </div>
                    <div className="col-md-8 text-right margin-top-56">


                        {/* <!--dropdown-button--> */}
                        <div className="dropdown-button">
                            <Select defaultValue="All" className="new-add-assign-select"
                                    onChange={this.handlePhaseChange}>
                                {phasesJSX}
                            </Select>
                        </div>

                        {/* <!--Search-button--> */}
                        <div className="new-btn-with-gradient-select search-bar">
                            <form className="search-container">
                                <input type="text" id="search-bar" placeholder="Search…"
                                       onChange={(e) => this.searchWork(e.target.value, searchFields)}/>
                                <a><i className="fas fa-search"/></a>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        )
    };

    getTableBody = () => {

        if (!this.props.worksList) return null;

        let worksList = this.state.works ? this.state.works : this.props.worksList;

        return worksList.map((work, index) => {

            return (
                <tr style={{cursor: 'pointer'}} key={index}
                    onClick={() => this.handleWorkClick(work.projectId)}>
                    <td>{work.name}</td>
                    <td>{work.phaseName}</td>
                    <td>{work.referenceNumber}</td>
                    <td>{work.station}</td>
                    <td>{work.divArea}</td>
                    <td width="200">{work.initOffice}</td>
                </tr>
            )
        });
    };


    renderWorkDetails = () => {


        if (this.props.projectProcessing) {
            return <NewLoader/>
        }


        // Table Headers
        let tableHeaders = ["Works Name", "Phase Name", "Reference Number", "Station", "Div/Area", "Initiating Office"];

        // let headersJSON = [
        //     {
        //         title: "Works Name",
        //         key: "name"
        //     },
        //     {
        //         title: "Phase Name",
        //         key: "phaseId"
        //     },
        //     {
        //         title: "Reference Number",
        //         key: "referenceNumber"
        //     },
        //     {
        //         title: "Station",
        //         key: "station"
        //     },
        //     {
        //         title: "Div/Area",
        //         key: "divArea"
        //     },
        //     {
        //         title: "Initiating Office",
        //         key: "officeName"
        //     },
        // ];


        let headerJSX = tableHeaders.map((head, index) => {
            return (
                <th key={index} scope="col">
                    {head}
                </th>
            )
        });

        let {workPagination} = this.props;

        let worksList = this.state.works;


        // Table Body
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className=" custom-table table-responsive">
                            <table className="border-bottom custom-padding-for-start-column table border-0 ">
                                <thead className="thead-dark">
                                <tr>
                                    {headerJSX}
                                </tr>
                                </thead>

                                <tbody>
                                {this.getTableBody()}
                                </tbody>
                            </table>


                            {workPagination ?
                                <div style={{margin: "50px 0px", textAlign: "right"}}>
                                    <Pagination showTotal={total => `Total ${total} items`}
                                                current={this.state.pageNumber}
                                                onChange={this.onPageChange}
                                                pageSize={workPagination.pageSize}
                                                total={worksList ? worksList.length : workPagination.totalEntries}/>
                                </div> : null}
                        </div>
                    </div>
                </div>
            </div>
        )
    };

    render() {


        return (
            <>
                {this.renderHeading()}
                {this.renderActionButtons()}
                {this.renderWorkDetails()}
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllWorks: (sortOrder, projectType, phaseId, pageNumber, isAssociated = false, cb) => dispatch(fetchAllWorks(sortOrder, projectType, phaseId, pageNumber, isAssociated, cb))
    }
};

const mapStateToProps = state => {
    return {
        worksList: state.project_reducer.worksList,
        workPagination: state.project_reducer.workPagination,
        projectProcessing: state.project_reducer.processing,
        phases: state.project_reducer.phases
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(ProjectWorks);

export default connected;