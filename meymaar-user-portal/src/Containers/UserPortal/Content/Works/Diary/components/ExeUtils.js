import {hasAuthority} from '../../../../../../utils/common-utils'
import {store} from "../../../../../../index";

export const canApprovePayRAR = (officeId) => {

    let authorizations = sessionStorage.getItem('authState');
    let newSession = JSON.parse(authorizations);
    let data = {};

    const storageKey = "diary-state";

    let locationState = sessionStorage.getItem(storageKey);

    let exeHead = "";

    if (locationState) {
        locationState = JSON.parse(locationState);
        let {reportingOffice} = locationState.execOffice;
        exeHead = reportingOffice && reportingOffice.uuid;
    }


    for (let i = 0; i < newSession.activeUser.roles.length; i++) {
        let office = newSession.activeUser.roles[i].office;
        let userOffice = office.uuid;
        // headOfficeProject = office.headOffice.uuid;

        if (officeId === userOffice) {
            data.inOffice = true
        }
        if (exeHead === userOffice) {
            data.inHead = true
        }
    }

    return data;
};

let isInOffice = () => {

    let returnData = {
        inOffice: false,
        inHead: false,
        authority: false
    };

    let authorizations = sessionStorage.getItem('authState');
    let exeOffice = "";
    let exeHead = "";
    let exeParentOffice = "";
    let newSession = JSON.parse(authorizations);

    const storageKey = "diary-state";

    let locationState = sessionStorage.getItem(storageKey);


    if (locationState) {
        locationState = JSON.parse(locationState);
        let {execOffice} = locationState;
        exeOffice = execOffice.uuid;
        exeParentOffice = execOffice.parentOffice;
        exeHead = execOffice.reportingOffice && execOffice.reportingOffice.uuid;
    }

    let officeProject = "";
    // let headOfficeProject;

    for (let i = 0; i < newSession.activeUser.roles.length; i++) {
        let office = newSession.activeUser.roles[i].office;
        officeProject = office.uuid;

        if (exeOffice === officeProject || (exeParentOffice && exeParentOffice.uuid === officeProject)) {
            returnData.inOffice = true;
        }
        if (exeHead === officeProject) {
            returnData.inHead = true
        }
    }


    returnData.authority = hasAuthority("CAN_MANAGE_SOB");


    return returnData
};

let getProjectId = () => {
    let {getState} = store;

    // let overViewData = sessionStorage.getItem('overview-projectId');
    // let data = JSON.parse(overViewData);
    let projectDetails = getState().project_reducer.projectDetails;
    return projectDetails.projectId;
};

export default {
    isInOffice,
    getProjectId
}