import React from 'react';

import 'antd/dist/antd.css'
import ConfirmationModal from '../../../../../../../../Components/ModalFactory/MessageModal/ConfirmationModal';
import {createRAR} from "../../../../../../../../utils/billing-server-utils";
import RARForm from '../../../../../../../../Components/CustomForms/RARForm/RARForm';

class CreateBill extends React.Component {

    state = {
        error: null,
        showConfirmModal: false
    };


    // Form Items & Button Layout
    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16}
    };
    buttonItemLayout = {
        wrapperCol: {span: 24},
    };


    /******************************** EVENTS ****************************/

    /********************* MODAL ************************/



    handleCancelClick = () => {
        this.setState({showConfirmModal: false})
    };
    toggleConfirmationModal = (data) => {
        console.log("Data",data);
        this.setState({showConfirmModal: !this.state.showConfirmModal, billData: data})
    };


    submitBill = () => {

        this.setState({showConfirmModal: false}, () => {
            createRAR(this.state.billData, this.props.successCB);
        });
    };

    renderConfirmationModal = () => {
        let message = 'Are you sure, you want to create RAR?';

        return (
            <ConfirmationModal isOpen={this.state.showConfirmModal}
                               okCB={this.submitBill} cancelCB={this.handleCancelClick}
                               message={message}/>
        );
    };
    /********************** END *************************/



    render() {
        console.log("Request Details",this.props.requestDetails);
        return (
            <div>
                {this.renderConfirmationModal()}
                <RARForm {...this.props} showModal={(data)=>this.toggleConfirmationModal(data)}/>
            </div>
        )
    }
}

export default CreateBill;