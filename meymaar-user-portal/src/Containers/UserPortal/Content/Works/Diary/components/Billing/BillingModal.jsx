import React from "react";
import PropTypes from "prop-types";
import { Modal } from "antd";

const BillingRequestModal = props => {
  const { cancelCB, visible, title } = props;
  if (!visible) return null;
  return (
    <Modal
      onCancel={cancelCB}
      className="custom-table-popup"
      visible={visible}
      centered={true}
      title={title}
      footer={null}
    >
      {props.children}
    </Modal>
  );
};

BillingRequestModal.propTypes = {
  cancelCB: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired
};
export default BillingRequestModal;
