import React from 'react';
import axio from '../../../../../../utils/axios';
import { Row, Col, Button, Modal, message } from 'antd';
import { handleError } from '../../../../../../store/store-utils';
import { NOTIFICATION_TIME } from '../../../../../../utils/common-utils';
class Preview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            file: null,
            binaryData: "",
            visible: false
        }
    }

    componentDidMount() {
        let fileId = this.props.fileId;
        this.getTheFile(fileId)
    }

    handleOk = () => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };

    handlePreview = () => {
        this.setState({
            visible: true
        })
    };

    getTheFile = (fileId) => {
        axio.get(`/project-file-management/office/download/${fileId}`, true)
            .then(res => {
                var url = window.URL.createObjectURL(res.data);
                this.setState({
                    binaryData: url
                })
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage,NOTIFICATION_TIME);
            });
    };

    render() {
        return (
            <div>
                {/* <iframe src={this.state.binaryData}></iframe> */}
                <img alt={"preview"} style={{ width: '100px', height: '100px' }} src={this.state.binaryData} onClick={this.handlePreview}/>
                <Modal
                    title="Preview"
                    className="chat-model"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    centered={true}
                    onCancel={this.handleCancel}
                    footer={null}
                    maskClosable={false}
                >
                    <Row>
                        <Col span={24}>
                        <img alt={"binary"} style={{ width: '100%', height: '100%' }} src={this.state.binaryData} />
                        </Col>
                    </Row>
                    <Row>
                        <Col className="custom-colum" span={24}><Button className="modal-button " onClick={this.handleCancel}>OK</Button></Col>
                    </Row>
                </Modal>
            </div>
        );
    }
}
export default Preview;