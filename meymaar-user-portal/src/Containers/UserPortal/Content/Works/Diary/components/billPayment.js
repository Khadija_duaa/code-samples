import React from 'react';
import axio from '../../../../../../utils/axios';
import { Row, Col, Input, Button, Modal, message } from 'antd';
import { handleError } from '../../../../../../store/store-utils';
import { NOTIFICATION_TIME } from '../../../../../../utils/common-utils';

class BillPayment extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: this.props.visible,
            amount: "",
            title: "",
            projectId: this.props.projectId,
            projectWallet: {
                availableAmount: 0
            },
            higherAmount: false
        }
    }

    componentDidMount() {
        axio.get(`/billing-management/availableAmount/${this.state.projectId}`)
            .then(res => {
                if (res.data) {
                    this.setState({
                        projectWallet: res.data
                    })

                }
            })
            .catch(error => {
                let errorMessage = handleError(error);
               message.error(errorMessage,NOTIFICATION_TIME);
            });
    }

    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
        this.props.closePayment()
    }

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
        this.props.closePayment()
    }

    changeHandler = (e) => {
        let amt = e.target.value
        this.setState({
            amount: amt
        })

        if (amt > this.state.projectWallet.availableAmount) {
            this.setState({
                higherAmount: true
            })
        } else {
            this.setState({
                higherAmount: false
            })
        }

    }

    payBill = () => {

        axio.post(`/billing-management/projectPayment`,
            {
                requestPayment: {
                    projectId: this.state.projectId,
                    assignedAmount: this.state.amount,
                    paymentStatus: "PAID"
                }
            })
            .then(res => {
                this.success("Paid the bill")
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
        this.setState({ visible: false })
        this.props.closePayment()
    }

    success = (str) => {
        message.success(str);
    };

    error = (str) => {
        message.error(str);
    }

    render() {

        return (
            <div>
                <Modal
                    title="ENTER AMOUNT TO PAY"
                    className="chat-model"
                    centered={true}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={null}
                    maskClosable={false}

                >
                    <Row>
                        <Col span={24}>
                            <Row>Amount</Row>
                            <Row><Input className="mason-input" id="amount" placeholder="" value={this.state.amount} onChange={this.changeHandler} /></Row>
                            {this.state.higherAmount && <Row style={{ color: "red" }}>Wallet Balance is less than amount you entered</Row>}
                        </Col>
                    </Row>
                    <Row>
                        <Col className="custom-colum" span={24}><Button className="modal-button" disabled={this.state.higherAmount} onClick={this.payBill}>SUBMIT</Button></Col>
                    </Row>
                </Modal>
            </div>
        );
    }
}
export default BillPayment;