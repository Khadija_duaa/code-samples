import React from "react";
import {Row, Col, Tabs} from "antd";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import billActions from "../../../../../../store/billing/billing_action";
import BillingRequest from "./Billing/BillingRequest";
import Requested from "./Billing/Requested";

const TabPane = Tabs.TabPane;

class BillingRequests extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectId: props.projectDetails.projectId,
            paidApproved: this.props.billing.paidApproved,
            activeTab: "1"
        };
    }

    componentDidMount = () => {
        this.props.getApprovedreject(this.state.projectId);
    };

    changeTab = (key) => {
        this.props.getApprovedreject(this.state.projectId);
        this.setState({
            activeTab: key
        })
    };

    handleNavigationBR = (key) => {
        this.props.getApprovedreject(this.state.projectId);
        switch (key) {
            case 'reject':
                this.setState({
                    activeTab: "2"
                });
                break;
            case 'approve':
                this.setState({
                    activeTab: "3"
                });
                break;

            default:
                break;
        }
    };

    render() {
        return (
            <>
                {/* right box */}
                <Col span={18}>
                    <Row className="">
                        <Col span={24}>
                            <div className="custom-box">
                                <Tabs defaultActiveKey={this.state.activeTab} activeKey={this.state.activeTab}
                                      className="custom-tabs" size="large" onChange={this.changeTab}>
                                    <TabPane tab="REQUESTED" key="1">
                                        <Requested projectId={this.state.projectId} case={true}
                                                   navigationBR={this.handleNavigationBR}/>
                                    </TabPane>
                                    <TabPane tab="REJECTED Bill Requests" key="2">
                                        <BillingRequest status="REJECTED"/>
                                    </TabPane>
                                    <TabPane tab="PROCESSED Bill Requests" key="3">
                                        <BillingRequest status="PROCESSED"/>
                                    </TabPane>
                                </Tabs>
                            </div>
                        </Col>
                    </Row>
                </Col>
                {/* right box */}
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        billing: state.billing_reducer,
        projectDetails: state.project_reducer.projectDetails
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getApprovedreject: bindActionCreators(
            billActions.getApprovedPaidBills,
            dispatch
        )
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BillingRequests);
