import React from 'react';
import {Row, Col, Tabs} from "antd";
import {connect} from 'react-redux';
import RarRequests from "./RarRequests/RarRequests";
import {fetchRARDetails} from "../../../../../../../utils/billing-server-utils";
import RejectedRAR from "./RejectedRAR/RejectedRAR";
import PaidRAR from "./PaidRAR/PaidRAR";
import NewLoader from "../../../../../../Loader/NewLoader";


const TabPane = Tabs.TabPane;

class RAR extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            activeTab: "1",
            rarData: null,
        }
    }

    componentDidMount() {
        this.getRARDetails();
    }

    /************************** API Calls ******************/

    getRARDetails = () => {
        let {activeTab} = this.state;

        let {projectDetails} = this.props;

        let apiURL = '';
        switch (activeTab) {
            case "1":
                apiURL = `/billing-management/getCurrentRar/${projectDetails.projectId}`;
                break;
            case "2":
                apiURL = `/billing-management/getAllRar/REJECTED/${projectDetails.projectId}`;
                break;
            case "3":
                apiURL = `/billing-management/getAllRar/PAID/${projectDetails.projectId}`;
                break;
            default:
                break;
        }

        if (apiURL !== '') {
            fetchRARDetails(apiURL, activeTab, (data) => {
                this.setState({rarData: data})
            })
        }
    };
    /************************** END ************************/

    /************************ EVENTS **********************/
    handleTabChange = (key) => {
        this.setState({activeTab: key,rarData:null}, () => {
            this.getRARDetails();
        })
    };

    renderTabs = () => {
        let {rarData} = this.state;
        return (
            <Tabs className="custom-tabs rar-tabs" size="large" activeKey={this.state.activeTab}
                  onChange={this.handleTabChange}>

                <TabPane tab="REQUESTED" key="1">
                    <RarRequests payCB={()=>this.handleTabChange("3")} approveCB={()=>this.handleTabChange("1")} rejectCB={()=>this.handleTabChange("2")} rarData={rarData}  {...this.props}/>
                </TabPane>

                <TabPane tab="REJECTED RAR Requests" key="2">
                    <RejectedRAR rarData={rarData} {...this.props}/>
                </TabPane>

                <TabPane tab="Paid RAR" key="3">
                    <PaidRAR rarData={rarData} {...this.props}/>
                </TabPane>

            </Tabs>
        )
    };

    /************************* END ***********************/


    render() {
        if (this.props.processing) return <NewLoader/>;
        return (
            <>
                <Col span={18}>
                    <Row className="">
                        <Col span={24}>
                            <div className="custom-box">
                                {this.renderTabs()}
                            </div>
                        </Col>
                    </Row>
                </Col>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        projectDetails: state.project_reducer.projectDetails,
        processing: state.project_reducer.processing
    }
};

const connected = connect(mapStateToProps)(RAR);
export default connected