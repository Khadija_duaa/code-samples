import React from 'react';
import {Row, Col, Tabs, Table, Modal, message} from 'antd';
import {connect} from 'react-redux';
import axio from '../../../../../../utils/axios';
import {store} from "../../../../../../index";
import moment from 'moment';
import ExeUtils from './ExeUtils';
import {NOTIFICATION_TIME} from '../../../../../../utils/common-utils';
import {handleError} from '../../../../../../store/store-utils';
import {setProjectProcessing} from "../../../../../../store/project/project-actions";
import NewLoader from "../../../../../Loader/NewLoader";

const TabPane = Tabs.TabPane;

class MaterialAtSite extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            requestedMaterialSiteData: [],
            userId: 'ablkjslkdf',
            requestedMaterialSite: [],
            visible: false,
            modalData: "",
            historyData: "",
            projectId: ExeUtils.getProjectId(),
            dataConsumed: "",
            dataDumped: "",
            currentUnit: ""

        }
    }

    componentDidMount = () => {
        let {dispatch} = store;
        dispatch(setProjectProcessing(true));
        axio.get(`/material-management/material/materialAtSite/${this.state.projectId}`)
            .then(res => {
                const requestedMaterialSiteData = [];
                res.data.stock.forEach(reaterial => {
                    requestedMaterialSiteData.push(
                        {
                            ...reaterial,
                            received: reaterial.material_stock ? reaterial.material_stock.total_dumped : '-',
                            consumed: reaterial.material_stock ? reaterial.material_stock.total_consumed : '-',
                            available: reaterial.material_stock ? reaterial.material_stock.total_dumped - reaterial.material_stock.total_consumed : '-',

                        });
                });


                this.setState({
                    requestedMaterialSite: requestedMaterialSiteData,
                });

                dispatch(setProjectProcessing(false));
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);

                dispatch(setProjectProcessing(false));
            });
    };


    error = () => {
        message.error('Could Not Open Details');
    };
    handleOk = () => {
        this.setState({
            visible: false,
        })
    };

    handleCancel = () => {
        this.setState({
            visible: false
        })
    };

    rowClick = (data) => {
        this.setState({
            modalData: data,
            currentUnit: data.data.unit,
        });
        this.getHistory(data.data.id);
        this.showModal();
    };

    showModal = () => {
        this.setState({
            visible: true,
        })
    };

    getHistory = (id) => {
        axio.get(`/material-management/material/getHistory/${this.state.projectId}/${id}`)
            .then(res => {
                console.log("history data", res.data);
                if (res.data.stock) {

                    let dataConsumed = res.data.stock.material_consumed.map((consumed) => {
                        return {
                            quantity: consumed.quantity,
                            unit: this.state.currentUnit,
                            date: consumed.date
                        }
                    });
                    let dataDumped = res.data.stock.material_dumped.map((dumped) => {
                        return {
                            quantity: dumped.quantity,
                            unit: this.state.currentUnit,
                            date: dumped.date
                        }
                    });

                    this.setState({
                        historyData: res.data.stock,
                        dataConsumed: dataConsumed,
                        dataDumped: dataDumped
                    })

                } else {
                    this.setState({
                        historyData: "",
                        dataConsumed: "",
                        dataDumped: ""
                    });
                    this.error()
                }

            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };


    render() {

        if(this.props.processing) return <NewLoader/>;

        const columnsConsumed = [{
            title: 'QUANTITY',
            dataIndex: 'quantity',
            width: 100,
        },
            {
                title: 'UNIT',
                dataIndex: 'unit',
                width: 100,
            }, {
                title: 'DATE',
                dataIndex: 'date',
                width: 200,
                render: text => (moment(text).format('DD-MM-YYYY'))
            }]


        const columnsDumped = [{
            title: 'QUANTITY',
            dataIndex: 'quantity',
            width: 100,
        },
            {
                title: 'UNIT',
                dataIndex: 'unit',
                width: 100,
            }, {
                title: 'DATE',
                dataIndex: 'date',
                width: 200,
                render: text => (moment(text).format('DD-MM-YYYY'))
            }];


        const columns = [{
            title: 'Item',
            dataIndex: 'item',
        }, {
            title: 'Material',
            dataIndex: 'material',
            width: 200,
        }, {
            title: 'Unit',
            dataIndex: 'unit',
        }, {
            title: 'Received',
            dataIndex: 'received',
            width: 200,
        }, {
            title: 'Consumed',
            dataIndex: 'consumed',
            width: 200,
        }, {
            title: 'Avail At Site',
            dataIndex: 'availatsite',
            width: 200,
        }
        ];
        const data = this.state.requestedMaterialSite.map((data) => {
            return {
                key: data.id,
                item: data.id,
                material: data.title,
                unit: data.unit,
                received: data.received,
                consumed: data.consumed,
                availatsite: data.available,
                data: data
            }
        });

        return (
            <>
                {/* right box */}
                <Col span={18}>
                    <Row className="">
                        <Col span={24}>
                            <div className="custom-box">
                                <div className="custom-title" style={{padding: "0 0 23px 0"}}>Overview</div>
                                <Table columns={columns}
                                       dataSource={data}
                                       className="custom-new-table-click"
                                       size="middle"
                                       pagination={true}
                                       onRowClick={(record) => this.rowClick(record)}
                                />
                            </div>
                        </Col>
                    </Row>
                </Col>

                {this.state.modalData !== "" && this.state.historyData !== "" && <Modal
                    title="MATERIAL CONSUMPTION AND DUMPING HISTORY"
                    visible={this.state.visible}
                    className="custom-table-popup"
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    destroyOnClose={true}
                    footer={null}
                    maskClosable={false}
                >
                    <>
                        <Row>
                            <div className="capital-money">{this.state.historyData.title}</div>
                        </Row>
                        <Tabs>
                            <TabPane tab="MATERIAL CONSUMED" key="1">
                                <Table className="custom-new-table" columns={columnsConsumed}
                                       dataSource={this.state.dataConsumed}
                                       className="custom-table-read-click"
                                       size="middle"
                                       pagination={true}
                                />
                            </TabPane>
                            <TabPane tab="MATERIAL DUMPED" key="2">
                                <Table className="custom-new-table" columns={columnsDumped}
                                       dataSource={this.state.dataDumped}
                                       className="custom-table-read"
                                       size="middle"
                                       pagination={true}
                                />
                            </TabPane>
                        </Tabs>
                    </>
                </Modal>}
                {/* right box */}
            </>
        )
    }
}

const mapStateToProps = state=>{
  return{
      processing : state.project_reducer.processing
  }
};

const connected = connect(mapStateToProps)(MaterialAtSite);
export default connected;