import React from 'react';
import {Row, Col} from 'antd';
class SOBHeaderData extends React.Component {
    render() {
        return (
            <Row className="custom-header">
                <Col span={4} style={{textAlign: 'left', marginLeft: '30px'}}>Date</Col>
                <Col span={4} style={{textAlign: 'left'}}>BQ Category</Col>
                <Col span={4} style={{textAlign: 'left'}}>Linked Works</Col>
                <Col span={4} style={{textAlign: 'left'}}>Mason Name</Col>
                <Col span={4} style={{textAlign: 'left'}}>Labor</Col>
                <Col span={4}/>
            </Row>
        )
    }
}

export default SOBHeaderData