import React from 'react';
import axio from '../../../../../../utils/axios';
import {Row, Col, Input, Button, Modal, message, Form} from 'antd';
import { handleError } from '../../../../../../store/store-utils';
import { NOTIFICATION_TIME } from '../../../../../../utils/common-utils';

const { TextArea } = Input;
class Create_Sob extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: this.props.visible,
            inputMessage: "",
            title: ""

        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.visible !== this.props.visible) {
            this.setState({
                visible: this.props.visible
            })
        }
    }

    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };



    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {



                console.log('Received values of form: ',values);
                let ref = this;
                axio.post(`/site-visit-management/site-visits/saveVisitComplete`, {
                    projectId: `${this.props.projectId}`,
                    isFromOffice: "true",
                    title: `${values.title}`,
                    comments: [{
                        comment: `${values.comment}`,
                        commentType: "text",
                        office: "true"
                    }]
                })
                    .then(res => {
                        ref.props.reload();
                        this.props.visibility();
                        this.setState({
                            visible: false,
                        });
                    })
                    .catch(error => {
                        this.setState({
                            visible: true,
                        });
                        let errorMessage = handleError(error);
                        message.error(errorMessage,NOTIFICATION_TIME);
                    });


            }

        });
    };

    addSob = () => {

        this.setState({ visible: false })
    };

    success = () => {
        message.success('Added successfully.');
    };

    error = () => {
        message.error('Could not save.');
    };






    render() {

        const { getFieldDecorator } = this.props.form;
        return(

            <Modal
                title="CREATE SOB"
                centered={true}
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={null}
            >
                <Form onSubmit={(e)=>this.handleSubmit(e)}layout={"horizontal"}>
                    <Row>
                        <Col span={24}>

                            <Form.Item label="Title">
                                {getFieldDecorator('title', {
                                    rules: [{required: true, message: 'Please enter title!'}]
                                })(<Input className="mason-input" id="title" placeholder="Enter title"
                                          />)}
                            </Form.Item>

                            <Form.Item label="Comment">
                                {getFieldDecorator('comment', {
                                    rules: [{required: true, message: 'Please type comment!'}]
                                })(<TextArea className="mason-input" id="inputMessage" placeholder="Type Comment"
                                            />)}
                            </Form.Item>

                            <Form.Item >
                                <Row type="flex" >
                                <Col span={12} >

                                    <Button className="modal-button "type="primary" value="large" htmlType="submit" >
                                        SUBMIT
                                    </Button>
                                </Col>
                                </Row>

                            </Form.Item>


                        </Col>
                    </Row>
                </Form>
            </Modal>
        )
    }
}

const CreateSob = Form.create({})(Create_Sob);
export default CreateSob;