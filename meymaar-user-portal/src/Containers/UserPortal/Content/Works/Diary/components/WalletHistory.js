import React from 'react';
import axio from '../../../../../../utils/axios';
import moment from 'moment';
import {Table, Card, message} from 'antd';
import {NOTIFICATION_TIME} from '../../../../../../utils/common-utils';
import {handleError} from '../../../../../../store/store-utils';

class WalletHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectId: this.props.projectId,
            doneLoading: false,
            mainData: ''
        }
    }

    componentDidMount() {
        this.getHistoryWallet();
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.isWalletAmountChanged) {
            this.getHistoryWallet()
        }

    }


    getHistoryWallet = () => {
        axio.get(`/billing-management/projectWalletHistory/${this.state.projectId}`)
            .then(res => {
                let dataResult = res.data.walletHistory;
                let tableData = dataResult.map((data, i) => {
                    let amount = 0;
                    let paymentMethod = "";
                    let previousWallet = 0;
                    let currentWallet = 0;

                    if (data.transactionAmount === null || data.transactionAmount < 1) {

                        amount = data.paidVettedAmount - data.outstandingVettedAmount;
                        previousWallet = data.availableAmount - amount;
                        paymentMethod = "Payment";
                        currentWallet = amount + previousWallet;
                    } else {
                        amount = data.transactionAmount;
                        previousWallet = data.availableAmount;
                        currentWallet = amount + previousWallet;
                        paymentMethod = "Top Up"
                    }

                    return {
                        amount: Number(amount).toFixed(2),
                        paymentMethod,
                        date: data.dateCreated,
                        currentBalance:Number(currentWallet).toFixed(2),
                        balance:previousWallet.toFixed(2),
                        key: i
                    }
                });
                this.setState({
                    mainData: tableData,
                    doneLoading: true,
                })

            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };

    render() {
        let tableWidth = '20%';
        const columns = [
            {
                title: 'Date',
                dataIndex: 'date',
                render: text => (moment(text).format('DD-MM-YYYY')),
                width:tableWidth
            },
            {
                title: 'Transaction Type',
                dataIndex: 'paymentMethod',
                width:tableWidth
            },
            {
                title: 'Amount',
                dataIndex: 'amount',
                width:tableWidth
            },
            {
                title: 'Previous Wallet',
                dataIndex: 'balance',
                width:tableWidth
            },
            {
                title: 'Current Wallet',
                dataIndex: 'currentBalance',
                width:tableWidth
            },
            ];

        return (

            <div>
                <Card title="Transactions">
                    <div>
                        {this.state.doneLoading &&
                        <Table columns={columns} dataSource={this.state.mainData} scroll={{y: 250}}
                               className="custom-progress-table custom-table-read wallet-table" pagination={false} size="middle"/>}
                    </div>
                </Card>
            </div>

        );
    }
}

export default WalletHistory;