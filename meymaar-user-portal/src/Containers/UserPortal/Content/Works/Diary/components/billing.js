import React from "react";
import { Row, Col, Tabs } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import billActions from "../../../../../../store/billing/billing_action";
import ActiveBill from './Billing/ActiveBill'
import BillStaus from "./Billing/BillStatus";
import exUtil from './ExeUtils'

const { TabPane } = Tabs;

class BillingRequests extends React.Component {
  constructor(props) {
    super(props);
    console.log(props.projectDetails.projectId);
    this.state = {
      projectId: props.projectDetails.projectId,
      paidApproved: this.props.billing,
      activeBill: "1"
    };
    exUtil.isInOffice();
  }

  componentDidMount = () => {
    this.props.getAllBills(this.state.projectId);
  };

  changeTab = (key) => {
    this.props.getAllBills(this.state.projectId);
    this.setState({
      activeBill:key
    })
  };

  handleNavigationBill = (key) => {
    this.props.getAllBills(this.state.projectId);
    switch(key){
      case 'reject':
      this.setState({
        activeBill:"2"
      });
      break;
      case 'approve':
      this.setState({
        activeBill:"3"
      });
      break;
      case 'paid':
      this.setState({
        activeBill:"4"
      });
      break;

        default:
          break;
    }
  };


  render() {

    return (
      <>
        {/* right box */}
        <Col span={18}>
          <Row className="">
            <Col span={24}>
              <div className="custom-box">
                <Tabs defaultActiveKey={this.state.activeBill} activeKey={this.state.activeBill} className="custom-tabs" size="large" onChange={this.changeTab}>
                  <TabPane tab="REQUESTED BILL" key="1">
                    <ActiveBill projectId={this.state.projectId} handleNavigationBill={this.handleNavigationBill}/>
                  </TabPane>
                  <TabPane tab="REJECTED BILLS" key="2">
                    <BillStaus status="REJECTED"  projectId={this.state.projectId} handleNavigationBill={this.handleNavigationBill}/>
                  </TabPane>
                  <TabPane tab="APPROVED BILLS" key="3">
                    <BillStaus status="APPROVED" projectId={this.state.projectId} handleNavigationBill={this.handleNavigationBill}/>
                  </TabPane>
                  <TabPane tab="PAID BILLS" key="4">
                    <BillStaus status="PAID" projectId={this.state.projectId} handleNavigationBill={this.handleNavigationBill}/>
                  </TabPane>
                </Tabs>
              </div>
            </Col>
          </Row>
        </Col>
        {/* right box */}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    billing: state.billing_reducer,
      projectDetails:state.project_reducer.projectDetails
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getAllBills: bindActionCreators(billActions.getAllBills, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BillingRequests);
