import React, { Fragment } from "react";
import { Col, Row, Table } from "antd";
import moment from "moment";
import { formatCurrency } from '../../../../../../../utils/common-utils'

class BillingRequestModalContent extends React.Component {
  columns = [
    {
      title: "BQ Item Type",
      dataIndex: "bqItemType",
      width: 140
      // render: text => moment(text).format("YYYY-MM-DD")
    },
    {
      title: "Description",
      dataIndex: "bqItemDescription"
      // width: 300
    },
    {
      title: "Vetted Work Amount",
      dataIndex: "vettedWorkAmount",
      width: 125,
      render: work => formatCurrency(work)
    },
    {
      title: "Amount",
      dataIndex: "bqItemAmount",
      width: 125,
      render: amount => formatCurrency(amount)
    }
  ];

  renderRejectionReason = () => {
    const { bill } = this.props;
    if (bill.paymentStatus === "REJECTED") {
      return (
        <Row gutter={12} className="fields-container-bill">
          <Col span={12} className="title-text-billing">Rejection Reason</Col>
          <Col span={12} className="data-billing"> {bill.rejectionReason}</Col>
        </Row>
      );
    }
  };

  render() {
    const { bill } = this.props;

    // const styles = {
    //   color: bill.paymentStatus === "PAID" ? "green" : "orange",
    //   fontStyle: "bold"
    // };

    return (
      <Fragment>
        {this.renderRejectionReason()}

        <Row>
          <Col span={24}>
            <Row gutter={12} className="fields-container-bill">
              <Col span={12} className="title-text-billing">Date Create</Col>
              <Col span={12} className="data-billing">{moment(bill.dateCreated).format("DD-MM-YYYY")}</Col>
            </Row>

            <Row gutter={12} className="fields-container-bill">
              <Col span={12} className="title-text-billing">Requested By</Col>
              <Col span={12} className="data-billing"> {!bill.userName ? "NULL" : bill.userName}</Col>
            </Row>

            <Row gutter={12} className="fields-container-bill">
              <Col span={12} className="title-text-billing">Status</Col>
              <Col span={12} className="data-billing">{bill.paymentStatus ==="REJECTED"? "Rejected": "Approved"}</Col>
            </Row>

            <Row gutter={12} className="fields-container-bill">
              <Col span={12} className="title-text-billing">Total Vetted Amount</Col>
              <Col span={12} className="data-billing" style={{color:"red"}}>{formatCurrency(bill.totalVettedAmount)}</Col>
            </Row>

          </Col>
        </Row>
        <br />

        <br />
        <Row>
          <Table
            className="custom-new-table-click"
            rowKey={(record, index) => index}
            columns={this.columns}
            dataSource={bill.billingBqsItem}
            bordered
            scroll={{ y: 360 }}
          />
        </Row>
      </Fragment>
    );
  }
}

export default BillingRequestModalContent;
