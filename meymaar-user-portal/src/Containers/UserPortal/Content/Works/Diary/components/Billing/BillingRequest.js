import React, {Fragment} from "react";
import {Row, Col, Table} from "antd";
import {connect} from "react-redux";
import moment from "moment";
import BillingRequestModal from "./BillingRequestModal";
import BillingRequestModalContent from "./BillingRequestModalContent";
import NewLoader from "../../../../../../Loader/NewLoader";

const columns = [
    {
        title: "Invoice ID",
        dataIndex: "viewId",
        width: 90
    },
    {
        title: "Date",
        dataIndex: "dateCreated",
        width: 90,
        render: text => moment(text).format("DD-MM-YYYY")
    },
    {
        title: "Amount",
        dataIndex: "totalVettedAmount",
        width: 90
    },
    {
        title: "Requested By",
        dataIndex: "userName",
        width: 90,
        render: (text) => text || "N/A"
    }
];

class BillingRequest extends React.Component {
    state = {
        showModal: false,
        billViewId: "",
        bill: {}
    };

    handleRowClick = event => {
        const {viewId} = event;
        this.setState(() => ({
            showModal: true,
            billViewId: viewId,
            bill: event
        }));
    };

    handleToggle = () => {
        this.setState(state => ({showModal: !state.showModal}));
    };

    getDataSource = () => {
        const {status} = this.props;
        const approvedBills = this.props.billing.paidApproved.history.filter(
            item => {
                return item.paymentStatus === status;
            }
        );

        return approvedBills;
    };

    render() {
        const {
            // eslint-disable-next-line
            billing: {paidApproved}
        } = this.props;

        if (paidApproved === "loading") {
            return (
                <>
                    <div>
                        <NewLoader/>
                    </div>
                </>
            );
        }
        if (paidApproved === "error") {
            return (
                <>
                    <div>
                        <div>Error</div>
                    </div>
                </>
            );
        }
        return (
            <Fragment>
                <BillingRequestModal
                    visible={this.state.showModal}
                    title={this.state.billViewId}
                    cancelCB={this.handleToggle}
                >
                    <BillingRequestModalContent bill={this.state.bill}/>
                </BillingRequestModal>
                <Row>
                    <Col>
                        <Table
                            className="custom-new-table-click"
                            columns={columns}
                            dataSource={this.getDataSource()}
                            onRowClick={this.handleRowClick}
                            rowKey={(record, index) => index}
                        />
                    </Col>
                </Row>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        billing: state.billing_reducer
    };
};

export default connect(mapStateToProps)(BillingRequest);
