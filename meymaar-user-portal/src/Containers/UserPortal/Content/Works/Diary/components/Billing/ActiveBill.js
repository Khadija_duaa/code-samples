
import React from 'react';
import {Row, Col, Button, Modal, message, Input} from 'antd';
import {formatCurrency, NOTIFICATION_TIME} from '../../../../../../../utils/common-utils'

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import axio from '../../../../../../../utils/axios';
import billActions from "../../../../../../../store/billing/billing_action";
import execUtil from '../ExeUtils';
import {handleError} from '../../../../../../../store/store-utils';
import NewLoader from '../../../../../../Loader/NewLoader';


// const customPanelStyle = {
//   background: '#f7f7f7',
//   borderRadius: 4,
//   marginBottom: 24,
//   border: 0,
//   overflow: 'hidden',
// };

class ActiveBill extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rights: false,
            createView: false,
            visible: false,
            projectId: this.props.projectId,
            comment: "",
            permissions: execUtil.isInOffice()
        }
    }

    componentDidMount() {
        this.props.getAllBills(this.state.projectId);
        let authorizations = sessionStorage.getItem('authState');
        let newSession = JSON.parse(authorizations);
        let rights = newSession.activeUser.authorities;
        for (let i = 0; i < rights.length; i++) {
            if (rights[i] === "CAN_APPROVE_BILL") {
                this.setState({
                    rights: true
                })
            }
        }
    }

    handleApprove = (id) => {
        axio.post(`/billing-management/approveBillStatus/${this.state.projectId}/${id}`)
            .then(res => {
                console.log(res.data);
                this.props.getAllBills(this.state.projectId);
                this.props.handleNavigationBill("approve")
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
        this.setState({visible: false})
    };


    error = () => {
        message.error('Could not save');
    };

    handleReject = (id) => {
        let myData = {
            "rejectionReason": this.state.comment
        };
        axio.put(`/billing-management/rejectBill/${this.state.projectId}/${id}`, myData)
            .then(res => {
                console.log(res.data);
                this.props.getAllBills(this.state.projectId);
                this.setState({
                    visible: false,
                });
                this.props.handleNavigationBill("reject")
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
                this.setState({
                    visible: false,
                });
            });
    };

    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    commentHandler = (e) => {
        this.setState({
            comment: e.target.value
        })
    };

    // createBill = () => {
    //   this.setState({
    //     createView: true
    //   })
    // };

    openMod = () => {
        this.setState({
            visible: true
        })
    };

    render() {
        if (this.props.billing.allBills === "loading") {
            return (<>
                <div>
                    <NewLoader/>
                </div>
            </>)
        } else if (this.props.billing.allBills === "error") {
            return (<>
                <div>
                    <div>Error</div>
                </div>
            </>)
        }
        else if (this.props.billing.allBills.pendingBills && this.props.billing.allBills.pendingBills.length <= 0) {
            return (<>
                <div>
                    <div>NO DATA FOUND</div>
                </div>
            </>)
        }
        else {
            let requested = this.props.billing.allBills.pendingBills;

            let handReceiptsD = {};
            if (requested[0] && requested[0].handReceipts.length > 0) {
                handReceiptsD = requested[0].handReceipts.map((item) => {
                    return {
                        amount: item.amount,
                        description: item.description
                    }
                })
            }
            handReceiptsD.push({
                amount: requested[0].totalHandReceiptAmount,
                description: "Total"
            });

            return (<>
                <div>
                    {requested && requested.length > 0 &&
                    <div style={{padding: "20px"}}><Row gutter={12} className="fields-container">


                        <Row>
                            <Col span={24}>

                                <Row gutter={12} className="fields-container-bill">
                                    <Col span={12} className="title-text-billing">Generated by</Col>
                                    <Col span={12} className="data-billing">{requested[0].generatedUserName}</Col>
                                </Row>

                                <Row gutter={12} className="fields-container-bill">
                                    <Col span={12} className="title-text-billing">Payment status</Col>
                                    <Col span={12} className="data-billing">{requested[0].paymentStatus}</Col>
                                </Row>

                                <Row gutter={12} className="fields-container-bill">
                                    <Col span={12} className="title-text-billing">Paid Amount</Col>
                                    <Col span={12}
                                         className="data-billing">{requested[0].wallet ? formatCurrency(requested[0].wallet.paidAmount) : formatCurrency(0)}</Col>
                                </Row>

                                <Row gutter={12} className="fields-container-bill">
                                    <Col span={12} className="title-text-billing">Store Amount</Col>
                                    <Col span={12}
                                         className="data-billing">{formatCurrency(requested[0].storeAmount)}</Col>
                                </Row>

                                <Row gutter={12} className="fields-container-bill">
                                    <Col span={12} className="title-text-billing">Advance Amount</Col>
                                    <Col span={12}
                                         className="data-billing">{formatCurrency(requested[0].advanceAmount)}</Col>
                                </Row>

                                <Row gutter={12} className="fields-container-bill">
                                    <Col span={12} className="title-text-billing">Hold Amount</Col>
                                    <Col span={12}
                                         className="data-billing">{formatCurrency(requested[0].holdingAmount)}</Col>
                                </Row>

                                <Row gutter={12} className="fields-container-bill">
                                    <Col span={12} className="title-text-billing">Hold %</Col>
                                    <Col span={12} className="data-billing">{requested[0].holdingPercentage}</Col>
                                </Row>

                            </Col></Row>
                    </Row>
                        <Row className="fields-container">
                        </Row>
                        <Row className="fields-container final-ammount">
                            <Col span={12} className="amount-container"><span
                                className="main-price-cus">FINAL AMOUNT:</span></Col>
                            <Col span={12}><span
                                className="main-price-cus">{requested[0].finalBillAmount ? formatCurrency(requested[0].finalBillAmount) : null}</span></Col>
                        </Row>

                        {
                            this.state.permissions.authority && (this.state.permissions.inHead || requested[0].wallet && requested[0].wallet.totalProjectAmount < 2000000) && requested[0].isEven &&
                        <Row style={{marginTop: "20px", marginBottom: '40px'}} gutter={14}>
                            <Col span={7}/>
                            <Col span={5}><Button type="primary" shape="round" size='large' className="submit-bill"
                                                  onClick={() => this.handleApprove(requested[0].id)}>APPROVE</Button></Col>
                            <Col span={5}><Button type="primary" shape="round" size='large' className="rejected-bill"
                                                  onClick={this.openMod}>REJECT</Button></Col>
                            <Col span={7}/></Row>
                        }
                        {this.state.permissions.authority && this.state.permissions.inOffice && !requested[0].isEven &&
                        <Row style={{marginTop: "20px", marginBottom: '40px'}} gutter={14}>
                            <Col span={7}/>
                            <Col span={5}><Button type="primary" shape="round" size='large' className="submit-bill"
                                                  onClick={() => this.handleApprove(requested[0].id)}>APPROVE</Button></Col>
                            <Col span={5}><Button type="primary" shape="round" size='large' className="rejected-bill"
                                                  onClick={this.openMod}>REJECT</Button></Col>
                            <Col span={7}/></Row>
                        }
                    </div>
                    }
                    <div><Modal
                        title="REJECTION COMMENT"
                        className="chat-model"
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                        footer={null}
                    >
                        <Row>
                            <Col span={24}>
                                <Row>Comment</Row>
                                <Row><Input className="mason-input" id="inputMessage" placeholder=""
                                            value={this.state.comment} onChange={this.commentHandler}/></Row>
                            </Col>
                        </Row>
                        <Row style={{marginTop: "15px"}}>
                            <Col style={{marginTop: "15px"}} className="custom-colum" span={24}><Button
                                className="modal-button "
                                onClick={() => this.handleReject(requested[0].id)}>SUBMIT</Button></Col>
                        </Row>
                    </Modal></div>
                </div>
            </>)
        }

    }

}


const mapStateToProps = (state) => {
    return {
        billing: state.billing_reducer
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getAllBills: bindActionCreators(
            billActions.getAllBills,
            dispatch
        )
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ActiveBill);