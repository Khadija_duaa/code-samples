import React from 'react';
import {Row, Col, Tabs, Button} from 'antd';
import ShowChat from '../showChat';
import CreateSob from '../CreatSob';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import sobActions from "../../../../../../../store/sob/sob-action";
import execUtil from '../ExeUtils'
import moment from 'moment'
import NewLoader from '../../../../../../Loader/NewLoader';
import SOBHeaderData from "./HeaderData";

const TabPane = Tabs.TabPane;

class SiteOrderBook extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,

            tableData: 0
        };
        this.columns = [{
            title: 'Description of Work',
            dataIndex: 'descriptionofwork',
            width: 500,
        }, {
            title: 'status',
            dataIndex: 'completed',
            width: 80,
            render: (text, record) => (
                this.state.dataSource.length >= 1
                    ? (
                        <Button className="view-details" onClick={() => this.handleView(record)}>
                            View Details
                        </Button>
                    ) : null
            ),
        }
        ];

        this.state = {
            detailWorkView: false,
            visibleModal: false,
            projectId: execUtil.getProjectId(),
            permissions: execUtil.isInOffice(),
            sobPendingData: [],
            sobData: [],
            sobGeData: [],
            sobVettedData: [],
            sobRejectedData: [],
            sobObject: {comments: []},

            inputMessage: "",
            reduxState: {
                sobData: "loading",
                rejected: "loading",
                pending: "loading",
                vetted: "loading"
            },

            modalTitle: false,
            create: false,
            currentTab: "2",
            count: 2,
        };
    }

    componentDidMount = () => {
        this.handleTab(this.state.currentTab);
    };


    handleView = (sob) => {

        this.setState({
            sobObject: sob,
            modalTitle: true
        });
        this.showModal();
    };


    showModal = () => {
        this.setState({
            visible: true,
            create: false
        });
    };


    createSob = () => {
        this.setState({
            create: true,
            visible: false
        });
        this.props.getSobData(this.state.projectId);
    };

    handleTab = (tab) => {
        switch (tab) {
            case '1':
                this.props.getSobData(this.state.projectId);
                this.setState({
                    currentTab: '1',
                    create: false,
                    visible: false,
                    isPending: false,
                });
                break;
            case '2':
                this.props.getPending(this.state.projectId);
                this.setState({
                    currentTab: '2',
                    create: false,
                    visible: false,
                    isPending: true,
                });
                break;

            case '3':
                this.props.getApproved(this.state.projectId);
                this.setState({
                    currentTab: '3',
                    create: false,
                    visible: false,
                    isPending: false,
                });
                break;

            case '4':
                this.props.getRejected(this.state.projectId);
                this.setState({
                    currentTab: '4',
                    create: false,
                    visible: false,
                    isPending: false,
                });
                break;

            default:
                break;
        }

    };

    refreshPage = (status) => {
        this.props.getSobData(this.state.projectId);
        this.props.getApproved(this.state.projectId);
        this.props.getRejected(this.state.projectId);
        this.props.getPending(this.state.projectId);

        if (status && status === "Approved") {

            this.setState({
                currentTab: '3',
                create: false,
                visible: false,
                isPending: false,
            });
        } else if (status && status === "Rejected") {
            this.setState({
                currentTab: '4',
                create: false,
                visible: false,
                isPending: false,
            });
        }

    };


    ResultData = (sob) => {
        let masons = [];

        let sobCount = [];
        sob.workDone && sob.workDone.bq && sob.workDone.bq.map((x, index) => {
            sobCount.push(index)
        });

        sob.workDone && sob.workDone.masons && sob.workDone.masons.map((mas) => {
            masons.push(sob.workDone.masons.length > 1 && mas.name !== sob.workDone.masons[0].name ? ", " + mas.name : mas.name)
        });
        let name = '';
        if (sob.workDone && sob.workDone.bq && sob.workDone.bq) {
            name = sob.workDone.bq[0] && sob.workDone.bq[0].name;


        }
        else {
            name = null
        }

        return (

            <span>
                <Col span={4}
                     style={{textAlign: 'left', marginLeft: '30px'}}>{moment(sob.date).format('MM-DD-YYYY')}</Col>
      <Col span={4} style={{textAlign: 'left'}}>{name}</Col>
                <Col span={4} style={{textAlign: 'left'}}>{sobCount.length}</Col>
      <Col span={4} style={{textAlign: 'left'}}>{masons}</Col>
      <Col span={4} style={{textAlign: 'left'}}>{sob.workDone.labor_count}</Col>
      <Col span={3}><Button className="view-details"
                            onClick={() => this.handleView(sob)}>View Details</Button></Col></span>
        )
    };


    /************************************ MODAL ***********************************/

    /***************** Rendering Chat Modal *************/

    closeChat = (status) => {
        this.setState({
            visible: false
        })
    };
    renderChatModal = () => {
        let {modalTitle, visible, isPending, currentTab, sobObject} = this.state;


        if (modalTitle && visible) {

            return (
                <ShowChat isPending={isPending} sobObj={sobObject} data={this.state} tab={currentTab}
                          closeChat={this.closeChat} rejectResolve={(status) => this.refreshPage(status)}/>
            )
        }
    };


    /***************** END *************/


    /***************** Rendering Create SOB Modal *************/

        // Sob visibility
    sobVisibility = () => {
        this.setState({
            create: false
        })
    };

    renderCreateSOBModal = () => {
        let {create} = this.state;

        if (create) {
            return (
                <CreateSob projectId={this.state.projectId} visible={create} visibility={this.sobVisibility}
                           reload={this.refreshPage}/>
            )
        }
    };

    /***************** END Rendering Create SOB Modal *************/

    /*********************************** END **************************************/


    /*********************************** Tabs ************************************/


    /******************* Rejected SOB TAB *****************/

    renderRejectedSOBDetails = () => {
        let {sobRejected} = this.props;
        if (!sobRejected || !sobRejected.length) return <h6>No Rejected SOB Exists!</h6>;

        return (
            <Col span={24}>
                <SOBHeaderData/>
                {sobRejected.map((sob, i) => {
                    if (sob.workDone) {

                        return (
                            <Row key={i} className="custom-row-table">
                                {this.ResultData(sob)}
                            </Row>
                        )
                    }
                })}
            </Col>
        )
    };

    renderRejectedSOBTab = () => {

        return (
            <TabPane onActive={this.state.sobData = this.state.sobRejectedData}
                     onTabClick={this.handleTab} tab="Rejected" key="4">
                {this.renderRejectedSOBDetails()}
            </TabPane>
        )
    };

    /******************* END *****************/


    /******************* Approved SOB TAB *****************/

    renderApprovedSOBDetails = () => {
        let {sobApproved} = this.props;
        if (!sobApproved || !sobApproved.length) return <h6>No Approved SOB Exists!</h6>;

        return (
            <Col span={24}>
                <SOBHeaderData/>
                {sobApproved.map((sob, i) => {
                    if (sob.workDone) {
                        return (
                            <Row key={i} className="custom-row-table">
                                {this.ResultData(sob)}
                            </Row>

                        )
                    }
                })}
            </Col>
        )
    };

    renderApprovedSOBTab = () => {
        return (
            <TabPane onActive={this.state.sobData = this.state.sobVettedData}
                     onTabClick={this.handleTab} tab="vetted/Approved" key="3">
                {this.renderApprovedSOBDetails()}
            </TabPane>
        )
    };

    /******************* END *****************/


    /******************* Pending SOB TAB *****************/

    renderPendingSOBDetails = () => {
        let {sobPending} = this.props;
        if (!sobPending || !sobPending.length) return <h6>No Pending SOB Exists!</h6>;


        return (
            <Col span={24}>
                <SOBHeaderData/>
                {sobPending.map((sob, i) => {
                    if (sob.workDone) {
                        return (
                            <Row key={i} className="custom-row-table">
                                {this.ResultData(sob)}
                            </Row>

                        )
                    }
                })}
            </Col>
        )
    };

    renderPendingSOBTab = () => {
        return (
            <TabPane onActive={this.state.sobData = this.state.sobPendingData} onTabClick={this.handleTab} tab="pending"
                     key="2">
                {this.renderPendingSOBDetails()}
            </TabPane>
        )
    };

    /******************* END *****************/


    /******************* Creating SOB TAB *****************/

    renderSOBDetails = () => {
        if (!this.props.sobDetails) return null;

        return this.props.sobDetails.map((sob, i) => {
            return <Row type="flex" align="middle" className="column-wrap" key={i}>
                <Col span={18} md={24} lg={18}>
                    <h6>{sob.title}</h6>
                </Col>

                <Col span={6} md={24} lg={6} className="view-details-column">
                    <Button className="view-details"
                            onClick={() => this.handleView(sob)}>
                        View Details
                    </Button>
                </Col>
            </Row>
        })
    };

    renderCreateSOBTab = () => {
        return (
            <TabPane tab="Ordered by ge" key={"1"}>
                {
                    (this.state.permissions.inOffice) && this.state.permissions.authority &&
                    <Row type="flex" align="middle" className="column-wrap">
                        <Col style={{paddingLeft: "25px"}} span={18}>
                            <Button type="danger" className="view-details" icon="plus"
                                    onClick={this.createSob}>ADD</Button></Col>
                    </Row>
                }
                {this.renderSOBDetails()}
            </TabPane>
        )
    };

    /******************* END *****************/


    /*********************************** Tabs END ************************************/

    render() {
        if (this.props.processing) return <NewLoader/>;
        return (
            <>
                {this.renderChatModal()}
                {this.renderCreateSOBModal()}

                {/* right box */}
                <Col span={18}>
                    <Row className="">
                        <Col span={24}>
                            <div className="custom-box">
                                <Tabs defaultActiveKey={this.state.currentTab} onChange={this.handleTab} size="large"
                                      className="custom-tabs">
                                    {this.renderCreateSOBTab()}
                                    {this.renderPendingSOBTab()}
                                    {this.renderApprovedSOBTab()}
                                    {this.renderRejectedSOBTab()}
                                </Tabs>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </>
        )
    }
}


const mapStateToProps = state => {
    return {
        sob: state.sob_reducer,
        sobDetails: state.sob_reducer.sobData,
        sobRejected: state.sob_reducer.rejected,
        sobPending: state.sob_reducer.pending,
        sobApproved: state.sob_reducer.vetted,
        processing: state.project_reducer.processing
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getSobData: bindActionCreators(sobActions.getSobData, dispatch),
        getApproved: bindActionCreators(sobActions.getApproved, dispatch),
        getRejected: bindActionCreators(sobActions.getRejected, dispatch),
        getPending: bindActionCreators(sobActions.getPending, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SiteOrderBook);