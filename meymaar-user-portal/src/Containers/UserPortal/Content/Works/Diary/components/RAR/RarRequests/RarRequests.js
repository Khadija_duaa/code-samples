import React from 'react';
import RARForm from '../../../../../../../../Components/CustomForms/RARForm/RARForm';
import ConfirmationModal from '../../../../../../../../Components/ModalFactory/MessageModal/ConfirmationModal';
import {Form, Input, Button, Modal} from 'antd';
import {payRAR, updateRARStatus} from "../../../../../../../../utils/billing-server-utils";
import {BUTTON_COLOR} from "../../../../../../../../utils/common-utils";


class RarRequests extends React.Component {

    state = {
        rejectionModal: false,
        showConfirmModal: false,
        requestDetails: null,
        formData: null,
    };

    formItemLayout = {
        labelCol: {span: 6},
        wrapperCol: {span: 18}
    };

    buttonItemLayout = {
        wrapperCol: {offset: 6},
    };


    /********************* EVENTS **********************/

    changeRARStatus = (status) => {
        let {form, projectDetails, rarData, rejectCB,approveCB} = this.props;
        let formData = {
            rejectionReason: form.getFieldValue('rejectionReason') ? form.getFieldValue('rejectionReason') : null,
            status: status
        };


        updateRARStatus(formData, projectDetails.projectId, rarData.id, status, approveCB, rejectCB);
    };
    /********************** END ************************/

    /**************************** MODAL **********************/
    handleCancelClick = () => {
        this.setState({showConfirmModal: false})
    };
    handleConfirmClick = (status) => {
        if (status === "APPROVED") {
            this.changeRARStatus(status);
        } else {
            let {projectDetails, rarData, payCB} = this.props;
            let data = {
                paidAmount: this.state.formData['paidAmount'],
                outStandingAmount:this.state.formData['outStandingAmount']
            };

            payRAR(data, projectDetails.projectId, rarData.id, payCB)
        }

    };

    renderConfirmationModal = () => {
        let {formData} = this.state;
        let message = `Are you sure, you want to ${formData ? "pay" : "approve"} RAR?`;

        return (
            <ConfirmationModal isOpen={this.state.showConfirmModal}
                               okCB={() => this.handleConfirmClick(formData ? "PAID" : "APPROVED")}
                               cancelCB={this.handleCancelClick}
                               message={message}/>
        );
    };

    toggleConfirmationModal = (data, isPayment = false) => {

        this.setState({showConfirmModal: !this.state.showConfirmModal, formData: isPayment ? data : null})
    };


    toggleRejectionModal = () => {
        this.setState({rejectionModal: !this.state.rejectionModal})
    };

    renderRejectionReasonForm = () => {
        let {getFieldDecorator} = this.props.form;
        return (
            <Form layout={"horizontal"}>
                <Form.Item {...this.formItemLayout} label={"Reason"}>
                    {getFieldDecorator('rejectionReason', {rules: [{required: true, message: "Required!",whitespace:true}]})(<Input
                        placeholder={"Reason"}/>)}
                </Form.Item>

                <Form.Item {...this.buttonItemLayout}>
                    <Button onClick={() => this.changeRARStatus('REJECTED')} style={{
                        height: "50px",
                        width: "100px",
                        backgroundColor: BUTTON_COLOR,
                        color: "#fff",
                    }}>Submit</Button>

                    <Button style={{height: "50px", width: "100px", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                            type={"default"} onClick={this.toggleRejectionModal}>Cancel</Button>
                </Form.Item>
            </Form>
        )
    };

    renderRejectionModal = () => {
        return (
            <Modal
                title="Rejection Reason"
                visible={this.state.rejectionModal}
                onOk={this.handleOk}
                onCancel={this.toggleRejectionModal}
                footer={null}
                centered={true}
            >
                {this.renderRejectionReasonForm()}
            </Modal>
        )
    };


    /************************** END *************************/


    render() {
        let {rarData} = this.props;


        if (!rarData) return <h6>No Requested RAR Exists!</h6>;

        let formData = {
            isPreview: true,
            requestDetails: rarData,
            isPayment: true
        };

        return (
            <div className={"preview-rar-form"}>
                {this.renderRejectionModal()}
                {this.renderConfirmationModal()}

                <RARForm  {...formData} showModal={(data) => this.toggleConfirmationModal(data, true)}
                          onApproveClick={this.toggleConfirmationModal}
                          onRejectionClick={this.toggleRejectionModal}/>
            </div>
        )
    }
}


export default Form.create()(RarRequests);