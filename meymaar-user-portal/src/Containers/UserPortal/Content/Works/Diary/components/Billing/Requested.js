import React from 'react';
import {Row, Col, Button, Input, message, Modal} from 'antd';
import {formatCurrency, hasAuthority, NOTIFICATION_TIME} from '../../../../../../../utils/common-utils'
import Moment from 'react-moment';
import CreateBill from './CreateBill/CreateBill';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import axio from '../../../../../../../utils/axios';
import billActions from "../../../../../../../store/billing/billing_action";
import execUtil from '../ExeUtils'
import {handleError} from '../../../../../../../store/store-utils';
import NewLoader from '../../../../../../Loader/NewLoader';
import {fetchRARData} from "../../../../../../../utils/billing-server-utils";


class Requested extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rights: false,
            createView: false,
            projectId: this.props.projectId,
            case: this.props.case,
            comment: "",
            viewReject: false,
            permissions: execUtil.isInOffice()
        }
    }

    componentDidMount() {
        this.props.getApprovedreject(this.state.projectId, () => {
            fetchRARData((requestDetails) => {
                this.setState({requestDetails});
            })
        });

        let authorizations = sessionStorage.getItem('authState');
        let newSession = JSON.parse(authorizations);
        let rights = newSession.activeUser.authorities;
        for (let i = 0; i < rights.length; i++) {
            if (rights[i] === "CAN_CREATE_BILL") {
                this.setState({
                    rights: true
                })
            }
        }
    }


    createBill = () => {
        this.setState({
            createView: true
        })
    };

    handleCloseCreate = () => {
        this.setState({
            createView: false
        })
    };

    commentHandler = (e) => {
        this.setState({
            comment: e.target.value
        })
    };
    rejectBill = (key) => {
        switch (key) {
            case 1:
                this.setState({
                    viewReject: true
                });
                break;
            case 2:
                let id = this.props.billing.paidApproved.requestedBills[0].id;
                let myData = {
                    "rejectionReason": this.state.comment
                };
                axio.put(`/billing-management/rejectBillRequest/${this.state.projectId}/${id}`, myData)
                    .then(res => {
                        this.setState({
                            viewReject: false,
                        });
                        this.props.getApprovedreject(this.state.projectId);
                        this.props.navigationBR('reject');
                    })
                    .catch(error => {
                        let errorMessage = handleError(error);
                        message.error(errorMessage, NOTIFICATION_TIME);
                        this.setState({
                            viewReject: false,
                        });
                    });
                break;

            default:
                break;
        }
    };

    error = () => {
        message.error('Could not save');
    };

    handleCreateBill = () => {
        this.setState({createView: false}, () => {
            this.props.navigationBR('approve');
        });

    };

    viewRejectToggle = () => {
        this.setState({
            viewReject: !this.state.viewReject
        })
    };


    /*********************** Create Bill Modal ********************/
    renderCreateBillModal = () => {
        if (!this.state.requestDetails) return null;
        return (
            <Modal
                title="Create RAR"
                className="create-bill-model"
                visible={this.state.createView}
                onOk={this.handleOk}
                onCancel={this.handleCloseCreate}
                footer={null}
                centered={true}
            >
                <CreateBill submitCB={this.handleCloseCreate}
                            successCB={this.handleCreateBill} requestDetails={this.state.requestDetails}/>
            </Modal>
        )
    };

    /*************************************************************/
    render() {
        // let wallet1 = {
        //     paidAmount: 0,
        //     totalVettedAmount: 0
        // };

        if (this.props.billing.paidApproved === "loading") {
            return (<>
                <div><NewLoader/></div>
            </>)
        } else if (this.props.billing.paidApproved === "error") {
            return (<>
                <div>
                    <div style={{color: "red"}}>Could Not Fetch Your Data</div>
                </div>
            </>)
        } else {
            let requested = this.props.billing.paidApproved.requestedBills;
            let wallet = this.props.billing.paidApproved.wallet;
            return (<>
                <div>

                    {requested && requested.length > 0 &&
                    <div>
                        <>
                            <Row>
                                <Col span={18} className="top-heading-bill"
                                     style={{fontWeight: "bold"}}>{requested[0].viewId ? requested[0].viewId : 'B11'}</Col>
                                <Col span={6} className="top-heading-bill" style={{fontWeight: "bold"}}><Moment
                                    format="DD-MM-YYYY">{requested[0] ? requested[0].dateCreated : ''}</Moment></Col>
                            </Row>

                            <Row>
                                <Col span={24}>
                                    <Row gutter={12} className="fields-container-bill">
                                        <Col span={12} className="title-text-billing">Requested By</Col>
                                        <Col span={12}
                                             className="data-billing">{requested[0] ? requested[0].userName : ''}</Col>
                                    </Row>

                                    <Row gutter={12} className="fields-container-bill">
                                        <Col span={12} className="title-text-billing">Status</Col>
                                        <Col span={12} className="data-billing"><span
                                            className="details-paragraph pending">Pending</span></Col>
                                    </Row>

                                    <Row gutter={12} className="fields-container-bill">
                                        <Col span={12} className="title-text-billing">Vetted Amount</Col>
                                        <Col span={12}
                                             className="data-billing">{requested[0] && requested[0].totalVettedAmount ? formatCurrency(requested[0].totalVettedAmount) : '0'}</Col>
                                    </Row>

                                    <Row gutter={12} className="fields-container-bill">
                                        <Col span={12} className="title-text-billing">Paid Vetted Amount</Col>
                                        <Col span={12}
                                             className="data-billing">{wallet && wallet.totalVettedAmount ? formatCurrency(wallet.totalVettedAmount) : formatCurrency(0)}</Col>
                                    </Row>


                                    <Row gutter={12} className="fields-container-bill">
                                        <Col span={12} className="title-text-billing">Project Cost</Col>
                                        <Col span={12}
                                             className="data-billing">{requested[0] ? formatCurrency(requested[0].totalProjectAmount) : formatCurrency(0)}</Col>
                                    </Row>


                                    {wallet && <div style={{paddingTop: "10px"}}>
                                        <Row gutter={12} className="fields-container-bill">
                                            <Col span={12} className="title-text-billing">Requested Bill Amount</Col>
                                            <Col span={12} className="data-billing"><span
                                                className="main-price-cus">{formatCurrency(requested[0].totalVettedAmount - wallet.totalVettedAmount)}</span></Col>
                                        </Row>
                                    </div>}

                                    {!wallet && <div style={{paddingTop: "10px"}}>
                                        <Row gutter={12} className="fields-container-bill">
                                            <Col span={12} className="title-text-billing">Requested Bill Amount</Col>
                                            <Col span={12} className="data-billing"><span
                                                className="main-price-cus">{formatCurrency(requested[0].totalVettedAmount)}</span></Col>
                                        </Row>
                                    </div>}
                                    <div style={{paddingTop: "15px"}}>
                                        <Row span={12} className="title-text-billing">Comment(s)</Row>
                                        <Row span={12}
                                             className="data-billing">{requested[0] ? requested[0].comment : "..."}</Row>
                                    </div>
                                </Col>
                            </Row>

                            <Row style={{border: "0.2px solid #e2e2e2", marginTop: "15px", marginBottom: "20px"}}/>
                            <Row className="scrollable-wrap">
                                <span className="scrollable-heading">Linked Vetted Works</span>
                                <Row className="if-data-scrollable">
                                    {requested[0].billingBqsItem.map((item, i) => {
                                        return (
                                            <Row key={i} type="flex" justify="center" align="middle"
                                                 className="scrollable-box"
                                                 gutter={32}>
                                                <Col span={4}><span
                                                    className='scrollable-sub-heading title-text-billing'>{item.bqItemType || 'Empty Data'}</span></Col>
                                                <Col span={14}>
                                                    <span
                                                        className="scrollable-paraprag">{item.bqItemDescription || 'Empty Data'}</span>
                                                </Col>
                                                <Col span={6}>
                                                    <span
                                                        className="scrollable-price">{formatCurrency(item.bqItemAmount) || 'Empty Data'}</span>
                                                </Col>
                                            </Row>
                                        )
                                    })}
                                </Row>

                                {this.state.viewReject &&
                                <div>
                                    <Modal
                                        onCancel={this.viewRejectToggle}
                                        className="custom-table-popup"
                                        visible={this.state.viewReject}
                                        centered={true}
                                        title={"Bill Rejection"}
                                        footer={null}
                                    >
                                        <Row>
                                            <Col span={24}>
                                                <Col span={24}>
                                                    <Row className="fields-container">Rejection Comment(s)</Row>
                                                    <Row><Input className="mason-input" id="inputMessage" placeholder=""
                                                                value={this.state.comment}
                                                                onChange={this.commentHandler}/></Row>
                                                </Col></Col>
                                        </Row>
                                        <Row className="fields-container">
                                            <Col span={24} style={{textAlign: "center", marginTop: "10px"}}><Button
                                                type="primary" shape="round" size='large' className="submit-bill"
                                                onClick={() => {
                                                    this.rejectBill(2)
                                                }}>Reject</Button></Col>
                                        </Row>
                                    </Modal>
                                </div>}

                                {this.state.case && hasAuthority('CAN_CREATE_RAR') && this.state.permissions.inOffice &&
                                <Row type="flex" justify="center">
                                    <Col span={4}>
                                        <Button type="primary" shape="round" size='large' className="submit-bill"
                                                          onClick={this.createBill}>
                                            Create RAR
                                        </Button>
                                    </Col>
                                    <Col span={5}>
                                        <Button type="primary" shape="round" size='large' className="reject-button " onClick={() => {this.rejectBill(1)}}>
                                            Reject
                                        </Button>
                                    </Col>
                                </Row>
                                }


                            </Row>
                        </>
                        {this.state.createView && this.renderCreateBillModal()}
                    </div>
                    }
                    {(!this.props.billing.paidApproved.requestedBills || this.props.billing.paidApproved.requestedBills.length <= 0) &&
                    <div>No Data found</div>
                    }
                </div>

            </>)
        }

    }

}


const mapStateToProps = (state) => {
    return {
        billing: state.billing_reducer
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getApprovedreject: bindActionCreators(
            billActions.getApprovedPaidBills,
            dispatch
        )
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Requested);