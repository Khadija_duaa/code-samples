import React from 'react';
import axio from '../../../../../../utils/axios';
import { Button } from 'reactstrap';
import { message } from 'antd'
import { handleError } from '../../../../../../store/store-utils';
import { NOTIFICATION_TIME } from '../../../../../../utils/common-utils';


class Upload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            file: null
        };
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {
        this.setState({
            file: URL.createObjectURL(event.target.files[0])
        });

        const formData = new FormData();
        let projectId = this.props.projectId;
        formData.append('upload', event.target.files[0], event.target.files[0].name);
        axio.post(`/project-file-management/office/upload/${projectId}`, formData)
            .then(response => {
                console.log("response", response);
                this.uploadImageConvo(response.data.file)
            }).catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            })
    }

    uploadImageConvo = (requestdata) => {
        let data = {
            "file": {
                "ext": requestdata.ext,
                "fileId": requestdata.fileId,
                "name": requestdata.name,
                "size": requestdata.size
            },
            commentType: "file"
        };

        console.log("data to be posted ---", data);
        axio.put(`/site-visit-management/site-visits/pushOfficerComments/` + this.props.sobId, data)
            .then(res => {
                console.log('success', res);
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };

    render() {
        return (
            <div>
                <input type={'file'}
                    multiple
                    ref={uploadElement => {
                        this.uploadElement = uploadElement
                    }}
                    accept={'.jpg,.jpeg,.png'}
                    hidden
                    onChange={(e) => {
                        this.handleChange(e);
                        // this.onDrop(e.target.files)
                    }} />
                <Button type="submit" size="sm" color="primary" style={{
                    height: "40px",
                    margin: "0px"
                }} onClick={() => this.uploadElement.click()}>
                    Upload
                </Button>
            </div>
        );
    }
}

export default Upload;