import React from 'react';
import { Modal, Table} from 'antd';


class LinkedWorks extends React.Component {




    render() {
        const {visibleModal} = this.props;

        const columns = [{
            title: 'Description of Work',
            dataIndex: 'contractorId',
            width: 600,
        },{
            title: 'Qty',
            dataIndex: 'first_name',
            width: 200,
        },{
            title: 'Unit',
            dataIndex: 'last_name',
            width: 200,
        }, {
            title: 'Progress',
            dataIndex: 'email',
            width: 200,
        },

        ];
        return (
            <div>
                <Modal
                    title="Works"
                    className="chat-model"
                    centered={true}
                    visible={visibleModal}
                    onCancel={this.props.handleCancel}
                    footer={null}
                    maskClosable={false}

                >
                    <Table
                        columns={columns}
                        className="new-table-design"
                        pagination={false}
                        size="large"
                    />
                </Modal>
            </div>
        );
    }
}
export default LinkedWorks;