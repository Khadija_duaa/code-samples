import React from "react";
import {
  Chart,
  Geom,
  Axis,
  Tooltip,
  Legend
} from "bizcharts";
import {  Row } from 'antd'
import DataSet from "@antv/data-set";
import BQDetails from '../BQDetails'

import axio from '../../../../../../../utils/axios';

class WorkOverviewChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projectId: this.props.projectId,
      key: 0,
      graphData: 0,
      err: "",
      idz: 0,
      visible: false,
      selectedId: 0,
    }
  }

  componentDidMount() {
      this.setState({
          graphData: [0,0,0],
          key: [],
          idz: {}
      });
    this.getDataGraph()
  }

  getDataGraph = () => {
    axio.get(`/bq-management/measurement-book/${this.state.projectId}`)
      .then(res => {
        if (res.data && res.data.measurementBook.length > 0) {

          let work = {
            name: "Total"
          };
          let done = { name: "Done" };
          let vetted = {name: "Vetted"};
          let keys = [];
          let myidz = {};
          res.data.measurementBook.forEach((item) => {
            let catagory = item.categoryName.split("(")[0];
            work = { ...work, [`${catagory}.`]: item.categoryWorkPercentage };
            done = { ...done, [`${catagory}.`]: item.categoryDonePercentage };
            vetted = { ...vetted, [`${catagory}.`]: item.categoryVettedPercentage };
            keys.push(`${catagory}.`);
            myidz = { ...myidz, [`${catagory}.`]: item.categoryId }
          });
          this.setState({
            graphData: [work, done, vetted],
            key: keys,
            idz: myidz
          })
        }
        else {
          this.setState({
            err: "The project setup has not been completed by contractor yet."
          })

        }
      })
      .catch(error => {
        console.log('error --->>>', error);
      });
  };

  handleVisible = () => {
    this.setState({
      visible: false
    })
  };

  onClick = (e) => {
    if (e.data && e.data._origin) {
      let myKey = e.data._origin.key;
      let myId = this.state.idz[myKey];
      this.setState({
        selectedId: myId,
        visible: true
      });
    }
  };
  render() {
    // let data = [
    //   {
    //     name: "London",
    //     "Jan.": 18.9,
    //     "Feb.": 28.8,
    //     "Mar.": 39.3,
    //     "Apr.": 81.4,
    //     'May.': 47,
    //     "Jun.": 20.3,
    //     "Jul.": 24,
    //     "Aug.": 35.6
    //   },
    //   {
    //     name: "Berlin",
    //     "Jan.": 12.4,
    //     "Feb.": 23.2,
    //     "Mar.": 34.5,
    //     "Apr.": 99.7,
    //     'May.': 52.6,
    //     "Jun.": 35.5,
    //     "Jul.": 37.4,
    //     "Aug.": 42.4
    //   }
    // ];
    let defaultKeys = ["Jan.", "Feb.", "Mar.", "Apr.", "May.", "Jun.", "Jul.", "Aug."];
    let ds = 0;
    let dv = 0;
    if (this.state.graphData !== 0) {
      // data[0] = this.state.graphData[0],
      // data[1] = this.state.graphData[1],
      defaultKeys = this.state.key;

      var colorSet = {
        'Total': "#4FAAEB",
        'Done': "#818185",
        'Vetted':"#51C3B8"
      };
      ds = new DataSet();
      dv = ds.createView().source(this.state.graphData);
      dv.transform({
        type: "fold",
        fields: defaultKeys,
        key: "key",
        value: "chartValue"
      });
    }



    // const content = (
    //   <div>
    //     <p><span className="custom-percentage-span c-blue">Paid:</span> 20 Million</p>
    //     <p><span className="custom-percentage-span c-blue">Remaining:</span> 20 Million</p>
    //   </div>
    // );
    return (
      <div>{
        this.state.err !== "" &&
        <Row className="custom-paragraph-data-result">{this.state.err}</Row>
      }
        {this.state.graphData && <Chart  height={400} data={dv} forceFit padding={[60, 30, 40, 30]} onClick={ev => {
          this.onClick(ev)
        }}>
          <Axis 
              name="key" 
              label={{
                autoRotate: false, // 是否需要自动旋转，默认为 true
                // offset: 40, // 设置标题 title 距离坐标轴线的距离
                width:50,
                textStyle: {
                  fontSize: '12',
                  textAlign: 'center',
                  fill: '#999',
                  width:'50px',
                  overflow:'hidden',
                  fontWeight: 'bold',
                },
                position: 'center', // 标题的位置，**新增**
              }} 
          />
          <Axis name="chartValue" />
          <Legend
            position="top-right"
            symbol="circle"
          />
          <Tooltip
            crosshairs={{
              type: "y"
            }}
          />
          <Geom
            type="interval"
            position="key*chartValue"
            color={["name", value => colorSet[value]]}
            adjust={[
              {
                type: "dodge",
                marginRatio: 1 / 32
              }
            ]}
          />
        </Chart>}
        {this.state.visible && <BQDetails visible={this.state.visible} workId={this.state.selectedId} projectId={this.state.projectId} handleVisible={this.handleVisible} />}
      </div>
    );
  }
}

export default WorkOverviewChart;