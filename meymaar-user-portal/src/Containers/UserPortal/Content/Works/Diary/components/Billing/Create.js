
import React from 'react';
import { Row, Col, Modal,  Input, Button, message, Checkbox } from 'antd';
import { formatCurrency, NOTIFICATION_TIME } from '../../../../../../../utils/common-utils';
import axio from '../../../../../../../utils/axios';
import { handleError } from '../../../../../../../store/store-utils';

class Create extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projectId: this.props.projectId,
      visible: this.props.visible,
      billData: this.props.billData,
      wallet: this.props.wallet,
      aboveNty: false,
      submitButton: true,
      isFinal: false,
      fileData: {},
      holdingAmount: 10,
      amount: this.props.billData.totalVettedAmount - this.props.wallet.totalVettedAmount,
      showThis :this.props.billData.totalVettedAmount - this.props.wallet.totalVettedAmount,
      formData: {
        storeAmount: 0,
        advanceAmount: 0,
      },
      handReceipts: [
        {
          amount: 0,
          description: ""
        },
        {
          amount: 0,
          description: ""
        }
      ],

    }
  }

  componentDidMount() {
    let totalVtted = this.state.billData.totalVettedAmount;
    let totalPaid = this.state.wallet.totalProjectAmount;
    let percent = (totalVtted / totalPaid) * 100;
    console.log("data -->", percent);
    if (percent > 90) {
      this.setState({
        aboveNty: true,
        holdingAmount: 5,
      })
    }
  }
  handleSubmit = () => {
    console.log("submit");
    let submitData = {
      handReceipts: this.state.handReceipts,
      handReceiptFile: this.state.fileData,
      holdingPercentage: this.state.holdingAmount,
      storeAmount: this.state.formData.storeAmount,
      advanceAmount: this.state.formData.advanceAmount,
      isFinal: this.state.isFinal
    };

    axio.post(`/billing-management/createBill/${this.state.projectId}/${this.state.billData.id}`, submitData)
      .then((res) => {
        console.log("Billing Response", res.data);
        this.props.handleCreateBill()
      })
      .catch(error => {
        let errorMessage = handleError(error);
        message.error(errorMessage, NOTIFICATION_TIME);
        //this.error(error.message)
      });
    this.props.handleClose();
    this.setState({ visible: false })
  };

  error = (err) => {
    message.error(`Could not save, ${err}`);
  };

  handleOk = () => {
    this.setState({
      visible: false,
    });
    this.props.handleClose()
  };

  handleCancel = (e) => {
    this.setState({
      visible: false,
    });
    this.props.handleClose()
  };

  // handleRecieptAmount = (data) => {
  //   let rcpt = [];
  //   let totalAmt = this.state.amount
  //   console.log("data recieved ---", data)
  //   let amounts = data.recieptData;
  //   let des = data.recieptDes;
  //   for (let i = 0; i < amounts.length; i++) {
  //     if (amounts[i] && des[i]) {

  //       rcpt.push({
  //         description: des[i],
  //         amount: parseInt(amounts[i])
  //       })
  //       totalAmt = totalAmt + parseInt(amounts[i])
  //     }
  //   }
  //   let storeAmt = this.state.formData.storeAmount;
  //   totalAmt = totalAmt - storeAmt
  //   this.setState({
  //     handReceipts: rcpt,
  //     amount: totalAmt
  //   })
  // }

  uploadFile = (event) => {
    const formData = new FormData();
    let projectId = this.state.projectId;
    formData.append('upload', event.target.files[0], event.target.files[0].name);
    axio.post(`/project-file-management/office/upload/${projectId}`, formData)
      .then(response => {
        console.log("response", response);
        if (response.data.file) {
          this.setState({
            fileData: response.data.file,

          })
        }
      }).catch(error => {
        let errorMessage = handleError(error);
        message.error(errorMessage, NOTIFICATION_TIME);
      })
  };

  handleChange = (e, key) => {
    let prevState = this.state.formData;
    prevState[key] = e.target.value;
    this.setState({
      formData: prevState
    });

    console.log("-----", e.target.value);
    let data = 0;
    let isValidAmount = e.target.value !== "" && e.target.value>=0;
    switch(key){
      case 'storeAmount':
      if(isValidAmount){
        data = (this.state.amount - parseFloat(e.target.value)) + parseFloat(this.state.formData.advanceAmount);
        this.setState({
          showThis: data
        })
      }
      break;
      case 'advanceAmount':
       if(isValidAmount){
        data = (this.state.amount + parseFloat(e.target.value)) - parseFloat(this.state.formData.storeAmount);
        this.setState({
          showThis: data
        })
       }
     
      break;

        default:
          break;
    }
  };

  onCheck = (e) => {
    this.setState({
      isFinal: e.target.checked
    })
  };



  render() {

    // const customPanelStyle = {
    //   background: '#f7f7f7',
    //   borderRadius: 4,
    //   marginBottom: 24,
    //   border: 0,
    //   overflow: 'hidden',
    // };

    return (<>
      <div style={{ padding: "20px" }}>
        <Modal
          title="Create Bill"
          className="chat-model"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={null}
        >

          <Row>
            <Col span={24}>
              <Row gutter={12} className="fields-container-bill">
                <Col span={12} className="title-text-billing">Vetted Work Amount</Col>
                <Col span={12} className="data-billing">{formatCurrency(this.state.billData.totalVettedAmount)}</Col>
              </Row>

              <Row gutter={12} className="fields-container-bill">
                <Col span={12} className="title-text-billing">Paid Amount</Col>
                <Col span={12} className="data-billing">{formatCurrency(this.state.wallet.paidAmount)}</Col>
              </Row>

              <Row gutter={12} className="fields-container-bill">
                <Col span={12} className="title-text-billing">Hold Amount %</Col>
                <Col span={12} className="data-billing">{this.state.holdingAmount}%</Col>
              </Row>

              <Row gutter={12} className="fields-container-bill">
                <Col span={12} className="title-text-billing">Hold Amount</Col>
                <Col span={12} className="data-billing">{this.state.wallet.totalHoldingAmount ? formatCurrency(this.state.wallet.totalHoldingAmount) : formatCurrency(0)}</Col>
              </Row>

              <Row gutter={12} className="fields-container-bill">
                <Col span={12} className="title-text-billing">Store Amount</Col>
                <Col span={12} className="data-billing">{this.state.wallet.totalStoreAmount ? formatCurrency(this.state.wallet.totalStoreAmount) : formatCurrency(0)}</Col>
              </Row>

              <Row gutter={12} className="fields-container-bill">
                <Col span={12} className="title-text-billing">Store</Col>
                <Col span={12} className="data-billing"><Input type="number" min={0} required value={this.state.formData.storeAmount} name="storeAmount" onChange={(e) => { this.handleChange(e, "storeAmount") }} /></Col>
              </Row>

              <Row gutter={12} className="fields-container-bill">
                <Col span={12} className="title-text-billing">Advance Amount</Col>
                <Col span={12} className="data-billing"><Input type="number" min={0} required value={this.state.formData.advanceAmount} name="advanceAmount" onChange={(e) => { this.handleChange(e, "advanceAmount") }} /></Col>
              </Row>

              <Row gutter={12} className="fields-container-bill">
                <Col span={12} className="title-text-billing">Final Bill</Col>
                <Col span={12} className="data-billing">{formatCurrency(this.state.showThis)}</Col>
              </Row>
            </Col>


            {this.state.aboveNty && <Row gutter={8}>
              <Col span={12}>
                <Row><Checkbox onChange={this.onCheck}>IS FINAL</Checkbox></Row>
              </Col>
            </Row>}

          </Row>
          <Row>
            {this.state.submitButton && <Col className="custom-colum" span={24}><Button className="modal-button " onClick={this.handleSubmit}>SUBMIT</Button></Col>}
          </Row>
        </Modal>
      </div ></>)

  }
}



export default Create;