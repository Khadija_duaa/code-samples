import moment from "moment";

const newColumns = [
    {
        title: "Item Id",
        dataIndex: "viewId",
        width: 90
    },
    {
        title: "Description",
        dataIndex: "totalVettedAmount",
        width: 260
    },
    {
        title: "Amount",
        dataIndex: "paidBy",
        width: 90
    },
    {
        title: "Date",
        dataIndex: "bqItemDate",
        width: 90,
        render: text => moment(text).format("DD-MM-YYYY")
    }
];

export default newColumns;