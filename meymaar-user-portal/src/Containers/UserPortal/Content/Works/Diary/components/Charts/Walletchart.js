import React from "react";
import {
    Chart,
    Geom,
    Axis,
    Tooltip
} from "bizcharts";
import { Card, message } from 'antd';
import axio from '../../../../../../../utils/axios';
import moment from 'moment';
import { NOTIFICATION_TIME } from "../../../../../../../utils/common-utils";
import { handleError } from "../../../../../../../store/store-utils";

class WalletChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            projectId: this.props.projectId,
            mainData: 0
        }
    }

    componentDidMount() {
        this.getHistoryWallet()
    }

    getHistoryWallet = () => {
        axio.get(`/billing-management/projectWalletHistory/${this.state.projectId}`)
            .then(res => {
                let dataResult = res.data.walletHistory;
                let tableData = dataResult.map((data) => {
                   
                    if (data.availableAmount === null) {
                        return {
                            year: moment(data.dateCreated).format("M/D/Y"),
                            value: 0,
                        }
                    } else {
                        return {
                            year: moment(data.dateCreated).format("M/D/Y"),
                            value: data.availableAmount,
                        }
                    }

                });
                this.setState({
                    mainData: tableData,
                    doneLoading: true,
                })

            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage,NOTIFICATION_TIME);
            });
    };


    render() {

        let data = [
            {
                year: "1991",
                value: 3
            },
            {
                year: "1992",
                value: 4
            },
            {
                year: "1993",
                value: 3.5
            },
            {
                year: "1994",
                value: 5
            },
            {
                year: "1995",
                value: 4.9
            },
            {
                year: "1996",
                value: 6
            },
            {
                year: "1997",
                value: 7
            },
            {
                year: "1998",
                value: 9
            },
            {
                year: "1999",
                value: 13
            }
        ];

        if (this.state.mainData !== 0) {
            data = this.state.mainData
        }
        const cols = {
            value: {
                min: 0
            },
            year: {
                range: [0, 1]
            }
        };
        return (
            <Card className="custom-title" title="Allocation of Funds">
                <div>
                    <Chart height={400} data={data} scale={cols} forceFit>
                        <Axis name="year" />
                        <Axis name="value" />
                        <Tooltip
                            crosshairs={{
                                type: "y"
                            }}
                        />
                        <Geom type="line" position="year*value" size={2} />
                        <Geom
                            type="point"
                            position="year*value"
                            size={4}
                            shape={"circle"}
                            style={{
                                stroke: "#fff",
                                lineWidth: 1
                            }}
                        />
                    </Chart>
                </div>
            </Card>
        );
    }
}

export default WalletChart;