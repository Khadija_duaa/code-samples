import React from "react";
import axio from "../../../../../../utils/axios";
import {Row, Col, Button, Modal, message, Table} from "antd";
import BQchart from "./Charts/BQcharts";
import {handleError} from "../../../../../../store/store-utils";
import {NOTIFICATION_TIME} from "../../../../../../utils/common-utils";

class CreateSob extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: this.props.visible,
            data: [],
            bqChartData: 0,
            projectId: this.props.projectId,
            tableData: 0,
            workId: this.props.workId
        };
    }

    componentDidMount() {
        this.getdata();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.visible !== this.props.visible) {
            this.setState({
                visible: this.props.visible
            });
        }
    }

    handleOk = e => {
        this.setState({
            visible: false
        });
        this.props.handleVisible();
    };

    handleCancel = e => {
        this.setState({
            visible: false
        });
        this.props.handleVisible();
    };

    getdata = () => {
        axio
            .get(
                `/bq-management/measurement-book-category/${this.state.projectId}/${
                    this.state.workId
                    }`
            )
            .then(res => {
                if (res.data) {
                    let newdata = {
                        workDone: res.data.categoryDonePercentage,
                        workCatagory: res.data.categoryWorkPercentage,
                        workVetted: res.data.categoryVettedPercentage
                    };
                    let tabled = res.data.bqItems.map(item => {
                        return {
                            description: item.description,
                            quantity: item.quantity,
                            unit: item.unit,
                            workDone: item.bqsItemProgress.doneWork,
                            vettedWork: item.bqsItemProgress.vettedWork
                        };
                    });

                    this.setState({
                        bqChartData: newdata,
                        tableData: tabled
                    });
                }
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };

    success = () => {
        message.success("Added successfully.");
    };

    error = () => {
        message.error("Could not save.");
    };

    render() {
        const column = [
            {
                title: "Description of Work",
                dataIndex: "description",
                width: 220
            },
            {
                title: "QTY",
                dataIndex: "quantity",
                width: 60
            },
            {
                title: "Unit",
                dataIndex: "unit",
                width: 60
            },
            {
                title: "Work Done",
                dataIndex: "workDone",
                width: 60
            },
            {
                title: "Vetted Work",
                dataIndex: "vettedWork",
                width: 60
            }
        ];

        return (
            <div>
                <Modal
                    title="BQS ITEMS"
                    className="chat-model"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    centered={true}
                    onCancel={this.handleCancel}
                    footer={null}
                >
                    <Row>
                        <Col span={24}>
                            <Row>
                                {this.state.bqChartData !== 0 && (
                                    <BQchart
                                        data={this.state.bqChartData}
                                        projectId={this.state.projectId}
                                    />
                                )}
                            </Row>
                            <Row>
                                <Table
                                    columns={column}
                                    className="custom-table-read"
                                    dataSource={this.state.tableData}
                                    size="small"
                                    bordered
                                    scroll={{y: 360}}
                                />
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="custom-colum" span={24}>
                            <Button className="modal-button " onClick={this.handleCancel}>
                                OK
                            </Button>
                        </Col>
                    </Row>
                </Modal>
            </div>
        );
    }
}

export default CreateSob;
