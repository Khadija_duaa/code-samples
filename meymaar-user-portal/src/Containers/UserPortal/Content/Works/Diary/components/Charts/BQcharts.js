import React from "react";
import {
    Chart,
    Geom,
    Axis,
    Tooltip,
    Coord,
} from "bizcharts";
import DataSet from "@antv/data-set";

class BQchart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectId : this.props.projectId
        };
    }
    render() {
        const data = [
            {
                type: "Vetted Work",
                percentage: this.props.data.workVetted
            },
            {
                type: "Work Done",
                percentage: this.props.data.workDone
            },
            {
                type: "Total Work",
                percentage: this.props.data.workCatagory
            },

        ];
        const ds = new DataSet();
        const dv = ds.createView().source(data);
        dv.source(data).transform({
            type: "sort",
            callback(a, b) {
                return a.percentage - b.percentage > 0;
            }
        });

        
        const cols = {
            sales: {
              tickInterval: 20
            }
          };

          let colorSet = {
            'Total Work': "rgb(68, 68, 213)",
            'Work Done': "#4FAAEB",
            'Vetted Work':"#818185"
          };
    
        return (
            <div>
                <Chart height={300}  data={dv} forceFit scale={cols}>
                    <Coord transpose />
                    <Axis style={{fontWeight:"bold"}}
                        name="type"
                        label={{
                            offset: 12
                        }}
                    />
                    <Axis name="percentage" />
                    <Tooltip />
                    <Geom type="interval" position="type*percentage" color={["type", value => colorSet[value]]}/>
                </Chart>
            </div>
        );
    }
}

export default BQchart;