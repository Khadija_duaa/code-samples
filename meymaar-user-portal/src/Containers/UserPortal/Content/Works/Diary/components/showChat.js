import React from 'react';
import axio from '../../../../../../utils/axios';
import {Row, Col, Form, Input, Button, Modal, message} from 'antd';
import moment from 'moment';
import Upload from './Upload';
import Preview from './Preview';
import executil from './ExeUtils'
import {handleError} from '../../../../../../store/store-utils';
import {NOTIFICATION_TIME} from '../../../../../../utils/common-utils';
import MessageModal from '../../../../../../Components/ModalFactory/MessageModal/ConfirmationModal';
import {store} from "../../../../../../index";
import {setProjectProcessing} from "../../../../../../store/project/project-actions";

class ShowChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: this.props.data.visible,
            showConfirmationModal: false,
            permissions: executil.isInOffice(),
            inputMessage: " ",
            actionCalled: "",
            sobObject: this.props.sobObj,
            projectId: this.props.data.projectId,
            comments: this.props.sobObj.comments,
            sobId: this.props.sobObj._id,
            myData: {},
            size: 12,
        }
    }


    componentDidMount() {

        if (this.props.isPending) {
            this.interval = setInterval(() => {
                this.refreshdata()
            }, 1000);
        }

        if (this.props.sobObj.workDone) {
            let data = {};

            data.comment = this.props.sobObj.workDone.work_description;
            data.date = moment(this.props.sobObj.workDone.date).format('MM-DD-YYYY');

            this.setState({
                myData: data
            })

        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
        this.setState({
            visible: false
        })
    }


    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
        this.props.closeChat();
        clearInterval(this.interval);
    };
    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
        this.props.closeChat();
        clearInterval(this.interval);
    };

    handleToggle = () => {
        let flag = this.state.showConfirmationModal;
        this.setState({
            showConfirmationModal: !flag,
        });
    };

    refreshdata = () => {
        axio.get(`/site-visit-management/site-visits/getCommentsById/${this.state.sobId}`)
            .then(res => {
                this.setState({
                    comments: res.data.visit.comments
                })
            }).catch((error) => {
            let errorMessage = handleError(error);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    };

    changeHandler = (e) => {
        this.setState({
            inputMessage: e.target.value
        })
    };

    sendMessage = () => {
        let sob = this.props.data.sobObject;
        if (sob._id) {
            const data = {
                "comment": `${this.state.inputMessage}`,
                "commentType": "text"
            };
            this.setState({
                inputMessage: ""
            });
            axio.put(`/site-visit-management/site-visits/pushOfficerComments/` + sob._id, data)
                .then(res => {
                    console.log('success', res);
                })
                .catch(error => {
                    let errorMessage = handleError(error);
                    message.error(errorMessage, NOTIFICATION_TIME);
                });
        }
    };

    approveReject = (action) => {

        switch (action) {
            case 'approve':
                this.sendAcknowledge('Approved');
                this.handleCancel();
                break;
            case 'reject':
                this.sendAcknowledge('Rejected');
                this.handleCancel();
                break;
            case 'close':
                this.sendAcknowledge('close');
                this.handleCancel();
                break;
            default:
                break;
        }
    };

    formSeparator = {
        marginBottom: '50px',
        border: 'linear-gradient(#f5f6fa, #d8d8d8) ',
        width: 'auto'
    };

    sendAcknowledge = (caseCall) => {
        let ref = this;
        let data = {
            "status": "Closed",
            "requestStatus": `${caseCall}`,
            "comment": `Request is being ${caseCall}`,
            "commentType": "text"
        };
        if (caseCall === "close") {
            data = {
                "status": "Closed",
                "comment": `Request is being ${caseCall}`,
                "commentType": "text"
            }
        }
        let {dispatch} = store;
        dispatch(setProjectProcessing(true));
        axio.put(`/site-visit-management/site-visits/pushOfficerComments/` + this.state.sobId, data)
            .then(res => {
                dispatch(setProjectProcessing(false));
                ref.props.rejectResolve(caseCall)
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));
            });
        this.setState({visible: false})
    };

    success = () => {
        message.success('Added successfully.');
    };

    error = () => {
        message.error('Could not save.');
    };

    getGravLetters = (name) => {
        let b = name.split(" ")[0][0].toUpperCase();
        let c = name.split(" ")[1][0].toUpperCase();
        return (b + "" + c);
    };

    // Message Confirmation Modals
    renderMessageModal = () => {
        let message = `Are you sure, you want to ${this.state.actionCalled}?`;

        return (
            <MessageModal isOpen={this.state.showConfirmationModal}
                          okCB={() => {
                              this.approveReject(this.state.actionCalled)
                          }} cancelCB={this.handleToggle}
                          message={message}/>
        );
    };

    messageModal = (action) => {
        this.setState({
            actionCalled: action,
            showConfirmationModal: true
        })
    };

    getCommentDate = (commentDate) => {

        let currentDate = moment(new Date(), "YYYY-MM-DD");


        let messageDate = moment(commentDate, "YYYY-MM-DD");
        let days = currentDate.diff(messageDate, 'days');

        return (
            <Row>
                {days === 0 ? `Today at ${moment(commentDate).format("h:mm A")}` : new moment(commentDate).format('lll')}
            </Row>
        )


    };


    render() {
        let {permissions} = this.state;
        return (
            <div>

                <Modal
                    title={[
                        <div key={0}>
                            <Row type="flex" align="middle" className="column-wrap">
                                <Col span={24}>
                                    <h4 style={{color: "white"}}>{this.state.sobObject.title}
                                        <span
                                            className="remark">{this.state.comments && this.state.comments.length} Remarks</span>
                                    </h4>
                                </Col>
                            </Row>
                            {this.props.tab === "1" &&
                            <Row>
                                <Col span={'auto'}><Button type="secondary"
                                                           onClick={() => {
                                                               this.messageModal('close')
                                                           }}>
                                    Close
                                </Button></Col></Row>
                            }
                            {this.props.tab === '2' && permissions.authority && permissions.inOffice &&
                            <Row>
                                <Button style={{background: '#51C3B8', color: '#fff', minWidth: "110px"}}
                                        onClick={() => {
                                            this.messageModal('approve')
                                        }}>
                                    APPROVE
                                </Button>

                                <Button type="danger" style={{minWidth: "110px", marginLeft: "10px"}}
                                        onClick={() => {
                                            this.messageModal('reject')
                                        }}>
                                    REJECT
                                </Button>
                            </Row>}
                        </div>
                    ]}
                    className="chat-model"
                    visible={this.state.visible}
                    centered={true}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={(this.props.tab === '2' || this.props.tab === '1') && [
                        <Row key={0} type="flex" align="middle" justify="space-between" className="chat-footer">
                            {permissions.inOffice && permissions.authority &&
                            <Col className="chat-icons" span={3}><Upload projectId={this.state.projectId}
                                                                         sobId={this.state.sobId}/></Col>}
                            <Col className="chat-input-wrap" span={20}>
                                {
                                    permissions.inOffice && permissions.authority &&
                                    <Form>
                                        <Input className="chat-input" id="inputMessage"
                                               style={{background: "#efefef", marginRight: "3px"}}
                                               onChange={this.changeHandler}
                                               value={this.state.inputMessage}/>

                                        <Button
                                            disabled={!this.state.inputMessage || !this.state.inputMessage.trim().length}
                                            onClick={this.sendMessage}>
                                            Send
                                        </Button>
                                    </Form>
                                }
                            </Col>
                        </Row>
                    ]}
                >
                    {/* sender msg */}
                    {this.renderMessageModal()}

                    <div>
                        <Row gutter={20} style={{background: "white", margin: "0px"}}>
                            {<Col md={24} lg={8} style={{
                                border: "1px solid #ebebeb",
                                height: "400px",
                                overflowY: "auto",
                                background: "#f2f2f2",
                            }}>
                                <Row gutter={12} style={{marginTop: "10px"}}>
                                    <Col span={12} style={{fontWeight: "600"}}>Requested Date:</Col>
                                    <Col
                                        span={12}>{moment(this.props.sobObj && this.props.sobObj.workDone && this.props.sobObj.workDone.date).format('MM-DD-YYYY')}</Col>
                                </Row>

                                {this.props.sobObj && this.props.sobObj.workDone && this.props.sobObj.workDone.bq && this.props.sobObj.workDone.bq.map((data, index) => {
                                    return (<div key={0}>
                                        <Row style={{fontWeight: "600", marginTop: "10px"}}>Description:</Row>
                                        <Row>{data.description}</Row>

                                        <Row gutter={12} style={{marginTop: "10px"}}>
                                            <Col span={12} style={{fontWeight: "600"}}>Progress:</Col>
                                            <Col span={12}>{parseFloat(data.percentage).toFixed(2)}%</Col>
                                        </Row>
                                        <hr style={this.formSeparator}/>
                                    </div>)
                                })
                                }

                            </Col>}
                            <Col md={24} lg={15} offset={1}
                                 style={{overflowY: "auto", height: "400px", background: "#f2f2f2"}}>
                                <Row className="">
                                    {this.state.comments.map((comment, i) => {
                                        return (

                                            <Row key={i}>

                                                <div hidden={comment.office === 'true'}>

                                                    <Row className="msg-box">
                                                        <Row>
                                                            <p style={{
                                                                fontSize: '13px',
                                                                marginBottom: '0px',
                                                                marginLeft: '57px',
                                                                color: 'blue'
                                                            }}>{comment.createdByName}</p>
                                                        </Row>
                                                        <Row type="flex" align="middle">
                                                            <Col
                                                                className="chat-avatar">{this.getGravLetters(comment.createdByName)}</Col>
                                                            {comment.comment !== "" && comment.commentType !== "file" &&
                                                            <Col className="chat-msg"
                                                                 style={{marginTop: '0px'}}>{comment.comment}</Col>}
                                                            {comment.commentType === "file" &&
                                                            comment.file.map((file, i) => {
                                                                return <Col key={i} className="chat-msg-mine"><Preview
                                                                    fileId={file.fileId}/></Col>
                                                            })
                                                            }
                                                        </Row>
                                                        <Row className="chat-time">
                                                            {this.getCommentDate(comment.createdAt)}
                                                        </Row>
                                                    </Row>
                                                </div>

                                                <div hidden={comment.office === 'false'}>
                                                    <Row className="msg-box">
                                                        <Row>
                                                            <p style={{
                                                                fontSize: '13px',
                                                                marginBottom: '-10px',
                                                                marginRight: '23px',
                                                                float: 'right',
                                                                color: 'blue'
                                                            }}>{comment.createdByName}</p>
                                                        </Row>
                                                        <Row align="middle">
                                                            {comment.comment !== "" && comment.commentType !== "file" &&
                                                            <Col className="chat-msg-mine"> {comment.comment}</Col>}
                                                            {comment.commentType === "file" &&
                                                            comment.file.map((file, i) => {
                                                                return <Col key={i} className="chat-msg-mine"><Preview
                                                                    fileId={file.fileId}/></Col>
                                                            })
                                                            }
                                                        </Row>
                                                        <Row className="chat-time-mine">
                                                            {this.getCommentDate(comment.createdAt)}
                                                        </Row>
                                                    </Row>
                                                </div>
                                            </Row>

                                        )
                                    })}
                                    <div style={{float: "left", clear: "both"}}
                                         ref={(el) => {
                                             this.messagesEnd = el;
                                         }}>
                                    </div>
                                </Row>
                            </Col>
                        </Row>
                    </div>
                </Modal>
            </div>
        );
    }
}

export default ShowChat;