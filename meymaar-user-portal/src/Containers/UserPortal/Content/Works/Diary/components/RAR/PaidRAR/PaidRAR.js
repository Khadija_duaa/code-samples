import React from 'react';
import {Table, Modal, Button} from 'antd';
import moment from 'moment';
import {formatCurrency} from "../../../../../../../../utils/common-utils";
import RARForm from '../../../../../../../../Components/CustomForms/RARForm/RARForm';
import BOQModal from "../../../../../../../../Components/ModalFactory/BOQModal/BOQModal";

class PaidRAR extends React.Component {

    state = {
        showModal: false,
        requestDetails: null,
        showBQModal: false,
        bqDetails: null,
    };

    /****************************** Modal ****************************************/

    toggleBQModal = (record) => {
        record = record && record.billRequest.billingBqsItem;
        this.setState({bqDetails: record, showBQModal: !this.state.showBQModal})
    };

    handleCancelOK = () => {
        this.setState({showBQModal: false})
    };

    renderBQDetailsModal = () => {

        return (
            <Modal
                className={"bq-details-modal"}
                title=" Paid RAR BOQ Details"
                visible={this.state.showBQModal}
                onOk={this.handleCancelOK}
                onCancel={this.handleCancelOK}
                destroyOnClose={true}
                footer={null}
                maskClosable={true}
                centered={true}
            >
                <BOQModal dataSource={this.state.bqDetails}/>
            </Modal>
        )
    };

    /*********************** Paid RAR Modal *****************/
    toggleModal = (data) => {
        this.setState({showModal: !this.state.showModal, requestDetails: data});
    };
    renderPaidRARModal = () => {
        if (!this.state.requestDetails) return null;
        let formData = {
            isPreview: true,
            requestDetails: this.state.requestDetails,
            isPayment: false
        };

        return (
            <Modal
                centered={true}
                title="Paid RAR"
                className="create-bill-model"
                visible={this.state.showModal}
                onOk={() => this.setState({showModal: false})}
                onCancel={() => this.setState({showModal: false})}
            >
                <RARForm {...formData}/>
            </Modal>
        )
    };
    /*********************** End *****************/


    /****************************** END ******************************************/


    /******************************* EVENTS **************************************/
    renderText = (text, record) => {
        return (
            <p onClick={() => this.toggleModal(record)}>
                {text}
            </p>
        )
    };

    renderTableData = () => {
        this.props.rarData.forEach((data, index) => {
            data.index = index;
            data.rarNumber = data.number ? data.number : "-";
            data.paidDate = moment(data.paymentDate).format("DD-MM-YYYY");
            data.paidBy = data.payerName;
            data.totalAmount = formatCurrency(parseFloat(data.netAmount).toFixed(2));
            data.amountPaid = formatCurrency(parseFloat(data.paidAmount).toFixed(2));
            data.outstandingAmount = formatCurrency(parseFloat(data.outStandingAmount).toFixed(2));
        });

        let columns = [
            {
                key: "rarNumber",
                title: "Receipt Number",
                dataIndex: "rarNumber",
                editable: false,
                render: (e, data) => this.renderText(data.rarNumber, data)
            },
            {
                key: "paidDate",
                title: "Payment Date",
                dataIndex: "paidDate",
                editable: false,
                render: (e, data) => this.renderText(data.paidDate, data)
            },
            {
                key: "paidBy",
                title: "Paid By",
                dataIndex: "paidBy",
                editable: false,
                render: (e, data) => this.renderText(data.paidBy, data)
            },
            {
                key: "totalAmount",
                title: "Net Amount",
                dataIndex: "totalAmount",
                editable: false,
                render: (e, data) => this.renderText(data.totalAmount, data)

            },
            {
                key: "amountPaid",
                title: "Paid Amount",
                dataIndex: "amountPaid",
                editable: false,
                render: (e, data) => this.renderText(data.amountPaid, data)
            },
            {
                key: "outstandingAmount",
                title: "Outstanding Amount",
                dataIndex: "outstandingAmount",
                editable: false,
                render: (e, data) => this.renderText(data.outstandingAmount, data)
            },
            {
                key: "bqDetails",
                title: "BOQ's",
                dataIndex: "bqDetails",
                editable: false,
                render: (e, data) => {
                    return (
                        <Button className={"view-details"} onClick={() => this.toggleBQModal(data)}>
                            View BOQ's
                        </Button>
                    )
                }
            },
        ];


        return (
            <Table
                className="custom-new-table-click rar-table"
                columns={columns}
                dataSource={this.props.rarData}
                rowKey={(record, index) => index}
            />
        )
    };

    render() {
        let {rarData} = this.props;

        if (!rarData || !rarData.length) return <h6>No Paid RAR Exists!</h6>;


        return (
            <div>
                {this.renderPaidRARModal()}
                {this.renderBQDetailsModal()}
                {this.renderTableData()}
            </div>
        )
    }
}

export default PaidRAR;