import React from 'react';
import {Row, Col, Tabs, Table, Modal, Button, Input, message} from 'antd';
import axio from '../../../../../../utils/axios'
import Moment from 'react-moment';
import './workCopy.css';
import moment from 'moment';
import {connect} from 'react-redux';
import excUtils from './ExeUtils'
import {handleError} from '../../../../../../store/store-utils';
import {NOTIFICATION_TIME} from '../../../../../../utils/common-utils';

const TabPane = Tabs.TabPane;
const {TextArea} = Input;
Moment.globalFormat = 'D MMM YYYY';

class Work extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            workLog: [],
            workDescription: [],
            bq: [],
            ad: [],
            mason: [],
            userId: 'ablkjslkdf',
            description: [],
            progress: '',
            materialData: "",
            projectId: excUtils.getProjectId(),
        };
        excUtils.isInOffice()
    }

    componentDidMount = () => {
        axio.get(`/work-diary/work-diary/getAdminWorkDiary/${this.state.projectId}`)
            .then(res => {
                const newData = [];
                res.data.work_done.forEach(work => {
                    newData.push(
                        {
                            ...work,
                            workDescription: work.work_description
                        });
                });


                this.setState({
                    workLog: newData,
                });
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };

    fetchMaterial = (workId) => {
        axio.get(`/material-management/material/getConsumedByWorkId/${this.state.projectId}/${workId}`)
            .then(res => {
                console.log("%%%%%%%%%%%%%%", res.data);
                if (res.data.materialConsumed.length > 0) {
                    let materials = res.data.materialConsumed.map((item) => {
                        return {
                            name: item.material_type.title,
                            quantity: item.quantity,
                            unit: item.material_type.unit
                        }
                    });
                    this.setState({
                        materialData: materials
                    })
                } else {
                    this.setState({
                        materialData: [{
                            name: "",
                            quantity: "",
                            unit: ""
                        }]
                    })
                }

            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };

    rowClick = (record) => {

        console.log('record', record);
        console.log('record.bq', record.allData.bq);
        console.log('record.masons', record.allData.masons);
        this.showModal(this.state.bq);
        this.setState({
            ad: record.allData,
            description: record.workDescription,
            progress: record.progress,
            bq: record.allData.bq,
            masons: record.allData.masons,
        }, console.log('exs', this.state));
        this.fetchMaterial(record.allData.id)
    };
    showModal = (record) => {
        this.setState({
            visible: true
        });
    };

    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    render() {

        let dataList = [];
        let List = [];

        const data = this.state.workLog.map((item, index) => {
            var element = <Moment format="DD-MM-YYYY">{item.date}</Moment>;

            item.bq && item.bq.map((data, i) => {
                let descriptionList = [];
                let progressList = [];
                descriptionList.push(data.description);
                progressList.push(parseFloat(data.percentage).toFixed(2));
                dataList = descriptionList;
                List = progressList;

            });


            return {
                key: item.id,
                workDescription: dataList,
                date: element,
                progress: List + '%',
                labor: item.labor_count,
                checkreq: item.is_visit_request ? 'Yes' : 'No',
                allData: item,
            }
        });

        const columns = [{
            title: 'Description of Work',
            dataIndex: 'workDescription',
            width: 250,
        },
            {
                title: 'Date',
                dataIndex: 'date',
                width: 120,
            }, {
                title: 'Progress',
                dataIndex: 'progress',
                width: 80,
            }, {
                title: 'Labor',
                dataIndex: 'labor',
                width: 80,
            }, {
                title: 'Check Req',
                dataIndex: 'checkreq',
                width: 80,
            }
        ];

        return (
            <>
                {/* right box */}
                <Col span={18}>
                    <div className={"right-nav"}>
                        <div className="custom-box">
                            <Tabs defaultActiveKey="4" className="custom-tabs" size={"large"}>
                                {/* <TabPane tab="Site Conditions" disabled key="1">Content of tab 1</TabPane>
                                    <TabPane tab="Material Dumping" disabled key="2">Content of tab 2</TabPane>
                                    <TabPane tab="Work Done" disabled key="3">Content of tab 3</TabPane> */}
                                <TabPane tab="Works Log" style={{marginTop: "23px"}} key="4">
                                    <Table
                                        columns={columns}
                                        dataSource={data}
                                        onRowClick={(record) => this.rowClick(record)}
                                        className="custom-new-table-click"
                                        size="default"
                                        style={{padding: "8px", fontSize: "20px"}}

                                    />
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </Col>
                {this.state.masons && this.state.materialData !== "" && <Modal
                    title="Work Log Details View"
                    visible={this.state.visible}
                    className="custom-table-popup"
                    destroyOnClose={true}
                    centered={true}
                    onCancel={this.handleCancel}
                    footer={null}
                >
                    {this.state.ad.bq.map((data, m) => {

                        let list = [];
                        list.push(data.description)

                    })}
                    <div>
                        <Row>
                            <Col span={11}>
                                <Row>
                                    <div className="custom-labels">Date</div>
                                </Row>
                                <Row><Input disabled={true} className="mason-input" placeholder=""
                                            value={moment(this.state.ad.date).format('DD-MM-YYYY')}/></Row>
                                <Row>
                                    <div className="custom-labels">Description</div>
                                </Row>
                                <Row><TextArea style={{minHeight: "120px"}} disabled={true} className="mason-input"
                                               placeholder="" value={this.state.description}/></Row>
                                <Row>
                                    <div className="custom-labels">Masons Name</div>
                                </Row>
                                <Row>{this.state.masons.map((msn) => {
                                    return <Input disabled={true} className="mason-input" placeholder="Username"
                                                  value={msn.name}/>
                                })}</Row>

                                <Row>
                                    <div className="custom-labels">Labor Count</div>
                                </Row>
                                <Row><Input disabled={true} className="mason-input" placeholder="Username"
                                            value={this.state.ad.labor_count}/></Row>
                            </Col>
                            <Col span={2}></Col>

                            <Col span={11} style={{"paddingTop": "16px"}}>

                                <Row><span className="custom-labels">Progress</span></Row>
                                <Row><Input disabled={true} className="mason-input" placeholder="Username"
                                            value={parseFloat(this.state.progress).toFixed(2) + '%'}/></Row>
                                <Row>
                                    <div className="custom-labels">Consumed Item(s)</div>
                                </Row>
                                {
                                    this.state.materialData !== "" && this.state.materialData[0].name && this.state.materialData.map((item) => {
                                        return <Row>
                                            <Col span={8}><Input disabled={true} className="mason-input"
                                                                 placeholder="name" value={item.name}/></Col>
                                            <Col span={1}></Col>
                                            <Col span={7}><Input disabled={true} className="mason-input"
                                                                 placeholder="quantity" value={item.quantity}/></Col>
                                            <Col span={1}></Col>
                                            <Col span={7}><Input disabled={true} className="mason-input"
                                                                 placeholder="unit" value={item.unit}/></Col>
                                        </Row>
                                    })
                                }
                            </Col>
                        </Row>
                        <Row>
                            <Col className="custom-colum" span={24}><Button className="modal-button "
                                                                            onClick={this.handleOk}>OK</Button></Col>
                        </Row>
                    </div>
                </Modal>}
                {/* right box */}
            </>
        )
    }
}


const mapStateToProps = state => {
    return {
        projectDetails: state.project_reducer.projectDetails
    }
};

const connected = connect(mapStateToProps)(Work);

export default connected;