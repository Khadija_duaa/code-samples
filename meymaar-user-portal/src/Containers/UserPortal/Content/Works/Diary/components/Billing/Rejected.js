import React, { Fragment } from "react";
import { Row, Col, Table } from "antd";
import { connect } from "react-redux";
import moment from "moment";
import BillingRequestModal from "./BillingModal";
import BillingRequestModalContent from "./BillingModalContent";
import NewLoader from "../../../../../../Loader/NewLoader";

const columns = [
  {
    title: "Invoice ID",
    dataIndex: "viewId",
    width: 90
  },
  {
    title: "Date",
    dataIndex: "dateCreated",
    width: 90,
    render: text => moment(text).format("DD-MM-YYYY")
  },
  {
    title: "Amount",
    dataIndex: "totalVettedAmount",
    width: 90
  },
  {
    title: "Paid By",
    dataIndex: "paidBy",
    width: 90,
    render: () => "N.A"
  }
];

class Approved extends React.Component {
  state = {
    showModal: false,
    billViewId: "",
    bill: {}
  };

  handleRowClick = event => {
    const { viewId } = event;
    console.log("Reject Even click", event);
    this.setState(() => ({
      showModal: true,
      billViewId: viewId,
      bill: event
    }));
  };

  handleToggle = () => {
    this.setState(state => ({ showModal: !state.showModal }));
  };

  getDataSource = () => {
    const data = this.props.billing.paidApproved.processedBillRequests.filter(
      item => {
        return item.paymentStatus === "REJECTED";
      }
    );

    return data;
  };

  render() {
    const {
      billing: { paidApproved }
    } = this.props;

    console.log("in the component Rejected Requests", paidApproved);
    if (paidApproved === "loading") {
      return (
        <>
          <div>
            <NewLoader/>
          </div>
        </>
      );
    }
    if (paidApproved === "error") {
      return (
        <>
          <div>
            <div>Error </div>
          </div>
        </>
      );
    }

    return (
      <Fragment>
        <BillingRequestModal
          visible={this.state.showModal}
          title={this.state.billViewId}
          cancelCB={this.handleToggle}
        >
          <BillingRequestModalContent bill={this.state.bill} />
        </BillingRequestModal>
        <Row>
          <Col>
            <Table
              className="custom-new-table-click"
              columns={columns}
              dataSource={this.getDataSource()}
              onRowClick={this.handleRowClick}
              rowKey={(record, index) => index}
            />
          </Col>
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    billing: state.billing_reducer
  };
};

export default connect(mapStateToProps)(Approved);
