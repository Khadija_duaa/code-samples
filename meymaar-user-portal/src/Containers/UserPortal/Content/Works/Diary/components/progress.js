import React from 'react';
import {Row, Col} from 'antd';
import execUtils from './ExeUtils';
import WorkOverviewChart from './Charts/WorkOverViewChart'

class Progres extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectId: execUtils.getProjectId(),
        }
    }

    render() {
        return (
            <>
                {/* right box */}
                <Col span={18}>
                    <div className={"right-nav"}>
                        <div className="custom-box" style={{minHeight: "inherit"}}>
                            <span className="custom-title custom-after line-blue">PROJECT OVERVIEW</span>
                            <WorkOverviewChart projectId={this.state.projectId}/>
                        </div>
                    </div>
                </Col>
                {/* right box */}
            </>
        )
    }
}


export default Progres;