import React from 'react';
import {Row, Col, message} from 'antd';
import WorkOverviewChart from './Charts/WorkOverViewChart';
import axio from '../../../../../../utils/axios';
import excUtils from './ExeUtils';
import {connect} from 'react-redux';
import {handleError} from '../../../../../../store/store-utils';
import {NOTIFICATION_TIME} from '../../../../../../utils/common-utils';
import NewLoader from "../../../../../Loader/NewLoader";

class WorkOverview extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            completed: 0,
            materialConsumed: 0,
            siteOrder: 0,
            projectId: excUtils.getProjectId(),
        }
    }

    componentDidMount() {
        this.loadData(`/bq-management/projectCost/`, 'project');
        this.loadData(`/site-visit-management/site-visits/getTotalApproved/`, 'site');
        this.loadData(`/material-management/material/getTotalMaterialConsumed/`, 'material')
    }

    loadData = (url, call) => {
        let {projectId} = this.props.projectDetails;
        //dispatch(setProjectProcessing(true));
        axio.get(`${url}${projectId}`)
            .then(res => {
                // console.log("reult of common "+ call, res.data);
                // dispatch(setProjectProcessing(false));
                if (res.data) {
                    switch (call) {
                        case 'project':
                            this.setState({
                                completed: res.data.cost.totalDonePercentage
                            });
                            break;
                        case 'material':
                            this.setState({
                                materialConsumed: res.data.consumed
                            });
                            break;
                        case 'site':
                            this.setState({
                                siteOrder: res.data.sob
                            });
                            break;

                        default:
                            break;

                    }
                }
            })
            .catch(error => {
                //dispatch(setProjectProcessing(false));
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };

    error = (str) => {
        message.error(str);
    };

    onClick() {

    }

    render() {
        if (this.props.processing) return <NewLoader/>;

        let {completed, materialConsumed, siteOrder} = this.state;
        return (
            <>
                {/* right box */}
                <Col span={18}>
                    <div className={"right-nav"}>
                        <Row gutter={45}>
                            <Col md={24} lg={8} span={8}>
                                <div className="custom-wallet-box">
                                    <span className="custom-title custom-after line-blue">PROJECT OVERVIEW</span>
                                    <span className="small-number"
                                          style={{color: "#4444d5"}}>{Math.floor(completed)}<span
                                        className="custom-percentage-span c-b">%</span></span>
                                    <div style={{marginTop: "0px"}}><span
                                        className="custom-paragraph-data-result amount-rows cardtext">COMPLETED</span>
                                    </div>
                                </div>
                            </Col>
                            <Col md={24} lg={8} span={8}>
                                <div className="custom-wallet-box">
                                    <span className="custom-title custom-after line-green">MATERIAL</span>
                                    <span className="small-number">{Math.floor(materialConsumed)}<span
                                        className="custom-percentage-span c-b">%</span></span>
                                    <div style={{marginTop: "0px"}}><span
                                        className="custom-paragraph-data-result amount-rows cardtext">CONSUMED</span>
                                    </div>
                                </div>
                            </Col>
                            <Col md={24} lg={8} span={8}>
                                <div className="custom-wallet-box">
                                    <span className="custom-title custom-after line-purple">SITE ORDER</span>
                                    <span className="small-number"
                                          style={{color: "#870fb8"}}>{Math.floor(siteOrder)}<span
                                        className="custom-percentage-span c-b">%</span></span>
                                    <div style={{marginTop: "0px"}}><span
                                        className="custom-paragraph-data-result amount-rows cardtext">MARKED</span>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row className="custom-space-bottom">
                            <Col span={24}>
                                <div className="chart-sob-display">
                                    <span className="custom-title custom-after line-blue">PROJECT OVERVIEW</span>
                                    <WorkOverviewChart projectId={this.state.projectId}/>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Col>
                {/* right box */}
            </>
        )
    }
}


const mapStateToProps = state => {
    return {
        projectDetails: state.project_reducer.projectDetails,
        processing: state.project_reducer.processing
    }
};

const connected = connect(mapStateToProps)(WorkOverview);

export default connected;