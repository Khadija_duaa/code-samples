import React from 'react';
import {store} from "../../../../../../index";
import {connect} from 'react-redux';
import {Row, Col, message, Button, Modal, Input, Tabs} from 'antd';
import axio from '../../../../../../utils/axios';
import WalletHistory from './WalletHistory';
import WalletCharts from './Charts/Walletchart'
import CostPie from './Charts/CostPie';
import execUtil from '../components/ExeUtils'
import {handleError} from '../../../../../../store/store-utils';
import {formatCurrency, hasAuthority, NOTIFICATION_TIME} from '../../../../../../utils/common-utils';
import {setProjectProcessing} from "../../../../../../store/project/project-actions";
import NewLoader from "../../../../../Loader/NewLoader";

const TabPane = Tabs.TabPane;

class Wallet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectWallet: '',
            projectId: execUtil.getProjectId(),
            walletAddVisible: false,
            addedAmount: '',
            permissions: execUtil.isInOffice(),
            isWalletAmountChanged: false,
            reload: 0,
            pieChart: {
                left: 0,
                paid: 0
            },
        };
    }

    componentDidMount() {
        this.fetchWalletData()
    }

    /******************************** Fetching Wallet Details **********************************/
    fetchWalletData = () => {
        let {dispatch} = store;

        dispatch(setProjectProcessing(true));
        axio.get(`/billing-management/availableAmount/${this.state.projectId}`)
            .then(res => {
                this.setState({
                    projectWallet: res.data
                });
                let piedata = {
                    paid: res.data.paidAmount,
                    left: (res.data.totalProjectAmount - res.data.paidAmount)
                };
                this.setState({
                    pieChart: piedata
                });
                dispatch(setProjectProcessing(false));
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));
            });
    };
    /************************************** END ************************************************/


    reload = () => {
        this.setState({
            reload: 1
        })
    };


    success = (str) => {
        message.success(str);
    };

    error = (str) => {
        message.error(str);
    };


    /********************************** Wallet Modal Events ************************************/

        // Add to wallet toggle modal
    addToWallet = () => {
        this.setState({
            walletAddVisible: true
        })
    };


    // API Call to add amount in the wallet
    addAmount = () => {
        let data = {
            allocatedAmount: this.state.addedAmount,
            projectId: this.state.projectId
        };

        let {dispatch} = store;

        dispatch(setProjectProcessing(true));
        axio.post(`/billing-management/updateWallet`, data)
            .then(() => {
                this.fetchWalletData();
                this.setState({isWalletAmountChanged: true});
                dispatch(setProjectProcessing(false));
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));
            });

        this.setState({walletAddVisible: false, addedAmount: ''})
    };

    // Handling OK Modal Callback
    handleOk = (e) => {
        this.setState({
            walletAddVisible: false,
        });

    };


    // Handling Cancel Callback
    handleCancel = (e) => {
        this.setState({
            walletAddVisible: false,
        });

    };


    // Change Function to set wallet amount into state
    changeHandler = (e) => {
        if (e.target.value >= 0) {
            this.setState({
                addedAmount: e.target.value
            })
        }
    };


    // On Key Press Events to restrict user to enter zero
    onKeyPress = (e) => {

        const characterCode = e.key;
        if (characterCode === 'Backspace') return;

        const characterNumber = Number(characterCode);
        if (characterNumber >= 0) {
            if (e.currentTarget.value && e.currentTarget.value.length) {
                return;
            }
            else if (characterNumber === 0) {
                e.preventDefault()
            }
        } else {
            e.preventDefault()
        }
    };


    // Rendering Add Amount Wallet Modal
    renderWalletModal = () => {
        return (
            <div>
                <Modal
                    title="ADD AMOUNT TO WALLET"
                    visible={this.state.walletAddVisible}
                    onOk={this.handleOk}
                    centered={true}
                    onCancel={this.handleCancel}
                    footer={null}
                    style={{textAlign: "center"}}
                    maskClosable={false}
                >
                    <Row>
                        <Col span={24}>
                            <Row>AMOUNT</Row>
                            <Row><Input style={{width: "400px"}} className="mason-input" id="amount"
                                        type="number" placeholder="" value={this.state.addedAmount}
                                        onChange={this.changeHandler} onKeyPress={this.onKeyPress}/></Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="custom-colum" span={24}>
                            <Button className="modal-button-add " onClick={this.addAmount}>ADD</Button>
                        </Col>
                    </Row>
                </Modal>
            </div>
        )
    };


    /************************************** END **********************************************/


    /********************************** Wallet UI Events ************************************/

    renderWalletHeaderAmounts = () => {
        let {projectWallet} = this.state;

        let boxesJSON = [
            {
                title: "Project Cost",
                titleClass: "custom-title custom-after line-green amount-rows",

                text: formatCurrency(projectWallet.totalProjectAmount),
                textClass: "custom-wallet-paragraph amount-color amount-color-green amount-rows",

                subTitle: "Total cost of project",
                subTitleClass: "custom-wallet-paragraph amount-rows"
            },
            {
                title: "Received Amount",
                titleClass: "custom-title custom-after line-blue amount-rows",

                text: formatCurrency(projectWallet.allocatedAmount),
                textClass: "custom-wallet-paragraph amount-color amount-color-blue amount-rows",

                subTitle: "Amount received",
                subTitleClass: "custom-wallet-paragraph amount-rows amount-rows"

            },
            {
                title: "Paid Amount",
                titleClass: "custom-title custom-after line-purple amount-rows",

                text: formatCurrency(projectWallet.paidAmount),
                textClass: "custom-wallet-paragraph amount-color amount-color-purple amount-rows",

                subTitle: "Paid to contractors",
                subTitleClass: "custom-wallet-paragraph amount-rows"
            },
            {
                title: "Wallet",
                titleClass: "custom-title custom-after line-red amount-rows",

                text: formatCurrency((projectWallet.allocatedAmount - projectWallet.paidAmount).toFixed(2)),
                textClass: "custom-wallet-paragraph amount-color amount-color-red amount-rows",

                subTitle: "Total amount in wallet",
                subTitleClass: "custom-wallet-paragraph amount-rows"
            },
        ];

        let boxesJSX = boxesJSON.map((boxData, index) => {
            return (
                <Col key={index} span={6} md={24} lg={6}>
                    <div className="custom-wallet-box">
                        <span className={boxData.titleClass}>{boxData.title}</span>
                        <span className={boxData.textClass}>{boxData.text}</span>
                        <span className={boxData.subTitleClass}>{boxData.subTitle}</span>
                    </div>
                </Col>
            )
        });
        return (
            <Row gutter={30}>
                {this.renderWalletModal()}
                {boxesJSX}
            </Row>
        )
    };

    // Rendering Wallet Add Button
    renderAddWalletButton = () => {
        let {permissions} = this.state;
        if (permissions.inOffice && hasAuthority('CAN_MANAGE_WALLET')) {
            return (
                <Row type="flex" align="middle" className="column-wrap-new">
                    <Col style={{paddingLeft: "0px"}} span={18}>
                        <Button className="submit-bill-new" icon="plus" onClick={this.addToWallet}>
                            ADD TO WALLET
                        </Button>
                    </Col>
                </Row>
            )
        }
    };

    // Rendering Wallet Tabs
    renderWalletTabs = () => {
        return (
            <Row>
                <Tabs defaultActiveKey="2" className="custom-tabs">
                    <TabPane tab="STATISTICS" key="1">
                        <Row>
                            <Col span={24}>
                                <div><WalletCharts projectId={this.state.projectId}/></div>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <div> {this.state.pieChart.left !== 0 &&
                                <CostPie paid={this.state.pieChart.paid} left={this.state.pieChart.left}/>}
                                </div>
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tab="DETAILS" key="2">`
                        <WalletHistory projectId={this.state.projectId}
                                       isWalletAmountChanged={this.state.isWalletAmountChanged}/>
                    </TabPane>
                </Tabs>
            </Row>
        )
    };

    /************************************* END **********************************************/



    render() {

        if (this.props.processing) return <NewLoader/>;
        return (
            <>
                {/* right box */}
                <Col span={18}>
                    {this.renderWalletHeaderAmounts()}

                    {this.renderAddWalletButton()}

                    {this.renderWalletTabs()}
                </Col>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        processing: state.project_reducer.processing
    }
};
export default connect(mapStateToProps)(Wallet);