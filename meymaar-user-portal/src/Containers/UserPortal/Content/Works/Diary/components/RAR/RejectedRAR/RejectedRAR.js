import React from 'react';
import {Table, Modal,Button} from 'antd';
import moment from 'moment';
import {formatCurrency} from "../../../../../../../../utils/common-utils";
import RARForm from '../../../../../../../../Components/CustomForms/RARForm/RARForm';
import BOQModal from '../../../../../../../../Components/ModalFactory/BOQModal/BOQModal';
class RejectedRAR extends React.Component {

    state = {
        showModal: false,
        requestDetails: null,
        showBQModal: false,
        bqDetails: null,
    };

    /****************************** Modal ****************************************/

    toggleBQModal = (record) => {
        record = record && record.billRequest.billingBqsItem;
        this.setState({bqDetails: record, showBQModal: !this.state.showBQModal})
    };
    handleCancelOK = () => {
        this.setState({showBQModal: false})
    };
    renderBQDetailsModal = () => {

        return (
            <Modal
                className={"bq-details-modal"}
                title=" Rejected RAR BOQ Details"
                visible={this.state.showBQModal}
                onOk={this.handleCancelOK}
                onCancel={this.handleCancelOK}
                destroyOnClose={true}
                footer={null}
                maskClosable={true}
                centered={true}
            >
                <BOQModal dataSource={this.state.bqDetails}/>
            </Modal>
        )
    };


    toggleModal = (data) => {
        this.setState({showModal: !this.state.showModal, requestDetails: data});
    };
    renderRejectedBillModal = () => {
        if (!this.state.requestDetails) return null;
        let formData = {
            isPreview: true,
            requestDetails: this.state.requestDetails,
            isPayment: false
        };

        return (
            <Modal
                centered={true}
                title="Rejected RAR"
                className="create-bill-model"
                visible={this.state.showModal}
                onOk={() => this.setState({showModal: false})}
                onCancel={() => this.setState({showModal: false})}
            >
                <RARForm {...formData}/>
            </Modal>
        )
    };

    /****************************** END ******************************************/



    /******************************* EVENTS **************************************/
    renderText = (text, record) => {
        return (
            <p onClick={() => this.toggleModal(record)}>
                {text}
            </p>
        )
    };


    renderTableData = () => {
        this.props.rarData.forEach((data, index) => {
            data.index = index;
            data.rarNumber = data.number ? data.number : "-";
            data.date = moment(data.dateCreated).format("DD-MM-YYYY");
            data.totalAmount = formatCurrency(parseFloat(data.netAmount).toFixed(2));
            data.createdBy = data.creatorUserName;
        });

        let columns = [
            {
                key: "rarNumber",
                title: "Receipt Number",
                dataIndex: "rarNumber",
                editable: false,
                render: (e, data) => this.renderText(data.rarNumber, data)
            },
            {
                key: "date",
                title: "Date",
                dataIndex: "date",
                editable: false,
                render: (e, data) => this.renderText(data.date, data)
            },
            {
                key: "totalAmount",
                title: "Net Amount",
                dataIndex: "totalAmount",
                editable: false,
                render: (e, data) => this.renderText(data.totalAmount, data)

            },
            {
                key: "createdBy",
                title: "Created By",
                dataIndex: "createdBy",
                editable: false,
                render: (e, data) => this.renderText(data.createdBy, data)
            },
            {
                key: "bqDetails",
                title: "BOQ's",
                dataIndex: "bqDetails",
                editable: false,
                render: (e, data) => {
                    return (
                        <Button className={"view-details"} onClick={() => this.toggleBQModal(data)}>
                            View BOQ's
                        </Button>
                    )
                }
            },
        ];


        return (
            <Table
                className="custom-new-table-click rar-table"
                columns={columns}
                dataSource={this.props.rarData}
                rowKey={(record, index) => index}
            />
        )
    };

    /****************************** END ******************************************/


    render() {
        let {rarData} = this.props;

        if (!rarData || !rarData.length) return <h6>No Rejected RAR Exists!</h6>;


        return (
            <div>
                {this.renderRejectedBillModal()}
                {this.renderBQDetailsModal()}
                {this.renderTableData()}
            </div>
        )
    }
}

export default RejectedRAR;