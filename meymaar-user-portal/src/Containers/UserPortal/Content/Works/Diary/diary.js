import React from "react";
import {Route, Switch, Link} from "react-router-dom";
import {Row, Col, Menu} from "antd";
import WorkOverview from "./components/workOverview";
import Work from "./components/work";
import Progres from "./components/progress";
import SiteOrderBook from "./components/SOB/siteOrderBook";
import MaterialAtSite from "./components/materialAtSite";
// import Billing from "./components/billing";
import BillingRequests from "./components/BillingRequests";
import RAR from './components/RAR/RAR';
import Wallet from "./components/Wallet";
import {connect} from 'react-redux';
import BreadCrumb from '../../../../../Components/BreadCrumb/BreadCrumb';

class Diary extends React.Component {

    componentDidMount() {
        let {projectDetails} = this.props;
        if (!projectDetails) {
            this.props.history.push('/dashboard/works/overview');
        } else {
            let diaryState = this.getDiaryState();

            let execOffice = '';

            if (diaryState && diaryState.execOffice) {
                execOffice = diaryState.execOffice;
            } else {
                let {projectWorkflow, exec_office, projectId} = this.props.projectDetails;

                let locationState = {
                    projectWorkFlow: projectWorkflow,
                    projectId: projectId,
                    execOffice: exec_office
                };

                sessionStorage.setItem("diary-state", JSON.stringify(locationState));
            }

            this.setState({execOffice});
        }
    }

    getDiaryState = () => {
        const storageKey = "diary-state";
        let locationState = sessionStorage.getItem(storageKey);
        if (locationState) {
            locationState = JSON.parse(locationState);
        }
        const {location} = this.props;
        if (location && location.state) {
            locationState = location.state;
        }

        sessionStorage.setItem(storageKey, JSON.stringify(locationState));
        return locationState;
    };

    /********************************* Events *****************************/

    getActiveKey = (menuJSON) => {
        let activeIndex = 1;
        let ctr = 0;

        for (let i = 0; i < menuJSON.length; i++) {
            let currentMenu = menuJSON[i];
            if (currentMenu.path === this.props.location.pathname) {
                activeIndex = ctr;
                break;
            }
            ctr++;
        }

        return activeIndex.toString();
    };

    renderDiaryMenu = () => {

        let diaryMenuJSON = [
            {
                title: "Work Overview",
                path: "/dashboard/diary"
            },
            {
                title: "Work Diary",
                path: "/dashboard/diary/work"
            },
            {
                title: "Measurement Book",
                path: "/dashboard/diary/wb"
            },
            {
                title: "Site Order Book (SOB)",
                path: "/dashboard/diary/siteorderbook"
            },
            {
                title: "Material At Site (MAS)",
                path: "/dashboard/diary/materialatsite"
            },
            {
                title: "Wallet",
                path: "/dashboard/diary/wallet"
            },
            {
                title: "Bill Requests",
                path: "/dashboard/diary/billingRequests"
            },
            {
                title: "RAR's",
                path: "/dashboard/diary/rarRequests"
            }
        ];
        let navStyle = {fontWeight: "600", fontSize: "16px"};

        let menuJSX = diaryMenuJSON.map((menu, index) => {
            return (
                <Menu.Item key={index}>
                    <Link style={navStyle} to={menu.path}>{menu.title}</Link>
                </Menu.Item>
            )
        });

        let selectedIndex = this.getActiveKey(diaryMenuJSON);
        return (
            <Col span={6}>
                <div className="left-nav custom-box-simple-nav">
                    <Menu className="overview-left-menu" defaultSelectedKeys={[selectedIndex]} style={navStyle}>
                        {menuJSX}
                    </Menu>
                </div>
            </Col>
        )
    };

    /******************************* END **********************************/



    render() {

        let {projectDetails} = this.props;
        if (!projectDetails) {
            return null;
        }

        return (
            <>
                <Row gutter={8}>
                    {/* left menu */}

                    {/*{this.getBreadCrumbs()}*/}

                    {/*<div style={{marginBottom: "70px"}}>*/}
                        {/*<h2>Project Execution for {projectDetails.name ? projectDetails.name : "no data"}</h2>*/}
                        {/*<h5>Reference*/}
                            {/*#: {projectDetails.referenceNumber ? projectDetails.referenceNumber : 'no data'}</h5>*/}
                    {/*</div>*/}


                    {this.renderDiaryMenu()}

                    {/* left menu */}

                    <Switch>
                        <Route path="/dashboard/diary/work" component={Work}/>
                        <Route path="/dashboard/diary/wb" component={Progres}/>
                        <Route path="/dashboard/diary/siteorderbook" component={SiteOrderBook}/>
                        <Route path="/dashboard/diary/materialatsite" component={MaterialAtSite}/>


                        <Route path="/dashboard/diary/billingRequests" component={BillingRequests}/>
                        <Route path="/dashboard/diary/rarRequests" component={RAR}/>


                        <Route path="/dashboard/diary/wallet" component={Wallet}/>
                        <Route path="/dashboard/diary" component={WorkOverview}/>
                    </Switch>
                </Row>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        projectDetails: state.project_reducer.projectDetails
    }
};

const connected = connect(mapStateToProps)(Diary);

export default connected;
