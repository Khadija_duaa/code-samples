import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";
import {isCompletedPhase, sortByKeyAsc} from "../../../../../utils/common-utils";
import ProjectPlanning from "./ProjectPlanning";
import './planningStyle.css';
import NewLoader from '../../../../Loader/NewLoader';
import BreadCrumb from '../../../../../Components/BreadCrumb/BreadCrumb';
import {Alert, message} from 'antd';
import StepsBarPlanning from "../../../../../Components/NewLayouts/ProjectPlanning/StepsBar/StepBarPlanning";
import {setPhaseGroup} from "../../../../../store/project/project-actions";

class Planning extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            processing: false,
            activePhaseIndex: null,
            currentPhase: null,
            currentStep: null
        }
    }

    componentWillMount = () => {

        let {projectDetails} = this.props;

        let errors = (msg) => {

            message.error(msg);
        };

        if (!projectDetails || !projectDetails.projectWorkflow || !projectDetails.phaseGroups) {
            this.props.history.push("/dashboard/works/overview");
        } else if (!projectDetails.phaseGroups.length) {
            errors('Phase Groups are not defined for this project');
            this.props.history.push("/dashboard/works/overview");
        }

    };


    /************************************************** EVENTS *******************************************/


    handleTabClickStepBar = (tabIndex, item) => {
        this.setState({processing: true}, () => {
            setTimeout(() => {
                this.setState({
                    activePhaseIndex: tabIndex,
                    currentPhase: item.phase,
                    currentStep: item,
                    processing: false,
                }, () => {
                    this.props.setPhaseGroup(item)
                });
            }, 500)
        });

    };


    setActivePhase = (phase) => {

        this.setState({currentPhase: phase});
    };



    /************************************************** END *******************************************/

    getDefaultStep = () => {

        let defaultStep = null;
        let {phaseGroups, projectWorkflow} = this.props.projectDetails;
        let {activePhaseIndex} = this.state;

        for (let index = 0; index < phaseGroups.length; index++) {

            let phaseStep = phaseGroups[index];
            let {phases} = phaseStep;


            for (let j = 0; j < phases.length; j++) {
                if ((!activePhaseIndex && projectWorkflow.defaultPhase.uuid === phases[j].phase.uuid) || (index === activePhaseIndex && phaseGroups[activePhaseIndex].phases[j].active)) {
                    defaultStep = {
                        step: phaseStep,
                        defaultPhase: phases[j].phase,
                    };
                }
            }

            if (defaultStep) {
                break;
            }

        }

        return defaultStep;
    };

    render() {

        let {projectDetails} = this.props;


        if (!projectDetails || !projectDetails.phaseGroups || !projectDetails.phaseGroups.length)
            return null;

        if (this.state.processing) {
            return <NewLoader/>
        }

        let markingRights = this.props.editRights;
        let stepDetails = this.getDefaultStep();

        let activePhase = this.state.currentPhase ? this.state.currentPhase : stepDetails ? stepDetails.defaultPhase : null;
        let activeStep = this.state.currentStep ? this.state.currentStep : stepDetails ? stepDetails.step : null;
        let isCompleted = isCompletedPhase(activePhase ? activePhase.uuid : null);

        let {activePhaseIndex, currentPhase} = this.state;
        let stepsBarProps = {
            projectDetails: this.props.projectDetails,
            currentPhase,
            activeStep,
            activeStepIndex: activePhaseIndex,
        };


        return (
            <div className={"col-md-12"} style={{paddingBottom: "84px "}}>


                <StepsBarPlanning {...stepsBarProps}
                                  onStepClick={(stepIndex, step) => this.handleTabClickStepBar(stepIndex, step)}/>

                <ProjectPlanning {...projectDetails} completed={isCompleted} currentStep={activeStep}
                                 markingRights={markingRights}
                                 projectDetails={this.props.projectDetails} currentPhase={activePhase}
                                 onPhaseClick={(phase) => this.setActivePhase(phase)}/>

            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        projectDetails: state.project_reducer.projectDetails,
        editRights: state.project_reducer.editRights
    }
};

const connected = connect(mapStateToProps, {setPhaseGroup})(Planning);

export default withRouter(connected);