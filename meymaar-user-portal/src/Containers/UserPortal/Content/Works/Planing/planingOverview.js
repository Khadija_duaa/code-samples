import React from 'react';
import Document from "../../../../../Components/DocumentContent/Document";
import {ModalBody, ModalHeader, Col, Row} from 'reactstrap';
import Modal from '../../../../../Components/Modal/Modal';
import './planningStyle.css';
import AddDocumentForm from '../../../../../Components/CustomForms/AddDocument/AddDocumentForm';
import axios from '../../../../../utils/axios';
import {handleError} from "../../../../../store/store-utils";
import {isDocumentsComplete, NOTIFICATION_TIME} from "../../../../../utils/common-utils";
import {message} from 'antd';
import 'antd/dist/antd.css';
import {store} from "../../../../../index";
import {connect} from 'react-redux';
import MarkingForm from '../../ProjectMarking/MarkingForm/MarkingForm';
import SanctionForm from "../../ProjectSanction/SanctionForm";
import NewLoader from '../../../../Loader/NewLoader';
import InformationModal from '../../../../../Components/ModalFactory/MessageModal/InformationModal'
import {setProcessing} from "../../../../../store/server/server-actions";
import DocumentTab from "../../../../../Components/NewLayouts/ProjectPlanning/DocumentTab/DocumentTab";
import PhaseStatusContainer
    from "../../../../../Components/NewLayouts/ProjectPlanning/PhaseStatusContainer/PhaseStatusContainer";
import Menu from "../../../../../Components/NewLayouts/ProjectPlanning/Menu/Menu";
import HeaderButton from "../../../../../Components/Buttons/HeaderButton/HeaderButton";

class PlaningOverview extends React.Component {

    constructor(props) {
        super(props);
        let str = window.navigator.userAgent;
        let isFireFoxBrowser = str.includes("Firefox");
        this.state = {
            activeDocumentTitle: '',
            processing: false,
            showInfoModal: false,
            showConfirmModal: false,
            isNextPhase: false,
            revoking: false,
            activeDocument: null,
            isFireFox:isFireFoxBrowser,
            nextPhaseMarking: props.phaseDetails.sanctionInfo && props.phaseDetails.sanctionInfo.nextPhaseMarkingInfo
        };
    }



    componentDidMount() {
        let {forms} = this.props.phaseDetails;
        let {activeDocument} = this.state;

        let activeForm = activeDocument || forms[0];
        this.handlePhaseFormClick(activeForm);

    }


    getPhaseStatusData = {
        tasksCompleted: '04/06',
        tasksCompletion: 'Phases Completed',
        ToDo: 'To Do',
        InProgress: 'In Progress',
        Complete: 'Complete'
    };

    /***************************************** Modals *****************************************/


    /*************** Additional Document Modal *****************/

    toggleDocumentModal = () => {
        this.setState({showModal: !this.state.showModal});
    };

    renderDocumentModal = () => {

        return (
            <Modal isOpen={this.state.showModal}>
                <ModalHeader className={"header"}>
                    Add New Document
                </ModalHeader>

                <ModalBody>
                    <AddDocumentForm {...this.props} toggleCB={this.toggleDocumentModal}/>
                </ModalBody>
            </Modal>
        )
    };

    /*************** END *****************/


    /******************** Information Modal if show info modal is true *****************/

    toggleInfoModal = () => {
        if (this.state.data) {
            this.setState({showInfoModal: false}, () => {
                this.props.history.push(this.state.infoPath)
            })
        } else {
            this.setState({showInfoModal: !this.state.showInfoModal, canRevoking: false})
        }
    };

    handleInfoCB = () => {
        if (this.state.revoking) {
            this.props.history.push('/dashboard/works/overview')
        } else {
            this.props.history.push('/dashboard/task-board');
        }

    };

    renderInformationModal = () => {
        let {data} = this.state;

        let message = '';

        if (data) {
            message = data;
        }

        return (
            <InformationModal isOpen={this.state.showInfoModal}
                              okCB={this.handleInfoCB} cancelCB={this.toggleInfoModal}
                              message={message}/>
        );
    };


    /******************** END *****************/


    /******************** Marking Document Modal *****************/

    toggleMarkingModal = () => {
        let {isNextPhase} = this.state;
        if (isNextPhase) {
            this.props.history.push('/dashboard/task-board');
        } else {
            this.setState({showMarkingModal: !this.state.showMarkingModal})
        }
    };

    handleMarkingCB = (data) => {

        // Setting showInfoModal True and show information on modal
        this.setState({
            showMarkingModal: false,
            showInfoModal: true,
            data,
            infoPath: "/dashboard/task-board"
        });
    };

    renderMarkingModal = () => {
        let {markingInfo} = this.props.phaseDetails;
        let {nextPhaseMarking, isNextPhase} = this.state;
        markingInfo = nextPhaseMarking && isNextPhase ? nextPhaseMarking : markingInfo;

        let markingHeaderText = "Please fill the following input fields";
        if (isNextPhase) {
            markingHeaderText = 'Sanction has been accorded successfully, kindly mark project to relevant person.';
        }

        return (
            <Modal style={{width: "500px"}} isOpen={this.state.showMarkingModal}>
                <ModalHeader className={"header"}>
                    Mark
                </ModalHeader>

                <ModalBody>
                    <MarkingForm {...this.props} headerText={markingHeaderText} markingInfo={markingInfo}
                                 successCB={(message) => this.handleMarkingCB(message)}
                                 cancelCB={this.toggleMarkingModal}/>
                </ModalBody>
            </Modal>
        )
    };

    /*************************** END **************************/


    /******************** Sanction Document Modal *****************/

    toggleSanctionModal = () => {
        let {sanctionInfo} = this.props.phaseDetails;
        let {canRevokeSanction} = sanctionInfo;
        this.setState({
            showSanctionModal: !this.state.showSanctionModal,
            revoking: !this.state.revoking && canRevokeSanction
        })
    };

    sanctionSuccessCB = (data) => {

        // Setting showInfoModal True and show information on modal

        let {canRevokeSanction, nextPhaseMarkingInfo} = this.props.phaseDetails.sanctionInfo;
        let isNextPhase = Boolean(nextPhaseMarkingInfo && !canRevokeSanction);
        if (!data || data === "") {
            data = `Sanction has been ${canRevokeSanction ? "revoked" : "accorded"} successfully`;
        }

        this.setState({
            showSanctionModal: false,
            showInfoModal: Boolean(!nextPhaseMarkingInfo || canRevokeSanction),
            isNextPhase: isNextPhase,
            showMarkingModal: isNextPhase,
            data,
            infoPath: '/dashboard/task-board'
        });
    };

    renderSanctionModal = () => {

        let {sanctionInfo, phaseEditable} = this.props.phaseDetails;
        let dataProps = {
            sanctionsInfo: sanctionInfo,
            editRights: phaseEditable,
            ...this.props
        };

        return (
            <Modal style={{width: "650px"}} isOpen={this.state.showSanctionModal}>
                <ModalHeader className={"header"}>
                    {sanctionInfo.canRevokeSanction ? "Revoke Sanction" : sanctionInfo.sanctionName}
                </ModalHeader>

                <ModalBody>
                    <SanctionForm {...dataProps} successCB={(message) => this.sanctionSuccessCB(message)}
                                  cancelCB={this.toggleSanctionModal}/>
                </ModalBody>
            </Modal>
        )
    };

    /************************** END *******************************/


    /***************************************** Modals END *****************************************/


    /************ Unselecting Document, Previous Document & Next Document Click *********/
    unSelectDocument = () => {
        this.setState({activeDocumentTitle: ''});
    };
    prevDocument = () => {
        let currentDoc = this.state.activeDocumentTitle;
        let formData = [...this.props.phaseDetails];

        let prevIndex = -1;

        for (let index = 0; index < formData.length; index++) {
            let _form = formData[index].form;
            if (currentDoc === _form.title) {
                prevIndex = index - 1;
                break;
            }
        }

        if (prevIndex > -1) {
            let activeDocumentTitle = formData[prevIndex].form.title;
            this.setState({activeDocumentTitle});
        }
    };
    nextDocument = () => {
        let currentDoc = this.state.activeDocumentTitle;
        let formData = [...this.props.phaseDetails];

        let nextIndex = -1;

        for (let index = 0; index < formData.length; index++) {
            let _form = formData[index].form;
            if (currentDoc === _form.title) {
                nextIndex = index + 1;
                break;
            }
        }


        if (nextIndex > -1 && nextIndex < formData.length) {

            this.handlePhaseFormClick(formData[nextIndex]);
        }
    };
    /*********************************** END ***********************************/


    /*********************************** Header Buttons List  **************************************/

    /*********************************** Header Buttons List END  **************************************/

    isDisabledPreviewDownload = () => {
        let isDownloadableURL = false;
        this.props.phaseDetails.forms.forEach(phase => {
            isDownloadableURL = isDownloadableURL || phase.form.downloadURL;
        });

        return isDownloadableURL;
    };

    previewDownloadAllDocuments = (isPreview = true) => {

        console.log("Preview Download - All Documents");

        let phaseData = {
            forms: []
        };

        this.props.phaseDetails.forms.forEach(phase => {
            phaseData.forms.push({...phase.form});
        });

        let {dispatch} = store;

        dispatch(setProcessing(true));
        axios.post(`/document-management/doc/create-pdfs/${this.props.projectId}/${this.props.phaseId}`, phaseData, true)
            .then(response => {

                if (isPreview) {

                    let blob = new Blob([response.data], {type: 'application/pdf'});
                    let pdf = URL.createObjectURL(blob);
                    window.open(pdf, "_blank");
                } else {

                    // Creating Link and click programmaticallly
                    const url = window.URL.createObjectURL(response.data);
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', `${this.props.currentPhase.name}.pdf`);
                    document.body.appendChild(link);
                    link.click();
                }
                dispatch(setProcessing(false));
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    };


    //Marking Button At on the of top navigation bar
    getMarkButtonPlanning = () => {
        let {markingInfo} = this.props.phaseDetails;
        if (!markingInfo) return null;

        let buttonProps = {
            className: "custom-button custom-button-dark create-button-planning",
            buttonStyle: {marginLeft: '10px', height: "auto",},
            onBtnClick: () => this.toggleMarkingModal(),
            imgURL: 'images/mark.svg',
            imgWidth: "20",
            imgColor: "white",
            imgStyle: {display: 'inline'},
            textStyle: {color: 'white', display: "inline", marginLeft: '10px'}
        };

        const {internalMarkingInfo, externalMarkingInfo} = markingInfo;

        if (internalMarkingInfo || externalMarkingInfo) {
            return (
                <HeaderButton {...buttonProps}>
                    Mark
                </HeaderButton>
            )
        }
        return null;
    };

    // Sanction Button on the top of navigation bar
    getSanctionButtonPlanning = () => {
        let {sanctionInfo} = this.props.phaseDetails;


        let isDocumentComplete = isDocumentsComplete(this.props.phaseDetails);
        if ((!sanctionInfo.canAccordSanction && !sanctionInfo.canRevokeSanction) || !isDocumentComplete) return null;
        let str = window.navigator.userAgent;

        let {isFireFox} = this.state;
        let buttonProps = {
            className: `custom-button ${sanctionInfo.canRevokeSanction ? "danger-button" : "custom-button-dark"} create-button-planning`,
            buttonStyle: {width: isFireFox?"-moz-fit-content":'fit-content', height: "auto"},
            onBtnClick: this.toggleSanctionModal,
            imgURL: 'images/shar.svg',
            imgHeight: "40",
            imgWidth: "20",
            imgColor: "white",
            imgStyle: {display: 'inline'},
            textStyle: {color: '#fff', display: "inline", marginLeft: '10px'}
        };


        return (
            <HeaderButton {...buttonProps}>
                {sanctionInfo.canRevokeSanction ? "Revoke Sanction" : sanctionInfo.sanctionName}
            </HeaderButton>
        )
    };


    /***************************** Phase Forms Events **********************/

        // Click handler when Phase Form item clicked
    handlePhaseFormClick = (activeDocumentProps) => {
        this.setState({processing: true}, () => {
            setTimeout(() => {
                let activeDocumentTitle = activeDocumentProps.form.title;
                this.setState({activeDocumentTitle, activeDocument: activeDocumentProps, processing: false});
            }, 300)
        })
    };

    // Rendering Phase Forms
    renderPhaseForms = () => {
        let {forms, phaseEditable} = this.props.phaseDetails;

        let phaseFormsJSX = forms.map((formData, index) => {
            let imageURL = !formData.isVisited ? 'url(images/unvisited-mark.svg)' : formData.complete ? 'url(images/checked.svg)' : 'url(images/warning.svg)';

            let style = {
                backgroundImage: imageURL,
                position: 'center',
                cursor: "pointer",
                border: this.state.activeDocumentTitle !== formData.form.title ? '2px solid #E0E5E8' : '2px solid #30BDA0',
                height: '105px',
                width: '115px',
                borderRadius: '7px',
                backgroundColor: '#FFFFFF',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center center',
                marginBottom: '5px',
                marginTop: '25px',
                marginRight: '0px',
                padding: '0px',
            };

            if (!formData.isVisited) {
                style.backgroundColor = '#FFFFFF';
            }


            let className = '';
            if (this.state.activeDocumentTitle !== formData.form.title) {
                className = "sidebar-content-highlighted-component"
            } else {
                className = "sidebar-content-highlighted-component-active"
            }

            return (
                <Row key={index} style={{marginLeft: '40px', marginRight: '-10px'}}>

                    <DocumentTab className={className} style={style} keyValue={index} onClickFunction={() => {
                        this.handlePhaseFormClick(formData)
                    }} formData={formData.form.title}/>

                </Row>

            )
        });

        let additionalDocJSX = () => {
            let style = {
                backgroundImage: 'url(images/upload.svg)',
                backgroundSize: "32px 32px",
                position: 'center',
                cursor: "pointer",
                height: '105px',
                width: '115px',
                borderRadius: '7px',
                backgroundColor: '#FFFFFF',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center center',
                marginBottom: '5px',
                marginTop: '25px',
                marginRight: '0px',
                padding: '0px',
                border: '2px dashed #30BDA0',
            };
            let fontStyle = {
                color: "#30BDA0",
            };


            return (
                <Row key={phaseFormsJSX.length} style={{marginLeft: '40px', marginRight: '-10px'}}>

                    <DocumentTab className={"sidebar-content-highlighted-component"} style={style}
                                 key={phaseFormsJSX.length}
                                 onClickFunction={() => this.toggleDocumentModal()} formData={'Add Docs'}
                                 fontStyle={fontStyle} iconColor={'#30BDA0'}/>

                </Row>
            )
        };


        if (phaseEditable) {
            phaseFormsJSX.push(additionalDocJSX());
        }

        return (
            <Col span={24} className="sidebar-contentComponent">
                <div style={{
                    maxWidth: "100%",
                    overflowX: "auto",
                    width: '100%',
                    height: '190px',
                    display: 'flex',
                    backgroundColor: '#F4F4F7',
                    borderRadius: '7px',
                    border: '1px solid #E0E5E8',
                    marginTop: '20px',

                }}>
                    <div style={{display: "none"}}/>
                    {phaseFormsJSX}
                </div>
            </Col>
        )
    };


    /************************************ Phase Forms Events End ************************************/


    /***************************************** EVENTS *****************************************/

    renderHeaderButtonsList = () => {
        let {projectId, name, markingHistory, currentProjectOfficeData} = this.props;
        let isPreview = true;//this.isDisabledPreviewDownload();

        let buttonProps = {
            className: `custom-button custom-button-light-planning create-button-planning ${!isPreview ? "disabled-preview-btn" : ""}`,
            onBtnClick: this.previewDownloadAllDocuments,
            buttonStyle: {marginLeft: "10px", height: "auto"},
            imgURL: 'images/preview.svg',
            imgHeight: "40",
            imgWidth: "20",
            imgStyle: {display: 'inline'},
            textStyle: {color: "#3d5470", display: "inline", marginLeft: '10px'},
        };

        let downloadButtonProps = {
            ...buttonProps,
            onBtnClick: () => this.previewDownloadAllDocuments(false),
            imgURL: "images/download.svg"
        };

        let str = window.navigator.userAgent;
        let {isFireFox} = this.state;

        let historyButtonProps = {
            ...buttonProps,
            className: "custom-button custom-button-light-planning create-button-planning",
            buttonStyle: {
                ...buttonProps.buttonStyle,
                width: isFireFox ? "-moz-fit-content" : 'fit-content',
                height: "auto"
            },
            imgURL: "images/alarm-clock-2.svg",
            onBtnClick: () => this.props.history.push({
                pathname: "/dashboard/works/marking-history",
                state: {
                    projectId,
                    markingHistory,
                    currentProjectOfficeData,
                    projectName: name,
                    returnCB: "/dashboard/works/planning"
                }
            }),

        };

        return (
            <Col span={24}>
                <div style={{
                    display: 'flex',
                    float: 'right',
                    textAlign: 'right',
                }}>


                    {this.getSanctionButtonPlanning()}

                    {this.getMarkButtonPlanning()}


                    <HeaderButton {...historyButtonProps}>
                        Marking History
                    </HeaderButton>


                    <HeaderButton disabled={!isPreview} {...buttonProps}>
                        Preview
                    </HeaderButton>


                    <HeaderButton disabled={!isPreview} {...downloadButtonProps}>
                        Download
                    </HeaderButton>


                </div>
            </Col>
        )
    };

    // Getting Active Clicked Document Properties
    getActiveDocumentProps = () => {
        let {activeDocumentTitle} = this.state;
        let activeDocumentProps = {};

        this.props.phaseDetails.forms.forEach(phase => {

            if (phase.form.title === activeDocumentTitle) {
                activeDocumentProps = {...phase};
            }
        });

        return activeDocumentProps;
    };


    renderContentPlanning = () => {
        let {form, editable} = this.getActiveDocumentProps();
        let {phaseEditable} = this.props.phaseDetails;

        if (this.state.processing || this.props.projectProcessing) {
            return <NewLoader/>
        }

        let editRights = phaseEditable && editable;

        let projectDetails = {
            ...this.props,
            editRights
        };

        if (!form || !form.formType) return null;

        return (
            <>
                <Document unSelectDocument={this.unSelectDocument} nextDocument={this.nextDocument}
                          prevDocument={this.prevDocument}
                          projectDetails={projectDetails} activeDocumentTitle={this.state.activeDocumentTitle}
                          {...this.getActiveDocumentProps()} />
            </>
        )
    };


    renderProjectOverviewPlanning = () => {
        // Nav & Render Content here
        return (
            <Col span={24}>
                {this.renderContentPlanning()}
            </Col>
        )
    };

    /************************************ EVENTS END ***********************************/


    /************************************ RIGHT PANEL ***************************************/

    renderRightPanel = () => {


        return (
            <Col className={'col-md-9'} style={{
                border: '1px solid #E0E5E8',
                height: '100%',
                padding: '25px',
                backgroundColor: '#FFFFFF',
                borderRadius: '7px',
            }}>
                <Row>
                    {this.renderHeaderButtonsList()}

                </Row>
                <Row>
                    {this.renderPhaseForms()}
                </Row>
                <Row>
                    {this.renderProjectOverviewPlanning()}
                </Row>
            </Col>

        )
    };

    /************************************ RIGHT PANEL END ***********************************/


    /************************************ LEFT PANEL ***************************************/
    renderLeftPanel = () => {
        let {projectDetails, setStateOfSecondParent, currentPhase, currentStep} = this.props;

        let {projectWorkflow} = projectDetails;


        let setFirstParentState = (obj) => {
            setStateOfSecondParent(obj)
        };


        return (
            <Col className={'col-md-3'} style={{height: '1500px'}}>
                <Row style={{height: '230px', width: "100%"}}>
                    <div style={{marginLeft: '26px', marginTop: '2px', width: "100%"}}>
                        <PhaseStatusContainer getPhaseStatusData={this.getPhaseStatusData}
                                              strokeColorProgressBar={'#30BDA0'}
                                              currentStep={currentStep}/>
                    </div>
                </Row>

                <Row style={{height: '750px', display: 'block'}}>
                    <div style={{height: '50px'}}>
                        <p style={{
                            marginTop: '18px',
                            marginLeft: '50px',
                            fontFamily: 'Roboto',
                            fontWeight: 'bold',
                            fontSize: '18px'
                        }}>Phase List</p>
                    </div>
                    <Col style={{height: '700px'}}>
                        <Menu setStateOfParent={setFirstParentState}
                              activePhase={currentPhase}
                              defaultPhase={projectWorkflow.defaultPhase} groupPhases={currentStep}/>
                    </Col>
                </Row>

            </Col>
        )
    };

    /************************************* LEFT PANEL END ***********************************/




    render() {
        return (
            <>
                {this.renderMarkingModal()}
                {this.renderDocumentModal()}
                {this.renderSanctionModal()}
                {this.renderInformationModal()}

                <Row style={{width: "100%"}}>
                    <Col style={{
                        height: '1400px',
                        maxHeight: '1400px',
                    }}>
                        <Row style={{marginTop: '2px'}}>
                            {this.renderLeftPanel()}
                            {this.renderRightPanel()}
                        </Row>
                    </Col>
                </Row>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.project_reducer.processing,
        projectProcessing: state.server_reducer.processing,
        activeUser: state.user_reducer.activeUser,
    }
};


const connected = connect(mapStateToProps)(PlaningOverview);

export default connected;