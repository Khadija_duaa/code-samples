import React from 'react';

class PlaningStart extends React.Component {
    render() {
        let {projectName, currentPhase} = this.props;


        return (
            <>
                <div className="planning-design-content text-center">
                    <h3>
                        {currentPhase.description} for<br/>
                        {projectName}
                    </h3>
                    <p>
                        As per requirement, there is need to add some documents for creation
                        of {currentPhase.description} <br/>Ready to set-up documents?
                    </p>
                    <button className="custom-button custom-button-dark create-button mt-5" style={{width:"300px",color: "#fff"}} onClick={this.props.onClick}>
                        Create {currentPhase.description}
                    </button>
                </div>
            </>
        )
    }
}


export default PlaningStart;