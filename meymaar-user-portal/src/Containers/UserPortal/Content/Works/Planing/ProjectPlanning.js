import React from 'react';
import {connect} from "react-redux";
import {fetchPhaseDetails} from "../../../../../store/project/project-actions";
import {withRouter} from "react-router-dom";
import PlaningStart from './planingStart';
import PlaningOverview from "./planingOverview";
import NewLoader from '../../../../Loader/NewLoader';

class ProjectPlanning extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isVisited: false,
            processing: true,
            currentPhaseId: null,
        }
    }

    componentDidMount() {
        if (this.state.currentPhaseId) {
            this.fetchPhaseDetailsPlanning();
        }
        else {
            this.fetchPhaseDetails();
        }
    }


    fetchPhaseDetails = () => {
        let {currentPhase, projectId} = this.props;


        this.setState({processing:true},()=>{
            this.props.getPhaseDetails(projectId, currentPhase ? currentPhase.uuid : null,
                (isVisited) => {
                    this.setState({isVisited, processing: false, currentPhaseId: currentPhase.uuid});
                }, () => this.setState({processing: false}));
        })
    };


    fetchPhaseDetailsPlanning = () => {
        let {projectId} = this.props;

        this.setState({processing:true},()=>{
            this.props.getPhaseDetails(projectId, this.state.currentPhaseId,
                (isVisited) => {
                    this.setState({isVisited, processing: false});
                }, () => this.setState({processing: false}));
        })
    };


    render() {
        let {onPhaseClick} = this.props;
        let setSecondParentState = (obj) => {
            this.setState({processing: false, currentPhase:obj.currentPhase, currentPhaseId: obj.currentPhase.uuid}, () => {
                this.fetchPhaseDetailsPlanning();
                onPhaseClick && onPhaseClick(obj.currentPhase)
            })
        };


        if (this.state.processing) return <NewLoader/>;

        if (!this.props.phaseDetails) return null;

        return (
            <div style={{ marginLeft:'-24px'}}>
                <PlaningOverview currentPhase={this.state.currentPhase}  {...this.props} phaseId={this.props.currentPhase && this.props.currentPhase.uuid}
                                  projectDetails={this.props.projectDetails}
                                  setStateOfSecondParent={setSecondParentState}/>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        getPhaseDetails: (projectId, phaseId, cb, errorCB) => dispatch(fetchPhaseDetails(projectId, phaseId, cb, errorCB))
    }
};

const mapStateToProps = (state) => {
    return {
        phaseDetails: state.project_reducer.phaseDetails,
        processing:state.project_reducer.processing
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(ProjectPlanning);

export default withRouter(connected);