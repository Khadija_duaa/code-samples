import React from 'react';
import 'antd/dist/antd.css';
import {getFormInputs} from "../../../../../store/form/form_actions";
import {connect} from "react-redux";
import JsonForm from '../../../../../Components/JSONForm/JsonForm';
import BreadCrumb from '../../../../../Components/BreadCrumb/BreadCrumb';

class CreateWork extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
            dob: '',
            formErrors: {}
        }
    }

    componentWillMount() {

        const {authorities} = this.props.activeUser;
        if (!authorities.includes('CAN_CREATE_WORK')) this.props.history.push('/dashboard/works');

        let formAPI = '/forms-management/form-fields/CREATE_WORK_FORM';
        this.props.loadFormData(formAPI);

    }


    getHeader = () => {
        let columnStyle = {
            padding: "10px 0px 10px 5%",
            borderBottom: "2px solid rgba(231, 232, 237,0.5)"
        };

        let {jsonFields} = this.props;

        return (
            <div className="title-row pt-3 pb-3 pl-5" style={columnStyle}>
                <h3 className="mb-0">
                    {jsonFields.title}
                </h3>
            </div>
        )
    };


    getBody = () => {


        const handleSubmit = (formData) => {
            formData.preventDefault();
        };

        let formProps = {
            successMessage: "Project has created successfully",
            formJSON: this.props.jsonFields,
            successCB: '/dashboard/works/overview',
            isDisabled: false,
            isComplete: false,
            isCreateWork: true,
        };

        return (
            <div style={{paddingTop: "1rem", paddingBottom: "1rem", paddingLeft: "3rem"}}>
                <JsonForm projectProps={formProps} onSubmit={handleSubmit}/>
            </div>
        )

    };

    getBreadCrumbs = () => {

        let breadCrumbsJSON = [
            {
                title: "Dashboard",
                returnPath: '/dashboard'
            },
            {
                title: 'Create Work'
            }
        ];
        return (
            <BreadCrumb className={"form-crumb"} style={{fontSize: "20px"}} crumbsJSON={breadCrumbsJSON}/>
        )
    };

    render() {


        let containerStyle = {
            border: "2px solid rgba(231, 232, 237,0.5)",
            padding: "unset",
            borderRadius: "4px",
            marginTop: "10px",
            marginBottom: "10px",
            backgroundColor: "#fff",
        };


        let formData = this.props.jsonFields;

        if (!formData)
            return null;

        return (
            <div className={"col-md-12"}>
                {this.getBreadCrumbs()}
                <div className="form-container mb-5" style={containerStyle}>
                    {this.getHeader()}
                    {this.getBody()}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        jsonFields: state.form_reducer.jsonFields,
        activeUser: state.user_reducer.activeUser
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadFormData: (url) => dispatch(getFormInputs(url)),
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(CreateWork);

export default connected;