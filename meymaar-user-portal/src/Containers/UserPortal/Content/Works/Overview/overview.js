import React from 'react';
import {withRouter, NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import {fetchProjectDetails, resetRedux} from "../../../../../store/project/project-actions";
import {formatDate, sortByKeyAsc} from "../../../../../utils/common-utils";
import NewLoader from '../../../../Loader/NewLoader';
import {MODEL_URL, SERVER_URL} from "../../../../../utils/config";

class Overview extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            projectId: ""
        }
    }

    getProjectState = () => {
        const storageKey = "overview-projectId";
        let locationState = sessionStorage.getItem(storageKey);


        if (locationState) {
            locationState = JSON.parse(locationState);
        }


        const {location} = this.props;

        if (location && location.state) {
            locationState = location.state;
        }


        console.log("Project Id: ", locationState && locationState.projectId);
        sessionStorage.setItem(storageKey, JSON.stringify(locationState));
        return locationState;

    };

    componentDidMount() {

        let state = this.getProjectState();

        this.setState({
            projectId: state && state.projectId
        });


        let projectFields = {
            fields: ["projectId", "name", "divArea",
                "station", "referenceNumber",
                "tbdDate", "fy", "fy_2",
                "office", "projectWorkflow",
                "markingHistory", "currentProjectOfficeData",
                "currentProjectOffice", "sanctions",
                "peProjectCostStr", "peProjectCost",
                "gbXml",
                "exec_office", "customWorkflow"]
        };


        this.props.resetProject();
        if(state && state.projectId){
            this.props.getProjectDetails(state.projectId, projectFields);
        }

    }


    // Event to Open 3D Model
    handleModelClick = (event) => {
        event.preventDefault();
        let {activeUser} = this.props;
        let {projectId} = this.getProjectState();
        window.open(`${MODEL_URL}/?token=${activeUser.token}&projectId=${projectId}&url=${SERVER_URL}`, "_blank");
    };


    getWorkDetails = () => {

        let {category,backCB,backTitle} = this.getProjectState();
        let projectState = {
            projectWorkFlow: this.props.projectDetails.projectWorkflow,
            projectId: this.state.projectId,
            execOffice: this.props.projectDetails.exec_office,
            backCB,
            backTitle
        };


        let {projectId, name, divArea, station, referenceNumber, tbdDate, fy, office, currentProjectOfficeData, markingHistory, gbXml} = this.props.projectDetails;


        let workJSON = [
            {
                text: "Planning and Design",
                subText: "Initiation, SBP, Design, PE, CA...",
                disable: false,
                to: "/dashboard/works/planning"
            },
            {
                text: "Execution and Tracking",
                subText: "Progress & Supervisor/Contractor Activities",
                disable: category === 'Planing' || category === 'inbox',
                to: {pathname: "/dashboard/diary", state: {...projectState}}
            },
            {
                text: "Marking History",
                subText: "Resources involved, Approvals, Comments",
                disable: false,
                to: {
                    pathname: "/dashboard/works/marking-history",
                    state: {
                        projectId,
                        currentProjectOfficeData,
                        projectName: name,
                        markingHistory,
                        returnCB: "/dashboard/works/overview"
                    }
                }
            },
            {
                text: "Work Program Timeline",
                subText: "Progress, Delays & Duration of Activities",
                disable: false,
                to: {
                    pathname: "/dashboard/project/timeline",
                }
            },
            {
                text: "3D Model Viewer",
                subText: "View Revit 3D Revit GBMXL Model",
                model: true,
                disable: !gbXml,
            },
        ];


        let workDetailsJSX = workJSON.map((work, index) => {
            if (work.disable) {
                return (
                    <div key={index} className={'link-disable work-links'}>
                        <a href={"javascript:;"}>
                            <strong>{work.text}</strong>
                            <br/>
                            <p>{work.subText}</p>
                            <i className="fas  fa-angle-right"/>
                        </a>
                    </div>
                )
            } else if (work.model) {
                return (
                    <div key={index} className="work-links" style={{cursor: "pointer"}} onClick={this.handleModelClick}>
                        <a href={"javascript:void(0);"}>
                            <strong>{work.text}</strong><br/><p>{work.subText}</p>
                            <i className="fas  fa-angle-right"/>
                        </a>
                    </div>
                )
            } else {
                return (
                    <div key={index} style={{cursor: "pointer"}} onClick={() => this.props.history.push(work.to)}
                         className={`work-links`}>
                        <NavLink to={work.to}>
                            <strong>{work.text}</strong>
                            <br/>
                            <p>{work.subText}</p>
                            <i className="fas  fa-angle-right"/>
                        </NavLink>
                    </div>
                )
            }
        });

        let headingStyle = {width: "30%"};
        let textSyle = {width: "70%"};
        return (
            <div className="work-detailes">
                <h2>{name}</h2>

                <div className="row">

                    <div className="col-md-8">
                        <div className="detailz-box full-width">
                            <p style={headingStyle}>Div/Area</p>
                            <span style={textSyle}> {divArea} </span>
                        </div>

                        <div className="detailz-box full-width">
                            <p style={headingStyle}>Reference # </p>
                            <span style={textSyle}>{referenceNumber}</span>
                        </div>

                        <div className="detailz-box full-width">
                            <p style={headingStyle}>Date </p>
                            <span style={textSyle}>{formatDate(tbdDate, 'DD-MM-YYYY')}</span>
                        </div>


                        <div className="detailz-box full-width">
                            <p style={headingStyle}>Station </p>
                            <span style={textSyle}> {station} </span>
                        </div>

                        <div className="detailz-box full-width">
                            <p style={headingStyle}>FY </p>
                            <span style={textSyle}> {fy} </span>
                        </div>

                        <div className="detailz-box full-width">
                            <p style={headingStyle}>Initiating Office </p>
                            <span style={textSyle}> {office.name} </span>
                        </div>

                        <div className="detailz-box full-width no-border-bottom">
                            <p style={headingStyle}>Currently Marked To </p>
                            <span style={textSyle}> {currentProjectOfficeData.name} </span>
                        </div>
                    </div>

                    <div className="work-details-box wd-link-heading col-md-4">
                        <div id="wd-link-1">
                            <p>Work Details</p>
                        </div>

                        {workDetailsJSX}

                    </div>
                </div>
            </div>
        )
    };

    getPhaseDetails = () => {
        let {projectWorkflow} = this.props.projectDetails;

        let sortedPhases = sortByKeyAsc(projectWorkflow.phases, 'sequence');

        let phasesJSX = sortedPhases.map((item, index) => {
            return (
                <div className="phases-deails" key={index}>
                    <span>{item.phase.name}</span>
                    <div className="progress">
                        <div className="progress-bar" role="progressbar" style={{width: '100%'}}
                             aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">100%
                        </div>
                    </div>
                </div>
            )
        });


        return (
            <div className="phases">
                <h2>Phases</h2>
                <p>Currently Active Phase: <strong> {projectWorkflow.defaultPhase.name} </strong></p>
                {phasesJSX}
            </div>
        )
    };



    render() {

        if (this.props.processing) return <NewLoader/>;


        if (!this.props.projectDetails)
            return null;

        return (
            <>

                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            {this.getWorkDetails()}
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getProjectDetails: (projectId, fields) => dispatch(fetchProjectDetails(projectId, fields)),
        resetProject: () => dispatch(resetRedux('projectDetails')),
    }
};

const mapStateToProps = (state) => {
    return {
        projectDetails: state.project_reducer.projectDetails,
        editRights: state.project_reducer.editRights,
        processing: state.project_reducer.processing,
        activeUser: state.user_reducer.activeUser
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Overview);

export default withRouter(connected);