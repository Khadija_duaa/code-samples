import React from 'react';
import {Row, Col} from 'antd';

const fontFamily = { fontFamily: 'Roboto' };

class ProfileHeader extends React.Component{

    /************************ Header User Information *************************/

    _style = {
        marginLeft: '-80px',
        marginTop: '-60px',
        width: '100vw ',
        maxWidth: '1681px',
        padding: '0px',
        backgroundColor: '#F0F1F6',
    };

    getUserInfo = () => {
        let userInfo = [
            {
                label: 'Service #',
                data: this.props.activeUser.principal.armyNumber,
            },
            {
                label: 'Designation',
                data: this.props.activeUser.roles[0].name,
            },
            {
                label: 'Rank',
                data: this.props.activeUser.roles[0].name,
            },
            {
                label: 'Domain',
                data: this.props.activeUser.roles[0].office.officeAuthority.domain.name,
            },
            {
                label: 'Office Location',
                data: this.props.activeUser.roles[0].office.geoLocation.name,
            },
            {
                label: 'Station',
                data: this.props.activeUser.roles[0].office.geoLocationEntity.name,
            },

        ];
        return (
            <Row style={{margin: '8px', paddingLeft: "80px"}}>
                <Col md={12}>
                    <Row
                        style={{margin: '0px'}}>
                        {
                            userInfo && userInfo.map((data, index) => {
                                return (
                                    <Col key={index} md={8} style={{marginBottom: '50px', display: 'block'}}>
                                        <p style={{
                                            fontSize: '17px',
                                            color: '#373737',
                                            marginBottom: '0px',
                                            ...fontFamily
                                        }}>
                                            {data.label}
                                        </p>
                                        <p style={{
                                            fontSize: '24px',
                                            fontWeight: 'bolder',
                                            color: '#373737',
                                            ...fontFamily
                                        }}>
                                            {data.data}
                                        </p>
                                    </Col>
                                )
                            })
                        }
                    </Row>
                </Col>
            </Row>
        )

    };

    getUserNameTitle = (activeUser) => {

        return (
            <Row style={{
                paddingTop: '80px',
                paddingLeft: '80px',
                display: 'flex',
                margin: '8px'
            }}>
                <p style={{ ...fontFamily, fontWeight: 'bolder', fontSize: '40px', color: 'black'}}>
                    {
                        activeUser && activeUser.principal && activeUser.principal.firstName && activeUser.principal.firstName
                    }
                </p>
                &nbsp;
                &nbsp;
                &nbsp;
                &nbsp;
                <p style={{ ...fontFamily, fontWeight: 'bolder', fontSize: '40px', color: 'black'}}>
                    {
                        activeUser && activeUser.principal && activeUser.principal.lastName && activeUser.principal.lastName
                    }
                </p>
            </Row>
        )

    };


    getUserInformation = () => {
        const {activeUser} = this.props;

        return (
            <Row style={this._style}>
                <Col>

                    {this.getUserNameTitle(activeUser)}

                    {this.getUserInfo()}

                </Col>
            </Row>

        )
    };

    /************************ End *************************/

    render(){
        return(
            <div>
                {this.getUserInformation()}
            </div>
        )
    }
}

export default ProfileHeader