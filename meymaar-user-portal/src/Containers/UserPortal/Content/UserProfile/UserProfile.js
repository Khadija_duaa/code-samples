import React from 'react';
import {Row, Form} from 'antd';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import SecurityQuestions from './SecurityQuestions';
import ProfileHeader from "./ProfileHeader";


class UserProfile extends React.Component {

    render() {
        return (
            <Row>
                <ProfileHeader activeUser={this.props.activeUser}/>
                <SecurityQuestions isForgot={false}/>
            </Row>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        activeUser: state.user_reducer.activeUser,
    }
};


const User_Profile = Form.create({})(UserProfile);
const connectedComponent = connect(mapStateToProps)(User_Profile);
export default withRouter(connectedComponent);
