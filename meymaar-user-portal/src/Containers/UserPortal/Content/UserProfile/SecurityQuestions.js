import React from 'react';
import {
  loadSecurityQuestions,
  saveSecurityQuestions
} from '../../../../store/user/user-actions';
import { withRouter } from 'react-router-dom';
import { Row, Col, Form, message, Input, Button } from 'antd';
import { connect } from 'react-redux';

const fontFamily = { fontFamily: 'Roboto' };

class SecurityQuestions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      questionList: []
    };
  }

  componentDidMount() {
    this.props.loadSecurityQuestions();
  }

  /********************************* Security Question & Answers *******************************/

  submit = (e, securityQuestions) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }
      securityQuestions &&
        securityQuestions.map((ques, j) => {
          this.state.questionList.push({
            question_id: ques.id,
            answer: values.answer[j + 1]
          });
        });

      this.props.saveSecurityQuestions(this.state.questionList);
    });
  };

  getSecurityQuestionsHeading = isForgot => {
    let headerText = 'Set-up Security Questions';
    let subText = 'Please setup three security questions for account security';

    if (isForgot) {
      headerText = 'Security Questions';
      subText = 'Please answer security questions for password recovery';
    }

    return (
      <Row>
        <Col
          md={24}
          style={{ marginLeft: '80px', display: 'block', paddingTop: '80px' }}
        >
          <p
            style={{
              ...fontFamily,
              fontWeight: 'bolder',
              fontSize: '40px',
              color: 'black',
              marginBottom: '0px'
            }}
          >
            {headerText}
          </p>
          <p style={{ ...fontFamily, fontSize: '17px', color: '#373737' }}>
            {subText}
          </p>
        </Col>
      </Row>
    );
  };

  getSecurityQuestions = (
    securityQuestions,
    getFieldDecorator,
    activeUser,
    isForgot
  ) => {
    return (
      <div>
        {securityQuestions &&
          securityQuestions.map((question, i) => {
            return (
              <div key={i} style={{ display: 'block', marginBottom: '50px' }}>
                <p
                  style={{
                    ...fontFamily,
                    fontSize: '19px',
                    fontWeight: 'bolder',
                    color: '#373737',
                    marginLeft: '17px',
                    marginBottom: '0.7em'
                  }}
                >
                  Question {question.id}
                </p>
                <p
                  style={{
                    ...fontFamily,
                    fontSize: '21px',
                    fontWeight: 'bolder',
                    color: '#373737',
                    marginLeft: '17px',
                    marginBottom: '0.7em'
                  }}
                >
                  {question.question}
                </p>

                <Form.Item style={{ marginLeft: '17px' }}>
                  {getFieldDecorator(`answer[${question.id}]`, {
                    initialValue:
                      !isForgot &&
                      activeUser &&
                      activeUser.principal.answers[i] &&
                      activeUser.principal.answers[i].answer,
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [
                      {
                        required: true,
                        message: 'Please enter answer!'
                      }
                    ]
                  })(
                    <Input
                      placeholder="Please type answer"
                      style={{ width: 1000, height: '45px' }}
                    />
                  )}
                </Form.Item>
              </div>
            );
          })}
      </div>
    );
  };

  getSubmitButton = isForgot => {
    const { securityQuestions } = this.props;
    return (
      <Form.Item style={{ marginLeft: '17px' }}>
        <Row type="flex" className="mt-10">
          <div className="edit-black-buttons">
            <Button
              type="primary"
              value="large"
              htmlType="submit"
              disabled={securityQuestions.length === 0 ? true : false}
            >
              <div style={{ color: 'white' }}>
                {isForgot ? 'Submit' : 'Save Changes'}
              </div>
            </Button>
          </div>
        </Row>
      </Form.Item>
    );
  };

  renderSecurityQuestionsPanel = () => {
    const { activeUser, securityQuestions, isForgot } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <Row
        style={{
          marginLeft: '-80px',
          padding: '0px'
        }}
      >
        <Col>
          {this.getSecurityQuestionsHeading(isForgot)}
          <Row>
            <Col
              style={{
                marginLeft: '80px',
                display: 'block',
                paddingTop: '35px'
              }}
            >
              <Form onSubmit={e => this.submit(e, securityQuestions)}>
                {this.getSecurityQuestions(
                  securityQuestions,
                  getFieldDecorator,
                  activeUser,
                  isForgot
                )}
                {this.getSubmitButton(isForgot)}
              </Form>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  };

  success = msg => {
    message.success(msg);
  };

  errors = msg => {
    message.error(msg);
  };

  /****************************** Security Question & Answers  ----- End *******************************/

  render() {
    return <div>{this.renderSecurityQuestionsPanel()}</div>;
  }
}

const mapStateToProps = state => {
  return {
    activeUser: state.user_reducer.activeUser,
    securityQuestions: state.user_reducer.securityQuestions,
    message: state.user_reducer.message
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadSecurityQuestions: () => dispatch(loadSecurityQuestions()),
    saveSecurityQuestions: data => dispatch(saveSecurityQuestions(data))
  };
};
const formCreated = Form.create({})(SecurityQuestions);

const connectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(formCreated);
export default withRouter(connectedComponent);
