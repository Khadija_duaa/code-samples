import React from 'react';
import { Row, Col, Table, Button,  Modal, Form, Select, Input, Popconfirm,   message} from 'antd';
import axio from '../../../../utils/axios';
import NewLoader from '../../../Loader/NewLoader';
import {NOTIFICATION_TIME} from "../../../../utils/common-utils";

const { Option } = Select;

message.config({
    top:70,
    duration:NOTIFICATION_TIME,
    maxCount:3
});

class AssignGeoLocationToDesvice extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            // id:this.props.id, enable this when id is dynamic
            // disable this when id is dynaic
            id: 11,
            devicesGeoLocations:[],
            projectMappingIdAdd: this.props.projectMappingIdAdd,
            isLoading:true,
        }
    }
    success = (msg) => {
        message.success(msg);
        // setTimeout(()=>{
        //         window.location.reload();
        //     }, 1000);
    };
    error = (msg) => {
        message.error(msg);
    };
    componentDidMount=()=>{
        this.assignGeoLocations();
    };
    assignGeoLocations=()=>{
        axio.get(`/device-management/device-detail/${this.props.deviceIds.id}`,)
        .then(res=> {
            console.log('devicesGeoLocations did mount', res);
            this.setState({
                isLoading:false,
                devicesGeoLocations: res.data.data.geoLocations,
            })
        })
        .catch(error=> {
        console.log('error', error);
        });
    };
    assignUserToDesviceModal = () => {
        this.setState({
            visible: true
        });
    };
    // handleDeviceLocationAssignOnChange=(addLocationStatus)=>{
    //     console.log('selected values of form for add device: ', addLocationStatus);
    // };
    handleSubmit = (e) => {
        e.preventDefault();
            this.props.form.validateFields((err, values) => {
              if (!err) {
                console.log('assign location to device: ', values);
                axio.post('/device-management/assign-device-geolocation', {
                        deviceProjectMapping: this.props.this_projectMappingId,
                        latitude: parseFloat(values.latitudeAdd),
                        longitude: parseFloat(values.longitudeAdd),
                        radius: parseFloat(values.radiusAdd),
                        active: parseFloat(values.activatedDevice),
                  }).then(res=> {
                    if(res.status===200){
                        this.success('Geolocation succesfully added!');
                        this.assignGeoLocations();
                        this.handleCancel()
                    }else if(res.status===400){
                        this.success('Geo location already exists!');
                    }
                  })
                  .catch(error=> {
                        this.error(error.message);
                        console.log('error', error);
                  });
              }
            });
    };
    // handleOnChange=(value)=>{
    //     // e.preventDefault();
    //     console.log('selected values of form: ', value);
    //     this.setState({
    //         selectedProject: value
    //     })
    // }

    showModal = () => {
            this.setState({
                visible: true
            });
    };
    handleOk = () => {
        this.setState({
            visible: false,
        });
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    deleteLocation=(record)=>{
        console.log('deleteLocation', record);
            // e.preventDefault();
            // var deleteId = b;
            axio.delete(`/device-management/delete-device-geolocation/${record}`)
            .then(res=> {
                axio.get(`/device-management/device-detail/${this.props.deviceIds.id}`,)
                .then(res=> {
                    this.setState({
                        devicesGeoLocations: res.data.data.geoLocations,
                    });
                    if(res.status===200){
                        this.success('Geolocation succesfully delete!');
                        this.assignGeoLocations();
                        this.handleCancel()
                    }else if(res.status===400){
                        this.success('Geolocation not deleted!');
                    }
                })
                .catch(error=> {
                console.log('error', error);
                this.error('Geolocation not deleted!');
                });
              })
    };
    
    render(){ 
        const columns = [{
            title: 'Latitude',
            dataIndex: 'latitude',
            name: 'Latitude',
            width: 300,
        },{
            title: 'Longitude',
            dataIndex: 'longitude',
            name: 'Longitude', 
            width: 300,
        },{
            title: 'Radius',
            dataIndex: 'radius',
            name: 'Radius',
            width: 100,
        },{
            title: 'Status',
            dataIndex: 'status',
            name: 'Status',
            width: 100,
        },
        {
            title: 'Action', 
            dataIndex: 'id', 
            // key: 'x', 
            width: 100,
            render: (text,record) =>  <Popconfirm placement="rightTop" title="Sure to delete?" className="delete-confirmation-popup" onConfirm={() => this.deleteLocation(record.key)} okText="Yes" cancelText="No">
                                            {/* <a href="javascript:;" className="delete-text" onClick={()=>this.deleteLocation(record)}>Delete</a> */}
                                            <a href="javascript:;" className="delete-text" >Delete</a>
                                      </Popconfirm>,
        },
        ];
        const data = this.state.devicesGeoLocations.map((data)=> {
            var status = data.activated===1? "true" : "false";
            return {
                key: data.id,
                longitude:data.longitude,
                latitude:data.latitude,
                radius:data.radius,
                status:status,
            }
        });
        const { getFieldDecorator } = this.props.form;  
        return(
                <>

                    <button style={{width:"20%", marginLeft :'18px'}} className="custom-button custom-button-dark create-button margin-60"
                            onClick={() => this.assignUserToDesviceModal(true)}>+ Assign Location to Device
                    </button>

                    {this.state.isLoading === false ?
                        <>
                            <Table
                                columns={columns}
                                dataSource={data}
                                className="margin-left-right new-table-design delete-last"
                                pagination={{pageSize: 5}}
                                size="large"
                            />
                        </>
                        :
                        <Row type="flex" justify="space-around" align="middle">
                            <Col value={100}>
                            <NewLoader/>
                        </Col>
                    </Row>
                    }
                   
                           
                        {/* assign geolocation to device */}
                        <Modal
                            title="Assign Location"
                            visible={this.state.visible}
                            className="custom-table-popup"
                            onOk={this.handleOk}
                            onCancel={this.handleCancel}
                            destroyOnClose={true}
                            maskClosable={false}
                            footer={false}
                            centered
                        >
                        
                         <Form onSubmit={this.handleSubmit}>
                                <Row className="custom-popup-row">
                                    
                                    <Col span={24} offset={4} className="custom-form-wrap">
                                        <Col span={16}>
                                            <Form.Item label="Latitude">
                                                {getFieldDecorator('latitudeAdd', {
                                                    rules: [{ required:true, message: 'Please enter Latitude!' }],
                                                    })(<Input />)}
                                            </Form.Item>

                                            <Form.Item label="Longitude">
                                                {getFieldDecorator('longitudeAdd', {
                                                    rules: [{ required:true, message: 'Please enter Longitude!' }],
                                                    })(<Input />)}
                                            </Form.Item>

                                            <Form.Item>
                                                {getFieldDecorator('projectMappingIdAdd', {
                                                    value: this.props.this_projectMappingId,
                                                    // rules: [{ required:true, message: 'Please enter Longitude!' }],
                                                    })(<Input hidden/>)}
                                            </Form.Item>

                                            <Form.Item label="Radius">
                                            {getFieldDecorator('radiusAdd', {
                                                rules: [{ required:true, message: 'Please enter Radius!', placeholder: 'Kilometre'}],
                                                })(<Input placeholder='metre'/>)}
                                            </Form.Item>

                                            <Form.Item label="Status">
                                                {getFieldDecorator('activatedDevice', {
                                                    rules: [{ required:true, message: 'Please select status!' }],
                                                })(
                                                    <Select value="Device" placeholder="Select status of device...">
                                                        <Option key="1" value="1">Active</Option>
                                                        <Option key="0" value="0">Deactive</Option> 
                                                    </Select>
                                                )}
                                            </Form.Item>
                                            <Form.Item >
                                                <Row type="flex" className="margin-top-40">
                                                        <Col span={12} className="edit-black-buttons float-right">
                                                            <Button type="primary" value="large" htmlType="submit">
                                                                Assign
                                                            </Button>
                                                        </Col>
                                                        <Col span={12} className="unfill-edit-black-buttons float-left">
                                                            <Button type="primary" value="large" onClick={this.handleCancel}>Cancel</Button>
                                                        </Col>
                                                </Row>
                                            </Form.Item>
                                        </Col>
                                    </Col>
                                </Row>
                                
                                </Form>
                        </Modal>
                        {/* assign geolocation to device */}
                </>
        )
    }
}


const AssignLocationDevice = Form.create({})(AssignGeoLocationToDesvice);
export default AssignLocationDevice;