import React from 'react';
import { Row, Col, Table, Button, Anchor, Modal, Form, Select, InputNumber, Input, Checkbox, Icon, message} from 'antd';
import axio from '../../../../utils/axios';
import {NOTIFICATION_TIME} from "../../../../utils/common-utils";
message.config({
    top:70,
    duration:NOTIFICATION_TIME,
    maxCount:3
});

class EditDeviceBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible2:false,
            devicesDataSet:this.props.devicesDataSet,
        }
    }

    editModal=()=>{
        this.setState({
            visible2: true
        });
    }
    edithandleOk = (e) => {
        this.setState({
            visible2: false,
        });
    }

    edithandleCancel = (e) => {
        this.setState({
            visible2: false,
        });
    }
    // componentWillReceiveProps(nextProps){
    //     this.setState({
    //         devicesDataSet:nextProps
    //     })
    // }
    success = () => {
        message.success('Device Updated successfull!');
        // setTimeout(()=>{
        //         window.location.reload();
        //     }, 1000);
    }
    error = () => {
        message.error('Device not updated!');
    }
    editHandleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
              this.setState({
                devicesDataSet:values,
              })
            console.log('update device values after submit---++===.....', values)
            axio.put('/device-management/update-device',{
                id:values.id,
                deviceId: values.deviceId,
                ram: values.ramAdd,
                model: values.modelAdd,
                active: 1,
                mac: values.macaAdd,
                storage: values.storageAdd,
                screen: values.screenAdd
            })
            .catch(error=> {
                console.log('error', error);
                this.error();
            });
            this.success();
            this.edithandleCancel();
        }
    });
    this.props.devicesApiData();
    }
    
 render(){
    console.log('edit device data 666666666666666666', this.props.devicesDataSet)
    
    const { getFieldDecorator } = this.props.form;  
    return(
        <>
            <Button type="primary" value="large"  onClick={this.editModal}>Edit</Button>
            <Modal
                title="Edit Device"
                visible={this.state.visible2}
                className="custom-table-popup"
                onOk={this.edithandleOk}
                onCancel={this.edithandleCancel}
                destroyOnClose={true}
                footer={false}
            >
            <Form onSubmit={this.editHandleSubmit}>
                            <Row className="custom-popup-row">
                                {/* <p className="center">Please fill the following the input fields</p> */}
                                <Col span={24} offset={4} className="custom-form-wrap">
                                    <Col span={16}>
                                        <Form.Item>
                                            {getFieldDecorator('id', {
                                                initialValue: this.state.devicesDataSet.id
                                                })(<Input hidden/>)}
                                        </Form.Item>
                                        <Form.Item label="Device Id">
                                            {getFieldDecorator('deviceId', {
                                                initialValue: this.state.devicesDataSet.device_id
                                                })(<Input />)}
                                        </Form.Item>

                                        <Form.Item label="RAM" className="custom-ant-input-number">
                                            {getFieldDecorator('ramAdd', { 
                                                initialValue: this.state.devicesDataSet.ram
                                                })(<InputNumber min={1} />)}
                                        </Form.Item>

                                        <Form.Item label="Model">
                                            {getFieldDecorator('modelAdd', {
                                                initialValue: this.state.devicesDataSet.model
                                                })(<Input />)}
                                        </Form.Item>

                                        <Form.Item label="MAC Address">
                                            {getFieldDecorator('macAdd', {
                                                initialValue: this.state.devicesDataSet.mac
                                                })(<Input />)}
                                        </Form.Item>

                                        <Form.Item label="Storage">
                                            {getFieldDecorator('storageAdd', {
                                                initialValue: this.state.devicesDataSet.storage
                                                })(<Input />)}
                                        </Form.Item>

                                        <Form.Item label="Screen Size">
                                            {getFieldDecorator('screenAdd', {
                                                initialValue: this.state.devicesDataSet.screen
                                                })(<Input />)}
                                        </Form.Item>

                                        <Form.Item >
                                            <Row type="flex" className="margin-top-40">
                                                    <Col span={12} className="edit-black-buttons float-right">
                                                        <Button type="primary" value="large" htmlType="submit">
                                                        Update
                                                        </Button>
                                                    </Col>
                                                    <Col span={12} className="unfill-edit-black-buttons float-left">
                                                        <Button type="primary" value="large" onClick={this.edithandleCancel}>Cancel</Button>
                                                    </Col>
                                            </Row>
                                        </Form.Item>
                                    </Col>
                                </Col>
                            </Row>
                        </Form>
            </Modal>
        </>
        )
        }
}

const EditDevice = Form.create({})(EditDeviceBtn);
export default EditDevice;