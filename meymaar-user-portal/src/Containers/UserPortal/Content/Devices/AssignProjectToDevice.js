import React from 'react';
import { Row, Col, Table, Button, Anchor, Modal, Form, Select, Input, Checkbox, Icon} from 'antd';
import axio from '../../../../utils/axios';

const { Option } = Select;

class AssignProjectToDevice extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            // id:this.props.id, enable this when id is dynamic
            // disable this when id is dynaic
            id: 11,
            devicesUsers:[],
            devicesGeoLocations:[],
            visible: false,
        }
    }
    componentDidMount=()=>{
        // get devices deatils and geolocations deatils by id at endpoint
        axio.get('/device-management/device-detail/11',)
        .then(res=> {
            this.setState({
                devicesUsers: res.data.data.users,
                devicesGeoLocations: res.data.data.geoLocations,
            })
        })
        .catch(error=> {
        console.log('error', error);
        });
        // get devices deatils and geolocations deatils by id at endpoint
    }
    assignUserToDesviceModal = () => {
        this.setState({
            visible: true
        });
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            axio.post('/device-management/assign-device-project',{
                deviceId: values.deviceId,
                projectId: values.projectId,
            })
            .catch(error=> {
                console.log('error', error);
            });
            console.log('Received values of form: ', values);
          }
        });

    }
    handleOnChange=(value)=>{
        // e.preventDefault();
        console.log('selected values of form: ', value);
        this.setState({
            selectedProject: value
        })
    }

    showModal = () => {
            this.setState({
                visible: true
            });
    }
    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e) => {
        this.setState({
            visible: true,
        });
    }

    render(){
        console.log('on click get record in sub component state user id 0', this.state.devicesUsers);
        console.log('this.props.id', this.props.id)
        console.log('this.props.device_id', this.props.device_id)
        const { getFieldDecorator } = this.props.form;
        return(
                <>
                   {/* <p>{this.state.id}</p> */}
                   {this.state.devicesUsers.map((users, i)=>{
                       return(
                            <>
                            <div className="col-md-12" key={users.name}>
                                <div className="row">
                                    <div  className=" location-details col-md-3">
                                        <p>Contarctor<br/>
                                        <strong> {users.name} </strong></p>
                                    </div>

                                    <div key={users.role} className=" location-details col-md-3">
                                        <p>Field Supervisor<br/>
                                        <strong>{users.role}</strong>
                                        </p>
                                    </div>
{/*
                                    <div className=" location-details col-md-12">
                                            <p className="assign-user-link pointer" onClick={this.assignUserToDesviceModal}>+ Assign User to Device</p>
                                    </div> */}
                                </div>
                            </div>

                            </>
                       )
                   })}
                   <div className=" location-details col-md-12">
                            <p className="assign-user-new-btn pointer" onClick={this.assignUserToDesviceModal}>+ Assign Users to Device</p>
                    </div>

                                {/* assign user to device */}
                                <Modal
                                title="Assign a Project"
                                visible={this.state.visible}
                                className="custom-table-popup"
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                                destroyOnClose={true}
                                footer={false}
                                maskClosable={false}
                                >
                                <Form onSubmit={this.handleSubmit}>
                                        <Row className="custom-popup-row">
                                            {/* <p className="center">Please fill the following the input fields</p> */}
                                            <Col span={24} offset={4} className="custom-form-wrap">
                                                <Col span={16}>
                                                    <Form.Item label="Device Id">
                                                        {getFieldDecorator('deviceId', {
                                                            initialValue: this.props.device_id
                                                            })(<Input disabled/>)}
                                                    </Form.Item>

                                                    <Form.Item label="Project Name">
                                                        {getFieldDecorator('projectId', {
                                                            rules: [{ required:true, message: 'Please select your project!' }],
                                                        })(
                                                            <Select value="this.state.devicesDataSet.device_id" placeholder="Select a project to assign device..." onChange={(value, event) => this.handleOnChange(value, event)}>
                                                                <Option value="project6">Project Name 1</Option>
                                                                <Option value="project6">Project Name 2</Option>
                                                            </Select>
                                                        )}
                                                    </Form.Item>

                                                    <Form.Item label="Project Id">
                                                        <Input value={this.state.selectedProject} disabled/>
                                                    </Form.Item>

                                                    <Form.Item >
                                                        <Row type="flex" className="margin-top-40">
                                                                <Col span={12} className="edit-black-buttons float-right">
                                                                    <Button type="primary" value="large" htmlType="submit">
                                                                        Add
                                                                    </Button>
                                                                </Col>
                                                                <Col span={12} className="unfill-edit-black-buttons float-left">
                                                                    <Button type="primary" value="large" onClick={this.handleCancel}>Cancel</Button>
                                                                </Col>
                                                        </Row>
                                                    </Form.Item>
                                                </Col>
                                            </Col>
                                        </Row>
                                        </Form>
                                </Modal>
                            {/* assign user to device */}
                </>
        )
    }
}


const AssignProjectDevice = Form.create({})(AssignProjectToDevice);
export default AssignProjectDevice;