import React from 'react';
import {Row, Col, Table, Button, Modal, Popconfirm, Form, Select, Input, message} from 'antd';
import axio from '../../../../utils/axios';
import NewLoader from '../../../Loader/NewLoader';
import {handleError} from "../../../../store/store-utils";
const _ = require('lodash');

const {Option} = Select;

class AssignUserToDevice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            devicesUsers: [],
            deviceRegisterUsers: [],
            isLoading: true,
        }
    }

    componentDidMount = () => {
        this.fetchDeviceDetails();
        this.contractorByProject();
    };

    fetchDeviceDetails = () => {
        axio.get(`/device-management/device-detail/${this.props.deviceIds.id}`,)
            .then(res => {
                this.setState({
                    isLoading: false,
                    devicesUsers: res.data.data.users,
                })
            })
            .catch(error => {
                // this.setState({isLoading:false});
                console.log('error', error);
                let errorMessage = handleError(error);
                this.error(errorMessage);
            });
    };
    contractorByProject = () => {
        axio.get(`/contractor-management/contractor/getByProject/${this.props.devicesDataSet.project.projectId}`,)
            .then(res => {

                this.setState({
                    deviceRegisterUsers: res.data.contractor,
                })
            })
            .catch(error => {
                // this.setState({isLoading:false});
                let errorMessage = handleError(error);
                this.error(errorMessage);
            });
    };
    toggleAssignUserModal = () => {
        this.setState({
            visible: true
        });
    };

    getUserRolesObject = (selectedUserIds, proMappingId) => {
        let {deviceRegisterUsers} = this.state;
        let userIds = [...selectedUserIds];
        userIds.forEach((userId, index) => {
            let userIndex = deviceRegisterUsers.findIndex(obj => obj.id === userId);
            userIds[index] = {
                user_id: deviceRegisterUsers[userIndex].id,
                user_role: deviceRegisterUsers[userIndex].role,
                project_mapping_id: proMappingId
            }
        });

        return userIds;
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                let userIds = this.getUserRolesObject(values.userId, values.promappingId);
                axio.post('/device-management/assign-user-device', {
                    projectMappingId: values.promappingId,
                    userIds
                })
                    .then(res => {
                        if (res.status === 200) {
                            this.success('User succesfully added!');
                            this.fetchDeviceDetails();
                        } else if (res.status === 400) {
                            this.error('User already exists!');
                        }
                    })
                    .catch(error => {
                        console.log('error', error);
                    });
            }
        });
        this.handleCancel();
    };


    showModal = () => {
        this.setState({
            visible: true
        });
    };
    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };
    success = (msg) => {
        message.success(msg);
    };
    error = (msg) => {
        message.error(msg)
    };

    deleteLocation = (record) => {
        axio.delete(`/device-management/delete-user-device/${record}`)
            .then(res => {
                if (res.status === 200) {
                    this.success('User successfully deleted!');
                    this.fetchDeviceDetails();
                } else if (res.status === 400) {
                }
            })
            .catch(error => {
                console.log('error', error);
            })

    };

    // Assign User Modal
    renderAssignUserModal = () => {
        const {getFieldDecorator} = this.props.form;
        let {deviceRegisterUsers,devicesUsers} = this.state;

        return (
            <Modal
                title="Assign User to Device"
                visible={this.state.visible}
                className="custom-table-popup"
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                destroyOnClose={true}
                maskClosable={false}
                footer={false}
                centered
            >
                <Form onSubmit={this.handleSubmit}>
                    <Row className="custom-popup-row">
                        {/* <p className="center">Please fill the following the input fields</p> */}
                        <Col span={24} offset={4} className="custom-form-wrap">
                            <Col span={16}>

                                {/* for testing is project maping id true */}
                                <Form.Item>
                                    {getFieldDecorator('promappingId', {
                                        initialValue: this.props.deviceIds.projectMappingId
                                    })(<Input type="hidden"/>)}
                                </Form.Item>

                                <Form.Item label="Select User">
                                    {getFieldDecorator('userId', {
                                        rules: [{required: true, message: 'Please select your project!'}],
                                    })(
                                        <Select mode="multiple" placeholder="Select user to assign device...">
                                            {deviceRegisterUsers.map((user) => {
                                                    return (
                                                        <Option key={user.id}
                                                                value={user.id}>{user.first_name} {user.last_name}</Option>
                                                    )
                                                }
                                            )}
                                        </Select>
                                    )}
                                </Form.Item>

                                <Form.Item>
                                    <Row type="flex" className="margin-top-40">
                                        <Col span={12} className="edit-black-buttons float-right">
                                            <Button type="primary" value="large" htmlType="submit">
                                                Assign
                                            </Button>
                                        </Col>
                                        <Col span={12} className="unfill-edit-black-buttons float-left">
                                            <Button type="primary" value="large"
                                                    onClick={this.handleCancel}>Cancel</Button>
                                        </Col>
                                    </Row>
                                </Form.Item>
                            </Col>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        )
    };

    render() {

        const columns = [{
            title: 'Name',
            dataIndex: 'user_name',
            name: 'Name',
            width: 200,
        }, {
            title: 'Role',
            dataIndex: 'user_role',
            name: 'Role',
            width: 200,
        },
            {
                title: 'Action',
                dataIndex: 'id',
                // key: 'x',
                width: 100,
                render: (text, record) => <Popconfirm placement="rightTop" title="Sure to unassign?"
                                                      className="delete-confirmation-popup"
                                                      onConfirm={() => this.deleteLocation(record.key)} okText="Yes"
                                                      cancelText="No">
                    {/* <a href="javascript:;" className="delete-text" onClick={()=>this.deleteLocation(record)}>Delete</a> */}
                    <a href="javascript:;" className="delete-text">Unassign</a>
                </Popconfirm>,

            },
        ];
        const data = this.state.devicesUsers.map((data) => {
            return {
                key: data.id,
                user_name: data.name,
                user_role: data.role,
            }
        });

        return (
            <>

                {this.renderAssignUserModal()}
                <button className="custom-button custom-button-dark create-button margin-60"
                        style={{marginLeft: '18px'}}
                        onClick={this.toggleAssignUserModal}>+ Assign User to Device
                </button>
                {this.state.isLoading === false ?
                    <>
                        <Table
                            columns={columns}
                            dataSource={data}
                            className="margin-left-right new-table-design delete-last"
                            pagination={{pageSize: 5}}
                            size="large"
                        />
                    </>
                    :
                    <Row type="flex" justify="space-around" align="middle">
                        <Col value={100}>
                            <NewLoader/>
                        </Col>
                    </Row>
                }


                {/* assign user to device */}
            </>
        )
    }
}


const AssignUserDevice = Form.create({})(AssignUserToDevice);
export default AssignUserDevice;