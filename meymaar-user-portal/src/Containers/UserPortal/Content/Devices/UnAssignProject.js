import React from 'react';
import { Row, Col, Table, Button, Anchor, Modal, Form, Select, InputNumber, Input, } from 'antd';
import axio from '../../../../utils/axios';

class UnAssignProjectBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible:false,
        }
    }
    showModal=()=>{
        this.setState({
            visible: true
        });
    }
    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    }
    unAssign = (e) => {
            var device_id = this.props.devicesDataSet.device_id;
            var project_id = this.props.devicesDataSet.project.projectId;
            
            axio.delete(`/device-management/remove-device-project/${project_id}/${device_id}`)
            .then(res=>{
                this.props.devicesApiData()
            })
            .catch(error=> {
                console.log('error', error);
            });
    }
    
 render(){ 
    const { getFieldDecorator } = this.props.form;  
    return(
        <>
            <Button type="primary" value="large"  onClick={this.unAssign}>UnAssignProject</Button>
            {/* <Modal
                title="Edit Device"
                visible={this.state.visible}
                className="custom-table-popup"
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                destroyOnClose={true}
                footer={false}
            >
            <Form onSubmit={this.handleSubmit}>
                            <Row className="custom-popup-row">
                                <p className="center">Please fill the following the input fields</p>
                                <Col span={24} offset={4} className="custom-form-wrap">
                                    <Col span={16}>
                                        <Form.Item>
                                            {getFieldDecorator('id', {
                                                initialValue: this.props.devicesDataSet.id
                                                })(<Input hidden/>)}
                                        </Form.Item>
                                        <Form.Item label="Device Id">
                                            {getFieldDecorator('deviceId', {
                                                initialValue: this.props.devicesDataSet.device_id
                                                })(<Input />)}
                                        </Form.Item>

                                        <Form.Item label="RAM" className="custom-ant-input-number">
                                            {getFieldDecorator('ramAdd', { 
                                                initialValue: this.props.devicesDataSet.ram
                                                })(<InputNumber min={1} />)}
                                        </Form.Item>

                                        <Form.Item label="Model">
                                            {getFieldDecorator('modelAdd', {
                                                initialValue: this.props.devicesDataSet.model
                                                })(<Input />)}
                                        </Form.Item>

                                        <Form.Item label="MAC Address">
                                            {getFieldDecorator('macAdd', {
                                                initialValue: this.props.devicesDataSet.mac
                                                })(<Input />)}
                                        </Form.Item>

                                        <Form.Item label="Storage">
                                            {getFieldDecorator('storageAdd', {
                                                initialValue: this.props.devicesDataSet.storage
                                                })(<Input />)}
                                        </Form.Item>

                                        <Form.Item label="Screen Size">
                                            {getFieldDecorator('screenAdd', {
                                                initialValue: this.props.devicesDataSet.screen
                                                })(<Input />)}
                                        </Form.Item>

                                        <Form.Item >
                                            <Row type="flex" className="margin-top-40">
                                                    <Col span={12} className="edit-black-buttons float-right">
                                                        <Button type="primary" value="large" htmlType="submit">
                                                        Update
                                                        </Button>
                                                    </Col>
                                                    <Col span={12} className="unfill-edit-black-buttons float-left">
                                                        <Button type="primary" value="large" onClick={this.handleCancel}>Cancel</Button>
                                                    </Col>
                                            </Row>
                                        </Form.Item>
                                    </Col>
                                </Col>
                            </Row>
                        </Form>
            </Modal> */}
        </>
        )
        }
}

const UnAssignProject = Form.create({})(UnAssignProjectBtn);
export default UnAssignProject;