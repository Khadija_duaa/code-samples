import React from 'react';
import { Row, Col, Table, Button,  Breadcrumb, Pagination,  InputNumber, Modal, Form, message,  Input,  Icon} from 'antd';
import axio from '../../../../utils/axios';
import AssignUserToDevice from './AssignUserToDevice';
import AssignLocationDevice from './AssignGeoLocationToDevice';
import AssignProject from './AssignProject';
import { Chart, Geom, Axis, Label, } from "bizcharts";
import NewLoader from '../../../Loader/NewLoader';



class Devices extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // firmData:[],
            showDetailsBox:false,
            assignLocationModal:false,
            showHide:true,
            firmsId:[],
            checkNick: false,
            // searchText: '',
            devicesData:[],
            detailsDevice:[],
            devices_Data2:[],
            pro_ject:{},
            the_firms:{},
            record:[],
            devicesDataSet:[],
            firmEmail:'',
            projectName : '',
            projectId : '',
            firmName : '',
            firmId : '',
            firmContact : '',
            rame: '',
            storage: '',
            screen: '',
            conatct: '',
            model:'',
            projectMappingIds:'',
            id:'',
            selectDeviceIdForProject:'ashdausdhisaud',
            devicesAndGeoLocationsData: [],
            selectedProject:'',
            assignDevice:'',
            deviceIds:'',
            totalDevices:'',
            assignedDevices:'',
            resStatus:'',
            error_device_id:true,
            unAssignComponnet:false,
            unAssignComponnetBtn:true,
            visible2:false,
            isAssign:false,
            this_projectMappingId:'',
            devicesRes:[],
            showButtons:false,
            new_project_maping_id:'',
            current:false,
            total_pages:'',
            isLoaded:false,
            filteredData : []
        }
    }

    pageChange = (page) => {
        this.setState({
            current: page,
        });
        this.devicesApiData(page)
    };

    filterTable=(e) => {
        console.log('e', e.target.value);
        
            let searcjQery = e.target.value.toLowerCase(),
            displayedContacts = this.state.devicesData.filter((el) => {
                console.log('el', el);
                let searchValue = el.device_id.toLowerCase();
                console.log('displayedContacts', searchValue.indexOf(searcjQery));
                return searchValue.indexOf(searcjQery) !== -1;
            });
            console.log('displayedContacts', displayedContacts);
            this.setState({
                filteredData: displayedContacts
            })
    };
    assignProjectFunc=()=>{
        this.setState({
            this_projectMappingId:!this.state.this_projectMappingId,
        })
    };
    getColumnSearchProps = (name) => ({
        filterDropdown: ({
          setSelectedKeys, selectedKeys, confirm, clearFilters,
        }) => (
          <div style={{ padding: 8, zoom: '1.3', display: 'inline-flex'}}>
            <Input
              ref={node => { this.searchInput = node; }}
              placeholder={`Search ${name}`}
              value={selectedKeys[0]}
              className="custom-new-search"
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
              style={{ width: 188, marginBottom: 0, marginRight: 8, display: 'block' }}
            />
            <Button
              type="primary"
              className="custom-new-search-button"
              onClick={() => this.handleSearch(selectedKeys, confirm)}
              icon="search"
              style={{ width: 40, marginRight: 8 }}
            >
            </Button>
            <Button
              onClick={() => this.handleReset(clearFilters)}
              icon="reload"
              style={{ width: 40 }}
            >
            </Button>
          </div>
        ),
        filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) => record[name].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: (visible) => {
          if (visible) {
            setTimeout(() => this.searchInput.select());
          }
        },
      });
    
      handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({ searchText: selectedKeys[0] });
      };
    
      handleReset = (clearFilters) => {
        clearFilters();
        this.setState({ searchText: '' });
      };
    
      success = (msg) => {
        message.success(msg, 10);
      };
      error = (msg) => {
        message.error(msg, 10);
      };

    //add new device
                showModal=()=>{
                    this.setState({
                        visible: true
                    });
                };
                handleOk = (e) => {
                    this.setState({
                        visible: false,
                    });
                };

                handleCancel = (e) => {
                    this.setState({
                        visible: false,
                    });
                };
                handleSubmit = (e) => {
                    e.preventDefault();
                    this.props.form.validateFields((err, values) => {
                      if (!err) {
                        axio.post('/device-management/register-device/',{
                            deviceId: values.deviceId,
                            ram: values.ramAdd,
                            model: values.modelAdd,
                            active: 1,
                            mac: values.macAdd,
                            storage: values.storageAdd,
                            screen: values.screenAdd
                        }).then(res=>{

                                this.success('Device Added Successfully!');
                                this.handleCancel();
                                this.devicesApiData(1);
                        })
                        .catch(error=> {
                            this.setState({
                                error_device_id: true,
                            });
                            this.error("Unable to register device. Device ID already exists!");
                            console.log('error', error);
                        });
                        // window.location.reload();
                        // console.log('new device add ', values);
                        this.devicesApiData();
                      }
                    });
                    
                };
    //add new device

    //edit model
    editModal=()=>{
        this.setState({
            visible2: true
        });
    }
    edithandleOk = (e) => {
        this.setState({
            visible2: false,
        });
    }

    edithandleCancel = (e) => {
        this.setState({
            visible2: false,
        });
    }

    editHandleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            this.setState({
                this_device_id:values.deviceId,
                this_ram: values.ramAdd,
                this_model: values.modelAdd,
                // this_active: values.active,
                this_mac: values.macAdd,
                this_storage: values.storageAdd,
                this_screen: values.screenAdd,
              })
            axio.put('/device-management/update-device',{
                id:values.id,
                deviceId: values.deviceId,
                ram: values.ramAdd,
                model: values.modelAdd,
                active: 1,
                mac: values.macAdd,
                storage: values.storageAdd,
                screen: values.screenAdd
            })
            .catch(error=> {
                console.log('error', error);
                this.error(error.message);
            });
            this.success('Device Updated Successfully!');
            this.edithandleCancel();
        }
    });
    this.devicesApiData();
    }
    //edit model

    //device un-assigned
    unAssign = (e) => {
        var device_id = this.state.devicesDataSet.device_id;
        var project_id = this.state.devicesDataSet.project.projectId;
        
        axio.delete(`/device-management/remove-device-project/${project_id}/${device_id}`)
        .then(res=> {
            if(res.status===200){
                this.success('Desvice Successfully Un-Assigned to Project!');
                this.devicesApiData();
                this.setState({
                    unAssignComponnet:true,
                    unAssignComponnetBtn:false,
                    isAssign:true,
                    this_projectMappingId:!this.state.this_projectMappingId,
                })
                window.location.reload();
            }else if(res.status===400){
                this.success('Desvice Not Un-Assigned to Project!');
            }
        })
        .catch(error=> {
            console.log('error', error);
        });
    }

    
    //device un-assigned


    componentDidMount=(page)=>{
        this.pageChange(page)
        // this.devicesApiData(page)
    }

    devicesApiData=(page)=>{
        
        axio.get(`/device-management/devices-paginated/${this.state.current===false ? 1 : page}`)
          .then(res=> {
            //   console.log('devices all data from api', res.status)
              if(res.status===500){
                  this.setState({
                      resStatus:res.status
                  })
              }else{
                    const allDevicesData= [];
                    res.data.devices.forEach(devices => {
                        allDevicesData.push(
                            {
                                ...devices,
                                projectName: devices.project.name,
                                projectId: devices.project.projectId,
                                firmName: devices.firm.firmName,
                                firmEmail: devices.firm.email,
                                firmId: devices.firm.firmId,
                                contact: devices.firm.contact,
                                firm: devices.firm,
                                project: devices.project,
                                ram: devices.ram,
                                id:devices.id,
                                screen: devices.ram,
                                storage:devices.storage,
                                projectMappingId:devices.projectMappingId,
                        });
                    });
                        this.setState({ 
                            devicesData : allDevicesData,
                            resStatus: res.status,
                            totalDevices: allDevicesData,
                            devicesRes:res.data
                        });
                        
            }
          })
          .catch(error=> {
            console.log('Something wrong please try again!', error);
            // window.location.reload();
          });
    }
    // componentWillUpdate(prevProps, prevState) {
    //     return this.state.value != prevState.value;
    // }
      
    showHide=()=>{
        this.setState({
            showHide:!this.state.showHide,
        })
        // console.log('showHide', this.state.showHide)
    }
    deviceClick = (record)=>{
        console.log('deviceClick record----------------------', record)
        this.setState({
            showDetailsBox:true,
            devicesDataSet: record,
            deviceId: record.device_id,
            this_device_id:record.device_id,
            this_ram:record.ram,
            this_mac:record.mac,
            this_screen:record.screen,
            // this_active:record.active,
            this_model:record.model,
            this_storage:record.storage,
            this_projectMappingId:record.projectMappingId,
            new_project_maping_id:record.projectMappingId,
        })
        this.props.history.push('/dashboard/devices/details');
      }
        assignLocationModalhandleOk = (assignLocationModal) => {
            this.setState({
                assignLocationModal
            });
        }
    
        assignLocationModalhandleCancel = (assignLocationModal) => {
            this.setState({
                assignLocationModal
            });
        }

    closePage = () => {
        //this.devicesApiData();
        this.setState({
                showDetailsBox:!this.state.showDetailsBox,devicesDataSet:[]
            },() => {
                this.props.history.push('/dashboard/devices')
            }
        )
        // window.location.reload();
    };

        render(){
            
            let {filteredData,devicesData} = this.state;
            let devideDetails = filteredData.length?filteredData:devicesData;


            const columns = [{
                title: 'Device Id',
                dataIndex: 'device_id',
                width: 100,
                name: 'Device', 
                // ...this.getColumnSearchProps('Device'),
            },{
                title: 'Project Name',
                dataIndex: 'project_name',
                width: 400,
                name: 'Project Name',
                // ...this.getColumnSearchProps('project_name'),
            },{
                title: 'Firm',
                dataIndex: 'firmName',
                width: 200,
                // name: 'Firm',
                // ...this.getColumnSearchProps('firmName'),
                // name: 'DeviceFirm', 
                // ...this.getColumnSearchProps('DeviceFirm'),
              }
            ];
            const data = devideDetails.map((data, index)=> {
              return {
                  key: data.device_id,
                  id:data.id,
                  mac: data.mac,
                  device_id : data.device_id,
                  Device: data.device_id,
                  DeviceFirm: data.firm,
                  project_name: data.projectName ,
                  firm: data.firm,
                  project: data.project,
                  firmName: data.firmName,
                  firmEmail: data.firmEmail,
                  model : data.model,
                  contact : data.contact,
                  ram: data.ram,
                  screen: data.ram,
                  storage:data.storage,
                  projectMappingId:data.projectMappingId,
              }
          });
          
          const { getFieldDecorator } = this.props.form;
          const DevicesAssigneddata = [
            {
                devicesName: "Total Device",
                deviceCount: this.state.devicesRes.totalDevices
            },
            {
                devicesName: "Assigned Devices",
                deviceCount: this.state.devicesRes.assignedDevices
            },
            {
                devicesName: "Devices in Stock",
                deviceCount: this.state.devicesRes.totalDevices - this.state.devicesRes.assignedDevices
            },
          ];
          const cols = {
            sales: {
              tickInterval: 20,
              max:data.sales,
            }
        };

        const breadCrumbStyle = {cursor: "pointer"};
        return (
            <>
                {/*device details page data */}
                {!this.state.showDetailsBox ?
                    <>
                        {this.state.resStatus===200 ?
                        <Row span={24} >
                        <Row gutter={20} style={{marginBottom:'60px'}} type="flex" justify="space-around" align="middle">
                               
                              
                                <Col span={6} value={100}>
                                <div className="device-box">
                                        <Chart height={140} data={DevicesAssigneddata} width={20} scale={cols} forceFit padding={0}>
                                        <Axis name="devicesName"  width={40} grid={null}/>
                                        <Axis name="deviceCount" width={40} label={false} grid={null}/>
                                        <Geom type="interval" position="devicesName*deviceCount" 
                                                    color={["devicesName", ["#4FAAEB", "#9AD681", "#FED46B"]]}

                                        >
                                            <Label
                                            content="deviceCount"
                                            offset={-15}
                                            textStyle={{
                                                fontSize: 16
                                            }}
                                            />
                                        </Geom>
                                        </Chart>
                                    </div>
                                </Col>
                                <Col span={6} value={100}>
                                   <div className="device-box">
                                       <span className="device-box-title">Total Devices</span>   
                                       <span className="device-box-total-devices blue-color">{this.state.devicesRes.totalDevices}</span>
                                   </div>
                                       </Col>
                                <Col span={6} value={100}>
                                   <div className="device-box">
                                       <span className="device-box-title">Assigned Devices</span>   
                                       <span className="device-box-total-devices green-color">{this.state.devicesRes.assignedDevices}</span>
                                   </div>
                                       </Col>
                               <Col span={6} value={100}>
                               
                                   <div className="device-box">
                                       <span className="device-box-title">Devices In Stock</span>   
                                       <span className="device-box-total-devices orange-color">{this.state.devicesRes.totalDevices - this.state.devicesRes.assignedDevices}</span>
                                   </div>
                                </Col>
                                        
                               {/*<Col span={6}>
                                   <div className="device-box">
                                       <span className="device-box-title">Type of Devices</span>   
                                       <span className="device-box-total-devices">87989</span>
                                   </div>
                               </Col>   */}
                   </Row>
                    <Col span={24} className="firms">
                        <h3 style={{marginBottom: "60px"}}>Devices Overview</h3>
                            <Row type="flex" justify="space-between" className="firm-filters">
                                <Col span={3}>
                                    <button className="custom-button custom-button-dark create-button" onClick={this.showModal}>+ Add a Device</button>
                                </Col>
                                <Col span={6}>
                                <div className="new-btn-with-gradient-select search-bar-devices">
                                    <form className="search-container">
                                        <input type="text" id="search-bar-input" placeholder="Search…" onChange={(e)=>this.filterTable(e)}/>
                                        <span><i className="fas fa-search"/></span>
                                    </form>
                                </div>
                                </Col>
                            </Row>
                            <Row className="">
                                <Col span={24}>
                                    {/* {this.state.resStatus===200 ? */}
                                        <Table  
                                        columns={columns} 
                                        dataSource={data} 
                                        className="" 
                                        defaultCurrent={1}
                                        defaultPageSize={2}
                                        pagination={false} 
                                        // pagination={{pageSize:15}} 
                                        size="large" 
                                        total={this.state.devicesRes.pages}
                                        onRow={(record) => ({
                                            onClick: () => {
                                                this.setState({
                                                    this_projectMappingId:record.projectMappingId,
                                                });
                                                this.deviceClick(record);
                                            }, 
                                            })}
                                         />
                                         <Pagination
                                            className="custom-pagination"
                                            defaultCurrent={1}
                                            defaultPageSize={15}
                                            onChange={this.pageChange}
                                            total={this.state.devicesRes.totalDevices}
                                         />
                                </Col>                              
                            </Row>
                        </Col>
                    </Row>
                     :  
                    
                    <NewLoader />
                
                    }
                    </>
                    :
                    <Row span={24}>
                        <Col span={24} className="breadcrumbs-back">
                            <Breadcrumb separator=">" style={{marginBottom: '20px'}}>
                                <Breadcrumb.Item style={breadCrumbStyle}
                                                 onClick={() => this.props.history.push('/dashboard')}>Dashboard</Breadcrumb.Item>
                                <Breadcrumb.Item style={breadCrumbStyle} onClick={this.closePage}>Devices</Breadcrumb.Item>
                                <Breadcrumb.Item>Device Details</Breadcrumb.Item>
                            </Breadcrumb>
                            <p className="custom-button custom-button-light pointer" onClick={this.closePage}><Icon
                                type="caret-left"/> Back</p>
                        </Col>


                            <Col span={18} className="devices-details-page-right-section">
                                <div className={this.state.showHide ? 'devices-details-page show': 'devices-details-page hide'}>
                                        {/* device details page data */}
                                        <h2>{this.state.this_model} #{this.state.this_device_id}</h2>
                                        <p style={{fontSize:"18px"}}><strong>Firm#:</strong> {this.state.this_mac || '00:00:00:00:00:00'}</p>
                                        
                                        <div className="row  devices-border-bottom">
                                            <div className=" detailz-box col-md-custom m-15 margin0right-10p">
                                                <p>Device Model</p>
                                                <span>{this.state.this_model}</span>
                                            </div>

                                            <div className=" detailz-box col-md-custom">
                                                <p>RAM</p>
                                                    <span>{this.state.this_ram===null? '0' : this.state.this_ram}GB</span>
                                            </div>

                                            <div className=" detailz-box col-md-custom m-15 margin0right-10p no-border-bottom">
                                                <p>Storage</p>
                                                    <span>{this.state.this_storage}</span>
                                            </div>

                                            <div className=" detailz-box col-md-custom no-border-bottom">
                                                <p>Screen</p>
                                                    <span>{this.state.this_screen===null? '0' : this.state.this_screen} Resolution</span>
                                            </div>
                                        </div>
                                        {/* device details page data */}
        
                                            {this.state.this_projectMappingId?
                                                <>
                                                    {/* assign user componnet */}
                                                    <h2>Device Users</h2>
                                                    {/* <p>Users uisng this device</p> */}
                                                    <div className="row  devices-border-bottom">
                                                        <AssignUserToDevice devicesApiData={this.devicesApiData} deviceIds={this.state.devicesDataSet} projectId={this.state.projectId} id={this.state.id} devicesDataSet={this.state.devicesDataSet} device_id={this.state.devicesDataSet.device_id}/>
                                                    </div>
                                                    {/* assign user componnet */}
                                                </>
                                                :
                                                ''
                                            }
                                            
                                            {this.state.this_projectMappingId?
                                                <>
                                                    {/* assign geolocation componnet */}
                                                    <h2>Device Geolocation</h2>
                                                    {/* <p>Define areas device will work</p> */}
                                                    <div className="row  devices-border-bottom">
                                                        <AssignLocationDevice devicesApiData={this.devicesApiData} id={this.state.id} deviceIds={this.state.devicesDataSet} this_projectMappingId={this.state.this_projectMappingId}/>
                                                    </div>
                                                    {/* assign geolocation componnet */}
                                                </>
                                                :
                                                ''
                                            }

                                </div>
                                <Col span={24} className="edit-black-buttons margin-60 devices-border-bottom">
                                            {/* <EditDevice devicesApiData={this.devicesApiData} id={this.state.id} devicesDataSet={this.state.devicesDataSet} /> */}

                                            <Button type="primary" value="large"  onClick={this.editModal}>Edit</Button>
                                            <Modal
                                                title="Edit Device"
                                                visible={this.state.visible2}
                                                className="custom-table-popup"
                                                onOk={this.edithandleOk}
                                                onCancel={this.edithandleCancel}
                                                destroyOnClose={true}
                                                footer={false}
                                                centered
                                            >
                                            <Form onSubmit={this.editHandleSubmit}>
                                                            <Row className="custom-popup-row">
                                                                
                                                                <Col span={24} offset={4} className="custom-form-wrap">
                                                                    <Col span={16}>
                                                                        <Form.Item>
                                                                            {getFieldDecorator('id', {
                                                                                initialValue: this.state.devicesDataSet.id
                                                                                })(<Input hidden/>)}
                                                                        </Form.Item>
                                                                        <Form.Item label="Device Id">
                                                                            {getFieldDecorator('deviceId', {
                                                                                initialValue: this.state.devicesDataSet.device_id,
                                                                                rules: [{ required:true, message: 'Please add screen size' }],
                                                                                })(<Input />)}
                                                                        </Form.Item>

                                                                        <Form.Item label="RAM" className="custom-ant-input-number">
                                                                            {getFieldDecorator('ramAdd', { 
                                                                                initialValue: this.state.devicesDataSet.ram,
                                                                                rules: [{ required:true, message: 'Please add screen size' }],
                                                                                })(<InputNumber min={1} />)}
                                                                        </Form.Item>

                                                                        <Form.Item label="Model">
                                                                            {getFieldDecorator('modelAdd', {
                                                                                initialValue: this.state.devicesDataSet.model,
                                                                                rules: [{ required:true, message: 'Please add screen size' }],
                                                                                })(<Input />)}
                                                                        </Form.Item>

                                                                        <Form.Item label="MAC Address">
                                                                            {getFieldDecorator('macAdd', {
                                                                                initialValue: this.state.devicesDataSet.mac,
                                                                                rules: [{ required:true, message: 'Please add screen size' }],
                                                                                })(<Input />)}
                                                                        </Form.Item>

                                                                        <Form.Item label="Storage">
                                                                            {getFieldDecorator('storageAdd', {
                                                                                initialValue: this.state.devicesDataSet.storage,
                                                                                rules: [{ required:true, message: 'Please add screen size' }],
                                                                                })(<Input />)}
                                                                        </Form.Item>

                                                                        <Form.Item label="Screen Size">
                                                                            {getFieldDecorator('screenAdd', {
                                                                                initialValue: this.state.devicesDataSet.screen,
                                                                                rules: [{ required:true, message: 'Please add screen size' }],
                                                                                })(<Input />)}
                                                                        </Form.Item>

                                                                        <Form.Item >
                                                                            <Row type="flex" className="margin-top-40">
                                                                                    <Col span={12} className="edit-black-buttons float-right">
                                                                                        <Button type="primary" value="large" htmlType="submit">
                                                                                        Update
                                                                                        </Button>
                                                                                    </Col>
                                                                                    <Col span={12} className="unfill-edit-black-buttons float-left">
                                                                                        <Button type="primary" value="large" onClick={this.edithandleCancel}>Cancel</Button>
                                                                                    </Col>
                                                                            </Row>
                                                                        </Form.Item>
                                                                    </Col>
                                                                </Col>
                                                            </Row>
                                                        </Form>
                                            </Modal>
                                            {this.state.this_projectMappingId ?
                                                <Button type="primary" value="large"  onClick={this.unAssign}>Unassign Project</Button>  //  <UnAssignProject devicesApiData={this.devicesApiData} devicesDataSet={this.state.devicesDataSet}/>
                                                :
                                                <AssignProject assignProjectFunc={this.assignProjectFunc} new_project_maping_id={this.state.new_project_maping_id} devicesApiData={this.devicesApiData} id={this.state.id} deviceIds={this.state.devicesDataSet} this_projectMappingId={this.state.this_projectMappingId} device_id={this.state.devicesDataSet.device_id}/>
                                            }
                                        </Col>
                                
                                </Col>
                                <Col span={6} className="text-center details-device-page">
                                        <div>
                                                <img alt={"device-img"} src="/images/device-img.PNG" />
                                        </div>
                                </Col>
                        </Row>
                    }
                        {/* Add New device */}
                        
                        <Modal
                                title="Add New Device"
                                visible={this.state.visible}
                                className="custom-table-popup"
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                                destroyOnClose={true}
                                footer={false}
                                centered
                                >
                                <Form onSubmit={this.handleSubmit}>
                                        <Row className="custom-popup-row">
                                            {/* <p className="center">Please fill the following the input fields</p> */}
                                            <Col span={24} offset={4} className="custom-form-wrap">
                                                <Col span={16}>
                                                    <Form.Item label="Device Id">
                                                        {getFieldDecorator('deviceId', {
                                                            initialValue: '',
                                                            rules: [{ required:true, message: 'Please enter device id'}],
                                                            })(<Input />)}
                                                    </Form.Item>

                                                    <Form.Item label="RAM" className="custom-ant-input-number">
                                                        {getFieldDecorator('ramAdd', {
                                                            initialValue: '',
                                                            rules: [{ required:true, message: 'Please add RAM' }],
                                                            })(<InputNumber min={1} />)}
                                                    </Form.Item>

                                                    <Form.Item label="Model">
                                                        {getFieldDecorator('modelAdd', {
                                                            initialValue: '',
                                                            rules: [{ required:true, message: 'Please add Model' }],
                                                            })(<Input />)}
                                                    </Form.Item>

                                                    <Form.Item label="MAC Address">
                                                        {getFieldDecorator('macAdd', {
                                                            initialValue: '',
                                                            rules: [{ required:true, message: 'Please add MAC Address' }],
                                                            })(<Input />)}
                                                    </Form.Item>

                                                    <Form.Item label="Storage">
                                                        {getFieldDecorator('storageAdd', {
                                                            initialValue: '',
                                                            rules: [{ required:true, message: 'Please add storage of device i.e, 4GB' }],
                                                            })(<Input />)}
                                                    </Form.Item>

                                                    <Form.Item label="Screen Size">
                                                        {getFieldDecorator('screenAdd', {
                                                            initialValue: '',
                                                            rules: [{ required:true, message: 'Please add screen size' }],
                                                            })(<Input />)}
                                                    </Form.Item>

                                                    <Form.Item >
                                                        <Row type="flex" className="margin-top-40">
                                                                <Col span={12} className="edit-black-buttons float-right">
                                                                    <Button type="primary" value="large" htmlType="submit">
                                                                        Add
                                                                    </Button>
                                                                </Col>
                                                                <Col span={12} className="unfill-edit-black-buttons float-left">
                                                                    <Button type="primary" value="large" onClick={this.handleCancel}>Cancel</Button>
                                                                </Col>
                                                        </Row>
                                                    </Form.Item>
                                                </Col>
                                            </Col>
                                        </Row>
                                    </Form>
                        </Modal>
                            {/* Add new device */}
                    </>
            )
        }
}

const DevicesMain = Form.create({})(Devices);
export default DevicesMain;