import React from 'react';
import { Row, Col, Table, Button, Anchor, Modal, Form, Select, Input, Checkbox, Icon} from 'antd';

class AddNewDevice extends React.Component {
    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    }
 render(){
    const { getFieldDecorator } = this.props.form;  
    return(

                <Form onSubmit={this.handleSubmit}>
                    <Row className="custom-popup-row">
                        {/* <p className="center">Please fill the following the input fields</p> */}
                        <Col span={24} offset={4} className="custom-form-wrap">
                            <Col span={16}>
                                <Form.Item label="Device Id">
                                    {getFieldDecorator('deviceId', {
                                        // initialValue: this.props.device_id
                                        })(<Input disabled/>)}
                                </Form.Item>

                                {/* <Form.Item label="Project Name">
                                    {getFieldDecorator('projectId', {
                                        rules: [{ required:true, message: 'Please select your project!' }],
                                    })(
                                        <Select value="this.state.devicesDataSet.device_id" placeholder="Select a project to assin device..." onChange={(value, event) => this.handleOnChange(value, event)}>
                                            <Option value="project6">Project Name 1</Option>
                                            <Option value="project6">Project Name 2</Option> 
                                        </Select>
                                    )}
                                </Form.Item> */}

                                <Form.Item label="Project Id">
                                    {/* <Input value={this.state.selectedProject} disabled/> */}
                                </Form.Item>

                                <Form.Item >
                                    <Row type="flex" className="margin-top-40">
                                            <Col span={12} className="edit-black-buttons float-right">
                                                <Button type="primary" value="large" htmlType="submit">
                                                    Add
                                                </Button>
                                            </Col>
                                            <Col span={12} className="unfill-edit-black-buttons float-left">
                                                <Button type="primary" value="large" onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                    </Row>
                                </Form.Item>
                            </Col>
                        </Col>
                    </Row>
                </Form>
        )
        }
}

const AddDevice = Form.create({})(AddNewDevice);
export default AddDevice;