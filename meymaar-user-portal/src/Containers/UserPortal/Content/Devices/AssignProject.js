import React from 'react';
import {Row, Col,  Button,  Modal, Form, Select,  message, Input, } from 'antd';
import {fetchAllExecutionProjects} from "../../../../store/project/project-actions";
import {connect} from 'react-redux';
import axio from '../../../../utils/axios';


const {Option} = Select;


class AssignProjectBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            worksList: [],
            this_projectMappingId: this.props.this_projectMappingId,
            deviceIds: this.props.deviceIds,
            new_project_maping_id: this.props.new_project_maping_id,
        }
    }

    componentDidMount() {
        console.log('deviceIds component did mount', this.state.deviceIds);
        this.props.fetchAllExecutionProjects();
    }

    showModal = () => {
        this.setState({
            visible: true
        });
    };
    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };

    success = (msg) => {
        message.success(msg);
    };
    error = (msg) => {
        message.error(msg);
    };


    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (err) {
                return;
            }

            axio.post('/device-management/assign-device-project', {
                deviceId: values.deviceId,
                projectId: values.projectId,
            }).then(res => {
                this.success('Desvice Successfully Assigned to Project!');
                this.props.assignProjectFunc();
                this.props.devicesApiData();
                this.handleCancel();
                window.location.reload();
            })
                .catch(error => {
                    console.log('error', error);
                    // this.error('Geolocation not Assigned!');
                });
            console.log('this.state.new_project_maping_id', this.state.new_project_maping_id)
        });
    };



    getSelectOptions = () => this.props.worksList.sort((a, b) => a.name.localeCompare(b.name)).map((project, index) => {

        return (
            <Option key={index} value={project.projectId}>{project.name}</Option>
        )
    });

    render() {

        if (!this.props.worksList) return null;


        // console.log('deviceIds={this.state.deviceIds}', this.props.deviceIds)
        // console.log('props worksList', this.props.worksList)
        const {getFieldDecorator} = this.props.form;
        console.log('new_project_maping_id', this.props.new_project_maping_id);
        return (
            <>
                <Button type="primary" value="large" onClick={this.showModal}>Assign</Button>
                <Modal
                    title="Assign to Project"
                    visible={this.state.visible}
                    className="custom-table-popup"
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    destroyOnClose={true}
                    footer={false}
                >
                    <Form onSubmit={this.handleSubmit}>
                        <Row className="custom-popup-row">
                            {/* <p className="center">Please fill the following the input fields</p> */}
                            <Col span={24} offset={4} className="custom-form-wrap">
                                <Col span={16}>
                                    <Form.Item>
                                        {getFieldDecorator('deviceId', {
                                            initialValue: this.props.device_id
                                        })(<Input hidden/>)}
                                    </Form.Item>

                                    <Form.Item label="Select project">
                                        {getFieldDecorator('projectId', {
                                            rules: [{required: true, message: 'Please select your project!'}],
                                        })(
                                            <Select placeholder="Select a project to assign device...">
                                                {this.getSelectOptions()}
                                            </Select>
                                        )}
                                    </Form.Item>

                                    <Form.Item>
                                        <Row type="flex" className="margin-top-40">
                                            <Col span={12} className="edit-black-buttons float-right">
                                                <Button type="primary" value="large" htmlType="submit">
                                                    Assign Project
                                                </Button>
                                            </Col>
                                            <Col span={12} className="unfill-edit-black-buttons float-left">
                                                <Button type="primary" value="large"
                                                        onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                        </Row>
                                    </Form.Item>
                                </Col>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            </>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllExecutionProjects: () => dispatch(fetchAllExecutionProjects())
    }
};

const mapStateToProps = state => {
    return {
        worksList: state.project_reducer.executionProjects,
        projectProcessing: state.project_reducer.processing
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(AssignProjectBtn);
const formAssign = Form.create({})(connected);
export default formAssign;