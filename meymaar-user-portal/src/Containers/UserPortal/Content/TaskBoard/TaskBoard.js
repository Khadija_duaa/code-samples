import React from 'react';
import {connect} from 'react-redux';
import {deletePrivateMessage, fetchAllTasks} from "../../../../store/project/project-actions";
import AccordianRow from '../../../../Components/Accordian/Row';
import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import {getObjectValue, NOTIFICATION_TIME} from "../../../../utils/common-utils";
import NewLoader from '../../../Loader/NewLoader';
import {Pagination, Select,message} from 'antd';
import MarkingForm from '../ProjectMarking/MarkingForm/MarkingForm';
import axios from "../../../../utils/axios";
import DeleteConfirmation from '../../../../Components/ModalFactory/ConfirmationModal/DeleteConfirmation';
import InformationModal from '../../../../Components/ModalFactory/MessageModal/InformationModal'
import {handleError} from "../../../../store/store-utils";

const Option = Select.Option;


const searchFields = ['name', 'phaseName', 'markReason', 'markedByName', 'initDate'];

class TaskBoard extends React.Component {

    // State
    state = {
        activeKey: null,
        tasks: null,
        pageNumber: 1,
        currentPhase: "all",
        markingInfo: null,
        processing: false,
        sortOrder: {},
        showInfoModal: false
    };

    // Getting Task Category from session
    getTaskCategory = () => {

        const category_key = 'task-category';
        let taskState = sessionStorage.getItem(category_key);

        const {location} = this.props;
        if (location && location.state) {
            taskState = location.state.task;
        }

        sessionStorage.setItem(category_key, taskState);
        return taskState;

    };


    componentDidMount() {
        this.fetchAllProjectTasks('Planing');
    }

    componentDidUpdate() {
        const {location} = this.props;

        if (this.state.pathname && location && location.pathname !== this.state.pathname) {
            this.fetchAllProjectTasks('Planing');
        }
    }


    /************************************************** Events ************************************************/


        // Fetching All Project Tasks
    fetchAllProjectTasks = (projectType) => {
        let task = this.getTaskCategory();
        let isInbox = task === 'Inbox';

        let order = {
            order: this.state.sortOrder
        };
        this.props.fetchAllTasks(order, projectType, this.state.currentPhase, this.state.pageNumber, isInbox, () => {
            this.setState({pathname: this.props.location.pathname, activeKey: -1, showConfirmModal: false});
        });
    };

    // Page change handler
    onPageChange = (page) => {
        this.setState({pageNumber: page}, () => {
            this.fetchAllProjectTasks('Planing');
        })
    };

    // Searching Tasks
    searchTasks = (searchStr, searchParams = []) => {
        if (!this.props.taskBoardList) return null;
        const tasks = [...this.props.taskBoardList];

        if (searchStr) {
            let filteredTasks = [];
            tasks.forEach(task => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(task, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredTasks.push(task);
                        break;
                    }
                }
            });
            this.setState({tasks: filteredTasks});
        }
        else {
            this.setState({tasks: null});
        }
    };

    // Phase Change handler
    handlePhaseChange = (phaseId) => {
        this.setState({currentPhase: phaseId}, () => {
            this.fetchAllProjectTasks('Planing');
        })
    };

    //  Fetching Marking Information
    fetchMarkingInfo = (activeIndex, task) => {
        axios.get(`/project-management/projects/${task.projectId}/${task.phaseId}/marking-info`)
            .then(response => {

                let markingData = response.data;
                delete  markingData['externalMarkingInfo'];
                this.setState({markingInfo: markingData, processing: false});
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage,NOTIFICATION_TIME);
                this.setState({processing: false});
            });
    };

    handleMarkingSuccessCB = (data) => {
        this.setState({showMarkingModal: false, showInfoModal: true, data});
    };

    /************************************************** End **************************************************/


    /************************************************** Modals ***********************************************/

        // Marking Modal Form
    handleMarkingCB = (task) => {
        this.setState({...task, showMarkingModal: true})
    };
    toggleMarkingModal = () => {
        this.setState({showMarkingModal: !this.state.showMarkingModal})
    };
    renderMarkingModal = () => {
        let dataProps = {
            markingInfo: this.state.markingInfo,
            isInboxProject: true
        };

        return (
            <Modal style={{width: "500px"}} isOpen={this.state.showMarkingModal}>
                <ModalHeader className={"header"}>
                    Mark
                </ModalHeader>

                <ModalBody>
                    <MarkingForm name={this.state.name} projectId={this.state.projectId} phaseId={this.state.phaseId}
                                 {...dataProps}
                                 successCB={(message) => this.handleMarkingSuccessCB(message)}
                                 cancelCB={this.toggleMarkingModal}/>
                </ModalBody>

            </Modal>
        )
    };


    // Delete Confirmation Modals
    handleDeleteMessageClick = () => {
        let {taskId} = this.state;
        this.props.deletePrivateMessage(taskId, () => {
            this.fetchAllProjectTasks('Planing');
        })
    };
    toggleConfirmationModal = () => {
        this.setState({showConfirmModal: !this.state.showConfirmModal});
    };
    renderConfirmationModal = () => {
        return (
            <DeleteConfirmation isOpen={this.state.showConfirmModal}
                                okCB={this.handleDeleteMessageClick} cancelCB={this.toggleConfirmationModal}
                                fileTitle={"Private"} otherText={"message"}/>
        );
    };


    //Information Modal Form
    toggleModal = () => {
        if (this.state.data) {
            this.setState({showInfoModal: false}, () => {
                this.props.history.push('/dashboard')
            })
        } else {
            this.setState({showInfoModal: !this.state.showInfoModal})
        }
    };

    handleInfoCB = () => {
        this.props.history.push('/dashboard');
    };

    renderInformationModal = () => {
        let {data} = this.state;

        let message = '';

        if (data) {
            //message = `project has been marked to ${data.officename?data.officename:data.username+"("+data.rolename+")"}`
            message = data;
        }


        return (
            <InformationModal isOpen={this.state.showInfoModal}
                              okCB={this.handleInfoCB} cancelCB={this.toggleModal}
                              message={message}/>
        );
    };

    /************************************************** End **************************************************/


    /******************************************** Sorting Region **************************************/
    //
    // sortedParams = ["name", "phaseId", "reason", "marked_by_name", "initiated_date"];
    //
    // handleSortClick = (key,order)=>{
    //     this.setState({sortOrder:[key,order]},()=>{
    //         this.fetchAllProjectTasks('Planing');
    //     })
    // };
    //
    // getSortedLayout = (header) => {
    //     let {ascending} = this.state;
    //
    //     if (this.sortedParams.includes(header.key)) {
    //         return (
    //             <div>
    //                 {header.title}
    //                 <span style={{
    //                     float: "right",
    //                     display: "inline-grid",
    //                     marginRight: "30px",
    //                     height: "-webkit-fill-available"
    //                 }}>
    //                     <Icon type="caret-up"
    //                           onClick={() => this.handleSortClick(header.key,'asc')}/>
    //                     <Icon type="caret-down"
    //                           onClick={() => this.handleSortClick(header.key,'desc')}/>
    //                 </span>
    //             </div>
    //         )
    //     } else {
    //         return header.title;
    //     }
    // };

    /************************************************** End **************************************************/




    renderHeading = () => {
        let task = this.getTaskCategory();

        let title = task === 'Inbox' ? "Projects In Initial Receipt" : "Taskboard";
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <h3>{title}</h3>
                    </div>
                </div>
            </div>
        )
    };


    renderActionButtons = () => {


        let {phases} = this.props;
        let phasesJSX = [];
        if (phases) {
            phasesJSX = this.props.phases.map((phase, index) => {
                return <Option key={index} value={phase.uuid}>{phase.name}</Option>
            });
        }
        phasesJSX.unshift(<Option key={0} value={"all"}>All</Option>);

        return (
            <div className="container-fluid">
                <div className="row button-list ">

                    <div className="col-md-12 text-right">

                        <div className="dropdown-button">
                            <Select defaultValue="all" className="new-add-assign-select"
                                    onChange={this.handlePhaseChange}>
                                {phasesJSX}
                            </Select>
                        </div>


                        {/* <!--Search Fields--> */}
                        <div className="new-btn-with-gradient-select search-bar">
                            <form className="search-container">
                                <input type="text" id="search-bar" placeholder="Search…"
                                       onChange={(e) => this.searchTasks(e.target.value, searchFields)}/>
                                <a><i className="fas fa-search"/></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    };


    getTaskBody = () => {

        if (!this.props.taskBoardList) return null;
        let taskList = this.state.tasks ? this.state.tasks : this.props.taskBoardList;
        let taskCategory = this.getTaskCategory();


        let handleRowClick = (index, task, category) => {
            if (this.state.activeKey === index) {
                this.setState({activeKey: -1});
            } else {
                if (category === 'Inbox') {
                    this.setState({activeKey: index, processing: true}, () => {
                        this.fetchMarkingInfo(index, task, category);
                    })
                } else {
                    this.setState({activeKey: index});
                }
            }
        };


        // Table Body
        return taskList.map((task, index) => {

            let {processing,markingInfo} = this.state;

            return (
                <AccordianRow key={index} style={{cursor: "pointer"}}
                              onClick={() => handleRowClick(index, task, taskCategory)}
                              {...task} processing={processing} task={taskCategory}
                              markingCB={() => markingInfo && this.handleMarkingCB(task)}
                              deleteCB={(id) => this.setState({showConfirmModal: true, taskId: id})}
                              isAccordian={index === this.state.activeKey}
                >
                    <td><a>{task.name}</a></td>
                    <td>{task.phaseName}</td>
                    <td>{task.markReason}</td>
                    <td>{task.markedByName}</td>
                    <td>{task.initDate}</td>
                    <td colSpan="2">
                        <i className={index === this.state.activeKey ? "fa fa-angle-up clickable display-collapse-button" : "fa fa-angle-down"}/>
                    </td>
                </AccordianRow>
            )
        });
    };


    renderTaskBoard = () => {

        let taskCategory = this.getTaskCategory();
        let {taskPagination} = this.props;
        let taskList = this.state.tasks;

        let reasonText = taskCategory === 'Inbox' ? "Initial Receipt" : "Reason";

        // Table Headers
        let tableHeaders = ["Project Name", "Phase Name", reasonText, "Requested By", "Marking Date"];
        let headerJSX = tableHeaders.map((head, index) => {
            return (
                <th key={index} scope="col " colSpan={index === tableHeaders.length - 1 ? 2 : ''}>
                    <span>{head}</span>
                </th>
            )
        });

        // let headersJSON = [
        //     {
        //         title: "Project Name",
        //         key: "name"
        //     },
        //     {
        //         title: "Phase Name",
        //         key: "phaseId"
        //     },
        //     {
        //         title: reasonText,
        //         key: "reason"
        //     },
        //     {
        //         title: "Requested By",
        //         key: "marked_by_name"
        //     },
        //     {
        //         title: "Marking Date",
        //         key: "initiated_date"
        //     }
        // ];
        // let headerJSX = headersJSON.map((head, index) => {
        //     return (
        //         <th key={index} scope="col">
        //             {this.getSortedLayout(head)}
        //         </th>
        //     )
        // });

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className=" custom-table table-responsive" style={{overflowX: "hidden"}}>
                            <table className="border-bottom collapse-table table border-0">
                                <thead className="thead-dark">

                                <tr>
                                    {headerJSX}
                                </tr>
                                </thead>

                                <tbody>
                                {this.getTaskBody()}
                                </tbody>
                            </table>

                            {taskPagination ?
                                <div style={{margin: "50px 0px", textAlign: "right"}}>
                                    <Pagination showTotal={total => `Total ${total} items`}
                                                current={this.state.pageNumber}
                                                pageSize={taskPagination.pageSize}
                                                total={taskList ? taskList.length : taskPagination.totalEntries}
                                                onChange={this.onPageChange}/>
                                </div> : null}
                        </div>
                    </div>
                </div>
            </div>
        )
    };


    render() {
        return (
            <>
                {this.renderMarkingModal()}
                {this.renderInformationModal()}
                {this.renderConfirmationModal()}
                {this.renderHeading()}
                {this.renderActionButtons()}
                {this.props.processing || this.props.serverProcessing ? <NewLoader/> : this.renderTaskBoard()}
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllTasks: (sortOder, projectType, phaseId, pageNumber, isInbox, cb) => dispatch(fetchAllTasks(sortOder, projectType, phaseId, pageNumber, isInbox, cb)),
        deletePrivateMessage: (taskId, cb) => dispatch(deletePrivateMessage(taskId, cb))
    }
};


const mapStateToProps = (state) => {
    return {
        taskBoardList: state.project_reducer.taskBoardList,
        processing: state.project_reducer.processing,
        serverProcessing : state.server_reducer.processing,
        taskPagination: state.project_reducer.taskPagination,
        phases: state.project_reducer.phases
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(TaskBoard);

export default connected;