import React from 'react';
import {Row, Col, Table, Form, message, Input, Modal, Button, Icon} from 'antd';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {BUTTON_COLOR} from "../../../../utils/common-utils";
import NewLoader from '../../../Loader/NewLoader';
import {loadBqCategories,saveBqCategory,deleteBqCategories} from "../../../../store/revitManagement/revit_actions";

class BqCategories extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showHide: true,
            radioValue: 1,
            visible: false,
            value: '',
            modalOpen: false,
            index:'',
            visibleAdd:false,
            isFormfilled:false
        }
    }


    componentDidMount() {

        this.props.loadBqCategories();

    };

    showModalDelete = (_id) => {


        this.setState({
            visible: true,
            index:_id
        });
    };


    showHide = () => {
        this.setState({
            showHide: !this.state.showHide,
        })
    };
    radioValue = (e) => {
        this.setState({
            radioValue: e.target.value,
        });
    };

    showModal = () => {

        this.setState({
            visibleAdd: true,
        });
    };

    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    };

    handleOkAdd = (e) => {
        this.setState({
            visibleAdd: false,
        });
    };


    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };

    handleCancelAdd = (e) => {
        this.setState({
            visibleAdd: false,
        });
    };



    success = (msg) => {

        message.success(msg);
    };

    errors = (msg) => {

        message.error(msg);
    };


    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {

            let formData = {types: [values.types]};


            if (!err) {

                this.props.saveBqCategory(formData,()=>{this.success(this.props.message && this.props.message)},() => {
                    this.errors(this.props.message && this.props.message)
                },()=>{this.props.loadBqCategories()})

            }


        })

    };


        onChangeBqCategory = (e) => {

            if(e.target.value.length > 0) {
                this.setState({isFormfilled: true})
            }
            else{
                this.setState({isFormfilled: false})
            }
        };


    renderHeading = () => {

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <h3>BQ Categories</h3>
                    </div>
                </div>
            </div>
        )
    };

    render(){




        const contractorColumns = [{
            title: 'Id',
            dataIndex: 'index',
            width: 200,

        },  {
            title: 'Name',
            dataIndex: 'type',
            width: 200,

        }, {
            title: 'Action',
            dataIndex: 'id',
            width: 200,

            render: (text, record) => (
                <span className="table-data1 edit-actions" ><a href="javascript:" onClick={(e)=>{
                    e.stopPropagation();
                    this.showModalDelete(record.id)}}><Icon
                    type="delete"/></a></span>
            )
        }

        ];


        let data = Array.isArray(this.props.bqCategories  && this.props.bqCategories );


        let tableDataBQCategory =data ? this.props.bqCategories && this.props.bqCategories.map((_data,index)=>{ return {
            type:_data.type,
            index:index+1,
            id:_data.id,

        }}): null;





        const { getFieldDecorator } = this.props.form;
        return(
            <>
                {this.props.processing ? <NewLoader/> : null}
                {this.renderHeading()}
                <button className="custom-button custom-button-dark create-button margin-60 mt-5"  onClick={()=>{this.showModal();}}>+ Add Category</button>

                <Row>
                    <Col span={24} className="firms">
                        <Row className="">
                            <Col span={24}>
                                <Table
                                    columns={contractorColumns}
                                    dataSource={tableDataBQCategory}
                                    className="zero-padding"
                                    size="large"
                                    pagination={{ pageSize: 10 }}
                                />
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <div>
                    <Modal
                        title="Confirmation"
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        centered={true}
                        onCancel={this.handleCancel}
                        footer={null}
                        style={{textAlign:"center"}}
                        maskClosable={false}
                    >
                        <Row>
                            <Col span={24}>
                                <Row> Are you sure, you want to delete row?</Row>

                            </Col>
                        </Row>
                        <Row style={{display:'flex', marginTop:'40px'}}>
                            <Col span={12}>
                                <Button style={{ height: "40px", width: "90%",backgroundColor: BUTTON_COLOR, color: "#fff",float:'right'}}
                                        onClick={()=>{
                                            this.handleOk();
                                            this.props.deleteBqCategories(this.state.index,()=>{this.props.loadBqCategories()},
                                                ()=>{this.success(this.props.message && this.props.message )},()=>{this.errors(this.props.error && this.props.error )})}}>
                                    Yes
                                </Button>
                            </Col>
                            <Col span={12}>
                                <Button style={{height: "40px", width: "90%", marginLeft: "5%", borderColor: BUTTON_COLOR, float:'left'}}
                                        onClick={()=>{this.handleCancel()}}>Cancel</Button>
                            </Col>
                        </Row>
                    </Modal>

                </div>

                <div>
                    <Modal
                        title="Add"
                        visible={this.state.visibleAdd}
                        onOk={this.handleOkAdd}
                        centered={true}
                        onCancel={this.handleCancelAdd}
                        footer={null}
                        maskClosable={false}
                    >


                        <Form onSubmit={(e) => {
                            this.handleSubmit(e)
                        }} layout={"horizontal"}>
                            <Row className="custom-popup-row">
                                <Col span={24} className="custom-form-wrap">



                                    <Form.Item label="Name" style={{marginLeft: '60px', width: '80%'}}>
                                        {getFieldDecorator('types', {
                                            rules: [{required: true, message: 'Please enter name!', whitespace: false}]
                                        })(<Input onChange={this.onChangeBqCategory} placeholder="Please enter name"/>)}
                                    </Form.Item>



                                    <Form.Item>
                                        <Row type="flex" className="margin-top-40" style={{marginLeft:'30px'}}>
                                            <Col span={12} className="edit-black-buttons float-right">
                                                <Button type="primary" value="large" htmlType="submit"
                                                        onClick={this.handleCancelAdd} disabled={!this.state.isFormfilled}>
                                                    <div style={{color:'white'}}>Submit</div>
                                                </Button>
                                            </Col>
                                            <Col span={12} className="unfill-edit-black-buttons float-left">
                                                <Button type="primary" value="large"
                                                        onClick={this.handleCancelAdd}>Cancel</Button>
                                            </Col>
                                        </Row>
                                    </Form.Item>


                                </Col>
                            </Row>
                        </Form>

                    </Modal>

                </div>
            </>
        )
    }
}



const mapStateToProps = (state) => {


    return {
        bqCategories: state.revit_setting.bqCategories && state.revit_setting.bqCategories,
        message: state.revit_setting.revitSetting && state.revit_setting.message,
        error: state.revit_setting.revitSetting && state.revit_setting.error ,
        processing:state.revit_setting.revitSetting && state.revit_setting.processing,
    }
};

const BqCategory = Form.create({})(BqCategories);

const connectedComponent = connect(mapStateToProps,{loadBqCategories,saveBqCategory,deleteBqCategories} )(BqCategory);
export default withRouter(connectedComponent);