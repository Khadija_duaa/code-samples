import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {MODEL_URL} from "../../../../../utils/config";
// import './ModelStyle.css';
// import {animate, initThree} from "./scripts/gv-thr";
// import {initHeadsUp} from "./scripts/gv-ctx";
// import {fetch3DFile} from "../../../../../utils/server-utils";
// import {callbackGbXML} from "./scripts/gv-cor";

class ModelViewer extends React.Component {

    componentDidMount() {
        let {projectDetails,activeUser} = this.props;

        if (!projectDetails) {
            this.props.history.push("/dashboard/works/overview");
        }
        else{
            let object = {
                token:activeUser.token,
                projectId:projectDetails.projectId
            };
            let encoded = btoa(JSON.stringify(object));
            console.log("Encoded",encoded);

            console.log("Decoded",JSON.parse(atob(encoded)));

            window.open(`${MODEL_URL}/?file=${encoded}`)
        }

    }

    render() {

        return (
            <div>
                {/*<div id="CORdivMenuInWorld">*/}
                    {/*<p id="CORmenuInWorldContents"/>*/}
                    {/*<div id="CORdivLog"/>*/}
                {/*</div>*/}


                {/*<div id="CORdivMenuRight" className="jumbotron">*/}
                    {/*<div id="CORdivItemsRight"/>*/}
                {/*</div>*/}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        projectDetails: state.project_reducer.projectDetails,
        activeUser:state.user_reducer.activeUser
    }
};

const connected = connect(mapStateToProps)(ModelViewer);

export default withRouter(connected);
