import {setPanelButtonInit} from "./gv-cor";
import * as COR from './gv-cor';

import {getCTXValues} from "./gv-ctx";

import * as GBX from './gv-gbx';
import {getGBXValues} from "./gv-gbx";


import {getTHRValues} from "./gv-thr";

import * as THREE from "three/src/Three";

var SAV = {};

//let CORdivMenuItems = document.getElementById('CORdivMenuItems');


SAV.initChanges =  () =>{ // called from bottom of file

    //SAV.initMenuSaveChanges('CORdivMenuItems');

    //setPanelButtonInit();
};


SAV.viewChanges = function () {

    document.getElementById('CORdivItemsRight').innerHTML =
        `
			<h3>Current for save changes file JSON source code</h3>
			<textArea id='txtSaveSource' style="height:300px;width:100%;" ></textArea>

			<p>
				<button onclick=SAV.setPopupChanges(); >Um, what the heck, Just do it.</button> << work-in-progress. Three.js surfaces updated but not the gbXML.
			</p>

		`;

    COR.setRightMenuWide();

    document.getElementById('txtSaveSource').value = JSON.stringify(getCTXValues().surfaceChanges, null, ' ');

};


SAV.saveChanges = function () {

    console.log('getCTXValues().surfaceChanges', getCTXValues().surfaceChanges);

    const output = JSON.stringify(getCTXValues().surfaceChanges, null, ' ');
    const blob = new Blob([output]);
    let a = document.body.appendChild(document.createElement('a'));
    a.href = window.URL.createObjectURL(blob);
    a.download = getGBXValues().gbjson.Campus.Building.id + '-changes.json';
    a.click();
    a = null;

};


export const openChanges = function (files) {

    const reader = new FileReader();
    reader.onload = function (event) {

        getCTXValues().surfaceChanges = JSON.parse(reader.result);
        SAV.setPopupChanges(files.files[0]);

    };

    reader.readAsText(files.files[0]);

};


SAV.setPopupChanges = function (file) {


    document.getElementById('divPopUpContents').innerHTML =
        `
			<h3>statistics for save changes file</h3>
			<div id=divSavHeader ></div>
			<h3>actions taken</h3>
			<div id=divSavContents ></div>
			<h3>save changes file source code</h3>
			<textArea id='txtSaveSource' style="height:300px;width:100%;" ></textArea>
		`;

    document.getElementById('divPopUp').style.display = 'block';
    window.scrollTo(0, 0);


    document.getElementById('txtSaveSource').value = JSON.stringify(getCTXValues().surfaceChanges, null, ' ');

    document.getElementById('divSavHeader').innerHTML =
        (file ?
            'name: <i>' + file.name + '</i><br>' +
            'size: <i>' + file.size.toLocaleString() + ' bytes</i><br>' +
            (file.type ? 'type: <i>' + file + '</i><br>' : '') +
            'modified: <i>' + file.lastModifiedDate.toLocaleDateString() + '</i><br>'
            : '') +
        (getCTXValues().surfaceChanges.deletes ? 'deletes: ' +getCTXValues().surfaceChanges.deletes.length + '<br>' : '') +
        (getCTXValues().surfaceChanges.types ? 'type changes: ' + getCTXValues().surfaceChanges.types.length + '<br>' : '') +
        (getCTXValues().surfaceChanges.CADObjectId ? 'cad id changes: ' + getCTXValues().surfaceChanges.CADObjectId.length + '<br>' : '') +
        (getCTXValues().surfaceChanges.oneAdjacent ? 'one adjacent changes: ' + getCTXValues().surfaceChanges.oneAdjacent.length + '<br>' : '') +
        (getCTXValues().surfaceChanges.twoAdjacent ? 'two adjacent changes: ' + getCTXValues().surfaceChanges.twoAdjacent.length + '<br>' : '') +
        '<br>';

    getUpdates();

    //console.log( '', files );
};


SAV.getSurfaceByName = function (name) {

    const surfacesXml = getGBXValues().gbxml.getElementsByTagName("Surface");
    let surfaceXml;

    for (let surface of surfacesXml) {

        if (name === surface.getElementsByTagName("Name")[0].innerHTML) {

            surfaceXml = surface;

            break;

        }

    }

    return surfaceXml;

};


export const getUpdates = () =>{

    if (getCTXValues().surfaceChanges.addAttributesMissing) {
        SAV.addAttributesMissing();
    }


    if (getCTXValues().surfaceChanges.deletes) {
        SAV.setDeletes();
    }


    if (getCTXValues().surfaceChanges.deleteDuplicateSurfaces) {
        SAV.setDeleteSurfaceDuplicates();
    }


    if (getCTXValues().surfaceChanges.types) {

        for (let item of getCTXValues().surfaceChanges.types) {

            //console.log( 'item', item );

            const surfaceXml = SAV.getSurfaceByName(item.name);

            if (!surfaceXml) {

                document.getElementById('divSavContents').innerHTML += 'Types changes - not found surface name: ' + item.name + '<br>';
                continue;

            } else {

                surfaceXml.attributes.getNamedItem('surfaceType').nodeValue = item.type;

                let surfaceMesh = getGBXValues().surfaceMeshes.children.find(element => element.userData.data.Name === item.name);
                surfaceMesh.userData.data.surfaceType = item.type;
                surfaceMesh.material.color.setHex(getGBXValues().colors[item.type]);
                surfaceMesh.material.needsUpdate = true;

                const twoAdjacents = ['InteriorWall', 'InteriorFloor', 'Ceiling', 'Air', 'RaisedFloor'];

                if (twoAdjacents.includes(item.type)) { // new is two adjacent

                    // if (!surfaceMesh.userData.data.AdjacentSpaceId) { // was Shade
                    //
                    //     const newAdj = GBX.gbxmlResponseXML.createElement("AdjacentSpaceId");
                    //     newAdj.setAttribute("spaceIdRef", "none");
                    //     const newAdjTxt = surfaceXml.appendChild(newAdj);
                    //
                    //     const newAdj2 = GBX.gbxmlResponseXML.createElement("AdjacentSpaceId");
                    //     newAdj2.setAttribute("spaceIdRef", "none");
                    //     const newAdjTxt2 = surfaceXml.appendChild(newAdj2);
                    //
                    //     surfaceMesh.userData.data.AdjacentSpaceId = [{"spaceIdRef": 'none'}, {"spaceIdRef": 'none'}];
                    //     console.log('was 0 / now 2', surfaceXml);
                    //
                    // } else if (Array.isArray(surfaceMesh.userData.data.AdjacentSpaceId) === true) { // was already two adjacent
                    //
                    //     console.log('was 2 / now 2', surfaceXml);
                    //
                    // } else { // was one adjacent
                    //
                    //     const newAdj = GBX.gbxmlResponseXML.createElement("AdjacentSpaceId");
                    //     newAdj.setAttribute("spaceIdRef", "none");
                    //     const newAdjTxt = surfaceXml.appendChild(newAdj);
                    //
                    //     surfaceMesh.userData.data.AdjacentSpaceId = [{"spaceIdRef": surfaceMesh.userData.data.AdjacentSpaceId.spaceIdRef}, {"spaceIdRef": 'none'}];
                    //     console.log('was 1 / now 2', surfaceXml);
                    //
                    // }

                } else { // new is Shade or one adjacent

                    console.log('new 0 or 1  ', surfaceXml);
                    console.log('mesh data[ 0 ]', surfaceMesh.userData.data.AdjacentSpaceId);
                    console.log('item.type', item.type);
                    console.log('Array.isArray( surfaceMesh.userData.data.AdjacentSpaceId )', Array.isArray(surfaceMesh.userData.data.AdjacentSpaceId));

                    if (Array.isArray(surfaceMesh.userData.data.AdjacentSpaceId) === true && item.type === 'Shade') {

                        const adjSpace1 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[1];
                        //console.log( 'adjSpace1',  adjSpace1 );

                        const removedId1 = adjSpace1.getAttribute('spaceIdRef');
                        const removed1 = surfaceXml.removeChild(adjSpace1);

                        const adjSpace2 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];
                        //console.log( 'adjSpace2', adjSpace2 );

                        const removedId2 = adjSpace2.getAttribute('spaceIdRef');
                        const removed2 = surfaceXml.removeChild(adjSpace2);
                        console.log('removedId2', removedId2);

                        delete(surfaceMesh.userData.data.AdjacentSpaceId);
                        console.log('surfaceMesh', surfaceMesh);

                        console.log('was 2 / now 0', surfaceXml);

                    } else if (Array.isArray(surfaceMesh.userData.data.AdjacentSpaceId) === true && item.type !== 'Shade') {


                        const adjSpace1 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[1];
                        //console.log( 'adjSpace1',  adjSpace1 );

                        const removedId1 = adjSpace1.getAttribute('spaceIdRef');
                        const removed1 = surfaceXml.removeChild(adjSpace1);

                        const adjSpace2 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];

                        const removedId2 = adjSpace2.getAttribute('spaceIdRef');

                        surfaceMesh.userData.data.AdjacentSpaceId = {"spaceIdRef": removedId2};
                        console.log('surfaceMesh', surfaceMesh);
                        console.log('surfaceXml', surfaceXml);

                        console.log(' was 2 / now 1 / ');

                    } else if (Array.isArray(surfaceMesh.userData.data.AdjacentSpaceId) === false && item.type !== 'Shade') {

                        if (surfaceMesh.userData.data.AdjacentSpaceId !== undefined) {

                            console.log('surfaceMesh', surfaceMesh.userData.data.AdjacentSpaceId);
                            console.log('surfaceXml', surfaceXml);

                            console.log('was 1 / now 1 / ');

                        } else {

                            // const newAdj = GBX.gbxmlResponseXML.createElement("AdjacentSpaceId");
                            // newAdj.setAttribute("spaceIdRef", surfaceMesh.userData.data.AdjacentSpaceId);
                            // const newAdjTxt = surfaceXml.appendChild(newAdj);

                            surfaceMesh.userData.data.AdjacentSpaceId = {"spaceIdRef": "none"};
                            console.log('surfaceMesh', surfaceMesh.userData.data.AdjacentSpaceId);
                            console.log('surfaceXml', surfaceXml);

                            console.log('was 0 / now 1 / ');

                        }

                    }

                }

                document.getElementById('divSavContents').innerHTML += 'Types changes - updated surface name: ' + item.name + '<br>';

            }

        }

    }


    if (getCTXValues().surfaceChanges.CADObjectId) {

        for (let item of getCTXValues().surfaceChanges.CADObjectId) {

            //const surfaceXml = GBX.gbxml.getElementsByTagName( "Surface"  )[ item.id ];

            const surfaceXml = SAV.getSurfaceByName(item.name);

            if (!surfaceXml) {

                document.getElementById('divSavContents').innerHTML += 'cad object id changes - not found item name: ' + item.name + '<br>'
                continue;

            } else {

                let cadObjId = surfaceXml.getElementsByTagName("CADObjectId")[0];

                if (cadObjId) {

                    cadObjId.innerHTML = item.cadId;

                    const surfaceMesh = getGBXValues().surfaceMeshes.children.find(element => element.userData.data.Name === item.name);
                    surfaceMesh.userData.data.CADObjectId = item.cadId;

                    document.getElementById('divSavContents').innerHTML += 'change cad object for item name: ' + item.name + '<br>';

                } else {

                    surfaceXml.setAttribute("CADObjectId", item.cadId);

                    const surfaceMesh = getGBXValues().surfaceMeshes.children.find(element => element.userData.data.Name === item.name);
                    surfaceMesh.userData.data.CADObjectId = item.cadId;

                    document.getElementById('divSavContents').innerHTML += 'Added cad object for item name: ' + item.name + '<br>';

                }

            }

        }

    }


    if (getCTXValues().surfaceChanges.oneAdjacent) {

        for (let item of getCTXValues().surfaceChanges.oneAdjacent) {

            //const surfaceXml = GBX.gbxml.getElementsByTagName( "Surface"  )[ item.name ];

            const surfaceXml = SAV.getSurfaceByName(item.name);

            if (!surfaceXml) {

                document.getElementById('divSavContents').innerHTML += 'adjacent 1 - not found item name: ' + item.name + '<br>'
                continue;

            } else {

                const adj = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];

                const att = adj.attributes.getNamedItem('spaceIdRef').nodeValue = item.spaceId;
                //console.log( 'adj', adj, att );

                const surfaceMesh = getGBXValues().surfaceMeshes.children.find(element => element.userData.data.name === item.name);
                surfaceMesh.userData.data.AdjacentSpaceId = {"spaceIdRef": item.spaceId};

                document.getElementById('divSavContents').innerHTML += 'change adjacent space 1 for item  name: ' + item.name + '<br>';

            }

        }

    }


    if (getCTXValues().surfaceChanges.twoAdjacent) {

        for (let item of getCTXValues().surfaceChanges.twoAdjacent) {

            const surfaceXml = getGBXValues().gbxml.getElementsByTagName("Surface")[item.name];

            if (!surfaceXml) {

                document.getElementById('divSavContents').innerHTML += 'adjacent 2 - not found item name: ' + item.name + '<br>';
                continue;

            } else {

                let adj1 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];
                let att1 = adj1.attributes.getNamedItem('spaceIdRef').nodeValue = item.spaceId[0];

                let adj2 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[1];
                let att2 = adj2.attributes.getNamedItem('spaceIdRef').nodeValue = item.spaceId[1];

                const surfaceMesh = getGBXValues().surfaceMeshes.children.find(element => element.userData.data.name === item.name);
                surfaceMesh.userData.data.AdjacentSpaceId = [{"spaceIdRef": item.spaceId[0]}, {"spaceIdRef": item.spaceId[1]}];

                document.getElementById('divSavContents').innerHTML += 'change adjacent space 2 for item  name: ' + item.name + '<br>';

            }

        }

    }


    if (getCTXValues().surfaceChanges.surfaceColors) {


        for (let type of GBX.surfaceTypes) {

            let color = getCTXValues().surfaceChanges.surfaceColors[type];
            //console.log( '', color );

            getGBXValues().colors[type] = color ? new THREE.Color(color.toLowerCase()) : getGBXValues().colors[type];

        }

        GBX.setAllVisible();

    }


    if (getCTXValues().surfaceChanges.groundPlane) {

        let meshGroundHelper = getTHRValues().scene.getObjectByName('groundHelper');
        const color = getCTXValues().surfaceChanges.groundPlane.color;
        let elevation = getCTXValues().surfaceChanges.groundPlane.elevation;

        if (!meshGroundHelper) {

            const bbox = new THREE.Box3().setFromObject(getGBXValues().surfaceMeshes);

            const geometry = new THREE.BoxBufferGeometry(3 * getGBXValues().surfaceMeshes.userData.radius, 3 * getGBXValues().surfaceMeshes.userData.radius, 1);
            const material = new THREE.MeshPhongMaterial({color: color, opacity: 0.85, transparent: true});
            meshGroundHelper = new THREE.Mesh(geometry, material);
            meshGroundHelper.name = 'groundHelper';
            meshGroundHelper.receiveShadow = true;
            meshGroundHelper.position.set(getTHRValues().axesHelper.position.x, getTHRValues().axesHelper.position.y, elevation);

            getTHRValues().scene.add(meshGroundHelper);


        }

    }


    if (getCTXValues().surfaceChanges.backgroundGradient === true) {
        SAV.setBackgroundGradient();
    }


};


SAV.setBackgroundGradient = function () {

    var col = function () {
        return (0.5 + 0.5 * Math.random()).toString(16).slice(2, 8);
    };
    var pt = function () {
        return (Math.random() * window.innerWidth).toFixed(0);
    };
    var image = document.body.style.backgroundImage;

    document.body.style.backgroundImage = image ? '' : 'radial-gradient( circle farthest-corner at ' +
        pt() + 'px ' + pt() + 'px, #' + col() + ' 0%, #' + col() + ' 50%, #' + col() + ' 100% ) ';

};


SAV.setDeletes = function () {

    for (let name of getCTXValues().surfaceChanges.deletes) {

        const surfaceXml = SAV.getSurfaceByName(name);

        console.log('surfaceXml', surfaceXml);

        if (!surfaceXml) {

            //console.log( 'id', id, surfaceXml );
            document.getElementById('divSavContents').innerHTML += 'Deletes - not found surface name: ' + name + '<br>';
            continue;

        } else {

            surfaceXml.remove();

            const surfaceMesh = getGBXValues().surfaceMeshes.children.find(element => element.userData.data.Name === name);
            getGBXValues().surfaceMeshes.remove(surfaceMesh);

            document.getElementById('divSavContents').innerHTML += 'Deleted surface name: ' + name + '<br>';

        }

    }

};


SAV.setDeleteSurfaceDuplicates = function () {


    for (let name of getCTXValues().surfaceChanges.deleteDuplicateSurfaces) {

        const surfaceXml = SAV.getSurfaceByName(name);

        //console.log( 'surfaceXml', surfaceXml);

        if (!surfaceXml) {

            //console.log( 'id', id, surfaceXml );
            document.getElementById('divSavContents').innerHTML += 'Deletes - not found surface name: ' + name + '<br>';
            continue;

        } else {

            surfaceXml.remove();

            const item = getGBXValues().surfaceJson.find(element => element.Name === name);
            const index = getGBXValues().surfaceJson.indexOf(item);

            if (index >= 0) {

                getGBXValues().surfaceJson.splice(index, 1);

            }

            const surfaceMesh = getGBXValues().surfaceMeshes.children.find(element => element.userData.data.Name === name);
            getGBXValues().surfaceMeshes.remove(surfaceMesh);

            document.getElementById('divSavContents').innerHTML += 'Deleted surface name: ' + name + '<br>';

        }

    }

    getCTXValues().surfaceChanges.deleteDuplicateSurfaces = [];
    //ISS.surfaceChanges.deleteDuplicateSurfaces = [];

};


SAV.addAttributesMissing = function () {

    let attributes = getCTXValues().surfaceChanges.addAttributesMissing;
    //console.log( 'attributes', attributes );

    for (let attrib in attributes) {


        getGBXValues().gbxml.setAttribute(attrib, attributes[attrib]);

        document.getElementById('divSavContents').innerHTML += `Added gbXML attribute ${attrib} with value of: ${attributes[attrib]}<br>`;
    }

};

SAV.initMenuSaveChanges =  (target)=> {

    document.getElementById(target).innerHTML =

        `<details id = 'detSaveChanges' class='app-menu' open>

			<summary>Save your changes to a file &nbsp</summary>

			<small><p>Save to file edits you make with right menu Heads-up Display. Apply your edits to next incoming gbXML source file update.</p></small>
			<p>
                <button onClick={SAV.initChanges}> Start a fresh session of save changes</button></p>

			<p><button onClick={SAV.initChanges}> View current changes</button></p>

			<p><button onClick={SAV.initChanges} > Save your changes to a file</button></p>

			<small>
                <p>
				Open a file of saved changes. Apply the edits to the current model
			</p></small>

			<p><input type="file" id="inpFile" onChange={openChanges(this)} /></p>

			<details id = "detSaveChangesSamples" className={"app-menu open"}>

			<summary>Save changes sample files</summary>



			<hr/>

		</details>`

};

SAV.initChanges();