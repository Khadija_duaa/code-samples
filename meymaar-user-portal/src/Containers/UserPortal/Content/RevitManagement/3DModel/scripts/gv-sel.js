import * as COR from './gv-cor';
import {getCORValues} from "./gv-cor";

import * as CTX from './gv-ctx';
import {getCTXValues} from "./gv-ctx";

import * as GBX from './gv-gbx';
import {getGBXValues} from "./gv-gbx";

import {getTHRValues} from "./gv-thr";

import * as THREE from "three/src/Three";


var SEL = {release: '14.0'};
SEL.item = null;
SEL.itemSurface = null;
SEL.spaceIndex = 0;
SEL.surfaceChanges = {};

export const getElementPanel =  (item) =>{

    item = item || {};

    item.divAttributes = item.divAttributes || 'SELdivSurface'; // used here
    item.divTarget = item.divTarget || 'SELdivElements'; // see divElement below
    item.element = item.element || 'Surface';  // used by SEL.setElementVisible
    item.name = item.name || 'itemName';
    item.optionValues = item.optionValues || [[item.id, item.Name, item.CADObjectId, 'yellow'], ['bbb', 2], ['ccc'], 3];
    // color here is naughty
    item.parent = item.parent || getGBXValues().surfacesJson; // used by SEL.setElementIdAttributes
    item.placeholder = item.placeholder || 'surface id';  // used below
    item.selItem = item.selItem || 'selItem'; // used below

    item.options = (item.element === "Surface" || item.element === "Openings") ?
        `<option selected >id</option><option >name</option>`
        :
        `<option>id</option><option selected >name</option>`;


    let options = '';

    item.optionValues.forEach(option =>
        options += '<option value=' + option[0] + ' title="id: ' + option[0] + '" style="background-color:' + option[3] + ';" >' + option[1] + '</option>'
    );

    SEL.item = item;
    //console.log( 'item', item );

    //const divTag = `<div id=${item.divTarget}></div>`;

    const divElement =

        `<div class=flex-container2  title="SEL${SEL.release}" >

				<div class=flex-div1 >
					<input oninput=SEL.setSelectedIndex(this,${item.selItem});
						placeholder="${ item.placeholder }" style=margin-bottom:0.5rem;width:6rem; >
					<br>
					<select id =${ item.selItem }
						 size=` + (item.optionValues.length < 10 ? item.optionValues.length : 10) +
        ` style=margin-bottom:0.5rem;min-width:6rem; >${options}</select>
					<br>
					<select onchange=SEL.setElementPanelSelect(this,${item.selItem},"${item.name}"); style=width:6rem; >
						${ item.options }
						<option>cad id</option>
					</select>
				</div>
				<div id = ${item.divAttributes} class=flex-left-div2 >Select an item to view its attributes</div>

			</div>`;

    console.log(item.divTarget);

    item.divTarget.innerHTML = divElement;


    const selectTarget = item.divTarget.getElementsByTagName('select')[0];
    //console.log( 'selectTarget', selectTarget );

    selectTarget.oninput = function () {

        SEL.setElementVisible(selectTarget.value, item);
        setElementIdAttributes(selectTarget.value, item);

    };

    selectTarget.onclick = selectTarget.oninput;

    return item;

};


////////// Set various element panel attributes

SEL.setElementPanelSelect = function (that, select, name) {


    let i = 0;

    const item = SEL[name] ? SEL[name] : SEL.item;  // to cover for NUM.setAreasByStorey

    let optionValues = item.optionValues;

    for (let option of select.options) {

        option.innerText = optionValues[i++][that.selectedIndex];

    }

};


SEL.setSelectedIndex = function (input, select) {

    const str = input.value.toLowerCase();

    // try using find
    for (let option of select.options) {

        if (option.innerHTML.toLowerCase().includes(str)) {

            select.value = option.value;

            return;

        }

    }

};


SEL.setElementVisible = function (id, item) {


    if (item.element === 'Surface') {

        setSurfaceVisible(id);

    } else if (item.element === 'Space') {

        SEL.setSpaceVisible(id);

    } else if (item.element === 'Storey') {

        SEL.setStoreyVisible(id);

    } else if (item.element === 'Zone') {

        SEL.setZoneVisible(id);

    } else if (item.element === 'Openings') {

        SEL.setOpeningVisible(id);

    }

};


export const setElementIdAttributes = function (id, item) {

    SEL.id = id;

    let arr = Array.isArray(item.parent) ? item.parent : [item.parent];

    const obj = arr.find(element => element.id === id);

    const divAttributes = document.getElementById(item.divAttributes);

    divAttributes.innerHTML = `<div>id: <b>${id}</b></div>`;

    for (let property in obj) {

        if (obj[property] !== null && typeof(obj[property]) === 'object') {

            if (property === 'AdjacentSpaceId') {

                //console.log( 'property', obj[ property ] );

                if (Array.isArray(obj[property])) {

                    divAttributes.innerHTML += SEL.getAttributeAdjacentSpace(obj[property][0].spaceIdRef, 1);
                    divAttributes.innerHTML += SEL.getAttributeAdjacentSpace(obj[property][1].spaceIdRef, 2);

                } else {

                    divAttributes.innerHTML += SEL.getAttributeAdjacentSpace(obj[property].spaceIdRef, 0);

                }

            }

        } else if (property === 'buildingStoreyIdRef' && obj[property]) {

            divAttributes.innerHTML += SEL.getAttributeStorey(obj[property]);

        } else if (item.element === 'Surface' && property === 'CADObjectId') {

            divAttributes.innerHTML += SEL.getAttributeCadObjectId(obj[property]);

        } else if (property === 'id' && obj[property]) {

            if (item.element === 'Openings') {

                divAttributes.innerHTML += SEL.getAttributeOpenings(obj[property]);

            } else if (item.element === 'Space') {

                divAttributes.innerHTML += SEL.getAttributeAdjacentSpace(obj[property], -1);

            } else if (item.element === 'Surface') {

                divAttributes.innerHTML += SEL.getAttributeSurfaceId(obj[property]);

            } else if (item.element === 'Storey') {

                divAttributes.innerHTML += SEL.getAttributeStorey(obj[property]);

            } else if (item.element === 'Zone') {

                divAttributes.innerHTML += SEL.getAttributeZone(obj[property]);

            } else {

                divAttributes.innerHTML += `<div><span class=attributeTitle >${property}:</span><br>
						<span class=attributeValue >${obj[property]}</span></div>`;

            }

        } else if (property === 'surfaceType') {

            divAttributes.innerHTML += SEL.getAttributeSurfaceType(obj[property]);

        } else if (property === 'zoneIdRef') {

            divAttributes.innerHTML += SEL.getAttributeZone(obj[property]);

        } else {

            divAttributes.innerHTML += `<div><span class=attributeTitle >${property}:</span>
					<span class=attributeValue >${obj[property]}</span></div>`;

        }

    }


    COR.setMenuButtonsClass(divAttributes);

};


/////


export const setPanelSurfaceAttributes = function (target, surfaceId) {

    const objects = getGBXValues().surfaceMeshes.visible === true ? getGBXValues().surfaceMeshes.children : getGBXValues().surfaceOpenings.children;
    const surfaceMesh = objects.find(element => element.userData.data.id === surfaceId);


    const recGeom = surfaceMesh.userData.data.RectangularGeometry;
    const height = parseFloat(recGeom.Height);
    const width = parseFloat(recGeom.Width);
    let openings = surfaceMesh.userData.data.Opening ? surfaceMesh.userData.data.Opening.length : 0;
    openings = openings ? openings : 0;

    target.innerHTML =

        `<details open >

			<summary>Surface ID: ` + surfaceId + `</summary>

			<div>rectangular width: ${width.toLocaleString()}</div>
			<div>rectangular height: ${height.toLocaleString()}</div>
			<div>rectangular area: ${(width * height).toLocaleString()}</div>
			<div>azimiuth: ${parseFloat(recGeom.Azimuth).toLocaleString()}</div>
			<div>tilt: ${parseFloat(recGeom.Tilt).toLocaleString()}</div>
			<div>vertices: ${surfaceMesh.geometry.vertices.length}</div>
			<div>openings: ${openings}</div>

		</details>

		<hr>`;

    //SEL.removeTelltales();
};


export const setPanelSpaceAttributes =  (target, spaceId, spaceIndex) =>{

    SEL.spaceIndex = spaceIndex >= 0 ? spaceIndex : SEL.spaceIndex;

    const item = {};

    console.log(target);
    document.getElementById(target).innerHTML =
        // 	<summary>Adjacent Space ` + ( spaceIndex > 0 ? spaceIndex : '' ) + `</summary>
        `<details open >

			<summary>Adjacent Space ` + (SEL.spaceIndex > -2 ? SEL.spaceIndex : '') + `</summary>

			<p>
				<button onclick=CTX.updateSpace(document.getElementById('SELselSpace').value,SEL.spaceIndex); >
				update the space associated with this surface</button>
			</p>

			<div id=SELdivSpace ></div>

			<div id=SELdivAtts ></div>
			

		</details>

		<hr>`;

    item.attribute = 'space';
    item.divAttributes = 'SELdivAtts';
    item.divTarget = document.getElementById('SELdivSpace');
    item.element = 'Space';
    item.name = 'itemSpace';
    //item.optionValues = item.optionValues;
    item.optionValues = getGBXValues().gbjson.Campus.Building.Space.map(item => [item.id, item.Name, item.CADObjectId]);
    item.parent = getGBXValues().gbjson.Campus.Building.Space;
    item.placeholder = 'space id';
    item.selItem = 'SELselSpace';

    //console.log( 'item.optionValues', item.optionValues);

    SEL.itemSpace = getElementPanel(item);

    const sel = document.getElementById(item.selItem);
    sel.value = spaceId;
    sel.click();

    SEL.setSpaceVisible(spaceId);



    COR.setMenuButtonsClass(document.getElementById('CORdivItemsRight'));

};


////////// get Attributes by individual items / All used by REP
// all used by REP

SEL.getAttributeAdjacentSpace = function (spaceIdRef, spaceIndex = 0) {
    console.log('getAttributeAdjacentSpace spaceIndex', spaceIndex);

    const txt =
        `<div>
			<span class=attributeTitle >adjacent space ` + (spaceIndex > -2 ? spaceIndex : "") + ` id</span>:<br>
			<!--
			<button onclick=SEL.setSpaceVisible("${spaceIdRef}"); >${spaceIdRef}</button>
			-->
			<button onClick=setPanelSpaceAttributes('CTXdivAttributes',"${spaceIdRef}",${ spaceIndex }); >${spaceIdRef}</button>

			<button onClick=SEL.setSpaceZoom("${spaceIdRef}",${spaceIndex}); >&#8981;</button>
		</div>`;

    return txt;


};


SEL.getAttributeCadObjectId = function (cadId) {
    //console.log( 'cadId', cadId );

    const txt =
        `<div>
			<span class=attributeTitle >cad object id</span>: <button onclick=SEL.setCadIdZoom("` + encodeURI(cadId) + `"); >&#8981;</button><br>
			<button id=buttId onclick=SEL.setCadObjectIdVisible(this.innerText); >${cadId}</button>
		</div>`;

    return txt;

};


SEL.getAttributeOpenings = function (openingId) {

    const txt =
        `<div>
			<span class=attributeTitle >id</span>:<br>
			<button onclick=SEL.setOpeningVisible("${openingId}"); >${openingId}</button>
			<button onclick=SEL.setOpeningZoom("${openingId}"); >&#8981;</button>
		</div>`;

    return txt;

};


SEL.getAttributeStorey = function (storeyId) {

    const txt =
        `<div>
			<span class=attributeTitle >storey id</span>:<br>
			<button onclick=SEL.setStoreyVisible("${storeyId}"); >${storeyId}</button>
			<button onclick=alert('could-be-added-if-needed'); >&#8981;</button>
		</div>`;

    return txt;

};


SEL.getAttributeSurfaceId = function (id) {

    const txt =
        `<div>
			<span class=attributeTitle >id</span>:<br>
			<button onclick=setSurfaceVisible(this.innerText); >${id}</button>
			<button onclick=SEL.setSurfaceZoom("${id}"); >&#8981;</button>
			<button onclick=SEL.setSurfaceVisibleToggle("${id}"); ><img src="images/assets/eye.png" height=12></button>
		</div>`;

    return txt;

};


SEL.getAttributeZone = function (zoneId) {

    const txt =
        `<div>
			<span class=attributeTitle >zone id</span>:<br>
			<button onclick=SEL.setZoneVisible(this.innerText); >${zoneId}</button>
		</div>`;

    //<button onclick=SEL.setZoneZoom("` + zoneId + `"); >&#8981;</button>

    return txt;

};


////////// get attributes by type
// used by REP

SEL.getAttributeSurfaceType = function (surfaceType) {

    const txt =
        `<div>
			<span class=attributeTitle >surface type</span>:<br>
			<button onclick=SEL.setSurfaceTypeVisible(this.innerText); >${surfaceType}</button>
			<button onclick=SEL.setSurfaceTypeZoom("` + surfaceType + `"); >&#8981;</button>
		</div>`;

    return txt;

};


////////// Show/Hide by Individual Elements
// used by REP

SEL.setCadObjectIdVisible = function (cadId) {

    cadId = decodeURI(cadId);

    SEL.setSurfaceGroupsVisible();

    getGBXValues().surfaceMeshes.children.forEach(element =>
        element.visible = element.userData.data.CADObjectId === cadId ? true : false);



    document.getElementById('CTXdivAttributes').innerHTML =

        `<details open>

				<summary>CAD Object ID</summary>

				<div><button id=SELbutCadId onclick=CTX.updateCadId('SELselCadId'); >Update cad object id of surface</button></div>

				<p><select id=SELselCadId size=10 ></select></p>

			</details>

			<hr>

			<details open>

				<summary>CAD Object Group</summary>

				<div><button id=SELbutCadGroup onclick=CTX.updateCadId('SELselCadGroup'); >Update cad object group of surface</button></div>

				<p id=SELdivCadIdGroup ></p>

			</details>

			<hr>`;


    SEL.setMenuPanelCadObjectsByType2('SELdivCadIdGroup', 'SELselCadGroup');


    const surfaces = getGBXValues().gbjson.Campus.Surface;
    const cadIds = [];

    for (let surface of surfaces) {

        //if ( !surface.CADObjectId ) { continue; }

        if (!surface.CADObjectId || typeof surface.CADObjectId !== 'string') {
            continue;

        }

        cadIds.push(surface.CADObjectId);
    }


    cadIds.sort();

    let txt = '';

    for (let id of cadIds) {

        txt += '<option>' + id + '</option>';

    }

    document.getElementById('SELselCadId').innerHTML = txt;

    document.getElementById('SELselCadId').value = cadId;

    //}

};


SEL.setOpeningVisible = function (openingId) {

    SEL.setSurfaceGroupsVisible(false, true, true);

    getGBXValues().surfaceOpenings.children.forEach(element => {

        element.visible = element.userData.data.id === openingId ? true : false;

        if (element.visible === true) {
            element.material.opacity = 1;
            element.material.side = 2;
            element.material.needsUpdate = true;
        }

    });

};


SEL.setSpaceVisible = function (spaceId) {

    SEL.setSurfaceGroupsVisible();

    for (let child of getGBXValues().surfaceMeshes.children) {

        child.visible = false;

        const adjacentSpaceId = child.userData.data.AdjacentSpaceId;
        //console.log( 'adjacentSpaceId', adjacentSpaceId );

        if (adjacentSpaceId && adjacentSpaceId.spaceIdRef && spaceId === adjacentSpaceId.spaceIdRef) {

            child.visible = true;

        } else if (Array.isArray(adjacentSpaceId) === true) {

            if (spaceId === adjacentSpaceId[0].spaceIdRef || spaceId === adjacentSpaceId[1].spaceIdRef) {

                child.visible = true;

            }

        }

    }

};


SEL.setStoreyVisible = function (storeyId) {

    SEL.setSurfaceGroupsVisible();

    const spaces = getGBXValues().gbjson.Campus.Building.Space;

    getGBXValues().surfaceMeshes.children.forEach(element => element.visible = false);

    for (let child of getGBXValues().surfaceMeshes.children) {

        const adjacentSpaceId = child.userData.data.AdjacentSpaceId;

        if (!adjacentSpaceId) {
            continue;
        }

        const spaceIdRef = Array.isArray(adjacentSpaceId) ? adjacentSpaceId[1].spaceIdRef : adjacentSpaceId.spaceIdRef;

        spaces.forEach(element => child.visible = element.id === spaceIdRef && element.buildingStoreyIdRef === storeyId ? true : child.visible);

    }

};


export const setSurfaceVisible =  (surfaceId) =>{

    SEL.setSurfaceGroupsVisible();

    getTHRValues().scene.remove(SEL.telltale);

    getGBXValues().surfaceMeshes.children.forEach(element => element.visible = element.userData.data.id === surfaceId );

    // testing

    const surface = getGBXValues().surfaceMeshes.children.find(item => item.userData.data.id === surfaceId);

    getCTXValues().intersected = surface;

    CTX.setHeadsUp();

   setPanelSurfaceAttributes( document.getElementById('CTXdivAttributes'), surfaceId );


};


SEL.setZoneVisible = function (zoneIdRef) {

    const spaces = getGBXValues().gbjson.Campus.Building.Space;

    SEL.setSurfaceGroupsVisible();

    getGBXValues().surfaceMeshes.children.forEach(element => element.visible = false);

    for (let child of getGBXValues().surfaceMeshes.children) {

        const adjacentSpaceId = child.userData.data.AdjacentSpaceId;
        //console.log( 'adjacentSpaceId', adjacentSpaceId );

        if (!adjacentSpaceId) {
            continue;
        }

        const spaceIdRef = Array.isArray(adjacentSpaceId) ? adjacentSpaceId[1].spaceIdRef : adjacentSpaceId.spaceIdRef;

        spaces.forEach(element => child.visible = element.id === spaceIdRef && element.zoneIdRef === zoneIdRef ? true : child.visible);

    }

    let zone;

    if (Array.isArray(getGBXValues().gbjson.Zone)) {

        zone = getGBXValues().gbjson.Zone.find(function (item) {
            return item.id === zoneIdRef;
        });

    } else {

        zone = getGBXValues().gbjson.Zone;

    }

};


///// Toggle Visible Show / Hide by Type of Element
// used by REP

SEL.setExposedToSunVisible = function () {

    SEL.setSurfaceGroupsVisible();

    getGBXValues().surfaceMeshes.children.forEach(element => element.visible = element.userData.data.exposedToSun === "true" );

};


SEL.setCadObjectTypeVisible = function (CADObjectGroupId) {
    // used by REP

    const cadId = CADObjectGroupId.trim();

    SEL.setSurfaceGroupsVisible();

    for (let child of getGBXValues().surfaceMeshes.children) {

        child.visible = false;

    }

    for (let child of getGBXValues().surfaceMeshes.children) {

        if (!child.userData.data.CADObjectId || typeof child.userData.data.CADObjectId !== 'string') {
            continue;
        }

        const id = child.userData.data.CADObjectId.replace(/\[(.*?)\]/gi, '').trim();

        if (id === cadId) {

            child.visible = true;

        } else {

            child.visible = false;

        }

    }

    if (document.getElementById('CTXdivAttributes')) {

        document.getElementById('CTXdivAttributes').innerHTML = '';

    }

};


SEL.setMenuPanelCadObjectsByType2 = function (target, selId) {

    const surfaces = getGBXValues().gbjson.Campus.Surface;
    const cadIds = [];

    for (let surface of surfaces) {

        if (!surface.CADObjectId || typeof surface.CADObjectId !== 'string') {
            continue;

        }

        const id = surface.CADObjectId.replace(/ \[(.*?)\]/gi, '').trim();

        if (!cadIds.includes(id)) {

            cadIds.push(id);

        }

    }

    cadIds.sort();

    let txt = '';

    for (let id of cadIds) {

        txt += '<option>' + id + '</option>';

    }

    const details = `<select id = "${selId}" size=10 >${txt}</select>`;

    document.getElementById(target).innerHTML = details;


    COR.setMenuButtonsClass(document.getElementById('CORdivItemsRight'));

};


SEL.setOpeningTypeVisible = function (type) {

    SEL.setSurfaceGroupsVisible(false, false, true);

    if (type) {

        getGBXValues().surfaceOpenings.children.forEach(element => element.visible = element.userData.data.openingType === type );

    } else {

        getGBXValues().surfaceOpenings.children.forEach(element => element.visible = true);

    }

};


////////// various toggles
// all used by REP

SEL.setSurfaceVisibleToggle = function (id) {

    getGBXValues().surfaceMeshes.visible = true;

    const surfaceMesh = getGBXValues().surfaceMeshes.children.find(element => element.userData.data.id === id);

    surfaceMesh.visible = !surfaceMesh.visible;

};


SEL.setSurfaceTypeVisible = function (type) {

    SEL.setSurfaceGroupsVisible();

    getGBXValues().surfaceMeshes.children.forEach(element => element.visible = element.userData.data.surfaceType === type);

    if (document.getElementById('CTXdivAttributes')) {

        document.getElementById('CTXdivAttributes').innerHTML =

            `<details open>

				<summary>Surface Type: ${type}</summary>

				<p><button onclick=CTX.updateSurfaceType() >Update surface type of the surface</button></p>

				<div><select id="SELselSurfaceType" /></div>


			</details>

			<hr>`;

    }

    //const surfaces = getGBXValues().gbjson.Campus.Surface;

    let txt = '';
    /*
    // gathers only types that are in use
    // could be use to highlight the full list
    const types = [];
    const typesCount = [];

    for ( let surface of surfaces ) {

        const index = types.indexOf( surface.surfaceType );

        if ( index < 0 ) {

            types.push( surface.surfaceType );
            typesCount[ types.length - 1 ] = 1;

        } else {

            typesCount[ index ] ++;

        }

    }
    */

    //for ( let i = 0; i < types.length; i++ ) {
    for (let i = 0; i < getGBXValues().surfaceTypes.length; i++) {

        txt +=
            `<option>${getGBXValues().surfaceTypes[i]}</option>`;

    }


    document.getElementById('SELselSurfaceType').innerHTML = txt;
    document.getElementById('SELselSurfaceType').size = getGBXValues().surfaceTypes.length;

    COR.setMenuButtonsClass(document.getElementById('CORdivItemsRight'));

};


SEL.setSurfaceTypeInvisible = function (button) {


    for (let child of getGBXValues().surfaceMeshes.children) {

        if (!child.userData.data) {
            continue;
        }

        if (child.userData.data.surfaceType === button.value && button.style.backgroundColor === getCORValues().colorButtonToggle) {

            child.visible = false;

        } else if (child.userData.data.surfaceType === button.value) {

            child.visible = true;

        }

    }

};


////////// Zoom

SEL.setCameraControls = function (meshes) {

    const bbox = new THREE.Box3();
    meshes.forEach(mesh => bbox.expandByObject(mesh));

    const sphere = bbox.getBoundingSphere(new THREE.Sphere());
    const center = sphere.center;
    const radius = sphere.radius;
    //console.log( 'center * radius', center, radius );

    getTHRValues().controls.target.copy(center);
    getTHRValues().camera.position.copy(center.clone().add(new THREE.Vector3(1.5 * radius, -1.5 * radius, 1.5 * radius)));

};


SEL.setCadIdZoom = function (cadId) {

    cadId = decodeURI(cadId);
    SEL.setCadObjectIdVisible(cadId);

    let meshes = getGBXValues().surfaceMeshes.children.filter(element => element.userData.data.CADObjectId === cadId);

    SEL.setCameraControls(meshes);

};


SEL.setOpeningZoom = function (openingId) {

    SEL.setOpeningVisible(openingId);

    let openings = getGBXValues().surfaceOpenings.children.find(element => element.userData.data.id === openingId);
    //console.log( 'openings', openings );

    openings = Array.isArray(openings) ? openings : [openings];

    SEL.setCameraControls(openings);

};


SEL.setSpaceZoom = function (id, index = 0) {

    SEL.setSpaceVisible(id, index);

    let meshes = [];

    for (let child of getGBXValues().surfaceMeshes.children) {

        const adjacentSpaceId = child.userData.data.AdjacentSpaceId;

        if (adjacentSpaceId && adjacentSpaceId.spaceIdRef && id === adjacentSpaceId.spaceIdRef) {

            meshes.push(child);

        } else if (Array.isArray(adjacentSpaceId) === true) {

            if (id === adjacentSpaceId[0].spaceIdRef || id === adjacentSpaceId[1].spaceIdRef) {

                meshes.push(child);

            }

        }

    }

    SEL.setCameraControls(meshes);

};


SEL.setSurfaceZoom = function (id) {

    const surfaceMesh = getGBXValues().surfaceMeshes.children.find(element => element.userData.data.id === id);

    SEL.setCameraControls([surfaceMesh]);

    getTHRValues().scene.remove(SEL.telltale);
    const geometry = new THREE.BoxGeometry(0.1, 0.1, 0.1);
    const material = new THREE.MeshNormalMaterial({opacity: 0.7, transparent: true});
    SEL.telltale = new THREE.Mesh(geometry, material);
    SEL.telltale.position.copy(getTHRValues().controls.target);
    getTHRValues().scene.add(SEL.telltale);

};


SEL.setSurfaceTypeZoom = function (surfaceType) {

    SEL.setSurfaceTypeVisible(surfaceType);

    let meshes = getGBXValues().surfaceMeshes.children.filter(element => element.userData.data.surfaceType === surfaceType);

    SEL.setCameraControls(meshes);

};


//////////


export const setPanelShowHide =  (target)=> { // move to GBX
    // used by CTX/ISS/REP

    document.getElementById(target).innerHTML =

        `<details open >

			<summary>Show || Hide / Zoom &nbsp</summary>

			<button onclick=SEL.toggleSurfacesVisible(); >surfaces</button>
				<button onclick=getGBXValues().surfaceEdges.visible=!getGBXValues().surfaceEdges.visible; >edges</button>
				<button onclick=getGBXValues().surfaceOpenings.visible=!getGBXValues().surfaceOpenings.visible; title="toggle the windows" >openings</button>
				<button onclick=GBX.setAllVisible(); >all</button>
				/
				<button onclick=SEL.setBuildingZoom(); >zoom all</button>

		</details>`;

};


SEL.toggleSurfacesVisible = function () {

    getGBXValues().surfaceMeshes.visible = !getGBXValues().surfaceMeshes.visible;

    getGBXValues().surfaceMeshes.children.forEach(child => child.visible = getGBXValues().surfaceMeshes.visible);

};


SEL.setSurfaceGroupsVisible = function (meshesVis = true, edgesVis = true, openingsVis = false) {

    getGBXValues().surfaceMeshes.visible = meshesVis;
    getGBXValues().surfaceEdges.visible = edgesVis;
    getGBXValues().surfaceOpenings.visible = openingsVis;

};


SEL.removeTelltales = function () {

    getTHRValues().scene.remove(getCTXValues().telltalesPolyloop);
    getTHRValues().scene.remove(getCTXValues().telltalesVertex);
    document.getElementById('CTXdivCoordinates').innerHTML = 'click a button';

};


SEL.setZoneZoom = function (zoneId) {

    SEL.setZoneVisible(zoneId);

    let meshes = getGBXValues().surfaceMeshes.children.filter(element => element.userData.data.zoneIdRef === zoneId);
    meshes = meshes.map(item => item.clone());

    const surfaceMeshes = new THREE.Object3D();
    surfaceMeshes.add(...meshes);

    const bbox = new THREE.Box3().setFromObject(surfaceMeshes);
    const sphere = bbox.getBoundingSphere();
    const center = sphere.center;
    const radius = sphere.radius;

    getTHRValues().controls.target.copy(center);
    getTHRValues().camera.position.copy(center.clone().add(new THREE.Vector3(1.5 * radius, -1.5 * radius, 1.5 * radius)));


};


SEL.setBuildingZoom = function (cadId) {

    const bbox = new THREE.Box3().setFromObject(getGBXValues().surfaceOpenings);
    const sphere = bbox.getBoundingSphere();
    const center = sphere.center;
    const radius = sphere.radius;

    getTHRValues().controls.target.copy(center);
    getTHRValues().camera.position.copy(center.clone().add(new THREE.Vector3(1.2 * radius, -1.2 * radius, 1.2 * radius)));

};

export const getSELValues = ()=>{
  return {
      itemSurface : SEL.itemSurface,
      id:SEL.id,
      surfaceChanges : SEL.surfaceChanges
  }
};