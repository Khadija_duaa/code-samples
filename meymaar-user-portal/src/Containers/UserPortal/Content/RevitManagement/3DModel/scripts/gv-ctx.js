import {getGBXValues} from "./gv-gbx";
import * as COR from './gv-cor';

import * as THR from './gv-thr';
import {getTHRValues} from "./gv-thr";

import * as SEL from './gv-sel';
import {getSELValues} from "./gv-sel";

import * as THREE from "three/src/Three";


var CTX = {release: '14.0'};

CTX.surfaceChanges = {};


export const initHeadsUp = () =>{
    CTX.mouse = new THREE.Vector2();
    getTHRValues().renderer.domElement.addEventListener('click', onRendererMouseMove, false);
    getTHRValues().renderer.domElement.addEventListener('touchstart', onRendererTouchStart, false);
};


export const onRendererMouseMove =  (event)=> {

    event.preventDefault();

    if (event.buttons > 0) {
        return;
    }

    CTX.mouse.x = (event.clientX / getTHRValues().renderer.domElement.clientWidth) * 2 - 1;
    CTX.mouse.y = -(event.clientY / getTHRValues().renderer.domElement.clientHeight) * 2 + 1;

    const raycaster = new THREE.Raycaster();
    raycaster.setFromCamera(CTX.mouse, getTHRValues().camera);

    let surfaceMeshes = getGBXValues().surfaceMeshes;
    let surfaceOpenings = getGBXValues().surfaceOpenings;

    const objects = surfaceMeshes && surfaceMeshes.visible === true ? surfaceMeshes.children : surfaceOpenings && surfaceOpenings.children;
    const intersects = raycaster.intersectObjects(objects);

    if (intersects.length > 0) {

        if (CTX.intersected !== intersects[0].object) {
            console.log( 'CTX.intersected', CTX.intersected );

            if (CTX.intersected && CTX.intersected.material.emissive) {
                CTX.intersected.material.emissive.setHex(CTX.intersected.currentHex);
            }
            if (CTX.intersected) {
                CTX.intersected.material.opacity = CTX.intersected.currentOpacity;
            }

            CTX.intersected = intersects[0].object;

            console.log('CTX.intersected', CTX.intersected);

            setHeadsUp(event);

            if (CTX.intersected.material.emissive) {

                CTX.intersected.currentHex = CTX.intersected.material.emissive.getHex();
                CTX.intersected.material.emissive.setHex(0x440000);

            }

            CTX.intersected.currentOpacity = CTX.intersected.material.opacity;
            CTX.intersected.material.opacity = 1;

        }

    } else {

        if (CTX.intersected && CTX.intersected.material.emissive) {
            CTX.intersected.material.emissive.setHex(CTX.intersected.currentHex);
        }
        if (CTX.intersected) {
            CTX.intersected.material.opacity = CTX.intersected.currentOpacity;
        }

        CTX.intersected = undefined;
        document.getElementById('CORdivMenuRight').style.display = 'none';

    }

};


CTX.xxxxonRendererMouseDown = function (event) {

    getTHRValues().renderer.domElement.removeEventListener('click', onRendererMouseMove, false);

};


export const onRendererTouchStart = (event) =>{

    event.preventDefault();

    event.clientX = event.touches[0].clientX;
    event.clientY = event.touches[0].clientY;

    onRendererMouseMove(event);

};


export const setHeadsUp = (event) =>{

    document.getElementById('CORdivMenuRight').style.display = 'block';
    document.getElementById('CORdivMenuRight').style.width = '20rem';
    document.getElementById('CORdivMenuRight').style.left = 'calc( 100% - 22rem )';


    document.getElementById('CORdivItemsRight').innerHTML =
        `
			<div id="CTXdivShowHide" class="mnuRightDiv" ></div>
			<div id="CTXdivEditSurface" class="mnuRightDiv" ></div>
			<div id="CTXdivItems" class=mnuRightDiv" ></div>
			<div id="CTXdivAttributes" class=mnuRightDiv" ></div>
			<div id="CTXdivTellTales" class="mnuRightDiv" ></div>
			`;

    SEL.setPanelShowHide('CTXdivShowHide');

    setPanelEditSurface('CTXdivEditSurface');

    setPanelSurface('CTXdivItems');

    setPanelTellTale('CTXdivTellTales');

    COR.setMenuButtonsClass(document.getElementById('CORdivItemsRight'));

};


export const setPanelSurface =  (target)=> {

    getTHRValues().controls.keys = false;

    const title = getGBXValues().surfaceMeshes.visible === true ? 'Surfaces' : 'Openings';

    document.getElementById(target).innerHTML =

        `<details open >

			<summary title="CTX${CTX.release}" >${ title } &nbsp; <a href=#../gv-CTX-context-menu/README.md >?</a></summary>

			<div id = "CTXdivPanelSurface" ></div>

			<hr/>

		</details>`;


    const item = getGBXValues().surfaceMeshes.visible === true ? getItemSurface() : getItemOpening();

    getSELValues().itemSurface = SEL.getElementPanel(item);


    console.log('CTX.intersected 222', CTX.intersected);

    const data = CTX.intersected.userData.data;

    let CTXselItemId = document.getElementById('CTXselItemId');
    let CTXdivAttributes = document.getElementById('CTXdivAttributes');

    CTXselItemId.value = data.id;

    SEL.setElementIdAttributes(CTXselItemId.value, item);
    SEL.setPanelSurfaceAttributes(CTXdivAttributes, data.id);

};


export const getItemSurface =  () =>{

    let item = {};
    item.attribute = 'id';
    item.divAttributes = 'CTXdivCardSurfaceAttributes';
    item.divTarget = document.getElementById('CTXdivPanelSurface');
    item.element = 'Surface';
    item.name = 'itemSurface';
    item.optionValues = getGBXValues().surfacesJson.map(item => [item.id, item.Name, item.CADObjectId]);
    item.parent = getGBXValues().surfacesJson;
    item.placeholder = 'surface id';
    item.selItem = 'CTXselItemId';

    return item;

};


export const getItemOpening =  () =>{

    let item = {};
    getGBXValues().openingsJSON = getGBXValues().surfaceOpenings.children.map(item => item.userData.data);

    item.attribute = 'id';
    item.divAttributes = 'CTXdivCardSurfaceAttributes';
    item.divTarget = document.getElementById('CTXdivPanelSurface');
    item.element = 'Opening';
    item.name = 'itemOpening';
    item.optionValues = getGBXValues().surfaceOpenings.children.map(item => [item.userData.data.id, item.userData.data.Name, item.userData.data.CADObjectId]);
    item.parent = getGBXValues().openingsJSON;
    item.placeholder = 'opening id';
    item.selItem = 'CTXselItemId';

    return item;

};


///// Surface Coordinates

export const setPanelTellTale =  (target) =>{

    document.getElementById(target).innerHTML =
        `<details>
			<summary>Surface Coordinates</summary>
			<p>
				<button onclick=CTX.displayTelltalesPolyloop(); title="gbXML data" >gbXML coordinates</button>
				<button onclick=CTX.displayTelltalesVertex(); title="Three.js data" >Three.js vertices</button>
				<button onclick=CTX.removeTelltales(); >remove telltales</button>
			<p>
			<div id=CTXdivCoordinates ></div>

		</details>`;

};


CTX.setCoordinateData = function () {

    let vertex = CTX.telltalesMeshes.children[document.getElementById('CTXselCoordinate').selectedIndex].position;
    console.log('vertex', vertex);

    let x = vertex;

    document.getElementById('CTXdivCoordinatesData').innerHTML =
        `
		X = ${vertex.x} <br>
		Y = ${vertex.y} <br>
		Z = ${vertex.z} <br>
		<p><button onclick=alert("Coming-soon"); >delete</button></p>`;

};


CTX.displayTelltalesPolyloop = function () {

    getTHRValues().scene.remove(CTX.telltalesPolyloop);

    //if( !CTX.intersected ) { return; }
    if (!getSELValues().id) {
        return;
    }

    CTX.telltalesPolyloop = new THREE.Object3D();
    CTX.telltalesMeshes = new THREE.Object3D();

    const surfacesJson = getGBXValues().surfacesJson.find(item => item.id === getSELValues().id);

    const vertices = surfacesJson.PlanarGeometry.PolyLoop.CartesianPoint;

    console.log('vertices', vertices);

    //const vertices = CTX.intersected.userData.data.PlanarGeometry.PolyLoop.CartesianPoint;

    let options = '';

    for (let i = 0; i < vertices.length; i++) {

        const vertex = vertices[i].Coordinate;
        const geometry = new THREE.BoxGeometry(0.1, 0.1, 0.1);
        const material = new THREE.MeshNormalMaterial();
        const mesh = new THREE.Mesh(geometry, material);
        // console.log( 'vertex', vertex );

        mesh.position.set(parseFloat(vertex[0]), parseFloat(vertex[1]), parseFloat(vertex[2]));

        let placard = THR.drawPlacard(i.toString(), 0.01, 200, parseFloat(vertex[0]) + 0.5, parseFloat(vertex[1]) + 0.5, parseFloat(vertex[2]) + 0.5);
        // console.log( 'placard', placard );
        CTX.telltalesPolyloop.add(placard);
        CTX.telltalesMeshes.add(mesh);

        options += '<option value=${vertex} > coordinate ' + (i + 1) + '</option>';

    }

    const openings = surfacesJson.Opening ? surfacesJson.Opening : [];

    //const openings = surfacesJson.PlanarGeometry.PolyLoop.CartesianPoint;

    for (let i = 0; i < openings.length; i++) {

        const opening = openings[i];
        //console.log( 'opening', opening );

        const vertices = opening.PlanarGeometry.PolyLoop.CartesianPoint;
        //console.log( 'vertices', vertices );

        for (let i = 0; i < vertices.length; i++) {

            const vertex = vertices[i].Coordinate;
            const geometry = new THREE.BoxGeometry(0.1, 0.1, 0.1);
            const material = new THREE.MeshNormalMaterial();
            const mesh = new THREE.Mesh(geometry, material);
            // console.log( 'vertex', vertex );

            mesh.position.set(parseFloat(vertex[0]), parseFloat(vertex[1]), parseFloat(vertex[2]));

           let placard = CTX.drawPlacard(i.toString(), 0.01, 10, parseFloat(vertex[0]) + 0.5, parseFloat(vertex[1]) + 0.5, parseFloat(vertex[2]) + 0.5);
            // console.log( 'placard', placard );
            CTX.telltalesPolyloop.add(placard);
            CTX.telltalesPolyloop.add(mesh);

        }

    }

    getTHRValues().scene.add(CTX.telltalesPolyloop, CTX.telltalesMeshes);

    document.getElementById('CTXdivCoordinates').innerHTML =

        `<div class=flex-container2 >

			<div class=flex-div1 >
				<p><select id=CTXselCoordinate onclick=CTX.setCoordinateData(); onchange=CTX.setCoordinateData(); size=6 style=min-width:6rem; >${options}</select></p>
			</div>

			<div id =CTXdivCoordinatesData class=flex-left-div2 >click a coordinate</div>

		</div>`;


};


CTX.displayTelltalesVertex = function () {

    getTHRValues().scene.remove(CTX.telltalesVertex);

    //if( !CTX.intersected ) { return; }
    if (!getSELValues().id) {
        return;
    }

    CTX.telltalesVertex = new THREE.Object3D();

    const surfaceMesh = getGBXValues().surfaceMeshes.children.find(item => item.userData.data.id === getSELValues().id);

    const vertices = CTX.intersected.geometry.vertices;

    let options = '';

    for (let i = 0; i < vertices.length; i++) {

        const vertex = vertices[i];
        const geometry = new THREE.BoxGeometry(0.1, 0.1, 0.1);
        geometry.applyMatrix(new THREE.Matrix4().makeTranslation(vertex.x, vertex.y, vertex.z));
        const material = new THREE.MeshNormalMaterial();
        const mesh = new THREE.Mesh(geometry, material);
        mesh.position.copy(CTX.intersected.position);
        mesh.quaternion.copy(CTX.intersected.quaternion);

        let placard = THR.drawPlacard(i.toString(), 0.01, 120, vertex.x, vertex.y, vertex.z + 0.5);
        placard.position.copy(CTX.intersected.position);
        placard.quaternion.copy(CTX.intersected.quaternion);

        // console.log( 'placard', placard );
        CTX.telltalesVertex.add(placard);
        CTX.telltalesVertex.add(mesh);

        options += '<option value=${vertex} > coordinate ' + (i + 1) + '</option>';

    }

    getTHRValues().scene.add(CTX.telltalesVertex);


};


CTX.removeTelltales = function () {

    getTHRValues().scene.remove(CTX.telltalesPolyloop);
    getTHRValues().scene.remove(CTX.telltalesVertex);
    document.getElementById('CTXdivCoordinates').innerHTML = 'click a button';

};


////////// Editing

export const  deleteSurface =  () =>{

    console.log("Delete");
    const id = document.getElementById('CTXselItemId').value;
    const proceed = window.confirm( 'OK to delete surface: ' + id + '?' );

    if ( !proceed ){ return; }


    if (!CTX.surfaceChanges.deletes) {
        CTX.surfaceChanges.deletes = [];
    }

    // remove from gbxml
    const surfacesResponse = getGBXValues().gbxml.getElementsByTagName("Surface");
    const surfaceXml = surfacesResponse[id];
    //console.log( 'id', id,'\nsurface to delete', surfaceXml );

    const name = surfaceXml.getElementsByTagName("Name")[0].innerHTML;
    //console.log( 'name', name );

    CTX.surfaceChanges.deletes.push(name);

    surfaceXml.remove();


    // remove from gbjson
    getGBXValues().surfacesJson = getGBXValues().surfacesJson.filter(element => element.id != id);

    // remove from three.js
    const surfaceMesh = getGBXValues().surfaceMeshes.children.find(element => element.userData.data.id === id);
    getGBXValues().surfaceMeshes.remove(surfaceMesh);


    const element = document.getElementById('divSurface' + id);
    // console.log( 'element', element );

    if (element) {

        element.innerHTML = '<p>Surface deleted</p>' + element.innerHTML;
        element.style.opacity = 0.2;

    }

};


CTX.addModifiedBy = function () {

    // not adding spaces and new lines nicely. Why?

    const documentHistoryXml = getGBXValues().gbxmlResponseXML.getElementsByTagName("DocumentHistory");

    const programInfoNew = getGBXValues().gbxmlResponseXML.createElement("ProgramInfo");

    programInfoNew.setAttribute("id", "ladybug-tools-spider");

    documentHistoryXml[0].appendChild(programInfoNew);

    const productNameNew = getGBXValues().gbxmlResponseXML.createElement("ProductName");

    const newText = getGBXValues().gbxmlResponseXML.createTextNode('Ladybug-Tools/spider');

    productNameNew.appendChild(newText);

    programInfoNew.appendChild(productNameNew);

    productNameNew.nodeValue = 'Ladybug-Tools/spider';


    const modifiedByNew = getGBXValues().gbxmlResponseXML.createElement("ModifiedBy");

    modifiedByNew.setAttribute("personId", "Your name");

    modifiedByNew.setAttribute("programId", "ladybug-tools-spider");

    modifiedByNew.setAttribute("date", (new Date()).toISOString());

    documentHistoryXml[0].appendChild(modifiedByNew);

    alert('Adding to gbXML:\n\n' + getGBXValues().gbxmlResponseXML.getElementsByTagName("ModifiedBy")[0].outerHTML);

};


CTX.saveFile = function () {

    //xmlText = prettifyXml( gbxmlResponseXML ); // not
    let xmlText = new XMLSerializer().serializeToString(getGBXValues().gbxml);

    xmlText = xmlText.replace(/encoding\=\"utf\-16\"/gi, '');
    //console.log( 'xmlText', xmlText );

    var blob = new Blob([xmlText]);
    var a = document.body.appendChild(document.createElement('a'));
    a.href = window.URL.createObjectURL(blob);
    a.download = getGBXValues().gbjson.Campus.Building.id + '.xml';
    a.click();
    //		delete a;
    a = null;

};


///////// Editing Elements after push update button

CTX.updateSurface = function (id) {
    // not used??

    getGBXValues().surfaceMeshes.children.forEach(function (element) {
        element.visible = element.userData.data.id === id ? true : false;
    });

    const surfaceMesh = getGBXValues().surfaceMeshes.children.find((element) => element.userData.data.id === id);
    console.log('surfaceMesh', surfaceMesh);

    CTX.intersected = surfaceMesh;

};

export const setPanelEditSurface = (target) =>{

    document.getElementById(target).innerHTML =
        `<details open>

			<summary>Edit the Surface</summary>

			<button class=toggle onclick=this.deleteSurface(); >delete surface</button>
				<button onclick=CTX.addModifiedBy(); title='add name, app, date and time of the edits' >modified by </button>
				<button onclick=CTX.saveFile(); title="creates a new file with the changes" >save edits</button>

			<hr>

		</details>`;

};



export const updateSpace = function (spaceId, spaceRef) {

    console.log('spaceId', spaceId);
    console.log('spaceRef', spaceRef);

    const surfaceJson = CTX.intersected.userData.data;
    const surfaceName = surfaceJson.Name;

    CTX.surfacesXml = getGBXValues().gbxml.getElementsByTagName("Surface");

    const surfaceXml = CTX.surfacesXml[surfaceJson.id];

    if (spaceRef === 0) {

        const spaceId = document.getElementById('SELselSpace').value;
        surfaceJson.AdjacentSpaceId.spaceIdRef = spaceId;

        console.log('surfaceXml', surfaceXml);

       let space = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];
        space.setAttribute("spaceIdRef", spaceId);


        if (!getSELValues().surfaceChanges.oneAdjacent) {
            getSELValues().surfaceChanges.oneAdjacent = [];
        }
        getSELValues().surfaceChanges.oneAdjacent.push({name: surfaceName, spaceId: spaceId});

    } else if (spaceRef === 1) {

        const spaceId = document.getElementById('SELselSpace').value;
        console.log('spaceId', spaceId);

        surfaceJson.AdjacentSpaceId[0].spaceIdRef = spaceId;
        //SELbutSpaceVis1.innerText = spaceId;

        console.log('surfaceXml', surfaceXml);

        let space = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];
        space.setAttribute("spaceIdRef", spaceId);


        if (!getSELValues().surfaceChanges.twoAdjacent) {
            getSELValues().surfaceChanges.twoAdjacent = [];
        }
        getSELValues().surfaceChanges.twoAdjacent.push({
            name: surfaceName,
            spaceId: [surfaceJson.AdjacentSpaceId[0].spaceIdRef, surfaceJson.AdjacentSpaceId[1].spaceIdRef]
        });

    } else if (spaceRef === 2) {

        const spaceId = document.getElementById('SELselSpace').value;
        surfaceJson.AdjacentSpaceId[1].spaceIdRef = spaceId;

        console.log('surfaceXml', surfaceXml);

       let space = surfaceXml.getElementsByTagName("AdjacentSpaceId")[1];
        space.setAttribute("spaceIdRef", spaceId);


        if (!getSELValues().surfaceChanges.twoAdjacent) {
            getSELValues().surfaceChanges.twoAdjacent = [];
        }
        getSELValues().surfaceChanges.twoAdjacent.push({
            name: surfaceName,
            spaceId: [surfaceJson.AdjacentSpaceId[0].spaceIdRef, surfaceJson.AdjacentSpaceId[1].spaceIdRef]
        });

    }

    console.log('surfaceJson', surfaceJson);


    setHeadsUp();

};


export const updateSurfaceType = () =>{

    const surface = CTX.intersected.userData.data;

    const id = surface.id;
    const spaceIdPrev = surface.AdjacentSpaceId;

    const typeNew = surface.surfaceType = document.getElementById('SELselSurfaceType').value;

    if (!CTX.surfaceChanges.surfaceTypes) {
        CTX.surfaceChanges.surfaceTypes = [];
    }
    CTX.surfaceChanges.surfaceTypes.push({name: surface.Name, surfaceType: typeNew});

    CTX.surfacesXml = getGBXValues().gbxml.getElementsByTagName("Surface");

    let surfaceXml = CTX.surfacesXml[id];

    surfaceXml.attributes.getNamedItem('surfaceType').nodeValue = typeNew;

    let surfaceMesh = getGBXValues().surfaceMeshes.children.find((element) => element.userData.data.id === id);
    surfaceMesh.material.color.setHex(getGBXValues().colors[typeNew]);
    surfaceMesh.material.needsUpdate = true;

    let surfaceJson = surfaceMesh.userData.data;

    const types = ['InteriorWall', 'InteriorFloor', 'Ceiling', 'Air', 'UndergroundCeiling', 'RaisedFloor'];

    if (typeNew === 'Shade') {

        // json
        delete surfaceJson.AdjacentSpaceId;

        // xml
        if (Array.isArray(spaceIdPrev) === true) { // type prev is two adjacents

            const adjSpace1 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[1];

            const removedId1 = adjSpace1.getAttribute('spaceIdRef');
            const removed1 = surfaceXml.removeChild(adjSpace1);

            const adjSpace2 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];

            const removedId2 = adjSpace2.getAttribute('spaceIdRef');
            const removed2 = surfaceXml.removeChild(adjSpace2);


            console.log('old 2 / new 0 / removed id1: ', removedId1, ' id2: ', removedId2, surfaceXml);

        } else { // type prev is single adjacent

            const adjSpace1 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];
            //console.log( 'spaceId',  spaceId);

            const removedId1 = adjSpace1.getAttribute('spaceIdRef');
            const removed1 = surfaceXml.removeChild(adjSpace1);

            console.log('old 1 / new 0 / id: ', removedId1, surfaceXml);

        }

    } else if (types.includes(typeNew)) { // type new is two adjacents


        if (Array.isArray(spaceIdPrev) === true) { // type prev is two adjacents

            // leave things untouched
            //console.log( ' prev 2 / now 2 spaceIdPrev', spaceIdPrev );

        } else if (spaceIdPrev) { // type prev is single adjacent

            //surfaceJson.AdjacentSpaceId = spaceIdPrev; //{ spaceIdRef: spaceIdPrev };
            let prevAdj = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];
            const prevId = prevAdj.getAttribute('spaceIdRef');

            surfaceJson.AdjacentSpaceId = [];
            let adjacentSpaceId = surfaceJson.AdjacentSpaceId;
            adjacentSpaceId[0] = {spaceIdRef: prevId};
            adjacentSpaceId[1] = {spaceIdRef: 'none'};

            console.log('old 1 / new 2 / prevId', prevId, surfaceXml);

        } else { // type prev is shade / no adjacent

            //surfaceJson.AdjacentSpaceId = { spaceIdRef: 'none' };

            surfaceJson.AdjacentSpaceId = [{"spaceIdRef": "none"}, {"spaceIdRef": "none"}];


            console.log('old 0 / new 2 / adjacentSpaceId', surfaceJson.adjacentSpaceId);

        }

    } else { // type new is single adjacent

        if (Array.isArray(spaceIdPrev) === true) { // type prev is two adjacents

            const adjacentXml2 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[1];
            const removed2 = surfaceXml.removeChild(adjacentXml2);

            const adjacentXml1 = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];
            const removed1 = surfaceXml.removeChild(adjacentXml1);

            const newAdj = getGBXValues().gbxmlResponseXML.createElement("AdjacentSpaceId");
            newAdj.setAttribute("spaceIdRef", spaceIdPrev[0].spaceIdRef);
            const newAdjTxt = surfaceXml.appendChild(newAdj);

            surfaceJson.AdjacentSpaceId = {spaceIdRef: spaceIdPrev[0].spaceIdRef};

            console.log('old 2 / new 1', newAdjTxt, surfaceXml);

        } else if (spaceIdPrev) { // type prev is single adjacent

            // leave things untouched
            const spaceId = surfaceXml.getElementsByTagName("AdjacentSpaceId")[0];

            console.log('old 1 / new 1 / no changes spaceId', spaceId, surfaceXml);

        } else { // type prev is no adjacent



            surfaceJson.AdjacentSpaceId = {spaceIdRef: 'none'};

            //surfaceMesh.userData.data.AdjacentSpaceId = 'none';
            console.log('old 0 / new 1 / no spaceIdPrev', spaceIdPrev, surfaceXml);

        }

    }

    //console.log( 'surfaceXml',  surfaceXml );
    //console.log( 'type surfaceJson', surfaceJson );

    setHeadsUp();
    SEL.setSurfaceVisible(id);

};


export const updateCadId = function (that) {

    let thatValue = document.getElementById(that);
    const surface = CTX.intersected.userData.data;

    const id = surface.id;

    CTX.surfacesXml = getGBXValues().gbxml.getElementsByTagName("Surface");

    let surfaceXml = CTX.surfacesXml[id];

    const cadObjId = surfaceXml.getElementsByTagName("CADObjectId")[0];

    console.log('cadObjId', cadObjId);

    if (cadObjId) {

        //surfaceXml.attributes.getNamedItem( 'CADObjectId' ).nodeValue = that.value;

        //cadObjId.innerHTML = that.value;


        surfaceXml.getElementsByTagName("CADObjectId")[0].innerHTML = thatValue.value;
        //console.log( 'surfaceXml',  surfaceXml );

       let surfaceMesh = getGBXValues().surfaceMeshes.children.find((element) => element.userData.data.id === id);

        surfaceMesh.userData.data.CADObjectId = thatValue.value;

        if (!CTX.surfaceChanges.CADObjectId) {
            CTX.surfaceChanges.CADObjectId = [];
        }
        CTX.surfaceChanges.CADObjectId.push({name: surface.Name, cadId: thatValue.value});

        setHeadsUp();

    } else {


        surfaceXml.setAttribute("CADObjectId", thatValue.value);


        let surfaceMesh = getGBXValues().surfaceMeshes.children.find((element) => element.userData.data.id === id);
        surfaceMesh.userData.data.CADObjectId = thatValue.value;

        if (!CTX.surfaceChanges.CADObjectId) {
            CTX.surfaceChanges.CADObjectId = [];
        }
        CTX.surfaceChanges.CADObjectId.push({name: surface.Name, cadId: thatValue.value});

    }

};

export const getCTXValues = () => {
    return {
        surfaceChanges : CTX.surfaceChanges,
        telltalesPolyloop : CTX.telltalesPolyloop,
        telltalesVertex : CTX.telltalesVertex
    }
};