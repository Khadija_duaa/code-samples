
import {getCTXValues} from "./gv-ctx";

import * as GBX from './gv-gbx';
import {getGBXValues} from "./gv-gbx";

import * as THR from './gv-thr';
import {getTHRValues} from "./gv-thr";

import * as SAV from './gv-sav';



var COR = { release: "14.2" };

COR.colorButtonToggle = null;
COR.releaseSourceURL = 'https://github.com/ladybug-tools/spider/tree/master/gbxml-viewer/r14/';

let themeName = localStorage.getItem( 'themeName' ) || 'https://bootswatch.com/_vendor/bootstrap/dist/css/bootstrap.css';



COR.updateCss = ( link ) =>{

	 = document.getElementById('CORcss').href = link;
	localStorage.setItem( '', link );
	setTheme();

};



export const setTheme = ()=>{

	const themesBootswatch = [
		{ 'Default': 'background-color: white; color: #007bff' },
	];


	const themesOthers = [
		{ link: "https://demos.creative-tim.com/material-kit/assets/css/material-kit.min.css", name: "Material Kit" },
		{ link: "https://www.gettemplate.com/demo/initio/assets/css/styles.css", name: 'Initio' },
		{ link: "https://blackrockdigital.github.io/startbootstrap-creative/css/creative.min.css", name: 'Creative' },
		{ link: "https://tympanus.net/Freebies/Cardio/css/cardio.css", name: 'Cardio' },
		{ link: "https://www.gettemplate.com/demo/magister/assets/css/magister.css", name: 'Magister' },
	];


	const txt1 = themesBootswatch.map( theme => {
		const name = Object.keys( theme )[ 0 ];
		const link = name === 'Default' ?
			'https://bootswatch.com/_vendor/bootstrap/dist/css/bootstrap.css' :
			`https://stackpath.bootstrapcdn.com/bootswatch/4.1.1/${ name.toLowerCase() }/bootstrap.min.css`;
		const bingo = link ===  ? '*' : '';
		return `<button class=theme onclick=updateCss("${ link }"); style="${ theme[name] }" >${ bingo }${ name }${ bingo }</button> `;

	});

	document.getElementById('CORdivBootswatch').innerHTML = '<p>Themes from <a href="https://bootswatch.com/" target=_blank>Bootswatch</a><br>' + txt1.join( '' );

	let txt = '<p>Themes from other sources</p>';

	for ( let theme of themesOthers ) {

		//name = Object.keys( theme )[ 0 ];
		//console.log( 'name', name );
		//console.log( 'link', theme.link );
		txt += `<button class="theme btn btn-secondary" onclick=updateCss("${ theme.link }"); style="${ theme.name }" >${ theme.name }</button> `;

	}

    document.getElementById('CORdivCssOthers').innerHTML = txt + '<p><small>these buttons are work-in-progress WIP</small></p>';


};



COR.updateDefaultFilePath = ()=> { // Used by COR.html drag and drop

	let CORinpFilePath = document.getElementById('CORinpFilePath');
	window.location.hash = CORinpFilePath.value;

	const thrFilePath = CORinpFilePath.value;
	.setItem('thrFilePath', thrFilePath );

};



COR.loadScript = function( source ){ // add buttonId?

	COR.resetLeftMenu();

	// change to add a check for if not already loaded?
	const script = document.head.appendChild( document.createElement( 'script' ) );
	script.src = source ;

	script.onload = function() { setMenuButtonsClass ( document.getElementById('CORdivMenuItems') ); };

};



// handle location.hash change events

COR.onHashChange = function() {

	const url = !window.location.hash ? COR.uriDefaultFile : window.location.hash.slice( 1 );
	const ulc = url.toLowerCase();

	COR.timeStart = Date.now();

	if ( ulc.endsWith( '.md' ) ) {

		COR.requestFile( url, COR.callbackMarkdown );

	} else if ( ulc.endsWith( '.xml' ) ) {

		requestGbxmlFile( url );

	} else {

		COR.requestFile( url, COR.callbackToTextarea );

	}

};



export const requestGbxmlFile = ( url ) =>{

	// COR.timeStart = Date.now();

	// THR.setSceneDispose( [ getGBXValues().surfaceMeshes, getGBXValues().surfaceEdges, getGBXValues().surfaceOpenings,getTHRValues().axesHelper ] );

	const xhr = new XMLHttpRequest();
	xhr.crossOrigin = 'anonymous';
	xhr.open( 'GET', url, true );

	xhr.onerror = function( xhr ) { console.log( 'error:', xhr ); };
	xhr.onload = callbackGbXML;
	xhr.send( null );


	function onRequestFileProgress( xhr ) {

		const fileAttributes = { name: xhr.target.responseURL.split( '/').pop() };

		document.getElementById('CORdivLog').innerHTML =
		`
			${fileAttributes.name}<br>
			bytes loaded: ${xhr.loaded.toLocaleString()} of ${xhr.total.toLocaleString() }<br>
		`;

	}

};

export const callbackGbXML = ( text ) =>{


    COR.timeStart = Date.now();

    THR.setSceneDispose( [ getGBXValues().surfaceMeshes, getGBXValues().surfaceEdges, getGBXValues().surfaceOpenings,getTHRValues().axesHelper ] );

    const meshes = GBX.parseFileXML(text);

    getTHRValues().scene.add( ...meshes );

    THR.zoomObjectBoundingSphere( getGBXValues().surfaceEdges );

    // document.getElementById('CORdivLog').innerHTML =
    //     `
		// file: ${ xhr.target.responseURL.split( '/').pop() }<br>
		// time: ${ Date.now () - COR.timeStart } ms ~
		// size: ${ xhr.loaded.toLocaleString() }
		// <br>`;

};


COR.openGbxmlFile = ( files ) =>{


	COR.timeStart = Date.now();

	THR.setSceneDispose( [ getGBXValues().surfaceMeshes, getGBXValues().surfaceEdges, getGBXValues().surfaceOpenings, getTHRValues().axesHelper ] );

	//COR.fileAttributes = files.files[ 0 ];

	const reader = new FileReader();
	reader.onprogress = onRequestFileProgress;
	reader.onload = function( event ) {

		console.log(reader.result);
		const meshes = GBX.parseFileXML( reader.result );


		getTHRValues().scene.add( ...meshes );

		THR.zoomObjectBoundingSphere( getGBXValues().surfaceEdges );

	};

	reader.readAsText( files.files[ 0 ] );

	function onRequestFileProgress( event ) {

		// document.getElementById('CORdivLog').innerHTML =
		// 	COR.fileAttributes.name + ' bytes loaded: ' + event.loaded.toLocaleString() +
		// 	//( event.lengthComputable ? ' of ' + event.total.toLocaleString() : '' ) +
		// '';

	}

};



COR.requestFile = ( url, callback ) => {

	const xhr = new XMLHttpRequest();
	xhr.crossOrigin = 'anonymous';
	xhr.open( 'GET', url, true );
	xhr.onerror = function( xhr ) { console.log( 'error:', xhr ); };
	xhr.onload = callback;
	xhr.send( null );

};



COR.requestFileAndProgress = function( url, callback ) {

	const xhr = new XMLHttpRequest();
	xhr.crossOrigin = 'anonymous';
	xhr.open( 'GET', url, true );
	xhr.onerror = function( xhr ) { console.log( 'error:', xhr ); };
	xhr.onprogress = onRequestFileProgress;
	xhr.onload = callback;
	xhr.send( null );

	function onRequestFileProgress( xhr ) {

		//console.log( 'xhr', xhr );

		COR.fileAttributes = { name: xhr.target.responseURL.split( '/').pop() };
		document.getElementById('CORdivLog').innerHTML = COR.fileAttributes.name + '<br>bytes loaded: ' + xhr.loaded.toLocaleString() + ' of ' + xhr.total.toLocaleString() ;

	}

};



// handle callbacks with file data events / gbxml callback in GBX

COR.callbackMarkdown = function( obj ){

	// const markdown = obj.target ? obj.target.responseText : obj;
    //
	// showdown.setFlavor('github');
	// const converter = new showdown.Converter();
    //
    //
	// document.getElementById('CORdivItemsRight').innerHTML = converter.makeHtml( markdown );
	// document.getElementById('CORdivMenuRight').style.display = 'block';
    //
	// window.scrollTo( 0, 0 );
	// document.getElementById('CORdivMenuRight').scrollTop = 0;
    //
	// document.getElementById('CORdivMenuRight').scrollTo( 0, 0 );


};



COR.callbackToTextarea = function( xhr ){

	const response = xhr.target.response;
	document.getElementById('CORdivItemsRight').innerHTML = '<textarea style=height:100%;width:100%; >' + response + '</textarea>';
    document.getElementById('CORdivMenuRight').style.display = 'block';

};



COR.callbackJson = function( xhr ) { // should be in CTX??

	const response = xhr.target.response;
    getCTXValues().surfaceChanges = JSON.parse( response );

	SAV.getUpdates();

};



// handle fileReader events

export const openFile = (files)=> {

	const reader = new FileReader();

	reader.onload = ( event )=> {


		if ( files.files[0].name.toLowerCase().endsWith( '.xml' ) ) {


			COR.openGbxmlFile( files );

		} else if ( files.files[0].name.toLowerCase().endsWith( '.md' ) ) {

			COR.callbackMarkdown( reader.result );

		} else {
			const response = reader.result;
			document.getElementById('CORdivItemsRight').innerHTML = '<textarea style=height:100%;width:100%; >' + response + '</textarea>';
            document.getElementById('CORdivMenuRight').style.display = 'block';
		}

		document.getElementById('CORdivLog').innerHTML =
			'name: ' + files.files[0].name + '<br>' +
			'size: ' + files.files[0].size.toLocaleString() + ' bytes<br>' +
			'type: ' + files.files[0].type + '<br>' +
			'modified: ' + files.files[0].lastModifiedDate.toLocaleDateString() +
		'';
	};


	reader.readAsText( files.files[0] );

};

export const setRightMenuWide = function() {
    let CORdivMenuRight = document.getElementById('CORdivMenuRight');

	document.getElementById('CORdivMenuRight').style.display = 'block';
	document.getElementById('CORdivMenuRight').style.left = '55%';
	document.getElementById('CORdivMenuRight').style.width = '40rem';

	window.scrollTo( 0, 0 );
	document.getElementById('CORdivMenuRight').scrollTop = 0;

	document.getElementById('CORdivMenuRight').scrollTo( 0, 0 );

};



COR.toggleNavLeft = function() {

	let CORdivMenuLeft = document.getElementById('CORdivMenuLeft');

	const width = CORdivMenuLeft.getBoundingClientRect().width;

	if ( CORdivMenuLeft.style.left === '' || CORdivMenuLeft.style.left === '0px' ) {

		CORdivMenuLeft.style.left = '-' + width + 'px';

	} else {

		CORdivMenuLeft.style.left = '0px';

	}

};



COR.toggleNavRight = ()=> {

	let CORdivMenuRight = document.getElementById('CORdivMenuRight');

	const width = document.getElementById('CORdivMenuRight').getBoundingClientRect().width;

	if ( document.getElementById('CORdivMenuRight').style.left === '100%' ) {

		if ( window.innerWidth > 900 ) {

			document.getElementById('CORdivMenuRight').style.left = ( window.innerWidth - 10 - width ) + 'px';

		} else {

			document.getElementById('CORdivMenuRight').style.left = '60%';
			document.getElementById('CORdivMenuRight').style.width = '38%';

		}

	} else {

		document.getElementById('CORdivMenuRight').style.left = '100%';

	}

};



////////// Menu style setting

COR.resetLeftMenu = function () {

	const menuButtons = document.getElementById('CORdetFeatures').querySelectorAll( "button" );
	//console.log( 'menuButtons', menuButtons );

	menuButtons.forEach( button => button.classList.remove( "active" ) );

};



export const setPanelButtonInit = () =>{ // called by each feature script init as it loads

	document.getElementById('CORdivMenuRight').style.display = 'none';

//	button.classList.add( "active" );

};



export const setMenuButtonsClass = ( target ) =>{
	// used by: COR.loadScript / SET"too many places! / CTX


	const buttons = target.getElementsByTagName( "button" );

	for ( let button of buttons ) {

		button.classList.add( "btn", "btn-secondary", "btn-sm" );

	}

};


COR.vvvvsetPanelButtonClear = function( button ) { //anybody using this??

	document.getElementById('CORdivMenuItems').innerHTML = '';

	button.style.fontStyle = '';
	button.style.backgroundColor = '';
	button.style.fontWeight = '';

};

export const getCORValues = ()=>{
	return {
        colorButtonToggle: COR.colorButtonToggle
	}
};
