import React from 'react';
import {
    Row,
    Col,
    Form,
    message,
    Select,
    Button,
} from 'antd';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {
    loadFieldParameters,
    saveFieldParameter,
    loadFieldParametersSubset
} from "../../../../store/revitManagement/revit_actions";
import NewLoader from '../../../Loader/NewLoader';
import MultiselectTwoSides from 'react-multiselect-two-sides';


const {Option} = Select;


class FieldParams extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            options: [],
            value: [],

        }
    }

    componentDidMount() {

        this.props.loadFieldParameters();
        this.props.loadFieldParametersSubset();


    };


    componentWillReceiveProps(nextProps) {

        let arraySubsetFieldParams = Array.isArray(nextProps && nextProps.fieldParamsSubset);

        let subsetDataList = [];
        if (arraySubsetFieldParams) {
            nextProps && nextProps.fieldParamsSubset && nextProps.fieldParamsSubset.map((data, i) => {
                subsetDataList.push(data.fieldParameter.value)
            })
        }
        else {
            subsetDataList = []
        }


        this.setState({value: subsetDataList, options: nextProps.TotalFieldParams});


    }

    handleChange = (value) => {


        this.setState({value});
    };


    success = (msg) => {

        message.success(msg, 4);
    };

    errors = (msg) => {

        this.setState({value: []});

        message.error(msg);
    };

    getActions = () => {
        if(!this.state.options.length || !this.state.value.length) return null;
        return (

            <Form.Item>
                <Row type="flex" className="margin-top-60">
                    <Col span={3} className="edit-black-buttons float-right">
                        <Button style={{marginLeft: '1300px'}} type="primary" value="large" htmlType="submit"
                                onClick={() => this.props.saveFieldParameter(this.state.value, () => {
                                        this.success(this.props.message && this.props.message)
                                    },
                                    () => {
                                        this.props.loadFieldParametersSubset()
                                    }, () => {
                                        this.errors(this.props.message && this.props.message)
                                    })}>
                            <div style={{color: 'white'}}>Submit</div>
                        </Button>
                    </Col>
                </Row>
            </Form.Item>
        )

    };


    renderHeading = () => {

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <h3>Field Parameters</h3>
                    </div>
                </div>
            </div>
        )
    };

    render() {

        const {options, value} = this.state;
        const selectedCount = value.length;
        const availableCount = options.length - selectedCount;


        return (
            <>
                {this.props.processing ? <NewLoader/> :
                    <Row>
                        {this.renderHeading()}
                        <Row>
                            <Col span={24}>

                                {options.length && value.length ? <MultiselectTwoSides
                                    {...this.state}
                                    className="msts_theme_example mt-5"
                                    onChange={this.handleChange}
                                    availableHeader="Available"
                                    availableFooter={`Available: ${availableCount}`}
                                    selectedHeader="Selected"
                                    selectedFooter={`Selected: ${selectedCount}`}
                                    labelKey="value"
                                    showControls
                                    searchable
                                /> : null}
                            </Col>


                        </Row>
                        <Row>
                            {this.getActions()}
                        </Row>
                    </Row>
                }

            </>
        )
    }
}


const mapStateToProps = (state) => {

    return {
        processing: state.revit_setting.processing && state.revit_setting.processing,
        TotalFieldParams: state.revit_setting.fieldParams && state.revit_setting.fieldParams,
        fieldParamsSubset: state.revit_setting.fieldParamsSubset && state.revit_setting.fieldParamsSubset,
        message: state.revit_setting.message && state.revit_setting.message,

    }
};

const FieldParam = Form.create({})(FieldParams);

const connectedComponent = connect(mapStateToProps, {
    loadFieldParameters,
    saveFieldParameter,
    loadFieldParametersSubset
})(FieldParam);
export default withRouter(connectedComponent);