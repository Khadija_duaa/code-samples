import React from 'react';
import {Row, Col, Table, Form, message, Modal, Button, Icon, Progress} from 'antd';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {loadRevitSettings,deleteRevitSetting,setRevitSetting} from "../../../../../store/revitManagement/revit_actions";
import {BUTTON_COLOR} from "../../../../../utils/common-utils";
import NewLoader from '../../../../Loader/NewLoader';


class Settings extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            showHide: true,
            radioValue: 1,
            visible: false,
            visible2: false,
            showDetailsBox: true,
            searchText: '',
            value: '',
            modalOpen: false,
            index:'',
        }
    }


    componentDidMount() {

        this.props.loadRevitSettings(true);



    };



    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({searchText: selectedKeys[0]});
    };

    handleReset = (clearFilters) => {
        clearFilters();
        this.setState({searchText: ''});
    };


    showHide = () => {
        this.setState({
            showHide: !this.state.showHide,
        })
    };
    radioValue = (e) => {
        this.setState({
            radioValue: e.target.value,
        });
    };
    closePage = () => {
        this.setState({
            showDetailsBox: false,
        });
    };
    showModal = () => {
        this.props.history.push({
            pathname: '/dashboard/revit-management/setting',
            state: {settings: true}
        });
        this.setState({
            visible: true,
        });
    };

    showModalUpdate = (_id) => {


        this.setState({
            visible: true,
            index:_id
        });
    };
    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };


    success = (msg) => {

        message.success(msg);
    };

    errors = (msg) => {

        message.error(msg);
    };


    handleSubmit = (e) => {
        e.preventDefault();

    };


    renderHeading = () => {

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <h3>Settings</h3>
                    </div>
                </div>
            </div>
        )
    };

    render(){
        // firms data

        const data = Array.isArray(this.props.revitSetting  && this.props.revitSetting );



        const contractorColumns = [{
            title: 'MaterialCategory',
            dataIndex: 'MaterialCategory',
            width: 200,

        },  {
            title: 'SOR Quantity',
            dataIndex: 'SorQuantity',
            width: 200,

        }, {
            title: 'Action',
            dataIndex: '_id',
            width: 200,

            render: (text, record) => (
                <span className="table-data1 edit-actions" ><a href="javascript:" onClick={() => {
                    this.props.setRevitSetting(record);
                    this.props.history.push({
                        pathname: '/dashboard/revit-management/setting',
                        state: {settings: true}
                    });
                }} style={{marginRight:'10px'}}><Icon
                    type="form"/></a><a href="javascript:" onClick={(e)=>{
                    e.stopPropagation();
                    this.showModalUpdate(record._id)}}><Icon
                    type="delete"/></a></span>
            )
        }

        ];





        return(
            <>
                {this.props.processing ? <NewLoader/> : null}
                {this.renderHeading()}
                <button className="custom-button custom-button-dark create-button margin-60 mt-5"  onClick={()=>{this.showModal();
                    this.props.setRevitSetting(null)}}>+ Add Setting</button>

                <Row>
                    <Col span={24} className="firms">
                        <Row className="">
                            <Col span={24}>
                                <Table
                                    columns={contractorColumns}
                                    dataSource={data ? this.props.revitSetting && this.props.revitSetting : null}
                                    className="zero-padding"
                                    pagination={false}
                                    size="large"s
                                    onRow={(record) => ({
                                        onClick: () => {
                                            this.props.setRevitSetting(record);
                                            this.props.history.push({
                                                pathname: '/dashboard/revit-management/setting',
                                                state: {settings: true}
                                            });
                                        }
                                    })}
                                />
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <div>
                    <Modal
                        title="Confirmation"
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        centered={true}
                        onCancel={this.handleCancel}
                        footer={null}
                        style={{textAlign:"center"}}
                        maskClosable={false}
                    >
                        <Row>
                            <Col span={24}>
                                <Row> Are you sure, you want to delete row?</Row>

                            </Col>
                        </Row>
                        <Row style={{display:'flex', marginTop:'40px'}}>
                            <Col span={12}>
                                <Button style={{ height: "40px", width: "90%",backgroundColor: BUTTON_COLOR, color: "#fff",float:'right'}}
                                        onClick={()=>{
                                            this.handleOk();
                                            this.props.deleteRevitSetting(this.state.index,()=>{this.props.loadRevitSettings(true)},
                                                ()=>{this.success(this.props.message && this.props.message )},()=>{this.errors(this.props.error && this.props.error )})}}>
                                    Yes
                                </Button>
                            </Col>
                            <Col span={12}>
                                <Button style={{height: "40px", width: "90%", marginLeft: "5%", borderColor: BUTTON_COLOR, float:'left'}}
                                        onClick={()=>{this.handleCancel()}}>Cancel</Button>
                            </Col>
                        </Row>
                    </Modal>

                </div>
            </>
        )
    }
}



const mapStateToProps = (state) => {


    return {

        revitSetting: state.revit_setting.revitSetting && state.revit_setting.revitSetting,
        message: state.revit_setting.revitSetting && state.revit_setting.message ,
        error: state.revit_setting.revitSetting && state.revit_setting.error ,
        processing:state.revit_setting.revitSetting && state.revit_setting.processing,
    }
};

const Setting = Form.create({})(Settings);

const connectedComponent = connect(mapStateToProps,{loadRevitSettings,deleteRevitSetting,setRevitSetting} )(Setting);
export default withRouter(connectedComponent);