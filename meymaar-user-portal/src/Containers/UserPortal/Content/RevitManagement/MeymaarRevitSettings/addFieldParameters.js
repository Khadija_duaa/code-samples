import React from 'react';
import {Row, Col, Form, message, Select, Modal, Button,} from 'antd';





const { Option } = Select;

class FieldParameters extends React.Component {

    constructor(props) {
        super(props);
        this.state ={
            isFormfilled:false

        }
    }


    success = (msg) => {

        message.success(msg);
    };

    errors = (msg) => {

        message.error(msg);
    };



    submit = (e, setParent,fieldParameters,onClickFieldParamSubmit ) =>{
        e.preventDefault();





        this.props.form.validateFields((err, values) => {




            const found = fieldParameters.some(el => el.FieldName === values.FieldName);




            if (!err) {



                if(found)
                {
                    this.errors('Parameter Already Exists!');

                }
                else{
                    this.setState({FieldParameters:values}, () =>{setParent({...this.state} )});
                    this.success('Parameter added successfully!');
                    onClickFieldParamSubmit();
                }


            }

        })
    };

    onChangeFieldParam = () => {
        this.setState({isFormfilled:true})
    };





    getFieldParameter = (getFieldDecorator,fieldParams) => {


        let arrayFieldParams = Array.isArray(fieldParams && fieldParams);

        let fieldParamsdata =arrayFieldParams ? fieldParams && fieldParams : [];

        return (

            <Form.Item label="Field Parameter" style={{marginLeft:'-100px'}}>
                {getFieldDecorator('FieldName', {
                    rules: [{required: true, message: "Please select!",}]
                })(<Select
                    onChange={this.onChangeFieldParam}
                    style={{ width: 1000}}
                    showSearch
                    size={'large'}
                    placeholder="Please select parameter"
                    filterOption={true}
                    optionFilterProp="children">
                    {
                        fieldParamsdata && fieldParamsdata.map((data,o) => {

                            return (
                                <Option key={o} value={data.fieldParameter.value} >{data.fieldParameter.key} &nbsp;  -  &nbsp; {data.fieldParameter.value}</Option>
                            )
                        })

                    }




                </Select>)}
            </Form.Item>
        )

    };

    getUnit = (getFieldDecorator) => {

        return (

            <Form.Item style={{marginLeft:'-100px'}}>
                {getFieldDecorator('Unit', {
                    rules: [{required: false, message: "Please select!",}]
                })(<Select className="custom-dropdown-menu" placeholder="Select unit"
                           style={{ width: 1000}}
                           size="large">
                    <Option value={'DUT_METERS'}>DUT_METERS</Option>
                    <Option value={'DUT_CENTIMETERS'}>DUT_CENTIMETERS</Option>
                    <Option value={'DUT_MILLIMETERS'}>DUT_MILLIMETERS</Option>
                    <Option value={'DUT_DECIMAL_FEET'}>DUT_DECIMAL_FEET</Option>
                    <Option value={'DUT_FEET_FRACTIONAL_INCHES'}>DUT_FEET_FRACTIONAL_INCHES</Option>
                    <Option value={'DUT_ACRES'}>DUT_ACRES</Option>
                    <Option value={'DUT_CUBIC_YARDS'}>DUT_CUBIC_YARDS</Option>
                    <Option value={'DUT_SQUARE_FEET'}>DUT_SQUARE_FEET</Option>
                    <Option value={'DUT_PERCENTAGE'}>DUT_PERCENTAGE</Option>
                    <Option value={'DUT_SQUARE_INCHES'}>DUT_SQUARE_INCHES</Option>
                    <Option value={'DUT_CUBIC_METERS_PER_HOUR'}>DUT_CUBIC_METERS_PER_HOUR</Option>
                    <Option value={'DUT_CUBIC_METERS_PER_SECOND'}>DUT_CUBIC_METERS_PER_SECOND</Option>
                    <Option value={'DUT_METERS_PER_SECOND'}>DUT_METERS_PER_SECOND</Option>
                    <Option value={'DUT_FEET_PER_MINUTE'}>DUT_FEET_PER_MINUTE</Option>
                    <Option value={'DUT_JOULES'}>DUT_JOULES</Option>
                    <Option value={'DUT_BRITISH_THERMAL_UNITS'}>DUT_BRITISH_THERMAL_UNITS</Option>
                    <Option value={'DUT_POUNDS_MASS_PER_CUBIC_FOOT'}>DUT_POUNDS_MASS_PER_CUBIC_FOOT</Option>
                </Select>)}
            </Form.Item>
        )

    };




    getActions = (getFieldDecorator ) => {





        return (

            <Form.Item>
                <Row type="flex" className="margin-top-40" style={{marginLeft:'40px'}}>
                    <Col span={12} className="edit-black-buttons float-right">
                        <Button type="primary" value="large" htmlType="submit"  disabled={!this.state.isFormfilled}>
                            <div style={{color:'white'}}>Submit</div>
                        </Button>
                    </Col>
                    <Col span={12} className="unfill-edit-black-buttons float-left">
                        <Button type="primary" value="large" onClick={this.props.handleCancel}>Cancel</Button>
                    </Col>
                </Row>
            </Form.Item>
        )

    };




    render(){

        const { getFieldDecorator } = this.props.form;
        return(

            <Modal
                title={'Add'}
                visible={this.props.visible}
                className="custom-table-popup"
                onOk={this.props.handleOk}
                onCancel={this.props.handleCancel}
                destroyOnClose={true}
                footer={false}
                maskClosable={false}
                centered
                size={"lg"}
                style={{maxWidth:'1200px', width:'70%'}}
            >
                <Form onSubmit={(e)=>this.submit(e, this.props.setStateOfParent, this.props.FieldParameters && this.props.FieldParameters,this.props.onClickFieldParamSubmit)}>
                    <Row className="custom-popup-row">
                        <Col span={24} offset={4} className="custom-form-wrap">
                            <Col span={16}>

                                {this.getFieldParameter(getFieldDecorator, this.props.fieldParamsSubset && this.props.fieldParamsSubset)}

                                {this.getUnit(getFieldDecorator)}

                                {this.getActions(getFieldDecorator,this.props.onClickFieldParamSubmit)}


                            </Col>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        )
    }
}



const FieldParameter = Form.create({})(FieldParameters);
export default FieldParameter;


