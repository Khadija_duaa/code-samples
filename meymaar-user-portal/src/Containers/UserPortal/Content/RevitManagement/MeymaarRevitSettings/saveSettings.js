import React from 'react';
import {
    Row,
    Col,
    Table,
    Form,
    message,
    Select,
    Button,
    Checkbox,
    Icon, Popconfirm, Collapse
} from 'antd';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import FieldParameter from "./addFieldParameters";
import Files from './addFile';
import {Badge} from 'reactstrap';
import BreadCrumb from '../../../../../Components/BreadCrumb/BreadCrumb';
import {
    loadFieldParameters,
    loadTakeoffMaterialCategories,
    loadScheduleMaterialCategories,
    saveRevitSetting,
    loadBqCategories,
    loadFieldParametersSubset
} from "../../../../../store/revitManagement/revit_actions";
import NewLoader from "../../../../Loader/NewLoader";

const Panel = Collapse.Panel;

let fieldParamList = [];
let filesList = [];


class SettingForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            file: null,
            index: '',
            selected: null,
            FinalSubmit: false,

            visible: false,
            visibleFile: false,
            fieldParam: null,
            differenceList: [],
            differenceListSORQuantity: [],
            fileNames: [],


            MaterialCategory: '',

            SortingParameters: [],

            SorQuantity: [],

            FieldParameters: [],

            Files: [],
            IsMaterialTakeOff: false,
            GrandTotal: false,


        };
        this.setParentState = this.setParentState.bind(this);

    }

    getLocationState = () => {
        let locationState = null;
        const {location} = this.props;

        if (location && location.state) {
            locationState = location.state;
        }

        return locationState;
    };

    componentDidMount() {

        let locationState = this.getLocationState();

        if (!locationState) {
            this.props.history.push('/dashboard/revit-management/settings');
        }
        else {


            fieldParamList = [];


            let listCompareSorting = this.props.updatedRevitSetting && this.props.updatedRevitSetting.SortingParameters && this.props.updatedRevitSetting.SortingParameters;

            let listCompareSOR = this.props.updatedRevitSetting && this.props.updatedRevitSetting.SortingParameters && this.props.updatedRevitSetting.SorQuantity;

            this.props.loadFieldParametersSubset();
            this.props.loadFieldParameters();
            this.props.loadTakeoffMaterialCategories();
            this.props.loadScheduleMaterialCategories();
            this.props.loadBqCategories();

            if (this.props.updatedRevitSetting) {

                console.log(this.props.updatedRevitSetting);
                let listSorQuantity = this.props.updatedRevitSetting && this.props.updatedRevitSetting.FieldParameters && this.props.updatedRevitSetting.FieldParameters.filter(item1 =>
                    !listCompareSorting.some(item2 => (item2 === item1.FieldName)));

                let listSortingParameters = this.props.updatedRevitSetting && this.props.updatedRevitSetting.FieldParameters && this.props.updatedRevitSetting.FieldParameters.filter(item1 =>
                    !listCompareSOR.some(item2 => (item2 === item1.FieldName)));


                let {MaterialCategory, SortingParameters, SorQuantity, FieldParameters, Files, IsMaterialTakeOff, GrandTotal} = this.props.updatedRevitSetting;
                this.props.form.setFieldsValue({MaterialCategory});

                this.setState({
                    MaterialCategory: MaterialCategory,
                    SortingParameters: SortingParameters,
                    SorQuantity: SorQuantity,
                    FieldParameters: FieldParameters

                    ,
                    Files: Files,
                    IsMaterialTakeOff: IsMaterialTakeOff,
                    GrandTotal: GrandTotal,
                    differenceListSORQuantity: listSorQuantity,

                    differenceList: listSortingParameters
                });
            }
        }

    }


    addFieldParameter = () => {
        this.setState({
            visible: true
        });
    };

    addFile = () => {


        this.setState({
            visibleFile: true,
            file: null,

        });
    };


    onChangeGrandTotal = (e) => {


        this.setState({
            GrandTotal: e.target.checked,


        });
    };


    handleOk = () => {


        this.setState({visible: true});
    };

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };

    onClickFieldParam = () => {
        setTimeout(() => {
            this.setState({visible: false})
        }, 1000);
    };

    onClickFileSubmit = () => {
        setTimeout(() => {
            this.setState({visibleFile: false})
        }, 1000);
    };

    handleFinalSubmit = () => {
        this.setState({
            FinalSubmit: false,
        });
    };

    handleFinalSubmitforRevitSetting = () => {

        this.setState({
            FinalSubmit: true,
        });
    };


    handleOkFile = (e) => {

        this.setState({visibleFile: true});
    };

    handleCancelFile = () => {
        this.setState({
            visibleFile: false,
        });
    };

    setFieldParams = (state) => {

        this.setState({
            FieldParameters: state,
        });
    };


    success = (msg) => {

        message.success(msg);
    };

    errors = (msg) => {

        message.error(msg);
    };


    deleteFieldParam = (k) => {


        fieldParamList = this.state.FieldParameters.filter(item => item.FieldName !== k);

        let sortingParam = this.props.updatedRevitSetting && this.props.updatedRevitSetting.SortingParameters && this.props.updatedRevitSetting.SortingParameters;


        let differenceList = fieldParamList.filter(item1 =>
            !this.state.SortingParameters.some(item2 => (item2 === item1.FieldName)));

        let differenceListSORQuantity = fieldParamList.filter(item1 =>
            !this.state.SorQuantity.some(item2 => (item2 === item1.FieldName)));

        this.setState({
            FieldParameters: fieldParamList,
            differenceList: differenceList,
            differenceListSORQuantity: differenceListSORQuantity
        });


    };


    deleteFile = (file) => {


        filesList = this.state.Files && this.state.Files.filter(item => item.FileName !== file);


        this.setState({
            Files: filesList
        });


    };


    setFieldParam = (fieldParam) => {


        this.setState({
            fieldParam: fieldParam
        });

    };


    formLabelLine = {
        marginBottom: '50px',
        color: '#808080',
        fontWeight: 'normal'
    };

    formSeparator = {
        marginBottom: '50px',
        border: '1px solid #DCDCDC',
        width: '1530px'
    };


    sorParameters = {
        fontWeight: 'lighter',
        marginBottom: '50px',
        marginTop: '30px',
        width: '90%',
        marginLeft: '73px'
    };


    columns = [{
        title: 'Parameter Field',
        key: 'Parameter Field',
        dataIndex: 'FieldName',
        name: 'Parameter Field',
        width: 100,
    }, {
        title: 'Unit',
        key: 'Unit',
        dataIndex: 'Unit',
        name: 'Unit',
        width: 400,
    },
        {
            title: 'Action',
            dataIndex: 'filedParamIndex',
            width: 100,
            render: (text, record) => <div>
                <Popconfirm placement="rightTop" title="Sure to delete?"
                            className="delete-confirmation-popup"
                            onConfirm={() => this.deleteFieldParam(record.FieldName)}
                            okText="Yes"
                            cancelText="No">


                    <a href="javascript:" className="actions" style={{marginLeft: '20px'}}><Icon
                        type="delete"/></a></Popconfirm>
            </div>


            ,

        },

    ];


    columnsFilter = [{
        title: 'By',
        dataIndex: 'By',
        name: 'By',
        width: 200,
    }, {
        title: 'Type',
        dataIndex: 'Type',
        name: 'Type',
        width: 200,
    },
        {
            title: 'Value',
            dataIndex: 'Value',
            name: 'Value',
            width: 200,
        },

    ];


    setParentStateFileName = (obj) => {
        let Files = [...this.state.Files];
        Files.push(obj);
        this.setState({Files: Files});
    };


    setParentState(obj) {

        fieldParamList = [...this.state.FieldParameters];
        fieldParamList.push(obj.FieldParameters);

        this.setState({
            FieldParameters: fieldParamList,
            differenceList: fieldParamList,
            differenceListSORQuantity: fieldParamList
        });
    }

    handleMaterialCategory = (value) => {

    };

    onChangeAllowTakeoff = (e) => {

        this.setState({
            IsMaterialTakeOff: e.target.checked,
        });
        this.props.form.setFieldsValue({
            MaterialCategory: [],
        });
    };


    getMaterialCategory = (getFieldDecorator, takeoffMaterialParams, scheduleMaterialParams) => {


        let arraySchedule = Array.isArray(scheduleMaterialParams);
        let arrayTakeoff = Array.isArray(takeoffMaterialParams);


        return (
            <Row style={{display: 'flex', marginBottom: '30px'}}>
                <Col className={"col-md-4"} style={{padding: '0px'}}>
                    <Form.Item label="Material Category">
                        {getFieldDecorator('MaterialCategory', {

                            rules: [{required: false}]
                        })(<Select
                            style={{width: 500}}
                            showSearch
                            size={'large'}
                            placeholder="Please select category"
                            onChange={this.handleMaterialCategory}
                            filterOption={true}
                            optionFilterProp="children"
                        >
                            {

                                this.state.IsMaterialTakeOff ?
                                    arrayTakeoff ? takeoffMaterialParams && takeoffMaterialParams.map((dataMaterial, f) => {

                                        return (
                                            <Select.Option key={dataMaterial._id}
                                                           value={dataMaterial.value}>{dataMaterial.key} &nbsp;  -  &nbsp; {dataMaterial.value}</Select.Option>
                                        )
                                    }) : null :
                                    arraySchedule ? scheduleMaterialParams && scheduleMaterialParams.map((dataSchedule, o) => {


                                        return (
                                            <Select.Option key={dataSchedule._id}
                                                           value={dataSchedule.value}>{dataSchedule.key} &nbsp;  -  &nbsp; {dataSchedule.value}</Select.Option>
                                        )
                                    }) : null
                            }
                        </Select>)}
                    </Form.Item>
                </Col>
                <Col className={"col-md-0"}>
                    <Form.Item style={{width: '300px'}}>
                        {getFieldDecorator('IsMaterialTakeOff', {
                            valuePropName: 'checked',
                            initialValue: this.props.updatedRevitSetting && this.props.updatedRevitSetting.IsMaterialTakeOff && this.props.updatedRevitSetting.IsMaterialTakeOff,
                            rules: [{required: false,}]
                        })(<Checkbox style={{paddingLeft: '10px', paddingTop: '45px'}} onChange={(e) => {
                            this.onChangeAllowTakeoff(e)
                        }}>Build Takeoff</Checkbox>)}
                    </Form.Item>
                </Col>

                <Col className={"col-md-21"} style={{marginLeft: '355px', marginTop: '20px'}}>
                    <button className="custom-button custom-button-dark create-button margin-60"
                            onClick={this.addFieldParameter}>Add Parameter
                    </button>
                </Col></Row>


        )

    };


    handleChangeSortingParams = (value) => {


        if (this.props.updatedRevitSetting && this.props.updatedRevitSetting) {

            this.setState({

                differenceListSORQuantity: this.state.FieldParameters.filter(item1 =>
                    !value.some(item2 => (item2 === item1.FieldName)))

            });


        }
        else {


            this.setState({

                    differenceListSORQuantity: fieldParamList.filter(item1 =>
                        !value.some(item2 => (item2 === item1.FieldName)))
                }
            );
        }


    };

    handleChangeSORQuantity = (value) => {


        if (this.props.updatedRevitSetting && this.props.updatedRevitSetting) {

            this.setState({

                differenceList: this.state.FieldParameters.filter(item1 =>
                    ![value].some(item2 => (item2 === item1.FieldName)))


            });


        }
        else {


            this.setState({

                differenceList: fieldParamList.filter(item1 =>
                    ![value].some(item2 => (item2 === item1.FieldName)))

            });

        }


    };


    getSortingParameter = (getFieldDecorator) => {

        let updatedRevitSetting = this.props.updatedRevitSetting && this.props.updatedRevitSetting !== null;
        let SortingParameters = this.props.updatedRevitSetting && this.props.updatedRevitSetting.SortingParameters && this.props.updatedRevitSetting.SortingParameters;
        let initial = updatedRevitSetting ? SortingParameters : [];


        return (

            <Form.Item label="Sorting Parameters">
                {getFieldDecorator('SortingParameters', {
                    initialValue: initial,
                    rules: [{required: false}]
                })(<Select
                    mode="multiple"
                    size={'large'}
                    placeholder="Please select parameters"
                    onChange={this.handleChangeSortingParams}
                >
                    {
                        Array.isArray(this.state.differenceList) && this.state.differenceList.map((data, b) => {
                            return (
                                <Select.Option key={b} value={data.FieldName}>{data.FieldName}</Select.Option>
                            )
                        })
                    }
                </Select>)}
            </Form.Item>
        )

    }

    getSORQuantity = (getFieldDecorator) => {

        let updatedRevitSetting = this.props.updatedRevitSetting && this.props.updatedRevitSetting !== null;
        let SorQuantity = this.props.updatedRevitSetting && this.props.updatedRevitSetting.SorQuantity && this.props.updatedRevitSetting.SorQuantity;
        let initial = updatedRevitSetting ? SorQuantity : [];

        return (

            <div style={{display: 'block'}}>
                <Form.Item label="Accounting Unit" style={{marginTop: '30px', width: '300px', marginBottom: '70px'}}>
                    {getFieldDecorator('SorQuantity', {
                        initialValue: initial,
                        rules: [{required: false}],
                    })(<Select
                        size={'large'}
                        placeholder="Please select SOR quantity"
                        onChange={this.handleChangeSORQuantity}
                    >
                        {
                            Array.isArray(this.state.differenceListSORQuantity) && this.state.differenceListSORQuantity.map((x, t) => {
                                return (
                                    <Select.Option key={t} value={x.FieldName}>{x.FieldName}</Select.Option>
                                )
                            })
                        }
                    </Select>)}
                </Form.Item>
                <hr style={this.formSeparator}/>
                <div style={{display: 'flex'}}>
                    <div style={{display: 'block'}}>
                        <h2>Files </h2>
                        <p style={this.formLabelLine}>Add and apply filters to files</p>
                    </div>
                    <div style={{marginLeft: '1000px', marginTop: '7px'}}>
                        <button className="custom-button custom-button-dark create-button margin-60"
                                onClick={this.addFile}>Add File
                        </button>
                    </div>
                </div>


            </div>
        )

    };


    getFieldParametersTable = (columns, fieldParamdata) => {


        return (

            <Table
                columns={columns}
                dataSource={fieldParamdata}
                className=" new-table-design delete-last"
                pagination={{pageSize: 5}}
                size="large"
            />
        )

    };


    getCheckBoxInputs = (getFieldDecorator) => {

        return (

            <Form.Item style={{marginTop: '30px'}}>
                <div style={{display: 'block'}}>
                    <Form.Item style={{width: '200px'}}>
                        {getFieldDecorator('GrandTotal', {
                            valuePropName: 'checked',
                            initialValue: this.props.updatedRevitSetting && this.props.updatedRevitSetting.GrandTotal && this.props.updatedRevitSetting.GrandTotal,
                            rules: [{required: false,}]
                        })(<Checkbox onChange={(e) => {
                            this.onChangeGrandTotal(e)
                        }}> Grand Total</Checkbox>)}
                    </Form.Item>


                </div>
            </Form.Item>
        )

    };

    getActions = () => {

        return (

            <Form.Item>
                <Row type="flex" className="margin-top-60">
                    <Col span={3} className="edit-black-buttons float-right">
                        <Button type="primary" value="large" htmlType="submit"
                                onClick={this.handleFinalSubmitforRevitSetting}>
                            <div style={{color: 'white'}}>Submit</div>
                        </Button>
                    </Col>
                    <Col span={3} className="unfill-edit-black-buttons float-left">
                        <Button type="primary" value="large"
                                onClick={() => this.props.history.push("/dashboard/revit-management/settings")}>Cancel</Button>
                    </Col>
                </Row>
            </Form.Item>
        )

    };

    closePage = () => {

        this.props.history.push('/dashboard/revit-management/settings')

    };


    getSORParameter = (getFieldDecorator, sorParams) => {


        return (
            <div>
                <div>
                    <p style={{
                        marginTop: '15px',
                        fontWeight: '70',
                        fontSize: '20px',
                        marginLeft: '70px',
                        color: 'black'
                    }}>SOR Parameters:</p>
                </div>
                <div style={{
                    marginLeft: '70px',
                    maxWidth: 'auto',
                    border: '1px solid #DCDCDC',
                    marginRight: '70px',
                    marginBottom: '30px',
                    fontWeight: 'lighter'
                }}>
                    {sorParams && sorParams.map((x, i) => {
                        return (
                            <Badge key={i} className="badge-SOR-Params">
                                {x}
                            </Badge>)
                    })}


                </div>
            </div>

        )

    };

    setFile = (file, index) => {


        this.setState({index: index, file: file, visibleFile: true});


    };

    getBreadCrumbs = () => {

        let breadCrumbsJSON = [
            {
                title: "Dashboard",
                returnPath: "/dashboard"
            },
            {
                title: "Revit Management",
                returnPath: "/dashboard/revit-management/settings"
            },
            {
                title: "Settings",
                returnPath: "/dashboard/revit-management/settings"
            },
            {
                title: 'Setting'
            }
        ];


        return (
            <Row span={24} style={{marginLeft: '-15px', marginBottom: '20px'}}>
                <Col span={24}>
                    <BreadCrumb style={{fontSize: "20px"}} crumbsJSON={breadCrumbsJSON}/>
                    <p className="custom-button custom-button-light pointer"
                       style={{marginTop: '-40px', marginLeft: '17px'}} onClick={this.closePage}><Icon
                        type="caret-left"/> Back</p>
                </Col>
            </Row>
        )
    };


    fileActions = (file, index) => {
        return (

            <div>
                <a href="javascript:" className="actions" onClick={() => {
                    this.setFile(file, index)
                }}><Icon
                    type="edit"/></a>
                <Popconfirm placement="rightTop" title="Sure to delete?"
                            className="delete-confirmation-popup"
                            okText="Yes"
                            onConfirm={() => this.deleteFile(file.FileName)}
                            cancelText="No">


                    <a href="javascript:" className="actions" style={{marginLeft: '20px', marginRight: '20px'}}><Icon
                        type="delete"/></a></Popconfirm>
            </div>
        )
    };


    getAllFiles = (getFieldDecorator, filterColumns) => {

        return (
            this.state.Files && this.state.Files.map((file, index) => (


                    <Collapse key={index} accordion defaultActiveKey={'1'} expandIconPosition={'left'}
                              onChange={this.onChange}
                              style={{fontWeight: 'bold'}}>
                        <Panel header={file.FileName} key={index} extra={this.fileActions(file, index)}>
                            {this.getSORParameter(getFieldDecorator, file.SorParameters && file.SorParameters)}


                            <div style={{marginLeft: '70px', display: 'block'}}>
                                <div>
                                    <p style={{fontWeight: '70', fontSize: '20px', color: 'black'}}>{'Filters:'}</p>
                                </div>
                                <div>
                                    <Table
                                        columns={filterColumns}
                                        dataSource={file.Filters}
                                        className='new-table-design1'
                                        pagination={{pageSize: 5}}
                                        size="large"
                                    />
                                </div>
                            </div>

                        </Panel>
                    </Collapse>

                )
            )
        )

    };

    handleSubmit = (e) => {


        e.preventDefault();


        this.props.form.validateFields((err, values) => {
            if (!err) {

                this.state.SorQuantity = values.SorQuantity;
                this.state.MaterialCategory = values.MaterialCategory;


                if (this.state.FinalSubmit) {

                    this.props.saveRevitSetting(values,true, () => {
                            this.success(this.props.message && this.props.message)
                        },
                        () => {
                            this.props.history.push('/dashboard/revit-management/settings')
                        }
                        , () => {
                            this.errors(this.props.message && this.props.message)
                        }
                    )
                }
                else {
                    return null
                }


            }

        });
    };


    render() {


        const FieldParameters = this.state.FieldParameters && this.state.FieldParameters.map((data, index) => {
            return {
                filedParamIndex: index,
                FieldName: data.FieldName,
                Unit: data.Unit,
            }
        });


        const {getFieldDecorator, getFieldValue} = this.props.form;

        const formItemLayout = {
            labelCol: {span: 24},
            wrapperCol: {span: 20}
        };


        return (
            <>
                {this.props.processing ? <NewLoader/> : null}
                {this.getBreadCrumbs()}
                <div style={{display: 'block'}}>
                    <h2>{this.props.updatedRevitSetting && this.props.updatedRevitSetting ? 'Update Setting' : 'Add Setting'}</h2>
                    <p style={this.formLabelLine}>Please fill the form to complete setting</p>
                    <hr style={this.formSeparator}/>
                </div>
                <Form onSubmit={this.handleSubmit} style={{width: '100%'}}>


                    <Row className="custom-popup-row">

                        <Col className="custom-form-wrap">
                            <Col>

                                {this.getMaterialCategory(getFieldDecorator, this.props.takeoffMaterialParams && this.props.takeoffMaterialParams, this.props.scheduleMaterialParams && this.props.scheduleMaterialParams)}

                                {this.getFieldParametersTable(this.columns, FieldParameters)}

                                {this.getSortingParameter(getFieldDecorator)}

                                {this.getSORQuantity(getFieldDecorator)}

                                {this.getAllFiles(getFieldDecorator, this.columnsFilter)}

                                {this.getCheckBoxInputs(getFieldDecorator)}

                                {this.getActions()}

                            </Col>
                            {
                                this.state.visible ?
                                    <FieldParameter SortingParameters={this.state.SortingParameters}
                                                    fieldParamsSubset={this.props.fieldParamsSubset && this.props.fieldParamsSubset}
                                                    FieldParameters={this.state.FieldParameters}
                                                    totalPages={this.props.totalPages && this.props.totalPages}
                                                    visible={this.state.visible} finalSubmit={this.handleFinalSubmit}
                                                    onClickFieldParamSubmit={this.onClickFieldParam}
                                                    handleOk={this.handleOk} handleCancel={this.handleCancel}
                                                    setStateOfParent={this.setParentState}/> : ''
                            }

                            {
                                this.state.visibleFile ?
                                    <Files visibleFile={this.state.visibleFile}
                                           fieldParams={this.props.fieldParams && this.props.fieldParams}
                                           bqCategories={this.props.bqCategories && this.props.bqCategories}
                                           setPagination={this.props.setPagination}
                                           totalPages={this.props.totalPages && this.props.totalPages}
                                           Files={this.state.Files} file={this.state.file} index={this.state.index}
                                           finalSubmit={this.handleFinalSubmit}
                                           TotalFieldParams={this.props.TotalFieldParams && this.props.TotalFieldParams}
                                           onClickFileSubmit={this.onClickFileSubmit}
                                           FieldParameters={this.state.FieldParameters && this.state.FieldParameters}
                                           handleOkFile={this.handleOkFile} handleCancelFile={this.handleCancelFile}
                                           setParentStateFileName={this.setParentStateFileName}/> : ''
                            }


                        </Col>
                    </Row>
                </Form>


            </>
        )
    }
}


const mapStateToProps = (state) => {


    return {
        fieldParams: state.revit_setting.activeParams && state.revit_setting.activeParams,
        TotalFieldParams: state.revit_setting.activeParams && state.revit_setting.fieldParams,
        fieldParamsSubset: state.revit_setting.activeParams && state.revit_setting.fieldParamsSubset,
        totalPages: state.revit_setting.totalPages && state.revit_setting.totalPages,
        takeoffMaterialParams: state.revit_setting.materialParams && state.revit_setting.materialParams,
        scheduleMaterialParams: state.revit_setting.scheduleMaterialParams && state.revit_setting.scheduleMaterialParams,
        updatedRevitSetting: state.revit_setting.updatedRevitSetting && state.revit_setting.updatedRevitSetting,
        bqCategories: state.revit_setting.bqCategories && state.revit_setting.bqCategories,
        message: state.revit_setting.message && state.revit_setting.message,
        processing: state.revit_setting.processing && state.revit_setting.processing,
    }

};

const saveSetting = Form.create({})(SettingForm);

const connectedComponent = connect(mapStateToProps, {
    loadFieldParameters, loadTakeoffMaterialCategories, loadScheduleMaterialCategories, loadFieldParametersSubset,
    saveRevitSetting, loadBqCategories
})(saveSetting);
export default withRouter(connectedComponent);