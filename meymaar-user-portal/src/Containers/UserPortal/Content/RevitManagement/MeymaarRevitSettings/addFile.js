import React from 'react';
import {Row, Col, Form, message, Input, Select, Modal, Button} from 'antd';



const { Option } = Select;

let filterParam = 0;

let   Filters=[];
let filterList=[];
let temp=[];
let SOR_ParamList =[];


class File extends React.Component {

    constructor(props) {
        super(props);
        this.state ={


        }
    }



    filterButtonAdd ={

        textAlign: 'center',
        padding: '16px',
        cursor : 'pointer',
        background: 'linear-gradient(#394C77, #394c64)',
        border: '1px solid #e9e9ea',
        borderRadius: '30px' ,
        maxWidth: '275px',
        marginLeft:'240px',
        marginTop:'30px' ,
        color:'white'

    };

    filterButtonRemove ={

        textAlign: 'center',
        padding: '16px',
        cursor : 'pointer',
        background: 'linear-gradient(#f5f6fa, #d8d8d8)',
        border: '1px solid #e9e9ea',
        borderRadius: '30px' ,
        maxWidth: '275px',
        marginLeft:'240px',
        marginTop:'30px' ,
        color:'black'

    };

    success = (msg) => {

        message.success(msg);
    };

    errors = (msg) => {

        message.error(msg);
    };

    componentDidMount() {
        SOR_ParamList =[];

        Filters = [];

        let arrayBqcategories = Array.isArray(this.props.bqCategories && this.props.bqCategories);

        let bqData =arrayBqcategories ? this.props.bqCategories && this.props.bqCategories : [];


        bqData.map((data,index) =>{

            let BQCategory = this.props.file && this.props.file.BQCategory && this.props.file.BQCategory;
            if(+data.id === +BQCategory)

            {
                BQCategory=data.type;
                this.props.form.setFieldsValue({BQCategory});
            }

        });

        this.setState({fileList: this.props.fileNameList && this.props.fileNameList, Filters:this.props.file && this.props.file.Filters && this.props.file.Filters})

    }






    updateFile = (index,data) => {



        temp =this.props.Files && this.props.Files;

        temp[index] = data;




    };

    submit = (e, setParentStateFileName,files,loading,index,onClickFileSubmit) =>{


        e.preventDefault();


        this.props.form.validateFields((err, values) => {
            const {filterKeys, By,Type,Value} = values;

            let foundFile = files.some(el => el.FileName === values.FileName);





            if (!err) {




                By && By.map((Param, index) => {


                    return (

                        Filters.push({By: By[index], Type: Type[index], Value: Value[index]})


                    )


                });





                if(this.props.file &&  this.props.file )

                {
                    let actionItem = files[index].FileName;

                    let updatedFilesList = files.filter( t => t.FileName !== actionItem);

                    let updatedFoundFile = updatedFilesList.some(el => el.FileName === values.FileName);
                    if(updatedFoundFile)
                    {
                        this.errors('File Already Exists!');
                        Filters=[];
                    }

                    else{

                        this.updateFile(index,{FileName:values.FileName,SorParameters:values.SorParameters,BQCategory:values.BQCategory,Filters:Filters});
                        this.success('File updated successfully!');
                    }

                }
                else{
                    if(foundFile)
                    {
                        this.errors('File Already Exists!');
                    }
                    else{
                        setParentStateFileName({FileName:values.FileName,SorParameters:values.SorParameters,BQCategory:values.BQCategory,Filters:Filters});
                        this.success('File added successfully!');
                        onClickFileSubmit();
                    }
                }











            }

        })};




    getFileName = (getFieldDecorator) => {



        return (

            <Form.Item label="Name" style={{marginLeft:'-100px'}}>
                {getFieldDecorator('FileName', {
                    initialValue: this.props.file && this.props.file === null ? null : this.props.file && this.props.file.FileName && this.props.file.FileName,
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [{
                        required: true,
                        message: "Please enter file name!",
                    }],
                })(
                    <Input  placeholder="Please type name" style={{width: 1000, height: '45px'}}/>
                )}
            </Form.Item>
        )

    };


    getSORParameters = (getFieldDecorator) => {



        let file =this.props.file &&  this.props.file !== null;
        let sor= this.props.file && this.props.file.SorParameters && this.props.file.SorParameters ;
        let initial=file ? sor : [];



        return (

            <Form.Item label="SOR Parameters" style={{marginLeft:'-100px'}}>
                {getFieldDecorator('SorParameters', {
                    initialValue: initial,
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [{
                        required: true,
                        message: "Please select!",
                    }],
                })(<Select
                    onChange={this.onChangeSORParameters}
                    style={{ width: 1000}}
                    mode = 'multiple'
                    size={'large'}
                    placeholder="Please select parameters"
                    showSearch
                    filterOption={true}
                    optionFilterProp="children"
                >
                    {
                        SOR_ParamList && SOR_ParamList.map((data,v) => {
                            return (
                                <Select.Option  key ={v} value={data.value}>{data.key} &nbsp;  -  &nbsp; {data.value}</Select.Option>
                            )
                        })
                    }


                </Select>)}
            </Form.Item>
        )

    };

    getBqCategories = (getFieldDecorator, bqCategories) => {

        let arrayBq=Array.isArray(bqCategories);
        return (

            <Form.Item label="BQ Categories" style={{marginLeft:'-100px'}}>
                {getFieldDecorator('BQCategory', {

                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [{
                        required: true,
                        message: "Please select!",
                    }],
                })(<Select
                    onChange={this.onChangeBqCategory}
                    style={{ width: 1000}}
                    mode={'single'}
                    size={'large'}
                    placeholder="Please select BQ category"

                >

                    {
                        arrayBq ? bqCategories && bqCategories.map((data,v) => {
                            return (
                                <Select.Option  key ={v} value={data.id}>{data.type}</Select.Option>
                            )
                        }): null
                    }

                </Select>)}
            </Form.Item>
        )

    };



    getFilterBy = (getFieldDecorator,filter,By) => {



        return (

            <Form.Item label="By" style={{marginLeft:'-100px'}}>
                {getFieldDecorator(`By[${filter}]`, {
                    initialValue: By && By,
                    validateTrigger: ['onChange', 'onBlur'],

                    rules: [{
                        required: true,
                        message: "Please select!",
                    }],
                })(
                    <Select
                        style={{ width: 1000}}
                        size={'large'}
                        placeholder="Please select"
                        showSearch
                        filterOption={true}
                        optionFilterProp="children"
                    >
                        {
                            SOR_ParamList && SOR_ParamList.map((data,t) => {
                                return (
                                    <Select.Option  key={t} value={data.value}>{data.key} &nbsp;  -  &nbsp; {data.value}</Select.Option>
                                )
                            })
                        }



                    </Select>
                )}
            </Form.Item>
        )

    };




    getFilterType = (getFieldDecorator,filter,Type) => {



        return (

            <Form.Item label="Type" style={{marginLeft:'-100px'}}>
                {getFieldDecorator(`Type[${filter}]`, {
                    initialValue: Type && Type,
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [{
                        required: true,
                        message: "Please select!",
                    }],
                })(
                    <Select
                        style={{ width: 1000}}
                        size={'large'}
                        placeholder="Please select"

                    >
                        <Option value={'Equal'}>Equal</Option>
                        <Option value={'NotEqual'}>Not Equal</Option>
                        <Option value={'GreaterThan'}>Greater Than</Option>
                        <Option value={'GreaterThanOrEqual'}>Greater Than Or Equal</Option>
                        <Option value={'LessThan'}>Less Than</Option>
                        <Option value={'LessThanOrEqual'}>LessT han Or Equal</Option>
                        <Option value={'Contains'}>Contains</Option>
                        <Option value={'NotContains'}>NotContains</Option>
                    </Select>
                )}
            </Form.Item>
        )

    };

    getFilterValue = (getFieldDecorator,filter,Value) => {



        return (

            <Form.Item label="Value" style={{marginLeft:'-100px'}}>
                {getFieldDecorator(`Value[${filter}]`, {
                    initialValue: Value && Value,
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [{
                        required: true,
                        message: "Please select",
                    }],
                })(
                    <Input style={{ width: 1000}}
                           placeholder="Please type value">
                    </Input>
                )}
            </Form.Item>
        )

    };

    addNewFile = () => {

        const {form} = this.props;

        const fileFilterKeys = form.getFieldValue("filterKeys");
        const nextKeys = fileFilterKeys.concat(filterParam);
        filterParam++;

        form.setFieldsValue({
            filterKeys: nextKeys
        });
    };


    removeFilter = (filter) => {



        const {form} = this.props;

        const keys = form.getFieldValue("filterKeys");


        form.setFieldsValue({
            filterKeys: keys.filter(key => key !== filter)
        });

    };




    getAddFilterButton = () => {


        return (

            <div  style={{marginTop:'35px', marginLeft:'288px'}} className="custom-button custom-button-dark create-button "
                  onClick={() =>{this.addNewFile()}}><p style={{marginTop:'10px', color:'white'}}>Add Filter</p></div>


        )

    };


    removeFilterEdit = (By, Type, Value) => {



        filterList = this.state.Filters.filter(item => item.By !== By || item.Type !== Type || item.Value !== Value);

        this.state.Filters= filterList;

        this.setState({Filters:this.state.Filters});



    };








    getActions = (loadingFile,onClickFileSubmit) => {

        return (

            <Form.Item >
                <Row type="flex" className="margin-top-40" style={{marginLeft:'40px'}}>
                    <Col span={12} className="edit-black-buttons float-right">
                        <Button type="primary" value="large" htmlType="submit" >
                            <div style={{color:'white'}}>Submit</div>
                        </Button>
                    </Col>
                    <Col span={12} className="unfill-edit-black-buttons float-left">
                        <Button type="primary" value="large" onClick={this.props.handleCancelFile}>Cancel</Button>
                    </Col>
                </Row>
            </Form.Item>
        )

    };


    getUpdatedFields = (filters, getFieldDecorator) => {



        return (

            filters && filters.map((data, v) => {

                return (
                    <div key ={v}>
                        {this.getFilterBy(getFieldDecorator, v, data.By)}

                        {this.getFilterType(getFieldDecorator, v, data.Type)}

                        {this.getFilterValue(getFieldDecorator, v, data.Value)}

                        <div  style={{marginTop:'35px', marginLeft:'280px'}} className="custom-button custom-button-light create-button " onClick={() =>{this.removeFilterEdit(data.By,data.Type, data.Value)}}><p style={{marginTop:'10px'}}>Remove Filter</p></div>
                    </div>


                )
            })

        )

    };






    render(){

        const {getFieldDecorator, getFieldValue} = this.props.form;


        getFieldDecorator('filterKeys', {initialValue: []});

        let filtersKeys = getFieldValue('filterKeys');





        const filterItems = filtersKeys.map((filter, s) => {

            return (
                <div key={s}>



                    {this.getFilterBy(getFieldDecorator,filter)}

                    {this.getFilterType(getFieldDecorator,filter)}

                    {this.getFilterValue(getFieldDecorator,filter)}


                    <div  style={{marginTop:'35px', marginLeft:'280px'}} className="custom-button custom-button-light create-button " onClick={() =>{this.removeFilter(filter)}}><p style={{marginTop:'10px'}}>Remove Filter</p></div>




                </div>
            );
        });


        return(

            <Modal
                title={this.props.file === null ? 'Add' : 'Update'}
                visible={this.props.visibleFile}
                className="custom-table-popup"
                onOk={this.props.handleOkFile}
                onCancel={this.props.handleCancelFile}
                destroyOnClose={true}
                footer={false}
                maskClosable={false}
                size={"lg"}
                style={{maxWidth:'1200px', width:'70%'}}
            >
                <Form onSubmit={(e)=>this.submit(e, this.props.setParentStateFileName,this.props.Files && this.props.Files,this.props.loading,this.props.index && this.props.index,this.props.onClickFileSubmit  )}>
                    <Row className="custom-popup-row">
                        <Col span={24} offset={4} className="custom-form-wrap">
                            <Col span={16}>

                                {this.getFileName(getFieldDecorator)}

                                {this.getSORParameters(getFieldDecorator)}

                                {this.getBqCategories(getFieldDecorator, this.props.bqCategories && this.props.bqCategories)}

                                {this.getAddFilterButton()}

                                {this.getUpdatedFields(this.state.Filters,getFieldDecorator)}

                                {filterItems}

                                {this.getActions(this.props.loadingFile,this.props.onClickFileSubmit)}


                            </Col>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        )
    }
}



const Files = Form.create({})(File);
export default Files;