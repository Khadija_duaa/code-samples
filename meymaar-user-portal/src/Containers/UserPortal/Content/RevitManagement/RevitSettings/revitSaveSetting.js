import React from 'react';
import {Row, Col, Table, Form, message, Select, Button, Checkbox, Icon, Input} from 'antd';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import BreadCrumb from '../../../../../Components/BreadCrumb/BreadCrumb';
import {
    loadFieldParameters,
    loadTakeoffMaterialCategories,
    loadScheduleMaterialCategories,
    saveRevitSetting,
    loadBqCategories,
    loadFieldParametersSubset
} from "../../../../../store/revitManagement/revit_actions";
import NewLoader from "../../../../Loader/NewLoader";


let fieldParamList = [];


class RevitSettingForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            file: null,
            index: '',
            selected: null,
            FinalSubmit: false,

            visible: false,
            visibleFile: false,
            fieldParam: null,
            differenceList: [],
            differenceListSORQuantity: [],
            fileNames: [],


            MaterialCategory: '',

            SortingParameters: [],

            SorQuantity: [],

            FieldParameters: [],

            Files: [],
            IsMaterialTakeOff: false,
            GrandTotal: false,


        };
        this.setParentState = this.setParentState.bind(this);
    }

    getLocationState = () => {
        let locationState = null;
        const {location} = this.props;

        if (location && location.state) {
            locationState = location.state;
        }

        return locationState;
    };

    componentDidMount() {

        let locationState = this.getLocationState();

        if (!locationState) {
            this.props.history.push('/dashboard/revit-management/revit-settings');
        }

        else {
            fieldParamList = [];


            this.props.loadFieldParametersSubset();
            this.props.loadFieldParameters();
            this.props.loadTakeoffMaterialCategories();
            this.props.loadScheduleMaterialCategories();
            this.props.loadBqCategories();

            if (this.props.updatedRevitSetting) {
                let {FileName, MaterialCategory, SortingParameters, SorQuantity,
                    FieldParameters, Files, IsMaterialTakeOff, GrandTotal, SorParameters,
                    BqCategory,Filters} = this.props.updatedRevitSetting;
                this.props.form.setFieldsValue({MaterialCategory});
                if (BqCategory && BqCategory !== 0) {
                    this.props.form.setFieldsValue({BqCategory})
                }

                this.setState({
                    MaterialCategory: MaterialCategory,
                    SortingParameters: SortingParameters,
                    SorQuantity: SorQuantity,
                    FieldParameters: FieldParameters,
                    Files: Files,
                    IsMaterialTakeOff: IsMaterialTakeOff,
                    GrandTotal: GrandTotal,
                    SorParameters,
                    differenceListSORQuantity: null,
                    differenceList: null,
                    disabled: true,
                    FileName,
                    Filters
                });
            }
        }


    }


    handleOk = () => {
        this.setState({visible: true});
    };

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };


    handleFinalSubmitforRevitSetting = () => {
        this.setState({
            FinalSubmit: true,
        });
    };

    success = (msg) => {
        message.success(msg);
    };

    errors = (msg) => {

        message.error(msg);
    };


    formLabelLine = {
        marginBottom: '50px',
        color: '#808080',
        fontWeight: 'normal'
    };

    formSeparator = {
        marginBottom: '50px',
        border: '1px solid #DCDCDC',
        width: '1530px'
    };


    columns = [
        {
            title: 'Parameter Field',
            key: 'Parameter Field',
            dataIndex: 'FieldName',
            name: 'Parameter Field',
            width: 100,
        }, {
            title: 'Unit',
            key: 'Unit',
            dataIndex: 'Unit',
            name: 'Unit',
            width: 400,
        }
    ];


    columnsFilter = [
        {
            title: 'By',
            dataIndex: 'ByName',
            name: 'ByName',
            width: 200,
        },
        {
            title: 'Type',
            dataIndex: 'Type',
            name: 'Type',
            width: 200,
        },
        {
            title: 'Value',
            dataIndex: 'Value',
            name: 'Value',
            width: 200,
        }
    ];


    setParentStateFileName = (obj) => {
        let Files = [...this.state.Files];
        Files.push(obj);
        this.setState({Files: Files});
    };


    setParentState(obj) {

        fieldParamList = [...this.state.FieldParameters];
        fieldParamList.push(obj.FieldParameters);

        this.setState({
            FieldParameters: fieldParamList,
            differenceList: fieldParamList,
            differenceListSORQuantity: fieldParamList
        });
    }


    onChangeAllowTakeoff = (e) => {

        this.setState({
            IsMaterialTakeOff: e.target.checked,
        });
        this.props.form.setFieldsValue({
            MaterialCategory: [],
        });
    };

    getFileName = (getFieldDecorator) => {
        let {FileName} = this.state;
        return (
            <Col className={"col-md-4"} style={{padding: '0px'}}>
                <Form.Item label="File Name">
                    {getFieldDecorator('FileName', {initialValue: FileName})(<Input/>)}
                </Form.Item>
            </Col>

        )
    };
    getMaterialCategory = (getFieldDecorator) => {
        let {disabled} = this.state;

        return (
            <Row style={{display: 'flex', marginBottom: '30px'}}>
                <Col className={"col-md-4"} style={{padding: '0px'}}>
                    <Form.Item label="Material Category">
                        {getFieldDecorator('MaterialCategory', {

                            rules: [{required: false}]
                        })(<Input disabled={disabled}/>)}
                    </Form.Item>
                </Col>

                <Col className={"col-md-0"}>
                    <Form.Item style={{width: '300px'}}>
                        {getFieldDecorator('IsMaterialTakeOff', {
                            valuePropName: 'checked',
                            initialValue: this.props.updatedRevitSetting && this.props.updatedRevitSetting.IsMaterialTakeOff && this.props.updatedRevitSetting.IsMaterialTakeOff,
                            rules: [{required: false,}]
                        })(<Checkbox style={{paddingLeft: '10px', paddingTop: '45px'}} disabled={this.state.disabled}
                                     onChange={(e) => {
                                         this.onChangeAllowTakeoff(e)
                                     }}>Build Takeoff</Checkbox>)}
                    </Form.Item>
                </Col>

            </Row>


        )

    };


    handleChangeSortingParams = (value) => {


        if (this.props.updatedRevitSetting && this.props.updatedRevitSetting) {

            this.setState({

                differenceListSORQuantity: this.state.FieldParameters.filter(item1 =>
                    !value.some(item2 => (item2 === item1.FieldName)))

            });
        }
        else {
            this.setState({

                    differenceListSORQuantity: fieldParamList.filter(item1 =>
                        !value.some(item2 => (item2 === item1.FieldName)))
                }
            );
        }
    };

    handleChangeSORQuantity = (value) => {

        if (this.props.updatedRevitSetting && this.props.updatedRevitSetting) {

            this.setState({

                differenceList: this.state.FieldParameters.filter(item1 =>
                    ![value].some(item2 => (item2 === item1.FieldName)))


            });


        }
        else {


            this.setState({

                differenceList: fieldParamList.filter(item1 =>
                    ![value].some(item2 => (item2 === item1.FieldName)))

            });

        }


    };


    getSortingParameter = (getFieldDecorator) => {

        let {updatedRevitSetting} = this.props;
        let {disabled} = this.state;
        let SortingGroupingParameters = updatedRevitSetting && updatedRevitSetting.SortingGroupingParameters && updatedRevitSetting.SortingGroupingParameters;
        let initialParams = [];
        SortingGroupingParameters && SortingGroupingParameters.forEach(param => {
            initialParams.push(param.FieldName);
        });

        return (

            <Form.Item label="Sorting & Grouping Parameters">
                {getFieldDecorator('SortingParameters', {
                    initialValue: initialParams,
                    rules: [{required: false}]
                })(<Select
                    mode="multiple"
                    size={'large'}
                    placeholder="Please select parameters"
                    onChange={this.handleChangeSortingParams}
                    disabled={disabled}
                >
                    {
                        Array.isArray(this.state.differenceList) && this.state.differenceList.map((data, b) => {
                            return (
                                <Select.Option key={b} value={data.FieldName}>{data.FieldName}</Select.Option>
                            )
                        })
                    }
                </Select>)}
            </Form.Item>
        )

    };

    getAccountingUnit = (getFieldDecorator) => {

        let {updatedRevitSetting} = this.props;
        let {FieldParameters} = this.state;
        let AccountingUnit = updatedRevitSetting && updatedRevitSetting.AccountingUnit && updatedRevitSetting.AccountingUnit;
        let initial = updatedRevitSetting ? AccountingUnit : null;

        return (
            <div style={{display: 'block'}}>
                <Form.Item label="Accounting Unit" style={{marginTop: '30px', width: '300px', marginBottom: '70px'}}>
                    {getFieldDecorator('AccountingUnit', {
                        initialValue: initial,
                        rules: [{required: true, message: "Required!"}],
                    })(<Select
                        size={'large'}
                        placeholder="Please select Accounting Unit"
                        onChange={this.handleChangeSORQuantity}
                    >
                        {
                            Array.isArray(FieldParameters) && FieldParameters.map((param, index) => {
                                return (
                                    <Select.Option key={index} value={param.Parameter}>{param.FieldName}</Select.Option>
                                )
                            })
                        }
                    </Select>)}
                </Form.Item>
            </div>
        )

    };


    getFieldParametersTable = (columns, fieldParamdata) => {


        return (

            <Table
                columns={columns}
                dataSource={fieldParamdata}
                className=" new-table-design delete-last"
                pagination={{pageSize: 5}}
                size="large"
            />
        )

    };


    getCheckBoxInputs = (getFieldDecorator) => {
        let {disabled} = this.state;
        let {updatedRevitSetting} = this.props;
        return (

            <Form.Item style={{marginTop: '30px'}}>
                <div style={{display: 'block'}}>
                    <Form.Item style={{width: '200px'}}>
                        {getFieldDecorator('GrandTotal', {
                            valuePropName: 'checked',
                            initialValue: updatedRevitSetting && updatedRevitSetting.GrandTotal && updatedRevitSetting.GrandTotal,
                            rules: [{required: false,}]
                        })(<Checkbox disabled={disabled}> Grand Total</Checkbox>)}
                    </Form.Item>


                </div>
            </Form.Item>
        )

    };

    getActions = () => {

        return (

            <Form.Item>
                <Row type="flex" className="margin-top-60">
                    <Col span={3} className="edit-black-buttons float-left">
                        <Button type="primary" value="large" htmlType="submit"
                                onClick={this.handleFinalSubmitforRevitSetting}>
                            <div style={{color: 'white'}}>Submit</div>
                        </Button>
                    </Col>
                    <Col span={3} className="unfill-edit-black-buttons float-right">
                        <Button type="primary" value="large"
                                onClick={() => this.props.history.push("/dashboard/revit-management/revit-settings")}>Cancel</Button>
                    </Col>
                </Row>
            </Form.Item>
        )

    };

    closePage = () => {

        this.props.history.push('/dashboard/revit-management/revit-settings')

    };


    getSORParameter = (getFieldDecorator) => {

        let initialParams = [];
        let {SorParameters, FieldParameters} = this.state;
        SorParameters && SorParameters.forEach((param) => {
            initialParams.push(param.Parameter);
        });

        return (
            <div>
                <Form.Item label="SOR Parameters">
                    {getFieldDecorator('SorParameters', {
                        initialValue: initialParams,
                        rules: [{required: true, message: "Required!"}]
                    })(<Select placeholder={"Select SOR Parameters"}
                               mode="multiple" size={'large'}>
                        {
                            Array.isArray(FieldParameters) && FieldParameters.map((param, index) => {
                                return (
                                    <Select.Option key={index} value={param.Parameter}>{param.FieldName}</Select.Option>
                                )
                            })
                        }
                    </Select>)}
                </Form.Item>
            </div>

        )

    };

    getBQCategories = (getFieldDecorator) => {
        let {bqCategories} = this.props;

        let arrayBq = Array.isArray(bqCategories);
        return (

            <Form.Item label="BQ Categories" style={{marginTop: '30px', width: '300px', marginBottom: '70px'}}>
                {getFieldDecorator('BqCategory', {
                    rules: [{
                        required: true,
                        message: "Required!",
                    }],
                })(<Select mode={'single'} placeholder="Please select BQ category">
                    {
                        arrayBq ? bqCategories && bqCategories.map((data, v) => {
                            return (
                                <Select.Option key={v} value={data.id}>{data.type}</Select.Option>
                            )
                        }) : null
                    }
                </Select>)}
            </Form.Item>
        )

    };

    getBreadCrumbs = () => {

        let breadCrumbsJSON = [
            {
                title: "Dashboard",
                returnPath: "/dashboard"
            },
            {
                title: "Revit Settings",
                returnPath: "/dashboard/revit-management/revit-settings"
            },
            {
                title: 'Setting'
            }
        ];


        return (
            <Row span={24} style={{marginLeft: '-15px', marginBottom: '20px'}}>
                <Col span={24}>
                    <BreadCrumb style={{fontSize: "20px"}} crumbsJSON={breadCrumbsJSON}/>
                    <p className="custom-button custom-button-light pointer"
                       style={{marginTop: '-40px', marginLeft: '17px'}} onClick={this.closePage}><Icon
                        type="caret-left"/> Back</p>
                </Col>
            </Row>
        )
    };


    getAllFilters = (getFieldDecorator, filterColumns) => {

        return (
            <div className={"mt-5"} style={{width:"100%"}}>
                <div>
                    <p style={{fontWeight: '70', fontSize: '20px', color: 'black'}}>{'Filters:'}</p>
                </div>
                <Table
                    columns={filterColumns}
                    dataSource={this.state.Filters}
                    className='new-table-design1'
                    pagination={{pageSize: 5}}
                    size="large"
                />
            </div>
        )
    };

    handleSubmit = (e) => {


        e.preventDefault();


        this.props.form.validateFields((err, values) => {
            if (!err) {

                let {FieldParameters} = this.state;
                let SorParameters = [];

                values.SorParameters.forEach((sorParam) => {
                    for (let i = 0; i < FieldParameters.length; i++) {
                        if (sorParam === FieldParameters[i].Parameter) {
                            SorParameters.push({
                                FieldName: FieldParameters[i].FieldName,
                                Parameter: FieldParameters[i].Parameter
                            });
                            break;
                        }
                    }
                });

                values.SorParameters = SorParameters;

                if (this.state.FinalSubmit) {
                    this.props.saveRevitSetting(values,false,() => {
                            this.success(this.props.message && this.props.message)
                        },
                        () => {
                            this.props.history.push('/dashboard/revit-management/revit-settings')
                        }
                        , () => {
                            this.errors(this.props.message && this.props.message)
                        }
                    )
                }
                else {
                    return null
                }


            }

        });
    };

    renderHeader = () => {
        return (
            <div style={{display: 'block'}}>
                <h2>{this.props.updatedRevitSetting && this.props.updatedRevitSetting ? 'Update Setting' : 'Add Setting'}</h2>
                <p style={this.formLabelLine}>Please fill the form to complete setting</p>
                <hr style={this.formSeparator}/>
            </div>
        )
    };

    render() {


        const FieldParameters = this.state.FieldParameters && this.state.FieldParameters.map((data, index) => {
            return {
                filedParamIndex: index,
                FieldName: data.FieldName,
                Unit: data.Unit ? data.Unit : "-",
            }
        });


        const {getFieldDecorator} = this.props.form;


        return (
            <>
                {this.props.processing ? <NewLoader/> : null}
                {this.getBreadCrumbs()}
                {this.renderHeader()}
                <Form onSubmit={this.handleSubmit} style={{width: '100%'}}>


                    <Row className="custom-popup-row">

                        <Col className="custom-form-wrap">
                            <Col>

                                {this.getFileName(getFieldDecorator)}
                                {this.getMaterialCategory(getFieldDecorator)}

                                {this.getFieldParametersTable(this.columns, FieldParameters)}

                                {this.getSortingParameter(getFieldDecorator)}
                                {this.getAllFilters(getFieldDecorator, this.columnsFilter)}

                                {this.getAccountingUnit(getFieldDecorator)}

                                {this.getSORParameter(getFieldDecorator, fieldParamList)}

                                {this.getBQCategories(getFieldDecorator)}

                                {this.getCheckBoxInputs(getFieldDecorator)}

                                {this.getActions()}

                            </Col>
                        </Col>
                    </Row>
                </Form>


            </>
        )
    }
}


const mapStateToProps = (state) => {


    return {
        fieldParams: state.revit_setting.activeParams && state.revit_setting.activeParams,
        TotalFieldParams: state.revit_setting.activeParams && state.revit_setting.fieldParams,
        fieldParamsSubset: state.revit_setting.activeParams && state.revit_setting.fieldParamsSubset,
        totalPages: state.revit_setting.totalPages && state.revit_setting.totalPages,
        takeoffMaterialParams: state.revit_setting.materialParams && state.revit_setting.materialParams,
        scheduleMaterialParams: state.revit_setting.scheduleMaterialParams && state.revit_setting.scheduleMaterialParams,
        updatedRevitSetting: state.revit_setting.updatedRevitSetting && state.revit_setting.updatedRevitSetting,
        bqCategories: state.revit_setting.bqCategories && state.revit_setting.bqCategories,
        message: state.revit_setting.message && state.revit_setting.message,
        processing: state.revit_setting.processing && state.revit_setting.processing,
    }

};

const revitSaveSetting = Form.create({})(RevitSettingForm);

const connectedComponent = connect(mapStateToProps, {
    loadFieldParameters, loadTakeoffMaterialCategories, loadScheduleMaterialCategories, loadFieldParametersSubset,
    saveRevitSetting, loadBqCategories
})(revitSaveSetting);
export default withRouter(connectedComponent);