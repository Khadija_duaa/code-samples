import React from 'react';
import {Row, Col, Table, Form, message} from 'antd';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {loadRevitSettings, deleteRevitSetting, setRevitSetting} from "../../../../../store/revitManagement/revit_actions";
import NewLoader from '../../../../Loader/NewLoader';


class RevitSettings extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showHide: true,
            radioValue: 1,
            visible: false,
            visible2: false,
            showDetailsBox: true,
            searchText: '',
            value: '',
            modalOpen: false,
            index: '',
        }
    }


    componentDidMount() {
        this.props.setRevitSetting([]);
        this.props.loadRevitSettings();
    };


    showHide = () => {
        this.setState({
            showHide: !this.state.showHide,
        })
    };
    radioValue = (e) => {
        this.setState({
            radioValue: e.target.value,
        });
    };


    showModal = () => {
        this.setState({
            visible: true,
        });
    };



    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };


    success = (msg) => {

        message.success(msg);
    };

    errors = (msg) => {

        message.error(msg);
    };


    handleSubmit = (e) => {
        e.preventDefault();
    };


    renderHeading = () => {

        return (
            <div className="container-fluid mb-5">
                <div className="row">
                    <div className="col-md-12">
                        <h3>Revit Settings</h3>
                    </div>
                </div>
            </div>
        )
    };

    renderSettingsData = () => {

        const data = Array.isArray(this.props.revitSetting && this.props.revitSetting);
        let revitDataSource = this.props.revitSetting && this.props.revitSetting;

        let accountingUnit = '';
        if (data) {
            revitDataSource.forEach((dataSource)=>{
                accountingUnit= '';
                for (let index = 0; index < dataSource.FieldParameters.length; index++) {
                    if (dataSource.AccountingUnit && dataSource.FieldParameters[index].Parameter === dataSource.AccountingUnit) {
                        accountingUnit = dataSource.FieldParameters[index].FieldName;
                        dataSource.AccountingUnit = accountingUnit;
                        break;
                    }
                }
            });
        }

        const contractorColumns = [
            {
                title: 'Material Category',
                dataIndex: 'FileName',
                width: 200,

            }, {
                title: 'Accounting Unit',
                dataIndex: 'AccountingUnit',
                width: 200,
            },
            {
                title: 'Action',
                dataIndex: '_id',
                width: 200,

                render: (text, record) => (
                    <span style={{textDecoration: "underline"}}>
                    <a href="javascript:" onClick={() => {
                        this.props.setRevitSetting(record);
                        this.props.history.push({
                            pathname: '/dashboard/revit-management/revit-settings-update',
                            state: {settings: true}
                        });
                    }} style={{marginRight: '10px'}}>Edit</a>
                </span>
                )
            }
        ];

        return (
            <Row>
                <Col span={24} className="firms">
                    <Row className="">
                        <Col span={24}>
                            <Table
                                columns={contractorColumns}
                                rowKey={(record, index) => index}
                                dataSource={data ? revitDataSource && revitDataSource : null}
                                className="zero-padding"
                                pagination={false}
                                size="large" s
                                onRow={(record) => ({
                                    onClick: () => {
                                        this.props.setRevitSetting(record);
                                        this.props.history.push({
                                            pathname: '/dashboard/revit-management/revit-settings-update',
                                            state: {settings: true}
                                        });
                                    }
                                })}
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
        )
    };

    render() {
        // firms data

        if (this.props.processing) return <NewLoader/>;

        return (
            <>
                {this.renderHeading()}
                {this.renderSettingsData()}
            </>
        )
    }
}


const mapStateToProps = (state) => {


    return {
        revitSetting: state.revit_setting.revitSetting && state.revit_setting.revitSetting,
        message: state.revit_setting.revitSetting && state.revit_setting.message,
        error: state.revit_setting.revitSetting && state.revit_setting.error,
        processing: state.revit_setting.revitSetting && state.revit_setting.processing,
    }
};


const Setting = Form.create({})(RevitSettings);

const
    connectedComponent = connect(mapStateToProps, {loadRevitSettings, deleteRevitSetting, setRevitSetting})(Setting);
export default withRouter(connectedComponent);