import React from 'react';
import { Row, Col } from 'antd';

class OfficeStatus extends React.Component {


  render() {
    
    return (
      <div className="office-status">
        <h5 className="office-stats-title">Office Status</h5>
        <div><Row className="office-stats-container" gutter={30}>
          <Col span={8} className="office-stats-cont-sec">
            <Row className="office-stats-m">
              <div className="office-tation-t">Total Online Stations</div>
              <div><Row><Col span={9} className="office-main-num">90</Col><Col span={1} className="office-sec-num">/</Col><Col className="office-sec-num" span={6}>101</Col></Row></div>
            </Row>
            <Row className="office-stats-m">
              <div className="office-tation-t">Total Online Office</div>
              <div><Row><Col span={9} className="office-main-num" style={{ color: "#4680ff" }}>50</Col><Col span={1} className="office-sec-num">/</Col><Col className="office-sec-num" span={6}>90</Col></Row></div>
            </Row>
          </Col>
          <Col span={8} className="office-stats-cont-sec">
            <div style={{ marginTop: "12%", height: "100%" }}>
                <div className="office-container-name">
                  <Row>
                          <Col span={16} className="office-text">
                              Sialkot Cantt
                          </Col>
                          <Col>
                            <Row><Col span={3} className="office-main-num-s">10</Col><Col span={1} className="office-sec-num-s">/</Col><Col className="office-sec-num-s" span={2}>11</Col></Row>
                          </Col>
                  </Row>
                </div>
                <div className="office-container-name">
                  <Row>
                          <Col span={16} className="office-text">
                              Lahore Cantt
                          </Col>
                          <Col>
                            <Row><Col span={3} className="office-main-num-s">30</Col><Col span={1} className="office-sec-num-s">/</Col><Col className="office-sec-num-s" span={2}>50</Col></Row>
                          </Col>
                  </Row>
                </div>
                <div className="office-container-name">
                  <Row>
                          <Col span={16} className="office-text">
                              Gujrat Cantt
                          </Col>
                          <Col>
                            <Row><Col span={3} className="office-main-num-s">10</Col><Col span={1} className="office-sec-num-s">/</Col><Col className="office-sec-num-s" span={2}>20</Col></Row>
                          </Col>
                  </Row>
                </div>
                <div className="office-container-name">
                  <Row>
                          <Col span={16} className="office-text">
                              Gujranwala Cantt
                          </Col>
                          <Col>
                            <Row><Col span={3} className="office-main-num-s">20</Col><Col span={1} className="office-sec-num-s">/</Col><Col className="office-sec-num-s" span={2}>40</Col></Row>
                          </Col>
                  </Row>
                </div>
                <div className="office-container-name">
                  <Row>
                          <Col span={16} className="office-text">
                              Abatabad Cantt
                          </Col>
                          <Col>
                            <Row><Col span={3} className="office-main-num-s">50</Col><Col span={1} className="office-sec-num-s">/</Col><Col className="office-sec-num-s" span={2}>70</Col></Row>
                          </Col>
                  </Row>
                </div>
            </div>
          </Col>
          <Col span={8}  className="office-stats-cont-sec">
            <div style={{ marginTop: "12%", height: "100%" }}>
                   <div className="office-container-name">
                      <Row>
                              <Col span={16} className="office-text">
                                  Rawalpindi Cantt
                              </Col>
                              <Col>
                                <Row><Col span={3} className="office-main-num-s">99</Col><Col span={1} className="office-sec-num-s">/</Col><Col className="office-sec-num-s" span={2}>101</Col></Row>
                              </Col>
                      </Row>
                    </div>
                    <div className="office-container-name">
                      <Row>
                              <Col span={16} className="office-text">
                                  Kashmir Cantt
                              </Col>
                              <Col>
                                <Row><Col span={3} className="office-main-num-s">40</Col><Col span={1} className="office-sec-num-s">/</Col><Col className="office-sec-num-s" span={2}>20</Col></Row>
                              </Col>
                      </Row>
                    </div>
                    <div className="office-container-name">
                      <Row>
                              <Col span={16} className="office-text">
                                  Islamabad Cantt
                              </Col>
                              <Col>
                                <Row><Col span={3} className="office-main-num-s">90</Col><Col span={1} className="office-sec-num-s">/</Col><Col className="office-sec-num-s" span={2}>100</Col></Row>
                              </Col>
                      </Row>
                    </div>
                    <div className="office-container-name">
                      <Row>
                              <Col span={16} className="office-text">
                                  Daska Cantt
                              </Col>
                              <Col>
                                <Row><Col span={3} className="office-main-num-s">50</Col><Col span={1} className="office-sec-num-s">/</Col><Col className="office-sec-num-s" span={2}>80</Col></Row>
                              </Col>
                      </Row>
                    </div>
                    <div className="office-container-name">
                      <Row>
                              <Col span={16} className="office-text">
                                  Karachi Cantt
                              </Col>
                              <Col>
                                <Row><Col span={3} className="office-main-num-s">20</Col><Col span={1} className="office-sec-num-s">/</Col><Col className="office-sec-num-s" span={2}>40</Col></Row>
                              </Col>
                      </Row>
                    </div>
            </div>
          </Col>
        </Row></div>
      </div>
    )
  }
}

export default OfficeStatus;