import React from 'react';
import {withRouter} from "react-router-dom";
import {Row, Col, Icon} from 'antd';
import {connect} from 'react-redux';

class ProjectsStats extends React.Component {
    renderProjectsStats = (rowGutter) => {
        let projectsStats = this.props.projectsStats;

        let statsArray = [
            {
                title: "Office Projects",
                subText: "Office Projects",
                data: projectsStats ? projectsStats.normalCount.myOfficeTotal : "-",
                textColor: "#24dbcb",
                link: null
            },
            {
                title: "In Planning & Design",
                subText: "See Office Planning & Design",
                data: projectsStats ? projectsStats.normalCount.myOfficePlaning : "-",
                textColor: "#4680ff",
                link: projectsStats ? {
                    pathname: "/dashboard/planning-works/myProjects",
                    state: {category: "Planing", type: "work"}
                } : null
            },
            {
                title: "In Execution",
                subText: "See Office Execution Projects",
                data: projectsStats ? projectsStats.normalCount.myOfficeExecution : "-",
                textColor: "#4680ff",
                link: projectsStats ? {
                    pathname: "/dashboard/execution-works/myProjects",
                    state: {category: "Execution", type: "work"}
                } : null
            },
            {
                title: "Associated Projects",
                subText: "Associated Projects",
                data: projectsStats ? projectsStats.normalCount.myOfficeAssocTotal : "-",
                textColor: "rgb(35, 219, 209)",
                link: null
            },
            {
                title: "In Planning & Design",
                subText: "See Planning & Design",
                data: projectsStats ? projectsStats.normalCount.myOfficeAssocPlaning : "-",
                textColor: "#ffa800",
                link: {
                    pathname: "/dashboard/planning-associated/associated",
                    state: {category: "Planing", type: "associated"}
                }
            },
            {
                title: "In Execution",
                subText: "See Execution Projects",
                data: projectsStats ? projectsStats.normalCount.myOfficeAssocExecution : "-",
                textColor: "#ffa800",
                link: {
                    pathname: "/dashboard/execution-associated/associated",
                    state: {category: "Execution", type: "associated"}
                }
            },
        ];

        return statsArray.map((stats, index) => {
            return (
                <Col key={index} span={rowGutter / statsArray.length}>
                    <div className="top-dashboard-stats">
                        <Row className="custom-new-row" gutter={12}>
                            <Col span={24}>
                                <Row>
                                    <div className="dash-main-chart-title">{stats.title}</div>
                                </Row>
                                <Row>
                                    <div className="dash-main-chart-num" style={{color: stats.textColor}}>
                                        {stats.data}
                                    </div>
                                </Row>
                            </Col>
                        </Row>
                        <Row style={{padding: "5%", paddingTop: "15%"}}>
                            <Col className="dash-nav-text" span={22}>{stats.subText}</Col>
                            <Col span={2}>
                                <Icon style={{display:`${stats.link?'block':'none'}`}} className="custom-arrow" type="arrow-right"
                                      onClick={() => stats.link ? this.props.history.push(stats.link) : null}/>
                            </Col>
                        </Row>
                    </div>
                </Col>
            )
        });

    };

    render() {
        let rowGutter = 24;
        return (
            <Row gutter={rowGutter} style={{width: '101%', marginRight: "unset"}}>
                {this.renderProjectsStats(rowGutter)}
            </Row>
        )
    }
}


const mapStateToProps = state => {
    return {
        projectsStats: state.stats_reducer.projectsStats
    }
};

const connected = connect(mapStateToProps)(ProjectsStats);
export default withRouter(connected);