
import React from 'react';
import { Row, Col, Tabs, Table, Modal, Button, Input } from 'antd';
import '../dashboard.css';
import OfficeTable from './TableOffice';
import ProjectTable from './TableProject'
class DataTables extends React.Component {
     constructor(props) {
          super(props)
          this.state = {
               view: 'office'
          }
     }

     componentDidMount() {

          this.setState({
               view: this.props.view
          })
     }

     render() {

          return (<>
               <div>

                    {this.state.view === "office" && <div>
                         <Row>
                              <h4>Offices in E in C</h4>
                         </Row>
                         <Row gutter={40} className="inner-container-child-table">
                              <Col><OfficeTable/></Col>
                         </Row>

                    </div>}
                    {this.state.view === "project" && <div>
                         <Row>
                              <h4>Projects in E in C</h4>
                         </Row>
                         <Row gutter={40} className="inner-container-child-table">
                         <Col><ProjectTable/></Col>
                         </Row>

                    </div>}
               </div></>)

     }
}



export default DataTables;