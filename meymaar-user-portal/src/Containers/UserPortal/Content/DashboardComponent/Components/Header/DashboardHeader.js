import React from 'react';
import {Row, Col, Select} from 'antd';

const Option = Select.Option;

export const DashboardHeader = ()=>{
   const getSelectOptions = () => {
        let optionsArray = [
            {
                value: "all",
                title: "All"
            },
            {
                value: "army",
                title: "ARMY"
            },
            {
                value: "paf",
                title: "PAF"
            },
            {
                value: "navy",
                title: "NAVY"
            },
            {
                value: "dp",
                title: "DP"
            },
        ];

        return optionsArray.map((option, index) => {
            return (
                <Option key={index} value={option.value}>{option.title}</Option>
            )
        })

    };

    return(
      <Row style={{marginBottom: "20px"}}>
          <Col span={12}>
              <h3>Dashboard</h3>
          </Col>
          <Col span={12}>
              <Select defaultValue="ALL" className="new-add-assign-select float-right" style={{width: 120}}>
                  {getSelectOptions()}
              </Select>
          </Col>
      </Row>
  )
};