import React, { PureComponent } from 'react';
import {
  ResponsiveContainer, PieChart, Pie, Legend, Cell
} from 'recharts';

// const COLORS = ['#4680ff','#f1b912', '#9e0c42', '#24dbcb'];
const COLORS = ['#4680ff','#FB6081', '#d1d1d4'];


const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx, cy, midAngle, innerRadius, outerRadius, percent, index, name, value
}) => {
   const radius = innerRadius + (outerRadius - innerRadius) * 1.2;
  const x = cx + radius * Math.cos(-midAngle * RADIAN)*1.2;
  const y = cy + radius * Math.sin(-midAngle * RADIAN)*1.2;
  let colorS = COLORS[index % COLORS.length];

  return (
    <text x={x} y={y} fill={colorS} style={{fontWeight:"600", fontSize:"16", color:colorS}} textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {value}
    </text>
  );
};

export default class Example extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
      data: this.props.data
    }
  }
   renderCustomizedLabel =() => {
       let label = this.state.data.map((item) => {
         return (item.name + ":" + item.value)
       });
       return label;
   };

  render() {
    return (
      <div style={{ width: '100%', height: '180px' }}>
        <ResponsiveContainer className="pi-charts-legends">
          <PieChart className="custom-pie custom-gauge">
            <Pie isAnimationActive={false} dataKey="value" data={this.state.data} fill="#8884d8" innerRadius={50} outerRadius={60} label={renderCustomizedLabel} labelLine={false}>
            {
              this.state.data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
            }
            {/* <Label value="any" offset={0} position="left" className="charts-heading-lable"/> */}
            </Pie>
            <Legend verticalAlign="middle" width={30} align="left" className="charts-new-legends" />
          </PieChart>
        </ResponsiveContainer>
      </div>
    );
  }
}
