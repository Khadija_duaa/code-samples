import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import '../ExecutionGraph/exec-style.css';
import {ComposedChart,  XAxis, YAxis, CartesianGrid, Tooltip, Bar, Legend, LabelList} from 'recharts';


class ExecutionLineGraph extends PureComponent {

    getExecutionStats = () => {
        let {executionStats} = this.props;
        let data = [];

        let percentageKeys = Object.keys(executionStats);
        percentageKeys.forEach((key, index) => {
            let {innerProgressMap} = executionStats[key];
            let innerKeys = Object.keys(innerProgressMap);
            let innerProgress = [];
            let innerKey = [];
            data.push({});
            innerKeys.forEach(inner => {
                innerProgress.push(innerProgressMap[inner]);
                innerKey.push(inner);
            });

            data[index].details = innerProgress;
            data[index].name = `${key} %`;
            data[index].Projects = executionStats[key].projectCount;
        });

        return data;
    };

    renderCustomizedLabel = (props) => {
        const {
            x, y, width, value,
        } = props;
        const radius = 10;

        return (
            <g>
                <circle cx={x + width / 2} cy={y - radius} r={radius} fill="#8884d8"/>
                <text x={x + width / 2} y={y - radius} fill="#fff" textAnchor="middle" dominantBaseline="middle">
                    {value}
                </text>
            </g>
        );
    };

    renderExeuctionGraph = () => {
        let executionData = this.getExecutionStats();
        return (
            <ComposedChart
                className={"exec-stats"}
                width={700}
                height={350}
                data={executionData}
                barGap={0}
                barCategoryGap={0}
                margin={{
                    top: 5, right: 30, left: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis/>
                <Tooltip/>
                <Legend/>

                <Bar dataKey="Projects" fill="#8884d8">
                    <LabelList dataKey={"Projects"} content={this.renderCustomizedLabel}/>
                </Bar>

            </ComposedChart>
        )
    };

    render() {
        if (!this.props.executionStats) return null;
        return (
            <>
                {this.renderExeuctionGraph()}
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        executionStats: state.stats_reducer.executionStats
    }
};

const connected = connect(mapStateToProps)(ExecutionLineGraph);
export default connected;