import React from 'react';
import ExecutionLineGraph from '../TopCharts/ExecutionLineGraph'

class ExecutionGraph extends React.Component {

    render() {
        return (
            <div className="execution-graph">
                <h5 className="execution-title">EXECUTION</h5>
                <div>
                    <ExecutionLineGraph/>
                </div>
            </div>
        )
    }
}

export default ExecutionGraph;