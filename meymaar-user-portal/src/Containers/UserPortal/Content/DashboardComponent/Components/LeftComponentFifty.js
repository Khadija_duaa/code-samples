import React from 'react';
import PlanningAndDesignChart from './chartsLeft/PlanningAndDesignChart';


class LeftComponent extends React.Component{
    render(){

        return (
            <div className="custom-box-full">
                <h5 className="charts-heading">Planning and design overview</h5>
                <PlanningAndDesignChart width={1563} height={500} dy={13}/>
            </div>
        )

        // return (
        //   <div className="custom-box-fifty-left">
        //       <div className={"finance-top"}>
        //           <h5 className={"finance-title"}>Planning and design overview</h5>
        //       </div>
        //        <PlanningAndDesignChart width={700} height={380} dy={13}/>
        //   </div>
        // )
    }
}


export default LeftComponent;