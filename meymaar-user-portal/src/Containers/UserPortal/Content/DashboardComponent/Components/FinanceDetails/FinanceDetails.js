import React from 'react';
import {Row, Col, Select, Form} from 'antd';
import {connect} from 'react-redux';
import {formatAmount} from "../../../../../../utils/common-utils";

const {Option} = Select;

class FinanceDetails extends React.Component {

    state = {
        fiscalDefault: null
    };

    renderFinancialYearStats = () => {
        let {financialStats, financeStyle} = this.props;
        let oneMillion = 1000000;
        let currentFiscalYear;
        let data = [];

        if (financialStats.length) {
            currentFiscalYear = this.state.fiscalDefault ? this.state.fiscalDefault : financialStats[0].fiscalYear;
            data = financialStats.filter((stat) => stat.fiscalYear === currentFiscalYear);
        }

        let jsx = <Row>
            <Col span={8} className="finance-col-words-titles">
                <Row className="finance-stats-title">Contracted Value</Row>
                <Row className="finance-stats-value">
                    {data.length ? `${formatAmount(parseFloat(data[0].cost) / oneMillion)} M` : "-"}
                </Row>
            </Col>
            <Col span={8} className="finance-col-words-titles">
                <Row className="finance-stats-title">Paid</Row>
                <Row className="finance-stats-value">
                    {data.length ? `${formatAmount(parseFloat(data[0].paidAmount) / oneMillion)} M` : "-"}
                </Row>
            </Col>
            <Col span={8} className="finance-col-words-titles">
                <Row className="finance-stats-title">Balance</Row>
                <Row className="finance-stats-value">
                    {data.length ? `${formatAmount((parseFloat(data[0].cost) - parseFloat(data[0].paidAmount)) / oneMillion)} M` : "-"}
                </Row>
            </Col>
        </Row>;


        return (
            <Row gutter={8} className="finance-layout-new" style={financeStyle && financeStyle}>
                {jsx}
            </Row>
        )
    };

    // Rendering Fiscal Year Select
    renderFiscalSelect = () => {
        let data = this.props.financialStats;

        let options = data.length && data.map((item, index) => {
            return (
                <Option key={index} value={item.fiscalYear}>{item.fiscalYear}</Option>
            )
        });

        return (
            <Row gutter={24}>
                <Col span={12} style={{float: "right", display: "flex"}}>
                    <p style={{width: "30%", fontWeight: "600", fontSize: "1.1em"}}>Fiscal Year:</p>
                    <Select style={{width: "70%"}} defaultValue={data.length && data[0].fiscalYear}
                            onChange={(id, event) => this.setState({fiscalDefault: event.props.children})}>
                        {options}
                    </Select>
                </Col>
            </Row>
        )
    };

    renderFinanceHeading = () => {

        let {titleClass, financeClass, titleStyle} = this.props;

        return <div className={financeClass && financeClass}>
            <h5 className={titleClass} style={titleStyle && titleStyle}>Finance Layout</h5>
        </div>;
    };

    render() {
        if (!this.props.financialStats) {
            return null;
        }

        return (
            <>
                {this.renderFinanceHeading()}
                {this.renderFiscalSelect()}
                {this.renderFinancialYearStats()}
            </>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        financialStats: state.stats_reducer.financialStats
    }
};

const connected = connect(mapStateToProps)(FinanceDetails);

export default Form.create()(connected);