import React from 'react';
import {DashboardHeader} from "./Header/DashboardHeader";
import ProjectsStats from './ProjectsStats/ProjectsStats';

class DashboardStats extends React.Component {

    render() {
        return (
            <div className="topDasboard-box">
                <DashboardHeader/>
                <ProjectsStats/>
            </div>
        )
    }
}

export default DashboardStats;