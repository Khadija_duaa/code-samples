import React from 'react';
import {Row, Col, Tabs, Table, Modal, Button, Input} from 'antd';
import '../dashboard.css'
import data from '../../../../../store/dashboard/data';
import {SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS} from 'constants';

class List extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {

    }

    render() {

        return (<>
            <div>
                <div className="small-heading">Projects under Authorised Oﬃces</div>
                <div className="list-container-dash">{
                    this.props.data.map((item, index) => {
                        return <Row key={index} className="custom-dash-list">
                            <Col span={16} className="list-text">{item.name}</Col>
                            <Col span={8} className="list-number">{item.value}</Col>
                        </Row>
                    })
                }</div>
            </div>
        </>)

    }
}


export default List;