import React from 'react';
import { Row, Col } from 'antd';
import Donut from './TopCharts/Donuts';
import CustomGuage from './TopCharts/CustomGuage'

class FinanceEffect extends React.Component {

     render() {

          const data = [
               { name: 'Approved', value: 33 }, 
               { name: 'Objection', value: 33 },
               { name: 'Remaining', value: 34 },
          ];

          const data1 = [
               { name: 'Processed', value: 13 }, 
               { name: 'Objection', value: 16 },
               { name: 'Remaining', value: 18 }
          ];

          const data2 = [
               { name: 'Processed', value: 20 }, 
               { name: 'Objection', value: 17 },
               { name: 'Remaining', value: 16 },
          ];

          return (
               <div className="custom-box-fifty-right">
                    <h5 className="execution-title">Financial Effects</h5>
                    <Row>
                         <Col span={12}>
                              <div style={{ borderRight: "1px solid #00000021", height: "356px" }}>
                                   <Row><Col span={24} style={{marginTop:"8%"}}><Donut data={data} /></Col></Row>
                              </div>
                         </Col>
                         <Col span={12}>
                              <Row> <div style={{ borderBottom: "1px solid #00000021", height: "178px" }}>
                              <Row><Col span={24}><p className="charts-heading-lable">DOs</p><CustomGuage data={data1}/></Col></Row>
                              </div></Row>
                              
                              <Row><div style={{ height: "178px" }}>
                              <Row><Col span={24}><p className="charts-heading-lable">Amendments</p><CustomGuage data={data2}/></Col></Row>
                              </div></Row>
                         </Col>
                    </Row>
               </div>
          )
     }
}

export default FinanceEffect;