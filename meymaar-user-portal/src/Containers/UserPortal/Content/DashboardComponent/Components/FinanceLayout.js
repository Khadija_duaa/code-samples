import React from 'react';
import {Row, Col} from 'antd';
import ProgressFinance from './TopCharts/ProgressFinance';
import FinanceDetails from './FinanceDetails/FinanceDetails';

class FinanceLayout extends React.Component {

    callback = (key) => {
        console.log(key);
    };

    renderProgressFinance = () => {
        let data1 = {
            amount: 60,
            total: 170,
            color: "#30bda0",
            dept: "ARMY"
        };

        let data2 = {
            amount: 20,
            total: 40,
            color: "#4680ff",
            dept: "NAVY"
        };

        let data3 = {
            amount: 76,
            total: 155,
            color: "#04c4dc",
            dept: "PAF"
        };

        let data4 = {
            amount: 19,
            total: 135,
            color: "#fb6180",
            dept: "DP"
        };


        return (
            <div className="finance-bottom">
                <Row className="finance-progress-container">
                    <Row gutter={24}>
                        <Col className="finance-box-dash" span={12}><ProgressFinance data={data1}/></Col>
                        <Col className="finance-box-dash" span={12}><ProgressFinance data={data2}/></Col>
                    </Row>
                    <Row gutter={24}>
                        <Col className="finance-box-dash" span={12}><ProgressFinance data={data3}/></Col>
                        <Col className="finance-box-dash" span={12}><ProgressFinance data={data4}/></Col>
                    </Row>
                </Row>
            </div>
        )
    };

    render() {

        let financeDetailsProps = {
          financeClass:"finance-top",
          titleClass : "finance-title",
          financeStyle : {height: "100%",minHeight:"130px"},
        };
        return (
            <div className="finance-layout-dash">
                <FinanceDetails {...financeDetailsProps}/>
                {this.renderProgressFinance()}
            </div>
        )
    }
}

export default FinanceLayout;