import React from 'react';
import OfficesMap from './OfficeAndUsers/OfficesMap';

class OfficeAndUsers extends React.Component{
    render(){

        return (
          <div className="custom-box-bottom">
               <OfficesMap/>
          </div>
        )
    }
}

export default OfficeAndUsers;