import React from 'react';
import {Row, Col} from 'antd';
import {connect} from 'react-redux';

class UserStats extends React.Component {

    renderUserStats = () => {
        let {contractorStats} = this.props;
        return (
            <>
                <Row style={{background: "white", height: "49%", boxShadow: "0px 0px 11px #e0e5e8"}}>
                    <h5 className="user-cont-title">Users Online</h5>
                    <div style={{marginTop: "1%"}}>
                        <Row className="user-stats-contract">
                            <div className="office-tation-t">Total Online Users</div>
                            <div><Row><Col span={9} className="office-main-num" style={{color: "#30bda0"}}>90</Col><Col
                                span={1} className="office-sec-num">/</Col><Col className="office-sec-num"
                                                                                span={6}>100</Col></Row></div>
                        </Row>
                    </div>
                </Row>
                <Row style={{height: "2%"}}/>
                <Row style={{background: "white", height: "49%", boxShadow: "0px 0px 11px #e0e5e8"}}>
                    <div className="user-cont-title">Contractors</div>
                    <div style={{marginTop: "1%"}}>
                        <Row className="user-stats-contract">
                            <div className="office-tation-t">Total Contracting Firms</div>
                            <div><Row><Col span={9} className="office-main-num"
                                           style={{color: "#30bda0"}}>{contractorStats?contractorStats.contractors:"-"}</Col></Row></div>
                        </Row>
                    </div>
                </Row>
            </>
        )
    };

    render() {
        return (
            <div className="user-stats">
                {this.renderUserStats()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        contractorStats: state.stats_reducer.contractorStats
    }
};

const connected = connect(mapStateToProps)(UserStats);
export default connected;