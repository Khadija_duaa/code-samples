import React from 'react';
import '../dashboard.css';

import DasboardStats from './DashboardStats'
import RightFifty from './RightComponentFifty';
import LeftFifty from './LeftComponentFifty'

import FinanceEffect from './FinanceEffect';
import OfficeStatus from './OfficeStatus';
import UserStats from './UserStats'

class MainDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {

        return (
            <>
                <DasboardStats/>
                <LeftFifty/>
                <RightFifty/>
                {/*<Executiongraph/>*/}
                <FinanceEffect/>
                <OfficeStatus/>
                <UserStats/>
            </>
        )
    }
}


export default MainDashboard;