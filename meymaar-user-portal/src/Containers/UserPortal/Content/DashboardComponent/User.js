import React from 'react';
import dashBoardAction from '../../../../store/dashboard/dashboard_actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MainDashboard from './Components/MainDashboard'
import {withRouter} from 'react-router-dom';
import './dashboard.css'
import {isGEDashboard} from "../../../../utils/server-utils";
import GEMainDashboard from "./GEComponents/GEMainDashboard";
import {hasAuthority} from "../../../../utils/common-utils";


class User extends React.Component {

    componentDidMount() {
        let {activeUser} = this.props;
        if(activeUser.principal.firstTime && !hasAuthority('SUPER_ADMIN')){
            this.props.history.push('/dashboard/profile');
        }else{
            this.props.getDashBoardData()
        }
    }



    render() {
        let isGEDash = isGEDashboard();

        if(isGEDash){
            return <GEMainDashboard/>
        }else{
            if (this.props.formStore.data !== null) {
                return (
                    <>
                        <MainDashboard data={this.props.formStore.data}/>
                    </>
                )
            }
            else if (this.props.formStore.loading) {
                return (
                    <></>
                )
            }
            else if (this.props.formStore.error) {
                return (<></>)
            }
        }
    }
}


const mapStateToProps = (state) => {
    return {
        formStore: state.dashboard_reducer,
        activeUser:state.user_reducer.activeUser
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDashBoardData: bindActionCreators(dashBoardAction.getDashboardData, dispatch)
    }
};

const connected =connect(mapStateToProps, mapDispatchToProps)(User);
export default withRouter(connected)
