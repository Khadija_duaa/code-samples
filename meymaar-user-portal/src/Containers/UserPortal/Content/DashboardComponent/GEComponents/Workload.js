import React from 'react';
import {connect} from 'react-redux';
import {Row, Col} from 'antd';
import {getWorkLoadStats} from "../../../../../utils/stats-common-utils";

class Workload extends React.Component {


    render() {
        let {projectsStats, phases} = this.props;
        if (!projectsStats || !phases) return null;

        let workLoadData = getWorkLoadStats();

        return (
            <div className="custom-box-fifty-right">
                <h5 className="execution-title">WORKLOAD</h5>
                <Row gutter={8}>
                    <Row>
                        {workLoadData.map((phaseStats, i) => {
                            return (
                                <Col key={i} span={12}>
                                    <div className="workload-container">
                                        <Row>
                                            <Col span={16} className="load-work-def">{phaseStats.name}</Col>
                                            <Col span={8} className="load-number">{phaseStats.value}</Col>
                                        </Row>
                                    </div>
                                </Col>
                            )
                        })
                        }
                    </Row>
                    {/* </Col> */}
                </Row>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        projectsStats: state.stats_reducer.projectsStats,
        phases: state.project_reducer.phases
    }
};

const connected = connect(mapStateToProps)(Workload);
export default connected;