import React from 'react';
import '../dashboard.css';
import DashboardStats from '../Components/DashboardStats';
import FinanceGE from './FinanceGE';
import LeftFifty from './LeftComponentFifty'
import Executiongraph from '../Components/ExecutionGraph/ExecutionGraph';
import FinanceEffect from './FinanceEffect';
import 'antd/dist/antd.css';
import Workload from './Workload'


class GEMainDashboard extends React.Component {
    render() {
        return (
            <>
                <DashboardStats/>
                <LeftFifty/>
                <Executiongraph/>
                <Workload/>
                <FinanceEffect/>
                <FinanceGE/>
            </>
        )
    }
}


export default GEMainDashboard;