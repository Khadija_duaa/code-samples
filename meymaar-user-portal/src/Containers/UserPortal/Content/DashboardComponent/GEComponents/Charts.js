
import React from 'react';
import { Row, Col, Tabs, Table, Modal, Button, Input } from 'antd';
import PieChart from './PieChart'
import '../dashboard.css'
import List from './List';
class Charts extends React.Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  componentDidMount() {

  }


  render() {
    let listData = [
      {
        name: " DW&CE",
        value: 49
      },
      {
        name: "ACE",
        value: 80
      },
      {
        name: "CMES",
        value: 56
      },
      {
        name: "GE",
        value: 44
      },
    ]
    return (<>
      <div>
        <Row>
          <h4>E in C</h4>
        </Row>
        <Row gutter={10} className="inner-container-child">
          <Col span={8} className="inner-container-dash"><List data={listData}/></Col>
          <Col span={14} className="inner-container-dash"><PieChart data={listData}/></Col>
        </Row>
      </div></>)

  }
}



export default Charts;