import React from 'react';
import ChartLeft from './chartsLeft/ChartLeft';


class LeftComponent extends React.Component{
    render(){

        return (
          <div className="custom-box-left">
               <h3>Execution</h3>
               <ChartLeft/>
          </div>
        )
    }
}

export default LeftComponent;