import React from 'react';
import { Row, Col } from 'antd';
import {
     G2,
     Chart,
     Geom,
     Axis,
     Tooltip,
     Coord,
     Label,
     Legend,
     View,
     Guide,
     Shape,
     Facet,
     Util
   } from "bizcharts";
import ChartOne from './charts/ChartOne';
import ChartTwo from './charts/ChartTwo';
import ChartThree from './charts/ChartThree';

class RightComponnet extends React.Component{
     
    render(){
     const data = [
          {
            sex: "男",
            sold: 0.45
          },
          {
            sex: "女",
            sold: 0.55
          }
        ];

        Shape.registerShape("interval", "radiusPie", {
          draw(cfg, container) {
            // 将归一化后的数据转换为画布上的坐标
            const points = cfg.origin.points;
            let path = [];
    
            for (let i = 0; i < cfg.origin.points.length; i += 4) {
              path.push(["M", points[i].x, points[i].y]);
              path.push(["L", points[i + 1].x, points[i + 1].y]);
              path.push(["L", points[i + 2].x, points[i + 2].y]);
              path.push(["L", points[i + 3].x, points[i + 3].y]);
              path.push(["L", points[i].x, points[i].y]);
              path.push(["z"]);
            }
    
            path = this.parsePath(path, true);
            const rect = container.addShape("path", {
              attrs: {
                fill: cfg.color || "#00D9DF",
                path
              }
            });
            const minH = Math.min(path[1][7], path[2][2]);
            const minW = Math.min(path[1][6], path[2][1]);
            const diffH = Math.abs(path[1][7] - path[2][2]);
            const diffW = Math.abs(path[1][6] - path[2][1]);
            container.addShape("circle", {
              attrs: {
                x: minW + diffW / 2,
                y: minH + diffH / 2,
                fill: cfg.color,
                radius: diffH / 2
              }
            });
            const minHH = Math.min(path[3][7], path[4][2]);
            const minWW = Math.min(path[3][6], path[4][1]);
            const diffHH = Math.abs(path[3][7] - path[4][2]);
            const diffWW = Math.abs(path[3][6] - path[4][1]);
            container.addShape("circle", {
              attrs: {
                x: minWW + diffWW / 2,
                y: minHH + diffHH / 2,
                fill: cfg.color,
                radius: diffH / 2
              }
            });
            return rect;
          }
        });
        const COLORS = ["#1890ff", "#f04864"];
        return (
            <div className="custom-box-right">
             <h3>Name...</h3>
            <Row>
                 <Col span={8}>
                    <ChartOne/>
                    <p className="chart-sub-heading"><strong className="text-center">FE</strong></p>
                 </Col>
                 <Col span={8}>
                    <ChartTwo/>
                    <p className="chart-sub-heading"><strong className="text-center">DOS</strong></p>
                 </Col>
                 <Col span={8}>
                    <ChartThree/>
                    <p className="chart-sub-heading"><strong className="text-center">A</strong></p>
                 </Col>
            </Row>
               
            </div>
        )
    }
}

export default RightComponnet;