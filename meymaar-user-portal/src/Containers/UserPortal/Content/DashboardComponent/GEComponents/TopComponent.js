import React from 'react';
import Donut from './TopCharts/Donuts';
import {Row, Col} from 'antd';


class TopComponnet extends React.Component{
    render(){
        const data1 = [
            { name: 'ARMY', value: 400 }, { name: 'PAF', value: 300 },
            { name: 'NAVY', value: 300 }, { name: 'DP', value: 200 },
          ];
      
          const data2 = [
            { name: 'ARMY', value: 12 }, { name: 'PAF', value: 4 },
            { name: 'NAVY', value: 5 },
          ];
      
          const data3 = [
            { name: 'ARMY', value: 32 }, { name: 'PAF', value: 12 },
          ];
        return (
            <div className="custom-box">
            <h3>DASHBOARD</h3>
            <Row gutter={12}>
                <Col span={8}><Donut data ={data1}/><h6 style={{textAlign:"center", marginTop:"-10%"}}>DSN</h6></Col>
                <Col span={8}><Donut data ={data2}/><h6 style={{textAlign:"center",  marginTop:"-10%"}}>PE</h6></Col>
                <Col span={8}><Donut data ={data3}/><h6 style={{textAlign:"center",  marginTop:"-10%"}}>CA</h6></Col>
            </Row>
               
            </div>
        )
    }
}

export default TopComponnet;