import React from 'react';
import {Row, Col, Select, Icon} from 'antd';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
class GEDashboardStats extends React.Component {

    getSelectOptions = () => {
        let optionsArray = [
            {
                value: "all",
                title: "All"
            },
            {
                value: "army",
                title: "ARMY"
            },
            {
                value: "paf",
                title: "PAF"
            },
            {
                value: "navy",
                title: "NAVY"
            },
            {
                value: "dp",
                title: "DP"
            },
        ];

        return optionsArray.map((option, index) => {
            return (
                <Option key={index} value={option.value}>{option.title}</Option>
            )
        })

    };
    renderDashboardHeader = () => {

        return (
            <Row style={{marginBottom: "20px"}}>
                <Col span={12}>
                    <h3>Dashboard</h3>
                </Col>
                <Col span={12}>
                    <Select defaultValue="ALL" className="new-add-assign-select float-right" style={{width: 120}}>
                        {this.getSelectOptions()}
                    </Select>
                </Col>
            </Row>
        )
    };

    renderProjectsStats = () => {
        let projectsStats = this.props.projectsStats;
        let rowGutter = 24;
        // {
        //     title: "IN Projects",
        //     subText: "See Expected Projects",
        //     data: 0,
        //     textColor: "#ffa800",
        //     link: null
        // },
        // {
        //     title: "Delayed Projects",
        //     subText: "See All Delayed Projects",
        //     data: 0,
        //     textColor: "#ec4561",
        //     link: null
        // }

        let statsArray = [
            {
                title: "Office Projects",
                subText: "See Office Projects",
                data: projectsStats ? projectsStats.normalCount.myOfficeTotal : "-",
                textColor: "#24dbcb",
                link: null
            },
            {
                title: "IN PLANNING & DESIGN",
                subText: "See Office Planning & Design",
                data: projectsStats ? projectsStats.normalCount.myOfficePlaning : "-",
                textColor: "#4680ff",
                link: projectsStats ? {
                    pathname: "/dashboard/planning-works/myProjects",
                    state: {category: "Planing", type: "work"}
                } : null
            },
            {
                title: "IN Execution",
                subText: "See Office Execution Projects",
                data: projectsStats ? projectsStats.normalCount.myOfficeExecution : "-",
                textColor: "#4680ff",
                link: projectsStats ? {
                    pathname: "/dashboard/execution-works/myProjects",
                    state: {category: "Execution", type: "work"}
                } : null
            },
            {
                title: "Associated Projects",
                subText: "See Associated Projects",
                data: projectsStats ? projectsStats.normalCount.myOfficeAssocTotal : "-",
                textColor: "rgb(35, 219, 209)",
                link: null
            },
            {
                title: "IN PLANNING & DESIGN",
                subText: "See Planning & Design",
                data: projectsStats ? projectsStats.normalCount.myOfficeAssocPlaning : "-",
                textColor: "#ffa800",
                link: {
                    pathname: "/dashboard/planning-associated/associated",
                    state: {category: "Planing", type: "associated"}
                }
            },
            {
                title: "IN Execution",
                subText: "See Execution Projects",
                data: projectsStats ? projectsStats.normalCount.myOfficeAssocExecution : "-",
                textColor: "#ffa800",
                link: {
                    pathname: "/dashboard/execution-associated/associated",
                    state: {category: "Execution", type: "associated"}
                }
            },
        ];

        let statsJSX = statsArray.map((stats, index) => {
            return (
                <Col key={index} span={rowGutter / statsArray.length}>
                    <div className="top-dashboard-stats">
                        <Row className="custom-new-row" gutter={12}>
                            <Col span={24}>
                                <Row>
                                    <div className="dash-main-chart-title">{stats.title}</div>
                                </Row>
                                <Row>
                                    <div className="dash-main-chart-num" style={{color: stats.textColor}}>
                                        {stats.data}
                                    </div>
                                </Row>
                            </Col>
                        </Row>
                        <Row style={{padding: "5%", paddingTop: "15%"}}>
                            <Col className="dash-nav-text" span={22}>{stats.subText}</Col>
                            <Col span={2}>
                                <Icon className="custom-arrow" type="arrow-right"
                                      onClick={() => stats.link ? this.props.history.push(stats.link) : null}/>
                            </Col>
                        </Row>
                    </div>
                </Col>
            )
        });

        return (
            <Row gutter={rowGutter} style={{width: '101%', marginRight: "unset"}}>
                {statsJSX}
            </Row>
        )
    };

    render() {
        return (
            <div className="topDasboard-box">
                {this.renderDashboardHeader()}
                {this.renderProjectsStats()}
            </div>
        )
    }
}


const Option = Select.Option;

const mapStateToProps = state => {
    return {
        projectsStats: state.stats_reducer.projectsStats
    }
};

const connected = connect(mapStateToProps)(GEDashboardStats);
export default withRouter(connected);