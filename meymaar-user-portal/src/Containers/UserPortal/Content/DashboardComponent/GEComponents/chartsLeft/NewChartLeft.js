import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {BarChart, Bar, LabelList, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import {getPlanningAndDesignStats} from "../../../../../../utils/stats-common-utils";


class NewChartLeft extends PureComponent {

    data = [
        {
            dataKey: "Total",
            stackId: 'c',
            fill: "#23dbd1",
        },
        {
            dataKey: "Received",
            stackId: 'a',
            fill: "#4680fe",
        },
        {
            dataKey: "Processed",
            stackId: 'b',
            fill: "#fc6181",
        },
        {
            dataKey: "Balance",
            stackId: 'b',
            fill: "#ffd7dc",
        },
        {
            dataKey: "Forecast",
            stackId: 'a',
            fill: "#9dbbfb",
        }
    ];

    renderBarChart = () => {
        const apiData = getPlanningAndDesignStats();

        return (
            <BarChart className="dashboard-planing-design-chart" width={1400} height={500} data={apiData}
                      margin={{top: 40, right: 30, left: 20, bottom: 5}}>
                <XAxis dataKey="name" dy={13}/>
                <YAxis/>
                <Tooltip className="tooltip-custom-design"/>
                <CartesianGrid stroke="#cccccc" strokeDasharray="0 0" vertical={false}/>
                <Legend verticalAlign="top" align="right" wrapperStyle={{lineHeight: '40px', marginTop: '-30px'}}/>
                {
                    this.data.map(bar => {
                        return (
                            <Bar key={bar.dataKey} {...bar}>
                                <LabelList dataKey={bar.dataKey} className="charts-center-value" position="insideTop"
                                           fill={bar.val === 0 ? '#000' : '#fff'}/>
                            </Bar>
                        )
                    })
                }
            </BarChart>
        )
    };

    render() {

        if (!this.props.projectsStats) return null;

        return (
            <>
                {this.renderBarChart()}
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        phases: state.project_reducer.phases,
        projectsStats: state.stats_reducer.projectsStats,
    }
};

const connected = connect(mapStateToProps)(NewChartLeft);
export default connected;