import React from 'react';
import { Row, Col } from 'antd';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
} from "bizcharts";

class ChartLeft extends React.Component{
     
    render(){
      const data = [
        {
          name: "Name4",
          values: 38
        },
        {
          name: "Name3",
          values: 10
        },
        {
          name: "Name2",
          values: 25
        },
        {
          name: "Name",
          values: 15
        }
      ];
      const cols = {
        values: {
          tickInterval: 20,
          max:100
        }
      };
        return (
          <Chart 
                height={400} 
                padding={[40, 0, 30, 30]}
                grid={false}
                data={data} scale={cols} forceFit>
                <Axis name="name" />
                <Axis name="values" />
                <Tooltip
                  crosshairs={{
                    type: "y"
                  }}
                />
                <Geom type="interval" position="name*values">
                <Label
                      content="values"
                      offset={-15}
                      textStyle={{
                          fontSize: 16
                      }}
                      />
                </Geom>
        </Chart>
        )
    }
}

export default ChartLeft;