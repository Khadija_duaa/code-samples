import React from 'react';
import {Row, Col, Icon} from 'antd';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import FinanceDetails from '../Components/FinanceDetails/FinanceDetails';

class FinanaceEffect extends React.Component {

    // Financial Details
    renderFinanceDetails = () => {

        return (
            <div style={{background: "white", boxShadow: '0px 0px 11px #e0e5e8', border: '0px'}}>
                <FinanceDetails titleClass={"execution-title"} titleStyle={{paddingTop:"3.8%",paddingBottom:"4.5%"}}/>
            </div>
        )
    };


    // All users count
    renderUsersCount = () => {
        return (
            <Col md={24} lg={12} style={{paddingRight: "0px"}}>
                <div className="top-dashboard-stats"
                     style={{boxShadow: '0px 0px 11px #e0e5e8', border: '0px', width: '100%'}}>
                    <Row className="custom-new-row" gutter={12}>
                        <Col span={1}/>
                        <Col span={23}>
                            <Row>
                                <div className="dash-main-chart-title">TOTAL USERS</div>
                            </Row>
                            <Row>
                                <div className="dash-main-chart-num">4,567</div>
                            </Row>
                        </Col>
                    </Row>
                    <Row style={{
                        padding: '9% 5% 5%'
                    }}>
                        <Col className="dash-nav-text" span={22}>See All</Col>
                        <Col span={2}><Icon className="custom-arrow" type="arrow-right"/></Col>
                    </Row>
                </div>
            </Col>
        )
    };


    renderUsersAndContractors = () => {
        let contractorsStats = this.props.contractorsStats;
        let deviceStats = this.props.deviceStats;

        let rowsJSON = [
            {
                divStyle: {boxShadow: '0px 0px 11px #e0e5e8', border: '0px', width: '100%'},
                title: "TOTAL DEVICES",
                numberColor: "rgba(0, 0, 0, 0.65)",
                data: deviceStats ? deviceStats.totalDevices : "-",
                link: deviceStats ? '/dashboard/devices' : null,
                subText: "See All Devices",
            },
            {
                divStyle: {boxShadow: '0px 0px 11px #e0e5e8', border: '0px', width: '94%'},
                title: "TOTAL CONTRACTORS",
                numberColor: "#24dbcb",
                data: contractorsStats ? contractorsStats.contractors : "-",
                link: contractorsStats ? '/dashboard/firms' : null,
                subText: "See All Users",
            }
        ];

        let usersContractorsJSX = rowsJSON.map((row, index) => {
            return (
                <Col key={index} md={24} lg={12} style={{paddingRight: "0px"}}>
                    <div className="top-dashboard-stats"
                         style={row.divStyle}>
                        <Row className="custom-new-row" gutter={12}>
                            <Col span={1}/>
                            <Col span={23}>
                                <Row>
                                    <div className="dash-main-chart-title">{row.title}</div>
                                </Row>
                                <Row>
                                    <div className="dash-main-chart-num" style={{color: row.numberColor}}>
                                        {row.data}
                                    </div>
                                </Row>
                            </Col>
                        </Row>
                        <Row style={{
                            padding: '9% 5% 5%'
                        }}>
                            <Col className="dash-nav-text" span={22}>{row.subText}</Col>
                            <Col span={2}>
                                <Icon className="custom-arrow" type="arrow-right"
                                      onClick={() => row.link ? this.props.history.push(row.link) : null}/>
                            </Col>
                        </Row>
                    </div>
                </Col>
            )
        });

        return (
            <Row gutter={45} type="flex">
                {usersContractorsJSX}
            </Row>
        )
    };

    render() {


        return (
            <div className="finance-and-dasboard-paid">
                {this.renderFinanceDetails()}

                <Row style={{height: "20px"}}/>

                {this.renderUsersAndContractors()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        contractorsStats: state.stats_reducer.contractorStats,
        deviceStats: state.stats_reducer.deviceStats
    }
};

const connected = connect(mapStateToProps)(FinanaceEffect);

export default withRouter(connected);