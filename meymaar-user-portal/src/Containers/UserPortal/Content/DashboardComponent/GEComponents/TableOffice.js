
import React from 'react';
import { Row, Col, Tabs, Table, Modal, Button, Input } from 'antd';
import '../dashboard.css'
import data from '../../../../../store/dashboard/data';
import { SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS } from 'constants';
class TableOffice extends React.Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  componentDidMount() {

  }

  render() {
     const columns = [{
          title: 'Name',
          dataIndex: 'name',
          width: 90,
        }, {
          title: 'Location',
          dataIndex: 'location',
          width: 90,
        }, {
          title: 'Domain',
          dataIndex: 'domain',
          width: 90,
        },
        {
          title: 'Projects',
          dataIndex: 'projects',
          width: 90,
        },
        {
          title: 'User Count',
          dataIndex: 'userCount',
          width: 80,
        }];
        const data = [{
          key: '1',
          name: 'CMES (A) - I',
          location: "Lahore",
          domain: 'work',
          projects:'Project maymar',
          userCount: 5,
        },{
          key: '2',
          name: 'CMES (A) - II',
          location: "Lahore",
          domain: 'works',
          projects:'Projects Maymar 1',
          userCount: 5
        },
        {
          key: '3',
          name: 'CMES (A) - III',
          location: "Lahore",
          domain: 'working',
          projects:'Construction',
          userCount: 5
        }];

    return (<>
      <div>
      <Table  columns={columns} dataSource={data}  size="middle" scroll={{ y: 350 }}/>
      </div></>)

  }
}



export default TableOffice;