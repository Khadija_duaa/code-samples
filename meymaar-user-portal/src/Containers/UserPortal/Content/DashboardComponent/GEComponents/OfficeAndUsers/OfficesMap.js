import React from 'react';
import { Row, Col, } from 'antd';


class OfficesMap extends React.Component{
    render(){

        return (
               <Row>
                    <Col span={14} className="dashboard-map">
                        <h3>Offices</h3>
                        <img src="/images/map.jpg"/>
                    </Col>
                    <Col span={10}>
                        <h3>Users State</h3>
                        <div span={24} class=" detailz-box-full-width col-md-custom"><span>User Online</span><p className="right">4090</p></div>
                        <div span={24} class=" detailz-box-full-width col-md-custom"><span>User Offline</span><p className="right">103</p></div>
                        <div span={24} class=" detailz-box-full-width col-md-custom"><span>User Not Login (Week)</span><p className="right">40</p></div>
                        <div span={24} class=" detailz-box-full-width col-md-custom"><span>User Not Login (Month)</span><p className="right">05</p></div>
                        <div span={24} class=" detailz-box-full-width col-md-custom"><span>Login 4 to 6 Hours</span><p className="right">05</p></div>
                        <div span={24} class=" detailz-box-full-width col-md-custom"><span>Login 4 to 8 Hours</span><p className="right">05</p></div>
                    </Col>
               </Row>
        )
    }
}

export default OfficesMap;