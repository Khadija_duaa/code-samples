import React from 'react';
import { Row, Col } from 'antd'
import { Line } from 'rc-progress';

class ProgressFinance extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      amount: this.props.data.amount,
      total: this.props.data.total,
      color: this.props.data.color,
      percent: "",
      dept:this.props.data.dept
    }
  }

  componentDidMount(){
    this.setState({
      percent: (this.props.data.amount/this.props.data.total)*100
    })
    
  }

  render() {

    return (
      <div>
        <Row>
          <Col span={24}>
            <Row>
              <Col span={8} className="finance-box-title">{this.state.dept}</Col>
              <Col span={12} className="finance-box-text">Contracted Amount</Col>
              <Col span={4} className="finance-box-val" style={{color: this.state.color}}>{this.state.total}M</Col>
            </Row>
            <Row>
              <Line percent={this.state.percent} strokeWidth="4" strokeColor={this.state.color} />
            </Row>
            <Row className="finance-box-result" style={{textAlign:"center", color:this.state.color}}>
              {this.state.amount}m
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default ProgressFinance;