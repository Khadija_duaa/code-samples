import React, { PureComponent } from 'react';
import {
  ResponsiveContainer, PieChart, Pie, Legend, Cell
} from 'recharts';

const COLORS = ['#30bda0','#d3d4d5'];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx, cy, midAngle, innerRadius, outerRadius, percent, index, name, value
}) => {
   const radius = innerRadius + (outerRadius - innerRadius) * 1.2;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);
  let colorS = COLORS[index % COLORS.length];

  return (
    <text x={x} y={y} fill={colorS} style={{fontWeight:"600", fontSize:"90%", color:colorS}} textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${name +":"+ value}`}
    </text>
  );
};

export default class FinanceGuage extends PureComponent {
  constructor(props){
    super(props)
    this.state = {
      data: [
          { name: 'PAID', value: 30 }, { name: 'BALANCE', value: 70 }
     ]
    }
  }
   renderCustomizedLabel =() => {
       let label = this.state.data.map((item) => {
         return (item.name + ":" + item.value)
       })
       return label;
   }


  render() {
    return (
      <div style={{ width: '95%', height: 240, textAlign:"center" }}>
        <ResponsiveContainer>
          <PieChart className="custom-pie">
            <Pie isAnimationActive={false} dataKey="value" data={this.state.data} fill="#8884d8" innerRadius={50} outerRadius={60} label={renderCustomizedLabel} labelLine={false}>
            {
            this.state.data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
            }
            </Pie>
          </PieChart>
        </ResponsiveContainer>
      </div>
    );
  }
}
