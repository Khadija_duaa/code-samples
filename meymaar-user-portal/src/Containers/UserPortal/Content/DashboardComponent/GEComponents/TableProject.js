
import React from 'react';
import { Row, Col, Tabs, Table, Modal, Button, Input } from 'antd';
import '../dashboard.css'
import data from '../../../../../store/dashboard/data';
import { SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS } from 'constants';
class TableProject extends React.Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  componentDidMount() {

  }

  render() {

     const columns = [{
          title: 'Name',
          dataIndex: 'name',
        }, {
          title: 'Age',
          dataIndex: 'age',
        }, {
          title: 'Address',
          dataIndex: 'address',
        }];
        const data = [{
          key: '1',
          name: 'John Brown',
          age: 32,
          address: 'New York No. 1 Lake Park',
        }, {
          key: '2',
          name: 'Jim Green',
          age: 42,
          address: 'London No. 1 Lake Park',
        }, {
          key: '3',
          name: 'Joe Black',
          age: 32,
          address: 'Sidney No. 1 Lake Park',
        }];

    return (<>
      <div>
      <Table columns={columns} dataSource={data}  size="middle" scroll={{ y: 350 }}/>
      </div></>)

  }
}



export default TableProject;