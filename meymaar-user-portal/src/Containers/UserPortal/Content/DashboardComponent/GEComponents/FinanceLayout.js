import React from 'react';
import { Row, Col } from 'antd';
import ProgressFinance from './TopCharts/ProgressFinance';
class FinanceLayout extends React.Component {

     render() {

          let data1 = {
               amount: 23,
               total: 30,
               color: "#30bda0",
               dept: "ARMY"
          }

          let data2 = {
               amount: 21,
               total: 45,
               color: "#4680ff",
               dept: "NAVY"
          }

          let data3 = {
               amount: 15,
               total: 35,
               color: "#04c4dc",
               dept: "PAF"
          }

          let data4 = {
               amount: 25,
               total: 55,
               color: "#fb6180",
               dept: "DP"
          };


          return (
               <div className="finance-layout-dash">
                    <div className="finance-middle">
                         <Row className="finance-stats">
                              <Col span={8}>
                                   <Row className="finance-stats-title">Contracted Value</Row>
                                   <Row className="finance-stats-value">500M</Row>
                              </Col>
                              <Col span={8}>
                                   <Row className="finance-stats-title">Paid</Row>
                                   <Row className="finance-stats-value">175M</Row>
                              </Col>
                              <Col span={8}>
                                   <Row className="finance-stats-title">Balance</Row>
                                   <Row className="finance-stats-value">325M</Row></Col>
                         </Row>
                    </div>
                    <div className="finance-bottom">
                         <Row className="finance-progress-container">
                              <Row gutter={24}>
                                   <Col className="finance-box-dash" span={12}><ProgressFinance data={data1}/></Col>
                                   <Col className="finance-box-dash" span={12}><ProgressFinance data={data2} /></Col>
                              </Row>
                              <Row gutter={24}>
                                   <Col className="finance-box-dash" span={12}><ProgressFinance data={data3}/></Col>
                                   <Col className="finance-box-dash" span={12}><ProgressFinance data={data4}/></Col>
                              </Row>
                         </Row>
                    </div>
               </div>
          )
     }
}

export default FinanceLayout;