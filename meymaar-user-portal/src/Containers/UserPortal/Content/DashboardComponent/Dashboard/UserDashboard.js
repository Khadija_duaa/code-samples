import React from 'react';
// import axios from '../../../../../utils/axios';

class UserDashboard extends React.Component{
    // componentDidMount(){
    //     axios.get('/office-management/offices', )
    //       .then(function (response) {
    //         console.log('response', response);
    //       })
    //       .catch(function (error) {
    //         console.log('error', error);
    //       });
    // }
    render(){
        return(
            <div className="col-12">
            {/* <!-- DO YOUR WORK HERE - START --> */}
            <div className="row">
                <div className="col-md-6">
                    <h2>Dashboard</h2>
                </div>

                <div className="col-md-6 text-right">
                    <select>
                        <option>Last 6 Months</option>
                        <option>Last 9 Months</option>
                        <option>Last 12 Months</option>
                    </select>
                </div>
            </div>
            {/* <!-- DO YOUR WORK HERE - END --> */}
        </div>
        )
    }
}

export default UserDashboard;