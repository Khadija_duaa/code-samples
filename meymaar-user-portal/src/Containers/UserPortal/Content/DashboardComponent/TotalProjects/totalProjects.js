import React from 'react';

class TotalProjects extends React.Component {
    render() {
        return (
            <>
                <div className="col-12">
                    {/* <!-- DO YOUR WORK HERE - START --> */}
                    <div className="row space-after-main-heading">
                        <div className="col-md-3 dashboard-stat-box">
                            <div>
                                <h3>124<span>Total Projects</span></h3>
                            </div>
                        </div>
                        <div className="col-md-3 dashboard-stat-box">
                            <div>
                                <h3>102<span>Planning Phase</span></h3>
                            </div>
                        </div>
                        <div className="col-md-3 dashboard-stat-box">
                            <div>
                                <h3>16<span>Execution Phasse</span></h3>
                            </div>
                        </div>
                        <div className="col-md-3 dashboard-stat-box">
                            <div>
                                <h3>4<span>Under Finalisation</span></h3>
                            </div>
                        </div>
                    </div>
                    {/* <!-- DO YOUR WORK HERE - END --> */}
                </div>
            </>
        )
    }
}

export default TotalProjects;