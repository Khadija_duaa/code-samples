import React from 'react';

class ProjectsOverview extends React.Component {
    render() {
        return (
            <div className={"col-md-12"}>
                <div className="row mt-5 mb-5">
                    <div className="col-md-12">
                        <div className="dashboard-chart-container">
                            <h3>Projects Overview</h3>
                            <div className="row">
                                <div className="col-md-6">
                                    <div id="donutchart" style={{height: '500px'}}/>
                                </div>
                                <div className="col-md-6">
                                    <div id="barchart" style={{height: '500px'}}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProjectsOverview;