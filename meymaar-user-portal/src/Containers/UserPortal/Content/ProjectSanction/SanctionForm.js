import React from 'react';
import {Form, Button, Input,  message} from 'antd';
import {BUTTON_COLOR, DELETE_BUTTON_COLOR, NOTIFICATION_TIME} from "../../../../utils/common-utils";
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {assignSanction, revokeSanction} from "../../../../store/project/project-actions";

import FormElements from "../../../../Components/FormElements/FormElements";
import {uploadFiles} from "../../../../utils/file-utils";
import NewLoader from '../../../Loader/NewLoader';
import ConfirmationModal from '../../../../Components/ModalFactory/MessageModal/ConfirmationModal';
import {handleError} from "../../../../store/store-utils";

message.config({
    top: 70,
    duration: NOTIFICATION_TIME,
    maxCount: 3
});

class SanctionForm extends React.Component {

    state = {
        showConfirmModal: false
    };

    /*************************************** Modal ***************************************/

    handleConfirmClick = () => {
        let fieldValues = this.state.sanctionData;
        let sanctionData = {};
        const values = {...fieldValues};
        const {projectId, phaseId, successCB,sanctionsInfo} = this.props;

        let {canRevokeSanction} = sanctionsInfo;
        let fileData = {};

        // New Sanction Flow
        sanctionData.password = fieldValues.password;

        if(canRevokeSanction){
            this.revokeSanctionData(projectId,phaseId,sanctionData,fieldValues.password,successCB);
        }
        else{
            if (values.upload) {
                fileData.concurrence = values.upload;
            }
            if (values.document) {
                fileData.file = values.document;
            }

            if (Object.keys(fileData).length) {
                uploadFiles(projectId, fileData)
                    .then(data => {
                        sanctionData = {...sanctionData, ...data};
                        this.assignSanctionData(projectId, phaseId, sanctionData, fieldValues.password, successCB);

                    }).catch(err => {
                    let errorMessage = handleError(err);
                    message.error(errorMessage, NOTIFICATION_TIME);
                });
            } else {
                this.assignSanctionData(projectId, phaseId, sanctionData, fieldValues.password, successCB);
            }
        }

    };

    handleCancelClick = () => {
        this.setState({showConfirmModal: false, sanctionData: null})
    };
    toggleConfirmationModal = (data) => {
        this.setState({showConfirmModal: !this.state.showConfirmModal, sanctionData: data})
    };

    renderConfirmationModal = () => {
        let {canRevokeSanction} = this.props.sanctionsInfo;

        let message = `Are you sure, you want to ${canRevokeSanction?"revoke":"assign"} sanction?`;

        return (
            <ConfirmationModal isOpen={this.state.showConfirmModal}
                               okCB={this.handleConfirmClick} cancelCB={this.handleCancelClick}
                               message={message}/>
        );
    };

    /************************************ END *************************************/


    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };
    buttonItemLayout = {
        wrapperCol: {offset: 8},
    };

    setFormErrorsCB = (errorMessage,password)=>{
        this.setState({showConfirmModal: false, sanctionData: null}, () => {
            this.props.form.setFields({
                password: {
                    value: password,
                    errors: [new Error(`${errorMessage}`)]
                }
            })
        });
    };


    /******************** Assign & Revoke Sanction ******************/

    // Assigning Sanction
    assignSanctionData = (projectId, phaseId, sanctionData, password, successCB) => {
        this.props.assignSanction(projectId, phaseId, sanctionData, (message) => {
            this.setState({showConfirmModal: false}, () => {
                successCB && successCB(message);
            });
        }, (errorMessage) => {
            this.setFormErrorsCB(errorMessage,password)
        })
    };


    // Revoking Sanction
    revokeSanctionData = (projectId, phaseId, sanctionData, password, successCB) => {
        this.props.revokeSanction(projectId, phaseId, sanctionData, (message) => {
            this.setState({showConfirmModal: false}, () => {
                successCB && successCB(message);
            });
        }, (errorMessage) => {
            this.setFormErrorsCB(errorMessage,password)
        })
    };


    /******************** End ******************/




    submitSanctionDetails = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, fieldValues) => {

            if (err) {
                return;
            }

            this.toggleConfirmationModal(fieldValues);
        })
    };

    getPasswordField = () => {
        let {getFieldDecorator} = this.props.form;
        return (
            <Form.Item label={"Enter Password"} {...this.formItemLayout}>
                {getFieldDecorator('password', {
                    rules: [{
                        required: true,
                        message: "Please enter your password!",
                        whitespace: true
                    }]
                })(
                    <Input.Password placeholder={"Enter Your Password"}/>)}
            </Form.Item>
        )
    };


    renderUploadButton = () => {

        const uploadJSON = {
            element: 'file',
            name: 'upload',
            title: 'Concurrence File',
            required: true,
            requiredMessage: "Concurrence File is required!",
            elementProps: {
                acceptFiles: ".pdf",
                fileExt: ["pdf"]
            }
        };

        let formElementProps = {
            element: uploadJSON,
            formItemLayout: this.buttonItemLayout,
            editRights: this.props.editRights,
            ...this.props
        };

        return <FormElements {...formElementProps}/>

    };



    renderDocumentUploadButton = () => {
        const {sanctionsInfo} = this.props;
        const uploadJSON = {
            element: 'file',
            label: "Sanction Document",
            name: 'document',
            title: 'Upload',
            required: sanctionsInfo.sanctionFileRequired,
            requiredMessage: "Document is required",
            elementProps: {
                acceptFiles: ".pdf",
                fileExt: ["pdf"]
            }
        };

        let formElementProps = {
            element: uploadJSON,
            formItemLayout: this.formItemLayout,
            editRights: this.props.editRights,
            ...this.props
        };

        return <FormElements {...formElementProps}/>
    };

    renderButtons = (canRevoke) => {
        let buttonText = canRevoke ? "Revoke Sanction" : "Give Sanction";

        let buttonStyle = {
            height: "50px",
            width: "45%",
            color: "#fff"
        };

        if(!canRevoke){
            buttonStyle.backgroundColor = BUTTON_COLOR;
        }

        return (
            <Form.Item {...this.buttonItemLayout}>
                <Button htmlType="submit" className={`${canRevoke?"danger-button":""}`} style={buttonStyle}>{buttonText}</Button>
                <Button style={{height: "50px", width: "45%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.cancelCB}>Cancel</Button>
            </Form.Item>
        )
    };

    render() {

        let {sanctionsInfo} = this.props;
        let {canRevokeSanction,concurrenceFileRequired} = sanctionsInfo;
        return (
            <div style={{paddingLeft: "10px", paddingRight: "10px"}}>
                {this.renderConfirmationModal()}

                <Form layout={"horizontal"} onSubmit={this.submitSanctionDetails}>
                    {this.getPasswordField()}

                    {!canRevokeSanction ? this.renderDocumentUploadButton() : null}


                    {/*{*/}
                    {/*sanctions.concurrenceFileRequired ? this.renderConcurrenceFields() : null*/}
                    {/*}*/}


                    {!canRevokeSanction && concurrenceFileRequired ? this.renderUploadButton() : null}

                    {this.props.processing ?
                        <NewLoader/> : null}

                    {this.renderButtons(canRevokeSanction)}
                </Form>
            </div>
        )
    }

}


const mapDispatchToProps = dispatch => {
    return {
        assignSanction: (projectId, phaseId, data, cb, errorCB) => dispatch(assignSanction(projectId, phaseId, data, cb, errorCB)),
        revokeSanction: (projectId, phaseId, data, cb, errorCB) => dispatch(revokeSanction(projectId, phaseId, data, cb, errorCB))
    }
};

const mapStateToProps = state => {
    return {
        processing: state.project_reducer.processing
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(SanctionForm);


const router = withRouter(connected);
export default Form.create()(router);