import React from 'react';
import {Button, Checkbox, Form, Input, Select} from 'antd';
import 'antd/dist/antd.css';
import {connect} from 'react-redux';
import {BUTTON_COLOR, formatName, hasAuthority} from "../../../../../utils/common-utils";
import {withRouter} from 'react-router-dom';
import {markProject} from "../../../../../store/project/project-actions";
import NewLoader from '../../../../Loader/NewLoader';


const Option = Select.Option;

class MarkingForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            departmentOptions: null,
            departmentId: null,
            headOffices: null,
            officeRoles: null,
            roleUsers: null,
            reasons: null,
            role_title: null,
            roleName: null,
            officeName: null,
            userName: null,
            error: null
        }
    }


    // Render roles with the help of Roles saved in state.
    handleRoleChange = (roleId, event) => {
        this.setState({roleId, roleName: event.props.children}, () => {
            this.props.form.resetFields('user_id');
        });
    };
    renderRoles = () => {
        let {internalMarkingInfo} = this.props.markingInfo;
        let officeId = this.props.form.getFieldValue('office_id');
        let roles = [];
        if (!this.props.form.getFieldValue('office_id')) return [];
        if (officeId) {
            internalMarkingInfo
                .filter(office => office.uuid === officeId)
                .forEach(office => {
                    office.data && office.data.forEach(role => roles.push(<Option key={role.uuid}
                                                                                  value={role.uuid}>{role.name}</Option>))
                });
            return roles;
        } else {
            return [];
        }

    };

    // Render Role users
    handleUserChange = (userId, event) => {
        this.setState({userName: event.props.children});
    };

    renderUsers = () => {
        let {roleId} = this.state;
        let officeId = this.props.form.getFieldValue('office_id');
        let {activeUser, isInboxProject} = this.props;
        let users = [];

        if (!this.props.form.getFieldValue('role_id')) return null;
        if (roleId) {
            this.props.markingInfo.internalMarkingInfo
                .filter(office => office.uuid === officeId)
                .forEach(office => office.data
                    .filter(role => role.uuid === roleId)
                    .forEach(rank => {
                        rank.users && rank.users.forEach(user => (activeUser.principal_id !== user.uuid || isInboxProject) && users.push(
                            <Option key={user.uuid}
                                    value={user.uuid}>{formatName(user)}</Option>));
                    }));
            return users;
        }
        else return [];
    };

    /******************** Roles & Users Fields  *****************/
    getInternalMarkingFields = () => {
        let {getFieldDecorator} = this.props.form;

        if (this.props.form.getFieldValue('is_external')) return null;

        return (
            [
                <Form.Item key={0} label={"Role"}>

                    {
                        getFieldDecorator(`role_id`, {
                            rules: [{required: true, message: 'Role is required'}],
                        })(
                            <Select size={"default"} id={"role"} className={"role"}
                                    onChange={this.handleRoleChange}
                                    placeholder={"Select Role "}>
                                {this.renderRoles()}
                            </Select>
                        )
                    }
                </Form.Item>
                ,
                <Form.Item key={1} label={"Name"}>
                    {
                        getFieldDecorator(`user_id`, {
                            rules: [{required: true, message: 'Person name is required'}],
                        })(
                            <Select size={"default"} id={"personName"} className={"personName"}
                                    placeholder={"Select Person "} onChange={this.handleUserChange}>
                                {this.renderUsers()}
                            </Select>
                        )
                    }
                </Form.Item>
            ]
        )
    };

    /******************** End  *****************/


    /******************** Office Fields *****************/

    handleOfficeChange = (officeId, event) => {
        this.setState({officeId, officeName: event.props.children}, () => {
            if (!this.props.form.getFieldValue('is_external')) {
                this.props.form.resetFields(['role_id', 'user_id'])
            }
        })
    };

    getOfficeFields = () => {
        let {getFieldDecorator, getFieldValue} = this.props.form;
        const {internalMarkingInfo, externalMarkingInfo} = this.props.markingInfo;


        let offices = internalMarkingInfo;

        if (getFieldValue('is_external')) {
            offices = [];

            if (getFieldValue('department_id')) {

                let department = externalMarkingInfo.filter(dept => dept.uuid === getFieldValue('department_id'));

                if (department && department.length && department[0] && department[0].data) {
                    department[0].data = department[0].data.filter(office => office !== null);
                    offices = department[0].data;
                }
            }
        }

        return (
            <Form.Item label={"Office"}>
                {
                    getFieldDecorator(`office_id`, {
                        initialValue: !getFieldValue('is_external') ? offices && offices.length && offices[0].uuid : null,
                        rules: [{required: true, message: 'Office is required'}]
                    })(
                        <Select size={"default"} id={"office"} className={"office"}
                                placeholder={"Select Office "} onChange={this.handleOfficeChange}>
                            {
                                offices.map(office => {
                                    return <Option key={office.uuid} value={office.uuid}>{office.name}</Option>
                                })
                            }
                        </Select>
                    )
                }
            </Form.Item>

        )
    };


    /******************** Department Fields  *****************/

    handleDepartmentChange = () => {
        this.props.form.resetFields('office_id');
    };
    renderDepartments = () => {
        let {getFieldDecorator} = this.props.form;
        const {externalMarkingInfo} = this.props.markingInfo;

        return (
            <Form.Item label={"Department"}>
                {
                    getFieldDecorator(`department_id`, {
                        initialValue: externalMarkingInfo && externalMarkingInfo.length ? externalMarkingInfo[0].uuid : null,
                        rules: [{required: true, message: 'Department is required'}]
                    })(
                        <Select size={"default"} id={"department"} className={"department"}
                                placeholder={"Select Department "} onChange={this.handleDepartmentChange}>
                            {
                                externalMarkingInfo.map(dept => {
                                    return <Option key={dept.uuid} value={dept.uuid}>{dept.name}</Option>
                                })
                            }
                        </Select>
                    )
                }
            </Form.Item>

        )
    };

    /******************** END  *****************/


    /******************** Reason Fields  *****************/

    getReasonElement = () => {
        let {getFieldDecorator} = this.props.form;
        const isExternal = this.props.form.getFieldValue('is_external');

        const reasonOptions = [];
        let {phaseDetails, isInboxProject} = this.props;

        if (isInboxProject) {
            reasonOptions.push(
                {
                    name: "Initial Reciept",
                    value: "Initial Reciept"
                }
            )
        } else {

            if (isExternal) {
                reasonOptions.push(
                    {
                        name: "Request for review and further process",
                        value: "Review Request"
                    }
                );
            }

            if (!isExternal) {
                reasonOptions.push(
                    {
                        name: "Marking to user",
                        value: "Marking to user"
                    }
                );
            }

            if (phaseDetails && phaseDetails.phaseEditable) {
                reasonOptions.push(
                    {
                        name: "Objection",
                        value: "Objection"
                    }
                );
            }

        }


        reasonOptions.push(
            {
                name: "Other",
                value: ""
            }
        );

        return (
            <Form.Item label={"Reason"}>
                {
                    getFieldDecorator(`preReason`, {initialValue: reasonOptions[0].value})(
                        <Select placeholder={"Select Reason"} size={"default"} id={"preReason"}
                                className={"preReason"}>
                            {reasonOptions.map(option => {
                                return (
                                    <Option key={option.name} value={option.value}>{option.name}</Option>
                                )
                            })}
                        </Select>
                    )
                }
            </Form.Item>
        )

    };

    getReasonOtherFields = () => {
        if (this.props.markType === 'override') return null;
        let {getFieldDecorator} = this.props.form;
        if (!this.props.form.getFieldValue('preReason')) {
            return (
                <Form.Item {...this.formItemLayout} label={""}>
                    {getFieldDecorator('reason', {
                        rules: [{
                            required: true,
                            message: 'Reason is required',
                            whitespace: true
                        }]
                    })(<Input
                        placeholder={"Reason"}/>)}
                </Form.Item>
            )
        }
    };

    /******************** END  *****************/


    /******************** Checkbox Field  *****************/

    handleCheckedChange = () => {
        this.props.form.resetFields(['department_id', 'office_id']);
    };

    getCheckbox = () => {
        let canMarkExternal = hasAuthority('CAN_MARK_EXTERNAL');
        let {externalMarkingInfo} = this.props.markingInfo;
        let {getFieldDecorator} = this.props.form;
        if (!canMarkExternal || !externalMarkingInfo) return null;


        return (
            <Form.Item>
                {
                    getFieldDecorator(`is_external`, {
                        valuePropName: 'checked'
                    })(
                        <Checkbox size={"default"} id={"isExternal"} className={"office"}
                                  onChange={this.handleCheckedChange}>
                            External Marking ?
                        </Checkbox>
                    )
                }
            </Form.Item>
        )
    };
    /********************* End ****************************/


    /*************** Form Submission *************/
    handleMarkingSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, fieldValues) => {

            if (err) {
                return;
            }

            let {markType, projectId, phaseId} = this.props;

            let reason = '';

            // Setting Fix Reason if Mark Type is Overrding
            if (markType === 'override') {
                reason = 'Marking Overriden';
            } else {
                reason = fieldValues.preReason || fieldValues.reason;
            }


            // Setting Role title if mark type is not external
            if (!fieldValues['is_external']) {
                fieldValues.role_title = this.state.role_title;
                delete fieldValues['is_external'];
            }


            fieldValues.is_review_request = Boolean(fieldValues['is_external'] && fieldValues.preReason && fieldValues.preReason === 'Review Request');
            fieldValues.reason = reason;
            fieldValues.phase_id = phaseId;

            // Deleting Extra Fields that are not necessary
            delete fieldValues['preReason'];

            if (fieldValues.role_id) {
                fieldValues.role_title = this.state.roleName;
            }

            fieldValues.project_name = this.props.name;
            this.props.markProject(projectId, phaseId, fieldValues,
                (markingMessage) => {
                    this.props.successCB && this.props.successCB(markingMessage);
                    this.props.form.resetFields();

                },
                (errorMessage) => {
                    this.setState({error: errorMessage})
                }
            );
        })
    };
    /*************** End *************/


    /*************** Form Buttons *************/
    renderButtons = () => {

        return (
            <Form.Item>
                <Button htmlType="submit" style={{
                    height: "50px",
                    width: "45%",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>Mark</Button>
                <Button style={{height: "50px", width: "45%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.cancelCB}>Cancel</Button>
            </Form.Item>
        )
    };
    /*************** End *************/

    renderForm = () => {
        const {getFieldDecorator, getFieldValue} = this.props.form;
        const {externalMarkingInfo} = this.props.markingInfo;

        return (
            <div className={"col-md-12"} style={{padding: "unset"}}>
                <Form layout={"horizontal"} onSubmit={this.handleMarkingSubmit}>

                    {this.getCheckbox()}

                    {externalMarkingInfo && getFieldValue('is_external') ? this.renderDepartments() : null}

                    {this.getOfficeFields()}

                    {this.getInternalMarkingFields()}

                    {this.getReasonElement()}

                    {this.getReasonOtherFields()}

                    <Form.Item label={"Comment"}>
                        {
                            getFieldDecorator(`public_comment`, {
                                rules: [{
                                    required: true,
                                    message: "Comment is required",
                                    whitespace: true
                                }]
                            })(
                                <Input placeholder={"Comment"}/>
                            )
                        }
                    </Form.Item>


                    {
                        !this.state.isPrivate ?
                            <Form.Item>
                                <u onClick={() => this.setState({isPrivate: true})}
                                   style={{cursor: "pointer", color: "#3B86FF", fontWeight: "bold"}}>
                                    + Add a private Message</u>
                            </Form.Item>
                            :
                            <Form.Item label={"Private Message"}>
                                {
                                    getFieldDecorator(`private_comment`)(
                                        <Input placeholder={" Private Message"}/>
                                    )
                                }
                            </Form.Item>

                    }

                    {
                        this.props.processing ?
                            <NewLoader/>
                            :
                            null
                    }
                    {this.renderButtons()}
                </Form>
            </div>
        )
    };

    render() {
        let {headerText} = this.props;

        return (
            <div style={{paddingLeft: "10px", paddingRight: "10px"}}>
                <p className="text-center">
                    {headerText}
                </p>

                {this.state.error ?
                    <p className="text-center" style={{color: "#f5222d"}}>
                        {this.state.error}
                    </p>
                    : null
                }
                {this.renderForm()}
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        markProject: (projectId, phaseId, data, cb, errorCB) => dispatch(markProject(projectId, phaseId, data, cb, errorCB))
    }
};

const mapStateToProps = state => {
    return {
        processing: state.project_reducer.processing,
        activeUser: state.user_reducer.activeUser
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(MarkingForm);
const router = withRouter(connected);
export default Form.create()(router);