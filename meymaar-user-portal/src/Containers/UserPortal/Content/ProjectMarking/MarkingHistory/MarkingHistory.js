import React from 'react';
import {fetchProjectDetails} from "../../../../../store/project/project-actions";
import {connect} from 'react-redux';
import {formatDate, initialCapital} from "../../../../../utils/common-utils";
import BackButton from "../../../../../Components/Buttons/BackButton";
import {withRouter} from 'react-router-dom';
import NewLoader from "../../../../Loader/NewLoader";


class MarkingHistory extends React.Component {

    state = {
        markingHistory: null
    };

    getLocationState = () => {
        const storageKey = "marking-State";
        let locationState = sessionStorage.getItem(storageKey);

        if (locationState) {
            locationState = JSON.parse(locationState);
        }

        const {location} = this.props;

        if (location && location.state) {
            locationState = location.state;
        }

        sessionStorage.setItem(storageKey, JSON.stringify(locationState));

        return locationState;
    };


    componentDidMount() {
        let locationState = this.getLocationState();


        if (!locationState.markingHistory) {
            let projectFields = {
                fields: ["markingHistory", "currentProjectOfficeData"]
            };

            this.props.getProjectDetails(locationState.projectId, projectFields, (details) => {
                this.setState({
                    markingHistory: details.markingHistory,
                    officeName: details.currentProjectOfficeData.name
                });
            });

        } else {
            this.setState({
                markingHistory: locationState.markingHistory,
                officeName: locationState.currentProjectOfficeData.name
            });
        }
    }

    renderHeader = () => {
        let locationState = this.getLocationState();
        let {returnCB} = locationState;
        return (
            <div style={{width: "100%", display: "inline-block", marginBottom: "30px"}}>
                <div style={{width: "70%", float: "left"}}>
                    <h2 className={"heading-worklist"}>Marking History / Minutes Sheet</h2>
                </div>
                <div style={{width: "30%", float: "right", textAlign: "right"}}>
                    <BackButton returnCB={() => this.props.history.push(returnCB)}
                                style={{marginTop: "20px", width: "100px", height: "40px"}}/>
                </div>
            </div>
        )
    };


    renderMarkingHistory = () => this.state.markingHistory.map((history, index) => {
        let {markingHistory} = this.state;

        let intitialOffice = history.initial_office_name ? history.initial_office_name : "";

        let markedToOffice = history.office_name ? history.office_name : "";
        let markedToRole = history.role_title ? history.role_title : "";

        let markedByOffice = history.marked_from_office_name ? history.marked_from_office_name : "";
        let markedByRole = history.marked_by_role_title ? history.marked_by_role_title : "";

        let durationText = `${history.start_time ? formatDate(history.start_time, "DD-MM-YYYY") : "Present"} - ${history.end_time ? formatDate(history.end_time, "DD-MM-YYYY") : "Present"}`;
        return (

            <div className={`minutes-sheet ${!history.role_title ? 'office-minutes' : ''}`}
                 style={index === markingHistory.length - 1 ? {border: "unset", marginLeft: "18px"} : null} key={index}>
                <span className={`dot-sh ${!history.role_title ? 'office-marked' : ''}`}/>

                <h3>{initialCapital(history.reason)}</h3>


                {index < markingHistory.length - 1 ?
                    <p className="minute-info" style={{lineHeight: "30px",fontSize:"15px"}}>

                        <strong>Marked to:</strong> {markedToRole && markedToRole} {markedToRole && <strong> of </strong>} {markedToOffice}
                        <br/>

                        <strong>Marked by:</strong> {markedByRole} <strong> of </strong> {markedByOffice}
                        <br/>
                        <strong>Date:</strong> {formatDate(history.date, "DD-MM-YYYY")}
                        <br/>
                        <strong>Duration:</strong> {durationText}
                    </p>

                    :

                    <p className="minute-info" style={{lineHeight: "30px"}}>

                        <strong>Project Name:</strong> {this.getLocationState().projectName}
                        <br/>

                        <strong>Initiated by:</strong> {history.initiated_by_role_title}
                        <strong> of </strong> {intitialOffice}
                        <br/>
                        <strong>Date Initiated:</strong> {formatDate(history.initiated_date, "DD-MM-YYYY")}
                        <br/>
                        <strong>Duration:</strong> {durationText}
                    </p>
                }

                <p className="minute-comment">
                    {history.public_comment}
                    <i className="fa fa-quote-left" aria-hidden="true"/>
                </p>
            </div>
        )
    });


    render() {

        if (this.props.processing) return <NewLoader/>;
        if (!this.state.markingHistory) return null;


        return (
            <div className="col-12">
                <div className="container-fluid">
                    <div className="row minutes-sheet-container">
                        <div className="col-md-12" style={{paddingRight: "unset"}}>
                            {this.renderHeader()}
                            {this.renderMarkingHistory()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getProjectDetails: (projectId, fields, cb) => dispatch(fetchProjectDetails(projectId, fields, true, cb))
    }
};

const mapStateToProps = (state) => {
    return {
        projectDetails: state.project_reducer.projectDetails,
        processing: state.project_reducer.processing
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(MarkingHistory);

export default withRouter(connected);