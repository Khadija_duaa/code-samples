import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {
    deleteAllNotifications,
    deleteNotification,
    fetchAllUserNotifications, resetRedux
} from "../../../../store/server/server-actions";
import {formatEpochTime, getObjectValue} from "../../../../utils/common-utils";
import NewLoader from "../../../Loader/NewLoader";
import {Icon, Popconfirm, Pagination} from 'antd';
import 'antd/dist/antd.css';
import {handleNotificationClick} from "../../../../utils/server-utils";
import ConfirmationModal from '../../../../Components/ModalFactory/MessageModal/ConfirmationModal';

let searchFields = ['title', 'time'];

class Notifications extends React.Component {

    state = {
        notifications: null,
        pageNumber: 1
    };

    componentDidMount() {

        if(!this.props.userNotifications){
            this.props.history.push('/dashboard')
        }else{
            this.getUserNotifications();
        }

    }

    getUserNotifications = () => {
        let {activeUser} = this.props;
        let {pageNumber} = this.state;
        this.props.fetchNotifications(activeUser.principal_id, pageNumber);
    };


    /******************************** EVENTS ***************************/

    searchNotification = (searchStr, searchParams = []) => {
        if (!this.props.allNotifications) return null;

        const notifications = [...this.props.allNotifications];
        if (searchStr) {
            let filteredNotifcations = [];
            notifications.forEach(notify => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];
                    const _val = getObjectValue(notify, param);

                    let isNumber = typeof (_val) === 'number';
                    let isString = typeof (_val) === 'string';

                    let isNumberIndex = _val && isNumber && _val.toString().toLowerCase().indexOf(searchStr.toLowerCase()) > -1;
                    let isStringIndex = _val && isString && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1;

                    if (isNumberIndex || isStringIndex) {
                        filteredNotifcations.push(notify);
                        break;
                    }
                }
            });
            this.setState({notifications: filteredNotifcations});
        } else {
            this.setState({notifications: null});
        }
    };

    onPageChange = (page) => {
        this.setState({pageNumber: page}, () => {
            this.getUserNotifications();
        })
    };

    handleAllDeleteClick = () => {
        let {activeUser} = this.props;

        this.props.deleteAllNotifications(activeUser.principal_id, () => {
            this.props.resetRedux('userNotifcations');
            this.props.resetRedux('allNotifications');
            this.props.resetRedux('notificationPagination');
            sessionStorage.removeItem('userNotification');
            this.props.history.push('/dashboard');
            window.location.reload();
        })
    };

    /****************************** END *******************************/



    /******************************** MODALS ***************************/


    toggleConfirmationModal = () => {
        this.setState({showConfirmationModal: !this.state.showConfirmationModal});
    };

    handleConfirmClick = () => {
        this.setState({showConfirmationModal:false},()=>{
            this.handleAllDeleteClick();
        })
    };

    renderDeleteNotificationModal = () => {
        let message = 'Are you sure, you want to delete all notifications?';

        return (
            <ConfirmationModal isOpen={this.state.showConfirmationModal} toggle={this.toggleConfirmationModal}
                               okCB={this.handleConfirmClick} cancelCB={this.toggleConfirmationModal}
                               message={message}/>
        );
    };


    /******************************** END ***************************/

        // Heading
    renderHeading = () => {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <h3>Notifications</h3>
                    </div>
                </div>
            </div>
        )
    };

    // Action Buttons
    renderActionsButtons = () => {
        let {allNotifications} = this.props;


        return (
            <div className="container-fluid">
                <div className="row button-list align-items-baseline">
                    <div className="col-md-4">
                        {
                            allNotifications && allNotifications.length ?
                                <button onClick={this.toggleConfirmationModal}
                                        className="custom-button custom-button-light" style={{color: "red"}}>
                                    Delete All Notifications
                                </button> : null
                        }
                    </div>


                    <div className="col-md-8 text-right margin-top-56">
                        {/* <!--Search-button--> */}
                        <div className="new-btn-with-gradient-select search-bar">
                            <form className="search-container">
                                <input type="text" id="search-bar" placeholder="Search…"
                                       onChange={(e) => this.searchNotification(e.target.value, searchFields)}/>
                                <a href={"javascript:;"}><i className="fas fa-search"/></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    };

    // Data Section
    handleDeleteNotification = (notification) => {
        this.props.deleteNotification(notification._id, () => {
            this.getUserNotifications();
        });
    };

    notificationClick = (notify) => {
        handleNotificationClick(notify, (returnPath) => {
            this.props.history.push(returnPath);
        });
    };
    getNotificationBody = () => {
        if (!this.props.allNotifications) return null;

        let notificationList = this.state.notifications ? this.state.notifications : this.props.allNotifications;

        let deleteStyle = {cursor: "pointer", color: "red", textDecoration: "underline"};

        // Table Body
        return notificationList.map((notify, index) => {
            return (
                <tr key={index} style={{cursor: "pointer"}}>
                    <td onClick={() => this.notificationClick(notify)}>{
                        notify.read ?
                            <Icon className={"notify-icon"} style={{marginRight: "20px"}} type={"notification"}/>
                            :
                            <Icon className={"notify-icon"} style={{marginRight: "20px"}} type="notification"
                                  theme="twoTone"/>
                    }{notify.title}</td>

                    <td onClick={() => handleNotificationClick(notify)}>{formatEpochTime(notify.time)}</td>

                    <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDeleteNotification(notify)}>
                        <td colSpan={2} style={deleteStyle} onClick={() => this.handleDelete}>Delete</td>
                    </Popconfirm>
                </tr>
            )
        });
    };


    renderNotifications = () => {
        let {processing, projectProcessing} = this.props;
        if (processing || projectProcessing) {
            return <NewLoader/>
        }

        //Table Headers
        let tableHeaders = ["Notification Title", "Time", "Delete"];
        let headerJSX = tableHeaders.map((head, index) => {
            return (
                <th key={index} scope="col">
                    <span>{head}</span>
                </th>
            )
        });


        let notificationList = this.state.notifications;
        let {notificationPagination} = this.props;

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className=" custom-table table-responsive">
                            <table className="border-bottom custom-padding-for-start-column table border-0 ">
                                <thead className="thead-dark">
                                <tr>
                                    {headerJSX}
                                </tr>
                                </thead>

                                <tbody>
                                {this.getNotificationBody()}
                                </tbody>
                            </table>

                            {notificationPagination ?
                                <div style={{margin: "50px 0px", textAlign: "right"}}>
                                    <Pagination showTotal={total => `Total ${total} items`}
                                                current={this.state.pageNumber}
                                                pageSize={notificationPagination.pageSize}
                                                total={notificationList ? notificationList.length : notificationPagination.totalEntries}
                                                onChange={this.onPageChange}/>
                                </div>
                                : null}
                        </div>
                    </div>
                </div>
            </div>
        )
    };

    render() {
        return (
            <>
                {this.renderDeleteNotificationModal()}
                {this.renderHeading()}
                {this.renderActionsButtons()}
                {this.renderNotifications()}
            </>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchNotifications: (userId, page) => dispatch(fetchAllUserNotifications(userId, page)),
        deleteNotification: (id, cb) => dispatch(deleteNotification(id, cb)),
        deleteAllNotifications: (userId, cb) => dispatch(deleteAllNotifications(userId, cb)),
        resetRedux:(key)=>dispatch(resetRedux(key))
    }
};


const mapStateToProps = state => {
    return {
        userNotifications :  state.server_reducer.userNotifications,
        allNotifications: state.server_reducer.allNotifications,
        activeUser: state.user_reducer.activeUser,
        processing: state.server_reducer.processing,
        projectProcessing: state.project_reducer.processing,
        notificationPagination: state.server_reducer.notificationPagination
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Notifications);
export default withRouter(connected);