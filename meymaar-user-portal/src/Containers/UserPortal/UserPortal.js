import React from 'react';
import { Icon } from 'antd';
import UserRoutes from './UserRoutes/UserRoutes';

class UserPortal extends React.Component {

    render() {
        return (
            <div>
                <main>
                    <UserRoutes/>
                </main>
            </div>
        )
    }
}


export default UserPortal