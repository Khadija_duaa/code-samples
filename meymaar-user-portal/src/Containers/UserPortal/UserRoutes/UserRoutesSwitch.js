import React, { Suspense, lazy } from 'react';
import {connect} from 'react-redux';
import {Route, Switch, withRouter} from 'react-router-dom';

import NewLoader from '../../../Containers/Loader/NewLoader';

import {hasAuthority} from "../../../utils/common-utils";


const CreateWork = lazy(()=>import("../Content/Works/CreateWork/CreateWork"));
const TaskBoard = lazy(()=>import("../Content/TaskBoard/TaskBoard"));
const BqCategories = lazy(()=>import("../Content/RevitManagement/bqCategories"));
const Domains = lazy(()=>import("../../AdminPortal/Content/Domains/Domains"));
const Users = lazy(()=>import("../../AdminPortal/Content/Users/Users"));

const Planning = lazy(()=>import("../Content/Works/Planing/Planning"));
const PlanningProjects = lazy(()=>import("../Content/Works/ProjectWorks/ProjectWorks"));
const Settings = lazy(()=>import("../Content/RevitManagement/MeymaarRevitSettings/settings"));
const ManageRanks = lazy(()=>import("../../AdminPortal/Content/Offices/ManageRanks/ManageRanks"));

const ExecutionProjects = lazy(()=>import("../Content/Works/ProjectExecution/ProjectExecution"));
const Notifications = lazy(()=>import("../Content/Notifications/Notifications"));
const PlaningOverview = lazy(()=>import("../Content/Works/Planing/planingOverview"));
const ContractingPower = lazy(()=>import("../../AdminPortal/Content/ContractingPower/ContractingPower"));

const officeAuthority = lazy(()=>import("../../AdminPortal/Content/OfficeAuthority/OfficeAuthority"));
const PlaningStart = lazy(()=>import("../Content/Works/Planing/planingStart"));
const MarkingHistory = lazy(()=>import("../Content/ProjectMarking/MarkingHistory/MarkingHistory"));
const Diary = lazy(()=>import("../Content/Works/Diary/diary"));

const RevitSetting = lazy(()=>import("../Content/RevitManagement/RevitSettings/revitSettings"));
const SaveSettings = lazy(()=>import("../Content/RevitManagement/MeymaarRevitSettings/saveSettings"));
const User = lazy(()=>import("../Content/DashboardComponent/User"));
const Devices = lazy(()=>import("../Content/Devices/Devices"));

const CreateOffice = lazy(()=>import("../../AdminPortal/Content/Offices/CreateOffice/CreateOffice"));
const Overview = lazy(()=>import("../Content/Works/Overview/overview"));
const Departments = lazy(()=>import("../../AdminPortal/Content/Departments/Departments"));
const Stations = lazy(()=>import("../../AdminPortal/Content/Stations/Stations"));

const CreateUser = lazy(()=>import("../../AdminPortal/Content/Users/CreateUser/CreateUser"));
const FirmsDetails = lazy(()=>import("../Content/Firms/FirmsDetails"));
const Offices = lazy(()=>import("../../AdminPortal/Content/Offices/Offices"));
const Firms = lazy(()=>import("../Content/Firms/Firms"));

const RevitSaveSetting = lazy(()=>import("../Content/RevitManagement/RevitSettings/revitSaveSetting"));
const OfficeDetails = lazy(()=>import("../../AdminPortal/Content/Offices/OfficeDetails/OfficeDetails"));
const FieldParam = lazy(()=>import("../Content/RevitManagement/fieldParameters"));
const UserProfile = lazy(()=>import("../Content/UserProfile/UserProfile"));
const Configurations = lazy(()=>import("../../AdminPortal/Content/SystemConfigurations/Configurations"));




class UserRoutesSwitch extends React.Component {

    render() {
        let activeUser = {...this.props.activeUser};
        let {firstTime} = activeUser.principal;

        let isSuperAdmin = hasAuthority('SUPER_ADMIN');

        let canManageOffices = hasAuthority('CAN_MANAGE_OFFICES') && !firstTime;
        let canManageUsers = hasAuthority('CAN_MANAGE_USERS') && !firstTime;

        let canManageRevit = hasAuthority('CAN_MANAGE_REVIT') && !firstTime;
        let canManageWorks = hasAuthority('CAN_MANAGE_WORKS') && !firstTime;

        return (
            <Suspense fallback={<NewLoader />}>
                <Switch>

                    {
                        (hasAuthority('CAN_MANAGE_GEO_LOC') && !firstTime) || isSuperAdmin ?
                            <Route path={"/dashboard/stations"} name={"Stations"} component={withRouter(Stations)}/> : null
                    }

                    {
                        (!firstTime && hasAuthority('CAN_MANAGE_DEPARTMENT')) || isSuperAdmin ?
                            <Route path={"/dashboard/departments"} name={"Departments"} component={withRouter(Departments)}/> : null
                    }

                    {
                        (!firstTime && hasAuthority('CAN_MANAGE_DOMAIN')) || isSuperAdmin ?
                            <Route path={"/dashboard/domains"} name={"Domains"} component={withRouter(Domains)}/> : null
                    }

                    {
                        (!firstTime && hasAuthority('CAN_MANAGE_CONTRACTUAL_POWERS')) || isSuperAdmin ?
                            <Route path={"/dashboard/contracting-powers"} name={"Contracting Power"}
                                   component={withRouter(ContractingPower)}/> : null
                    }

                    {
                        (!firstTime && hasAuthority('CAN_MANAGE_OFFICE_AUTHS')) || isSuperAdmin ?
                            <Route path={"/dashboard/authorities"} name={"Authorities"} component={withRouter(officeAuthority)}/>
                            : null
                    }

                    {
                        (!firstTime && hasAuthority('CAN_MANAGE_CONFIGURATION')) || isSuperAdmin ?
                            <Route path={"/dashboard/configurations"} name={"Configurations"}
                                   component={withRouter(Configurations)}/> : null
                    }

                    {
                        canManageOffices || isSuperAdmin ?
                            [
                                <Route key={0} path={"/dashboard/offices/users"} name={"OfficeUsers"}
                                       component={withRouter(OfficeDetails)}/>,
                                <Route key={1} path={"/dashboard/offices/create"} name={"CreateOffice"}
                                       component={withRouter(CreateOffice)}/>,


                                <Route key={2} path={"/dashboard/offices/ranks"} name={"OfficeRanks"}
                                       component={withRouter(ManageRanks)}/>,
                                <Route key={3} path={"/dashboard/offices"} name={"Offices"} component={withRouter(Offices)}/>

                            ]
                            : null
                    }

                    {
                        canManageUsers || isSuperAdmin ?
                            [
                                <Route key={4} path={"/dashboard/users/create"} name={"CreateUsers"}
                                       component={withRouter(CreateUser)}/>,

                                <Route key={5} path={"/dashboard/users"} name={"Users"} component={withRouter(Users)}/>

                            ] :
                            null
                    }


                    {
                        canManageRevit && hasAuthority('CAN_MANAGE_REVIT_SETTINGS') ?
                            [
                                <Route key={6} path="/dashboard/revit-management/revit-settings" component={withRouter(RevitSetting)}/>,
                                <Route key={7} path="/dashboard/revit-management/revit-settings-update"
                                       component={withRouter(RevitSaveSetting)}/>
                            ] : null
                    }

                    {
                        canManageRevit && hasAuthority('CAN_MANAGE_SETTINGS') ?
                            [
                                <Route key={8} path="/dashboard/revit-management/settings" component={withRouter(Settings)}/>,
                                <Route key={9} path="/dashboard/revit-management/setting" component={withRouter(SaveSettings)}/>
                            ]
                            : null
                    }

                    {
                        canManageRevit ?
                            [
                                <Route key={10} path="/dashboard/revit-management/bq-categories" component={withRouter(BqCategories)}/>,
                                <Route key={11} path="/dashboard/revit-management/field-parameters" component={withRouter(FieldParam)}/>
                            ]
                            : null
                    }


                    {
                        canManageWorks ?
                            [
                                <Route key={12} path='/dashboard/task-board' component={withRouter(TaskBoard)}/>,
                                <Route key={13} path="/dashboard/inbox-works" component={withRouter(TaskBoard)}/>,

                                <Route key={14} exact path="/dashboard/works/create" component={withRouter(CreateWork)}/>,
                                <Route key={15} exact path="/dashboard/works/overview" component={withRouter(Overview)}/>,
                                <Route key={16} exact path="/dashboard/works/planning" component={withRouter(Planning)}/>,
                                <Route key={17} exact path="/dashboard/works/marking-history" component={withRouter(MarkingHistory)}/>,

                                <Route key={18} path="/dashboard/planning-works/myProjects" component={withRouter(PlanningProjects)}/>,
                                <Route key={19} path="/dashboard/execution-works/myProjects" component={withRouter(ExecutionProjects)}/>,


                                <Route key={20} path="/dashboard/execution-associated/associated"
                                       component={withRouter(ExecutionProjects)}/>,
                                <Route key={21} path="/dashboard/planning-associated/associated"
                                       component={withRouter(PlanningProjects)}/>,

                                <Route key={22} path="/dashboard/diary" component={withRouter(Diary)}/>,
                                <Route key={23} path="/dashboard/planning-start" component={withRouter(PlaningStart)}/>,
                                <Route key={24} path="/dashboard/planning-overview" component={withRouter(PlaningOverview)}/>,
                            ]
                            : null
                    }


                    {
                        (!firstTime && hasAuthority('CAN_MANAGE_CONTRACTOR')) ?
                            [
                                <Route key={25} path="/dashboard/firms" component={withRouter(Firms)}/>,
                                <Route key={26} path="/dashboard/firms/details" component={withRouter(FirmsDetails)}/>
                            ]
                            : null
                    }


                    {
                        (!firstTime && hasAuthority('CAN_MANAGE_DEVICES')) ?
                            <Route key={27} path="/dashboard/devices" component={withRouter(Devices)}/>
                            : null
                    }

                    <Route key={30} path="/dashboard/profile" component={withRouter(UserProfile)}/>

                    {
                        !firstTime?<Route key={28} path="/dashboard/notifications" component={withRouter(Notifications)}/>:null
                    }

                    <Route key={29} path='/dashboard' component={withRouter(User)}/>



                </Switch>
            </Suspense>
        )
    }
}

const mapStateToProps = state =>{
    return{
        activeUser: state.user_reducer.activeUser
    }
};

const connected = connect(mapStateToProps)(UserRoutesSwitch);
export default withRouter(connected);