import React from 'react';
import UserNav from "../LeftMenu/UserNav";
import TopNav from "../TopNav/TopNav";
import Popup from "../../../Components/Popup/Popup";
import UserRoutesSwitch from './UserRoutesSwitch';

class UserRoutes extends React.Component {

    renderUserPanelRoutes = () => {

        return (
            <div className="panel-container">
                <TopNav/>
                <Popup/>
                <div className={"content"}>
                    <div className="row no-gutters content-container right-panel-horizontal-spacing">
                        <UserRoutesSwitch/>
                    </div>
                </div>
            </div>
        )
    };


    render() {

        return (
            <div className="container-fluid pl-0 pr-0">
                <UserNav/>
                {this.renderUserPanelRoutes()}
            </div>
        )
    }
}


export default UserRoutes;