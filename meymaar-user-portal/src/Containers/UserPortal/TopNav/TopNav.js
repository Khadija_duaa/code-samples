import React from 'react';
import {connect} from 'react-redux';
import {formatEpochTime, formatName, hasAuthority} from "../../../utils/common-utils";
import {logoutUser} from "../../../store/user/user-actions";
import {withRouter, NavLink} from 'react-router-dom';
import {Dropdown, Menu, Icon,} from 'antd';
import NewLoader from '../../Loader/NewLoader';
import {fetchUserRecentNotifications, handleNotificationClick} from "../../../utils/server-utils";
import {deleteNotification} from "../../../store/server/server-actions";
import BreadCrumb from "../../../Components/BreadCrumb/BreadCrumb";


class TopNav extends React.Component {


    // Left Container => Create Work Button
    renderLeftPanel = () => {
        const {authorities,principal} = this.props.activeUser;
        if(principal.firstTime) return null;
        if (authorities.includes('CAN_CREATE_WORK')) {
            return (
                <div style={{padding: "unset", marginTop: '1px'}}>
                    <button className="custom-button custom-button-light"
                            onClick={() => this.props.history.push("/dashboard/works/create")}>
                        + Create New Work
                    </button>
                </div>
            )
        }

        return (
            <div className="col-md-5 align-middle right-panel-top-bar-left"/>
        );
    };


    // Right Container => Notifications & Logout button

    // Notification Area
    onMenuClick = (notification) => {
        handleNotificationClick(notification, (returnPath) => {
            this.props.history.push(returnPath);
        });
    };

    handleDeleteClick = (notificationId) => {
        let {activeUser} = this.props;
        this.props.deleteNotification(notificationId, () => {
            fetchUserRecentNotifications(activeUser.principal_id);
        })
    };


    menuJSX = () => {

        let {notifications} = this.props;
        let menuStyle = {
            height: "100%",
            marginTop: "10px",
            overflowY: "auto",
            overflowX: "hidden",
            padding: "15px",
            width: "100%",
            maxWidth: "470px",
            maxHeight:"500px"
        };


        let counter = -1;
        if (!notifications || !notifications.length) {
            return (
                <Menu>
                    <Menu.Item disabled>
                        No Notifcations!!
                    </Menu.Item>
                </Menu>
            )
        }

        let menuNAV = notifications.map((notify) => {
            counter++;
            return (
                [
                    <Menu.Item key={counter} style={{padding: "10px", cursor: "default"}}>
                        <div style={{display: "inline-block", width: "100%"}}>
                            <div style={{width: "10%", float: "left", cursor: "pointer"}}
                                 onClick={() => this.onMenuClick(notify)}>
                                {
                                    notify.read ?
                                        <Icon className={"notify-icon"} type={"notification"}/>
                                        :
                                        <Icon className={"notify-icon"} type="notification" theme="twoTone"/>
                                }
                            </div>
                            <div style={{float: "right", width: "90%"}}>
                                <div style={{cursor: "pointer"}}>

                                    <b onClick={() => this.onMenuClick(notify)}
                                       style={{display: "inline-block"}}>{notify.title}</b>

                                    <Icon type="close-circle" theme="filled"
                                          onClick={() => this.handleDeleteClick(notify._id)}
                                          style={{float: "right", display: "inline-block"}}/>
                                </div>

                                <div style={{cursor: "pointer"}} onClick={() => this.onMenuClick(notify)}>
                                    <p style={{whiteSpace: "normal"}}>{notify.body}</p>
                                    <p style={{
                                        marginBottom: "unset",
                                        textAlign: "right"
                                    }}>{formatEpochTime(notify.time)}</p>
                                </div>
                            </div>
                        </div>
                    </Menu.Item>
                    ,
                    <Menu.Divider key={counter + 2}/>
                ]
            )
        });

        menuNAV.push(
            <Menu.Item key={counter++}>
                <NavLink to={"/dashboard/notifications"}
                         style={{color: "#3B86FF", fontWeight: "bold", textAlign: "center"}}>
                    See All Notifications
                    <Icon type="arrow-right" style={{marginLeft: "20px"}}/>
                </NavLink>
            </Menu.Item>
        );

        return (
            <Menu style={menuStyle}>
                <Menu.Item style={{cursor: "default", fontWeight: "bold", fontSize: "16px"}}>
                    Notifications
                </Menu.Item>
                <Menu.Divider/>
                {menuNAV}
            </Menu>
        );
    };

    getUnreadCount = () => {
        let {notifications} = this.props;
        let sum = 0;
        if (!notifications) return null;
        notifications.filter(notify => {
            if (!notify.read) {
                return sum++;
            }
        });

        return sum;
    };

    renderNotificationButton = () => {
        const {notifications} = this.props;


        let notificationStyle = `${notifications && notifications.length && this.getUnreadCount() > 0 ? 'pointer' : 'default'}`;

        return (
            <span className="notification-container" style={{cursor: notificationStyle}}>
                <Dropdown overlayClassName={"customMenu"} overlay={this.menuJSX} trigger={['click']}>
                    <div>
                        <img style={{cursor: notificationStyle}} alt={"bell"} src="./images/bell.svg"
                             className="notification-bell"/>
                        {notifications && notifications.length && this.getUnreadCount() > 0 ?
                            <span className="notification-counter">{this.getUnreadCount()}</span> : null}
                    </div>
                </Dropdown>
            </span>
        )
    };


    // Logout Button Area

    renderLogoutButton = () => {
        const {activeUser} = this.props;
        let office, role = null;

        if (activeUser.roles && activeUser.roles.length) {
            let roles = activeUser.roles[0];
            office = roles.office ? roles.office.name : null;
            role = roles.name;
        }


        return (
            [
                <div key={0} role="button" id="memberLinks" className="login-options-container"
                     data-toggle="dropdown"
                     aria-haspopup="true" aria-expanded="false">
                    {`${formatName(activeUser.principal)} ${role ? ' (' + role + ' of ' + office + ') ' : ""}`}
                    <img alt={"arrow"} src="./images/arrow-bottom.svg" className="login-options-arrow"/>
                </div>,

                <div key={1} className="dropdown-menu login-options" aria-labelledby="memberLinks">
                    {!hasAuthority('SUPER_ADMIN') && !activeUser.principal.firstTime?<a className="dropdown-item" onClick={() => this.props.history.push("/dashboard/profile")}>
                        <img src={"/images/settings.svg"} style={{marginRight:"10px",width:"17px"}}/> Profile Settings
                    </a>:null}
                    <a className="dropdown-item" style={{marginTop:"5px"}} onClick={() => this.props.logoutUser(() => window.location.reload())}>
                        <img src={"/images/logout.svg"} style={{marginRight:"10px",width:"17px"}}/> Logout
                    </a>
                </div>
            ]
        )
    };

    renderRightPanel = () => {
        let {activeUser} = this.props;
        return (
            <div className="col-md-7 align-middle text-right right-panel-top-bar-right">
                {
                    this.props.processing ?
                        <div><NewLoader/></div>
                        :
                        <div>
                            {!activeUser.principal.firstTime?this.renderNotificationButton():null}

                            <img alt={"member"} src="./images/member.svg"/>

                            {this.renderLogoutButton()}
                        </div>
                }
            </div>
        )
    };

    getBreadCrumbsPlanning = (pathName) => {

        let title = "Marking History";
        if (pathName.includes('planning')) {
            title = 'Planning & Design'
        } else if (pathName.includes('diary')) {
            title = 'Execution & Tracking'
        }
        let breadCrumbsJSON = [
            {
                title: "Overview",
                returnPath: '/dashboard/works/overview'
            },
            {
                title
            }
        ];


        return (
            <BreadCrumb className={"document-crumb"} style={{fontSize: "15px"}} crumbsJSON={breadCrumbsJSON}/>
        )
    };

    _style = {
        fontWeight: 'bolder',
        fontSize: '17px',
        color: 'black'
    };

    getHeaderProjectInfo = (projectDetails, pathName) => {

        return (

            <div>
                <div style={{display: 'flex', marginBottom: '-20px'}}>
                    <p style={this._style}>REF # {projectDetails && projectDetails.referenceNumber}</p>
                    <p style={this._style}> &nbsp; -  &nbsp; </p>
                    <p style={this._style}>{projectDetails && projectDetails.name}</p>
                </div>
                <div>
                    {pathName === '/dashboard/works/overview' ? this.getBreadCrumbsOverview() : this.getBreadCrumbsPlanning(pathName)}
                </div>
            </div>
        )
    };
    getProjectState = () => {
        const storageKey = "overview-projectId";
        let locationState = sessionStorage.getItem(storageKey);


        if (locationState) {
            locationState = JSON.parse(locationState);
        }


        const {location} = this.props;

        if (location && location.state) {
            locationState = location.state;
        }

        sessionStorage.setItem(storageKey, JSON.stringify(locationState));
        return locationState;

    };

    getBreadCrumbsOverview = () => {

        let projectState = this.getProjectState();
        let breadCrumbsJSON = [
            {
                title: projectState.backTitle,
                returnPath: projectState.backCB
            },
            {
                title: 'Overview'
            }
        ];


        return (
            <BreadCrumb className={"document-crumb"} style={{fontSize: "15px"}} crumbsJSON={breadCrumbsJSON}/>
        )
    };

    getHeader = () => {
        let {projectDetails} = this.props;
        let {location} = this.props;

        let pathName = location && location.pathname;

        return (
            <div style={{display: 'block'}}
                 className="col-md-5 align-middle right-panel-top-bar-left">
                {projectDetails && (pathName === '/dashboard/works/overview' || pathName === '/dashboard/works/planning'
                    || pathName === '/dashboard/works/marking-history' || pathName.includes('diary')) ? this.getHeaderProjectInfo(projectDetails, pathName) : this.renderLeftPanel()}
            </div>
        )
    };


    render() {

        return (
            <div className="fixed-top-bar">
                <div className="row no-gutters right-panel-top-bar custom-top-bar right-panel-horizontal-spacing">
                    {this.getHeader()}
                    {this.renderRightPanel()}
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        activeUser: state.user_reducer.activeUser,
        processing: state.user_reducer.processing,
        notifications: state.server_reducer.userNotifications,
        projectDetails: state.project_reducer && state.project_reducer.projectDetails
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logoutUser: (cb) => dispatch(logoutUser(cb)),
        deleteNotification: (id, cb) => dispatch(deleteNotification(id, cb))
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(TopNav);
export default withRouter(connected);