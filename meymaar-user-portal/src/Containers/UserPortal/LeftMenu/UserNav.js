import React from 'react';
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {withRouter} from 'react-router-dom';
import 'antd/dist/antd.css'
import {navJSONArray} from "../../../utils/utilsJSON";

class UserNav extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isClose: false,
            isManageDeveice: false,
            isManageRevit:false,
            navJson: navJSONArray(this.props.activeUser.principal),
        }
    }

    toggleMenu = () => {
        this.setState({isClose: !this.state.isClose});
    };

    getWelcomeText = () => {
        const hideClass = this.state.isClose ? 'nav-hide' : '';

        const hideStyle = {
            left: "0px",
            float: "right"
        };

        const showStyle = {
            left: "5px",
        };
        return (
            <div>
                <div className="top-logo-container custom-top-logo">
                    <a href={"javascript:;"} style={hideClass ? {...showStyle} : {...hideStyle}}>
                        <img alt={"bars"} onClick={this.toggleMenu} src={"/images/bars.svg"} className="header-menu-bars pa"/>
                    </a>

                    <img alt={"logo"} style={{cursor: "pointer",left: "17px",marginLeft: "95px"}} width={100} onClick={() => this.props.history.push("/dashboard")}
                         src={"/images/meymaar_logo.svg"}
                         className={`left-menu-main-link hide-from-menu ${hideClass}`}/>
                </div>
            </div>
        )
    };

    getNavMenus = () => {

        const hideClass = this.state.isClose ? 'nav-hide' : '';
        const {location} = this.props;

        let navJSX = this.state.navJson.map((nav, index) => {

            if(!nav.showMenu) return null;
            else if (nav.submenu) {

                const subMenuJSX = nav.subOptions.map((subNav, i) => {
                    if(!subNav.showMenu) return null;
                    return (
                        <li key={i} className={nav.to === location.pathname || nav.to.pathname === location.pathname ? "active" : ""}>
                            <NavLink to={subNav.to}>
                                {subNav.title}
                            </NavLink>
                        </li>
                    )
                });

                return (
                    <li key={index}>
                        <a href={`#worksubmenu${index}`} data-toggle="collapse" aria-expanded="false"
                           className="dropdown-toggle">
                            <img alt={nav.title} src={nav.image} className="header-menu-bars"/>
                            <span className={`hide-from-menu ${hideClass}`}>
                                {nav.title}
                                </span>
                        </a>
                        <ul id={`worksubmenu${index}`}
                            className={`collapse ${this.state.isClose ? 'sub-menu-links' : ''}`}>
                            {subMenuJSX}
                        </ul>
                    </li>
                )
            }
            else {
                return (
                    <li key={index} className={nav.to === location.pathname || nav.to.pathname === location.pathname ? "active" : ""}>
                        <NavLink to={nav.to}>
                            <img alt={nav.image === "" ? null :"menu"}  src={nav.image === ""? null : nav.image} className="header-menu-bars"/>
                            <span  style={{textAlign :nav.image === "" ? 'center': null, wordSpacing :nav.image === "" ? '7px': null}} className={nav.image === "" ? null :`hide-from-menu ${hideClass}`}>
                                {nav.title}
                                </span>

                        </NavLink>
                    </li>
                )
            }
        });

        return (
            <div className="left-menu-links custom-left-nav">
                <ul>
                    {navJSX}
                </ul>
            </div>
        )
    };

    render() {

        return (

            <div className={`left-panel ${this.state.isClose ? 'closeNavPanel' : 'openNavPanel'}`} id="leftPanel">
                {this.getWelcomeText()}
                {this.getNavMenus()}
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        activeUser: state.user_reducer.activeUser
    }
};


const connected = connect(mapStateToProps)(UserNav);
export default withRouter(connected);