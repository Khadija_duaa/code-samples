import React from 'react';
import {connect} from 'react-redux';
import {loginUser, logout} from "../store/user/user-actions";
import {withRouter} from 'react-router-dom';

import SecurityQuestions from '../Containers/UserPortal/Content/UserProfile/SecurityQuestions';
import LoginForm from "./LoginForm/LoginForm";

class MeymarLogin extends React.Component {



    componentDidMount() {
        if (this.props.userAuthenticated) {
            this.props.logout();
        }
    }

    componentDidUpdate() {
        if (this.props.userAuthenticated) {
            this.props.history.push('/');
        }
    }


    render() {

        return (
            <div className="m-auto pl-2 pr-2 text-center login-div">

                <LoginForm/>
                {/*<SecurityQuestions isForgot={false}/>*/}

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        activeUser: state.user_reducer.activeUser,
        userAuthenticated: state.user_reducer.authenticated,
    }
};
const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(logout())
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(MeymarLogin);
export default withRouter(connected);