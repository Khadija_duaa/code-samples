
const GET_URL = () => {
    const {REACT_APP_ENV} = process.env;
    let SERVER_URL_OBJ = '', NOTIFICATION_URL_OBJ = '', MODEL_URL_OBJ = '';

    switch (REACT_APP_ENV.trim()) {
        case 'local' : {
            SERVER_URL_OBJ = 'http://app.meymaar:8080';
            MODEL_URL_OBJ = 'http://app.meymaar:3001';
            NOTIFICATION_URL_OBJ = "http://app.meymaar:5700";
            break;
        }
        case 'dev' : {
            SERVER_URL_OBJ = 'http://dev.app.meymaar:8080';
            MODEL_URL_OBJ = 'http://dev.app.meymaar:3001';
            NOTIFICATION_URL_OBJ = "http://dev.app.meymaar:5700";
            break;
        }
        case 'staging' : {
            SERVER_URL_OBJ = 'http://app.meymaar:8080';
            MODEL_URL_OBJ = 'http://app.meymaar:3001';
            NOTIFICATION_URL_OBJ = "http://app.meymaar:5700";
            break;
        }
        case 'production' : {
            SERVER_URL_OBJ = 'http://app.meymaar:8080';
            MODEL_URL_OBJ = 'http://app.meymaar:3001';
            NOTIFICATION_URL_OBJ = "http://app.meymaar:5700";
            break;
        }
        default :
            break;
    }
    return {SERVER_URL_OBJ, MODEL_URL_OBJ, NOTIFICATION_URL_OBJ};
};

export const {SERVER_URL_OBJ, MODEL_URL_OBJ, NOTIFICATION_URL_OBJ} = GET_URL();