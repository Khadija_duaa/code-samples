export const handleError = (error) => {
    let errorMesage = 'Some error occurred, Unable to connect to server';

    console.error('error', error);


    if (error.response) {
        console.error('error.response', error.response);
        let {status} = error.response;
        if(status >= 500 ){
            errorMesage = "Server Error Occurred !";
            return errorMesage;
        }else if(status === 401){
            errorMesage = "Access Denied!";
            return errorMesage;
        }
    }

    if (error.response && error.response.data) {
        console.error('error.response.data', error.response.data);

        errorMesage = error.response.data.message;
    }

    return errorMesage;
};