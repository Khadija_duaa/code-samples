import * as actions from './server-action-types';
import axios from '../../utils/axios';
import {handleError} from "../store-utils";
import {message} from 'antd';
import 'antd/dist/antd.css';
import {logout} from "../user/user-actions";
import {NOTIFICATION_TIME, sortByKeyAsc} from "../../utils/common-utils";


message.config({
    top: 70,
    duration: NOTIFICATION_TIME,
    maxCount: 3
});

// SAVING USER notifications
export const setUserNotifications = (notifications) => {
    return {
        type: actions.SET_NOTIFICATIONS,
        payload: notifications
    }
};

// Saving All Domains of a project and display in dropdown
export const saveDomains = (payload) => {
    return {
        type: actions.SET_ALL_DOMAINS,
        payload: payload
    }
};


export const resetRedux = (key) => {
    return {
        type: actions.RESET_SERVER_REDUX,
        payload: key
    }
};


export const setSocketIO = (socket) => {
    return {
        type: "SET_SOCKETIO",
        payload: socket
    };
};


// Saving All Users of a project and display in users view
const setAdmins = (payload) => {
    return {
        type: actions.SET_ALL_ADMINS,
        payload: payload
    }
};


// Saving All Users of a project and display in users view
const setUsers = (payload) => {
    return {
        type: actions.SET_ALL_USERS,
        payload: payload
    }
};

// Saving Domain wise All Users of a project and display in users view
const setDomainUsers = (payload) => {
    return {
        type: actions.SET_ALL_DOMAIN_USERS,
        payload: payload
    }
};


// Saving All Offices of a project to display in offices view
const setOffices = (payload) => {
    return {
        type: actions.SET_ALL_OFFICES,
        payload: payload
    }
};


// Saving details of particular office and display in office users view
const setOfficeDetails = (payload) => {
    return {
        type: actions.SET_OFFICE_DETAILS,
        payload: payload
    }
};


// Saving details of particular office and display in office users view
const setSubOfficeDetails = (payload) => {
    return {
        type: actions.SET_SUB_OFFICES,
        payload: payload
    }
};


// Saving details of particular office and display in office users view
const setLinkedOfficeDetails = (payload) => {
    return {
        type: actions.SET_LINKED_OFFICES,
        payload: payload
    }
};

// Saving All Designations of a project and display in Manage Ranks view
const saveDesignations = (payload) => {
    return {
        type: actions.SET_ALL_DESIGNATONS,
        payload: payload
    }
};

// Saving All Stations inside reducer
const saveStations = (payload) => {
    return {
        type: actions.SET_ALL_STATIONS,
        payload: payload
    }
};


// Saving Privilleges of a role and display in Manage Ranks view
const savePrivilleges = (payload) => {
    return {
        type: actions.SET_ALL_PRIVILLEGES,
        payload: payload
    }
};

// Saving All Notifications
const setAllNotifications = (payload) => {
    return {
        type: actions.SET_ALL_NOTIFICATIONS,
        payload: payload
    }
};

// Saving Departments
const saveDepartments = (payload) => {
    return {
        type: actions.SET_ALL_DEPARTMENTS,
        payload: payload
    }
};

//Saving Contracting Powers
const saveContractingPowers = (payload) => {
    return {
        type: actions.SET_ALL_CONTRACTING_POWERS,
        payload: payload
    }
};


//Saving Contracting Powers
const saveOfficeAuthorites = (payload) => {
    return {
        type: actions.SET_ALL_OFFICE_AUTHORITIES,
        payload: payload
    }
};


// Setting Loader processing boolean
export const setProcessing = (payload) => {
    return {
        type: actions.PROCESSING,
        payload: payload
    }
};


/********************************** Department Server Actions ******************************/

// Creating Department
export const createDepartments = (data, cb, errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.post('/office-management/departments', data)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            errorCB && errorCB(errorMessage);
        })
    };
};

// Fetching All Department
export const getAllDepartments = (cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get('/office-management/departments')
            .then(response => {
                dispatch(setProcessing(false));
                let {departments} = response.data;
                dispatch(saveDepartments(departments));
                cb && cb(departments);
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    };
};

// Updating Department
export const updateDepartment = (data, id, cb, errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.put(`/office-management/departments/${id}`, data)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            errorCB && errorCB(errorMessage);
        })
    }
};

/************************************************* END **************************************/


/********************************** Domains Server Actions ******************************/

// Creating Department
export const createDomain = (data, cb, errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.post('/office-management/domains', data)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            errorCB && errorCB(errorMessage);
        })
    };
};

// Updating Domain
export const updateDomain = (data, id, cb, errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.put(`/office-management/domains/${id}`, data)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            errorCB && errorCB(errorMessage);
        })
    }
};

/************************************************* END **************************************/


/********************************** Contracting Powers Server Actions ******************************/

// Creating Contracting Power
export const createContractingPower = (data, cb, errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.post('/office-management/contractual-powers', data)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {

            let errorMessage = handleError(err);
            errorCB && errorCB(errorMessage);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));

        })
    };
};

// Fetching All Department
export const getAllContractingPowers = (cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get('/office-management/contractual-powers')
            .then(response => {
                dispatch(setProcessing(false));
                let {contractualPowers} = response.data;
                dispatch(saveContractingPowers(contractualPowers));
                cb && cb(contractualPowers);
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    };
};

// Updating Department
export const updateContractingPower = (data, id, cb, errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.put(`/office-management/contractual-powers/${id}`, data)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            errorCB && errorCB(errorMessage);
        })
    }
};

// Fetching All Department
export const getPowerNextParent = (cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get('/office-management/contractual-powers/next-parent')
            .then(response => {
                dispatch(setProcessing(false));
                let {nextParent} = response.data;
                cb && cb(nextParent);
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    };
};

/************************************************* END **************************************/


/********************************** Contracting Powers Server Actions ******************************/

// Creating Office Authority
export const createOfficeAuthority = (data, cb, errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.post('/office-management/office-authorities', data)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {

            let errorMessage = handleError(err);
            errorCB && errorCB(errorMessage);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));

        })
    };
};

// Fetching All Office Authorities
export const getAllOfficeAuthorities = (cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get('/office-management/office-authorities')
            .then(response => {
                dispatch(setProcessing(false));
                let {officeAuthorities} = response.data;
                dispatch(saveOfficeAuthorites(officeAuthorities));
                cb && cb(officeAuthorities);
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    };
};

// Updating Office Authority
export const updateOfficeAuthority = (data, id, cb, errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.put(`/office-management/office-authorities/${id}`, data)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            errorCB && errorCB(errorMessage);
        })
    }
};

// Fetching Next Parents Info
export const getNextParentInfo = (departmentId, domainId, cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(`/office-management/office-authorities/of/${departmentId}/belongs-to/${domainId}/next-parent`)
            .then(response => {
                dispatch(setProcessing(false));
                let nextParentInfo = response.data;
                cb && cb(nextParentInfo);
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    };
};


export const fetchAuthorityPowers = (authId, cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(`/office-management/office-authorities/${authId}/update-info`)
            .then(response => {
                dispatch(setProcessing(false));
                cb && cb(response.data);
            }).catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));
        })
    }
};
/************************************************* END **************************************/


/******************************************* STATIONS Server Actions *******************************************/

export const getAllStations = (cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get('/office-management/geo-locations')
            .then(response => {
                let {geoLocations} = response.data;
                dispatch(setProcessing(false));
                dispatch(saveStations(geoLocations));
                cb && cb(geoLocations);

            }).catch(err => {
            let errorMessage = handleError(err);
            dispatch(setProcessing(false));
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    }
};

export const createStation = (data, cb, errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.post('/office-management/geo-locations', data)
            .then(() => {
                cb && cb();
            }).catch(err => {
            let errorMessage = handleError(err);
            dispatch(setProcessing(false));
            errorCB && errorCB(errorMessage);
        })

    }
};

export const updateStation = (data, id, cb, errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.put(`/office-management/geo-locations/${id}`, data)
            .then(() => {
                cb && cb();
            }).catch(err => {
            let errorMessage = handleError(err);
            dispatch(setProcessing(false));
            message.error(errorMessage, NOTIFICATION_TIME);
            errorCB && errorCB(errorMessage);
        })
    }
};

/************************************************* END ***********************************/


/******************************************* USERS Server Actions *******************************************/

// POST API to create user ( Admin, User) and display success message otherwise error fields returned with message
export const createUser = (submitURL = '', data, superAdmin = false, successCB, errorCB, successText) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.post(submitURL, data)
            .then(() => {
                setTimeout(() => {

                    dispatch(setProcessing(false));
                    if (superAdmin) {
                        message.success("Admin has created successfully", NOTIFICATION_TIME);
                        dispatch(logout());
                    } else {
                        message.success(successText, NOTIFICATION_TIME);
                        successCB && successCB();
                    }
                }, 1000);
            }).catch(err => {

            let errorMessage = handleError(err);
            dispatch(setProcessing(false));

            message.error(errorMessage, NOTIFICATION_TIME);
            if (err && err.response && err.response.data && err.response.data.errors && err.response.data.errors.length) {
                let {errors} = err.response.data;
                errorCB && errorCB(errors);
            }
        })
    }
};


export const updateUser = (submitURL, data, successCB, errorCB, successText) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.put(submitURL, data)
            .then(() => {
                setTimeout(() => {

                    dispatch(setProcessing(false));
                    message.success("User has updated successfully", NOTIFICATION_TIME);
                    successCB && successCB();
                }, 1000);
            }).catch(err => {

            let errorMessage = handleError(err);
            dispatch(setProcessing(false));

            message.error(errorMessage, NOTIFICATION_TIME);
            if (err && err.response && err.response.data && err.response.data.errors && err.response.data.errors.length) {
                let {errors} = err.response.data;
                errorCB && errorCB(errors);
            }
        })
    }
};


// GET API to fetch all USERS and save in reducer using setUser Method
export const fetchAllUsers = (pageSize, domainId) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        let apiURL = '';
        if (!domainId) {
            apiURL = `/user-management/users/?size=${pageSize}`
        } else {
            apiURL = `/office-management/users/domain/${domainId}`
        }

        axios.get(apiURL)
            .then(response => {

                if (!domainId) {
                    dispatch(setUsers(response.data.content));
                } else {
                    dispatch(setDomainUsers(response.data.ranks));
                }


                dispatch(setProcessing(false));
            }).catch(err => {
            let errorMessage = handleError(err);

            dispatch(setProcessing(false));
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    }
};


// GET API to fetch all ADMINS and save in reducer using setAdmins Method
export const fetchAllAdmins = () => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.get('/office-management/users/admins')
            .then(response => {

                dispatch(setAdmins(response.data));
                dispatch(setProcessing(false));
            }).catch(err => {
            let errorMessage = handleError(err);

            dispatch(setProcessing(false));
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    }
};


// POST API to Assign Role to User (roleId -> to -> userId) and display success message otherwise display error message
export const assignRoleToUser = (userId, roleId, successCB, errorCB, successMessage) => {

    return (dispatch) => {

        dispatch(setProcessing(true));

        axios.post(`/user-roles-management/user-roles/${roleId}/to/${userId}`)
            .then(response => {
                setTimeout(() => {
                    dispatch(setProcessing(false));
                    message.success(successMessage, NOTIFICATION_TIME);
                    successCB && successCB();
                }, 1000);
            }).catch(err => {
            let errorMessage = handleError(err);
            dispatch(setProcessing(false));

            message.error(errorMessage, NOTIFICATION_TIME);

            if (err && err.response && err.response.data && err.response.data.errors && err.response.data.errors.length) {
                let {errors} = err.response.data;
                errorCB && errorCB(errors);
            }

        })
    }
};


// Delete API to Revoke Role from User (roleId -> from -> userId) and display success message otherwise display error message
export const revokeRoleFromUser = (data, successCB) => {

    return (dispatch) => {

        dispatch(setProcessing(true));

        axios.delete(`/user-roles-management/user-roles/${data.rankId}/from/${data.userId}`)
            .then(response => {
                setTimeout(() => {
                    dispatch(setProcessing(false));
                    message.success(data.successMessage, NOTIFICATION_TIME);
                    successCB && successCB();
                }, 1000);
            }).catch(err => {
            let errorMessage = handleError(err);
            dispatch(setProcessing(false));

            message.error(errorMessage, NOTIFICATION_TIME);
        })
    }
};

/************************************************* END ********************************************************/


// Fetching Potential Parents Info
export const getPotentialParents = (authId, cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(`/office-management/offices/potential-parents/${authId}`)
            .then(response => {

                let offices = response.data.offices;
                offices = offices.length ? offices : null;
                cb && cb(offices);
                dispatch(setProcessing(false));
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    };
};

/******************************************* OFFICE Server Actions *******************************************/


// Fetching Departments & Domain Authorities Info
export const getDepartmentAuthorities = (departmentId, domainId, cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(`/office-management/office-authorities/of/${departmentId}/belongs-to/${domainId}`)
            .then(response => {

                let officeAuthorities = response.data.officeAuthorities;
                officeAuthorities = officeAuthorities.length ? officeAuthorities : null;
                cb && cb(officeAuthorities);
                dispatch(setProcessing(false));
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    };
};


// POST API to create office and display success message otherwise error fields returned with message
export const createOffice = (data, successCB, errorCB, successText) => {
    return (dispatch) => {

        dispatch(setProcessing(true));

        axios.post('/office-management/offices', data)
            .then(response => {
                setTimeout(() => {
                    dispatch(setProcessing(false));
                    message.success(successText, NOTIFICATION_TIME);
                    successCB && successCB();
                }, 1000);
            }).catch(err => {

            let errorMessage = handleError(err);
            dispatch(setProcessing(false));

            message.error(errorMessage, NOTIFICATION_TIME);
            if (err && err.response && err.response.data && err.response.data.errors && err.response.data.errors.length) {
                let {errors} = err.response.data;
                errorCB && errorCB(errors);
            }
        })
    }
};


// PUT API to update office and display success message otherwise error fields returned with message
export const updateOffice = (officeId, data, successCB, errorCB, successText) => {
    return (dispatch) => {

        dispatch(setProcessing(true));

        axios.put(`/office-management/offices/${officeId}`, data)
            .then(response => {
                setTimeout(() => {
                    dispatch(setProcessing(false));
                    message.success(successText, NOTIFICATION_TIME);
                    successCB && successCB();
                }, 1000);
            }).catch(err => {

            let errorMessage = handleError(err);
            dispatch(setProcessing(false));

            message.error(errorMessage, NOTIFICATION_TIME);
            if (err && err.response && err.response.data && err.response.data.errors && err.response.data.errors.length) {
                let {errors} = err.response.data;
                errorCB && errorCB(errors);
            }
        })
    }
};


// GET API to fetch all OFFICES and save in reducer using setOffices Method
export const fetchAllOffices = (domainId, cb) => {

    return (dispatch) => {

        dispatch(setProcessing(true));

        let apiURL = '';
        if (!domainId) {
            apiURL = `/office-management/offices`
        } else {
            apiURL = `/office-management/offices/domain/${domainId}`
        }


        axios.get(apiURL)
            .then(response => {
                response.data.offices.forEach(office => {
                    office.department = office.officeAuthority.department.name;
                    office.domain = office.officeAuthority.domain.name;
                    office.station = office.geoLocationEntity ? office.geoLocationEntity.name : "-";
                    office.head = office.headOffice ? office.headOffice.name : "-";
                    office.name = office.parentOffice ? `${office.parentOffice.name} - ${office.name}` : office.name;
                });
                dispatch(setOffices(response.data.offices));
                dispatch(setProcessing(false));

                cb && cb(response.data.offices);
            }).catch(err => {

            let errorMessage = handleError(err);
            dispatch(setProcessing(false));
            message.error(errorMessage, NOTIFICATION_TIME);

        });
    }
};


// GET API to fetch Office Details of particular Office ID and save in reducer using setOfficeDetails Method
export const fetchOfficeDetails = (officeId) => {

    return (dispatch) => {

        dispatch(setProcessing(true));

        console.log("Office Id", officeId);
        axios.get(`/office-management/users/office-only/${officeId}`)
            .then(response => {

                console.log(response.data);

                dispatch(setOfficeDetails(response.data.ranks));

                dispatch(setProcessing(false));

            }).catch(err => {

            let errorMessage = handleError(err);
            dispatch(setProcessing(false));

            message.error(errorMessage, NOTIFICATION_TIME);
        })
    }
};


// GET API to fetch all Deginations of an office and save in reducer using setDesignation Method
export const fetchDesignations = (officeID, cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.get(`/office-management/designations/office/${officeID}`)
            .then(response => {
                let {designations} = response.data;
                console.log("All designations", response);
                dispatch(setProcessing(false));
                dispatch(saveDesignations(designations));

                designations = sortByKeyAsc(designations, 'id');

                if (designations.length) {
                    cb && cb(designations[0].uuid)
                }

            }).catch(err => {

            let errorMessage = handleError(err);
            dispatch(setProcessing(false));
            message.error(errorMessage, NOTIFICATION_TIME);


        })
    }
};


// POST API to create a Deginations of an office and send role id in callback
export const createDesignation = (data, cb) => {

    console.log("Create Designation", data);

    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.post(`/office-management/designations`, data)
            .then(response => {

                console.log("Designation created", response.data);
                let {uuid} = response.data;

                dispatch(setProcessing(false));

                cb && cb(uuid);

            }).catch(err => {

            let errorMessage = handleError(err);
            dispatch(setProcessing(false));
            message.error(errorMessage, NOTIFICATION_TIME);

        })
    }
};

// PUT API to Update a Degination
export const updateDesignation = (data, designationId, cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.put(`/office-management/designations/${designationId}`, data)
            .then(response => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    }
};

/************************************************* END ********************************************************/


/******************************************* PRIVILLEGE Server Actions *******************************************/

// GET API to fetch Role Privilleges of a role and save in reducer using setPrivilleges Method
export const getRolePrivilleges = (roleId) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.get(`/privileges-management/roles-privileges/${roleId}`)
            .then(response => {

                let privileges = response.data.privileges;


                dispatch(setProcessing(false));
                dispatch(savePrivilleges(privileges));

            }).catch(err => {

            let errorMessage = handleError(err);
            dispatch(setProcessing(false));
            message.error(errorMessage, NOTIFICATION_TIME);


        })
    }
};

export const getAllPriviliges = () => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get('/privileges-management/privileges')
            .then(response => {
                let privillegeResponse = response.data;

                dispatch(setProcessing(false));
                dispatch(savePrivilleges(privillegeResponse));
            }).catch(err => {
            let errorMessage = handleError(err);
            dispatch(setProcessing(false));
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    }
};

export const assignPrivilegesToRole = (data, cb) => {

    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.post('/privileges-management/roles', data)
            .then(response => {

                //message.success("Privilleges has assigned to role successfully", NOTIFICATION_TIME);
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            let errorMessage = handleError(err);
            dispatch(setProcessing(false));
            message.error(errorMessage, NOTIFICATION_TIME)
        })
    }
};


/************************************************* END ********************************************************/


/********************************** Fetching Notifications ******************************/

//Fetching All Notifications
export const fetchAllUserNotifications = (userId, pageNumber) => {
    return (dispatch) => {

        dispatch(resetRedux('allNotifications'));
        dispatch(resetRedux('notificationPagination'));
        dispatch(setProcessing(true));

        axios.get(`/notification-service/getNotification/user/${userId}/${pageNumber}`)
            .then(response => {
                dispatch(setAllNotifications(response.data));
                dispatch(setProcessing(false));

            }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        })
    }

};

export const deleteNotification = (notificationId, cb) => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));
        axios.delete(`/notification-service/delNotification/${notificationId}`)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));
        })
    }
};

export const deleteAllNotifications = (userId, cb) => {
    return (dispatch) => {
        dispatch(resetRedux('allNotifications'));
        dispatch(resetRedux('notificationPagination'));
        dispatch(setProcessing(true));

        axios.delete(`/notification-service/delNotification/user/${userId}`)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));
        })
    };
};

/************************************************* END *************************************/


/********************************** Saving Sub Office Data *********************************/
export const createSubOffice = (data, cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.post(`/office-management/offices/sub-office`, data)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));
        })
    }
};

export const fetchSubOfficeDetails = (id) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(`/office-management/offices/${id}/sub-offices`)
            .then(response => {
                dispatch(setProcessing(false));
                dispatch(setSubOfficeDetails(response.data.subOffices));
            }).catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));
        })
    }
};


/************************************************* END **************************************/


/********************************** Saving Linked Office Data *********************************/
export const createLinkedOffice = (officeId, linkedOffice, cb,errorCB) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.post(`/office-management/offices/${officeId}/link-with/${linkedOffice}`)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb();
            }).catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));
            errorCB && errorCB();
        })
    }
};

export const getLinkedOffices = (officeId) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(`/office-management/offices/${officeId}/linked-offices`)
            .then(response => {
                dispatch(setProcessing(false));
                dispatch(setLinkedOfficeDetails(response.data.linkedOffices));
            }).catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));
        })
    }
};

export const unlinkOffice = (officeId, linkedOffice,cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.delete(`/office-management/offices/${linkedOffice}/unlink-with/${officeId}`)
            .then(() => {
                dispatch(setProcessing(false));
                cb && cb()
            }).catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));
        })
    }
};


/************************************************* END **************************************/