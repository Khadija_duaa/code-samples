import * as actions from './server-action-types';

const getDomains = () => {
    let domains = sessionStorage.getItem('domains');
    if (domains) return JSON.parse(domains);
    return null;
};


const getNotifications = () => {
    let notifications = sessionStorage.getItem('userNotification');
    if (notifications) {
        return JSON.parse(notifications);
    }
    return null;
};


const initState = () => {
    return {
        processing: false,
        projectDomains: getDomains(),

        users: null,
        admins: null,

        offices: null,
        officeDetails: null,
        subOffices: null,
        linkedOffices:null,

        designations: null,
        privilleges: null,

        stations: null,
        departments: null,
        contractingPowers : null,

        officeAuthorities : null,

        socket: null,
        userNotifications: getNotifications(),
        allNotifications: null,
        notificationPagination: null
    }
};

const server_reducer = (state = initState(), action) => {
    let newState = {...state};

    switch (action.type) {

        case actions.SET_NOTIFICATIONS:
            saveNotifications(newState, action.payload);
            break;

        case actions.SET_ALL_NOTIFICATIONS:
            saveAllNotifications(newState, action.payload);
            break;

        case actions.SET_ALL_DOMAINS:
            setProjectDomains(newState, action.payload);
            break;

        case actions.SET_ALL_ADMINS:
            saveAdmins(newState, action.payload);
            break;

        case actions.SET_ALL_USERS:
            saveUsers(newState, action.payload);
            break;

        case actions.SET_ALL_DOMAIN_USERS:
            saveDomainUsers(newState, action.payload);
            break;

        case actions.SET_ALL_OFFICES:
            saveOffices(newState, action.payload);
            break;

        case actions.SET_OFFICE_DETAILS:
            saveOfficeDetails(newState, action.payload);
            break;


        case actions.SET_SUB_OFFICES:
            saveSubOfficeDetails(newState, action.payload);
            break;


        case actions.SET_LINKED_OFFICES:
            saveLinkedOffices(newState, action.payload);
            break;

        case actions.SET_ALL_DESIGNATONS:
            saveDesignations(newState, action.payload);
            break;

        case actions.SET_ALL_PRIVILLEGES:
            savePrivilleges(newState, action.payload);
            break;

        case actions.SET_ALL_STATIONS:
            saveStations(newState, action.payload);
            break;

        case actions.SET_ALL_DEPARTMENTS:
            saveDepartments(newState, action.payload);
            break;

        case actions.SET_ALL_CONTRACTING_POWERS:
            saveContractingPowers(newState,action.payload);
            break;

        case actions.SET_ALL_OFFICE_AUTHORITIES:
            saveAllOfficeAuthorities(newState,action.payload);
            break;

        case actions.RESET_SERVER_REDUX:
            resetRedux(newState, action.payload);
            break;

        case "SET_SOCKETIO" :
            setSocket(newState, action.payload);
            break;
        case actions.PROCESSING:
            setProcessing(newState, action.payload);
            break;

        default:
            break;

    }

    return newState;
};


const saveNotifications = (state, data) => {
    state.userNotifications = data;
    sessionStorage.setItem('userNotification', JSON.stringify(data));
};

const saveAllNotifications = (state, data) => {
    state.notificationPagination = data.pagination;
    state.allNotifications = data.notifications;
};

const setProjectDomains = (state, domains) => {
    state.projectDomains = domains;
    sessionStorage.setItem('domains', JSON.stringify(domains));
};

const saveAdmins = (state, adminsData) => {
    let admins = [];

    adminsData.ranks && adminsData.ranks.forEach(rank => {
        rank.users && rank.users.forEach(user => {
            admins.push({...user});
        })
    });

    state.admins = admins;
};

const saveUsers = (state, usersData) => {
    state.users = usersData;
};

const saveDomainUsers = (state, domainUsersData) => {
    let domainUsers = [];

    domainUsersData.forEach(rank => {
        rank.users && rank.users.forEach(user => {
            domainUsers.push({...user})
        })
    });

    state.users = domainUsers;
};

const saveOffices = (state, offices) => {
    state.offices = offices;
};

const saveOfficeDetails = (state, rankDetails) => {
    let users = [];

    rankDetails.forEach(rank => {
        rank.users && rank.users.forEach(user => {
            let rankName = rank.name;
            let rankId = rank.uuid;
            users.push({...user, rankName, rankId});
        })
    });


    state.officeDetails = users;
};

const saveSubOfficeDetails = (state, subOffices) => {
    state.subOffices = subOffices;
};

const saveLinkedOffices = (state, linkedOffices) => {
    state.linkedOffices = linkedOffices;
};


const saveDesignations = (state, designations) => {
    state.designations = designations;
};

const savePrivilleges = (state, privilleges) => {

    state.privilleges = privilleges;
};

const saveStations = (state, stations) => {
    state.stations = stations;
};

const saveDepartments = (state, departments) => {
    state.departments = departments;
};

const saveContractingPowers = (state,powers)=>{
  state.contractingPowers = powers;
};

const saveAllOfficeAuthorities = (state,authorites) =>{
  state.officeAuthorities = authorites;
};

const resetRedux = (state, key) => {
    state[key] = null;
};

const setProcessing = (state, data) => {
    state.processing = data;
};

const setSocket = (state, socket) => {
    state.socket = socket;
    //sessionStorage.setItem('Socket', JSON.stringify(socket));
};


export default server_reducer;