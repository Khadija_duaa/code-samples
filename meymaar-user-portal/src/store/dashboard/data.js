 const data = {
     "office": [
          {
               "name": "DP&W",
               "offices": [
                    {
                         "name": "ACE",
                         "offices": [
                              {
                                   "name": "CMES1",
                                   "offices": [
                                        {
                                             "name": "GE"
                                        },
                                        {
                                             "name": "GE2"
                                        }
                                   ]
                              },
                              {
                                   "name": "CEMES2",
                                   "offices": [
                                        {
                                             "name": "GE3"
                                        },
                                        {
                                             "name": "GE4"
                                        }
                                   ]
                              }
                         ]
                    }
               ]
          }
     ]
}

export default data