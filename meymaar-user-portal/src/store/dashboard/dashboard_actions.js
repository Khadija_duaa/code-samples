import dashData from './data';
import * as actions from './dashboard_action_types'

const setLoading = () => {
     return {
          type: actions.GET_DATA,
          payload: {
               loading: true,
               error: false,
               data: null
          }
      }
};

// const setError = () => {
//      return {
//           type: actions.GET_DATA,
//           payload: {
//                loading: false,
//                error: true,
//                data: null
//           }
//       }
// };

const setData = (data) => {
     return {
          type: actions.GET_DATA,
          payload: {
               loading: false,
               error: false,
               data: data
          }
      }
};



const getDashboardData = () => {
     return (dispatch) => {
          dispatch(setLoading());
          dispatch(setData(dashData))
     }
};


export default {
     getDashboardData
}