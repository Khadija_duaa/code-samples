import * as actions from './dashboard_action_types';

const initState = () => {
     return {
          loading: false,
          error: false,
          data: false,
     }
};

const dashboard_reducer = (state = initState(), action) => {
     let newState = { ...state };
     switch (action.type) {
          case actions.GET_DATA:
               return {newState, ...action.payload};
          default:
               break;
     }

     return newState;
};

export default dashboard_reducer;