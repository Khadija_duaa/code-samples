import * as actions from './sob-action-types';

const initState = () => {
     return {
          sobData: null,
          rejected: null,
          pending: null,
          vetted: null
     }
};

const sob_reducer = (state = initState(), action) => {
     let newState = { ...state };
     switch (action.type) {
          case actions.GET_SOB_DATA:
               newState.sobData = action.payload.sobData;
               return newState;
          case actions.GET_REJECTED:
               newState.rejected = action.payload.rejected;
               return newState;
          case actions.GET_PENDING:
               newState.pending = action.payload.pending;
               return newState;
          case actions.GET_VETTED:
               newState.vetted = action.payload.vetted;
               return newState;
          default:
               break;
     }
     return newState;
};

export default sob_reducer;