import axios from "../../utils/axios";
import {message} from 'antd';
import * as actions from "./sob-action-types";
import {setProjectProcessing} from "../project/project-actions";
import {handleError} from "../store-utils";
import {NOTIFICATION_TIME} from "../../utils/common-utils";


message.config({
    top: 70,
    duration: NOTIFICATION_TIME,
    maxCount: 3
});

const getSobData = (projectId) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        const urlSob = `/site-visit-management/site-visits/office/getOfficeOpenChat/${projectId}`;
        axios.get(urlSob)
            .then(res => {

                let isViewByOffice = false;
                res.data.sob.forEach(sobObj => {
                    isViewByOffice = false;

                    sobObj.comments.forEach((comment, i) => {
                        if (comment.office === "true") {
                            isViewByOffice = true;
                            return;
                        }
                    });
                    sobObj.isViewedByOffice = isViewByOffice;

                });

                dispatch({
                    type: actions.GET_SOB_DATA,
                    payload: {
                        sobData: res.data.sob
                    }
                });


                dispatch(setProjectProcessing(false));
            })
            .catch(error => {


                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);

                dispatch(setProjectProcessing(false));
            });

    }
};

const getRejected = (projectId) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        const urlSob = `/site-visit-management/site-visits/office/getRequestRejectedChat/${projectId}`;
        axios.get(urlSob)
            .then(res => {

                let isViewByOffice = false;
                res.data.sob.forEach(sobObj => {
                    isViewByOffice = false;

                    sobObj.comments.forEach((comment, i) => {
                        if (comment.office === "true") {
                            isViewByOffice = true;
                            return;
                        }
                    });
                    sobObj.isViewedByOffice = isViewByOffice;

                });
                dispatch({
                    type: actions.GET_REJECTED,
                    payload: {
                        rejected: res.data.sob
                    }
                });

                dispatch(setProjectProcessing(false));
            })
            .catch(error => {


                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);

            });

    }
};

const getPending = (projectId) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));

        const urlSob = `/site-visit-management/site-visits/office/getRequestOpenChat/${projectId}`;
        axios.get(urlSob)
            .then(res => {
                dispatch(setProjectProcessing(false));

                let isViewByOffice = false;
                res.data.sob.forEach(sobObj => {
                    isViewByOffice = false;

                    sobObj.comments.forEach((comment, i) => {
                        if (comment.office === "true") {
                            isViewByOffice = true;
                            return;
                        }
                    });
                    sobObj.isViewedByOffice = isViewByOffice;

                });
                dispatch({
                    type: actions.GET_PENDING,
                    payload: {
                        pending: res.data.sob
                    }
                });

                dispatch(setProjectProcessing(false));

            })
            .catch(error => {

                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));
            });

    }
};


const getApproved = (projectId) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        const urlSob = `/site-visit-management/site-visits/office/getRequestApprovedChat/${projectId}`;
        axios.get(urlSob)
            .then(res => {
                let isViewByOffice = false;
                res.data.sob.forEach(sobObj => {
                    isViewByOffice = false;

                    sobObj.comments.forEach((comment, i) => {
                        if (comment.office === "true") {
                            isViewByOffice = true;
                            return;
                        }
                    });
                    sobObj.isViewedByOffice = isViewByOffice;

                });
                dispatch({
                    type: actions.GET_VETTED,
                    payload: {
                        vetted: res.data.sob
                    }
                });

                dispatch(setProjectProcessing(false));
            })
            .catch(error => {

                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));

            });

    }
};

export default {getApproved, getRejected, getSobData, getPending}