export const GET_PENDING = "PENDING";
export const GET_REJECTED = "REJECTED";
export const GET_VETTED = "VETTED";
export const GET_SOB_DATA = "SOB_DATA";