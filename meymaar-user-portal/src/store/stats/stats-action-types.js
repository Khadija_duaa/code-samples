export const SET_Projects_Stats = 'ACTION_SET_WorkLoad_Stats';
export const SET_FINANCIAL_STATS = 'ACTION_SET_FINANCIAL_STATS';
export const SET_CONTRACTORS_STATS = 'ACTION_SET_CONTRACTORS_STATS';
export const SET_EXECUTION_PROGRESS = 'ACTION_SET_EXECUTION_PROGRESS';
export const SET_DEVICES_STATS = 'ACTION_SET_DEVICES_STATS';