import * as actions from './stats-action-types';


export const setProjectsStats = (data) => {
    return {
        type: actions.SET_Projects_Stats,
        payload: data
    }
};

export const setFinancialStats = (data) => {
    return {
        type: actions.SET_FINANCIAL_STATS,
        payload: data
    }
};


export const setContractorsStats = (data) => {
    return {
        type: actions.SET_CONTRACTORS_STATS,
        payload: data
    }
};

export const setExecutionProgress = (data) => {
    return {
        type: actions.SET_EXECUTION_PROGRESS,
        payload: data
    }
};

export const setDevicesCount = (data) => {
    return {
        type: actions.SET_DEVICES_STATS,
        payload: data
    }
};

