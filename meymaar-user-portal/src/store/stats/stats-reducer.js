import * as actions from './stats-action-types';


const getProjectStats = () => {
    let projectStats = sessionStorage.getItem('project-Stats');

    if (projectStats) return JSON.parse(projectStats);
    return null;
};

const getFinancialStats = () => {
    let financialStats = sessionStorage.getItem('financial-Stats');

    if (financialStats) return JSON.parse(financialStats);
    return null;
};

const getContractorStats = () => {
    let contractorsStats = sessionStorage.getItem('contractors-Stats');

    if (contractorsStats) return JSON.parse(contractorsStats);
    return null;
};

const getExecutionStats = () => {
    let executionStats = sessionStorage.getItem('execution-Stats');
    if (executionStats) return JSON.parse(executionStats);

    return null;
};

const getDeviceStats = () => {
    let devicesStats = sessionStorage.getItem('device-Stats');
    if (devicesStats) return JSON.parse(devicesStats);
    return null;

};
const initState = () => {
    return {
        projectsStats: getProjectStats(),
        financialStats: getFinancialStats(),
        contractorStats: getContractorStats(),
        executionStats: getExecutionStats(),
        deviceStats: getDeviceStats()
    }
};


const stats_reducer = (state = initState(), action) => {
    let newState = {...state};
    switch (action.type) {

        case actions.SET_Projects_Stats:
            saveProjectsState(newState, action.payload);
            break;

        case actions.SET_FINANCIAL_STATS:
            saveFinancialStats(newState, action.payload);
            break;

        case actions.SET_CONTRACTORS_STATS:
            saveContractorsStats(newState, action.payload);
            break;

        case actions.SET_EXECUTION_PROGRESS:
            saveExecutionProgress(newState, action.payload);
            break;

        case actions.SET_DEVICES_STATS:
            saveDeviceStats(newState, action.payload);
            break;

        default:
            break;
    }
    return newState;
};

/************************ Statistics Functions *********************/
const saveProjectsState = (state, data) => {
    state.projectsStats = data;
    sessionStorage.setItem('project-Stats', JSON.stringify(data));
};

const saveFinancialStats = (state, data) => {
    state.financialStats = [...data];
    sessionStorage.setItem('financial-Stats', JSON.stringify(data));
};

const saveContractorsStats = (state, data) => {
    state.contractorStats = data;
    sessionStorage.setItem('contractors-Stats', JSON.stringify(data));
};

const saveExecutionProgress = (state, data) => {
    state.executionStats = data;
    sessionStorage.setItem('execution-Stats', JSON.stringify(data));
};

const saveDeviceStats = (state, data) => {
    state.deviceStats = data;
    sessionStorage.setItem('device-Stats',JSON.stringify(data));
};
/************************ END *********************/


export default stats_reducer;