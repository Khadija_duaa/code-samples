import * as actions from './billing_action_types';

const initState = () => {
     return {
          loading: false,
          error: false,
          data: false,
          paidApproved: "loading",
          allBills: "loading"
     }
};

const billing_reducer = (state = initState(), action) => {
     let newState = { ...state };
     switch (action.type) {
          case actions.GET_REQUESTED:
               return { newState, ...action.payload }
          case actions.GET_ALL_BILLS:
               return { newState, ...action.payload }
          case actions.GET_GENERATED:
               return { newState, ...action.payload }
          case actions.CREATE_BILL:
               return { newState, ...action.payload }
          case actions.APPROVE_BILL:
               return { newState, ...action.payload }
          case actions.REJECT_BILL:
               return { newState, ...action.payload }
          case actions.PAY_BILL:
               return { newState, ...action.payload }
          case actions.GET_PAID_APPROVED:
               return { newState, ...action.payload }
          default:
               break;
     }
     return newState;
};

export default billing_reducer;