import axios from "../../utils/axios";
import * as actions from "./billing_action_types";
import {handleError} from "../store-utils";
import {message} from 'antd';
import {NOTIFICATION_TIME} from "../../utils/common-utils";

message.config({
    top:70,
    duration:NOTIFICATION_TIME,
    maxCount:3
});

const getApprovedPaidBills = (projectId, cb) => {
    return dispatch => {
        dispatch({
            type: actions.GET_PAID_APPROVED,
            payload: {
                loading: true,
                error: false,
                paidApproved: "loading",
                allBills: "loading"
            }
        });

        axios.get(`/billing-management/all-billing-requests/${projectId}`)
            .then(res => {
                let {billingData} = res.data;
                dispatch({
                    type: actions.GET_PAID_APPROVED,
                    payload: {
                        loading: false,
                        error: false,
                        paidApproved:billingData,
                        allBills: "loading"
                    }
                });
                billingData && billingData.requestedBills.length && cb && cb();
            })
            .catch(error => {
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);

                dispatch({
                    type: actions.GET_PAID_APPROVED,
                    payload: {
                        loading: false,
                        error: true,
                        paidApproved: "error",
                        allBills: "loading"
                    }
                });
            });
    };
};

const getAllBills = (projectId) => {
    return dispatch => {
        dispatch({
            type: actions.GET_ALL_BILLS,
            payload: {
                loading: true,
                error: false,
                allBills: "loading",
                paidApproved: "loading"
            }
        });
        axios
            .get(`/billing-management/allBills/${projectId}`)
            .then(res => {
                if (res.data) {
                    dispatch({
                        type: actions.GET_ALL_BILLS,
                        payload: {
                            loading: false,
                            error: false,
                            allBills: res.data,
                            paidApproved: "loading"
                        }
                    });
                } else {
                    dispatch({
                        type: actions.GET_ALL_BILLS,
                        payload: {
                            loading: false,
                            error: true,
                            allBills: "error",
                            paidApproved: "loading"
                        }
                    });
                }
            })
            .catch(error => {
                dispatch({
                    type: actions.GET_ALL_BILLS,
                    payload: {
                        loading: false,
                        error: true,
                        allBills: "error",
                        paidApproved: "loading"
                    }
                });
            });
    };
};

const getRequestedbills = () => {
};

const getRejectedBills = () => {
};

const getGeneratedBills = () => {
};

const approveBill = () => {
};

const createBill = () => {
};

const rejectBill = () => {
};

const payBill = () => {
};

export default {
    getRequestedbills,
    getRejectedBills,
    getGeneratedBills,
    createBill,
    approveBill,
    rejectBill,
    payBill,
    getApprovedPaidBills,
    getAllBills
};
