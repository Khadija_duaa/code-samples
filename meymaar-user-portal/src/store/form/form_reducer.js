import * as actions from './form_action_types';

const initState = () => {
    return {
        jsonFields: null,
        previewJSON:{},
        processing:false
    }
};

const form_reducer = (state = initState(), action) => {
    let newState = {...state};
    switch (action.type) {
        case actions.SET_FORM:
            setJSONFields(newState,action.payload);
            break;

        case actions.SET_FORM_PROCESSING:
            setProcessing(newState,action.payload);
            break;

        case actions.SET_PREVIEW:
            newState.previewJSON = {...action.payload};
            break;
        default:
            break;
    }

    return newState;
};

const setJSONFields = (state,fields)=>{
    state.jsonFields = fields
};

const setProcessing = (state,data)=>{
  state.processing = data;
};
export default form_reducer;