import * as actions from './form_action_types';
import axios from '../../utils/axios';
import {SERVER_URL} from "../../utils/config";
import {message} from 'antd';
import 'antd/dist/antd.css';
import {handleError} from "../store-utils";
import {NOTIFICATION_TIME} from "../../utils/common-utils";

message.config({
    top:70,
    duration:NOTIFICATION_TIME,
    maxCount:3
});

const setFormInput = (data) => {
    return {
        type: actions.SET_FORM,
        payload: data
    }
};

const setFormPreview = (data) => {
    return {
        type: actions.SET_PREVIEW,
        payload: data
    }
};

const setProcessing = (data)=>{
    return{
        type:actions.SET_FORM_PROCESSING,
        payload:data
    }
};



// Get Form Fiels against  form URL
export const getFormInputs = (url="") => {
    return (dispatch, getState) => {

        dispatch(setProcessing(true));
        axios.get(url)
            .then(response => {
                dispatch(setFormInput(response.data.formData.form));
                dispatch(setProcessing(false));
            }).catch(error=>{
                let errorMessage = handleError(error);
                message.error(errorMessage,NOTIFICATION_TIME);
        });
    }
};


// Get Form Preview  on Preview Click
export const getFormPreview = () => {
    return (dispatch, getState) => {
        axios.get(`${SERVER_URL}/api/form/previewData`)
            .then(response => {
                dispatch(setFormPreview(response.data));
            }).catch(error=>{
            console.error(error);
        });
    }
};




// Submit FILE Data on submit url given by Fields API
export const sendFileData = (url,formData) => {
    return (dispatch, getState) => {
        axios.post(`${SERVER_URL}${url}`,formData)
            .then(response => {

            }).catch(error=>{
            console.error(error);
        });
    }
};

