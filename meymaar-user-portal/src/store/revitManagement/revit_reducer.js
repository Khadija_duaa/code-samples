import * as actions from './revit_action-types';




const initState = () => {
    return {
        message: null,
        error: null,
        fieldParams: [],
        materialParams:[],
        scheduleMaterialParams:[],
        revitSetting:[],
        bqCategories:[],
        updatedRevitSetting:null,
        processing: false,
        recordsPerPage: 320,
        currentPage: 1,
        activeParams: [],
        initialPaginatedData:[],
        fieldParamsSubset:[],
    };
};



const revitSetting = (state = initState(), action) => {
    let newState = {...state};

    switch (action.type) {

        case actions.FIELD_PARAMETERS:
            setFieldParameters(newState, action.payload);
            break;
        case actions.MATERIAL_CATEGORIES:
            setMaterialCategory(newState, action.payload);
            break;
        case actions.SCHEDULE_MATERIAL_CATEGORIES:
            setScheduleMaterialCategory(newState, action.payload);
            break;
        case actions.REVIT_SETTINGS:
            setRevitSettings(newState, action.payload);
            break;
        case actions.DELETE_REVIT_SETTING:
            deleteRevitSettings(newState, action.payload);
            break;
        case actions.ADD_REVIT_SETTING:
            addSetting(newState, action.payload);
            break;
        case actions.SET_REVIT_SETTING:
            setRevitSetting(newState, action.payload);
            break;
        case actions.SET_BQ_CATEGORIES:
            setBqCategories(newState, action.payload);
            break;
        case actions.PROCESSING_REVIT:
            setProcessing(newState, action.payload);
            break;
        case actions.ADD_FIELD_PARAMETER_SUBSET:
            addFieldParameter(newState, action.payload);
            break;
        case actions.FIELD_PARAMETERS_SUBSET:
            setFieldParametersSubset(newState, action.payload);
            break;
        case actions.ADD_BQ_CATEGORY:
            addBqCategory(newState, action.payload);
            break;
        case actions.DELETE_BQ_CATEGORY:
            deleteBqCategory(newState, action.payload);
            break;
        default :
            break;
    }

    return newState;
};


const setFieldParameters = (state, data) => {


    state.fieldParams = data;

};

const setFieldParametersSubset = (state, data) => {


    state.fieldParamsSubset = data;


};

const setMaterialCategory = (state, data) => {

    state.materialParams = data;

};

const setScheduleMaterialCategory = (state, data) => {

    state.scheduleMaterialParams = data;

};


const setRevitSettings = (state,data) => {
    state.revitSetting = data;
};

const setBqCategories = (state,data) => {

    state.bqCategories = data;
};

const setProcessing = (state, processing) => {
    state.processing = processing;
};

const setRevitSetting = (state, data) => {
    state.updatedRevitSetting = data;
};


const deleteRevitSettings = (state,data) => {
    state.message = data.message;
    state.error = data.error;
    if(state.message ) {
        let newSettings = [...state.revitSetting];
        newSettings.splice(data.id, 1);
        state.revitSetting = newSettings;
    }
};

const deleteBqCategory = (state,data) => {
    state.message = data.message;
    state.error = data.error;
    if(state.message ) {
        let newBqCategories = [...state.bqCategories];
        newBqCategories.splice(data.id, 1);
        state.bqCategories = newBqCategories;
    }
};

const addSetting = (state,data) => {
    state.message = data.message;
};


const addFieldParameter = (state,data) => {
    state.message = data.message;
};

const addBqCategory = (state,data) => {
    state.message = data.message;
};





export default revitSetting;