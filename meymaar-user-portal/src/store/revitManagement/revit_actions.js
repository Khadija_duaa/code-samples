import axios from "../../utils/axios";
import * as actions from './revit_action-types'


const setFieldParameters = (fieldParams) => {
    return {
        type: actions.FIELD_PARAMETERS,
        payload: fieldParams,

    }
};


const setFieldParametersSubset = (fieldParams) => {
    return {
        type: actions.FIELD_PARAMETERS_SUBSET,
        payload: fieldParams,

    }
};

const setMaterialCategory = (Params) => {
    return {
        type: actions.MATERIAL_CATEGORIES,
        payload: Params,

    }
};

const addBqCategory = (Params) => {
    return {
        type: actions.ADD_BQ_CATEGORY,
        payload: Params,

    }
};


const setRevitSettings = (settings) => {
    return {
        type: actions.REVIT_SETTINGS,
        payload: settings,

    }
};

const setScheduleMaterialCategory = (Params) => {
    return {
        type: actions.SCHEDULE_MATERIAL_CATEGORIES,
        payload: Params,

    }
};

const deleteRevitSettings = (setting) => {
    return {
        type: actions.DELETE_REVIT_SETTING,
        payload: setting,

    }
};


const deleteBqCategory = (setting) => {
    return {
        type: actions.DELETE_BQ_CATEGORY,
        payload: setting,

    }
};


const addSetting = (setting) => {
    return {
        type: actions.ADD_REVIT_SETTING,
        payload: setting,

    }
};

export const setRevitSetting = (setting) => {
    return {
        type: actions.SET_REVIT_SETTING,
        payload: setting,

    }
};

export const setBqCategories = (bqCategories) => {

    return {
        type: actions.SET_BQ_CATEGORIES,
        payload: bqCategories,

    }
};

const setProcessing = (processing) => {
    return {
        type: actions.PROCESSING_REVIT,
        payload: processing
    };
};


export const addFieldParameter = (payload) => {
    return {
        type: actions.ADD_FIELD_PARAMETER_SUBSET,
        payload
    };
};


export const loadFieldParameters = () => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.get(`/revit-settings/settings/get-field-parameters`)
            .then((response) => {


                const data = response.data.data.sort((a, b) => a.key.localeCompare(b.key));
                dispatch(setFieldParameters(data));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setFieldParameters({message: err.response && err.response.data && err.response.data.message}));
                dispatch(setProcessing(false));
            });
    };
};

export const loadFieldParametersSubset = () => {
    return (dispatch,) => {
        dispatch(setProcessing(true));

        axios.get(`/revit-settings/settings/get-field-parameters-subset`)
            .then((response) => {


                const data = response.data.subset;
                dispatch(setFieldParametersSubset(data));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setFieldParametersSubset({message: err.response && err.response.data && err.response.data.message}));
                dispatch(setProcessing(false));
            });
    };
};


export const loadBqCategories = () => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.get(`/bq-management/getBqCategories`)
            .then((response) => {
                const data = response.data.categories;
                dispatch(setBqCategories(data));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setBqCategories({message: err.response && err.response.data && err.response.data.message}));
                dispatch(setProcessing(false));
            });
    };
};


export const loadTakeoffMaterialCategories = () => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(`/revit-settings/settings/get-takeoff-material-category`)
            .then((response) => {
                const data = response.data.data.sort((a, b) => a.key.localeCompare(b.key));
                dispatch(setMaterialCategory(data));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setProcessing(false));
                dispatch(setMaterialCategory({message: err.response && err.response.data && err.response.data.message}));
            });
    };
};

export const loadScheduleMaterialCategories = () => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        axios.get(`/revit-settings/settings/get-schedule-material-category`)
            .then((response) => {
                const data = response.data.data.sort((a, b) => a.key.localeCompare(b.key));
                dispatch(setScheduleMaterialCategory(data));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setProcessing(false));
                dispatch(setScheduleMaterialCategory({message: err.response && err.response.data && err.response.data.message}));
            });
    };
};

export const loadRevitSettings = (isMeymaarRevit = false) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        let apiURL = `/revit-settings/settings/${isMeymaarRevit ? 'get-settings' : 'get-revit-settings'}`;
        axios.get(apiURL)
            .then((response) => {

                const {data} = response.data;


                dispatch(setRevitSettings(data));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setRevitSettings({message: err.response && err.response.data && err.response.data.message}));
                dispatch(setProcessing(false));
            });
    };
};

export const deleteRevitSetting = (id, cb, callBack, errorCallBack) => {
    return (dispatch, getState) => {


        axios.delete(`/revit-settings/settings/delete-settings/${id}`)
            .then((response) => {

                dispatch(deleteRevitSettings({id: id, message: response.data.message}));
                cb && cb();
                callBack && callBack();


            })
            .catch(err => {
                dispatch(deleteRevitSettings({error: err.response && err.response.data && err.response.data.message}));
                errorCallBack && errorCallBack();
            });
    };
};

export const saveRevitSetting = (data, isMeymaarRevit = false, callBack, cb, errorCallBack) => {

    return (dispatch, getState) => {
        let url = `/revit-settings/settings/save-settings`;
        let urlUpdate = `/revit-settings/settings/${isMeymaarRevit?'update-settings':'update-revit-settings'}`;

        dispatch(setProcessing(true));
        let axiosPromise = null;

        let existingId = getState().revit_setting.updatedRevitSetting ? getState().revit_setting.updatedRevitSetting._id : null;

        if (existingId) {
            urlUpdate += `/${existingId}`;
            axiosPromise = axios.put(urlUpdate, data);
        }
        else {

            axiosPromise = axios.post(url, data);
        }

        axiosPromise.then((response) => {
            dispatch(setProcessing(false));

            dispatch(addSetting({message: response.data && response.data.message}));
            callBack && callBack();
            cb && cb();

        }).catch(err => {
            dispatch(setProcessing(false));
            dispatch(addSetting({message: err.response && err.response.data && err.response.data.message}));
            errorCallBack && errorCallBack()
        });
    };
};

export const saveFieldParameter = (value, cb, callBack, errorCallBack) => {


    let data = {fieldParameters: value};

    return (dispatch) => {
        let url = `/revit-settings/settings/save-field-parameters-subset`;


        let axiosPromise = null;


        axiosPromise = axios.post(url, data);


        axiosPromise.then((response) => {


            dispatch(addFieldParameter({message: response.data && response.data.message}));

            cb && cb();

            callBack && callBack();


        }).catch(err => {

            dispatch(addFieldParameter({message: err.response && err.response.data && err.response.data.message}));
            errorCallBack && errorCallBack()
        });
    };
};


export const saveBqCategory = (value, cb, errorCallBack, callBack) => {


    return (dispatch) => {

        let url = `/bq-management/updateBqCategory`;


        let axiosPromise = null;


        axiosPromise = axios.post(url, value);


        axiosPromise.then((response) => {


            dispatch(addBqCategory({message: response.data && response.data.message}));

            cb && cb();
            callBack && callBack();


        }).catch(err => {

            dispatch(addBqCategory({message: err.response && err.response.data && err.response.data.message}));
            errorCallBack && errorCallBack()
        });
    };
};

export const deleteBqCategories = (id, cb, callBack, errorCallBack) => {
    return (dispatch, getState) => {


        axios.delete(`/bq-management/removeBqCategory/${id}`)
            .then((response) => {

                dispatch(deleteBqCategory({id: id, message: response.data.message}));
                cb && cb();
                callBack && callBack();


            })
            .catch(err => {

                dispatch(deleteBqCategory({error: err.response && err.response.data && err.response.data.message}));
                errorCallBack && errorCallBack();
            });
    };
};



