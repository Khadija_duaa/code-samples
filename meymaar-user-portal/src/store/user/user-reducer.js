import * as actions from './user-action-types';

const loadAuthStateFromLocalStorage = () => {
  let authState = sessionStorage.getItem('authState');

  if (authState && authState.length > 0) {
    return JSON.parse(authState);
  }

  return null;
};

const _initState = {
  activeUser: null,
  authenticated: false,
  processing: false,
  notification: null,
  securityQuestions: []
};

const initState = () => {
  const prevState = loadAuthStateFromLocalStorage();
  if (!prevState) {
    return { ..._initState };
  }
  return prevState;
};

const user_reducer = (state = initState(), action) => {
  let newState = { ...state };
  switch (action.type) {
    case actions.SET_ACTIVE_USER:
      login(newState, action.payload);
      break;

    case actions.RESET_NOTIFICATIONS:
      resetNotification(newState);
      break;
    case actions.SHOW_NOTIFICATION:
      showNotification(newState, action.payload);
      break;
    case actions.LOGOUT_USER:
      logout(newState);
      break;
    case actions.GET_SECURITY_QUESTIONS:
      setSecurityQuestions(newState, action.payload);
      break;
    case actions.SET_PROCESSING:
      setProcessing(newState, action.payload);
      break;
    case actions.ADD_SECURITY_QUESTIONS:
      addSecurityQuestions(newState, action.payload);
      break;
    default:
      break;
  }

  return newState;
};

const login = (state, data) => {
  state.activeUser = data;
  state.authenticated = true;
  state.userNotifications = [];

  sessionStorage.setItem('authState', JSON.stringify(state));
};

const resetNotification = state => {
  state.notification = null;
};
const showNotification = (state, data) => {
  state.notification = data;
};

const setProcessing = (state, processing) => {
  state.processing = processing;
};

const setSecurityQuestions = (state, data) => {
  state.securityQuestions = data;
};

const addSecurityQuestions = (state, data) => {
  state.message = data.message;
  state.activeUser.principal.firstTime = data.firstTime;
  state.activeUser.principal.answers = data.answers;
  sessionStorage.setItem('authState', JSON.stringify(state));
};

const logout = state => {
  state.activeUser = {};
  state.authenticated = false;
  state.processing = false;
  state.notification = null;
  state.userNotifications = null;

  let removeKeys = [
    'userNotification',
    'authState',
    'phases',
    'diary-state',
    'domains',
    'Socket',
    'execution-category',
    'work-category',
    'overview-projectId',
    'project-Stats',
    'financial-Stats',
    'contractors-Stats',
    'device-Stats',
    'execution-Stats'
  ];

  removeKeys.forEach(key => {
    sessionStorage.removeItem(key);
  });
};

export default user_reducer;
