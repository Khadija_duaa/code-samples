export const SET_ACTIVE_USER = 'ACTION_SET_ACTIVE_USER';
export const LOGOUT_USER = 'ACTION_LOGOUT_USER';
export const SET_PROCESSING = 'ACTION_AUTH_PROCESSING';
export const SET_NOTIFICATIONS = 'ACTION_SET_NOTIFICATIONS';
export const CREATE_ADMIN = 'ACTION_CREATE_ADMIN';
export const GET_SECURITY_QUESTIONS = 'GET_SECURITY_QUESTIONS';
export const ADD_SECURITY_QUESTIONS = 'ADD_SECURITY_QUESTIONS';
export const SHOW_NOTIFICATION = 'ACTION_SET_NOTIFICATION';
export const RESET_NOTIFICATIONS = 'ACTION_RESET_NOTIFICATIONS';
