import * as actions from './user-action-types';
import axios from '../../utils/axios';
import {handleError} from "../store-utils";
import {fetchAllDomains, fetchAllPhases, fetchUserRecentNotifications} from "../../utils/server-utils";
import {message} from 'antd';
import 'antd/dist/antd.css';
import {resetPhases, saveAllPhases} from "../project/project-actions";
import {subscribeUser} from "../../socket-io/socket-utils";
import {hasAuthority, NOTIFICATION_TIME} from "../../utils/common-utils";
import {loadFieldParameters} from "../revitManagement/revit_actions";
import {
    fetchContractorStats, fetchDevicesStats,
    fetchExecutionStats,
    fetchFinancialStats,
    fetchProjectsStats
} from "../../utils/stats-server-utils";

import React from "react";

const setActiveUser = (data) => {
    return {
        type: actions.SET_ACTIVE_USER,
        payload: data
    }
};

const setSecurityQuestions = (data) => {
    return {
        type: actions.GET_SECURITY_QUESTIONS,
        payload: data
    }
};



export const setNotification = data => {
    return {
        type: actions.SHOW_NOTIFICATION,
        payload: data
    }
};

export const resetNotification = () => {
    return {
        type: actions.RESET_NOTIFICATIONS
    }
};

export const setProcessing = (processing) => {
    return {
        type: actions.SET_PROCESSING,
        payload: processing
    }
};

export const logout = () => {
    return {
        type: actions.LOGOUT_USER,
    }
};

const addSecurityQuestions = (question) => {
    return {
        type: actions.ADD_SECURITY_QUESTIONS,
        payload: question,

    }
};


export const logoutUser = (cb) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        setTimeout(() => {
            dispatch(setProcessing(false));
            dispatch(logout());
            cb && cb();
        }, 1000);
    }
};


export const loginUser = (loginData, errorCB) => {

    return (dispatch, getState) => {
        dispatch(setProcessing(true));

        axios.post('/authorization-manager/login', loginData)
            .then(response => {

                setTimeout(() => {
                    const {data} = response;

                    console.log(data);

                    let {socket} = getState().server_reducer;

                    if (socket) {
                        subscribeUser(data);
                    }


                    dispatch(setProcessing(false));
                    dispatch(resetPhases());
                    dispatch(setActiveUser(data));

                    fetchAllDomains();
                    fetchAllPhases((phases)=>{
                        dispatch(saveAllPhases(phases));
                    });

                    fetchUserRecentNotifications(data.principal_id);
                    if (hasAuthority('CAN_MANAGE_REVIT')) {
                        loadFieldParameters();
                    }

                    fetchProjectsStats();
                    fetchFinancialStats();
                    fetchContractorStats();
                    fetchExecutionStats();
                    fetchDevicesStats();


                }, 1000);
            })
            .catch(err => {
                dispatch(setProcessing(false));
                const errorMessage = handleError(err);
                //message.error(errorMessage, NOTIFICATION_TIME);

                errorCB && errorCB(errorMessage);
            })
    }
};


export const loadSecurityQuestions = () => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.get(`/user-management/questions/all`)
            .then((response) => {
                const {data} = response && response;
                dispatch(setSecurityQuestions(data.questions));

                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setProcessing(false));
            });
    };
};

export const saveSecurityQuestions = ( data) => {
    return (dispatch, getState) => {
        let url ="";
        let isCreate = true;
        if (getState().user_reducer.activeUser.principal.answers.length) {
            url = `/user-management/users/update-answers`;
            isCreate = false;
        }
        else {
            url = `/user-management/users/set-security-questions`
        }

        dispatch(setProcessing(true));



        let existingUserName = getState().user_reducer.activeUser.principal.username;

        url  += `/${existingUserName}`;


       let axiosPromise = axios.post(url, data);

        axiosPromise.then((response) => {
             const {data} = response;
             let successMessage = `Security Questions has ${isCreate?'saved':'updated'} successfully`;
            dispatch(setProcessing(false));

            dispatch(addSecurityQuestions({firstTime:data.firstTime,  answers:data.answers}));
            message.success(successMessage,NOTIFICATION_TIME);
            if(isCreate){
                window.location.reload();
            }
        }).catch(err => {
            dispatch(setProcessing(false));
            let errorMessaage = handleError(err);
            message.error(errorMessaage,NOTIFICATION_TIME);
        });
    };
};

