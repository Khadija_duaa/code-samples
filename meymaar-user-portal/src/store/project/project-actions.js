import {message} from "antd";
import "antd/dist/antd.css";

import * as actions from "./project-action-types";

import axios from "../../utils/axios";
import {
    downloadJSONFile,
    formatDate,
    getPhaseName,
    NOTIFICATION_TIME
} from "../../utils/common-utils";
import {handleError} from "../store-utils";
import {setProcessing} from "../server/server-actions";

message.config({
    top: 70,
    duration: NOTIFICATION_TIME,
    maxCount: 3
});

export const setBQFiles = data => {
    return {
        type: actions.SET_BQ_FILES_DATA,
        payload: data
    };
};

export const resetRedux = key => {
    return {
        type: actions.RESET_REDUX,
        payload: key
    };
};

export const resetPhases = () => {
    return {
        type: actions.RESET_PHASES
    };
};

const setAllWorks = data => {
    return {
        type: actions.SET_WORKS_LIST,
        payload: data
    };
};

const setAllTasks = data => {
    return {
        type: actions.SET_TASKS_LIST,
        payload: data
    };
};

const setProjectDetails = data => {
    return {
        type: actions.SET_PROJECT_DETAILS,
        payload: data
    };
};

const setPhaseDetails = data => {
    return {
        type: actions.SET_PHASE_DETAILS,
        payload: data
    };
};

export const saveAllPhases = data => {
    return {
        type: actions.SET_PHASES,
        payload: data
    };
};

export const setEditRights = data => {
    return {
        type: actions.SET_EDIT_RIGHTS,
        payload: data
    };
};

const setAllExecutionProjects = data => {
    return {
        type: actions.SET_EXECUTION_PROJECTS,
        payload: data
    };
};

const setDrawingData = data => {
    return {
        type: actions.SET_DRAWING_DETAILS,
        payload: data
    };
};

export const setProjectProcessing = data => {
    return {
        type: actions.SET_PROJECT_PROCESSING,
        payload: data
    };
};

export const setPhaseGroup = group => {
    return {
        type: actions.SET_PHASE_GROUP,
        payload: group
    };
};

/******************************** PROJECT Actions *******************************/

export const fetchAllWorks = (
    sortOrder,
    projectType,
    phaseId,
    pageNumber,
    isAssociated,
    cb
) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        dispatch(resetRedux("worksList"));
        dispatch(resetRedux("workPagination"));

        let url = `/project-marking/project-marking/myOfficeProjects/${projectType}/${phaseId}/${pageNumber}`;

        if (isAssociated) {
            url = `/project-marking/project-marking/myOfficeAssocProjects/${projectType}/${phaseId}/${pageNumber}`;
        }

        axios
            .post(url, sortOrder)
            .then(response => {
                let worksData = response.data.response;

                if (Object.keys(worksData).length || worksData.length) {
                    worksData.projects.forEach(work => {
                        let phase = getPhaseName(work.phaseId);
                        work.phaseName = phase ? phase : "N/A";
                        work.initOffice = work.office.name;
                    });
                    dispatch(setAllWorks(worksData));
                }

                dispatch(setProjectProcessing(false));

                cb && cb();
            })
            .catch(err => {
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);

                dispatch(setProjectProcessing(false));
            });
    };
};

export const fetchAllTasks = (
    sortOrder,
    projectType,
    phaseId,
    pageNumber,
    isInbox = false,
    cb
) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));

        dispatch(resetRedux("taskBoardList"));
        dispatch(resetRedux("taskPagination"));

        let _url = `/project-marking/project-marking/myProjects/${projectType}/${phaseId}/${pageNumber}`;

        if (isInbox) {
            _url = `/project-marking/project-marking/inbox/${phaseId}/${pageNumber}`;
        }

        axios
            .post(_url, sortOrder)
            .then(response => {
                let tasksData = response.data.response;

                if (Object.keys(tasksData).length || tasksData.length) {
                    tasksData.projects.forEach(task => {
                        let {marking} = task;
                        let phase = getPhaseName(task.phaseId);
                        task.phaseName = phase ? phase : "N/A";
                        task.markReason = marking.reason ? marking.reason : "N/A";
                        task.markedByName = marking.marked_by_name;
                        task.initDate = marking.initiated_date
                            ? formatDate(marking.initiated_date, "DD-MM-YYYY")
                            : "N/A";
                    });
                    dispatch(setAllTasks(tasksData));
                }

                cb && cb();
                dispatch(setProjectProcessing(false));
            })
            .catch(err => {
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));
            });
    };
};

export const deletePrivateMessage = (taskId, cb) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .put(`/project-marking/project-marking/deletePrivateComment/${taskId}`)
            .then(() => {
                dispatch(setProjectProcessing(false));
                cb && cb();
            })
            .catch(err => {
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));
            });
    };
};

export const fetchProjectDetails = (
    projectId,
    fields,
    markingHistory = false,
    cb
) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));

        axios
            .post(`/projects-cache/fetch-project-data/${projectId}`, fields)
            .then(response => {
                let {project} = response.data;

                project.projectId = projectId;

                !markingHistory && dispatch(setProjectDetails(project));

                dispatch(setProjectProcessing(false));

                if (markingHistory) {
                    cb && cb(project);
                } else {
                    cb && cb();
                }
            })
            .catch(err => {
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));
            });
    };
};

export const fetchPhaseDetails = (projectId, phaseId, cb, errorCB) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        dispatch(resetRedux("sanctionDetails"));

        // Added timeout to have cache refresh the project
        setTimeout(() => {
            axios
                .get(`/project-management/projects/${projectId}/phase/${phaseId}`)
                .then(response => {
                    let {forms} = response.data;
                    let isPhaseVisited = false;

                    forms.forEach(form => {
                        isPhaseVisited = isPhaseVisited || form.isVisited;
                    });

                    dispatch(setPhaseDetails(response.data));
                    dispatch(setProjectProcessing(false));
                    cb && cb(isPhaseVisited);
                })
                .catch(err => {
                    dispatch(setProjectProcessing(false));
                    let errorMessages = handleError(err);
                    message.error(errorMessages, NOTIFICATION_TIME);

                    errorCB && errorCB();
                });
        }, 500);
    };
};

// Submit Form Data on submit url given by Fields API
export const createProject = (url, formData, successMessage, cb) => {
    console.log(formData);
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(url, formData)
            .then(response => {
                message.success(successMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));

                cb && cb(response.data.project.uuid);
            })
            .catch(error => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

// Submit Form Data on submit url given by Fields API
export const submitFormData = (
    projectId,
    phaseId,
    form,
    formData,
    successMessage,
    cb
) => {
    let _url = form.submitURL.replace("{PROJECT_ID}", projectId);

    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .post(_url, formData)
            .then(() => {
                dispatch(
                    fetchPhaseDetails(projectId, phaseId, () => {
                        dispatch(setProcessing(false));
                        cb && cb();
                        message.success(successMessage, NOTIFICATION_TIME);
                    })
                );
            })
            .catch(error => {
                dispatch(setProcessing(false));
                let errorMessage = handleError(error);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

export const updateFormAsVisited = (formName, projectDetails) => {
    return (dispatch, getState) => {
        let phaseDetails = getState().project_reducer.phaseDetails;
        let isCompleted = false;
        phaseDetails.forms.forEach(data => {
            if (data.form.formName === formName) {
                data.isVisited = true;
                isCompleted = data.complete;
            }
        });

        if (isCompleted) {
            dispatch(
                fetchPhaseDetails(projectDetails.projectId, projectDetails.phaseId)
            );
        } else {
            dispatch(setPhaseDetails(phaseDetails));
        }
    };
};

/******************************** END *******************************/

/******************************** BOQ File Actions *******************************/

// POST API to UPLOAD new BQ File
export const uploadBQFile = (fileData, projectId, phaseId, cb, errorCB) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(`/bq-management/uploadBq/${projectId}`, fileData)
            .then(() => {
                message.success("BQ File has uploaded successfully", NOTIFICATION_TIME);

                dispatch(
                    fetchPhaseDetails(projectId, phaseId, () => {
                        dispatch(setProjectProcessing(false));
                        cb && cb();
                    })
                );
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);

                if (
                    err &&
                    err.response &&
                    err.response.status &&
                    err.response.status < 500
                ) {
                    errorCB && errorCB(errorMessage);
                } else {
                    cb && cb();
                    message.error(errorMessage, NOTIFICATION_TIME);
                }
            });
    };
};

//POST API to send EDITED BQ Data
export const submitEditedBQData = (
    projectId,
    phaseId,
    officeId,
    bqId,
    data,
    cb
) => {
    console.log("Edited Bq's data", data);
    return dispatch => {
        dispatch(setProjectProcessing(true));

        axios
            .put(
                `/bq-management/update-bqs-item/${projectId}/bq/${bqId}/office/${officeId}`,
                data
            )
            .then(() => {
                dispatch(setProjectProcessing(false));
                message.success("BQ Data has updated successfully", NOTIFICATION_TIME);

                dispatch(
                    fetchPhaseDetails(projectId, phaseId, () => {
                        cb && cb();
                    })
                );
            })
            .catch(err => {
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);

                dispatch(setProjectProcessing(false));
            });
    };
};

// DELETE API to delete bq file
export const deleteBQFile = (projectId, phaseId, bqId, cb) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .delete(`/bq-management/bqs/${projectId}/${bqId} `)
            .then(response => {
                dispatch(setProjectProcessing(false));

                message.success("BQ File has deleted successfully", NOTIFICATION_TIME);

                let {bqs} = response.data;

                dispatch(setBQFiles(bqs));

                dispatch(
                    fetchPhaseDetails(projectId, phaseId, () => {
                        cb && cb();
                    })
                );
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

export const submitSORPercentage = (projectId, phaseId, data, cb) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(`/fields-management/project-data/${projectId}`, data)
            .then(() => {
                dispatch(setProjectProcessing(false));

                message.success(
                    "SOR percentage has saved successfully",
                    NOTIFICATION_TIME
                );
                dispatch(
                    fetchPhaseDetails(projectId, phaseId, () => {
                        cb && cb();
                    })
                );
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

export const submitUpdatedBQSchedule = (projectId, phaseId, data, cb) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(`/bq-management/updateBqs/${projectId}`, data)
            .then(() => {
                dispatch(setProjectProcessing(false));

                message.success(
                    "Schedule has submitted successfully",
                    NOTIFICATION_TIME
                );
                dispatch(fetchPhaseDetails(projectId, phaseId, cb));
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

/******************************** END *******************************/

/******************************** Document Actions *******************************/

export const createNewDocument = (projectId, phaseId, fileData, title, cb) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(`/project-file-management/office/upload/${projectId}`, fileData)
            .then(response => {
                let {file} = response.data;

                dispatch(sendDocumentData(projectId, phaseId, title, file, cb));
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

const sendDocumentData = (projectId, phaseId, formTitle, fileResponse, cb) => {
    return dispatch => {
        axios
            .post(
                `/project-management/projects/${projectId}/at/${phaseId}/as/${formTitle}`,
                fileResponse
            )
            .then(() => {
                dispatch(
                    fetchPhaseDetails(projectId, phaseId, () => {
                        cb && cb();
                    })
                );
            })
            .catch(err => {
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

export const deleteAdditionalFile = (projectId, phaseId, fileId, cb) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .delete(`/project-management/projects/file/${fileId}`)
            .then(() => {
                dispatch(setProjectProcessing(false));
                cb && cb();
                console.log(projectId, phaseId);

                dispatch(fetchPhaseDetails(projectId, phaseId));
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

export const submitFieldManagementData = (
    projectId,
    phaseId,
    data,
    submitURL,
    isDelete = false,
    cb
) => {
    let _url = submitURL.replace("{PROJECT_ID}", projectId);

    if (isDelete) {
        _url = `${_url}?sync=true`;
    }

    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(_url, data)
            .then(() => {
                dispatch(fetchPhaseDetails(projectId, phaseId, cb));
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

export const deletePreviewFile = (projectId, fileId, cb) => {
    return () => {
        axios
            .delete(`/project-file-management/office/delete/${projectId}/${fileId}`)
            .then(() => {
                cb && cb();
            })
            .catch(err => {
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

/******************************** END *******************************/

/******************************** Marking & Sanction Actions *******************************/

export const markProject = (projectId, phaseId, markingData, cb, errorCB) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .post(
                `/project-marking/project-marking/transfer/${projectId}`,
                markingData
            )
            .then(response => {
                dispatch(setProcessing(false));

                cb && cb(response.data.markingMessage);
            })
            .catch(err => {
                dispatch(setProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
                errorCB && errorCB(errorMessage);
            });
    };
};

export const assignSanction = (projectId, phaseId, data, cb, errorCB) => {
    return dispatch => {
        dispatch(setProcessing(true));
        axios
            .post(`/project-management/sanctions/${projectId}/${phaseId}`, data)
            .then(response => {
                console.log(response.data);
                if (response.data.downloadData) {
                    let downloadData = response.data.downloadData;
                    downloadJSONFile(downloadData, downloadData.name);
                }

                dispatch(setProcessing(false));

                let {data} = response;
                if (data.sanction && data.sanction.markingMessage) {
                    cb && cb(response.data.sanction.markingMessage);
                } else {
                    cb && cb("");
                }
            })
            .catch(err => {
                dispatch(setProcessing(false));
                let errorMessage = handleError(err);

                if (
                    err &&
                    err.response &&
                    err.response.status &&
                    err.response.status < 500
                ) {
                    errorCB && errorCB(errorMessage);
                }
            });
    };
};

export const revokeSanction = (projectId, phaseId, data, cb, errorCB) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(
                `/project-management/sanctions/revoke/${projectId}/${phaseId}`,
                data
            )
            .then(() => {
                dispatch(setProjectProcessing(false));
                cb && cb("");
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);

                if (
                    err &&
                    err.response &&
                    err.response.status &&
                    err.response.status < 500
                ) {
                    errorCB && errorCB(errorMessage);
                }
            });
    };
};

/******************************** END *******************************/

/******************************** Drawing Sheet Actions *******************************/

//Submit File
export const submitFiles = (
    projectId,
    phaseId,
    keyName,
    fileData,
    fileName,
    submitURL,
    cb
) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(`/project-file-management/office/upload/${projectId}`, fileData)
            .then(response => {
                console.log(response.data);

                response.data.file.fileName = fileName;

                let data = {
                    [keyName]: response.data.file
                };

                dispatch(
                    submitFieldManagementData(
                        projectId,
                        phaseId,
                        data,
                        submitURL,
                        false,
                        cb
                    )
                );
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

//Submit Drawing Sheet File
export const submitDrawingSheet = (
    projectId,
    phaseId,
    activeDrawing,
    sheetData,
    fileData,
    cb
) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));

        axios
            .post(`/project-file-management/office/upload/${projectId}`, fileData)
            .then(response => {
                dispatch(setProjectProcessing(false));
                sheetData.file = response.data.file;

                if (activeDrawing) {
                    dispatch(
                        updateDrawingSheet(
                            projectId,
                            phaseId,
                            activeDrawing.id,
                            sheetData,
                            cb
                        )
                    );
                } else {
                    dispatch(submitDrawingSheetData(projectId, phaseId, sheetData, cb));
                }
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));

                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

export const updateDrawingSheet = (
    projectId,
    phaseId,
    drawingId,
    sheetData,
    cb
) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .put(
                `/drawings-management/projects/${projectId}/drawings/${drawingId}`,
                sheetData
            )
            .then(() => {
                dispatch(setProjectProcessing(false));
                message.success(
                    "Drawing sheet has updated succesfully",
                    NOTIFICATION_TIME
                );

                dispatch(fetchPhaseDetails(projectId, phaseId));
                dispatch(fetchAllDrawingSheets(projectId, cb));
            })
            .catch(err => {
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));
            });
    };
};
//Submitting Drawing Sheet Data
export const submitDrawingSheetData = (projectId, phaseId, sheetData, cb) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(
                `/drawings-management/projects/assignDrawing/${projectId}`,
                sheetData
            )
            .then(() => {
                dispatch(setProjectProcessing(false));
                message.success(
                    "Drawing sheet has submitted succesfully",
                    NOTIFICATION_TIME
                );

                dispatch(fetchPhaseDetails(projectId, phaseId));
                dispatch(fetchAllDrawingSheets(projectId, cb));
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

//Fetching All Drawing Sheet Data
export const fetchAllDrawingSheets = (projectId, cb) => {
    return dispatch => {
        axios
            .get(`/drawings-management/projects/drawings/${projectId}`)
            .then(response => {
                dispatch(setDrawingData(response.data.drawings));
                cb && cb();
            })
            .catch(err => {
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
                cb && cb();
            });
    };
};

//Delete Drawing Sheet Data
export const deleteDrawingSheet = (projectId, phaseId, drawingNumber, cb) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .delete(
                `/drawings-management/projects/${projectId}/drawings/${drawingNumber}`
            )
            .then(() => {
                cb && cb();

                dispatch(setProjectProcessing(false));
                message.success(
                    "Drawing sheet has deleted succesfully",
                    NOTIFICATION_TIME
                );
                dispatch(fetchPhaseDetails(projectId, phaseId));
                dispatch(fetchAllDrawingSheets(projectId));
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));

                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};
/******************************** END *******************************/

/******************************** Bid File Actions *******************************/

export const submitWinningBid = (projectId, phaseId, data, cb, errorCB) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(`/fields-management/winning-bid/${projectId}`, data)
            .then(() => {
                dispatch(setProjectProcessing(false));

                dispatch(fetchPhaseDetails(projectId, phaseId, cb));
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
                errorCB && errorCB();
            });
    };
};

export const retenderBid = (projectId, phaseId, cb, errorCB) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .delete(`/project-management/projects/${projectId}/re-tender`)
            .then(response => {
                console.log(response.data);
                dispatch(setProjectProcessing(false));
                dispatch(fetchPhaseDetails(projectId, phaseId, cb));
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
                errorCB && errorCB();
            });
    };
};

export const revisionAdminApprovalBid = (
    projectId,
    phaseId,
    data,
    cb,
    errorCB
) => {
    return dispatch => {
        dispatch(setProjectProcessing(false));
        axios
            .post(
                `/project-management/projects/${projectId}/revise-project-cost`,
                data
            )
            .then(() => {
                dispatch(setProjectProcessing(false));

                let projectFields = {
                    fields: [
                        "projectId",
                        "name",
                        "divArea",
                        "station",
                        "referenceNumber",
                        "tbdDate",
                        "fy",
                        "fy_2",
                        "office",
                        "projectWorkflow",
                        "markingHistory",
                        "currentProjectOfficeData",
                        "currentProjectOffice",
                        "sanctions",
                        "gbXml",
                        "exec_office",
                        "customWorkflow"
                    ]
                };

                dispatch(fetchProjectDetails(projectId, projectFields));
                cb && cb();
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
                errorCB && errorCB(errorMessage);
            });
    };
};

export const deleteWinningBid = (projectId, phaseId, cb) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .delete(`/fields-management/remove-bid/${projectId}`)
            .then(response => {
                dispatch(fetchPhaseDetails(projectId, phaseId, cb));
            })
            .catch(err => {
                dispatch(setProjectProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

export const submitCheckedBidFirms = (projectId, phaseId, data, cb) => {
    return dispatch => {
        dispatch(setProjectProcessing(true));
        axios
            .post(`/fields-management/select-firms/${projectId}`, data)
            .then(() => {
                dispatch(fetchPhaseDetails(projectId, phaseId, cb));
            })
            .catch(err => {
                let errorMessage = handleError(err);
                dispatch(setProjectProcessing(false));
                message.error(errorMessage, NOTIFICATION_TIME);
            });
    };
};

/******************************** END *******************************/

/***************************************** FIRMS & Devices Actions ***********************************/

export const fetchAllExecutionProjects = () => {
    return dispatch => {
        dispatch(setProjectProcessing(true));

        axios
            .get("/project-marking/project-marking/projects/in/execution")
            .then(response => {
                dispatch(setProjectProcessing(false));

                dispatch(setAllExecutionProjects(response.data.projects));
            })
            .catch(err => {
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
                dispatch(setProjectProcessing(false));
            });
    };
};

/******************************** END *******************************/
