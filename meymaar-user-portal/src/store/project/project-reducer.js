import * as actions from './project-action-types';
import {sortByKeyAsc} from "../../utils/common-utils";

const getPhases = () => {
    let phases = sessionStorage.getItem('phases');

    if (phases) return JSON.parse(phases);
    return null;
};


const initState = () => {
    return {

        inboxProjects: null,
        worksList: null,
        taskBoardList: null,

        projectDetails: null,
        projectFields: null,

        phaseDetails: null,
        phases: getPhases(),

        processing: null,

        bqFiles: null,
        editRights: false,

        markingHistory: null,
        sanctionDetails: null,

        drawingDetails: null,

        workPagination: null,
        taskPagination: null,

        executionProjects: null,
        phaseGroup: null,

    }
};

const project_reducer = (state = initState(), action) => {
    let newState = {...state};
    switch (action.type) {

        case actions.SET_WORKS_LIST:
            saveWorksList(newState, action.payload);
            break;

        case actions.SET_TASKS_LIST:
            saveTasksList(newState, action.payload);
            break;

        case actions.SET_PROJECT_DETAILS:
            saveProjectDetails(newState, action.payload);
            break;

        case actions.SET_PHASE_DETAILS:
            savePhaseDetails(newState, action.payload);
            break;

        case actions.SET_PHASE_GROUP:
            setPhaseGroup(newState, action.payload);
            break;

        case actions.SET_FORM_DATA:
            updateFormData(newState, action.payload);
            break;

        case actions.SET_BQ_FILES_DATA:
            saveBQFilesData(newState, action.payload);
            break;

        case actions.SET_EDIT_RIGHTS:
            setEditRights(newState, action.payload);
            break;

        case actions.SET_PHASES:
            savePhases(newState, action.payload);
            break;


        case actions.SET_EXECUTION_PROJECTS:
            setExecutionProjects(newState, action.payload);
            break;

        case actions.SET_SANCTION_DETAILS:
            setSanctionDetails(newState, action.payload);
            break;

        case actions.SET_DRAWING_DETAILS:
            setDrawingDetails(newState, action.payload);
            break;

        case actions.RESET_PHASES:
            resetProjectPhases(newState);
            break;
        case actions.RESET_REDUX:
            resetRedux(newState, action.payload);
            break;

        case actions.SET_PROJECT_PROCESSING:
            setProjectProcessing(newState, action.payload);
            break;

        default:
            break;
    }
    return newState;
};

const setPhaseGroup = (state, data) => {

    state.phaseGroup = data.phases;
};


const saveWorksList = (state, data) => {
    state.workPagination = data.pagination;
    state.worksList = [...data.projects];
};

const saveTasksList = (state, data) => {
    state.taskPagination = data.pagination;
    state.taskBoardList = [...data.projects];
};

const savePhases = (state, data) => {
    state.phases = [...data];
    sessionStorage.setItem('phases', JSON.stringify(data));
};

const saveProjectDetails = (state, projectDetails) => {

    let phases = sortByKeyAsc(projectDetails.projectWorkflow.phases, 'sequence');
    for (let p = 0; p < projectDetails.phaseGroups.length; p++) {
        let group = projectDetails.phaseGroups[p];

        let _phases = [];

        for (let i = 0; i < group.phases.length; i++) {

            let groupPhasesId = group.phases[i];
            let phase = phases.filter(phases => phases.phase.uuid === groupPhasesId);
            if (phase && phase.length) {
                _phases.push(...phase);
            }
        }
        //projectDetails.phaseGroups[p].phases = _phases;

        if(_phases.length && typeof(_phases[0]) === "object"){
            projectDetails.phaseGroups[p].phases = _phases;
        }else{
            projectDetails.phaseGroups.splice(p,1);
            p--;
        }
    }

    state.projectDetails = projectDetails;
};


const savePhaseDetails = (state, phaseDetails) => {
    state.phaseDetails = {...phaseDetails};
};


const updateFormData = (state, data) => {
    let phaseDetails = [...state.phaseDetails];

    phaseDetails.forms.forEach(formData => {
        if (formData.form.formName === data.formName) {
            formData.data = {...data.phaseData};

            let complete = false;
            for (let index = 0; index < formData.fields.length; index++) {
                let field = formData.fields[index];

                if (field.required && formData.data[field.name]) {
                    complete = true;
                } else if (field.required) {
                    complete = false;
                    break;
                }
            }

            formData.complete = complete;
        }
    });

    state.phaseDetails = phaseDetails;
};


const saveBQFilesData = (state, filesData) => {
    state.bqFiles = [...filesData];
};


const setExecutionProjects = (state, data) => {
    state.executionProjects = [...data];
};


const setSanctionDetails = (state, data) => {
    state.sanctionDetails = {...data};
};

const setDrawingDetails = (state, data) => {
    state.drawingDetails = [...data];
};
const resetRedux = (state, key) => {
    state[key] = null;
};

const resetProjectPhases = (state) => {
    state.phases = null;
    sessionStorage.removeItem('phases');
};

const setEditRights = (state, data) => {
    state.editRights = data
};
const setProjectProcessing = (state, data) => {
    state.processing = data;
};

export default project_reducer;
