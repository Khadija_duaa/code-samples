/*************************** Project Action Types *************************************/

// Saving Inbox, Office, My Assigned & Execution Projects
export const SET_WORKS_LIST = 'ACTION_SET_WORKS_LIST';
export const SET_TASKS_LIST = 'ACTION_SET_TASKS_LIST';
export const SET_EXECUTION_PROJECTS = 'ACTION_SET_EXECUTION_PROJECTS';

// Saving project details Action Type
export const SET_PROJECT_DETAILS = 'ACTION_SET_PROJECT_DETAILS';

export const SET_PHASE_GROUP = 'ACTION_SET_PHASE_GROUP';

// Phase Action Types
export const SET_PHASE_DETAILS = 'ACTION_SET_PHASE_DETAILS';
export const SET_PHASES = 'ACTION_SET_PHASES';
export const RESET_PHASES = 'ACTION_RESET_PHASES';

export const SET_FORM_DATA = 'ACTION_SET_FORM_DATA';

// Saving BQ Files Data
export const SET_BQ_FILES_DATA = 'ACTION_SET_BQ_FILES_DATA';

// Saving Edit Rights
export const SET_EDIT_RIGHTS = 'ACTION_SET_EDIT_RIGHTS';

// Saving Sanction Details
export const SET_SANCTION_DETAILS = 'ACTION_SET_SANCTION_DETAILS';

// Saving Drawing Sheets
export const SET_DRAWING_DETAILS = 'ACTION_SET_DRAWING_DETAILS';

/*************************** End ******************************/



/*************************** End ******************************/

// Setting Project Proecessing Boolean
export const SET_PROJECT_PROCESSING = 'ACTION_SET_PROJECT_PROCESSING';

// Resetting Reducer's objects
export const RESET_REDUX = 'ACTIONS_SET_RESET_REDUX';
