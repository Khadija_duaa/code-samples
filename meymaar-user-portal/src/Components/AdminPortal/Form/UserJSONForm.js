import React from 'react';
import {Form, Input, Button, DatePicker, Select} from 'antd';
import {connect} from 'react-redux';
import moment from 'moment';

import 'antd/dist/antd.css';
import './formStyle.css';

import {BUTTON_COLOR} from "../../../utils/common-utils";
import {createUser, updateUser} from "../../../store/server/server-actions";
import {isMobile, isValidPhone} from "../../../utils/validations";

import InputMask from '../../InputMask/InputMask';
import NewLoader from '../../../Containers/Loader/NewLoader';

class UserJSONForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
            dob: '',
            formErrors: {}
        }
    }

    componentDidMount() {
        if (this.props.user) {
            delete this.props.user['dob'];
            this.props.form.setFieldsValue(this.props.user);
        }
    }

    getHeader = () => {

        let {headerText} = this.props;

        if (this.props.user) {
            headerText = "Update User";
        }

        return (
            <div className={"col-md-12 custom-form-header"} >
                <h4 style={{WebkitTextStroke: "0.5px"}}>{headerText}</h4>
            </div>
        )
    };


    /******************** MODIFICATION OF RULES *******************/

    validationPhone = (rules, value, callback) => {
        if (value) {
            if (!isValidPhone(value) || !isMobile(value)) {
                callback('Phone Format XXXX-XXXXXXX');
            } else {
                callback();
            }
        } else {
            callback();
        }
    };

    validateCNIC = (rules, value, callback) => {
        if (value) {
            let cnic_no_regex = /^[0-9+]{5}-[0-9+]{7}-[0-9]{1}/;

            if (!cnic_no_regex.test(value)) {
                callback('CNIC Format XXXXX-XXXXXXX-X');
            }
            else {
                callback();
            }
        } else {
            callback();
        }
    };

    validateUsername = (rules, value, callback) => {
        let aplhaNumeric = /^[a-z0-9A-Z._]+$/i;
        if (value && !aplhaNumeric.test(value)) {
            callback('Username can only alphanumeric');
        }else{
            callback();
        }
    };


    modifyConfigurations = (config, item) => {

        let _rules = [...config];

        switch (item.validationType) {
            case 'phone':
                _rules.push({validator: this.validationPhone});
                break;

            case 'cnic':
                _rules.push({validator: this.validateCNIC});
                break;

            // case 'username':
            //     _rules.push({validator: this.validateUsername});
            //     break;

            default:
                break;
        }

        return _rules;
    };


    /***************************** END *****************************/


    getBody = () => {

        let handleSubmit = (e) => {
            e.preventDefault();

            let {successCB} = this.props;
            this.props.form.validateFields((err, fieldsValue) => {

                if (err) {
                    return;
                }


                const values = {...fieldsValue};
                console.log(values);

                values['middleName'] = values['middleName']?values['middleName'].trim():null;
                values['email'] = values['email']?values['email'].trim():null;

                if (this.props.user) {
                    let url = `/user-management/users/${this.props.user.uuid}`;
                    this.props.updateUser(url, values,
                        () => {
                            this.props.form.resetFields();
                            successCB && successCB();
                        },
                        (errors) => {

                            errors.forEach(errorField => {
                                this.props.form.setFields({
                                    [errorField.field]: {
                                        value: fieldsValue[errorField.field],
                                        errors: [new Error(`${errorField.error}`)],
                                    }
                                });
                            });
                        },
                        this.props.successText
                    );
                } else {
                    this.props.createUser(this.props.submitURL, values, this.props.isSuperAdmin,
                        () => {
                            this.props.form.resetFields();
                            successCB && successCB();
                        },
                        (errors) => {

                            errors.forEach(errorField => {
                                this.props.form.setFields({
                                    [errorField.field]: {
                                        value: fieldsValue[errorField.field],
                                        errors: [new Error(`${errorField.error}`)],
                                    }
                                });
                            });
                        },
                        this.props.successText
                    );
                }
            })
        };

        let cancelClick = (e) => {
            e.preventDefault();
            // this.props.form.resetFields();
            let {successCB} = this.props;

            successCB && successCB();
        };

        let formJSX = () => {


            let handleConfirmBlur = (e) => {
                const value = e.target.value;
                this.setState({confirmDirty: this.state.confirmDirty || !!value});
            };

            let validateDate = (rule, value, callback) => {

                let startDate = moment(new (), "YYYY-MM-DD");
                let endDate = moment(value, "YYYY-MM-DD");
                let years = startDate.diff(endDate, 'year');

                if (years < 18) {
                    callback("Minimum age limit is 18 years");
                } else {
                    callback();
                }
            };

            let validateToNextPassword = (rule, value, callback) => {
                const form = this.props.form;

                if (value && this.state.confirmDirty) {
                    form.validateFields(['confirmPassword'], {force: true});
                }

                if (value && value.length < 6) {
                    callback('Password must be at least 6 characters or more');
                }
                else {
                    callback();
                }
            };

            let compareToFirstPassword = (rule, value, callback) => {

                const form = this.props.form;

                if (value && value !== form.getFieldValue('password')) {
                    callback('Password and confirm password mismatched!');
                } else {
                    callback();
                }
            };

            handleConfirmBlur = (e) => {
                const value = e.target.value;
                this.setState({confirmDirty: this.state.confirmDirty || !!value});
            };


            let renderForm = () => {

                const {getFieldDecorator} = this.props.form;

                const formItemLayout = {
                    labelCol: {span: 4},
                    wrapperCol: {span: 6},
                };

                return this.props.formJSON.map((item, index) => {
                    let formItem = '';


                    let config = {
                        rules: [{required: item.required, message: `${item.label} is required!`}],
                    };

                    if(item.type !== "date"){
                        config.rules[0].whitespace = item.whitespace;
                    }

                    config.rules = this.modifyConfigurations(config.rules, item);

                    switch (item.type) {

                        case 'text':

                            formItem = <Form.Item key={index} {...formItemLayout} label={item.label}>
                                {
                                    getFieldDecorator(`${item.value}`, config)(<Input placeholder={item.placeholder}/>)
                                }
                            </Form.Item>;

                            break;

                        case 'password':
                            config.rules.push({validator: validateToNextPassword}, {validator: compareToFirstPassword});
                            formItem = <Form.Item key={index} {...formItemLayout} label={item.label}>
                                {
                                    getFieldDecorator(`${item.value}`, config)(<Input.Password
                                        placeholder={item.placeholder}/>)
                                }
                            </Form.Item>;

                            break;

                        case 'confirm':

                            config.rules.push({validator: compareToFirstPassword});

                            formItem = <Form.Item key={index} {...formItemLayout} label={item.label}>
                                {
                                    getFieldDecorator(`${item.value}`, config)(<Input.Password
                                        placeholder={item.placeholder} onBlur={handleConfirmBlur}/>)
                                }
                            </Form.Item>;

                            break;

                        case 'mask':

                            formItem = <Form.Item key={index} {...formItemLayout} label={item.label}>
                                {
                                    getFieldDecorator(`${item.value}`, config)(<InputMask placeholder={item.placeholder} mask={item.mask}/>)
                                }
                            </Form.Item>;

                            break;


                        case 'date':

                            config.rules.push({
                                validator: validateDate
                            });


                            if (this.props.user) {
                                config.initialValue = moment(this.props.dob);
                                config.rules[0].type = "object";
                            }

                            formItem = <Form.Item key={index} {...formItemLayout} label={item.label}>
                                {
                                    getFieldDecorator(`${item.value}`, config)(<DatePicker format={"YYYY-MM-DD"} placeholder={item.placeholder}/>)
                                }
                            </Form.Item>;

                            break;

                        case 'email':

                            config.rules.push({
                                type: 'email',
                                message: 'The input is not valid E-mail!',
                            });

                            formItem = <Form.Item key={index} {...formItemLayout} label={item.label}>
                                {
                                    getFieldDecorator(`${item.value}`, config)(<Input placeholder={item.placeholder}/>)
                                }
                            </Form.Item>;

                            break;

                        case 'number':

                            formItem = <Form.Item key={index} {...formItemLayout} label={item.label}>
                                {
                                    getFieldDecorator(`${item.value}`, config)(<Input placeholder={item.placeholder}/>)
                                }
                            </Form.Item>;

                            break;

                        case 'select':

                            const Option = Select.Option;

                            formItem = <Form.Item key={index} {...formItemLayout} label={item.label}>
                                {
                                    getFieldDecorator(`${item.value}`, config)(
                                            <Select placeholder={item.placeholder} mode={item.mode}>
                                                {
                                                    item.options.map((option, index) => {
                                                        return (
                                                            <Option key={index} value={option}>{option}</Option>
                                                        )
                                                    })
                                                }
                                            </Select>
                                    )
                                }
                            </Form.Item>;
                            break;

                        default:
                            break;
                    }

                    return formItem;
                });
            };

            let renderButtons = () => {
                const buttonItemLayout = {
                    wrapperCol: {offset: 4},
                };

                return (
                    <Form.Item {...buttonItemLayout} >
                        <Button htmlType="submit" style={{
                            height: "40px",
                            width: "12%",
                            backgroundColor: BUTTON_COLOR,
                            color: "#fff"
                        }}>{this.props.user ? "Update" : "Create"}</Button>
                        <Button style={{height: "40px", width: "12%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                                type={"default"} onClick={cancelClick}>Cancel</Button>
                    </Form.Item>
                )
            };

            return (
                <Form layout={"horizontal"} onSubmit={handleSubmit}>
                    {renderForm()}
                    {
                        this.props.processing ?
                            <NewLoader/>
                            :
                            null
                    }
                    {renderButtons()}
                </Form>
            )
        };

        return (
            <div className={"col-md-12"} style={{paddingLeft: "5%"}}>
                <h6 style={{WebkitTextStroke: "0.6px", margin: "3% 0px"}}>
                    {this.props.bodyHeaderText}
                </h6>

                {formJSX()}
            </div>
        )
    };

    render() {

        let containerStyle = {
            border: "2px solid rgba(231, 232, 237,0.5)",
            padding: "unset",
            borderRadius: "4px",
            marginTop: "10px",
            marginBottom: "10px",
            ...this.props.style
        };

        return (
            <div style={containerStyle} className={"custom-form"}>
                {this.getHeader()}
                {this.getBody()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        formErrors: state.server_reducer.fieldErrors,
        processing: state.server_reducer.processing,
        activeUser: state.user_reducer.activeUser
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        createUser: (url, data, superAdmin = false, successCB, errorCB, succesText) => dispatch(createUser(url, data, superAdmin, successCB, errorCB, succesText)),
        updateUser: (url, data, successCB, errorCB, successText) => dispatch(updateUser(url, data, successCB, errorCB, successText))
    }
};

const formCreated = Form.create()(UserJSONForm);
const connected = connect(mapStateToProps, mapDispatchToProps)(formCreated);
export default connected