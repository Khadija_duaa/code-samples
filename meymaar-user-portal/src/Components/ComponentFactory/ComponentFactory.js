import React from 'react';
import BQForm from '../CustomForms/BOQ/BQForm';
import Drawings from "../CustomForms/Drawings/Drawings";
import FileView from '../CustomForms/Fileview/Fileview';
import TakingOffSheet from '../CustomForms/Revitt/TOS/TakingOffSheet';

class ComponentFactory extends React.Component{

    getComponent = ()=>{
        let {form} = this.props;
        switch (form.reactComponent){
            case 'FILE':
                return <FileView projectProps={this.props}/>;
            default:
                break;
        }

        switch (form.formName){
            case 'BOQ':
                return <BQForm projectProps = {this.props}/>;
            case 'DRAWINGS':
                return  <Drawings projectProps = {this.props}/>;
            case 'TOSs':
                return <TakingOffSheet projectProps={this.props}/>;
            default:
                break;
        }
    };
    render(){
        return(
            <div>{this.getComponent()}</div>
        )
    }
}

export default ComponentFactory