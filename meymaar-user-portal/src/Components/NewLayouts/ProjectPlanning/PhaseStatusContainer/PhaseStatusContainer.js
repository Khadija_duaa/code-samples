import React from 'react';
import {Row, Col, Progress} from 'antd';


class PhaseStatusContainer extends React.Component {

    formSeparator = {
        marginBottom: '12px',
        width: "100%"
    };

    fontStyling = {
        fontFamily: 'Roboto',
        fontSize: '14px',
        marginLeft:"15px"
    };


    render() {

        const {strokeColorProgressBar, currentStep} = this.props;
        let completedTaskCount = 0;
        currentStep && currentStep.phases && currentStep.phases.map((data) => {

            if (data.completed === true) {
                completedTaskCount++
            }
        });

        let percentageTaskCompletion = parseFloat(((completedTaskCount * 100) / currentStep.phases.length).toFixed(0));

        return (
            <div style={{
                backgroundColor: '#F4F4F7',
                border: '1px solid #E0E5E8',
                height: '230px',
                borderRadius: '5px'
            }}>
                <Row>
                    <Row style={{marginBottom: '-10px', display: 'flex', marginLeft: '8px'}}>
                        <Col style={{marginLeft: '6px', marginTop: '18px'}}>
                            <h2>{completedTaskCount}</h2>
                        </Col>
                        <Col style={{marginLeft: '7px', marginTop: '18px'}}>
                            <h2>/</h2>
                        </Col>
                        <Col style={{marginLeft: '10px', marginTop: '18px'}}>
                            <h2>{currentStep.phases && currentStep.phases.length}</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col style={{marginLeft: '14px'}}>
                            <b>{this.props.getPhaseStatusData.tasksCompletion}</b>
                        </Col>
                    </Row>
                    <Row style={{marginTop: '2px', marginBottom: '-5px'}}>
                        <Col className={"col-md-12"}>
                            <Progress width={50} strokeColor={strokeColorProgressBar}
                                      percent={percentageTaskCompletion}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col className={"col-md-12"}>
                            <hr style={this.formSeparator}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col style={{display: 'block', paddingLeft:"15px", paddingRight:"15px"}}>
                            <Row style={{display: 'flex'}}>
                                <img src={'images/unvisited-mark.svg'} height="18" style={{marginTop:"3px"}}/>
                                <p style={this.fontStyling}>{this.props.getPhaseStatusData.ToDo}</p>
                            </Row>
                            <Row style={{display: 'flex'}}>
                                <img src={'images/warning.svg'} height="18" style={{marginTop:"3px"}}/>
                                <p style={this.fontStyling}>{this.props.getPhaseStatusData.InProgress}</p>
                            </Row>
                            <Row style={{display: 'flex'}}>
                                <img src={'images/checked.svg'} height="18" style={{marginTop:"3px"}}/>
                                <p style={this.fontStyling}>{this.props.getPhaseStatusData.Complete}</p>
                            </Row>
                        </Col>
                    </Row>


                </Row>
            </div>
        )
    }
}

export default PhaseStatusContainer;