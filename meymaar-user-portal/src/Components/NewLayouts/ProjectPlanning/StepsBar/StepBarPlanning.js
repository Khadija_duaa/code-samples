import React from 'react';
import {Row} from 'antd';
import {sortByKeyAsc} from "../../../../utils/common-utils";
import Step from "./Step";


class StepsBarPlanning extends React.Component {

    renderPhases = () => {
        let {activeStepIndex, onStepClick, projectDetails} = this.props;


        let stepColors = {
            primaryColor: "#30BDA0",
            secondaryColor: "#E0E5E8",
            activeTextColor: "#000",
            completeStepColor: "#84BD5A",
        };

        let {projectWorkflow} = projectDetails;
        let {phaseGroups} = projectDetails;
        let {defaultPhase} = projectWorkflow;

        let phases = sortByKeyAsc(projectWorkflow.phases, 'sequence');
        let stepsFlexPercentage = Math.floor(100 / phases.length);

        let columnStyle = {
            flex: `0 0 ${stepsFlexPercentage}%`,
            textAlign: "center",
            marginBottom: "30px",
        };


        return phaseGroups.map((item, index) => {
            let activeStep = false;
            let itemActive = false;
            let booleanActiveStep = false;
            let isAllPhasesCompleted = Boolean(item.phases.length);

            for (let i = 0; i < item.phases.length; i++) {
                isAllPhasesCompleted = isAllPhasesCompleted && item.phases[i].completed;
                itemActive = itemActive || item.phases[i].active;
                if (item.phases[i].phase.uuid === defaultPhase.uuid) {
                    booleanActiveStep = true;
                    break;
                }

            }

            if (index === activeStepIndex || booleanActiveStep) {
                activeStep = true;
            }

            // Step Bottom line color
            let bottomLineColor = isAllPhasesCompleted ? stepColors.completeStepColor : activeStep ? stepColors.primaryColor : stepColors.secondaryColor;


            // //Step title color
            let textColor = activeStep ? stepColors.activeTextColor : isAllPhasesCompleted ? stepColors.completeStepColor : stepColors.activeTextColor;

            // Step Title Style
            let stepTitleStyle = {
                cursor: `${itemActive ? 'pointer' : 'default'}`,
                color: textColor,
                opacity: `${activeStep || isAllPhasesCompleted ? '1' : '0.5'}`
            };

            let isCursorPointer = activeStep || isAllPhasesCompleted;

            columnStyle = {
                ...columnStyle,
                cursor: isCursorPointer ? "pointer" : "default"
            };

            // Step Properties
            let stepProps = {
                columnStyle,
                span: parseInt(Math.ceil(24 / phaseGroups.length)),
                step: item,
                stepTitle: `${item.name}`,
                stepTitleStyle,
                bottomLineColor,
                bottomLineWidth: "3px",
                bottomLineRadius: "0px",
                bottomLineCursor: isCursorPointer ? "pointer" : "default",
                index,
                onStepClick: isCursorPointer ? (index, item) => onStepClick(index, item) : null
            };

            // Rendering Step Component
            return (
                <Step key={index} {...stepProps}/>
            )
        });
    };

    render() {

        return (
            <Row gutter={10} className="phases-steps-bar" style={{marginLeft:'-19px'}}>
                {this.renderPhases()}
            </Row>
        )
    }
}

export default StepsBarPlanning;