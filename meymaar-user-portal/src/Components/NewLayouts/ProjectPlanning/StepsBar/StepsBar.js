import React from 'react';
import {Row} from 'antd';
import {sortByKeyAsc} from "../../../../utils/common-utils";
import Step from "./Step";


class StepsBar extends React.Component {

    renderPhases = () => {
        let {activeStepIndex, currentPhase, onStepClick, projectDetails} = this.props;

        console.log('its planning props', this.props.activeStepIndex, this.props.currentPhase, this.props.onStepClick, this.props.projectDetails);

        let stepColors = {
            primaryColor: "#30BDA0",
            secondaryColor: "#E0E5E8",
            activeTextColor: "#000",
            completeStepColor: "#84BD5A",
        };

        let {projectWorkflow} = projectDetails;
        let {defaultPhase} = projectWorkflow;

        let phases = sortByKeyAsc(projectWorkflow.phases, 'sequence');
        let stepsFlexPercentage = Math.floor(100 / phases.length);

        let columnStyle = {
            flex: `0 0 ${stepsFlexPercentage}%`,
            textAlign: "center",
            marginBottom: "30px",
        };


        return phases.map((item, index) => {
            let activeStep = false;

            //
            if (index === activeStepIndex || (!currentPhase && item.phase.uuid === defaultPhase.uuid)) {
                activeStep = true;
            }

            // Step Bottom line color
            let bottomLineColor = item.completed ? stepColors.completeStepColor : activeStep || item.phase.uuid === defaultPhase.uuid ? stepColors.primaryColor : stepColors.secondaryColor;


            //Step title color
            let textColor = activeStep ? stepColors.activeTextColor : item.completed ? stepColors.completeStepColor : stepColors.activeTextColor;


            // Step Title Style
            let stepTitleStyle = {
                cursor: `${item.active ? 'pointer' : 'default'}`,
                color: textColor,
                opacity: `${activeStep ? '1' : '0.5'}`
            };

            let isCursorPointer = Boolean(activeStep || item.phase.uuid === defaultPhase.uuid || item.completed);

            columnStyle.cursor = isCursorPointer ? "pointer" : "default";

            // Step Properties
            let stepProps = {
                columnStyle,
                span: parseInt(Math.ceil(24 / phases.length)),
                step: item,
                stepTitle: `${item.phase.name}`,
                stepTitleStyle,
                bottomLineColor,
                bottomLineWidth: "3px",
                bottomLineRadius: "0px",
                bottomLineCursor: isCursorPointer ? "pointer" : "default",
                index,
                onStepClick: (index, item) => onStepClick(index, item)
            };

            // Rendering Step Component
            return (
                <Step key={index} {...stepProps}/>
            )
        });
    };

    render() {
        return (
            <div>
                <div>
                    <Row gutter={10} className="phases-steps-bar">
                        {this.renderPhases()}
                    </Row>
                </div>
            </div>
        )
    }
}

export default StepsBar;