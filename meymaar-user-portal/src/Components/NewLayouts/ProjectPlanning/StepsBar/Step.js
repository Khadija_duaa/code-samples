import React from 'react';
import {Col} from 'antd';

const zeroPad = require('zero-pad');
const BottomLine = (props) => {
    let borderStyle = {
        width: "100%",
        border: `${props.borderWidth} solid ${props.borderColor || "#000"}`,
        borderRadius: props.borderRadius,
        cursor: props.cursor
    };

    return (
        <div style={borderStyle}/>
    )
};

class Step extends React.Component {
    render() {
        let {
            columnStyle, span, step,
            stepTitle, stepTitleStyle, bottomLineColor,
            bottomLineWidth, bottomLineRadius, bottomLineCursor,
            index, onStepClick
        } = this.props;


        return (
            <Col style={columnStyle} span={span} key={index} onClick={() => onStepClick && onStepClick(index, step)}>
                <p style={stepTitleStyle}>
                    {zeroPad(index + 1)} {stepTitle}
                </p>
                <BottomLine borderColor={bottomLineColor} borderWidth={bottomLineWidth}
                            borderRadius={bottomLineRadius} cursor={bottomLineCursor}/>
            </Col>
        )
    }
}

export default Step