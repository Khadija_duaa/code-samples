import React from 'react';
import MenuItem from "./MenuItem";

const zeroPad = require('zero-pad');
const fontFamily = { fontFamily: 'Roboto' };

class Menu extends React.Component {

    constructor(props) {
        //let {projectDetails} = props;
        super(props);
        this.state = {
            processing: false,
            activePhaseIndex: null,
            currentPhase: null,
        }
    }

    renderMenuItems = (isActiveTask, groupPhases, setStateOfParent) => {




        // For default menu, background color is default.
        // Disabled menu , change background color to something light,
        // Active Menu item , change background color to white.

        // Change padding for active menu to 30px 20px
        let menuStyle = {

        };


        let     iconStyle = {
            width: "20px",
            height: "20px",
            marginBottom: "4px",
            marginRight: '12px',
            marginTop:"5px"
        };


        // Add Background color if menu item is active.
        let numberStyle = {
            ...fontFamily,
            color: "#747474",
            fontSize: '16px',
            fontWeight: 'bold',
            marginLeft: "20px",
            height: '40px',
            width: '40px',

        };

        let numberStyleActive = {
            ...fontFamily,
            color: "#FFFFFF",
            fontSize: '18px',
            fontWeight: 'bold',
            marginLeft: "23px",
            backgroundColor: '#30BDA0',
            borderRadius: '50px',
            border:'2px solid #30BDA0',
            height: '40px',
            width: '40px',


        };

        let onTaskMenuClick = (menuIndex, item, setParent) => {

            if (!item.active) return null;

            this.setState({processing: true}, () => {
                setTimeout(() => {
                    this.setState({
                        activePhaseIndex: menuIndex,
                        currentPhase: item.phase,
                        processing: false,
                    }, () => {
                        setParent({...this.state})
                    });
                }, 0)
            });

        };

        // Add active-menu if menu is clicked, otherwise nothing ""
        // Image complete will be there for complete forms otherwise nothing.

        let {activePhase} = this.props;

        return (
            groupPhases && groupPhases.phases.map((obj, i) => {
                let menutTextStyle = {
                    ...fontFamily,
                    color: "#747474",
                    marginLeft: "14px",
                    marginRight:"10px",
                    width: "200px",
                    wordBreak: "break-word",
                    textTransform: "capitalize",
                    fontSize: '16px',
                    fontWeight: 'bold',
                    cursor: `${obj.active ? 'pointer' : 'default'}`,
                    marginBottom:"unset"
                };

                let menutTextStyleActive = {
                    ...fontFamily,
                    color: "#747474",
                    marginLeft: "19px",
                    marginRight:"10px",
                    width: "200px",
                    wordBreak: "break-word",
                    textTransform: "capitalize",
                    fontSize: '18px',
                    fontWeight: 'bold',
                    cursor: `${obj.active ? 'pointer' : 'default'}`,
                    marginBottom:"unset"
                };


                let activeStep = false;
                if ((activePhase && activePhase.uuid === obj.phase.uuid)) {
                    activeStep = true;
                }

                let isCursorPointer = Boolean(activeStep || obj.completed);
                let imageURL = null;
                if (obj.completed) {
                    imageURL = 'images/checked.svg'
                } else if (obj.active && !obj.completed) {
                    imageURL = 'images/warning.svg'
                } else if (!obj.active) {
                    imageURL = 'images/unvisited-mark.svg'
                }

                return (
                    <div key={i} onClick={() => onTaskMenuClick(i, obj, setStateOfParent)}
                         style={{cursor: isCursorPointer ? "pointer" : "default"}}>
                        <MenuItem key={i} className={activeStep ? "menu-item active-menu " : "menu-item default-menu"}
                                   >

                            <div>
                                <span className={"number"}
                                      style={activeStep ? numberStyleActive : numberStyle}>{zeroPad(i + 1)}</span>
                            </div>

                            <p style={activeStep ? menutTextStyleActive : menutTextStyle}>{obj.phase.name}</p>
                            {imageURL ? <img src={imageURL} alt={"checked"}
                                             style={iconStyle}/> : null}
                        </MenuItem>
                    </div>
                )
            })

        )
    };

    render() {
        const {isActiveTask, groupPhases} = this.props;


        return (
            <div style={{display: "grid", minWidth:"720px", width: "100%", marginLeft: '11px'}}>
                {this.renderMenuItems(isActiveTask, groupPhases, this.props.setStateOfParent)}
            </div>
        )
    }
}

export default Menu;