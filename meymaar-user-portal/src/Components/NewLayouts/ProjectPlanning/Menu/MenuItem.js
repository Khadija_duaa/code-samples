import React from 'react';
import {Col} from 'antd';
import 'antd/dist/antd.css'
import './menuItemStyle.css';

class MenuItem extends React.Component {
    render() {
        let {menuStyle,className,span} = this.props;

        return (
            <Col className={className} span={span} style={menuStyle && menuStyle} >
                {this.props.children && this.props.children}
            </Col>
        )
    }
}

export default MenuItem