import React from 'react';
import {Icon} from "antd";

class DocumentTab extends React.Component{


    render(){


        let textStyle = {
            whiteSpace: 'nowrap',
            textOverflow: 'ellipsis',
            fontSize:'15px',
            fontFamily: 'Roboto',
            width:'100px',
            height:'30px',
            overflow:"hidden",
            marginLeft:'10px',
            marginRight:'10px',
        };
        const {onClickFunction,keyValue,iconColor} = this.props;

        return(
            <div style={{ display: 'block'}}>

                <div className={this.props.className} key={keyValue} style={this.props.style}
                     onClick={onClickFunction}>
                </div>

                <div style={{textAlign:'center'}}>
                    <p style={textStyle}>{this.props.formData}</p>
                </div>
            </div>
        )
    }
}

export default DocumentTab;