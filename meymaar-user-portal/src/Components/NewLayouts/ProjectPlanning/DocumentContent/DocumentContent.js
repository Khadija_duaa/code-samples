import React from 'react';
import {Row,Col,Progress} from 'antd';

const fontFamily = { fontFamily: 'Roboto' };

class DocumentContent extends React.Component {

    fontStyling = {
        fontFamily: 'Roboto',
        fontSize:'14px'
    };


    render() {

        const {activeDocumentTitle} = this.props;

        return (
            <Row style={{marginTop:'20px', marginLeft:'45px'}}>
                <Col style={{
                    width: '1070px', maxWidth: '1070px', overflowY:'auto', border: '1px solid #E0E5E8', backgroundColor: '#F4F4F7',
                    borderRadius: '7px'
                }}>
                    <Row style={{
                        backgroundColor: '#FFFFFF',
                        borderBottom: '1px solid #E0E5E8',
                        height: '70px',
                        maxHeight: '70px',
                        display:'flex',
                        borderRadius: '7px'
                    }}>
                        <Col span={7} offset={0} style={{ marginTop:'17px'}}>
                            <p style={{ ...fontFamily, fontSize:'20px', fontWeight:'bold', textAlign:'center', }}>
                                {activeDocumentTitle}
                            </p>
                        </Col>
                        <Col span={6} offset={10}  style={{ marginTop:'20px'}}>
                            <Row style={{display:'flex'}} justify="end">
                                <Col style={{display:'flex'}}>
                                    <img src={'images/edit.svg'} height="18" style={{marginTop:'4px'}} />
                                    <p style={{ ...fontFamily, fontSize:'16px',fontWeight:'bold',marginLeft:'8px', marginRight:'15px'}}>
                                        {'Edit'}
                                    </p>
                                </Col>
                                <Col style={{display:'flex'}}>
                                    <img src={'images/preview.svg'} height="18" style={{marginTop:'4px'}}/>
                                    <p  style={{ ...fontFamily, fontSize:'16px',fontWeight:'bold',marginLeft:'8px', marginRight:'15px'}}>
                                        {'Preview'}
                                    </p>
                                </Col>
                                <Col style={{display:'flex'}}>
                                    <img src={'images/download.svg'} height="18" style={{marginTop:'4px'}}/>
                                    <p  style={{ ...fontFamily, fontSize:'16px',fontWeight:'bold',marginLeft:'8px', marginRight:'15px'}}>
                                        {'Download'}
                                    </p>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row style={{ height:'1040px', maxHeight:'1040px',backgroundColor: '#F4F4F7'}}>
                        <Col style={{height:'9600px', maxHeight:'960px', border:'1px solid #E0E5E8', backgroundColor: '#FFFFFF',margin:'40px'}}>
                        </Col>
                    </Row>

                </Col>
            </Row>
        )
    }
}

export default DocumentContent;