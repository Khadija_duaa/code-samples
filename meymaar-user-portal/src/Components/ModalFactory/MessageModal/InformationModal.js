import React from 'react';
import {ModalBody, ModalHeader, ModalFooter} from 'reactstrap';
import Modal from '../../../Components/Modal/Modal';
import {BUTTON_COLOR} from "../../../utils/common-utils";
import {Button} from 'antd';
import 'antd/dist/antd.css';


class InformationModal extends React.Component{
    render(){

        let buttonStyle = {
            height: "40px",
            width: "70px",
            float:"right"
        };

        return (
            <div className={"col-md-24"}>
                <Modal isOpen={this.props.isOpen} toggle={this.props.toggle}>

                    <ModalHeader className={"header"}>
                        Information
                    </ModalHeader>

                    <ModalBody className={"modal-body"}>
                        {this.props.message}
                    </ModalBody>

                    <ModalFooter style={{border: "unset"}}>
                        <Button style={{...buttonStyle, backgroundColor: BUTTON_COLOR, color: "#fff"}}
                                onClick={this.props.okCB}>
                            OK
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }

}



export default InformationModal