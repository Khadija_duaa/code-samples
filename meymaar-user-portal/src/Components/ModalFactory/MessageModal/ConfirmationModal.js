import React from 'react';
import {ModalBody, ModalHeader, ModalFooter} from 'reactstrap';
import Modal from '../../../Components/Modal/Modal';
import {BUTTON_COLOR} from "../../../utils/common-utils";
import {Button} from 'antd';
import 'antd/dist/antd.css';
import {connect} from 'react-redux';
import NewLoader from '../../../Containers/Loader/NewLoader';

class ConfirmationModal extends React.Component{
    render(){

        let buttonStyle = {
            height: "40px",
            width: "50%"
        };

        return (
            <div className={"col-md-12"}>
                <Modal className={"confirmation"} isOpen={this.props.isOpen} toggle={this.props.toggle}>

                    <ModalHeader className={"header"}>
                        Confirmation
                    </ModalHeader>

                    <ModalBody className={"modal-body"}>
                        {this.props.message}

                        {this.props.processing ? <NewLoader/> : null}
                    </ModalBody>

                    <ModalFooter style={{border: "unset"}}>

                        <Button style={{...buttonStyle, backgroundColor: BUTTON_COLOR, color: "#fff"}}
                                onClick={this.props.okCB}>
                            Yes
                        </Button>

                        <Button style={{...buttonStyle, marginLeft: "5%", borderColor: BUTTON_COLOR}}
                                onClick={this.props.cancelCB}>No</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }

}


const mapStateToProps = state => {
    return {
        processing: state.project_reducer.processing
    }
};


export default connect(mapStateToProps)(ConfirmationModal)