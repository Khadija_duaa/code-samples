import React from 'react';
import {Modal, Table, Button} from 'antd';
import {formatCurrency} from "../../../utils/common-utils";


class BOQModal extends React.Component {

    getColumns = () => {
        const column = [
            {
                title: "Description of Work",
                dataIndex: "bqItemDescription",
            },
            {
                title: "Type",
                dataIndex: "bqItemType",
                width: 200
            },
            {
                title: "Amount",
                dataIndex: "bqItemAmount",
                width: 200,
                render: (record, data) => {
                    return (
                        <>{formatCurrency(data.bqItemAmount)}</>
                    )
                }
            },
            {
                title: "Vetted Work",
                dataIndex: "vettedWorkAmount",
                width: 200,
                render: (record, data) => {
                    return (
                        <>{formatCurrency(data.vettedWorkAmount)}</>
                    )
                }
            }
        ];

        return column;
    };


    render() {
        let {dataSource} = this.props;
        return (
            <div>
                <Table className="custom-new-table" columns={this.getColumns()}
                       dataSource={dataSource} size="middle" pagination={true}
                       rowKey={(record, index) => index}
                />
            </div>
        )
    }
}

export default BOQModal;