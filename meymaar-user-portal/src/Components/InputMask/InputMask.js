import {Input} from "antd";
import React from "react";
import ReactInputMask from 'react-input-mask';
import PropTypes from 'prop-types';

const InputMask = (props) => {
    return (
        <ReactInputMask {...props}>
            { (inputProps) => <Input disabled={props.disabled ? props.disabled : null} /> }
        </ReactInputMask>
    );
};

InputMask.propTypes = {
    mask: PropTypes.string,
    maskChar: PropTypes.string,
    formatChars: PropTypes.object,
    alwaysShowMask: PropTypes.bool,
    forwardedRef: PropTypes.func
};

export default InputMask;