import React from 'react';
import {Button, Icon, Upload} from 'antd';
import 'antd/dist/antd.css';

const uuidv1 = require('uuid/v1');

class UploadButton extends React.Component {
    state = {
        fileList : []
    };
    getUploadProps = ()=>{
        const {form, element, editRights, setFileDataCB, fileData} = this.props;
        const {multiple} = element.elementProps;

        let {fileList} = this.state;

        let elementData = fileData[element.name];

        return {
            showUploadList: {showRemoveIcon: editRights},
            multiple,
            accept: ".pdf",

            onRemove: file => {
                const index = fileList.indexOf(file);
                const newFileList = fileList.slice();
                newFileList.splice(index, 1);
                let fileData =   {...elementData, additionalFiles: newFileList};

                form.setFieldsValue({[element.name]: newFileList});
                this.setState({fileList: newFileList},()=>{
                    setFileDataCB && setFileDataCB(fileData);
                })

            },
            beforeUpload: file => {
                fileList.push(file);

                if (fileList.length > 1 && !multiple) {
                    form.setFieldsValue({[element.name]: [file]});
                    fileList.splice(0, 1);
                    this.setState({fileList: fileData},()=>{
                        let fileData = {...elementData, additionalFiles: [file]};
                        setFileDataCB && setFileDataCB(fileData);
                    })


                } else {
                    let fileData = {...elementData, additionalFiles: fileList};
                    form.setFieldsValue({[element.name]: fileList});
                    this.setState({fileList},()=>{
                        setFileDataCB && setFileDataCB(fileData);
                    });
                }
                return false;
            },
            fileList,
        };
    };
    render() {


        if (!this.props.editRights) return null;

        const uploadProps = this.getUploadProps();
        let fieldLabel = this.props.element.label;

        return (
            <Upload {...uploadProps} className={"json-upload mt-3 mb-3"}>
                <Button>
                    <Icon type="upload"/> {`Add ${fieldLabel} File`}
                </Button>
            </Upload>
        )
    }
}

export default UploadButton;

