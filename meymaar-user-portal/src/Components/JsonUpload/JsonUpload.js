import React from "react";
import UploadButton from "./UploadButton/UploadButton";
import FileViews from "./FileViews/FileViews";

class JsonUpload extends React.Component {

    constructor(props) {
        super(props);

        // Setting Initial File Object for that element
        let fileData = props.fileProps.fileData;
        let {initialValue} = props.fileProps;

        this.state = {
            fileData: {existingFiles: initialValue || [], removedFiles: []}
        }
    }

    componentDidMount() {
        let {setFileDataCB} = this.props.fileProps;
        setFileDataCB && setFileDataCB(this.state.fileData);
    }


    renderUploadButton = () => {
        return (
            <UploadButton {...this.props.fileProps}/>
        )
    };

    renderFileViews = () => {
        if (!this.props.fileProps.initialValue) return null;

        return (
            <FileViews {...this.props.fileProps}/>
        )
    };

    render() {
        return (
            <div>
                {this.renderFileViews()}
                {this.renderUploadButton()}
            </div>
        )
    }
}

export default JsonUpload