import React from 'react';
import {previewDownloadFile} from "../../../utils/server-utils";
import {Button, Col, Row} from "antd";
import DeleteConfirmation from "../../ModalFactory/ConfirmationModal/DeleteConfirmation";

class FileViews extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showConfirmModal: false,
            activeFile: null
        }
    }

    /************************** Events *****************************/


    handleDeleteFileClick = () => {
        let {activeFile} = this.state;

        const {element, setFileDataCB} = this.props;
        let {fileData, initialValue} = this.props;

        let elementData = fileData[element.name];
        let removedFiles = elementData.removedFiles;
        let addedFiles = elementData.additionalFiles || [];

        initialValue = initialValue.filter(file => file.fileId !== activeFile.fileId);

        fileData = {
            ...elementData,
            existingFiles: initialValue,
            removedFiles: [...removedFiles, activeFile]
        };
        this.props.form.setFieldsValue({[element.name]: [...initialValue,...addedFiles]});
        this.setState({showConfirmModal: false}, () => {
            setFileDataCB && setFileDataCB(fileData);
        })
    };

    toggleConfirmationModal = () => {
        this.setState({showConfirmModal: !this.state.showConfirmModal});
    };

    renderConfirmationModal = () => {

        let {activeFile} = this.state;

        if (!activeFile) return null;
        let fileName = activeFile.name.split(".pdf");

        return (
            <DeleteConfirmation isOpen={this.state.showConfirmModal}
                                okCB={this.handleDeleteFileClick} cancelCB={this.toggleConfirmationModal}
                                fileTitle={fileName[0]}/>
        );

    };


    handleDownloadPreviewClick = (isPreview = false, jsonFile) => {
        let {projectDetails} = this.props;
        let data = {
            downloadURL: `/project-file-management/download/${projectDetails.projectId}/file/${jsonFile.fileId}`,
        };

        let dataProps = {
            projectId: projectDetails.projectId,
            phaseId: projectDetails.phaseId,
            data,
            isPreview: isPreview,
            title: jsonFile.fileName || "File"
        };

        previewDownloadFile(dataProps);
    };


    /*************************** END  *****************************/

    getFileViews = () => {
        const jsonFiles = this.props.initialValue;
        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            marginRight: "20px",
            marginBottom: "20px",
        };

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width: "100%"
        };

        let canEdit = this.props.editRights;

        let jsonFileJSX = jsonFiles.map((file, index) => {
            let fileName = file.name.split(".pdf");
            return (
                <Col key={index} span={7} style={{overflowWrap: "break-word", minWidth: "140px", marginRight: "20px"}}>

                    <Col span={24} style={boxStyle}>
                        <div style={divStyle}>

                            <img alt={"Drawing"} src={`/fileIcons/pdf.svg`} width={"70px"}/>
                            <p style={{fontWeight: "normal", fontSize: "18px", width: "100%", marginTop: "5px"}}>
                                {fileName[0] || "File"}
                            </p>

                        </div>
                    </Col>

                    <div span={24} style={{textAlign: "center"}}>


                        <Button type={"primary"} shape={"circle"} icon={"file-pdf"}
                                onClick={() => this.handleDownloadPreviewClick(true, file)}/>

                        <Button type={"primary"} shape={"circle"} icon={"download"} style={{marginLeft: "10px"}}
                                onClick={() => this.handleDownloadPreviewClick(false, file)}/>

                        {
                            canEdit ?
                                <Button type={"danger"} shape={"circle"} icon={"delete"} style={{marginLeft: "10px"}}
                                        onClick={() => this.setState({
                                            activeFile: file,
                                            showConfirmModal: true
                                        })}/> : null
                        }

                    </div>
                </Col>
            )
        });


        return (
            <Row className={"mb-3"}>
                <Col span={24}>
                    {this.renderConfirmationModal()}

                    {jsonFileJSX}
                </Col>
            </Row>
        );
    };

    render() {

        return (
            <div>
                {this.getFileViews()}
            </div>
        )
    }
}

export default FileViews