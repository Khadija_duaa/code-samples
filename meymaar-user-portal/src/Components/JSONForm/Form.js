import React from 'react';
import {getFormInputs, getFormPreview} from "../../store/form/form_actions";
import {connect} from "react-redux";
import JsonForm from './JsonForm';

class Form extends React.Component{

    // getLocationState = () => {
    //     let state = {};
    //     let history = this.props.history;
    //     if (history && history.location && history.location.state) {
    //         state = history.location.state;
    //     }
    //     return state;
    // };

    componentWillMount(){

        // const {formURL} = this.getLocationState();
        //
        // if(!formURL){
        //     this.props.history.push("/");
        // }else {
        //     this.props.loadFormData(formURL);
        // }


        this.props.loadFormData('');

    }


    render(){

        let formData = this.props.jsonFields;
        let is_formJSON = Object.keys(formData).length > 0;

        const previewJSON = {};// this.props.previewJSON;

        const handleSubmit = (formData) => {
            console.log(formData);
            formData.preventDefault();
        };

        return(

            <div>
                {is_formJSON?<JsonForm previewJSON={previewJSON} formJSON={formData} onSubmit={handleSubmit}/>:''}
                {/*<JsonForm formJSON={json} previewJSON={previewJSON} onSubmit={handleSubmit}/>*/}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        jsonFields: state.form_reducer.jsonFields,
        previewJSON: state.form_reducer.previewJSON
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadFormData: (url) => dispatch(getFormInputs(url)),
        previewData: () => dispatch(getFormPreview())
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Form);

export default connected;