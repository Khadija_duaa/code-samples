import React from "react";
import {connect} from "react-redux";
import moment from "moment";
import {withRouter} from "react-router-dom";

import { Form, Input, Radio, Select, Checkbox, Button, DatePicker, InputNumber,
    TimePicker } from "antd";
import "antd/dist/antd.css";

import { BUTTON_COLOR, isValidFileObjects } from '../../utils/common-utils';
import { fileProcessor, isFieldExist,getDynamicSelectURL} from '../../utils/json-form-utils';

import DynamicSelect from "../Dynamic/DynamicSelect/DynamicSelect";
import GridTable from "../GridTable/GridTable";

import { createProject, submitFormData } from "../../store/project/project-actions";
import JsonUpload from "../JsonUpload/JsonUpload";
import WizardTable from '../Table/WizardTable/WizardTable';

import BQForm from '../CustomForms/BOQ/BQForm';
import Drawings from '../CustomForms/Drawings/Drawings';
import TakingOffSheet from '../CustomForms/Revitt/TOS/TakingOffSheet';

import NewLoader from "../../Containers/Loader/NewLoader";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const {Option} = Select;
const {TextArea} = Input;

let col = null;

class JsonForm extends React.Component {
    constructor(props) {
        super(props);

        let key = props.formJSON && props.formJSON.pages[0].fields[0].name;
        let keyLength = props.data && Object.keys(props.data).length;
        let notDataExist =
            props.data &&
            ((keyLength === 1 && key && !props.data[key]) || !keyLength);

        this.state = {
            disabled: props.projectProps.isDisabled,
            isEmpty: props.projectProps.isComplete && notDataExist,
            removedFiles: [],
            fileData: {}
        };
    }

    componentDidMount() {
        if (this.props.projectProps.data) {
            let fields = ["datepicker", "timePicker"];
            let otherFields = this.getDateTimeFields(fields);
            let data = {...this.props.projectProps.data};
            if (otherFields.length) {
                otherFields.forEach(field => {
                    delete data[field];
                });
            }

            this.props.form.setFieldsValue(data);
        }
    }

    /*********************************** EVENTS & Consts *********************************/
    formItemLayout = {
        labelCol: {span: 6},
        wrapperCol: {span: 8}
    };

    buttonItemLayout = {
        wrapperCol: {offset: 6}
    };

    submitJSONForm = (formData)=>{
        let {projectDetails,formJSON} = this.props.projectProps;

        let successMessage = "Form has submitted successfully";
        let formSubmit = this.props.projectProps.onFormSubmit;

        this.props.submitFormData(
          projectDetails.projectId,
          projectDetails.phaseId,
          formJSON,
          formData,
          successMessage,
          formSubmit && formSubmit
        );
    };

    handleFormSubmit = (e,formData= null) => {
        // e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {
            if (err) {
                return;
            }

            let values = formData ? {...formData}:{...fieldsValue};

            let {formJSON, successMessage, successCB} = this.props.projectProps;
            let {fileData} = this.state;
            let {projectDetails} = this.props.projectProps;
            if(Object.keys(fileData).length){
                fileProcessor(projectDetails.projectId,fileData,(data = null)=>{

                    values = data? {...values,...data} : {...values};
                    this.submitJSONForm(values);
                });
            }
            else if (formJSON.formName === "CREATE_WORK_FORM") {
                this.props.createProject(
                    formJSON.submitURL,
                    values,
                    successMessage,
                    projectId => {
                        this.props.history.push({
                            pathname: successCB,
                            state: {category: "Planing", projectId}
                        });
                    }
                );
            } else {
               this.submitJSONForm(values)
            }
        });
    };

    getDateTimeFields = fields => {
        let otherFields = [];
        let formFields = this.props.projectProps.formJSON.pages[0].fields;
        for (let index = 0; index < formFields.length; index++) {
            let item = formFields[index];
            if (fields.includes(item.element)) {
                otherFields.push(item.elementProps.name);
            }
        }
        return otherFields;
    };

    /*********************************** END *********************************/

    /*********************************** Rendering Form Elements  *********************************/

    renderLabel = (element, key) => {
        return (
            <FormItem key={key}>
                <p className={"formLabel"}>{element.label}</p>
            </FormItem>
        );
    };

    renderInput = (element, key, getFieldDecorator) => {
        return !element.multiple ? (
            <FormItem key={key} label={element.label} {...this.formItemLayout}>
                {getFieldDecorator(`${element.elementProps.name}`, {
                    rules: [
                        {
                            required: true,
                            message: `Required!`
                        }
                    ]
                })(<Input disabled={this.state.disabled} {...element.elementProps} />)}
            </FormItem>
        ) : null;
    };

    renderInputNumber = (element, key, getFieldDecorator) => {
        return !element.multiple ? (
            <FormItem key={key} label={element.label} {...this.formItemLayout}>
                {getFieldDecorator(`${element.elementProps.name}`, {
                    rules: [
                        {
                            required: true,
                            message: `Required!`
                        }
                    ]
                })(
                    <InputNumber
                        disabled={this.state.disabled}
                        style={{width: "100%"}}
                        {...element.elementProps}
                    />
                )}
            </FormItem>
        ) : null;
    };

    renderPercentageField = (element,key, getFieldDecorator)=>{
        return (
          <FormItem key={key} label={element.label} {...this.formItemLayout}>
              {getFieldDecorator(`${element.elementProps.name}`, {
                  rules: [
                      {
                          required: true,
                          message: `Required!`
                      }
                  ]
              })(
                <InputNumber
                  style={{width:"100%"}}
                  disabled={this.state.disabled}
                  min={0}
                  max={100}
                  formatter={value => `${value}%`}
                  parser={value => value.replace('%', '')}
                />
              )}
          </FormItem>
        );
    };
    renderTextArea = (element, key, getFieldDecorator) => {
        return (
            <FormItem key={key} label={element.label} {...this.formItemLayout}>
                {getFieldDecorator(`${element.elementProps.name}`, {
                    rules: [
                        {
                            required: true,
                            message: `Required!`
                        }
                    ]
                })(
                    <TextArea disabled={this.state.disabled} {...element.elementProps} />
                )}
            </FormItem>
        );
    };

    renderSelect = (element, key, getFieldDecorator) => {
        const options = options =>
            options.map((option, index) => {
                return (
                    <Option key={index} name={option} value={option.value}>
                        {option.name}
                    </Option>
                );
            });

        return (
            <FormItem key={key} label={element.label} {...this.formItemLayout}>
                {getFieldDecorator(`${element.elementProps.name}`, {
                    rules: [
                        {
                            required: true,
                            message: `Required!`
                        }
                    ]
                })(
                    <Select disabled={this.state.disabled} {...element.elementProps}>
                        {options(element.elementProps.options)}
                    </Select>
                )}
            </FormItem>
        );
    };

    setInitialCB = (name, value) => {
        this.props.form.setFieldsInitialValue({[name]: value});
    };
    renderDynamicSelect = (element, key, getFieldDecorator) => {
        let {isCreateWork} = this.props.projectProps;
        let apiURL = getDynamicSelectURL(element.elementProps.name);
        let {elementProps} = element;
        elementProps.optionURL = apiURL || elementProps.optionURL;

        return (
            <FormItem key={key} label={element.label} {...this.formItemLayout}>
                {getFieldDecorator(elementProps.name, {
                    rules: [
                        {
                            required: true,
                            message: `Required!`
                        }
                    ]
                })(
                    <DynamicSelect
                        disabled={this.state.disabled}
                        {...elementProps}
                        isCreateWork={isCreateWork}
                        setInitialCB={value =>
                            this.setInitialCB(elementProps.name, value)
                        }
                    />
                )}
            </FormItem>
        );
    };

    renderRadio = (element, key, getFieldDecorator) => {
        let radioButtons = radioOptions =>
            radioOptions.map((radio, index) => {
                return (
                    <Radio key={index} value={radio}>
                        {radio}
                    </Radio>
                );
            });

        return (
            <FormItem key={key} {...this.formItemLayout}>
                {getFieldDecorator(`${element.elementProps.name}`, {
                    rules: [
                        {
                            required: true,
                            message: `Required!`
                        }
                    ]
                })(
                    <RadioGroup disabled={this.state.disabled} {...element.elementProps}>
                        {radioButtons(element.elementProps.radioOptions)}
                    </RadioGroup>
                )}
            </FormItem>
        );
    };

    renderCheckbox = (element, key, getFieldDecorator) => {
        return (
            <FormItem key={key} label={element.label} {...this.formItemLayout}>
                {getFieldDecorator(`${element.elementProps.name}`, {
                    rules: [
                        {
                            required: true,
                            message: `Required!`
                        }
                    ]
                })(
                    <Checkbox disabled={this.state.disabled} {...element.elementProps} />
                )}
            </FormItem>
        );
    };

    renderDatePicker = (element, key, getFieldDecorator) => {
        let {data} = this.props;

        let configRules = {
            rules: [
                {
                    required: true,
                    message: "Required!"
                }
            ]
        };
        if (data && data[element.elementProps.name]) {
            configRules.initialValue = moment(data[element.elementProps.name]);
        }
        return (
            <Form.Item key={key} {...this.formItemLayout} label={element.label}>
                {getFieldDecorator(`${element.elementProps.name}`, configRules)(
                    <DatePicker
                        disabled={this.state.disabled}
                        {...element.elementProps}
                        format={element.elementProps.dateFormat}
                        onChange={(date, dateString) =>
                            this.setState({[element.elementProps.name]: dateString})
                        }
                    />
                )}
            </Form.Item>
        );
    };

    renderTimePicker = (element, key, getFieldDecorator) => {
        let {data} = this.props;

        let configRules = {
            initialValue:
                data && data[element.name] ? moment(data[element.name]) : moment(),
            rules: [
                {
                    type: "object",
                    required: true,
                    message: "Required!"
                }
            ]
        };

        return (
            <Form.Item key={key} {...this.formItemLayout} label={element.label}>
                {getFieldDecorator(`${element.elementProps.name}`, configRules)(
                    <TimePicker
                        style={{width: "100%"}}
                        disabled={this.state.disabled}
                        {...element.elementProps}
                        format={element.elementProps.timeFormat}
                        onChange={(date, timeString) =>
                            this.setState({[element.elementProps.name]: timeString})
                        }
                    />
                )}
            </Form.Item>
        );
    };

    renderTable = (element, key, getFieldDecorator) => {
        let {name, columns} = element.elementProps;
        let {disabled} = this.state;

        if (!col && columns) {
            col = columns;
        }

        delete element.elementProps["columns"];
        return (
            <FormItem key={key}>
                {getFieldDecorator(name, {initialValue: this.props.projectProps.data[name]})(
                    <GridTable
                        tableCol={col}
                        disabled={disabled}
                        isMultiple={element.multiple}
                        {...element.elementProps}
                    />
                )}
            </FormItem>
        );
    };

    // Rendering File Views
    validateFile = (rule, value, callback) => {
        let fileExt = ["pdf"];
        if (value) {
            let {size} = value;
            let fileObjects = isValidFileObjects(value, fileExt);

            if (!fileObjects.isValidSize) {
                callback("Maximum file size limit is: 300 MB");
            } else if (!fileObjects.isValidFormat) {
                callback(`Invalid file format provided`);
            } else {
                callback();
            }
        } else {
            callback();
        }
    };
    renderFilesView = (element, key, getFieldDecorator) => {
        const parentRef = this;

        let {name} = element;
        let {form} = this.props;
        let {data} = this.props.projectProps;
        let {fileData} = this.state;
        let initialValue = (fileData[element.name] && fileData[element.name].existingFiles) || data[element.name];

        let {projectDetails} = this.props.projectProps;
        let uploadProps = {
            form,
            element,
            initialValue,
            editRights: !this.state.disabled,
            fileData,
            setRemoveFileCB: (files) => this.setState({removedFiles: files}),
            setFileDataCB : (uploadFileData)=>{
                let removedFiles = [...uploadFileData.removedFiles] || [];
                let existingFiles = [...uploadFileData.existingFiles] || [...initialValue];

                let files = {...uploadFileData,existingFiles,removedFiles};

                this.setState((prevState)=>{
                    return prevState.fileData = {...prevState.fileData,[element.name]:{...files}}
                })
            },
            projectDetails
        };

        return (
            <FormItem key={key} label={element.label} {...this.buttonItemLayout} >
                {getFieldDecorator(name, {
                    rules: [{
                        required: true,
                        message: "File is required!"
                    }, {validator: this.validateFile}]
                })(
                    <JsonUpload fileProps={{...uploadProps}}/>
                )}
            </FormItem>
        );
    };


    /*********************************** END ****************************************/

      // Rendering Custom Views
      renderCustomComponents = (field,index,getFieldDecorator)=>{
          let {name} = field.elementProps;

          switch (name) {
              case 'pe_checklist':
              case 'sbp_user':
              case 'sbp_sb':
                  let wizardProps = {
                      elementName:name,
                      onSubmit: (e,wizardValue) =>this.handleFormSubmit(e,wizardValue),
                      ...this.props
                  };
                  return <WizardTable key={index} {...wizardProps}/>;
              case 'bqs':
                  return <BQForm projectProps = {this.props.projectProps}/>;
              case 'drawings':
                  return  <Drawings projectProps = {this.props.projectProps}/>;
              case 'tos':
                  return <TakingOffSheet projectProps={this.props}/>;
              case 'firms':

                  break;
              case 'ctc':
                  break;
              case 'bids':
                  break;
              case 'readonly_file':
                  break;
          }
      };

      // Rendering Form Elements on the basis of element type
    renderPage = (field, index) => {
        const {getFieldDecorator} = this.props.form;

        switch (field.element) {
            case "label":
                return this.renderLabel(field, index);
            case "input":
                return this.renderInput(field, index, getFieldDecorator);
            case "number":
                return this.renderInputNumber(field, index, getFieldDecorator);
            case "percentage":
                return this.renderPercentageField(field, index, getFieldDecorator);
            case "textArea":
                return this.renderTextArea(field, index, getFieldDecorator);
            case "select":
                return this.renderSelect(field, index, getFieldDecorator);
            case "dynamicSelect":
                return this.renderDynamicSelect(field, index, getFieldDecorator);
            case "radio":
                return this.renderRadio(field, index, getFieldDecorator);
            case "checkbox":
                return this.renderCheckbox(field, index, getFieldDecorator);
            case "table":
                return this.renderTable(field, index, getFieldDecorator);
            case "datepicker":
                return this.renderDatePicker(field, index, getFieldDecorator);
            case "timePicker":
                return this.renderTimePicker(field, index, getFieldDecorator);
            case "file":
                return this.renderFilesView(field, index, getFieldDecorator);
            case "custom":
                return this.renderCustomComponents(field,index,getFieldDecorator);
            default:
                return null;
        }
    };


    handleResetClick = () => {
        this.props.form.resetFields();
        this.setState({removedFiles: []})
    };
    // Rendering Form Submit And Reset Button
    renderButtons = () => {
        let {disabled} = this.state;
        if (disabled) return null;
        let formFields = this.props.projectProps.formJSON.pages[0].fields;
        let isCustomExist = isFieldExist(formFields,'custom');
        let isLabelExist = isFieldExist(formFields,'label');
        let isTableExist = isFieldExist(formFields,'table');

        if(isCustomExist) return null;

        let buttonsJSX = (
            <>
                <Button
                    onClick={this.handleFormSubmit}
                    style={{
                        height: "50px",
                        width: "120px",
                        backgroundColor: BUTTON_COLOR,
                        color: "#fff"
                    }}
                >
                    Submit
                </Button>

                {!isTableExist ? (
                    <Button
                        style={{
                            height: "50px",
                            width: "120px",
                            marginLeft: "2%",
                            borderColor: BUTTON_COLOR
                        }}
                        type={"default"}
                        onClick={this.handleResetClick}
                    >
                        Reset
                    </Button>
                ) : null}
            </>
        );

        if (isTableExist || isLabelExist) {
            return <Form.Item>{buttonsJSX}</Form.Item>;
        }
        return <Form.Item {...this.buttonItemLayout}>{buttonsJSX}</Form.Item>;
    };

    // Json Form Data
    renderFormData = () => {
        let {formJSON} = this.props.projectProps;
        let {isEmpty, disabled} = this.state;

        if (isEmpty && disabled) {
            return <h5 style={{textAlign: "center"}}>--- No Data Exists ---</h5>;
        }

        let formJSX = formJSON.pages[0].fields.map((item, index) => {
            return this.renderPage(item, index);
        });

        return (
            <Form layout={"horizontal"} id={formJSON._id}>
                {formJSX}
                {this.props.processing ? <NewLoader/> : null}
                {this.renderButtons()}
            </Form>
        );
    };

    // Render Method of JSON Form
    render() {
        return (
            <div>
                {this.renderFormData()}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        createProject: (url, formJSON, message, successCB) =>
            dispatch(createProject(url, formJSON, message, successCB)),
        submitFormData: (projectId, phaseId, form, formData, message, successCB) =>
            dispatch(
                submitFormData(projectId, phaseId, form, formData, message, successCB)
            )
    };
};

const mapStateToProps = state => {
    return {
        processing: state.project_reducer.processing
    };
};

const createForm = Form.create()(JsonForm);
const connected = connect(
    mapStateToProps,
    mapDispatchToProps
)(createForm);

export default withRouter(connected);
