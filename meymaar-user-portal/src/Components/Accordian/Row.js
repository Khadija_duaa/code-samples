import React, {Fragment} from 'react';
import {NavLink} from 'react-router-dom';
import {formatDate} from "../../utils/common-utils";
import NewLoader from "../../Containers/Loader/NewLoader";

class Row extends React.Component {

    /******************* EVENTS ********************/


    /***********************************************/

    getNamePanel = (marking) => {
        return (
            <div className="col-4">
                <ul className="name-pannel">
                    <li>
                        <strong>Initiated By: </strong>
                        {marking.initiated_by_role_title ? `${marking.initiated_by_role_title}` : "N/A"}
                    </li>
                    <li>
                        <strong>Date Initiated: </strong>
                        {marking.initiated_date ? formatDate(marking.initiated_date, "DD-MM-YYYY") : "N/A"}
                    </li>
                </ul>
            </div>
        )
    };

    getRequestPanel = (marking) => {
        let {task} = this.props;
        let reasonText = task === "Inbox"?"Initial Receipt":"Reason:";
        return (
            <div className="col-5">
                <ul className="request-pannel name-pannel">
                    <li><strong>{reasonText}</strong> <span>{marking.reason ? marking.reason : "N/A"}</span></li>
                    <li><strong>Requested By:</strong>
                        <span> {marking.marked_by_name ? marking.marked_by_name : "N/A"} </span>
                    </li>
                    <li><strong>Date Requested:</strong> <span> {formatDate(this.props.date, "DD-MM-YYYY")} </span></li>
                </ul>
            </div>
        )
    };

    getLinkPanel = (marking, projectId) => {

        let {task} = this.props;

        let taskJSX = '';


        let backText = "Taskboard";
        let backCB = "/dashboard/task-board";

        if (task === 'Inbox') {
            taskJSX = <a href={"javascript:;"} onClick={this.props.markingCB}>
                Assign Project To User
            </a>
        } else {
            taskJSX = <NavLink to={{pathname: "/dashboard/works/overview", state: {projectId, category: "Planing",backTitle:backText,backCB:backCB}}}>
                Review Work
            </NavLink>
        }

        return (
            <div className="col-3">
                <ul className="link-pannel">
                    <li>
                        {taskJSX}
                    </li>
                    <li>
                        <NavLink to={{
                            pathname: "/dashboard/works/marking-history",
                            state: {
                                projectId,
                                projectName: this.props.name,
                                returnCB: task==="Inbox"?"/dashboard/inbox-works":"/dashboard/task-board"
                            }
                        }}>
                            Marking History
                        </NavLink>
                    </li>
                </ul>
            </div>
        )

    };


    renderTaskDetailBox = () => {
        let {marking, projectId} = this.props;

        return (
            <div className=" project-detail-box container-fluid">
                <div className="row">

                    {this.getNamePanel(marking)}

                    {this.getRequestPanel(marking)}

                    {this.getLinkPanel(marking, projectId)}

                </div>
            </div>
        )
    };

    renderComments = () => {
        let {public_comment} = this.props.marking;

        if (!public_comment) return null;

        return (
            <div className="comments">
                <p><strong>Comments:</strong></p>
                <p>{this.props.marking.public_comment}</p>
            </div>
        )
    };


    renderMessage = () => {

        let {private_comment,id} = this.props.marking;


        if (!private_comment) return null;


        return (
            <div className="message">
                <p>
                    <strong>
                        <i style={{marginRight: "10px"}} className="fas fa-quote-left"/>
                        Private Message
                    </strong>
                    <span>
                        <a href="javascript:void(0);" onClick={()=>this.props.deleteCB(id)}>
                            <i style={{marginRight: "10px"}} className="fas fa-trash-alt"/>
                            Delete
                        </a>
                    </span>
                </p>

                <p id="msg-detail">
                    {this.props.marking.private_comment}
                </p>
            </div>
        )
    };

    renderCommentMessageBox = () => {
        let {task} = this.props;

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className=" cmt-msg-block col-12">
                        {this.renderComments()}
                        {task !== 'Inbox'?this.renderMessage():null}
                    </div>
                </div>
            </div>
        )
    };

    getAccordianData = () => {
        return (
            <tr>
                <td colSpan="6" id="collaps-msg">

                    {this.renderTaskDetailBox()}
                    {this.renderCommentMessageBox()}
                </td>
            </tr>
        )
    };


    // Rendering Method
    render() {
        let {children, onClick, isAccordian, style, processing} = this.props;

        let className = '';
        if (isAccordian) {
            className = 'selected-row';
        }

        return (
            <Fragment>
                <tr className={className} style={style} onClick={onClick}>
                    {children && children}
                </tr>

                {
                    processing && className==='selected-row' ? <tr>
                        <td colSpan="6" id="collaps-msg">
                            <NewLoader/></td>
                    </tr> : isAccordian ?
                        this.getAccordianData()
                        :
                        null
                }
            </Fragment>
        )
    }
}


export default Row;