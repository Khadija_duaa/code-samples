import React, {Component} from 'react';
import DhtmlXGantt from './DhtmlXGantt/DhtmlXGantt';
import Toolbar from './Toolbar/Toolbar';
import {connect} from 'react-redux';

import {changeActivityStatus, fetchGroupedActivities} from "../../../utils/gantt-server-utils";
import NewLoader from "../../../Containers/Loader/NewLoader";
import {SERVER_URL} from "../../../utils/config";


import './Toolbar/Toolbar.css';
import './DhtmlXGantt/DhtmlXGantt.css'
import ConfirmationModal from "../../ModalFactory/MessageModal/ConfirmationModal";

class DhtmlXGanttChart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentZoom: 'Days',
            messages: [],
            activitiesData: null,
            processing: false,
            showConfirmModal: false,
            width:0,
            height:0
        }
    }

    // Getting Session State
    getSessionState = () => {
        let storageKey = '';
        let chartItems = sessionStorage.getItem(storageKey);

        if (chartItems) {
            chartItems = JSON.parse(chartItems);
        }

        let historyParams = this.props.match.params;
        let {activeUser, projectDetails} = this.props;
        let token = (activeUser && activeUser.token) || historyParams.token;
        let projectId = (projectDetails && projectDetails.projectId) || historyParams.projectId;
        let url = new URL(window.location);
        if (historyParams.token) {
            let splited = url.origin.split(':');
            url = splited[0] + ":" + splited[1] + ":8080"
        }

        let serverURL = historyParams.token ? url : SERVER_URL;
        chartItems = {token, projectId, serverURL};

        sessionStorage.setItem(storageKey, JSON.stringify(chartItems));
        return chartItems;
    };


    componentDidMount() {
        let historyParams = this.props.match.params;
        let {activeUser, projectDetails} = this.props;

        if (activeUser && !projectDetails) {
            this.props.history.push('/dashboard/works/overview');
        } else if (activeUser || (historyParams.projectId && historyParams.token)) {
            this.fetchGroupedActivities();
        }
        // this.updateWindowDimensions();
        // window.addEventListener("resize", this.updateWindowDimensions);
    }
    // componentWillUnmount() {
    //     window.removeEventListener("resize", this.updateWindowDimensions);
    // }
    //
    // updateWindowDimensions = () => {
    //     this.setState({ width: window.innerWidth, height: window.innerHeight });
    // };

    fetchGroupedActivities = () => {
        let chartState = this.getSessionState();
        console.log(chartState);
        fetchGroupedActivities(chartState.serverURL, chartState.projectId, chartState.token, (processing) => {
            this.setState({processing, showConfirmModal: false});
        }, (activities) => {
            this.setState({activitiesData: activities, showConfirmModal: false})
        })
    };
    /***************************** Modal ************************************/

    toggleConfirmationModal = () => {
        this.setState({showConfirmModal: !this.state.showConfirmModal})
    };

    handleConfirmClick = () => {
        let chartItems = this.getSessionState();
        let {activityStatus} = this.state.activitiesData;
        let status = activityStatus === "PENDING" ? "APPROVED" : "REJECTED";

        changeActivityStatus(chartItems.serverURL, chartItems.projectId, chartItems.token, status, (processing) => {
            this.setState({processing})
        }, () => {
            this.fetchGroupedActivities();
        })
    };


    renderConfirmationModal = () => {
        let {activityStatus} = this.state.activitiesData;

        let message = `Are you sure, you want to ${activityStatus === 'PENDING' ? "approve" : "reject"} work program timeline?`;

        return (
            <ConfirmationModal isOpen={this.state.showConfirmModal}
                               okCB={this.handleConfirmClick} cancelCB={this.toggleConfirmationModal}
                               message={message}/>
        );
    };

    /***************************** END **************************************/

    /**************************** Events ***********************************/
        // Zoom Change Function to set current zoom ( Days, Week, Month & Year)
    handleZoomChange = (zoom) => {
        this.setState({
            currentZoom: zoom
        });
    };


    // Rendering Header Tool Bar which has Zoom & Approve / RejectClick
    renderHeaderBar = () => {

        const {currentZoom, activitiesData} = this.state;
        return (
            <div className="zoom-bar">
                <Toolbar
                    zoom={currentZoom}
                    onZoomChange={this.handleZoomChange}
                    {...activitiesData}
                    onApproveRejectClick={this.toggleConfirmationModal}
                />
            </div>
        )
    };

    // Rendering Body which has Gantt Chart
    renderGanttChart = () => {
        const {currentZoom, activitiesData} = this.state;

        return (

            <div className="gantt-container">
                <DhtmlXGantt
                    readOnly={true}
                    tasks={activitiesData.activities}
                    zoom={currentZoom}
                />
            </div>
        )
    };

    /**************************** End ***********************************/



    render() {
        if (this.state.processing) return <NewLoader/>;

        if (!this.state.activitiesData) return null;

        console.log(this.state.width,this.state.height);
        return (
            <div>
                {this.renderConfirmationModal()}
                {this.renderHeaderBar()}
                {this.renderGanttChart()}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        projectDetails: state.project_reducer.projectDetails,
        activeUser: state.user_reducer.activeUser
    }
};

const connected = connect(mapStateToProps)(DhtmlXGanttChart);
export default connected;
