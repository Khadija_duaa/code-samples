import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Button} from 'antd';

class Toolbar extends Component {
    handleZoomChange = (e) => {
        if (this.props.onZoomChange) {
            this.props.onZoomChange(e.target.value)
        }
    };


    renderZoomButtons = () => {
        const zoomRadios = ['Days', 'Months', 'Year'].map((value) => {
            const isActive = this.props.zoom === value;
            return (
                <label key={value} className={`radio-label ${isActive ? 'radio-label-active' : ''}`}>
                    <input type='radio'
                           checked={isActive}
                           onChange={this.handleZoomChange}
                           value={value}/>
                    {value}
                </label>
            );
        });


        return (
            <div style={{width: "50%"}}>
                <b style={{fontSize: "20px"}}>Zoom: </b>
                {zoomRadios}
            </div>

        )
    };


    renderApproveRejectButtons = () => {
        let {onApproveRejectClick, isContractor, activityStatus} = this.props;
        let isApproved = activityStatus === 'APPROVED';

        if (isContractor) return null;

        return (
            <div style={{width: "50%", textAlign: "right"}}>
                <Button className={isApproved ? `reject-button` : "custom-button-dark"} onClick={onApproveRejectClick}
                        style={{marginTop: "unset", marginRight: "20px", height:"40px"}}>
                    {activityStatus === 'PENDING' ? "Approve Timeline" : "Reject Timeline"}
                </Button>
                <Button icon={"rollback"} style={{display: "inline",height:"40px"}}
                        onClick={() => this.props.history.push('/dashboard/works/overview')}>Back</Button>
            </div>
        )
    };


    render() {

        return (
            <div className="tool-bar" style={{display: "flex", width: "100%"}}>
                {this.renderZoomButtons()}
                {this.renderApproveRejectButtons()}
            </div>
        );
    }
}

export default withRouter(Toolbar)