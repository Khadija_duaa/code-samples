import React, {Component} from 'react';
import {gantt} from 'dhtmlx-gantt';
import 'dhtmlx-gantt/codebase/ext/dhtmlxgantt_tooltip';
import 'dhtmlx-gantt/codebase/dhtmlxgantt.css';
import moment from "moment";

export default class DhtmlXGantt extends Component {

    // instance of gantt.dataProcessor
    dataProcessor = null;


    componentDidMount() {


        this.modifyGanttConfigurations();

        // console.log(gantt);
        // console.log(gantt.config);

        const {tasks} = this.props;

        gantt.init(this.ganttContainer);
        gantt.parse(tasks);
    }

    modifyGanttConfigurations = () => {
        // XML Date Configuration
        gantt.config.xml_date = "%Y-%m-%d %H:%i";



        // Disabling Editing on drag and edit
        gantt.config.details_on_create = false;
        gantt.config.drag_progress = false;

        gantt.config.drag_move = false;
        gantt.config.drag_lightbox = false;
        gantt.config.drag_links = false;

        gantt.config.drag_resize = false;
        gantt.config.readonly = true;
        gantt.config.details_on_dblclick = false;


        gantt.config.task_height = 40;
        gantt.config.row_height = 50;

        gantt.config.open_split_tasks = true;

        gantt.templates.tooltip_text = (start, end, task) => {
            return "<b>Task:</b> " + task.text + "<br/><b>Duration:</b> " + task.duration;
        };

        gantt.config.columns = [
            {
                name: "text",
                label: "Task Name",
                tree: true,
                resize: true,
                template: (obj) => "<b>" + obj.text + "</b>"
            },
            {name: "start_date", label: "Start Date", align: "center",template:(obj)=>moment(obj.start_date).format("DD-MM-YYYY")},
            {name: "end_date", label: "End Date", align: "center",template:(obj)=>moment(obj.end_date).format("DD-MM-YYYY")},
            {name: "duration", label: "Duration", align: "center"},
        ];

        gantt.config.layout = {
            css: "gantt_container",
            cols: [
                {
                    width:550,
                    min_width: 550,
                    // adding horizontal scrollbar to the grid via the scrollX attribute
                    rows:[
                        {view: "grid", scrollX: "gridScroll", scrollable: true, scrollY: "scrollVer"},
                        {view: "scrollbar", id: "gridScroll"}
                    ]
                },
                {
                    rows:[
                        {view: "timeline", scrollX: "scrollHor", scrollY: "scrollVer"},
                        {view: "scrollbar", id: "scrollHor"}
                    ]
                },
                {view: "scrollbar", id: "scrollVer"}
            ]
        };
    };


    shouldComponentUpdate(nextProps) {
        return this.props.zoom !== nextProps.zoom;
    }

    componentDidUpdate() {
        gantt.render();
    }

    componentWillUnmount() {
        if (this.dataProcessor) {
            this.dataProcessor.destructor();
            this.dataProcessor = null;
        }
    }

    /**
     * applies one of the predefined settings of the time scale
     */
    setZoom = (value) => {
        switch (value) {
            case 'Hours':
                gantt.config.scale_unit = 'day';
                gantt.config.date_scale = '%d %M';

                gantt.config.scale_height = 60;
                gantt.config.min_column_width = 30;
                gantt.config.subscales = [
                    {unit: 'hour', step: 1, date: '%H'}
                ];
                break;
            case 'Days':
                gantt.config.min_column_width = 70;
                gantt.config.scale_unit = 'week';
                gantt.config.date_scale = '#%W';
                gantt.config.subscales = [
                    {unit: 'day', step: 1, date: '%d %M'}
                ];
                gantt.config.scale_height = 60;
                break;
            case 'Months':
                gantt.config.min_column_width = 70;
                gantt.config.scale_unit = 'month';
                gantt.config.date_scale = '%F';
                gantt.config.scale_height = 60;
                gantt.config.subscales = [
                    {unit: 'week', step: 1, date: '#%W'}
                ];
                break;
            case 'Year':
                gantt.config.min_column_width = 70;
                gantt.config.scale_unit = 'year';
                gantt.config.date_scale = '%Y';
                gantt.config.scale_height = 60;
                gantt.config.subscales = [
                    {unit: 'month', step: 1, date: '#%F'}
                ];
                break;
            default:
                break;
        }
    };


    render() {
        const {zoom} = this.props;
        this.setZoom(zoom);
        return (
            <div
                ref={(input) => {
                    this.ganttContainer = input
                }}
                style={{width: '100%', height: '100%'}}
            />
        );

    }
}