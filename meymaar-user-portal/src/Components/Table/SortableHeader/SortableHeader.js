import React from 'react';

class SortableHeader extends React.Component {
    render() {
        let {tableHeaders, getSortedLayout} = this.props;

        let headerJSX = tableHeaders.map((head, index) => {
            return (
                index < tableHeaders.length - 1 ?
                    <th key={index} scope={"col"}>{getSortedLayout(head)}</th>
                    :
                    <th key={index} scope={"col"} colSpan={"2"}>{getSortedLayout(head)}</th>
            )
        });

        return (
            <thead className="thead-dark">
                <tr>
                    {headerJSX}
                </tr>
            </thead>
        )
    }
}

export default SortableHeader