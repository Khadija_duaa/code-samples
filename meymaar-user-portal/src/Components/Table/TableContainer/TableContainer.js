import React from 'react';

class TableContainer extends React.Component{
    render(){
        return(
            <div className={"container-fluid"}>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <div className=" custom-table table-responsive">
                            <table className=" collapse-table table border-0 ">
                                {this.props.children && this.props.children}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default TableContainer;