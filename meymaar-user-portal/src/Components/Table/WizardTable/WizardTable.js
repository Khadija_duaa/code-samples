import React from 'react';
import GridTable from "../../GridTable/GridTable";
import {Form, Button} from 'antd';
import {connect} from "react-redux";
import {submitFormData} from "../../../store/project/project-actions";
import {withRouter} from "react-router-dom";
import {BUTTON_COLOR} from "../../../utils/common-utils";
import Questionnaire from "../../../Components/Popup/QuestionnaireForm";
import NewLoader from "../../../Containers/Loader/NewLoader";

const FormItem = Form.Item;

let col = null;

class WizardTable extends React.Component {

    constructor(props) {
        super(props);
        let {name} = props.projectProps.formJSON.pages[0].fields[0];

        let keyLength = props.projectProps.data && Object.keys(props.projectProps.data).length;
        let notDataExist = props.projectProps.data && ((keyLength === 1 && name && !props.projectProps.data[name]) || !keyLength);
        this.state = {

            disabled: props.projectProps.isDisabled,
            isEmpty: props.projectProps.isComplete && notDataExist,
            visible: false,
            questionList: [],
            values: [],
            tableFieldsData: this.getTableDefaultData()
        };
    }


    // Getting Table Default Data
    getTableDefaultData = () => {
        let {formJSON, data} = this.props.projectProps;
        let {elementProps, name} = formJSON.pages[0].fields[0];

        let tableFieldsData = elementProps[name];
        if (data && data[name] && data[name].length) {
            tableFieldsData = data[name];
        }
        return tableFieldsData;
    };

    /******************* Utils Function ***************************/

    formItemLayout = {
        labelCol: {span: 6},
        wrapperCol: {span: 8},
    };

    getTableField = () => {
        let formFields = this.props.projectProps.formJSON.pages[0].fields;
        let {elementName} = this.props;
        for (let index = 0; index < formFields.length; index++) {
            let item = formFields[index];
            if (item.element === 'custom' && item.name ===  elementName) {
                return item;
            }
        }
    };

    getEditableColumns = () => {
        return col && col.filter(col => col.editable);
    };

    /*********************** END ********************************/

    /*********************************** EVENTS & Consts *********************************/


    /************************** Header Functions ******************************/
    renderHeaderButtons = () => {

        if (this.state.disabled) return null;
        let btnStyle = {
            height: "50px",
            width: "120px",
            cursor: "pointer",
            backgroundColor: BUTTON_COLOR,
            color: "#fff"
        };

        return (
            <div style={{marginBottom: "40px"}}>
                <Button className={"custom-button custom-button-light"} onClick={this.wizardHandleClick}
                        style={btnStyle}>
                    Start Wizard
                </Button>
            </div>
        )
    };
    /************************** End ***************************/


    /************************** Form Functions ******************************/

    renderTableData = () => {
        let tableFields = this.getTableField();

        let {getFieldDecorator} = this.props.form;
        let {name, columns} = tableFields.elementProps;
        let {disabled} = this.state;


        if (!col && columns) {
            col = columns;
        }

        delete tableFields.elementProps['columns'];
        return (

            <FormItem>
                {
                    getFieldDecorator(name, {initialValue: this.props.projectProps.data[name]})(
                        <GridTable tableCol={col} disabled={disabled}
                                   isMultiple={tableFields.elementProps.multiple} {...tableFields.elementProps}/>)
                }
            </FormItem>
        )

    };
    renderButtons = () => {
        let {disabled} = this.state;
        if (disabled) return null;
        let {onSubmit} = this.props;
        return <Button onClick={onSubmit && onSubmit} style={{
            height: "50px",
            width: "120px",
            backgroundColor: BUTTON_COLOR,
            color: "#fff",
        }}>Submit</Button>
    };
    renderFormData = () => {

        let {isEmpty, disabled} = this.state;
        if (isEmpty && disabled) {
            return (
                <h5 style={{textAlign: "center"}}>
                    --- No Data Exists ---
                </h5>
            )
        }


        return (
            <>
                {this.renderTableData()}
                {this.renderButtons()}
            </>
        )
    };
    /************************** End ******************************/


    /************************** Wizard Functions ******************************/


    saveChanges = (tableFields) => {

        let {onSubmit} = this.props;
        onSubmit && onSubmit(this.state.tableFieldsData);
    };

    setParentState = (obj, tableFields, isSaveClicked, index) => {


        let tableFieldsData = [...this.state.tableFieldsData];
        let editableColumns = this.getEditableColumns();

        editableColumns.forEach(col => {
            tableFieldsData[index][col.key] = obj[col.key];
        });


        this.setState({tableFieldsData, visible: !isSaveClicked}, () => {
            isSaveClicked && this.saveChanges(tableFields)
        });
    };

    handleOk = () => {
        this.setState({visible: true});
    };


    handleCancel = () => {
        this.setState({visible: false})
    };

    wizardHandleClick = () => {
        this.setState({
            visible: true,
            tableFieldsData: this.getTableDefaultData()
        });
    };

    renderQuestionnaireModal = () => {
        if (!this.state.visible) return null;

        let tableFields = this.getTableField();

        let editableColumns = this.getEditableColumns();

        return <Questionnaire editableColumns={editableColumns}
                              visible={this.state.visible} handleOk={this.handleOk}
                              handleCancel={this.handleCancel}
                              setStateOfParent={this.setParentState} tableFields={tableFields}
                              tableFieldsData={this.state.tableFieldsData}
        />
    };

    /************************** End ******************************/

    render() {
        if (this.props.processing) {
            return <NewLoader/>;
        }


        return (
            <div>
                {this.renderQuestionnaireModal()}
                {this.renderHeaderButtons()}
                {this.renderFormData()}
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        submitFormData: (projectId, phaseId, form, formData, message, successCB) => dispatch(submitFormData(projectId, phaseId, form, formData, message, successCB))
    }
};


const mapStateToProps = (state) => {
    return {
        processing: state.project_reducer.processing
    }
};

export default WizardTable