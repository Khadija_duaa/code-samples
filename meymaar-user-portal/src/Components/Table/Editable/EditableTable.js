import React from 'react';
import {Table,Button} from 'antd';
import 'antd/dist/antd.css';
import {Form} from "antd/lib/index";
import EditableCell from './EditableCell';
import './EditableStyle.css';


const EditableContext = React.createContext();

class EditableTable extends React.Component {

    constructor(props) {

        super(props);
        this.state = {
            dataSource: props.dataSource,
            edited: false,
        }
    }


    //handle Edit Save
    handleSave = (row) => {
        const newData = [...this.state.dataSource];
        const index = newData.findIndex(item => row.bqId === item.bqId);


        const item = newData[index];


        if (row[row.editedKey] !== item[row.editedKey]) {
            delete item['editedKey'];
            delete row['editedKey'];

            newData.splice(index, 1, {
                ...item,
                ...row,
            });

            this.setState({dataSource: newData,edited:true});
        }
    };

    /*************************************** Editable Context **************************************/

    EditableRow = ({form, index, ...props}) => {
        props.className = 'editable-row';
        return (
            <EditableContext.Provider value={form}>
                <tr {...props}/>
            </EditableContext.Provider>
        );
    };

    EditableFormRow = Form.create()(this.EditableRow);


    render() {

        const components = {
            body: {
                row: this.EditableFormRow,
                cell: EditableCell,
            },
        };

        const tableColumns = this.props.columns.map((col) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    editable: col.editable,
                    dataIndex: col.dataIndex,
                    inputType: 'text',
                    title: col.title,
                    handleSave: this.handleSave,
                }),
            };
        });


        return (
            <div>
                <Table components={components} dataSource={this.state.dataSource}
                       rowKey={(record,index) => index}
                       columns={tableColumns}
                       bordered
                />

                <Button type={"primary"} disabled={!this.state.edited} onClick={()=>this.props.onSubmit(this.state.dataSource)}> Submit </Button>
            </div>
        )
    }
}

export default EditableTable;