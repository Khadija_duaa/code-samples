import React from 'react';
import {Table, Button} from 'antd';
import 'antd/dist/antd.css';
import {connect} from 'react-redux';
import NewLoader from "../../../Containers/Loader/NewLoader";

class CheckboxTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSource: props.dataSource,
            columns: props.columns,
            selectedFirms:[],
            selectedRowKeys: props.rowKeys
        }
    }


    render() {

        let {dataSource, columns, selectedFirms,selectedRowKeys} = this.state;
        const hasSelected = selectedFirms.length >= 2;

        const rowSelection = {
            selectedRowKeys,
            onChange: (selectedRowKeys, selectedRows) => {
                this.setState({selectedRowKeys,selectedFirms: selectedRows});
            },

            getCheckboxProps: record => {
                let checkboxProps = {
                    disabled: !this.props.editRights,
                    name: record.name,
                };

                return checkboxProps;
            },

        };

        return (
            <div>
                <Table rowKey={(record, index) => index} rowSelection={rowSelection}
                       columns={columns}
                       dataSource={dataSource}/>


                {this.props.processing ? <NewLoader/> : null}


                {this.props.editRights ? <Button style={{color: "#fff", margin: "10px 10px 10px 0px"}}
                                                 type={"primary"}
                                                 icon={"save"} onClick={() => this.props.onSubmit(selectedFirms)}
                                                 disabled={!hasSelected}>
                    Save Data
                </Button> : null}

            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        processing: state.project_reducer.processing
    }
};


const connected = connect(mapStateToProps)(CheckboxTable);
export default connected;