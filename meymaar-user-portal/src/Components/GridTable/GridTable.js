import React from 'react';
import {Button, Popconfirm} from 'antd';
import 'antd/dist/antd.css';
import ReactDataGrid from "react-data-grid";
import {Editors} from "react-data-grid-addons";
import './GridStyles.css';

const {DropDownEditor} = Editors;


class GridTable extends React.Component {


    constructor(props) {
        super(props);

        let rows = this.props[this.props.name];


        if (this.props.value && this.props.value.length)
            rows = this.props.value;

        rows.forEach((row, index) => {
            row.rowId = index;
        });

        this.state = {
            rows
        };

    }


    handleRowDelete = (row) => {
        let {rows} = this.state;
        rows = rows.filter(item => item.rowId !== row.rowId);

        this.setState({rows}, () => (
            this.props.onChange(rows))
        );
    };


    onGridRowsUpdated = ({fromRow, toRow, updated}) => {
        const rows = this.state.rows.slice();
        for (let i = fromRow; i <= toRow; i++) {
            if(!rows[i]._title){
                rows[i] = {...rows[i], ...updated};
            }
        }

        this.setState({rows}, () => {
            this.props.onChange(rows);
        });
    };

    addRow = (event) => {

        let stateRows = [...this.state.rows];

        stateRows.push({rowId: stateRows.length});
        this.setState({rows: stateRows});
        event.preventDefault();
    };

    getEditorOptions = (options) => <DropDownEditor options={options}/>;


    getColumns = () => {

        let dataCol = [...this.props.tableCol];
        let {disabled} = this.props;

        if (disabled) {
            dataCol = dataCol.filter(a => a.key !== 'delete');
        }

        dataCol.forEach(column => {
            let newCol = {...column};
            for (let key in newCol) {

                if (newCol.hasOwnProperty(key) && key === 'editorTypes') {
                    column.editor = this.getEditorOptions(newCol[key]);
                }
            }
        });


        if (this.props.isMultiple && !disabled) {

            dataCol = dataCol.filter(column=>column.key !== 'delete');

            dataCol.push({
                name: 'Actions',
                key: 'delete',
                getRowMetaData: (row) => row,
                formatter: ({dependentValues}) => (
                    <span>
                            <Popconfirm title="Sure to delete?"
                                        onConfirm={!disabled ? () => this.handleRowDelete(dependentValues) : null}>
                                <a href={"javascript:;"} style={disabled ? {
                                    color: "#D0D0D0",
                                    pointerEvents: 'none',
                                    cursor: "default"
                                } : null}>Delete</a>
                            </Popconfirm>
                        </span>
                )
            });
        }
        return dataCol;
    };

    renderAddRowButton = () => {
        let jsx = '';

        if (this.props.isMultiple) {
            jsx = <Button style={{margin: "10px 0px", width: "100px", height: "40px", color: "#fff"}} type={"primary"}
                          onClick={this.addRow}> Add Row</Button>
        }

        return jsx;
    };

    rowRenderer = ({renderBaseRow, ...props}) => {
        let rowStyle = {
            marginLeft: "0px",
            position: "relative",
            top: "50%",
            transform: "translateY(-50%)",
            paddingLeft: "10%",
            fontWeight: "600"
        };
        return (
            <div>
                {props.row._title ?
                    <div style={{height: "60px", borderRight: "1px solid #eee", borderBottom: "1px solid #dddddd"}}>
                        <h5 style={rowStyle}>{props.row._title}</h5>
                    </div>
                    : renderBaseRow(props)
                }
            </div>
        )
    };

    render() {
        return (
            <div>
                <ReactDataGrid
                    columns={this.getColumns()}
                    rowGetter={i => this.state.rows[i]}
                    rowRenderer={this.rowRenderer}
                    rowsCount={this.state.rows.length}
                    onGridRowsUpdated={this.onGridRowsUpdated}
                    enableCellSelect={true}
                    minHeight={this.props.minHeight}
                    rowHeight={this.props.rowHeight || 80}
                />
                {!this.props.disabled ? this.renderAddRowButton() : null}
            </div>
        );
    }
}

export default GridTable;