import React from 'react';
import ReactDataGrid from 'react-data-grid';

const privilages = (value) => {

    return {
        filterable: true,
        resizable: true,

        formatter: (props) => {

            return (

                <div style={value === 0?{fontWeight: "bold", color: "#3B86FF"}:null}>
                    {props.value}
                </div>
            )
        }
    }
};


const columns = [
    {key: "ser", name: "Ser", editable: false, ...privilages()},
    {key: "description", name: "Description of work", editable: false},
    {
        key: "quantity",
        name: "Qty",
        editable: true,
        ...privilages(0),
        events: {

            onDoubleClick: (ev, args) => {
                // console.log(args);
                // console.log(
                //     "The user entered edit mode on row: " +
                //     args.rowIdx +
                //     " & column: " +
                //     args.idx
                // );
            }
        }
    },
    {key: "unit", name: "Unit", editable: false},
    {key: "rate", name: "Rate", editable: true},
    {key: "amount", name: "Amount", editable: true,},
];

const rows = [
    {ser: 1, description: "Excavation", quantity: 1069, unit: "Construction", rate: 320, amount: 349000},
    {ser: 2, description: "Hard Soil", quantity: 28, unit: "Construction", rate: 1400, amount: 39640},
    {ser: 3, description: "Cement Concrete", quantity: 68, unit: "Construction", rate: 920, amount: 62970},
    {ser: 4, description: "Walls, sills, coping", quantity: 3, unit: "Construction", rate: 4000, amount: 14000}
];


class GridTableEventsSelection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {columns: columns, rows: rows}
    }

    onGridRowsUpdated = ({fromRow, toRow, updated}) => {

        this.setState(state => {
            const rows = state.rows.slice();
            for (let i = fromRow; i <= toRow; i++) {
                rows[i] = {...rows[i], ...updated};
            }
            return {rows};
        });
    };

    RowRenderer = ({renderBaseRow, ...props}) => {

        return (

            <div>
                {renderBaseRow(props)}

            </div>
        )
    };

    render() {
        return (
            <div>
                <ReactDataGrid
                    columns={this.state.columns}
                    rowGetter={i => {
                        return (this.state.rows[i])
                    }}
                    rowsCount={this.state.rows.length}
                    onGridRowsUpdated={this.onGridRowsUpdated}
                    enableCellSelect={true}
                    enableCellAutoFocus={true}
                    rowRenderer={this.RowRenderer}

                />
            </div>
        )
    }
}


export default GridTableEventsSelection;