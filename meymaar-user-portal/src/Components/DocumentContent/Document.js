import React from 'react';
import axios from '../../utils/axios';
import {handleError} from "../../store/store-utils";
import {Row, Col, message, Button} from 'antd';
import 'antd/dist/antd.css';
import {NOTIFICATION_TIME} from "../../utils/common-utils";
import JsonForm from '../JSONForm/JsonForm';
import {connect} from 'react-redux';
import './DocumentStyle.css';
import {updateFormAsVisited} from "../../store/project/project-actions";
import ComponentFactory from "../ComponentFactory/ComponentFactory";
import ReadonlyView from "./ReadonlyView/ReadonlyView";
import Bids from '../CustomForms/Bids/Bids';
import CTC from "../CustomForms/CTC/CTC";
import RVT from '../CustomForms/Revitt/RevitFile/RVT';
import GBXML from '../CustomForms/GBXML/GBXMLFile';
import {previewDownloadFile} from "../../utils/server-utils";
import BidFirms from "../CustomForms/BidFirms/BidFirms";
import SBPCustomForm from "../CustomForms/PAFForm/SBPCustomForm";
import WizardTable from '../Table/WizardTable/WizardTable';
import NewLoader from "../../Containers/Loader/NewLoader";
import {store} from "../../index";
import {setProcessing} from "../../store/server/server-actions";
import None from "../CustomForms/None/None";


class Document extends React.Component {

    state = {
        isEditing: false,
        isDisabled: false,
        formData: this.props.form
    };

    componentDidMount() {
        let {projectDetails, form, isVisited} = this.props;
        let {dispatch} = store;
        if (!isVisited && projectDetails.phaseDetails.phaseEditable) {
            dispatch(setProcessing(true));
            axios.post(`/project-management/projects/${projectDetails.projectId}/visited/${form.formName}`)
                .then(() => {
                    this.props.updateFormAsVisited(form.formName, projectDetails);
                    dispatch(setProcessing(false));
                }).catch(err => {
                dispatch(setProcessing(false));
                let errorMessage = handleError(err);
                message.error(errorMessage, NOTIFICATION_TIME);
            })
        }
    }

    /******************************* EVENTS *************************/

    /******* Rendering Dynamic Form *************/
    renderDynamicForm = () => {
        let {formName} = this.props;
        let formProps = {
            formJSON: {...this.state.formData},
            ...this.props,
            isComplete: this.props.complete,
            isDisabled: !this.props.projectDetails.editRights
        };

        if (formName === 'FORM2') {
            formProps.formsSections = 2;
            return <SBPCustomForm projectProps={formProps} refresh={true}
                                  backCB={() => this.setState({isEditing: false, isDisabled: false})}/>
        }

        return <JsonForm projectProps={formProps} backCB={() => this.setState({isEditing: false, isDisabled: false})}/>
    };

    /******* End *************/

    /******* Handeling Download Preview Form *************/
    handleDownloadPreview = (isPreview = true) => {
        let {projectDetails} = this.props;
        let formData = {...this.state.formData};
        console.log("Form Data", formData);

        let dataProps = {
            projectId: projectDetails.projectId,
            phaseId: projectDetails.phaseId,
            data: formData,
            isPreview: isPreview,
            title: formData.title
        };

        previewDownloadFile(dataProps);

    };

    /******************** End *************/



    getForm = () => {
        let {form} = this.props;
        let {editRights} = this.props.projectDetails;

        switch (form.formType) {
            case 'dynamic':
                return this.renderDynamicForm();
            case 'react':
                return <ComponentFactory disable={!editRights} returnCB={() => this.setState({
                    isEditing: false,
                    isDisabled: false
                })} {...this.props} />;
            default:
                return null;
        }
    };


    renderDocumentContent = () => {
        let {form, activeDocumentTitle} = this.props;

        if (form.formType === 'react' && form.formName === 'FIRMS') {
            return <BidFirms {...this.props}/>
        }
        else if (form.formType === 'none' && form.formName === 'CTC') {
            return <CTC projectProps={this.props}/>
        }
        else if (form.formType === 'none' && form.formName === 'REVIT') {
            return <RVT projectProps={this.props}/>
        }
        else if (form.formType === 'none' && form.formName === 'GBXML') {
            return <GBXML projectProps={this.props}/>
        }
        else if (form.formType === "react" && form.formName === "BIDS") {
            return <Bids projectProps={this.props}/>
        }
        // For Readonly file
        else if (form.formType === "react" && form.formName === "READONLY_FILE") {
            return <ReadonlyView projectProps={this.props}/>
        }
        else if (form.formType === "none") {
            return <None onPreviewClick={this.handleDownloadPreview}
                         onDownloadClick={() => this.handleDownloadPreview(false)} projectProps={this.props}
                         activeDocumentTitle={activeDocumentTitle}/>
        } else {
            return this.getForm();
        }
    };

    renderContent = () => {
        return (
            <div style={{
                margin: '50px',
                backgroundColor: "#FFF",
                height: "90%",
                border: '1px solid #E0E5E8',
                padding: "50px",
                maxHeight: '90%',
                overflowY: 'auto',
                minHeight:"inherit"
            }}>
                {this.renderDocumentContent()}
            </div>
        )
    };


    // Rendering Header Preview and Download Buttons
    renderHeaderButtons = () => {
        if (!this.props.complete || this.props.form.ignoreDocument) return null;
        return (
            <div style={{display: 'flex', float: 'right'}}>

                <a style={{display: 'flex'}} onClick={this.handleDownloadPreview}>
                    <img src={'images/preview.svg'} height="18" style={{marginTop: '4px'}}/>
                    <p style={{
                        fontSize: '17px',
                        fontWeight: 'bold',
                        marginLeft: '10px',
                        marginRight: '15px'
                    }}>
                        {'Preview'}
                    </p>
                </a>

                <a style={{display: 'flex'}} onClick={() => this.handleDownloadPreview(false)}>
                    <img src={'images/download.svg'} height="18" style={{marginTop: '4px'}}/>
                    <p style={{
                        fontSize: '16px',
                        fontWeight: 'bold',
                        marginLeft: '10px',
                    }}>
                        {'Download'}
                    </p>
                </a>

            </div>
        )
    };

    renderDocumentNav = () => {

        let {activeDocumentTitle} = this.props;
        return (

            <Row style={{
                backgroundColor: '#FFFFFF',
                borderBottom: '1px solid #E0E5E8',
                height: 'auto',
                padding: "0px 50px",
                borderRadius: "10px 10px 0px 0px",
            }}>
                <Col span={20} style={{marginTop: '17px'}}>
                    <p style={{fontSize: '20px', fontWeight: 'bold'}}>
                        {activeDocumentTitle}
                    </p>
                </Col>
                <Col span={4} style={{marginTop: '20px'}}>
                    {this.renderHeaderButtons()}
                </Col>
            </Row>

        )
    };

    render() {

        if (this.props.processing) return <NewLoader/>;

        return (
            <div style={{paddingTop: "40px"}}>
                <Row style={{
                    height: '100%', minHeight:"1040px",width: '100%',
                    border: '1px solid #E0E5E8',
                    borderRadius: "10px 10px 0px 0px",
                    maxWidth: '100%', backgroundColor: '#F4F4F7'
                }}>
                    <Col style={{
                        minHeight:"960px"
                    }}>
                        {this.renderDocumentNav()}
                        {this.renderContent()}
                    </Col>
                </Row>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        updateFormAsVisited: (formName, projectDetails) => dispatch(updateFormAsVisited(formName, projectDetails))
    }
};
const mapStateToProps = (state) => {
    return {
        processing: state.server_reducer.processing,
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Document);
export default connected