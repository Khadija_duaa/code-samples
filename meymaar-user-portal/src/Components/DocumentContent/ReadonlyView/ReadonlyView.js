import React from 'react';
import {Row, Col, Button} from 'antd';
import 'antd/dist/antd.css';

import {connect} from 'react-redux';
import {deleteAdditionalFile} from "../../../store/project/project-actions";
import DeleteConfirmation from "../../ModalFactory/ConfirmationModal/DeleteConfirmation";
import {previewDownloadFile} from "../../../utils/server-utils";


// PDF is opening in new tab for preview
class ReadonlyView extends React.Component {

    state = {
        isVisible: false,
        showConfirmModal: false
    };

    handleDownloadPreview = (isPreview = true)=>{

        console.log("Preview Download - Readonly View");

        let {form, projectDetails} = this.props.projectProps;

        console.log("Preview Download - Documents");

        let dataProps = {
            projectId: projectDetails.projectId,
            phaseId : projectDetails.phaseId,
            data:form,
            isPreview:isPreview,
            title:form.title
        };

        previewDownloadFile(dataProps);

    };

    handleFileDelete = () => {
        let {data, projectDetails} = this.props.projectProps;

        this.props.deleteAdditionalFile(projectDetails.projectId, projectDetails.phaseId, data.file.fileId, () => {
            this.toggleConfirmationModal();
            this.props.projectProps.prevDocument();
        });
    };

    toggleConfirmationModal = () => {
        this.setState({showConfirmModal: !this.state.showConfirmModal});
    };

    renderConfirmationModal = () => {

        return (
            <DeleteConfirmation isOpen={this.state.showConfirmModal}
                                okCB={this.handleFileDelete} cancelCB={this.toggleConfirmationModal}
                                fileTitle={this.props.projectProps.form.title}/>
        );

    };


    getOverview = () => {

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width:"100%"
        };
        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            minWidth: "224px",
        };
        let textStyle = {
            fontSize: "18px",
            marginLeft: "5%",
            fontWeight: "normal",
            color: "#000"
        };
        let buttonStyle = {
            marginTop: "3%",
            marginRight: "2%"
        };

        let {form,projectDetails} = this.props.projectProps;

        let jsx = (
            <Row>
                <Col span={24} style={{display: 'block'}}>

                        <Col span={5} style={boxStyle}>
                            <div style={divStyle}>
                                <img alt={"pdf"} src={`/fileIcons/pdf.svg`} width={"70px"}/>
                                <p style={{
                                    fontWeight: "normal",
                                    fontSize: "18px",
                                    marginTop: "5px",
                                    width: "100%",
                                    overflowWrap: "break-word"
                                }}>{form.title}</p>
                            </div>
                        </Col>


                        <Col span={12} style={{paddingTop: "30px", display:'block'}}>

                        <span style={textStyle}>
                            <Button type={"primary"} shape={"circle"}
                                    style={buttonStyle}
                                    size={"large"}
                                    onClick={this.handleDownloadPreview}
                                    icon={"eye"}/>
                                 Preview
                        </span>

                            <br/>


                            <span style={textStyle}>
                            <Button type={"primary"} shape={"circle"}
                                    style={buttonStyle}
                                    size={"large"}
                                    icon={"download"}
                                    onClick={() => this.handleDownloadPreview(false)}/>
                                 Download
                        </span>

                            <br/>
                            {
                                form.deletable && projectDetails.editRights ?
                                    <span style={textStyle}>
                                    <Button type={"danger"} shape={"circle"}
                                            style={buttonStyle}
                                            size={"large"}
                                            icon={"delete"}
                                            onClick={() => this.setState({showConfirmModal: true})}/>
                                         Delete
                                </span> : null

                            }

                        </Col>
                </Col>
            </Row>
        );

        return jsx;
    };

    render() {
        return (
            <div>
                {this.renderConfirmationModal()}
                {this.getOverview()}
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        deleteAdditionalFile: (projectId, phaseId, fileId, cb) => dispatch(deleteAdditionalFile(projectId, phaseId, fileId, cb))
    }
};

const connected = connect(null, mapDispatchToProps)(ReadonlyView);
export default connected