import React from 'react';

class HeaderTitle extends React.Component{
    render(){
        return(
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        {this.props.children && this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export default HeaderTitle;