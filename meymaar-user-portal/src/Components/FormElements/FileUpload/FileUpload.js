import React from 'react';
import {Upload, Button, Icon} from 'antd';
import 'antd/dist/antd.css';
const uuidv1 = require('uuid/v1');
class FileUpload extends React.Component {

    render() {
        const {form, element, editRights, isEditing,fileData} = this.props;

        const uploadProps = {
            showUploadList: {showRemoveIcon: false},
            accept: element.elementProps.acceptFiles,
            onRemove: () => {
                form.setFieldsValue({[element.name]: ''});

            },

            beforeUpload: (fileInfo) => {
                form.setFieldsValue({[element.name]: fileInfo});
                return false;
            },

            onChange: (change) => {

                this.props.form.setFieldsValue({[element.name]: change.file});

                if (change.fileList.length > 1) {
                    change.fileList.splice(0, 1);
                }
            }
        };

        if(isEditing){
            if(!Object.keys(fileData).includes('uuid')){
                fileData.uid = uuidv1();
            }
            fileData.lastModified = new Date().getTime();
            fileData.type = "application/pdf";
            uploadProps.defaultFileList = [fileData];
        }
        return (
            <Upload {...uploadProps} disabled={!editRights}>
                <Button disabled={!editRights}>
                    <Icon type="upload"/> {isEditing ? "Replace File" : element.title}
                </Button>
            </Upload>
        )
    }
}

export default FileUpload;

