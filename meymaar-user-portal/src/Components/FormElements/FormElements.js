import React from 'react';
import {Form, Input, DatePicker, InputNumber} from 'antd';
import 'antd/dist/antd.css';
import FileUpload from "./FileUpload/FileUpload";
import moment from "moment/moment";
import {isFraction, isNumber} from "../../utils/common-utils";

class FormElements extends React.Component {

    /********************* Validators **********************/

    validateFile = (rule, value, callback) => {

        let {fileExt} = this.props.element.elementProps;

        if (value) {
            let {size, name} = value;

            let ext = name.split('.');

            let fileSize = size / 1048576;

            if (fileSize > 300) {
                callback("Maximum file size limit is: 300 MB");
            } else if (!fileExt.includes(ext[ext.length - 1])) {
                callback(`File with formats ${fileExt} are acceptable only!`);
            } else {
                callback();
            }
        } else {
            callback();
        }
    };


    validateDate = (rule, value, callback) => {

        let {getFieldValue} = this.props.form;

        if (getFieldValue('dateOfDrawing')) {

            let startDate = moment(getFieldValue('dateOfDrawing'));
            let endDate = moment(value);
            let days = startDate.diff(endDate);
            if (days > 0) {
                callback("Date of revision must be after or same to date of drawing");
            } else {
                callback();
            }

        } else {
            callback();
        }

    };

    validatePercentage = (rule, value, callback) => {
        if (value && (!isNumber(value) || !isFraction(value) || value < 0 || value > 100)) {
            callback("Invalid percentage value");
        } else {
            callback();
        }
    };
    /************************END **************************/

    modifyRules = (rules, item) => {
        let _rules = [...rules];


        switch (item.element) {
            case 'file':
                _rules.push({validator: this.validateFile});
                break;

            case 'percentInput':
                _rules.push({validator: this.validatePercentage});
                break;
            case 'datePicker':
                if (item.name === 'dateOfRevision') {
                    _rules.push({validator: this.validateDate});
                }

                break;
            default:
                break;
        }


        return _rules;
    };


    getElement = (item, editRights) => {

        switch (item.element) {

            case 'input':
                return <Input disabled={!editRights} {...item.elementProps}/>;

            case 'percentInput':
                return <InputNumber  {...item.elementProps}
                                     formatter={value => `${value}%`}
                                     parser={value => value.replace('%', '')}
                />;
            case 'datePicker':
                return <DatePicker disabled={!editRights}  {...item.elementProps}/>;

            case 'file':
                return <FileUpload {...this.props}/>;
            default:
                return null;
        }
    };

    render() {
        let {getFieldDecorator} = this.props.form;
        let {element, formItemLayout, editRights, initialValue} = this.props;

        let configRules = [{required: element.required, message: element.requiredMessage}];

        if (element.whitespace) {
            configRules[0].whitespace = element.whitespace;
        }

        configRules = this.modifyRules(configRules, element);


        return (
            <Form.Item {...formItemLayout} label={element.label}>
                {getFieldDecorator(element.name, {
                    initialValue,
                    rules: configRules
                })(this.getElement(element, editRights))}
            </Form.Item>
        )
    }
}


export default FormElements;