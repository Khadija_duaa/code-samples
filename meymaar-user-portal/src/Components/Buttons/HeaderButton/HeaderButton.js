import React from 'react'

class HeaderButton extends React.Component {

    render() {
        let {className, buttonStyle, onBtnClick, divStyle, textStyle,disabled} = this.props;
        let {imgURL, imgStyle, svgColor, imgWidth, imgHeight} = this.props;

        return (
            <button disabled={disabled && disabled} className={className && className}
                    style={buttonStyle && buttonStyle} onClick={onBtnClick}>
                <div style={divStyle && divStyle}>
                    <img src={imgURL}
                         height={imgHeight && imgHeight}
                         width={imgWidth && imgWidth}
                         color={svgColor && svgColor}
                         style={imgStyle && imgStyle}/>

                    <div style={textStyle && textStyle}>
                        {this.props.children && this.props.children}
                    </div>
                </div>
            </button>
        )
    }
}

export default HeaderButton;