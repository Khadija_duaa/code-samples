import React from 'react';
import {Button} from 'antd';
import 'antd/dist/antd.css';

class BackButton extends React.Component{

    render(){
        let {returnCB} = this.props;
        let btnStyle = {
            ...this.props.style
        };

        return(
            <div>
                <Button className={"custom-button custom-button-light header-button"} style={btnStyle} type={"default"} icon={"rollback"} onClick={returnCB && returnCB}>
                    Back
                </Button>
            </div>
        )
    }
}


export default BackButton;