import React from 'react';

class RowButtonsList extends React.Component {
    render() {
        let {buttonText,onCreateClick,onSearchChange} = this.props;
        return (
            <div className="container-fluid">
                <div className="row button-list align-items-baseline">
                    <div className="col-md-4">
                        <button className="custom-button custom-button-dark create-button"
                                onClick={onCreateClick}>
                            {buttonText}
                        </button>
                    </div>

                    <div className="col-md-8 text-right margin-top-56">
                        <div className="search-bar new-btn-with-gradient-select">
                            <form className="search-container">
                                <input type="text" id="search-bar" placeholder="Search…"
                                       onChange={onSearchChange}/>
                                <a><i className="fas fa-search"/></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default RowButtonsList