import React from 'react';
import {Select,message} from 'antd';
import 'antd/dist/antd.css';
import axios from '../../../utils/axios';
import {getObjectSpecificValue, NOTIFICATION_TIME} from "../../../utils/common-utils";
import {store} from "../../../index";
import {setProcessing} from "../../../store/user/user-actions";
import {handleError} from "../../../store/store-utils";

const Option = Select.Option;


class DynamicSelect extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            opt: null,
        }
    }


    componentDidMount = () => {
        let {optionURL,optionValue, keyName, setInitialCB, isCreateWork} = this.props;
        let {dispatch} = store;
        dispatch(setProcessing(true));
        axios.get(`/${optionURL}`)
            .then(response => {

                this.setState({
                    opt: response.data[keyName]
                }, () => {
                    if (isCreateWork) {
                        let {opt} = this.state;
                        let value = opt.length ? getObjectSpecificValue(opt[0], optionValue).toString() : " ";
                        setInitialCB && setInitialCB(value);
                    }
                    dispatch(setProcessing(false));
                });
            })
            .catch(error => {
                dispatch(setProcessing(false));
                let errorMessage = handleError(error);
                message.error(errorMessage,NOTIFICATION_TIME);
            });
    };

    render() {
        let {name, placeholder, optionValue, optionName, disabled, isCreateWork} = this.props;
        let {opt} = this.state;
        if (!opt) return null;

        let defaultValue = opt.length ? getObjectSpecificValue(opt[0], optionValue).toString() : " ";

        return (
            <Select defaultValue={isCreateWork && defaultValue} disabled={disabled} name={name}
                    placeholder={placeholder} style={{width: "100%"}} onChange={this.props.onChange}>
                {
                    this.state.opt.map((options, index) => {
                        return (
                            <Option key={index} value={getObjectSpecificValue(options, optionValue)}>
                                {getObjectSpecificValue(options, optionName)}
                            </Option>
                        )
                    })
                }
            </Select>
        )
    }
}

export default DynamicSelect;