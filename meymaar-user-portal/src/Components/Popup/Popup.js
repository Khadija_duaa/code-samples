import React from 'react';
import {notification, Icon} from 'antd';
import {withRouter}  from 'react-router-dom';
import 'antd/dist/antd.css';
import {connect} from 'react-redux';
import {fetchProjectDetails} from "../../store/project/project-actions";
import {resetNotification} from "../../store/user/user-actions";
import {fetchUserRecentNotifications, handleNotificationClick} from "../../utils/server-utils";


class Popup extends React.Component {


    handleNotificationClick = (notify)=>{
        notification.destroy();
        this.props.resetNotification();
        handleNotificationClick(notify,(returnPath)=>{
            this.props.history.push(returnPath);
        });

    };

    openNotification = (notify) => {
        let {activeUser} = this.props;
        fetchUserRecentNotifications(activeUser.principal_id);


        let notificationJSX = () => {
            return (
                <div style={{cursor:"default",marginTop: "5px"}} onClick={()=>this.handleNotificationClick(notify)}>
                    <p>
                        {notify.body}
                    </p>
                </div>
            )
        };

        notification.config({
            placement: "bottomRight",
            duration:3
        });

        notification.open({
            message: notify.title,
            description: notificationJSX(),
            icon: <Icon type="notification" style={{color: '#108ee9'}}/>,
        });

        this.props.resetNotification();
    };


    render() {

        if (!this.props.data) return null;

        return (
            <div>
                {this.openNotification(this.props.data)}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.user_reducer.notification,
        activeUser:state.user_reducer.activeUser
    }
};

const mapDispatchToProps = (dispatch)=>{
  return {
       fetchProject :(projectId,fields,cb)=>dispatch(fetchProjectDetails(projectId,fields,false,cb)),
      resetNotification : ()=>dispatch(resetNotification())
  }
};
const connected = connect(mapStateToProps,mapDispatchToProps)(Popup);
export default withRouter(connected);