import React from 'react';
import {Row, Col, Form, message, Input, Select, Modal, Button} from 'antd';


const {Option} = Select;

class Questionnaire extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

            index: 0,
            values: [],
            isSaveButtonClicked: false,

        }
    }

    success = (msg) => {
        message.success(msg);
    };

    errors = (msg) => {

        message.error(msg);
    };


    handleClickNext = () => {
        let isLast = this.state.index === this.props.tableFieldsData.length - 1;
        if (isLast) {
            this.handleSaveChanges();
        }
        else {
            let {index} = this.state;
            if (index + 2 < this.props.tableFieldsData.length && this.props.tableFieldsData[index+1]._title) {
                index+=2;
            }else{
                index++;
            }
            setTimeout(() => {
                this.setState({index, isSaveButtonClicked: false});
            }, 300)
        }
    };

    handleClickPrevious = () => {
        let {index} = this.state;
        if (index - 2 > 0 && this.props.tableFieldsData[index-1]._title) {
            index -= 2;
        }else{
            index--;
        }
        setTimeout(() => {
            this.setState({index, isSaveButtonClicked: false});
        }, 300)
    };

    handleSaveChanges = () => {
        this.setState({isSaveButtonClicked: true})
    };


    submit = (e, setParent, tableFields) => {

        e.preventDefault();

        this.props.form.validateFields((err, values) => {

            let {editableColumns} = this.props;
            editableColumns && editableColumns.forEach(col => {
                if (!values[col.key]) {
                    values[col.key] = "";
                }
            });


            setParent(values, tableFields, this.state.isSaveButtonClicked, this.state.index);

            this.props.form.resetFields();
        })
    };


    /************************ Text Element ********************/

    getTextElement = (getFieldDecorator, data, editColumn, index) => {
        return (
            <Form.Item key={index}>
                {getFieldDecorator(editColumn.key, {
                    initialValue: data && data[this.state.index][editColumn.key],
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [{required: false,}]
                })(
                    <Input placeholder={`Please type ${editColumn.key}`}/>)}
            </Form.Item>
        )

    };
    /************************ End ********************/


    /************************ Select Element ********************/

    getSelectElement = (getFieldDecorator, data, editColumn, index) => {
        let optionsJSX = editColumn.editorTypes.map((option, index) => {
            return (
                <Option key={index} value={option.id}>{option.value}</Option>
            )
        });

        return (
            <Form.Item key={index}>
                {getFieldDecorator(editColumn.key, {
                    initialValue: data && data[this.state.index][editColumn.key],
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [{required: false}]
                })(<Select placeholder="Please select">
                    {optionsJSX}
                </Select>)}
            </Form.Item>
        )

    };
    /************************ End ********************/


    /************************ Questions ********************/

    getQuestion = (tableData) => {
        return (
            <div style={{display: 'flex', marginTop: '20px'}}>
                <p style={{fontWeight: 'bold', fontSize: '20px', marginRight: '5px'}}>
                    {tableData[this.state.index].id}.
                </p>
                <p style={{fontWeight: 'bold', fontSize: '20px'}}>
                    {tableData[this.state.index] && tableData[this.state.index].question}
                </p>
            </div>
        )
    };

    /************************ End ********************/


    /************************* Wizard Fields ******************/
    renderWizardFields = (tableFieldsData, editableColumns) => {
        let {getFieldDecorator} = this.props.form;
        return editableColumns && editableColumns.map((editColumn, index) => {
            if (editColumn.editorTypes && editColumn.editorTypes.length) {
                return this.getSelectElement(getFieldDecorator, tableFieldsData && tableFieldsData, editColumn, index)
            } else {
                return this.getTextElement(getFieldDecorator, tableFieldsData && tableFieldsData, editColumn, index)
            }
        })
    };
    /*************************** END **************************/


    /************************ Form Buttons ********************/

    renderButtons = (data) => {
        let isLast = this.state.index === data.length - 1;
        let isFirst = this.state.index === 0;

        return (

            <Form.Item>
                <Row type="flex" className="margin-top-40">
                    <Col span={12} className="edit-black-buttons float-right">
                        {
                            <Button disabled={isFirst} type="primary" value="large" htmlType={"submit"}
                                    onClick={this.handleClickPrevious}>
                                <div style={{color: 'white'}}>Previous</div>
                            </Button>
                        }
                    </Col>

                    <Col span={12} className="edit-black-buttons float-left">
                        {
                            <Button type="primary" value="large" htmlType={"submit"} onClick={this.handleClickNext}>
                                <div style={{color: 'white'}}>{isLast ? "Finish & Save" : "Next"}</div>
                            </Button>
                        }
                    </Col>
                </Row>
                <Row>
                    <Col span={12} className="edit-black-buttons  margin-top-40">
                        <Button type="primary" value="large" style={{width: '505px'}} htmlType="submit"
                                onClick={this.handleSaveChanges}>
                            <div style={{color: 'white'}}>Save Changes</div>

                        </Button>
                    </Col>
                </Row>
            </Form.Item>
        )

    };

    /****************************** End *********************/

    render() {

        let {tableFieldsData, editableColumns} = this.props;
        return (

            <Modal
                title={'Questionnaire'}
                visible={this.props.visible}
                className="custom-table-popup"
                onOk={this.props.handleOk}
                onCancel={this.props.handleCancel}
                destroyOnClose={true}
                footer={false}
                maskClosable={false}
                centered
                size={"lg"}
            >
                <Form
                    onSubmit={(e) => this.submit(e, this.props.setStateOfParent, this.props.tableFields && this.props.tableFields)}>
                    <Row className="custom-popup-row">
                        <Col span={24} offset={4} className="custom-form-wrap">
                            <Col span={16}>


                                {this.getQuestion(tableFieldsData)}

                                {this.renderWizardFields(tableFieldsData, editableColumns)}

                                {this.renderButtons(tableFieldsData)}

                            </Col>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        )
    }
}


const QuestionnaireForm = Form.create()(Questionnaire);
export default QuestionnaireForm;


