import React from 'react';
import {Row, Col, Breadcrumb} from 'antd';
import 'antd/dist/antd.css';
import {withRouter} from 'react-router-dom';

class BreadCrumb extends React.Component {

    handleCrumbClick = (index, crumb) => {
        let {crumbsJSON} = this.props;
        if (index < crumbsJSON.length - 1) {
            return this.props.history.push(crumb.returnPath)
        } else {
            return null;
        }
    };


    renderBreacdCrumbs = () => {

        let {crumbsJSON} = this.props;
        let breadCrumbStyle = {cursor: 'pointer', ...this.props.style};

        return crumbsJSON.map((crumb, index) => {


            return (
                <Breadcrumb.Item key={index} style={index < crumbsJSON.length - 1 ? breadCrumbStyle : {
                    ...breadCrumbStyle,
                    cursor: "default"
                }} onClick={() => this.handleCrumbClick(index, crumb)}>
                    {crumb.title}
                </Breadcrumb.Item>
            )
        })

    };

    render() {

        return (
            <div className={`container-fluid ${this.props.className || 'breadcrumbs-back'}`}>
                <Row span={24}>
                    <Col span={24}>
                        <Breadcrumb separator=">" className={"separator"} style={{marginBottom: '20px'}}>
                            {this.renderBreacdCrumbs()}
                        </Breadcrumb>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default withRouter(BreadCrumb);