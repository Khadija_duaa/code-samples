import React from 'react';
import {Form, Button, Row, Col} from 'antd';
import 'antd/dist/antd.css';
import {connect} from 'react-redux';
import FormElements from "../../FormElements/FormElements";
import MessageModal from '../../ModalFactory/MessageModal/ConfirmationModal';
import {
    deletePreviewFile,
    submitFieldManagementData,
    submitFiles
} from "../../../store/project/project-actions";
import DeleteConfirmation from '../../ModalFactory/ConfirmationModal/DeleteConfirmation';

import {previewDownloadFile} from "../../../utils/server-utils";


class None extends React.Component {


    /*<------------------------------------------------------------------------------------------>*/


    /****************** Rendering File View of CTC ************************/

    handleDownloadPreview = (isPreview = true) => {
        let {projectDetails} = this.props.projectProps;
        let formData = {...this.state.formData};

        let dataProps = {
            projectId: projectDetails.projectId,
            phaseId: projectDetails.phaseId,
            data: formData,
            isPreview: isPreview,
            title: formData.title
        };

        previewDownloadFile(dataProps);

    };

    handleDownloadPreview = (isPreview = true) => {
        let {projectDetails} = this.props.projectProps;
        let formData = {...this.state.formData};
        console.log("Form Data",formData);

        let dataProps = {
            projectId: projectDetails.projectId,
            phaseId: projectDetails.phaseId,
            data: formData,
            isPreview: isPreview,
            title: formData.title
        };

        previewDownloadFile(dataProps);

    };

    getOverview = () => {
        let {form,complete} = this.props.projectProps;

        if(!complete) return null;

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width: "100%"
        };
        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            minWidth: "200px",
            marginLeft:'50px',
            marginTop:'50px'
        };
        let textStyle = {
            fontSize: "18px",
            marginLeft: "5%",
            fontWeight: "normal",
            color: "#000"
        };
        let buttonStyle = {
            marginTop: "3%",
            marginRight: "2%"
        };


        let jsx = (
            <Row>
                <Col span={24} style={{display: 'block'}}>
                    <Row>
                        <Col span={5} style={boxStyle}>
                            <div
                                style={divStyle}>
                                <img alt={"pdf"} src={`/fileIcons/pdf.svg`} width={"70px"}/>
                                <p style={{
                                    fontWeight: "normal",
                                    fontSize: "18px",
                                    marginTop: "5px",
                                    width: "100%"
                                }}>{form.title}</p>
                            </div>
                        </Col>
                    </Row>
                    <Row>

                        {!form.ignoreDocument ?
                            <Col span={12} style={{paddingTop: "30px", marginLeft: '50px', display: 'block'}}>
                                <span style={textStyle}>
                                    <Button type={"primary"} shape={"circle"}
                                            style={buttonStyle}
                                            size={"large"}
                                            onClick={this.handleDownloadPreview}
                                            icon={"eye"}/>
                                       Download
                                </span>

                                <br/>

                                <span style={textStyle}>
                                    <Button type={"primary"} shape={"circle"}
                                            style={buttonStyle}
                                            size={"large"}
                                            icon={"download"}
                                            onClick={() => this.handleDownloadPreview(false)}/>
                                    Preview
                                </span>
                            </Col> :
                            null
                        }
                    </Row>
                </Col>
            </Row>
        );

        return jsx;
    };

    /****************************** END ***********************/


    render() {
        let {form,complete} = this.props.projectProps;

        if(!complete) return null;
        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            minWidth: "200px",
            overflowWrap: "break-word"
        };
        let textStyle = {
            fontSize: "18px",
            marginLeft: "5%",
            fontWeight: "normal",
            color: "#000"
        };
        let buttonStyle = {
            marginTop: "3%",
            marginRight: "2%"
        };

        return (
            <Row>
                <Col span={24}>
                    <Col span={5} style={boxStyle}>
                        <div
                            style={{
                                position: "absolute",
                                top: "50%",
                                left: "50%",
                                textAlign: "center",
                                transform: "translate(-50%, -50%)",
                                width: "100%"
                            }}>
                            <img alt={"pdf"} src={`/fileIcons/pdf.svg`} width={"70px"}/>
                            <p style={{
                                fontWeight: "normal",
                                fontSize: "18px",
                                marginTop: "5px",
                                width: "100%"
                            }}>{this.props.projectProps.form.title}</p>
                        </div>
                    </Col>


                    <Col span={12} style={{paddingTop: `50px`}}>

                        {!form.ignoreDocument ?
                            <div>
                                <span style={textStyle}>
                                    <Button type={"primary"} shape={"circle"}
                                            style={buttonStyle}
                                            size={"large"}
                                            onClick={this.props.onPreviewClick}
                                            icon={"eye"}/>
                                         Preview
                                </span>

                                <br/>

                                <span style={textStyle}>
                                    <Button type={"primary"} shape={"circle"}
                                            style={buttonStyle}
                                            size={"large"}
                                            icon={"download"}
                                            onClick={this.props.onDownloadClick}/>
                                         Download
                                </span>
                            </div> :
                            null
                        }

                    </Col>
                </Col>
            </Row>
        );

    }
}


export default Form.create()(None);
