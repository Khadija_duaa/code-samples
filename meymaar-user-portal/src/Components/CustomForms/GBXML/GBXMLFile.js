import React from 'react';
import {Form, Button, Row, Col} from 'antd';
import 'antd/dist/antd.css';
import {connect} from 'react-redux';
import FormElements from "../../FormElements/FormElements";
import MessageModal from '../../ModalFactory/MessageModal/ConfirmationModal';
import {
    deletePreviewFile,
    submitFieldManagementData,
    submitFiles
} from "../../../store/project/project-actions";
import DeleteConfirmation from '../../ModalFactory/ConfirmationModal/DeleteConfirmation';

import {previewDownloadFile} from "../../../utils/server-utils";
import {hasAuthority} from "../../../utils/common-utils";


class GBXMLFile extends React.Component {

    state = {
        showModal: false,
        showConfirmModal: false,
        canUploadRevit: hasAuthority('CAN_UPLOAD_REVIT_MODEL')
    };


    toggleModal = () => {
        this.setState({showModal: !this.state.showModal})
    };

    // Modal Success Callback Confirmation Event
    handleConfirmClick = () => {
        let {projectDetails} = this.props.projectProps;
        let {projectId, phaseId} = projectDetails;

        let {getFieldValue} = this.props.form;
        let formData = new FormData();

        formData.append('upload', getFieldValue('upload'), getFieldValue('upload').name);

        let submitURL = '/fields-management/accept-gbXml/{PROJECT_ID}';

        this.props.submitGBXMLFile(projectId, phaseId, 'gbXml', formData, 'GBXML', submitURL, this.toggleModal);


    };

// Message Confirmation Modals
    renderMessageModal = () => {
        let message = 'Are you sure, you want to upload  GBXML File?';

        return (
            <MessageModal isOpen={this.state.showModal}
                          okCB={this.handleConfirmClick} cancelCB={this.toggleModal}
                          message={message}/>
        );
    };


    submitGBXMLForm = (event) => {
        event.preventDefault();

        let {form} = this.props;

        form.validateFields((err) => {
            if (err) {
                return;
            }
            this.setState({showModal: true});
        });


    };


    formItemLayout = {
        labelCol: {span: 6},
        wrapperCol: {span: 8},
    };
    buttonItemLayout = {
        wrapperCol: {offset: 6},
    };

    uploadJSON = {
        element: 'file',
        name: 'upload',
        title: 'Select File',
        required: true,
        requiredMessage: "File is required!",
        elementProps: {
            acceptFiles: ".xml",
            fileExt: ["xml"]
        }
    };


    getUploadButton = () => {

        let formElementProps = {
            element: this.uploadJSON,
            editRights: this.props.projectProps.projectDetails.editRights,
            ...this.props
        };

        return <FormElements {...formElementProps}/>
    };

    renderGBXMLButton = () => {
        let {editRights} = this.props.projectProps.projectDetails;
        return (
            <Form.Item>
                <Button type={"primary"} disabled={!editRights} htmlType={"submit"}>
                    Submit
                </Button>
            </Form.Item>
        )
    };

    renderGBXMLFormView = () => {

        let {editRights} = this.props.projectProps.projectDetails;

        if (!editRights) {
            return <h5> You can not edit this document!</h5>
        }

        return (
            <div>
                {this.renderMessageModal()}


                <h3 style={{marginBottom: "50px"}}>Upload GBXML File </h3>

                <Form layout={"vertical"} onSubmit={this.submitGBXMLForm}>
                    {this.getUploadButton()}
                    {this.renderGBXMLButton()}
                </Form>
            </div>
        )
    };

    /*<------------------------------------------------------------------------------------------>*/


    /****************** Rendering File View of GBXML ************************/

    handleDownloadPreview = (isPreview = true) => {

        console.log("Preview Download - Readonly View");

        let {form, projectDetails} = this.props.projectProps;

        console.log("Preview Download - Documents");

        let dataProps = {
            projectId: projectDetails.projectId,
            phaseId: projectDetails.phaseId,
            data: form,
            isPreview: isPreview,
            title: form.title
        };

        previewDownloadFile(dataProps);

    };


    toggleConfirmationModal = () => {
        this.setState({showConfirmModal: !this.state.showConfirmModal});
    };


    handleDeleteFileClick = () => {
        let {projectDetails, data} = this.props.projectProps;

        let url = '/fields-management/delete-gbXml/{PROJECT_ID}';
        this.props.submitFieldData(projectDetails.projectId, projectDetails.phaseId, data, url, true, () => {
            this.props.deletePreviewFile(projectDetails.projectId, data.gbXml.fileId, this.toggleConfirmationModal);
        });

    };

    renderConfirmationModal = () => {

        let {data} = this.props.projectProps;
        return (
            <DeleteConfirmation isOpen={this.state.showConfirmModal}
                                okCB={this.handleDeleteFileClick} cancelCB={this.toggleConfirmationModal}
                                fileTitle={data.gbXml.fileName} otherText={"file"}/>
        );
    };

    getOverview = () => {

        let {data, projectDetails} = this.props.projectProps;
        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width: "100%"
        };
        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            minWidth: "140px"
        };
        let textStyle = {
            fontSize: "18px",
            marginLeft: "5%",
            fontWeight: "normal",
            color: "#000"
        };
        let buttonStyle = {
            marginTop: "3%",
            marginRight: "2%"
        };


        let jsx = (
            <Row>
                <Col span={24}>
                    <Col span={5} style={boxStyle}>
                        <div style={divStyle}>
                            <img alt={"pdf"} src={`/fileIcons/pdf.svg`} width={"70px"}/>
                            <p style={{
                                fontWeight: "normal",
                                fontSize: "18px",
                                marginTop: "5px",
                                width: "100%",
                                overflowWrap: "break-word"
                            }}>{data.gbXml.fileName}</p>
                        </div>
                    </Col>

                    <Col span={12} style={{paddingTop: "30px"}}>

                        {/*<span style={textStyle}>*/}
                        {/*<Button type={"primary"} shape={"circle"}*/}
                        {/*style={buttonStyle}*/}
                        {/*size={"large"}*/}
                        {/*onClick={this.handleDownloadPreview}*/}
                        {/*icon={"eye"}/>*/}
                        {/*Preview*/}
                        {/*</span>*/}

                        {/*<br/>*/}


                        <span style={textStyle}>
                            <Button type={"primary"} shape={"circle"}
                                    style={buttonStyle}
                                    size={"large"}
                                    icon={"download"}
                                    onClick={() => this.handleDownloadPreview(false)}/>
                                 Download
                        </span>

                        <br/>
                        {projectDetails.editRights && this.state.canUploadRevit ? <span style={textStyle}>
                                    <Button type={"danger"} shape={"circle"}
                                            style={buttonStyle}
                                            size={"large"}
                                            icon={"delete"}
                                            onClick={() => this.setState({showConfirmModal: true})}/>
                                         Delete
                                </span> : null}

                    </Col>
                </Col>
            </Row>
        );

        return jsx;
    };

    /****************************** END ***********************/


    render() {

        let {data} = this.props.projectProps;


        if (!Object.keys(data).length || !data.gbXml ) {
            if(!this.state.canUploadRevit){
                return <h5>You do not have authority to upload gbxml files</h5>
            }
            return this.renderGBXMLFormView();
        }


        return (
            <div>
                {this.renderConfirmationModal()}
                {this.getOverview()}
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        submitGBXMLFile: (projectId, phaseId, key, fileData, fileName, url, cb) => dispatch(submitFiles(projectId, phaseId, key, fileData, fileName, url, cb)),
        submitFieldData: (projectId, phaseId, data, submitURL, deleting, cb) => dispatch(submitFieldManagementData(projectId, phaseId, data, submitURL, deleting, cb)),
        deletePreviewFile: (projectId, fileId, cb) => dispatch(deletePreviewFile(projectId, fileId, cb)),
    }
};


const connected = connect(null, mapDispatchToProps)(GBXMLFile);

export default Form.create()(connected);
