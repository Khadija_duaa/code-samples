import React from 'react';
import {Row,Col,Button} from 'antd';
import EditableTable from "../../Table/Editable/EditableTable";
import {fetchProjectBOQ} from "../../../utils/server-utils";
import {connect} from 'react-redux';
import {submitUpdatedBQSchedule} from "../../../store/project/project-actions";


class Schedule extends React.Component {

    state = {
        dataSource: []
    };

    componentDidMount() {
        fetchProjectBOQ(this.props.projectDetails.projectId, (bqs) => {
            let dataSource = [];
            bqs.forEach((bq, index) => {
                dataSource.push({});
                dataSource[index].bqId = bq.id;
                dataSource[index].name = bq.name;
                dataSource[index].numberOfJobs = '';
                dataSource[index].completionTime = '';
            });

            this.setState({dataSource});
        })
    }


    columns = [
        {
            key: "name",
            title: "BQ Name",
            dataIndex: "name",
            editable: false,
            render: (text, record) => {

                return <div key={record.id}>{text}</div>
            }
        },
        {
            key: "numberOfJobs",
            title: "Number of Jobs",
            dataIndex: "numberOfJobs",
            editable: true,
        },
        {
            key: "completionTime",
            title: "Completion Time",
            dataIndex: "completionTime",
            editable: true
        }
    ];

    handleSubmit = (data) => {
        console.log("State Data", data);
        let {projectId,phaseId} = this.props.projectDetails;

        this.props.submitSchedules(projectId,phaseId,data,this.props.nextDocument);
    };


    renderTableView = () => {
        let {projectDetails} = this.props;

        if (!projectDetails.editRights) {
            return <h5> You can not edit this document!</h5>
        }

        return (

            <div>
                <h3>Schedules</h3>
                <EditableTable dataSource={this.state.dataSource} columns={this.columns}
                               onSubmit={(data) => this.handleSubmit(data)}/>
            </div>
        )
    };

    renderFileView = ()=>{
        let {projectDetails, data, form} = this.props.projectProps;
        let {name} = form.pages[0].fields[0];
        if (!data[name]) return null;


        let boxStyle = {
            minHeight: "200px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            marginTop: "20px",
            marginRight: "20px",
            marginBottom: "20px",
        };

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width:"100%"
        };


        let fileJSX = (
            <Col span={5} style={{minWidth: "140px", marginTop: "20px", marginRight: "20px"}}>

                <Col span={24} style={boxStyle}>
                    <div style={divStyle}>
                        <img alt={"BOQ"} src={`/fileIcons/pdf.svg`} width={"70px"}/>
                        <p style={{
                            fontWeight: "normal",
                            fontSize: "18px",
                            marginTop: "5px",
                            width:"100%"
                        }}>Schedule A</p>
                    </div>
                </Col>

                <div span={24} style={{textAlign: "center"}}>
                    <Button type={"primary"} shape={"circle"} icon={"download"}
                            onClick={() => this.handleDownloadPreview(false)}/>

                    <Button type={"primary"} shape={"circle"} icon={"eye"} style={{marginLeft: "10px"}}
                            onClick={() => this.handleDownloadPreview(true)}/>
                    {
                        projectDetails.editRights ?
                            <Button type={"danger"} shape={"circle"} icon={"delete"} style={{marginLeft: "10px"}}
                                    onClick={() => this.setState({
                                        showConfirmModal: true
                                    })}/> : null
                    }
                </div>
            </Col>
        );

        return (
            <div>
                <Row>
                    <Col span={24}>
                        {fileJSX}
                    </Col>
                </Row>
            </div>
        );
    };
    render() {

        if (!this.state.dataSource.length) return null;

        if (!Object.keys(this.props.data).length) {
            return this.renderTableView()
        }

        return (
            <div>
                {this.renderFileView()}
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        submitSchedules: (projectId, phaseId,data,cb) => {
            dispatch(submitUpdatedBQSchedule(projectId,phaseId, data,cb))
        }
    }
};


const connected = connect(null, mapDispatchToProps)(Schedule);

export default connected;