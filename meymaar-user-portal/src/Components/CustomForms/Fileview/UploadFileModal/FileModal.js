import React from 'react';
import {Form, Button} from 'antd';
import 'antd/dist/antd.css';
import {submitFiles} from "../../../../store/project/project-actions";
import {connect} from "react-redux";
import FormElements from "../../../FormElements/FormElements";
import {BUTTON_COLOR} from "../../../../utils/common-utils";
import NewLoader from '../../../../Containers/Loader/NewLoader';

class FileModal extends React.Component {


    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };

    buttonItemLayout = {
        wrapperCol: {offset: 8},
    };

    handleFileSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {

            if (err) {
                return;
            }

            const values = {...fieldsValue};

            let {projectDetails, form} = this.props.projectProps;
            let {projectId, phaseId} = projectDetails;
            let {name} = form.pages[0].fields[0];

            let fileName = fieldsValue[this.fileJSON[0].name];

            name = this.props.isBQ ? 'tos' : name;
            form.submitURL = this.props.isBQ ? '/fields-management/project-field-data/{PROJECT_ID}' : form.submitURL;

            const formData = new FormData();
            formData.append('upload', values.upload, values.upload.name);


            this.props.submitFile(projectId, phaseId, name, formData, fileName, form.submitURL, this.props.toggleCB);
        })
    };

    fileJSON = [
        {
            element: 'input',
            name: 'name',
            type: 'formItem',
            label: 'File Name',
            required: true,
            whitespace: true,
            requiredMessage: "Required!",
            elementProps: {
                placeholder: 'File name'
            }

        },
        {
            element: 'file',
            name: 'upload',
            type: 'formButton',
            title: "Select File",
            elementProps: {
                acceptFiles: ".pdf",
                fileExt: ["pdf"]
            },
            required: true,
            requiredMessage: "Required!"
        }
    ];

    getFormButtons = () => {
        return (
            <Form.Item {...this.buttonItemLayout}>

                <Button htmlType="submit" style={{
                    height: "50px",
                    width: "100px",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>Upload File</Button>

                <Button style={{height: "50px", width: "90px", marginLeft: "2%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.cancelCB}>Cancel</Button>
            </Form.Item>
        )
    };


    renderForm = () => this.fileJSON.map((item, index) => {

        let formElementProps = {
            formItemLayout: item.type === 'formItem' ? this.formItemLayout : this.buttonItemLayout,
            element: item,
            editRights: this.props.projectProps.projectDetails.editRights,
            ...this.props
        };

        return <FormElements key={index} {...formElementProps}/>
    });

    render() {

        return (
            <div>
                <Form layout={"horizontal"} onSubmit={this.handleFileSubmit}>
                    {this.renderForm()}
                    {this.props.processing ? <NewLoader/> : null}
                    {this.getFormButtons()}
                </Form>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        submitFile: (projectId, phaseId, key, fileData, fileName, url, cb) => dispatch(submitFiles(projectId, phaseId, key, fileData, fileName, url, cb))
    }
};


const mapStateToProps = (state) => {
    return {
        processing: state.project_reducer.processing
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(FileModal);
export default Form.create()(connected)