import React from 'react';
import {connect} from 'react-redux';
import {Row, Col, Form, Button} from 'antd';
import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../Components/Modal/Modal';
import 'antd/dist/antd.css';
import FileModal from './UploadFileModal/FileModal';


import {deletePreviewFile, submitFieldManagementData} from "../../../store/project/project-actions";
import DeleteConfirmation from "../../ModalFactory/ConfirmationModal/DeleteConfirmation";
import {previewDownloadFile} from "../../../utils/server-utils";
import NewLoader from '../../../Containers/Loader/NewLoader';

class Fileview extends React.Component {


    state = {
        showConfirmModal: false
    };


    /*************************** Upload File Modal **********************************/

        // Upload Modal

    toggleUploadModal = () => {
        this.setState({showModal: !this.state.showModal});
    };


    renderFileModal = () => {

        return (
            <Modal style={{width: "500px"}} isOpen={this.state.showModal}>
                <ModalHeader style={{border: "unset"}}>
                    Upload File
                </ModalHeader>

                <ModalBody>
                    <FileModal {...this.props} toggleCB={this.toggleUploadModal} cancelCB={this.toggleUploadModal}/>
                </ModalBody>
            </Modal>
        )
    };

    /************************************************************/


    /********************* Delete File Methods & Modal *********************/


    handleFileDelete = () => {

        let {form, data, projectDetails} = this.props.projectProps;
        let {name} = form.pages[0].fields[0];
        let {activeFile} = this.state;

        //data[name] = data[name].filter(file => file.fileId !== this.state.activeFile.fileId);
        data[name] = null;

        this.props.submitFieldData(projectDetails.projectId, projectDetails.phaseId, data, form.submitURL, true, () => {
            this.props.deletePreviewFile(projectDetails.projectId, activeFile.fileId, this.toggleConfirmationModal);
        });

    };

    toggleConfirmationModal = () => {
        this.setState({showConfirmModal: !this.state.showConfirmModal});
    };

    renderDeleteConfirmation = () => {
        return (
            <DeleteConfirmation isOpen={this.state.showConfirmModal}
                                okCB={this.handleFileDelete} cancelCB={this.toggleConfirmationModal}
                                fileTitle={this.props.projectProps.form.title}/>
        );

    };

    /************************************************************************/

    handleDownloadPreview = (isPreview,title) => {
        console.log("Preview Download - Fileview");

        let {form, projectDetails} = this.props.projectProps;

        let dataProps = {
            projectId: projectDetails.projectId,
            phaseId: projectDetails.phaseId,
            data: form,
            isPreview: isPreview,
            title
        };

        previewDownloadFile(dataProps);
    };

    getFileOverview = () => {


        let {projectDetails, data, form, disable} = this.props.projectProps;
        let {name} = form.pages[0].fields[0];
        if (!data[name] && !projectDetails.editRights) {
            return <h4>You do not have rights to upload document!</h4>
        } else if (!data[name]) return null;


        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            marginTop: "20px",
            marginRight: "20px",
            marginBottom: "20px",
        };

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width: "100%"
        };

        let file = data[name];


        let fileJSX = <Col span={5}
                           style={{
                               overflowWrap: "break-word",
                               minWidth: "140px",
                               marginTop: "20px",
                               marginRight: "20px"
                           }}>

                <Col span={24} style={boxStyle}>
                    <div style={divStyle}>
                        <img alt={"BOQ"} src={`/fileIcons/pdf.svg`} width={"70px"}/>
                        <p style={{
                            fontWeight: "normal",
                            fontSize: "18px",
                            marginTop: "5px",
                            width: "100%"
                        }}>{file.fileName}</p>
                    </div>
                </Col>

                <div span={24} style={{textAlign: "center"}}>
                    <Button type={"primary"} shape={"circle"} icon={"download"}
                            onClick={() => this.handleDownloadPreview(false,file.fileName)}/>

                    <Button type={"primary"} shape={"circle"} icon={"eye"} style={{marginLeft: "10px"}}
                            onClick={() => this.handleDownloadPreview(true,file.fileName)}/>
                    {
                        projectDetails.editRights && !disable ?
                            <Button type={"danger"} shape={"circle"} icon={"delete"} style={{marginLeft: "10px"}}
                                    onClick={() => this.setState({
                                        activeFile: file,
                                        showConfirmModal: true
                                    })}/> : null
                    }
                </div>
            </Col>
        ;
        return (
            <div>
                <Row>
                    <Col span={24}>
                        {fileJSX}
                    </Col>
                </Row>
            </div>
        );
    };


    renderAddFileButton = () => {

        let {data, form, projectDetails} = this.props.projectProps;
        let {name} = form.pages[0].fields[0];
        let {multiple} = form.pages[0].fields[0];

        if (!multiple && data && data[name] && Object.keys(data).length) return null;


        return (
            <Button disabled={!projectDetails.editRights} style={{margin: "10px 0px", color: "#fff"}} type={"primary"}
                    size={"large"}
                    onClick={this.toggleUploadModal}>
                Add File
            </Button>

        )
    };

    renderHeaderButtons = () => {
        let {projectDetails} = this.props.projectProps;

        return (
            <div style={{width: "100%", display: "inline-block", marginBottom: "30px"}}>
                {projectDetails.editRights ?
                    <div style={{width: "50%", float: "left"}}>
                        {this.renderAddFileButton()}
                    </div> : null}
            </div>
        )
    };

    //Render Main Method

    render() {



        // Landing Overview to view, download and delete document file
        return (
            <div>
                {this.renderFileModal()}
                {this.renderDeleteConfirmation()}

                {this.renderHeaderButtons()}
                {this.getFileOverview()}
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        submitFieldData: (projectId, phaseId, data, submitURL, deleting, cb) => dispatch(submitFieldManagementData(projectId, phaseId, data, submitURL, deleting, cb)),
        deletePreviewFile: (projectId, fileId, cb) => dispatch(deletePreviewFile(projectId, fileId, cb))
    }
};


const connected = connect(null, mapDispatchToProps)(Fileview);
export default Form.create()(connected)