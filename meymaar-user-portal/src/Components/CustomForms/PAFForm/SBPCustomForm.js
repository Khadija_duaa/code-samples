import React from 'react';
import {Form, Button, Col, Input, InputNumber} from 'antd';
import NewLoader from '../../../Containers/Loader/NewLoader';
import './PAFStyle.css';
import {connect} from 'react-redux';
import 'antd/dist/antd.css';
import {BUTTON_COLOR} from "../../../utils/common-utils";
import {submitFormData} from "../../../store/project/project-actions";
import {memberRankJSON, presidentRankJSON} from "../../../utils/utilsJSON";

let dataKeys = ['Branch', 'Name', 'PakNo', 'Rank'];

class SBPCustomForm extends React.Component {


    constructor(props) {
        super(props);

        let key = props.projectProps.formJSON && props.projectProps.formJSON.pages[0].fields[0].name;
        let keyLength = props.projectProps.data && Object.keys(props.projectProps.data).length;
        let notDataExist = props.projectProps.data && ((keyLength === 1 && key && !props.projectProps.data[key]) || !keyLength);

        let userJSON = [];

        if (this.props.projectProps.data && this.props.projectProps.data[key]) {

            let membersLength = this.props.projectProps.data[key].members.length;

            for (let index = 0; index < membersLength; index++) {
                let memberObject = [...memberRankJSON];
                let object = {...memberObject[0]};
                object['key'] = index;

                userJSON = [...userJSON, {...object}];
            }
        } else {
            userJSON = [...memberRankJSON];
        }

        this.state = {
            disabled: props.projectProps.isDisabled,
            isEmpty: props.projectProps.isComplete && notDataExist,
            userFormJSON: userJSON
        };
    }

    componentDidMount() {
        let key = this.props.projectProps.formJSON && this.props.projectProps.formJSON.pages[0].fields[0].name;
        let {data} = this.props.projectProps;
        if (data && data[key]) {
            let presidentValues = data[key].president;
            let {members} = data[key];
            let membersValues = this.parseMembersJSON(members);


            let formData = {
                ...presidentValues,
                ...membersValues
            };

            this.props.form.setFieldsValue(formData);
        }
    }


    /*************************** Events  & Validations ****************/


        // Resetting Form Event
    handleResetClick = () => {
        this.setState({userFormJSON: memberRankJSON}, () => {
            this.props.form.resetFields();
        })
    };

    // Adding Member Event
    handleAddMemberClick = () => {
        let userRankJSON = this.props.form.getFieldValue('userRankJSON');
        let memberObject = [...memberRankJSON];

        let object = {...memberObject[0]};
        object.key = userRankJSON[userRankJSON.length - 1].key + 1;

        let userJSON = [...userRankJSON, {...object}];

        this.props.form.setFieldsValue({userRankJSON: userJSON})
    };

    // Removing Member Click
    handleRemoveMemberClick = (key) => {
        let userRankJSON = this.props.form.getFieldValue('userRankJSON');
        userRankJSON = userRankJSON && userRankJSON.filter(rank => rank.key !== key);
        this.props.form.setFieldsValue({userRankJSON});
    };


    validateNumber = (rules, value, callback) => {

        let regex = /^[0-9]*$/;
        if (value && !regex.test(value)) {
            callback('Invalid Input Value!');
        } else {
            callback();
        }
    };

    parseMembersJSON = (members) => {
        let membersData = {};

        for (let index = 0; index < members.length; index++) {
            let member = members[index];

            dataKeys.forEach((key, keyIndex) => {
                membersData[`member${index}${dataKeys[keyIndex]}`] = member[`member${dataKeys[keyIndex]}`];
            });
        }

        return membersData;
    };
    /*************************** END ******************/


    /****************************************** Getting Form Item *****************************************/
    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };
    buttonItemLayout = {
        wrapperCol: {offset: 8},
    };

    getFormItem = (rank, isMember = false, index) => {
        let {getFieldDecorator} = this.props.form;
        let jsx = '';
        let name = isMember ? `member${index}${rank.name}` : rank.name;


        switch (rank.type) {
            case 'text':
                jsx =
                    <Form.Item {...this.formItemLayout} label={rank.label}
                               style={rank.isStyle ? rank.style : {marginTop: "0px"}}>
                        {
                            getFieldDecorator(name, {
                                rules: [{
                                    required: rank.required,
                                    message: rank.message,
                                    whitespace: rank.whitespace
                                }]

                            })(<Input disabled={this.state.disabled} placeholder={rank.placeholder}/>)
                        }
                    </Form.Item>;
                break;
            case 'number':
                jsx = <Form.Item {...this.formItemLayout} label={rank.label}
                                 style={rank.isStyle ? rank.style : {marginTop: "0px"}}>
                    {
                        getFieldDecorator(name, {
                            rules: [{
                                required: rank.required,
                                message: rank.message,
                            },
                                {validator: this.validateNumber}
                            ]

                        })(<InputNumber disabled={this.state.disabled} style={{width: "100%"}} min={0}
                                        placeholder={rank.placeholder}/>)
                    }
                </Form.Item>;
                break;

            default:
                break;
        }

        return jsx;
    };
    /****************************************** END *****************************************/


    /************************ Form Submission Event ****************************/
    handleFormSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {

            if (err) {
                return;
            }

            const values = {...fieldsValue};

            let {formJSON} = this.props.projectProps;

            delete values['userRankJSON'];

            let keys = Object.keys(values);

            const presidentValues = keys.filter(key => key.includes('president'))
                .reduce((obj, key) => {
                    obj[key] = values[key];
                    return obj;
                }, {});

            const memberValues = keys.filter(key => key.includes('member')).sort()
                .reduce((obj, key) => {
                    obj[key] = values[key];
                    return obj;
                }, {});

            let ctr = 0;
            let index = 0;

            let membersArray = [];
            for (let key in memberValues) {
                if (index > 0 && (index) % 4 === 0) {
                    ctr++;
                    index = 0;
                }

                if (index < 1) {
                    membersArray.push({});
                }

                if (memberValues.hasOwnProperty(key)) {
                    membersArray[ctr][`member${dataKeys[index]}`] = memberValues[key];
                }
                index++;
            }

            let formData = {
                form2: {
                    president: {...presidentValues},
                    members: membersArray
                }
            };

            let successMessage = "Form has submitted successfully";
            let {backCB} = this.props;
            let {projectId,phaseId} = this.props.projectProps.projectDetails;
            this.props.submitFormData(projectId, phaseId, formJSON, formData, successMessage, backCB);

        })
    };

    // Rendering Form Submit And Reset Button
    renderButtons = () => {
        let {disabled} = this.state;
        if (disabled) return null;
        return (
            <Form.Item style={{textAlign: "center", marginBottom: "24px", marginTop: "24px"}}>
                <Button onClick={this.handleFormSubmit} style={{
                    height: "50px",
                    width: "120px",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff",
                }}>Submit</Button>

                <Button style={{height: "50px", width: "120px", marginLeft: "2%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.handleResetClick}>Reset</Button>
            </Form.Item>
        )
    };


    /************************ End ****************************/


    /****************************** 2nd Section ***************************/

        // Rendering Second Section
    renderSecondSection = () => {
        let textStyle = {
            textDecoration: "underline", textAlign: "center", marginTop: "50px", marginBottom: "40px"
        };
        let totalRanks = this.props.projectProps.formsSections > 1 && presidentRankJSON.length;
        this.props.form.getFieldDecorator('userRankJSON', {initialValue: this.state.userFormJSON});
        let userRankJSON = this.props.form.getFieldValue('userRankJSON');
        let {disabled} = this.state;
        return (
            <div>
                <h5 style={textStyle}>Members</h5>

                {
                    userRankJSON.map((rankObj, index) => {
                        let isLast = index === userRankJSON.length - 1;
                        return (
                            <div key={index} style={{marginTop: `${index > 0 ? "15px" : "0px"}`}}>
                                <div style={{width: "90%", display: "inline-block"}}>
                                    {
                                        rankObj.data.map((rank, keyIndex) => {
                                            return (
                                                <div className={`columns col-md-6`} key={keyIndex + totalRanks}>
                                                    {this.getFormItem(rank, true, rankObj.key)}
                                                </div>
                                            )
                                        })
                                    }
                                </div>

                                {!disabled ? <div style={{float: "right", width: "10%"}}>
                                    {
                                        isLast && userRankJSON.length < 10 ? <Button type={"primary"} shape={"circle"} size={"default"}
                                                         icon={"plus"} onClick={this.handleAddMemberClick}/> : null
                                    }
                                    {userRankJSON.length > 1 ? <Button type={"danger"} shape={"circle"} size={"default"}
                                                                       icon={"delete"}
                                                                       onClick={() => this.handleRemoveMemberClick(rankObj.key)}/> : null
                                    }
                                </div> : null
                                }
                            </div>
                        )
                    })
                }
            </div>
        )
    };

    /****************************** End ***************************/


    /****************************** 1st Section ***************************/
        // Rendering 1st Section
    renderFirstSection = () => {
        let textStyle = {
            textDecoration: "underline", textAlign: "center", marginBottom: "40px"
        };


        return (
            <div>
                <h5 style={textStyle}>President</h5>
                {
                    <div style={{width: "90%"}}>
                        {presidentRankJSON.map((rank, index) => {
                            return (
                                <div className={"columns col-md-6"} key={index}>
                                    {this.getFormItem(rank)}
                                </div>
                            )
                        })}
                    </div>

                }
            </div>
        )
    };

    /****************************** End ***************************/


        //Rendering a Form
    renderForm = () => {
        let {isEmpty, disabled} = this.state;
        let {formsSections} = this.props;

        if (isEmpty && disabled) {
            return (
                <h5 style={{textAlign: "center"}}>
                    --- No Data Exists ---
                </h5>
            )
        }
        return (
            <Col md={24}>
                <Form className={"paf-form"}>
                    {formsSections > 1 ? this.renderFirstSection() : null}
                    {this.renderSecondSection()}
                    {this.props.processing ? <NewLoader/> : null}
                    {this.renderButtons()}
                </Form>
            </Col>
        )
    };


    render() {
        return (
            <div>
                {this.renderForm()}
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        submitFormData: (projectId, phaseId, form, formData, message, successCB) => dispatch(submitFormData(projectId, phaseId, form, formData, message, successCB))
    }
};


const mapStateToProps = state => {
    return {
        processing: state.project_reducer.processing
    }
};


const createForm = Form.create()(SBPCustomForm);
const connected = connect(mapStateToProps, mapDispatchToProps)(createForm);
export default connected;