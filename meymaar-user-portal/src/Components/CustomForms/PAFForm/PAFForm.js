import React from 'react';
import {Form, Button, Col, Input, Icon} from 'antd';
import NewLoader from '../../../Containers/Loader/NewLoader';
import './PAFStyle.css';
import BackButton from '../../Buttons/BackButton';
import {connect} from 'react-redux';
import 'antd/dist/antd.css';
import {BUTTON_COLOR} from "../../../utils/common-utils";
import {submitFormData} from "../../../store/project/project-actions";
import {memberRankJSON, presidentRankJSON} from "../../../utils/utilsJSON";


class PAFForm extends React.Component {

    state = {
        disabled: this.props.isDisabled,
        memberJSON: memberRankJSON,
    };


    formItemLayout = {
        labelCol: {span: 6},
        wrapperCol: {span: 18},
    };
    buttonItemLayout = {
        wrapperCol: {offset: 6},
    };

    // Form Submission Event
    handleFormSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {

            if (err) {
                return;
            }

            const values = {...fieldsValue};

            let {formJSON} = this.props;

            let successMessage = "Form has submitted successfully";
            let formSubmit = this.props.onFormSubmit;
            this.props.submitFormData(this.props.projectId, this.props.phaseId, formJSON, values, successMessage, formSubmit && formSubmit);

        })
    };


    // Rendering President Section
    renderPresidentSection = () => {
        let textStyle = {
            textDecoration: "underline", textAlign: "center"
        };
        let {getFieldDecorator} = this.props.form;


        return (
            <div>
                <div className={"col-md-10"}>
                    <h5 style={textStyle}>President</h5>
                </div>
                {
                    presidentRankJSON.map((rank, index) => {
                        return (
                            <div className={"columns col-md-5"} key={index}>
                                <Form.Item {...this.formItemLayout} label={rank.label}>
                                    {
                                        getFieldDecorator(rank.name, {
                                            rules: [{
                                                required: rank.required,
                                                message: rank.message,
                                            }]

                                        })(<Input placeholder={rank.placeholder}/>)
                                    }
                                </Form.Item>
                            </div>
                        )
                    })
                }
            </div>
        )
    };

    // Rendering Member Section

    addField = () => {
        let {memberJSON} = this.state;
        let index = memberJSON.length + 1;
        memberJSON.push(
            [
                {
                    label: "Rank",
                    name: `member${index}Rank`,
                    required: true,
                    message: "Required!",
                    placeholder: "Rank",
                },
                {
                    label: "Name",
                    name: `member${index}Name`,
                    required: true,
                    message: "Required!",
                    placeholder: "Name",
                },
                {
                    label: "Pak No",
                    name: `member${index}PakNo`,
                    required: true,
                    message: "Required!",
                    placeholder: "Pak No"
                },
                {
                    label: "Branch",
                    name: `member${index}Branch`,
                    required: true,
                    message: "Required!",
                    placeholder: "Branch",
                }
            ]
        );

        this.setState({memberJSON});
    };


    handleDeleteRowClick = (index) => {
        let {memberJSON} = this.state;
        delete memberJSON[index];

        this.setState({memberJSON},()=>{
            console.log(memberJSON);
        });
    };

    renderMemberSection = () => {
        let textStyle = {
            textDecoration: "underline", textAlign: "center", marginTop: "50px"
        };
        let {getFieldDecorator} = this.props.form;

        let totalRanks = presidentRankJSON.length;

        return (
            <div>
                <div className={"col-md-10"}>
                    <h5 style={textStyle}>Members</h5>
                </div>
                {
                    this.state.memberJSON.map((rankArray, index) => {
                        return (
                            <div key={index} style={{marginTop: `${index > 0 ? "15px" : "0px"}`}}>
                                <div style={{display: "inline"}}>
                                    {
                                        rankArray.map((rank, keyIndex) => {
                                            // let size = keyIndex > rankArray.length -2 && keyIndex > 0?"5":"6";
                                            return (
                                                <div className={`columns col-md-5`} key={keyIndex + totalRanks}>
                                                    <Form.Item {...this.formItemLayout} label={rank.label}
                                                               style={rank.isStyle ? rank.style : {marginTop: "0px"}}>
                                                        {
                                                            getFieldDecorator(rank.name, {
                                                                rules: [{
                                                                    required: rank.required,
                                                                    message: rank.message
                                                                }]

                                                            })(<Input placeholder={rank.placeholder}/>)
                                                        }
                                                    </Form.Item>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                                {
                                    index > 0 ?
                                        <Icon style={{marginTop: "10px", verticalAlign: "top", fontSize: "20px"}}
                                              type="delete" theme="filled"
                                              onClick={() => this.handleDeleteRowClick(index)}/> : null
                                }
                            </div>
                        )
                    })
                }
            </div>
        )
    };

    renderAddFieldSection = () => {
        return (
            <Form.Item>
                <Button type="dashed" style={{width: '10%'}} onClick={this.addField}>
                    <Icon type="plus"/> Add field
                </Button>
            </Form.Item>
        )
    };

    // Rendering Form Submit And Reset Button
    renderButtons = () => {
        let {disabled} = this.state;
        if (disabled) return null;
        return (
            <Form.Item style={{textAlign: "center", marginBottom: "24px", marginTop: "24px"}}>
                <Button onClick={this.handleFormSubmit} style={{
                    height: "50px",
                    width: "120px",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff",
                }}>Submit</Button>

                <Button style={{height: "50px", width: "120px", marginLeft: "2%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={() => this.props.form.resetFields()}>Reset</Button>
            </Form.Item>
        )
    };


    //Rendering a Form
    renderForm = () => {
        return (
            <Col md={24}>
                <Form className={"paf-form"}>
                    {this.renderPresidentSection()}
                    {this.renderMemberSection()}
                    {this.renderAddFieldSection()}
                    {this.props.processing ? <NewLoader/> : null}
                    {this.renderButtons()}
                </Form>
            </Col>
        )
    };

    // Rendering Back Button
    renderBackButton = () => {
        if (!this.props.isComplete) return null;
        return <div style={{marginBottom: "40px", textAlign: "right"}}><BackButton returnCB={this.props.backCB}/></div>;
    };


    render() {
        return (
            <div>
                {this.renderBackButton()}
                {this.renderForm()}
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        submitFormData: (projectId, phaseId, form, formData, message, successCB) => dispatch(submitFormData(projectId, phaseId, form, formData, message, successCB))
    }
};


const mapStateToProps = state => {
    return {
        processing: state.project_reducer.processing
    }
};


const createForm = Form.create()(PAFForm);
const connected = connect(mapStateToProps, mapDispatchToProps)(createForm);
export default connected;