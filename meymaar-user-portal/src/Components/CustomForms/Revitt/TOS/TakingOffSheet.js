import React from 'react';
import {Row, Col, Form, Button} from 'antd';
import 'antd/dist/antd.css';

import {previewDownloadFile} from "../../../../utils/server-utils";

import TOSDetails from "./Details/TOSDetails";
//import {fetchRevitTakeOffs} from "../../../../utils/revit-server-utils";

class TakingOffSheet extends React.Component {

    state = {
        isDetails: false,
        takeOff: null,
        revitTakeOffs: null
    };



    handleDownloadPreview = (isPreview) => {
        console.log("Preview Download - Take Off");

        let {form, projectDetails} = this.props.projectProps;

        let dataProps = {
            projectId: projectDetails.projectId,
            phaseId: projectDetails.phaseId,
            data: form,
            isPreview: isPreview,
            title: form.title
        };

        previewDownloadFile(dataProps);
    };

    getFileOverview = () => {
        let {data, form} = this.props.projectProps;
        let {name} = form.pages[0].fields[0];
        // let {revitTakeOffs} = this.state;

        if (!data[name] || !Object.keys(data[name]).length) {
            return <h5 style={{marginTop: "10px", textAlign: "center"}}>
                --- No Take Off Sheet Exists For This Project ---
            </h5>;
        }

        // if (!revitTakeOffs.takeOffs.length) {
        //     return <h5 style={{marginTop: "10px", textAlign: "center"}}>
        //         --- No Taking of Sheet Exists For This Project ---
        //     </h5>;
        // }

        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            marginTop: "20px",
            marginRight: "20px",
            marginBottom: "20px",
        };

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width: "100%"
        };

        //data[name].takeOffs
        //let {takeOffs} = revitTakeOffs;

        let fileJSX = data[name].map((file, index) => {

            return (
                <Col key={index} span={5}
                     style={{overflowWrap: "break-word", minWidth: "140px", marginTop: "20px", marginRight: "20px"}}>

                    <Col span={24} style={boxStyle}>
                        <div style={divStyle}>
                            <img alt={"BOQ"} src={`/fileIcons/pdf.svg`} width={"70px"}/>
                            <p style={{
                                fontWeight: "normal",
                                fontSize: "18px",
                                marginTop: "5px",
                                width: "100%",
                                letterSpacing:"0.2px"
                            }}>{file.fileName}</p>
                        </div>
                    </Col>

                    <div span={24} style={{textAlign: "center"}}>
                        {/*<Button type={"primary"} shape={"circle"} icon={"download"}*/}
                        {/*onClick={() => this.setState({isDetails: true, takeOff: file.takeOffData})}/>*/}

                        <Button type={"primary"} shape={"circle"} icon={"eye"} style={{marginLeft: "10px"}}
                                onClick={() => this.setState({isDetails: true, takeOff: file.data})}/>
                    </div>
                </Col>
            )
        });

        //takeOff: file.takeOffData
        return (
            <div>
                <Row>
                    <Col span={24}>
                        {fileJSX}
                    </Col>
                </Row>
            </div>
        );
    };


    //Render Main Method
    render() {

        let {isDetails, takeOff} = this.state;


        if (isDetails) {
            return <TOSDetails returnCB={() => this.setState({isDetails: false, takeOff: null})} takeOff={takeOff}/>
        }

        return (
            <div>
                {this.getFileOverview()}
            </div>
        )
    }
}


export default Form.create()(TakingOffSheet)