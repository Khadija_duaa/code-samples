import React from 'react';
import {Table} from 'antd';
import 'antd/dist/antd.css';
import './TOSStyle.css';
import BackButton from '../../../../Buttons/BackButton';

class TOSDetails extends React.Component {

    /********************************** EVENTS ************************************/
    handleExpandClick = (event,record)=>{
        console.log(record);
    };
    /********************************** END ************************************/


    renderHeaderButtons = () => {
        let {returnCB} = this.props;

        return (
            <div style={{width: "100%", display: "inline-block", marginBottom: "30px", textAlign: "right"}}>
                {
                    <BackButton returnCB={returnCB} style={{width: "100px", height: "40px"}}/>
                }
            </div>
        )
    };


    getColumns = () => {
        let {takeOff} = this.props;
        let keys = Object.keys(takeOff[0]);

        let columns = [];

        keys.forEach(col=>{
           let data = {title: col,key:col, dataIndex: col};
           columns.push(data);
        });

        return columns;
    };



    render() {

        return (
            <div>
                {this.renderHeaderButtons()}
                <h3 style={{marginBottom: "40px"}}>Taking of Sheet Details</h3>

                <Table className={"custom-new-table tos-table"}
                       dataSource={this.props.takeOff}
                       rowKey={(record,index) => index}
                       columns={this.getColumns()}
                       bordered
                       pagination={{ pageSize: 10 }}
                />
            </div>
        )
    }
}

export default TOSDetails;