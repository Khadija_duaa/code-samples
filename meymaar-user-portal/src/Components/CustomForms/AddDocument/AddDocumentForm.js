import React from 'react';
import {connect} from 'react-redux';
import {Form, Input, Upload, Button, Icon} from 'antd';
import 'antd/dist/antd.css';
import {BUTTON_COLOR} from "../../../utils/common-utils";
import {createNewDocument} from "../../../store/project/project-actions";

const fileExt = ["pdf", "docx"];

class AddDocumentForm extends React.Component {

    formItemLayout = {
        labelCol: {span: 6},
        wrapperCol: {span: 18},
    };

    buttonItemLayout = {
        wrapperCol: {offset: 6},
    };


    handleBQSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {

            if (err) {
                return;
            }

            const values = {...fieldsValue};

            let {projectId, phaseId} = this.props;

            const formData = new FormData();
            formData.append('upload', values.upload.file, values.upload.file.name);

            this.props.createNewDocument(projectId, phaseId, formData, values.name, () => {
                this.props.toggleCB && this.props.toggleCB();
            })
        })
    };


    getTitleField = () => {
        let {getFieldDecorator} = this.props.form;
        return (
            <Form.Item key={1} {...this.formItemLayout} label={"Name"}>
                {getFieldDecorator('name', {rules: [{required: true, message: 'Document name is required',whitespace:true}]})(<Input
                    placeholder={"Name"}/>)}
            </Form.Item>
        )
    };

    // Minimum file size is 10MB
    validateFile = (rule, value, callback) => {

        //const form = this.props.form;
        let document = this.props.form.getFieldValue('upload');

        if (document) {
            let {size, name} = document.file;

            let ext = name.split('.');

            let fileSize = size / 1048576;

            if (fileSize > 300) {
                callback("Maximum file size limit is: 300 MB");
            } else if (!fileExt.includes(ext[ext.length-1])) {
                callback(`File with formats ${fileExt} are acceptable only!`);
            } else {
                callback();
            }
        } else {
            callback();
        }
    };

    renderUploadButton = () => {
        let {form} = this.props;
        const {getFieldDecorator} = this.props.form;

        const uploadProps = {

            showUploadList:{showRemoveIcon:false},
            accept: ".pdf,.docx",

            onRemove: () => {
                form.setFieldsValue({'upload': ''});
            },

            beforeUpload: (fileInfo) => {

                form.setFieldsValue({'upload': fileInfo});
                return false;
            },

            onChange: (change) => {

                form.setFieldsValue({'upload': change.file});

                if (change.fileList.length > 1) {
                    change.fileList.splice(0, 1);
                }
            }
        };

        return (
            <Form.Item {...this.buttonItemLayout}>
                {getFieldDecorator('upload', {
                    rules: [{
                        required: true,
                        message: 'Upload file'
                    }, {validator: this.validateFile}]
                })(
                    <Upload {...uploadProps}>
                        <Button>
                            <Icon type="upload"/> Select File
                        </Button>
                    </Upload>)}
            </Form.Item>
        )

    };

    renderFormButtons = () => {
        return (
            <Form.Item {...this.buttonItemLayout}>

                <Button htmlType="submit" style={{
                    height: "50px",
                    width: "100px",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>Upload File</Button>

                <Button style={{height: "50px", width: "90px", marginLeft: "2%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={() => this.props.toggleCB()}>Cancel</Button>
            </Form.Item>
        )
    };


    render() {
        return (
            <div>
                <Form onSubmit={this.handleBQSubmit} layout={"horizontal"}>

                    {this.getTitleField()}
                    {this.renderUploadButton()}
                    {this.renderFormButtons()}

                </Form>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        createNewDocument: (projectId, phaseId, file, title, cb) => dispatch(createNewDocument(projectId, phaseId, file, title, cb))
    }
};

const connected = connect(null, mapDispatchToProps)(AddDocumentForm);
export default Form.create()(connected);