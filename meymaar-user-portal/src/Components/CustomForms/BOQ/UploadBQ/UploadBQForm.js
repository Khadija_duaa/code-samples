import React from 'react';
import {Form, Input, Button, Select, Checkbox, InputNumber} from 'antd';
import 'antd/dist/antd.css';
import {BUTTON_COLOR} from "../../../../utils/common-utils";
import {connect} from 'react-redux';
import {uploadBQFile} from "../../../../store/project/project-actions";
import FormElements from "../../../FormElements/FormElements";
import {fetchBQCategories, fetchBQNames} from "../../../../utils/server-utils";
import NewLoader from '../../../../Containers/Loader/NewLoader';

const Option = Select.Option;

class UploadBQForm extends React.Component {

    componentDidMount() {
        let {projectId} = this.props.projectProps.projectDetails;

        fetchBQNames(projectId, (bqNames) => {
            this.setState({bqNames})
        });

        fetchBQCategories((bqCategories) => this.setState({bqCategories}));
    }

    state = {
        fileList: [],
        drawing: null,
        bqNames: [],
        error: null
    };

    uploadJSON = {
        element: 'file',
        name: 'file',
        title: 'Select File',
        required: true,
        requiredMessage: "File is required!",
        elementProps: {
            acceptFiles: ".csv",
            fileExt: ["csv"]
        }
    };

    formItemLayout = {
        labelCol: {span: 10},
        wrapperCol: {span: 14},
    };

    buttonItemLayout = {
        wrapperCol: {offset: 10},
    };

    /********************************** EVENTS *************************************/

    handleBQSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {

            if (err) {
                return;
            }


            const values = {...fieldsValue};

            let {projectId, phaseId, name} = this.props.projectProps.projectDetails;

            let isExternal = values.isExternal || false;

            let finalCategory = values.preCategory || values.category;
            let finalName = '';


            if (!isExternal) {
                finalName = values.preName ? values.preName === 'other' ? values.name : values.preName : values.name;
            } else {
                finalName = values.preName || values.name;
            }


            let externalPercentage = values.externalPercentage || 0;


            const formData = new FormData();

            formData.append('file', values[this.uploadJSON.name], values[this.uploadJSON.name].name);
            formData.append('category', finalCategory);
            formData.append('name', finalName);
            formData.append('externalPercentage', externalPercentage);
            formData.append('isInternal', !isExternal);
            formData.append('projectName', name);

            this.props.uploadBQFile(formData, projectId, phaseId, this.props.toggleCB, (message) => {
                this.setState({error: message})
            })
        })
    };

    /********************************** END *************************************/


    /********************************** Form Elements *************************************/

    renderCategoryOptions = () => {
        const {getFieldDecorator} = this.props.form;

        if (!this.state.bqCategories) return null;


        const options = () => this.state.bqCategories.map((option, index) => {
            return (
                <Option key={index} name={option.name} value={option.id}>{option.type}</Option>
            )
        });


        return (
            <Form.Item label={"Category"} {...this.formItemLayout}>
                {
                    getFieldDecorator(`preCategory`, {
                        initialValue: this.state.bqCategories[0].id
                    })(<Select placeholder={"Select BQ Category"}>
                        {options()}
                    </Select>)
                }
            </Form.Item>
        )
    };

    validateText = (rule, value, callback) => {
        if (value && value.toLowerCase() === 'other' && !this.props.form.getFieldValue('isExternal')) {
            callback(`Building name can not be ${value}`);
        } else {
            callback();
        }
    };

    getDescriptionOtherField = () => {
        let {getFieldDecorator, getFieldValue} = this.props.form;
        let {bqNames} = this.state;

        if (getFieldValue('preName') === 'other' && bqNames.length && !getFieldValue('isExternal')) {
            return (
                <Form.Item {...this.formItemLayout} label={"Name"}>
                    {
                        getFieldDecorator('name', {
                            rules: [{
                                required: true,
                                message: 'Building name is required',
                                whitespace: true
                            }, {validator: this.validateText}]
                        })(<Input placeholder={"Building Name"}/>)
                    }
                </Form.Item>
            )
        }
    };


    getBQDescriptionField = () => {
        let {getFieldDecorator, getFieldValue} = this.props.form;
        let {bqNames} = this.state;


        if (bqNames.length && !getFieldValue('isExternal')) {

            const namesJSX = bqNames.map((bqName, index) => {
                return <Option key={index} value={bqName}>{bqName}</Option>
            });

            namesJSX.push(<Option key={bqNames.length} value={"other"}>Other</Option>);

            return (
                <Form.Item {...this.formItemLayout} label={"Building Name"}>
                    {getFieldDecorator('preName', {
                        rules: [{
                            required: true,
                            message: "Building name is required",
                            whitespace: true
                        }]
                    })(<Select
                        placeholder={"Select Building"}>{namesJSX}</Select>)}
                </Form.Item>
            )
        } else {
            return (
                <Form.Item {...this.formItemLayout} label={"BQ Name"}>
                    {
                        getFieldDecorator('name', {
                            rules: [{
                                required: true,
                                message: 'BQ name is required',
                                whitespace: true
                            }, {validator: this.validateText}]
                        })(<Input placeholder={"BQ Name"}/>)
                    }
                </Form.Item>
            )
        }
    };

    isInternalUploaded = () => {
        let {data} = this.props.projectProps;
        let isInternal = false;

        if (data.bqs.length) {
            data.bqs.forEach(bq => {
                isInternal = isInternal || bq.isInternal;
            });
        }

        return isInternal;
    };

    getExternalCheckbox = () => {
        let {getFieldDecorator} = this.props.form;

        if (!this.isInternalUploaded()) return null;

        return (
            <Form.Item {...this.formItemLayout} label={"External"}>
                {getFieldDecorator('isExternal', {initialValue: false, valuePropName: 'checked'})(<Checkbox/>)}
            </Form.Item>
        )
    };

    getExternalPercentageField = () => {
        let {getFieldDecorator} = this.props.form;

        if (!this.props.form.getFieldValue('isExternal')) return null;

        return (
            <Form.Item {...this.formItemLayout} label={"External Percentage"}>
                {getFieldDecorator('externalPercentage', {
                    initialValue: 0,
                    rules: [{
                        required: true,
                        message: "Percentage is required"
                    }]
                })(<InputNumber style={{width: "100%"}} min={-100} max={100} formatter={value => `${value}%`}
                                parser={value => value.replace('%', '')}
                />)}
            </Form.Item>
        )

    };

    getFormButtons = () => {
        let isDisabled = !this.state.bqNames || !this.state.bqCategories;
        return (
            <Form.Item {...this.buttonItemLayout}>

                {this.props.processing ? <NewLoader/> : null}

                <Button htmlType="submit" disabled={isDisabled} style={{
                    height: "50px",
                    width: "100px",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>Upload File</Button>

                <Button style={{height: "50px", width: "90px", marginLeft: "2%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={() => this.props.toggleCB()}>Cancel</Button>
            </Form.Item>
        )
    };

    getUploadButton = () => {

        let formElementProps = {
            formItemLayout: this.buttonItemLayout,
            element: this.uploadJSON,
            editRights: this.props.projectProps.projectDetails.editRights,
            ...this.props
        };

        return <FormElements {...formElementProps}/>
    };

    /********************************** END *************************************/




    render() {
        let error = this.state.error;

        return (
            <div>
                {error ?
                    <label style={{fontSize: "18px", textAlign: "center", color: "#f5222d",width:"100%",marginBottom:"20px"}}>
                        {this.state.error}
                    </label>
                    : null
                }

                <Form onSubmit={this.handleBQSubmit}>


                    {this.getBQDescriptionField()}
                    {this.getDescriptionOtherField()}

                    {this.renderCategoryOptions()}

                    {this.getExternalCheckbox()}
                    {this.getExternalPercentageField()}

                    {this.getUploadButton()}
                    {this.getFormButtons()}
                </Form>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        uploadBQFile: (formData, projectId, phaseId, cb, errorCB) => dispatch(uploadBQFile(formData, projectId, phaseId, cb, errorCB)),
    }
};


const connected = connect(null, mapDispatchToProps)(UploadBQForm);
export default Form.create('UploadBOQ')(connected)