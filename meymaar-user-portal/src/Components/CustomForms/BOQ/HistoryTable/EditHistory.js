import React from 'react';
import {Table,message} from 'antd';
import axios from '../../../../utils/axios';
import {handleError} from "../../../../store/store-utils";
import {NOTIFICATION_TIME} from "../../../../utils/common-utils";


class EditHistory extends React.Component {

    state = {
        dataSource:null
    };

    componentDidMount() {


         let {projectId,bqId,bqItemId} = this.props;

         axios.get(`/bq-management/bqItemHistory/${projectId}/${bqId}/${bqItemId}`)
             .then(response=>{
                 this.setState({dataSource:response.data.bqItemHistory});
             }).catch(err=>{
                 let errorMessage = handleError(err);
                 message.s(errorMessage,NOTIFICATION_TIME);
         })
    }

    prepareHistoryTableColumns = () => {
        let dataSource= [...this.state.dataSource];
        console.log(dataSource);
        const columns = [];

        dataSource.forEach(data => {
            let columns = {
                key : data.name,
            };

            console.log(data);

        });

        return columns;
    };

    setStrikeThorugh = (text,colorCode)=> <strike style={{fontWeight:"bold",color:colorCode}}>{text}</strike>;

    render() {


        const tableColumns = [
            {key: "serial", title: "Serial", dataIndex: "serial", editable: false},
            {key: "description", title: "Description of work", dataIndex: "description", editable: false},
            {key: "quantity", title: "Qty", dataIndex: "quantity", editable: false,render:(text,record)=>this.setStrikeThorugh(text,record.colorCode)},
            {key: "unit", title: "Unit", dataIndex: "unit", editable: false},
            {key: "scheduleItem", title: "Schedule Item", dataIndex: "scheduleItem", editable: false},
            {key: "rate", title: "Rate", dataIndex: "rate", editable: false},
            {key: "amount", title: "Amount", dataIndex: "amount", editable: false,render:(text,record)=>this.setStrikeThorugh(text,record.colorCode)}
        ];


        if(!this.state.dataSource) return null;

        this.prepareHistoryTableColumns();

        return (
            <>
                <Table dataSource={this.state.dataSource} pagination={{ pageSize: 5 }} rowKey={(record,index)=>index}  columns={tableColumns} bordered/>
            </>
        )
    }
}


export default EditHistory