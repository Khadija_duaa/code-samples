import React from 'react';
import {Table, Button, Popconfirm} from 'antd';
import 'antd/dist/antd.css';
import {Form} from "antd/lib/index";
import EditableCell from './EditableCell';
import {connect} from 'react-redux';
import './EditableStyle.css';
import {submitEditedBQData} from "../../../../store/project/project-actions";
import {activeUserColor, formatAmount} from "../../../../utils/common-utils";
import FileModal from '../../Fileview/UploadFileModal/FileModal';
import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import ConfirmationModal from '../../../../Components/ModalFactory/MessageModal/ConfirmationModal';
import './bq_style.css';

import BQRowForm from "../BQRowForm/BQRowForm";
import NewLoader from '../../../../Containers/Loader/NewLoader';

const EditableContext = React.createContext();


class BQDetails extends React.Component {

    constructor(props) {
        super(props);


        this.state = {
            clickedIndex: -1,
            editedBqsData: [],
            isEditing: false,
            inkColor: activeUserColor(),
            bqId: props.id,
            dataSource: props.bqsItems,
            showModal: false,
            showUploadModal: false,
            rowData: null
        }
    }


    /*************************************** EVENTS **************************************/

    canEdit = () => {
        let {projectDetails, disable} = this.props.projectProps;

        return projectDetails.editRights && !disable && !this.props.isRevit;
    };

    handleDelete = (record, key) => {
        const dataSource = [...this.state.dataSource];
        let editedRows = this.state.editedBqsData;


        record.colorCode = this.state.inkColor;

        let delRow = {action: "DELETE", ...record};
        let otherRows = editedRows.filter(item => item.id !== record.id);


        editedRows = [...otherRows, delRow];


        this.setState({
            dataSource: dataSource.filter(item => item.serial !== key),
            editedBqsData: editedRows,
            isEditing: true
        });
    };

    handleSave = (row) => {
        const newData = [...this.state.dataSource];
        const index = newData.findIndex(item => row.serial === item.serial);
        let {editedBqsData} = this.state;

        const item = newData[index];

        newData.splice(index, 1, {
            ...item,
            ...row,
        });


        if (row[row.editedKey] !== item[row.editedKey]) {
            row.colorCode = this.state.inkColor;
            row.amount = parseFloat(row.quantity) * parseFloat(row.rate);
            let newRow = {action: "UPDATE", ...row};

            editedBqsData.push(newRow);

            this.setState({dataSource: newData, editedBqsData: editedBqsData, isEditing: true});
        }
    };

    handleAddRowClick = (newRow) => {

        let {bqsItems} = this.props;

        let newSerial = parseInt(bqsItems[bqsItems.length - 1].serial) + 1;


        let addedRow = {
            id: null,
            serial: newSerial,
            ...newRow,
            action: "ADD",
            colorCode: this.state.inkColor
        };


        let rowData = {
            editedBqsData: [
                addedRow
            ]
        };

        this.setState({showModal: false, rowData, showConfirmationModal: true});
    };

    submitBQDetails = () => {
        let {rowData} = this.state;
        let data = {editedBqsData: this.state.editedBqsData};

        if (rowData) {
            data = rowData;
        }

        let {projectId, phaseId, currentProjectOffice} = this.props.projectProps.projectDetails;

        this.props.submitEditedBQData(projectId, phaseId, currentProjectOffice, this.props.id, data, () => {
            this.props.successCB && this.props.successCB();
        });
    };

    renderTableFooter = () => {


        let {dataSource} = this.state;
        let bqTotalAmount = dataSource.reduce((a, b) => {
            return a + b.amount;
        }, 0);

        return (
            <div style={{margin: "0px 0px 100px 0px", width: "100%"}}>
                <div style={{width: "50%", float: "left"}}>
                    <Button disabled={!this.canEdit() || this.state.isEditing} style={{color: "#fff"}} type={"primary"}
                            onClick={this.toggleAddRowModal}> Add Row </Button>
                </div>

                <div style={{width: "50%", float: "right"}}>
                    <div>
                        <p>
                            <strong>Total BQ Items: </strong>{this.state.dataSource.length}
                        </p>

                        <p>
                            <strong>Total BQ Amount: </strong>{formatAmount(bqTotalAmount)}
                        </p>
                    </div>
                </div>
            </div>
        )
    };
    /*************************************** END **************************************/


    /*************************************** MODAL **************************************/

    toggleAddRowModal = () => {
        this.setState({showModal: !this.state.showModal});
    };

    renderAddRowModal = () => {
        return (
            <Modal style={{width: "650px"}} isOpen={this.state.showModal} toggle={this.toggleAddRowModal}>
                <ModalHeader style={{border: "unset"}}>
                    Add Row
                </ModalHeader>

                <ModalBody>
                    <BQRowForm cancelCB={() => this.toggleAddRowModal()}
                               addRowCB={(values) => this.handleAddRowClick(values)}/>
                </ModalBody>

            </Modal>
        )
    };

    // Upload Modal


    renderFileModal = () => {

        return (
            <Modal style={{width: "500px"}} isOpen={this.state.showUploadModal} toggle={this.handleCancelClick}>
                <ModalHeader style={{border: "unset"}}>
                    Upload New Taking off Sheet
                </ModalHeader>

                <ModalBody>
                    <FileModal {...this.props} isBQ={true} toggleCB={this.submitBQDetails}
                               cancelCB={this.handleCancelClick}/>
                </ModalBody>
            </Modal>
        )
    };
    toggleConfirmationModal = () => {
        this.setState({showConfirmationModal: !this.state.showConfirmationModal});
    };
    handleCancelClick = () => {
        this.setState({showConfirmationModal: false, showUploadModal: false}, () => this.submitBQDetails());
    };
    handleConfirmClick = () => {
        this.setState({showConfirmationModal: false, showUploadModal: true})
    };
    renderTOSConfirmationModal = () => {
        let message = 'Have you made changes in Taking off Sheet ?';

        return (
            <ConfirmationModal isOpen={this.state.showConfirmationModal} toggle={this.handleCancelClick}
                               okCB={this.handleConfirmClick} cancelCB={this.handleCancelClick}
                               message={message}/>
        );
    };

    /************************************************************/

    /*************************************** END **************************************/


    /*************************************** Editable Context **************************************/

    EditableRow = ({form, index, ...props}) => {

        let {editedBqsData} = this.state;

        props.className = 'editable-row';

        let dataProps = Object.assign({}, props);

        dataProps.defaultForm = true;

        let recordProps = {...dataProps.children[0]};

        if (editedBqsData.length) {

            let editedClass = false;
            editedBqsData.forEach(data => {
                editedClass = editedClass || data.serial === recordProps.props.record.serial;
            });

            if (editedClass) {
                props.className += ` bq-row-color-${this.state.inkColor.split('#')[1]}`;
            }
        }


        return (
            <EditableContext.Provider value={form}>
                <tr {...props}/>
            </EditableContext.Provider>
        );
    };
    EditableFormRow = Form.create()(this.EditableRow);


    renderStrikedHistoryRow = (text, record, key) => {

        let {editedBqsData} = this.state;

        let bqHistory = [...record.bqsDataHistories];

        let historyJSX = bqHistory.map((history, index) => {

            if ((index + 1 < bqHistory.length && history[key] === bqHistory[index + 1][key]) || (index + 1 === bqHistory.length && history[key] === text)) {
                return (
                    <div key={index} style={{fontWeight: "bold", color: history.colorCode}}>
                        <p>{key === 'amount' ? formatAmount(history[key]) : history[key]}</p>
                    </div>
                )
            }

            let strikeColor = record.colorCode;

            if (index + 1 < bqHistory.length) {
                strikeColor = history.colorCode;
            }


            return (
                <strike style={{color: strikeColor}} key={index}>
                    <p className={`bq-row-color-${history.colorCode.split('#')[1]}`}>
                        {key === 'amount' ? formatAmount(history[key]) : history[key]}
                    </p>
                </strike>
            )

        });

        let isEditedRow = editedBqsData.length && editedBqsData.filter(item => item.serial === record.serial);

        return (
            <div>
                <div>
                    {historyJSX}
                </div>
                <p style={{color: isEditedRow.length ? this.state.inkColor : record.colorCode}}>{key === 'amount' ? formatAmount(text) : text}</p>
            </div>
        )
    };

    columns = [
        {
            key: "serial",
            title: "Serial",
            dataIndex: "serial",
            editable: false,
        },
        {
            key: "description",
            title: "Description of work",
            dataIndex: "description",
            editable: false,
        },
        {
            key: "quantity",
            title: "Qty",
            dataIndex: "quantity",
            editable: this.canEdit(),
            render: (text, record) => this.renderStrikedHistoryRow(text, record, 'quantity')
        },
        {
            key: "unit",
            title: "Unit",
            dataIndex: "unit",
            editable: false,
        },
        {
            key: "scheduleItem",
            title: "Schedule Item",
            dataIndex: "scheduleItem",
            editable: false,
        },
        {
            key: "rate",
            title: "Rate",
            dataIndex: "rate",
            editable: this.canEdit(),
            render: (text, record) => this.renderStrikedHistoryRow(text, record, 'rate')
        },
        {
            key: "amount",
            title: "Amount",
            dataIndex: "amount",
            editable: false,
            render: (text, record) => this.renderStrikedHistoryRow(text, record, 'amount')
        },

        {
            key: "action", title: 'Action', dataIndex: 'action',
            render: (text, record) => {

                return (
                    <div style={{textAlign: "center"}}>
                        {this.state.dataSource.length > 1 ? (
                            <Popconfirm title="Sure to delete?"
                                        onConfirm={() => this.handleDelete(record, record.serial)}>
                                <a href={"javascript:;"} style={!this.canEdit() ? {
                                    color: "#D0D0D0",
                                    pointerEvents: 'none',
                                    cursor: "default"
                                } : null}>Delete</a>
                            </Popconfirm>
                        ) : null}
                    </div>
                )
            }
        }
    ];

    /*************************************** END **************************************/

    getButtons = () => {
        let {successCB} = this.props;
        return (
            <div>
                {this.canEdit() ?
                    <Button style={{color: "#fff"}}
                            type={"primary"}
                            icon={"save"}
                            disabled={!this.state.isEditing}
                            onClick={this.toggleConfirmationModal}>Save BQ Data</Button>
                    : null
                }

                <Button className={"custom-button custom-button-light"}
                        style={{padding: "unset", marginLeft: this.canEdit() ? "20px" : "0px"}} type={"default"}
                        icon={"rollback"}
                        onClick={successCB && successCB}>Back</Button>

            </div>
        )
    };

    render() {

        let dataSource = this.state.dataSource;


        // Calculating Amount

        if (dataSource && dataSource.length) {
            dataSource.forEach((item, index) => {
                item.index = index;
                item.amount = parseFloat(item.rate) * parseFloat(item.quantity);
            })
        }


        const components = {
            body: {
                row: this.EditableFormRow,
                cell: EditableCell,
            },
        };

        const tableColumns = this.columns.map((col) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    editable: col.editable,
                    dataIndex: col.dataIndex,
                    inputType: col.dataIndex === 'quantity' || col.dataIndex === 'rate' ? 'number' : 'text',
                    title: col.title,
                    handleSave: this.handleSave,
                }),
            };
        });

        return (
            <div>
                {this.renderTOSConfirmationModal()}
                {this.renderAddRowModal()}
                {this.renderFileModal()}

                <h3 style={{marginBottom: "40px"}}>BQ Details</h3>

                <Table components={components} dataSource={dataSource}
                       rowKey={record => record.index}
                       columns={tableColumns}
                       bordered
                />

                {this.renderTableFooter()}


                {this.props.processing ? <NewLoader/> : null}
                {this.getButtons()}
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        submitEditedBQData: (projectId, phaseId, currentOffice, bqId, data, cb) => dispatch(submitEditedBQData(projectId, phaseId, currentOffice, bqId, data, cb)),
    }
};


const mapStateToProps = (state) => {
    return {
        processing: state.project_reducer.processing,
        activeUser: state.user_reducer.activeUser
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(BQDetails);

export default connected;