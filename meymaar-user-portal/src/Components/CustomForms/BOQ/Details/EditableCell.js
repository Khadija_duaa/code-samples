import React from 'react';
import {Form, Input} from 'antd';
import 'antd/dist/antd.css';


const FormItem = Form.Item;


class EditableCell extends React.Component {
    state = {
        editing: false,
    };


    componentDidMount() {
        if (this.props.editable) {
            document.addEventListener('click', this.handleClickOutside, true);
        }
    }

    componentWillUnmount() {
        if (this.props.editable) {
            document.removeEventListener('click', this.handleClickOutside, true);
        }
    }


    handleClickOutside = (e) => {
        const {editing} = this.state;

        if (editing && this.cell !== e.target && !this.cell.contains(e.target)) {
            this.save();
        }
    };

    toggleEdit = () => {
        const editing = !this.state.editing;
        this.setState({editing}, () => {

            if (editing) {
                this.input.focus();
            }
        });
    };

    save = () => {
        const {record, handleSave} = this.props;

        this.props.form.validateFields((error, values) => {
            if (error) {
                return;
            }
            this.toggleEdit();

            handleSave({...record, ...values,editedKey:Object.keys(values)[0]});
        });
    };

    getInput = (inkColor) => {

        return <Input ref={node =>(this.input = node)}
                      onPressEnter={()=>this.save()}/>;
    };

    render() {
        const {editing} = this.state;
        const {
            editable,
            dataIndex,
            title,
            record,
            index,
            inputType,
            handleSave,
            ...restProps
        } = this.props;

        const {getFieldDecorator} = this.props.form;


        return (
            <td ref={node => (this.cell = node)} {...restProps}>
                {
                    editable ? (
                        editing ? (
                            <FormItem style={{margin: 0}}>
                                {getFieldDecorator(dataIndex, {
                                    rules: [{
                                        required: true,
                                        message: `${title} is required.`,
                                    }],
                                    initialValue: record[dataIndex],
                                })(this.getInput())}
                            </FormItem>
                        ) : (
                            <div
                                className="editable-cell-value-wrap"
                                style={{paddingRight: 24}}
                                onClick={this.toggleEdit}
                            >
                                {restProps.children}
                            </div>
                        )
                    ) : restProps.children
                }
            </td>
        );
    }
}

export default Form.create()(EditableCell);