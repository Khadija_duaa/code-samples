import React from 'react';
import {Row, Col, Button, Form, Divider, Icon, Input} from 'antd';
import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../Components/Modal/Modal';
import 'antd/dist/antd.css';
import UploadBQForm from "./UploadBQ/UploadBQForm";
import BQDetails from './Details/BQDetails';
import {
    deleteBQFile,
    resetRedux,
    setBQFiles,
    submitEditedBQData,
    submitSORPercentage
} from "../../../store/project/project-actions";
import {connect} from 'react-redux';
import DeleteConfirmation from "../../ModalFactory/ConfirmationModal/DeleteConfirmation";
import {fetchRevitBQs} from "../../../utils/revit-server-utils";

class BQForm extends React.Component {

    state = {
        showModal: false,
        showConfirmModal: false,
        isDetails: false,
        activeBQ: null,
        processing: false,
        mesDisable: true,
        revitBQs: null
    };

    componentDidMount() {
        this.props.resetFiles();
        let {projectDetails} = this.props.projectProps;
        fetchRevitBQs(projectDetails.projectId, (bqs) => {
            this.setState({revitBQs: bqs});
        });
    }


    /************************** EVENTS ************************/

    handleBQClick = (activeBQ, isRevit) => {
        this.setState({processing: true}, () => {
            setTimeout(() => {
                this.setState({activeBQ, isDetails: true, isRevit, processing: false});
            }, 300)
        })
    };

    handleDeleteBQClick = () => {

        let {activeBQ} = this.state;


        let {projectId, phaseId} = this.props.projectProps.projectDetails;

        this.props.deleteBQFile(projectId, phaseId, activeBQ.id, () => {
            this.toggleConfirmationModal();
        });
    };

    handleMESSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {
            if (err) {
                return;
            }

            let {phaseId, projectId} = this.props.projectProps.projectDetails;

            this.props.submitSORPercentage(projectId, phaseId, fieldsValue, () => {
                this.setState({mesDisable: true});
            });
        })
    };

    isExternalUploaded = () => {
        let {data} = this.props.projectProps;
        let isExternal = false;
        data.bqs.forEach(bq => {
            isExternal = isExternal || !bq.isInternal;
        });

        return isExternal;
    };

    canEdit = () => {
        let {projectDetails, disable} = this.props.projectProps;

        return projectDetails.editRights && !disable;
    };


    getDeleteButton = (bqFile, isInternal) => {
        let {data} = this.props.projectProps;
        let internalCount = data.bqs.filter(bq => bq.isInternal === true).length;

        if (isInternal && internalCount < 2 && this.isExternalUploaded()) return null;


        if (this.canEdit()) {
            return (
                <Button type={"danger"} shape={"circle"} icon={"delete"} style={{marginLeft: "10px"}}
                        onClick={() => this.setState({
                            activeBQ: bqFile,
                            showConfirmModal: true
                        })}/>
            )
        }
    };
    /************************** END ************************/


    /**************************************** MODAL *********************************************/

        // Upload Modal

    toggleUploadModal = () => {
        this.setState({showModal: !this.state.showModal});
    };
    renderBQModal = () => {

        return (
            <Modal style={{width: "550px"}} isOpen={this.state.showModal} toggle={this.toggleUploadModal}>
                <ModalHeader style={{border: "unset"}}>
                    Upload BQ File
                </ModalHeader>

                <ModalBody>
                    <UploadBQForm {...this.props} toggleCB={this.toggleUploadModal}/>
                </ModalBody>
            </Modal>
        )
    };


    //Confirmation Modal

    toggleConfirmationModal = () => {
        this.setState({showConfirmModal: !this.state.showConfirmModal});
    };
    renderConfirmationModal = () => {

        let {activeBQ} = this.state;

        if (!activeBQ) return null;
        let bqTitle = `${activeBQ.name} ${activeBQ.bqsCategory ? '( ' + activeBQ.bqsCategory.type + ' )' : ""}`;

        return (
            <DeleteConfirmation isOpen={this.state.showConfirmModal}
                                okCB={this.handleDeleteBQClick} cancelCB={this.toggleConfirmationModal}
                                fileTitle={bqTitle}/>
        );

    };


    /**************************************** END ********************************************/

    /*********** MES Service ***********/

    renderRevitBQFiles = () => {

    };
    renderBQFiles = (isInternal = false, isRevit = false) => {

        let {data} = this.props.projectProps;
        let {revitBQs} = this.state;
        if ((!data.bqs || !data.bqs.length) && (!revitBQs || !revitBQs.length)) return null;

        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            marginTop: "20px",
            marginRight: "20px",
            marginBottom: "20px",
        };

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width: "100%"
        };

        let bqsData = [];
        if (isRevit && isInternal) {
            bqsData = this.state.revitBQs;
        } else {
            bqsData = data.bqs.filter(bq => bq.isInternal === isInternal && bq.isRevittBq === isRevit);

        }

        if (!bqsData) return null;
        let bqJSX = bqsData.map((bqFile, index) => {
            return (
                <Col key={index} span={5}
                     style={{overflowWrap: "break-word", minWidth: "140px", marginTop: "20px", marginRight: "20px"}}>

                    <Col span={24} style={boxStyle}>
                        <div style={divStyle}>
                            <img alt={"BOQ"} src={`/fileIcons/BOQ.svg`} width={"70px"}/>
                            <p style={{
                                fontWeight: "normal",
                                fontSize: "18px",
                                marginTop: "5px",
                                width: "100%"
                            }}>{bqFile.name} <br/> {bqFile.bqsCategory ? `(${bqFile.bqsCategory.type})` : ""}</p>
                        </div>
                    </Col>

                    <div span={24} style={{textAlign: "center"}}>
                        <Button type={"primary"} shape={"circle"} icon={this.canEdit() && !isRevit ? "edit" : "eye"}
                                onClick={() => this.handleBQClick(bqFile, isRevit && isInternal)}/>

                        {!isRevit && this.getDeleteButton(bqFile, isInternal)}
                    </div>
                </Col>
            )
        });

        let headingText = "";

        if (!isRevit && isInternal) {
            headingText = "Internal BQ's";
        } else if (!isRevit && !isInternal) {
            headingText = "External BQ's";
        } else if (isRevit) {
            headingText = "Revit BQ's"
        }

        return (
            <div>
                {bqJSX.length ? <h3>{headingText}</h3> : null}
                <Row>
                    <Col span={24}>
                        {bqJSX}
                    </Col>
                </Row>
            </div>
        );
    };

    getMesForm = () => {
        let {data} = this.props.projectProps;

        const {getFieldDecorator} = this.props.form;
        let notBQ = !data.bqs || !data.bqs.length;


        return (
            <Form onSubmit={this.handleMESSubmit}>
                <Form.Item>
                    {getFieldDecorator('percentAboveSor', {
                        rule: [{
                            required: !this.state.mesDisable,
                            message: "Ruling percentage is required"
                        }],
                        initialValue: data.percentAboveSor
                    })(<Input type={"number"} required max={999} min={-50} style={{width: "35%"}}
                              disabled={this.state.mesDisable || !this.canEdit() || notBQ}
                              addonAfter={<Icon type="edit"
                                                onClick={!notBQ ? () => this.setState({mesDisable: !this.state.mesDisable}) : null}
                                                style={{cursor: `${this.canEdit() && !notBQ ? 'pointer' : 'default'}`}}/>}
                    />)}
                </Form.Item>


                <Form.Item>
                    <Button type={"default"} htmlType={"submit"}
                            disabled={this.state.mesDisable || !this.canEdit() || notBQ}>
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        )
    };

    renderMES = () => {

        return (
            <div style={{margin: "80px 0px 50px 0px"}}>
                <h5>Ruling Percentage</h5>
                {this.getMesForm()}
            </div>
        )
    };

    renderHeaderButtons = () => {

        return (
            <div style={{width: "100%", display: "inline-block", marginBottom: "30px"}}>
                {this.canEdit() ? <div style={{width: "50%", float: "left"}}>
                    <Button disabled={!this.canEdit()}
                            style={{color: "#fff"}} type={"primary"} size={"large"}
                            onClick={this.toggleUploadModal}>
                        Add BQs File
                    </Button>
                </div> : null}
            </div>
        )
    };

    render() {

        let {data} = this.props.projectProps;
        let {revitBQs} = this.state;
        if (this.state.isDetails) {
            return (
                <BQDetails isRevit={this.state.isRevit} {...this.props} {...this.state.activeBQ}
                           successCB={() => this.setState({isDetails: false, isRevit: false})}/>
            )
        }

        if ((!revitBQs || !revitBQs.length) && (data.bqs && !data.bqs.length && !this.canEdit())) {
            return <h4>NO BQ Exist!</h4>
        }

        return (
            <div>
                {this.renderBQModal()}
                {this.renderConfirmationModal()}

                {this.renderHeaderButtons()}

                {this.renderBQFiles(true, true)}

                {
                    data.bqs && data.bqs.length ?
                        <div>
                            {revitBQs && revitBQs.length ? <Divider/> : null}
                            {this.renderBQFiles(true, false)}

                            {this.renderMES()}

                            <Divider/>

                            {this.renderBQFiles(false, false)}

                        </div> : null
                }
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setBQFiles: (bqs) => dispatch(setBQFiles(bqs)),
        deleteBQFile: (projectId, phaseId, bqItemId, cb) => dispatch(deleteBQFile(projectId, phaseId, bqItemId, cb)),
        submitSORPercentage: (projectId, phaseId, data, cb) => dispatch(submitSORPercentage(projectId, phaseId, data, cb)),
        resetFiles: () => dispatch(resetRedux('bqFiles')),
        submitDeletedBQS: (projectId, phaseId, officeId, bqId, data, cb) => dispatch(submitEditedBQData(projectId, phaseId, officeId, bqId, data, cb)),
    }
};


const mapStateToProps = (state) => {
    return {
        bqFiles: state.project_reducer.bqFiles,
        processing: state.project_reducer.processing,
        revitProcessing: state.server_reducer.processing,
        activeUser: state.user_reducer.activeUser
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(BQForm);

export default Form.create()(connected);