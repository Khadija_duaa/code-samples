import React from 'react';
import {Form, Input, Button} from 'antd';
import 'antd/dist/antd.css';
import {BUTTON_COLOR} from "../../../../utils/common-utils";


class BQRowForm extends React.Component {

    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };

    buttonItemLayout = {
        wrapperCol: {offset: 8},
    };

    handleRowSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {

            if (err) {
                return;
            }

            const values = {...fieldsValue};

            values.amount = parseFloat(parseFloat(values.rate) * parseFloat(values.quantity));

            this.props.addRowCB && this.props.addRowCB(values);

        })
    };

    getBQForm = () => {
        let {getFieldDecorator} = this.props.form;

        let formJson = [
            {
                label: "Description of work",
                value: "description",
            },

            {
                label: "Quantity",
                value: "quantity",
            },
            {
                label: "Unit",
                value: "unit",
            },
            {
                label: "Schedule Item",
                value: "scheduleItem",
            },
            {
                label: "Rate",
                value: "rate",
            },
        ];

        return formJson.map((item, index) => {
            return (
                <Form.Item label={item.label} key={index} {...this.formItemLayout}>
                    {getFieldDecorator(item.value, {
                        rules: [{
                            required: true,
                            message: `${item.label} is required`,
                            whitespace: true
                        }]
                    })(<Input placeholder={item.label}/>)}
                </Form.Item>
            )
        });


    };


    renderFormButtons = () => {
        return (
            <Form.Item {...this.buttonItemLayout}>
                <Button htmlType="submit" style={{
                    height: "50px",
                    width: "120px",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>Add Row</Button>

                <Button style={{height: "50px", width: "120px", marginLeft: "2%", borderColor: BUTTON_COLOR}}
                        type={"default"} onClick={this.props.cancelCB}>Cancel</Button>
            </Form.Item>
        )
    };


    render() {
        return (
            <div>
                <Form onSubmit={this.handleRowSubmit}>
                    {this.getBQForm()}
                    {this.renderFormButtons()}
                </Form>
            </div>
        )
    }
}

export default Form.create()(BQRowForm);