import React from 'react';
import {Form, Input, InputNumber, DatePicker, Row, Col, Button,} from 'antd';
import 'antd/dist/antd.css'
import {formatCurrency, getProjectData, hasAuthority, isFraction, isNumber} from "../../../utils/common-utils";
import moment from 'moment';
import './billStyle.css';
import {LBR} from "../../LineBreak/LBR";
import {canApprovePayRAR} from "../../../Containers/UserPortal/Content/Works/Diary/components/ExeUtils";

let ordinal = require('ordinal');

//import FormElements from "../../../../../../../../Components/FormElements/FormElements";

let fields = [
    'withHoldingAmount', 'holdingAmtForMaterialByContractor', 'previousRarSumAmount', 'storeAmtInLastRar',
    'storeAmount', 'waterSum', 'rentOfBuildings', 'transportIncomeTaxAmount', 'tpStore', 'compensation'
];

class RARForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            disabled: props.isPreview,
            deductionFields: null
        }
    }


    // Form Items & Button Layout
    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16}
    };

    // Form Items & Button Layout
    reasonItemLayout = {
        labelCol: {span: 5},
        wrapperCol: {span: 19}
    };

    buttonItemLayout = {
        wrapperCol: {span: 24},
    };

    /************************************************** UTILS *****************************************************/
    canMakePayment = () => {
        let {requestDetails} = this.props;
        return (requestDetails.paymentStatus === "APPROVED" || requestDetails.paymentStatus === "PAID");
    };


    calculateOutStanding = () => {
        let {getFieldValue} = this.props.form;

        let paidAmount = getFieldValue('paidAmount');
        if (paidAmount && !isNaN(paidAmount) && isNumber(paidAmount)) {
            return getFieldValue('netAmount') - parseFloat(paidAmount.toFixed(2));
        } else {
            return getFieldValue('netAmount');
        }
    };
    /*************************************************** END ******************************************************/

    /*********** Billing Form EVENTS ************/

    getOrdinalNumber = () => {
        let {requestDetails} = this.props;
        return requestDetails.number ? ordinal(requestDetails.number) : null;
    };

    handleSubmitClick = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, fieldValues) => {
            if (err) return;

            let formData = {...fieldValues};
            formData.number = this.props.requestDetails.number;
            this.props.showModal(formData);
            // this.showConfirm();
        })
    };

    // Get Reciept Heading Text
    getRecieptHeading = () => {
        return (
            <h4 style={{textAlign: "center", margin: "20px 0px"}}>{this.getOrdinalNumber()} Running Account Receipt</h4>
        )
    };

    renderRejectionReason = (reason) => {
        let {getFieldDecorator} = this.props.form;

        return (
            <Form.Item {...this.reasonItemLayout} className={"reason-label"} label={"Rejection Reason"}>
                {getFieldDecorator('reason')(<h5 style={{color: "#F53649", lineHeight: "unset"}}>{reason}</h5>)}
            </Form.Item>
        )
    };

    // Header Fields of Create Bill Form
    getHeaderFields = () => {
        let {requestDetails} = this.props;
        let {getFieldDecorator} = this.props.form;
        let {disabled} = this.state;
        return (
            [
                <Row key={0}>
                    <Col md={16} offset={0}>
                        <Form.Item label={"SDO Voucher"} {...this.formItemLayout}>
                            {getFieldDecorator('sdoVoucherNumber', {
                                initialValue: requestDetails['sdoVoucherNumber'],
                                rules: [{
                                    required: !disabled,
                                    message: "Required!",
                                    whitespace: true
                                }]
                            })(
                                <Input disabled={disabled}
                                       placeholder={"SDO Voucher"}/>)}
                        </Form.Item>
                    </Col>

                    <Col md={7} offset={1}>
                        <Form.Item label={"Dated"} {...this.formItemLayout}>
                            {getFieldDecorator('dateCreated', {initialValue: !disabled ? moment(new Date()) : moment(requestDetails['dateCreated'])})(
                                <DatePicker
                                    disabled={true} format={"DD-MM-YYYY"}/>)}
                        </Form.Item>
                    </Col>
                </Row>,

                <Row key={1}>
                    <Col md={16} offset={0}>
                        <Form.Item label={"MAD Voucher No"} {...this.formItemLayout}>
                            {getFieldDecorator('madVoucherNumber', {
                                initialValue: requestDetails['madVoucherNumber'],
                                rules: [{
                                    required: !disabled,
                                    message: "Required!",
                                    whitespace: true
                                }]
                            })(
                                <Input disabled={disabled}
                                       placeholder={"MAD Voucher No"}/>)}
                        </Form.Item>
                    </Col>
                    <Col md={7} offset={1}>
                        <Form.Item label={"Dated"} {...this.formItemLayout}>
                            {getFieldDecorator('dateOptional', {initialValue: disabled && requestDetails['dateOptional'] ? moment(requestDetails['dateOptional']) : null})(
                                <DatePicker disabled={disabled} format={"DD-MM-YYYY"}/>)}
                        </Form.Item>
                    </Col>
                </Row>
            ]
        )
    };


    // Work Name
    getWorkName = () => {

        return (
            <Row>
                <Col md={16} offset={0}>
                    <Form.Item label={"Name of Work"} {...this.formItemLayout}>
                        {this.props.form.getFieldDecorator('projectName', {initialValue: getProjectData('name')})(<h5
                            className={"heading"}>{getProjectData('name')}</h5>)}
                    </Form.Item>
                </Col>
            </Row>

        )
    };


    // Contractor Name & Contractor Number
    getContractorFields = () => {
        let {getFieldDecorator} = this.props.form;

        let {contractorName, contractNumber} = this.props.requestDetails;
        return (
            [
                <Row key={2} style={{marginTop: "10px"}}>
                    <Col md={16} offset={0}>
                        <Form.Item label={"Name of Contractor"} {...this.formItemLayout}>
                            {getFieldDecorator('contractorName', {initialValue: contractorName})(<h5
                                className={"heading"}
                                style={{textDecoration: "underline"}}>
                                {contractorName}
                            </h5>)}
                        </Form.Item>
                    </Col>
                </Row>,

                <Row key={3} style={{marginTop: "10px"}}>
                    <Col md={16} offset={0}>
                        <Form.Item label={"Contract No/Year"} {...this.formItemLayout}>
                            {getFieldDecorator('contractNumber', {initialValue: contractNumber})(<h5
                                className={"heading"}
                                style={{textDecoration: "underline"}}>
                                {contractNumber}
                            </h5>)}
                        </Form.Item>
                    </Col>
                </Row>
            ]
        )
    };

    //Rendering Number Input.
    renderNumberInput = (fieldName, placeholder) => {
        let {disabled} = this.state;
        let {requestDetails} = this.props;
        return (
            <Form.Item>
                {this.props.form.getFieldDecorator(fieldName, {
                    initialValue: disabled ? requestDetails[fieldName] : 0,
                    rules: [{
                        required: !disabled,
                        message: "Required!"
                    },
                        {validator: this.validateNumber}
                    ]
                })(
                    <InputNumber style={{width: "100%"}} disabled={disabled} min={0} max={999999}
                                 placeholder={placeholder}/>)}
            </Form.Item>
        )
    };

    renderPercentInput = (fieldName, initial, label,) => {
        let formItemStyle = {
            width: "auto"
        };
        let {disabled} = this.state;
        let {requestDetails} = this.props;

        formItemStyle.marginLeft = label === "" ? 10 : 0;

        if (fieldName === 'transportIncomeTax' && requestDetails.number > 1) {
            disabled = true;
        }

        return (
            <Form.Item label={label} style={formItemStyle}>
                {this.props.form.getFieldDecorator(fieldName, {
                    initialValue: disabled ? requestDetails[fieldName] : initial,
                    rules: [{
                        required: !disabled,
                        message: "Required!"
                    }, {validator: this.validatePercentage}]
                })(<InputNumber disabled={disabled} style={{width: "100%"}} min={0} max={100}
                                formatter={value => `${value}%`}
                                parser={value => value.replace('%', '')}
                />)}
            </Form.Item>
        )
    };

    /***************** END ********************/


    /******************** Input Validation ************/

    validateNumber = (rules, value, callback) => {

        let notMaterial = Boolean(rules.field !== 'materialByContractor');

        let {getFieldValue} = this.props.form;
        let deductions = this.calculateDeductions();
        let netAmount = parseFloat((getFieldValue('totalStoreWork') - deductions).toFixed(2));

        if (value && !isFraction(value)) {
            callback('Invalid Input Value!');
        } else if (notMaterial && (value > getFieldValue('totalStoreWork') || netAmount < 0)) {
            callback('Value can not exceed!!');
        } else {
            callback();
        }
    };

    validatePaymentNumber = (rules, value, callback) => {
        let netAmount = this.props.requestDetails['netAmount'].toFixed(2);

        if (value && (!isFraction(value) || !isNumber(value))) {
            callback('Invalid Input Value!');
        } else if (value && value > netAmount) {
            callback("Value can not larger than Net amount");
        } else {
            callback();
        }
    };

    validatePercentage = (rule, value, callback) => {

        if (value && (!isNumber(value) || !isFraction(value) || value < 0 || value > 100)) {
            callback("Invalid percentage value");
        } else {
            callback();
        }
    };
    /************************ END *********************/


    /************ Bill Detail EVENTS *************/


        // Rendering Number Input Row Field
    renderNumberRow = (rowData, data, otherClass) => {
        return (
            <Row>
                <Col span={data.headingSpan} className={`title-text-billing ${otherClass && otherClass}`}>
                    <label>{rowData.label}</label>
                    <div>
                        {rowData.title}
                    </div>
                </Col>

                <Col span={data.dataSpan} offset={1}>
                    {this.renderNumberInput(rowData.name, rowData.placeholder)}
                </Col>

                <Col span={data.dataSpan} offset={1} className="data-billing"/>
            </Row>
        )
    };


    // Calculating Water Sum Amount
    getWaterSumAmount = (key) => {
        let {getFieldValue} = this.props.form;
        let value = getFieldValue(key);

        if (this.state.disabled) {
            return this.props.requestDetails['waterSum'];
        }

        return !isNaN(value) && value ? parseFloat((((value / 100) * getFieldValue('vettedWorkAmount')) / 100).toFixed(2)) : 0
    };


    // Calculating Income Tax Amount
    getIncomeTaxAmount = (key) => {
        if (this.state.disabled) {
            return this.props.requestDetails['transportIncomeTaxAmount'];
        }

        let {getFieldValue} = this.props.form;
        let value = getFieldValue(key);
        return !isNaN(value) && value ? parseFloat(((value / 100) * (getFieldValue('vettedWorkAmount') - getFieldValue('storeAmount'))).toFixed(2)) : 0
    };

    // Calculating All Deductions
    calculateDeductions = () => {
        let {getFieldsValue} = this.props.form;

        let deductionValues = getFieldsValue(fields);

        let values = Object.values(deductionValues);
        let deductionSum = 0;

        values.forEach(value => {
            let isValidValue = isNumber(value) || isFraction(value);
            deductionSum += value && value !== "" && isValidValue ? parseFloat(value) : parseFloat(0);
        });

        return parseFloat(deductionSum.toFixed(2));
    };

    // Calculating Net Amount
    calculateNetAmount = () => {
        if (this.state.disabled) {
            return this.props.requestDetails['netAmount'];
        }

        let {getFieldValue} = this.props.form;

        return parseFloat(((getFieldValue('totalOutstanding') + getFieldValue('totalStoreWork')) - getFieldValue('deductions')).toFixed(2));
    };

    // Rendering Fixed Deduction Detail Rows
    renderFixDeductionRows = (data) => {
        let {form} = this.props;
        let materialByContractor = form.getFieldValue('materialByContractor');
        let serial2Value = (data.workDone * (data.serial2Percentage / 100));
        let serial3Value = ((materialByContractor && isNumber(materialByContractor) ? materialByContractor : 0) * (data.serial3Percentage / 100));

        let isVettedTotalEqual = form.getFieldValue('vettedWorkAmount') === form.getFieldValue('totalProjectCost');

        let rowsJSON = [
            {
                label: "a",
                title: `${data.serial2Percentage}% of serial 2 as per condition 64 of PAFW-2249@4 or`,
                title2: `${data.serial2Percentage}% of serial 2 authorised vide E-in-C's Branch No___of____`,
                dataText1: !isVettedTotalEqual ? formatCurrency(serial2Value) : formatCurrency(0),
                isBold: false,
                value: isVettedTotalEqual ? 0 : serial2Value,
                dataText2: '',
                name: "withHoldingAmount"
            },
            {
                label: "b",
                title: `${data.serial3Percentage}% of serial 3 as per condition 64 of PAFW-2249`,
                dataText1: formatCurrency(serial3Value),
                isBold: false,
                value: serial3Value,
                dataText2: '',
                name: "holdingAmtForMaterialByContractor"
            },
            {
                label: "c",
                title: "Total value of previous RAR's",
                dataText1: formatCurrency(data.previousRAR),
                isBold: false,
                value: data.previousRAR,
                dataText2: '',
                name: "previousRarSumAmount"
            }
        ];


        return rowsJSON.map((row, index) => {
            return (
                <Row key={index}>
                    <Col span={data.headingSpan} className="title-text-billing">
                        <label>{row.label}.</label>
                        <div>
                            {row.title}
                            {row.title2 ? <><LBR/>{row.title2}</> : null}
                        </div>
                    </Col>

                    {row.dataText1 === '' ?
                        <Col span={data.dataSpan} offset={1} className="data-billing"/>
                        :
                        <Col span={data.dataSpan} offset={1}>
                            <Form.Item>
                                {form.getFieldDecorator(row.name, ({initialValue: row.value}))(
                                    <span className={"data-billing"}>{row.dataText1}</span>)}
                            </Form.Item>
                        </Col>
                    }
                    {row.dataText2 === '' ?
                        <Col span={data.dataSpan} offset={1} className="data-billing"/>
                        :
                        <Col span={data.dataSpan} offset={1} className="data-billing">
                            <Form.Item>
                                {form.getFieldDecorator(row.name, ({initialValue: row.value}))(<span
                                    className={"data-billing"}>{row.dataText2}</span>)}
                            </Form.Item>
                        </Col>
                    }
                </Row>
            )
        })
    };


    //Rendering Other Deduction Changes
    renderOtherDeductionFields = (data) => {
        let {getFieldDecorator} = this.props.form;

        let rowData = [
            {
                label: "2.",
                title: "Rent of Building",
                name: "rentOfBuildings",
                placeholder: "Building Rent"
            },
            {
                label: "4.",
                title: "TP or other store on hire.",
                name: "tpStore",
                placeholder: "TP or Store"
            }
        ];


        return (
            <div>
                <Row>
                    <Col span={data.headingSpan} className="title-text-billing other-detail">
                        <label>1.</label>
                        {this.renderPercentInput('waterPercentage', 0, "Water")}
                    </Col>

                    <Col span={data.dataSpan} offset={1}>
                        <Form.Item>
                            {getFieldDecorator('waterSum', {initialValue: this.getWaterSumAmount('waterPercentage')})(
                                <span className={"data-billing"}>
                                    {
                                        formatCurrency(this.getWaterSumAmount('waterPercentage'))
                                    }
                                    </span>)}

                        </Form.Item>
                    </Col>

                    <Col span={data.dataSpan} offset={1}/>
                </Row>

                {this.renderNumberRow(rowData[0], data, "other-detail")}

                <Row>
                    <Col span={data.headingSpan} className="title-text-billing other-detail">
                        <label>3.</label>
                        Transport Income Tax
                        @ {this.renderPercentInput('transportIncomeTax', data.incomeTax, "")} without store
                    </Col>

                    <Col span={data.dataSpan} offset={1}>
                        <Form.Item>
                            {getFieldDecorator('transportIncomeTaxAmount', {initialValue: this.getIncomeTaxAmount('transportIncomeTax')})(
                                <span className={"data-billing"}>
                                    {
                                        formatCurrency(this.getIncomeTaxAmount('transportIncomeTax'))
                                    }
                                    </span>)}
                        </Form.Item>
                    </Col>

                    <Col span={data.dataSpan} offset={1}/>
                </Row>

                {this.renderNumberRow(rowData[1], data, "other-detail")}
            </div>
        )
    };


    // Rendering Deduction Other Changes
    renderDeductionOtherChanges = (data) => {
        return (
            <Row>
                <Col span={24} className={"title-text-billing deduction-text"}>
                    <label>f.</label>
                    <div style={{width: "100%"}}>
                        Other changes during the period of last RAR
                        {this.renderOtherDeductionFields(data)}
                    </div>
                </Col>
            </Row>
        )
    };


    // Rendering Deduction Details And Other Changes Above
    renderDeductionDetails = (data) => {
        let {getFieldDecorator} = this.props.form;
        let rowData = [
            {
                label: "d.",
                title: "Total value of store accounted for in the last RAR",
                name: "storeAmtInLastRar",
                placeholder: "Store Accounted"
            },
            {
                label: "e.",
                title: "Value of store issued since last RAR upto date of Voucher USAR No _____________ to and\n" +
                "                            corresponding intent.",
                name: "storeAmount",
                placeholder: "Last RAR Issued Value"
            },
            {
                label: "g.",
                title: "Compensation as per condition 50 of PAFW-2249",
                name: "compensation",
                placeholder: "Compensation"
            }
        ];

        return (
            <div>
                {this.renderFixDeductionRows(data)}
                {this.renderNumberRow(rowData[0], data)}
                {this.renderNumberRow(rowData[1], data)}

                {this.renderDeductionOtherChanges(data)}

                {this.renderNumberRow(rowData[2], data)}

                <Row>
                    <Col span={data.headingSpan} className="total-deduction">
                        Total of deductions
                    </Col>

                    <Col span={data.dataSpan} offset={1}>
                        <Form.Item>
                            {getFieldDecorator('deductions', {initialValue: parseFloat(this.calculateDeductions().toFixed(2))})(
                                <span className={"data-billing"}>
                                    {formatCurrency(this.calculateDeductions())}
                                    </span>)}
                        </Form.Item>
                    </Col>

                    <Col span={data.dataSpan} offset={1}/>
                </Row>

            </div>
        )
    };


    // Rendering Deduction Section And Details Above
    renderDeductionsSection = (data) => {
        return (
            <Row className={"fields-container-bill deduction-row"}>
                <Col span={24} className={"title-text-billing deduction-text"}>
                    <label>5.</label>
                    <div style={{width: "100%"}}>
                        Deductions
                        {this.renderDeductionDetails(data)}
                    </div>
                </Col>
            </Row>
        )
    };


    // Rendering Fix Header Rows which contains same UI
    renderFixTableRows = (data) => {
        let {getFieldDecorator} = this.props.form;
        let rowsJSON = [
            {
                title: "Estimated value of work order to date under contract.",
                dataText1: "Rupees",
                dataText2: formatCurrency(data.projectCost),
                value: data.projectCost,
                name: "totalProjectCost"
            },
            {
                title: "Estimated value of work executed on contract rate.",
                dataText1: "",
                dataText2: formatCurrency(data.workDone),
                value: data.workDone,
                name: "vettedWorkAmount"
            }
        ];

        return rowsJSON.map((row, index) => {
            return (
                <Row key={index} className="fields-container-bill">
                    <Col span={data.headingSpan} className="title-text-billing">
                        <label>{index + 1}.</label>
                        {row.title}
                    </Col>
                    {
                        row.dataText1 === "" ? <Col span={data.dataSpan} offset={1}/>
                            :
                            <Col className={"data-billing"} span={data.dataSpan} offset={1}>
                                {row.dataText1}
                            </Col>
                    }
                    <Col span={data.dataSpan} offset={1} className="data-billing">
                        <Form.Item>
                            {getFieldDecorator(row.name, {initialValue: row.value})(<span
                                className={"data-billing"}>{row.dataText2}</span>)
                            }
                        </Form.Item>
                    </Col>
                </Row>
            )
        })
    };


    renderPaymentFields = (data) => {
        let {getFieldDecorator} = this.props.form;
        let {requestDetails} = this.props;
        let isPaid = requestDetails.paymentStatus === "PAID";
        let paymentText = isPaid ? "Paid Amount" : "Enter Payment Amount";
        let style = {width: "100%", textAlign: "right", marginTop: "unset"};

        if (!isPaid) {
            let officeData = canApprovePayRAR(requestDetails.rarPaymentOffice);
            if (!officeData.inOffice || !hasAuthority('CAN_PAY_RAR')) return null;
        }

        return (
            <>
                <Row className="fields-container-bill">
                    <Col span={data.headingSpan} className="title-text-billing">
                        <p style={style}>{paymentText}</p>
                    </Col>

                    <Col span={data.dataSpan} offset={1} className={"data-billing"}>
                        <Form.Item>
                            {getFieldDecorator('paidAmount', {
                                initialValue: isPaid ? requestDetails['paidAmount'] : 0,
                                rules: [{
                                    required: !isPaid,
                                    message: "Required!"
                                }, {validator: this.validatePaymentNumber}]
                            })(
                                <InputNumber style={{width: "100%"}} min={0} disabled={isPaid}/>)}
                        </Form.Item>
                    </Col>


                    <Col span={data.dataSpan} offset={1} className="data-billing"/>
                </Row>
                <Row className="fields-container-bill">
                    <Col span={data.headingSpan} className="title-text-billing">
                        <p style={style}>Outstanding Amount</p>
                    </Col>

                    <Col span={data.dataSpan} offset={1}/>

                    <Col span={data.dataSpan} offset={1} className="data-billing">
                        <Form.Item>
                            {
                                getFieldDecorator('outStandingAmount', {initialValue: this.calculateOutStanding()})(
                                    <span
                                        className={"data-billing"}>{formatCurrency(this.calculateOutStanding())}</span>)
                            }
                        </Form.Item>
                    </Col>
                </Row>
            </>
        )
    };
    /***************** END ********************/

    getTotalStoreWorkAmount = (data) => {
        let {getFieldValue} = this.props.form;
        if (getFieldValue('materialByContractor') && isNumber(getFieldValue('materialByContractor'))) {
            return parseFloat((getFieldValue('materialByContractor') + data.workDone).toFixed(2));
        } else {
            return parseFloat(data.workDone.toFixed(2));
        }

    };

    getPreviousOutstanding = (data) => {
        return parseFloat(data.totalOutstanding.toFixed(2));
    };

    // Rendering Table View Fields & Inputs
    getTableFields = () => {
        let {totalProjectCost, vettedWorkAmount, previousRarSumAmount, serial4fIncomeTaxPercentage, wallet} = this.props.requestDetails;
        let data = {
            headingSpan: 14,
            dataSpan: 4,
            projectCost: totalProjectCost,
            workDone: vettedWorkAmount,
            totalOutstanding: wallet ? wallet.totalOutStandingAmount : 0,
            serial2Percentage: 10,
            serial3Percentage: 25,
            previousRAR: parseFloat(previousRarSumAmount.toFixed(2)),
            incomeTax: serial4fIncomeTaxPercentage
        };
        let {form} = this.props;
        let {getFieldDecorator} = form;


        return (
            <Row className={"create-bill"} style={{borderTop: "2px solid black", margin: "30px 0px"}}>
                <Col span={24}>

                    {this.renderFixTableRows(data)}

                    <Row className="fields-container-bill">
                        <Col span={data.headingSpan} className="title-text-billing">
                            <label>3.</label>
                            <div>
                                Estimated value of material provided by contractor on which he is eligible for secured
                                advances.

                                <p className={"data-billing"} style={{textAlign: "center"}}>
                                    Total value of work done and store (2+3)</p>
                            </div>
                        </Col>

                        <Col span={data.dataSpan} offset={1}>
                            {this.renderNumberInput('materialByContractor', "Store Amount")}

                            <Form.Item>
                                {getFieldDecorator('totalStoreWork', {initialValue: this.getTotalStoreWorkAmount(data)})(
                                    <p className={"data-billing"}>
                                        {formatCurrency(this.getTotalStoreWorkAmount(data))}
                                    </p>)}
                            </Form.Item>
                        </Col>

                        <Col span={data.dataSpan} offset={1} className="data-billing"/>
                    </Row>


                    <Row className="fields-container-bill">
                        <Col span={data.headingSpan} className="title-text-billing">
                            <label>4.</label>
                            <div>
                                Total Outstanding Amount
                            </div>
                        </Col>

                        <Col span={data.dataSpan} offset={1}/>

                        <Col span={data.dataSpan} offset={1} className="data-billing">
                            <Form.Item>
                                {getFieldDecorator('totalOutstanding', {initialValue: this.getPreviousOutstanding(data)})(
                                    <span
                                        className={"data-billing"}>{formatCurrency(this.getPreviousOutstanding(data))}</span>)
                                }
                            </Form.Item>
                        </Col>
                    </Row>

                    {this.renderDeductionsSection(data)}
                    <Row className="fields-container-bill">
                        <Col span={data.headingSpan} className="title-text-billing">
                            <p style={{width: "100%", margin: "unset", textAlign: "right"}}>Net Amount</p>
                        </Col>

                        <Col span={data.dataSpan} offset={1}/>

                        <Col span={data.dataSpan} offset={1} className="data-billing">
                            <Form.Item>
                                {
                                    getFieldDecorator('netAmount', {initialValue: this.calculateNetAmount()})(
                                        <span className={"data-billing"}>
                                            {formatCurrency(this.calculateNetAmount())}
                                    </span>)
                                }
                            </Form.Item>
                        </Col>
                    </Row>
                    {this.canMakePayment() ? this.renderPaymentFields(data) : null}
                </Col>
            </Row>
        )
    };

    // Rendering Final Bill Check
    // getFinalBillCheck = () => {
    //     let {form, requestDetails} = this.props;
    //     let {disabled} = this.state;
    //
    //     return (
    //         <Form.Item>
    //             {form.getFieldDecorator('isFinal', {
    //                 valuePropName: 'checked',
    //                 initialValue: disabled ? requestDetails['isFinal'] : false
    //             })(<Checkbox disabled={disabled}>Is Final RAR?</Checkbox>)}
    //         </Form.Item>
    //     )
    // };

    // Rendering Form Submit Buttons
    getFormButtons = () => {
        let {paymentStatus, rarApprovalOffice, rarPaymentOffice, isEven} = this.props.requestDetails;
        let submitButtonText = paymentStatus && paymentStatus === 'APPROVED' ? 'PAY RAR' : 'SUBMIT';
        let approvalData = {};
        let paymentData = {};

        if (paymentStatus && paymentStatus === 'PENDING') {
            approvalData = canApprovePayRAR(rarApprovalOffice);
            paymentData = canApprovePayRAR(rarPaymentOffice);
        } else if (paymentStatus && paymentStatus === "APPROVED") {
            approvalData = canApprovePayRAR(rarPaymentOffice);
        }


        let buttonsJSX = '';

        // Check is Even here, isEven and
        let canApproveRAR = Object.keys(approvalData).length && ((isEven && approvalData.inOffice) || (!isEven && paymentData.inOffice));

        if (paymentStatus === "PENDING" && hasAuthority('CAN_APPROVE_RAR') && this.props.isPayment && canApproveRAR) {
            buttonsJSX = <div>
                <Button className={`modal-button ${this.state.disabled ? 'modal-button-preview' : ''}`}
                        onClick={this.props.onApproveClick}>
                    APPROVE
                </Button>

                <Button className="reject-button " onClick={this.props.onRejectionClick}>
                    REJECT
                </Button>
            </div>
        }
        else if (paymentStatus === "APPROVED" && hasAuthority('CAN_PAY_RAR') && Object.keys(approvalData).length && approvalData.inOffice) {
            buttonsJSX = <Button className="modal-button modal-button-preview"
                                 onClick={this.handleSubmitClick}>{submitButtonText}</Button>
        } else if (!this.props.isPayment) {
            buttonsJSX = <Button className={"modal-button"} style={{marginLeft: !paymentStatus && "0px"}}
                                 onClick={this.handleSubmitClick}>{submitButtonText}</Button>
        }

        return (
            <Form.Item {...this.buttonItemLayout}>
                <Col className="custom-colum" md={24}>
                    {buttonsJSX}
                </Col>
            </Form.Item>
        )
    };

    /***************************** END ****************************/


    render() {

        let {requestDetails} = this.props;
        let canShowButtons = !requestDetails.paymentStatus || requestDetails.paymentStatus === "PENDING" || requestDetails.paymentStatus === "APPROVED";
        return (
            <div>
                <Form layout={"inline"} className={"createBillForm"}>
                    {this.getRecieptHeading()}
                    {requestDetails.paymentStatus === "REJECTED" ? this.renderRejectionReason(requestDetails.rejectionReason) : null}
                    {this.getHeaderFields()}
                    {this.getWorkName()}
                    {this.getContractorFields()}
                    {this.getTableFields()}
                    {/*{this.getFinalBillCheck()}*/}
                    {canShowButtons ? this.getFormButtons() : null}
                </Form>
            </div>
        )
    }
}

export default Form.create()(RARForm);