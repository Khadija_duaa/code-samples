import React from 'react';
import CheckboxTable from "../../Table/CheckboxTable/CheckboxTable";
import {fetchAllFirms, previewDownloadFile} from "../../../utils/server-utils";
import 'antd/dist/antd.css';
import {connect} from 'react-redux';
import {submitCheckedBidFirms} from "../../../store/project/project-actions";
import NewLoader from "../../../Containers/Loader/NewLoader";

class BidFirms extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSource: null,
            isDetails: false
        }
    }

    componentDidMount() {
        this.fetchFirms();
    }

    fetchFirms = () => {
        fetchAllFirms((dataSource) => {
            this.setState({dataSource});
        })
    };

    columns = [
        {
            title: 'Firm Id',
            dataIndex: 'firm_id',
        },
        {
            title: 'Firm Name',
            dataIndex: 'firm_name',
        },
        {
            title: 'Email',
            dataIndex: 'email',
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
        }
    ];

    handleSubmit = (data) => {

        let {projectId, phaseId} = this.props.projectDetails;

        let formData = {
            [this.props.fields[0].name]: data
        };

        this.props.submitCheckedBidFirms(projectId, phaseId, formData, () => {
            this.fetchFirms();
        });

    };

    renderCheckboxTable = () => {
        let {projectDetails} = this.props;
        let {dataSource} = this.state;
        if (!projectDetails.editRights) {
            return <h5> You can not edit this document!</h5>
        }

        if (!dataSource || !Object.keys(dataSource).length) return null;

        return (
            <div>
                <h3>Bid Firms</h3>
                <CheckboxTable editRights={projectDetails.editRights} columns={this.columns}
                               dataSource={dataSource}
                               onSubmit={(data) => this.handleSubmit(data)}/>
            </div>
        )
    };


    handleDownloadPreview = (isPreview = true) => {

        console.log("Preview Download - Readonly View");

        let {form, projectDetails} = this.props;

        console.log("Preview Download - Documents");

        let dataProps = {
            projectId: projectDetails.projectId,
            phaseId: projectDetails.phaseId,
            data: form,
            isPreview: isPreview,
            title: form.title
        };

        previewDownloadFile(dataProps);

    };

    getSelectedRowKeys = (selectedFirms, dataSource) => {
        let rowKeys = [];

        dataSource.forEach((data, index) => {

            let record = selectedFirms && selectedFirms.filter(firm => firm.firm_id === data.firm_id);
            if (record && record.length) {
                rowKeys.push(index);
            }

        });

        return rowKeys;
    };

    render() {

        if (this.props.processing) {
            return <NewLoader/>
        }
        let {data, projectDetails} = this.props;
        if (!this.state.dataSource) return null;
        if (!Object.keys(data).length || !data.selectedFirms) {
            return this.renderCheckboxTable();
        }

        let rowKeys = this.getSelectedRowKeys(data.selectedFirms, this.state.dataSource);

        return (
            <div>
                <CheckboxTable columns={this.columns}
                               editRights={projectDetails.editRights}
                               dataSource={this.state.dataSource}
                               rowKeys={rowKeys}
                               onSubmit={(data) => this.handleSubmit(data)}
                               successCB={() => this.setState({isDetails: false, view: false})}/>
            </div>

        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        submitCheckedBidFirms: (projectId, phaseId, data, cb) => dispatch(submitCheckedBidFirms(projectId, phaseId, data, cb))
    }
};

const mapStateToProps = (state) => {
    return {
        processing: state.project_reducer.processing
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(BidFirms);
export default connected;