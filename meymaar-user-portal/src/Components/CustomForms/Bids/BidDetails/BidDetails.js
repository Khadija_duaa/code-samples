import React from 'react';
import {fetchFirmDetails} from "../../../../utils/server-utils";
import {Button, Col, Row, Divider, Table} from 'antd';
import 'antd/dist/antd.css';

import BidBQDetails from "../BidsBQDetails/BidBQDetails";
import BackButton from "../../../Buttons/BackButton";

class BidDetails extends React.Component {

    state = {
        firmDetails: null,
        activeBQ: null,
        isDetails: false
    };

    componentDidMount() {
        let {firmId} = this.props;

        fetchFirmDetails(firmId, (details) => {
            this.setState({firmDetails: details});
        })
    }

    renderFirmView = () => {
        let {firmDetails} = this.state;
        if (!this.state.firmDetails) return null;

        return (
            <div style={{width: "100%", display: "inline-block"}}>
                <div style={{width: "50%", float: "left"}}>
                    <h3>Firm Name: {firmDetails.firm_name}</h3>
                </div>
                <div style={{width: "50%", float: "right", textAlign: "right"}}>
                    <BackButton returnCB={this.props.successCB}
                                style={{width: "100px", height: "40px"}}/>
                </div>
            </div>

        )
    };


    // Rendering BOQ's
    handleBQClick = (activeBQ) => {

        this.setState({processing: true}, () => {
            setTimeout(() => {
                this.setState({activeBQ, isDetails: true, processing: false});
            }, 300)
        })

    };

    renderBQView = () => {

        let {quotedBqs} = this.props;

        if (!quotedBqs.length) return null;

        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            marginTop: "20px",
            marginRight: "20px",
            marginBottom: "20px",
        };

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width:"100%"
        };


        let bqJSX = quotedBqs.map((bqFile, index) => {
            return (
                <Col key={index} span={5} style={{overflowWrap: "break-word",minWidth: "140px", marginTop: "20px", marginRight: "20px"}}>

                    <Col span={24} style={boxStyle}>
                        <div style={divStyle}>
                            <img alt={"BOQ"} src={`/fileIcons/BOQ.svg`} width={"70px"}/>
                            <p style={{
                                fontWeight: "normal",
                                fontSize: "18px",
                                marginTop: "5px",
                                width:"100%"
                            }}>{bqFile.name}</p>
                        </div>
                    </Col>


                    <div span={24} style={{textAlign: "center"}}>
                        <Button type={"primary"} shape={"circle"} icon={"eye"}
                                onClick={() => this.handleBQClick(bqFile)}/>
                    </div>
                </Col>
            )
        });


        return (
            <div style={{marginTop:"50px"}}>
                <h3>BQ's</h3>
                <Row>
                    <Col span={24}>
                        {bqJSX}
                    </Col>
                </Row>
            </div>
        );

    };

    renderScheduleA = () => {
        let {quotedScheduleA} = this.props;

        let columns = [
            {
                key: "serial",
                title: "Serial",
                dataIndex: "serial",
                editable: false,
            },
            {
                key: "description",
                title: "Description",
                dataIndex: "description",
                editable: false,
            },
            {
                key: "drgNo",
                title: "Drawing No.",
                dataIndex: "drgNo",
                editable: false

            },
            {
                key: "unit",
                title: "Unit",
                dataIndex: "unit",
                editable: false
            },
            {
                key: "rate",
                title: "Rate",
                dataIndex: "rate",
                editable: false
            },
            {
                key: "noOfUnits",
                title: "Number of Units",
                dataIndex: "noOfUnits",
                editable: false
            },
            {
                key: "totalAmount",
                title: "Total Amount",
                dataIndex: "totalAmount",
                editable: false
            },
            {
                key: "periodOfCompletion",
                title: "Period of Completion",
                dataIndex: "periodOfCompletion",
                editable: false
            },
        ];

        return (
            <div style={{marginTop:"40px"}}>
                <h3 style={{marginBottom:"30px"}}>Schedule A</h3>
                <Table dataSource={quotedScheduleA}
                       rowKey={(record, index) => index}
                       columns={columns}
                       bordered
                />
            </div>
        )
    };


    render() {

        if (this.state.isDetails) {
            return (
                <BidBQDetails activeBQ={this.state.activeBQ}
                              successCB={() => this.setState({isDetails: false})}/>
            )
        }

        return (
            <div>
                {this.renderFirmView()}
                <Divider style={{margin: "30px 0px"}}/>
                {this.renderBQView()}
                <Divider style={{margin: "30px 0px"}}/>
                {this.renderScheduleA()}

            </div>
        )
    }
}

export default BidDetails;

