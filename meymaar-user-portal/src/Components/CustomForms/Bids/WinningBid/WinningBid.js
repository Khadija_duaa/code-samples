import React from 'react';
import {Col, Row, Button} from 'antd';
import 'antd/dist/antd.css';

import {formatAmount} from "../../../../utils/common-utils";

class WinningBid extends React.Component {
    render() {
        let {winningBid, reasonWinningBid,currentOffice } = this.props;


        let boxStyle = {
            minHeight: "200px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            marginRight: "20px",
            marginBottom: "20px",
            minWidth: "200px",
        };

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width: "100%",
        };

        let textStyle = {
            fontSize: "18px",
            marginLeft: "5%",
            fontWeight: "normal",
            color: "#000",
        };

        let reasonStyle = {
            maxHeight: "100px",
            height: "100%",
            overflowY: "auto",
            fontWeight: "bold",
            wordBreak: "break-all"
        };
        return (
            <Row>
                <Col span={24}>
                    <Col span={5} style={{minWidth: "140px"}}>
                        <Col span={24} style={boxStyle}>
                            <div style={divStyle}>
                                <img alt={"BID"} src={`/fileIcons/BID.svg`} width={"70px"}/>
                                <p style={{
                                    fontWeight: "normal",
                                    fontSize: "18px",
                                    marginTop: "5px",
                                    width: "100%"
                                }}>{winningBid.firmName} <br/> Rs. {formatAmount(winningBid.quotedAmount)}</p>
                            </div>
                        </Col>
                        {this.props.editRights ? <Col span={24} style={{textAlign: "center"}}>
                            <Button type={"danger"} shape={"circle"} icon={"delete"}
                                    onClick={this.props.onDeleteClick}/>
                        </Col> : null}
                    </Col>

                    <Col span={19} style={{paddingTop: "50px"}}>
                        <Row style={textStyle}>

                            <Col span={6}>
                                <p style={{marginBottom: "unset", display: "inline"}}>Reason: </p>
                            </Col>

                            <Col span={18}>
                                <p style={reasonStyle}>{reasonWinningBid}</p>
                            </Col>
                        </Row>

                        <Row style={textStyle}>
                            <Col span={6}>
                                <p style={{marginBottom: "unset", display: "inline"}}>Controlling Officer: </p>
                            </Col>

                            <Col span={18}>
                                <strong style={{wordBreak: "break-all"}}>{currentOffice.name}</strong>
                            </Col>
                        </Row>


                    </Col>
                </Col>
            </Row>
        )
    }
}

export default WinningBid;