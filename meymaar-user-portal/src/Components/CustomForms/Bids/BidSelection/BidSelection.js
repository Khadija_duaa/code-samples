import React from 'react';
import {Form, Button, Select, Input} from 'antd';
import 'antd/dist/antd.css';
import {retenderBid, revisionAdminApprovalBid, submitWinningBid} from "../../../../store/project/project-actions";
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import MessageModal from '../../../ModalFactory/MessageModal/ConfirmationModal';
import {getContractingOffices} from "../../../../utils/server-utils";
import NewLoader from '../../../../Containers/Loader/NewLoader';
import {ModalBody, ModalHeader} from 'reactstrap';
import Modal from '../../../../Components/Modal/Modal';
import {BUTTON_COLOR, hasAuthority} from "../../../../utils/common-utils";

const Option = Select.Option;

class BidSelection extends React.Component {

    state = {
        showModal: false,
        userRoles: null,
        subOffices: [],
        contractingOffices: null,
        showPasswordModal: false
    };


    componentDidMount() {
        let {projectId} = this.props.bidProps.projectProps.projectDetails;
        getContractingOffices(projectId, (contractingOffices) => {
            this.setState({contractingOffices})
        })
    }

    /*********************** Modal *************************/

        // Toggle Modal
    toggleModal = () => {
        this.setState({showModal: !this.state.showModal})
    };

    // Modal Success Callback Confirmation Event
    handleConfirmClick = () => {
        let {projectDetails} = this.props.bidProps.projectProps;
        let {projectId, phaseId} = projectDetails;

        let {getFieldValue} = this.props.form;

        switch (getFieldValue('action')) {
            case 'Winning Bid':
                let data = {
                    firmId: getFieldValue('winningBid').toString(),
                    reasonWinningBid: getFieldValue('reasonWinningBid'),
                    caNumber: getFieldValue('caNumber'),
                    engIncharge: getFieldValue('engIncharge'),
                    engInchargeRole: this.state.engInchargeRole,
                    ctrlOfficer: getFieldValue('ctrlOfficer'),
                    ctrlOfficerRole : this.state.ctrlOfficerRole
                };

                this.props.submitWinningBid(projectId, phaseId, data,null,this.toggleModal);
                break;

            case 'Re-Tender Contract':
                this.props.retenderBid(projectId, phaseId, () => this.props.history.push('/dashboard/works/overview'),this.toggleModal);
                break;

            case 'Revise Admin Approval':
                this.props.revisionAdminApprovalBid(projectId, phaseId, this.toggleModal);
                break;

            default:
                break;
        }
    };

    // Message Confirmation Modals
    renderMessageModal = () => {
        let message = '';
        let action = this.props.form.getFieldValue('action');

        if (!action) return null;

        if (action === 'Winning Bid') {
            let {bidName} = this.state;
            message = `Are you sure, you want to choose bid by ${bidName} as winning bid?`;
        }
        else if (action === 'Re-Tender Contract') {
            message = "Are you sure, you want to Re Tender Contract?";
        }

        return (
            <MessageModal isOpen={this.state.showModal}
                          okCB={this.handleConfirmClick} cancelCB={this.toggleModal}
                          message={message}/>
        );
    };


    /****************************** Password Modal ************************/

        //Setting Form ErrorCB
    setFormErrorsCB = (errorMessage, password) => {
        this.props.form.setFields({
            password: {
                value: password,
                errors: [new Error(`${errorMessage}`)]
            }
        })
    };


    togglePasswordModal = () => {
        this.setState({showPasswordModal: !this.state.showPasswordModal})
    };

    handlePasswordSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {
            if (err) {
                return;
            }
            let {projectDetails} = this.props.bidProps.projectProps;
            let {projectId, phaseId} = projectDetails;

            let data = {
                password: fieldsValue.password
            };

            this.props.revisionAdminApprovalBid(projectId, phaseId, data,
                () => {
                    this.props.history.push('/dashboard/works/overview');
                }, (errorMessage) => {
                    this.setFormErrorsCB(errorMessage, fieldsValue.password)
                })
        })
    };

    renderPasswordModal = () => {
        let {getFieldDecorator} = this.props.form;
        if (!this.state.showPasswordModal) return null;

        let formItemLayout = {
            labelCol: {span: 8},
            wrapperCol: {span: 16},
        };

        let buttonItemLayout = {
            wrapperCol: {offset: 8},
        };

        return (
            <Modal style={{width: "600px"}} isOpen={this.state.showPasswordModal}>
                <ModalHeader style={{border: "unset"}}>
                    Revise Admin Approval
                </ModalHeader>

                <ModalBody>
                    <Form onSubmit={this.handlePasswordSubmit}>
                        <Form.Item label={"Password"} {...formItemLayout} >
                            {
                                getFieldDecorator('password', {rules: [{required: true, message: "Required!"}]})(
                                    <Input.Password style={{width: "100%"}} placeholder={"Enter your password"}/>)
                            }
                        </Form.Item>

                        <Form.Item {...buttonItemLayout}>

                            <Button htmlType={"submit"} style={{
                                height: "40px",
                                width: "40%",
                                backgroundColor: BUTTON_COLOR,
                                color: "#fff"
                            }}>Revise Approval</Button>

                            <Button style={{height: "40px", width: "40%", marginLeft: "5%", borderColor: BUTTON_COLOR}}
                                    type={"default"} onClick={this.togglePasswordModal}>Cancel</Button>
                        </Form.Item>
                    </Form>
                </ModalBody>
            </Modal>
        )
    };

    /*********************** End *************************/

        // Form Item Layout & Button Item Layout
    formItemLayout = {
        labelCol: {span: 7},
        wrapperCol: {span: 12},
    };
    buttonItemLayout = {
        wrapperCol: {offset: 7},
    };


    // Submitting Bid which opens Modal
    handleBidSubmit = (event) => {
        event.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {

            if (err) {
                return;
            }

            if (fieldsValue.action === "Revise Admin Approval") {
                console.log("Show Password");
                this.setState({showPasswordModal: true})
            } else {
                this.setState({showModal: true})
            }

        });
    };


    // Set Firm Name of winning bid
    handleWinningBidChange = (bid, event) => {
        this.setState({bidName: event.props.children})
    };


    // Winning Bids Actions
    renderBidActions = () => {
        let {getFieldDecorator} = this.props.form;
        let {phaseDetails} = this.props.bidProps.projectProps.projectDetails;

        const reasonOptions = [
            {
                name: "Choose Winning Bid",
                value: "Winning Bid"
            }
        ];

        if (hasAuthority('CAN_GIVE_SANCTION')) {
            reasonOptions.push({
                name: "Re-Tender Contract",
                value: "Re-Tender Contract"
            })
        }

        if (phaseDetails.canReviseProjectCost) {
            reasonOptions.push({
                name: "Revise Admin Approval",
                value: "Revise Admin Approval"
            })
        }

        return (
            <Form.Item label={"Action Type"} {...this.formItemLayout}>
                {
                    getFieldDecorator('action', {initialValue: reasonOptions[0].value})(<Select
                        disabled={!this.props.editRights}>
                        {reasonOptions.map((option, index) => {
                            return <Option key={index} value={option.value}>{option.name}</Option>
                        })}
                    </Select>)
                }
            </Form.Item>
        )
    };


    /*********************************** Rendering Winning Bids Field ********************************/

        // Rendering Other Bids Field if winning bid chosen.
    handleOfficerRoleChange = (value, event) => {
        this.setState({engInchargeRole: event.props.children});
    };


    // Rendering Office Fields
    renderSubOfficesFields = () => {

        let {contractingOffices} = this.state;

        let roleOptions = contractingOffices.engineersInCharge.map((office, index) => {
            return <Option key={index} value={office.uuid}>{office.name}</Option>
        });

        return (
            <Form.Item label={"Engineer Incharge"} {...this.formItemLayout}>
                {
                    this.props.form.getFieldDecorator('engIncharge', {rules: [{required: true, message: "Required!"}]})(
                        <Select placeholder={"Select Engineer Incharge"} disabled={!this.props.editRights}
                                onChange={this.handleOfficerRoleChange}>
                            {roleOptions}
                        </Select>)
                }
            </Form.Item>
        )
    };


    // Rendering Other Bids Field if winning bid chosen.
    handleControllingOfficerChange = (value, event) => {
        this.setState({ctrlOfficerRole: event.props.children});
    };


    //Rendering Controlling Officer Fields
    renderControllingOfficer = () => {
        let {contractingOffices} = this.state;

        let officeOptions = contractingOffices.controllingOfficers.map((office, index) => {
            return <Option key={index} value={office.uuid}>{office.name}</Option>
        });

        return (
            <Form.Item  label={"Controlling Officer"} {...this.formItemLayout}>
                {
                    this.props.form.getFieldDecorator('ctrlOfficer', {rules: [{required: true, message: "Required!"}]})(
                        <Select onChange={this.handleControllingOfficerChange} placeholder={"Select Controlling Office"} disabled={!this.props.editRights}>
                            {officeOptions}
                        </Select>)
                }
            </Form.Item>
        )
    };


    // Render CA Number Fields
    renderCANumber = () => {
        return (

            <Form.Item  label={"CA No."} {...this.formItemLayout}>
                {
                    this.props.form.getFieldDecorator('caNumber', {
                        rules: [{
                            required: true,
                            message: "Required!",
                            whitespace: true
                        }]
                    })(
                        <Input placeholder={"e.g: ENC-A-105/2019"} disabled={!this.props.editRights}/>)
                }
            </Form.Item>
        )
    };


    // Render Bidding Reason Field
    renderWinningBidReason = () => {
        return (
            <Form.Item label={"Reason"} {...this.formItemLayout}>
                {
                    this.props.form.getFieldDecorator('reasonWinningBid', {
                        rules: [{
                            required: true,
                            message: "Required!",
                            whitespace: true
                        }]
                    })(
                        <Input placeholder={"Winning bid reason"} disabled={!this.props.editRights}/>)
                }
            </Form.Item>
        )
    };


    // Rendering Bid Firms For Selection of Winning Bid
    renderBidFirms = () => {
        let {form, bidProps} = this.props;
        let {data} = bidProps.projectProps;

        let bidOptions = data.bids.map((bids, index) => {
            return <Option key={index} value={bids.firmId}>{bids.firmName}</Option>
        });

        return (
            <Form.Item label={"Select Winning Bid"} {...this.formItemLayout}>
                {
                    form.getFieldDecorator('winningBid', {rules: [{required: true, message: "Required!"}]})(
                        <Select placeholder={"Select Bid"} disabled={!this.props.editRights}
                                onChange={this.handleWinningBidChange}>
                            {bidOptions}
                        </Select>
                    )}
            </Form.Item>
        )
    };


    // Rendering Winning Bid Fields
    renderWinningBidFields = () => {

        let {form} = this.props;

        let {contractingOffices} = this.state;

        if (form.getFieldValue('action') !== 'Winning Bid' || !contractingOffices) return null;

        return (
            <>
                {this.renderBidFirms()}
                {this.renderWinningBidReason()}
                {this.renderCANumber()}
                {this.renderControllingOfficer()}
                {this.renderSubOfficesFields()}
            </>
        )
    };


    /*************************************************** END ***************************************************/

        // Rendering Form Submit Buttons
    renderSubmitButtons = () => {
        let {contractingOffices} = this.state;
        let notContractingOffices = !contractingOffices && this.props.form.getFieldValue('action') === 'Winning Bid';

        return (
            <Form.Item {...this.buttonItemLayout}>
                <Button type={"primary"} disabled={!this.props.editRights || notContractingOffices} htmlType={"submit"}>
                    Submit
                </Button>
            </Form.Item>
        )
    };


    render() {
        if (this.props.processing || this.props.serverProcessing) return <NewLoader/>;
        return (
            <div>
                {this.renderMessageModal()}
                {this.renderPasswordModal()}
                <Form layout={"vertical"} onSubmit={this.handleBidSubmit}>
                    {this.renderBidActions()}
                    {this.renderWinningBidFields()}
                    {this.renderSubmitButtons()}
                </Form>
            </div>
        )
    }
}

const
    mapDispatchToProps = (dispatch) => {
        return {
            submitWinningBid: (projectId, phaseId, data, cb,errorCB) => dispatch(submitWinningBid(projectId, phaseId, data, cb,errorCB)),
            retenderBid: (projectId, phaseId, cb,errorCB) => dispatch(retenderBid(projectId, phaseId, cb,errorCB)),
            revisionAdminApprovalBid: (projectId, phaseId, reviseData, cb, errorCB) => dispatch(revisionAdminApprovalBid(projectId, phaseId, reviseData, cb, errorCB))
        }
    };

const
    mapStateToProps = state => {
        return {
            processing: state.project_reducer.processing,
            serverProcessing : state.server_reducer.processing
        }
    };

const
    connected = connect(mapStateToProps, mapDispatchToProps)(BidSelection);

const
    createdForm = Form.create()(connected);

export default withRouter(createdForm);