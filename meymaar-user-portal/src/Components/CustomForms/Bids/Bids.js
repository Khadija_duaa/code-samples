import React from 'react';
import {Button, Form, Col, Row, Divider, Table, message} from 'antd';
import FormElements from "../../FormElements/FormElements";
import {BUTTON_COLOR, formatAmount, NOTIFICATION_TIME, sortByKeyAsc} from "../../../utils/common-utils";
import {connect} from 'react-redux';
import {deleteWinningBid, submitFieldManagementData} from "../../../store/project/project-actions";
import DeleteConfirmation from '../../ModalFactory/ConfirmationModal/DeleteConfirmation';
import BidSelection from './BidSelection/BidSelection';
import BidDetails from "./BidDetails/BidDetails";
import WinningBid from './WinningBid/WinningBid';
import InformationModal from "../../ModalFactory/MessageModal/InformationModal";
import NewLoader from "../../../Containers/Loader/NewLoader";

class Bids extends React.Component {

    state = {
        isDetails: false,
        showConfirmModal: false,
        firmName: null,
        showActions: true,
        showActionMessageModal: false
    };
    /************************* Rendering Upload JSON View & Form Elements *******************/

    formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };

    buttonItemLayout = {
        wrapperCol: {offset: 8},
    };

    uploadJSON = {
        element: 'file',
        name: 'upload',
        title: 'Select File',
        required: true,
        requiredMessage: "File is required!",
        elementProps: {
            acceptFiles: ".json",
            fileExt: ["json"]
        }
    };

    submitJSONFile = (event) => {
        event.preventDefault();

        this.props.form.validateFields((err, fieldValues) => {
            if (err) {
                return;
            }

            let {projectDetails, form} = this.props.projectProps;
            let {projectId, phaseId} = projectDetails;

            let reader = new FileReader();
            reader.onload = (event) => {
                try {
                    let data = {
                        bids: JSON.parse(event.target.result)
                    };

                    //this.props.submitFile(projectId, phaseId, name, formData,fileName, form.submitURL,);
                    this.props.submitFieldManagement(projectId, phaseId, data, form.submitURL, false)
                } catch (e) {
                    let data = "Invalid File Provided!";
                    message.error(data, NOTIFICATION_TIME);
                }
            };

            reader.readAsText(fieldValues[this.uploadJSON.name]);
        })
    };
    getUploadButton = () => {

        let formElementProps = {
            element: this.uploadJSON,
            editRights: this.props.projectProps.projectDetails.editRights,
            ...this.props
        };

        return <FormElements {...formElementProps}/>
    };
    renderUploadView = () => {

        let {editRights} = this.props.projectProps.projectDetails;

        if (!editRights) {
            return <h5> You can not edit this document!</h5>
        }


        return (
            <Form layout={"vertical"} onSubmit={this.submitJSONFile}>
                {this.getUploadButton()}

                <Form.Item>
                    <Button htmlType="submit" style={{
                        height: "50px",
                        width: "100px",
                        backgroundColor: BUTTON_COLOR,
                        color: "#fff"
                    }}>Upload</Button>
                </Form.Item>
            </Form>
        )
    };

    /************************* END *******************/


    /************************* Rendering Bids Json *******************/

    handleBidClick = (activeBid) => {
        this.setState({activeBid, isDetails: true});
    };

    renderBids = (data) => {

        let bidsArray = data.bids;


        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            marginTop: "20px",
            marginRight: "20px",
            marginBottom: "20px",
        };

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width: "100%",
        };

        let jsx = bidsArray.map((bid, index) => {
            return (
                <Col key={index} span={5}
                     style={{overflowWrap: "break-word", minWidth: "140px", marginTop: "20px", marginRight: "20px"}}>

                    <Col span={24} style={boxStyle}>
                        <div style={divStyle}>
                            <img alt={"BOQ"} src={`/fileIcons/BID.svg`} width={"70px"}/>
                            <p style={{
                                fontWeight: "normal",
                                fontSize: "18px",
                                marginTop: "5px",
                                width: "100%"
                            }}>{bid.firmName} <br/> Rs. {formatAmount(bid.quotedAmount)}</p>
                        </div>
                    </Col>


                    <div span={24} style={{textAlign: "center"}}>
                        <Button type={"primary"} shape={"circle"} icon={"eye"}
                                onClick={() => this.handleBidClick(bid)}/>
                    </div>
                </Col>
            )
        });


        return (
            <Row>
                <Col span={24}>
                    {jsx}
                </Col>
            </Row>
        );

    };

    /************************* END *******************/


    /************************* Bid Selection *******************/

    toggleConfirmationModal = () => {
        this.setState({showConfirmModal: !this.state.showConfirmModal});
    };


    handleDeleteBidClick = () => {
        let {projectDetails} = this.props.projectProps;
        this.props.deleteWinningBid(projectDetails.projectId, projectDetails.phaseId, () => {
            this.setState({showConfirmModal: false})
        })
    };

    renderConfirmationModal = () => {

        return (
            <DeleteConfirmation isOpen={this.state.showConfirmModal}
                                okCB={this.handleDeleteBidClick} cancelCB={this.toggleConfirmationModal}
                                fileTitle={this.state.firmName} otherText={"bid"}/>
        );
    };


    toggleActionModal = () => {
        this.setState({showActionMessageModal: !this.state.showActionMessageModal});
    };
    renderActionMessageModal = () => {

        let message = "Request completed, please mark to required authority for further action (if necessary) !";
        return (
            <InformationModal isOpen={this.state.showActionMessageModal}
                              okCB={this.toggleActionModal}
                              message={message}/>
        );
    };


    renderActionSection = (data) => {

        let {editRights, currentProjectOfficeData} = this.props.projectProps.projectDetails;


        if (data.winningBid) {
            let {winningBid} = data;

            return (
                <div>
                    <h3>Winning Bid</h3>,
                    <WinningBid {...data} currentOffice={currentProjectOfficeData} editRights={editRights}
                                onDeleteClick={() => this.setState({
                                    showConfirmModal: true,
                                    firmName: winningBid.firmName
                                })}/>
                </div>
            )
        }

        else if (!this.state.showActions) return null;

        return (
            <div>
                <h3 style={{marginBottom: "50px"}}>Actions</h3>,
                <BidSelection editRights={editRights}
                              hideActionCB={() => this.setState({showActionMessageModal: true, showActions: false})}
                              bidProps={this.props}/>
            </div>

        )
    };

    /************************* END *******************/

    comparativeStatement = (data) => {

        let {projectDetails} = this.props.projectProps;
        let tableData = [];

        data.bids.forEach((bid, index) => {
            tableData.push({});
            tableData[index].serial = index + 1;
            tableData[index].firmName = bid.firmName;
            tableData[index].bid = bid;

            let bqAmount = 0;

            bid.quotedBqs.forEach(bq => {
                bq.bqItems.forEach(bqItem => {
                    bqAmount = bqAmount + parseFloat(bqItem.amount);
                });
            });

            tableData[index].amount = bqAmount;
        });


        let columns = [
            {
                key: "serial",
                title: "Serial",
                dataIndex: "serial",
                editable: false,
            },
            {
                key: "firmName",
                title: "Firm Name",
                dataIndex: "firmName",
                editable: false,
            },
            {
                key: "amount",
                title: "Bid Amount",
                dataIndex: "amount",
                editable: false,
                sorter: (a, b) => a.amount - b.amount,
                render: (text, record) => {
                    return <p key={record.id}>{formatAmount(text)}</p>
                }
            },
            {
                key: "details",
                title: "View Detail",
                dataIndex: "detail",
                render: (text, record) => {
                    return <a href={"javascript:;"} key={record.id} onClick={() => this.handleBidClick(record.bid)}>View
                        Details</a>
                }
            }
        ];

        let comparativeData = sortByKeyAsc(tableData, 'amount');

        return (
            <>
                <Divider style={{margin: "50px 0px"}}/>
                <div>
                    <div style={{marginBottom: "50px"}}>
                        <h3 style={{marginBottom: "10px"}}>
                            Comparative Statement
                        </h3>
                        <h5>PE Estimated Amount: <span
                            style={{marginLeft: "10px"}}>{projectDetails.peProjectCostStr}</span></h5>
                    </div>
                    <Table dataSource={comparativeData}
                           rowKey={(record, index) => index}
                           columns={columns}
                           bordered
                    />

                </div>
            </>
        );

    };


    render() {

        let {data} = this.props.projectProps;

        if (this.props.processing) return <NewLoader/>;
        if (this.state.isDetails) {
            return (
                <BidDetails {...this.props} {...this.state.activeBid}
                            successCB={() => this.setState({isDetails: false})}/>
            )
        }

        if (!data.bids || !data.bids.length) {
            return this.renderUploadView()
        }


        return (
            <div>
                {this.renderConfirmationModal()}
                {this.renderActionMessageModal()}
                {this.renderBids(data)}

                {this.comparativeStatement(data)}

                <Divider style={{margin: "50px 0px"}}/>

                {this.renderActionSection(data)}
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        submitFieldManagement: (projectId, phaseId, data, submitURL, isDelete, cb) => dispatch(submitFieldManagementData(projectId, phaseId, data, submitURL, isDelete, cb)),
        deleteWinningBid: (projectId, phaseId, cb) => dispatch(deleteWinningBid(projectId, phaseId, cb))
    }
};

const mapStateToProps = state => {
    return {
        processing: state.project_reducer.processing
    }
};
const connected = connect(mapStateToProps, mapDispatchToProps)(Bids);

export default Form.create()(connected);


