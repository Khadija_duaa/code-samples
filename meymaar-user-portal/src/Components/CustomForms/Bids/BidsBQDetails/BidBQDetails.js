import React from 'react';
import {Table,Button} from 'antd';
import 'antd/dist/antd.css';


class BidBQDetails extends React.Component{
    render(){

        let {successCB} = this.props;
        let columns = [
            {
                key: "serial",
                title: "Serial",
                dataIndex: "serial",
                editable: false,
            },
            {
                key: "description",
                title: "Description",
                dataIndex: "description",
                editable: false,
            },
            {
                key: "qty",
                title: "Quantity",
                dataIndex: "qty",
                editable:false

            },
            {
                key: "unit",
                title: "Unit",
                dataIndex: "unit",
                editable: false
            },
            {
                key: "rate",
                title: "Rate",
                dataIndex: "rate",
                editable: false
            },
            {
                key: "amount",
                title: "Amount",
                dataIndex: "amount",
                editable: false
            }
        ];

        return(

            <div>
                <h3>BQ Details</h3>

                <Table dataSource={this.props.activeBQ.bqItems}
                       rowKey={(record,index) => index}
                       columns={columns}
                       bordered
                />

                <Button style={{margin: "10px 10px 10px 0px"}} type={"default"} icon={"rollback"}
                        onClick={successCB && successCB}>Back</Button>
            </div>
        )
    }
}

export default BidBQDetails
