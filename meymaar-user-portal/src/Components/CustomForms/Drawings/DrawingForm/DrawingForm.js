import React from 'react';
import {Form, Button,} from 'antd';
import {drawingFormJSON} from "../../../../utils/utilsJSON";
import {BUTTON_COLOR} from "../../../../utils/common-utils";
import 'antd/dist/antd.css';
import GridTable from "../../../GridTable/GridTable";
import {submitDrawingSheet, updateDrawingSheet} from "../../../../store/project/project-actions";
import {connect} from 'react-redux';
import FormElement from '../../../FormElements/FormElements';
import moment from "moment/moment";
import NewLoader from '../../../../Containers/Loader/NewLoader';


let col = null;
class DrawingForm extends React.Component {

    /***************************** Form Items ***************************/

        //Form Submit Event
    submitDrawingDetails = (event) => {
        event.preventDefault();

        this.props.form.validateFields((err, fieldValues) => {

            if (err) {
                return;
            }

            let {projectId, phaseId} = this.props.projectProps.projectDetails;

            let formData = new FormData();

            if (!this.props.isEditing || (this.props.isEditing && fieldValues.upload)) {
                console.log("fieldValues",fieldValues.upload);
                formData.append('upload', fieldValues.upload, fieldValues.upload.name);
            }

            if (this.props.isEditing) {
                fieldValues.sheets.forEach(sheet => {
                    delete sheet['id'];
                });
            }

            if (this.props.form.getFieldValue('dateOfRevision')) {

                let startDate = moment(fieldValues.dateOfDrawing);

                let endDate = moment(fieldValues.dateOfRevision);

                let days = startDate.diff(endDate);
                if (days > 0) {
                    this.props.form.setFields({
                        dateOfRevision: {
                            value: fieldValues.dateOfRevision,
                            errors: [new Error('Date of revision must be after or same to date of drawing')],
                        }
                    });
                } else {
                    if (this.props.isEditing && !fieldValues.upload) {
                        this.props.updateDrawingSheet(projectId, phaseId, this.props.activeDrawing.id, fieldValues, this.props.successCB);
                    } else {
                        this.props.submitDrawingSheet(projectId, phaseId, this.props.activeDrawing, fieldValues, formData, this.props.successCB);
                    }

                }
            } else {
                if (this.props.isEditing && !fieldValues.upload) {
                    this.props.updateDrawingSheet(projectId, phaseId, this.props.activeDrawing.id, fieldValues, this.props.successCB);
                } else {
                    this.props.submitDrawingSheet(projectId, phaseId, this.props.activeDrawing, fieldValues, formData, this.props.successCB);
                }

            }
        });
    };

    //Form Item Layout
    formItemLayout = {
        labelCol: {span: 5},
        wrapperCol: {span: 8},
    };

    //Custom JSON Form Items
    getDrawingForm = () => drawingFormJSON.map((item, index) => {
        const {getFieldDecorator} = this.props.form;

        let formElementProps = {
            formItemLayout: this.formItemLayout,
            element: item,
            editRights: this.props.editRights,
            ...this.props
        };

        let {isEditing, activeDrawing} = this.props;


        switch (item.element) {


            case 'heading':
                return <h5 key={index}>{item.title}</h5>;


            case 'input':
                formElementProps.initialValue = isEditing ? activeDrawing[item.name] : null;
                return <FormElement key={index} {...formElementProps}/>;

            case 'file':
                formElementProps.element.required = !isEditing;
                formElementProps.fileData = isEditing ? activeDrawing['file'] : null;
                return <FormElement key={index} {...formElementProps}/>;

            case 'datePicker':
                formElementProps.initialValue = isEditing && activeDrawing[item.name] ? moment(activeDrawing[item.name]) : null;
                return <FormElement key={index} {...formElementProps}/>;


            case 'table':
                let {name,columns} = item.elementProps;



                if (!col && columns){
                    col = columns;
                }

                delete item.elementProps['columns'];
                return (
                    <Form.Item key={index}>
                        {
                            getFieldDecorator(name, {initialValue: this.props.isEditing ? this.props.activeDrawing.sheets : null})(
                                <GridTable tableCol={col} disabled={!formElementProps.editRights}
                                           isMultiple={item.multiple}  {...item.elementProps}/>)
                        }
                    </Form.Item>
                );

            default:
                return null;
        }
    });


    // Form Buttons
    getFormButtons = () => {
        let {successCB, editRights, isEditing} = this.props;


        return (
            <div>
                {editRights ? <Button onClick={this.submitDrawingDetails} icon={"save"} style={{
                    height: "50px",
                    width: "90px",
                    backgroundColor: BUTTON_COLOR,
                    color: "#fff"
                }}>{isEditing ? "Update" : "Submit"}</Button> : null}


                <Button style={{height: "50px", width: "90px", marginLeft: "2%", borderColor: BUTTON_COLOR}}
                        type={"default"} icon={"rollback"} onClick={successCB && successCB}>Back</Button>

            </div>
        )
    };


    /***************************** End ***************************/
    render() {
        return (
            <div>
                <Form layout={"horizontal"} style={{paddingBottom:"20px"}}>
                    {this.getDrawingForm()}
                    {this.props.processing ? <NewLoader/> : null}
                    {this.getFormButtons()}
                </Form>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        submitDrawingSheet: (projectId, phaseId, activeDrawing, sheetData, formData, cb) => dispatch(submitDrawingSheet(projectId, phaseId, activeDrawing, sheetData, formData, cb)),
        updateDrawingSheet: (projectId, phaseId, drawingId, sheetData, cb) => dispatch(updateDrawingSheet(projectId, phaseId, drawingId, sheetData, cb))
    }
};

const mapStateToProps = state => {
    return {
        processing: state.project_reducer.processing
    }
};


const connected = connect(mapStateToProps, mapDispatchToProps)(DrawingForm);
export default Form.create()(connected);