import React from 'react';
import {Button, Row, Col} from 'antd';
import 'antd/dist/antd.css';
import DrawingForm from "./DrawingForm/DrawingForm";
import {connect} from 'react-redux';
import {deleteDrawingSheet, fetchAllDrawingSheets} from "../../../store/project/project-actions";
import DeleteConfirmation from '../../ModalFactory/ConfirmationModal/DeleteConfirmation';
import {previewDownloadFile} from "../../../utils/server-utils";
// import BackButton from "../../Buttons/BackButton";
import {hasAuthority} from "../../../utils/common-utils";

//import NewLoader from '../../../Containers/Loader/NewLoader';

class Drawings extends React.Component {

    state = {
        isAdd: false,
        isEditing: false,
        activeDrawing: null,
        showConfirmModal: false,
        canUploadRevit: hasAuthority('CAN_UPLOAD_REVIT_MODEL')
    };

    componentDidMount() {
        let {projectId} = this.props.projectProps.projectDetails;

        this.props.fetchAllDrawings(projectId);
    }

    /*************************** EVENTS *****************************/

    toggleConfirmationModal = () => {
        this.setState({showConfirmModal: !this.state.showConfirmModal});
    };
    renderConfirmationModal = () => {

        let {activeDrawing} = this.state;

        if (!activeDrawing) return null;

        return (
            <DeleteConfirmation isOpen={this.state.showConfirmModal}
                                okCB={this.handleDeleteDrawingClick} cancelCB={this.toggleConfirmationModal}
                                fileTitle={activeDrawing.drawingNumber}/>
        );

    };


    handleDeleteDrawingClick = () => {
        let {activeDrawing} = this.state;

        let {projectId, phaseId} = this.props.projectProps.projectDetails;

        this.props.deleteDrawingSheet(projectId, phaseId, activeDrawing.drawingNumber, this.toggleConfirmationModal);
    };


    handleDownloadPreviewClick = (isPreview = false, drawing) => {
        let {form, projectDetails} = this.props.projectProps;

        console.log("Preview Download - Documents");


        let data = {
            downloadURL: `/project-file-management/download/${projectDetails.projectId}/file/${drawing.file.fileId}`,
            formType: form.formType,
            submitType: form.submitType,
        };

        let dataProps = {
            projectId: projectDetails.projectId,
            phaseId: projectDetails.phaseId,
            data,
            isPreview: isPreview,
            title: drawing.drawingNumber
        };

        previewDownloadFile(dataProps);
    };

    /***************************  *****************************/

    getDrawingFiles = () => {

        if (!this.props.drawingDetails) return null;


        let {projectDetails, disable} = this.props.projectProps;

        let boxStyle = {
            minHeight: "240px",
            borderRadius: "3px",
            border: "1px solid #D4D5DD",
            marginTop: "20px",
            marginRight: "20px",
            marginBottom: "20px",
        };

        let divStyle = {
            position: "absolute",
            top: "50%",
            left: "50%",
            textAlign: "center",
            transform: "translate(-50%, -50%)",
            width: "100%"
        };

        let canEdit = projectDetails.editRights && this.state.canUploadRevit && !disable;

        let drawingJSX = this.props.drawingDetails.map((drawing, index) => {
            return (
                <Col key={index} span={5} style={{overflowWrap: "break-word", minWidth: "140px", marginRight: "20px"}}>

                    <Col span={24} style={boxStyle}>
                        <div style={divStyle}>

                            <img alt={"Drawing"} src={`/fileIcons/pdf.svg`} width={"70px"}/>
                            <p style={{fontWeight: "normal", fontSize: "18px", width: "100%", marginTop: "5px"}}>
                                {drawing.drawingNumber}
                            </p>

                        </div>
                    </Col>

                    <div span={24} style={{textAlign: "center"}}>

                        <Button type={"primary"} shape={"circle"} icon={canEdit ? "edit" : "eye"}
                                style={{marginRight: "10px"}}
                                onClick={() => this.setState({
                                    activeDrawing: drawing,
                                    isEditing: true
                                })}/>

                        <Button type={"primary"} shape={"circle"} icon={"file-pdf"}
                                onClick={() => this.handleDownloadPreviewClick(true, drawing)}/>

                        <Button type={"primary"} shape={"circle"} icon={"download"} style={{marginLeft: "10px"}}
                                onClick={() => this.handleDownloadPreviewClick(false, drawing)}/>

                        {
                            canEdit ?
                                <Button type={"danger"} shape={"circle"} icon={"delete"} style={{marginLeft: "10px"}}
                                        onClick={() => this.setState({
                                            activeDrawing: drawing,
                                            showConfirmModal: true
                                        })}/> : null
                        }

                    </div>
                </Col>
            )
        });


        return (
            <Row>
                <Col span={24}>
                    {drawingJSX}
                </Col>
            </Row>
        );
    };


    renderHeaderButtons = () => {
        let {projectDetails,  disable} = this.props.projectProps;
        // let canBack = this.props.drawingDetails && this.props.drawingDetails.length;
        return (
            <div style={{width: "100%", display: "inline-block", marginBottom: "30px"}}>
                {projectDetails.editRights && this.state.canUploadRevit && !disable ?
                    <div style={{width: "50%", float: "left"}}>
                        <Button disabled={!projectDetails.editRights} onClick={() => this.setState({isAdd: true})}
                                style={{color: "#fff"}} type={"primary"} size={"large"}>
                            Add Sheet
                        </Button>
                    </div> : null}
            </div>
        )
    };


    render() {

        let {drawingDetails} = this.props;
        let {isAdd, isEditing, activeDrawing} = this.state;

        if ((drawingDetails && !drawingDetails.length )&& !this.state.canUploadRevit) {
            return <h5>You do not have authority to upload drawing sheets</h5>
        }
        if (isAdd || isEditing) {
            let dataProps = {
                isAdd,
                isEditing,
                activeDrawing: isEditing ? activeDrawing : null
            };
            let {projectDetails, disable} = this.props.projectProps;
            let canEdit = projectDetails.editRights && this.state.canUploadRevit && !disable;

            return <DrawingForm editRights={canEdit} {...this.props} {...dataProps}
                                successCB={() => this.setState({isAdd: false, isEditing: false, activeDrawing: null})}/>
        }

        return (
            <div>

                {this.renderConfirmationModal()}

                {this.renderHeaderButtons()}
                {this.getDrawingFiles()}

            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        fetchAllDrawings: (projectId) => dispatch(fetchAllDrawingSheets(projectId)),
        deleteDrawingSheet: (projectId, phaseId, drawingNumber, cb) => dispatch(deleteDrawingSheet(projectId, phaseId, drawingNumber, cb))
    }
};

const mapStateToProps = state => {
    return {
        drawingDetails: state.project_reducer.drawingDetails
    }
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Drawings);

export default connected;