import React from 'react';

export const LBR = (props)=>{
    let jsx = [];
    let breakCounts = props.breakCounts || 1;
    for(let index = 0; index < breakCounts; index++){
        jsx.push(<br key={index}/>)
    }

    return jsx;
};