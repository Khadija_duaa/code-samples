import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import user_reducer from './store/user/user-reducer';
import sob_reducer from './store/sob/sob-reducer'
import server_reducer from './store/server/server-reducer';
import form_reducer from './store/form/form_reducer';
import project_reducer from './store/project/project-reducer';
import dashboard_reducer from './store/dashboard/dashboard_reducer';
import billing_reducer from './store/billing/billing_reducer';
import revit_setting from './store/revitManagement/revit_reducer';
import stats_reducer from './store/stats/stats-reducer';

import thunk from 'redux-thunk';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import {Provider} from 'react-redux';

import './index.css';
import 'typeface-montserrat';
import App from './App';



import connectSocket from './socket-io/socket-io';

// Setup socket
export const socket = connectSocket();

const rootReducers = combineReducers({
    user_reducer,
    server_reducer,
    form_reducer,
    project_reducer,

    dashboard_reducer,
    billing_reducer,
    sob_reducer,
    revit_setting,
    stats_reducer,
});


export const store = createStore(
    rootReducers,
    applyMiddleware(thunk)
);


ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
