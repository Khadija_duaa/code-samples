import io from 'socket.io-client';
import * as events from './socket-events';
import {store} from '../index';
import {NOTIFICATION_URL} from "../utils/config";
import {setSocketIO} from "../store/server/server-actions";
import {setNotification} from "../store/user/user-actions";

const setup = () => {
    const socket = io(NOTIFICATION_URL, {
        transports: ['websocket']
    });

    socket.on(events.NEW_CONNECTION, (data) => {
        //socket.join('room1');
        console.log('##Socket## Established with server');
        const {dispatch} = store;
        dispatch(setSocketIO(socket));
    });

    socket.on('NOTIFICATION', (data) => {
        const {dispatch} = store;
        dispatch(setNotification(data));
    });


    socket.on(events.SYNC_NOTIFICATION, (data) => {
        const {dispatch} = store;
        dispatch(setNotification(data));
    });


    socket.on(events.DISCONNECTION, () => {
        console.log('##Socket## disconnected from server');
    });


    return socket;
};

export default setup;