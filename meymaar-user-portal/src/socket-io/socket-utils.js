import * as events from './socket-events';
import {socket} from "../index";

export const subscribeUser = (user) => {
   socket.emit(events.SUBSCRIBE,`user/${user.principal_id}`);
    console.log('Joining room', `user/${user.principal_id}`);
};