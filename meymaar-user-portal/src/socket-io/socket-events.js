export const NEW_CONNECTION = 'connect';
export const SUBSCRIBE = 'SUBSCRIBE';

export const DISCONNECTION = 'disconnect';
export const SYNC_NOTIFICATION = 'item';
