import React, {Component} from 'react';
import MeymarLogin from "./Containers/MeymarLogin";
import {HashRouter, Route, Switch, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import Dashboard from "./Containers/Dashboard";
import {subscribeUser} from "./socket-io/socket-utils";
import DhtmlXGanttChart from "./Components/GanttChart/DhtmlX/DhtmlXGanttChart";
import {hasAuthority} from "./utils/common-utils";

class App extends Component {

    componentDidMount() {
        let {activeUser} = this.props;

        if (activeUser) {
            console.log("Token: ", activeUser.token);
            setTimeout(() => {
                let {socket} = this.props;
                if (socket) {
                    console.log("Connection with Socket");
                    subscribeUser(activeUser);
                }
            }, 4000)
        }

    }

    getAuthRedirect = () => {
        if (this.props.authenticated) {
            let {activeUser} = this.props;
            if (activeUser.principal.firstTime && !hasAuthority('SUPER_ADMIN')) {
                return <Redirect to={"/dashboard/profile"}/>
            } else {
                return <Redirect to={"/dashboard"}/>
            }
        } else {
            return <Redirect to={"/login"}/>
        }
    };

    getHome = () => {
        if (this.props.authenticated) {
            return (
                [
                    <Route key={0} path="/dashboard" name="Home" component={Dashboard}/>,
                ]
            )
        }
        return null;
    };


    render() {
        return (
            <div className="App">
                <HashRouter>
                    <Switch>
                        <Route exact path="/project/timeline/:token/:projectId" name="GanttChart"
                               component={DhtmlXGanttChart}/>
                        <Route exact path="/dashboard/project/timeline" name="GanttChart"
                               component={DhtmlXGanttChart}/>
                        {this.getHome()}
                        <Route path="/login" name="Login Page" component={MeymarLogin}/>
                        {this.getAuthRedirect()}
                    </Switch>
                </HashRouter>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        activeUser: state.user_reducer.activeUser,
        socket: state.server_reducer.socket,
        authenticated: Boolean(state.user_reducer.authenticated),
        error: state.user_reducer.error,
    }
};

export default connect(mapStateToProps)(App);
