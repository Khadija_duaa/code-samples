import axios from 'axios';
import {message} from 'antd';
import 'antd/dist/antd.css';
import {handleError} from "../store/store-utils";
import {NOTIFICATION_TIME} from "./common-utils";
import {parseActivityData} from "./gantt-common-utils";

const getAxiosOptions = (token) => {
    return {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    };

};

// Fetching Grouped Activities
export const fetchGroupedActivities = (serverURL, projectId, token, processingCB, successCB) => {


    processingCB && processingCB(true);
    axios.get(`${serverURL}/bq-management/activity/group/all/${projectId}`, getAxiosOptions(token))
        .then(response => {
            processingCB && processingCB(false);
            let {activitiesData} = response.data;
            let activities = null;
            if (activitiesData.activities.length) {
                // Get Parsed data according to gantt chart
                activities = parseActivityData(activitiesData.activities);
            }

            activitiesData.activities = activities;
            successCB && successCB(activitiesData);
        }).catch(err => {

        processingCB && processingCB(false);
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);

    })
};


// Updating Status of Gantt Chart
export const changeActivityStatus = (serverURL, projectId, token, status, processCB, successCB) => {

    processCB && processCB(true);
    axios.put(`${serverURL}/bq-management/activity/status/${projectId}`, {status}, getAxiosOptions(token))
        .then(() => {
            processCB && processCB(false);
            successCB && successCB();
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
        processCB && processCB(false);
    })
};