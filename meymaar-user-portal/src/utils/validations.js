import PhoneNumber from 'awesome-phonenumber';

export const isValidPhone = (phone)=>{
    let pn = new PhoneNumber(phone, 'PK');
    return pn.isValid();
};


export const isMobile = (phone)=>{
    let pn = new PhoneNumber(phone, 'PK');
    return pn.isMobile();
};
