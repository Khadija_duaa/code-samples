import React from 'react';

export const getURL = (notification)=>{

    if(notification.action === 'ACTION_MARKING'){
        return '/dashboard/planning-overview'
    }else{
        return '/dashboard/diary';
    }
};