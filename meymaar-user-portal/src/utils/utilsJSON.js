import { drawingInfoForm } from "./json/drawing";
import nav from "./json/nav";
import { presidentRankFields, userRankFields } from "./json/genericFormJson";
import { userBioForm } from "./json/user";

export const createUserJSON = userBioForm;
export const drawingFormJSON = drawingInfoForm;
export const memberRankJSON = userRankFields;
export const navJSONArray = nav;
export const presidentRankJSON = presidentRankFields;
