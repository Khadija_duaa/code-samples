import axios from './axios';
import {store} from "../index";
import {NOTIFICATION_TIME} from "./common-utils";
import {handleError} from "../store/store-utils";
import {message} from "antd/lib/index";
import {setProjectProcessing} from "../store/project/project-actions";

// Fetching RAR Requested Data
export const fetchRARData = (cb) => {
    let {dispatch, getState} = store;
    let {paidApproved} = getState().billing_reducer;

    let sessionState = JSON.parse(sessionStorage.getItem('overview-projectId'));
    let billingRequestId =  paidApproved.requestedBills[0].id;


    dispatch(setProjectProcessing(true));
    axios.get(`/billing-management/getRarData/${sessionState.projectId}/${billingRequestId}`)
        .then(res => {
            dispatch(setProjectProcessing(false));
            cb && cb(res.data.rar);
        })
        .catch(error => {
            dispatch(setProjectProcessing(false));
            let errorMessage = handleError(error);
            message.error(errorMessage, NOTIFICATION_TIME);
        });
};

//Creating RAR
export const createRAR = (formData, cb) => {
    let {dispatch, getState} = store;
    let sessionState = JSON.parse(sessionStorage.getItem('overview-projectId'));
    let billingRequestId = getState().billing_reducer.paidApproved.requestedBills[0].id;

    //console.log(formData);
    dispatch(setProjectProcessing(true));
    axios.post(`/billing-management/createRar/${sessionState.projectId}/${billingRequestId}`, formData)
        .then(() => {
            dispatch(setProjectProcessing(false));
            cb && cb();
        }).catch(err => {
        dispatch(setProjectProcessing(false));
        let errorMessage = handleError(err);
        message.error(errorMessage);
    })
};

// Fetching Requested,Rejected or Paid Rar Data
export const fetchRARDetails = (apiURL, key, cb) => {
    let {dispatch} = store;
    dispatch(setProjectProcessing(true));
    axios.get(apiURL)
        .then((response) => {
            dispatch(setProjectProcessing(false));

            let rarData = key === "1" ? response.data.rar : response.data.rars;

            cb && cb(rarData);
        }).catch(err => {
        dispatch(setProjectProcessing(false));
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};

// Approving Or Rejecting RAR
export const updateRARStatus = (data, projectId, rarId, status, approveCB, rejectCB) => {
    let {dispatch} = store;

    dispatch(setProjectProcessing(true));
    axios.put(`/billing-management/updateRarStatus/${projectId}/${rarId}`, data)
        .then((response) => {
            message.success(`RAR has ${status.toLowerCase()} successfully`, NOTIFICATION_TIME);
            dispatch(setProjectProcessing(false));

            if (status === "APPROVED") {
                approveCB && approveCB(response.data.rar);
            } else {
                rejectCB && rejectCB();
            }
        }).catch(err => {
        dispatch(setProjectProcessing(false));
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};

export const payRAR = (data, projectId, rarId, cb) => {
    let {dispatch} = store;
    dispatch(setProjectProcessing(true));
    axios.post(`/billing-management/rarPayment/${projectId}/${rarId}`, data)
        .then(() => {
            message.success(`RAR has paid successfully`, NOTIFICATION_TIME);
            dispatch(setProjectProcessing(false));
            cb && cb();
        }).catch(err => {
        let errorMessage = handleError(err);
        dispatch(setProjectProcessing(false));
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};