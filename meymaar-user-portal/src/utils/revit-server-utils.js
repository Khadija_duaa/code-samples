import axios from './axios';
import {message} from 'antd';
import 'antd/dist/antd.css';
import {handleError} from "../store/store-utils";
import {NOTIFICATION_TIME} from "./common-utils";

export const fetchRevitBQs = (projectId, cb) => {
    // dispatch(setProjectProcessing(true));
    axios.get(`/bq-management/revitBqs/${projectId}`)
        .then(response => {
            // dispatch(setProjectProcessing(false));
            console.log(response.data);
            let {revitBqs} = response.data;
            cb && cb(revitBqs);
        }).catch(err => {
        // dispatch(setProjectProcessing(false));
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};