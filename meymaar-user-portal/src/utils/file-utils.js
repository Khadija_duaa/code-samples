import axios from './axios';
import {message} from "antd";
import "antd/dist/antd.css";
import { NOTIFICATION_TIME } from './common-utils';

message.config({
    top: 70,
    duration: NOTIFICATION_TIME,
    maxCount: 3
});

export const uploadFiles = async (projectId, filesRequests) => {
    // {
    //     "concurrence" : FILE,
    //     "file" : FILE
    // };

    const files = [];
    for (let key in filesRequests) {
        if (filesRequests.hasOwnProperty(key)) {
            files.push(filesRequests[key]);
        }
    }

    const uploadUrl = `/project-file-management/office/upload/${projectId}`;

    const formDatas = files.map(file => {
        const formData = new FormData();
        formData.append("upload", file, file.name);
        return formData;
    });

    const promises = [];

    formDatas.forEach(formData => {
        promises.push(axios.post(uploadUrl, formData));
    });

    // wait for all to complete

    let fileResponses = await Promise.all(promises);

    let response = {};
    let index = -1;
    for (let key in filesRequests) {
        if (filesRequests.hasOwnProperty(key)) {
            response[key] = fileResponses[++index].data.file;
        }
    }


    return response;
};



export const deleteFiles = (projectId,filesRequests)=>{
    const files = [];
    for (let key in filesRequests) {
        if (filesRequests.hasOwnProperty(key)) {
            files.push(filesRequests[key]);
        }
    }

    const promises = [];

    files.forEach(file => {
        let deleteUrl = `/project-file-management/office/delete/${projectId}/${file.fileId}`;
        promises.push(axios.delete(deleteUrl));
    });

    // wait for all to complete

     return Promise.all(promises);

};