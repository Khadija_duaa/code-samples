import React from 'react';
import Loader from "react-loader-spinner";


const loader = (props) => {
  let _props = {
    type: "Triangle",
    color: "Black",
    height: "50px",
    width: "50px",
    ...props
  };



  return (
    <div>
      <Loader {..._props}/>
    </div>
  );
};

export default loader;
