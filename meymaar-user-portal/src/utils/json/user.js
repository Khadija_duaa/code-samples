export const userBioForm = [
  {
    type: "number",
    label: "Service #",
    value: "armyNumber",
    placeholder: "Service #",
    required: true,
    whitespace: true
  },
  {
    type: "text",
    label: "Rank",
    value: "rank",
    placeholder: "Rank",
    required: true,
    whitespace: true
  },
  {
    type: "text",
    label: "First Name",
    value: "firstName",
    placeholder: "First Name",
    required: true,
    whitespace: true
  },
  {
    type: "text",
    label: "Middle Name",
    value: "middleName",
    placeholder: "Middle Name",
    required: false,
    whitespace: true
  },
  {
    type: "text",
    label: "Last Name",
    value: "lastName",
    placeholder: "Last Name",
    required: true,
    whitespace: true
  },
  {
    type: "mask",
    mask: "99999-9999999-9",
    label: "CNIC",
    validationType: "cnic",
    value: "cnic",
    placeholder: "CNIC",
    required: true,
    whitespace: true
  },
  {
    type: "date",
    label: "DOB",
    value: "dob",
    placeholder: "DOB",
    required: true,
    whitespace: true
  },
  {
    type: "mask",
    mask: "9999-9999999",
    label: "Phone Number",
    validationType: "phone",
    value: "phoneNumber",
    placeholder: "Phone Number",
    required: true,
    whitespace: true
  },
  {
    type: "email",
    label: "Email",
    value: "email",
    placeholder: "Email",
    required: false,
    whitespace: false
  },
  {
    type: "text",
    label: "Username",
    value: "username",
    placeholder: "Username",
    validationType: "username",
    required: true,
    whitespace: true
  },
  {
    type: "password",
    label: "Password",
    value: "password",
    validationType: "password",
    placeholder: "Password",
    required: true,
    whitespace: true
  },
  {
    type: "confirm",
    label: "Confirm Password",
    value: "confirmPassword",
    validationType: "confirmPassword",
    placeholder: "Confirm Password",
    required: true,
    whitespace: true
  }
];
