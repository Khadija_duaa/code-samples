export const drawingInfoForm = [
  {
    element: "heading",
    title: "Drawing Number"
  },
  {
    element: "input",
    name: "drawingNumber",

    required: true,
    whitespace: true,
    requiredMessage: "Required!",
    elementProps: {
      placeholder: "Drawing Number"
    }
  },
  {
    element: "heading",
    title: "Date of Drawing"
  },
  {
    element: "datePicker",
    name: "dateOfDrawing",

    required: true,
    requiredMessage: "Required!",
    elementProps: {
      placeholder: "Date of Drawing",
      format: "DD-MM-YYYY"
    }
  },
  {
    element: "heading",
    title: "Date of Revision"
  },
  {
    element: "datePicker",
    name: "dateOfRevision",

    required: false,
    requiredMessage: "Required!",
    elementProps: {
      placeholder: "Date of Revision",
      format: "DD-MM-YYYY"
    }
  },
  {
    element: "heading",
    title: "Sheet Details"
  },
  {
    element: "table",
    name: "sheets",
    multiple: true,
    elementProps: {
      columns: [
        {
          key: "sheetNumber",
          name: "Sheet No.",
          editable: true,
          width: 500,
          resizable: true
        },
        {
          key: "description",
          name: "Description",
          editable: true,
          width: 500,
          resizable: true
        }
      ],
      sheets: [],
      name: "sheets"
    }
  },
  {
    element: "heading",
    title: "File Attachment"
  },
  {
    element: "file",
    name: "upload",
    title: "Select File",
    elementProps: {
      acceptFiles: ".doc,.pdf",
      fileExt: ["doc", "pdf"]
    },
    required: true,
    requiredMessage: "Required!"
  }
];
