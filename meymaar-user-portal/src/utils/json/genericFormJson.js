export const presidentRankFields = [
  {
    label: "Rank",
    name: "presidentRank",
    required: true,
    message: "Required!",
    placeholder: "Rank",
    type: "text",
    whitespace: true
  },
  {
    label: "Name",
    name: "presidentName",
    required: true,
    message: "Required!",
    placeholder: "Name",
    type: "text",
    whitespace: true
  },
  {
    label: "Service No",
    name: "presidentPakNo",
    required: true,
    message: "Required!",
    placeholder: "Service No",
    type: "number",
    whitespace: false
  },
  {
    label: "Branch",
    name: "presidentBranch",
    required: true,
    message: "Required!",
    placeholder: "Branch",
    type: "text",
    whitespace: true
  }
];

export const userRankFields = [
  {
    key: 0,
    data: [
      {
        label: "Rank",
        name: "Rank",
        required: true,
        message: "Required!",
        placeholder: "Rank",
        type: "text",
        whitespace: true
      },
      {
        label: "Name",
        name: "Name",
        required: true,
        message: "Required!",
        placeholder: "Name",
        type: "text",
        whitespace: true
      },
      {
        label: "Service No",
        name: "PakNo",
        required: true,
        message: "Required!",
        placeholder: "Service No",
        type: "number",
        whitespace: true
      },
      {
        label: "Branch",
        name: "Branch",
        required: true,
        message: "Required!",
        placeholder: "Branch",
        type: "text",
        whitespace: true
      }
    ]
  }
];
