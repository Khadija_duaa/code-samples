import {hasAuthority} from "../common-utils";

const nav = principal => {
    console.log("its principle", principal);
    let isSuperAdmin = hasAuthority("SUPER_ADMIN");
    let canManageWorks = hasAuthority("CAN_MANAGE_WORKS");

    return [
        {
            title: "Dashboard",
            to: "/dashboard",
            image: "/images/bars-vertical.svg",
            submenu: false,
            showMenu: !principal.firstTime || isSuperAdmin
        },

        {
            title: "Revit Management",
            image: "/images/revit.svg",
            to: "",
            submenu: true,
            showMenu:
                principal.firstTime && !isSuperAdmin
                    ? false
                    : hasAuthority("CAN_MANAGE_REVIT"),
            subOptions: [
                {
                    title: "Settings",
                    showMenu: hasAuthority("CAN_MANAGE_SETTINGS"),
                    to: {
                        pathname: "/dashboard/revit-management/settings",
                        state: {category: "Settings"}
                    }
                },
                {
                    title: "Revit Settings",
                    showMenu: hasAuthority("CAN_MANAGE_REVIT_SETTINGS"),
                    to: {
                        pathname: "/dashboard/revit-management/revit-settings",
                        state: {category: "Settings"}
                    }
                },
                {
                    title: "Field Parameters",
                    showMenu: true,
                    to: {
                        pathname: "/dashboard/revit-management/field-parameters",
                        state: {category: "Field Parameters"}
                    }
                },
                {
                    title: "BQ Categories",
                    showMenu: true,
                    to: {
                        pathname: "/dashboard/revit-management/bq-categories",
                        state: {category: "Field Parameters"}
                    }
                }
            ]
        },
        {
            title: "Initial Receipt",
            to: {
                pathname: "/dashboard/inbox-works",
                state: {task: "Inbox"}
            },
            image: "/images/inbox.svg",
            showMenu: principal.firstTime && !isSuperAdmin ? false : canManageWorks,
            submenu: false
        },
        {
            title: "Task Board",
            to: {
                pathname: "/dashboard/task-board",
                state: {task: "Taskboard"}
            },
            image: "/images/list.svg",
            showMenu: principal.firstTime && !isSuperAdmin ? false : canManageWorks,
            submenu: false
        },
        {
            title: "My Works",
            to: "",
            image: "/images/works.svg",
            submenu: true,
            showMenu: principal.firstTime && !isSuperAdmin ? false : canManageWorks,
            subOptions: [
                {
                    title: "Planning",
                    showMenu: canManageWorks,
                    to: {
                        pathname: "/dashboard/planning-works/myProjects",
                        state: {category: "Planing", type: "work"}
                    }
                },
                {
                    title: "Execution",
                    showMenu: canManageWorks,
                    to: {
                        pathname: "/dashboard/execution-works/myProjects",
                        state: {category: "Execution", type: "work"}
                    }
                }
            ]
        },
        {
            title: "Associated Works",
            to: "",
            image: "/images/associated.svg",
            submenu: true,
            showMenu: principal.firstTime && !isSuperAdmin ? false : canManageWorks,
            subOptions: [
                {
                    title: "Planning",
                    showMenu: canManageWorks,
                    to: {
                        pathname: "/dashboard/planning-associated/associated",
                        state: {category: "Planing", type: "associated"}
                    }
                },
                {
                    title: "Execution",
                    showMenu: canManageWorks,
                    to: {
                        pathname: "/dashboard/execution-associated/associated",
                        state: {category: "Execution", type: "associated"}
                    }
                }
            ]
        },
        {
            title: "Firms",
            to: "/dashboard/firms",
            image: "/images/firm.svg",
            submenu: false,
            showMenu:
                principal.firstTime && !isSuperAdmin
                    ? false
                    : hasAuthority("CAN_MANAGE_CONTRACTOR")
        },
        {
            title: "Devices",
            to: "/dashboard/devices",
            image: "/images/device.svg",
            submenu: false,
            showMenu:
                principal.firstTime && !isSuperAdmin
                    ? false
                    : hasAuthority("CAN_MANAGE_DEVICES")
        },
        {
            title: "Configurations",
            to: "/dashboard/configurations",
            image: "/images/department.svg",
            submenu: false,
            showMenu: hasAuthority("CAN_MANAGE_CONFIGURATION") || isSuperAdmin
        },
        {
            title: "Departments",
            to: "/dashboard/departments",
            image: "/images/department.svg",
            submenu: false,
            showMenu: hasAuthority("CAN_MANAGE_DEPARTMENT") || isSuperAdmin
        },
        {
            title: "Domains",
            to: "/dashboard/domains",
            image: "/images/domain.svg",
            submenu: false,
            showMenu: hasAuthority("CAN_MANAGE_DOMAIN") || isSuperAdmin
        },
        {
            title: "Powers",
            to: "/dashboard/contracting-powers",
            image: "/images/power.svg",
            submenu: false,
            showMenu: hasAuthority("CAN_MANAGE_CONTRACTUAL_POWERS") || isSuperAdmin
        },
        {
            title: "Authorities",
            to: "/dashboard/authorities",
            image: "/images/office_authority.svg",
            submenu: false,
            showMenu: hasAuthority("CAN_MANAGE_OFFICE_AUTHS") || isSuperAdmin
        },
        {
            title: "Stations",
            to: "/dashboard/stations",
            image: "/images/stations.svg",
            submenu: false,
            showMenu: hasAuthority("CAN_MANAGE_GEO_LOC") || isSuperAdmin
        },
        {
            title: "Offices",
            to: "/dashboard/offices",
            image: "/images/offices.svg",
            submenu: false,
            showMenu: hasAuthority("CAN_MANAGE_OFFICES") || isSuperAdmin
        },
        {
            title: "Users",
            to: "/dashboard/users",
            image: "/images/staff.svg",
            submenu: false,
            showMenu: hasAuthority("CAN_MANAGE_USERS") || isSuperAdmin
        }
    ];
};

export default nav;
