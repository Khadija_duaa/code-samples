import axios from './axios';
import {store} from "../index";
import {message} from 'antd';
import 'antd/dist/antd.css';
import {handleError} from "../store/store-utils";
import {NOTIFICATION_TIME} from "./common-utils";
import {
    setContractorsStats, setDevicesCount,
    setExecutionProgress,
    setFinancialStats,
    setProjectsStats
} from "../store/stats/stats-actions";


// Fetching Work Load Statistics.
export const fetchProjectsStats = (cb) => {
    let {dispatch} = store;
    axios.get(`/project-marking/project-marking/stats`)
        .then(response => {
            dispatch(setProjectsStats(response.data.stats));
            cb && cb();
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};


// Fetching Financial Layout Statistics.
export const fetchFinancialStats = () => {
    let {dispatch} = store;

    axios.get(`/billing-management/getProgressList`)
        .then(response => {
            dispatch(setFinancialStats(response.data.progressList));
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};


// Fetching Financial Layout Statistics.
export const fetchContractorStats = () => {
    let {dispatch} = store;

    axios.get(`/contractor-management/contractor/stats`)
        .then(response => {
            dispatch(setContractorsStats(response.data.stats));
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};


// Fethcing Execution Stats
export const fetchExecutionStats = () => {
    let {dispatch} = store;
    axios.get(`/bq-management/getProgressOfVettedWork`)
        .then(response => {
            dispatch(setExecutionProgress(response.data.progress));
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};

// Fetching Deveices Count Stats
export const fetchDevicesStats = () => {
    let {dispatch} = store;
    axios.get(`/device-management/get-devices-count`)
        .then(response => {
            let deviceData = {
                totalDevices : response.data.totalDevices,
                assignedDevices : response.data.assignedDevices
            };
            dispatch(setDevicesCount(deviceData));
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};