import React from "react";
import Moment from "react-moment";
import moment from "moment";
import PhoneNumber from "awesome-phonenumber";

import {store} from "../index";
import {deleteFiles, uploadFiles} from "./file-utils";

const _ = require("lodash");
export const BUTTON_COLOR = "#3d5470";
export const DELETE_BUTTON_COLOR = "#ff4d4f";
export const NOTIFICATION_TIME = 3;

// Formatting Date according to specific format provided
export const formatDate = (date, format) => {
    return moment(date).format(format);
};

// Sorting Array by a specific key in ascending order
export const sortByKeyAsc = (array, key) => {
    return array.sort((a, b) => {
        let x = a[key];
        let y = b[key];

        return x < y ? -1 : x > y ? 1 : 0;
    });
};

// Formatting Names
export const formatName = user => {
    let name = user.firstName + " " + user.lastName;

    return initialCapital(name);
};

// Getting object Value for searching
export const getObjectValue = (obj, path) => {
    let val = null;

    if (path.indexOf(".") > -1) {
        let paths = path.split(".");
        val = obj;
        paths.forEach(_path => {
            val = val[_path];
        });
    } else {
        val = obj[path];
    }

    return val;
};

// Formatting Amount
export const formatAmount = amount => {
    if (isNaN(amount)) {
        return amount;
    }

    return parseFloat(amount)
        .toFixed(2)
        .replace(/\d(?=(\d{3})+\.)/g, "$&,");
};

// Formatting Currency
export const formatCurrency = amount => {
    if (isNaN(amount)) {
        return amount;
    }

    const formatter = new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "PKR",
        minimumFractionDigits: 2
    });
    let resultAmount = "" + formatter.format(amount);
    return resultAmount;
};

// Validating whether user has some authority or not
export const hasAuthority = authority => {
    let {getState} = store;

    let isAllowed = false;

    let activeUser = getState().user_reducer.activeUser;

    if (activeUser.authorities.includes(authority)) isAllowed = true;

    return isAllowed;
};

// Formatting to capitalize
export const capitilaze = value => {
    return value.toUpperCase();
};

// Formatting Initial Capital
export const initialCapital = str => {
    if (str) {
        str = str.toLowerCase();
        let words = str.split(" ");

        const capitalizedWords = [];

        words.forEach(word => {
            capitalizedWords.push(
                `${word.charAt(0).toUpperCase()}${
                    word.length > 1 ? word.substr(1) : ""
                }`
            );
        });

        return capitalizedWords.join(" ");
    }

    return str;
};

// Validating phase is completed or not
export const isCompletedPhase = phaseId => {
    let {getState} = store;

    let projectDetails = getState().project_reducer.projectDetails;

    if (!projectDetails) return null;

    let completed = false;

    let {phases} = projectDetails.projectWorkflow;

    phases.forEach(item => {
        if (item.phase.uuid === phaseId) {
            completed = item.completed;
        }
    });

    return completed;
};

// Validating whether all documents in phase completed or not
export const isDocumentsComplete = phaseDetails => {
    let isCompleted = true;

    phaseDetails.forms.forEach(formData => {
        isCompleted =
            formData.isVisited &&
            ((isCompleted && formData.complete && formData.mandatory) ||
                (isCompleted && !formData.mandatory));
    });

    return isCompleted;
};

// Getting active user colour for boq details
export const activeUserColor = () => {
    let {getState} = store;

    let activeUser = getState().user_reducer.activeUser;
    let projectDetails = getState().project_reducer.projectDetails;

    if (!projectDetails) return null;

    let {currentProjectOfficeData} = projectDetails;
    let {roles} = activeUser;

    let activeColor = "";

    roles.forEach(role => {
        if (role.office.uuid === currentProjectOfficeData.uuid) {
            if (!currentProjectOfficeData.role) {
                activeColor = role.color;
            } else if (role.uuid === currentProjectOfficeData.role.uuid) {
                activeColor = currentProjectOfficeData.role.color;
            }
        }
    });

    return activeColor;
};

// Downloading JSON File
export const getPhaseStatus = id => {
    let {getState} = store;
    let {projectWorkflow} = getState().project_reducer.projectDetails;
    let status = false;

    for (let index = 0; index < projectWorkflow.phases.length; index++) {
        let currentPhase = projectWorkflow.phases[index];

        if (currentPhase.phase.uuid === id) {
            status = currentPhase.active;
            break;
        }
    }

    return status;
};

// Downloading JSON File
export const downloadJSONFile = (data, fileName) => {
    let filename = `${fileName}.json`;
    let contentType = "application/json;charset=utf-8;";
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        let blob = new Blob([decodeURIComponent(encodeURI(JSON.stringify(data)))], {
            type: contentType
        });
        navigator.msSaveOrOpenBlob(blob, filename);
    } else {
        let a = document.createElement("a");
        a.download = filename;
        a.href =
            "data:" + contentType + "," + encodeURIComponent(JSON.stringify(data));
        a.target = "_blank";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    console.log("Converting to JSON");
};

// Fetching Phase Names by phase id
export const getPhaseName = id => {
    if (!id) return null;

    let {getState} = store;
    let {phases} = getState().project_reducer;

    if (!phases) return null;

    let phaseName = "";

    for (let index = 0; index < phases.length; index++) {
        let phase = phases[index];
        let {phaseData} = phase;
        if (phase.uuid === id) {
            phaseName = phase.name;
            break;
        } else if (
            phaseData.preRequisitePhase &&
            phaseData.preRequisitePhase.uuid === id
        ) {
            phaseName = phase.phaseData.preRequisitePhase.name;
            break;
        }
    }

    return phaseName;
};

// Formatting phone number according to country region
export const formatPhoneNumber = (phone, countryRegion) => {
    let pn = new PhoneNumber(phone, countryRegion);
    return pn.getNumber("international");
};

// Compare ascending wise
export const compareAsc = key => {
    return (a, b) => {
        if (a[key] < b[key]) return -1;
        if (a[key] > b[key]) return 1;
        return 0;
    };
};

// Compare descending wise
export const compareDesc = key => {
    return (a, b) => {
        if (a[key] > b[key]) return -1;
        if (a[key] < b[key]) return 1;
        return 0;
    };
};

// Formatting Epoch time using Moment
export const formatEpochTime = epochTime => {
    return (
        <Moment unix format={"DD-MM-YYYY hh:mm A"}>
            {epochTime / 1000}
        </Moment>
    );
};

// Getting Project Details from reducer
export const getProjectData = key => {
    const {getState} = store;

    return getState().project_reducer.projectDetails[key];
};

// Validating Fraction upto 2 decimal
export const isFraction = value => {
    if (isNaN(value)) return false;
    let numeric = /^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/;

    return numeric.test(value);
};

// Validating value is number
export const isNumber = value => {
    return typeof value === "number";
};

// Fetching Object's specific value
export const getObjectSpecificValue = (obj, path) => {
    let val = null;

    if (path.indexOf(".") > -1) {
        let paths = path.split(".");
        val = obj;
        paths.forEach(_path => {
            if (_path === "__notExists") {
                val = _.isEmpty(val);
            } else {
                val = !_.isEmpty(val) && !_.isNil(val) ? val[_path] : undefined;
            }
        });
    } else {
        val = obj[path];
    }

    return val;
};

export const pad = d => {
    return d < 10 ? "0" + d.toString() : d.toString();
};


export const isValidFileObjects = (files, extensions) => {
    let isValidFormat = true;
    let isValidSize = true;

    for (let index = 0; index < files.length; index++) {
        let file = files[index];
        let ext = file.name.split('.');
        let fileSize = file.size / 1048576;

        isValidFormat = extensions.includes(ext[ext.length - 1]);
        isValidSize = fileSize <= 300;
        if (!isValidFormat || !isValidSize) {
            break
        }
    }

    return {
        isValidFormat,
        isValidSize
    };
};

