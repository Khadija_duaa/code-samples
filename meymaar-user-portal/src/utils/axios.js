import axios from 'axios';
import {store} from '../index';
import {SERVER_URL} from "../utils/config";

const getJwt = () => {
    const {activeUser} = store.getState().user_reducer;

    if (activeUser) {
        return activeUser.token;
    }

    return '';
};

const getOptions = (isFile) => {


    let options = {
        headers: {
            'Authorization': `Bearer ${getJwt()}`,
        }
    };

    if(isFile){
        options.responseType='blob';
    }

    return options;

};


const prepareUrl = (api) => `${SERVER_URL}${api}`;

const wrapper = {
    get: (api,isFile=false) => axios.get(prepareUrl(api), getOptions(isFile)),
    post: (api, formData = {},isFile=false) => axios.post(prepareUrl(api), formData, getOptions(isFile)),
    put: (api, formData = {}) => axios.put(prepareUrl(api), formData, getOptions()),
    delete: (api) => axios.delete(prepareUrl(api), getOptions()),
};

export default wrapper;