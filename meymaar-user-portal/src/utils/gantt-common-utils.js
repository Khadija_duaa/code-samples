import moment from "moment/moment";

const getData = (activity, itemActivities) => {


    let end_date = itemActivities ? itemActivities[itemActivities.length - 1].endDate : activity.endDate;
    return {
        id: itemActivities ? activity.bqCategory.id : activity.id,
        text: itemActivities ? activity.bqCategory.type : activity.name,
        start_date: moment(activity.startDate).format("YYYY-MM-DD"),
        end_date: moment(end_date).format("YYYY-MM-DD"),
        duration: activity.duration,
        progress: parseFloat(activity.progress) / 100,
        parent: activity.parent ? activity.parent : null,
    };
};

const getLinks = (from, to) => {
    return {
        id: from.id,
        source: from.id,
        target: to.id,
        type: '0'
    }
};

export const parseActivityData = (activities) => {
    let dataTasks = [];
    let dataLinks = [];

    activities.forEach((task) => {
        let activity = task;
        let keys = Object.keys(activity);
        let data = getData(activity[keys[0]][0], activity[keys[0]]);
        data.open = true;
        dataTasks.push(data);

        activity[keys[0]].forEach((itemActivity) => {
            itemActivity.parent = data.id;
            dataTasks.push(getData(itemActivity));

            if (itemActivity.preReqActivity) {
                dataLinks.push(getLinks(itemActivity.preReqActivity, itemActivity))
            }
        });
    });

    return {
        data: dataTasks,
        links: dataLinks
    };
};
