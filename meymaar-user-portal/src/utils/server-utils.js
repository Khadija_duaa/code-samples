import axios from './axios';
import {store} from "../index";
import {saveDomains, setProcessing, setUserNotifications} from "../store/server/server-actions";
import 'antd/dist/antd.css';
import {handleError} from "../store/store-utils";
import {NOTIFICATION_TIME} from "./common-utils";
import {message} from "antd/lib/index";
import {fetchProjectDetails, setProjectProcessing} from "../store/project/project-actions";


message.config({
    top: 70,
    duration: NOTIFICATION_TIME,
    maxCount: 3
});

const getActionData = (action, notificationData) => {
    let actionData = {};
    let data = notificationData.data;

    switch (action) {
        case 'ACTION_WORK_DIARY':
            actionData.path = '/dashboard/diary';
            actionData.projectId = data.project_id;
            break;

        case 'REQUEST_BILL':
            actionData.path = '/dashboard/diary/billingRequests';
            actionData.projectId = notificationData.bill.projectId;
            break;


        case 'ACTION_SOB':
            actionData.path = '/dashboard/diary/siteorderbook';
            actionData.projectId = data.projectId;
            break;

        default:
            break;
    }
    return actionData;
};


export const handleNotificationClick = (notification, cb) => {
    let {data, action} = notification;

    let path = '';

    if (!notification.read) {
        let body = {
            notifications: [notification._id]
        };
        markNotificationRead(body);
    }

    if (action === 'ACTION_MARKING') {
        let {role_id} = data.id;

        path = {
            pathname: '/dashboard/works/overview',
            state: {
                projectId: data.id.project_id,
                category: 'Planing',
                backTitle: `Office Planning Projects`,
                backCB: "/dashboard/planning-works/myProjects"
            }
        };

        if (!role_id) {
            path = {
                pathname: "/dashboard/inbox-works",
                state: {task: "Inbox"}
            };
        } else {
            const category_key = 'workType-category';
            let sessionState = {type: "work"};
            sessionStorage.setItem(category_key, JSON.stringify(sessionState));
        }

        cb && cb(path);
    } else {
        let projectFields = {
            fields: ["projectId", "name", "divArea",
                "station", "referenceNumber",
                "tbdDate", "fy", "fy_2",
                "office", "projectWorkflow",
                "markingHistory", "currentProjectOfficeData",
                "currentProjectOffice", "sanctions",
                "peProjectCostStr", "peProjectCost",
                "gbXml",
                "exec_office", "customWorkflow"]
        };
        let notificationData = getActionData(action, data);
        path = {
            pathname: notificationData.path,
            state: {
                projectId: notificationData.projectId,
                category: 'Execution',
            }
        };

        let returnCB = () => {
            let sessionState = {
                projectId: notificationData.projectId,
                category: 'Execution',
                backTitle: `Notifications`,
                backCB: "/dashboard/notifications"
            };

            sessionStorage.setItem('overview-projectId', JSON.stringify(sessionState));
            cb && cb(path);
        };

        const {dispatch} = store;
        dispatch(fetchProjectDetails(notificationData.projectId, projectFields, false, returnCB));
    }
};

export const markNotificationRead = (data, cb) => {
    axios.post('/notification-service/readNotification', data)
        .then(response => {
            const {getState} = store;
            let activeUser = getState().user_reducer.activeUser;
            fetchUserRecentNotifications(activeUser.principal_id);

            cb && cb();
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};

//Fetching User Recent Notifications
export const fetchUserRecentNotifications = (userId) => {
    axios.get(`/notification-service/getRecentNotification/user/${userId}`)
        .then(response => {
            const {dispatch} = store;
            dispatch(setUserNotifications(response.data.notifications));
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};


// Fetching All domains which are saved in project
export const fetchAllDomains = (cb) => {
    const {dispatch} = store;
    dispatch(setProcessing(true));

    axios.get('/office-management/domains')
        .then(response => {

            dispatch(saveDomains(response.data.domains));
            dispatch(setProcessing(false));
            cb && cb(response.data.domains);

        }).catch(err => {
        dispatch(setProcessing(false));
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);

    })
};


//Fetching All Phase Names
export const fetchAllPhases = (cb) => {
    axios.get('/phase-management/project-phases')
        .then(response => {


            //sortByKeyAsc(response.data.projectType.phases,'sequence');
            let sequencedPhases = response.data.projectPhases.sort((a, b) => parseFloat(a.sequence.toString()) - parseFloat(b.sequence.toString()));

            let phaseData = [];
            sequencedPhases.forEach((item, index) => {
                phaseData.push({});
                phaseData[index].uuid = item.phase.uuid;
                phaseData[index].name = item.phase.name;
                phaseData[index].phaseData = item;
            });

            cb && cb(phaseData);
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME)
    })
};


//Fetching All Department Options
export const getDepartmentOptions = (successCB) => {
    axios.get('/office-management/departments')
        .then(response => {
            successCB && successCB(response.data.departments);
        })
        .catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        });
};

//Fetching All Stations Options
export const getStationOptions = (successCB) => {
    axios.get('/office-management/geo-locations')
        .then(response => {
            successCB && successCB(response.data.geoLocations);
        })
        .catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        });
};


// Fetching All Offices: /office-management/offices/department/c53f86b5-8141-4dbc-b1c6-23a08980da9b
export const getOfficeOptions = (successCB) => {
    axios.get(`/office-management/offices`)
        .then(response => {
            successCB && successCB(response.data.offices);
        })
        .catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        });
};


export const getDepartmentOffices = (departmentId, successCB) => {
    axios.get(`/office-management/offices/works-in/${departmentId}`)
        .then(response => {
            successCB && successCB(response.data.offices);
        })
        .catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
        });
};

//Fetching All Roles of Perticular Office
export const getRolesByOfficeOptions = (officeId, successCB) => {

    axios.get(`/office-management/designations/office/${officeId}`)
        .then(response => {
            successCB && successCB(response.data.designations);
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};

export const getAllSubOfficesOptions = (officeId, successCB) => {
    axios.get(`/office-management/offices/${officeId}/sub-offices/`)
        .then(response => {
            successCB && successCB(response.data.subOffices);
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};


//Fetching All Contracting Offices
export const getContractingOffices = (projectId, successCB) => {

    axios.get(`/project-management/projects/${projectId}/execution-info`)
        .then(response => {
            successCB && successCB(response.data);
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};


//Fetching firm details
export const fetchFirmDetails = (firmId, cb) => {
    axios.get(`/contractor-management/contractor/firm/get/${firmId}`)
        .then(response => {
            cb && cb(response.data.firm);
        })
        .catch(error => {
            let errorMessage = handleError(error);
            message.error(errorMessage, NOTIFICATION_TIME);
        });
};

//Fetching All BQ Names of Project
export const fetchBQNames = (projectId, cb) => {
    let {dispatch} = store;
    dispatch(setProjectProcessing(true));
    axios.get(`/bq-management/bqNames/${projectId}`)
        .then(response => {
            dispatch(setProjectProcessing(false));
            cb && cb(response.data.bqNames)
        }).catch(err => {
        dispatch(setProjectProcessing(false));
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};

export const fetchBQCategories = (cb) => {
    let {dispatch} = store;
    dispatch(setProjectProcessing(true));
    axios.get(`/bq-management/getBqCategories`)
        .then(response => {
            dispatch(setProjectProcessing(false));
            cb && cb(response.data.categories)

        }).catch(err => {
        dispatch(setProjectProcessing(false));
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};
//Fetching Project BOQ
export const fetchProjectBOQ = (projectId, cb) => {
    axios.get(`/bq-management/bqs/${projectId}`)
        .then(response => {
            cb && cb(response.data.bqs);
        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    })
};

//Previewing & Downloading File
export const previewDownloadFile = (props) => {
    let {dispatch} = store;

    let jsonData = {...props.data};
    dispatch(setProcessing(true));
    axios.post(`/document-management/doc/create-pdf/${props.projectId}/${props.phaseId}`, jsonData, true)
        .then(response => {

            if (props.isPreview) {

                console.log("Preview", response.data);
                //Previewing Document
                let blob = new Blob([response.data], {type: 'application/pdf'});
                console.log("blob", blob);
                let pdf = URL.createObjectURL(blob);
                window.open(pdf, "_blank");

            } else {

                // Creating Link and click programmaticallly and download
                const url = window.URL.createObjectURL(response.data);
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', `${props.title}.pdf`);
                document.body.appendChild(link);
                link.click();
            }

            dispatch(setProcessing(false));
        })
        .catch(err => {
            let errorMessage = handleError(err);
            message.error(errorMessage, NOTIFICATION_TIME);
            dispatch(setProcessing(false));
        })

};
//Fetching All Firms Data
export const fetchAllFirms = (cb) => {
    let {dispatch} = store;

    dispatch(setProjectProcessing(true));
    axios.get('/contractor-management/contractor/firm/get')
        .then(res => {
            dispatch(setProjectProcessing(false));
            cb && cb(res.data.firms);
        })
        .catch(error => {
            dispatch(setProjectProcessing(false));
            let errorMessage = handleError(error);
            message.error(errorMessage, NOTIFICATION_TIME);
        });
};


// Validating Dashboard
export const isGEDashboard = () => {
    let {getState} = store;

    let {activeUser} = getState().user_reducer;

    let isGE = false;

    activeUser.roles && activeUser.roles.forEach(role => {
        let {officeAuthority} = role.office;
        isGE = isGE || (officeAuthority.name.includes('GE'));
    });


    return isGE;
};


// Fetching Geo Location By Departments
export const fetchGeoLocationsByDepartment = (deptId, cb) => {
    let {dispatch} = store;
    dispatch(setProcessing(true));
    axios.get(`/office-management/geo-locations/offices-at/${deptId}`)
        .then(response => {

            dispatch(setProcessing(false));
            let {geoLocations} = response.data;
            let geoLocationData = null;
            if (geoLocations.length) {
                geoLocationData = geoLocations;
            }
            cb && cb(geoLocationData);

        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
        dispatch(setProcessing(false));
    })
};

export const fetchAuthoritiesByDeptAndLocation = (deptId, locationId, cb) => {
    let {dispatch} = store;
    dispatch(setProcessing(true));
    axios.get(`/office-management/office-authorities/of/${deptId}/at/${locationId}`)
        .then(response => {

            dispatch(setProcessing(false));
            let {officeAuthorities} = response.data;
            let officeAuthorityData = null;
            if (officeAuthorities.length) {
                officeAuthorityData = officeAuthorities;
            }
            cb && cb(officeAuthorityData);

        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
        dispatch(setProcessing(false));
    })
};


export const fetchOfficesByAuthAndLocation = (locationId, authId, cb) => {
    let {dispatch} = store;
    dispatch(setProcessing(true));
    axios.get(`/office-management/offices/auth/${authId}/at/${locationId}`)
        .then(response => {

            dispatch(setProcessing(false));

            console.log("Auth Offices", response.data);
            let {offices} = response.data;
            let officesData = null;
            if (offices.length) {
                officesData = offices;
            }
            cb && cb(officesData);

        }).catch(err => {
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
        dispatch(setProcessing(false));
    })
};

/***************************** Firms ***********************/
export const fetchFirmsApiData = (cb) => {
    let {dispatch} = store;
    dispatch(setProjectProcessing(true));
    axios.get('/contractor-management/contractor/firm/get')
        .then(res => {
            dispatch(setProjectProcessing(false));
            cb && cb(res.data.firms);
        })
        .catch(error => {
            dispatch(setProjectProcessing(false));
            let errorMessage = handleError(error);
            message.error(errorMessage, NOTIFICATION_TIME);
        });
};
