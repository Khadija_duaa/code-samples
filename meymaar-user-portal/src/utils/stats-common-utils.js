import {store} from "../index";

export const getWorkLoadStats = () => {
    let {getState} = store;
    let {projectsStats} = getState().stats_reducer;
    let {phases} = getState().project_reducer;

    let data = [];
    let myOfficePlanningStats = projectsStats.phaseCount.myOfficePlaning;

    phases && phases.forEach((phase) => {
        if (phase.name.toLowerCase().includes('initiation')) return;

        let beforeLength = data.length;
        for (let i = 0; i < myOfficePlanningStats.length; i++) {
            let stats = myOfficePlanningStats[i];
            if (stats.phase_id && stats.phase_id === phase.uuid) {
                data.push({
                    name: phase.name,
                    value: stats.count,
                    phaseId: phase.uuid,
                });

                break;
            }
        }

        if (beforeLength === data.length) {
            data.push({
                name: phase.name,
                value: 0,
                phaseId: phase.uuid
            })
        }
    });

    return data;
};

export const getPlanningAndDesignStats = () => {
    let workLoadData = getWorkLoadStats();
    let {getState} = store;
    let {projectsStats} = getState().stats_reducer;

    let myOfficeAssociatedStats = projectsStats.phaseCount.myOfficeAssociatedPlaning;


    workLoadData.forEach(data => {
        for (let index = 0; index < myOfficeAssociatedStats.length; index++) {
            let associatedStat = myOfficeAssociatedStats[index];
            if (data.phaseId === associatedStat.phase_id) {
                data.Processed = associatedStat.count;
                break;
            } else {
                data.Processed = 0;
            }
        }

        data.Total = data.value + (myOfficeAssociatedStats.length ? data.Processed : 0);
        data.Received = data.value;
        data.Processed = !myOfficeAssociatedStats.length ? 0 : data.Processed;
        data.Balance = null;
        data.Forecast = null;
    });

    return workLoadData;
};