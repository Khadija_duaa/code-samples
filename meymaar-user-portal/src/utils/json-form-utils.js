import { deleteFiles, uploadFiles } from './file-utils';
import { handleError } from '../store/store-utils';
import { message } from 'antd';
import { NOTIFICATION_TIME } from './common-utils';
import { store } from '../index';
import { setProjectProcessing } from '../store/project/project-actions';
import { setProcessing } from '../store/server/server-actions';

/************************** FILES **********************************/

// Getting Additional Files
const getAdditionalFiles = (fileData) => {
    let additionalFiles = [];
    for (let key in fileData) {
        if (fileData.hasOwnProperty(key)) {
            let keyFiles = fileData[key].additionalFiles;
            if (keyFiles && keyFiles.length) {
                additionalFiles = [...additionalFiles, ...keyFiles];
            }
        }
    }
    return additionalFiles;
};

// Getting Removed Files
const getRemovedFiles = (fileData) => {
    let removedFiles = [];
    for (let key in fileData) {
        if (fileData.hasOwnProperty(key)) {
            let keyFiles = fileData[key].removedFiles;
            if (keyFiles && keyFiles.length) {
                removedFiles = [...removedFiles, ...keyFiles];
            }
        }
    }
    return removedFiles;
};

// Getting Existing Files
const getExistingFiles = (fileData) => {
    let existingFiles = {};
    for (let key in fileData) {
        if (fileData.hasOwnProperty(key)) {
            let keyFiles = fileData[key].existingFiles;
            if (keyFiles && keyFiles.length) {
                existingFiles[key] = [...keyFiles];
            }
        }
    }
    return existingFiles;
};

// Uploading New Additional Files
const uploadNewFiles = (projectId, fileData, keys) => {
    return Promise.all(keys.map((entity) => {
        let files = fileData[entity].additionalFiles;
        if (files && files.length) {
            return uploadFiles(projectId, files);
        }else {
            return Promise.resolve();
        }
    }));
};

// Uploading New Additional Files
const getFileArray = (key, files, existingFiles) => {
    let dataFiles = [...existingFiles];
    const filesResponse = { ...files };
    for (let key in files) {
        if (files.hasOwnProperty(key)) {
            dataFiles.push(filesResponse[key]);
        }
    }

    return {
        [key]: dataFiles,
    };
};


// Save Added Files and merge with existing files
const saveAddedFiles = (fileData,existingFiles,projectId,successCB)=>{
    let Keys = Object.keys(fileData);
    let {dispatch} = store;

    dispatch(setProjectProcessing(true));
    uploadNewFiles(projectId, fileData, Keys)
        .then(filesResponse => {
            let newFileJSON = {};
            dispatch(setProjectProcessing(false));
            filesResponse.forEach((newFiles, index) => {
                let key = Keys[index];

                let existingData = Object.keys(existingFiles).length && existingFiles[key] ? existingFiles[key] : [];
                let dataFiles = getFileArray(key, newFiles, existingData);

                newFileJSON = { ...newFileJSON, ...dataFiles };
            });
            successCB && successCB(newFileJSON);
        }).catch(err => {
        dispatch(setProjectProcessing(false));
        let errorMessage = handleError(err);
        message.error(errorMessage, NOTIFICATION_TIME);
    });
};

// FILE Processor to save , remove and return proper json
export const fileProcessor = (projectId, fileData, successCB) => {

    const removedFiles = getRemovedFiles(fileData);
    const additionalFiles = getAdditionalFiles(fileData);
    const existingFiles = getExistingFiles(fileData);

    if (removedFiles.length) {
        deleteFiles(projectId, removedFiles)
            .then(()=>{
                if(additionalFiles.length){
                    saveAddedFiles(fileData,existingFiles,projectId,successCB);
                }else{
                    successCB && successCB(existingFiles);
                }
            })
    }
    else if(additionalFiles.length){
        saveAddedFiles(fileData,existingFiles,projectId,successCB);
    }else {
        successCB && successCB();
    }

};

/*********************************** END *******************************************/

// Returning api url for dynamic selects used in json form
export const getDynamicSelectURL = (elementName) => {
    let apiURL = null;
    switch (elementName) {
        case 'station':
            apiURL = 'office-management/geo-locations';
            break;
        case 'initialPhaseId':
            apiURL = 'phase-management/project-phases/dolphin';
            break;

        default:
            break;
    }

    return apiURL;
};


// Check whether specified field exist in json Form
export const isFieldExist = (formFields, field) => {
    let hasField = false;
    for (let index = 0; index < formFields.length; index++) {
        let item = formFields[index];

        if (item.element === field) {
            hasField = true;
            break;
        }
    }

    return hasField;
};