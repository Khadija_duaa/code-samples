const fs = require('fs');
const path = require('path');

const prettierOptions = JSON.parse(
    fs.readFileSync(path.resolve(__dirname, '.prettierrc'), 'utf8'),
);
module.exports = {
    env: {
        browser: true,
        es6: true,
    },
    extends: [
        // 'plugin:react/recommended',
        // 'airbnb',
        // 'prettier'
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parser: "babel-eslint",
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: [
        // 'react',
        // 'prettier'
    ],
    rules: {
       // 'prettier/prettier': ['error', prettierOptions],
    },
};
