import { StyleSheet } from 'react-native';

export default function ({ Colors, Gutters, Layout }) {
  const shallow = {
    ...Layout.center,
    minHeight: 40,
  };

  const base = {
    ...shallow,
    backgroundColor: Colors.primary,
  };
  const rounded = {
    ...shallow,
    borderRadius: 50,
  };

  return StyleSheet.create({
    shallow,
    base,
    rounded,
    outline: {
      ...shallow,
      backgroundColor: Colors.transparent,
      borderWidth: 1,
      borderColor: Colors.white,
    },
    outlineRounded: {
      ...rounded,
      backgroundColor: Colors.transparent,
      borderWidth: 1,
      borderColor: Colors.white,
    },
  });
}
