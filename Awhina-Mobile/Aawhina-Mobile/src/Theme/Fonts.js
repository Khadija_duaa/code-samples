/**
 * This file contains all application's style relative to fonts
 */
import { StyleSheet } from 'react-native';

/**
 *
 * @param Theme can be spread like {Colors, NavigationColors, Gutters, Layout, Common, ...args}
 * @return {*}
 */
export default function ({ Font, FontSize, Colors }) {
  return StyleSheet.create({
    textSmallest: {
      fontFamily: Font.light,
      fontSize: FontSize.smallest,
      color: Colors.text,
    },
    textSmall: {
      fontFamily: Font.light,
      fontSize: FontSize.small,
      color: Colors.text,
    },
    textRegular: {
      fontFamily: Font.regular,
      fontSize: FontSize.regular,
      color: Colors.text,
    },
    textMedium: {
      fontFamily: Font.medium,
      fontSize: FontSize.medium,
      color: Colors.text,
    },
    textLarge: {
      fontFamily: Font.semibold,
      fontSize: FontSize.large,
      color: Colors.text,
    },
    titleSmallest: {
      fontFamily: Font.light,
      fontSize: FontSize.smallest * 2,
      color: Colors.text,
    },
    titleSmall: {
      fontFamily: Font.light,
      fontSize: FontSize.small * 2,
      color: Colors.text,
    },
    titleRegular: {
      fontFamily: Font.regular,
      fontSize: FontSize.regular * 2,
      color: Colors.text,
    },
    titleMedium: {
      fontFamily: Font.medium,
      fontSize: FontSize.medium * 2,
      color: Colors.text,
    },
    titleLarge: {
      fontFamily: Font.semibold,
      fontSize: FontSize.large * 2,
      color: Colors.text,
    },
    textCenter: {
      textAlign: 'center',
    },
    textJustify: {
      textAlign: 'justify',
    },
    textLeft: {
      textAlign: 'left',
    },
    textRight: {
      textAlign: 'right',
    },
    fontSemiBold: {
      fontWeight: '500',
    },
    fontBold: {
      fontWeight: 'bold',
    },
  });
}
