/**
 * This file defines the base application styles.
 *
 * Use it to define generic component styles (e.g. the default text styles, default button styles...).
 */
import { StyleSheet } from 'react-native';
import buttonStyles from './components/Buttons';
/**
 *
 * @param Theme can be spread like {Colors, NavigationColors, Gutters, Layout, Common, ...args}
 * @return {*}
 */
export default function ({ Colors, ...args }) {
  return {
    button: buttonStyles({ Colors, ...args }),
    ...StyleSheet.create({
      backgroundPrimary: {
        backgroundColor: Colors.primary,
      },
      backgroundReset: {
        backgroundColor: Colors.transparent,
      },
      textInput: {
        borderBottomWidth: 1,
        borderBottomColor: Colors.white,
        borderColor: Colors.text,
        backgroundColor: Colors.transparent,
        color: Colors.white,
        minHeight: 50,
        textAlign: 'center',
      },
      borderPx: {
        borderWidth: 1,
      },
      border2: {
        borderWidth: 2,
      },
    }),
    colors: { ...Colors },
  };
}
