const Colors = {
  primary: '#114B1E',
  text: 'white',
  inputBackground: '#114B1E',
};

const NavigationColors = {
  primary: Colors.primary,
};

export default {
  Colors,
  NavigationColors,
};
