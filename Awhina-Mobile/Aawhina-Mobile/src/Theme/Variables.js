import { widthToDp } from '../../Utils/Responsive'
/**
 * This file contains the application's variables.
 *
 * Define color, sizes, etc. here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

/**
 * Colors
 */
export const Colors = {
  // Example colors:
  transparent: 'rgba(0,0,0,0)',
  inputBackground: '#FFFFFF',
  white: '#ffffff',
  black: '#000000',
  text: '#ffffff',
  textInput: '#212529',
  primary: '#114B1E',
  success: '#114B1E',
  error: '#D00000',
  black: '#000000',
  chatBubble: '#99CCAA',
  chat: '#BBEECC',
  chatLight: '#f2fffa',
  tip: '#ebc634',
}

export const NavigationColors = {
  primary: Colors.primary,
}

/**
 * FontSize
 */
export const FontSize = {
  smallest: widthToDp(3),
  small: widthToDp(4),
  regular: widthToDp(5),
  medium: widthToDp(6),
  large: widthToDp(7),
}

export const Font = {
  light: 'Montserrat-Light',
  regular: 'Montserrat-Regular',
  medium: 'Montserrat-Medium',
  semibold: 'Montserrat-SemiBold',
}

/**
 * Metrics Sizes
 */
const tiny = widthToDp(1) // 1
const small = tiny * 2 // 2
const regular = tiny * 3 // 3
const medium = regular * 2 // 6
const large = regular * 3 // 9
const extraLarge = regular * 4 // 12

export const MetricsSizes = {
  tiny,
  small,
  regular,
  medium,
  large,
  extraLarge,
}

export default {
  Colors,
  NavigationColors,
  FontSize,
  MetricsSizes,
  Font,
}
