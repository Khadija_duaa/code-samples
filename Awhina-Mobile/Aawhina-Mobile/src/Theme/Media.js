/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Entypo from 'react-native-vector-icons/Entypo'
import Feather from 'react-native-vector-icons/Feather'
import EvilIcons from 'react-native-vector-icons/EvilIcons'

import Watermark_1 from '@/Assets/Svgs/Watermark-1.svg'
import Watermark_2 from '@/Assets/Svgs/Watermark-2.svg'
import Hotlink_1 from '@/Assets/Svgs/Hotlink-1.svg'
import Hotlink_2 from '@/Assets/Svgs/Hotlink-2.svg'

/**
 *
 * @param Theme can be spread like {Colors, NavigationColors, Gutters, Layout, Common, ...args}
 * @return {*}
 */
export default function () {
  return {
    splashLogo: require('@/Assets/Images/SplashLogo.png'),
    victimImage: require('@/Assets/Images/victim-default.png'),
    videos: [require('@/Assets/Videos/video-01.mp4')],
    svgs: {
      Watermark_1,
      Watermark_2,
      Hotlink_1,
      Hotlink_2,
    },
    feicon: (icon, size = 30, color = '#ffffff', style = {}) => (
      <Feather name={icon} size={size} color={color} style={[{ ...style }]} />
    ),
    evicon: (icon, size = 30, color = '#ffffff', style = {}) => (
      <EvilIcons name={icon} size={size} color={color} style={[{ ...style }]} />
    ),
    entypo: (icon, size = 30, color = '#ffffff', style = {}) => (
      <Entypo name={icon} size={size} color={color} style={[{ ...style }]} />
    ),
    ionicon: (icon, size = 30, color = '#ffffff', style = {}) => (
      <Ionicons name={icon} size={size} color={color} style={[{ ...style }]} />
    ),
    faicon: (icon, size = 30, color = '#ffffff', style = {}) => (
      <FontAwesome
        name={icon}
        size={size}
        color={color}
        style={[
          {
            borderWidth: 1,
            borderColor: color,
            borderRadius: size - 3,
            padding: 10,
            ...style,
          },
        ]}
      />
    ),
  }
}
