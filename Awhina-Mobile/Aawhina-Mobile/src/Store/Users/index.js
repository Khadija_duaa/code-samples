import { createSlice } from '@reduxjs/toolkit'

const usersSlice = createSlice({
  name: 'users',
  initialState: { list: [], notes: [] },
  reducers: {
    fetchAll: (state, { payload }) => {
      state.list = payload
    },
    fetchOne: (state, { payload }) => {
      state = state.list.map(list => ({ list: { ...list, chat: payload } }))
    },
    fetchNotes: (state, { payload }) => {
      state.notes = payload
    },
    logout: state => {
      state = { list: [], notes: [] }
    },
  },
})

export const { fetchAll, fetchOne, fetchNotes, logout } = usersSlice.actions

export default usersSlice.reducer
