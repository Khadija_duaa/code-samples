import { createSlice } from '@reduxjs/toolkit';

const organizationsSlice = createSlice({
  name: 'organizations',
  initialState: { list: [], staff: [] },
  reducers: {
    fetchAll: (state, { payload }) => {
      state.list = payload;
    },
    fetchStaff: (state, { payload }) => {
      state.staff = payload;
    },
    // logout: (state) => {
    //   state = { list: [] };
    // },
  },
});

export const { fetchAll, logout } = organizationsSlice.actions;

export default organizationsSlice.reducer;
