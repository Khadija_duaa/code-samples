import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  user: null,
  token: null,
}

const authSlice = createSlice({

  name: 'auth',
  initialState: { ...initialState },
  reducers: {
    login: (state, { payload }) => (state = payload),
    profile: (state, { payload }) =>
      (state = { ...state, user: { ...payload } }),
    logout: state => (state = { ...initialState }),
  },
})

export const { login, profile, logout } = authSlice.actions

export default authSlice.reducer
