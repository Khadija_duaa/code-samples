import { createSlice } from '@reduxjs/toolkit';

const slice = createSlice({
  name: 'theme',
  initialState: { theme: null, color: '#000000' },
  reducers: {
    changeTheme: (state, { payload: { theme, color } }) => {
      if (typeof theme !== 'undefined') {
        state.theme = theme;
      }
      if (typeof color !== 'undefined') {
        state.color = color;
      }
    },
    setDefaultTheme: (state, { payload: { theme, color } }) => {
      if (!state.theme) {
        state.theme = theme;
        state.color = color;
      }
    },
  },
});

export const { changeTheme, setDefaultTheme } = slice.actions;

export default slice.reducer;
