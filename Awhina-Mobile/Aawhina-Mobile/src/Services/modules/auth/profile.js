import {login, profile} from '@/Store/Auth'
import Toast from 'react-native-simple-toast'

export default builder =>
  builder.mutation({
    query: payload => ({
      url: `staff/${payload._id}`,
      method: 'PUT',
      body: payload,
    }),
    async onQueryStarted(
      arg,
      {
        dispatch,
        getState,
        extra,
        requestId,
        queryFulfilled,
        getCacheEntry,
        updateCachedData, // available for query endpoints only
      },
    ) {
      try {
        const response = await queryFulfilled

        dispatch(profile(response.data.staff))
      } catch ({ error }) {
          Toast.show(error.error || error.data.message)
        console.log('Err :::', error.error || error.data.message)
      }
    },
  })
