import { login } from '@/Store/Auth';

export default (builder, url) =>
  builder.mutation({
    query: (payload) => ({
      url,
      method: 'POST',
      body: payload,
    }),
    async onQueryStarted(
      arg,
      {
        dispatch,
        getState,
        extra,
        requestId,
        queryFulfilled,
        getCacheEntry,
        updateCachedData, // available for query endpoints only
      },
    ) {
      try {
        const response = await queryFulfilled;

        dispatch(login(response.data));
      } catch ({ error }) {
        console.log('Err ::: ', error.error || error.data.message);
      }
    },
  });
