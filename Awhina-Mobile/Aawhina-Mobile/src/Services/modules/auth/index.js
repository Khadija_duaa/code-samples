import { api } from '@/Services/api'
import login from './login'
import register from './register'
import profile from './profile'

export const authApi = api.injectEndpoints({
  endpoints: builder => ({
    victimLogin: login(builder, '/victim-login'),
    staffLogin: login(builder, '/login'),
    register: register(builder, '/victim'),
    profile: profile(builder),
  }),
  overrideExisting: true,
})

export const {
  useVictimLoginMutation,
  useStaffLoginMutation,
  useRegisterMutation,
  useProfileMutation,
} = authApi
