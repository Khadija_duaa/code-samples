import { api } from '@/Services/api';
import organizationList from './organizationList';
import staffList from './staffList';

export const organizationApi = api.injectEndpoints({
  endpoints: (builder) => ({
    organizationList: organizationList(builder),
    staffList: staffList(builder),
  }),
  overrideExisting: true,
});

export const { useLazyOrganizationListQuery, useLazyStaffListQuery } =
  organizationApi;
