import { fetchAll } from '@/Store/Organizations';

export default (builder) =>
  builder.query({
    query: () => ({
      url: `organization`,
      method: 'GET',
    }),
    async onQueryStarted(
      arg,
      {
        dispatch,
        getState,
        extra,
        requestId,
        queryFulfilled,
        getCacheEntry,
        updateCachedData, // available for query endpoints only
      },
    ) {
      try {
        const response = await queryFulfilled;

        dispatch(fetchAll(response.data.organization));
      } catch (err) {
        console.log('Err ::: ', err);
      }
    },
  });
