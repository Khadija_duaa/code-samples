import { fetchStaff } from '@/Store/Organizations';

export default (builder) =>
  builder.query({
    query: (id) => ({
      url: `staff?search=${id}`,
      method: 'GET',
    }),
    async onQueryStarted(
      arg,
      {
        dispatch,
        getState,
        extra,
        requestId,
        queryFulfilled,
        getCacheEntry,
        updateCachedData, // available for query endpoints only
      },
    ) {
      try {
        const response = await queryFulfilled;

        dispatch(fetchStaff(response.data.staff));
      } catch (err) {
        console.log('Err ::: ', err);
      }
    },
  });
