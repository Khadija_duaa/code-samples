import { fetchOne } from '@/Store/Users';

export default (build) =>
  build.query({
    query: (id) => ({
      url: `chat?search=${id}&order=desc&sort=created_at`,
      method: 'GET',
    }),
    async onQueryStarted(
      arg,
      {
        dispatch,
        getState,
        extra,
        requestId,
        queryFulfilled,
        getCacheEntry,
        updateCachedData, // available for query endpoints only
      },
    ) {
      try {
        const response = await queryFulfilled;

        dispatch(fetchOne(response.data.chat));
      } catch (err) {
        console.log('Err ::: ', err);
      }
    },
  });
