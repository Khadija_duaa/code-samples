import { Config } from '@/Config';

export default (build) =>
  build.mutation({
    query: ({requestId, status}) => ({
      url: `victim-request/${requestId}/status`,
      method: 'PUT',
      body: {status},
    }),
    async onQueryStarted(
      arg,
      {
        dispatch,
        getState,
        extra,
        requestId,
        queryFulfilled,
        getCacheEntry,
        updateCachedData, // available for query endpoints only
      },
    ) {
      try {
        const response = await queryFulfilled;

        // dispatch(fetchAll(response.data.chat));
      } catch (err) {
        console.log('Err ::: ', err);
      }
    },
  });
