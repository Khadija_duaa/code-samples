import { fetchAll } from '@/Store/Users';

export default (build) =>
  build.mutation({
    query: (payload) => ({
      url: `victim-request`,
      method: 'POST',
      body: payload,
    }),
    async onQueryStarted(
      arg,
      {
        dispatch,
        getState,
        extra,
        requestId,
        queryFulfilled,
        getCacheEntry,
        updateCachedData, // available for query endpoints only
      },
    ) {
      try {
        const response = await queryFulfilled;

        dispatch(fetchAll(response.data.chat));
      } catch (err) {
        console.log('Err ::: ', err);
      }
    },
  });
