import { fetchAll } from '@/Store/Users';

export default (build) =>
  build.query({
    query: ({ id, params }) => ({
      url: `victim-request?mode=mo&user_id=${id}&${params}`,
      method: 'GET',
    }),
    async onQueryStarted(
      arg,
      {
        dispatch,
        getState,
        extra,
        requestId,
        queryFulfilled,
        getCacheEntry,
        updateCachedData, // available for query endpoints only
      },
    ) {
      try {
        const response = await queryFulfilled;

        dispatch(fetchAll(response.data.victim_request));
      } catch (err) {
        console.log('Err ::: ', err);
      }
    },
  });
