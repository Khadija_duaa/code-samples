import { Config } from '@/Config';

export default (build) =>
  build.mutation({
    query: (payload) => ({
      url: `chat`,
      method: 'POST',
      body: payload,
    }),
    async onCacheEntryAdded(
      arg,
      { updateCachedData, cacheDataLoaded, cacheEntryRemoved },
    ) {
      // create a websocket connection when the cache subscription starts
      const ws = new WebSocket(Config.WS_URL);
      try {
        // wait for the initial query to resolve before proceeding
        await cacheDataLoaded;

        // when data is received from the socket connection to the server,
        // if it is a message and for the appropriate channel,
        // update our query result with the received message
        const listener = (event) => {
          const data = JSON.parse(event.data);
          if (data.channel !== arg) return;

          updateCachedData((draft) => {
            draft.push(data);
          });
        };

        ws.addEventListener('message', listener);
      } catch {
        // no-op in case `cacheEntryRemoved` resolves before `cacheDataLoaded`,
        // in which case `cacheDataLoaded` will throw
      }
      // cacheEntryRemoved will resolve when the cache subscription is no longer active
      await cacheEntryRemoved;
      // perform cleanup steps once the `cacheEntryRemoved` promise resolves
      ws.close();
    },
    async onQueryStarted(
      arg,
      {
        dispatch,
        getState,
        extra,
        requestId,
        queryFulfilled,
        getCacheEntry,
        updateCachedData, // available for query endpoints only
      },
    ) {
      try {
        const response = await queryFulfilled;

        // dispatch(fetchAll(response.data.chat));
      } catch (err) {
        console.log('Err ::: ', err);
      }
    },
  });
