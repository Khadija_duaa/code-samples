export default build =>
  build.mutation({
    query: id => ({
      url: `victim-request/${id}`,
      method: 'DELETE',
    }),
  })
