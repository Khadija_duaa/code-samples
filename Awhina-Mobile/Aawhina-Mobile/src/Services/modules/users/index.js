import { api } from '@/Services/api'
import requestList from './requestList'
import chatList from './chatList'
import userRequest from './userRequest'
import deleteRequest from './deleteRequest'
import updateStatus from './updateStatus'
import updateUrgency from './updateUrgency'
import archive from './archive'
import block from './block'
import chat from './chat'
import notesList from './notesList'
import notes from './notes'

export const usersApi = api.injectEndpoints({
  endpoints: builder => ({
    requestList: requestList(builder),
    userRequest: userRequest(builder),
    deleteRequest: deleteRequest(builder),
    chatList: chatList(builder),
    chat: chat(builder),
    notesList: notesList(builder),
    notes: notes(builder),
    updateStatus: updateStatus(builder),
    archive: archive(builder),
    block: block(builder),
    updateUrgency: updateUrgency(builder)
  }),
  overrideExisting: true,
})

export const {
  useLazyRequestListQuery,
  useUserRequestMutation,
  useDeleteRequestMutation,
  useLazyChatListQuery,
  useChatMutation,
  useLazyNotesListQuery,
  useNotesMutation,
  useUpdateStatusMutation,
  useUpdateUrgencyMutation,
  useArchiveMutation,
  useBlockMutation,
} = usersApi
