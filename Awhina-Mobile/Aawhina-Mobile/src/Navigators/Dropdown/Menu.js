import React from 'react'
import {
  Text,
  TouchableOpacity,
  View,
  TouchableWithoutFeedback,
} from 'react-native'
import { useSelector } from 'react-redux'

import { useTheme } from '@/Hooks'
import { CloseButton } from '@/Components'
import { navigate, navigateAndSimpleReset, push } from '../utils'
import { logout } from '@/Store/Auth'
import { useDispatch } from 'react-redux'

const Menu = ({
  menuItems,
  alignItems = { alignItems: 'stretch' },
  justifyContent = { justifyContent: 'center' },
  backgroundColor = '#ffffff',
  layout = { flex: 1 },
  dropdown = false,
  setShow,
  showClose = true,
}) => {
  const { Common, Fonts, Gutters, Layout, Media } = useTheme()
  const [hide, setHide] = React.useState(false)
  const { user } = useSelector(state => state.auth)
  const [mergedMenuItems, setMergedMenuItems] = React.useState([])
  const dispatch = useDispatch()
  let defaultMenuItems = []

  React.useEffect(() => {
    defaultMenuItems = menuItems
      ? menuItems
      : user.type === 'victim'
      ? [
          {
            title: 'Chat',
            onPress: () => {
              navigate('Messages', {
                label: 'Messages',
                type: null,
              })
            },
          },
          {
            title: 'Emergency Links',
            onPress: () => {
              navigate('HotlinksNew', {
                label: 'Emergency Links',
              })
            },
          },
          {
            title: 'Our Kaimahi',
            onPress: () => {
              navigate('OurKaimahi', {
                label: 'Our Kaimahi',
              })
            },
          },
        ]
      : [
          {
            title: 'Chat',
            onPress: () => {
              navigate('Messages', {
                label: 'Messages',
                filters: {},
              })
            },
          },
          {
            title: 'Archived Messages',
            onPress: () => {
              navigate('Messages', {
                label: 'Archived Messages',
                filters: {
                  status: 'archived',
                },
              })
            },
          },
          {
            title: 'My Profile',
            onPress: () => {
              navigate('Profile')
            },
          },
        ]

    user &&
      setMergedMenuItems(
        showClose
          ? [
              ...defaultMenuItems,
              {
                title: 'Logout',
                onPress: () => {
                  dispatch(logout())
                },
              },
            ]
          : menuItems,
      )
  }, [user])

  return (
    <View style={[Layout.absolute, Layout.inset, { backgroundColor }]}>
      {!dropdown && showClose && <CloseButton setShow={setShow} />}
      <View style={[Gutters.mediumTMargin, layout, alignItems, justifyContent]}>
        {dropdown && (
          <View style={[Layout.alignItemsCenter]}>
            <Text>{dropdown.title}</Text>
            <TouchableWithoutFeedback onPress={() => setHide(!hide)}>
              {!hide
                ? Media.ionicon('chevron-down', 20, Common.colors.primary)
                : Media.ionicon('chevron-up', 20, Common.colors.primary)}
            </TouchableWithoutFeedback>
          </View>
        )}
        {!hide &&
          mergedMenuItems.map((menuItem, index) => (
            <View
              key={`menu-item-${index}`}
              style={[
                {
                  borderBottomWidth: 1,
                  borderBottomColor: Common.colors.primary,
                  opacity: 0.75,
                },
              ]}
            >
              <View
                style={[
                  Layout.justifyContentBetween,
                  Layout.row,
                  Gutters.smallHPadding,
                  Gutters.mediumVPadding,
                ]}
              >
                <TouchableOpacity
                  onPress={() => {
                    setTimeout(() => {
                      setShow(false)
                      menuItem.onPress()
                    }, 500)
                  }}
                >
                  <Text
                    style={[
                      Fonts.textRegular,
                      { color: Common.colors.primary, fontWeight: '500' },
                    ]}
                  >
                    {menuItem.title}
                  </Text>
                </TouchableOpacity>

                {menuItem.component && menuItem.component}
              </View>
            </View>
          ))}
      </View>
    </View>
  )
}

export default Menu
