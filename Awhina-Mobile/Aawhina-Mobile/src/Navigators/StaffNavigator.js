import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import {
  StaffMessagesContainer,
  StaffChatContainer,
  NotesContainer,
  ProfileContainer,
} from '@/Containers'

const Stack = createNativeStackNavigator()

const VictimNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      {/* <Stack.Screen name="Welcome" component={WelcomeContainer} /> */}
      <Stack.Screen name="Messages" component={StaffMessagesContainer} />
      <Stack.Screen name="Chat" component={StaffChatContainer} />
      <Stack.Screen name="Notes" component={NotesContainer} />
      <Stack.Screen name="Profile" component={ProfileContainer} />
    </Stack.Navigator>
  )
}

export default VictimNavigator
