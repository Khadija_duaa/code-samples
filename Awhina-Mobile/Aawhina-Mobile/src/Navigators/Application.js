import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { useSelector } from 'react-redux'

import { GestureHandlerRootView } from 'react-native-gesture-handler'

import { useTheme } from '@/Hooks'
import {
  AuthNavigator,
  VictimNavigator,
  StaffNavigator,
  navigationRef,
} from '@/Navigators'
import { useLazyOrganizationListQuery } from '@/Services/modules/organizations'

// @refresh reset
const ApplicationNavigator = () => {
  const { Layout, Common, NavigationTheme } = useTheme()
  const [organizations] = useLazyOrganizationListQuery()
  const { user, token } = useSelector(state => state.auth)

  React.useEffect(() => {
    token && organizations()
  }, [token])

  return (
    <GestureHandlerRootView
      style={[Layout.fill, { backgroundColor: Common.colors.white }]}
    >
      <NavigationContainer theme={NavigationTheme} ref={navigationRef}>
        {user ? (
          user.type === 'staff' ? (
            <StaffNavigator />
          ) : (
            <VictimNavigator />
          )
        ) : (
          <AuthNavigator />
        )}
      </NavigationContainer>
    </GestureHandlerRootView>
  )
}

export default ApplicationNavigator
