import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {
  WelcomeContainer,
  HotlinksContainer,
  HotlinksNewContainer,
  OurKaimahiContainer,
  LocationContainer,
  KaimahiIntroContainer,
  VictimMessagesContainer,
  VictimChatContainer,
} from '@/Containers';

const Stack = createNativeStackNavigator();

const VictimNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Welcome" component={WelcomeContainer} />
      <Stack.Screen name="Hotlinks" component={HotlinksContainer} />
      <Stack.Screen name="HotlinksNew" component={HotlinksNewContainer} />
      <Stack.Screen name="OurKaimahi" component={OurKaimahiContainer} />
      <Stack.Screen name="Location" component={LocationContainer} />
      <Stack.Screen name="KaimahiIntro" component={KaimahiIntroContainer} />
      <Stack.Screen name="Messages" component={VictimMessagesContainer} />
      <Stack.Screen name="Chat" component={VictimChatContainer} />
    </Stack.Navigator>
  );
};

export default VictimNavigator;
