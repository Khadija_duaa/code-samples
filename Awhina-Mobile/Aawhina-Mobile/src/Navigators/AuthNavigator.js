import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { StartupContainer, LoginContainer, SignupContainer } from '@/Containers'

const Stack = createNativeStackNavigator()

const AuthNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Startup" component={StartupContainer} />
      <Stack.Screen name="Login" component={LoginContainer} />
      <Stack.Screen name="Signup" component={SignupContainer} />
    </Stack.Navigator>
  )
}

export default AuthNavigator
