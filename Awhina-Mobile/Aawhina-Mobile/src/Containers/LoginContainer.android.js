import React from 'react'
import {
    ActivityIndicator,
    SafeAreaView,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
    TouchableOpacity,
    ScrollView, StyleSheet
} from 'react-native'
import DeviceInfo from 'react-native-device-info'
import Toast from 'react-native-simple-toast'
import { useTheme } from '@/Hooks'

import {
    useVictimLoginMutation,
    useStaffLoginMutation,
} from '@/Services/modules/auth'

import { AnimatedInputField, Brand, HeaderBar } from '@/Components'

import { goBack } from '@/Navigators/utils'
import { Config } from '@/Config'
import KeyboardAvoidingView from 'react-native/Libraries/Components/Keyboard/KeyboardAvoidingView'
import {heightToDp} from "../../Utils/Responsive";

const LoginContainer = ({ route }) => {
    const { Colors, Fonts, Gutters, Media, Layout } = useTheme()
    const [user, setUser] = React.useState({
        device_id: DeviceInfo.getUniqueId(),
        email: '',
        password: '',
        pin: '',
        mode: 'fe',
    })

    const _styles = StyleSheet.create({
        viewContent : {
            borderBottomWidth: 1,
            borderBottomColor: Colors.white
        },
    })
    const [type, setType] = React.useState(null)
    const [victimLogin, { isLoading: isVictimLoading }] = useVictimLoginMutation()
    const [staffLogin, { isLoading: isStaffLoading }] = useStaffLoginMutation()

    const loginVictim = () => {
        const { password, pin } = user

        setUser({
            ...user,
            password: '',
            pin: '',
        })

        victimLogin({ ...user, password, pin })
            .then(response => {
                response.error &&
                Toast.show(
                    response.error.data
                        ? response.error.data.message
                        : response.error.error,
                )
            })
            .catch(error => {
                setUser({ ...user, pin: '', password: '' })

                Toast.show(error.data.message)
            })
    }

    const loginStaff = () => {
        const { password, pin } = user

        setUser({
            ...user,
            password: '',
            pin: '',
        })

        staffLogin({ ...user, password, pin })
            .then(response => {
                response.error &&
                Toast.show(
                    response.error.data
                        ? response.error.data.message
                        : response.error.error,
                )
            })
            .catch(error => {
                setUser({ ...user, pin: '', password: '' })

                Toast.show(error.data.message)
            })
    }

    const CharField = ({ value }) => {
        const minLength = ['', '', '', '']

        return (
            <View style={[Layout.row, Layout.justifyContentCenter]}>
                {minLength.map((key, index) => (
                    <TextInput
                        secureTextEntry={true}
                        editable={false}
                        selectTextOnFocus={false}
                        value={value[index] && value[index]}
                        key={`char-${index}`}
                        style={[
                            Gutters.regularVPadding,
                            Gutters.smallHMargin,
                            Fonts.textCenter,
                            Fonts.extraLargeText,
                            {
                                color: Colors.white,
                                width: Layout.fullWidth.width / 8,
                                borderBottomColor: Colors.white,
                                borderBottomWidth: 2,
                            },
                        ]}
                    />
                ))}
            </View>
        )
    }

    const VirtualKeyboard = ({ initialValue }) => {
        let input = initialValue
        const rows = [
            [
                { type: 'string', val: 1 },
                { type: 'string', val: 2 },
                { type: 'string', val: 3 },
            ],
            [
                { type: 'string', val: 4 },
                { type: 'string', val: 5 },
                { type: 'string', val: 6 },
            ],
            [
                { type: 'string', val: 7 },
                { type: 'string', val: 8 },
                { type: 'string', val: 9 },
            ],
            [
                { type: 'string', val: '' },
                { type: 'string', val: 0 },
                {
                    type: 'component',
                    val: 'del',
                    component: Media.ionicon('chevron-back'),
                },
            ],
        ]
        const btnSize = heightToDp(9.5)

        return rows.map(cols => (
            <View style={[[Layout.rowCenter, Layout.justifyContentBetween]]}>
                {cols.map((col, index) =>
                    col.val !== '' ? (
                        <TouchableOpacity
                            key={`key-${index}`}
                            style={[
                                col.type !== 'component' && Layout.border,
                                Layout.center,
                                Gutters.smallVMargin,
                                {
                                    width: btnSize,
                                    height: btnSize,
                                    borderRadius: col.type !== 'component' ?  btnSize / 2 : 0,
                                    borderColor: col.type !== 'component' && Colors.white,
                                },
                            ]}
                            onPress={() => {
                                input =
                                    col.val === 'del'
                                        ? input.substring(0, input.length - 1)
                                        : `${input}${col.val}`

                                setUser({
                                    ...user,
                                    pin: input,
                                    password: input,
                                })
                            }}
                        >
                            {col.type === 'string' ? (
                                <Text style={[Fonts.titleSmallest]}>{col.val}</Text>
                            ) : (
                                <View key={`component-${index}`}>{col.component}</View>
                            )}
                        </TouchableOpacity>
                    ) : (
                        <View
                            key={`nokey-${index}`}
                            style={[
                                Gutters.smallVMargin,
                                {
                                    width: btnSize,
                                    height: btnSize,
                                },
                            ]}
                        ></View>
                    ),
                )}
            </View>
        ))
    }

    React.useEffect(() => {
        const { type: userType } = route.params
        userType && setType(userType)
    }, [route.params])

    React.useEffect(() => {
        if (user.password.length >= 4) {
            type === 'victim' ? loginVictim() : type === 'staff' ? loginStaff() : null
        }
    }, [user.password])

    return (

        <ScrollView
            style={[[Layout.fill], { backgroundColor: Colors.primary }]}
            contentContainerStyle={[ {flexGrow: 1}]}
        >
            <HeaderBar
                LeftHeader={() => (
                    <TouchableWithoutFeedback onPress={goBack}>
                        {Media.entypo('chevron-thin-left', 25, Colors.white)}
                    </TouchableWithoutFeedback>
                )}
                showRightHeader={false}
                layout={'absolute'}
                color={Colors.white}
            />
                <View style={[[Gutters.largeHPadding],{flex: 10}]}>
                    <Brand
                        title={Config.APP_NAME}
                        subtitle={
                            'Lorem ipsum dolor sit amet, sectetur  adipiscing elit, leased do eius'
                        }
                    />
                </View>
                {type === 'victim' ? (
                    <View style={{flex: 1}}>
                        <CharField value={user.pin} />
                    </View>
                ) : (
                        <View style={[[Gutters.largeHMargin, Gutters.largeHPadding]]}>
                            <View style={_styles.viewContent}>
                                <AnimatedInputField
                                    placeholder="Email"
                                    valid={true}
                                    errorText="Please enter email"
                                    onChangeText={val => setUser({ ...user, email: val })}
                                    value={user.email}
                                />
                            </View>
                        </View>
                )}
                <ActivityIndicator animating={isStaffLoading || isVictimLoading} />
                <View
                    style={[[
                        Gutters.largeBMargin,
                        Gutters.largeHMargin,
                        Gutters.largeHPadding,
                    ], { flex: 7}]}
                >
                    <VirtualKeyboard initialValue={user.pin} />
                </View>
            </ScrollView>
    )
}

export default LoginContainer
