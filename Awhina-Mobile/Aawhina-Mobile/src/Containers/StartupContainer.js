import React from 'react'
import {
  View,
  // ActivityIndicator,
  Text,
  // TextInput,
  TouchableOpacity,
} from 'react-native'
import { StatusbarContainer } from '@/Containers'
import { BrandSplash } from '@/Components'
import { useTheme } from '@/Hooks'
import { navigate, navigateAndSimpleReset } from '@/Navigators/utils'

const StartupContainer = () => {
  const { Common, Fonts, Gutters, Layout } = useTheme()

  return (
    <StatusbarContainer backgroundColor={Common.colors.black}>
      <View
        style={[Layout.fill, Layout.colVCenter, Layout.justifyContentAround]}
      >
        <BrandSplash />
        {/* {(isLoading || isFetching) && <ActivityIndicator />} */}

        <View
          style={[Gutters.mediumHMargin, { width: Layout.fullWidth.width / 2 }]}
        >
          <TouchableOpacity
            style={[Common.button.outlineRounded, Gutters.regularBMargin]}
            onPress={() => {
              navigate('Login', { type: 'victim' })
            }}
          >
            <Text style={[Gutters.regularVPadding, Fonts.textSmall]}>
              Whaanau login
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[Common.button.outlineRounded, Gutters.regularBMargin]}
            onPress={() => {
              navigate('Login', { type: 'staff' })
            }}
          >
            <Text style={[Gutters.regularVPadding, Fonts.textSmall]}>
              Kaimahi login
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[Common.button.outlineRounded, Gutters.regularBMargin]}
            onPress={() => {
              navigate('Signup')
            }}
          >
            <Text style={[Gutters.regularVPadding, Fonts.textSmall]}>
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </StatusbarContainer>
  )
}

export default StartupContainer
