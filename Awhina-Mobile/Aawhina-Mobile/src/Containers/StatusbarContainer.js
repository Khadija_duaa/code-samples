import React from 'react';
import { SafeAreaView, View } from 'react-native';
import { useTheme } from '@/Hooks';
import { HeaderBar } from '@/Components';
import { Menu } from '@/Navigators';

const StatusbarContainer = React.memo(
  ({
    safe,
    children,
    backgroundColor,
    headerColor,
    margin,
    layout = 'fullWidth',
    header = false,
    color = '#ffffff',
    LeftHeader,
    showRightHeader = true,
    menuItems,
  }) => {
    const { Common, Gutters, Layout } = useTheme();
    const [show, setShow] = React.useState(false);

    const SafeOrView = ({ children }) =>
      safe ? (
        <SafeAreaView style={[Layout.fill, { backgroundColor }]}>
          {children}
        </SafeAreaView>
      ) : (
        <View style={[Layout.fill, { backgroundColor }]}>{children}</View>
      );

    return (
      <SafeOrView>
        {header ? (
          !show ? (
            <HeaderBar
              LeftHeader={LeftHeader}
              showRightHeader={showRightHeader}
              headerColor={headerColor ? headerColor : backgroundColor}
              setShow={setShow}
              layout={layout}
              color={color}
            />
          ) : (
            <View style={[Layout.absolute, Layout.inset, { backgroundColor }]}>
              <Menu
                setShow={setShow}
                menuItems={menuItems}
                backgroundColor={backgroundColor}
              />
            </View>
          )
        ) : null}

        <View
          style={[
            margin ? margin : Gutters.regularHMargin,
            Layout.fill,
            backgroundColor,
            { zIndex: -1 },
          ]}
        >
          {children}
        </View>
      </SafeOrView>
    );
  },
);

export default StatusbarContainer;
