import React from 'react'
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  ScrollView,
} from 'react-native'
import { useSelector } from 'react-redux'

import { HeaderBar } from '@/Components'
import { Menu } from '@/Navigators'
import { useTheme } from '@/Hooks'
import { goBack } from '@/Navigators/utils'
import { Config } from '@/Config'
import { useChatMutation, useLazyChatListQuery } from '@/Services/modules/users'
import { heightToDp } from '../../../Utils/Responsive'

const ChatContainer = ({ route }) => {
  const { Colors, Common, Layout, Gutters, Fonts, Media } = useTheme()
  const { user } = useSelector(state => state.auth)
  const [show, setShow] = React.useState(false)
  const [currentUser, setCurrentUser] = React.useState(null)
  const [chatMessage, setChatMessage] = React.useState({
    message: '',
  })
  const [messages, setMessages] = React.useState([])
  const [chat] = useChatMutation()
  const [chatList] = useLazyChatListQuery()
  const [victimRequestId, setVictimRequestId] = React.useState(null)

  let chatInterval = null

  React.useEffect(() => {
    setCurrentUser(route.params.payload.user)
    setVictimRequestId(route.params.payload.victim_request_id)
  }, [route.params])

  React.useEffect(() => {
    chatInterval = setInterval(() => {
      init()
    }, Config.REFRESH_INTERVAL)
  }, [victimRequestId])

  React.useEffect(() => () => clearInterval(chatInterval))

  const init = () =>
    victimRequestId &&
    chatList(victimRequestId).then(response => {
      setMessages(response.data.chat)
    })

  const handleMessage = () => {
    chat(chatMessage)
      .then(() => init())
      .then(() =>
        setChatMessage({
          victim_request_id: victimRequestId,
          message: '',
          file: null,
        }),
      )
  }

  const Message = ({ item }) => {
    const loggedIn = item.from_user_id._id === user._id
    const userImageUrl = `${Config.API_URL}staff/profile-image/${item.from_user_id.photo}`

    return (
      <View
        style={[
          Layout.fill,
          Gutters.smallMargin,
          loggedIn ? Layout.rowReverse : Layout.row,
        ]}
      >
        {loggedIn ? (
          <View
            style={[
              Layout.avatar,
              {
                width: 30,
                height: 30,
                marginHorizontal: 6,
                backgroundColor: user.color,
              },
            ]}
          />
        ) : (
          <Image
            source={{
              uri: userImageUrl,
            }}
            style={[
              Layout.avatar,
              {
                width: 30,
                height: 30,
                marginHorizontal: 6,
                backgroundColor: Colors.white,
              },
            ]}
          />
        )}
        <View
          style={[
            Gutters.smallPadding,
            {
              borderRadius: 8,
              backgroundColor: loggedIn ? Colors.chatBubble : Colors.white,
              flexShrink: 1,
            },
          ]}
        >
          <Text style={[]}>{item.message}</Text>
        </View>
      </View>
    )
  }

  return (
    <View style={[Layout.fill, { backgroundColor: Colors.chat }]}>
      {!show ? (
        <HeaderBar
          LeftHeader={() => (
            <TouchableWithoutFeedback onPress={goBack}>
              {Media.entypo('chevron-thin-left', 25, Colors.primary)}
            </TouchableWithoutFeedback>
          )}
          headerColor={Colors.chat}
          setShow={setShow}
          layout={'fullWidth'}
          color={Colors.primary}
        />
      ) : (
        <View
          style={[
            Layout.absolute,
            Layout.inset,
            { backgroundColor: Colors.chat, zIndex: 999 },
          ]}
        >
          <Menu setShow={setShow} backgroundColor={Colors.chat} />
        </View>
      )}
      {currentUser && (
        <ScrollView
          style={[Layout.fill, { backgroundColor: Colors.chat }]}
          contentContainerStyle={[{ flexGrow: 1 }]}
        >
          <View style={[Layout.center, { borderBottomWidth: 1 }]}>
            <Image
              source={{
                uri: `${Config.API_URL}${currentUser.type}/profile-image/${currentUser.photo}`,
              }}
              style={[Layout.avatar]}
            />
            <Text
              style={[
                Fonts.textRegular,
                Gutters.regularVPadding,
                ,
                {
                  color: Common.colors.black,
                },
              ]}
            >
              {currentUser.name}
            </Text>
          </View>

          <View style={[Layout.scrollSpaceBetween]}>
            <FlatList
              data={messages}
              keyExtractor={message => message._id}
              renderItem={Message}
              inverted
            />
          </View>

          <View
            style={[
              Layout.center,
              Layout.row,
              Gutters.regularTPadding,
              Gutters.mediumBMargin,
              {
                borderTopWidth: 1,
                justifyContent: 'flex-end',
                marginBottom: heightToDp(3),
              },
            ]}
          >
            <TouchableOpacity
              onPress={init}
              style={[Layout.fill, Layout.center]}
            >
              {Media.ionicon('refresh-outline', 30, Common.colors.black)}
            </TouchableOpacity>

            <TextInput
              value={chatMessage.message}
              placeholder={'type your message here ...'}
              style={[
                Gutters.regularPadding,
                // Layout.selfStretch,
                {
                  flex: 5,
                  backgroundColor: Colors.chatLight,
                  borderRadius: 30,
                },
              ]}
              onChangeText={str =>
                setChatMessage({
                  victim_request_id: victimRequestId,
                  message: str,
                })
              }
            />

            <TouchableOpacity
              onPress={handleMessage}
              style={[Layout.fill, Layout.center]}
            >
              {Media.ionicon(
                'chevron-forward-circle-outline',
                40,
                Common.colors.black,
              )}
            </TouchableOpacity>
          </View>
        </ScrollView>
      )}
    </View>
  )
}

export default ChatContainer
