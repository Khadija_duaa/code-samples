/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React from 'react'
import {
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
} from 'react-native'
import Video from 'react-native-video'
import { Menu } from '@/Navigators'

import { useTheme } from '@/Hooks'
import { goBack, push } from '@/Navigators/utils'
import { HeaderBar, HtmlViewer, Swiper } from '@/Components'
import { Config } from '@/Config'
import { useUserRequestMutation } from '@/Services/modules/users'

const KaimahiIntroContainer = ({ route }) => {
  const { Colors, Common, Layout, Gutters, Fonts, Media } = useTheme()
  const [show, setShow] = React.useState(false)
  const [paused, setPaused] = React.useState(true)
  const [videoHeight, setVideoHeight] = React.useState(0)
  const videoPlayer = React.useRef(null)
  const [userRequest] = useUserRequestMutation()

  const [staffList, setStaffList] = React.useState([])
  const [organization, setOrganization] = React.useState({})
  const [reason, setReason] = React.useState(null)
  const [currentStaff, setCurrentStaff] = React.useState({})
  // const { user } = useSelector(state => state.auth)

  React.useEffect(() => {
    setStaffList(route.params.organization.staff)
    setOrganization(route.params.organization)
  }, [route.params])

  React.useEffect(() => {
    staffList.length && handleIndexChanged(0)
  }, [staffList])

  const handleIndexChanged = index => {
    setCurrentStaff(staffList[index])
  }

  const onPressChat = () => {
    userRequest({
      staff_id: currentStaff._id,
      reason,
    }).then(response => {
      setReason(null)
      push('Chat', {
        payload: {
          user: currentStaff,
          victim_request_id: response.data.victim_request._id,
        },
      })
    })
  }

  return (
    <View style={[Layout.fill, { backgroundColor: Colors.chat }]}>
      {!show ? (
        <HeaderBar
          LeftHeader={() => (
            <TouchableWithoutFeedback onPress={goBack}>
              {Media.entypo('chevron-thin-left', 25, Colors.primary)}
            </TouchableWithoutFeedback>
          )}
          headerColor={Colors.transparent}
          setShow={setShow}
          layout={'absolute'}
          color={Colors.primary}
        />
      ) : (
        <View
          style={[
            Layout.absolute,
            Layout.inset,
            { backgroundColor: Colors.chat },
          ]}
        >
          <Menu setShow={setShow} backgroundColor={Colors.chat} />
        </View>
      )}
      <ScrollView
        style={[Layout.fill, { zIndex: -1 }]}
        contentContainerStyle={[Layout.scrollSpaceBetween]}
      >
        <View
          style={[
            Layout.fullWidth,
            { height: (videoHeight || Layout.fullHeight.height / 2.5) + 100 },
          ]}
        >
          {staffList.length > 0 && (
            <Swiper
              color={Colors.primary}
              top={300}
              onIndexChanged={handleIndexChanged}
            >
              {staffList.map(staff => {
                const staffImageUri =
                  staff.files.length > 0
                    ? `${Config.API_URL}staff/video/${staff.files[0].filename}`
                    : `${Config.API_URL}staff/profile-image/${staff.photo}`

                return (
                  <View style={[Layout.fill, Layout.relative]} key={staff._id}>
                    {staff.files.length > 0 ? (
                      <>
                        <View style={[Layout.absolute, { zIndex: -1 }]}>
                          <Video
                            style={[
                              Layout.fullWidth,
                              {
                                height: videoHeight,
                              },
                            ]}
                            source={{
                              uri: staffImageUri,
                            }}
                            paused={paused}
                            repeat={false}
                            controls={false}
                            ref={videoPlayer}
                            resizeMode={'cover'}
                            onEnd={() => {
                              videoPlayer.current.seek(0)
                              setPaused(true)
                            }}
                            onLoad={response => {
                              const { width, height } = response.naturalSize
                              const heightScaled =
                                height * (Layout.fullWidth.width / width)

                              setVideoHeight(heightScaled)
                            }}
                          />
                        </View>

                        <View
                          style={[
                            Layout.fullWidth,
                            Layout.center,
                            {
                              height: videoHeight,
                            },
                          ]}
                        >
                          {paused ? (
                            <TouchableOpacity
                              onPress={() => setPaused(!paused)}
                            >
                              {Media.ionicon(paused ? 'play' : 'pause')}
                            </TouchableOpacity>
                          ) : (
                            <TouchableWithoutFeedback
                              style={[
                                Layout.fill,
                                Layout.relative,
                                { zIndex: 9999999 },
                              ]}
                              onPress={() => setPaused(!paused)}
                            >
                              <View style={[Layout.absolute, Layout.inset]} />
                            </TouchableWithoutFeedback>
                          )}
                        </View>
                      </>
                    ) : (
                      <View
                        style={[
                          Layout.fill,
                          { height: Layout.fullHeight.height / 2.5 },
                        ]}
                      >
                        <Image
                          key={`staff-${staff._id}`}
                          source={{ uri: staffImageUri }}
                          style={[Layout.fill]}
                        />
                      </View>
                    )}

                    <View style={[Layout.center, Gutters.mediumTMargin]}>
                      <Text
                        style={[Fonts.textRegular, { color: Colors.primary }]}
                      >
                        {staff.name}
                      </Text>
                    </View>
                  </View>
                )
              })}
            </Swiper>
          )}
        </View>

        <View style={[Gutters.mediumHMargin]}>
          <View style={[Layout.center, Layout.row, Gutters.regularTPadding]}>
            <TextInput
              value={reason}
              placeholder={'type your reason here ...'}
              style={[
                Gutters.regularPadding,
                // Layout.selfStretch,
                {
                  flex: 5,
                  backgroundColor: Colors.chatLight,
                  borderRadius: 30,
                },
              ]}
              onChangeText={str => setReason(str)}
            />
          </View>

          {staffList.length > 0 && (
            <View style={[Layout.colCenter]}>
              <View
                style={[
                  Layout.rowCenter,
                  Layout.alignItemsEnd,
                  Gutters.mediumMargin,
                  { flex: 3 },
                ]}
              >
                <View
                  style={[
                    Gutters.mediumHMargin,
                    Layout.row,
                    Layout.justifyContentBetween,
                    Layout.alignItemsCenter,
                  ]}
                >
                  <TouchableOpacity
                    onPress={onPressChat}
                    disabled={!reason}
                    style={[
                      Common.button.outlineRounded,
                      { borderColor: '#000000' },
                    ]}
                  >
                    <View style={[Common.button.shallow]}>
                      <Text
                        style={[Gutters.largeHPadding, Common.colors.black]}
                      >
                        Chat
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}

          {organization.description && (
            <HtmlViewer source={organization.description} />
          )}

          {staffList.length > 0 && (
            <View style={[Layout.colCenter]}>
              <View
                style={[
                  Layout.rowCenter,
                  Layout.alignItemsEnd,
                  Gutters.mediumMargin,
                  { flex: 3 },
                ]}
              >
                <View
                  style={[
                    Gutters.mediumHMargin,
                    Layout.row,
                    Layout.justifyContentBetween,
                    Layout.alignItemsCenter,
                  ]}
                >
                  <TouchableOpacity
                    onPress={onPressChat}
                    disabled={!reason}
                    style={[
                      Common.button.outlineRounded,
                      { borderColor: '#000000' },
                    ]}
                  >
                    <View style={[Common.button.shallow]}>
                      <Text
                        style={[Gutters.largeHPadding, Common.colors.black]}
                      >
                        Chat
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
        </View>

        <View
          style={[
            Layout.fill,
            Layout.center,
            Gutters.largePadding,
            { backgroundColor: Common.colors.black },
          ]}
        >
          <Text style={[{ color: Common.colors.white }]}>
            {organization.name}
          </Text>
        </View>
      </ScrollView>
    </View>
  )
}

export default KaimahiIntroContainer
