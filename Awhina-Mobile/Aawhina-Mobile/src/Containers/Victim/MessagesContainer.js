import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
} from 'react-native'
import { useSelector } from 'react-redux'

import Swipeable from 'react-native-gesture-handler/Swipeable'
import moment from 'moment'

import { StatusbarContainer } from '@/Containers'
import { useTheme } from '@/Hooks'
import { push } from '@/Navigators/utils'
import {
  useDeleteRequestMutation,
  useLazyRequestListQuery,
} from '@/Services/modules/users'
import { Config } from '@/Config'

const MessagesContainer = () => {
  const { Colors, Layout, Gutters, Fonts, Media } = useTheme()
  const [requests] = useLazyRequestListQuery({
    refetchOnMountOrArgChange: true,
  })
  const [deleteRequest] = useDeleteRequestMutation()
  const [messages, setMessages] = React.useState([])
  const { user } = useSelector(state => state.auth)

  let prevOpenedRow
  const row = []

  React.useEffect(() => {
    requests({ id: user._id }).then(response => {
      const { victim_request } = response.data

      setMessages(victim_request)
    })
  }, [user])

  const deleteItem = id => {
    deleteRequest(id).then(() => {
      const filteredRequests = messages.filter(message => message._id !== id)

      setMessages(filteredRequests)
    })
  }

  const RenderItem = ({ item, index, handleDelete }) => {
    const closeRow = index => {
      if (prevOpenedRow && prevOpenedRow !== row[index]) {
        prevOpenedRow.close()
      }
      prevOpenedRow = row[index]
    }

    const RightActions = (progress, dragX) => {
      return (
        <TouchableOpacity onPress={() => handleDelete(item._id)}>
          <View
            style={[
              Layout.center,
              Layout.fill,
              {
                width: Layout.fullWidth.width / 4,
                backgroundColor: Colors.error,
              },
            ]}
          >
            {Media.faicon('trash', 30, Colors.white, {
              borderWidth: 0,
              color: Colors.white,
            })}
          </View>
        </TouchableOpacity>
      )
    }

    const staffImageUri = `${Config.API_URL}staff/profile-image/${
      item.staff_id && item.staff_id.photo
    }`

    return (
      <Swipeable
        renderRightActions={RightActions}
        onBegan={() => closeRow(index)}
        ref={ref => (row[index] = ref)}
        rightOpenValue={-100}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            push('Chat', {
              payload: { user: item.staff_id, victim_request_id: item._id },
            })
          }}
          style={[Layout.fill, Layout.row, Layout.alignItemsCenter]}
        >
          <View
            style={[
              Layout.row,
              Layout.alignItemsStart,
              Gutters.regularVPadding,
              Gutters.regularHPadding,
              {
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(0, 0, 0, 0.1)',
                backgroundColor: Colors.chat,
                shadowColor: Colors.primary,
                shadowOffset: {
                  width: 0,
                  height: 12,
                },
                shadowOpacity: 0.5,
                shadowRadius: 16.0,
                elevation: 24,
              },
            ]}
          >
            {staffImageUri && (
              <Image
                source={{ uri: staffImageUri }}
                style={[Layout.avatar, { width: 60, height: 60 }]}
              />
            )}
            <View style={[Layout.fill, Gutters.regularHMargin]}>
              <Text
                style={[
                  Fonts.textSmall,
                  { color: Colors.black, fontWeight: '500' },
                ]}
              >
                {item.staff_id.name}
              </Text>
              <Text
                style={[
                  Fonts.textSmallest,
                  { color: Colors.black, opacity: 0.75 },
                ]}
              >
                {item.reason}
              </Text>
            </View>
            <View style={[Layout.alignItemsStart]}>
              <Text style={[Fonts.textSmallest, { color: Colors.black }]}>
                {moment(item.created_at).format('ddd')}
              </Text>
              <Text
                style={[
                  Fonts.textSmallest,
                  { color: Colors.black, opacity: 0.5 },
                ]}
              >
                {moment(item.created_at).format('hh:mm')}
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Swipeable>
    )
  }

  return (
    <StatusbarContainer
      backgroundColor={Colors.chat}
      margin={{ margin: 0 }}
      LeftHeader={() => <View></View>}
      header={true}
      color={Colors.primary}
    >
      <KeyboardAvoidingView
        style={[Layout.fill, { backgroundColor: Colors.chat }]}
      >
        <View style={[Gutters.regularHMargin]}>
          <View
            style={[
              Layout.row,
              Layout.justifyContentBetween,
              Layout.alignItemsCenter,
              Gutters.regularTMargin,
            ]}
          >
            <Text style={[Fonts.textMedium, { color: Colors.black }]}>
              Messages
            </Text>
            {Media.ionicon('search', 30, Colors.black)}
          </View>
          <View style={[{ width: '10%', borderWidth: 2 }]}></View>
        </View>

        <View style={[Layout.scrollSpaceBetween]}>
          {messages.length > 0 && (
            <FlatList
              data={messages}
              keyExtractor={item => item._id}
              style={[]}
              renderItem={({ item, index }) => (
                <RenderItem
                  item={item}
                  index={index}
                  handleDelete={deleteItem}
                />
              )}
            />
          )}
        </View>

        <View style={[Gutters.regularPadding, { borderTopWidth: 1 }]}>
          <View
            style={[
              Layout.row,
              Layout.justifyContentBetween,
              Layout.alignItemsCenter,
              Gutters.regularBMargin,
            ]}
          >
            <Text
              style={[
                Fonts.textRegular,
                Gutters.regularVPadding,
                { color: Colors.black },
              ]}
            >
              Find New Kaimahi
            </Text>
            <TouchableOpacity onPress={() => {}}>
              {Media.evicon('plus', 40, Colors.black)}
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    </StatusbarContainer>
  )
}

export default MessagesContainer
