import React from 'react'
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native'
import { useSelector } from 'react-redux'

import { StatusbarContainer } from '@/Containers'
import { useTheme } from '@/Hooks'
import { push } from '@/Navigators/utils'
import { Config } from '@/Config'

const OurKaimahiContainer = () => {
  const TEXT_LENGTH = 250
  const TEXT_HEIGHT = 24
  const OFFSET = TEXT_LENGTH / 2 - TEXT_HEIGHT / 2

  const { Common, Layout, Gutters, Fonts, Media } = useTheme()
  const { list } = useSelector(state => state.organizations)
  const RenderItem = ({ item }) => {
    const logoUri = `${Config.API_URL}staff/profile-image/${item.staff[0].photo}`
    const bannerUri = `${Config.API_URL}organization/image/${item.banner}`

    return (
      <TouchableOpacity
        onPress={() =>
          push('KaimahiIntro', {
            organization: item,
          })
        }
        style={[
          Layout.fill,
          Layout.center,
          Gutters.smallTPadding,
          { backgroundColor: item.color },
        ]}
      >
        <Image
          source={{
            uri: logoUri,
          }}
          style={[{ width: 50, height: 50 }]}
        />

        <View style={[Layout.fill, Layout.center]}>
          <View style={{ width: TEXT_HEIGHT, height: TEXT_LENGTH }}>
            <Text
              style={[
                Fonts.textSmall,
                {
                  transform: [
                    { rotate: '-90deg' },
                    { translateX: -OFFSET + 10 },
                    { translateY: -OFFSET },
                  ],
                  width: TEXT_LENGTH,
                  height: TEXT_HEIGHT,
                },
              ]}
            >
              {item.name}
            </Text>
          </View>

          <Image
            source={{ uri: bannerUri }}
            style={[{ width: 40, height: 150 }]}
          />
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <StatusbarContainer
      backgroundColor={Common.colors.white}
      margin={{ margin: 0 }}
      LeftHeader={() => (
        <TouchableWithoutFeedback onPress={() => push('Location')}>
          {Media.feicon('map-pin', 30, Common.colors.primary)}
        </TouchableWithoutFeedback>
      )}
      header={true}
      color={Common.colors.primary}
    >
      <View style={[Layout.fill, { backgroundColor: Common.colors.white }]}>
        <View
          style={[Layout.fill, Layout.column, Layout.relative, { zIndex: 999 }]}
        >
          <View
            style={[Gutters.regularHMargin, Gutters.largeVMargin, { flex: 1 }]}
          >
            <Text
              style={[
                Fonts.textLarge,
                { color: '#000000', textTransform: 'capitalize' },
              ]}
            >
              {Config.APP_NAME}
            </Text>
          </View>

          {list && (
            <View style={[Layout.fill, Layout.row, { flex: 9 }]}>
              {list.map(
                org =>
                  org.staff.length > 0 && (
                    <RenderItem item={org} key={org._id} />
                  ),
              )}
            </View>
          )}
        </View>

        <View
          style={[
            Layout.absolute,
            Layout.inset,
            { zIndex: -1, top: '-70%', left: '-40%' },
          ]}
        >
          <Media.svgs.Watermark_2
            width={'130%'}
            height={'130%'}
            fill={'#000'}
            style={[{ borderWidth: 1 }]}
          />
        </View>
      </View>
    </StatusbarContainer>
  )
}

export default OurKaimahiContainer
