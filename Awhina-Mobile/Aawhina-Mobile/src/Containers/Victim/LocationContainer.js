import React from 'react'
import { Platform, View } from 'react-native'
import MapView, {
  Marker,
  PROVIDER_DEFAULT,
  PROVIDER_GOOGLE,
} from 'react-native-maps' // remove PROVIDER_GOOGLE import if not using Google Maps

import { StatusbarContainer } from '@/Containers'
import { useTheme } from '@/Hooks'

const LocationContainer = () => {
  const { Colors, Layout } = useTheme()

  return (
    <StatusbarContainer
      backgroundColor={Colors.white}
      headerColor={Colors.primary}
      margin={{ margin: 0 }}
      header
      layout="absolute"
      color={Colors.white}
    >
      <View style={[Layout.fill]}>
        <MapView
          provider={
            Platform.OS === 'android' ? PROVIDER_GOOGLE : PROVIDER_DEFAULT
          }
          style={[Layout.fill]}
          showsUserLocation={false}
          zoomEnabled={true}
          zoomControlEnabled={true}
          initialRegion={{
            latitude: -36.848461,
            longitude: 174.763336,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        >
          <Marker
            coordinate={{ latitude: -36.848461, longitude: 174.763336 }}
            title={'JavaTpoint'}
            description={'Java Training Institute'}
          />
        </MapView>
      </View>
    </StatusbarContainer>
  )
}

export default LocationContainer
