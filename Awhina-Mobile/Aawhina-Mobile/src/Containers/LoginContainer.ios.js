import React from 'react'
import {
  ActivityIndicator,
  SafeAreaView,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import DeviceInfo from 'react-native-device-info'
import Toast from 'react-native-simple-toast'
import { useTheme } from '@/Hooks'

import {
  useVictimLoginMutation,
  useStaffLoginMutation,
} from '@/Services/modules/auth'

import { AnimatedInputField, Brand, CharField, HeaderBar } from '@/Components'

import { goBack } from '@/Navigators/utils'
import { Config } from '@/Config'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import KeyboardAvoidingView from 'react-native/Libraries/Components/Keyboard/KeyboardAvoidingView'

const LoginContainer = ({ route }) => {
  const { Colors, Fonts, Gutters, Media, Layout } = useTheme()
  const [user, setUser] = React.useState({
    device_id: DeviceInfo.getUniqueId(),
    email: '',
    password: '',
    pin: '',
    mode: 'fe',
  })
  const [type, setType] = React.useState(null)
  const [victimLogin, { isLoading: isVictimLoading }] = useVictimLoginMutation()
  const [staffLogin, { isLoading: isStaffLoading }] = useStaffLoginMutation()

  const loginVictim = () => {
    const { password, pin } = user

    victimLogin({ ...user, password, pin })
      .then(response => {
        response.error &&
          Toast.show(
            response.error.data
              ? response.error.data.message
              : response.error.error,
          )

        setUser({
          ...user,
          password: '',
          pin: '',
        })
      })
      .catch(error => {
        setUser({ ...user, pin: '', password: '' })

        Toast.show(error.data.message)
      })
  }

  const loginStaff = () => {
    const { password, pin } = user

    staffLogin({ ...user, password, pin })
      .then(response => {
        response.error &&
          Toast.show(
            response.error.data
              ? response.error.data.message
              : response.error.error,
          )

        setUser({
          ...user,
          password: '',
          pin: '',
        })
      })
      .catch(error => {
        setUser({ ...user, pin: '', password: '' })

        Toast.show(error.data.message)
      })
  }

  const VirtualKeyboard = ({ initialValue }) => {
    let input = initialValue
    const rows = [
      [
        { type: 'string', val: 1 },
        { type: 'string', val: 2 },
        { type: 'string', val: 3 },
      ],
      [
        { type: 'string', val: 4 },
        { type: 'string', val: 5 },
        { type: 'string', val: 6 },
      ],
      [
        { type: 'string', val: 7 },
        { type: 'string', val: 8 },
        { type: 'string', val: 9 },
      ],
      [
        { type: 'string', val: '' },
        { type: 'string', val: 0 },
        {
          type: 'component',
          val: 'del',
          component: Media.ionicon('chevron-back'),
        },
      ],
    ]
    const btnSize = 65

    return rows.map((cols, rindex) => (
      <View
        style={[Layout.rowCenter, Layout.justifyContentBetween]}
        key={`col-${rindex}`}
      >
        {cols.map((col, cindex) =>
          col.val !== '' ? (
            <TouchableOpacity
              key={`col-${rindex}-${cindex}`}
              style={[
                col.type !== 'component' && Layout.border,
                Layout.center,
                Gutters.smallVMargin,
                {
                  width: btnSize,
                  height: btnSize,
                  borderRadius: col.type !== 'component' ? btnSize / 2 : 0,
                  borderColor: col.type !== 'component' && Colors.white,
                },
              ]}
              onPress={() => {
                input =
                  col.val === 'del'
                    ? input.substring(0, input.length - 1)
                    : `${input}${col.val}`

                setUser({
                  ...user,
                  pin: input,
                  password: input,
                })
              }}
            >
              {col.type === 'string' ? (
                <Text style={[Fonts.titleSmallest]}>{col.val}</Text>
              ) : (
                <View key={`component-${cindex}`}>{col.component}</View>
              )}
            </TouchableOpacity>
          ) : (
            <View
              key={`nokey-${rindex}-${cindex}`}
              style={[
                Gutters.smallVMargin,
                {
                  width: btnSize,
                  height: btnSize,
                },
              ]}
            ></View>
          ),
        )}
      </View>
    ))
  }

  React.useEffect(() => {
    const { type: userType } = route.params
    userType && setType(userType)
  }, [route.params])

  React.useEffect(() => {
    if (user.password.length >= 4) {
      type === 'victim' ? loginVictim() : type === 'staff' ? loginStaff() : null
    }
  }, [user.password])

  return (
    <View style={[Layout.fill, { backgroundColor: Colors.primary }]}>
      <HeaderBar
        LeftHeader={() => (
          <TouchableWithoutFeedback onPress={goBack}>
            {Media.entypo('chevron-thin-left', 25, Colors.white)}
          </TouchableWithoutFeedback>
        )}
        showRightHeader={false}
        layout={'absolute'}
        color={Colors.white}
      />

      <ScrollView
        contentContainerStyle={[
          Layout.fill,
          Layout.justifyContentBetween,
          Gutters.largeVMargin,
          Gutters.largeTPadding,
        ]}
      >
        <Brand
          title={Config.APP_NAME}
          subtitle={
            'Lorem ipsum dolor sit amet, sectetur  adipiscing elit, leased do eius'
          }
        />

        <KeyboardAvoidingView
          behavior={Platform.select({
            android: 'position',
            ios: 'padding',
          })}
          keyboardVerticalOffset={Platform.select({
            android: 20,
          })}
        >
          <View style={[Gutters.largeHMargin, Gutters.largeHPadding]}>
            {type === 'staff' && (
              <AnimatedInputField
                placeholder="Email"
                valid={true}
                errorText="Please enter email"
                onChangeText={val => setUser({ ...user, email: val })}
                value={user.email}
              />
            )}

            <CharField value={user.pin} />
          </View>
        </KeyboardAvoidingView>

        <ActivityIndicator
          animating={isStaffLoading || isVictimLoading}
          style={[Gutters.smallVMargin]}
        />
        <View
          style={[
            Gutters.largeBMargin,
            Gutters.largeHMargin,
            Gutters.largeHPadding,
          ]}
        >
          <VirtualKeyboard initialValue={user.pin} />
        </View>
      </ScrollView>
    </View>
  )
}

export default LoginContainer
