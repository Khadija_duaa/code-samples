import React from 'react'
import {
  ActivityIndicator,
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  StyleSheet,
} from 'react-native'
import DeviceInfo from 'react-native-device-info'
import Toast from 'react-native-simple-toast'

import { useTheme } from '@/Hooks'
import { AnimatedInputField, Brand } from '@/Components'

import { navigate } from '@/Navigators/utils'
import { useRegisterMutation } from '@/Services/modules/auth'
import KeyboardAvoidingView from 'react-native/Libraries/Components/Keyboard/KeyboardAvoidingView'
import AnimatedInput from 'react-native-animated-input'
import Colors from 'react-native/Libraries/NewAppScreen/components/Colors'
import { heightToDp, widthToDp } from '../../Utils/Responsive'

const SignupContainer = () => {
  const [isValid, setIsValid] = React.useState(false)
  const [user, setUser] = React.useState({
    device_id: DeviceInfo.getUniqueId(),
    email: '',
    name: '',
    phone: '',
    password: '',
    color: '#FFFFFF',
  })
  const { Colors, Fonts, Layout, Common, Gutters } = useTheme()

  const [register, { isLoading }] = useRegisterMutation() // use the query

  const validateEmail = mail =>
    /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)

  React.useEffect(() => {
    setIsValid(
      validateEmail(user.email) &&
        user.name !== '' &&
        user.phone !== '' &&
        user.password !== '',
    )
  }, [user])

  const getRandomColor = () => {
    var letters = '01234567'
    var color = '#'
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * letters.length)]
    }
    return color
  }

  const handleSignup = () => {
    register({ ...user, color: getRandomColor() })
      .then(response => {
        response.error && Toast.show(response.error.data.errors.join(','))
      })
      .catch(({ error }) => {
        Toast.show(error.error)
      })
  }

  const onChange = (key, val) => setUser({ ...user, [key]: val })

  const roundedSignUp = {
    borderRadius: widthToDp(20),
    ...Layout.center,
    height: heightToDp(7),
    width: widthToDp(33),
  }

  const _styles = StyleSheet.create({
    viewContent: {
      borderBottomWidth: 1,
      borderBottomColor: Colors.white,
    },
    outlineRoundedSignUp: {
      ...roundedSignUp,
      backgroundColor: Colors.transparent,
      borderWidth: 1,
      borderColor: Colors.white,
    },
  })

  return (
    <ScrollView
      style={[
        [Layout.fill, Layout.column],
        { backgroundColor: Colors.primary },
      ]}
      contentContainerStyle={[[Gutters.largeHPadding], { flexGrow: 1 }]}
    >
      <View style={{ flex: 30 }}>
        <Brand
          title={'Sign up'}
          subtitle={
            'Lorem ipsum dolor sit amet, sectetur  adipiscing elit, leased do eius'
          }
        />
      </View>
      <View style={[Layout.column, Layout.fill]}>
        <View style={_styles.viewContent}>
          <AnimatedInputField
            placeholder="Name"
            valid={true}
            errorText="Please enter name"
            onChangeText={val => onChange('name', val)}
            value={user.name}
          />
        </View>
        <View style={_styles.viewContent}>
          <AnimatedInputField
            placeholder="Email"
            valid={true}
            errorText="Please enter email"
            onChangeText={val => onChange('email', val)}
            value={user.email}
          />
        </View>

        <View style={_styles.viewContent}>
          <AnimatedInputField
            placeholder="Phone"
            valid={true}
            errorText="Please enter phone"
            onChangeText={val => onChange('phone', val)}
            value={user.phone}
          />
        </View>
        <View style={_styles.viewContent}>
          <AnimatedInputField
            placeholder="4 Digit Pin"
            valid={true}
            errorText="Please enter 4 Digit Pin"
            onChangeText={val => onChange('password', val)}
            value={user.password}
          />
        </View>
      </View>
      <ActivityIndicator animating={isLoading} />
      <View style={{ flex: 1 }} />
      <View
        style={[[Layout.alignItemsCenter], { flexDirection: 'row', flex: 15 }]}
      >
        <View style={[[Layout.alignItemsCenter, Layout.column], { flex: 1 }]}>
          <TouchableOpacity
            disabled={!isValid}
            onPress={() => {
              // setIsLoading(true);
              // setIsLoading(false);
              handleSignup()
              // navigate('Welcome');
            }}
            style={_styles.outlineRoundedSignUp}
          >
            <View>
              <Text style={[[Fonts.textSmall], { color: 'white' }]}>Next</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  )
}

export default SignupContainer
