import React from 'react'
import {
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import DeviceInfo from 'react-native-device-info'
import Toast from 'react-native-simple-toast'

import { useTheme } from '@/Hooks'
// import { StatusbarContainer } from '@/Containers';
import {
  AnimatedInputField,
  Brand,
  HeaderBar,
  LoadingButton,
} from '@/Components'

import { goBack } from '@/Navigators/utils'
import { useRegisterMutation } from '@/Services/modules/auth'

const SignupContainer = () => {
  // const [isLoading, setIsLoading] = React.useState(false);
  const [isValid, setIsValid] = React.useState(false)
  const [user, setUser] = React.useState({
    device_id: DeviceInfo.getUniqueId(),
    email: '',
    name: '',
    phone: '',
    password: '',
    color: '#FFFFFF',
  })
  const { Colors, Fonts, Layout, Media, Common, Gutters } = useTheme()
  const [register, { isLoading }] = useRegisterMutation() // use the query

  const validateEmail = mail =>
    /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)

  React.useEffect(() => {
    setIsValid(
      validateEmail(user.email) &&
        user.name !== '' &&
        user.phone !== '' &&
        user.password !== '',
    )
  }, [user])

  const getRandomColor = () => {
    var letters = '01234567'
    var color = '#'
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * letters.length)]
    }
    return color
  }

  const handleSignup = () => {
    register({ ...user, color: getRandomColor() })
      .then(response => {
        response.error && Toast.show(response.error.data.errors.join(','))
      })
      .catch(({ error }) => {
        Toast.show(error.error)
      })
  }

  const onChange = (key, val) => setUser({ ...user, [key]: val })

  return (
    <KeyboardAvoidingView
      style={[Layout.fill, { backgroundColor: Colors.primary }]}
      behavior={Platform.select({
        android: 'position',
        ios: 'padding',
      })}
      keyboardVerticalOffset={Platform.select({
        android: 20,
      })}
    >
      <HeaderBar
        LeftHeader={() => (
          <TouchableWithoutFeedback onPress={goBack}>
            {Media.entypo('chevron-thin-left', 25, Colors.white)}
          </TouchableWithoutFeedback>
        )}
        showRightHeader={false}
        layout={'absolute'}
        color={Colors.white}
      />

      <ScrollView
        contentContainerStyle={[
          Layout.fill,
          Layout.justifyContentBetween,
          Gutters.regularHMargin,
          Gutters.largeVMargin,
          Gutters.largeTPadding,
        ]}
      >
        <Brand
          title={'Sign up'}
          subtitle={
            'Lorem ipsum dolor sit amet, sectetur  adipiscing elit, leased do eius'
          }
        />
        <View style={[Layout.col]}>
          <AnimatedInputField
            placeholder="Name"
            valid={true}
            errorText="Please enter name"
            onChangeText={val => onChange('name', val)}
            value={user.name}
          />

          <AnimatedInputField
            placeholder="Email"
            valid={true}
            errorText="Please enter email"
            onChangeText={val => onChange('email', val)}
            value={user.email}
          />

          <AnimatedInputField
            placeholder="Phone"
            valid={true}
            keyboardType={'phone-pad'}
            errorText="Please enter phone"
            onChangeText={val => onChange('phone', val)}
            value={user.phone}
          />

          <AnimatedInputField
            placeholder="4 Digit Pin"
            valid={true}
            keyboardType={'numeric'}
            errorText="Please enter 4 Digit Pin"
            onChangeText={val => onChange('password', val)}
            value={user.password}
          />
        </View>
        {/* <ActivityIndicator animating={isLoading} /> */}

        <View style={[Gutters.largeHMargin, Gutters.smallBMargin]}>
          <LoadingButton
            label={'Next'}
            handlePress={handleSignup}
            isLoading={isLoading}
          />
          {/* <TouchableOpacity
            disabled={!isValid}
            onPress={() => {
              // setIsLoading(true);
              // setIsLoading(false);
              handleSignup()
              // navigate('Welcome');
            }}
            style={[Common.button.outlineRounded, Gutters.smallVPadding]}
          >
            <View style={[Common.button.shallow]}>
              <Text style={[Gutters.regukarVPadding, Fonts.textSmall]}>
                Next
              </Text>
            </View>
          </TouchableOpacity> */}
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  )
}

export default SignupContainer
