import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  useWindowDimensions,
  Platform,
  Dimensions,
} from 'react-native'
import Video from 'react-native-video'
import { StatusbarContainer } from '@/Containers'
import { Swiper, ToggleButton } from '@/Components'
import { useTheme } from '@/Hooks'
import { navigateAndSimpleReset } from '@/Navigators/utils'
import { heightToDp } from '../../Utils/Responsive'

const height = Dimensions.get('screen').height
const WelcomeContainer = () => {
  const { Colors, Layout, Gutters, Fonts, Media } = useTheme()
  const [muted, setMuted] = React.useState(false)

  return (
    <StatusbarContainer backgroundColor={Colors.black} margin={{ margin: 0 }}>
      <View style={[Layout.fill, Layout.colCenter]}>
        <Swiper>
          {Media.videos.map((video, index) => (
            <Video
              resizeMode="contain"
              source={video}
              muted={muted}
              paused={false}
              repeat={false}
              playInBackground={true}
              volume={100}
              key={`video-${index}`}
              style={[
                Platform.OS === 'android'
                  ? {
                      height: height,
                    }
                  : {
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      bottom: 0,
                      right: 0,
                    },
              ]}
            />
          ))}
        </Swiper>
        <View
          style={[
            Layout.absolute,
            Layout.fullWidth,
            {
              bottom: heightToDp(8),
              zIndex: 99999,
            },
          ]}
        >
          <View
            style={[
              Gutters.mediumHMargin,
              Layout.row,
              Layout.justifyContentBetween,
              Layout.alignItemsCenter,
            ]}
          >
            <TouchableOpacity
              onPress={() => {
                navigateAndSimpleReset('Hotlinks')
              }}
            >
              <Text style={[Fonts.textRegular]}>Skip</Text>
            </TouchableOpacity>

            <ToggleButton icon={'volume'} toggle={muted} setToggle={setMuted} />
          </View>
        </View>
      </View>
    </StatusbarContainer>
  )
}

export default WelcomeContainer
