/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import { View, Text } from 'react-native'

import { StatusbarContainer } from '@/Containers'
import { useTheme } from '@/Hooks'
import { navigateAndSimpleReset } from '@/Navigators/utils'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Swiper } from '@/Components'
import { Config } from '@/Config'

const HotlinksNewContainer = () => {
  const { Common, Colors, Layout, Gutters, Fonts, Media } = useTheme()
  const [screens, setScreens] = React.useState([
    [
      {
        icon: {
          image: Media.svgs.Hotlink_1,
          props: {
            style: [
              Layout.absolute,
              Gutters.largeTMargin,
              { right: 0, top: 30 },
            ],
            width: 150,
            height: 150,
            fill: '#000',
          },
        },
      },
      {
        string: {
          text: 'Koorero Mai',
          style: [Fonts.titleRegular, Fonts.fontSemiBold, { color: '#000000' }],
          underline: false,
        },
        substring: {
          text: 'Talk to us',
          style: [Fonts.titleSmallest, { color: '#000000' }],
          underline: false,
        },
      },
      {
        string: {
          text: '1737 Need to talk?',
          style: [Fonts.textSmall, { color: '#000000' }],
          underline: true,
        },
        substring: {
          text: '1737',
          style: [Fonts.textSmall, { color: '#000000' }],
          underline: false,
        },
      },
      {
        string: {
          text: Config.APP_NAME,
          style: [Fonts.textSmall, { color: '#000000' }],
          underline: true,
        },
        substring: {
          text: '0800SAFEHELP',
          style: [Fonts.textSmall, { color: '#000000' }],
          underline: false,
        },
      },
      {
        string: {
          text: 'Emergency',
          style: [Fonts.textSmall, { color: '#000000' }],
          underline: true,
        },
        substring: {
          text: '111',
          style: [Fonts.textSmall, { color: '#000000' }],
          underline: false,
        },
      },
    ],
  ])

  return (
    <StatusbarContainer
      backgroundColor={Colors.chat}
      headerColor={Colors.chat}
      margin={{ margin: 0 }}
      header
      color={Colors.primary}
    >
      <View style={[Layout.fill, Gutters.largeTMargin]}>
        <View
          style={[
            Gutters.largeTMargin,
            Gutters.regularHMargin,
            Layout.relative,
            Layout.fill,
            Layout.scrollSpaceBetween,
            Gutters.largeBMargin,
          ]}
        >
          <Swiper color={'#000000'} style={[Layout.relative, { zIndex: 999 }]}>
            {screens.map((screen, sindex) => (
              <View style={[Layout.fill]} key={`screen-${sindex}`}>
                <View
                  style={[
                    Layout.col,
                    Layout.justifyContentAround,
                    Layout.fill,
                    Gutters.largeVPadding,
                  ]}
                >
                  {screen.map((link, lindex) =>
                    link.icon ? (
                      <link.icon.image
                        {...link.icon.props}
                        key={`icon-${lindex}`}
                      />
                    ) : (
                      <View
                        style={[Layout.alignItemsStretch]}
                        key={`links-${lindex}`}
                      >
                        <Text style={[link.string.style]}>
                          {link.string.text}
                        </Text>
                        {link.string.underline && (
                          <View
                            style={[
                              {
                                width: '8%',
                                borderWidth: 1,
                                borderColor: '#000',
                                marginVertical: 4,
                              },
                            ]}
                          />
                        )}
                        <Text style={[link.substring.style]}>
                          {link.substring.text}
                        </Text>
                      </View>
                    ),
                  )}
                </View>
              </View>
            ))}
          </Swiper>

          <View style={[Layout.inset, Layout.absolute, { zIndex: -999 }]}>
            <Media.svgs.Watermark_1
              width={'125%'}
              height={'130%'}
              fill={'#000'}
            />
          </View>
        </View>

        <View style={[Layout.rowCenter, Gutters.largeBMargin]}>
          <View
            style={[
              Gutters.mediumHMargin,
              Layout.row,
              Layout.justifyContentBetween,
              Layout.alignItemsCenter,
            ]}
          ></View>
        </View>
      </View>
    </StatusbarContainer>
  )
}

export default HotlinksNewContainer
