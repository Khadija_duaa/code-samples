import React from 'react'
import {
  FlatList,
  KeyboardAvoidingView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { useSelector } from 'react-redux'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import moment from 'moment'

import { StatusbarContainer } from '@/Containers'
import { useTheme } from '@/Hooks'
import { push } from '@/Navigators/utils'
import {
  useDeleteRequestMutation,
  useLazyRequestListQuery,
} from '@/Services/modules/users'
import { Filters } from '@/Components'

const MessagesContainer = ({ route }) => {
  const { Colors, Layout, Gutters, Fonts, Media } = useTheme()
  const { user } = useSelector(state => state.auth)
  const [requests] = useLazyRequestListQuery({
    refetchOnMountOrArgChange: true,
  })
  const [deleteRequest] = useDeleteRequestMutation()
  const [messages, setMessages] = React.useState([])
  const [label, setLabel] = React.useState('')
  const [filters, setFilters] = React.useState(null)
  const [showFilters, setShowFilters] = React.useState(false)

  let prevOpenedRow
  const row = []

  React.useEffect(() => {
    if (route.params) {
      setLabel(route.params.label)
      setFilters(route.params.filters)
    }
  }, [route.params])

  React.useEffect(() => {
    let params = ''

    filters &&
      Object.keys(filters).map(key => (params += `${key}=${filters[key]}&`))
    params = params.substring(0, params.length - 1)

    init(params)
  }, [filters])

  const init = params => {
    requests({ id: user._id, params }).then(response => {
      const { victim_request } = response.data

      setMessages(victim_request)
    })
  }

  const deleteItem = id => {
    deleteRequest(id).then(() => {
      const filteredRequests = messages.filter(message => message._id !== id)

      setMessages(filteredRequests)
    })
  }

  const RenderItem = ({ item, index, handleDelete }) => {
    const closeRow = index => {
      if (prevOpenedRow && prevOpenedRow !== row[index]) {
        prevOpenedRow.close()
      }
      prevOpenedRow = row[index]
    }

    const RightActions = () => {
      return (
        <TouchableOpacity onPress={() => handleDelete(item._id)}>
          <View
            style={[
              Layout.center,
              Layout.fill,
              {
                width: Layout.fullWidth.width / 4,
                backgroundColor: Colors.error,
              },
            ]}
          >
            {Media.faicon('trash', 30, Colors.white, {
              borderWidth: 0,
              color: Colors.white,
            })}
          </View>
        </TouchableOpacity>
      )
    }

    return (
      <Swipeable
        renderRightActions={RightActions}
        onBegan={() => closeRow(index)}
        ref={ref => (row[index] = ref)}
        rightOpenValue={-100}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            push('Chat', {
              payload: { user: item.victim_id, victim_request_id: item._id },
            })
          }}
          style={[Layout.fill, Layout.row, Layout.alignItemsCenter]}
        >
          <View
            style={[
              Layout.row,
              Layout.alignItemsStart,
              Gutters.regularVPadding,
              Gutters.regularHPadding,
              {
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(0, 0, 0, 0.1)',
                backgroundColor: Colors.chat,
                shadowColor: Colors.primary,
                shadowOffset: {
                  width: 0,
                  height: 12,
                },
                shadowOpacity: 0.5,
                shadowRadius: 16.0,
                elevation: 24,
              },
            ]}
          >
            <View
              style={[
                Layout.avatar,
                Gutters.smallTMargin,
                {
                  width: 30,
                  height: 30,
                  backgroundColor: item.victim_id.color,
                },
              ]}
            />
            <View style={[Layout.fill, Gutters.smallHMargin]}>
              <Text
                style={[
                  Fonts.textSmall,
                  { color: Colors.black, fontWeight: '500' },
                ]}
              >
                {item.victim_id.name}
              </Text>
              <Text
                style={[
                  Fonts.textSmallest,
                  { color: Colors.black, opacity: 0.75 },
                ]}
              >
                {item.reason}
              </Text>
            </View>
            <View style={[Layout.alignItemsStart]}>
              <Text style={[Fonts.textSmallest, { color: Colors.black }]}>
                {moment(item.created_at).format('ddd')}
              </Text>
              <Text
                style={[
                  Fonts.textSmallest,
                  { color: Colors.black, opacity: 0.5 },
                ]}
              >
                {moment(item.created_at).format('hh:mm')}
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Swipeable>
    )
  }

  return (
    <StatusbarContainer
      backgroundColor={Colors.chat}
      margin={{ margin: 0 }}
      LeftHeader={() => <View></View>}
      header={true}
      color={Colors.primary}
    >
      <KeyboardAvoidingView
        style={[Layout.fill, { backgroundColor: Colors.chat }]}
      >
        <View style={[Gutters.regularHMargin]}>
          <View
            style={[
              Layout.row,
              Layout.justifyContentBetween,
              Layout.alignItemsCenter,
              Gutters.regularTMargin,
            ]}
          >
            <Text style={[Fonts.textMedium, { color: Colors.black }]}>
              {label || 'Messages'}
            </Text>
            {Media.ionicon('search', 30, Colors.black)}
          </View>
          <View style={[{ width: '10%', borderWidth: 2 }]}></View>
        </View>

        <View style={[Layout.scrollSpaceBetween]}>
          {messages.length > 0 && (
            <FlatList
              data={messages}
              keyExtractor={item => item._id}
              style={[]}
              renderItem={({ item, index }) => (
                <RenderItem
                  item={item}
                  index={index}
                  handleDelete={deleteItem}
                />
              )}
            />
          )}
        </View>

        <View style={[Gutters.regularPadding, { borderTopWidth: 1 }]}>
          <View
            style={[
              Layout.row,
              Layout.justifyContentBetween,
              Layout.alignItemsCenter,
              Gutters.regularBMargin,
            ]}
          >
            <View style={[Layout.fill, Layout.center]}>
              <Text
                style={[
                  Fonts.textRegular,
                  Gutters.regularVPadding,
                  { color: Colors.black },
                ]}
              >
                Filter
              </Text>
            </View>
            <TouchableOpacity onPress={() => setShowFilters(true)}>
              {Media.faicon('sliders', 20, Colors.black, { borderWidth: 0 })}
            </TouchableOpacity>
          </View>
        </View>
        {showFilters && (
          <Filters
            filters={filters}
            setShow={setShowFilters}
            setFilters={payload => setFilters(payload)}
          />
        )}
      </KeyboardAvoidingView>
    </StatusbarContainer>
  )
}

export default MessagesContainer
