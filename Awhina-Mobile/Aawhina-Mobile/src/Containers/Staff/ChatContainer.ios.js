import React from 'react'
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { useSelector } from 'react-redux'

import { HeaderBar } from '@/Components'
import { Menu } from '@/Navigators'
import { useTheme } from '@/Hooks'
import { goBack, navigate, navigateAndSimpleReset } from '@/Navigators/utils'
import { Config } from '@/Config'
import {
  useUpdateStatusMutation,
  useArchiveMutation,
  useBlockMutation,
  useChatMutation,
  useLazyChatListQuery,
} from '@/Services/modules/users'
import Filters from '@/Containers/Staff/Filters'

const ChatContainer = ({ route }) => {
  const { Colors, Common, Layout, Gutters, Fonts, Media } = useTheme()
  const { user } = useSelector(state => state.auth)
  const [show, setShow] = React.useState(false)
  const [hide, setHide] = React.useState(true)
  const [currentUser, setCurrentUser] = React.useState(null)
  const [chatMessage, setChatMessage] = React.useState({
    message: '',
  })
  const [messages, setMessages] = React.useState([])
  const [filters, setFilters] = React.useState(null)
  const [showFilters, setShowFilters] = React.useState(false)

  const [updateStatus] = useUpdateStatusMutation()
  const [archive] = useArchiveMutation()
  const [block] = useBlockMutation()
  const [chat] = useChatMutation()

  const [chatList] = useLazyChatListQuery()
  const [victimRequestId, setVictimRequestId] = React.useState(null)

  let chatInterval = null

  React.useEffect(() => {
    setCurrentUser(route.params.payload.user)
    setVictimRequestId(route.params.payload.victim_request_id)
  }, [route.params.payload])

  React.useEffect(() => {
    console.log('victimRequestId ::: ', victimRequestId)
    chatInterval = setInterval(() => {
      init()
    }, Config.REFRESH_INTERVAL)

    return () => clearInterval(chatInterval)
  }, [victimRequestId])

  // React.useEffect(() => () => clearInterval(chatInterval))

  const init = () =>
    victimRequestId &&
    chatList(victimRequestId).then(response => {
      setMessages(response.data.chat)
    })

  const handleMessage = () => {
    chat(chatMessage)
      .then(() => init())
      .then(() =>
        setChatMessage({
          victim_request_id: victimRequestId,
          message: '',
          file: null,
        }),
      )
  }

  const Message = ({ item }) => {
    const loggedIn = item.from_user_id._id === user._id
    const userImageUrl = `${Config.API_URL}staff/profile-image/${item.from_user_id.photo}`

    return (
      <View
        style={[
          Layout.fill,
          Gutters.smallMargin,
          loggedIn ? Layout.rowReverse : Layout.row,
        ]}
      >
        {loggedIn ? (
          <Image
            source={{
              uri: userImageUrl,
            }}
            style={[
              Layout.avatar,
              {
                width: 30,
                height: 30,
                marginHorizontal: 6,
                backgroundColor: Colors.white,
              },
            ]}
          />
        ) : (
          <View
            style={[
              Layout.avatar,
              {
                width: 30,
                height: 30,
                marginHorizontal: 6,
                backgroundColor: currentUser.color,
              },
            ]}
          />
        )}
        <View
          style={[
            Gutters.smallPadding,
            {
              borderRadius: 8,
              backgroundColor: loggedIn ? Colors.chatBubble : Colors.white,
              flexShrink: 1,
            },
          ]}
        >
          <Text style={[]}>{item.message}</Text>
        </View>
      </View>
    )
  }

  return (
    <View style={[Layout.fill, { backgroundColor: Colors.chat }]}>
      {!show ? (
        <HeaderBar
          LeftHeader={() => (
            <TouchableWithoutFeedback onPress={goBack}>
              {Media.entypo('chevron-thin-left', 25, Colors.primary)}
            </TouchableWithoutFeedback>
          )}
          headerColor={Colors.chat}
          setShow={setShow}
          layout={'fullWidth'}
          color={Colors.primary}
        />
      ) : (
        <View
          style={[
            Layout.absolute,
            Layout.inset,
            { backgroundColor: Colors.chat, zIndex: 999 },
          ]}
        >
          <Menu setShow={setShow} backgroundColor={Colors.chat} />
        </View>
      )}
      {currentUser && (
        <KeyboardAvoidingView
          style={[Layout.fill, { backgroundColor: Colors.chat }]}
          behavior={Platform.select({
            android: 'position',
            ios: 'padding',
          })}
          keyboardVerticalOffset={Platform.select({
            android: 20,
          })}
        >
          <View style={[Layout.row, Layout.center, { borderBottomWidth: 1 }]}>
            <View
              style={[
                Layout.avatar,
                {
                  backgroundColor: currentUser.color,
                  width: 15,
                  height: 15,
                },
              ]}
            />
            <Text
              style={[
                Fonts.titleSmall,
                Gutters.regularPadding,
                {
                  color: Common.colors.black,
                },
              ]}
            >
              {currentUser.name}
            </Text>

            <TouchableWithoutFeedback onPress={() => setHide(!hide)} style={[]}>
              {Media.entypo(
                hide ? 'chevron-thin-down' : 'chevron-thin-up',
                25,
                Colors.primary,
                {
                  borderWidth: 0,
                },
              )}
            </TouchableWithoutFeedback>
          </View>

          {hide ? (
            <View style={[Layout.scrollSpaceBetween]}>
              <View style={[Layout.scrollSpaceBetween]}>
                <FlatList
                  data={messages}
                  keyExtractor={message => message._id}
                  renderItem={Message}
                  inverted
                />
              </View>

              <View
                style={[
                  Layout.center,
                  Layout.row,
                  Gutters.regularTPadding,
                  Gutters.mediumBMargin,
                  { borderTopWidth: 1 },
                ]}
              >
                <TouchableOpacity
                  onPress={init}
                  style={[Layout.fill, Layout.center]}
                >
                  {Media.ionicon('refresh-outline', 30, Common.colors.black)}
                </TouchableOpacity>

                <TextInput
                  value={chatMessage.message}
                  placeholder={'type your message here ...'}
                  style={[
                    Gutters.regularPadding,
                    // Layout.selfStretch,
                    {
                      flex: 5,
                      backgroundColor: Colors.chatLight,
                      borderRadius: 30,
                    },
                  ]}
                  onChangeText={str =>
                    setChatMessage({
                      victim_request_id: victimRequestId,
                      message: str,
                    })
                  }
                />

                <TouchableOpacity
                  onPress={handleMessage}
                  style={[Layout.fill, Layout.center]}
                >
                  {Media.ionicon(
                    'chevron-forward-circle-outline',
                    40,
                    Common.colors.black,
                  )}
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <View style={[Layout.scrollSpaceBetween, Layout.relative]}>
              <Menu
                backgroundColor={Colors.chat}
                menuItems={[
                  {
                    title: 'User Notes',
                    onPress: () => {
                      navigate('Notes', { victimRequestId })
                    },
                  },
                  {
                    title: 'User Status / Urgency',
                    onPress: () => setShowFilters(true),
                    // onPress: urgency => {
                    //     updateStatus(victimRequestId).then(() => setHide(true))
                    // },
                  },
                  {
                    title: 'Archive',
                    onPress: () => {
                      updateStatus({
                        requestId: victimRequestId,
                        status: 'archived',
                      }).then(() => setHide(true))
                    },
                  },
                  {
                    title: 'Block',
                    onPress: () => {
                      updateStatus({
                        requestId: victimRequestId,
                        status: 'in_process',
                      }).then(() => setHide(true))
                    },
                  },
                ]}
                showClose={false}
                setShow={setHide}
              />
            </View>
          )}
        </KeyboardAvoidingView>
      )}
      {showFilters && (
        <Filters
          victimRequestId={victimRequestId}
          filters={filters}
          setShow={setShowFilters}
          setFilters={payload => setFilters(payload)}
        />
      )}
    </View>
  )
}

export default ChatContainer
