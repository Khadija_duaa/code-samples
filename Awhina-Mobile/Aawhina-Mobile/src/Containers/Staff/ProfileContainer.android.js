import React from 'react'
import {
    View,
    Text,
    KeyboardAvoidingView,
    TouchableOpacity,
    TouchableWithoutFeedback, StyleSheet, ScrollView,
} from 'react-native'
import { useSelector } from 'react-redux'
import Toast from 'react-native-simple-toast'
import { profile } from '@/Store/Auth'
import { AnimatedInputField, CharField, HeaderBar } from '@/Components'
import { useTheme } from '@/Hooks'
import { useProfileMutation } from '@/Services/modules/auth'
import { goBack } from '@/Navigators/utils'



const ProfileContainer = () => {
    const [updateProfile] = useProfileMutation()
    const { user } = useSelector(state => state.auth)
    const [profile, setProfile] = React.useState({
        ...user,
        old_password: '',
        new_password: '',
    })
    const { Common, Colors, Fonts, Gutters, Layout, Media } = useTheme()

    const onChange = (key, val) => setProfile({ ...profile, [key]: val })

    const _styles = StyleSheet.create({
        viewContent: {
            borderBottomWidth: 1,
            borderBottomColor: Colors.white,
        },
    })

    return (
        <ScrollView
            style={[
                [Layout.fill],
                { backgroundColor: Colors.chat },
            ]}
            contentContainerStyle={[[Gutters.largeHPadding], { flexGrow: 1 }]}
        >
            <HeaderBar
                LeftHeader={() => (
                    <TouchableWithoutFeedback onPress={goBack}>
                        {Media.entypo('chevron-thin-left', 25, Colors.primary)}
                    </TouchableWithoutFeedback>
                )}
                showRightHeader={false}
                headerColor={Colors.chat}
                setShow={null}
                layout={'fullWidth'}
                color={Colors.primary}
            />

                <View style={[Gutters.regularMargin]}>
                    <View
                        style={[
                            Layout.row,
                            Layout.justifyContentBetween,
                            Layout.alignItemsCenter,
                            Gutters.regularTMargin,
                        ]}
                    >
                        <Text style={[Fonts.textMedium, { color: Colors.black }]}>
                            My Profile
                        </Text>
                    </View>
                    <View
                        style={[Gutters.regularTMargin, { width: '10%', borderWidth: 2 }]}
                    ></View>

                    <View style={[Gutters.largeTMargin]}>
                        <View style={[Gutters.largeTMargin]}>
                            <View style={_styles.viewContent}>
                            <AnimatedInputField
                                placeholder="Name"
                                valid={true}
                                errorText="Please enter name"
                                onChangeText={val => onChange('name', val)}
                                value={profile.name}
                                style={[{ color: Colors.black }]}
                            />
                            </View>
                        </View>

                        <View style={[Gutters.largeTMargin]}>
                            <View style={_styles.viewContent}>
                            <AnimatedInputField
                                placeholder="Phone"
                                valid={true}
                                keyboardType={'phone-pad'}
                                errorText="Please enter phone"
                                onChangeText={val => onChange('phone', val)}
                                value={profile.phone}
                                style={[{ color: Colors.black }]}
                            />
                            </View>
                        </View>


                        <View style={[Gutters.largeTMargin]}>
                            <View style={_styles.viewContent}>
                            <AnimatedInputField
                                placeholder="Old Pin"
                                valid={true}
                                keyboardType='number-pad'
                                errorText="Please enter 4 Digit Pin"
                                onChangeText={val => onChange('old_password', val)}
                                value={profile.old_password}
                                style={[{ color: Colors.black }]}
                                secureTextEntry={true}
                            />
                            </View>
                        </View>

                        <View style={[Gutters.largeTMargin]}>
                            <View style={_styles.viewContent}>
                            <AnimatedInputField
                                placeholder="New Pin"
                                valid={true}
                                keyboardType='number-pad'
                                errorText="Please enter 4 Digit Pin"
                                onChangeText={val => onChange('new_password', val)}
                                value={profile.new_password}
                                style={[{ color: Colors.black }]}
                                secureTextEntry={true}
                            />
                            </View>
                        </View>


                        <View style={[Layout.rowCenter, Gutters.largeTMargin]}>
                            <View
                                style={[
                                    Gutters.mediumHMargin,
                                    Layout.row,
                                    Layout.justifyContentBetween,
                                    Layout.alignItemsCenter,
                                ]}
                            >
                                <TouchableOpacity
                                    onPress={() => {
                                        updateProfile(profile).then(response => {
                                            Toast.show(response.data.message)
                                        })
                                    }}
                                    style={[
                                        Common.button.outlineRounded,
                                        { borderColor: '#000000' },
                                    ]}
                                >
                                    <View style={[Common.button.shallow]}>
                                        <Text style={[Gutters.largeHPadding, Colors.black]}>
                                            Update
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
        </ScrollView>
    )
}

export default ProfileContainer
