import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import DropDownPicker from 'react-native-dropdown-picker'

import { useTheme } from '@/Hooks'
import { CloseButton } from '@/Components'
import {
    useArchiveMutation,
    useBlockMutation,
    useUpdateStatusMutation,
    useUpdateUrgencyMutation
} from "@/Services/modules/users";

const Filters = ({ filters, setFilters, setShow , victimRequestId}) => {
    const [updateStatus] = useUpdateStatusMutation()
    const [updateUrgency] = useUpdateUrgencyMutation()
    const { Common, Colors, Fonts, Gutters, Layout } = useTheme()
    const [open, setOpen] = React.useState({
        urgency: false,
        status: false,
    })
    const [urgency, setUrgency] = React.useState(
        filters && filters.urgency ? filters.urgency : false,
    )
    const [status, setStatus] = React.useState(
        filters && filters.status ? filters.status : false,
    )

    const getApisStatus = async () => {
        let statusBol = false;
        let urgencyBol = false;
        if(status === "blocked")
        {
         statusBol = await updateStatus({requestId: victimRequestId, status: 'blocked'}).then(() => {
             return true
         })
        }
        else if(status === "archived")
        {
           statusBol = await updateStatus({requestId: victimRequestId, status: 'archived'}).then(() => {
              return true
          });
        }
        else
        {
            //do nothing
        }
        if( urgency === "high")
        {
            urgencyBol =  await updateUrgency({requestId: victimRequestId, priority: 'high'}).then(() => {
               return true
           });
        }
        else if(urgency === "low")
        {
            urgencyBol = await updateUrgency({requestId: victimRequestId, priority: 'low'}).then(() => {
               return true
           });
        }
        else
        {
            //do nothing
        }
        return urgencyBol && statusBol;
    }


    const onApply = async () => {
        let resultBol = await getApisStatus() ;
        if(resultBol)
        {
            setShow(false)
        }
        else
        {
            setShow(true)
        }
    }

    return (
        <View
            style={[Layout.absolute, Layout.inset, { backgroundColor: Colors.chat }]}
        >
            <CloseButton setShow={setShow} style={{ top: 20, right: 10 }} />
            <View style={[Layout.fill, Gutters.regularVPadding]}>
                <View style={[Layout.fill, Gutters.mediumHPadding]}>
                    <View style={[ Gutters.largeVPadding, Gutters.largeVMargin]}>
                        <Text style={[Fonts.textLarge, { color: Colors.black }]}>
                            User Status/Urgency
                        </Text>
                        <View style={[{ width: '10%', borderWidth: 2 }]}></View>
                    </View>

                    <View
                        style={[
                            Layout.scrollSpaceBetween,
                            // Gutters.largeVMargin,
                        ]}
                    >
                        <View style={[Layout.fill, Layout.alignItemsStart]}>
                            <View style={[Layout.column, { width: '100%', zIndex: 1 }]}>
                                <Text
                                    style={[
                                        Fonts.textMedium,
                                        Gutters.mediumBMargin,
                                        { color: Colors.black, fontWeight: '100' },
                                    ]}
                                >
                                    Urgency
                                </Text>
                                <DropDownPicker
                                    open={open.urgency}
                                    value={urgency}
                                    style={[{ borderRadius: 4 }]}
                                    textStyle={[{ fontSize: 14 }]}
                                    items={[
                                        {
                                            label: 'Low',
                                            value: 'low',
                                        },
                                        {
                                            label: 'High',
                                            value: 'high',
                                        },

                                    ]}
                                    setOpen={() => setOpen({ ...open, urgency: !open.urgency })}
                                    // setValue={(val) => setFilters({ ...filters, urgency: val })}
                                    onChangeItem={item => setUrgency(item.value)}
                                    setValue={setUrgency}
                                />
                            </View>

                            <View style={[Layout.column, { width: '100%', zIndex: -1 }]}>
                                <Text
                                    style={[
                                        Fonts.textMedium,
                                        Gutters.largeTMargin,
                                        Gutters.largeTPadding,
                                        Gutters.mediumBMargin,
                                        { color: Colors.black, fontWeight: '100' },
                                    ]}
                                >
                                    Status
                                </Text>
                                <DropDownPicker
                                    open={open.status}
                                    value={status}
                                    style={[{ borderRadius: 4 }]}
                                    textStyle={[{ fontSize: 14 }]}
                                    items={[
                                        { label: 'Blocked', value: 'blocked' },
                                        { label: 'Archived', value: 'archived' },
                                    ]}
                                    setOpen={() => setOpen({ ...open, status: !open.status })}
                                    setValue={setStatus}
                                    onChangeItem={item => setStatus(item.value)}
                                />
                            </View>
                        </View>
                    </View>
                </View>

                <View
                    style={[
                        Layout.center,
                        Gutters.regularVPadding,
                        { borderTopWidth: 1 },
                    ]}
                >
                    <TouchableOpacity
                        style={[
                            Common.button.outlineRounded,
                            {
                                borderColor: Colors.black,
                            },
                        ]}
                        onPress={
                            onApply
                        }
                    >
                        <Text
                            style={[
                                Gutters.regularVPadding,
                                Gutters.largeHPadding,
                                Fonts.textSmall,
                                {
                                    color: Colors.black,
                                },
                            ]}
                        >
                            Apply
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Filters
