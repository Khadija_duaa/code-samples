import React from 'react'
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { useSelector } from 'react-redux'

import { HeaderBar } from '@/Components'
import { Menu } from '@/Navigators'
import { useTheme } from '@/Hooks'
import { goBack } from '@/Navigators/utils'
import {
  useNotesMutation,
  useLazyNotesListQuery,
} from '@/Services/modules/users'
import { Config } from '@/Config'

const NotesContainer = ({ route }) => {
  const { Colors, Common, Layout, Gutters, Fonts, Media } = useTheme()
  const { user } = useSelector(state => state.auth)
  const [show, setShow] = React.useState(false)
  const [messages, setMessages] = React.useState([])
  const [message, setMessage] = React.useState({
    victim_request_id: null,
    note: '',
  })
  const [notes] = useNotesMutation()
  const [notesList] = useLazyNotesListQuery()

  React.useEffect(() => {
    setMessage({
      note: '',
      // victim_request_id: route.params.victimRequestId,
    })
  }, [route.params])

  React.useEffect(() => {
    init()
  }, [message.victim_request_id])

  const init = () =>
    // notesList(victimRequestId).then(response => {
    //   setMessages(response.data.notes)
    // })
    notesList().then(response => {
      setMessages(response.data.staffnotes)

      console.log('Response ::: ', response)
    })

  const handleMessage = () => {
    notes(message)
      .then(() => init())
      .then(() =>
        setMessage({
          // victim_request_id: victimRequestId,
          note: '',
        }),
      )
  }

  const Message = ({ item }) => {
    const userImageUrl = `${Config.API_URL}staff/profile-image/${user.photo}`

    return (
      <View style={[Layout.fill, Gutters.smallMargin, Layout.rowReverse]}>
        <Image
          source={{
            uri: userImageUrl,
          }}
          style={[
            Layout.avatar,
            {
              width: 30,
              height: 30,
              marginHorizontal: 6,
              backgroundColor: Colors.white,
            },
          ]}
        />
        <View
          style={[
            Gutters.smallPadding,
            {
              borderRadius: 8,
              backgroundColor: Colors.chatBubble,
              flexShrink: 1,
            },
          ]}
        >
          <Text style={[]}>{item.note}</Text>
        </View>
      </View>
    )
  }

  return (
    <View style={[Layout.fill, { backgroundColor: Colors.chat }]}>
      {!show ? (
        <HeaderBar
          LeftHeader={() => (
            <TouchableWithoutFeedback onPress={goBack}>
              {Media.entypo('chevron-thin-left', 25, Colors.primary)}
            </TouchableWithoutFeedback>
          )}
          headerColor={Colors.chat}
          setShow={setShow}
          layout={'fullWidth'}
          color={Colors.primary}
        />
      ) : (
        <View
          style={[
            Layout.absolute,
            Layout.inset,
            { backgroundColor: Colors.chat, zIndex: 999 },
          ]}
        >
          <Menu setShow={setShow} backgroundColor={Colors.chat} />
        </View>
      )}

      <KeyboardAvoidingView
        style={[Layout.fill, { backgroundColor: Colors.chat }]}
        // behavior={Platform.select({
        //   android: 'position',
        //   ios: 'position',
        // })}
        // keyboardVerticalOffset={Platform.select({
        //   android: 20,
        // })}
      >
        <View style={[Layout.row, { borderBottomWidth: 1 }]}>
          <View style={[Gutters.regularLMargin]}>
            <Text
              style={[
                Fonts.textLarge,
                {
                  fontWeight: '400',
                  color: Colors.black,
                },
              ]}
            >
              Notes
            </Text>
          </View>
        </View>

        <View style={[Layout.scrollSpaceBetween]}>
          <FlatList
            data={messages}
            keyExtractor={message => message._id}
            renderItem={Message}
            inverted
          />
        </View>

        <View
          style={[
            Layout.center,
            Layout.row,
            Gutters.regularTPadding,
            Gutters.mediumBMargin,
            { borderTopWidth: 1 },
          ]}
        >
          <TouchableOpacity onPress={init} style={[Layout.fill, Layout.center]}>
            {Media.ionicon('refresh-outline', 30, Common.colors.black)}
          </TouchableOpacity>

          <TextInput
            value={message.note}
            placeholder={'New note ...'}
            style={[
              Gutters.regularPadding,
              // Layout.selfStretch,
              {
                flex: 5,
                backgroundColor: Colors.chatLight,
                borderRadius: 30,
              },
            ]}
            onChangeText={str =>
              setMessage({
                // victim_request_id: victimRequestId,
                note: str,
              })
            }
          />

          <TouchableOpacity
            onPress={handleMessage}
            style={[Layout.fill, Layout.center]}
          >
            {Media.ionicon(
              'chevron-forward-circle-outline',
              40,
              Common.colors.black,
            )}
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </View>
  )
}

export default NotesContainer
