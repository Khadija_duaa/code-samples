import { ActivityIndicator, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { useTheme } from '@/Hooks'

const LoadingButton = ({ handlePress, label, isLoading }) => {
  const { Common, Fonts, Gutters, Layout } = useTheme()

  return (
    <TouchableOpacity
      style={[
        Layout.row,
        Layout.justifyContentBetween,
        Common.button.outlineRounded,
        Common.button.shallow,
        Gutters.largeHMargin,
      ]}
      disabled={isLoading}
      onPress={!isLoading && handlePress}
    >
      <Text style={[Gutters.regularVPadding, Fonts.textSmall]}>{label}</Text>
      {isLoading && <ActivityIndicator style={[Gutters.regularLMargin]} />}
    </TouchableOpacity>
  )
}

export default LoadingButton
