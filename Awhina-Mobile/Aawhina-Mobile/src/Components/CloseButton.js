import React from 'react'
import { TouchableWithoutFeedback, View } from 'react-native'
import { useTheme } from '@/Hooks'

const CloseButton = ({ setShow, style = {} }) => {
  const { Common, Gutters, Layout, Media } = useTheme()
  return (
    <View
      style={[Layout.absolute, { top: 30, right: 5, zIndex: 999, ...style }]}
    >
      <TouchableWithoutFeedback
        onPress={() => setShow(false)}
        style={[Gutters.smallPadding]}
      >
        {Media.ionicon('close-outline', 40, Common.colors.primary)}
      </TouchableWithoutFeedback>
    </View>
  )
}

export default CloseButton
