import React from 'react';
import RenderHtml, { defaultSystemFonts } from 'react-native-render-html';
import { useTheme } from '@/Hooks';

const systemFonts = ['Montserrat', ...defaultSystemFonts];

const HtmlViewer = ({ source }) => {
  const { Layout } = useTheme();

  return (
    // <FontLoader>
    <RenderHtml
      contentWidth={Layout.fullWidth.width}
      systemFonts={systemFonts}
      source={{ html: source }}
    />
    // </FontLoader>
  );
};

export default HtmlViewer;
