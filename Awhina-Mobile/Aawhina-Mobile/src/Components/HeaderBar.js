import React from 'react'
import { TouchableWithoutFeedback, View } from 'react-native'
import { useTheme } from '@/Hooks'
import { goBack } from '@/Navigators/utils'

const HeaderBar = ({
  layout,
  LeftHeader,
  showRightHeader = true,
  setShow,
  color,
  headerColor,
}) => {
  const { Colors, Gutters, Layout, Media } = useTheme()

  return (
    <View
      style={[
        layout && Layout[layout],
        Layout.fullWidth,
        Layout.row,
        Layout.justifyContentBetween,
        Gutters.largeTPadding,
        Gutters.smallBPadding,
        Gutters.smallHPadding,
        { zIndex: 99999, backgroundColor: headerColor },
      ]}
    >
      {LeftHeader ? (
        <LeftHeader />
      ) : (
        <TouchableWithoutFeedback onPress={goBack}>
          {Media.entypo(
            'chevron-thin-left',
            25,
            color ? color : Colors.primary,
          )}
        </TouchableWithoutFeedback>
      )}
      {showRightHeader && (
        <View style={[Gutters.smallMargin]}>
          <TouchableWithoutFeedback
            onPress={() => setShow(true)}
            style={[
              Layout.justifyContentBetween,
              Layout.alignItemsEnd,
              { height: 20 },
            ]}
          >
            <View
              style={[
                Layout.justifyContentBetween,
                Layout.alignItemsEnd,
                { height: 20 },
              ]}
            >
              <View
                style={[
                  {
                    borderBottomWidth: 1,
                    borderColor: color ? color : Colors.primary,
                    width: 25,
                    height: 1,
                  },
                ]}
              />
              <View
                style={[
                  {
                    borderBottomWidth: 1,
                    borderColor: color ? color : Colors.primary,
                    width: 25,
                    height: 1,
                  },
                ]}
              />
              <View
                style={[
                  {
                    borderBottomWidth: 1,
                    borderColor: color ? color : Colors.primary,
                    width: 15,
                    height: 1,
                  },
                ]}
              />
            </View>
          </TouchableWithoutFeedback>
        </View>
      )}
    </View>
  )
}

export default HeaderBar
