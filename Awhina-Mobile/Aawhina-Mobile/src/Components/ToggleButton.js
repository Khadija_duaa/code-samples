import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { useTheme } from '@/Hooks';

const ToggleButton = ({ toggle, setToggle }) => {
  const { Media, Common, Gutters } = useTheme();

  return (
    <TouchableOpacity
      onPress={() => setToggle(!toggle)}
      style={[
        // Gutters.regularPadding,
        {
          borderWidth: 1,
          borderColor: Common.colors.white,
          borderRadius: 30,
        },
      ]}
    >
      <View style={[Gutters.regularPadding]}>
        {toggle
          ? Media.ionicon('volume-off', 25)
          : Media.ionicon('volume-low', 25)}
      </View>
    </TouchableOpacity>
  );
};

export default ToggleButton;
