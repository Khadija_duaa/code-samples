import React from 'react';
import PropTypes from 'prop-types';
import {View, Image, Text, Dimensions, Platform} from 'react-native';
import RadialGradient from 'react-native-radial-gradient';

import { useTheme } from '@/Hooks';
import {heightToDp, widthToDp} from "../../Utils/Responsive";

const Brand = ({ title, subtitle = '' }) => {
    const { Fonts, Gutters, Layout, Common } = useTheme();
    const width = Dimensions.get('screen').width;
    const height = width;

    return (
        <View style={[(Layout.relative, Layout.colVCenter)]}>
            <RadialGradient
                style= {[
                    Platform.OS === 'android' && Layout.center ,
                    {
                        width: Platform.select({
                            ios: width, //30 percent of margin is applied to IOS
                            android:'100%',

                        }),
                        height: Platform.select({
                            ios: height,
                            android:'100%',

                        }),
                    },
                ]}
                colors={['rgba(255, 255, 255, 1)', 'rgba(17, 75, 30, 0)']}

                stops={Platform.OS === 'android' ? [widthToDp(0), widthToDp(0.5)] : [0, 0.5]}
                center={Platform.OS === 'android' ?  [widthToDp(37), heightToDp(13)] : [width / 2, height / 2]}
                radius={Platform.OS === 'android' ?   widthToDp(25) : width }
            >
                <View
                    style={[
                        Layout.colCenter,
                        {
                            top:  heightToDp(6),
                            flex: 1
                        },
                    ]}
                >
                    <Text style={[Fonts.titleRegular]}>{title}</Text>
                    <View  >
                        <View style={{color: 'white', borderBottomColor: 'white', borderBottomWidth: 3, width: widthToDp(10), height: heightToDp(3)}}></View>
                    </View>
                    <View  style={{ flex: 4, paddingRight: widthToDp(10), paddingLeft: widthToDp(10), paddingTop: heightToDp(2)}}>
                        <Text
                            style={[[Fonts.textSmall, Fonts.textCenter], { flexShrink: 1, overflow: 'hidden'}]}
                        >
                            {subtitle}
                        </Text>
                    </View>
                </View>
            </RadialGradient>
        </View>
    );
};

Brand.propTypes = {
    height: PropTypes.number,
    mode: PropTypes.oneOf(['contain', 'cover', 'stretch', 'repeat', 'center']),
    width: PropTypes.number,
};

Brand.defaultProps = {
    height: 200,
    mode: 'contain',
    width: 200,
};

export default Brand;
