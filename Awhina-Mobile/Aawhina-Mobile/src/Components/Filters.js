import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import DropDownPicker from 'react-native-dropdown-picker'

import { useTheme } from '@/Hooks'
import { CloseButton } from '@/Components'

const Filters = ({ filters, setFilters, setShow }) => {
  const { Common, Colors, Fonts, Gutters, Layout } = useTheme()
  const [open, setOpen] = React.useState({
    urgency: false,
    status: false,
  })
  const [urgency, setUrgency] = React.useState(
    filters && filters.urgency ? filters.urgency : false,
  )
  const [status, setStatus] = React.useState(
    filters && filters.status ? filters.status : false,
  )

  return (
    <View
      style={[Layout.absolute, Layout.inset, { backgroundColor: Colors.chat }]}
    >
      <CloseButton setShow={setShow} style={{ top: 20, right: 10 }} />
      <View style={[Layout.fill, Gutters.regularVPadding]}>
        <View style={[Layout.fill, Gutters.mediumHPadding]}>
          <View style={[]}>
            <Text style={[Fonts.textMedium, { color: Colors.black }]}>
              Filter
            </Text>
            <View style={[{ width: '10%', borderWidth: 2 }]}></View>
          </View>

          <View
            style={[
              Layout.scrollSpaceBetween,
              // Gutters.largeVMargin,
              Gutters.largeVPadding,
            ]}
          >
            <View style={[Layout.fill, Layout.alignItemsStart]}>
              <View style={[Layout.column, { width: '100%', zIndex: 99999 }]}>
                <Text
                  style={[
                    Fonts.textMedium,
                    Gutters.mediumBMargin,
                    { color: Colors.black, fontWeight: '100' },
                  ]}
                >
                  Urgency
                </Text>
                <DropDownPicker
                  open={open.urgency}
                  value={urgency}
                  style={[{ borderRadius: 4 }]}
                  textStyle={[{ fontSize: 14 }]}
                  items={[
                    {
                      label: 'Low',
                      value: 'low',
                    },
                    {
                      label: 'High',
                      value: 'high',
                    },
                  ]}
                  setOpen={() => setOpen({ ...open, urgency: !open.urgency })}
                  // setValue={(val) => setFilters({ ...filters, urgency: val })}
                  setValue={setUrgency}
                />
              </View>

              <View style={[Layout.column, { width: '100%', zIndex: 99998 }]}>
                <Text
                  style={[
                    Fonts.textMedium,
                    Gutters.largeTMargin,
                    Gutters.largeTPadding,
                    Gutters.mediumBMargin,
                    { color: Colors.black, fontWeight: '100' },
                  ]}
                >
                  Status
                </Text>
                <DropDownPicker
                  open={open.status}
                  value={status}
                  style={[{ borderRadius: 4 }]}
                  textStyle={[{ fontSize: 14 }]}
                  items={[
                    {
                      label: 'Unassigned',
                      value: 'unassigned',
                    },
                    { label: 'In Progress', value: 'in_process' },
                    { label: 'Resolved', value: 'resolved' },
                    { label: 'Blocked', value: 'blocked' },
                    { label: 'Archived', value: 'archived' },
                  ]}
                  setOpen={() => setOpen({ ...open, status: !open.status })}
                  setValue={setStatus}
                />
              </View>
            </View>
          </View>
        </View>

        <View
          style={[
            Layout.center,
            Gutters.regularVPadding,
            { borderTopWidth: 1, zIndex: -1 },
          ]}
        >
          <TouchableOpacity
            style={[
              Common.button.outlineRounded,
              {
                borderColor: Colors.black,
              },
            ]}
            onPress={() => {
              setFilters({ ...filters, urgency, status })
              setShow(false)
            }}
          >
            <Text
              style={[
                Gutters.regularVPadding,
                Gutters.largeHPadding,
                Fonts.textSmall,
                {
                  color: Colors.black,
                },
              ]}
            >
              Apply
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

export default Filters
