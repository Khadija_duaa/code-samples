import React from 'react'
import PropTypes from 'prop-types'
import { View, Image, Text, Dimensions } from 'react-native'
import RadialGradient from 'react-native-radial-gradient'

import { useTheme } from '@/Hooks'

const Brand = ({ title, subtitle = '' }) => {
  const { Fonts, Gutters, Layout, Common } = useTheme()
  const width = Dimensions.get('screen').width - 30
  const height = width

  return (
    <View style={[]}>
      <RadialGradient
        style={[
          Layout.absolute,
          {
            width,
            height: height,
            top: '-50%',
          },
        ]}
        colors={['rgba(255, 255, 255, 1)', 'rgba(17, 75, 30, 0)']}
        stops={[0, 0.5]}
        center={[width / 2, height / 2]}
        radius={width}
      ></RadialGradient>
      <View style={[Layout.colCenter]}>
        <Text style={[Fonts.titleRegular]}>{title}</Text>
        <View
          style={[Layout.fullWidth, Gutters.largeVPadding, { width: '20%' }]}
        >
          <View style={[Common.border2, { borderColor: '#ffffff' }]}></View>
        </View>
        <View style={[Gutters.largeHMargin]}>
          <Text
            style={[Fonts.textSmall, Fonts.textCenter, Gutters.smallHMargin]}
          >
            {subtitle}
          </Text>
        </View>
      </View>
    </View>
  )
}

Brand.propTypes = {
  height: PropTypes.number,
  mode: PropTypes.oneOf(['contain', 'cover', 'stretch', 'repeat', 'center']),
  width: PropTypes.number,
}

Brand.defaultProps = {
  height: 200,
  mode: 'contain',
  width: 200,
}

export default Brand
