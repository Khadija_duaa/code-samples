import React from 'react'
import { View } from 'react-native'
import RNSwiper from 'react-native-swiper'
import { useTheme } from '@/Hooks'

const Swiper = ({
  children,
  onIndexChanged = () => {},
  color = '#ffffff',
  style = {},
  top = -500,
}) => {
  const { Layout, Gutters } = useTheme()

  return (
    <RNSwiper
      style={[style]}
      dotColor={color}
      paginationStyle={[Layout.absolute, { top }]}
      activeDotColor={color}
      onIndexChanged={index => onIndexChanged(index)}
      loop={false}
      dot={
        <View
          style={[
            {
              borderWidth: 1,
              borderColor: color,
              width: 8,
              height: 8,
              borderRadius: 4,
              marginLeft: 3,
              marginRight: 3,
              marginTop: 3,
              marginBottom: 3,
            },
          ]}
        />
      }
    >
      {children}
    </RNSwiper>
  )
}

export default Swiper
