import React from 'react'
import { View, TextInput } from 'react-native'
import { useTheme } from '@/Hooks'

const CharField = ({ value }) => {
  const { Colors, Fonts, Gutters, Layout } = useTheme()
  const minLength = ['', '', '', '']

  return (
    <View style={[Layout.row, Layout.justifyContentCenter]}>
      {minLength.map((key, index) => (
        <TextInput
          secureTextEntry={true}
          editable={false}
          selectTextOnFocus={false}
          value={value[index] && value[index]}
          key={`char-${index}`}
          style={[
            Gutters.regularVPadding,
            Gutters.smallHMargin,
            Fonts.textCenter,
            Fonts.extraLargeText,
            {
              color: Colors.white,
              width: Layout.fullWidth.width / 8,
              borderBottomColor: Colors.white,
              borderBottomWidth: 2,
            },
          ]}
        />
      ))}
    </View>
  )
}

export default CharField
