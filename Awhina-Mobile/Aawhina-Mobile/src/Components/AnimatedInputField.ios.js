import React from 'react'
import AnimatedInput from 'react-native-animated-input'
import { TextInput } from 'react-native'
import { useTheme } from '@/Hooks'

const AnimatedInputField = props => {
  const { Common, Layout, Gutters, Fonts, Media } = useTheme()
  const {
    placeholder,
    errorText,
    value,
    valid,
    onChangeText,
    keyboardType = 'default',
    style,
  } = props

  return (
    <AnimatedInput
      {...props}
      placeholder={placeholder}
      valid={valid}
      errorText={errorText}
      onChangeText={onChangeText}
      value={value}
      styleLabel={[{ color: style ? style.color : '#ffffff' }]}
      autoCorrect={false}
      autoCapitalize="none"
      keyboardType={keyboardType}
      styleBodyContent={{
        borderBottomWidth: 1,
        borderBottomColor: style ? style.color : '#ffffff',
      }}
      styleInput={[{ color: style ? style.color : '#ffffff' }]}
    />
  )
}

export default AnimatedInputField
