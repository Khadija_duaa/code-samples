import React from 'react';
import { Image, View, Dimensions } from 'react-native';

import { useTheme } from '@/Hooks';

const BrandSplash = () => {
  const { Fonts, Gutters, Layout, Media } = useTheme();

  return <Image style={Layout.fullWidth} source={Media.splashLogo} />;
};

export default BrandSplash;
