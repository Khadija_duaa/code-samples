import React from 'react';
import AnimatedInput from 'react-native-animated-input';
import { TextInput } from 'react-native';
import { useTheme } from '@/Hooks';
import {heightToDp} from "../../Utils/Responsive";

const AnimatedInputField = props => {
    const { Colors, Layout, Gutters, Fonts, Media } = useTheme()
    const {
        placeholder,
        errorText,
        value,
        valid,
        onChangeText,
        keyboardType = 'default',
    } = props

    return (
        // <TextInput
        //   value={value}
        //   onChangeText={onChangeText}
        //   style={[Layout.fullWidth]}
        // />
        <AnimatedInput
            {...props}
            placeholder={placeholder}
            valid={valid}
            errorText={errorText}
            onChangeText={onChangeText}
            value={value}
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType={keyboardType}
            styleLabel={[{color: Colors.white, marginBottom: 1}]}
            styleBodyContent={{
                borderBottomColor: 'transparent',
            }}
            styleInput={[{color: Colors.white, height: heightToDp(10),paddingBottom: heightToDp(4)}]}
        />
    )
}

export default AnimatedInputField;
