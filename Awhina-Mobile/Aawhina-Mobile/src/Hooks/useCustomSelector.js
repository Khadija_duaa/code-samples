import { useMemo } from 'react'
import { useSelector } from 'react-redux'

export default function (selector) {
  const keys = useSelector(state => state[selector])

  return () => keys, [keys]
}
