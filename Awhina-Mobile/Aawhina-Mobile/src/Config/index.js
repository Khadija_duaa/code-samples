export const Config = {
  APP_NAME: 'AAWHINA',
  APP_TEXT:
    'lorem ipsum dlore site amet, lorem ipsum dlore site amet, lorem ipsum dlore site amet, lorem ipsum dlore site amet, lorem ipsum dlore site amet',
  // API_URL: 'http://localhost:8888/',
  API_URL: 'https://aawhinaapi-stg.azurewebsites.net/',
  WS_URL: 'ws://localhost:8080',
  REFRESH_INTERVAL: 3000,
}
