﻿
//admin = [];

var _defaultFont = "";
$.ajaxSetup({
    statusCode: {
        401: function () {
            $("#session-alert").modal('show');
        }
    }
});
function changeMenu(t)
{
    var a = $("#selected-client-id").val();
    var b = $("#selected-client-uid").val();
    var c = $("#selected-visa-id").val();
    var d = $("#selected-client-agent-id").val();
    var e = $("#selected-client-name").html();
    var f = $("#selected-client-ezm").html();
    var g = $("#selected-client-inz").html();
    var h = $("#selected-client-type").html();
    var i = $("#selected-client-dob").html();
    var j = $("#selected-client-uploaded").html();
    if (t) {
        blockUI();
        $.ajax({
            type: "GET",
            contentType: "html",
            url: '/Home/LoadMenuS',
            async: true,
            success: function (data) {
                unblockUI();
                $("#main-menu").replaceWith(data);
                $("#content-area").removeClass("col-lg-9").removeClass("col-md-9").addClass("col-lg-11").addClass("col-md-11");
                $("#selected-client-id").val(a);
                $("#selected-client-uid").val(b);
                $("#selected-visa-id").val(c);
                $("#selected-client-agent-id").val(d);
                $("#selected-client-name").html(e);
                $("#selected-client-ezm").html(f);
                $("#selected-client-inz").html(g);
                $("#selected-client-type").html(h);
                $("#selected-client-dob").html(i);
                $("#selected-client-uploaded").html(j);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                handleErrors(textStatus);
            }
        });
    }
    else
    {
        blockUI();
        $.ajax({
            type: "GET",
            contentType: "html",
            url: '/Home/LoadMenu',
            async: true,
            success: function (data) {
                unblockUI();
                $("#main-menu").replaceWith(data);
                $("#selected-client-id").val(a);
                $("#selected-client-uid").val(b);
                $("#selected-visa-id").val(c);
                $("#selected-client-agent-id").val(d);
                $("#selected-client-name").html(e);
                $("#selected-client-ezm").html(f);
                $("#selected-client-inz").html(g);
                $("#selected-client-type").html(h);
                $("#selected-client-dob").html(i);
                $("#selected-client-uploaded").html(j);
                $("#content-area").removeClass("col-lg-11").removeClass("col-md-11").addClass("col-lg-9").addClass("col-md-9");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                handleErrors(textStatus);
            }
        });
    }

}
function blockUI() {
    $(".overlay").show()
}
function unblockUI() {
    $(".overlay").hide();
}
function sessionExpired(ref)
{
    window.location = "../Home";

    $(ref).modal('hide');
}
function showComingSoon() {
    return $().toastmessage('showToast', {
        text: ScriptResourcesList['scriptComingSoon'],
        sticky: false,
        position: 'bottom-right',
        type: 'warning'

    });
}
function showUpdateMessage() {
    return $().toastmessage('showToast', {
        text: ScriptResourcesList['scriptWeareupdatingforms'],
        sticky: false,
        position: 'bottom-right',
        type: 'warning'

    });
}
function underDevelopement()
{
    return $().toastmessage('showToast', {
        text: ScriptResourcesList['scriptUnderMaintenance'],
        sticky: false,
        position: 'bottom-right',
        type: 'warning'

    });
}
function successToast()
{
    return $().toastmessage('showToast', {
        text: ScriptResourcesList['scriptUpdated'],
        sticky: false,
        position: 'bottom-right',
        type: 'success'

    });
}
function showErrorToast(text) {
    return $().toastmessage('showToast', {
        text: text,
        sticky: false,
        position: 'bottom-right',
        type: 'error'

    });
}
function showSuccessToast(text) {
    return $().toastmessage('showToast', {
        text: text,
        sticky: false,
        position: 'bottom-right',
        type: 'success'

    });
}
function showWarningToast(text) {
    return $().toastmessage('showToast', {
        text: text,
        sticky: false,
        position: 'bottom-right',
        type: 'warning'

    });
}
function errorToast() {
    return $().toastmessage('showToast', {
        text: ScriptResourcesList['scriptFailed'],
        sticky: false,
        position: 'bottom-right',
        type: 'error'

    });
}



function handleErrors(status) {
    unblockUI();
    if (status === "401") {
        if (window.confirm(ScriptResourcesList['scriptSessionExpired'])) {
            return false;
        }
        
    }
    else if (status === "502") {
        return showWarningToast("Your syncing in progress please don't close the browser, you will get an email notification when the syncing is complete");
    }
    else {
        $(".btn").prop("disabled", false);
        return $().toastmessage('showToast', {
            text: ScriptResourcesList['scriptFailed'],
            sticky: false,
            position: 'bottom-right',
            type: 'error'

        });
    }
}