import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getFilenotesRequest: ["data"],
  getFilenotesSuccess: ["success"],
  getFilenotesFailure: ["error"],

  addFilenotesRequest: ["data"],
  addFilenotesSuccess: ["addfnSuccess"],
  addFilenotesFailure: ["addfnError"],

  delFilenotesRequest: ["data"],
  delFilenotesSuccess: ["delfnSuccess"],
  delFilenotesFailure: ["delfnError"]
});

export const FilenotesTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  filenotesData: null,
  fetching: false,
  error: null,
  addfnFetching: false,
  addfnSuccess: null,
  addfnError: null,
  delfnFetching: false,
  delfnError: null
});

/* ------------- Reducers ------------- */

// we're attempting to get Client file notes info data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get Client file notes info data
export const success = (state, { success }) =>
  state.merge({ fetching: false, filenotesData: success });

// we've had a problem in geting Client file notes info data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to add cleint file notes info data
export const addrequest = state => {
  console.log("on redux call");
  return state.merge({ addfnFetching: true });
};

// we've successfully add cleint file notes info data
export const addsuccess = (state, { addSuccess }) =>
  state.merge({ addfnFetching: false, addfnSuccess: addSuccess });

// we've had a problem in updating Client Documents info data
export const addfailure = (state, { addfnError }) =>
  state.merge({ addfnFetching: false, addfnError });

// we're attempting to delete Client file notes info data
export const delrequest = state => {
  console.log("on redux call");
  return state.merge({ delfnFetching: true });
};

// we've successfully delete Client file notes info data
export const delsuccess = (state, { deleteSuccess }) =>
  state.merge({ delDocFetching: false, delfnSuccess: deleteSuccess });

// we've had a problem in deleting Client file notes info data
export const delfailure = (state, { delfnError }) =>
  state.merge({ delfnfFetching: false, delfnError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_FILENOTES_REQUEST]: request,
  [Types.GET_FILENOTES_SUCCESS]: success,
  [Types.GET_FILENOTES_FAILURE]: failure,

  [Types.ADD_FILENOTES_REQUEST]: addrequest,
  [Types.ADD_FILENOTES_SUCCESS]: addsuccess,
  [Types.ADD_FILENOTES_FAILURE]: addfailure,

  [Types.DEL_FILENOTES_REQUEST]: delrequest,
  [Types.DEL_FILENOTES_SUCCESS]: delsuccess,
  [Types.DEL_FILENOTES_FAILURE]: delfailure
});
