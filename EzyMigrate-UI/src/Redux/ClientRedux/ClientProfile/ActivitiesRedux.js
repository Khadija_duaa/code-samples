import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getActivitiesRequest: ["data"],
  getActivitiesSuccess: ["success"],
  getActivitiesFailure: ["error"]
});

export const ActivitiesTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  activitiesData: null,
  fetching: false,
  error: null
});

/* ------------- Reducers ------------- */

// we're attempting to get Client Documents info data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get Client Documents info data
export const success = (state, { success }) =>
  state.merge({ fetching: false, activitiesData: success });

// we've had a problem in geting Client Documents info data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_ACTIVITIES_REQUEST]: request,
  [Types.GET_ACTIVITIES_SUCCESS]: success,
  [Types.GET_ACTIVITIES_FAILURE]: failure
});
