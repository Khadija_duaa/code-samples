import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getVisaInfoRequest: ["data"],
  getVisaInfoSuccess: ["success"],
  getVisaInfoFailure: ["error"],

  addVisaInfoRequest: ["data"],
  addVisaInfoSuccess: ["addSuccess"],
  addVisaInfoFailure: ["addError"],

  delVisaInfoRequest: ["data"],
  delVisaInfoSuccess: ["deleteSuccess"],
  delVisaInfoFailure: ["delVisaError"]
});

export const VisaInfoTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  visaInfoData: null,
  fetching: false,
  error: null,
  addVisaFetching: false,
  addSuccess: null,
  delVisaFetching: false,
  delVisaError: null
});

/* ------------- Reducers ------------- */

// we're attempting to get visa info data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get visa info data
export const success = (state, { success }) =>
  state.merge({ fetching: false, visaInfoData: success });

// we've had a problem in geting visa info data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to add visa info data
export const addrequest = state => {
  console.log("on redux call");
  return state.merge({ addVisaFetching: true });
};

// we've successfully add visa info data
export const addsuccess = (state, { addSuccess }) =>
  state.merge({ addVisaFetching: false, addSuccess: addSuccess });

// we've had a problem in adding visa info data
export const addfailure = (state, { addError }) =>
  state.merge({ addVisaFetching: false, addError });

// we're attempting to delete visa info data
export const delrequest = state => {
  console.log("on redux call");
  return state.merge({ delVisaFetching: true });
};

// we've successfully delete visa info data
export const delsuccess = (state, { deleteSuccess }) =>
  state.merge({ delVisaFetching: false, deleteSuccess: deleteSuccess });

// we've had a problem in deleting visa info data
export const delfailure = (state, { delVisaError }) =>
  state.merge({ delVisaFetching: false, delVisaError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_VISA_INFO_REQUEST]: request,
  [Types.GET_VISA_INFO_SUCCESS]: success,
  [Types.GET_VISA_INFO_FAILURE]: failure,

  [Types.ADD_VISA_INFO_REQUEST]: addrequest,
  [Types.ADD_VISA_INFO_SUCCESS]: addsuccess,
  [Types.ADD_VISA_INFO_FAILURE]: addfailure,

  [Types.DEL_VISA_INFO_REQUEST]: delrequest,
  [Types.DEL_VISA_INFO_SUCCESS]: delsuccess,
  [Types.DEL_VISA_INFO_FAILURE]: delfailure
});
