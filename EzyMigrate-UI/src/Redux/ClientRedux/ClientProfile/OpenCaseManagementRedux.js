import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getOcmRequest: [],
  getOcmSuccess: ["success"],
  getOcmFailure: ["error"]
});

export const OpenCaseManagementTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  onSuccess: null,
  fetching: false,
  error: null
});

/* ------------- Reducers ------------- */

// we're attempting to get open case management data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get open case management data
export const success = (state, { success }) =>
  state.merge({ fetching: false, onSuccess: success });

// we've had a problem getting open case management data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_OCM_REQUEST]: request,
  [Types.GET_OCM_SUCCESS]: success,
  [Types.GET_OCM_FAILURE]: failure
});
