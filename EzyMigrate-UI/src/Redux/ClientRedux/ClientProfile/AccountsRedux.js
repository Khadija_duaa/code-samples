import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getClAccountRequest: ["data"],
  getClAccountSuccess: ["success"],
  getClAccountFailure: ["error"],

  addAccInvoiceRequest: ["data"],
  addAccInvoiceSuccess: ["addfnSuccess"],
  addAccInvoiceFailure: ["addfnError"],

  delAccInvoiceRequest: ["data"],
  delAccInvoiceSuccess: ["delfnSuccess"],
  delAccInvoiceFailure: ["delfnError"],

  addAccSupInvoiceRequest: ["data"],
  addAccSupInvoiceSuccess: ["addsupinvSuccess"],
  addAccSupInvoiceFailure: ["addsupinvError"],

  delAccSupInvoiceRequest: ["data"],
  delAccSupInvoiceSuccess: ["delsupinvSuccess"],
  delAccSupInvoiceFailure: ["delfnError"],

  addAccOPRequest: ["data"],
  addAccOPSuccess: ["addopSuccess"],
  addAccOPFailure: ["addopError"],

  delAccOPRequest: ["data"],
  delAccOPSuccess: ["delopSuccess"],
  delAccOPFailure: ["delopError"],

  updAccOPRequest: ["data"],
  updAccOPSuccess: ["updopSuccess"],
  updAccOPFailure: ["updopError"],

  addAccComRequest: ["data"],
  addAccComSuccess: ["addComSuccess"],
  addAccComPFailure: ["addComError"]
});

export const ClAccountTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  clAccountData: null,
  fetching: false,
  error: null,
  addAccInvFetching: false,
  addAccInvSuccess: null,
  addAccInvError: null,
  delAccInvFetching: false,
  delAccInvError: null,
  addAccSupInvFetching: false,
  addAccSupInvSuccess: null,
  addAccSupInvFailure: null,
  delAccSupInvFetching: false,
  delAccSupInvSuccess: null,
  delAccSupInvFailure: null,
  addOPFetching: false,
  addOPSuccess: null,
  addOPFailure: null,
  updOPFetching: false,
  updOPSuccess: null,
  updOPError: null,
  delOPFetching: false,
  delOPSuccess: null,
  delOPFailure: null,
  addComFetching: false,
  addComSuccess: null,
  addComFailure: null
});

/* ------------- Reducers ------------- */

// we're attempting to get Client account info data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get Client account info data
export const success = (state, { success }) =>
  state.merge({ fetching: false, clAccountData: success });

// we've had a problem in geting Client account info data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to add cleint account info data
export const addrequest = state => {
  console.log("on redux call");
  return state.merge({ addAccInvFetching: true });
};

// we've successfully add cleint account info data
export const addsuccess = (state, { addSuccess }) =>
  state.merge({ addAccInvFetching: false, addAccInvSuccess: addSuccess });

// we've had a problem in updating Client account info data
export const addfailure = (state, { addAccInvError }) =>
  state.merge({ addAccInvFetching: false, addAccInvError });

// we're attempting to delete Client account info data
export const delrequest = state => {
  console.log("on redux call");
  return state.merge({ delAccInvFetching: true });
};

// we've successfully delete Client account info data
export const delsuccess = (state, { deleteSuccess }) =>
  state.merge({ delDocFetching: false, delAccInvSuccess: deleteSuccess });

// we've had a problem in deleting Client account info data
export const delfailure = (state, { delAccInvError }) =>
  state.merge({ delAccInvfFetching: false, delAccInvError });

// we're attempting to add cleint account info data
export const addsupinvrequest = state => {
  console.log("on redux call");
  return state.merge({ addAccSupInvFetching: true });
};

// we've successfully add cleint account info data
export const addsupinvsuccess = (state, { addSuccess }) =>
  state.merge({ addAccSupInvFetching: false, addAccSupInvSuccess: addSuccess });

// we've had a problem in updating Client account info data
export const addsupinvfailure = (state, { addAccSupInvError }) =>
  state.merge({ addAccSupInvFetching: false, addAccSupInvError });

// we're attempting to delete Client account info data
export const delsupinvrequest = state => {
  console.log("on redux call");
  return state.merge({ delAccInvFetching: true });
};

// we've successfully delete Client account info data
export const delsupinvsuccess = (state, { deleteSuccess }) =>
  state.merge({ delOPFetching: false, delAccSupInvSuccess: deleteSuccess });

// we've had a problem in deleting Client account info data
export const delsupinvfailure = (state, { delAccSupInvError }) =>
  state.merge({ delAccSupInvfFetching: false, delAccSupInvError });

// we're attempting to add cleint account info data
export const addoprequest = state => {
  console.log("on redux call");
  return state.merge({ addOPFetching: true });
};

// we've successfully add cleint account info data
export const addopsuccess = (state, { addSuccess }) =>
  state.merge({ addOPFetching: false, addOPSuccess: addSuccess });

// we've had a problem in adding Client account info data
export const addopfailure = (state, { addOPFailure }) =>
  state.merge({ addOPFetching: false, addOPFailure });

// we're attempting to delete Client account outgoing payment info data
export const deloprequest = state => {
  console.log("on redux call");
  return state.merge({ delOPFetching: true });
};

// we've successfully delete Client account outgoing payment info data
export const delopsuccess = (state, { deleteSuccess }) =>
  state.merge({ delOPFetching: false, delOPSuccess: deleteSuccess });

// we've had a problem in deleting Client account outgoing payment info data
export const delopfailure = (state, { delopError }) =>
  state.merge({ delOPFetching: false, delopError });

// we're attempting to update Client account outgoing payment info data
export const updoprequest = state => {
  console.log("on redux call");
  return state.merge({ updOPFetching: true });
};

// we've successfully update Client account outgoing payment info data
export const updopsuccess = (state, { deleteSuccess }) =>
  state.merge({ updOPFetching: false, updOPSuccess: deleteSuccess });

// we've had a problem in updating Client account outgoing payment info data
export const updopfailure = (state, { updOPError }) =>
  state.merge({ updOPFetching: false, updOPError });

// we're attempting to update Client account agent commission info data
export const addcomrequest = state => {
  console.log("on redux call");
  return state.merge({ addComFailure: true });
};

// we've successfully update Client account agent commission info data
export const addcomsuccess = (state, { addComSuccess }) =>
  state.merge({ addComFetching: false, addComSuccess: addComSuccess });

// we've had a problem in updating Client account agent commission info data
export const addcomfailure = (state, { addComError }) =>
  state.merge({ addComFetching: false, addComError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_CL_ACCOUNT_REQUEST]: request,
  [Types.GET_CL_ACCOUNT_SUCCESS]: success,
  [Types.GET_CL_ACCOUNT_FAILURE]: failure,

  [Types.ADD_ACC_INVOICE_REQUEST]: addrequest,
  [Types.ADD_ACC_INVOICE_SUCCESS]: addsuccess,
  [Types.ADD_ACC_INVOICE_FAILURE]: addfailure,

  [Types.DEL_ACC_INVOICE_REQUEST]: delrequest,
  [Types.DEL_ACC_INVOICE_SUCCESS]: delsuccess,
  [Types.DEL_ACC_INVOICE_FAILURE]: delfailure,

  [Types.ADD_ACC_SUP_INVOICE_REQUEST]: addsupinvrequest,
  [Types.ADD_ACC_SUP_INVOICE_SUCCESS]: addsupinvsuccess,
  [Types.ADD_ACC_SUP_INVOICE_FAILURE]: addsupinvfailure,

  [Types.DEL_ACC_SUP_INVOICE_REQUEST]: delsupinvrequest,
  [Types.DEL_ACC_SUP_INVOICE_SUCCESS]: delsupinvsuccess,
  [Types.DEL_ACC_SUP_INVOICE_FAILURE]: delsupinvfailure,

  [Types.ADD_ACC_O_P_REQUEST]: addoprequest,
  [Types.ADD_ACC_O_P_SUCCESS]: addopsuccess,
  [Types.ADD_ACC_O_P_FAILURE]: addopfailure,

  [Types.DEL_ACC_O_P_REQUEST]: deloprequest,
  [Types.DEL_ACC_O_P_SUCCESS]: delopsuccess,
  [Types.DEL_ACC_O_P_FAILURE]: delopfailure,

  [Types.UPD_ACC_O_P_REQUEST]: updoprequest,
  [Types.UPD_ACC_O_P_SUCCESS]: updopsuccess,
  [Types.UPD_ACC_O_P_FAILURE]: updopfailure,

  [Types.ADD_ACC_COM_REQUEST]: addcomrequest,
  [Types.ADD_ACC_COM_SUCCESS]: addcomsuccess,
  [Types.ADD_ACC_COM_FAILURE]: addcomfailure
});
