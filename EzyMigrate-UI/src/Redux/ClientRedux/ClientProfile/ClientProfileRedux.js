import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getClientProfileRequest: ["data"],
  getClientProfileSuccess: ["success"],
  getClientProfileFailure: ["error"],

  updClientInfoRequest: ["data"],
  updClientInfoSuccess: ["updSuccess"],
  updClientInfoFailure: ["updError"]
});

export const ClientProfileTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  clientProfileData: null,
  fetching: false,
  error: null,
  updFetching: false,
  updSuccess: null
});

/* ------------- Reducers ------------- */

// we're attempting to get client profile data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get client profile data
export const success = (state, { success }) =>
  state.merge({ fetching: false, clientProfileData: success });

// we've had a problem in geting client profile data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to update client profile data
export const updrequest = state => {
  console.log("on redux call");
  return state.merge({ updFetching: true });
};

// we've successfully update client profile data
export const updsuccess = (state, { updSuccess }) =>
  state.merge({ updFetching: false, updSuccess: updSuccess });

// we've had a problem in updating client profile data
export const updfailure = (state, { updError }) =>
  state.merge({ updFetching: false, updError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_CLIENT_PROFILE_REQUEST]: request,
  [Types.GET_CLIENT_PROFILE_SUCCESS]: success,
  [Types.GET_CLIENT_PROFILE_FAILURE]: failure,

  [Types.UPD_CLIENT_INFO_REQUEST]: updrequest,
  [Types.UPD_CLIENT_INFO_SUCCESS]: updsuccess,
  [Types.UPD_CLIENT_INFO_FAILURE]: updfailure
});
