import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getAdInfoRequest: ["data"],
  getAdInfoSuccess: ["success"],
  getAdInfoFailure: ["error"],

  updAdInfoRequest: ["data"],
  updAdInfoSuccess: ["updAdSuccess"],
  updAdInfoFailure: ["updAdError"],

  delAdInfoRequest: ["data"],
  delAdInfoSuccess: ["delAdSuccess"],
  delAdInfoFailure: ["delAdError"]
});

export const AdmissionTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  admissionData: null,
  fetching: false,
  error: null,
  updAdFetching: false,
  updAdSuccess: null,
  delAdFetching: false,
  delAdError: null
});

/* ------------- Reducers ------------- */

// we're attempting to get admissoin info data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get admissoin info data
export const success = (state, { success }) =>
  state.merge({ fetching: false, admissionData: success });

// we've had a problem in geting admissoin info data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to update admissoin info data
export const updrequest = state => {
  console.log("on redux call");
  return state.merge({ updAdFetching: true });
};

// we've successfully update admissoin info data
export const updsuccess = (state, { addSuccess }) =>
  state.merge({ updAdFetching: false, updAdSuccess: addSuccess });

// we've had a problem in updating admissoin info data
export const updfailure = (state, { addError }) =>
  state.merge({ updAdFetching: false, addError });

// we're attempting to delete admission info data
export const delrequest = state => {
  console.log("on redux call");
  return state.merge({ delAdFetching: true });
};

// we've successfully delete admission info data
export const delsuccess = (state, { deleteSuccess }) =>
  state.merge({ delAdFetching: false, delAdSuccess: deleteSuccess });

// we've had a problem in deleting admission info data
export const delfailure = (state, { delAdError }) =>
  state.merge({ delAdFetching: false, delAdError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_AD_INFO_REQUEST]: request,
  [Types.GET_AD_INFO_SUCCESS]: success,
  [Types.GET_AD_INFO_FAILURE]: failure,

  [Types.UPD_AD_INFO_REQUEST]: updrequest,
  [Types.UPD_AD_INFO_SUCCESS]: updsuccess,
  [Types.UPD_AD_INFO_FAILURE]: updfailure,

  [Types.DEL_AD_INFO_REQUEST]: delrequest,
  [Types.DEL_AD_INFO_SUCCESS]: delsuccess,
  [Types.DEL_AD_INFO_FAILURE]: delfailure
});
