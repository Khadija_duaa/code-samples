import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getDocInfoRequest: ["data"],
  getDocInfoSuccess: ["success"],
  getDocInfoFailure: ["error"],

  updDocInfoRequest: ["data"],
  updDocInfoSuccess: ["updDocSuccess"],
  updDocInfoFailure: ["updDocError"],

  delDocInfoRequest: ["data"],
  delDocInfoSuccess: ["delDocSuccess"],
  delDocInfoFailure: ["delDocError"]
});

export const ClDocumentTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  clDocData: null,
  fetching: false,
  error: null,
  updDocFetching: false,
  updDocSuccess: null,
  delDocFetching: false,
  delDocError: null,
  updDocSuccess: null,
  updDocError: null
});

/* ------------- Reducers ------------- */

// we're attempting to get Client Documents info data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get Client Documents info data
export const success = (state, { success }) =>
  state.merge({ fetching: false, clDocData: success });

// we've had a problem in geting Client Documents info data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to update Client Documents info data
export const updrequest = state => {
  console.log("on redux call");
  return state.merge({ updDocFetching: true });
};

// we've successfully update Client Documents info data
export const updsuccess = (state, { addSuccess }) =>
  state.merge({ updDocFetching: false, updDocSuccess: addSuccess });

// we've had a problem in updating Client Documents info data
export const updfailure = (state, { updDocError }) =>
  state.merge({ updDocFetching: false, updDocError });

// we're attempting to delete Client Documents info data
export const delrequest = state => {
  console.log("on redux call");
  return state.merge({ delDocFetching: true });
};

// we've successfully delete Client Documents info data
export const delsuccess = (state, { deleteSuccess }) =>
  state.merge({ delDocFetching: false, delDocSuccess: deleteSuccess });

// we've had a problem in deleting Client Documents info data
export const delfailure = (state, { delDocError }) =>
  state.merge({ delDocFetching: false, delDocError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_DOC_INFO_REQUEST]: request,
  [Types.GET_DOC_INFO_SUCCESS]: success,
  [Types.GET_DOC_INFO_FAILURE]: failure,

  [Types.UPD_DOC_INFO_REQUEST]: updrequest,
  [Types.UPD_DOC_INFO_SUCCESS]: updsuccess,
  [Types.UPD_DOC_INFO_FAILURE]: updfailure,

  [Types.DEL_DOC_INFO_REQUEST]: delrequest,
  [Types.DEL_DOC_INFO_SUCCESS]: delsuccess,
  [Types.DEL_DOC_INFO_FAILURE]: delfailure
});
