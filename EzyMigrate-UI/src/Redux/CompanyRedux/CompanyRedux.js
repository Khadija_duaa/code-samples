import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getCompanyRequest: ["id"],
  getCompanySuccess: ["success"],
  getCompanyFailure: ["error"],

  addCompanyRequest: ["data", "token"],
  addCompanySuccess: ["addsuccess"],
  addCompanyFailure: ["adderror"],

  updEmployerRequest: ["data"],
  updEmployerSuccess: ["updsuccess"],
  updEmployerFailure: ["upderror"],

  dltEmployerRequest: ["data"],
  dltEmployerSuccess: ["dltsuccess"],
  dltEmployerFailure: ["dlterror"]
});

export const CompanyTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  companyData: null,
  fetching: false,
  error: null,
  addFetching: false,
  addSuccess: null,
  addError: null,
  updFetching: false,
  updSuccess: null,
  updError: null,
  dltFetching: false,
  dltSuccess: null,
  dltFailure: null
});

/* ------------- Reducers ------------- */

// we're attempting to get company data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get company data
export const success = (state, { success }) =>
  state.merge({ fetching: false, companyData: success });

// we've had a problem in company data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to add company data
export const addrequest = state => {
  console.log("on redux call");
  return state.merge({ addFetching: true });
};

// we've successfully added company data
export const addsuccess = (state, { addSuccess }) =>
  state.merge({ addFetching: false, addSuccess: addSuccess });

// we've had a problem in adding company data
export const addfailure = (state, { addError }) =>
  state.merge({ addFetching: false, addError });

// we're attempting to update company data
export const updrequest = state => {
  console.log("on redux call");
  return state.merge({ updFetching: true });
};

// we've successfully updated company data
export const updsuccess = (state, { updSuccess }) =>
  state.merge({ updFetching: false, updSuccess: updSuccess });

// we've had a problem updating company data
export const updfailure = (state, { updError }) =>
  state.merge({ updFetching: false, updError });

// we're attempting to delete company data
export const dltrequest = state => {
  console.log("on redux call");
  return state.merge({ dltFetching: true });
};

// we've successfully deleted company data
export const dltsuccess = (state, { dltSuccess }) =>
  state.merge({ dltFetching: false, dltSuccess: dltSuccess });

// we've had a problem deleting company data
export const dltfailure = (state, { dltError }) =>
  state.merge({ dltFetching: false, dltError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_COMPANY_REQUEST]: request,
  [Types.GET_COMPANY_SUCCESS]: success,
  [Types.GET_COMPANY_FAILURE]: failure,

  [Types.ADD_COMPANY_REQUEST]: addrequest,
  [Types.ADD_COMPANY_SUCCESS]: addsuccess,
  [Types.ADD_COMPANY_FAILURE]: addfailure,

  [Types.UPD_COMPANY_REQUEST]: updrequest,
  [Types.UPD_COMPANY_SUCCESS]: updsuccess,
  [Types.UPD_COMPANY_FAILURE]: updfailure,

  [Types.DLT_COMPANY_REQUEST]: dltrequest,
  [Types.DLT_COMPANY_SUCCESS]: dltsuccess,
  [Types.DLT_COMPANY_FAILURE]: dltfailure
});
