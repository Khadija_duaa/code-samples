import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getEmployerRequest: ["id"],
  getEmployerSuccess: ["success"],
  getEmployerFailure: ["error"],

  getAllEmployerRequest: ["data"],
  getAllEmployerSuccess: ["allEmpSuccess"],
  getAllEmployerFailure: ["allEmpError"],

  addEmployerRequest: ["data", "token"],
  addEmployerSuccess: ["addsuccess"],
  addEmployerFailure: ["adderror"],

  updEmployerRequest: ["data"],
  updEmployerSuccess: ["updsuccess"],
  updEmployerFailure: ["upderror"],

  dltEmployerRequest: ["data"],
  dltEmployerSuccess: ["dltsuccess"],
  dltEmployerFailure: ["dlterror"]
});

export const EmployerTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  employerData: null,
  fetching: false,
  error: null,
  allEmpFetching: false,
  allEmpSuccess: null,
  allEmpFailure: null,
  addFetching: false,
  addSuccess: null,
  addError: null,
  updFetching: false,
  updSuccess: null,
  updError: null,
  dltFetching: false,
  dltSuccess: null,
  dltFailure: null
});

/* ------------- Reducers ------------- */

// we're attempting to get employer data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get employer data
export const success = (state, { success }) =>
  state.merge({ fetching: false, clDocData: success });

// we've had a problem in employer info data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to all employer data
export const allemprequest = state => {
  console.log("on redux call");
  return state.merge({ allEmpFetching: true });
};

// we've successfully get all employer data
export const allempsuccess = (state, { allEmpSuccess }) =>
  state.merge({ allEmpFetching: false, allEmpSuccess: allEmpSuccess });

// we've had a problem in getting all employer data
export const allempfailure = (state, { allEmpError }) =>
  state.merge({ allEmpFetching: false, allEmpError });

// we're attempting to all employer data
export const addemprequest = state => {
  console.log("on redux call");
  return state.merge({ addFetching: true });
};

// we've successfully get all employer data
export const addempsuccess = (state, { addSuccess }) =>
  state.merge({ addFetching: false, addSuccess: addSuccess });

// we've had a problem in getting all employer data
export const addempfailure = (state, { addError }) =>
  state.merge({ addFetching: false, addError });

// we're attempting to update employer data
export const updemprequest = state => {
  console.log("on redux call");
  return state.merge({ updFetching: true });
};

// we've successfully updated employer data
export const updempsuccess = (state, { updSuccess }) =>
  state.merge({ updFetching: false, updSuccess: updSuccess });

// we've had a problem updating employer data
export const updempfailure = (state, { updError }) =>
  state.merge({ updFetching: false, updError });

// we're attempting to delete employer data
export const dltemprequest = state => {
  console.log("on redux call");
  return state.merge({ dltFetching: true });
};

// we've successfully deleted employer data
export const dltempsuccess = (state, { dltSuccess }) =>
  state.merge({ dltFetching: false, dltSuccess: dltSuccess });

// we've had a problem deleting employer data
export const dltempfailure = (state, { dltError }) =>
  state.merge({ dltFetching: false, dltError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_EMPLOYER_REQUEST]: request,
  [Types.GET_EMPLOYER_SUCCESS]: success,
  [Types.GET_EMPLOYER_FAILURE]: failure,

  [Types.GET_ALL_EMPLOYER_REQUEST]: allemprequest,
  [Types.GET_ALL_EMPLOYER_SUCCESS]: allempsuccess,
  [Types.GET_ALL_EMPLOYER_FAILURE]: allempfailure,

  [Types.ADD_EMPLOYER_REQUEST]: addemprequest,
  [Types.ADD_EMPLOYER_SUCCESS]: addempsuccess,
  [Types.ADD_EMPLOYER_FAILURE]: addempfailure,

  [Types.UPD_EMPLOYER_REQUEST]: updemprequest,
  [Types.UPD_EMPLOYER_SUCCESS]: updempsuccess,
  [Types.UPD_EMPLOYER_FAILURE]: updempfailure,

  [Types.DLT_EMPLOYER_REQUEST]: dltemprequest,
  [Types.DLT_EMPLOYER_SUCCESS]: dltempsuccess,
  [Types.DLT_EMPLOYER_FAILURE]: dltempfailure
});
