import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getEmpContactRequest: ["id"],
  getEmpContactSuccess: ["success"],
  getEmpContactFailure: ["error"],

  getAllEmpContactRequest: ["data"],
  getAllEmpContactSuccess: ["allEmpSuccess"],
  getAllEmpContactFailure: ["allEmpError"],

  addEmpContactRequest: ["data", "token"],
  addEmpContactSuccess: ["addsuccess"],
  addEmpContactFailure: ["adderror"],

  updEmpContactRequest: ["data"],
  updEmpContactSuccess: ["updsuccess"],
  updEmpContactFailure: ["upderror"],

  dltEmpContactRequest: ["data"],
  dltEmpContactSuccess: ["dltsuccess"],
  dltEmpContactFailure: ["dlterror"]
});

export const EmployerTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  empContactsData: null,
  fetching: false,
  error: null,
  allFetching: false,
  allSuccess: null,
  allFailure: null,
  addFetching: false,
  addSuccess: null,
  addError: null,
  updFetching: false,
  updSuccess: null,
  updError: null,
  dltFetching: false,
  dltSuccess: null,
  dltFailure: null
});

/* ------------- Reducers ------------- */

// we're attempting to get employer contact data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get employer contact data
export const success = (state, { success }) =>
  state.merge({ fetching: false, empContactsData: success });

// we've had a problem in employer contact data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to all employer contact data
export const allemprequest = state => {
  console.log("on redux call");
  return state.merge({ allEmpFetching: true });
};

// we've successfully get all employer contact data
export const allempsuccess = (state, { allSuccess }) =>
  state.merge({ allFetching: false, allSuccess: allSuccess });

// we've had a problem in getting all employer contact data
export const allempfailure = (state, { allError }) =>
  state.merge({ allFetching: false, allError });

// we're attempting to all employer contact data
export const addemprequest = state => {
  console.log("on redux call");
  return state.merge({ addFetching: true });
};

// we've successfully get all employer contact data
export const addempsuccess = (state, { addSuccess }) =>
  state.merge({ addFetching: false, addSuccess: addSuccess });

// we've had a problem in getting all employer contact data
export const addempfailure = (state, { addError }) =>
  state.merge({ addFetching: false, addError });

// we're attempting to update employer contact data
export const updemprequest = state => {
  console.log("on redux call");
  return state.merge({ updFetching: true });
};

// we've successfully updated employer contact data
export const updempsuccess = (state, { updSuccess }) =>
  state.merge({ updFetching: false, updSuccess: updSuccess });

// we've had a problem updating employer contact data
export const updempfailure = (state, { updError }) =>
  state.merge({ updFetching: false, updError });

// we're attempting to delete employer contact data
export const dltemprequest = state => {
  console.log("on redux call");
  return state.merge({ dltFetching: true });
};

// we've successfully deleted employer data
export const dltempsuccess = (state, { dltSuccess }) =>
  state.merge({ dltFetching: false, dltSuccess: dltSuccess });

// we've had a problem deleting employer data
export const dltempfailure = (state, { dltError }) =>
  state.merge({ dltFetching: false, dltError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_EMP_CONTACT_REQUEST]: request,
  [Types.GET_EMP_CONTACT_SUCCESS]: success,
  [Types.GET_EMP_CONTACT_FAILURE]: failure,

  [Types.GET_ALL_EMP_CONTACT_REQUEST]: allemprequest,
  [Types.GET_ALL_EMP_CONTACT_SUCCESS]: allempsuccess,
  [Types.GET_ALL_EMP_CONTACT_FAILURE]: allempfailure,

  [Types.ADD_EMP_CONTACT_REQUEST]: addemprequest,
  [Types.ADD_EMP_CONTACT_SUCCESS]: addempsuccess,
  [Types.ADD_EMP_CONTACT_FAILURE]: addempfailure,

  [Types.UPD_EMP_CONTACT_REQUEST]: updemprequest,
  [Types.UPD_EMP_CONTACT_SUCCESS]: updempsuccess,
  [Types.UPD_EMP_CONTACT_FAILURE]: updempfailure,

  [Types.DLT_EMP_CONTACT_REQUEST]: dltemprequest,
  [Types.DLT_EMP_CONTACT_SUCCESS]: dltempsuccess,
  [Types.DLT_EMP_CONTACT_FAILURE]: dltempfailure
});
