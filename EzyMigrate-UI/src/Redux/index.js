import { combineReducers } from "redux";
import configureStore from "./CreateStore";
import rootSaga from "../Sagas/";

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  appAccount: require("./AppAccountRedux").reducer,
  login: require("./LoginRedux/LoginRedux").reducer,
  // employerData: require("./EmployerManagementRedux/EmployerRedux").reducer,
  userAccount: require("./SuperUserSettingRedux/AccountSettingsRedux").reducer
});

export default () => {
  let finalReducers = reducers;
  // If rehydration is on use persistReducer otherwise default combineReducers

  let { store, sagasManager, sagaMiddleware } = configureStore(
    finalReducers,
    rootSaga
  );

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require("./").reducers;
      store.replaceReducer(nextRootReducer);

      const newYieldedSagas = require("../Sagas").default;
      sagasManager.cancel();
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware(newYieldedSagas);
      });
    });
  }

  return store;
};
