import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getManagerRequest: [],
  getManagerSuccess: ["success"],
  getManagerFailure: ["error"],

  updManagerRequest: ["data"],
  updManagerSuccess: ["success"],
  updManagerFailure: ["error"]
});

export const ManagerTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  managerData: null,
  fetching: false,
  error: null,
  updFetching: false,
  updError: null,
  updSuccess: null
});

/* ------------- Reducers ------------- */

// we're attempting to get sus manager data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get sus manager data
export const success = (state, { success }) =>
  state.merge({ fetching: false, managerData: success });

// we've had a problem in geting manager data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to add sus manager data
export const updrequest = state => state.merge({ updFetching: true });

// we've successfully added sus manager data
export const updsuccess = (state, { updSuccess }) =>
  state.merge({ updFetching: false, updSuccess: updSuccess });

// we've had a problem in adding manager data
export const updfailure = (state, { updError }) =>
  state.merge({ updFetching: false, updError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_MANAGER_REQUEST]: request,
  [Types.GET_MANAGER_SUCCESS]: success,
  [Types.GET_MANAGER_FAILURE]: failure,

  [Types.UPD_MANAGER_REQUEST]: updrequest,
  [Types.UPD_MANAGER_SUCCESS]: updsuccess,
  [Types.UPD_MANAGER_FAILURE]: updfailure
});
