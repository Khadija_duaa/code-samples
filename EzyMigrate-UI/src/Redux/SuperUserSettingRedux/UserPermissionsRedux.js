import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getUserPermissionsRequest: [],
  getUserPermissionsSuccess: ["success"],
  getUserPermissionsFailure: ["error"],

  addUserPermissionsRequest: ["data"],
  addUserPermissionsSuccess: ["success"],
  addUserPermissionsFailure: ["error"],

  updUserPermissionsRequest: ["data"],
  updUserPermissionsSuccess: ["success"],
  updUserPermissionsFailure: ["error"]
});

export const SusUserPermssionsTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  userPermissionData: null,
  fetching: false,
  error: null,
  addFetching: false,
  addError: null,
  addSuccess: null,
  updFetching: false,
  updError: null,
  updSuccess: null
});

/* ------------- Reducers ------------- */

// we're attempting to get sus user Permssions
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get sus user Permssions
export const success = (state, { success }) =>
  state.merge({ fetching: false, userPermssionData: success });

// we've had a problem in geting sus user Permssions
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to add sus user Permssions
export const addrequest = state => state.merge({ addFetching: true });

// we've successfully added sus user Permssions
export const addsuccess = (state, { addSuccess }) =>
  state.merge({ addFetching: false, addSuccess: addSuccess });

// we've had a problem in adding user Permssions
export const addfailure = (state, { addError }) =>
  state.merge({ addFetching: false, addError });

// we're attempting to add sus user Permssions
export const updrequest = state => state.merge({ updFetching: true });

// we've successfully added sus user Permssions
export const updsuccess = (state, { updSuccess }) =>
  state.merge({ updFetching: false, updSuccess: updSuccess });

// we've had a problem in adding user Permssions
export const updfailure = (state, { updError }) =>
  state.merge({ updFetching: false, updError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_USER_PERMISSIONS_REQUEST]: request,
  [Types.GET_USER_PERMISSIONS_SUCCESS]: success,
  [Types.GET_USER_PERMISSIONS_FAILURE]: failure,

  [Types.ADD_USER_PERMISSIONS_REQUEST]: addrequest,
  [Types.ADD_USER_PERMISSIONS_SUCCESS]: addsuccess,
  [Types.ADD_USER_PERMISSIONS_FAILURE]: addfailure,

  [Types.UPD_USER_PERMISSIONS_REQUEST]: updrequest,
  [Types.UPD_USER_PERMISSIONS_SUCCESS]: updsuccess,
  [Types.UPD_USER_PERMISSIONS_FAILURE]: updfailure
});
