import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getBranchRequest: [],
  getBranchSuccess: ["success"],
  getBranchFailure: ["error"],

  addBranchRequest: ["data"],
  addBranchSuccess: ["success"],
  addBranchFailure: ["error"],

  updBranchRequest: ["data"],
  updBranchSuccess: ["success"],
  updBranchFailure: ["error"]
});

export const SusBranchTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  branchData: null,
  fetching: false,
  error: null,
  addFetching: false,
  addError: null,
  addSuccess: null,
  updFetching: false,
  updError: null,
  updSuccess: null
});

/* ------------- Reducers ------------- */

// we're attempting to get sus Branch
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get sus Branch
export const success = (state, { success }) =>
  state.merge({ fetching: false, userCompanyData: success });

// we've had a problem in geting sus Branch
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to add sus Branch
export const addrequest = state => state.merge({ addFetching: true });

// we've successfully added sus Branch
export const addsuccess = (state, { addSuccess }) =>
  state.merge({ addFetching: false, addSuccess: addSuccess });

// we've had a problem in adding Branch
export const addfailure = (state, { addError }) =>
  state.merge({ addFetching: false, addError });

// we're attempting to add sus Branch
export const updrequest = state => state.merge({ updFetching: true });

// we've successfully added sus Branch
export const updsuccess = (state, { updSuccess }) =>
  state.merge({ updFetching: false, updSuccess: updSuccess });

// we've had a problem in adding Branch
export const updfailure = (state, { updError }) =>
  state.merge({ updFetching: false, updError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_BRANCH_REQUEST]: request,
  [Types.GET_BRANCH_SUCCESS]: success,
  [Types.GET_BRANCH_FAILURE]: failure,

  [Types.ADD_BRANCH_REQUEST]: addrequest,
  [Types.ADD_BRANCH_SUCCESS]: addsuccess,
  [Types.ADD_BRANCH_FAILURE]: addfailure,

  [Types.UPD_BRANCH_REQUEST]: updrequest,
  [Types.UPD_BRANCH_SUCCESS]: updsuccess,
  [Types.UPD_BRANCH_FAILURE]: updfailure
});
