import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getOwnerRequest: [],
  getOwnerSuccess: ["success"],
  getOwnerFailure: ["error"],

  updOwnerRequest: ["data"],
  updOwnerSuccess: ["success"],
  updOwnerFailure: ["error"]
});

export const OwnerTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  ownerData: null,
  fetching: false,
  error: null,
  updFetching: false,
  updError: null,
  updSuccess: null
});

/* ------------- Reducers ------------- */

// we're attempting to get sus owner data
export const request = state => {
  console.log("on redux call");
  return state.merge({ fetching: true });
};

// we've successfully get sus owner data
export const success = (state, { success }) =>
  state.merge({ fetching: false, ownerData: success });

// we've had a problem in geting owner data
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to add sus owner data
export const updrequest = state => state.merge({ updFetching: true });

// we've successfully added sus owner data
export const updsuccess = (state, { updSuccess }) =>
  state.merge({ updFetching: false, updSuccess: updSuccess });

// we've had a problem in adding owner data
export const updfailure = (state, { updError }) =>
  state.merge({ updFetching: false, updError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_OWNER_REQUEST]: request,
  [Types.GET_OWNER_SUCCESS]: success,
  [Types.GET_OWNER_FAILURE]: failure,

  [Types.UPD_OWNER_REQUEST]: updrequest,
  [Types.UPD_OWNER_SUCCESS]: updsuccess,
  [Types.UPD_OWNER_FAILURE]: updfailure
});
