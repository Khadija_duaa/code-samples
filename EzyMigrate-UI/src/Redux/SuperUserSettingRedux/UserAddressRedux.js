import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getUserAddressRequest: [],
  getUserAddressSuccess: ["success"],
  getUserAddressFailure: ["error"],

  addUserAddressRequest: ["data"],
  addUserAddressSuccess: ["success"],
  addUserAddressFailure: ["error"]
});

export const SusUserContactTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  userAddressData: null,
  fetching: false,
  error: null,
  addFetching: false,
  addError: null,
  addSuccess: null
});

/* ------------- Reducers ------------- */

// we're attempting to get sus user address
export const request = state => state.merge({ fetching: true });

// we've successfully get sus user address
export const success = (state, { success }) =>
  state.merge({ fetching: false, userAddressData: success });

// we've had a problem in geting sus user address
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error });

// we're attempting to add sus user address
export const addrequest = state => state.merge({ addFetching: true });

// we've successfully added sus user address
export const addsuccess = (state, { addSuccess }) =>
  state.merge({ addFetching: false, addSuccess: addSuccess });

// we've had a problem in adding user address
export const addfailure = (state, { addError }) =>
  state.merge({ addFetching: false, addError });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_USER_ADDRESS_REQUEST]: request,
  [Types.GET_USER_ADDRESS_SUCCESS]: success,
  [Types.GET_USER_ADDRESS_FAILURE]: failure,

  [Types.ADD_USER_ADDRESS_REQUEST]: addrequest,
  [Types.ADD_USER_ADDRESS_SUCCESS]: addsuccess,
  [Types.ADD_USER_ADDRESS_FAILURE]: addfailure
});
