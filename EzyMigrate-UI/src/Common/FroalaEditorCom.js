import React, { useState, Fragment, useEffect, useRef } from "react";
import FroalaEditor from "react-froala-wysiwyg";
import "froala-editor/js/plugins.pkgd.min.js";

const FroalaEditorCom = ({
  onUploadAvatar,
  model,
  onModelChange,
  setLoading,
}) => {
  let config = {
    key:
      "YNB3fA3A7A8B6A4C3A-9UJHAEFZMUJOYGYQEa1c1ZJg1RAeF5C4C3G3E2C2A3D6B3E3==",
    toolbarSticky: false,
    zIndex: 999999,
    imageInsertButtons: ["imageBack", "|", "imageUpload"],
    toolbarButtons: {
      moreText: {
        buttons: [
          "bold",
          "italic",
          "subscript",
          "superscript",
          "fontFamily",
          "fontSize",
          "textColor",
          "backgroundColor",
          "undo",
          "textColor",
          "redo",
          "fullscreen",
          "print",
          "spellChecker",
          "selectAll",
          "html",
          "help",
        ],
      },

      moreParagraph: {
        buttons: [
          "alignLeft",
          "alignCenter",
          "formatOLSimple",
          "alignRight",
          "alignJustify",
          "formatOL",
          "formatUL",
          "paragraphFormat",
          "paragraphStyle",
          "lineHeight",
          "outdent",
          "indent",
          "quote",
        ],
      },
      moreRich: {
        buttons: [
          "insertLink",
          "insertImage",
          "insertTable",
          "emoticons",
          "fontAwesome",
          "specialCharacters",
          "embedly",
          "insertHR",
        ],
      },
      moreMisc: {
        buttons: [
          "undo",
          "redo",
          "fullscreen",
          "spellChecker",
          "selectAll",
          "html",
          "help",
        ],
      },
    },
    imageEditButtons: [
      "imageDisplay",
      "imageAlign",
      "imageInfo",
      "imageRemove",
    ],
    events: {
      keydown: function(e) {
        if (e.keyCode === 13) {
          e.preventDefault();
          e.stopPropagation();
          e.stopImmediatePropagation();
        }
      },
      "image.beforeUpload": function(files) {
        let editor = this;
        if (files.length) {
          // Create a File Reader.
          let reader = new FileReader();
          let result = files[0];
          let formData = new FormData();
          formData.append("File", result);
          setLoading && setLoading(true);
          onUploadAvatar(formData)
            .then((res) => {
              editor.image.insert(res.payload, null, null, editor.image.get());
              setLoading && setLoading(false);
            })
            .catch(() => {
              setLoading && setLoading(false);
            });
          // Read image as base64.

          reader.readAsDataURL(files[0]);
        }
        editor.popups.hideAll();
        // Stop default upload chain.
        return false;
      },
    },
  };
  return (
    <div>
      <FroalaEditor
        ref={(el) => {
          config = el;
        }}
        model={model}
        config={config}
        onModelChange={onModelChange}
      />
    </div>
  );
};

export default FroalaEditorCom;
