export  const SystemGeneratedEmails = [
  {
    emailType: "Invitation to www.checkmyvisa.co.nz",
    emailContent: "<p>Your Immigration Adviser / Lawyer has registered your email address with www.checkmyvisa.co.nz.please click on the ACTIVATE button below to create your free account.</p><p>With checkmyvisa.co.nz, you can stay in touch with your Immigration Adviser / Lawyer more efficiently.All future e mail communication with your Immigration Adviser / Lawyer will be through www.checkmyvisa.co.nz .</p><p>Whenever your Immigration adviser / lawyer, sends you a new email or update your visa status,you will automatically receive a notification in your email.</p>",
  },
  {
    emailType: "Invoice Email",
    emailContent: "<p>Here's invoice @InvoiceNumber for @Amount.</p><p>The amount outstanding of @Amount is due on @DueDate.</p>",
  },
  {
    emailType: "Notifications",
    emailContent: "<p>Your Immigration Adviser / Lawyer has left a message for you to review.</p>",
  },
  {
    emailType: "Update Visa Status Notification",
    emailContent: "<p>Your Immigration Adviser / Lawyer has updated your Visa Status.</p>",
  },
  {
    emailType: "Questionnaire Link",
    emailContent: "<p>Please fill the form on the following link.</p>",
  },
  {
    emailType: "Invoice Follow-up",
    emailContent: "<p>This is to remind you that your invoice @InvoiceNo with amount : @Amount is due on @DueDate</p>",
  }
]