import React from "react";
import Select from "react-select";
import "./TimeTrackingStyles.css";
import HeaderBar from "../Components/Header/HeaderBar";

import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import { Images } from "../Themes";
import PotentialHeaderTabs from "../Components/Header/PotentialHeaderTabs";
import Modal from "react-awesome-modal";

const headOption = [
  { tabName: "WORK TYPE", linkName: "/work-type" },
  { tabName: "VISA TYPE PRICE", linkName: "/visa-type-price" },
  { tabName: "SERVICE TYPE", linkName: "/service-type" }
];

class ServiceType extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addServiceTypeVisible: false
    };
  }

  openServiceTypeModal = () => {
    this.setState({
      addServiceTypeVisible: true
    });
  };

  closeServiceTypeModal = () => {
    this.setState({
      addServiceTypeVisible: false
    });
  };

  render() {
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            <PotentialHeaderTabs data={headOption} activeTab="SERVICE TYPE" />

            <div className="report-container">
              <div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: 20,
                    marginRight: 20
                  }}
                >
                  <div className="pciq-top-div" style={{ marginTop: 0 }}>
                    <span
                      className="pc-top-div-text"
                      style={{ color: "#0A3C5D" }}
                    >
                      SERVICE TYPE
                    </span>
                  </div>
                </div>

                <div className="ca-gray-cont" style={{ paddingLeft: 20 }}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "flex-end",
                      marginRight: 20
                    }}
                  >
                    <div onClick={this.openServiceTypeModal}>
                      <img
                        src={Images.plusIcon}
                        style={{ width: 20, height: 20 }}
                      />
                    </div>
                  </div>
                  <table
                    className="ca-invoice-table-cont"
                    style={{
                      width: `calc(100% - 20px)`,
                      borderSpacing: 0,
                      marginTop: 20
                    }}
                  >
                    <tbody>
                      <tr style={{ backgroundColor: "#EBEBEB" }}>
                        <th className="tt-wt-heading">NAME</th>
                        <th className="tt-wt-heading">BASE PRICE</th>
                        <th className="tt-wt-heading">TIME IN HOURS</th>
                        <th
                          className="tt-wt-heading"
                          style={{ width: 80 }}
                        ></th>
                      </tr>
                      <tr style={{ backgroundColor: "#FFFFFF" }}>
                        <td className="tt-wt-data-row">Test Visa</td>
                        <td className="tt-wt-data-row">10.00</td>
                        <td className="tt-wt-data-row">1.00</td>
                        <td className="tt-wt-data-row">
                          <div style={{ display: "flex" }}>
                            <div>
                              <img
                                src={Images.editBorderBlue}
                                style={{ width: 15, height: 15 }}
                              />
                            </div>
                            <div style={{ marginLeft: 5 }}>
                              <img
                                src={Images.deleteBlue}
                                style={{ width: 15, height: 15 }}
                              />
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr style={{ backgroundColor: "#F9F9F9" }}>
                        <td className="tt-wt-data-row">Test Visa</td>
                        <td className="tt-wt-data-row">10.00</td>
                        <td className="tt-wt-data-row">1.00</td>
                        <td className="tt-wt-data-row">
                          <div style={{ display: "flex" }}>
                            <div>
                              <img
                                src={Images.editBorderBlue}
                                style={{ width: 15, height: 15 }}
                              />
                            </div>
                            <div style={{ marginLeft: 5 }}>
                              <img
                                src={Images.deleteBlue}
                                style={{ width: 15, height: 15 }}
                              />
                            </div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            {/* ======== Add service type popup design ======== */}

            <Modal
              visible={this.state.addServiceTypeVisible}
              width="500"
              height="270"
              effect="fadeInUp"
              onClickAway={() => this.closeServiceTypeModal()}
            >
              <div style={{ padding: 20 }}>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div className="sus-modal-label" style={{ border: 0 }}>
                    <span className="sus-modal-label-text">SERVICE TYPE</span>
                  </div>
                  <div onClick={this.closeWorkTypeModal}>
                    <img src={Images.grayCross} style={{ width: 15 }} />
                  </div>
                </div>
                <div style={{ width: "100%", marginLeft: 20 }}>
                  <div class="emp-ae-input-width">
                    <p style={{ width: "30%" }}>Name</p>
                    <div class="profile-input-border" style={{ width: "65%" }}>
                      <input
                        className="profile-input"
                        placeholder=""
                        type="text"
                        onChange={this.myChangeHandler}
                      />
                    </div>
                  </div>

                  <div class="emp-ae-input-width" style={{ marginTop: 15 }}>
                    <p style={{ width: "30%" }}>Base Price</p>
                    <div class="profile-input-border" style={{ width: "65%" }}>
                      <input
                        className="profile-input"
                        placeholder=""
                        type="text"
                        onChange={this.myChangeHandler}
                      />
                    </div>
                  </div>

                  <div class="emp-ae-input-width" style={{ marginTop: 15 }}>
                    <p style={{ width: "30%" }}>Time In Hours</p>
                    <div class="profile-input-border" style={{ width: "65%" }}>
                      <input
                        className="profile-input"
                        placeholder=""
                        type="text"
                        onChange={this.myChangeHandler}
                      />
                    </div>
                  </div>
                </div>

                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: 30
                  }}
                >
                  <div></div>
                  <div style={{ display: "flex" }}>
                    <div
                      onClick={this.closeServiceTypeModal}
                      className="sus-modal-button"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="sus-modal-button-text">SAVE</span>
                    </div>
                    <div
                      onClick={this.closeServiceTypeModal}
                      className="sus-modal-button"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="sus-modal-button-text">CLOSE</span>
                    </div>
                  </div>
                </div>
              </div>
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceType;
