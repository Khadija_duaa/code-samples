import React from "react";
import Select from "react-select";
import "./TimeTrackingStyles.css";
import HeaderBar from "../Components/Header/HeaderBar";

import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import { Images } from "../Themes";
import PotentialHeaderTabs from "../Components/Header/PotentialHeaderTabs";
import Modal from "react-awesome-modal";

const headOption = [
  { tabName: "WORK TYPE", linkName: "/work-type" },
  { tabName: "VISA TYPE PRICE", linkName: "/visa-type-price" },
  { tabName: "SERVICE TYPE", linkName: "/service-type" }
];

class VisaTypePrice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      workTypePopupVisible: false
    };
  }

  openWorkTypeModal = () => {
    this.setState({
      workTypePopupVisible: true
    });
  };

  closeWorkTypeModal = () => {
    this.setState({
      workTypePopupVisible: false
    });
  };

  render() {
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            <PotentialHeaderTabs
              data={headOption}
              activeTab="VISA TYPE PRICE"
            />

            <div className="report-container">
              <div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: 20,
                    marginRight: 20
                  }}
                >
                  <div className="pciq-top-div" style={{ marginTop: 0 }}>
                    <span
                      className="pc-top-div-text"
                      style={{ color: "#0A3C5D" }}
                    >
                      VISA TYPE PRICE
                    </span>
                  </div>
                  <div onClick={this.openWorkTypeModal}>
                    <img
                      src={Images.plusIcon}
                      style={{ width: 20, height: 20 }}
                    />
                  </div>
                </div>

                <div className="ca-gray-cont" style={{ paddingLeft: 20 }}>
                  <table
                    className="ca-invoice-table-cont"
                    style={{
                      width: `calc(100% - 20px)`,
                      borderSpacing: 0,
                      marginTop: 20
                    }}
                  >
                    <tbody>
                      <tr style={{ backgroundColor: "#EBEBEB" }}>
                        <th
                          className="tt-wt-heading"
                          style={{ textAlign: "left" }}
                        >
                          NAME
                        </th>
                        <th
                          className="tt-wt-heading"
                          style={{ textAlign: "left" }}
                        >
                          BASE PRICE
                        </th>
                        <th
                          className="tt-wt-heading"
                          style={{ textAlign: "left" }}
                        >
                          TIME IN HOURS
                        </th>
                      </tr>
                      <tr style={{ backgroundColor: "#FFFFFF" }}>
                        <td
                          className="tt-wt-data-row"
                          style={{ textAlign: "left", width: "48%" }}
                        >
                          Test Visa
                        </td>
                        <td
                          className="tt-wt-data-row"
                          style={{ textAlign: "left", width: "20%" }}
                        >
                          <div style={{ display: "flex" }}>
                            <div className="tt-vtp-text-border-cont">
                              <span>111.00</span>
                            </div>
                          </div>
                        </td>
                        <td className="tt-wt-data-row">
                          <div style={{ display: "flex" }}>
                            <div className="tt-vtp-text-border-cont">
                              <span>2.00</span>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr style={{ backgroundColor: "#F9F9F9" }}>
                        <td
                          className="tt-wt-data-row"
                          style={{ textAlign: "left", width: "48%" }}
                        >
                          Test Visa
                        </td>
                        <td
                          className="tt-wt-data-row"
                          style={{ textAlign: "left", width: "20%" }}
                        >
                          <div style={{ display: "flex" }}>
                            <div className="tt-vtp-text-border-cont">
                              <span>111.00</span>
                            </div>
                          </div>
                        </td>
                        <td className="tt-wt-data-row">
                          <div style={{ display: "flex" }}>
                            <div className="tt-vtp-text-border-cont">
                              <span>2.00</span>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            {/* ======== Add work visa desing ======== */}

            <Modal
              visible={this.state.workTypePopupVisible}
              width="500"
              height="180"
              effect="fadeInUp"
              onClickAway={() => this.closeWorkTypeModal()}
            >
              <div style={{ padding: 20 }}>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div className="sus-modal-label" style={{ border: 0 }}>
                    <span className="sus-modal-label-text">WORK TYPE</span>
                  </div>
                  <div onClick={this.closeModal}>
                    <img src={Images.grayCross} style={{ width: 15 }} />
                  </div>
                </div>
                <div style={{ width: "100%", marginLeft: 20 }}>
                  <div class="emp-ae-input-width">
                    <p style={{ width: "30%" }}>Name</p>
                    <div class="profile-input-border" style={{ width: "65%" }}>
                      <input
                        className="profile-input"
                        placeholder="Name"
                        type="text"
                        onChange={this.myChangeHandler}
                      />
                    </div>
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: 30
                  }}
                >
                  <div></div>
                  <div style={{ display: "flex" }}>
                    <div
                      onClick={this.closeWorkTypeModal}
                      className="sus-modal-button"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="sus-modal-button-text">SAVE</span>
                    </div>
                    <div
                      onClick={this.closeWorkTypeModal}
                      className="sus-modal-button"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="sus-modal-button-text">CLOSE</span>
                    </div>
                  </div>
                </div>
              </div>
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}

export default VisaTypePrice;
