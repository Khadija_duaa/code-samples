import React from "react";
import { connect } from "react-redux";
import { createCode, createToken, getXeroToken } from "../store/Actions";
import { Spin } from "antd";
import { bindActionCreators } from "redux";
import { Redirect, Switch, withRouter } from "react-router-dom";

class Xero extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  componentWillMount() {
    this.setState({ loading: true });
    this.props.getXeroToken().then(() => {
      this.setState({ loading: false });
    });
  }

  getComponents = () => {
    if (
      window.location.pathname === "/xero-sync-add-account" &&
      this.props.isExists.isExists === true
    ) {
      this.props.history.push("/xero-sync-invoices");
    } else if (
      window.location.pathname === "/xero-sync" &&
      this.props.isExists.isExists === true
    ) {
      this.props.history.push("/xero-sync-invoices");
    } else if (
      window.location.pathname === "/xero-sync-invoices" &&
      this.props.isExists.isExists === true
    ) {
      this.props.history.push("/xero-sync-invoices");
    } else if (
      window.location.pathname === "/xero-sync-add-account" &&
      this.props.isExists.isExists === false
    ) {
      this.props.history.push("/xero-sync-add-account");
    } else if (
      window.location.pathname === "/xero-sync" &&
      this.props.isExists.isExists === false
    ) {
      this.props.history.push("/xero-sync-add-account");
    } else if (
      window.location.pathname === "/xero-sync-invoices" &&
      this.props.isExists.isExists === false
    ) {
      this.props.history.push("/xero-sync-add-account");
    } else {
    }
  };

  render() {
    return (
      <div>
        {this.state.loading ? (
          <div className={"spinner"}>
            <Spin size="large" />
          </div>
        ) : (
          this.getComponents()
        )

        // this.props.isExists ?
        //     (
        //
        //         this.props.history.push("/xero-sync-invoices")
        //         // <XeroModule
        //         //     createToken={this.props.createToken}
        //         //     createCode={this.props.createCode}
        //         //     isExists={this.props.isExists}/>
        //         // <div className="ca-gray-cont">
        //         //     {"testing"}
        //         // </div>
        //     ) :
        //     (this.props.history.push("/xero-sync-add-account"))
        // <XeroModule
        //     createToken={this.props.createToken}
        //     createCode={this.props.createCode}
        //     isExists={this.props.isExists}/>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isExists: state && state.xeroReducer && state.xeroReducer.isExists,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getXeroToken: bindActionCreators(getXeroToken, dispatch),
});

Xero = connect(mapStateToProps, mapDispatchToProps)(Xero);

export default withRouter(Xero);
