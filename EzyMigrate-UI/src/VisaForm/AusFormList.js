import React from "react";
import Select from "react-select";
import "./VisaFormStyles.css";
import HeaderBar from "../Components/Header/HeaderBar";

import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import { Images } from "../Themes";
import PotentialHeaderTabs from "../Components/Header/PotentialHeaderTabs";
import Modal from "react-awesome-modal";

const headOption = [
  { tabName: "NZ FOMRS LIST", linkName: "/nz-form-list" },
  { tabName: "AUS FORMS LIST", linkName: "/aus-form-list" }
];

class AusFormList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      workTypePopupVisible: false
    };
  }

  openWorkTypeModal = () => {
    this.setState({
      workTypePopupVisible: true
    });
  };

  closeWorkTypeModal = () => {
    this.setState({
      workTypePopupVisible: false
    });
  };

  render() {
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            <PotentialHeaderTabs data={headOption} activeTab="AUS FORMS LIST" />

            <div className="report-container">
              <div>
                <div className="ca-gray-cont" style={{ paddingLeft: 20 }}>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <img
                      src={Images.pointer}
                      style={{ width: 20, height: 20 }}
                    />
                    <span
                      className="cv-normal-text"
                      style={{ marginLeft: 30, fontSize: 10 }}
                    >
                      DETAILS OF CHILD OR OTHER DEPENDENT FAMILY MEMBER AGED 18
                      YEARD OF OVER
                    </span>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: 10
                    }}
                  >
                    <img
                      src={Images.pointer}
                      style={{ width: 20, height: 20 }}
                    />
                    <span
                      className="cv-normal-text"
                      style={{ marginLeft: 30, fontSize: 10 }}
                    >
                      APPLICATION OF PARENT TO MIGRATE TO AUSTRALIA
                    </span>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: 10
                    }}
                  >
                    <img
                      src={Images.pointer}
                      style={{ width: 20, height: 20 }}
                    />
                    <span
                      className="cv-normal-text"
                      style={{ marginLeft: 30, fontSize: 10 }}
                    >
                      PERSONAL PERTICULARS FOR ASSESSMENT INCLUDING CHARACTER
                      ASSSESSMENT
                    </span>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: 10
                    }}
                  >
                    <img
                      src={Images.pointer}
                      style={{ width: 20, height: 20 }}
                    />
                    <span
                      className="cv-normal-text"
                      style={{ marginLeft: 30, fontSize: 10 }}
                    >
                      REQUEST FOR ACCESS TO DOCUMENTS OR INFORMATION
                    </span>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: 10
                    }}
                  >
                    <img
                      src={Images.pointer}
                      style={{ width: 20, height: 20 }}
                    />
                    <span
                      className="cv-normal-text"
                      style={{ marginLeft: 30, fontSize: 10 }}
                    >
                      STATUTORY DECLARATION BY A SUPPORTING WITNESS IN RELATION
                      TO A PARTNER OR PROSPECTIVE MARRIAGE VISA APPLICATION
                    </span>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: 10
                    }}
                  >
                    <img
                      src={Images.pointer}
                      style={{ width: 20, height: 20 }}
                    />
                    <span
                      className="cv-normal-text"
                      style={{ marginLeft: 30, fontSize: 10 }}
                    >
                      CHANGE OF ADDRESS AND/OR PASSPORT DETAILS
                    </span>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: 10
                    }}
                  >
                    <img
                      src={Images.pointer}
                      style={{ width: 20, height: 20 }}
                    />
                    <span
                      className="cv-normal-text"
                      style={{ marginLeft: 30, fontSize: 10 }}
                    >
                      ADVISE BY A MIGRATION AGENT/EXEMPT PERSON OF PROVIDING
                      IMMIGRATION ASSISTANCE
                    </span>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: 10
                    }}
                  >
                    <img
                      src={Images.pointer}
                      style={{ width: 20, height: 20 }}
                    />
                    <span
                      className="cv-normal-text"
                      style={{ marginLeft: 30, fontSize: 10 }}
                    >
                      CONTSENT TO GRANT AN AUTRALIAN VISA TO A CHILD UNDER THE
                      AGE OF 18 YEARS
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AusFormList;
