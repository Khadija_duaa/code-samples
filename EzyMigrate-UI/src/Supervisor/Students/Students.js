import React from "react";
import { UploadOutlined } from "@ant-design/icons";

import {
  Collapse,
  Spin,
  Modal,
  Form,
  Button,
  Input,
  Popconfirm,
  Select,
  DatePicker,
  message,
  Table,
  Upload,
} from "antd";
import jwt_decode from "jwt-decode";
import moment from "moment";
import getUserBranchPermissions from "../../Components/getUserBranchPermissions";
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};
class Students extends React.Component {
  formRef = React.createRef();

  state = {
    initLoading: true,
    loading: false,
    data: [],
    students: [],
    batchModal: false,
    isUpdate: false,
    currentBatch: null,
    studentUploadModal: false,
    batchId: null,
    studentModal: false,
  };
  componentDidMount = () => {
    this.setState({ loading: true });

    let { id } = this.props.match.params;
    this.setState({ batchId: id });
    this.getBatchStudents(id);
  };
  getBatchStudents = (id) => {
    this.props
      .onGetAllBatchStudents(id)
      .then(() => this.setState({ loading: false }))

      .catch(() => this.setState({ loading: false }));
  };
  componentWillReceiveProps = (props, newprop) => {
    props.students?.users &&
      this.setState({
        students: props.students?.users,
        pages: props.students?.count,
        loading: false,
      });
  };
  createBatch = (values) => {
    const { isUpdate, currentBatch } = this.state;
    this.setState({ loading: true });

    let batchFunc = isUpdate
      ? this.props.onUpdateBatch
      : this.props.onCreateBatch;
    batchFunc({ ...values, id: currentBatch.id ? currentBatch.id : 0 })
      .then(() => {
        this.setState({ loading: false, batchModal: false }, () => {
          message.success(
            `Batch ${isUpdate ? "Updated" : "Created"} Successfully!`
          );
          this.getBatches();
        });
      })
      .catch(() => {
        this.setState({ loading: false });
        message.error("Some error occured!");
      });
  };
  columns = [
    {
      title: "Full Name",
      dataIndex: "fullName",
      key: "fullName",
      align: "center",
    },
    {
      title: "Email",
      dataIndex: "alternateEmail",
      key: "alternateEmail",
      align: "center",
    },

    {
      title: "Actions",
      align: "center",
      render: (item) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Button
            className="ts-send-btn"
            size="medium"
            style={{ marginLeft: 4, marginRight: 4 }}
            type="primary"
            key="list-loadmore-more"
            onClick={() => this.getImpersonInfo(item.id)}
          >
            Impersonate
          </Button>
          {/* <Button
            className="ts-send-btn"
            size="medium"
            style={{ marginLeft: 4, marginRight: 4 }}
            type="primary"
            key="list-loadmore-more"
            onClick={() => {
              this.setState({ loading: true });
              this.props
                .onRemoveStudentAccount({
                  id: item.id,
                  delete: true,
                  modifiedBy: localStorage.getItem("userId"),
                })
                .then(() => {
                  let { batchId } = this.state;
                  this.setState({ loading: false });
                  this.getBatchStudents(batchId);
                  message.success("Account removed!");
                })
                .catch(() => {
                  message.error("Could't removed account!");

                  this.setState({ loading: false });
                });
            }}
          >
            Delete
          </Button> */}
        </div>
      ),
    },
  ];
  enableUpdate = (item) => {
    this.setState(
      {
        isUpdate: true,
        batchModal: true,
        currentBatch: item,
      },
      () =>
        this.formRef.current.setFieldsValue({
          batchName: item.batchName,
          batchUniqueNumber: item.batchUniqueNumber,
          startDate: moment(item.startDate),
          endDate: moment(item.endDate),
        })
    );
  };
  uploadStudentModal = () => {
    const { studentUploadModal, loading } = this.state;
    return (
      <div>
        <Modal
          style={{ marginTop: "-75px" }}
          width="50%"
          visible={studentUploadModal}
          onCancel={() =>
            this.setState({ studentUploadModal: false }, () =>
              this.formRef?.current?.resetFields()
            )
          }
          footer={null}
          maskClosable={false}
          header={true}
          title={`Upload Accounts`}
        >
          <div style={{ width: "100%" }}>
            <div>
              <Form
                ref={this.formRef}
                {...layout}
                name="basic"
                onFinish={this.uploadBatchAccounts}
              >
                <Form.Item
                  name="file"
                  rules={[
                    {
                      required: false,
                      message: "Please select your file!",
                    },
                  ]}
                >
                  <Upload
                    beforeUpload={() => false}
                    onChange={this.onHandleChange}
                  >
                    <Button icon={<UploadOutlined />}></Button>
                  </Upload>
                </Form.Item>
                <Form.Item {...tailLayout} style={{ textAlign: "end" }}>
                  <Button
                    loading={loading}
                    disabled={loading}
                    type="primary"
                    htmlType="submit"
                  >
                    Upload
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
        </Modal>
      </div>
    );
  };
  onHandleChange = (e) => {
    this.setState({ file: e.fileList[0].originFileObj });
  };
  uploadBatchAccounts = () => {
    let formdata = new FormData();
    let { file, batchId } = this.state;
    formdata.append("file", file);
    formdata.append("BatchId", batchId);
    this.setState({ loading: true });
    this.props
      .onUploadBatchAccounts(formdata)
      .then((response) => {
        message.success("Accounts Imported Successfully!");
        this.setState({ studentUploadModal: false, loading: false }, () =>
          this.formRef?.current?.resetFields()
        );
        this.getBatchStudents(batchId);
      })
      .catch((error) => {
        this.setState({ loading: false });
        message.error("Could't import accounts!");
      });
  };

  studentAddModal = () => {
    const { studentModal, loading } = this.state;
    return (
      <div>
        <Modal
          style={{ marginTop: "-75px" }}
          width="50%"
          visible={studentModal}
          onCancel={() => this.setState({ studentModal: false })}
          footer={null}
          maskClosable={false}
          header={true}
          title={`Add Account`}
        >
          <div style={{ width: "100%" }}>
            <div>
              <Form
                ref={this.formRef}
                {...layout}
                name="basic"
                onFinish={this.addStudentAccount}
              >
                <Form.Item
                  colon={true}
                  labelAlign="left"
                  label="Full Name"
                  name="fullName"
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                    },
                  ]}
                >
                  {/* eslint-disable-next-line react/jsx-no-undef */}
                  <Input />
                </Form.Item>
                <Form.Item
                  colon={true}
                  labelAlign="left"
                  label="Email"
                  name="email"
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                    },
                  ]}
                >
                  {/* eslint-disable-next-line react/jsx-no-undef */}
                  <Input />
                </Form.Item>
                <Form.Item
                  colon={true}
                  labelAlign="left"
                  label="Password"
                  name="password"
                  style={{ width: "100%" }}
                  rules={[
                    {
                      required: true,
                    },
                  ]}
                >
                  {/* eslint-disable-next-line react/jsx-no-undef */}
                  <Input />
                </Form.Item>
                <Form.Item {...tailLayout} style={{ textAlign: "end" }}>
                  <Button
                    loading={loading}
                    disabled={loading}
                    type="primary"
                    htmlType="submit"
                  >
                    ADD
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
        </Modal>
      </div>
    );
  };

  addStudentAccount = (values) => {
    let { batchId } = this.state;
    this.setState({ loading: true });
    this.props
      .onAddBatchAccount([{ ...values, batchId: parseInt(batchId) }])
      .then(() => {
        message.success("Account added successfully!");
        this.setState({ loading: false, studentModal: false }, () =>
          this.getBatchStudents(batchId)
        );
      })
      .catch(() => {
        message.error("Account already exist!");
        this.setState({ loading: false });
      });
  };

  getImpersonInfo = (id) => {
    this.setState({ loading: true });
    this.props
      .onGetImpersonInfo(id)
      .then(async (res) => {
        let supervisorKeys = JSON.stringify(Object.entries(localStorage));
        this.setState({ loading: false });
        localStorage.clear();

        localStorage.removeItem("admintoken");
        localStorage.setItem("token", res.payload.accessToken);
        localStorage.setItem("refreshToken", res.payload.refreshToken);
        let token = localStorage.getItem("token");
        var jwtDecoded = jwt_decode(res.payload.accessToken);
        localStorage.setItem("userId", jwtDecoded.sub);
        localStorage.setItem("userEmail", jwtDecoded.email);
        localStorage.setItem("userOwner", jwtDecoded.IsOwner);
        localStorage.setItem("selectedBranchId", jwtDecoded.BranchId);
        localStorage.setItem("companyId", jwtDecoded.Com);
        localStorage.setItem("userName", jwtDecoded.FullName);
        localStorage.setItem("supervisorData", supervisorKeys);
        // this.props.getToken(token);
        let tokenData = localStorage.getItem("notificationToken");
        getUserBranchPermissions({
          userId: jwtDecoded.sub,
          branchId: jwtDecoded.BranchId,
        });
        this.props.onNotificationAdd(jwtDecoded.sub, tokenData).then((res) => {
          console.log("Notification token submitted");
        });
        this.props.history.push("/dashboardBI");
      })
      .catch(() => {
        this.setState({ loading: false });
      });
  };
  render() {
    const { loading, students, pages } = this.state;
    let { batchName } = this.props.match.params;

    return (
      <Spin spinning={loading} size="large">
        <div>
          <div style={{ display: "flex" }}>
            <div className="page-container">
              <div className="list-heading">
                <span className="header-text" style={{ fontWeight: "500" }}>
                  Student Accounts ({batchName})
                </span>
              </div>
              <div style={{ display: "flex" }}>
                <div className="file-notes-container supervisor-batches">
                  <div
                    style={{
                      display: "flex",
                      marginRight: 4,
                      justifyContent: "flex-end",
                    }}
                  >
                    <span>
                      <Button
                        className="ts-send-btn"
                        size="medium"
                        type="primary"
                        key="list-loadmore-more"
                        onClick={() =>
                          this.setState({ studentModal: true }, () =>
                            this.formRef?.current?.resetFields()
                          )
                        }
                        style={{ marginLeft: 4, marginRight: 4 }}
                      >
                        Add Account
                      </Button>
                    </span>
                    <span>
                      <Button
                        className="ts-send-btn"
                        size="medium"
                        type="primary"
                        key="list-loadmore-more"
                        style={{ marginLeft: 4, marginRight: 4 }}
                        onClick={() =>
                          this.setState({ studentUploadModal: true })
                        }
                      >
                        Import From Excel
                      </Button>
                    </span>
                  </div>
                  <Table
                    rowClassName={(record, index) =>
                      index % 2 === 0 ? "table-row-light" : "table-row-dark"
                    }
                    columns={this.columns}
                    dataSource={students}
                    pagination={{
                      defaultCurrent: 1,
                      total: pages,
                      showSizeChanger: true,
                      defaultPageSize: 5,
                      pageSizeOptions: ["5", "10", "15", "20", "50"],
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.uploadStudentModal()}
        {this.studentAddModal()}
      </Spin>
    );
  }
}

export default Students;
