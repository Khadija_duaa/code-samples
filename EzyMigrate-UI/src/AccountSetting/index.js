import React, { Fragment } from "react";
import ClientTags from "./ClientTags/ClientTags";
import DocumentTypes from "./DocumentTypes/DocumentTypes";
import ReminderSetting from "./ReminderSetting/ReminderSetting";

const Selection = ({
  onGetClientTag,
  clientTagRes,
  onAddClientTag,
  addClientTagRes,
  onRemoveClientTag,
  removeClientTagRes,

  onUpdetaClientTag,
  updateClientTagRes,

  onAddReminderSettings,
  addReminderSettingsRes,

  onGetReminderSettings,
  reminderSettingRes
}) => {
  return (
    <Fragment>
      <ClientTags
        onGetClientTag={onGetClientTag}
        clientTagRes={clientTagRes}
        onAddClientTag={onAddClientTag}
        addClientTagRes={addClientTagRes}
        onRemoveClientTag={onRemoveClientTag}
        removeClientTagRes={removeClientTagRes}
        onUpdetaClientTag={onUpdetaClientTag}
        updateClientTagRes={updateClientTagRes}
      />
      <ReminderSetting
        onAddReminderSettings={onAddReminderSettings}
        addReminderSettingsRes={addReminderSettingsRes}
        onGetReminderSettings={onGetReminderSettings}
        reminderSettingRes={reminderSettingRes}
      />
      {/* <ClientTagForm
        onAddClientTag={onAddClientTag}
        addClientTagRes={addClientTagRes}
      /> */}
      {/* <DocumentTypes
        onGetClientTag={onGetClientTag}
        clientTagRes={clientTagRes}
        onAddClientTag={onAddClientTag}
        addClientTagRes={addClientTagRes}
        onRemoveClientTag={onRemoveClientTag}
        removeClientTagRes={removeClientTagRes}
        onUpdetaClientTag={onUpdetaClientTag}
        updateClientTagRes={updateClientTagRes}
      /> */}
    </Fragment>
  );
};

export default Selection;
