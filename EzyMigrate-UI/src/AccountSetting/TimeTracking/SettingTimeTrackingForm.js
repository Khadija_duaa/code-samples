import React, { Fragment, useEffect } from "react";
import { Form, Input, Button, message } from "antd";

let clientprofileid = JSON.parse(
  window.localStorage.getItem("clientprofileid")
);

const SettingTimeTrackingForm = ({
  onAddClientTag,
  addClientTagRes,

  handleCancel,

  onGetClientTag,
  clientTagRes,

  setLoading,

  onUpdetaClientTag,
  updateClientTagRes,

  createMode,

  updatedata
}) => {
  // useEffect(() => {
  //   form.setFieldsValue({
  //     name: updatedata.name
  //   });
  // }, [form, updatedata.name]);
  const [form] = Form.useForm();
  const onFinish = values => {
    setLoading(true);
    console.log("Received values of form:", values);

    if (createMode === "add-tag") {
      const data = {
        subjectId: clientprofileid && clientprofileid,
        name: values.name,
        deletedDate: "2021-01-26T13:09:36.895Z",
        createdBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6"
      };
      onAddClientTag(data)
        .then(() => handleCancel())
        .then(() => onGetClientTag())
        .then(() => {
          setLoading(false);
          message.success("Successfully Added!");
        });
    } else {
      const updata = {
        id: updatedata.id,
        subjectId: updatedata && updatedata.subjectId,
        name: values.name,
        deletedDate: "2021-01-28T09:33:05.590Z",
        createdBy: updatedata && updatedata.createdBy
      };
       
      onUpdetaClientTag(updata)
        .then(() => handleCancel())
        .then(() => onGetClientTag())
        .then(() => {
          setLoading(false);
          message.success("Successfully Updated!");
        });
    }
  };

  return (
    <Fragment>
      <Form name="main" onFinish={onFinish} form={form}>
        <Form.Item name="name">
          <Input placeholder="Add New" />
        </Form.Item>
        <Form.Item>
          <Button className="form-btn" type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Fragment>
  );
};
export default SettingTimeTrackingForm;
