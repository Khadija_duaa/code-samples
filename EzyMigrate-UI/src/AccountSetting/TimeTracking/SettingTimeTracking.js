import React, { useState, Fragment, useEffect } from "react";
import {
  DeleteOutlined,
  EditOutlined,
  LeftCircleFilled,
} from "@ant-design/icons";

import HeaderBar from "../../Components/Header/HeaderBar";
import Sidebar from "../../Components/SideBar";
import DocumentTypesForm from "./SettingTimeTrackingForm";
import { Images } from "../../Themes";
import { Modal, Spin, Form, Input, Button, Select } from "antd";
import { useLocation } from "react-router-dom";
import history from "../../services/history";

const SettingTimeTracking = ({
  onGetTimeTracking,
  timeTrackingRes,
  onAddTimeTracking,
  onSetActiveInnerTab,
}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [createMode, setCreateMode] = useState("");
  const [loading, setLoading] = useState(false);
  const [updatedata, setUpdatedata] = useState({});

  useEffect(() => {
    setLoading(true);
    onGetTimeTracking()
      .then(() => {
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });
  }, [onGetTimeTracking]);

  // const removeTag = (id) => {
  //   setLoading(true);
  //
  //   const remove = {
  //     id: id,
  //     delete: true,
  //     modifiedBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  //   };
  //
  //   onRemoveClientTag(remove).then(() => {
  //     onGetClientTag().then(() => {
  //       setLoading(false);
  //       message.success("Successfully Deleted!");
  //     });
  //   });
  // };

  const showModal = (value) => {
    setIsModalVisible(true);
    setCreateMode(value);
    setUpdatedata(value);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <Fragment>
      <div>
        <div style={{ display: "flex" }}>
          <div style={{ width: "100%", height: "100%" }}>
            <div
              className="d-flex align-item Reminder-top"
              style={{ justifyContent: "space-between" }}
            >
              <div className="d-flex align-item">
                <div className="client-tag-top">
                  <img src={Images.timeTracking} className="sus-bottom-icon" />
                </div>
                <span className="top-text">TIME TRACKING</span>
              </div>
              <LeftCircleFilled
                onClick={() => onSetActiveInnerTab("")}
                className="ac-back-icon"
              />
            </div>
            <div className="time-tracking">
              <div className="client-tag-form"></div>
              <div className="width-52">
                <div className="client-tag-table">
                  <Form>
                    <div className="reminder-set-form margin-top-34">
                      <h3>TIME TRACKING</h3>
                      <Form.Item className="d-block">
                        <Select>
                          <Select.Option key="1">
                            Show Time Tracking Popup On Task Completed
                          </Select.Option>
                          <Select.Option key="2">
                            Don't Show Time Tracking Popup On Task Completed
                          </Select.Option>
                        </Select>
                        <span>left in client's passport expiry</span>
                      </Form.Item>
                    </div>

                    <div className="reminder-set-form margin-top-34">
                      <Form.Item className="d-block">
                        <Button className="button-blue" type="primary">
                          UPDATE
                        </Button>
                      </Form.Item>
                    </div>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default SettingTimeTracking;
