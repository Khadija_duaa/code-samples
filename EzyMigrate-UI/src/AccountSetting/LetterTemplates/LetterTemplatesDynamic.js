import React, { useState, Fragment, useEffect } from "react";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

import HeaderBar from "../../Components/Header/HeaderBar";
import Sidebar from "../../Components/SideBar";
import DocumentTypesForm from "./LetterTemplatesForm";
import { Images } from "../../Themes";
import { apiRefresh } from "../../services/api";
import * as types from "../../store/Constants";
import { Table, Modal, Spin, message, Checkbox } from "antd";

const LetterTemplatesDynamic = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [createMode, setCreateMode] = useState("");
  const [loading, setLoading] = useState(false);
  const [updatedata, setUpdatedata] = useState({});
  const [keys, setKeys] = useState([]);

  useEffect(() => {
    setLoading(true);
    const VisaOpt = {
      url: `v1/config/DynamicKeys/All`,
    };
    VisaOpt.types = [
      types.GET_DOCUMENT_TYPE_SUCCESS,
      types.GET_DOCUMENT_TYPE_FAILURE,
    ];

    apiRefresh.get(VisaOpt).then((res) => {
      setLoading(false);
      setKeys(res.items);
    });
  }, []);

  const columns = [
    {
      title: "Key",
      key: "key",
      dataIndex: "key",
    },
    {
      title: "Value",
      key: "keyDsecription",
      dataIndex: "keyDsecription",
    },
  ];
  return (
    <Fragment>
      <div>
        <div style={{ display: "flex" }}>
          <div className="dynamicForm">
            <div className="">
              <div className="client-tag-form"></div>
              <div>
                <div className="client-tag-table">
                  <Spin size="large" spinning={loading}>
                    <Table
                      rowClassName={(record, index) =>
                        index % 2 === 0 ? "table-row-light" : "table-row-dark"
                      }
                      columns={columns}
                      dataSource={keys}
                      pagination={false}
                    />
                  </Spin>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default LetterTemplatesDynamic;
