import React, { Fragment, useEffect, useState } from "react";
import { Form, Input, Button, message, Radio } from "antd";
import * as types from "../../store/Constants";
import { apiRefresh } from "../../services/api";
import FroalaEditorCom from "../../Containers/FroalaEditorCom";

let clientprofileid = JSON.parse(
  window.localStorage.getItem("clientprofileid")
);

let selectedBranchId = localStorage.getItem("selectedBranchId");

const LetterTemplatesForm = ({
  handleCancel,
  setLoading,

  createMode,

  updatedata,

  onGetLetterTemplates,

  onAddLetterTemplates,

  onUpdetaLetterTemplates,
  onGetDocumentDownload,
}) => {
  const [letterString, setLetterString] = useState("");

  const [form] = Form.useForm();
  const onFinish = (values) => {
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    setLoading(true);
    var header = false;
    if (values.header == 1) {
      header = true;
    }
    if (createMode === "add-letter-template") {
      const data = {
        id: 0,
        type: "LETTER",
        name: values.name,
        content: letterString,
        createdBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        createdDate: "2021-02-24T07:32:20.831Z",
        modifiedBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        modifiedDate: "2021-02-24T07:32:20.831Z",
        deletedDate: "2021-02-24T07:32:20.831Z",
        branchId: selectedBranchId && selectedBranchId,
        header: header,
        defaultContract: true,
      };

      onAddLetterTemplates(data)
        .then(() => handleCancel())
        .then(() => onGetLetterTemplates())
        .then(() => {
          setLoading(false);
          message.success("Successfully Added!");
        });
    } else {
      const update = {
        id: updatedata.id,
        type: "LETTER",
        name: values.name,
        content: letterString,
        branchId: selectedBranchId && selectedBranchId,
        header: header,
        defaultContract: true,
      };

      onUpdetaLetterTemplates(update)
        .then(() => handleCancel())
        .then(() => onGetLetterTemplates())
        .then(() => {
          setLoading(false);
          message.success("Successfully Updated!");
        });
    }
  };
  useEffect(() => {
    if (updatedata) {
      var Header = 2;
      if (updatedata.header == true) {
        Header = 1;
      }

      form.setFieldsValue({
        name: updatedata.name,
        header: Header,
      });
      setLetterString(updatedata.content);
    }
  }, [updatedata]);
  const openPDF = () => {
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    var formData = new FormData();
    formData.append(`Html`, letterString);
    formData.append(`FileTitle`, "Letter");
    formData.append(`BranchId`, selectedBranchId)
    const docTypeOpt = {
      url: `v1/config/GetPdf`,
    };

    docTypeOpt.types = [
      types.GET_DOCUMENT_TYPE_SUCCESS,
      types.GET_DOCUMENT_TYPE_FAILURE,
    ];
    apiRefresh.post(docTypeOpt, formData).then((res) => {


      const docViewOpt = {
        url: `v1/document/GetDocumentBytesforAttachment/${res.uri}/${selectedBranchId}`,
      };

      docViewOpt.types = [
        types.GET_DOCUMENT_PREVIEW_SUCCESS,
        types.GET_DOCUMENT_PREVIEW_FAILURE,
      ];
      apiRefresh.getFile(docViewOpt).then((response) => {
        const file = new Blob([response], { type: "application/pdf" });
        const fileURL = URL.createObjectURL(file);
        const pdfWindow = window.open();
        pdfWindow.location.href = fileURL;
      })
    });
  };

  return (
    <Fragment>
      <Form
        className="width-100 border-box padding-20"
        name="main"
        onFinish={onFinish}
        form={form}
      >
        <div className="width-100 d-flex align-center letter-font">
          <div className="w-20 ">
            <p>Name</p>
          </div>
          <div className="w-80">
            <Form.Item
              name="name"
              rules={[{ required: true, message: "Required!" }]}
            >
              <Input placeholder="Add New" />
            </Form.Item>
          </div>
        </div>
        <div className="width-100 d-flex align-center letter-font letter-spacing">
          <div className="w-20 ">
            <p>Show Header</p>
          </div>
          <div className="w-80">
            <Form.Item name="header">
              <Radio.Group defaultValue={2}>
                <Radio value={1}>Yes</Radio>
                <Radio value={2}>No</Radio>
              </Radio.Group>
            </Form.Item>
          </div>
        </div>
        <Form.Item className="" name="content">
          <div className="width-100 d-flex align-center letter-font letter-spacing">
            <div className="w-20">
              <p>Contents</p>
            </div>
            <div className="w-80 letter-froala">
              <FroalaEditorCom
                setLoading={(value) => setLoading(value)}
                model={letterString}
                onModelChange={(value) => setLetterString(value)}
              />
            </div>
          </div>
        </Form.Item>
        <Form.Item className="d-flex">
          <Button
            className="form-btn button-blue"
            type="primary"
            htmlType="submit"
          >
            Submit
          </Button>
          <div className="margin-btns">
            <Button
              className="form-btn button-blue"
              onClick={() => openPDF()}
              type="primary"
            >
              Preview
            </Button>
          </div>
          <div className="margin-btns">
            <Button
              onClick={() => handleCancel()}
              className="form-btn button-blue"
              type="primary"
            >
              Cancel
            </Button>
          </div>
        </Form.Item>
        <Form.Item></Form.Item>
      </Form>
    </Fragment>
  );
};
export default LetterTemplatesForm;
