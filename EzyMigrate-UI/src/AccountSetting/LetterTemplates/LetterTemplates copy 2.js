import React, {
  useState,
  useCallback,
  useRef,
  Fragment,
  useEffect
} from "react";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

import HeaderBar from "../../Components/Header/HeaderBar";
import Sidebar from "../../Components/SideBar";
import LetterTemplatesForm from "./LetterTemplatesForm";
import { Images } from "../../Themes";
import { Table, Modal, Spin, message } from "antd";
import LetterTemplatesDynamic from "./LetterTemplatesDynamic";
import { DndProvider, useDrag, useDrop } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import update from "immutability-helper";
import * as jsx from "react/jsx-runtime";

const type = "DragableBodyRow";

const DragableBodyRow = ({
  index,
  moveRow,
  className,
  style,
  ...restProps
}) => {
  const ref = React.useRef();
  const [{ isOver, dropClassName }, drop] = useDrop(
    () => ({
      accept: type,
      collect: monitor => {
        const { index: dragIndex } = monitor.getItem() || {};
        if (dragIndex === index) {
          return {};
        }
        return {
          isOver: monitor.isOver(),
          dropClassName:
            dragIndex < index ? " drop-over-downward" : " drop-over-upward"
        };
      },
      drop: item => {
        moveRow(item.index, index);
      }
    }),
    [index]
  );
  const [, drag] = useDrag(
    () => ({
      item: { type, index },
      collect: monitor => ({
        isDragging: monitor.isDragging()
      })
    }),
    []
  );
  drop(drag(ref));
  return (
    <tr
      ref={ref}
      className={`${className}${isOver ? dropClassName : ""}`}
      style={{ cursor: "move", ...style }}
      {...restProps}
    />
  );
};

const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Age",
    dataIndex: "age",
    key: "age"
  },
  {
    title: "Address",
    dataIndex: "address",
    key: "address"
  }
];

// import {
//   sortableContainer,
//   sortableElement,
//   sortableHandle,
// } from "react-sortable-hoc";
// const { sortableContainer, sortableElement, sortableHandle } = ReactSortableHoc;
// const DraggableContainer = sortableContainer(({ children }) => children);
// const DraggableElement = sortableElement(({ children }) => children);
// const DraggableHandle = sortableHandle(({ children }) => children);

const LetterTemplates = ({
  onGetLetterTemplates,
  LetterTemplatesRes,

  onAddLetterTemplates,

  onRemoveLetterTemplates,

  onUpdetaLetterTemplates
}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [createMode, setCreateMode] = useState("");
  const [loading, setLoading] = useState(false);
  const [updatedata, setUpdatedata] = useState({});
  const [expandedRowKeys, setExpandedRowKeys] = useState("");
  const [data, setData] = useState([
    {
      key: "1",
      name: "John Brown",
      age: 32,
      address: "New York No. 1 Lake Park"
    },
    {
      key: "2",
      name: "Jim Green",
      age: 42,
      address: "London No. 1 Lake Park"
    },
    {
      key: "3",
      name: "Joe Black",
      age: 32,
      address: "Sidney No. 1 Lake Park"
    }
  ]);

  const components = {
    body: {
      row: DragableBodyRow
    }
  };

  const moveRow = useCallback(
    (dragIndex, hoverIndex) => {
      const dragRow = data[dragIndex];
      setData(
        update(data, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragRow]
          ]
        })
      );
    },
    [data]
  );

  useEffect(() => {
     
    setLoading(true);
    onGetLetterTemplates().then(() => {
      setLoading(false);
    });
  }, [onGetLetterTemplates]);

  const removeTag = id => {
    setLoading(true);
     
    const remove = {
      id: id
    };
     
    onRemoveLetterTemplates(remove).then(() => {
      onGetLetterTemplates().then(() => {
        setLoading(false);
        message.success("Successfully Deleted!");
      });
    });
  };

  if (LetterTemplatesRes && LetterTemplatesRes.items) {
    var findTemplate = LetterTemplatesRes.items.filter(
      obj => obj.type === "LETTER"
    );
  }

  const showModal = (value, modelType) => {
     
    setIsModalVisible(value);
    setCreateMode(modelType);
    setUpdatedata(value);
  };

  // const handleOk = () => {
  //   setIsModalVisible(false);
  // };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  // const onTableRowExpand = (expanded, record) => {
  //   var keys = [];
  //   if (expanded) {
  //     keys.push(record.id); // I have set my record.id as row key. Check the documentation for more details.
  //   }

  //   setExpandedRowKeys(keys);
  // };

  // const columns = [
  //   {
  //     title: "Name",
  //     dataIndex: "name",
  //     key: "name",
  //     render: (text, record) => {
  //        
  //       return record && record.type === "LETTER" ? record.name : null;
  //     },
  //   },
  //   {
  //     title: "Action",
  //     dataIndex: "action",
  //     render: (text, record) => {
  //       return (
  //         <div className="table-action">
  //           <EditOutlined onClick={() => showModal(record, "edit-value")} />
  //           <DeleteOutlined onClick={() => removeTag(record && record.id)} />
  //         </div>
  //       );
  //     },
  //   },
  // ];

  // const Handle = styled.div`
  //   flex: none;
  //   width: 7.5px;
  //   height: 100%;

  //   &::before {
  //     content: "";
  //     border-left: 4px dotted #ccc;
  //     display: block;
  //     height: 20px;
  //     margin: 15px 3px;
  //   }

  //   &:hover::before {
  //     border-color: #888;
  //   }
  // `;

  // const Row = ({ key, index, children, ...rest }) => (
  //   <DraggableElement key={key} index={index}>
  //     <div {...rest}>
  //       <DraggableHandle>
  //         <Handle />
  //       </DraggableHandle>
  //       {children}
  //     </div>
  //   </DraggableElement>
  // );

  // const rowProps = ({ rowIndex }) => ({
  //   tagName: Row,
  //   index: rowIndex,
  // });

  // class DraggableTable extends React.PureComponent {
  //   state = {
  //     data: this.props.data,
  //   };

  //   table = React.createRef();

  //   getContainer = () => {
  //     return this.table.current.getDOMNode().querySelector(".BaseTable__body");
  //   };
  //   getHelperContainer = () => {
  //     return this.table.current.getDOMNode().querySelector(".BaseTable__table");
  //   };
  //   rowProps = (args) => {
  //     // don't forget to passing the incoming rowProps
  //     const extraProps = callOrReturn(this.props.rowProps);
  //     return {
  //       ...extraProps,
  //       tagName: Row,
  //       index: args.rowIndex,
  //     };
  //   };
  //   handleSortEnd = ({ oldIndex, newIndex }) => {
  //     const data = [...this.state.data];
  //     const [removed] = data.splice(oldIndex, 1);
  //     data.splice(newIndex, 0, removed);
  //     this.setState({ data });
  //   };
  // }

  return (
    <Fragment>
      <div>
        <div style={{ display: "flex" }}>
          <div className="client-tag">
            <div className="d-flex align-item client-top">
              <div className="client-tag-top">
                <img src={Images.letterTemplate} className="sus-bottom-icon" />
              </div>
              <span to="/client-tags" className="top-text">
                Letter Templates
              </span>
            </div>
            <div className="w-80">
              <div className="client-tag-form"></div>
              <div>
                <div className="client-tag-table">
                  <div className="d-end space-between">
                    <div className="add-tag-btn">
                      <>
                        <img
                          src={Images.addIcon}
                          className="icons-client"
                          type="primary"
                          onClick={() =>
                            showModal(!isModalVisible, "add-letter-template")
                          }
                        />
                      </>
                    </div>
                    {isModalVisible && (
                      <LetterTemplatesForm
                        onAddLetterTemplates={onAddLetterTemplates}
                        handleCancel={handleCancel}
                        onGetLetterTemplates={onGetLetterTemplates}
                        setLoading={setLoading}
                        onUpdetaLetterTemplates={onUpdetaLetterTemplates}
                        createMode={createMode}
                        updatedata={updatedata}
                      />
                    )}
                  </div>
                  <Spin size="large" spinning={loading}>
                    <DndProvider backend={HTML5Backend}>
                      <Table
                        columns={columns}
                        dataSource={data}
                        components={components}
                        onRow={(record, index) => ({
                          index,
                          moveRow
                        })}
                      />
                    </DndProvider>
                    {/* <DraggableContainer
                      useDragHandle
                      getContainer={this.getContainer}
                      helperContainer={this.getHelperContainer}
                      onSortEnd={this.handleSortEnd}
                    >
                      <Table
                        {...this.props}
                        ref={this.table}
                        data={this.state.data}
                        fixed={false}
                        rowProps={rowProps}
                      />
                    </DraggableContainer> */}
                  </Spin>
                </div>
              </div>
              <div className="top-60">
                <h3>DYNAMIC KEYS</h3>
                <div className="dynamic-keys">
                  <p>
                    You can use the following keys in the contracts and letters
                    to get the respective value for each client
                  </p>
                </div>
                <div>
                  <LetterTemplatesDynamic />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default LetterTemplates;
