import React, { useState, Fragment, useEffect } from "react";
import {
  DeleteOutlined,
  EditOutlined,
  LeftCircleFilled,
} from "@ant-design/icons";

import HeaderBar from "../../../Components/Header/HeaderBar";
import Sidebar from "../../../Components/SideBar";
import { Images } from "../../../Themes";
import {
  Modal,
  Spin,
  Form,
  Select,
  message,
  Checkbox,
  Button,
  Tooltip,
} from "antd";
import { RightCircleTwoTone } from "@ant-design/icons";
import { useLocation } from "react-router-dom";
import history from "../../../services/history";

const DailyEmailSettings = ({
  onGetEmailSetting,
  emailSetRes,
  onUpdateEmailSetting,
  onSetActiveInnerTab,
}) => {
  const [loading, setLoading] = useState(false);
  const [dailyTaskEmail, setDailyTaskEmail] = useState();
  const [sendNotification, setSendNotification] = useState();
  const [toClient, setToClient] = useState();
  const [emailImport, setEmailImport] = useState();
  const [dailyMeeting, setDailyMeeting] = useState();
  const state = useLocation().state;

  useEffect(() => {
    setLoading(true);
    onGetEmailSetting()
      .then(() => {
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });
  }, [onGetEmailSetting]);

  const [form] = Form.useForm();
  const onCheckMailChange = (values) => {
    setLoading(true);
    console.log("Received values of form:", values);
    const data = {
      userId: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      branchId: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      dailyTaskEmail: dailyTaskEmail,
      emailImportNotification: emailImport,
      processingPersonNotification: toClient,
      processingPersonNotificationPotential: sendNotification,
      dailyMeetingEmail: dailyMeeting,
    };
    onUpdateEmailSetting(data).then(() => {
      onGetEmailSetting()
        .then(() => {
          setLoading(false);
          message.success("Successfully Updated!");
        })
        .catch((err) => {
          setLoading(false);
        })
        .catch((error) => {
          setLoading(false);
        });
    });
  };

  const onDailyTaskEmailChange = (e) => {
    console.log("checked = ", e.target.checked);
    setDailyTaskEmail(e.target.checked);
  };
  const ondailyMeetingEmailChange = (e) => {
    console.log("checked = ", e.target.checked);
    setDailyMeeting(e.target.checked);
  };
  const onSendNotificationChange = (e) => {
    console.log("checked = ", e.target.checked);
    setSendNotification(e.target.checked);
  };
  const onToClientChange = (e) => {
    console.log("checked = ", e.target.checked);
    setToClient(e.target.checked);
  };
  const onEmailImportNotificationChange = (e) => {
    console.log("checked = ", e.target.checked);
    setEmailImport(e.target.checked);
  };
  return (
    <Fragment>
      <div>
        <div style={{ display: "flex" }}>
          <div style={{ width: "100%", height: "100%" }}>
            <div
              className="d-flex align-item Reminder-top"
              style={{ justifyContent: "space-between" }}
            >
              <div className="d-flex align-item">
                <div className="client-tag-top">
                  <img
                    src={Images.dailyMailSetting}
                    className="sus-bottom-icon"
                  />
                </div>
                <span className="top-text">Daily Mail Setting</span>
                <Tooltip
                  placement="topLeft"
                  title={`This is where you can update your daily email notifications that Ezymigrate sends you.`}
                >
                  <img className="ac-info-icon" src={Images.info} />
                </Tooltip>
              </div>
              <LeftCircleFilled
                onClick={() => onSetActiveInnerTab("")}
                className="ac-back-icon"
              />
            </div>
            <div className="default-font-box">
              <div>
                <div className="client-tag-table">
                  <Form
                    form={form}
                    onChange={onCheckMailChange}
                    className="d-flex space-between"
                  >
                    <div className="reminder-set-form margin-top-34">
                      <div className="margin-top">
                        <Form.Item>
                          <Checkbox
                            // checked={this.state.checked}
                            // disabled={this.state.disabled}
                            onChange={onDailyTaskEmailChange}
                          >
                            Send daily email for assigned task(s)
                          </Checkbox>
                        </Form.Item>
                      </div>
                      <div className="margin-top">
                        <Form.Item>
                          <Checkbox
                            // checked={this.state.checked}
                            // disabled={this.state.disabled}
                            onChange={ondailyMeetingEmailChange}
                          >
                            Send Daily Email For Meetings
                          </Checkbox>
                        </Form.Item>
                      </div>
                      <div className="margin-top">
                        <Form.Item>
                          <Checkbox
                            // checked={this.state.checked}/
                            // disabled={this.state.disabled}
                            onChange={onSendNotificationChange}
                          >
                            Send notification when a potential client file is
                            assigned to a processing person
                          </Checkbox>
                        </Form.Item>
                      </div>
                      <div className="margin-top">
                        <Form.Item>
                          <Checkbox
                            // checked={this.state.checked}
                            // disabled={this.state.disabled}
                            onChange={onToClientChange}
                          >
                            Send notification when a client file is assigned to
                            a processing person
                          </Checkbox>
                        </Form.Item>
                      </div>
                      <div className="margin-top">
                        <Form.Item>
                          <Checkbox
                            // checked={this.state.checked}
                            // disabled={this.state.disabled}
                            onChange={onEmailImportNotificationChange}
                          >
                            Send notification on email import if you are a
                            processing person
                          </Checkbox>
                        </Form.Item>
                      </div>
                    </div>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default DailyEmailSettings;
