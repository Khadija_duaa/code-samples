import React, { Fragment, useEffect } from "react";
import { Form, Input, Button, message } from "antd";

let clientprofileid = JSON.parse(
  window.localStorage.getItem("clientprofileid")
);

const DocumentTypesForm = ({
  onAddClientTag,
  addClientTagRes,

  handleCancel,

  onGetClientTag,
  clientTagRes,

  setLoading,

  onUpdetaClientTag,
  updateClientTagRes,

  createMode,

  updatedataRes,

  onGetDocumentType,

  onAddDocumentType,

  onUpdetaDocumentType,

  checkbox
}) => {
  // useEffect(() => {
  //   form.setFieldsValue({
  //     name: updatedataRes.name
  //   });
  // }, [form, updatedataRes]);
  const [form] = Form.useForm();
  const onFinish = values => {
    

    if (createMode === "add-document") {
      var selectedBranchId = localStorage.getItem("selectedBranchId");
      const data = {
        branchId: selectedBranchId,
        name: values.name,
        createdBy: "13f0ede9-a591-47a1-9064-f299a666ca58"
      };
       
      onAddDocumentType(data)
        .then(() => handleCancel())
        .then(() => onGetDocumentType())
        .then(() => {
          message.success("Successfully Added!");
        });
    } else {
      
      setLoading(true);
      var selectedBranchId = localStorage.getItem("selectedBranchId");
      const update = {
        id: updatedataRes.id,
        branchId: selectedBranchId,
        name: values.name,
        modifiedBy: "13f0ede9-a591-47a1-9064-f299a666ca58"
      };
       
      onUpdetaDocumentType(update)
        .then(() => handleCancel())
        .then(() => onGetDocumentType())
        .then(() => {
          setLoading(false);
          message.success("Successfully Updated!");
        });
    }
  };

  return (
    <Fragment>
      <Form name="main" onFinish={onFinish} form={form}>
        <Form.Item name="name">
          <Input placeholder="Add New" />
        </Form.Item>
        <Form.Item>
          <Button className="form-btn" type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Fragment>
  );
};
export default DocumentTypesForm;
