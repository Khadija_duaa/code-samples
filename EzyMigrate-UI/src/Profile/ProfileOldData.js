import React, { useState } from "react";
import { Link } from "react-router-dom";
import DatePicker from "react-date-picker";
import Sidebar from "../Components/SideBar";
import { Form, Input, Button, Select, message, Upload, Spin } from "antd";
import moment from "moment";

import HeaderBar from "../Components/Header/HeaderBar";
import ProfileSideBar from "../Components/ProfileSideBar";
import "./ProfileStyles.css";
import crossGreen from "../svgs/cross-green.svg";
import HeaderBarTabs from "../Components/Header/HeaderTabs";
import { Images } from "../Themes";
import ProfileTopBarTabs from "../Components/Shared/ProfileTopBar";
import ProgressBar from "../Components/Shared/Progressbar";

import ProcessingPerson from "../Components/Client/ProcessingPerson";
import Connections from "../Components/Client/Connections";
import PersonalInformation from "../Components/Client/PersonalInformation";
import CurrentVisa from "../Components/Client/CurrentVisa";
import Medicals from "../Components/Client/Medicals";
import Passport from "../Components/Client/Passport";
import InzLogin from "../Components/Client/InzLogin";
import NZQADetail from "../Components/Client/NZQADetail";
import BillingAddress from "../Components/Client/BillingAddress";

import ClientProfileActions from "../Redux/ClientRedux/ClientProfile/ClientProfileRedux";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const headOption = [
  {
    tabName: "Client Profile",
    imageName: Images.clientProfile,
    linkName: "/profile",
  },
  { tabName: "Visas", imageName: Images.visas, linkName: "/visa-flow" },
  { tabName: "Admission", imageName: Images.admission, linkName: "/admission" },
  { tabName: "Documents", imageName: Images.documents, linkName: "/documents" },
  { tabName: "Email", imageName: Images.email, linkName: "" },
  {
    tabName: "Activities",
    imageName: Images.activities,
    linkName: "/activities",
  },
  {
    tabName: "File Notes",
    imageName: Images.documents,
    linkName: "/file-notes",
  },
  {
    tabName: "Accounts",
    imageName: Images.accounts,
    linkName: "/client-account",
  },
  {
    tabName: "Open Case Management",
    imageName: Images.caseManagement,
    linkName: "/Case-management",
  },
  { tabName: "Questionnaire", imageName: Images.questionnare, linkName: "" },
  { tabName: "Chat", imageName: Images.supplier, linkName: "" },
  { tabName: "Print Case", imageName: Images.print, linkName: "" },
];

const topBar = [
  { tabName: "CLIENT INFORMATION", linkName: "/profile" },
  { tabName: "EMPLOYER INFORMATION", linkName: "/employer" },
  { tabName: "JOB HISTORY", linkName: "/job-history" },
  { tabName: "QUALIFICATION", linkName: "/qualification" },
  { tabName: "OTHER INFO", linkName: "" },
];

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      selectedOption: null,
      date: new Date(),
      signedClicked: false,
      onshore: true,
      active: true,
      allowUpdate: false,
      areaAccess: false,
      updLoading: false,
      addClientImageUrl: "",
      loadUploadImage: false,
      showClient: false,
    };

    this.props.onGetClientProfile();
    this.props.onGetAllClient();
    this.props.onGetGroups();
    // this.props.onGetGroupMembers();
    this.props.onGetCountries();
    this.props.onGetTeamMember();
    this.props.onGetAccessingAuth();
    this.props.onGetVisaType();
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    let data = [
      {
        clientId: clientprofileid,
        authority: 0,
        submittedDate: "2020-12-09T18:21:04.061Z",
        returnedDate: "2020-12-09T18:21:04.061Z",
        refNumber: "1234",
        createdBy: "4c08dfd5-dd40-c6cc-c58c-d6a9d19d0192",
      },
    ];
    // this.props.onAddAccAuthorities(data)
  }

  componentDidUpdate(PrevProps) {
    if (PrevProps.clientProfileData !== this.props.clientProfileData) {
      if (!PrevProps.clientProfileData) {
        for (var i = 0; i < this.props.clientProfileData.groups.length; i++) {
          this.props.onGetGroupMembers(
            this.props.clientProfileData.groups[i].groupId
          );
        }
      }
      this.setState({
        addClientImageUrl: this.props.clientProfileData.imageBlobUrl,
      });
      if (this.props.clientProfileData.clientPermission) {
        this.setState({
          signed: this.props.clientProfileData.clientPermission.signed,
          onshore: this.props.clientProfileData.clientPermission.onshore,
          active: this.props.clientProfileData.clientPermission.active,
          allowUpdate: this.props.clientProfileData.clientPermission
            .allowUpdate,
          areaAccess: this.props.clientProfileData.clientPermission.areaAccess,
        });
      }
      var email = "";
      var secondaryEmail = "";
      var otherEmail = "";
      if (this.props.clientProfileData.emails.length > 0) {
        for (var i = 0; i < this.props.clientProfileData.emails.length; i++) {
          if (this.props.clientProfileData.emails[i].emailTypeId === 1) {
            email = this.props.clientProfileData.emails[i].address;
          }
          if (this.props.clientProfileData.emails[i].emailTypeId === 2) {
            secondaryEmail = this.props.clientProfileData.emails[i].address;
          }
          if (this.props.clientProfileData.emails[i].emailTypeId === 3) {
            otherEmail = this.props.clientProfileData.emails[i].address;
          }
        }
      }
      var billAddressData = null;
      var clientAddressData = null;
      if (this.props.clientProfileData.addresses.length > 0) {
        const findBillAddress = this.props.clientProfileData.addresses.find(
          (obj) => obj.addressTypeId === 2
        );
        if (findBillAddress) {
          billAddressData = {
            contactPerson: findBillAddress.contactPerson,
            flat: findBillAddress.flat,
            streetNumber: findBillAddress.streetNumber,
            suburb: findBillAddress.suburb,
            city: findBillAddress.city,
            country: findBillAddress.country,
            zipcode: findBillAddress.zip,
          };
        }

        const findAddress = this.props.clientProfileData.addresses.find(
          (obj) => obj.addressTypeId === 1
        );
        if (findAddress) {
          clientAddressData = {
            address: findAddress.city,
          };
        }
      }
      var medicalData = null;
      if (this.props.clientProfileData.medicals.length > 0) {
        medicalData = {
          medicalIssueDate: this.props.clientProfileData.medicals[0]
            .medicalIssueDate,
          medicalExpiryDate: this.props.clientProfileData.medicals[0]
            .medicalExpiryDate,
          medicalGrading: this.props.clientProfileData.medicals[0]
            .medicalGrading,
          xrayIssueDate: this.props.clientProfileData.medicals[0].xrayIssueDate,
          xrayExpiryDate: this.props.clientProfileData.medicals[0]
            .xrayExpiryDate,
          xrayGrading: this.props.clientProfileData.medicals[0].xrayGrading,
          medicalNotes: this.props.clientProfileData.medicals[0].medicalNotes,
          medicalNotesDetail: this.props.clientProfileData.medicals[0]
            .medicalNotesDetail,
        };
      }
      var phoneData = {
        mobile: "",
        secondaryMobile: "",
        overseasMobile: "",
        landLine: "",
        otherMobile: "",
      };
      if (this.props.clientProfileData.phones.length > 0) {
        const findMobile = this.props.clientProfileData.phones.find(
          (obj) => obj.phoneTypeId === 1
        );
        if (findMobile) {
          phoneData.mobile = findMobile.contact;
        }
        const findSecondaryMobile = this.props.clientProfileData.phones.find(
          (obj) => obj.phoneTypeId === 2
        );
        if (findSecondaryMobile) {
          phoneData.secondaryMobile = findSecondaryMobile.contact;
        }
        const findOverseasMobile = this.props.clientProfileData.phones.find(
          (obj) => obj.phoneTypeId === 3
        );
        if (findOverseasMobile) {
          phoneData.overseasMobile = findOverseasMobile.contact;
        }
        const findLandLineMobile = this.props.clientProfileData.phones.find(
          (obj) => obj.phoneTypeId === 4
        );
        if (findLandLineMobile) {
          phoneData.landLine = findLandLineMobile.contact;
        }
        const findOtherMobile = this.props.clientProfileData.phones.find(
          (obj) => obj.phoneTypeId === 5
        );
        if (findOtherMobile) {
          phoneData.otherMobile = findOtherMobile.contact;
        }
      }
      // this.formRef.current.setFieldsValue({
      //   visaCountryId: this.props.clientProfileData.visaCountryId.toString(),
      //   visaCountryType: this.props.clientProfileData.visaCountyType.toString(),
      //   title: this.props.clientProfileData.title,
      //   middleName: this.props.clientProfileData.middleName,
      //   gender: this.props.clientProfileData.gender,
      //   dateOfBirth: moment(this.props.clientProfileData.dateOfBirth),
      //   address: clientAddressData.address,
      //   nationalityId: this.props.clientProfileData.nationalityId,
      //   saleDate: moment(this.props.clientProfileData.saleDate),
      //   sourceId: this.props.clientProfileData.sourceId.toString(),
      //   jobSectorId: this.props.clientProfileData.jobSectorId.toString(),
      //   companyOptional: this.props.clientProfileData.companyOptional,
      //   clientSerial: this.props.clientProfileData.clientSerial,
      //   nationalityCountry: this.props.clientProfileData.nationalityCountry,
      //   firstName: this.props.clientProfileData.firstName,
      //   lastName: this.props.clientProfileData.lastName,
      //   age: this.props.clientProfileData.age,
      //   dealWorth: this.props.clientProfileData.dealWorth,
      //   maritalStatus: this.props.clientProfileData.maritalStatus,
      //   dependentChildren: this.props.clientProfileData.dependentChildren,
      //   sourceDescription: this.props.clientProfileData.sourceDescription,
      //   occupation: this.props.clientProfileData.occupation,
      //   visaDenied: this.props.clientProfileData.visaDenied,
      //   deniedText: this.props.clientProfileData.deniedText,
      //   visaText: this.props.clientProfileData.visaText,
      //   currentVisaTypeId: this.props.clientProfileData.currentVisaTypeId.toString(),
      //   currentNewZealandVisaExpiry: moment(
      //     this.props.clientProfileData.currentNewZealandVisaExpiry
      //   ),
      //   travelConditionsValidTo: moment(
      //     this.props.clientProfileData.travelConditionsValidTo
      //   ),
      //   clientNumber: this.props.clientProfileData.clientNumber,
      //   inzUserName: this.props.clientProfileData.inzUserName,
      //   inzPassword: this.props.clientProfileData.inzPassword,
      //   inzFeeDate: moment(this.props.clientProfileData.inzFeeDate),
      //   nzqaOnlineSubDate: moment(
      //     this.props.clientProfileData.nzqaOnlineSubDate
      //   ),
      //   nzqaDocumentSubDate: moment(
      //     this.props.clientProfileData.nzqaDocumentSubDate
      //   ),
      //   nzqaDocumentRetDate: moment(
      //     this.props.clientProfileData.nzqaDocumentRetDate
      //   ),
      //   email: email,
      //   secondaryEmail: secondaryEmail,
      //   otherEmail: otherEmail,
      //   contactPerson: billAddressData ? billAddressData.contactPerson : "",
      //   flat: billAddressData ? billAddressData.flat : "",
      //   streetNumber: billAddressData ? billAddressData.streetNumber : "",
      //   suburb: billAddressData ? billAddressData.suburb : "",
      //   city: billAddressData ? billAddressData.city : "",
      //   billCountry: billAddressData ? billAddressData.country : "",
      //   zipcode: billAddressData ? billAddressData.zipcode : "",
      //   clientAddress: clientAddressData ? clientAddressData.address : "",
      //   mobilePhone: phoneData ? phoneData.mobile : "",
      //   secondaryMobile: phoneData ? phoneData.secondaryMobile : "",
      //   overseasMobile: phoneData ? phoneData.overseasMobile : "",
      //   landLine: phoneData ? phoneData.landLine : "",
      //   otherMobile: phoneData ? phoneData.otherMobile : "",
      //   medicalIssueDate: medicalData
      //     ? moment(medicalData.medicalIssueDate)
      //     : "",
      //   medicalExpiryDate: medicalData
      //     ? moment(medicalData.medicalExpiryDate)
      //     : "",
      //   medicalGrading: medicalData ? medicalData.medicalGrading : "",
      //   xrayIssueDate: medicalData ? moment(medicalData.xrayIssueDate) : "",
      //   xrayExpiryDate: medicalData ? moment(medicalData.xrayExpiryDate) : "",
      //   xrayGrading: medicalData ? medicalData.xrayGrading : "",
      //   medicalNotes: medicalData ? medicalData.medicalNotes : "",
      //   medicalNotesDetail: medicalData ? medicalData.medicalNotesDetail : "",
      //   passportNo:
      //     this.props.clientProfileData.passports.length > 0
      //       ? this.props.clientProfileData.passports[0].passportNo
      //       : "",
      //   passportCountry:
      //     this.props.clientProfileData.passports.length > 0
      //       ? this.props.clientProfileData.passports[0].passportCountry.toString()
      //       : "",
      //   passportIssueDate:
      //     this.props.clientProfileData.passports.length > 0
      //       ? moment(
      //           this.props.clientProfileData.passports[0].passportIssueDate
      //         )
      //       : "",
      //   passportExpiryDate:
      //     this.props.clientProfileData.passports.length > 0
      //       ? moment(
      //           this.props.clientProfileData.passports[0].passportExpiryDate
      //         )
      //       : "",
      //   secondPassportNo:
      //     this.props.clientProfileData.passports.length > 1
      //       ? this.props.clientProfileData.passports[1].passportNo
      //       : "",
      //   secondPassportCountry:
      //     this.props.clientProfileData.passports.length > 1
      //       ? this.props.clientProfileData.passports[1].passportCountry.toString()
      //       : "",
      //   secondPassportIssueDate:
      //     this.props.clientProfileData.passports.length > 1
      //       ? moment(
      //           this.props.clientProfileData.passports[1].passportIssueDate
      //         )
      //       : "",
      //   secondPassportExpiryDate:
      //     this.props.clientProfileData.passports.length > 1
      //       ? moment(
      //           this.props.clientProfileData.passports[1].passportExpiryDate
      //         )
      //       : ""
      // });
    }
  }

  formRef = React.createRef();

  myChangeHandler = (text) => {
    this.setState({ username: text });
  };

  onChange = (value) => {
    console.log(`selected ${value}`);
  };

  onBlur = () => {
    console.log("blur");
  };

  onFocus = () => {
    console.log("focus");
  };

  onSearch = (val) => {
    console.log("search:", val);
  };

  onChangeDate = (date) => this.setState({ date });

  handleChange = (selectedOption) => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };

  addPoliceCertificateInfo = (policeCertificateData) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    let data = {
      clientId: clientprofileid,
      issueDate: "2020-12-07T08:08:09.142Z",
      certificateExpiryDate: "2020-12-07T08:08:09.142Z",
      country: 168,
      createdBy: "4c08dfd5-dd40-c6cc-c58c-d6a9d19d0192",
    };
  };

  addCertificate = (data) => {
    this.props.onAddPoliceCertificate(data).then(() => {
      message.success("Police Certificate added successfully");
      this.props.onGetClientProfile();
    });
  };

  addAuthority = (data) => {
    this.props.onAddAccAuthorities(data).then(() => {
      message.success("Accessing Authority added successfully");
      this.props.onGetClientProfile();
    });
  };

  addProcessingGroup = (data) => {
    this.props.onAddProcessingGroups(data).then(() => {
      this.props.onGetClientProfile();
      this.props.onGetGroupMembers(data.groupId);
    });
  };

  addProcessingPerson = (data) => {
    this.props.onAddProcessingPerson(data).then(() => {
      this.props.onGetClientProfile();
    });
  };

  removeCertificate = (data) => {
    //  
    this.props.onRemovePoliceCertificate(data).then(() => {
      message.success("Police Certificate removed successfully");
      this.props.onGetClientProfile();
    });
  };

  removeAuthority = (data) => {
    //  
    this.props.onRemoveAccesingAuthority(data).then(() => {
      message.success("Accessing Authority removed successfully");
      this.props.onGetClientProfile();
    });
  };

  removeProcessingGroup = (data) => {
    this.props.onRemoveProcessingGroup(data).then(() => {
      message.success("Processing group removed successfully");
      this.props.onGetClientProfile();
    });
  };

  removeProcessingPerson = (data) => {
    this.props.onRemoveProcessingPerson(data).then(() => {
      message.success("Processing person removed successfully");
      this.props.onGetClientProfile();
    });
  };

  searchConnection = (data) => {
    this.props.onSearchConnection(data).then((res) => {});
  };

  addConnection = (data) => {
    this.props.onAddConnection(data).then((res) => {
      this.props.onGetClientProfile();
      message.success("Client connection added successfully");
    });
  };

  removeConnection = (data) => {
    this.props.onRemoveConnection(data).then((res) => {
      this.props.onGetClientProfile();
      message.success("Client connection removed successfully");
    });
  };

  personalInfoUpdate = (values) => {
    const userId = localStorage.getItem("userId");
    if (this.props.clientProfileData.addresses.length > 0) {
      let addressData = [];
      for (var i = 0; i < this.props.clientProfileData.addresses.length; i++) {
        let addressValues = {
          id: this.props.clientProfileData.addresses[i].id,
          clientId: this.props.clientProfileData.addresses[i].clientId,
          contactPerson: this.props.clientProfileData.addresses[i]
            .contactPerson,
          flat: this.props.clientProfileData.addresses[i].flat,
          building: this.props.clientProfileData.addresses[i].building,
          streetName: this.props.clientProfileData.addresses[i].streetName,
          suburb: this.props.clientProfileData.addresses[i].suburb,
          streetNumber: this.props.clientProfileData.addresses[i].streetNumber,
          city:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? values.address
              : this.props.clientProfileData.addresses[i].city,
          state: this.props.clientProfileData.addresses[i].state,
          zip: this.props.clientProfileData.addresses[i].zip,
          country: this.props.clientProfileData.addresses[i].country,
          addressTypeId: this.props.clientProfileData.addresses[i]
            .addressTypeId,
          modifiedBy: userId,
        };
        addressData.push(addressValues);
      }
      this.props.onUpdClientAddress(addressData);
    }
    if (this.props.clientProfileData.emails.length > 0) {
      let emailData = [];
      for (var i = 0; i < this.props.clientProfileData.emails.length; i++) {
        let emailValues = {
          id: this.props.clientProfileData.emails[i].id,
          clientId: this.props.clientProfileData.emails[i].clientId,
          address:
            this.props.clientProfileData.emails[i].emailTypeId === 1
              ? values.email
              : this.props.clientProfileData.emails[i].emailTypeId === 2
              ? values.secondaryEmail
              : values.otherEmail,
          emailTypeId: this.props.clientProfileData.emails[i].emailTypeId,
          modifiedBy: userId,
        };
        emailData.push(emailValues);
      }
      this.props.onUpdClientEmail(emailData);
    }
    if (this.props.clientProfileData.phones.length > 0) {
      let phonesData = [];
      for (var i = 0; i < this.props.clientProfileData.phones.length; i++) {
        let phonesValues = {
          id: this.props.clientProfileData.phones[i].id,
          clientId: this.props.clientProfileData.phones[i].clientId,
          contact:
            this.props.clientProfileData.phones[i].phoneTypeId === 1
              ? values.mobilePhone
              : this.props.clientProfileData.phones[i].phoneTypeId === 2
              ? values.secondaryMobile
              : this.props.clientProfileData.phones[i].phoneTypeId === 3
              ? values.overseasMobile
              : this.props.clientProfileData.phones[i].phoneTypeId === 4
              ? values.landLine
              : values.otherMobile,
          phoneTypeId: this.props.clientProfileData.phones[i].phoneTypeId,
          modifiedBy: userId,
        };
        phonesData.push(phonesValues);
      }
      this.props.onUpdClientPhone(phonesData);
    }

    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: values.firstName
        ? values.firstName
        : this.props.clientProfileData.firstName,
      lastName: values.lastName
        ? values.lastName
        : this.props.clientProfileData.lastName,
      middleName: values.middleName
        ? values.middleName
        : this.props.clientProfileData.middleName,
      title: values.title ? values.title : this.props.clientProfileData.title,
      gender: values.gender
        ? values.gender
        : this.props.clientProfileData.gender,
      dateOfBirth: values.dateOfBirth
        ? values.dateOfBirth
        : this.props.clientProfileData.dateOfBirth,
      maritalStatus: values.maritalStatus
        ? values.maritalStatus
        : this.props.clientProfileData.maritalStatus,
      dependentChildren: values.dependentChildren
        ? values.dependentChildren
        : this.props.clientProfileData.dependentChildren,
      notes: "string",
      occupation: values.occupation
        ? values.occupation
        : this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: values.inzUserName
        ? values.inzUserName
        : this.props.clientProfileData.inzUserName,
      inzPassword: values.inzPassword
        ? values.inzPassword
        : this.props.clientProfileData.inzPassword,
      imageBlobUrl: this.props.clientProfileData.imageBlobUrl,
      nationalityId: values.nationalityId
        ? values.nationalityId
        : this.props.clientProfileData.nationalityId,
      nationalityCountry: values.nationalityCountry
        ? values.nationalityCountry
        : this.props.clientProfileData.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        values.currentVisaTypeId
          ? values.currentVisaTypeId
          : this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry
        ? values.currentNewZealandVisaExpiry
        : this.props.clientProfileData.currentNewZealandVisaExpiry,
      travelConditionsValidTo: values.travelConditionsValidTo
        ? values.travelConditionsValidTo
        : this.props.clientProfileData.travelConditionsValidTo,
      visaText: values.visaText
        ? values.visaText
        : this.props.clientProfileData.visaText,
      visaDenied: values.visaDenied,
      deniedText: values.deniedText
        ? values.deniedText
        : this.props.clientProfileData.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: values.inzFeeDate
        ? values.inzFeeDate
        : this.props.clientProfileData.inzFeeDate,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: values.nzqaOnlineSubDate
        ? values.nzqaOnlineSubDate
        : this.props.clientProfileData.nzqaOnlineSubDate,
      nzqaDocumentSubDate: values.nzqaDocumentSubDate
        ? values.nzqaDocumentSubDate
        : this.props.clientProfileData.nzqaDocumentSubDate,
      nzqaDocumentRetDate: values.nzqaDocumentRetDate
        ? values.nzqaDocumentRetDate
        : this.props.clientProfileData.nzqaDocumentRetDate,
      visaCountryId: parseInt(
        values.visaCountryId
          ? values.visaCountryId
          : this.props.clientProfileData.visaCountryId
      ),
      visaCountyType: parseInt(
        values.visaCountryType
          ? values.visaCountryType
          : this.props.clientProfileData.visaCountyType
      ),
      age: values.age ? values.age : this.props.clientProfileData.age,
      jobSectorId: parseInt(
        values.jobSectorId
          ? values.jobSectorId
          : this.props.clientProfileData.jobSectorId
      ),
      sourceId: parseInt(
        values.sourceId
          ? values.sourceId
          : this.props.clientProfileData.sourceId
      ),
      sourceDescription: values.sourceDescription
        ? values.sourceDescription
        : this.props.clientProfileData.sourceDescription,
      clientSerial: values.clientSerial
        ? values.clientSerial
        : this.props.clientProfileData.clientSerial,
      companyOptional: values.companyOptional
        ? values.companyOptional
        : this.props.clientProfileData.companyOptional,
      dealWorth: values.dealWorth
        ? values.dealWorth
        : this.props.clientProfileData.dealWorth,
      saleDate: values.saleDate
        ? values.saleDate
        : this.props.clientProfileData.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };
    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props.onGetClientProfile();
      })
      .catch(() => {
        this.setState({ updLoading: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile();
      });
  };

  onUpdateCurrentVisa = (values) => {
    const userId = localStorage.getItem("userId");
    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: values.firstName
        ? values.firstName
        : this.props.clientProfileData.firstName,
      lastName: values.lastName
        ? values.lastName
        : this.props.clientProfileData.lastName,
      middleName: values.middleName
        ? values.middleName
        : this.props.clientProfileData.middleName,
      title: values.title ? values.title : this.props.clientProfileData.title,
      gender: values.gender
        ? values.gender
        : this.props.clientProfileData.gender,
      dateOfBirth: values.dateOfBirth
        ? values.dateOfBirth
        : this.props.clientProfileData.dateOfBirth,
      maritalStatus: values.maritalStatus
        ? values.maritalStatus
        : this.props.clientProfileData.maritalStatus,
      dependentChildren: values.dependentChildren
        ? values.dependentChildren
        : this.props.clientProfileData.dependentChildren,
      notes: "string",
      occupation: values.occupation
        ? values.occupation
        : this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: values.inzUserName
        ? values.inzUserName
        : this.props.clientProfileData.inzUserName,
      inzPassword: values.inzPassword
        ? values.inzPassword
        : this.props.clientProfileData.inzPassword,
      imageBlobUrl: this.props.clientProfileData.imageBlobUrl,
      nationalityId: values.nationalityId
        ? values.nationalityId
        : this.props.clientProfileData.nationalityId,
      nationalityCountry: values.nationalityCountry
        ? values.nationalityCountry
        : this.props.clientProfileData.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        values.currentVisaTypeId
          ? values.currentVisaTypeId
          : this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry
        ? values.currentNewZealandVisaExpiry
        : this.props.clientProfileData.currentNewZealandVisaExpiry,
      travelConditionsValidTo: values.travelConditionsValidTo
        ? values.travelConditionsValidTo
        : this.props.clientProfileData.travelConditionsValidTo,
      visaText: values.visaText
        ? values.visaText
        : this.props.clientProfileData.visaText,
      visaDenied: this.props.clientProfileData.visaDenied,
      deniedText: values.deniedText
        ? values.deniedText
        : this.props.clientProfileData.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: values.inzFeeDate
        ? values.inzFeeDate
        : this.props.clientProfileData.inzFeeDate,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: values.nzqaOnlineSubDate
        ? values.nzqaOnlineSubDate
        : this.props.clientProfileData.nzqaOnlineSubDate,
      nzqaDocumentSubDate: values.nzqaDocumentSubDate
        ? values.nzqaDocumentSubDate
        : this.props.clientProfileData.nzqaDocumentSubDate,
      nzqaDocumentRetDate: values.nzqaDocumentRetDate
        ? values.nzqaDocumentRetDate
        : this.props.clientProfileData.nzqaDocumentRetDate,
      visaCountryId: parseInt(
        values.visaCountryId
          ? values.visaCountryId
          : this.props.clientProfileData.visaCountryId
      ),
      visaCountyType: parseInt(
        values.visaCountryType
          ? values.visaCountryType
          : this.props.clientProfileData.visaCountyType
      ),
      age: values.age ? values.age : this.props.clientProfileData.age,
      jobSectorId: parseInt(
        values.jobSectorId
          ? values.jobSectorId
          : this.props.clientProfileData.jobSectorId
      ),
      sourceId: parseInt(
        values.sourceId
          ? values.sourceId
          : this.props.clientProfileData.sourceId
      ),
      sourceDescription: values.sourceDescription
        ? values.sourceDescription
        : this.props.clientProfileData.sourceDescription,
      clientSerial: values.clientSerial
        ? values.clientSerial
        : this.props.clientProfileData.clientSerial,
      companyOptional: values.companyOptional
        ? values.companyOptional
        : this.props.clientProfileData.companyOptional,
      dealWorth: values.dealWorth
        ? values.dealWorth
        : this.props.clientProfileData.dealWorth,
      saleDate: values.saleDate
        ? values.saleDate
        : this.props.clientProfileData.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };

    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props.onGetClientProfile();
      })
      .catch(() => {
        this.setState({ updLoading: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile();
      });
  };

  onUpdateNZQA = (values) => {
    const userId = localStorage.getItem("userId");
    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: values.firstName
        ? values.firstName
        : this.props.clientProfileData.firstName,
      lastName: values.lastName
        ? values.lastName
        : this.props.clientProfileData.lastName,
      middleName: values.middleName
        ? values.middleName
        : this.props.clientProfileData.middleName,
      title: values.title ? values.title : this.props.clientProfileData.title,
      gender: values.gender
        ? values.gender
        : this.props.clientProfileData.gender,
      dateOfBirth: values.dateOfBirth
        ? values.dateOfBirth
        : this.props.clientProfileData.dateOfBirth,
      maritalStatus: values.maritalStatus
        ? values.maritalStatus
        : this.props.clientProfileData.maritalStatus,
      dependentChildren: values.dependentChildren
        ? values.dependentChildren
        : this.props.clientProfileData.dependentChildren,
      notes: "string",
      occupation: values.occupation
        ? values.occupation
        : this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: values.inzUserName
        ? values.inzUserName
        : this.props.clientProfileData.inzUserName,
      inzPassword: values.inzPassword
        ? values.inzPassword
        : this.props.clientProfileData.inzPassword,
      imageBlobUrl: this.props.clientProfileData.imageBlobUrl,
      nationalityId: values.nationalityId
        ? values.nationalityId
        : this.props.clientProfileData.nationalityId,
      nationalityCountry: values.nationalityCountry
        ? values.nationalityCountry
        : this.props.clientProfileData.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        values.currentVisaTypeId
          ? values.currentVisaTypeId
          : this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry
        ? values.currentNewZealandVisaExpiry
        : this.props.clientProfileData.currentNewZealandVisaExpiry,
      travelConditionsValidTo: values.travelConditionsValidTo
        ? values.travelConditionsValidTo
        : this.props.clientProfileData.travelConditionsValidTo,
      visaText: values.visaText
        ? values.visaText
        : this.props.clientProfileData.visaText,
      visaDenied: this.props.clientProfileData.visaDenied,
      deniedText: values.deniedText
        ? values.deniedText
        : this.props.clientProfileData.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: values.inzFeeDate
        ? values.inzFeeDate
        : this.props.clientProfileData.inzFeeDate,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: values.nzqaOnlineSubDate
        ? values.nzqaOnlineSubDate
        : this.props.clientProfileData.nzqaOnlineSubDate,
      nzqaDocumentSubDate: values.nzqaDocumentSubDate
        ? values.nzqaDocumentSubDate
        : this.props.clientProfileData.nzqaDocumentSubDate,
      nzqaDocumentRetDate: values.nzqaDocumentRetDate
        ? values.nzqaDocumentRetDate
        : this.props.clientProfileData.nzqaDocumentRetDate,
      visaCountryId: parseInt(
        values.visaCountryId
          ? values.visaCountryId
          : this.props.clientProfileData.visaCountryId
      ),
      visaCountyType: parseInt(
        values.visaCountryType
          ? values.visaCountryType
          : this.props.clientProfileData.visaCountyType
      ),
      age: values.age ? values.age : this.props.clientProfileData.age,
      jobSectorId: parseInt(
        values.jobSectorId
          ? values.jobSectorId
          : this.props.clientProfileData.jobSectorId
      ),
      sourceId: parseInt(
        values.sourceId
          ? values.sourceId
          : this.props.clientProfileData.sourceId
      ),
      sourceDescription: values.sourceDescription
        ? values.sourceDescription
        : this.props.clientProfileData.sourceDescription,
      clientSerial: values.clientSerial
        ? values.clientSerial
        : this.props.clientProfileData.clientSerial,
      companyOptional: values.companyOptional
        ? values.companyOptional
        : this.props.clientProfileData.companyOptional,
      dealWorth: values.dealWorth
        ? values.dealWorth
        : this.props.clientProfileData.dealWorth,
      saleDate: values.saleDate
        ? values.saleDate
        : this.props.clientProfileData.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };
    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props.onGetClientProfile();
      })
      .catch(() => {
        this.setState({ updLoading: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile();
      });
  };

  onUpdateMedicals = (values) => {
    const userId = localStorage.getItem("userId");
    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: values.firstName
        ? values.firstName
        : this.props.clientProfileData.firstName,
      lastName: values.lastName
        ? values.lastName
        : this.props.clientProfileData.lastName,
      middleName: values.middleName
        ? values.middleName
        : this.props.clientProfileData.middleName,
      title: values.title ? values.title : this.props.clientProfileData.title,
      gender: values.gender
        ? values.gender
        : this.props.clientProfileData.gender,
      dateOfBirth: values.dateOfBirth
        ? values.dateOfBirth
        : this.props.clientProfileData.dateOfBirth,
      maritalStatus: values.maritalStatus
        ? values.maritalStatus
        : this.props.clientProfileData.maritalStatus,
      dependentChildren: values.dependentChildren
        ? values.dependentChildren
        : this.props.clientProfileData.dependentChildren,
      notes: "string",
      occupation: values.occupation
        ? values.occupation
        : this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: values.inzUserName
        ? values.inzUserName
        : this.props.clientProfileData.inzUserName,
      inzPassword: values.inzPassword
        ? values.inzPassword
        : this.props.clientProfileData.inzPassword,
      imageBlobUrl: this.props.clientProfileData.imageBlobUrl,
      nationalityId: values.nationalityId
        ? values.nationalityId
        : this.props.clientProfileData.nationalityId,
      nationalityCountry: values.nationalityCountry
        ? values.nationalityCountry
        : this.props.clientProfileData.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        values.currentVisaTypeId
          ? values.currentVisaTypeId
          : this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry
        ? values.currentNewZealandVisaExpiry
        : this.props.clientProfileData.currentNewZealandVisaExpiry,
      travelConditionsValidTo: values.travelConditionsValidTo
        ? values.travelConditionsValidTo
        : this.props.clientProfileData.travelConditionsValidTo,
      visaText: values.visaText
        ? values.visaText
        : this.props.clientProfileData.visaText,
      visaDenied: this.props.clientProfileData.visaDenied,
      deniedText: values.deniedText
        ? values.deniedText
        : this.props.clientProfileData.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: values.inzFeeDate
        ? values.inzFeeDate
        : this.props.clientProfileData.inzFeeDate,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: values.nzqaOnlineSubDate
        ? values.nzqaOnlineSubDate
        : this.props.clientProfileData.nzqaOnlineSubDate,
      nzqaDocumentSubDate: values.nzqaDocumentSubDate
        ? values.nzqaDocumentSubDate
        : this.props.clientProfileData.nzqaDocumentSubDate,
      nzqaDocumentRetDate: values.nzqaDocumentRetDate
        ? values.nzqaDocumentRetDate
        : this.props.clientProfileData.nzqaDocumentRetDate,
      visaCountryId: parseInt(
        values.visaCountryId
          ? values.visaCountryId
          : this.props.clientProfileData.visaCountryId
      ),
      visaCountyType: parseInt(
        values.visaCountryType
          ? values.visaCountryType
          : this.props.clientProfileData.visaCountyType
      ),
      age: values.age ? values.age : this.props.clientProfileData.age,
      jobSectorId: parseInt(
        values.jobSectorId
          ? values.jobSectorId
          : this.props.clientProfileData.jobSectorId
      ),
      sourceId: parseInt(
        values.sourceId
          ? values.sourceId
          : this.props.clientProfileData.sourceId
      ),
      sourceDescription: values.sourceDescription
        ? values.sourceDescription
        : this.props.clientProfileData.sourceDescription,
      clientSerial: values.clientSerial
        ? values.clientSerial
        : this.props.clientProfileData.clientSerial,
      companyOptional: values.companyOptional
        ? values.companyOptional
        : this.props.clientProfileData.companyOptional,
      dealWorth: values.dealWorth
        ? values.dealWorth
        : this.props.clientProfileData.dealWorth,
      saleDate: values.saleDate
        ? values.saleDate
        : this.props.clientProfileData.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };
    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props.onGetClientProfile();
      })
      .catch(() => {
        this.setState({ updLoading: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile();
      });
  };

  onUpdateInzUserDetail = (values) => {
    const userId = localStorage.getItem("userId");
    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: values.firstName
        ? values.firstName
        : this.props.clientProfileData.firstName,
      lastName: values.lastName
        ? values.lastName
        : this.props.clientProfileData.lastName,
      middleName: values.middleName
        ? values.middleName
        : this.props.clientProfileData.middleName,
      title: values.title ? values.title : this.props.clientProfileData.title,
      gender: values.gender
        ? values.gender
        : this.props.clientProfileData.gender,
      dateOfBirth: values.dateOfBirth
        ? values.dateOfBirth
        : this.props.clientProfileData.dateOfBirth,
      maritalStatus: values.maritalStatus
        ? values.maritalStatus
        : this.props.clientProfileData.maritalStatus,
      dependentChildren: values.dependentChildren
        ? values.dependentChildren
        : this.props.clientProfileData.dependentChildren,
      notes: "string",
      occupation: values.occupation
        ? values.occupation
        : this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: values.inzUserName
        ? values.inzUserName
        : this.props.clientProfileData.inzUserName,
      inzPassword: values.inzPassword
        ? values.inzPassword
        : this.props.clientProfileData.inzPassword,
      imageBlobUrl: this.props.clientProfileData.imageBlobUrl,
      nationalityId: values.nationalityId
        ? values.nationalityId
        : this.props.clientProfileData.nationalityId,
      nationalityCountry: values.nationalityCountry
        ? values.nationalityCountry
        : this.props.clientProfileData.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        values.currentVisaTypeId
          ? values.currentVisaTypeId
          : this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry
        ? values.currentNewZealandVisaExpiry
        : this.props.clientProfileData.currentNewZealandVisaExpiry,
      travelConditionsValidTo: values.travelConditionsValidTo
        ? values.travelConditionsValidTo
        : this.props.clientProfileData.travelConditionsValidTo,
      visaText: values.visaText
        ? values.visaText
        : this.props.clientProfileData.visaText,
      visaDenied: this.props.clientProfileData.visaDenied,
      deniedText: values.deniedText
        ? values.deniedText
        : this.props.clientProfileData.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: values.inzFeeDate
        ? values.inzFeeDate
        : this.props.clientProfileData.inzFeeDate,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: values.nzqaOnlineSubDate
        ? values.nzqaOnlineSubDate
        : this.props.clientProfileData.nzqaOnlineSubDate,
      nzqaDocumentSubDate: values.nzqaDocumentSubDate
        ? values.nzqaDocumentSubDate
        : this.props.clientProfileData.nzqaDocumentSubDate,
      nzqaDocumentRetDate: values.nzqaDocumentRetDate
        ? values.nzqaDocumentRetDate
        : this.props.clientProfileData.nzqaDocumentRetDate,
      visaCountryId: parseInt(
        values.visaCountryId
          ? values.visaCountryId
          : this.props.clientProfileData.visaCountryId
      ),
      visaCountyType: parseInt(
        values.visaCountryType
          ? values.visaCountryType
          : this.props.clientProfileData.visaCountyType
      ),
      age: values.age ? values.age : this.props.clientProfileData.age,
      jobSectorId: parseInt(
        values.jobSectorId
          ? values.jobSectorId
          : this.props.clientProfileData.jobSectorId
      ),
      sourceId: parseInt(
        values.sourceId
          ? values.sourceId
          : this.props.clientProfileData.sourceId
      ),
      sourceDescription: values.sourceDescription
        ? values.sourceDescription
        : this.props.clientProfileData.sourceDescription,
      clientSerial: values.clientSerial
        ? values.clientSerial
        : this.props.clientProfileData.clientSerial,
      companyOptional: values.companyOptional
        ? values.companyOptional
        : this.props.clientProfileData.companyOptional,
      dealWorth: values.dealWorth
        ? values.dealWorth
        : this.props.clientProfileData.dealWorth,
      saleDate: values.saleDate
        ? values.saleDate
        : this.props.clientProfileData.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };

    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props.onGetClientProfile();
      })
      .catch(() => {
        this.setState({ updLoading: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile();
      });
  };

  onUpdatePassport = (values) => {
    const userId = localStorage.getItem("userId");
    if (this.props.clientProfileData.passports.length > 0) {
      let passportData = [];
      for (var i = 0; i < this.props.clientProfileData.passports.length; i++) {
        let passportValues = {
          id: this.props.clientProfileData.passports[i].id,
          clientId: this.props.clientProfileData.passports[i].clientId,
          passportNo: i === 0 ? values.passportNo : values.secondPassportNo,
          passportCountry:
            i === 0
              ? parseInt(values.passportCountry)
              : parseInt(values.secondPassportCountry),
          passportIssueDate:
            i === 0 ? values.passportIssueDate : values.secondPassportIssueDate,
          passportExpiryDate:
            i === 0
              ? values.passportExpiryDate
              : values.secondPassportExpiryDate,
          modifiedBy: userId,
        };
        passportData.push(passportValues);
      }
      this.props
        .onUpdClientPassport(passportData)
        .then(() => {
          this.setState({ updLoading: false });
          message.success("Client passport updated successfully");
          this.props.onGetClientProfile();
        })
        .catch(() => {
          this.setState({ updLoading: false });
          message.error("Client Passport update failed");
          this.props.onGetClientProfile();
        });
    }
  };

  onUpdateBillingAddress = (values) => {
    const userId = localStorage.getItem("userId");
    if (this.props.clientProfileData.addresses.length > 0) {
      let addressData = [];
      for (var i = 0; i < this.props.clientProfileData.addresses.length; i++) {
        let addressValues = {
          id: this.props.clientProfileData.addresses[i].id,
          clientId: this.props.clientProfileData.addresses[i].clientId,
          contactPerson:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].contactPerson
              : values.contactPerson,
          flat:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].flat
              : values.flat,
          building: this.props.clientProfileData.addresses[i].building,
          streetName: this.props.clientProfileData.addresses[i].streetName,
          suburb:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].suburb
              : values.suburb,
          streetNumber:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].streetNumber
              : values.streetNumber,
          city:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].city
              : values.city,
          state: this.props.clientProfileData.addresses[i].state,
          zip:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].zip
              : values.zipcode,
          country:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].country
              : values.billCountry,
          addressTypeId: this.props.clientProfileData.addresses[i]
            .addressTypeId,
          modifiedBy: userId,
        };
        addressData.push(addressValues);
      }
      this.props
        .onUpdClientAddress(addressData)
        .then(() => {
          this.setState({ updLoading: false });
          message.success("Client billing address updated successfully");
          this.props.onGetClientProfile();
        })
        .catch(() => {
          this.setState({ updLoading: false });
          message.error("Client billing address update failed");
          this.props.onGetClientProfile();
        });
    }
  };

  onProfileTopUpdate = (values) => {
    const userId = localStorage.getItem("userId");
    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: this.props.clientProfileData.firstName,
      lastName: this.props.clientProfileData.lastName,
      middleName: this.props.clientProfileData.middleName,
      title: this.props.clientProfileData.title,
      gender: this.props.clientProfileData.gender,
      dateOfBirth: this.props.clientProfileData.dateOfBirth,
      maritalStatus: this.props.clientProfileData.maritalStatus,
      dependentChildren: this.props.clientProfileData.dependentChildren,
      notes: "string",
      occupation: this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: this.props.clientProfileData.inzUserName,
      inzPassword: this.props.clientProfileData.inzPassword,
      imageBlobUrl: this.state.addClientImageUrl
        ? this.state.addClientImageUrl
        : this.props.clientProfileData.imageBlobUrl,
      nationalityId: this.props.clientProfileData.nationalityId,
      nationalityCountry: this.props.clientProfileData.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: this.props.clientProfileData
        .currentNewZealandVisaExpiry,
      travelConditionsValidTo: this.props.clientProfileData
        .travelConditionsValidTo,
      visaText: this.props.clientProfileData.visaText,
      visaDenied: this.props.clientProfileData.visaDenied,
      deniedText: this.props.clientProfileData.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: this.props.clientProfileData.inzFeeDate,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: this.props.clientProfileData.nzqaOnlineSubDate,
      nzqaDocumentSubDate: this.props.clientProfileData.nzqaDocumentSubDate,
      nzqaDocumentRetDate: this.props.clientProfileData.nzqaDocumentRetDate,
      visaCountryId: parseInt(this.props.clientProfileData.visaCountryId),
      visaCountyType: parseInt(this.props.clientProfileData.visaCountyType),
      age: values.age ? values.age : this.props.clientProfileData.age,
      jobSectorId: parseInt(this.props.clientProfileData.jobSectorId),
      sourceId: parseInt(this.props.clientProfileData.sourceId),
      sourceDescription: this.props.clientProfileData.sourceDescription,
      clientSerial: this.props.clientProfileData.clientSerial,
      companyOptional: this.props.clientProfileData.companyOptional,
      dealWorth: this.props.clientProfileData.dealWorth,
      saleDate: this.props.clientProfileData.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };
    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props.onGetClientProfile();
      })
      .catch(() => {
        this.setState({ updLoading: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile();
      });
  };

  onFinish = (values) => {
    const userId = localStorage.getItem("userId");
    this.setState({ updLoading: true });
    console.log("Success:", values);
    if (this.props.clientProfileData.addresses.length > 0) {
      let addressData = [];
      for (var i = 0; i < this.props.clientProfileData.addresses.length; i++) {
        let addressValues = {
          id: this.props.clientProfileData.addresses[i].id,
          clientId: this.props.clientProfileData.addresses[i].clientId,
          contactPerson:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].contactPerson
              : values.contactPerson,
          flat:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].flat
              : values.flat,
          building: this.props.clientProfileData.addresses[i].building,
          streetName: this.props.clientProfileData.addresses[i].streetName,
          suburb:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].suburb
              : values.suburb,
          streetNumber:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].streetNumber
              : values.streetNumber,
          city:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? values.address
              : values.city,
          state: this.props.clientProfileData.addresses[i].state,
          zip:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].zip
              : values.zipcode,
          country:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].country
              : values.billCountry,
          addressTypeId: this.props.clientProfileData.addresses[i]
            .addressTypeId,
          modifiedBy: userId,
        };
        addressData.push(addressValues);
      }
      this.props.onUpdClientAddress(addressData);
    }
    if (this.props.clientProfileData.emails.length > 0) {
      let emailData = [];
      for (var i = 0; i < this.props.clientProfileData.emails.length; i++) {
        let emailValues = {
          id: this.props.clientProfileData.emails[i].id,
          clientId: this.props.clientProfileData.emails[i].clientId,
          address:
            this.props.clientProfileData.emails[i].emailTypeId === 1
              ? values.email
              : this.props.clientProfileData.emails[i].emailTypeId === 2
              ? values.secondaryEmail
              : values.otherEmail,
          emailTypeId: this.props.clientProfileData.emails[i].emailTypeId,
          modifiedBy: userId,
        };
        emailData.push(emailValues);
      }
      this.props.onUpdClientEmail(emailData);
    }
    if (this.props.clientProfileData.phones.length > 0) {
      let phonesData = [];
      for (var i = 0; i < this.props.clientProfileData.phones.length; i++) {
        let phonesValues = {
          id: this.props.clientProfileData.phones[i].id,
          clientId: this.props.clientProfileData.phones[i].clientId,
          contact:
            this.props.clientProfileData.phones[i].phoneTypeId === 1
              ? values.mobilePhone
              : this.props.clientProfileData.phones[i].phoneTypeId === 2
              ? values.secondaryMobile
              : this.props.clientProfileData.phones[i].phoneTypeId === 3
              ? values.overseasMobile
              : this.props.clientProfileData.phones[i].phoneTypeId === 4
              ? values.landLine
              : values.otherMobile,
          phoneTypeId: this.props.clientProfileData.phones[i].phoneTypeId,
          modifiedBy: userId,
        };
        phonesData.push(phonesValues);
      }
      this.props.onUpdClientPhone(phonesData);
    }
    if (this.props.clientProfileData.passports.length > 0) {
      let passportData = [];
      for (var i = 0; i < this.props.clientProfileData.passports.length; i++) {
        let passportValues = {
          id: this.props.clientProfileData.passports[i].id,
          clientId: this.props.clientProfileData.passports[i].clientId,
          passportNo: i === 0 ? values.passportNo : values.secondPassportNo,
          passportCountry:
            i === 0
              ? parseInt(values.passportCountry)
              : parseInt(values.secondPassportCountry),
          passportIssueDate:
            i === 0 ? values.passportIssueDate : values.secondPassportIssueDate,
          passportExpiryDate:
            i === 0
              ? values.passportExpiryDate
              : values.secondPassportExpiryDate,
          modifiedBy: userId,
        };
        passportData.push(passportValues);
      }
      this.props.onUpdClientPassport(passportData);
    }
    if (this.props.clientProfileData.medicals.length > 0) {
      let medicalData = [];
      for (var i = 0; i < this.props.clientProfileData.medicals.length; i++) {
        let medicalValues = {
          id: this.props.clientProfileData.medicals[i].id,
          clientId: this.props.clientProfileData.medicals[i].clientId,
          er: this.props.clientProfileData.medicals[i].er,
          medicalIssueDate: values.medicalIssueDate
            ? values.medicalIssueDate
            : "",
          medicalExpiryDate: values.medicalExpiryDate
            ? values.medicalExpiryDate
            : "",
          xrayIssueDate: values.xrayIssueDate ? values.xrayIssueDate : "",
          xrayExpiryDate: values.xrayExpiryDate ? values.xrayExpiryDate : "",
          medicalGrading: values.medicalGrading ? values.medicalGrading : "",
          xrayGrading: values.xrayGrading ? values.xrayGrading : "",
          medicalNotes: values.medicalNotes ? values.medicalNotes : "",
          medicalNotesDetail: values.medicalNotesDetail
            ? values.medicalNotesDetail
            : "",
          modifiedBy: userId,
        };
        medicalData.push(medicalValues);
      }

      this.props.onUpdClientMedical(medicalData);
    }

    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: values.firstName,
      lastName: values.lastName,
      middleName: values.middleName,
      title: values.title,
      gender: values.gender,
      dateOfBirth: values.dateOfBirth,
      maritalStatus: values.maritalStatus,
      dependentChildren: values.dependentChildren,
      notes: "string",
      occupation: values.occupation
        ? values.occupation
        : this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: values.inzUserName,
      inzPassword: values.inzPassword,
      imageBlobUrl: this.state.addClientImageUrl
        ? this.state.addClientImageUrl
        : this.props.clientProfileData.imageBlobUrl,
      nationalityId: values.nationalityId,
      nationalityCountry: values.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(values.currentVisaTypeId),
      currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry,
      travelConditionsValidTo: values.travelConditionsValidTo,
      visaText: values.visaText,
      visaDenied: this.props.clientProfileData.visaDenied,
      deniedText: values.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: values.inzFeeDate,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: values.nzqaOnlineSubDate,
      nzqaDocumentSubDate: values.nzqaDocumentSubDate,
      nzqaDocumentRetDate: values.nzqaDocumentRetDate,
      visaCountryId: parseInt(values.visaCountryId),
      visaCountyType: parseInt(values.visaCountryType),
      age: values.age,
      jobSectorId: parseInt(values.jobSectorId),
      sourceId: parseInt(values.sourceId),
      sourceDescription: values.sourceDescription,
      clientSerial: values.clientSerial,
      companyOptional: values.companyOptional,
      dealWorth: values.dealWorth,
      saleDate: values.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };

    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props.onGetClientProfile();
      })
      .catch(() => {
        this.setState({ updLoading: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile();
      });
  };

  uploadImage = (info, id) => {
    this.setState({ loadUploadImage: true });
    if (id) {
      this.setState({ imageUpdateId: id });
    }

    let formData = new FormData();
    formData.append("File", info);
    this.props.onUploadAvatar(formData).then(() => {
      if (this.props.imageUploadSuccess) {
        this.setState({
          addClientImageUrl: this.props.imageUploadSuccess,
          loadUploadImage: false,
        });
      }
    });
  };

  handleChangeImage = (info) => {
    console.log("show file data ====== ", info);
    if (info.file.status === "uploading") {
      // this.setState({ loading: true });
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl) =>
        this.setState({
          imageUrl,
          loading: false,
        })
      );
    }
  };

  setShowClient = () => {
    this.setState({ showClient: !this.state.showClient });
  };

  render() {
    const {
      selectedOption,
      signed,
      onshore,
      active,
      allowUpdate,
      areaAccess,
      updLoading,
      addClientImageUrl,
      showClient,
    } = this.state;

    const {
      clientProfileData,
      countriesData,
      groupsData,
      teamMembers,
      searchConnectionData,
      accessingAuthData,
      groupMembersData,
      visaTypeData,
    } = this.props;

    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            <HeaderBarTabs data={headOption} activeTab="Client Profile" />
            <ProfileTopBarTabs data={topBar} activeTab="CLIENT INFORMATION" />
            <div style={{ display: "flex" }}>
              <div style={{ width: "56%" }}>
                <div style={{ width: "100%" }}>
                  <div className="profile-first-box">
                    <div>
                      <div style={{ flexDirection: "row", display: "flex" }}>
                        <div className="profile-cont-left">
                          <div className="profile-img-cont">
                            <Upload
                              name="avatar"
                              listType="picture-card"
                              className="avatar-uploader"
                              showUploadList={false}
                              action={this.uploadImage}
                              onChange={this.handleChangeImage}
                            >
                              <Spin spinning={this.state.loadUploadImage}>
                                {addClientImageUrl ? (
                                  <img
                                    src={addClientImageUrl}
                                    alt="avatar"
                                    style={{ width: 105, height: 105 }}
                                  />
                                ) : (
                                  <img
                                    src={Images.dummyUserImage}
                                    className="profile-img"
                                  />
                                )}
                              </Spin>
                            </Upload>
                          </div>
                          <h3>
                            {clientProfileData
                              ? clientProfileData.firstName +
                                " " +
                                clientProfileData.lastName
                              : ""}
                          </h3>
                          <h5>
                            {!clientProfileData
                              ? ""
                              : clientProfileData.visaCountyType === 1
                              ? "STUDENT"
                              : clientProfileData.visaCountyType === 2
                              ? "VISA"
                              : "UNSUCCESSFULL"}
                          </h5>
                          <div style={{ display: "flex", marginTop: 15 }}>
                            <div>
                              <img
                                src={Images.cross}
                                style={{ width: 13, height: 13 }}
                              />
                            </div>
                            <div style={{ marginLeft: 10 }}>
                              <img src={Images.download} className="svg-img" />
                            </div>
                            <div style={{ marginLeft: 10 }}>
                              <img
                                src={Images.multimediaOption}
                                className="svg-img"
                              />
                            </div>
                            <div style={{ marginLeft: 10 }}>
                              <img
                                src={Images.down}
                                className="svg-img"
                                style={{ transform: `rotate(180deg)` }}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="right-cont">
                          <div className="profile-puq-cont">
                            <div className="profile-print-box">
                              <img
                                src={Images.printWhite}
                                className="profile-print-icon"
                              />
                            </div>
                            <div style={{ display: "flex" }}>
                              <div
                                className="profile-updbtn-cont"
                                style={{ paddingLeft: 10, cursor: "pointer" }}
                                onClick={this.onProfileTopUpdate}
                              >
                                <span className="profile-updbtn-text">
                                  UPDATE
                                </span>
                                <img
                                  src={Images.updateWhite}
                                  style={{
                                    width: 10,
                                    height: 10,
                                    marginLeft: 8,
                                  }}
                                />
                              </div>
                              <div
                                className="profile-updbtn-cont"
                                style={{
                                  marginLeft: 5,
                                  backgroundColor: "#0F7EB6",
                                  border: 1,
                                  borderStyle: "solid",
                                  borderColor: "#0F7EB6",
                                }}
                              >
                                <span className="profile-updbtn-text">
                                  QUESTIONNAIRE
                                </span>
                                <img
                                  src={Images.rightArrow}
                                  style={{
                                    transform: `rotate(90deg)`,
                                    width: 10,
                                    height: 10,
                                    marginLeft: 3,
                                  }}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="date-div">
                            <div>
                              <span className="date-text">
                                Created On:{" "}
                                {clientProfileData
                                  ? moment(
                                      clientProfileData.createdDate
                                    ).format("DD/MM/YYYY")
                                  : ""}
                              </span>
                            </div>
                            <div>
                              <span className="date-text">
                                Modified On: 02/02/2020 24:04:02
                              </span>
                            </div>
                          </div>
                          <div className="lv-main-cont">
                            <div className="label-value-cont">
                              <div className="label-cont">
                                <span className="label-text">EZM ID</span>
                              </div>
                              <div className="value-cont">
                                <span className="value-text">
                                  {clientProfileData
                                    ? clientProfileData.clientNumber
                                    : ""}
                                </span>
                              </div>
                            </div>
                            <div className="label-value-cont">
                              <div className="label-cont">
                                <span className="label-text">INZ ID</span>
                              </div>
                              <div className="value-cont">
                                <span className="value-text">
                                  {clientProfileData
                                    ? clientProfileData.clientNumberIZM
                                    : ""}
                                </span>
                              </div>
                            </div>
                            <div className="label-value-cont">
                              <div
                                className="label-cont"
                                style={{ marginLeft: 0.5 }}
                              >
                                <span className="label-text">DOB</span>
                              </div>
                              <div className="value-cont">
                                <span className="value-text">
                                  {clientProfileData
                                    ? moment(
                                        clientProfileData.createdDate
                                      ).format("DD/MM/YYYY")
                                    : ""}
                                </span>
                              </div>
                            </div>
                          </div>

                          <div className="buttons-row">
                            <div>
                              <div className="black-button">
                                <span className="black-button-text">
                                  Signed
                                </span>
                              </div>
                              <div
                                className={
                                  signed ? "right-green-btn-cont" : "cross-cont"
                                }
                                style={{ cursor: "pointer" }}
                                onClick={() =>
                                  this.setState({ signed: !signed })
                                }
                              >
                                {signed && (
                                  <img
                                    src={Images.tickWhite}
                                    className="svg-btn-img"
                                    style={{ marginRight: 10 }}
                                  />
                                )}
                                <img
                                  src={Images.btnImage}
                                  className="profile-btn-img"
                                />
                                {!signed && (
                                  <img
                                    src={Images.crossWhite}
                                    className="svg-btn-img"
                                    style={{ marginLeft: 10 }}
                                  />
                                )}
                              </div>
                              <div></div>
                            </div>
                            <div style={{ marginLeft: 10 }}>
                              <div className="black-button">
                                <span className="black-button-text">
                                  Onshore
                                </span>
                              </div>
                              <div
                                className={
                                  onshore
                                    ? "right-green-btn-cont"
                                    : "cross-cont"
                                }
                                style={{ cursor: "pointer" }}
                                onClick={() =>
                                  this.setState({ onshore: !onshore })
                                }
                              >
                                {onshore && (
                                  <img
                                    src={Images.tickWhite}
                                    className="svg-btn-img"
                                    style={{ marginRight: 10 }}
                                  />
                                )}
                                <img
                                  src={Images.btnImage}
                                  className="profile-btn-img"
                                />
                                {!onshore && (
                                  <img
                                    src={Images.crossWhite}
                                    className="svg-btn-img"
                                    style={{ marginLeft: 10 }}
                                  />
                                )}
                              </div>
                              <div></div>
                            </div>
                            <div style={{ marginLeft: 10 }}>
                              <div className="black-button">
                                <span className="black-button-text">
                                  Active
                                </span>
                              </div>
                              <div
                                className={
                                  active ? "right-green-btn-cont" : "cross-cont"
                                }
                                style={{ cursor: "pointer" }}
                                onClick={() =>
                                  this.setState({ active: !active })
                                }
                              >
                                {active && (
                                  <img
                                    src={Images.tickWhite}
                                    className="svg-btn-img"
                                    style={{ marginRight: 10 }}
                                  />
                                )}
                                <img
                                  src={Images.btnImage}
                                  className="profile-btn-img"
                                />
                                {!active && (
                                  <img
                                    src={Images.crossWhite}
                                    className="svg-btn-img"
                                    style={{ marginLeft: 10 }}
                                  />
                                )}
                              </div>
                              <div></div>
                            </div>
                          </div>

                          <div>
                            <div className="agent-tag-cont">
                              <div className="agent-tag">
                                <img
                                  src={crossGreen}
                                  style={{ width: 8, height: 8 }}
                                />
                                <span className="tag-text">
                                  Agent Name: Test Agent
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ProcessingPerson
                    clientInfo={clientProfileData}
                    groupsData={groupsData}
                    personsData={teamMembers}
                    onAddProcessingGroup={this.addProcessingGroup}
                    onRemoveProcessingGroup={this.removeProcessingGroup}
                    onAddProcessingPerson={this.addProcessingPerson}
                    onRemoveProcessingPerson={this.removeProcessingPerson}
                    groupMembers={groupMembersData}
                  />
                  <Connections
                    clientInfo={clientProfileData}
                    onSearchConnection={this.searchConnection}
                    searchedConnections={searchConnectionData}
                    onAddConnection={this.addConnection}
                    onRemoveConnection={this.removeConnection}
                  />
                  <div
                    className="profile-additional-box"
                    style={{ paddingBottom: 50 }}
                  >
                    <PersonalInformation
                      clientInfo={clientProfileData}
                      countriesData={countriesData}
                      accessingAuthData={accessingAuthData}
                      visaTypeData={visaTypeData}
                      updatePersonalInfo={this.personalInfoUpdate}
                    />
                    <CurrentVisa
                      clientInfo={clientProfileData}
                      countriesData={countriesData}
                      visaTypeData={visaTypeData}
                      updateProfileCurrentVisa={this.onUpdateCurrentVisa}
                    />
                    <Medicals
                      clientInfo={clientProfileData}
                      countriesData={countriesData}
                      visaTypeData={visaTypeData}
                      updateProfileMedicals={this.onUpdateMedicals}
                    />
                    <Passport
                      clientInfo={clientProfileData}
                      countriesData={countriesData}
                      onAddCertificate={this.addCertificate}
                      onAddAuthority={this.addAuthority}
                      onRemoveCertificate={this.removeCertificate}
                      onRemoveAuthority={this.removeAuthority}
                      accessingAuthData={accessingAuthData}
                      visaTypeData={visaTypeData}
                      updateProfilePassport={this.onUpdatePassport}
                    />
                    <InzLogin
                      clientInfo={clientProfileData}
                      countriesData={countriesData}
                      visaTypeData={visaTypeData}
                      updateProfileInzDetail={this.onUpdateInzUserDetail}
                      isClient={true}
                    />
                    <NZQADetail
                      clientInfo={clientProfileData}
                      countriesData={countriesData}
                      visaTypeData={visaTypeData}
                      updateProfileNZQA={this.onUpdateNZQA}
                    />
                    <BillingAddress
                      clientInfo={clientProfileData}
                      countriesData={countriesData}
                      visaTypeData={visaTypeData}
                      updateProfileBillingAddress={this.onUpdateBillingAddress}
                    />
                    <div
                      className="denied-cont"
                      style={{ justifyContent: "space-between" }}
                    >
                      <div className="denied-cont">
                        <div className="profile-down-arrow-cont">
                          <img
                            src={Images.whiteArrow}
                            className="profile-down-arrow-icon"
                          />
                        </div>
                        <span className="denied-text">Client Area Access</span>
                      </div>
                      <Button
                        loading={updLoading}
                        className="button-blue"
                        onClick={this.onUpdateNZQA}
                      >
                        <span style={{ color: "#FFFFFF" }}>Update</span>
                      </Button>
                    </div>
                    <div className="form-container">
                      <div
                        className="buttons-row"
                        style={{ paddingTop: 20, marginBottom: 0 }}
                      >
                        {/*<div>
                            <div className="black-button" style={{ width: 95 }}>
                              <span
                                className="black-button-text"
                                style={{ fontSize: 8 }}
                              >
                                Allow Update Partner
                              </span>
                            </div>
                            <div className="cross-cont" style={{ width: 95 }}>
                              <img src={Images.btnImage} className="profile-btn-img" />
                              <img
                                src={Images.crossWhite}
                                className="svg-btn-img"
                                style={{ paddingLeft: 28, paddingRight: 28 }}
                              />
                            </div>
                            <div></div>
                          </div>*/}
                        <div style={{ marginLeft: 10 }}>
                          <div className="black-button" style={{ width: 95 }}>
                            <span
                              className="black-button-text"
                              style={{ fontSize: 8 }}
                            >
                              Allow Update
                            </span>
                          </div>
                          <div
                            className={
                              allowUpdate
                                ? "right-green-btn-cont"
                                : "cross-cont"
                            }
                            style={{ width: 95, cursor: "pointer" }}
                            onClick={() =>
                              this.setState({ allowUpdate: !allowUpdate })
                            }
                          >
                            {allowUpdate && (
                              <img
                                src={Images.tickWhite}
                                className="svg-btn-img"
                                style={{ marginRight: 20 }}
                              />
                            )}
                            <img
                              src={Images.btnImage}
                              className="profile-btn-img"
                            />
                            {!allowUpdate && (
                              <img
                                src={Images.crossWhite}
                                className="svg-btn-img"
                                style={{ marginLeft: 20 }}
                              />
                            )}
                          </div>
                          <div></div>
                        </div>
                        <div style={{ marginLeft: 10 }}>
                          <div className="black-button" style={{ width: 95 }}>
                            <span
                              className="black-button-text"
                              style={{ fontSize: 8 }}
                            >
                              Area Access
                            </span>
                          </div>
                          <div
                            className={
                              areaAccess ? "right-green-btn-cont" : "cross-cont"
                            }
                            style={{ width: 95, cursor: "pointer" }}
                            onClick={() =>
                              this.setState({ areaAccess: !areaAccess })
                            }
                          >
                            {areaAccess && (
                              <img
                                src={Images.tickWhite}
                                className="svg-btn-img"
                                style={{ marginRight: 20 }}
                              />
                            )}
                            <img
                              src={Images.btnImage}
                              className="profile-btn-img"
                            />
                            {!areaAccess && (
                              <img
                                src={Images.crossWhite}
                                className="svg-btn-img"
                                style={{ marginLeft: 20 }}
                              />
                            )}
                          </div>
                          <div></div>
                        </div>
                      </div>
                    </div>
                    {/*<div
                        className="button-blue-cont"
                        style={{ marginTop: 23, marginLeft: 10 }}
                      >
                        <Form.Item>
                          <Button className="button-blue" htmlType="submit">
                            <span style={{ color: "#FFFFFF" }}>Update</span>
                          </Button>
                        </Form.Item>
                      </div>*/}
                  </div>
                </div>
                {/*<div
                    style={{
                      float: "left",
                      position: "fixed",
                      left: 250,
                      bottom: 30
                    }}
                  >
                      <Button
                        loading={updLoading}
                        className="button-blue"
                        htmlType="submit"
                      >
                        <span style={{ color: "#FFFFFF" }}>Update</span>
                      </Button>
                  </div>*/}
              </div>
              <div
                className="profile-first-box"
                style={{ width: "44%", marginLeft: 30, height: 428 }}
              >
                <div style={{ display: "flex" }}>
                  <div>
                    <ProfileSideBar
                      showClient={showClient}
                      setShowClient={this.setShowClient}
                    />
                  </div>
                  <div>
                    {showClient === true ? (
                      <div className="form-container">
                        <div
                          style={{
                            paddingTop: 5,
                            paddingLeft: 8,
                            paddingRight: 8,
                          }}
                        >
                          <span className="visa-type-text">Client Tags</span>
                        </div>
                        <div
                          style={{
                            paddingTop: 5,
                            paddingLeft: 8,
                            paddingRight: 8,
                          }}
                        >
                          <span className="visa-date-text">
                            Visa Status Date: 07/02/2020
                          </span>
                        </div>
                        <div style={{ padding: 10 }}>
                          <Select
                            style={{ width: "100%" }}
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={options}
                          />
                        </div>
                        <div className="button-blue-cont">
                          <div className="button-blue">
                            <span style={{ color: "#FFFFFF" }}>Save</span>
                          </div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}

                    <div className="form-container">
                      <div
                        style={{
                          paddingTop: 5,
                          paddingLeft: 8,
                          paddingRight: 8,
                        }}
                      >
                        <span className="visa-type-text">
                          Visa Type - INVESTOR PLUS (INVESTOR 1 CATEGORY)
                        </span>
                      </div>
                      <div
                        style={{
                          paddingTop: 5,
                          paddingLeft: 8,
                          paddingRight: 8,
                        }}
                      >
                        <span className="visa-date-text">
                          Visa Status Date: 07/02/2020
                        </span>
                      </div>
                      <div style={{ padding: 10 }}>
                        <Select
                          style={{ width: "100%" }}
                          value={selectedOption}
                          onChange={this.handleChange}
                          options={options}
                        />
                      </div>
                      <div className="button-blue-cont">
                        <div className="button-blue">
                          <span style={{ color: "#FFFFFF" }}>Save</span>
                        </div>
                      </div>
                    </div>

                    <div className="form-container">
                      <div
                        style={{
                          paddingTop: 5,
                          paddingLeft: 8,
                          paddingRight: 8,
                        }}
                      >
                        <span className="visa-type-text">
                          Visa Type - CITIZENSHIP
                        </span>
                      </div>
                      <div
                        style={{
                          paddingTop: 5,
                          paddingLeft: 8,
                          paddingRight: 8,
                        }}
                      >
                        <span className="visa-date-text">
                          Visa Status Date: 07/02/2020
                        </span>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          padding: 10,
                        }}
                      >
                        <div>
                          <span className="case-priority">Case Priority</span>
                          <div style={{ display: "flex", marginTop: 5 }}>
                            <div className="priority-low-border"></div>
                            <div
                              className="priority-high-filled"
                              style={{ marginLeft: 2 }}
                            ></div>
                            <div
                              className="priority-medium-border"
                              style={{ marginLeft: 2 }}
                            ></div>
                          </div>
                        </div>
                        <div>
                          <div style={{ display: "flex" }}>
                            <div>
                              <div className="case-priority-btn">
                                <span className="case-priority-text">Paid</span>
                              </div>
                              <div
                                className="case-priority-cross"
                                style={{ borderRadius: 0 }}
                              >
                                <div className="white-box"></div>
                                <img
                                  src={Images.crossWhite}
                                  style={{ width: 8, height: 8 }}
                                />
                              </div>
                              <div></div>
                            </div>
                            <div style={{ marginLeft: 10 }}>
                              <div className="case-priority-btn">
                                <span className="case-priority-text">
                                  Signed
                                </span>
                              </div>
                              <div
                                className="case-priority-cross"
                                style={{ borderRadius: 0 }}
                              >
                                <div className="white-box"></div>
                                <img
                                  src={Images.crossWhite}
                                  style={{ width: 8, height: 8 }}
                                />
                              </div>
                              <div></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="profile-progressbar-cont">
                        <ProgressBar
                          bgcolor="#CCCCCC"
                          completed={30}
                          color="#9D0C0E"
                        />
                      </div>

                      <div style={{ padding: 10 }}>
                        <Select
                          style={{ width: "100%" }}
                          value={selectedOption}
                          onChange={this.handleChange}
                          options={options}
                        />
                      </div>

                      <div className="button-blue-cont">
                        <div className="button-blue">
                          <span style={{ color: "#FFFFFF" }}>Save</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Profile;
