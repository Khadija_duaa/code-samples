import React, { Fragment, useState, useEffect } from "react";
import { Select, message, DatePicker, Spin } from "antd";
import moment from "moment";

import UpdateStatusCard from "../../Components/ClientVisa/UpdateStatusCard";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const { Option } = Select;

const dateFormat = "DD/MM/YYYY";

const UpdataVisaStatus = ({
  visaStatusData,
  visaAppData,
  onUpdateCaseStatus,
  onGetVisaApplication,

  onSetActiveKey,
  onUpdatePriority,
  visaPriorityRes,

  onSignedVisa,
  onPaidVisa,
  activeKey,
  onGetVisaStatus,
}) => {
  const [caseStatusId, setCaseStatusId] = useState("0");
  const [caseStatusDate, setCaseStatusDate] = useState("");
  const [caseExpiryDate, setCaseExpiryDate] = useState("");
  const [caseApprovedDate, setCaseApprovedDate] = useState("");
  const [loadVisa, setLoadVisa] = useState(false);
  const [apiCall, setApiCall] = useState(false);
  console.log("show visa  status data ", visaStatusData);

  useEffect(() => {
    if (activeKey === "1") {
      setApiCall(true);
      onGetVisaStatus();
      onGetVisaApplication()
        .then((res) => {
          setLoadVisa(false);
        })
        .catch((err) => {
          setLoadVisa(false);
        });
    }
  }, [activeKey]);

  if (visaStatusData && visaStatusData.items) {
    var visaStatuses = [];
    for (var i = 0; i < visaStatusData.items.length; i++) {
      visaStatuses.push(
        <Option key={visaStatusData.items[i].id}>
          {visaStatusData.items[i].name}
        </Option>
      );
    }
  }

  const onChange = (value) => {
    console.log(`selected ${value}`);

    setCaseStatusId(value);
  };

  const onChangeDate = (value, dateString) => {
    setCaseStatusDate(value);
  };

  const onChangeExpiryDate = (value, dateString) => {
    setCaseExpiryDate(value);
  };

  const onChangeApprovedDate = (value, dateString) => {
    setCaseApprovedDate(value);
  };

  const updateStatus = (visa) => {
    let data = {
      id: visa.id,
      caseStatusId: parseInt(caseStatusId),
      approveDate: caseApprovedDate || new Date(),
      expiryDate: caseExpiryDate || new Date(),
      date: caseStatusDate || new Date(),
      branchVisaTypeId: visa.branchVisaTypeId,
    };
    onUpdateCaseStatus(data).then((res) => {
      message.success("Visa status updated successfully");
      onGetVisaApplication();
    });
  };

  return (
    <Fragment>
      <Spin spinning={loadVisa}>
        {visaAppData &&
          visaAppData.items.map((visa, index) => {
            return (
              <div key={index}>
                <UpdateStatusCard
                  visa={visa}
                  onUpdateCaseStatus={onUpdateCaseStatus}
                  onGetVisaApplication={onGetVisaApplication}
                  visaStatusData={visaStatusData}
                  onSetActiveKey={onSetActiveKey}
                  onUpdatePriority={onUpdatePriority}
                  visaPriorityRes={visaPriorityRes}
                  onSignedVisa={onSignedVisa}
                  onPaidVisa={onPaidVisa}
                />
              </div>
            );
          })}
      </Spin>
    </Fragment>
  );
};

export default UpdataVisaStatus;
