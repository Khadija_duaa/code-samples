import React, { useState } from "react";
import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import {
  Form,
  Input,
  Button,
  Select,
  message,
  Upload,
  Spin,
  Modal,
  Row,
  Col,
  DatePicker,
  Dropdown,
  Menu,
} from "antd";
import moment from "moment";

import { saveAs } from "file-saver";

import HeaderBar from "../Components/Header/HeaderBar";
import ProfileSideBar from "../Components/ProfileSideBar";
import "./ProfileStyles.css";
import crossGreen from "../svgs/cross-green.svg";
import HeaderBarTabs from "../Components/Header/HeaderTabs";
import { Images } from "../Themes";
import ProfileTopBarTabs from "../Components/Shared/ProfileTopBar";
import ProgressBar from "../Components/Shared/Progressbar";

import ProcessingPerson from "../Components/Client/ProcessingPerson";
import Connections from "../Components/Client/Connections";
import PersonalInformation from "../Components/Client/PersonalInformation";
import CurrentVisa from "../Components/Client/CurrentVisa";
import Medicals from "../Components/Client/Medicals";
import Passport from "../Components/Client/Passport";
import InzLogin from "../Components/Client/InzLogin";
import NZQADetail from "../Components/Client/NZQADetail";
import BillingAddress from "../Components/Client/BillingAddress";
import ClientAreaEmail from "../Components/Client/ClientAreaEmail";
import { setSelectedBranchId } from "./../store/Actions";

import activityData from "../Components/ClientActivity/ActivityData";

import headOption from "../Components/Header/HeaderTabOptions";
import AddAutoReminder from "../Components/Reminder/AddAutoReminder";
import { LinkQuestionnaire } from "../Containers";
var selectedBranchId = localStorage.getItem("selectedBranchId");

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

// var headOption = [];

const topBar = [
  { tabName: "CLIENT INFORMATION", linkName: "/profile" },
  { tabName: "EMPLOYER INFORMATION", linkName: "/employer" },
  { tabName: "JOB HISTORY", linkName: "/job-history" },
  { tabName: "QUALIFICATION", linkName: "/qualification" },
  { tabName: "OTHER INFO", linkName: "/client-other-info" },
];

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

const dateFormat = "DD/MM/YYYY";

var menu = (
  <Menu>
    {/* <Menu.Item key="0">
      <Link to="/profile">CLIENT PROFILE</Link>
    </Menu.Item>
    <Menu.Item key="1">
      <Link to="/partner-profile">PARTNER DETAILS</Link>
    </Menu.Item>
    {/* <Menu.Divider />
    <Menu.Item key="2">
      <Link to="/client-family-member">FAMILY DETAILS</Link>
    </Menu.Item>
    */}
  </Menu>
);

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      selectedOption: null,
      date: new Date(),
      signedClicked: false,
      onshore: false,
      active: false,
      allowUpdate: false,
      areaAccess: false,
      updLoading: false,
      addClientImageUrl: "",
      loadUploadImage: false,
      showClient: false,
      visible: false,
      loadClient: true,
      signedModalVisible: false,
      headerOptions: [],
      selectedQuestionniareId: null,
      showDetail: false,
    };
    setSelectedBranchId(selectedBranchId);
    // this.setState({ loadClient: true });
    this.props.onSetActiveKey(null);
    let clientProfileIdMain = localStorage.getItem("clientProfileIdMain");
    let userId = localStorage.getItem("userId");
    let clientId = props.location.state
      ? props.location.state.id
      : clientProfileIdMain && clientProfileIdMain;
    setTimeout(() => {
      if (!clientId) {
        this.setState({ loadClient: false });
      }
    }, 1500);
    // if (this.props.clientProfileData && !props.location.state) {
    //   clientId = this.props.clientProfileData.id;
    // }
    if (clientId) {
      this.props
        .onGetClientProfile(clientId)
        .then((res) => {
          let clientNumber =
            res.payload.clientNumber || res.payload.clientNumberIZM;
          localStorage.setItem("ezmId", res.payload.clientNumberIZM);
          localStorage.setItem("inzId", res.payload.clientNumber);
          this.setState({ loadClient: false });
          this.setProfileData(res.payload);
          let data = {
            ezmid: res.payload.clientNumberIZM,
          };
          this.props.onCreateClientLink(data).then((resp) => {
            let tempData = {
              templateName: "clientemail",
              branchId: localStorage.getItem("selectedBranchId"),
              parameters: [
                {
                  key: "Content",
                  value: "CheckMyVisa",
                },
                {
                  key: "ClientId",
                  value: clientId,
                },
                {
                  key: "UserId",
                  value: userId,
                },
                {
                  key: "Link",
                  value:
                    "https://appcmv-stage.ezymigrate.co.nz/setPassword/" +
                    clientNumber,
                },
              ],
            };
            this.props.onSetCmvLinkTemplate(tempData);
            // this.props.onGetVisaApplication();
          });
        })
        .catch((err) => {
          this.setState({ loadClient: false });
        });
      this.props
        .onGetAllFilledQuestionnaire(clientId)
        .then((res) => {
          this.setState({ loading: false });
        })
        .catch((err) => {
          this.setState({ loading: false });
        });
    } else {
      setTimeout(() => {
        this.setState({ loadClient: false });
      }, 1500);
    }
    let selectedTab = {
      headName: "Client Profile",
      headPath: "/profile",
    };
    var branchId = localStorage.getItem("selectedBranchId");
    this.props.onSetClientTab(selectedTab);
    // this.props.onGetUserBranch();
    this.props.getClientSource();
    this.props.onGetClientLink();
    // this.props.onGetAllClient();
    this.props.onGetGroups();
    // this.props.onGetGroupMembers();
    this.props.onGetCountries();
    this.props.onGetTeamMember();
    this.props.onGetAccessingAuth();
    this.props.onGetVisaType();
    // this.props.onGetVisaTypeByCountry("168");
    // this.props.onGetVisaStatus();
    this.props.onSetClientTab(selectedTab);
    // this.props.onGetReminder(branchId);

    // this.props.onAddAccAuthorities(data)
  }

  setProfileData = (data) => {
    var clientEmails = data.emails.find((obj) => obj.emailTypeId == 1);
    var clientSecondaryEmail = data.emails.find((obj) => obj.emailTypeId == 2);
    var clientOtherEmail = data.emails.find((obj) => obj.emailTypeId == 3);

    let profileData = {
      image: data.imageBlobUrl,
      fullName: data.firstName + " " + data.lastName,
      ezmid: data.clientNumberIZM,
      inzNumber: data.clientNumber,
      createdDate: data.createdDate,
      modifiedDate: data.modifiedDate,
      primaryEmail: (clientEmails && clientEmails.address) || "",
      secondaryEmail:
        (clientSecondaryEmail && clientSecondaryEmail.address) || "",
      otherEmail: (clientOtherEmail && clientOtherEmail.address) || "",
      mobileNumber: data.phones.length > 0 ? data.phones[0] : null,
    };

    localStorage.setItem("profileData", JSON.stringify(profileData));
  };

  componentDidUpdate(PrevProps) {
    if (PrevProps.clientTab !== this.props.clientTab) {
      var options = headOption(this.props.clientTab);
      this.setState({ headerOptions: options });
    }
    if (
      PrevProps.allFilledQuestionnaireRes !=
      this.props.allFilledQuestionnaireRes
    ) {
      menu = (
        <Menu>
          {this.props.allFilledQuestionnaireRes.map((questionnaire, index) => (
            <Menu.Item key={index}>
              <div
                onClick={() =>
                  this.setState({
                    selectedQuestionniareId: questionnaire.questionnaireId,
                    showDetail: true,
                  })
                }
              >
                {questionnaire.questionnaireName}
              </div>
            </Menu.Item>
          ))}
        </Menu>
      );
    }
    if (PrevProps.clientProfileData !== this.props.clientProfileData) {
      if (!PrevProps.clientProfileData) {
        for (var i = 0; i < this.props.clientProfileData.groups.length; i++) {
          this.props.onGetGroupMembers(
            this.props.clientProfileData.groups[i].groupId
          );
        }
      }
      this.setState({
        addClientImageUrl: this.props.clientProfileData.imageBlobUrl,
      });
      if (this.props.clientProfileData.clientPermission) {
        this.setState({
          signed: this.props.clientProfileData.clientPermission.signed,
          onshore: this.props.clientProfileData.clientPermission.onshore,
          active: this.props.clientProfileData.clientPermission.active,
          allowUpdate: this.props.clientProfileData.clientPermission
            .allowUpdate,
          areaAccess: this.props.clientProfileData.clientPermission.areaAccess,
        });
      }
      var email = "";
      var secondaryEmail = "";
      var otherEmail = "";
      if (this.props.clientProfileData.emails.length > 0) {
        for (var i = 0; i < this.props.clientProfileData.emails.length; i++) {
          if (this.props.clientProfileData.emails[i].emailTypeId === 1) {
            email = this.props.clientProfileData.emails[i].address;
          }
          if (this.props.clientProfileData.emails[i].emailTypeId === 2) {
            secondaryEmail = this.props.clientProfileData.emails[i].address;
          }
          if (this.props.clientProfileData.emails[i].emailTypeId === 3) {
            otherEmail = this.props.clientProfileData.emails[i].address;
          }
        }
      }
      var billAddressData = null;
      var clientAddressData = null;
      if (this.props.clientProfileData.addresses.length > 0) {
        const findBillAddress = this.props.clientProfileData.addresses.find(
          (obj) => obj.addressTypeId === 2
        );
        if (findBillAddress) {
          billAddressData = {
            contactPerson: findBillAddress.contactPerson,
            flat: findBillAddress.flat,
            streetNumber: findBillAddress.streetNumber,
            suburb: findBillAddress.suburb,
            city: findBillAddress.city,
            country: findBillAddress.country,
            zipcode: findBillAddress.zip,
          };
        }
        const findAddress = this.props.clientProfileData.addresses.find(
          (obj) => obj.addressTypeId === 1
        );
        if (findAddress) {
          clientAddressData = {
            address: findAddress.city,
          };
        }
      }
      var medicalData = null;
      if (this.props.clientProfileData.medicals.length > 0) {
        medicalData = {
          medicalIssueDate: this.props.clientProfileData.medicals[0]
            .medicalIssueDate,
          medicalExpiryDate: this.props.clientProfileData.medicals[0]
            .medicalExpiryDate,
          medicalGrading: this.props.clientProfileData.medicals[0]
            .medicalGrading,
          xrayIssueDate: this.props.clientProfileData.medicals[0].xrayIssueDate,
          xrayExpiryDate: this.props.clientProfileData.medicals[0]
            .xrayExpiryDate,
          xrayGrading: this.props.clientProfileData.medicals[0].xrayGrading,
          medicalNotes: this.props.clientProfileData.medicals[0].medicalNotes,
          medicalNotesDetail: this.props.clientProfileData.medicals[0]
            .medicalNotesDetail,
        };
      }
      var phoneData = {
        mobile: "",
        secondaryMobile: "",
        overseasMobile: "",
        landLine: "",
        otherMobile: "",
      };
      if (this.props.clientProfileData.phones.length > 0) {
        const findMobile = this.props.clientProfileData.phones.find(
          (obj) => obj.phoneTypeId === 1
        );
        if (findMobile) {
          phoneData.mobile = findMobile.contact;
        }
        const findSecondaryMobile = this.props.clientProfileData.phones.find(
          (obj) => obj.phoneTypeId === 2
        );
        if (findSecondaryMobile) {
          phoneData.secondaryMobile = findSecondaryMobile.contact;
        }
        const findOverseasMobile = this.props.clientProfileData.phones.find(
          (obj) => obj.phoneTypeId === 3
        );
        if (findOverseasMobile) {
          phoneData.overseasMobile = findOverseasMobile.contact;
        }
        const findLandLineMobile = this.props.clientProfileData.phones.find(
          (obj) => obj.phoneTypeId === 4
        );
        if (findLandLineMobile) {
          phoneData.landLine = findLandLineMobile.contact;
        }
        const findOtherMobile = this.props.clientProfileData.phones.find(
          (obj) => obj.phoneTypeId === 5
        );
        if (findOtherMobile) {
          phoneData.otherMobile = findOtherMobile.contact;
        }
      }
      // this.formRef.current.setFieldsValue({
      //   visaCountryId: this.props.clientProfileData.visaCountryId.toString(),
      //   visaCountryType: this.props.clientProfileData.visaCountyType.toString(),
      //   title: this.props.clientProfileData.title,
      //   middleName: this.props.clientProfileData.middleName,
      //   gender: this.props.clientProfileData.gender,
      //   dateOfBirth: moment(this.props.clientProfileData.dateOfBirth),
      //   address: clientAddressData.address,
      //   nationalityId: this.props.clientProfileData.nationalityId,
      //   saleDate: moment(this.props.clientProfileData.saleDate),
      //   sourceId: this.props.clientProfileData.sourceId.toString(),
      //   jobSectorId: this.props.clientProfileData.jobSectorId.toString(),
      //   companyOptional: this.props.clientProfileData.companyOptional,
      //   clientSerial: this.props.clientProfileData.clientSerial,
      //   nationalityCountry: this.props.clientProfileData.nationalityCountry,
      //   firstName: this.props.clientProfileData.firstName,
      //   lastName: this.props.clientProfileData.lastName,
      //   age: this.props.clientProfileData.age,
      //   dealWorth: this.props.clientProfileData.dealWorth,
      //   maritalStatus: this.props.clientProfileData.maritalStatus,
      //   dependentChildren: this.props.clientProfileData.dependentChildren,
      //   sourceDescription: this.props.clientProfileData.sourceDescription,
      //   occupation: this.props.clientProfileData.occupation,
      //   visaDenied: this.props.clientProfileData.visaDenied,
      //   deniedText: this.props.clientProfileData.deniedText,
      //   visaText: this.props.clientProfileData.visaText,
      //   currentVisaTypeId: this.props.clientProfileData.currentVisaTypeId.toString(),
      //   currentNewZealandVisaExpiry: moment(
      //     this.props.clientProfileData.currentNewZealandVisaExpiry
      //   ),
      //   travelConditionsValidTo: moment(
      //     this.props.clientProfileData.travelConditionsValidTo
      //   ),
      //   clientNumber: this.props.clientProfileData.clientNumber,
      //   inzUserName: this.props.clientProfileData.inzUserName,
      //   inzPassword: this.props.clientProfileData.inzPassword,
      //   inzFeeDate: moment(this.props.clientProfileData.inzFeeDate),
      //   nzqaOnlineSubDate: moment(
      //     this.props.clientProfileData.nzqaOnlineSubDate
      //   ),
      //   nzqaDocumentSubDate: moment(
      //     this.props.clientProfileData.nzqaDocumentSubDate
      //   ),
      //   nzqaDocumentRetDate: moment(
      //     this.props.clientProfileData.nzqaDocumentRetDate
      //   ),
      //   email: email,
      //   secondaryEmail: secondaryEmail,
      //   otherEmail: otherEmail,
      //   contactPerson: billAddressData ? billAddressData.contactPerson : "",
      //   flat: billAddressData ? billAddressData.flat : "",
      //   streetNumber: billAddressData ? billAddressData.streetNumber : "",
      //   suburb: billAddressData ? billAddressData.suburb : "",
      //   city: billAddressData ? billAddressData.city : "",
      //   billCountry: billAddressData ? billAddressData.country : "",
      //   zipcode: billAddressData ? billAddressData.zipcode : "",
      //   clientAddress: clientAddressData ? clientAddressData.address : "",
      //   mobilePhone: phoneData ? phoneData.mobile : "",
      //   secondaryMobile: phoneData ? phoneData.secondaryMobile : "",
      //   overseasMobile: phoneData ? phoneData.overseasMobile : "",
      //   landLine: phoneData ? phoneData.landLine : "",
      //   otherMobile: phoneData ? phoneData.otherMobile : "",
      //   medicalIssueDate: medicalData
      //     ? moment(medicalData.medicalIssueDate)
      //     : "",
      //   medicalExpiryDate: medicalData
      //     ? moment(medicalData.medicalExpiryDate)
      //     : "",
      //   medicalGrading: medicalData ? medicalData.medicalGrading : "",
      //   xrayIssueDate: medicalData ? moment(medicalData.xrayIssueDate) : "",
      //   xrayExpiryDate: medicalData ? moment(medicalData.xrayExpiryDate) : "",
      //   xrayGrading: medicalData ? medicalData.xrayGrading : "",
      //   medicalNotes: medicalData ? medicalData.medicalNotes : "",
      //   medicalNotesDetail: medicalData ? medicalData.medicalNotesDetail : "",
      //   passportNo:
      //     this.props.clientProfileData.passports.length > 0
      //       ? this.props.clientProfileData.passports[0].passportNo
      //       : "",
      //   passportCountry:
      //     this.props.clientProfileData.passports.length > 0
      //       ? this.props.clientProfileData.passports[0].passportCountry.toString()
      //       : "",
      //   passportIssueDate:
      //     this.props.clientProfileData.passports.length > 0
      //       ? moment(
      //           this.props.clientProfileData.passports[0].passportIssueDate
      //         )
      //       : "",
      //   passportExpiryDate:
      //     this.props.clientProfileData.passports.length > 0
      //       ? moment(
      //           this.props.clientProfileData.passports[0].passportExpiryDate
      //         )
      //       : "",
      //   secondPassportNo:
      //     this.props.clientProfileData.passports.length > 1
      //       ? this.props.clientProfileData.passports[1].passportNo
      //       : "",
      //   secondPassportCountry:
      //     this.props.clientProfileData.passports.length > 1
      //       ? this.props.clientProfileData.passports[1].passportCountry.toString()
      //       : "",
      //   secondPassportIssueDate:
      //     this.props.clientProfileData.passports.length > 1
      //       ? moment(
      //           this.props.clientProfileData.passports[1].passportIssueDate
      //         )
      //       : "",
      //   secondPassportExpiryDate:
      //     this.props.clientProfileData.passports.length > 1
      //       ? moment(
      //           this.props.clientProfileData.passports[1].passportExpiryDate
      //         )
      //       : ""
      // });
    }
  }

  formRef = React.createRef();

  myChangeHandler = (text) => {
    this.setState({ username: text });
  };

  onChange = (value) => {
    console.log(`selected ${value}`);
  };

  onBlur = () => {
    console.log("blur");
  };

  onFocus = () => {
    console.log("focus");
  };

  onSearch = (val) => {
    console.log("search:", val);
  };

  onChangeDate = (date) => this.setState({ date });

  handleChange = (selectedOption) => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };

  addPoliceCertificateInfo = (policeCertificateData) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    let userId = localStorage.getItem("userId");
    let data = {
      clientId: clientprofileid,
      issueDate: "2020-12-07T08:08:09.142Z",
      certificateExpiryDate: "2020-12-07T08:08:09.142Z",
      country: 168,
      createdBy: userId,
    };
  };

  // addCertificate = (data) => {
  //   let clientprofileid = JSON.parse(
  //     window.localStorage.getItem("clientprofileid")
  //   );
  //   this.props.onAddPoliceCertificate(data).then(() => {
  //     message.success("Police Certificate added successfully");
  //     this.props.onGetClientProfile(clientprofileid);
  //   });
  // };

  addAuthority = (data) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.props.onAddAccAuthorities(data).then(() => {
      message.success("Accessing Authority added successfully");
      this.props.onGetClientProfile(clientprofileid);
    });
  };

  addProcessingGroup = (data, groupName) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.props.onAddProcessingGroups(data).then(() => {
      this.props.onGetClientProfile(clientprofileid);
      this.props.onGetGroupMembers(data.groupId);
      let userName = localStorage.getItem("userName");
      var profileData = JSON.parse(localStorage.getItem("profileData"));
      let myData = {
        clientName: profileData.fullName,
        logMessage: "Processing team " + groupName + " added by " + userName,
        date: moment(new Date()).format("DD/MM/YYYY"),
        logType: "Client Information",
        invoiceId: "0",
      };
      activityData(myData);
    });
  };

  addProcessingPerson = (data, personName) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.props.onAddProcessingPerson(data).then(() => {
      this.props.onGetClientProfile(clientprofileid);
      message.success("Updated!");
      let userName = localStorage.getItem("userName");
      var profileData = JSON.parse(localStorage.getItem("profileData"));
      let myData = {
        clientName: profileData.fullName,
        logMessage:
          "Client Information " + personName + " added by " + userName,
        date: moment(new Date()).format("DD/MM/YYYY"),
        logType: "Client Information",
        invoiceId: "0",
      };
      activityData(myData);
    });
  };

  removeCertificate = (data) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.props.onRemovePoliceCertificate(data).then(() => {
      message.success("Police Certificate removed successfully");
      this.props.onGetClientProfile(clientprofileid);
    });
  };

  removeAuthority = (data) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.props.onRemoveAccesingAuthority(data).then(() => {
      message.success("Accessing Authority removed successfully");
      this.props.onGetClientProfile(clientprofileid);
    });
  };

  removeProcessingGroup = (data, groupName) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.props.onRemoveProcessingGroup(data).then(() => {
      message.success("Processing group removed successfully");
      this.props.onGetClientProfile(clientprofileid);
      let userName = localStorage.getItem("userName");
      var profileData = JSON.parse(localStorage.getItem("profileData"));
      let myData = {
        clientName: profileData.fullName,
        logMessage: "Processing team " + groupName + " removed by " + userName,
        date: moment(new Date()).format("DD/MM/YYYY"),
        logType: "Client Information",
        invoiceId: "0",
      };
      activityData(myData);
    });
  };

  removeProcessingPerson = (data, personName) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.props.onRemoveProcessingPerson(data).then(() => {
      message.success("Processing person removed successfully");
      this.props.onGetClientProfile(clientprofileid);
      let userName = localStorage.getItem("userName");
      var profileData = JSON.parse(localStorage.getItem("profileData"));
      let myData = {
        clientName: profileData.fullName,
        logMessage:
          "Client Information " + personName + " removed by " + userName,
        date: moment(new Date()).format("DD/MM/YYYY"),
        logType: "Client Information",
        invoiceId: "0",
      };
      activityData(myData);
    });
  };

  searchConnection = (data) => {
    this.props.onSearchConnection(data).then((res) => {});
  };

  addConnection = (data, connectionData) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.props.onAddConnection(data).then((res) => {
      this.props.onGetClientProfile(clientprofileid);
      message.success("Client connection added successfully");
      let userName = localStorage.getItem("userName");
      var profileData = JSON.parse(localStorage.getItem("profileData"));
      let myData = {
        clientName: profileData.fullName,
        logMessage:
          "connection " +
          connectionData.firstName +
          " " +
          connectionData.lastName +
          " added by " +
          userName,
        date: moment(new Date()).format("DD/MM/YYYY"),
        logType: "Client Information",
        invoiceId: "0",
      };
      activityData(myData);
    });
  };

  removeConnection = (data, connectionName) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.props.onRemoveConnection(data).then((res) => {
      this.props.onGetClientProfile(clientprofileid);
      message.success("Client connection removed successfully");
      let userName = localStorage.getItem("userName");
      var profileData = JSON.parse(localStorage.getItem("profileData"));
      let myData = {
        clientName: profileData.fullName,
        logMessage: "connection " + connectionName + " removed by " + userName,
        date: moment(new Date()).format("DD/MM/YYYY"),
        logType: "Client Information",
        invoiceId: "0",
      };
      activityData(myData);
    });
  };

  checkDuplicateEmail = (values, content) => {
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    let data = {
      email: values.email,
      emailTypeId: 1,
      branchId: selectedBranchId,
    };

    this.props
      .onCheckUniqueEmail(data)
      .then((res) => {
        // this.personalInfoUpdate(values, content);
      })
      .catch((err) => {
        if (err.response && err.response.status === 409) {
          const create = window.confirm(
            "Client with email: " +
              values.email +
              " already exist. Do you want to continue?"
          );
          if (create) {
            // this.personalInfoUpdate(values, content);
          }
        }
      });
  };

  personalInfoUpdate = (values, content) => {
    let date1 = new Date();
    let date2 = new Date(this.props.clientProfileData.dateOfBirth);
    let yearsDiff = 0;
    var setReminder = false;
    if (
      this.props.clientProfileData.dateOfBirth !== "1900-01-01T00:00:00+00:00"
    ) {
      yearsDiff = date1.getFullYear() - date2.getFullYear();
    }
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    const userId = localStorage.getItem("userId");
    this.setState({ loadClient: true });
    if (this.props.clientProfileData.addresses.length > 0) {
      let addressData = [];
      for (var i = 0; i < this.props.clientProfileData.addresses.length; i++) {
        let addressValues = {
          id: this.props.clientProfileData.addresses[i].id,
          clientId: this.props.clientProfileData.addresses[i].clientId,
          contactPerson: this.props.clientProfileData.addresses[i]
            .contactPerson,
          flat: this.props.clientProfileData.addresses[i].flat,
          building: this.props.clientProfileData.addresses[i].building,
          streetName: this.props.clientProfileData.addresses[i].streetName,
          suburb: this.props.clientProfileData.addresses[i].suburb,
          streetNumber: this.props.clientProfileData.addresses[i].streetNumber,
          city:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? values.address || ""
              : this.props.clientProfileData.addresses[i].city,
          state: this.props.clientProfileData.addresses[i].state,
          zip: this.props.clientProfileData.addresses[i].zip,
          country: this.props.clientProfileData.addresses[i].country,
          addressTypeId: this.props.clientProfileData.addresses[i]
            .addressTypeId,
          modifiedBy: userId,
        };
        addressData.push(addressValues);
      }
      this.props.onUpdClientAddress(addressData);
    }
    if (this.props.clientProfileData.emails.length > 0) {
      let emailData = [];
      for (var i = 0; i < this.props.clientProfileData.emails.length; i++) {
        let emailValues = {
          id: this.props.clientProfileData.emails[i].id,
          clientId: this.props.clientProfileData.emails[i].clientId,
          address:
            this.props.clientProfileData.emails[i].emailTypeId === 1
              ? values.email || ""
              : this.props.clientProfileData.emails[i].emailTypeId === 2
              ? values.secondaryEmail || ""
              : values.otherEmail || "",
          emailTypeId: this.props.clientProfileData.emails[i].emailTypeId,
          modifiedBy: userId,
        };
        emailData.push(emailValues);
      }
      this.props.onUpdClientEmail(emailData);
    }
    if (this.props.clientProfileData.phones.length > 0) {
      let phonesData = [];
      for (var i = 0; i < this.props.clientProfileData.phones.length; i++) {
        let phonesValues = {
          id: this.props.clientProfileData.phones[i].id,
          clientId: this.props.clientProfileData.phones[i].clientId,
          contact:
            this.props.clientProfileData.phones[i].phoneTypeId === 1
              ? (values.mobilePhone && values.mobilePhone.toString()) || ""
              : this.props.clientProfileData.phones[i].phoneTypeId === 2
              ? values.secondaryMobile || ""
              : this.props.clientProfileData.phones[i].phoneTypeId === 3
              ? values.overseasMobile || ""
              : this.props.clientProfileData.phones[i].phoneTypeId === 4
              ? values.landLine || ""
              : values.otherMobile || "",
          countryCodeId:
            this.props.clientProfileData.phones[i].phoneTypeId === 1
              ? parseInt(values.countryCodeId) ||
                this.props.clientProfileData.phones[i].countryCodeId
              : this.props.clientProfileData.phones[i].countryCodeId,
          phoneTypeId: this.props.clientProfileData.phones[i].phoneTypeId,
          modifiedBy: userId,
        };
        phonesData.push(phonesValues);
      }
      this.props.onUpdClientPhone(phonesData);
    }
    var dateOfBirth = "";
    if (values.dateOfBirth) {
      dateOfBirth = moment(values.dateOfBirth).format(
        "YYYY-MM-DDT00:00:00+00:00"
      );
      // dateOfBirth.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
      // dateOfBirth.toISOString();
      // dateOfBirth.format();
    }

    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: values.agentId || "00000000-0000-0000-0000-000000000000",
      clientTag: this.props.clientProfileData.clientTag,
      firstName: values.firstName
        ? values.firstName
        : this.props.clientProfileData.firstName,
      lastName: values.lastName
        ? values.lastName
        : this.props.clientProfileData.lastName,
      middleName: values.middleName
        ? values.middleName
        : this.props.clientProfileData.middleName,
      title: values.title ? values.title : "",
      gender: values.gender ? parseInt(values.gender) : 0,
      dateOfBirth: dateOfBirth ? dateOfBirth : "1900-01-01T00:00:00+00:00",
      maritalStatus: values.maritalStatus
        ? values.maritalStatus
        : this.props.clientProfileData.maritalStatus,
      dependentChildren: values.dependentChildren
        ? parseInt(values.dependentChildren)
        : 0,
      notes: content || "",
      occupation: values.occupation ? values.occupation : "",
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: values.inzUserName
        ? values.inzUserName
        : this.props.clientProfileData.inzUserName,
      inzPassword: values.inzPassword
        ? values.inzPassword
        : this.props.clientProfileData.inzPassword,
      imageBlobUrl: this.props.clientProfileData.imageBlobUrl,
      nationalityId: values.nationalityId !== "" ? values.nationalityId : "",
      nationalityCountry: values.nationalityCountry
        ? values.nationalityCountry.toString()
        : "",
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        values.currentVisaTypeId
          ? values.currentVisaTypeId
          : this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry
        ? values.currentNewZealandVisaExpiry
        : this.props.clientProfileData.currentNewZealandVisaExpiry,
      travelConditionsValidTo: values.travelConditionsValidTo
        ? values.travelConditionsValidTo
        : this.props.clientProfileData.travelConditionsValidTo,
      visaText: values.visaText
        ? values.visaText
        : this.props.clientProfileData.visaText,
      visaDenied: values.visaDenied,
      deniedText: values.deniedText ? values.deniedText : "",
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: values.inzFeeDate
        ? moment(values.inzFeeDate).format("YYYY-MM-DDT00:00:00+00:00")
        : this.props.clientProfileData.inzFeeDate,
      interestedVisa: parseInt(values.interestedVisa) || 0,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: values.nzqaOnlineSubDate
        ? moment(values.nzqaOnlineSubDate).format("YYYY-MM-DDT00:00:00+00:00")
        : this.props.clientProfileData.nzqaOnlineSubDate,
      nzqaDocumentSubDate: values.nzqaDocumentSubDate
        ? moment(values.nzqaDocumentSubDate).format("YYYY-MM-DDT00:00:00+00:00")
        : this.props.clientProfileData.nzqaDocumentSubDate,
      nzqaDocumentRetDate: values.nzqaDocumentRetDate
        ? moment(values.nzqaDocumentRetDate).format("YYYY-MM-DDT00:00:00+00:00")
        : this.props.clientProfileData.nzqaDocumentRetDate,
      visaCountryId: parseInt(
        values.visaCountryId
          ? values.visaCountryId
          : this.props.clientProfileData.visaCountryId
      ),
      visaCountyType: parseInt(
        values.visaCountryType
          ? values.visaCountryType
          : this.props.clientProfileData.visaCountyType
      ),
      age: yearsDiff !== 0 ? yearsDiff : this.props.clientProfileData.age,
      jobSectorId: parseInt(
        values.jobSectorId
          ? values.jobSectorId
          : this.props.clientProfileData.jobSectorId
      ),
      sourceId: parseInt(
        values.sourceId
          ? values.sourceId
          : this.props.clientProfileData.sourceId
      ),
      sourceDescription: values.sourceDescription
        ? values.sourceDescription
        : "",
      clientSerial: values.clientSerial
        ? values.clientSerial
        : this.props.clientProfileData.clientSerial,
      companyOptional: values.companyOptional ? values.companyOptional : "",
      dealWorth: values.dealWorth ? values.dealWorth : "",
      saleDate: values.saleDate
        ? moment(values.saleDate).format("YYYY-MM-DDT00:00:00+00:00")
        : "1900-01-01T00:00:00+00:00",
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };

    var dob = moment(dateOfBirth).format("DD/MM/YYYY");
    var oldDob = moment(this.props.clientProfileData.dateOfBirth).format(
      "DD/MM/YYYY"
    );

    if (dob != oldDob && dob != "01/01/1900") {
      setReminder = true;
    }
    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        if (setReminder) {
          var profileData = JSON.parse(localStorage.getItem("profileData"));

          let data = {
            subjectId: clientprofileid,
            subjectName: profileData && profileData.fullName,
            branchId: localStorage.getItem("selectedBranchId"),
            reminderTypeId: 1,
            reminderDescription: "Date of Birth Reminder",
            reminderDetail: "Date of Birth",
            isCompleted: false,
            reminderDate: moment(dateOfBirth).format(
              "YYYY-MM-DDT00:00:00+00:00"
            ),
            createdBy: localStorage.getItem("userId"),
          };
          this.props.onAddReminderTask(data);
        }
        this.props
          .onGetClientProfile(clientprofileid)
          .then((res) => {
            this.setState({ loadClient: false });
            this.setProfileData(res.payload);
          })
          .catch(() => {
            this.setState({ loadClient: false });
          });
        let userName = localStorage.getItem("userName");
        let myData = {
          clientName: "",
          logMessage: "Client Information updated by " + userName,
          date: moment(new Date()).format("DD/MM/YYYY"),
          logType: "Client Information",
          invoiceId: "0",
        };
        activityData(myData);
      })
      .catch(() => {
        this.setState({ updLoading: false, loadClient: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile(clientprofileid);
      });
  };

  onUpdateCurrentVisa = (values) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    const userId = localStorage.getItem("userId");
    this.setState({ loadClient: true });
    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: values.firstName
        ? values.firstName
        : this.props.clientProfileData.firstName,
      lastName: values.lastName
        ? values.lastName
        : this.props.clientProfileData.lastName,
      middleName: values.middleName
        ? values.middleName
        : this.props.clientProfileData.middleName,
      title: values.title ? values.title : this.props.clientProfileData.title,
      gender: values.gender
        ? values.gender
        : this.props.clientProfileData.gender,
      dateOfBirth: this.props.clientProfileData.dateOfBirth,
      maritalStatus: values.maritalStatus
        ? values.maritalStatus
        : this.props.clientProfileData.maritalStatus,
      dependentChildren: values.dependentChildren
        ? values.dependentChildren
        : this.props.clientProfileData.dependentChildren,
      notes: this.props.clientProfileData.notes,
      occupation: values.occupation
        ? values.occupation
        : this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: values.inzUserName
        ? values.inzUserName
        : this.props.clientProfileData.inzUserName,
      inzPassword: values.inzPassword
        ? values.inzPassword
        : this.props.clientProfileData.inzPassword,
      imageBlobUrl: this.props.clientProfileData.imageBlobUrl,
      nationalityId: values.nationalityId
        ? values.nationalityId
        : this.props.clientProfileData.nationalityId,
      nationalityCountry: values.nationalityCountry
        ? values.nationalityCountry.toString()
        : this.props.clientProfileData.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        values.currentVisaTypeId
          ? values.currentVisaTypeId
          : this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry
        ? moment(values.currentNewZealandVisaExpiry).format(
            "YYYY-MM-DDT00:00:00+00:00"
          )
        : "1900-01-01T00:00:00+00:00",
      travelConditionsValidTo: values.travelConditionsValidTo
        ? moment(values.travelConditionsValidTo).format(
            "YYYY-MM-DDT00:00:00+00:00"
          )
        : "1900-01-01T00:00:00+00:00",
      visaText: values.visaText
        ? values.visaText
        : this.props.clientProfileData.visaText,
      visaDenied: this.props.clientProfileData.visaDenied,
      deniedText: values.deniedText
        ? values.deniedText
        : this.props.clientProfileData.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: values.inzFeeDate
        ? values.inzFeeDate
        : this.props.clientProfileData.inzFeeDate,
      interestedVisa: this.props.clientProfileData.interestedVisa,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: values.nzqaOnlineSubDate
        ? values.nzqaOnlineSubDate
        : this.props.clientProfileData.nzqaOnlineSubDate,
      nzqaDocumentSubDate: values.nzqaDocumentSubDate
        ? values.nzqaDocumentSubDate
        : this.props.clientProfileData.nzqaDocumentSubDate,
      nzqaDocumentRetDate: values.nzqaDocumentRetDate
        ? values.nzqaDocumentRetDate
        : this.props.clientProfileData.nzqaDocumentRetDate,
      visaCountryId: parseInt(
        values.visaCountryId
          ? values.visaCountryId
          : this.props.clientProfileData.visaCountryId
      ),
      visaCountyType: parseInt(
        values.visaCountryType
          ? values.visaCountryType
          : this.props.clientProfileData.visaCountyType
      ),
      age: values.age ? values.age : this.props.clientProfileData.age,
      jobSectorId: parseInt(
        values.jobSectorId
          ? values.jobSectorId
          : this.props.clientProfileData.jobSectorId
      ),
      sourceId: parseInt(
        values.sourceId
          ? values.sourceId
          : this.props.clientProfileData.sourceId
      ),
      sourceDescription: values.sourceDescription
        ? values.sourceDescription
        : this.props.clientProfileData.sourceDescription,
      clientSerial: values.clientSerial
        ? values.clientSerial
        : this.props.clientProfileData.clientSerial,
      companyOptional: values.companyOptional
        ? values.companyOptional
        : this.props.clientProfileData.companyOptional,
      dealWorth: values.dealWorth
        ? values.dealWorth
        : this.props.clientProfileData.dealWorth,
      saleDate: values.saleDate
        ? values.saleDate
        : this.props.clientProfileData.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };

    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props
          .onGetClientProfile(clientprofileid)
          .then(() => {
            this.setState({ loadClient: false });
          })
          .catch(() => {
            this.setState({ loadClient: false });
          });
        if (
          moment(data.currentNewZealandVisaExpiry).format("DD/MM/YYYY") !=
            moment(
              this.props.clientProfileData.currentNewZealandVisaExpiry
            ).format("DD/MM/YYYY") &&
          moment(data.currentNewZealandVisaExpiry).format("DD/MM/YYYY") !=
            "01/01/1900"
        ) {
          let myData = {
            title: "Current Visa Expiry",
            reminderDate: values.currentNewZealandVisaExpiry
              ? moment(values.currentNewZealandVisaExpiry).format(
                  "YYYY-MM-DDT00:00:00+00:00"
                )
              : "1900-01-01T00:00:00+00:00",
          };
          AddAutoReminder(myData);
        }
        if (
          moment(data.travelConditionsValidTo).format("DD/MM/YYYY") !=
            moment(this.props.clientProfileData.travelConditionsValidTo).format(
              "DD/MM/YYYY"
            ) &&
          moment(data.travelConditionsValidTo).format("DD/MM/YYYY") !=
            "01/01/1900"
        ) {
          let tcData = {
            title: "Travel Condition Valid To",
            reminderDate: values.travelConditionsValidTo
              ? moment(values.travelConditionsValidTo).format(
                  "YYYY-MM-DDT00:00:00+00:00"
                )
              : "1900-01-01T00:00:00+00:00",
          };
          AddAutoReminder(tcData);
        }
      })
      .catch(() => {
        this.setState({ updLoading: false, loadClient: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile(clientprofileid);
      });
  };

  onUpdateNZQA = (values) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    const userId = localStorage.getItem("userId");
    this.setState({ loadClient: true });
    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: values.firstName
        ? values.firstName
        : this.props.clientProfileData.firstName,
      lastName: values.lastName
        ? values.lastName
        : this.props.clientProfileData.lastName,
      middleName: values.middleName
        ? values.middleName
        : this.props.clientProfileData.middleName,
      title: values.title ? values.title : this.props.clientProfileData.title,
      gender: values.gender
        ? values.gender
        : this.props.clientProfileData.gender,
      dateOfBirth: this.props.clientProfileData.dateOfBirth,
      maritalStatus: values.maritalStatus
        ? values.maritalStatus
        : this.props.clientProfileData.maritalStatus,
      dependentChildren: values.dependentChildren
        ? values.dependentChildren
        : this.props.clientProfileData.dependentChildren,
      notes: this.props.clientProfileData.notes,
      occupation: values.occupation
        ? values.occupation
        : this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: values.inzUserName
        ? values.inzUserName
        : this.props.clientProfileData.inzUserName,
      inzPassword: values.inzPassword
        ? values.inzPassword
        : this.props.clientProfileData.inzPassword,
      imageBlobUrl: this.props.clientProfileData.imageBlobUrl,
      nationalityId: values.nationalityId
        ? values.nationalityId
        : this.props.clientProfileData.nationalityId,
      nationalityCountry: values.nationalityCountry
        ? values.nationalityCountry.toString()
        : this.props.clientProfileData.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        values.currentVisaTypeId
          ? values.currentVisaTypeId
          : this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry
        ? values.currentNewZealandVisaExpiry
        : this.props.clientProfileData.currentNewZealandVisaExpiry,
      travelConditionsValidTo: values.travelConditionsValidTo
        ? values.travelConditionsValidTo
        : this.props.clientProfileData.travelConditionsValidTo,
      visaText: values.visaText
        ? values.visaText
        : this.props.clientProfileData.visaText,
      visaDenied: this.props.clientProfileData.visaDenied,
      deniedText: values.deniedText
        ? values.deniedText
        : this.props.clientProfileData.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: values.inzFeeDate
        ? values.inzFeeDate
        : this.props.clientProfileData.inzFeeDate,
      interestedVisa: this.props.clientProfileData.interestedVisa,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: values.nzqaOnlineSubDate
        ? moment(values.nzqaOnlineSubDate).format("YYYY-MM-DDT00:00:00+00:00")
        : "1900-01-01T00:00:00+00:00",
      nzqaDocumentSubDate: values.nzqaDocumentSubDate
        ? moment(values.nzqaDocumentSubDate).format("YYYY-MM-DDT00:00:00+00:00")
        : "1900-01-01T00:00:00+00:00",
      nzqaDocumentRetDate: values.nzqaDocumentRetDate
        ? moment(values.nzqaDocumentRetDate).format("YYYY-MM-DDT00:00:00+00:00")
        : "1900-01-01T00:00:00+00:00",
      visaCountryId: parseInt(
        values.visaCountryId
          ? values.visaCountryId
          : this.props.clientProfileData.visaCountryId
      ),
      visaCountyType: parseInt(
        values.visaCountryType
          ? values.visaCountryType
          : this.props.clientProfileData.visaCountyType
      ),
      age: values.age ? values.age : this.props.clientProfileData.age,
      jobSectorId: parseInt(
        values.jobSectorId
          ? values.jobSectorId
          : this.props.clientProfileData.jobSectorId
      ),
      sourceId: parseInt(
        values.sourceId
          ? values.sourceId
          : this.props.clientProfileData.sourceId
      ),
      sourceDescription: values.sourceDescription
        ? values.sourceDescription
        : this.props.clientProfileData.sourceDescription,
      clientSerial: values.clientSerial
        ? values.clientSerial
        : this.props.clientProfileData.clientSerial,
      companyOptional: values.companyOptional
        ? values.companyOptional
        : this.props.clientProfileData.companyOptional,
      dealWorth: values.dealWorth
        ? values.dealWorth
        : this.props.clientProfileData.dealWorth,
      saleDate: values.saleDate
        ? values.saleDate
        : this.props.clientProfileData.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };
    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props
          .onGetClientProfile(clientprofileid)
          .then(() => {
            this.setState({ loadClient: false });
          })
          .catch(() => {
            this.setState({ loadClient: false });
          });
      })
      .catch(() => {
        this.setState({ updLoading: false, loadClient: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile(clientprofileid);
      });
  };

  onUpdateMedicals = (values, content) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    const userId = localStorage.getItem("userId");
    this.setState({ loadClient: true });
    // let data = {
    //   id: this.props.clientProfileData.id,
    //   branchId: this.props.clientProfileData.branchId,
    //   clientNumber: this.props.clientProfileData.clientNumber,
    //   familyId: this.props.clientProfileData.familyId,
    //   processingGroupId: 0,
    //   agentId: this.props.clientProfileData.agentId,
    //   clientTag: this.props.clientProfileData.clientTag,
    //   firstName: values.firstName
    //     ? values.firstName
    //     : this.props.clientProfileData.firstName,
    //   lastName: values.lastName
    //     ? values.lastName
    //     : this.props.clientProfileData.lastName,
    //   middleName: values.middleName
    //     ? values.middleName
    //     : this.props.clientProfileData.middleName,
    //   title: values.title ? values.title : this.props.clientProfileData.title,
    //   gender: values.gender
    //     ? values.gender
    //     : this.props.clientProfileData.gender,
    //   dateOfBirth: values.dateOfBirth
    //     ? values.dateOfBirth
    //     : this.props.clientProfileData.dateOfBirth,
    //   maritalStatus: values.maritalStatus
    //     ? values.maritalStatus
    //     : this.props.clientProfileData.maritalStatus,
    //   dependentChildren: values.dependentChildren
    //     ? values.dependentChildren
    //     : this.props.clientProfileData.dependentChildren,
    //   notes: "string",
    //   occupation: values.occupation
    //     ? values.occupation
    //     : this.props.clientProfileData.occupation,
    //   occupationOrganization: this.props.clientProfileData
    //     .occupationOrganization,
    //   inzUserName: values.inzUserName
    //     ? values.inzUserName
    //     : this.props.clientProfileData.inzUserName,
    //   inzPassword: values.inzPassword
    //     ? values.inzPassword
    //     : this.props.clientProfileData.inzPassword,
    //   imageBlobUrl: this.props.clientProfileData.imageBlobUrl,
    //   nationalityId: values.nationalityId
    //     ? values.nationalityId
    //     : this.props.clientProfileData.nationalityId,
    //   nationalityCountry: values.nationalityCountry
    //     ? values.nationalityCountry
    //     : this.props.clientProfileData.nationalityCountry,
    //   skypeID: this.props.clientProfileData.skypeID,
    //   preferredName: this.props.clientProfileData.preferredName,
    //   isSubscribed: this.props.clientProfileData.isSubscribed,
    //   arbitaryJson: this.props.clientProfileData.arbitaryJson,
    //   dependentClientIds: this.props.clientProfileData.dependentClientIds,
    //   modifiedBy: userId,
    //   currentVisaTypeId: parseInt(
    //     values.currentVisaTypeId
    //       ? values.currentVisaTypeId
    //       : this.props.clientProfileData.currentVisaTypeId
    //   ),
    //   medicalGrading: values.medicalGrading ? values.medicalGrading : "",
    //   currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry
    //     ? values.currentNewZealandVisaExpiry
    //     : this.props.clientProfileData.currentNewZealandVisaExpiry,
    //   travelConditionsValidTo: values.travelConditionsValidTo
    //     ? values.travelConditionsValidTo
    //     : this.props.clientProfileData.travelConditionsValidTo,
    //   visaText: values.visaText
    //     ? values.visaText
    //     : this.props.clientProfileData.visaText,
    //   visaDenied: this.props.clientProfileData.visaDenied,
    //   deniedText: values.deniedText
    //     ? values.deniedText
    //     : this.props.clientProfileData.deniedText,
    //   clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
    //   inzFeeDate: values.inzFeeDate
    //     ? values.inzFeeDate
    //     : this.props.clientProfileData.inzFeeDate,
    //   memberType: this.props.clientProfileData.memberType,
    //   clientId: this.props.clientProfileData.clientId,
    //   nzqaOnlineSubDate: values.nzqaOnlineSubDate
    //     ? values.nzqaOnlineSubDate
    //     : this.props.clientProfileData.nzqaOnlineSubDate,
    //   nzqaDocumentSubDate: values.nzqaDocumentSubDate
    //     ? values.nzqaDocumentSubDate
    //     : this.props.clientProfileData.nzqaDocumentSubDate,
    //   nzqaDocumentRetDate: values.nzqaDocumentRetDate
    //     ? values.nzqaDocumentRetDate
    //     : this.props.clientProfileData.nzqaDocumentRetDate,
    //   visaCountryId: parseInt(
    //     values.visaCountryId
    //       ? values.visaCountryId
    //       : this.props.clientProfileData.visaCountryId
    //   ),
    //   visaCountyType: parseInt(
    //     values.visaCountryType
    //       ? values.visaCountryType
    //       : this.props.clientProfileData.visaCountyType
    //   ),
    //   age: values.age ? values.age : this.props.clientProfileData.age,
    //   jobSectorId: parseInt(
    //     values.jobSectorId
    //       ? values.jobSectorId
    //       : this.props.clientProfileData.jobSectorId
    //   ),
    //   sourceId: parseInt(
    //     values.sourceId
    //       ? values.sourceId
    //       : this.props.clientProfileData.sourceId
    //   ),
    //   sourceDescription: values.sourceDescription
    //     ? values.sourceDescription
    //     : this.props.clientProfileData.sourceDescription,
    //   clientSerial: values.clientSerial
    //     ? values.clientSerial
    //     : this.props.clientProfileData.clientSerial,
    //   companyOptional: values.companyOptional
    //     ? values.companyOptional
    //     : this.props.clientProfileData.companyOptional,
    //   dealWorth: values.dealWorth
    //     ? values.dealWorth
    //     : this.props.clientProfileData.dealWorth,
    //   saleDate: values.saleDate
    //     ? values.saleDate
    //     : this.props.clientProfileData.saleDate,
    //   clientPermission: {
    //     signed: this.state.signed,
    //     onshore: this.state.onshore,
    //     active: this.state.active,
    //     allowUpdate: this.state.allowUpdate,
    //     areaAccess: this.state.areaAccess
    //   }
    // };
    let data = null;
    if (
      this.props.clientProfileData &&
      this.props.clientProfileData.medicals.length > 0
    ) {
      data = [
        {
          id: this.props.clientProfileData.medicals[0].id,
          clientId: clientprofileid,
          er: values.er || "",
          medicalIssueDate: values.medicalIssueDate
            ? moment(values.medicalIssueDate).format(
                "YYYY-MM-DDT00:00:00+00:00"
              )
            : "1900-01-01T00:00:00+00:00",
          medicalExpiryDate: values.medicalExpiryDate
            ? moment(values.medicalExpiryDate).format(
                "YYYY-MM-DDT00:00:00+00:00"
              )
            : "1900-01-01T00:00:00+00:00",
          xrayIssueDate: values.xrayIssueDate
            ? moment(values.xrayIssueDate).format("YYYY-MM-DDT00:00:00+00:00")
            : "1900-01-01T00:00:00+00:00",
          xrayExpiryDate: values.xrayExpiryDate
            ? moment(values.xrayExpiryDate).format("YYYY-MM-DDT00:00:00+00:00")
            : "1900-01-01T00:00:00+00:00",
          medicalGrading: values.medicalGrading.toString() || "",
          xrayGrading: values.xrayGrading.toString() || "",
          medicalNotes: values.medicalNotes || "",
          medicalNotesDetail: content,
          modifiedBy: userId,
        },
      ];
    } else {
      data = [
        {
          clientId: clientprofileid,
          er: values.er || "",
          medicalIssueDate: values.medicalIssueDate
            ? moment(values.medicalIssueDate).format(
                "YYYY-MM-DDT00:00:00+00:00"
              )
            : "1900-01-01T00:00:00+00:00",
          medicalExpiryDate: values.medicalExpiryDate
            ? moment(values.medicalExpiryDate).format(
                "YYYY-MM-DDT00:00:00+00:00"
              )
            : "1900-01-01T00:00:00+00:00",
          xrayIssueDate: values.xrayIssueDate
            ? moment(values.xrayIssueDate).format("YYYY-MM-DDT00:00:00+00:00")
            : "1900-01-01T00:00:00+00:00",
          xrayExpiryDate: values.xrayExpiryDate
            ? moment(values.xrayExpiryDate).format("YYYY-MM-DDT00:00:00+00:00")
            : "1900-01-01T00:00:00+00:00",
          medicalGrading: values.medicalGrading.toString() || "",
          xrayGrading: values.xrayGrading.toString() || "",
          medicalNotes: values.medicalNotes || "",
          medicalNotesDetail: content,
          modifiedBy: userId,
        },
      ];
    }

    this.props
      .onUpdClientMedical(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props
          .onGetClientProfile(clientprofileid)
          .then(() => {
            this.setState({ loadClient: false });
          })
          .catch(() => {
            this.setState({ loadClient: false });
          });
        if (
          moment(data[0].medicalExpiryDate).format("DD/MM/YYYY") !=
            moment(
              this.props.clientProfileData.medicals[0].medicalExpiryDate
            ).format("DD/MM/YYYY") &&
          moment(data[0].medicalExpiryDate).format("DD/MM/YYYY") != "01/01/1900"
        ) {
          let myData = {
            title: "Medical Expiry",
            reminderDate: values.medicalExpiryDate
              ? moment(values.medicalExpiryDate).format(
                  "YYYY-MM-DDT00:00:00+00:00"
                )
              : "1900-01-01T00:00:00+00:00",
          };
          AddAutoReminder(myData);
        }
        if (
          moment(data[0].xrayExpiryDate).format("DD/MM/YYYY") !=
            moment(
              this.props.clientProfileData.medicals[0].xrayExpiryDate
            ).format("DD/MM/YYYY") &&
          moment(data[0].xrayExpiryDate).format("DD/MM/YYYY") != "01/01/1900"
        ) {
          let xrayData = {
            title: "Xray Expiry",
            reminderDate: values.xrayExpiryDate
              ? moment(values.xrayExpiryDate).format(
                  "YYYY-MM-DDT00:00:00+00:00"
                )
              : "1900-01-01T00:00:00+00:00",
          };
          AddAutoReminder(xrayData);
        }
      })
      .catch(() => {
        this.setState({ updLoading: false, loadClient: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile(clientprofileid);
      });
  };

  onUpdateInzUserDetail = (values) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    const userId = localStorage.getItem("userId");
    this.setState({ loadClient: true });
    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: values.clientNumber || "",
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: values.firstName
        ? values.firstName
        : this.props.clientProfileData.firstName,
      lastName: values.lastName
        ? values.lastName
        : this.props.clientProfileData.lastName,
      middleName: values.middleName
        ? values.middleName
        : this.props.clientProfileData.middleName,
      title: values.title ? values.title : this.props.clientProfileData.title,
      gender: values.gender
        ? values.gender
        : this.props.clientProfileData.gender,
      dateOfBirth: this.props.clientProfileData.dateOfBirth,
      maritalStatus: values.maritalStatus
        ? values.maritalStatus
        : this.props.clientProfileData.maritalStatus,
      dependentChildren: values.dependentChildren
        ? values.dependentChildren
        : this.props.clientProfileData.dependentChildren,
      notes: this.props.clientProfileData.notes,
      occupation: values.occupation
        ? values.occupation
        : this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: values.inzUserName ? values.inzUserName : "",
      inzPassword: values.inzPassword ? values.inzPassword : "",
      imageBlobUrl: this.props.clientProfileData.imageBlobUrl,
      nationalityId: values.nationalityId
        ? values.nationalityId
        : this.props.clientProfileData.nationalityId,
      nationalityCountry: values.nationalityCountry
        ? values.nationalityCountry.toString()
        : this.props.clientProfileData.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        values.currentVisaTypeId
          ? values.currentVisaTypeId
          : this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: values.currentNewZealandVisaExpiry
        ? values.currentNewZealandVisaExpiry
        : this.props.clientProfileData.currentNewZealandVisaExpiry,
      travelConditionsValidTo: values.travelConditionsValidTo
        ? values.travelConditionsValidTo
        : this.props.clientProfileData.travelConditionsValidTo,
      visaText: values.visaText
        ? values.visaText
        : this.props.clientProfileData.visaText,
      visaDenied: this.props.clientProfileData.visaDenied,
      deniedText: values.deniedText
        ? values.deniedText
        : this.props.clientProfileData.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: values.inzFeeDate
        ? moment(values.inzFeeDate).format("YYYY-MM-DDT00:00:00+00:00")
        : "1900-01-01T00:00:00+00:00",
      interestedVisa: this.props.clientProfileData.interestedVisa,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: values.nzqaOnlineSubDate
        ? values.nzqaOnlineSubDate
        : this.props.clientProfileData.nzqaOnlineSubDate,
      nzqaDocumentSubDate: values.nzqaDocumentSubDate
        ? values.nzqaDocumentSubDate
        : this.props.clientProfileData.nzqaDocumentSubDate,
      nzqaDocumentRetDate: values.nzqaDocumentRetDate
        ? values.nzqaDocumentRetDate
        : this.props.clientProfileData.nzqaDocumentRetDate,
      visaCountryId: parseInt(
        values.visaCountryId
          ? values.visaCountryId
          : this.props.clientProfileData.visaCountryId
      ),
      visaCountyType: parseInt(
        values.visaCountryType
          ? values.visaCountryType
          : this.props.clientProfileData.visaCountyType
      ),
      age: values.age ? values.age : this.props.clientProfileData.age,
      jobSectorId: parseInt(
        values.jobSectorId
          ? values.jobSectorId
          : this.props.clientProfileData.jobSectorId
      ),
      sourceId: parseInt(
        values.sourceId
          ? values.sourceId
          : this.props.clientProfileData.sourceId
      ),
      sourceDescription: values.sourceDescription
        ? values.sourceDescription
        : this.props.clientProfileData.sourceDescription,
      clientSerial: values.clientSerial
        ? values.clientSerial
        : this.props.clientProfileData.clientSerial,
      companyOptional: values.companyOptional
        ? values.companyOptional
        : this.props.clientProfileData.companyOptional,
      dealWorth: values.dealWorth
        ? values.dealWorth
        : this.props.clientProfileData.dealWorth,
      saleDate: values.saleDate
        ? values.saleDate
        : this.props.clientProfileData.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };

    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props
          .onGetClientProfile(clientprofileid)
          .then(() => {
            this.setState({ loadClient: false });
          })
          .catch(() => {
            this.setState({ loadClient: false });
          });
      })
      .catch(() => {
        this.setState({ updLoading: false, loadClient: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile(clientprofileid);
      });
  };

  onUpdatePassport = (values) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    const userId = localStorage.getItem("userId");
    this.setState({ loadClient: true });
    var setReminder = false;
    var setSecondReminder = false;

    let passportData = [];
    if (this.props.clientProfileData.passports.length > 0) {
      for (var i = 0; i < this.props.clientProfileData.passports.length; i++) {
        let passportValues = {
          id: this.props.clientProfileData.passports[i].id,
          clientId: this.props.clientProfileData.passports[i].clientId,
          passportNo:
            i === 0 ? values.passportNo || "" : values.secondPassportNo || "",
          passportCountry:
            i === 0
              ? parseInt(values.passportCountry) || 0
              : parseInt(values.secondPassportCountry) || 0,
          passportIssueDate:
            i === 0
              ? values.passportIssueDate
                ? moment(values.passportIssueDate).format(
                    "YYYY-MM-DDT00:00:00+00:00"
                  )
                : "1900-01-01T00:00:00+00:00"
              : values.secondPassportIssueDate
              ? moment(values.secondPassportIssueDate).format(
                  "YYYY-MM-DDT00:00:00+00:00"
                )
              : "1900-01-01T00:00:00+00:00",
          passportExpiryDate:
            i === 0
              ? values.passportExpiryDate
                ? moment(values.passportExpiryDate).format(
                    "YYYY-MM-DDT00:00:00+00:00"
                  )
                : "1900-01-01T00:00:00+00:00"
              : values.secondPassportExpiryDate
              ? moment(values.secondPassportExpiryDate).format(
                  "YYYY-MM-DDT00:00:00+00:00"
                )
              : "1900-01-01T00:00:00+00:00",
          passportType: this.props.clientProfileData.passports[i].passportType,
          modifiedBy: userId,
        };
        var expDate = moment(passportValues.passportExpiryDate).format(
          "DD/MM/YYYY"
        );
        var oldDate = moment(
          this.props.clientProfileData.passports[i].passportExpiryDate
        ).format("DD/MM/YYYY");
        if (i == 0 && expDate != oldDate && expDate != "01/01/1900") {
          setReminder = true;
        }
        if (i == 1 && expDate != oldDate && expDate != "01/01/1900") {
          setSecondReminder = true;
        }
        passportData.push(passportValues);
      }
    } else {
      passportData = [
        {
          passportNo: values.passportNo || "",
          passportCountry: values.passportCountry || 0,
          passportType: 1,
          passportIssueDate: values.passportIssueDate
            ? moment(values.passportIssueDate).format(
                "YYYY-MM-DDT00:00:00+00:00"
              )
            : "1900-01-01T00:00:00+00:00",
          passportExpiryDate: values.passportExpiryDate
            ? moment(values.passportExpiryDate).format(
                "YYYY-MM-DDT00:00:00+00:00"
              )
            : "1900-01-01T00:00:00+00:00",
          clientId: clientprofileid,
        },
        {
          passportNo: values.secondPassportNo || "",
          passportCountry: values.secondPassportCountry || 0,
          passportType: 2,
          passportIssueDate: values.secondPassportIssueDate
            ? moment(values.secondPassportIssueDate).format(
                "YYYY-MM-DDT00:00:00+00:00"
              )
            : "1900-01-01T00:00:00+00:00",
          passportExpiryDate: values.secondPassportExpiryDate
            ? moment(values.secondPassportExpiryDate).format(
                "YYYY-MM-DDT00:00:00+00:00"
              )
            : "1900-01-01T00:00:00+00:00",
          clientId: clientprofileid,
        },
      ];
    }

    this.props
      .onUpdClientPassport(passportData)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Client passport updated successfully");
        var profileData = JSON.parse(localStorage.getItem("profileData"));
        var clientprofileid = JSON.parse(
          localStorage.getItem("clientprofileid")
        );
        if (setReminder) {
          let myData = {
            title: "Passport Expiry",
            reminderDate: values.passportExpiryDate
              ? moment(values.passportExpiryDate).format(
                  "YYYY-MM-DDT00:00:00+00:00"
                )
              : "1900-01-01T00:00:00+00:00",
          };
          AddAutoReminder(myData);
        }
        if (setSecondReminder) {
          let myDataSecond = {
            title: "Passport Expiry",
            reminderDate: values.secondPassportExpiryDate
              ? moment(values.secondPassportExpiryDate).format(
                  "YYYY-MM-DDT00:00:00+00:00"
                )
              : "1900-01-01T00:00:00+00:00",
          };
          AddAutoReminder(myDataSecond);
        }
        this.props
          .onGetClientProfile(clientprofileid)
          .then(() => {
            this.setState({ loadClient: false });
          })
          .catch(() => {
            this.setState({ loadClient: false });
          });
      })
      .catch(() => {
        this.setState({ updLoading: false, loadClient: false });
        message.error("Client Passport update failed");
        this.props.onGetClientProfile(clientprofileid);
      });
  };

  onUpdateBillingAddress = (values) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    const userId = localStorage.getItem("userId");
    this.setState({ loadClient: true });
    if (this.props.clientProfileData.addresses.length > 0) {
      let addressData = [];
      for (var i = 0; i < this.props.clientProfileData.addresses.length; i++) {
        let addressValues = {
          id: this.props.clientProfileData.addresses[i].id,
          clientId: this.props.clientProfileData.addresses[i].clientId,
          contactPerson:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].contactPerson
              : values.contactPerson || "",
          flat:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].flat
              : values.flat || "",
          building: this.props.clientProfileData.addresses[i].building,
          streetName: this.props.clientProfileData.addresses[i].streetName,
          suburb:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].suburb
              : values.suburb || "",
          streetNumber:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].streetNumber
              : values.streetNumber || "",
          city:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].city
              : values.city || "",
          state: this.props.clientProfileData.addresses[i].state,
          zip:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].zip
              : values.zipcode || "",
          country:
            this.props.clientProfileData.addresses[i].addressTypeId === 1
              ? this.props.clientProfileData.addresses[i].country
              : parseInt(values.billCountry) || 0,
          addressTypeId: this.props.clientProfileData.addresses[i]
            .addressTypeId,
          modifiedBy: userId,
        };
        addressData.push(addressValues);
      }
      this.props
        .onUpdClientAddress(addressData)
        .then(() => {
          this.setState({ updLoading: false });
          message.success("Client billing address updated successfully");
          this.props
            .onGetClientProfile(clientprofileid)
            .then(() => {
              this.setState({ loadClient: false });
            })
            .then(() => {
              this.setState({ loadClient: false });
            });
        })
        .catch(() => {
          this.setState({ updLoading: false, loadClient: false });
          message.error("Client billing address update failed");
          this.props.onGetClientProfile(clientprofileid);
        });
    }
  };

  onProfileTopUpdate = (toggleName) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.setState({ loadClient: true });
    const userId = localStorage.getItem("userId");
    let data = {
      id: this.props.clientProfileData.id,
      branchId: this.props.clientProfileData.branchId,
      clientNumber: this.props.clientProfileData.clientNumber,
      familyId: this.props.clientProfileData.familyId,
      processingGroupId: 0,
      agentId: this.props.clientProfileData.agentId,
      clientTag: this.props.clientProfileData.clientTag,
      firstName: this.props.clientProfileData.firstName,
      lastName: this.props.clientProfileData.lastName,
      middleName: this.props.clientProfileData.middleName,
      title: this.props.clientProfileData.title,
      gender: this.props.clientProfileData.gender,
      dateOfBirth: this.props.clientProfileData.dateOfBirth,
      maritalStatus: this.props.clientProfileData.maritalStatus,
      dependentChildren: this.props.clientProfileData.dependentChildren,
      notes: this.props.clientProfileData.notes,
      occupation: this.props.clientProfileData.occupation,
      occupationOrganization: this.props.clientProfileData
        .occupationOrganization,
      inzUserName: this.props.clientProfileData.inzUserName,
      inzPassword: this.props.clientProfileData.inzPassword,
      imageBlobUrl: this.state.addClientImageUrl
        ? this.state.addClientImageUrl
        : "",
      nationalityId: this.props.clientProfileData.nationalityId,
      nationalityCountry: this.props.clientProfileData.nationalityCountry,
      skypeID: this.props.clientProfileData.skypeID,
      preferredName: this.props.clientProfileData.preferredName,
      isSubscribed: this.props.clientProfileData.isSubscribed,
      arbitaryJson: this.props.clientProfileData.arbitaryJson,
      dependentClientIds: this.props.clientProfileData.dependentClientIds,
      modifiedBy: userId,
      currentVisaTypeId: parseInt(
        this.props.clientProfileData.currentVisaTypeId
      ),
      currentNewZealandVisaExpiry: this.props.clientProfileData
        .currentNewZealandVisaExpiry,
      travelConditionsValidTo: this.props.clientProfileData
        .travelConditionsValidTo,
      visaText: this.props.clientProfileData.visaText,
      visaDenied: this.props.clientProfileData.visaDenied,
      deniedText: this.props.clientProfileData.deniedText,
      clientNumberIZM: this.props.clientProfileData.clientNumberIZM,
      inzFeeDate: this.props.clientProfileData.inzFeeDate,
      interestedVisa: this.props.clientProfileData.interestedVisa,
      memberType: this.props.clientProfileData.memberType,
      clientId: this.props.clientProfileData.clientId,
      nzqaOnlineSubDate: this.props.clientProfileData.nzqaOnlineSubDate,
      nzqaDocumentSubDate: this.props.clientProfileData.nzqaDocumentSubDate,
      nzqaDocumentRetDate: this.props.clientProfileData.nzqaDocumentRetDate,
      visaCountryId: parseInt(this.props.clientProfileData.visaCountryId),
      visaCountyType: parseInt(this.props.clientProfileData.visaCountyType),
      age: this.props.clientProfileData.age,
      jobSectorId: parseInt(this.props.clientProfileData.jobSectorId),
      sourceId: parseInt(this.props.clientProfileData.sourceId),
      sourceDescription: this.props.clientProfileData.sourceDescription,
      clientSerial: this.props.clientProfileData.clientSerial,
      companyOptional: this.props.clientProfileData.companyOptional,
      dealWorth: this.props.clientProfileData.dealWorth,
      saleDate: this.props.clientProfileData.saleDate,
      clientPermission: {
        signed: this.state.signed,
        onshore: this.state.onshore,
        active: this.state.active,
        allowUpdate: this.state.allowUpdate,
        areaAccess: this.state.areaAccess,
      },
    };
    this.props
      .onUpdClientProfile(data)
      .then(() => {
        this.setState({ updLoading: false });
        message.success("Profile updated successfully");
        this.props
          .onGetClientProfile(clientprofileid)
          .then(() => {
            this.setState({ loadClient: false });
          })
          .catch(() => {
            this.setState({ loadClient: false });
          });
        if (toggleName === "onActive") {
          let userName = localStorage.getItem("userName");
          var profileData = JSON.parse(localStorage.getItem("profileData"));
          let active_inActive = this.state.active ? "Active" : "Inactive";
          let myData = {
            clientName: profileData.fullName,
            logMessage:
              "Client status updated to " + active_inActive + " by " + userName,
            date: moment(new Date()).format("DD/MM/YYYY"),
            logType: "Client Information",
            invoiceId: "0",
          };
          activityData(myData);
        }
        if (toggleName === "onSigned") {
          let userName = localStorage.getItem("userName");
          var profileData = JSON.parse(localStorage.getItem("profileData"));
          let signed_unSigned = this.state.signed ? "Signed" : "UnSigned";
          let myData = {
            clientName: profileData.fullName,
            logMessage:
              "Client contract SIGNED status updated as " +
              signed_unSigned +
              " by " +
              userName,
            date: moment(new Date()).format("DD/MM/YYYY"),
            logType: "Client Information",
            invoiceId: "0",
          };
          activityData(myData);
        }
        if (toggleName === "onShore") {
          let userName = localStorage.getItem("userName");
          var profileData = JSON.parse(localStorage.getItem("profileData"));
          let onshore_offShore = this.state.onshore ? "Onshore" : "Offshore";
          let myData = {
            clientName: profileData.fullName,
            logMessage:
              "Client status updated to " +
              onshore_offShore +
              " by " +
              userName,
            date: moment(new Date()).format("DD/MM/YYYY"),
            logType: "Client Information",
            invoiceId: "0",
          };
          activityData(myData);
        }
      })
      .catch(() => {
        this.setState({ updLoading: false, loadClient: false });
        message.error("Profile update failed");
        this.props.onGetClientProfile(clientprofileid);
      });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  uploadImage = (info, id) => {
    this.setState({ loadUploadImage: true });
    if (id) {
      this.setState({ imageUpdateId: id });
    }

    let formData = new FormData();
    formData.append("File", info);
    this.props.onUploadAvatar(formData).then(() => {
      if (this.props.imageUploadSuccess) {
        this.setState({
          addClientImageUrl: this.props.imageUploadSuccess,
          loadUploadImage: false,
        });
      }
      let userName = localStorage.getItem("userName");
      var profileData = JSON.parse(localStorage.getItem("profileData"));
      let myData = {
        clientName: profileData.fullName,
        logMessage: "Client profile picture uploaded by " + userName,
        date: moment(new Date()).format("DD/MM/YYYY"),
        logType: "Client Information",
        invoiceId: "0",
      };
      activityData(myData);
    });
  };

  handleChangeImage = (info) => {
    console.log("show file data ====== ", info);
    if (info.file.status === "uploading") {
      // this.setState({ loading: true });
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl) =>
        this.setState({
          imageUrl,
          loading: false,
        })
      );
    }
  };

  // setShowClient = () => {
  //   this.setState({ showClient: !this.state.showClient });
  // }
  onLogout = () => {
    this.props.onUserLogout().then(() => {
      localStorage.setItem("token", null);
      this.props.history.push("/");
    });
  };

  onFinishSigned = (values) => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.setState({ loadClient: true });
    let data = {
      id: clientprofileid,
      lastAgreementSigned: values.sigedAgreementDate,
    };

    this.props
      .onUpdateSignedDate(data)
      .then((res) => {
        this.setState({ signed: true, signedModalVisible: false });
        this.setState({ loadClient: false });
        this.onProfileTopUpdate();
        this.formRef.current.resetFields();
        let userName = localStorage.getItem("userName");
        var profileData = JSON.parse(localStorage.getItem("profileData"));
        let signed_unSigned = this.state.signed ? "enabled" : "disabled";
        let myData = {
          clientName: profileData.fullName,
          logMessage:
            "Client contract SIGNED status " +
            moment(values.sigedAgreementDate).format("DD/MM/YYYY") +
            " " +
            signed_unSigned +
            " by " +
            userName,
          date: moment(new Date()).format("DD/MM/YYYY"),
          logType: "Client Information",
          invoiceId: "0",
        };
        activityData(myData);
      })
      .catch((err) => {
        this.setState({ loadClient: false });
      });
  };

  downlaodImage = () => {
    if (this.state.addClientImageUrl) {
      saveAs(this.state.addClientImageUrl, "image.jpg"); // Put your image url here.
    } else {
      message.warning("Image not attached with this profile!");
    }
  };

  render() {
    const {
      selectedOption,
      signed,
      onshore,
      active,
      allowUpdate,
      areaAccess,
      updLoading,
      addClientImageUrl,
      showClient,
      visible,
      loadClient,
      signedModalVisible,
      headerOptions,
      showDetail,
      selectedQuestionniareId,
    } = this.state;

    const {
      clientProfileData,
      countriesData,
      groupsData,
      teamMembers,
      accessingAuthData,
      groupMembersData,
      visaTypeData,
      visaStatusData,
      onUpdateCaseStatus,
      onGetVisaApplication,
      visaAppData,
      onSendEmailLink,
      cmvLinkTemplateData,
      employerJobHistoryCurrentRes,
      onSetActiveKey,
      activeKey,
      clientSourceListing,
      onAddPoliceCertificate,
      onGetClientProfile,
      onAddAccAuthorities,
      onSearchClient,
      searchClientData,
      onUpdatePriority,
      visaPriorityRes,
      onSignedVisa,
      onPaidVisa,
      remindersRes,
      onUpdateReminder,
      onGetClientTask,
      onUpdateTask,
      onGetAllUsers,
      onAddTaskFollower,
      onRemoveTasks,
      onAddDailyTasks,
      onGetClientFamily,
      onUpdateCompletedTask,
      onAddTaskFileNote,
      onAddTaskComment,
      onGetTaskComments,
      onGetTaskFollowers,
      countryVisaTypeData,
      onRemoveFollower,
      onGetReminder,
      onGetVisaStatus,

      onGetAdmissionProgram,
      onGetAdmissionStatuses,

      onGetVisaType,
      onGetVisaTypeByCountry,
      admissionStatusData,
      admissionProgramData,
      onUpdAdmissionStatus,
      onStartNewApplication,
      onAddDraftClient,
      onAddReminderTask,
    } = this.props;

    // console.log("================", headOption(this.props.clientTab));

    return (
      <div>
        <Spin spinning={loadClient} size="large">
          <div>
            {/* <HeaderBar branchData={userBranchData} onLogout={this.onLogout} /> */}
          </div>
          <div style={{ display: "flex" }}>
            <div>{/* <Sidebar activeScreen="Dashboard (BI)" /> */}</div>
            <div className="page-container">
              {headerOptions.length > 0 && (
                <HeaderBarTabs
                  data={headerOptions}
                  activeTab="Client Profile"
                />
              )}
              <ProfileTopBarTabs data={topBar} activeTab="CLIENT INFORMATION" />

              <div className="profile-container" style={{ marginTop: 15 }}>
                <div
                  className={
                    activeKey
                      ? "content-width-open-sidebar"
                      : "content-width-close-sidebar"
                  }
                >
                  <Modal
                    className="reminder-model-main width-modal-outgoing-payments"
                    title="AGREEMENT SIGNED"
                    visible={signedModalVisible}
                    onCancel={() =>
                      this.setState({ signedModalVisible: false })
                    }
                    footer={null}
                    maskClosable={false}
                  >
                    <Form onFinish={this.onFinishSigned} ref={this.formRef}>
                      <div
                        className="form-container"
                        style={{
                          display: "flex",
                          marginTop: 10,
                          justifyContent: "space-between",
                          padding: 20,
                          alignItems: "center",
                        }}
                      >
                        <p className="medical-label">Date</p>
                        <div
                          style={{
                            display: "flex",
                            border: "none",
                            width: "48%",
                          }}
                        >
                          <Form.Item
                            name="sigedAgreementDate"
                            style={{ width: "100%" }}
                            rules={[
                              {
                                required: true,
                                message: "Required!",
                              },
                            ]}
                          >
                            <DatePicker format={dateFormat} />
                          </Form.Item>
                        </div>
                      </div>
                      <Row>
                        <Col xs={4} offset={18} style={{ display: "flex" }}>
                          <Form.Item>
                            <Button
                              type="primary"
                              htmlType="submit"
                              style={{ marginRight: "10px" }}
                            >
                              Save
                            </Button>
                          </Form.Item>
                          <Form.Item>
                            <Button
                              type="primary"
                              onClick={() =>
                                this.setState({ signedModalVisible: false })
                              }
                            >
                              Close
                            </Button>
                          </Form.Item>
                        </Col>
                      </Row>
                    </Form>
                  </Modal>
                  {clientProfileData && (
                    <div>
                      <div className="profile-first-box">
                        <div>
                          <div
                            style={{ flexDirection: "row", display: "flex" }}
                          >
                            <div className="profile-cont-left">
                              <div className="profile-img-cont ant-upload-profile">
                                <Upload
                                  name="avatar"
                                  listType="picture-card"
                                  className="avatar-uploader ant-upload-profile"
                                  showUploadList={false}
                                  action={this.uploadImage}
                                  onChange={this.handleChangeImage}
                                >
                                  <Spin spinning={this.state.loadUploadImage}>
                                    {addClientImageUrl ? (
                                      <img
                                        src={addClientImageUrl}
                                        alt="avatar"
                                        style={{ width: 105, height: 105 }}
                                      />
                                    ) : (
                                      <img
                                        src={Images.dummyUserImage}
                                        className="profile-img"
                                      />
                                    )}
                                  </Spin>
                                </Upload>
                              </div>
                              <h3
                                style={{
                                  textAlign: "center",
                                  paddingLeft: 5,
                                  paddingRight: 5,
                                }}
                              >
                                {clientProfileData
                                  ? clientProfileData.firstName +
                                    " " +
                                    clientProfileData.lastName
                                  : ""}
                              </h3>
                              <h5>
                                {!clientProfileData
                                  ? ""
                                  : clientProfileData.visaCountyType === 1
                                  ? "STUDENT"
                                  : clientProfileData.visaCountyType === 2
                                  ? "VISA"
                                  : clientProfileData.visaCountyType === 3 &&
                                    "UNSUCCESSFULL"}
                              </h5>
                              <div style={{ display: "flex", marginTop: 15 }}>
                                <div
                                  style={{ cursor: "pointer" }}
                                  onClick={() => {
                                    this.setState({ addClientImageUrl: "" });
                                    setTimeout(() => {
                                      this.onProfileTopUpdate();
                                    }, 500);
                                  }}
                                >
                                  <img
                                    src={Images.cross}
                                    style={{ width: 13, height: 13 }}
                                  />
                                </div>
                                <div
                                  style={{ marginLeft: 10, cursor: "pointer" }}
                                  onClick={this.downlaodImage}
                                >
                                  <img
                                    src={Images.download}
                                    className="svg-img"
                                  />
                                </div>
                                <div style={{ marginLeft: 10 }}>
                                  <img
                                    src={Images.multimediaOption}
                                    className="svg-img"
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="right-cont">
                              <div className="profile-puq-cont">
                                <div
                                  className="profile-print-box"
                                  style={{ width: 26, height: 26 }}
                                >
                                  <img
                                    src={Images.printWhite}
                                    className="profile-print-icon"
                                  />
                                </div>
                                <div style={{ display: "flex" }}>
                                  <div
                                    className="profile-updbtn-cont"
                                    style={{
                                      paddingLeft: 10,
                                      cursor: "pointer",
                                      height: 26,
                                    }}
                                    onClick={this.onProfileTopUpdate}
                                  >
                                    <span className="profile-updbtn-text">
                                      UPDATE
                                    </span>
                                    <img
                                      src={Images.updateWhite}
                                      style={{
                                        width: 10,
                                        height: 10,
                                        marginLeft: 8,
                                      }}
                                    />
                                  </div>
                                  <div
                                    className="profile-updbtn-cont"
                                    style={{
                                      marginLeft: 5,
                                      backgroundColor: "#0F7EB6",
                                      border: 1,
                                      borderStyle: "solid",
                                      borderColor: "#0F7EB6",
                                      height: 26,
                                    }}
                                  >
                                    <span className="profile-updbtn-text">
                                      QUESTIONNAIRE
                                    </span>
                                    <Dropdown
                                      placement="bottomRight"
                                      overlay={menu}
                                      trigger={["click"]}
                                    >
                                      <a
                                        className="ant-dropdown-link"
                                        style={{
                                          display: "flex",
                                          justifyContent: "center",
                                          alignItems: "center",
                                          width: 20,
                                          height: 20,
                                        }}
                                        onClick={(e) => e.preventDefault()}
                                      >
                                        <img
                                          src={Images.rightArrow}
                                          style={{
                                            width: 10,
                                            height: 10,
                                            transform: `rotate(90deg)`,
                                          }}
                                        />
                                      </a>
                                    </Dropdown>
                                    {/* <img
                                      src={Images.rightArrow}
                                      style={{
                                        transform: `rotate(90deg)`,
                                        width: 10,
                                        height: 10,
                                        marginLeft: 3,
                                      }}
                                    /> */}
                                  </div>
                                </div>
                              </div>
                              <div className="date-div">
                                <div>
                                  <span className="date-text">
                                    Created On:{" "}
                                    {clientProfileData
                                      ? moment(
                                          clientProfileData.createdDate
                                        ).format("DD/MM/YYYY")
                                      : ""}
                                  </span>
                                </div>
                                <div>
                                  <span className="date-text">
                                    Modified On:{" "}
                                    {clientProfileData &&
                                      moment(
                                        clientProfileData.modifiedDate
                                      ).format("DD/MM/YYYY")}
                                  </span>
                                </div>
                              </div>
                              <div className="lv-main-cont">
                                <div className="label-value-cont">
                                  <div className="label-cont">
                                    <span className="label-text">EZM ID</span>
                                  </div>
                                  <div className="value-cont">
                                    <span className="value-text">
                                      {clientProfileData
                                        ? clientProfileData.clientNumberIZM
                                        : ""}
                                    </span>
                                  </div>
                                </div>
                                <div className="label-value-cont">
                                  <div className="label-cont">
                                    <span className="label-text">INZ ID</span>
                                  </div>
                                  <div className="value-cont">
                                    <span className="value-text">
                                      {clientProfileData
                                        ? clientProfileData.clientNumber
                                        : ""}
                                    </span>
                                  </div>
                                </div>
                                <div className="label-value-cont">
                                  <div
                                    className="label-cont"
                                    style={{ marginLeft: 0.5 }}
                                  >
                                    <span className="label-text">DOB</span>
                                  </div>
                                  <div className="value-cont">
                                    <span className="value-text">
                                      {clientProfileData &&
                                      clientProfileData.dateOfBirth !==
                                        "1900-01-01T00:00:00+00:00"
                                        ? moment(
                                            clientProfileData.dateOfBirth
                                          ).format("DD/MM/YYYY")
                                        : ""}
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div className="buttons-row">
                                <div>
                                  <div className="black-button">
                                    <span className="black-button-text">
                                      Signed
                                    </span>
                                  </div>
                                  <div
                                    className={
                                      signed
                                        ? "right-green-btn-cont"
                                        : "cross-cont"
                                    }
                                    style={{ cursor: "pointer" }}
                                    onClick={() => {
                                      if (!signed) {
                                        this.setState({
                                          signedModalVisible: true,
                                        });
                                      } else {
                                        this.setState({ signed: !signed });
                                        setTimeout(() => {
                                          this.onProfileTopUpdate("onSigned");
                                        }, 500);
                                      }
                                    }}
                                  >
                                    {signed && (
                                      <img
                                        src={Images.tickWhite}
                                        className="svg-btn-img"
                                        style={{ marginRight: 10 }}
                                      />
                                    )}
                                    <img
                                      src={Images.btnImage}
                                      className="profile-btn-img"
                                    />
                                    {!signed && (
                                      <img
                                        src={Images.crossWhite}
                                        className="svg-btn-img"
                                        style={{ marginLeft: 10 }}
                                      />
                                    )}
                                  </div>
                                  <div></div>
                                </div>
                                <div style={{ marginLeft: 10 }}>
                                  <div className="black-button">
                                    <span className="black-button-text">
                                      Onshore
                                    </span>
                                  </div>
                                  <div
                                    className={
                                      onshore
                                        ? "right-green-btn-cont"
                                        : "cross-cont"
                                    }
                                    style={{ cursor: "pointer" }}
                                    onClick={() => {
                                      this.setState({ onshore: !onshore });
                                      setTimeout(() => {
                                        this.onProfileTopUpdate("onShore");
                                      }, 500);
                                    }}
                                  >
                                    {onshore && (
                                      <img
                                        src={Images.tickWhite}
                                        className="svg-btn-img"
                                        style={{ marginRight: 10 }}
                                      />
                                    )}
                                    <img
                                      src={Images.btnImage}
                                      className="profile-btn-img"
                                    />
                                    {!onshore && (
                                      <img
                                        src={Images.crossWhite}
                                        className="svg-btn-img"
                                        style={{ marginLeft: 10 }}
                                      />
                                    )}
                                  </div>
                                  <div></div>
                                </div>
                                <div style={{ marginLeft: 10 }}>
                                  <div className="black-button">
                                    <span className="black-button-text">
                                      Active
                                    </span>
                                  </div>
                                  <div
                                    className={
                                      active
                                        ? "right-green-btn-cont"
                                        : "cross-cont"
                                    }
                                    style={{ cursor: "pointer" }}
                                    onClick={() => {
                                      this.setState({ active: !active });
                                      setTimeout(() => {
                                        this.onProfileTopUpdate("onActive");
                                      }, 500);
                                    }}
                                  >
                                    {active && (
                                      <img
                                        src={Images.tickWhite}
                                        className="svg-btn-img"
                                        style={{ marginRight: 10 }}
                                      />
                                    )}
                                    <img
                                      src={Images.btnImage}
                                      className="profile-btn-img"
                                    />
                                    {!active && (
                                      <img
                                        src={Images.crossWhite}
                                        className="svg-btn-img"
                                        style={{ marginLeft: 10 }}
                                      />
                                    )}
                                  </div>
                                  <div></div>
                                </div>
                              </div>

                              {clientProfileData.agentName && (
                                <div>
                                  <div className="agent-tag-cont">
                                    <div className="agent-tag">
                                      <span className="tag-text">
                                        Agent Name:{" "}
                                        {clientProfileData.agentName}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                      <ProcessingPerson
                        clientInfo={clientProfileData}
                        groupsData={groupsData}
                        personsData={teamMembers}
                        onAddProcessingGroup={this.addProcessingGroup}
                        onRemoveProcessingGroup={this.removeProcessingGroup}
                        onAddProcessingPerson={this.addProcessingPerson}
                        onRemoveProcessingPerson={this.removeProcessingPerson}
                        groupMembers={groupMembersData}
                      />
                      <Connections
                        clientInfo={clientProfileData}
                        onSearchConnection={this.searchConnection}
                        searchedConnections={searchClientData}
                        onAddConnection={this.addConnection}
                        onRemoveConnection={this.removeConnection}
                        onSearchClient={onSearchClient}
                        history={this.props.history}
                        onGetClientProfile={onGetClientProfile}
                      />
                      <div
                        className="profile-additional-box"
                        style={{ paddingBottom: 50 }}
                      >
                        <PersonalInformation
                          clientInfo={clientProfileData}
                          countriesData={countriesData}
                          accessingAuthData={accessingAuthData}
                          visaTypeData={visaTypeData}
                          updatePersonalInfo={this.personalInfoUpdate}
                          clientSourceListing={clientSourceListing}
                          personsData={teamMembers}
                        />
                        <CurrentVisa
                          clientInfo={clientProfileData}
                          countriesData={countriesData}
                          visaTypeData={visaTypeData}
                          updateProfileCurrentVisa={this.onUpdateCurrentVisa}
                        />
                        <Medicals
                          clientInfo={clientProfileData}
                          countriesData={countriesData}
                          visaTypeData={visaTypeData}
                          updateProfileMedicals={this.onUpdateMedicals}
                        />
                        <Passport
                          clientInfo={clientProfileData}
                          countriesData={countriesData}
                          onAddCertificate={onAddPoliceCertificate}
                          onAddAuthority={onAddAccAuthorities}
                          onRemoveCertificate={this.removeCertificate}
                          onRemoveAuthority={this.removeAuthority}
                          accessingAuthData={accessingAuthData}
                          visaTypeData={visaTypeData}
                          updateProfilePassport={this.onUpdatePassport}
                          onGetClientProfile={onGetClientProfile}
                          onAddReminderTask={onAddReminderTask}
                        />
                        <InzLogin
                          clientInfo={clientProfileData}
                          countriesData={countriesData}
                          visaTypeData={visaTypeData}
                          updateProfileInzDetail={this.onUpdateInzUserDetail}
                          isClient={true}
                        />
                        <NZQADetail
                          clientInfo={clientProfileData}
                          countriesData={countriesData}
                          visaTypeData={visaTypeData}
                          updateProfileNZQA={this.onUpdateNZQA}
                        />
                        <BillingAddress
                          clientInfo={clientProfileData}
                          countriesData={countriesData}
                          visaTypeData={visaTypeData}
                          updateProfileBillingAddress={
                            this.onUpdateBillingAddress
                          }
                        />
                        <div
                          className="denied-cont"
                          style={{ justifyContent: "space-between" }}
                        >
                          <div className="denied-cont">
                            <div className="profile-down-arrow-cont">
                              <img
                                src={Images.whiteArrow}
                                className="profile-down-arrow-icon"
                              />
                            </div>
                            <span className="denied-text">
                              Client Area Access
                            </span>
                          </div>
                          {/* <Button
                          loading={updLoading}
                          className="button-blue"
                          onClick={this.onUpdateNZQA}
                        >
                          <span style={{ color: "#FFFFFF" }}>Update</span>
                        </Button> */}
                        </div>
                        <div className="form-container">
                          <div
                            className="buttons-row"
                            style={{ paddingTop: 20, marginBottom: 0 }}
                          >
                            {/*<div>
                            <div className="black-button" style={{ width: 95 }}>
                              <span
                                className="black-button-text"
                                style={{ fontSize: 8 }}
                              >
                                Allow Update Partner
                              </span>
                            </div>
                            <div className="cross-cont" style={{ width: 95 }}>
                              <img src={Images.btnImage} className="profile-btn-img" />
                              <img
                                src={Images.crossWhite}
                                className="svg-btn-img"
                                style={{ paddingLeft: 28, paddingRight: 28 }}
                              />
                            </div>
                            <div></div>
                          </div>*/}
                            <div style={{ marginLeft: 10 }}>
                              <div
                                className="black-button"
                                style={{ width: 95 }}
                              >
                                <span
                                  className="black-button-text"
                                  style={{ fontSize: 8 }}
                                >
                                  Allow Update
                                </span>
                              </div>
                              <div
                                className={
                                  allowUpdate
                                    ? "right-green-btn-cont"
                                    : "cross-cont"
                                }
                                style={{ width: 95, cursor: "pointer" }}
                                onClick={() => {
                                  this.setState({ allowUpdate: !allowUpdate });
                                  setTimeout(() => {
                                    this.onProfileTopUpdate();
                                  }, 500);
                                }}
                              >
                                {allowUpdate && (
                                  <img
                                    src={Images.tickWhite}
                                    className="svg-btn-img"
                                    style={{ marginRight: 20 }}
                                  />
                                )}
                                <img
                                  src={Images.btnImage}
                                  className="profile-btn-img"
                                />
                                {!allowUpdate && (
                                  <img
                                    src={Images.crossWhite}
                                    className="svg-btn-img"
                                    style={{ marginLeft: 20 }}
                                  />
                                )}
                              </div>
                              <div></div>
                            </div>
                            <div style={{ marginLeft: 10 }}>
                              <div
                                className="black-button"
                                style={{ width: 95 }}
                              >
                                <span
                                  className="black-button-text"
                                  style={{ fontSize: 8 }}
                                >
                                  Area Access
                                </span>
                              </div>
                              <div
                                className={
                                  areaAccess
                                    ? "right-green-btn-cont"
                                    : "cross-cont"
                                }
                                style={{ width: 95, cursor: "pointer" }}
                                onClick={() => {
                                  var isPrimaryEmail =
                                    clientProfileData &&
                                    clientProfileData.emails.find(
                                      (obj) =>
                                        obj.emailTypeId === 1 &&
                                        obj.address !== ""
                                    );
                                  if (isPrimaryEmail) {
                                    this.setState({ areaAccess: !areaAccess });
                                    if (!areaAccess) {
                                      this.setState({ visible: true });
                                    }
                                    setTimeout(() => {
                                      this.onProfileTopUpdate();
                                    }, 500);
                                  } else {
                                    this.setState({ areaAccess: false });
                                    message.error(
                                      "Please fill primary email field!"
                                    );
                                  }
                                }}
                              >
                                {areaAccess && (
                                  <img
                                    src={Images.tickWhite}
                                    className="svg-btn-img"
                                    style={{ marginRight: 20 }}
                                  />
                                )}
                                <img
                                  src={Images.btnImage}
                                  className="profile-btn-img"
                                />
                                {!areaAccess && (
                                  <img
                                    src={Images.crossWhite}
                                    className="svg-btn-img"
                                    style={{ marginLeft: 20 }}
                                  />
                                )}
                              </div>
                              <div></div>
                            </div>
                          </div>
                        </div>
                        <Modal
                          visible={showDetail}
                          onCancel={() => this.setState({ showDetail: false })}
                          footer={null}
                          width="80%"
                        >
                          <>
                            <LinkQuestionnaire
                              questionnaireId={selectedQuestionniareId}
                              // onGetDynamicLink={onGetDynamicLink}
                              // onGetCountries={getCountries}
                              // onSubmitQuestionnaire={onSubmitQuestionnaire}
                              // onGetQuestionnaire={onGetQuestionnaire}
                              // onGetFilledQuestionnaire={
                              //   onGetFilledQuestionnaire
                              // }
                              // dynamicLinkData={dynamicLinkData}
                              // countriesData={allCountriesData}
                              // questionnaireData={questionnaireData}
                              // filledQuestionnaireRes={filledQuestionnaireRes}
                              notLink={true}
                              clientprofileid={JSON.parse(
                                localStorage.getItem("clientprofileid")
                              )}
                              isPotential={true}
                            />
                          </>
                        </Modal>

                        {visible && (
                          <ClientAreaEmail
                            handleCancel={this.handleCancel}
                            profileData={clientProfileData}
                            visible={visible}
                            emailContent={
                              cmvLinkTemplateData &&
                              cmvLinkTemplateData.htmlTemplate
                            }
                            onGetEmailContent={
                              this.props && this.props.onGetEmailContent
                            }
                            onSendEmail={onSendEmailLink}
                            onAddDraftClient={onAddDraftClient}
                          />
                        )}
                        {/*<div
                        className="button-blue-cont"
                        style={{ marginTop: 23, marginLeft: 10 }}
                      >
                        <Form.Item>
                          <Button className="button-blue" htmlType="submit">
                            <span style={{ color: "#FFFFFF" }}>Update</span>
                          </Button>
                        </Form.Item>
                      </div>*/}
                      </div>
                    </div>
                  )}
                  {/*<div
                    style={{
                      float: "left",
                      position: "fixed",
                      left: 250,
                      bottom: 30
                    }}
                  >
                      <Button
                        loading={updLoading} 
                        className="button-blue"
                        htmlType="submit"
                      >
                        <span style={{ color: "#FFFFFF" }}>Update</span>
                      </Button>
                  </div>*/}
                </div>
                <div
                  className=""
                  style={{
                    width: activeKey ? "438px" : "115px",
                    marginLeft: 20,
                  }}
                >
                  <div style={{ display: "flex" }}>
                    <div style={{ width: "100%", marginTop: -30 }}>
                      <ProfileSideBar
                        onGetClientTag={this.props.onGetClientTag}
                        clientTagRes={this.props.clientTagRes}
                        getClientRes={this.props.getClientRes}
                        onGetProfileClientTag={this.props.onGetProfileClientTag}
                        onAddProfileClientTag={this.props.onAddProfileClientTag}
                        addProfielTagRes={this.props.addProfielTagRes}
                        onRemoveProfileClientTag={
                          this.props.onRemoveProfileClientTag
                        }
                        removeProfileTagRes={this.props.removeProfileTagRes}
                        visaStatusData={visaStatusData}
                        onUpdateCaseStatus={onUpdateCaseStatus}
                        visaAppData={visaAppData}
                        onGetVisaApplication={onGetVisaApplication}
                        employerJobHistoryCurrentRes={
                          employerJobHistoryCurrentRes
                        }
                        onSetActiveKey={onSetActiveKey}
                        activeKey={activeKey}
                        onUpdatePriority={onUpdatePriority}
                        visaPriorityRes={visaPriorityRes}
                        onSignedVisa={onSignedVisa}
                        onPaidVisa={onPaidVisa}
                        remindersRes={remindersRes && remindersRes.items}
                        onUpdateReminder={onUpdateReminder}
                        onGetClientTask={onGetClientTask}
                        onUpdateTask={onUpdateTask}
                        onGetAllUsers={onGetAllUsers}
                        onAddTaskFollower={onAddTaskFollower}
                        onRemoveTasks={onRemoveTasks}
                        onAddDailyTasks={onAddDailyTasks}
                        onGetClientFamily={onGetClientFamily}
                        onUpdateCompletedTask={onUpdateCompletedTask}
                        onAddTaskFileNote={onAddTaskFileNote}
                        onAddTaskComment={onAddTaskComment}
                        onGetTaskComments={onGetTaskComments}
                        onGetTaskFollowers={onGetTaskFollowers}
                        visaTypeData={countryVisaTypeData}
                        onRemoveFollower={onRemoveFollower}
                        onLoadClient={loadClient}
                        onGetReminder={onGetReminder}
                        onGetVisaStatus={onGetVisaStatus}
                        onGetAdmissionProgram={onGetAdmissionProgram}
                        onGetAdmissionStatuses={onGetAdmissionStatuses}
                        onGetVisaType={onGetVisaType}
                        onGetVisaTypeByCountry={onGetVisaTypeByCountry}
                        admissionStatusData={admissionStatusData}
                        admissionProgramData={admissionProgramData}
                        onUpdAdmissionStatus={onUpdAdmissionStatus}
                        onStartNewApplication={onStartNewApplication}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Spin>
      </div>
    );
  }
}

export default Profile;
