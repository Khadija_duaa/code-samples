import React from "react";
import Select from "react-select";
import "./AgentStyles.css";
import HeaderBar from "../Components/Header/HeaderBar";

import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import { Images } from "../Themes";
import PotentialHeaderTabs from "../Components/Header/PotentialHeaderTabs";
import Modal from "react-awesome-modal";

const headOption = [{ tabName: "AGENTS", linkName: "/agent" }];

class Agents extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      linkVisible: false
    };
  }

  openModal = () => {
    this.setState({
      visible: true
    });
  };

  closeModal = () => {
    this.setState({
      visible: false
    });
  };

  openLinkModal = () => {
    this.setState({
      linkVisible: true
    });
  };

  closeLinkModal = () => {
    this.setState({
      linkVisible: false
    });
  };

  render() {
    const { selectedOption } = this.state;
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            <PotentialHeaderTabs data={headOption} activeTab="AGENTS" />

            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: 20,
                marginRight: 20
              }}
            >
              <div className="pciq-top-div" style={{ marginTop: 0 }}>
                <span className="pc-top-div-text" style={{ color: "#0A3C5D" }}>
                  AGENTS
                </span>
              </div>
              <div onClick={this.openModal}>
                <img src={Images.plusIcon} style={{ width: 20, height: 20 }} />
              </div>
            </div>

            <div className="report-container">
              <div>
                <table
                  className="ca-invoice-table-cont"
                  style={{ borderSpacing: 0, marginTop: 20 }}
                >
                  <tbody>
                    <tr style={{ backgroundColor: "#F8F9FB" }}>
                      <th className="ca-table-heading">Name</th>
                      <th className="ca-table-heading">Country</th>
                      <th className="ca-table-heading">City</th>
                      <th className="ca-table-heading">Email</th>
                      <th className="ca-table-heading">
                        Commission Pain | Due
                      </th>
                      <th className="ca-table-heading">Login Link</th>
                      <th className="ca-table-heading"></th>
                    </tr>
                    <tr style={{ backgroundColor: "#FFFFFF" }}>
                      <td className="report-table-content-text">test agent</td>
                      <td className="report-table-content-text">india</td>
                      <td className="report-table-content-text">dehli</td>
                      <td className="report-table-content-text">
                        vshgh@gmail.com
                      </td>
                      <td className="report-table-content-text">
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between"
                          }}
                        >
                          <span>47392.00</span>
                          <span>1024700.00</span>
                        </div>
                      </td>
                      <td
                        className="report-table-content-text"
                        style={{ display: "flex", justifyContent: "center" }}
                      >
                        <div
                          className="ag-login-link-cont"
                          onClick={this.openLinkModal}
                        >
                          <span style={{ color: "#33A9D9" }}>LOGIN LINK</span>
                        </div>
                      </td>
                      <td className="report-table-content-text">
                        <div style={{ display: "flex" }}>
                          <div>
                            <img
                              src={Images.disable}
                              style={{ width: 15, height: 15 }}
                            />
                          </div>
                          <div style={{ marginLeft: 5 }}>
                            <img
                              src={Images.docType}
                              style={{ width: 15, height: 15 }}
                            />
                          </div>
                          <div style={{ marginLeft: 5 }}>
                            <img
                              src={Images.crossRed}
                              style={{ width: 15, height: 15 }}
                            />
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr style={{ backgroundColor: "#FFFFFF" }}>
                      <td className="report-table-content-text">test agent</td>
                      <td className="report-table-content-text">india</td>
                      <td className="report-table-content-text">dehli</td>
                      <td className="report-table-content-text">
                        vshgh@gmail.com
                      </td>
                      <td className="report-table-content-text">
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between"
                          }}
                        >
                          <span>47392.00</span>
                          <span>1024700.00</span>
                        </div>
                      </td>
                      <td
                        className="report-table-content-text"
                        style={{ display: "flex", justifyContent: "center" }}
                      >
                        <div className="ag-login-link-cont">
                          <span style={{ color: "#33A9D9" }}>LOGIN LINK</span>
                        </div>
                      </td>
                      <td className="report-table-content-text">
                        <div style={{ display: "flex" }}>
                          <div>
                            <img
                              src={Images.disable}
                              style={{ width: 15, height: 15 }}
                            />
                          </div>
                          <div style={{ marginLeft: 5 }}>
                            <img
                              src={Images.docType}
                              style={{ width: 15, height: 15 }}
                            />
                          </div>
                          <div style={{ marginLeft: 5 }}>
                            <img
                              src={Images.crossRed}
                              style={{ width: 15, height: 15 }}
                            />
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              {/* ======== Add agent desing ====== */}

              {/* <Modal
                visible={this.state.visible}
                width="500"
                height="550"
                effect="fadeInUp"
                onClickAway={() => this.closeModal()}
              >
                <div style={{ padding: 40 }}>
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <div className="sus-modal-label">
                      <span className="sus-modal-label-text">ADD AGENT</span>
                    </div>
                    <div onClick={this.closeModal}>
                      <img src={Images.crossRed} style={{ width: 20 }} />
                    </div>
                  </div>
                  <div style={{ display: "flex" }}>
                    <div
                      className="ca-gray-cont"
                      style={{
                        display: "flex",
                        width: "100%",
                        border: 0,
                        borderRadius: 5,
                        paddingRight: 20
                      }}
                    >
                      <div style={{ marginTop: -20 }}>
                        <div className="ag-user-icon-cont">
                          <img
                            src={Images.userCircle}
                            style={{ width: 25, height: 25 }}
                          />
                        </div>
                      </div>

                      <div
                        style={{ width: `calc(100% - 100px)`, marginLeft: 20 }}
                      >
                        <div class="profile-input-border">
                          <input
                            className="profile-input"
                            placeholder="Full Name"
                            type="text"
                            onChange={this.myChangeHandler}
                          />
                        </div>

                        <div
                          class="profile-input-border"
                          style={{ marginTop: 10 }}
                        >
                          <input
                            className="profile-input"
                            placeholder="Email"
                            type="text"
                            onChange={this.myChangeHandler}
                          />
                        </div>

                        <div
                          class="profile-input-border"
                          style={{ marginTop: 10 }}
                        >
                          <input
                            className="profile-input"
                            placeholder="Password"
                            type="text"
                            onChange={this.myChangeHandler}
                          />
                        </div>

                        <div
                          class="profile-input-border"
                          style={{ marginTop: 10 }}
                        >
                          <input
                            className="profile-input"
                            placeholder="Country"
                            type="text"
                            onChange={this.myChangeHandler}
                          />
                        </div>

                        <div
                          class="profile-input-border"
                          style={{ marginTop: 10 }}
                        >
                          <input
                            className="profile-input"
                            placeholder="City"
                            type="text"
                            onChange={this.myChangeHandler}
                          />
                        </div>

                        <div
                          class="profile-input-border"
                          style={{ marginTop: 10 }}
                        >
                          <input
                            className="profile-input"
                            placeholder="0"
                            type="text"
                            onChange={this.myChangeHandler}
                          />
                        </div>

                        <div
                          class="profile-input-border"
                          style={{ marginTop: 10 }}
                        >
                          <input
                            className="profile-input"
                            placeholder="Address"
                            type="text"
                            onChange={this.myChangeHandler}
                          />
                        </div>

                        <div
                          class="profile-input-border"
                          style={{ marginTop: 10 }}
                        >
                          <input
                            className="profile-input"
                            placeholder="Description"
                            type="text"
                            onChange={this.myChangeHandler}
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      marginTop: 30
                    }}
                  >
                    <div></div>
                    <div style={{ display: "flex" }}>
                      <div className="sus-modal-button">
                        <span className="sus-modal-button-text">ADD NEW</span>
                      </div>
                      <div
                        onClick={this.closeModal}
                        className="sus-modal-button"
                        style={{ marginLeft: 10 }}
                      >
                        <span className="sus-modal-button-text">CLOSE</span>
                      </div>
                    </div>
                  </div>
                </div>
              </Modal> */}

              {/* ======== Show link ======== */}

              {/* <Modal
                visible={this.state.linkVisible}
                width="500"
                height="200"
                effect="fadeInUp"
                onClickAway={() => this.closeLinkModal()}
              >
                <div style={{ padding: 20 }}>
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <div className="sus-modal-label">
                      <span className="sus-modal-label-text">LOGIN LINK</span>
                    </div>
                    <div onClick={this.closeModal}>
                      <img src={Images.crossRed} style={{ width: 20 }} />
                    </div>
                  </div>
                  <div
                    className="ca-gray-cont"
                    style={{ border: 0, padding: 10, margin: 0 }}
                  >
                    <p
                      class="medical-label"
                      style={{ fontSize: 11, marginLeft: 15 }}
                    >
                      http://agent.ezymigrate.co.nz/Home/Agent?u=ZXp5bWlncmFOZT-BAZ21haWwuY29t&p=VGVZzdEAOMTE=
                    </p>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      marginTop: 15
                    }}
                  >
                    <div></div>
                    <div style={{ display: "flex" }}>
                      <div
                        onClick={this.closeLinkModal}
                        className="sus-modal-button"
                        style={{ marginLeft: 10 }}
                      >
                        <span className="sus-modal-button-text">CLOSE</span>
                      </div>
                    </div>
                  </div>
                </div>
              </Modal>
             */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Agents;
