import React from "react";
import "../Clients/ClientsStyles.css";
import { saveAs } from "file-saver";
import {
  Col,
  Row,
  Select,
  DatePicker,
  Button,
  message,
  Form,
  Table,
  Modal,
  Spin,
  Input,
} from "antd";
import { bindActionCreators } from "redux";
import {
  sendDynamicLinkEmail,
  getPDF,
  getEmailContent,
  getPDFSchoolReceipt,
  getPDFSchoolInvoice,
  editInvoice,
  sendDynamicLinkEmailBlob,
} from "../store/Actions/index";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import FroalaEditor from "react-froala-wysiwyg";
import { apiRefresh } from "../services/api";
import FroalaEditorCom from "../Containers/FroalaEditorCom";
import { getFilterList } from "../Common/getEmailContentList";

const layoutSendingReceipt = {
  labelCol: { span: 8, offset: 0 },
  wrapperCol: { span: 20, offset: 2 },
};
const { Option } = Select;
class SendEmail extends React.Component {
  formRef = React.createRef();
  constructor(props) {
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    super(props);
    this.state = {
      sendingReceiptEmail: "",
      loading: false,
      selectedBranchId: selectedBranchId,
      resultList: []
    };
  }

  componentWillMount() {
    this.props && this.props.onGetEmailContent(this.state.selectedBranchId).then((res) => {
      let resultList = getFilterList(res);
      this.setState({ resultList: resultList})
    })
  }

  componentDidMount() {
    let _toEmail = this.props && this.props.selectedSchoolEmail;
    let setEmailData = this.props.setEmailData;

    this.setState({
      sendingReceiptEmail: setEmailData && setEmailData.htmlTemplate,
    });
    this.formRef.current.setFieldsValue({
      from: localStorage.getItem("selectedBranchName"),
      to: _toEmail,
      subject: `Receipt for Invoice # ${this.props.invoiceNo} from ${localStorage.getItem("selectedBranchName")}`,
    });
  }

  componentWillReceiveProps(nextProps, nextContext) {}

  onFinishSendingReceipt = (values) => {
    let _from = values.from === undefined ? "" : values.from;
    let _to = values.to === undefined ? "" : values.to;
    let _cc = values.cc === undefined ? "" : values.cc;
    let _bcc = values.bcc === undefined ? "" : values.bcc;
    let _subject = values.subject === undefined ? "" : values.subject;
    let selectedBranchId = localStorage.getItem("selectedBranchId");

    const formData = new FormData();
    formData.append("Message", this.state.sendingReceiptEmail);
    formData.append("Subject", _subject);
    formData.append("From", _from);
    formData.append("Priority", "10");
    formData.append("FrequencyCode", "client");
    formData.append("ModuleId", "10");
    formData.append("BranchId", selectedBranchId);
    formData.append("ClientId", this.props.clientId);
    formData.append(`Recipients[0].name`, _to);
    formData.append(`Recipients[0].type`, "To");
    if(_cc !== "") {
      formData.append(`Recipients[1].name`, _cc);
      formData.append(`Recipients[1].type`, "CC");
    }
    if(_bcc !== "") {
      formData.append(`Recipients[2].name`, _bcc);
      formData.append(`Recipients[2].type`, "Bcc");
    }
    formData.append(
      `Attachments[0].type`,
      this.props && this.props.attDataArray && this.props.attDataArray[0].type
    );
    formData.append(
      `Attachments[0].blobURL`,
      this.props && this.props.attDataArray && this.props.attDataArray[0].url
    );
    formData.append(
      `Attachments[0].name`,
      this.props && this.props.attDataArray && this.props.attDataArray[0].name
    );
    this.setState({ loading: true });
    let userId = localStorage.getItem("userId");
    let clientprofileid = JSON.parse(localStorage.getItem("clientprofileid"));

    this.props
      .sendDynamicLinkEmail(formData)
      .then(async () => {
        message.success("Email sent successfully!");

        let _statusId;
        if (
          this.props.state.dueAmount !== 0 &&
          this.props.state.dueAmount > 0
        ) {
          if (
            this.props.state.invoiceStatuses &&
            this.props.state.invoiceStatuses.length > 0
          ) {
            _statusId = this.props.state.invoiceStatuses[1].id;
          }
        } else {
          _statusId = this.props.state.invoiceStatuses[5].id;
        }

        await this.props.getSentStatus(_statusId);

        this.props
          .editInvoice(this.props.state)
          .then(() => {
            message.success("Invoice is updated successfully!");
            this.props.handleCancelSendingReceiptModal();
            this.setState({ loading: false });
          })
          .catch(() => {
            this.setState({ loading: false });
          });
      })
      .catch(() => {
        this.setState({ loading: false });
      });
  };


  onClickPDFFile = () => {
    this.setState({ loading: true });
    let _data = {
      id: this.props && this.props.invoiceId,
      paymentId: this.props && this.props.paymentId,
    };
    if (this.props && this.props.isPDFRelatedToInvoice === true) {
      this.props
        .getPDF(_data)
        .then((response) => {
          let blob = new Blob([response.payload], {
            type: "application/octet-stream",
          });
          this.setState({ loading: false });
          saveAs(blob, `Invoice-${this.props.invoiceNo}.pdf`);
        })
        .catch((e) => {
          this.setState({ loading: false });
          message.error("Unable to download, please try again!");
        });
    } else {
      this.props
        .getPDF(_data)
        .then((response) => {
          let blob = new Blob([response.payload], {
            type: "application/octet-stream",
          });
          this.setState({ loading: false });
          saveAs(blob, `Invoice-${this.props.invoiceNo}.pdf`);
        })
        .catch((e) => {
          this.setState({ loading: false });
          message.error("Unable to download, please try again!");
        });
    }
  };

  getSendingReceiptForm = (props) => {
    return (
      <div className="border-box-modal-sending-receipt add-employer-para">
        <Row>
          <Col xs={3} offset={21}>
            <div
              className="pdf-file-btn"
              style={{ backgroundColor: "#f0ad4e" }}
            >
              <a
                className="pdf-file-text"
                onClick={this.onClickPDFFile}
              >{`RECEIPT-${this.props.invoiceNo}-.PDF`}</a>
            </div>
          </Col>
        </Row>
        <Form.Item
          colon={false}
          labelAlign="left"
          label="From"
          name="from"
          rules={[
            { required: true, message: "Required!" },
            // { type: "email", message: "Invalid!" },
          ]}
        >
          <Input disabled />
        </Form.Item>
        <Form.Item
          colon={false}
          labelAlign="left"
          label="To"
          name="to"
          rules={[
            { required: true, message: "Required!" },
            { type: "email", message: "Invalid!" },
          ]}
        >
          <Input placeholder="Enter Email" />
        </Form.Item>
        <Form.Item
          colon={false}
          labelAlign="left"
          label="Ccc"
          name="cc"
          rules={[{ type: "email", message: "Invalid!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          colon={false}
          labelAlign="left"
          label="Bcc"
          name="bcc"
          rules={[{ type: "email", message: "Invalid!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          colon={false}
          labelAlign="left"
          label="Subject"
          name="subject"
          rules={[{ required: true, message: "Required!" }]}
        >
          <Input />
        </Form.Item>
        <Row>
          <Col xs={8} style={{marginTop: "3vh", marginBottom: "4vh"}}>
            {/* eslint-disable-next-line react/jsx-no-undef */}
            <Select placeholder={"Select content type"} virtual={false} onChange={(value) => { this.setState({ sendingReceiptEmail: value})}}>

              <Option value={""}>{"Select"}</Option>
              {this.state &&
              this.state.resultList.map((item, index) => (
                <Option value={item.emailContent}>{item.emailType}</Option>
              ))}
            </Select>
          </Col>
        </Row>
        <Row>
          <Col xs={24}>
            {/* eslint-disable-next-line react/jsx-no-undef */}
            <FroalaEditorCom
              model={this.state.sendingReceiptEmail}
              onModelChange={this.getEditorValue}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={2} offset={20} style={{ display: "flex" }}>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                style={{ marginRight: "10px" }}
                className="button-blue"
              >
                Send
              </Button>
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                onClick={props.handleCancelSendingReceiptModal}
                className="button-blue"
              >
                Close
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </div>
    );
  };

  getEditorValue = (value) => {
    this.setState({ sendingReceiptEmail: value });
  };

  render() {
    let a = this.props;

    return (
      <div className="reminder-model">
        <Modal
          className="reminder-model-main width-modal-sending-receipt"
          title="SEND EMAIL NOTIFICATION"
          visible={this.props.visibleSendingReceiptModal}
          onCancel={this.props.handleCancelSendingReceiptModal}
          footer={null}
          maskClosable={false}
        >
          <Spin size="large" spinning={this.state.loading}>
            <Form
              {...layoutSendingReceipt}
              onFinish={this.onFinishSendingReceipt}
              ref={this.formRef}
            >
              {this.getSendingReceiptForm(this.props)}
            </Form>
          </Spin>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => ({
  sendDynamicLinkEmail: bindActionCreators(sendDynamicLinkEmailBlob, dispatch),
  getPDFSchoolReceipt: bindActionCreators(getPDFSchoolReceipt, dispatch),
  getPDF: bindActionCreators(getPDFSchoolInvoice, dispatch),
  editInvoice: bindActionCreators(editInvoice, dispatch),
  onGetEmailContent: bindActionCreators(getEmailContent, dispatch),
});
SendEmail = connect(mapStateToProps, mapDispatchToProps)(SendEmail);

export default withRouter(SendEmail);
