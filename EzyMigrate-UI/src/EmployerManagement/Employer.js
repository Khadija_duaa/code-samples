import React from "react";
import Select from "react-select";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import "./EmployerManagementStyles.css";
import HeaderBar from "../Components/Header/HeaderBar";

import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import { Images } from "../Themes";
import PotentialHeaderTabs from "../Components/Header/PotentialHeaderTabs";
import DatePicker from "react-date-picker";

import EmployerActions from "../Redux/EmployerManagementRedux/EmployerRedux";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" }
];

const headOption = [
  { tabName: "EMPLOYER MANAGEMENT", linkName: "/employer-management" },
  { tabName: "ACCOUNTS", linkName: "/" }
];

class EmployerManagement extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
    fetching: PropTypes.bool,
    attemptToAddEmployer: PropTypes.func,
    attemptToGetEmployer: PropTypes.func,
    attemptToGetAllEmployer: PropTypes.func,
    attemptToUpdateEmployer: PropTypes.func,
    attemptToDeleteEmployer: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "employers",
      extend: false,
      activeECPTab: "profile",
      activeDocTab: "docDocument",
      selectedFile: null,

      employerName: "",
      companyName: "",
      employerEmail: "",
      contactNo: "",
      employerAddress: ""
    };

    // this.props.attemptToGetAllEmployer('3c6c0edc-c03e-4e4d-b878-ab29f863e381')
  }

  myChangeHandler = text => {
    this.setState({ username: text });
  };

  onChange = value => {
    console.log(`selected ${value}`);
  };

  onBlur = () => {
    console.log("blur");
  };

  onFocus = () => {
    console.log("focus");
  };

  onSearch = val => {
    console.log("search:", val);
  };

  onChangeMainTab = value => {
    this.setState({ activeMainTab: value });
  };

  onChangeTab = value => {
    this.setState({ activeTab: value });
  };

  onChangeECPTab = value => {
    this.setState({ activeECPTab: value });
  };

  onChangeDocTab = value => {
    this.setState({ activeDocTab: value });
  };

  onFileChange = event => {
    console.log("show file", event.target.files[0]);
    // Update the state
    this.setState({ selectedFile: event.target.files[0].name });
  };

  handleChange = selectedOption => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };

  handleCheck = () => {
    this.setState({ checked: !this.state.checked });
  };

  onSubmit = () => {
    var form = new FormData();
    form.append("companyId", "3fa85f64-5717-4562-b3fc-2c963f66afa6");
    form.append("name", "Test Name");
    form.append("email", "test@email.com");
    form.append("contactNo", "012345678");
    form.append("business", "Test business name");
    form.append("address", "test address");
    form.append("agreementUrl", null);
    form.append("xeroID", "");

    let token = "Bearer " + this.props.authToken;

    this.props.attemptToAddEmployer(form, token);
  };

  render() {
    const {
      selectedOption,
      employerName,
      employerEmail,
      companyName,
      contactNo,
      employerAddress
    } = this.state;
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            <PotentialHeaderTabs
              data={headOption}
              activeTab="EMPLOYER MANAGEMENT"
            />

            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: 20,
                marginRight: 20
              }}
            >
              <div></div>
              <div onClick={this.openModal}>
                <img src={Images.plusIcon} style={{ width: 20, height: 20 }} />
              </div>
            </div>

            <div
              className="sus-tab-container"
              style={{ marginLeft: 20, marginTop: 5 }}
            >
              <div
                onClick={() => this.onChangeTab("employers")}
                className={
                  this.state.activeTab == "employers"
                    ? "sus-active-tab"
                    : "sus-inactive-tab"
                }
              >
                <span
                  className={
                    this.state.activeTab == "employers"
                      ? "sus-active-tab-text"
                      : "sus-inactive-tab-text"
                  }
                >
                  EMPLOYERS
                </span>
              </div>
              <div
                onClick={() => this.onChangeTab("suppliers")}
                className={
                  this.state.activeTab == "suppliers"
                    ? "sus-active-tab"
                    : "sus-inactive-tab"
                }
              >
                <span
                  className={
                    this.state.activeTab == "suppliers"
                      ? "sus-active-tab-text"
                      : "sus-inactive-tab-text"
                  }
                >
                  SUPPLIERS
                </span>
              </div>
              <div
                onClick={() => this.onChangeTab("recruiters")}
                className={
                  this.state.activeTab == "recruiters"
                    ? "sus-active-tab"
                    : "sus-inactive-tab"
                }
              >
                <span
                  className={
                    this.state.activeTab == "recruiters"
                      ? "sus-active-tab-text"
                      : "sus-inactive-tab-text"
                  }
                >
                  RECRUITERS
                </span>
              </div>
            </div>
            <div className="emp-management-container">
              {/* ========== Employer management Employer Desing ========= */}
              {false && (
                <div>
                  <div
                    style={{ display: "flex", marginLeft: 20, marginTop: 10 }}
                  >
                    <div style={{ width: "25%" }}>
                      <p>Date To</p>
                      <div
                        className="profile-input-border"
                        style={{
                          width: `calc(100% - 7px)`,
                          overflow: "inherit",
                          paddingTop: 6,
                          paddingBottom: 6,
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "space-between",
                          paddingRight: 5
                        }}
                      >
                        <DatePicker
                          calendarIcon={null}
                          clearIcon={null}
                          onChange={this.onChangeDate}
                          value={this.state.date}
                        />
                        <img
                          src={Images.calendar}
                          className="profile-calendar-icon"
                        />
                      </div>
                    </div>

                    <div style={{ marginLeft: 20, width: "25%" }}>
                      <p>Date From</p>
                      <div
                        className="profile-input-border"
                        style={{
                          width: `calc(100% - 7px)`,
                          overflow: "inherit",
                          paddingTop: 6,
                          paddingBottom: 6,
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "space-between",
                          paddingRight: 5
                        }}
                      >
                        <DatePicker
                          calendarIcon={null}
                          clearIcon={null}
                          onChange={this.onChangeDate}
                          value={this.state.date}
                        />
                        <img
                          src={Images.calendar}
                          className="profile-calendar-icon"
                        />
                      </div>
                    </div>

                    <div
                      class="input-cont-width"
                      style={{ marginLeft: 20, width: "25%" }}
                    >
                      <p>Employer Name</p>
                      <div class="profile-input-border">
                        <input
                          className="profile-input"
                          placeholder=""
                          type="text"
                          onChange={this.myChangeHandler}
                        />
                      </div>
                    </div>
                  </div>

                  <div
                    style={{ display: "flex", marginLeft: 20, marginTop: 10 }}
                  >
                    <div class="input-cont-width" style={{ width: "25%" }}>
                      <p>City</p>
                      <div class="profile-input-border">
                        <input
                          className="profile-input"
                          placeholder=""
                          type="text"
                          onChange={this.myChangeHandler}
                        />
                      </div>
                    </div>

                    <div
                      class="input-cont-width"
                      style={{ marginLeft: 20, width: "25%" }}
                    >
                      <p>Occupation</p>
                      <div class="profile-input-border">
                        <input
                          className="profile-input"
                          placeholder=""
                          type="text"
                          onChange={this.myChangeHandler}
                        />
                      </div>
                    </div>

                    <div
                      class="input-cont-width"
                      style={{ marginLeft: 20, width: "25%" }}
                    >
                      <p>Job Sector</p>
                      <div class="profile-input-border">
                        <input
                          className="profile-input"
                          placeholder=""
                          type="text"
                          onChange={this.myChangeHandler}
                        />
                      </div>
                    </div>
                  </div>

                  <table
                    className="ca-invoice-table-cont"
                    style={{ borderSpacing: 1, border: 0, marginTop: 40 }}
                  >
                    <tbody>
                      <tr style={{ backgroundColor: "#F8F9FB" }}>
                        <th className="ca-table-heading">Name</th>
                        <th className="ca-table-heading">
                          Business/Company Name
                        </th>
                        <th className="ca-table-heading">Contact No</th>
                        <th className="ca-table-heading">Email</th>
                        <th className="ca-table-heading"></th>
                      </tr>
                      <tr style={{ backgroundColor: "#FFFFFF" }}>
                        <td className="report-table-content-text">
                          outsourcenz
                        </td>
                        <td className="report-table-content-text">hgdfv</td>
                        <td className="report-table-content-text">065464</td>
                        <td className="report-table-content-text">
                          annahyat@gmail.com
                        </td>
                        <td className="report-table-content-text">
                          <div style={{ display: "flex" }}>
                            <div>
                              <img
                                src={Images.docEmp}
                                style={{ width: 15, height: 15 }}
                              />
                            </div>
                            <div style={{ marginLeft: 5 }}>
                              <img
                                src={Images.userEmp}
                                style={{ width: 15, height: 15 }}
                              />
                            </div>
                            <div style={{ marginLeft: 5 }}>
                              <img
                                src={Images.docType}
                                style={{ width: 15, height: 15 }}
                              />
                            </div>
                            <div style={{ marginLeft: 5 }}>
                              <img
                                src={Images.crossRed}
                                style={{ width: 15, height: 15 }}
                              />
                            </div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              )}

              {/* =========== Desing from Client profile in employers =========== */}

              {true && (
                <div>
                  <div
                    className="sus-tab-container"
                    style={{ marginLeft: 20, marginTop: 30 }}
                  >
                    <div
                      onClick={() => this.onChangeECPTab("profile")}
                      className={
                        this.state.activeECPTab == "profile"
                          ? "sus-active-tab"
                          : "sus-inactive-tab"
                      }
                    >
                      <span
                        className={
                          this.state.activeECPTab == "profile"
                            ? "sus-active-tab-text"
                            : "sus-inactive-tab-text"
                        }
                      >
                        PROFILE
                      </span>
                    </div>
                    <div
                      onClick={() => this.onChangeECPTab("fileNotes")}
                      className={
                        this.state.activeECPTab == "fileNotes"
                          ? "sus-active-tab"
                          : "sus-inactive-tab"
                      }
                    >
                      <span
                        className={
                          this.state.activeECPTab == "fileNotes"
                            ? "sus-active-tab-text"
                            : "sus-inactive-tab-text"
                        }
                      >
                        FILE NOTES
                      </span>
                    </div>
                    <div
                      onClick={() => this.onChangeECPTab("documents")}
                      className={
                        this.state.activeECPTab == "documents"
                          ? "sus-active-tab"
                          : "sus-inactive-tab"
                      }
                    >
                      <span
                        className={
                          this.state.activeECPTab == "documents"
                            ? "sus-active-tab-text"
                            : "sus-inactive-tab-text"
                        }
                      >
                        DOCUMENTS
                      </span>
                    </div>
                    <div
                      onClick={() => this.onChangeECPTab("email")}
                      className={
                        this.state.activeECPTab == "email"
                          ? "sus-active-tab"
                          : "sus-inactive-tab"
                      }
                    >
                      <span
                        className={
                          this.state.activeECPTab == "email"
                            ? "sus-active-tab-text"
                            : "sus-inactive-tab-text"
                        }
                      >
                        EMAIL
                      </span>
                    </div>
                    <div
                      onClick={() => this.onChangeECPTab("case")}
                      className={
                        this.state.activeECPTab == "case"
                          ? "sus-active-tab"
                          : "sus-inactive-tab"
                      }
                    >
                      <span
                        className={
                          this.state.activeECPTab == "case"
                            ? "sus-active-tab-text"
                            : "sus-inactive-tab-text"
                        }
                      >
                        CASE
                      </span>
                    </div>
                    <div
                      onClick={() => this.onChangeECPTab("reports")}
                      className={
                        this.state.activeECPTab == "reports"
                          ? "sus-active-tab"
                          : "sus-inactive-tab"
                      }
                    >
                      <span
                        className={
                          this.state.activeECPTab == "reports"
                            ? "sus-active-tab-text"
                            : "sus-inactive-tab-text"
                        }
                      >
                        REPORTS
                      </span>
                    </div>
                  </div>

                  {this.state.activeECPTab == "profile" && (
                    <div className="emp-inner-gray-cont">
                      <div className="pciq-top-div" style={{ marginTop: 10 }}>
                        <span
                          className="pc-top-div-text"
                          style={{ color: "#0A3C5D" }}
                        >
                          PROFILE
                        </span>
                      </div>

                      <div className="emp-white-cont">
                        <div class="emp-input-width">
                          <p style={{ width: "30%" }}>Name</p>
                          <div
                            class="profile-input-border"
                            style={{ width: "50%" }}
                          >
                            <input
                              className="profile-input"
                              placeholder="Name"
                              type="text"
                              value={employerName}
                              onChange={this.myChangeHandler}
                            />
                          </div>
                        </div>

                        <div class="emp-input-width" style={{ marginTop: 25 }}>
                          <p style={{ width: "30%" }}>Business/Company Name</p>
                          <div
                            class="profile-input-border"
                            style={{ width: "50%" }}
                          >
                            <input
                              className="profile-input"
                              placeholder="Business"
                              type="text"
                              value={companyName}
                              onChange={this.myChangeHandler}
                            />
                          </div>
                        </div>

                        <div class="emp-input-width" style={{ marginTop: 25 }}>
                          <p style={{ width: "30%" }}>Email</p>
                          <div
                            class="profile-input-border"
                            style={{ width: "50%" }}
                          >
                            <input
                              className="profile-input"
                              placeholder="Email"
                              type="text"
                              value={employerEmail}
                              onChange={this.myChangeHandler}
                            />
                          </div>
                        </div>

                        <div class="emp-input-width" style={{ marginTop: 25 }}>
                          <p style={{ width: "30%" }}>Contact No</p>
                          <div
                            class="profile-input-border"
                            style={{ width: "50%" }}
                          >
                            <input
                              className="profile-input"
                              placeholder="Contact No"
                              type="text"
                              value={contactNo}
                              onChange={this.myChangeHandler}
                            />
                          </div>
                        </div>

                        <div class="emp-input-width" style={{ marginTop: 25 }}>
                          <p style={{ width: "30%" }}>Address</p>
                          <div
                            class="profile-input-border"
                            style={{ width: "50%" }}
                          >
                            <input
                              className="profile-input"
                              placeholder="Address"
                              type="text"
                              value={employerAddress}
                              onChange={this.myChangeHandler}
                            />
                          </div>
                        </div>

                        <div class="emp-input-width" style={{ marginTop: 25 }}>
                          <p style={{ width: "30%" }}>Agreement</p>
                          <div
                            class="profile-input-border"
                            style={{
                              width: "50%",
                              display: "flex",
                              alignItems: "center"
                            }}
                          >
                            <input
                              type="file"
                              onChange={this.onFileChange}
                              style={{ marginLeft: 5 }}
                            />
                          </div>
                        </div>

                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            marginTop: 30,
                            marginRight: 40
                          }}
                        >
                          <div></div>
                          <div
                            onClick={this.onSubmit}
                            className="sus-modal-button"
                          >
                            <span className="sus-modal-button-text">SAVE</span>
                          </div>
                        </div>
                      </div>

                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          marginTop: 20,
                          marginRight: 20
                        }}
                      >
                        <div className="pciq-top-div" style={{ marginTop: 0 }}>
                          <span
                            className="pc-top-div-text"
                            style={{ color: "#0A3C5D" }}
                          >
                            CONTACTS
                          </span>
                        </div>
                        <div onClick={this.openModal}>
                          <img
                            src={Images.plusIcon}
                            style={{ width: 20, height: 20 }}
                          />
                        </div>
                      </div>

                      <div className="emp-white-cont">
                        <table
                          className="ca-invoice-table-cont"
                          style={{
                            borderSpacing: 0,
                            borderColor: "#BFBFBF",
                            borderRadius: 0
                          }}
                        >
                          <tbody>
                            <tr style={{ backgroundColor: "#FFFFFF" }}>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5", textAlign: "left" }}
                              >
                                Name
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5", textAlign: "left" }}
                              >
                                Email
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5", textAlign: "left" }}
                              >
                                Number
                              </th>
                            </tr>
                          </tbody>
                        </table>
                      </div>

                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          marginTop: 20,
                          marginRight: 20
                        }}
                      >
                        <div className="pciq-top-div" style={{ marginTop: 0 }}>
                          <span
                            className="pc-top-div-text"
                            style={{ color: "#0A3C5D" }}
                          >
                            JOBS
                          </span>
                        </div>
                        <div onClick={this.openModal}>
                          <img
                            src={Images.plusIcon}
                            style={{ width: 20, height: 20 }}
                          />
                        </div>
                      </div>

                      <div className="emp-white-cont">
                        <table
                          className="ca-invoice-table-cont"
                          style={{
                            borderSpacing: 1,
                            borderColor: "#BFBFBF",
                            borderRadius: 0
                          }}
                        >
                          <tbody>
                            <tr style={{ backgroundColor: "#F8F9FB" }}>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Job Order No
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Job Title
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Position
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Open Date
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Close Date
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Required
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Contract
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Remuneration
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Location
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                ANZSCO Code
                              </th>
                              <th className="ca-table-heading"></th>
                            </tr>
                            <tr style={{ backgroundColor: "#FFFFFF" }}>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                123
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                Director
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                director
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                23/01/2020
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                23/01/2020
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                2
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                78
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                98.00
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                6564
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                y6565
                              </td>
                              <td className="report-table-content-text">
                                <div style={{ display: "flex" }}>
                                  <div>
                                    <img
                                      src={Images.multimediaBlue}
                                      style={{ width: 15, height: 15 }}
                                    />
                                  </div>
                                  <div>
                                    <img
                                      src={Images.disable}
                                      style={{
                                        marginLeft: 5,
                                        width: 15,
                                        height: 15
                                      }}
                                    />
                                  </div>
                                  <div style={{ marginLeft: 5 }}>
                                    <img
                                      src={Images.docType}
                                      style={{ width: 15, height: 15 }}
                                    />
                                  </div>
                                  <div style={{ marginLeft: 5 }}>
                                    <img
                                      src={Images.crossRed}
                                      style={{ width: 15, height: 15 }}
                                    />
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>

                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          marginTop: 20,
                          marginRight: 20
                        }}
                      >
                        <div className="pciq-top-div" style={{ marginTop: 0 }}>
                          <span
                            className="pc-top-div-text"
                            style={{ color: "#0A3C5D" }}
                          >
                            CLIENTS
                          </span>
                        </div>
                        <div onClick={this.openModal}>
                          <img
                            src={Images.plusIcon}
                            style={{ width: 20, height: 20 }}
                          />
                        </div>
                      </div>

                      <div className="emp-white-cont">
                        <table
                          className="ca-invoice-table-cont"
                          style={{
                            borderSpacing: 1,
                            borderColor: "#BFBFBF",
                            borderRadius: 0
                          }}
                        >
                          <tbody>
                            <tr style={{ backgroundColor: "#F8F9FB" }}>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Status
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Name
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Email
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                DOB
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Country
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Employer Name
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Job Titel
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Pay Details
                              </th>
                              <th
                                className="ca-table-heading"
                                style={{ color: "#0366A5" }}
                              >
                                Client Tags
                              </th>
                            </tr>
                            <tr style={{ backgroundColor: "#FFFFFF" }}>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                <div style={{ display: "flex" }}>
                                  <div
                                    className="report-column-btn"
                                    style={{ backgroundColor: "#3AB449" }}
                                  >
                                    <span style={{ color: "#FFFFFF" }}>
                                      Active
                                    </span>
                                  </div>
                                </div>
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                Ashley Rawin
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                Ab@gmail.com
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                21/08/1990
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              >
                                New zealand
                              </td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              ></td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              ></td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              ></td>
                              <td
                                className="report-table-content-text"
                                style={{ color: "#0366A5" }}
                              ></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  )}

                  {this.state.activeECPTab == "documents" && (
                    <div
                      className="emp-inner-gray-cont"
                      style={{ borderColor: "#E1E1E1" }}
                    >
                      <div
                        className="sus-tab-container"
                        style={{ marginLeft: 20, marginTop: 30 }}
                      >
                        <div
                          onClick={() => this.onChangeDocTab("docDocument")}
                          className={
                            this.state.activeDocTab == "docDocument"
                              ? "sus-active-tab"
                              : "sus-inactive-tab"
                          }
                        >
                          <span
                            className={
                              this.state.activeDocTab == "docDocument"
                                ? "sus-active-tab-text"
                                : "sus-inactive-tab-text"
                            }
                          >
                            DOUCMENT
                          </span>
                        </div>
                        <div
                          onClick={() => this.onChangeDocTab("docChecklist")}
                          className={
                            this.state.activeDocTab == "docChecklist"
                              ? "sus-active-tab"
                              : "sus-inactive-tab"
                          }
                        >
                          <span
                            className={
                              this.state.activeDocTab == "docChecklist"
                                ? "sus-active-tab-text"
                                : "sus-inactive-tab-text"
                            }
                          >
                            DOCUMENT CHECKLIST
                          </span>
                        </div>
                      </div>

                      {this.state.activeDocTab == "docDocument" && (
                        <div className="emp-doc-sub-tab-cont">
                          <div className="emp-doc-btn-row">
                            <div />
                            <div style={{ display: "flex" }}>
                              <div
                                className="cv-org-btn-cont"
                                style={{ marginLeft: 25 }}
                              >
                                <div className="cv-plus-cont">
                                  <span className="cv-plus-icon">+</span>
                                </div>
                                <span className="cv-org-btn-text">
                                  ADD DOCUMENTS
                                </span>
                              </div>

                              <div>
                                <div className="cv-actions-cont">
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontWeight: "500", marginLeft: 0 }}
                                  >
                                    Actions
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="emp-white-cont">
                            <table
                              className="ca-invoice-table-cont"
                              style={{
                                borderSpacing: 0,
                                borderColor: "#BFBFBF",
                                borderRadius: 0
                              }}
                            >
                              <tbody>
                                <tr style={{ backgroundColor: "#FFFFFF" }}>
                                  <th className="emp-small-column">
                                    <input
                                      type="checkbox"
                                      className="sus-checkbox"
                                      onChange={this.handleCheck}
                                      defaultChecked={this.state.checked}
                                    />
                                  </th>
                                  <th className="emp-small-column">#</th>
                                  <th
                                    className="ca-table-heading"
                                    style={{
                                      color: "#0366A5",
                                      textAlign: "left"
                                    }}
                                  >
                                    Document
                                  </th>
                                  <th
                                    className="ca-table-heading"
                                    style={{ color: "#0366A5" }}
                                  >
                                    Title
                                  </th>
                                  <th
                                    className="ca-table-heading"
                                    style={{ color: "#0366A5" }}
                                  >
                                    Action
                                  </th>
                                </tr>
                              </tbody>
                            </table>
                          </div>

                          <div className="emp-white-cont">
                            <table
                              className="ca-invoice-table-cont"
                              style={{
                                borderSpacing: 0,
                                borderColor: "#BFBFBF",
                                borderRadius: 0
                              }}
                            >
                              <tbody>
                                <tr style={{ backgroundColor: "#FFFFFF" }}>
                                  <td
                                    className="report-table-content-text"
                                    style={{
                                      color: "#0366A5",
                                      paddingLeft: 0,
                                      paddingRight: 0,
                                      width: 25
                                    }}
                                  >
                                    <input
                                      type="checkbox"
                                      className="sus-checkbox"
                                      onChange={this.handleCheck}
                                      defaultChecked={this.state.checked}
                                    />
                                  </td>
                                  <td
                                    className="report-table-content-text"
                                    style={{
                                      color: "#0366A5",
                                      paddingLeft: 0,
                                      paddingRight: 0,
                                      width: 25
                                    }}
                                  >
                                    1
                                  </td>
                                  <td
                                    className="report-table-content-text"
                                    style={{
                                      color: "#0366A5",
                                      textAlign: "left"
                                    }}
                                  >
                                    <div>
                                      <span
                                        className="cv-doc-text"
                                        style={{ color: "#1081B8" }}
                                      >
                                        Merge-66598939340573894-PDF.pdf
                                      </span>
                                    </div>
                                    <div className="cv-doc-date-text-cont">
                                      <span className="cv-doc-date-text">
                                        12/02/2019 | 20 KB
                                      </span>
                                    </div>
                                  </td>
                                  <td
                                    className="report-table-content-text"
                                    style={{ color: "#0366A5" }}
                                  >
                                    <div>
                                      <div
                                        className="cv-title-box"
                                        style={{ padding: 3 }}
                                      >
                                        <span
                                          className="cv-normal-text"
                                          style={{ fontSize: 7, marginLeft: 5 }}
                                        >
                                          Merge-66598939340573894-PDF.pdf
                                        </span>
                                      </div>
                                    </div>
                                  </td>
                                  <td
                                    className="report-table-content-text"
                                    style={{
                                      display: "flex",
                                      justifyContent: "flex-end"
                                    }}
                                  >
                                    <div style={{ display: "flex" }}>
                                      <div className="cv-action-icons-border">
                                        <img
                                          src={Images.download}
                                          className="cv-action-icon"
                                        />
                                      </div>
                                      <div
                                        className="cv-action-icons-border"
                                        style={{ marginLeft: 5 }}
                                      >
                                        <img
                                          src={Images.visibility}
                                          className="cv-action-icon"
                                        />
                                      </div>
                                      <div
                                        className="cv-action-icons-border"
                                        style={{ marginLeft: 5 }}
                                      >
                                        <img
                                          src={Images.download}
                                          className="cv-action-icon"
                                        />
                                      </div>
                                      <div className="cv-action-icons-border">
                                        <img
                                          src={Images.fileNotes}
                                          className="cv-action-icon"
                                        />
                                      </div>
                                      <div
                                        className="cv-action-icons-border"
                                        style={{ marginLeft: 5 }}
                                      >
                                        <img
                                          src={Images.multimediaBlue}
                                          className="cv-action-icon"
                                        />
                                      </div>
                                      <div
                                        className="cv-action-icons-border"
                                        style={{ marginLeft: 5 }}
                                      >
                                        <img
                                          src={Images.deleteIcon}
                                          className="cv-action-icon"
                                        />
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      )}

                      {this.state.activeDocTab == "docChecklist" && (
                        <div
                          className="emp-doc-sub-tab-cont"
                          style={{ border: 0 }}
                        >
                          <div
                            className="emp-white-cont"
                            style={{ padding: 0, margin: 0 }}
                          >
                            <table
                              className="ca-invoice-table-cont"
                              style={{
                                borderSpacing: 1,
                                backgroundColor: "#ACACAC",
                                border: 0,
                                borderRadius: 0
                              }}
                            >
                              <tbody>
                                <tr>
                                  <td
                                    colSpan={3}
                                    style={{ backgroundColor: "#FFFFFF" }}
                                  >
                                    <div className="emp-doc-checklist-slect-option">
                                      <Select
                                        value={selectedOption}
                                        onChange={this.handleChange}
                                        options={options}
                                      />
                                    </div>
                                  </td>
                                </tr>
                                <tr style={{ backgroundColor: "#F8F9FB" }}>
                                  <th
                                    className="ca-table-heading"
                                    style={{ textAlign: "left" }}
                                  >
                                    Title
                                  </th>
                                  <th className="ca-table-heading">
                                    Link Client
                                  </th>
                                  <th className="ca-table-heading">Action</th>
                                </tr>
                                <tr style={{ backgroundColor: "#F3F4F6" }}>
                                  <td colSpan={3} style={{ height: 40 }}></td>
                                </tr>
                                <tr style={{ backgroundColor: "#FFFFFF" }}>
                                  <td
                                    className="report-table-content-text"
                                    style={{ textAlign: "left" }}
                                  >
                                    XYTTTASD
                                  </td>
                                  <td className="report-table-content-text">
                                    Please{" "}
                                    <span style={{ color: "#0366A5" }}>
                                      click here
                                    </span>{" "}
                                    to open
                                  </td>
                                  <td
                                    className="report-table-content-text"
                                    style={{
                                      display: "flex",
                                      justifyContent: "center"
                                    }}
                                  >
                                    <div className="sus-modal-button">
                                      <span className="sus-modal-button-text">
                                        SEND
                                      </span>
                                    </div>
                                  </td>
                                </tr>
                                <tr style={{ backgroundColor: "#FFFFFF" }}>
                                  <td
                                    className="report-table-content-text"
                                    style={{ textAlign: "left" }}
                                  >
                                    XYTTTASD
                                  </td>
                                  <td className="report-table-content-text">
                                    Please{" "}
                                    <span style={{ color: "#0366A5" }}>
                                      click here
                                    </span>{" "}
                                    to open
                                  </td>
                                  <td
                                    className="report-table-content-text"
                                    style={{
                                      display: "flex",
                                      justifyContent: "center"
                                    }}
                                  >
                                    <div className="sus-modal-button">
                                      <span className="sus-modal-button-text">
                                        SEND
                                      </span>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      )}
                    </div>
                  )}
                </div>
              )}
            </div>

            {false && (
              <div
                className="report-container"
                style={{ paddingTop: 30, paddingBottom: 30 }}
              >
                <div className="pciq-top-div">
                  <span
                    className="pc-top-div-text"
                    style={{ color: "#0A3C5D" }}
                  >
                    VISA DETAIL
                  </span>
                </div>

                <div className="pc-text-field-row" style={{ marginBottom: 20 }}>
                  <div>
                    <span className="cv-normal-text">Show</span>
                  </div>
                  <div
                    className="select-options"
                    style={{ width: 100, marginLeft: 15 }}
                  >
                    <Select
                      value={selectedOption}
                      onChange={this.handleChange}
                      options={options}
                    />
                  </div>

                  <div>
                    <span className="cv-normal-text">entries</span>
                  </div>
                </div>

                <table
                  className="ca-invoice-table-cont"
                  style={{
                    borderSpacing: 1,
                    border: 0,
                    backgroundColor: "#BFBFBF",
                    borderRadius: 0
                  }}
                >
                  <tbody>
                    <tr style={{ backgroundColor: "#F8F9FB" }}>
                      <th className="ca-table-heading">Sn</th>
                      <th className="ca-table-heading">Name</th>
                      <th className="ca-table-heading">Email</th>
                      <th className="ca-table-heading">Visa Type</th>
                      <th className="ca-table-heading">Visa Status</th>
                      <th className="ca-table-heading">Visa Modified Date</th>
                      <th className="ca-table-heading">Filter Status</th>
                    </tr>
                    <tr style={{ backgroundColor: "#FFFFFF" }}>
                      <td className="report-table-content-text">1</td>
                      <td className="report-table-content-text">
                        Benedict Thomas
                      </td>
                      <td className="report-table-content-text">
                        vshgh@gmail.com
                      </td>
                      <td className="report-table-content-text">
                        visitor visa
                      </td>
                      <td className="report-table-content-text">Active</td>
                      <td className="report-table-content-text">13/01/2020</td>
                      <td className="report-table-content-text"></td>
                    </tr>
                    <tr style={{ backgroundColor: "#FFFFFF" }}>
                      <td className="report-table-content-text">2</td>
                      <td className="report-table-content-text">
                        Benedict Thomas
                      </td>
                      <td className="report-table-content-text">
                        vshgh@gmail.com
                      </td>
                      <td className="report-table-content-text">
                        visitor visa
                      </td>
                      <td className="report-table-content-text">Active</td>
                      <td className="report-table-content-text">13/01/2020</td>
                      <td className="report-table-content-text"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

// const mapStateToProps = state => ({
//   authToken: state.appAccount.authToken
// });

// const mapDispatchToProps = dispatch => ({
//   attemptToAddEmployer: (data, token) =>
//     dispatch(EmployerActions.addEmployerRequest(data, token)),
//   attemptToGetEmployer: id => dispatch(EmployerActions.getEmployerRequest(id)),
//   attemptToGetAllEmployer: id =>
//     dispatch(EmployerActions.getAllEmployerRequest(id)),
//   attemptToUpdateEmployer: data =>
//     dispatch(EmployerActions.updEmployerRequest(data)),
//   attemptToDeleteEmployer: id =>
//     dispatch(EmployerActions.dltEmployerRequest(id))
// });

// EmployerManagement = connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(EmployerManagement);

export default EmployerManagement;
