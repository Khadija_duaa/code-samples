import React, { Fragment } from "react";
// import { Images } from "./../../Themes";
import { Spin, message } from "antd";
import EmployerManagementHead from "./EmployerManagement/index";
import HeaderBar from "../Components/Header/HeaderBar";
import Sidebar from "../Components/SideBar";

let clientprofileid = JSON.parse(
  window.localStorage.getItem("clientprofileid")
);

const EmployerMain = () => {
  return (
    <Fragment>
      <div className="width-100">
        <EmployerManagementHead />
      </div>
    </Fragment>
  );
};

export default EmployerMain;
