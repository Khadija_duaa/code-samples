import React from "react";
import { Avatar } from "antd";
import { Link } from "react-router-dom";
import { Tabs, Radio } from "antd";

const { TabPane } = Tabs;

const EmployerManagementTabs = () => {
  return (
    <div className="client-profile-tabs">
      <Tabs defaultActiveKey="1" type="card" size={"small"} className="mar-r">
        <TabPane tab="Client Information" key="1"></TabPane>
        <TabPane tab="Employar Information" key="2"></TabPane>
        <TabPane tab="Job History" key="3"></TabPane>
        <TabPane tab="Qualification" key="4"></TabPane>
        <TabPane
          key="5"
          tab={
            <a
              target="_blank"
              href="https://ezymigrate.co.nz/Questionnaire/ViewQuestionnaire3?para=MjU4YTI3ODE4OWEx"
              style={{ color: "#858585" }}
            >
              Basic Assesment Form
            </a>
          }
        >
          <iframe
            width="100%"
            height={800}
            src="https://ezymigrate.co.nz/Questionnaire/ViewQuestionnaire3?para=MjU4YTI3ODE4OWEx"
            title="Basic Assesment Form"
          ></iframe>
        </TabPane>
        <TabPane
          key="6"
          tab={
            <a
              target="_blank"
              href="https://ezymigrate.co.nz/Questionnaire/Index?para=MjU4YTI3ODE4OWEx"
              style={{ color: "#858585" }}
            >
              Assesment Form
            </a>
          }
        >
          <iframe
            width="100%"
            height={800}
            src="https://ezymigrate.co.nz/Questionnaire/Index?para=MjU4YTI3ODE4OWEx"
            title="Assesment Form"
          ></iframe>
        </TabPane>
      </Tabs>
    </div>
  );
};

export default EmployerManagementTabs;
