import React, { useEffect, useState } from "react";
import {
  CloseCircleOutlined,
  FileTextOutlined,
  MenuOutlined
} from "@ant-design/icons";
import {
  sortableContainer,
  sortableElement,
  sortableHandle
} from "react-sortable-hoc";
import { Table, message } from "antd";

const EmployerCreditNotes = ({}) => {
  const [loading, setLoading] = useState(false);

  //   useEffect(() => {
  //     // setLoading(true);
  //      
  //     ongetEmployerContact(userDataEmp && userDataEmp.id).then(() => {
  //       // setLoading(false);
  //     });
  //   }, [ongetEmployerContact, userDataEmp]);

  //   const removeTag = id => {
  //     setLoading(true);
  //      
  //     const remove = {
  //       id: id,
  //       delete: true,
  //       modifiedBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  //     };
  //      
  //     onRemoveEmployerContact(remove).then(() => {
  //       ongetEmployerContact(userDataEmp && userDataEmp.id);
  //       setLoading(false);
  //       message.success("Successfully Deleted!");
  //     });
  //   };

  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street"
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street"
    }
  ];

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: "10%"
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email"
    },
    {
      title: "Number",
      dataIndex: "number",
      key: "number"
    },
    {
      title: "Action",
      dataIndex: "action",
      render: (text, record) => {
        return (
          <div className="table-action">
            <FileTextOutlined />
            <CloseCircleOutlined
              style={{
                color: "white",
                backgroundColor: "red",
                borderRadius: "44px"
              }}
              //   onClick={() => removeTag(record && record.id)}
            />
          </div>
        );
      }
    }
  ];

  // const showModal = (value, modelType) => {
  //    
  //   setIsModalVisible(value);
  //   setCreateMode(modelType);
  //   setUpdatedata(value);
  // };
  return (
    <div>
      <div>
        <div style={{ width: "90%", margin: "auto" }}>
          <h2 style={{ color: "gray", marginTop: "10px" }}>CREDIT NOTES</h2>
        </div>
        <Table
          className="job-table incoive-table table-head employer-table border-3 "
          dataSource={dataSource && dataSource}
          columns={columns}
        />
      </div>
    </div>
  );
};
export default EmployerCreditNotes;
