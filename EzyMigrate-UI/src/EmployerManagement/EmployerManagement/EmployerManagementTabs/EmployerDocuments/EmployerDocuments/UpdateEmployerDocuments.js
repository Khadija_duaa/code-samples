import React, { Fragment, useState } from "react";
import { Upload, Button, message, Form } from "antd";
import { FileDoneOutlined, PlusOutlined } from "@ant-design/icons";

const UpdateEmployerDocuments = ({
  onAddDocumentTittle,
  onGetEmployerDocument,

  employerDocumentRes,

  userDataEmp
}) => {
  const [fileList, setFileList] = useState([]);

  const UpdateTittle = record => {
    // setLoading(true);
     
    const update = {
      documentTitle: employerDocumentRes && employerDocumentRes.blobFileName,
      id: 0
    };
     
    onAddDocumentTittle(update).then(() => {
       
      onGetEmployerDocument(userDataEmp && userDataEmp.id);
      // setLoading(false);
      message.success("Successfully Updated!");
    });
  };

  return (
    <Fragment>
      <div className="promotional-banner upload-csv-file-sec">
        <p>Do you want to update title of this document ?</p>
      </div>
      <Button htmlType="submit" type="primary" onClick={UpdateTittle}>
        OK
      </Button>
    </Fragment>
  );
};

export default UpdateEmployerDocuments;
