import React, { Fragment, useState, useEffect } from "react";
import {
  Form,
  Input,
  Button,
  message,
  Row,
  Col,
  Select,
  Checkbox,
  Space
} from "antd";
import { CloseCircleOutlined, PlusOutlined } from "@ant-design/icons";
import CreateEmailDocuments from "./CreateEmailDocuments";
import CreateEmailEditors from "./CreateEmailEditors";
import FroalaEditorCom from "../../../../../Containers/FroalaEditorCom";

const { Option } = Select;

const content = ["one", "two", "three", "four", "five"];

function onRequiredChange(e) {
  console.log(`checked = ${e.target.checked}`);
}

function onLetterChange(value) {
  console.log(`selected ${value}`);
}
function onChange(value) {
  console.log(`selected ${value}`);
}

function onBlur() {
  console.log("blur");
}

function onFocus() {
  console.log("focus");
}

function onSearch(val) {
  console.log("search:", val);
}

var contractsArray = [];

const DocumentChecklistForm = ({
  onSendEmailLink,

  onGetLetterTemplates,
  LetterTemplatesRes,

  onAddEmployerEmail
}) => {
  const [to, setTo] = useState("");
  const [ccc, setCcc] = useState("");
  const [bcc, setBcc] = useState("");
  const [dataSource, setDataSource] = useState([]);
  const [lettersData, setLettersData] = useState([]);
  const [contractsData, setContractsData] = useState([]);
  const [letterString, setLetterString] = useState();

  useEffect(() => {
    // setLoading(true);
     
    onGetLetterTemplates().then(() => {
      // setLoading(false);
    });
  }, [onGetLetterTemplates]);

  const onContractChange = obj => {
     
    setContractsData(prevState => [...prevState, obj]);
  };

  const handleEditNote = value => {
     
    setLetterString(value);
  };

  const removeEmploye = index => {
    //  
    // contractsData.splice(index, 1);
    //  
    // setContractsData(newArr);
  };

  useEffect(() => {
    letterTempaltesFilter(LetterTemplatesRes);
  }, [LetterTemplatesRes]);

  const letterTempaltesFilter = letterTemplatesRes => {
    let filterList = [];
    let filterListLetters = [];
    if (
      letterTemplatesRes &&
      letterTemplatesRes.items &&
      letterTemplatesRes.items.length > 0
    ) {
      letterTemplatesRes.items.map((data, index) => {
        if (data.type === "CONTRACT") {
          data.index = index;
          data.key = `${index + 1}`;
          filterList.push(data);
        }
      });
       
      setDataSource(filterList);
    }
    if (
      letterTemplatesRes &&
      letterTemplatesRes.items &&
      letterTemplatesRes.items.length > 0
    ) {
      letterTemplatesRes.items.map((data, index) => {
        if (data.type === "LETTER") {
          data.index = index;
          data.key = `${index + 1}`;
          filterListLetters.push(data);
        }
      });
       
      setLettersData(filterListLetters);
    }
  };

  const [form] = Form.useForm();
  const onFinish = values => {
    // setLoading(true);
     
    console.log("Received values of form:", values);

    const formData = new FormData();
    formData.append(`Recipients[0].name`, to && to);
    formData.append(`Recipients[0].type`, "To");
    formData.append(`Recipients[1].name`, ccc && ccc);
    formData.append(`Recipients[1].type`, "CC");
    formData.append(`Recipients[2].name`, bcc);
    formData.append(`Recipients[2].type`, "Bcc");
    formData.append("Subject", values && values.subject);
    formData.append("Message", letterString);
    formData.append("From", "usamamateen079@gmail.com");
    formData.append("Priority", "10");
    formData.append("FrequencyCode", "employer");
    formData.append("ModuleId", "10");

    formData.append("Attachments", []);
     
    onSendEmailLink(formData)
      // .then(() => onGetDocumentChecklist())
      .then(() => {
        // setLoading(false);
        message.success("Successfully Sent!");
      });

    const draftData = {
      id: 0,
      emailMessage: letterString && letterString,
      isRead: true,
      attachmentName: "string",
      attachmentUrl: "string",
      sizeInKB: 0,
      subject: values && values.subject,
      from: "usamamateen079@gmail.com",
      import: true,
      to: to && to,
      cc: ccc && ccc,
      bcc: bcc && bcc,
      notClients: true,
      clientReply: true,
      clientReplyMail: 0,
      cUserId: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      importMessageId: "string",
      draft: true,
      emailType: 0,
      importText: "string",
      employerId: "43d03cc6-e8ad-444c-9e6e-79bd05120928",
      importedDate: "2021-05-04T19:08:58.521Z",
      autoImport: true,
      isSent: true,
      clientEmailType: 0,
      timeStamp: "2021-05-04T19:08:58.521Z"
    };
     
    onAddEmployerEmail(draftData);
  };

  return (
    <Fragment>
      <Form onFinish={onFinish} form={form} className="width-100" name="main">
        <div className="border-box-checklist add-employer-para">
          <Row gutter={8}>
            <Col>
              <Form.Item required={false}>
                <Select
                  showSearch
                  style={{ width: 250 }}
                  placeholder="CONTRACTS"
                  optionFilterProp="children"
                  // onFocus={onFocus}
                  // onBlur={onBlur}
                  // onSearch={onSearch}
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {dataSource &&
                    dataSource.map(item => (
                      <Option key={item.id}>
                        <span onClick={() => onContractChange(item)}>
                          {item.name}
                        </span>
                      </Option>
                    ))}
                </Select>
              </Form.Item>
            </Col>
            <Col>
              <Form.Item required={false}>
                <Select
                  showSearch
                  style={{ width: 250 }}
                  placeholder="LETTERS"
                  optionFilterProp="children"
                  onChange={onLetterChange}
                  onFocus={onFocus}
                  onBlur={onBlur}
                  onSearch={onSearch}
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {lettersData &&
                    lettersData.map(item => (
                      <Option key={item.id}> {item.name}</Option>
                    ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <div className="create-email-form" style={{ width: "90%" }}>
            <Form.Item required={false}>
              <Checkbox onChange={onRequiredChange}>Send me a copy</Checkbox>
            </Form.Item>
            <Form.Item name="to" required={false}>
              <Input
                placeholder="To"
                value={to}
                onChange={e => setTo(e.target.value)}
              />
            </Form.Item>
            <Form.Item name="ccc" required={false}>
              <Input
                placeholder="CC"
                value={ccc}
                onChange={e => setCcc(e.target.value)}
              />
            </Form.Item>
            <Form.Item name="cc" required={false}>
              <Select
                showSearch
                style={{ width: 70 }}
                placeholder="CC"
                optionFilterProp="children"
                onChange={onChange}
                onFocus={onFocus}
                onBlur={onBlur}
                onSearch={onSearch}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }
              >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </Form.Item>
            <Form.Item name="bcc" required={false}>
              <Input
                placeholder="Bcc"
                value={bcc}
                onChange={e => setBcc(e.target.value)}
              />
            </Form.Item>
            <Form.Item name="subject" required={false}>
              <Input placeholder="Subject" value={ccc} />
            </Form.Item>
          </div>
          <div className="margin-top-20 letter-froala">
            <FroalaEditorCom
              model={letterString}
              onModelChange={value => setLetterString(value)}
            />
          </div>

          {contractsData &&
            contractsData.length > 0 &&
            contractsData.map((data, index) => {
               
              return (
                <Fragment>
                  <div className="close-editor">
                    <CloseCircleOutlined
                      style={{
                        color: "white",
                        backgroundColor: "red",
                        borderRadius: "44px",
                        // width: "30px",
                        fontSize: "20px",
                        marginRight: "8px"
                      }}
                      onClick={() => removeEmploye(index)}
                    />
                  </div>
                  <div
                    style={{ marginTop: "11px" }}
                    className="  letter-froala"
                  >
                    <FroalaEditorCom
                      model={data.content}
                      onModelChange={value => handleEditNote(value)}
                    />

                  </div>
                </Fragment>
              );
            })}

          <div className="margin-top-20">
            <CreateEmailDocuments />
          </div>
        </div>
        <Row className="margin-top-12" gutter={10}>
          <Col>
            <Form.Item>
              <Button
                type="primary"
                className="login-form-button save-btn"
                htmlType="submit"
              >
                SEND NOW
              </Button>
            </Form.Item>
          </Col>
          <Col>
            <Form.Item>
              <Button
                onClick={() => onAddEmployerEmail()}
                type="primary"
                className="login-form-button save-btn"
              >
                SAVE AS DRAFT
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Fragment>
  );
};
export default DocumentChecklistForm;
