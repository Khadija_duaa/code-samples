import React, { Fragment, useEffect, useState } from "react";
import { Tabs, Modal } from "antd";
import EmployerEmailHistory from "./EmployerEmailHistory/EmployerEmailHistory";
// import AddEmployerDocuments from "./EmployerEmailHistory/AddEmployerDocuments";
// import UpdateEmployerDocuments from "./EmployerEmailHistory/UpdateEmployerDocuments";
import CreateEmail from "./CreateEmail/CreateEmail";

const { TabPane } = Tabs;

const EmployerEmailTabs = ({
  userDataEmp,

  onAddEmployerDocument,

  onRemoveEmployerDocument,

  onUpdateEmployerDocument,

  onUploadAvatar,
  imageUploadSuccess,

  onGetDocumentChecklist,
  docChecklistRes,

  onGetEmployerCheckList,
  employerCheckListRes,

  onAddEmployerCheckList,

  location,
  history,

  onSendEmailLink,

  onGetLetterTemplates,
  LetterTemplatesRes,

  onAddEmployerEmail,

  onGetEmployerHistory,
  employerHistoryRes,

  onGetAutoEmailImport,
  emailImportRes,

  onAddAutoEmailImport,

  onAddAnyTamplate,
  addAnyTemRes,

  onDeleteCreateEmail,

  onGetSignature,
  signatureRes,

  onGetDocuments,
  documentRes,
  onGetEmployerDocument,

  onGetImapForAll,
  imapForAllRes,
  onGetPdf,
  onGetDocumentDownload,
  onAddEmailDocument,
  employerDocumentRes,
}) => {
  const [modalType, setModalType] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [handleKey, setHandleKey] = useState("1");
  const [profileData, setProfileData] = useState(null);

  //   useEffect(() => {
  //     onGetEmployerData(userDataEmp && userDataEmp.id);
  //   }, [onGetEmployerData, userDataEmp]);

  useEffect(() => {
    var data = JSON.parse(localStorage.getItem("profileData"));
    setProfileData(data);
  }, []);

  const showModal = (modalTypeName) => {
    setModalType(modalTypeName);
    setIsModalVisible(true);
    // setAddReminders(true);
    // setUpdateTitle(id);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const callback = (key) => {
    setHandleKey(key);
  };

  return (
    <Fragment>
      <div style={{ display: "flex" }}>
        <div className="employerin-box">
          <div className="bg-white ">
            <Tabs
              activeKey={handleKey}
              type="card"
              size={"small"}
              className="mar-r employer-doc-tab"
              onChange={(key) => callback(key)}
            >
              <TabPane tab="HISTORY" key="1">
                <div
                  style={{
                    border: "2px solid #c9c9ca",
                    padding: "20px",
                    backgroundColor: "#f0f2f5b8",
                  }}
                >
                  <EmployerEmailHistory
                    userDataEmp={userDataEmp}
                    showModal={showModal}
                    onGetEmployerHistory={onGetEmployerHistory}
                    employerHistoryRes={employerHistoryRes}
                    onGetAutoEmailImport={onGetAutoEmailImport}
                    emailImportRes={emailImportRes}
                    onAddAutoEmailImport={onAddAutoEmailImport}
                    onDeleteCreateEmail={onDeleteCreateEmail}
                    profileData={profileData}
                    onSendEmailLink={onSendEmailLink}
                    onGetLetterTemplates={onGetLetterTemplates}
                    LetterTemplatesRes={LetterTemplatesRes}
                    onAddEmployerEmail={onAddEmployerEmail}
                    onAddAnyTamplate={onAddAnyTamplate}
                    addAnyTemRes={addAnyTemRes}
                    onUploadAvatar={onUploadAvatar}
                    imageUploadSuccess={imageUploadSuccess}
                    setHandleKey={setHandleKey}
                    onGetSignature={onGetSignature}
                    signatureRes={signatureRes}
                    onGetDocuments={onGetDocuments}
                    documentRes={documentRes}
                    onGetEmployerDocument={onGetEmployerDocument}
                    onGetImapForAll={onGetImapForAll}
                    imapForAllRes={imapForAllRes}
                    onGetPdf={onGetPdf}
                    onAddEmailDocument={onAddEmailDocument}
                    employerDocumentRes={employerDocumentRes}
                    onGetDocumentDownload={onGetDocumentDownload}
                  />
                </div>
              </TabPane>
              <TabPane tab="CREATE" key="2">
                {handleKey === "1" ?
                  null:
                <div
                  style={{
                    border: "2px solid #c9c9ca",
                    padding: "20px",
                    backgroundColor: "#f0f2f5b8",
                  }}
                >
                  <CreateEmail
                    onSendEmailLink={onSendEmailLink}
                    onGetLetterTemplates={onGetLetterTemplates}
                    LetterTemplatesRes={LetterTemplatesRes}
                    onAddEmployerEmail={onAddEmployerEmail}
                    userDataEmp={userDataEmp}
                    onAddAnyTamplate={onAddAnyTamplate}
                    addAnyTemRes={addAnyTemRes}
                    onUploadAvatar={onUploadAvatar}
                    imageUploadSuccess={imageUploadSuccess}
                    onGetEmployerHistory={onGetEmployerHistory}
                    setHandleKey={setHandleKey}
                    onGetSignature={onGetSignature}
                    signatureRes={signatureRes}
                    onGetDocuments={onGetDocuments}
                    documentRes={documentRes}
                    onGetEmployerDocument={onGetEmployerDocument}
                    onGetPdf={onGetPdf}
                    onGetDocumentDownload={onGetDocumentDownload}
                    onDeleteCreateEmail={onDeleteCreateEmail}
                    onAddEmailDocument={onAddEmailDocument}
                    employerDocumentRes={employerDocumentRes}
                  />
                </div>}
              </TabPane>
            </Tabs>
          </div>
        </div>
      </div>

      {/* {isModalVisible && (
        <div className="reminder-model">
          <Modal
            className="reminder-model-main"
            title={
              (modalType === "add-documents" && "ADD DOCUMENTS") ||
              (modalType === "update-documents" && "UPDATE DOCUMENTS")
            }
            visible={isModalVisible}
            onCancel={handleCancel}
            // Header={true}
            footer={false}
          >
            {modalType === "add-documents" && (
              <AddEmployerDocuments
                onAddEmployerDocument={onAddEmployerDocument}
                onUploadAvatar={onUploadAvatar}
                imageUploadSuccess={imageUploadSuccess}
              />
            )}

            {modalType === "update-documents" && (
              <UpdateEmployerDocuments
                onAddEmployerDocument={onAddEmployerDocument}
              />
            )}
          </Modal>
        </div>
      )} */}
    </Fragment>
  );
};

export default EmployerEmailTabs;
