import React, { Fragment, useEffect } from "react";
import {
  Form,
  Input,
  Button,
  message,
  Row,
  Col,
  DatePicker,
  InputNumber,
  Select,
} from "antd";
import FroalaEditor from "react-froala-wysiwyg";
import "froala-editor/js/plugins.pkgd.min.js";

function onOpenDateChange(date, dateString) {
  console.log(date, dateString);
}

function onChange(date, dateString) {
  console.log(date, dateString);
}

function onNumberChange(value) {
  console.log("changed", value);
}

const JobEmployerForm = ({
  handleCancel,

  // setLoading,

  onUpdetaClientTag,
  updateClientTagRes,

  createMode,

  updatedata,

  onGetEmployerJob,

  onAddEmployerJob,

  userDataEmp,

  showModal,
}) => {
  // useEffect(() => {
  //   form.setFieldsValue({
  //     name: updatedata.name
  //   });
  // }, [form, updatedata.name]);
  const [form] = Form.useForm();
  const onFinish = (values) => {
    // setLoading(true);
    console.log("valuesvaluesvalues", values);

    const data = {
      employerId: userDataEmp && userDataEmp.id,
      jobStatusId: 1,
      jobTitle: values.job_tittle,
      liaId: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      salePersonId: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      jobOrderNo: values.job_no,
      position: "string",
      openDate: "2021-03-30T05:35:20.512Z",
      closeDate: "2021-03-30T05:35:20.512Z",
      required: 0,
      contract: "string",
      location: "string",
      remuneration: 0,
      agency: "string",
      conLocationtract: "string",
      siteAddress: "string",
      anzscoCode: "string",
      skillLevel: "string",
      experinceRequired: "string",
      otherRequiremts: "string",
      policy: "string",
      visaLength: "string",
      comments: "string",
      laborMarketTestExpiry: "2021-03-30T05:35:20.512Z",
      laborMarketTestExpiry1: "2021-03-30T05:35:20.512Z",
      advertisingExpiry: "2021-03-30T05:35:20.512Z",
      skillMatesReportExpiry: "2021-03-30T05:35:20.512Z",
      createdBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    };

    onAddEmployerJob(data).then(() => {
      onGetEmployerJob(userDataEmp && userDataEmp.id);
      message.success("Successfully Added!");
    });
  };

  let config = {
    key:
      "YNB3fA3A7A8B6A4C3A-9UJHAEFZMUJOYGYQEa1c1ZJg1RAeF5C4C3G3E2C2A3D6B3E3==",
    height: "auto",
    toolbarSticky: false,
    events: {
      "charCounter.update": () => {
        // Do something here.
        // this is the editor instance.
        console.log("char");
      },
    },
  };

  return (
    <Fragment>
      <Form
        onFinish={onFinish}
        form={form}
        className="emp-content-box p-box add-job-form width-100"
        name="main"
      >
        <Row gutter={8} className="margin-top">
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Job Order No</p>
          </Col>
          <Col span={8}>
            <Form.Item name="job_no" required={true}>
              <Input placeholder="Job Order No" />
            </Form.Item>
          </Col>
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Job Tittle</p>
          </Col>
          <Col span={8}>
            <Form.Item name="job_tittle" required={true}>
              <Input placeholder="Job Tittle" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8} className="margin-top">
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Open Date</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={true}>
              <DatePicker onChange={onOpenDateChange} />
            </Form.Item>
          </Col>
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Close Date</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={false}>
              <DatePicker onChange={onChange} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8} className="margin-top">
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Position</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={true}>
              <Input placeholder="Position" />
            </Form.Item>
          </Col>
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Remuneration </p>
          </Col>
          <Col span={8}>
            <Form.Item>
              <InputNumber onChange={onNumberChange} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8} className="margin-top">
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Experience Required</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={false}>
              <Input placeholder="Position" />
            </Form.Item>
          </Col>
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Required</p>
          </Col>
          <Col span={8}>
            <Form.Item>
              <InputNumber onChange={onNumberChange} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8} className="margin-top">
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Other Requirements</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={false}>
              <Input placeholder="Position" />
            </Form.Item>
          </Col>
          <Col span={4}>
            <p style={{ fontSize: 13 }}>ANZSCO Code</p>
          </Col>
          <Col span={8}>
            <Form.Item>
              <Input placeholder="ANZSCO Code" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8} className="margin-top">
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Policy</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={false}>
              <Input placeholder="Policy" />
            </Form.Item>
          </Col>
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Contract</p>
          </Col>
          <Col span={8}>
            <Form.Item>
              <Input placeholder="Contract" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8} className="margin-top">
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Visa Length</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={false}>
              <Input placeholder="Visa Length" />
            </Form.Item>
          </Col>
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Location</p>
          </Col>
          <Col span={8}>
            <Form.Item>
              <Input placeholder="Location" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8} className="margin-top">
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Advertising Expiry</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={false}>
              <DatePicker onChange={onChange} />
            </Form.Item>
          </Col>
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Address</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={false}>
              <Input placeholder="Address" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8} className="margin-top">
          <Col span={4}>
            <p style={{ fontSize: 13 }}>LIA</p>
          </Col>
          <Col span={8}>
            <Form.Item>
              <Select>
                <Select.Option value="demo">Demo</Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Skill Match Report Expiry</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={false}>
              <Input placeholder="Address" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8} className="margin-top">
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Skill Level</p>
          </Col>
          <Col span={8}>
            <Form.Item name="how-many-years" required={false}>
              <DatePicker onChange={onChange} />
            </Form.Item>
          </Col>
          <Col span={4}>
            <p style={{ fontSize: 13 }}>Sales Person</p>
          </Col>
          <Col span={8}>
            <Form.Item>
              <Select>
                <Select.Option value="demo">Demo</Select.Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Form.Item className="margin-top-20" name="invitation_to">
          <div className="letter-froala emp-froala">
            <h4>Immigration Strategies</h4>
            <div className="letter-froala">
              <FroalaEditor
                ref={(el) => {
                  config = el;
                }}
                config={config}
                name="model_data"
                // model={letterString}
                // onModelChange={handleModelChange}
              />
            </div>

            <div className="document-checklist--btn">
              <Button
                // onClick={() => emailTypeData(data.emailType)}
                htmlType="submit"
                type="primary"
                className="button-blue"
              >
                Update
              </Button>
            </div>
          </div>
        </Form.Item>
      </Form>
    </Fragment>
  );
};
export default JobEmployerForm;
