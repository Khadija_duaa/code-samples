import React, { Fragment, useState, useEffect } from "react";
import { Tabs, Modal } from "antd";
import ProfileEmployer from "./ProfileEmployer";
import ContactEmployer from "./ContactEmployer/ContactEmployer";
import JobEmployer from "./JobEmployer/JobEmployer";
import ClientEmployer from "./ClientEmployer/ClientEmployer";

const EmployerManagementHead = ({
  ongetEmployerContact,
  EmployerContactRes,

  onAddEmployerContact,

  onGetEmployerJob,
  employerJobRes,

  onAddEmployerJob,

  userDataEmp,

  onRemoveEmployerContact,

  onUpdateEmployerManag,

  onUpdateEmployerContact,

  onGetTeamMember,
  teamMembers,

  onUpdateEmployerJob,

  onRemoveEmployerJob,

  onGetClientSearch,
  clientSearchRes,
  singleEmployeFuc,
  onGetClientEmp,
  clientEmpRes,

  onSearchClient,
  searchClientData,

  onGetClientByEmpId,
  empClientEmpRes,

  onLinkEmpWithClient,

  onAddEmployerJobHistory,

  onGetClientTag,
  clientTagRes,

  onGetProfileClientTag,
  getClientRes,

  onGetEmployerManag,
  employerManagRes,

  docWithNameRes,
  onAddDocWithName,

  onGetClientJobHis,
  clientJobHisRes,

  onGetSingleClientHis,
  singleJobHisRes
}) => {
  return (
    <Fragment>
      <div className="border-box-emp-manag emp-profile-box-employer">
        <h4 className="top-text heading-forms"
            style={{marginLeft: '40px !important', fontWeight: 600 , fontSize: "14px",
              fontFamily: "Poppins"
        }}>Profile</h4>
        <ProfileEmployer
          singleEmployeFuc={singleEmployeFuc}
          userDataEmp={userDataEmp}
          onUpdateEmployerManag={onUpdateEmployerManag}
          onGetEmployerManag={onGetEmployerManag}
          employerManagRes={employerManagRes}
          docWithNameRes={docWithNameRes}
          onAddDocWithName={onAddDocWithName}
        />

        <ContactEmployer
          ongetEmployerContact={ongetEmployerContact}
          EmployerContactRes={EmployerContactRes}
          onAddEmployerContact={onAddEmployerContact}
          userDataEmp={userDataEmp}
          onRemoveEmployerContact={onRemoveEmployerContact}
          onUpdateEmployerContact={onUpdateEmployerContact}
        />
        <JobEmployer
          onGetEmployerJob={onGetEmployerJob}
          employerJobRes={employerJobRes}
          onAddEmployerJob={onAddEmployerJob}
          userDataEmp={userDataEmp}
          onGetTeamMember={onGetTeamMember}
          teamMembers={teamMembers}
          onUpdateEmployerJob={onUpdateEmployerJob}
          onRemoveEmployerJob={onRemoveEmployerJob}
          onGetClientSearch={onGetClientSearch}
          clientSearchRes={clientSearchRes}
          onSearchClient={onSearchClient}
          searchClientData={searchClientData}
          onGetClientByEmpId={onGetClientByEmpId}
          empClientEmpRes={empClientEmpRes}
          onLinkEmpWithClient={onLinkEmpWithClient}
          onAddEmployerJobHistory={onAddEmployerJobHistory}
          onGetSingleClientHis={onGetSingleClientHis}
          singleJobHisRes={singleJobHisRes}
        />
        <ClientEmployer
          onGetClientEmp={onGetClientEmp}
          clientEmpRes={clientEmpRes}
          empClientEmpRes={empClientEmpRes}
          onLinkEmpWithClient={onLinkEmpWithClient}
          userDataEmp={userDataEmp}
          onGetClientByEmpId={onGetClientByEmpId}
          onGetClientTag={onGetClientTag}
          clientTagRes={clientTagRes}
          onGetProfileClientTag={onGetProfileClientTag}
          getClientRes={getClientRes}
          onGetClientJobHis={onGetClientJobHis}
          clientJobHisRes={clientJobHisRes}
        />
      </div>
    </Fragment>
  );
};
export default EmployerManagementHead;
