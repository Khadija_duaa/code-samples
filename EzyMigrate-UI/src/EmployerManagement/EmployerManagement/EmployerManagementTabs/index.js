import React, { useEffect, useState } from "react";
import { Spin, Tabs } from "antd";
import ProfileEmployer from "./ProfileEmployer/index";
import FileNotesMain from "./FileNotes";
import EmployerDocuments from "./EmployerDocuments/index";
import EmployerEmailTabs from "./EmployerEmail";
import EmployerCase from "./EmployerCase/EmployerCase";
import EmployerInvoices from "./EmployerInvoices/EmployerInvoices";
import EmployerCreditNotes from "./EmployerCreditNotes/EmployerCreditNotes";
import AddInvoice from "./EmployerInvoices/AddInvoice";
import { getCompany } from "../../../store/Actions";
import { useDispatch } from "react-redux";
import jwt_decode from "jwt-decode";

const { TabPane } = Tabs;

const EmployerManagementTabs = ({
  ongetEmployerContact,
  EmployerContactRes,

  onAddEmployerContact,

  onGetEmployerJob,
  employerJobRes,

  onAddEmployerJob,

  onGetEmployerFile,
  employerFileRes,

  onAddEmployerFile,

  onGetEmployerData,
  singleEmployerRes,
  singleEmployeFuc,
  userDataEmp,

  onRemoveEmployerContact,

  onGetLetterTemplates,
  LetterTemplatesRes,

  onRemoveEmployerFile,

  handleCancel,

  onUpdateEmployerManag,

  onUpdateEmployerFile,

  onUpdateEmployerContact,

  onGetEmployerDocument,
  employerDocumentRes,

  onAddEmployerDocument,

  onRemoveEmployerDocument,

  onUpdateEmployerDocument,

  onUploadAvatar,
  imageUploadSuccess,

  onGetDocumentChecklist,
  docChecklistRes,

  onGetEmployerCheckList,
  employerCheckListRes,

  onAddEmployerCheckList,

  location,
  history,

  onSendEmailLink,

  onAddEmployerEmail,

  onGetEmployerHistory,
  employerHistoryRes,

  onGetAutoEmailImport,
  emailImportRes,

  onAddAutoEmailImport,

  onAddDocumentTittle,

  onAddAnyTamplate,
  addAnyTemRes,

  onGetTeamMember,
  teamMembers,

  onDeleteCreateEmail,

  onGetDocumentDownload,
  onGetDocumentPdf,

  onGetFilteredType,
  documentFilterRes,

  onAddFilteredType,

  onUpdateEmployerJob,

  onRemoveEmployerJob,

  onGetClientSearch,
  clientSearchRes,

  onGetClientEmp,
  clientEmpRes,

  onSearchClient,
  searchClientData,

  onGetClientByEmpId,
  empClientEmpRes,

  onLinkEmpWithClient,

  onAddEmployerJobHistory,

  onGetClientTag,
  clientTagRes,

  onGetProfileClientTag,
  getClientRes,

  onGetEmployerManag,
  employerManagRes,

  docWithNameRes,
  onAddDocWithName,

  onGetClientJobHis,
  clientJobHisRes,

  onGetSingleClientHis,
  singleJobHisRes,

  onGetEmployerCase,
  onGetCaseDocument,
  onUpdateCaseHistory,
  onGetVisaFileNotes,
  visaFileNotesRes,
  onRemoveSubjectCase,
  onRemoveCaseStatus,
  employerCaseRes,
  onGetVisaTypeByCountry,
  onStartNewApplication,
  onGetVisaType,
  visaTypeData,

  visaStatusData,
  onGetVisaStatus,

  onUpdateCaseStatus,

  onGetSignature,
  signatureRes,

  onGetDocuments,
  documentRes,
  onGetImapForAll,
  imapForAllRes,
  onGetPdf,
  onAddEmailDocument,
}) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [addInvoice, setAddInvoice] = useState("");
  const [parentInvoiceId, setParentInvoiceId] = useState(0);
  const [childrenTotalAmount, setChildrenTotalAmount] = useState(0);
  const [parentTotalAmount, setParentTotalAmount] = useState(0);

  useEffect(() => {
    onGetEmployerData(userDataEmp && userDataEmp.id);
  }, []);

  useEffect(() => {
    setLoading(false);
  }, [loading]);

  useEffect(() => {
    dispatch(getCompany());
  }, [dispatch]);

  console.log(
    "userDataEmpuserDataEmpuserDataEmpuserDataEmp2313123123123123",
    userDataEmp
  );

  const onClickInvoicesTab = (key) => {
    if (key === "7") {
      setLoading(true);
      setAddInvoice("invoices");
    } else {
      setAddInvoice("");
    }
  };

  const getChildInfo = (
    parentInvoiceId,
    childrenTotalAmount,
    parentTotalAmount,
    addInvoice
  ) => {
    setParentInvoiceId(parentInvoiceId);
    setChildrenTotalAmount(childrenTotalAmount);
    setParentTotalAmount(parentTotalAmount);
    setAddInvoice(addInvoice);
  };

  return (
    <div style={{ display: "flex" }}>
      <div className="box-emp">
        <div className="employer-manag-tabs">
          <Tabs
            defaultActiveKey="1"
            type="card"
            size={"small"}
            className="mar-r employer-tabs-content"
            onChange={onClickInvoicesTab}
          >
            <TabPane tab="PROFILE" key="1" className="employer-tabs-content">
              <ProfileEmployer
                singleEmployeFuc={singleEmployeFuc}
                ongetEmployerContact={ongetEmployerContact}
                EmployerContactRes={EmployerContactRes}
                onAddEmployerContact={onAddEmployerContact}
                onGetEmployerJob={onGetEmployerJob}
                employerJobRes={employerJobRes}
                onAddEmployerJob={onAddEmployerJob}
                userDataEmp={userDataEmp}
                onRemoveEmployerContact={onRemoveEmployerContact}
                onUpdateEmployerManag={onUpdateEmployerManag}
                onUpdateEmployerContact={onUpdateEmployerContact}
                onGetTeamMember={onGetTeamMember}
                teamMembers={teamMembers}
                onUpdateEmployerJob={onUpdateEmployerJob}
                onRemoveEmployerJob={onRemoveEmployerJob}
                onGetClientSearch={onGetClientSearch}
                clientSearchRes={clientSearchRes}
                onGetClientEmp={onGetClientEmp}
                clientEmpRes={clientEmpRes}
                onSearchClient={onSearchClient}
                searchClientData={searchClientData}
                onGetClientByEmpId={onGetClientByEmpId}
                empClientEmpRes={empClientEmpRes}
                onLinkEmpWithClient={onLinkEmpWithClient}
                onAddEmployerJobHistory={onAddEmployerJobHistory}
                onGetClientTag={onGetClientTag}
                clientTagRes={clientTagRes}
                onGetProfileClientTag={onGetProfileClientTag}
                getClientRes={getClientRes}
                onGetEmployerManag={onGetEmployerManag}
                employerManagRes={employerManagRes}
                docWithNameRes={docWithNameRes}
                onAddDocWithName={onAddDocWithName}
                onGetClientJobHis={onGetClientJobHis}
                clientJobHisRes={clientJobHisRes}
                onGetSingleClientHis={onGetSingleClientHis}
                singleJobHisRes={singleJobHisRes}
              />
            </TabPane>
            <TabPane tab="FILE NOTES" key="2">
              <FileNotesMain
                onGetEmployerFile={onGetEmployerFile}
                employerFileRes={employerFileRes}
                onAddEmployerFile={onAddEmployerFile}
                onGetEmployerJob={onGetEmployerJob}
                employerJobRes={employerJobRes}
                onGetLetterTemplates={onGetLetterTemplates}
                LetterTemplatesRes={LetterTemplatesRes}
                userDataEmp={userDataEmp}
                onRemoveEmployerFile={onRemoveEmployerFile}
                handleCancel={handleCancel}
                onUpdateEmployerFile={onUpdateEmployerFile}
                onAddAnyTamplate={onAddAnyTamplate}
                addAnyTemRes={addAnyTemRes}
              />
            </TabPane>
            <TabPane tab="DOCUMENTS" key="3">
              <EmployerDocuments
                onGetEmployerDocument={onGetEmployerDocument}
                employerDocumentRes={employerDocumentRes}
                userDataEmp={userDataEmp}
                onAddEmployerDocument={onAddEmployerDocument}
                onRemoveEmployerDocument={onRemoveEmployerDocument}
                onUpdateEmployerDocument={onUpdateEmployerDocument}
                onUploadAvatar={onUploadAvatar}
                imageUploadSuccess={imageUploadSuccess}
                onGetDocumentChecklist={onGetDocumentChecklist}
                docChecklistRes={docChecklistRes}
                onGetEmployerCheckList={onGetEmployerCheckList}
                employerCheckListRes={employerCheckListRes}
                onAddEmployerCheckList={onAddEmployerCheckList}
                location={location}
                history={history}
                onSendEmailLink={onSendEmailLink}
                onAddDocumentTittle={onAddDocumentTittle}
                onGetDocumentDownload={onGetDocumentDownload}
                onGetDocumentPdf={onGetDocumentPdf}
                onGetFilteredType={onGetFilteredType}
                documentFilterRes={documentFilterRes}
                onAddFilteredType={onAddFilteredType}
              />
            </TabPane>
            <TabPane tab="EMAIL" key="4">
              {/* <div style={{height: "100vh"}}>
                <h3 style={{display: "flex", justifyContent: "center"}}>
                  Coming Soon!
                </h3>
              </div> */}
              <EmployerEmailTabs
                onGetEmployerDocument={onGetEmployerDocument}
                employerDocumentRes={employerDocumentRes}
                userDataEmp={userDataEmp}
                onAddEmployerDocument={onAddEmployerDocument}
                onRemoveEmployerDocument={onRemoveEmployerDocument}
                onUpdateEmployerDocument={onUpdateEmployerDocument}
                onUploadAvatar={onUploadAvatar}
                imageUploadSuccess={imageUploadSuccess}
                onGetDocumentChecklist={onGetDocumentChecklist}
                docChecklistRes={docChecklistRes}
                onGetEmployerCheckList={onGetEmployerCheckList}
                employerCheckListRes={employerCheckListRes}
                onAddEmployerCheckList={onAddEmployerCheckList}
                location={location}
                history={history}
                onSendEmailLink={onSendEmailLink}
                onGetLetterTemplates={onGetLetterTemplates}
                LetterTemplatesRes={LetterTemplatesRes}
                onAddEmployerEmail={onAddEmployerEmail}
                onGetEmployerHistory={onGetEmployerHistory}
                employerHistoryRes={employerHistoryRes}
                onGetAutoEmailImport={onGetAutoEmailImport}
                emailImportRes={emailImportRes}
                onAddAutoEmailImport={onAddAutoEmailImport}
                onAddAnyTamplate={onAddAnyTamplate}
                addAnyTemRes={addAnyTemRes}
                onDeleteCreateEmail={onDeleteCreateEmail}
                onGetSignature={onGetSignature}
                signatureRes={signatureRes}
                onGetDocuments={onGetDocuments}
                documentRes={documentRes}
                onGetImapForAll={onGetImapForAll}
                imapForAllRes={imapForAllRes}
                onGetPdf={onGetPdf}
                onGetDocumentDownload={onGetDocumentDownload}
                onAddEmailDocument={onAddEmailDocument}
              />
            </TabPane>
            <TabPane tab="CASE" key="5">
              <EmployerCase
                onGetEmployerCase={onGetEmployerCase}
                onGetCaseDocument={onGetCaseDocument}
                onUpdateCaseHistory={onUpdateCaseHistory}
                onGetVisaFileNotes={onGetVisaFileNotes}
                onGetDocumentDownload={onGetDocumentDownload}
                visaFileNotesRes={visaFileNotesRes}
                onRemoveSubjectCase={onRemoveSubjectCase}
                onRemoveCaseStatus={onRemoveCaseStatus}
                userDataEmp={userDataEmp}
                employerCaseRes={employerCaseRes}
                onGetVisaTypeByCountry={onGetVisaTypeByCountry}
                onStartNewApplication={onStartNewApplication}
                onGetVisaType={onGetVisaType}
                visaTypeData={visaTypeData}
                visaStatusData={visaStatusData}
                onGetVisaStatus={onGetVisaStatus}
                onUpdateCaseStatus={onUpdateCaseStatus}
              />
            </TabPane>
            <TabPane tab="REPORTS" key="6">
              <div style={{ height: "100vh" }}>
                <h3 style={{ display: "flex", justifyContent: "center" }}>
                  Coming Soon
                </h3>
              </div>
            </TabPane>
            {JSON.parse(
              decodeURIComponent(
                escape(
                  window.atob(localStorage.getItem("userSystemPermissions"))
                )
              )
            ).find((x) => x.role.toLowerCase() == "employer management account" || x.role.toLowerCase() == "supplier management account")
              .status == 1 ? (
              <TabPane tab="INVOICES" key="7">
                {/*<div style={{height: "100vh"}}>*/}
                {/*  <h3 style={{display: "flex", justifyContent: "center"}}>*/}
                {/*    Coming Soon!*/}
                {/*  </h3>*/}
                {/*</div>*/}
                {loading ? (
                  <div className={"spinner"}>
                    <Spin size="large" />
                  </div>
                ) : addInvoice === "invoices" ? (
                  <EmployerInvoices
                    userDataEmp={userDataEmp}
                    getChildInfo={getChildInfo}
                  />
                ) : null}
                {addInvoice === "add" ? (
                  <div className="border-box-emp-manag emp-profile-box-employer">
                    <AddInvoice
                      userDataEmp={userDataEmp}
                      parentInvoiceId={parentInvoiceId}
                      childrenTotalAmount={childrenTotalAmount}
                      parentTotalAmount={parentTotalAmount}
                      getChildInfo={getChildInfo}
                    />
                  </div>
                ) : null}
              </TabPane>
            ) : null}
            {JSON.parse(
              decodeURIComponent(
                escape(
                  window.atob(localStorage.getItem("userSystemPermissions"))
                )
              )
            ).find((x) => x.role.toLowerCase() == "employer management account" || x.role.toLowerCase() == "supplier management account")
              .status == 1 ? (
              <TabPane tab="CREDIT NOTES" key="8">
                <div style={{ height: "100vh" }}>
                  <h3 style={{ display: "flex", justifyContent: "center" }}>
                    Coming Soon
                  </h3>
                </div>
                {/*<EmployerCreditNotes />*/}
              </TabPane>
            ) : (
              ""
            )}
          </Tabs>
        </div>
      </div>
    </div>
  );
};

export default EmployerManagementTabs;
