import React, { Fragment, useState, useEffect } from "react";
import { Select, message, Button, DatePicker } from "antd";
import moment from "moment";
// import activityData from "../ClientActivity/ActivityData";

const { Option } = Select;

const dateFormat = "DD/MM/YYYY";

const StartEmployerVisa = ({
  visaTypeData,
  onStartNewApplication,
  newApplicationWith,
  onGetVisaCategory,
  visaCategoryData,
  onCloseModal,
  onGetEmployerCase,
  userDataEmp,
  //   onSetActiveKey,
}) => {
  const [visaTypeId, setVisaTypeId] = useState("");
  const [startDate, setStartDate] = useState("");
  const [category, setCategory] = useState(0);
  const [destination, setDestination] = useState(0);
  const [loading, setLoading] = useState(false);
  const [prevNewApplicationWith, setPrevNewApplicatonWith] = useState("");
  var visaTypeOptions = [];
  var categoryOptions = [];
  if (visaTypeData) {
    for (var i = 0; i < visaTypeData.items.length > 0; i++) {
      visaTypeOptions.push(
        <Option value={visaTypeData.items[i].id}>
          {visaTypeData.items[i].visaTypeName}
        </Option>
      );
    }
  }

  if (visaCategoryData) {
    for (var i = 0; i < visaCategoryData.items.length > 0; i++) {
      categoryOptions.push(
        <Option value={visaCategoryData.items[i].id}>
          {visaCategoryData.items[i].name}
        </Option>
      );
    }
  }

  if (newApplicationWith !== prevNewApplicationWith) {
    setPrevNewApplicatonWith(newApplicationWith);
    setVisaTypeId("");
    setStartDate("");
    setCategory(0);
    setDestination(0);
  }

  const onChangeVisaType = (value) => {
    console.log(`selected ${value}`);
    setVisaTypeId(value);
    if (newApplicationWith === "au") {
      onGetVisaCategory(value);
    }
  };

  const onChangeCategory = (value) => {
    console.log(`selected ${value}`);
    setCategory(value);
  };

  const onChangeDestination = (value) => {
    console.log(`selected ${value}`);
    setDestination(value);
  };

  const onChange = (date, dateString) => {
    console.log(date, dateString);
    setStartDate(date);
  };

  const onSubmit = () => {
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    setLoading(true);
    var userId = localStorage.getItem("userId");
    var visaType =
      visaTypeData && visaTypeData.items.find((obj) => obj.id === visaTypeId);
    if (visaTypeId === "") {
      message.error("Please select visa type!");
      setLoading(false);
    }
    let data = {
      subjectId: userDataEmp.id,
      branchVisaTypeId: visaTypeId,
      caseStatusId: 1,
      startDate: startDate || new Date(),
      createdBy: userId,
      isPaid: false,
      isSigned: false,
      casePriority: "Medium",
      country: "",
      expiryDate: new Date(),
      approveDate: new Date(),
      visaTypeName: visaType.visaTypeName,
      isCompleted: false,
      subCategory: parseInt(category),
      destination: parseInt(destination),
      applicationCountry: newApplicationWith === "au" ? 1 : 0,
    };
    onStartNewApplication(data)
      .then((res) => {
        message.success("Visa application created successfully.");
        onGetEmployerCase(userDataEmp.id);
        onCloseModal();
        // onSetActiveKey("1");
        // let userName = localStorage.getItem("userName");
        // var profileData = JSON.parse(localStorage.getItem("profileData"));
        // let myData = {
        //   clientName: profileData.fullName,
        //   logMessage:
        //     "Application with status Preparing " + " Added by " + userName,
        //   date: moment(new Date()).format("DD/MM/YYYY"),
        //   logType: "Client Visa",
        //   invoiceId: "0",
        // };
        // activityData(myData);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });
  };
  return (
    <Fragment>
      <div className="form-container" style={{ marginLeft: 0, marginRight: 0 }}>
        <div
          style={{
            paddingTop: 5,
            paddingLeft: 8,
            paddingRight: 8,
          }}
        >
          {/* <span className="visa-date-text">
            {"Visa Status Date: " + moment(new Date()).format("DD/MM/YYYY")}
          </span> */}
        </div>
        <div style={{ padding: 10 }}>
          <Select
            showSearch
            optionFilterProp="children"
            style={{ width: "100%" }}
            onChange={onChangeVisaType}
            placeholder="Select Visa Types"
            value={visaTypeId}
            onChange={(e) => onChangeVisaType(e)}
          >
            <Option value="">Please Select Visa</Option>
            <Option value={10042}>Employer accreditation</Option>
            <Option value={10070}>Employer Approval in Principal</Option>
          </Select>
        </div>
        {newApplicationWith === "au" && visaTypeId && (
          <div style={{ padding: 10 }}>
            <Select
              showSearch
              optionFilterProp="children"
              style={{ width: "100%" }}
              onChange={onChangeCategory}
              // value={selectedOption}
              // onChange={this.handleChange}
            >
              {categoryOptions}
            </Select>
          </div>
        )}
        {newApplicationWith === "au" && (
          <div style={{ padding: 10 }}>
            <Select
              showSearch
              optionFilterProp="children"
              style={{ width: "100%" }}
              onChange={onChangeDestination}
              // value={selectedOption}
              // onChange={this.handleChange}
            >
              <Option value="1">New South Wales</Option>
              <Option value="2">Queensland</Option>
              <Option value="6">South Australia</Option>
              <Option value="4">Tasmania</Option>
              <Option value="3">Victoria</Option>
              <Option value="5">Western Australia</Option>
            </Select>
          </div>
        )}
        <div style={{ padding: 10, paddingTop: 0 }}>
          <DatePicker
            value={startDate}
            onChange={onChange}
            format={dateFormat}
          />
        </div>

        <div
          className="button-blue-cont"
          style={{ width: "100%", display: "flex", justifyContent: "flex-end" }}
        >
          {/* <div
            className="button-blue"
            style={{ cursor: "pointer" }}
            onClick={onSubmit}
          > */}
          <Button
            loading={loading}
            style={{ color: "#FFFFFF" }}
            onClick={onSubmit}
            className="button-blue"
          >
            Save
          </Button>
          {/* <span style={{ color: "#FFFFFF" }}>Save</span> */}
          {/* </div> */}
        </div>
      </div>
    </Fragment>
  );
};

export default StartEmployerVisa;
