import React, { Fragment, useState } from "react";
import EmployerManagementTable from "./EmployerManagementTable";
import { Form, Input, Button, Select, Col, Row } from "antd";

const EmployerManagement = ({
  onGetEmployerManag,
  employerManagRes,
  expandedRowsEntriesKeys,
  onAddEmployerManag,
  showModal,
  employerTabble,
  setEmployerTabble,

  onDeleteEmployer,

  onSearchEmployer,

  onGetEmployerData,
  singleEmployerRes,

  singleEmployeFuc,

  userDataEmp,

  setLoading,
  loading,

  searchEmployerRes,

  userId,

  onGetLetterTemplates,

  onGetEmployerFile,
  employerFileRes,

  onAddEmployerFile,

  onGetEmployerJob,
  employerJobRes,
  LetterTemplatesRes,
  handleCancel,

  singleEmployeIcons,

  onUpdateEmployerFile,

  onRemoveEmployerFile,

  onAddEmployerContact,

  onAddAnyTamplate,

  handleSetDefault,
  setIsSearch,
  isSearch,

  onGetClientByEmpId,
  empClientEmpRes,
  onGetImapForAll,
  imapForAllRes,
}) => {
  let selectedBranchId = localStorage.getItem("selectedBranchId");
  const [isSelectType, setIsSelectType] = useState();
  const [isSelector, setIsSelector] = useState();

  const [form] = Form.useForm();
  const onSearch = (values) => {
    setIsSearch(true);
    setLoading(true);

    console.log("valuesvaluesvalues", values);
    const data = {
      branchId: selectedBranchId && selectedBranchId,
      name: values && values.name ? values.name : "",
      business:
        values && values.business_company ? values.business_company : "",
      city: values && values.city ? values.city : "",
      jobSector: isSelector && isSelector ? isSelector : "",
      occupation: values && values.occupation ? values.occupation : "",
      yearsOfBusiness: "",
      isPotential: false,
      employerType: isSelectType && isSelectType ? isSelectType : "",
      pageSize: 12,
      pageNumber: 0,
    };

    onSearchEmployer(data).then(() => {
      onGetEmployerManag(selectedBranchId && selectedBranchId);
      form.resetFields();
      setLoading(false);
    });
  };

  const handleChange = (value) => {
    console.log(`selected ${value}`);
    setIsSelectType(value);
  };

  const handleSectorChange = (value) => {
    console.log(`selected ${value}`);
    setIsSelector(value);
  };

  console.log("searchEmployerRessearchEmployerRes==>>", searchEmployerRes);

  return (
    <Fragment>
      <div>
        <div style={{ display: "flex" }}>
          <div className="client-tag">
            <div className="employer-manag">
              <div className="client-tag-form"></div>
              <div className="">
                <div className="client-tag-table ">
                  <Form
                    form={form}
                    onFinish={onSearch}
                    className="w-80-t"
                    // {...layout}
                    name="employer-form"
                    initialValues={{ remember: true }}
                    colon={false}
                  >
                    {" "}
                    <div className="reminder-set-form text-style margin-top-34">
                      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={8}>
                          <Form.Item
                            label="Name"
                            name="name"
                            rules={[
                              {
                                required: false,
                                message: "Required!",
                              },
                            ]}
                          >
                            <Input />
                          </Form.Item>
                        </Col>
                        <Col span={8}>
                          <Form.Item
                            label="Business/Company Name"
                            name="business_company"
                            rules={[
                              {
                                required: false,
                                message: "Required!",
                              },
                            ]}
                          >
                            <Input />
                          </Form.Item>
                        </Col>
                        <Col span={8}>
                          <Form.Item
                            label="Type"
                            className="d-block"
                            name="type"
                          >
                            <Select
                              defaultValue="Employers"
                              onChange={handleChange}
                            >
                              <Select.Option value="Employer">
                                Employers
                              </Select.Option>
                              <Select.Option value="Supplier">
                                Supplier
                              </Select.Option>
                              <Select.Option value="Recruiter">
                                Recruiters
                              </Select.Option>
                            </Select>
                          </Form.Item>
                        </Col>
                      </Row>
                    </div>
                    <div className="reminder-set-form text-style margin-top-12">
                      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={8}>
                          <Form.Item label="Job Sector" className="d-block">
                            <Select
                              defaultValue="Select job sector"
                              onChange={handleSectorChange}
                            >
                              <Select.Option value="Select job sector">
                                Select job sector
                              </Select.Option>
                              <Select.Option value="Administrative">
                                Administrative
                              </Select.Option>
                              <Select.Option value="AgricultureFarming">
                                Agriculture / Farming
                              </Select.Option>
                              <Select.Option value="Automotive">
                                Automotive
                              </Select.Option>
                              <Select.Option value="Construction">
                                Construction
                              </Select.Option>
                              <Select.Option value="Electrical">
                                Electrical
                              </Select.Option>
                              <Select.Option value="Engineer">
                                Engineer
                              </Select.Option>
                              <Select.Option value="Finance">
                                Finance
                              </Select.Option>
                              <Select.Option value="FMCG">FMCG</Select.Option>
                              <Select.Option value="Hospitality">
                                Hospitality
                              </Select.Option>
                              <Select.Option value="Human Resources">
                                HumanResources
                              </Select.Option>
                              <Select.Option value="Insurance">
                                Insurance
                              </Select.Option>
                              <Select.Option value="ICT">ICT</Select.Option>
                              <Select.Option value="Legal">Legal</Select.Option>
                              <Select.Option value="Marketing">
                                Marketing
                              </Select.Option>
                              <Select.Option value="Medical">
                                Medical
                              </Select.Option>
                              <Select.Option value="Real estate">
                                Realestate
                              </Select.Option>
                              <Select.Option value="Retail">
                                Retail
                              </Select.Option>
                              <Select.Option value="Sales">Sales</Select.Option>
                              <Select.Option value="Supply chain">
                                Supplychain
                              </Select.Option>
                              <Select.Option value="Teachers">
                                Teachers
                              </Select.Option>
                              <Select.Option value="Trades">
                                Trades
                              </Select.Option>
                              <Select.Option value="Not Employed">
                                NotEmployed
                              </Select.Option>
                            </Select>
                          </Form.Item>
                        </Col>
                        <Col span={8}>
                          <Form.Item
                            label="Occupation"
                            name="occupation"
                            rules={[
                              {
                                required: false,
                                message: "Required!",
                              },
                            ]}
                          >
                            <Input />
                          </Form.Item>
                        </Col>
                        <Col span={8}>
                          <Form.Item
                            label="City"
                            name="city"
                            rules={[
                              {
                                required: false,
                                message: "Required!",
                              },
                            ]}
                          >
                            <Input />
                          </Form.Item>
                        </Col>
                      </Row>
                    </div>
                    <div
                      style={{
                        marginBottom: 45,
                        zIndex: 2,
                        position: "relative",
                        marginTop: 22,
                      }}
                      className="reminder-set-form  d-flex float-right"
                    >
                      <Row gutter={12}>
                        <Col>
                          <Form.Item>
                            <Button
                              onClick={() => {
                                handleSetDefault();
                                form.resetFields();
                              }}
                              className="employer-btn button-blue"
                              type="primary"
                            >
                              Set Default
                            </Button>
                          </Form.Item>
                        </Col>
                        <Col>
                          <Form.Item>
                            <Button
                              className="employer-btn button-blue"
                              htmlType="submit"
                              type="primary"
                            >
                              Search
                            </Button>
                          </Form.Item>
                        </Col>
                        <Col>
                          <Form.Item>
                            <Button
                              onClick={() => showModal("add-new")}
                              className="green-btn employer-btn"
                              // htmlType="submit"
                              type="primary"
                              style={{ borderRadius: 5 }}
                            >
                              Add New
                            </Button>
                          </Form.Item>
                        </Col>
                      </Row>
                    </div>
                  </Form>
                  <EmployerManagementTable
                    _expandedRowsEntriesKeys={expandedRowsEntriesKeys}
                    onGetEmployerManag={onGetEmployerManag}
                    employerManagRes={employerManagRes}
                    employerTabble={employerTabble}
                    setEmployerTabble={setEmployerTabble}
                    onDeleteEmployer={onDeleteEmployer}
                    singleEmployeFuc={singleEmployeFuc}
                    userDataEmp={userDataEmp}
                    onGetEmployerData={onGetEmployerData}
                    singleEmployerRes={singleEmployerRes}
                    userId={userId}
                    onGetLetterTemplates={onGetLetterTemplates}
                    onGetEmployerFile={onGetEmployerFile}
                    employerFileRes={employerFileRes}
                    onAddEmployerFile={onAddEmployerFile}
                    onGetEmployerJob={onGetEmployerJob}
                    employerJobRes={employerJobRes}
                    LetterTemplatesRes={LetterTemplatesRes}
                    userDataEmp={userDataEmp}
                    userDataEmp={userDataEmp}
                    handleCancel={handleCancel}
                    singleEmployeIcons={singleEmployeIcons}
                    setLoading={setLoading}
                    loading={loading}
                    onUpdateEmployerFile={onUpdateEmployerFile}
                    onRemoveEmployerFile={onRemoveEmployerFile}
                    searchEmployerRes={searchEmployerRes}
                    isSearch={isSearch}
                    onAddAnyTamplate={onAddAnyTamplate}
                    onGetClientByEmpId={onGetClientByEmpId}
                    empClientEmpRes={empClientEmpRes}
                    onGetImapForAll={onGetImapForAll}
                    imapForAllRes={onGetImapForAll}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default EmployerManagement;
