import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LetterTemplates from "../AccountSetting/LetterTemplates/LetterTemplates";

import {
  getLetterTemplates,
  addLetterTemplates,
  removeLetterTemplates,
  updetaLetterTemplates,
  getDocumentDownload,
  sortLetterTemplates,
  setActiveInnerTab,
} from "../store/Actions";

const mapStateToProps = (state) => {
  return {
    LetterTemplatesRes: state.LetterTemplatesReducer.LetterTemplatesRes,
    addLetterTemplatesRes: state.LetterTemplatesReducer.addLetterTemplatesRes,
    removeLetterTemplatesRes:
      state.LetterTemplatesReducer.removeLetterTemplatesRes,
    updateLetterTemplatesRes:
      state.LetterTemplatesReducer.updateLetterTemplatesRes,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetLetterTemplates: bindActionCreators(getLetterTemplates, dispatch),
    onAddLetterTemplates: bindActionCreators(addLetterTemplates, dispatch),
    onRemoveLetterTemplates: bindActionCreators(
      removeLetterTemplates,
      dispatch
    ),
    onUpdetaLetterTemplates: bindActionCreators(
      updetaLetterTemplates,
      dispatch
    ),
    onGetDocumentDownload: bindActionCreators(getDocumentDownload, dispatch),
    onSortLetterTemplates: bindActionCreators(sortLetterTemplates, dispatch),
    onSetActiveInnerTab: bindActionCreators(setActiveInnerTab, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LetterTemplates);
