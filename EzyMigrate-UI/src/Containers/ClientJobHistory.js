import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Employer from "../Clients/Employer";

import {
  getEmployerManag,
  getClientJobHistory,
  addEmployerJobHistory,
  updEmployerJobHistory,
  removeEmployerJobHistory,
  onGetClientJobStatuses
} from "../store/Actions";

const mapStateToProps = state => {
  return {
    employerManagRes: state.employerManagReducer.employerManagRes,
    clientEmployerRes: state.clientEmployerReducer.clientEmployerRes,
    employerJobHistoryRes:
      state.EmployerJobHistoryReducer.employerJobHistoryRes,
    clientTab: state.employerManagReducer.clientTab
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetEmployerManag: bindActionCreators(getEmployerManag, dispatch),
    onGetEmployerJobHistory: bindActionCreators(getClientJobHistory, dispatch),
    onAddEmployerJobHistory: bindActionCreators(
      addEmployerJobHistory,
      dispatch
    ),
    onUpdEmployerJobHistory: bindActionCreators(
      updEmployerJobHistory,
      dispatch
    ),
    onRemoveEmployerJobHistory: bindActionCreators(
      removeEmployerJobHistory,
      dispatch
    ),

    onGetClientJobStatuses: bindActionCreators(onGetClientJobStatuses, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Employer);
