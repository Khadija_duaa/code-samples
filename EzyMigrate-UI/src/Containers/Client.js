import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Theme from "../Components/Theme";

import {
  searchMainClient,
  userLogout,
  getUpdatedBranch,
  searchPotentialClient,
  closeAndOpenSidebar,
  getPotentialClientInfo,
  getPotentialClientUpdateBol,
  getClientProfile,
  getUserDetail,
  removeNotificationToken,
  userActivityLog,
  getTeamMember,
  getUserBranch,
} from "../store/Actions";

const mapStateToProps = (state) => {
  return {
    searchClientData: state.clientProfileReducer.searchConnectionData,
    branchData: state.branchReducer.branchData,
    mainClientsData: state.clientProfileReducer.clientsData,
    potentialClientsData: state.clientProfileReducer.potentialClientsData,
    sideBarRes: state.employerManagReducer.sideBarRes,
    userDetail: state.userDetailReducer.userDetail,
    loader: state.clientProfileReducer && state.clientProfileReducer.loader,
    userActivityRes: state.userReducer.userActivityRes,
    teamMembers: state.teamMemberReducer.teamMembers,
    userBranchData: state.branchReducer.userBranchData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPotentialClientInfo: bindActionCreators(
      getPotentialClientInfo,
      dispatch
    ),
    getPotentialClientUpdateBol: bindActionCreators(
      getPotentialClientUpdateBol,
      dispatch
    ),
    onSearchClient: bindActionCreators(searchMainClient, dispatch),
    onSearchPotentialClient: bindActionCreators(
      searchPotentialClient,
      dispatch
    ),
    onUserLogout: bindActionCreators(userLogout, dispatch),
    onGetBranch: bindActionCreators(getUpdatedBranch, dispatch),
    onCloseAndOpenSidebar: bindActionCreators(closeAndOpenSidebar, dispatch),
    onGetClientProfile: bindActionCreators(getClientProfile, dispatch),
    onGetUserDetail: bindActionCreators(getUserDetail, dispatch),
    onNotificationRemove: bindActionCreators(removeNotificationToken, dispatch),
    onGetUserActivity: bindActionCreators(userActivityLog, dispatch),
    onGetTeamMember: bindActionCreators(getTeamMember, dispatch),
    onGetUserBranch: bindActionCreators(getUserBranch, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Theme);
