import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FileNotes from "../FileNotes/FileNotes";

import {
  addFileNotes,
  getFileNotes,
  updateFileNotes,
  deleteFileNotes,
  getTemplates,
  getDynamicKeys,
  getLetterTemplates,
  getVisaType,
  getUserBranch,
  userLogout,
  getClientTag,
  getProfileClientTag,
  addProfileClientTag,
  removeProfileClientTag,
  getVisaStatus,
  updateCaseStatus,
  getVisaApplication,
  sendDynamicLinkEmail,
  createClientLink,
  getClientLink,
  setCmvLinkTemplate,
  removeClient,
  getClientJobHistoryCurrent,
  setActiveKey,
  updateSignedDate,
  getClientSource,
  searchMainClient,
  checkUniqueEmail,
  updatePriority,
  signedVisa,
  paidVisa,
  getReminderTasks,
  updateReminder,
  addTaskComment,
  getTasksComments,
  getTasksFollowers,
  getClientTasks,
  updetaTasks,
  getUsers,
  addTaskFollower,
  removeTasks,
  addDailyTasks,
  getClientFamily,
  updateCompletedTask,
  addTaskFileNote,
  getClientProfile,
  getClientVisaFileNotes,
  getVisaTypeByCountry,
  getAdmissionStatuses,
  getAdmissionProgram,
} from "../store/Actions";
import { setClientTab } from "../store/Actions/EmployerManagement";

const mapStateToProps = (state) => {
  return {
    fileNotesData: state.fileNotesReducer.fileNotesData,
    templateData: state.fileNotesReducer.templateData,
    dynamicKeysRes: state.fileNotesReducer.dynamicKeysRes,
    clientTab: state.employerManagReducer.clientTab,
    LetterTemplatesRes: state.LetterTemplatesReducer.LetterTemplatesRes,
    visaAppData: state.clientVisaReducer.visaAppData,
    cmvLinkTemplateData: state.clientProfileReducer.cmvLinkTemplateData,
    removeClientRes: state.clientProfileReducer.removeClientRes,
    employerJobHistoryCurrentRes:
      state.EmployerJobHistoryReducer.employerJobHistoryCurrentRes,
    activeKey: state.employerManagReducer.activeKey,
    clientSourceListing:
      state &&
      state.allClientReducer &&
      state.allClientReducer.clientSourceListing &&
      state.allClientReducer.clientSourceListing.items &&
      state.allClientReducer.clientSourceListing.items,
    searchClientData: state.clientProfileReducer.clientsData,
    uniqueEmailRes: state.clientProfileReducer.uniqueEmailRes,
    clientTab: state.employerManagReducer.clientTab,
    visaPriorityRes: state.visaTypeReducer.visaPriorityRes,
    remindersRes: state.reminderReducer.reminderTaskRes,
    updReminderRes: state.reminderReducer.updReminderRes,
    clientProfileData: state.clientProfileReducer.clientProfileData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAddFileNotes: bindActionCreators(addFileNotes, dispatch),
    onGetFileNotes: bindActionCreators(getFileNotes, dispatch),
    onUpdateFileNotes: bindActionCreators(updateFileNotes, dispatch),
    onDeleteFileNotes: bindActionCreators(deleteFileNotes, dispatch),
    onGetTemplates: bindActionCreators(getTemplates, dispatch),
    onGetDynamicKeys: bindActionCreators(getDynamicKeys, dispatch),
    onGetLetterTemplates: bindActionCreators(getLetterTemplates, dispatch),
    onGetVisaStatus: bindActionCreators(getVisaStatus, dispatch),
    onUpdateCaseStatus: bindActionCreators(updateCaseStatus, dispatch),
    onGetVisaApplication: bindActionCreators(getVisaApplication, dispatch),
    onSendEmailLink: bindActionCreators(sendDynamicLinkEmail, dispatch),
    onCreateClientLink: bindActionCreators(createClientLink, dispatch),
    onGetClientLink: bindActionCreators(getClientLink, dispatch),
    onSetCmvLinkTemplate: bindActionCreators(setCmvLinkTemplate, dispatch),
    onGetClientJobHistoryCurrent: bindActionCreators(
      getClientJobHistoryCurrent,
      dispatch
    ),
    onSetActiveKey: bindActionCreators(setActiveKey, dispatch),
    onUpdateSignedDate: bindActionCreators(updateSignedDate, dispatch),
    getClientSource: bindActionCreators(getClientSource, dispatch),
    onSearchClient: bindActionCreators(searchMainClient, dispatch),
    onCheckUniqueEmail: bindActionCreators(checkUniqueEmail, dispatch),
    onSetClientTab: bindActionCreators(setClientTab, dispatch),
    onUpdatePriority: bindActionCreators(updatePriority, dispatch),
    onSignedVisa: bindActionCreators(signedVisa, dispatch),
    onPaidVisa: bindActionCreators(paidVisa, dispatch),
    onGetReminder: bindActionCreators(getReminderTasks, dispatch),
    onUpdateReminder: bindActionCreators(updateReminder, dispatch),
    onAddTaskComment: bindActionCreators(addTaskComment, dispatch),
    onGetTaskComments: bindActionCreators(getTasksComments, dispatch),
    onGetTaskFollowers: bindActionCreators(getTasksFollowers, dispatch),
    onGetClientTask: bindActionCreators(getClientTasks, dispatch),
    onUpdateTask: bindActionCreators(updetaTasks, dispatch),
    onGetAllUsers: bindActionCreators(getUsers, dispatch),
    onAddTaskFollower: bindActionCreators(addTaskFollower, dispatch),
    onRemoveTasks: bindActionCreators(removeTasks, dispatch),
    onAddDailyTasks: bindActionCreators(addDailyTasks, dispatch),
    onGetClientFamily: bindActionCreators(getClientFamily, dispatch),
    onUpdateCompletedTask: bindActionCreators(updateCompletedTask, dispatch),
    onAddTaskFileNote: bindActionCreators(addTaskFileNote, dispatch),
    onGetVisaType: bindActionCreators(getVisaType, dispatch),
    onGetClientTag: bindActionCreators(getClientTag, dispatch),
    onGetProfileClientTag: bindActionCreators(getProfileClientTag, dispatch),
    onAddProfileClientTag: bindActionCreators(addProfileClientTag, dispatch),
    onRemoveProfileClientTag: bindActionCreators(
      removeProfileClientTag,
      dispatch
    ),
    onGetClientProfile: bindActionCreators(getClientProfile, dispatch),
    onGetVisaTypeByCountry: bindActionCreators(getVisaTypeByCountry, dispatch),
    onGetAdmissionProgram: bindActionCreators(getAdmissionProgram, dispatch),
    onGetAdmissionStatuses: bindActionCreators(getAdmissionStatuses, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FileNotes);
