import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DashboardPotentialClient from "./../Dashboard/PotenialClient";

import {
  getDashboardPotentialClient,
  getProcessingPersons,
  addCaseTask,
  getClientFile,
  addClientFile,
  getPotentialClientPendingTask,
  getPotentialClientInfo,
  getPotentialClientUpdateBol
} from "../store/Actions";

const mapStateToProps = (state) => {
  return {
    processingPersons: state.dashboardPotentialClientReducer.processingPersons,
    potentialClientList:state.dashboardPotentialClientReducer.potentialClientList,
    potentialClientError:state.dashboardPotentialClientReducer.potentialClientError,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetDashboardPotentialClients: bindActionCreators(getDashboardPotentialClient, dispatch),
    onGetProcessingPersons: bindActionCreators(getProcessingPersons, dispatch),
    onGetCaseTask: bindActionCreators(getPotentialClientPendingTask, dispatch),
    onAddTask: bindActionCreators(addCaseTask, dispatch),
    onGetClientFile: bindActionCreators(getClientFile, dispatch),
    onAddClientFile: bindActionCreators(addClientFile, dispatch),
    onGetPotentialClient: bindActionCreators(getPotentialClientInfo, dispatch),
    onGetUpdateBol: bindActionCreators(getPotentialClientUpdateBol, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardPotentialClient);
