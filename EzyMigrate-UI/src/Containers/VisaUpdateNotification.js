import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import VisaUpdateNotification from "../AccountSetting/VisaUpdateNotification/VisaUpdateNotification";

import { setActiveInnerTab, updateVisaNotification } from "../store/Actions";

const mapStateToProps = (state) => {
  return {
    visaNotificationRes: state.visaNotificationReducer.visaNotificationRes,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onUpdateVisaNotification: bindActionCreators(
      updateVisaNotification,
      dispatch
    ),
    onSetActiveInnerTab: bindActionCreators(setActiveInnerTab, dispatch),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VisaUpdateNotification);
