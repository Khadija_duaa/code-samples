import User from "./User";
import UserDetail from "./UserDetail";
import TeamMember from "./TeamMember";
import BranchSetting from "./Branch";
import VisaStatus from "./VisaStatus";
import VisaType from "./VisaType";
import PotentialClientStatus from "./PotentialClientStatus";
import CustomQuestionnaire from "./Questionnaire";
import QuestionnaireList from "./QuestionnaireList";
import PreviewQuestionnaire from "./PreviewQuestionnaire";
import SubmitQuestionnaire from "./SubmitQuestionnaire";
import UpdateQuestionnaire from "./UpdateQuestionnaire";
import Profile from "./ClientProfile";
import Partner from "./Partner";
import Qualification from "./ClientQualification";
import FileNotes from "./FileNotes";
import AllClients from "./AllClients";
import ClientQuestionnaire from "./ClientQuestionnaire";
import LinkQuestionnaire from "./LinkQuestionnaire";
import VisaFlow from "./ClientVisa";
import Admission from "./ClientAdmission";
import Theme from "./Client";
import Accounts from "./Accounts";
import ClientList from "./ClientList";
import HeaderBarTop from "./HeaderBar";
import DocumentSync from "./DocumentSync";
import Employer from "./ClientEmployer";
import JobHistory from "./EmployerJobHistory";
import CaseManagement from "./CaseManagement";
import FamilyMember from "./FamilyMember";
import Activities from "./Activities";
import ChecklistCategory from "./ChecklistCategory";
import AddChecklist from "./AddChecklist";
import Chats from "./Chats";
import AdminLogin from "./AdminLogin";
import Companies from "./Companies";
import BranchDetail from "./BranchDetail";
import PotentialEmployer from "./PotentialEmployerManagement";
import Reports from "./Reports";
import EmployerReports from "./EmployerReports";
import ClientSummary from "./ClientSummary";
import PotentialClientReport from "./PotentialClientReport";
import VisaReport from "./VisaReports";
import DashboardPotentialClientReport from "./DashboardPotentialClient";
import Mail from "./Mail";
import DashboardReminders from "./DashboardReminders";
import DashboardEmployers from "./DashboardEmployers";
import DashboardStudent from "./DashboardStudent";
import DashboardBi from "./DashboardBi";
import PCleintCustomQuestionnaire from "./PQuestionnaire";
import CalendarSync from "./CalendarSync";
import TeamMemberSettings from "./TeamMemberSettings";
import AddBranch from "./AddBranch";
import OrganizationSetting from "./OrganizationSetting";
import CmvFAQs from "./CmvFaqs";
import OtherInfo from "./ClientOtherInfo";
import OutlookIntegration from "./OutlookIntegration";
import ForgotPassword from "./ForgotPassword";
import SupervisorLogin from "./SupervisorLogin";
import UpdateMappProperty from "./UpdateMappProperty";

import Supervisor from "./Supervisor";
import SupervisorBatchesStudents from "./SupervisorBatchesStudents";
export {
  User,
  UserDetail,
  TeamMember,
  BranchSetting,
  VisaStatus,
  VisaType,
  PotentialClientStatus,
  CustomQuestionnaire,
  QuestionnaireList,
  Profile,
  Partner,
  PreviewQuestionnaire,
  Qualification,
  FileNotes,
  SubmitQuestionnaire,
  UpdateQuestionnaire,
  AllClients,
  ClientQuestionnaire,
  LinkQuestionnaire,
  VisaFlow,
  Admission,
  Theme,
  Accounts,
  ClientList,
  HeaderBarTop,
  DocumentSync,
  Employer,
  JobHistory,
  CaseManagement,
  FamilyMember,
  Activities,
  ChecklistCategory,
  AddChecklist,
  Chats,
  AdminLogin,
  Companies,
  BranchDetail,
  PotentialEmployer,
  Reports,
  EmployerReports,
  ClientSummary,
  PotentialClientReport,
  VisaReport,
  DashboardPotentialClientReport,
  Mail,
  DashboardReminders,
  DashboardEmployers,
  DashboardStudent,
  DashboardBi,
  PCleintCustomQuestionnaire,
  CalendarSync,
  TeamMemberSettings,
  AddBranch,
  OrganizationSetting,
  CmvFAQs,
  OtherInfo,
  OutlookIntegration,
  ForgotPassword,
  SupervisorLogin,
  UpdateMappProperty,
  Supervisor,
  SupervisorBatchesStudents,
};
