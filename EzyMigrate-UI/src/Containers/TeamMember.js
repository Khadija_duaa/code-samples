import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import TeamMember from "../SuperUserSetting/AddTeamMember";

import {
  addTeamMember,
  addUserPassword,
  getBranch,
  updateManager,
  changeUsersPassword,
  disableUser,
  uploadAvatar,
  assignBranch,
  unassignBranch,
  getStorage,
  exportStorage,
  getTeamMember,
} from "../store/Actions";

const mapStateToProps = (state) => {
  return {
    teamMembers: state.teamMemberReducer.teamMembers,
    teamMemberError: state.teamMemberReducer.teamMemberError,
    addTeamMemberSuccess: state.teamMemberReducer.addTeamMemberSuccess,
    branchData: state.branchReducer.branchData,
    updManagerSuccess: state.managerReducer.updManagerSuccess,
    imageUploadSuccess: state.userDetailReducer.imageUploadSuccess,
    branchData: state.branchReducer.branchData,
    storageData: state.userDetailReducer.storageData,
    exportSuccess: state.userDetailReducer.exportSuccess,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetTeamMember: bindActionCreators(getTeamMember, dispatch),
    onAddTeamMember: bindActionCreators(addTeamMember, dispatch),
    onAddPassword: bindActionCreators(addUserPassword, dispatch),
    onGetBranch: bindActionCreators(getBranch, dispatch),
    onUpdateManager: bindActionCreators(updateManager, dispatch),
    onChangeUsersPassword: bindActionCreators(changeUsersPassword, dispatch),
    onDisableUser: bindActionCreators(disableUser, dispatch),
    onUploadAvatar: bindActionCreators(uploadAvatar, dispatch),
    onAssignBranch: bindActionCreators(assignBranch, dispatch),
    onUnassignBranch: bindActionCreators(unassignBranch, dispatch),
    onGetStorage: bindActionCreators(getStorage, dispatch),
    onExportData: bindActionCreators(exportStorage, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamMember);
