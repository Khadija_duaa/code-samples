import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ProfileSidebar from "./../Profile/Profile";

import { getProfileClientTag } from "../store/Actions";

const mapStateToProps = state => {
  return {
    getClientRes: state.allClientReducer.getClientRes
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetProfileClientTag: bindActionCreators(getProfileClientTag, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSidebar);
