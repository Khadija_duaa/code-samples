import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ProfileSidebar from "./../../src/Components/ProfileSideBar";

import { getClientTag, addClientTag, removeClientTag } from "../store/Actions";

const mapStateToProps = state => {
  return {
    clientTagRes: state.accountSetReducer.clientTagRes
    // addClientTagRes: state.accountSetReducer.addClientTagRes,
    // removeClientTagRes: state.accountSetReducer.removeClientTagRes,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetClientTag: bindActionCreators(getClientTag, dispatch)
    // onAddClientTag: bindActionCreators(addClientTag, dispatch),
    // onRemoveClientTag: bindActionCreators(removeClientTag, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSidebar);
