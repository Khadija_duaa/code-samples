import React from "react";
import {
  Col,
  Row,
  Select,
  DatePicker,
  Button,
  message,
  Form,
  Table,
  Modal,
  Spin,
  Input,
  InputNumber,
} from "antd";
import { bindActionCreators } from "redux";
import { addTax, editTax, getAllTaxesListing } from "../store/Actions";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

const layoutSendingReceipt = {
  labelCol: { span: 7, offset: 2 },
  wrapperCol: { span: 14, offset: 1 },
};

class AddTax extends React.Component {
  formRef = React.createRef();
  constructor(props) {
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    super(props);
    this.state = {
      name: "",
      percent: 0,
      number: "",
      branchId: selectedBranchId,
      isButtonDisabled: false,
    };
  }

  componentWillMount() {
    let _taxInfo = this.props && this.props.taxInfo;

    if (_taxInfo && _taxInfo) {
      this.setState(
        {
          id: _taxInfo.id,
          name: _taxInfo.name,
          percent: _taxInfo.percent,
          number: _taxInfo.number,
        },
        () => {
          this.formRef.current.setFieldsValue(
            {
              name: _taxInfo.name,
              percent: _taxInfo.percent,
              number: _taxInfo.number,
            },
            () => {}
          );
        }
      );
    }
  }

  componentDidMount() {}

  onFinish = (values) => {
    let _name = values.name;
    let _percent = values.percent;
    let _number = values.number.toString();

    this.setState(
      {
        name: _name,
        percent: _percent,
        number: _number,
      },
      () => {
        if (this.props && this.props.taxInfo && this.props.taxInfo.id) {
          this.setState({
            isButtonDisabled: false,
          });
          this.props
            .editTax(this.state)
            .then(() => {
              message.success("Tax is updated successfully!");
              this.props.getAllTaxesListing(this.state.branchId);
              this.props.handleCancelAddTaxModal();
            })
            .catch(() => {
              this.setState({
                isButtonDisabled: false,
              });
            });
        } else {
          this.setState({
            isButtonDisabled: true,
          });
          this.props
            .addTax(this.state)
            .then(() => {
              message.success("Tax is added successfully!");
              this.props.getAllTaxesListing(this.state.branchId);
              this.props.handleCancelAddTaxModal();
            })
            .catch(() => {
              this.setState({
                isButtonDisabled: false,
              });
            });
        }
      }
    );
  };

  getAddTaxForm = (props) => {
    return (
      <div className="border-box-modal-sending-receipt add-employer-para">
        <Form.Item
          colon={false}
          labelAlign="left"
          label="Name"
          name="name"
          rules={[
            { required: true, message: "Required!" },
            // ({ getFieldValue }) => ({
            //   validator(rule, value) {
            //     if (value.length > 20) {
            //
            //       return Promise.reject("Character limit exceeded");
            //     } else {
            //       return Promise.resolve();
            //     }
            //   }
            // })
          ]}
        >
          <Input size="medium" style={{ width: "-webkit-fill-available" }} />
        </Form.Item>
        <Form.Item
          colon={false}
          labelAlign="left"
          label="Number"
          name="number"
          rules={[
            { required: true, message: "Required!" },
            // ({ getFieldValue }) => ({
            //   validator(rule, value) {
            //     if (Math.ceil(Math.log10(value + 1)) > 10) {
            //
            //       return Promise.reject("Character limit exceeded");
            //     } else {
            //       return Promise.resolve();
            //     }
            //   }
            // }),
            // ({ getFieldValue }) => ({
            //   validator(rule, value) {
            //     if (value < 0) {
            //       return Promise.reject("Type only positive numbers");
            //     } else {
            //       return Promise.resolve();
            //     }
            //   }
            // })
          ]}
        >
          <Input size="medium" style={{ width: "-webkit-fill-available" }} />
        </Form.Item>
        <Form.Item
          colon={false}
          labelAlign="left"
          label="Percentage"
          name="percent"
          rules={[
            { required: true, message: "Required!" },
            //
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (Math.ceil(Math.log10(value + 1)) > 11) {
                  return Promise.reject("Character limit exceeded");
                } else {
                  return Promise.resolve();
                }
              },
            }),
            // ({ getFieldValue }) => ({
            //   validator(rule, value) {
            //     if (value < 0) {
            //       return Promise.reject("Type only positive numbers");
            //     } else {
            //       return Promise.resolve();
            //     }
            //   }
            // })
          ]}
        >
          <InputNumber
            size="medium"
            style={{ width: "-webkit-fill-available" }}
          />
        </Form.Item>
      </div>
    );
  };

  render() {
    return (
      <div className="reminder-model">
        <Modal
          className="reminder-model-main width-modal-outgoing-payments"
          title="Tax"
          visible={this.props.visibleAddTaxModal}
          onCancel={this.props.handleCancelAddTaxModal}
          footer={null}
          maskClosable={false}
        >
          <Form
            {...layoutSendingReceipt}
            onFinish={this.onFinish}
            ref={this.formRef}
          >
            {this.getAddTaxForm(this.props)}
            <Row>
              <Col xs={4} offset={18} style={{ display: "flex" }}>
                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    style={{ marginRight: "10px" }}
                    className={"button-blue"}
                    disabled={this.state.isButtonDisabled}
                  >
                    Save
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button
                    type="primary"
                    className={"button-blue"}
                    onClick={this.props.handleCancelAddTaxModal}
                  >
                    Close
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  let a =
    state &&
    state.invoicesReducer &&
    state.invoicesReducer.taxInfo &&
    state.invoicesReducer.taxInfo;

  return {
    taxInfo:
      state &&
      state.invoicesReducer &&
      state.invoicesReducer.taxInfo &&
      state.invoicesReducer.taxInfo,
  };
};

const mapDispatchToProps = (dispatch) => ({
  addTax: bindActionCreators(addTax, dispatch),
  editTax: bindActionCreators(editTax, dispatch),
  getAllTaxesListing: bindActionCreators(getAllTaxesListing, dispatch),
});

AddTax = connect(mapStateToProps, mapDispatchToProps)(AddTax);

export default withRouter(AddTax);
