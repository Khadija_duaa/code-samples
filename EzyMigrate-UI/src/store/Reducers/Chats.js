import * as types from "../Constants";
import { openNotificationWithIcon } from "../../Common/reUseFunction";

export const chatReducer = (state = {}, action) => {
  switch (action.type) {
    case types.CHATS_DISPLAY_SUCCESS:
      return { ...state, chatData: action.payload };
    case types.CHATS_DISPLAY_FAILURE:
       
      return { ...state, chatData: [] };
    case types.GET_CLIENTS_SUCCESS:
      return { ...state, clientData: action.payload };
    case types.GET_CLIENTS_FAILURE:
       
      return { ...state, clientData: [] };
    case types.GET_USERS_SUCCESS:
      return { ...state, userData: action.payload };
    case types.GET_USERS_FAILURE:
       
      return { ...state, userData: [] };
    case types.ADD_CHAT_THREAD_SUCCESS:
      return { ...state, addThreadRes: action.payload };
    case types.ADD_CHAT_THREAD_FAILURE:
       
      return { ...state, addThreadRes: null };
    case types.GET_CHAT_MESSAGES_SUCCESS:
      return { ...state, messageData: action.payload };
    case types.GET_CHAT_MESSAGES_FAILURE:
       
      return { ...state, messageData: [] };
    case types.ADD_THREAD_MESSAGE_SUCCESS:
      return { ...state, addMessageRes: action.payload };
    case types.ADD_THREAD_MESSAGE_FAILURE:
       
      return { ...state, addMessageRes: null };
    case types.ADD_CHAT_MEMBER_SUCCESS:
      return { ...state, addMemberRes: action.payload };
    case types.ADD_CHAT_MEMBER_FAILURE:
       
      return { ...state, addMemberRes: null };
    case types.ADD_CHAT_FILENOTES_SUCCESS:
      return { ...state, addFileNoteRes: action.payload };
    case types.ADD_CHAT_FILENOTES_FAILURE:
       
      return { ...state, addFileNoteRes: null };
    default:
      return state;
  }
};
