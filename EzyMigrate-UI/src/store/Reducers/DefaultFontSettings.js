import * as types from "../Constants";
import { openNotificationWithIcon } from "../../Common/reUseFunction";

export const defaultFontReducer = (state = {}, action) => {
  switch (action.type) {
    case types.GET_DETAULT_FONT_SUCCESS:
      return { ...state, defaultFontRes: action.payload };
    case types.GET_DETAULT_FONT_FAILURE:
       
      if (action.payload.response === "undefined") {
        window.location.assign("/login");
      }

    case types.PUT_DETAULT_FONT_SUCCESS:
      return { ...state, addDefaultFontRes: action.payload };
    case types.PUT_DETAULT_FONT_FAILURE:
       
      if (action.payload.response === "undefined") {
        window.location.assign("/login");
      }

    default:
      return state;
  }
};
