import * as types from "../Constants";
import { openNotificationWithIcon } from "../../Common/reUseFunction";

export const timeTrackingReducer = (state = {}, action) => {
  switch (action.type) {
    case types.GET_TIME_TRACKINGS_SUCCESS:
      return { ...state, timeTrackingRes: action.payload };
    case types.GET_TIME_TRACKINGS_FAILURE:
       
      if (action.payload.response === "undefined") {
        window.location.assign("/login");
      }

    case types.ADD_TIME_TRACKINGS_SUCCESS:
      return { ...state, addTimeTrackingRes: action.payload };
    case types.ADD_TIME_TRACKINGS_FAILURE:
       
      if (action.payload.response === "undefined") {
        window.location.assign("/login");
      }

    default:
      return state;
  }
};
