import * as types from "../Constants";
import { openNotificationWithIcon } from "../../Common/reUseFunction";

export const mailChimpReducer = (state = {}, action) => {
  switch (action.type) {
    case types.GET_MAIL_CHIMP_SUCCESS:
      return { ...state, mailChimpRes: action.payload };
    case types.GET_MAIL_CHIMP_FAILURE:
       
      if (action.payload.response === "undefined") {
        window.location.assign("/login");
      }

    case types.ADD_MAIL_CHIMP_FAILURE:
      return { ...state, addMailChimpRes: action.payload };
    case types.ADD_MAIL_CHIMP_FAILURE:
       
      if (action.payload.response === "undefined") {
        window.location.assign("/login");
      }

    default:
      return state;
  }
};
