import * as types from "../Constants";
import { openNotificationWithIcon } from "../../Common/reUseFunction";

export const dashboardRemindersReducer = (state = {}, action) => {
  switch (action.type) {
    case types.GET_DASHBOARD_REMINDERS_SUCCESS:
      return { ...state, dashboardRemindersList: action.payload };
    case types.GET_DASHBOARD_REMINDERS_FAILURE:
      openNotificationWithIcon("error", "Error", action.payload.message);
      return { ...state, dashboardRemindersError: action.payload };
    case types.GET_DASHBOARD_REMINDERS_TYPE_SUCCESS:
      return { ...state, remindersTypes: action.payload };
    case types.GET_DASHBOARD_REMINDERS_TYPE_FAILURE:
      return { ...state, remindersTypesError: action.payload };
    default:
      return state;
  }
};
