import * as types from "../Constants";
import { openNotificationWithIcon } from "../../Common/reUseFunction";

export const timeZoneReducer = (state = {}, action) => {
  switch (action.type) {
    case types.GET_TIME_ZONE_SUCCESS:
      return { ...state, timeZoneRes: action.payload };
    case types.GET_TIME_ZONE_FAILURE:
       
      if (action.payload.response === "undefined") {
        window.location.assign("/login");
      }

    case types.PUT_TIME_ZONE_SUCCESS:
      return { ...state, updateTimeZoneRes: action.payload };
    case types.PUT_TIME_ZONE_FAILURE:
       
      if (action.payload.response === "undefined") {
        window.location.assign("/login");
      }

    default:
      return state;
  }
};
