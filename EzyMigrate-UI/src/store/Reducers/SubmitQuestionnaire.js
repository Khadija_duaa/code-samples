import * as types from "../Constants";

export const submitQuestionnaireReducer = (state = {}, action) => {
  switch (action.type) {
    case types.SUBMIT_QUESTIONNAIRE_SUCCESS:
      return { ...state, subQuesSuccess: action.payload };
    case types.SUBMIT_QUESTIONNAIRE_FAILURE:
      return { ...state, subQuesFailure: action.payload };

    default:
      return state;
  }
};
