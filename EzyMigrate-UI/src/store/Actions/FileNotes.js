import { api } from "../../services/api";
import * as types from "../Constants";

export const addFileNotes = (data) => {
  const options = {
    url: "v1/client/filenote",
  };

  options.types = [types.ADD_FILE_NOTES_SUCCESS, types.ADD_FILE_NOTES_FAILURE];

  return api.post(options, data);
};

export const getFileNotes = (data) => {
  let clientprofileid = JSON.parse(
    window.localStorage.getItem("clientprofileid")
  );
  let options = null;
  if (data == null || data == 0) {
    options = {
      url: "v1/client/filenote/All/" + clientprofileid,
    };
  } else {
    options = {
      url: "v1/client/filenote/All/Visa/" + clientprofileid + "/" + data,
    };
  }

  options.types = [types.GET_FILE_NOTES_SUCCESS, types.GET_FILE_NOTES_FAILURE];

  return api.get(options);
};

export const updateFileNotes = (data) => {
  const options = {
    url: "v1/client/filenote",
  };

  options.types = [
    types.UPDATE_FILE_NOTES_SUCCESS,
    types.UPDATE_FILE_NOTES_FAILURE,
  ];

  return api.put(options, data);
};

export const deleteFileNotes = (data) => {
  const options = {
    url: "v1/client/filenote",
  };

  options.types = [
    types.DELETE_FILE_NOTES_SUCCESS,
    types.DELETE_FILE_NOTES_FAILURE,
  ];

  return api.delete(options, data);
};

export const getTemplates = () => {
  var branchId = localStorage.getItem("selectedBranchId");
  const options = {
    url: "v1/template/All/" + branchId,
  };

  options.types = [types.GET_TEMPLATE_SUCCESS, types.GET_TEMPLATE_FAILURE];

  return api.get(options);
};

export const getDynamicKeys = () => {
  const options = {
    url: "v1/config/DynamicKeys/All",
  };

  options.types = [
    types.GET_DYNAMIC_KEYS_SUCCESS,
    types.GET_DYNAMIC_KEYS_FAILURE,
  ];

  return api.get(options);
};

export const getClientVisaFileNotes = (data) => {
  const options = {
    url: "v1/client/filenote/All/Visa/" + data.clientId + "/" + data.caseId,
  };

  options.types = [
    types.GET_VISA_FILE_NOTES_SUCCESS,
    types.GET_VISA_FILE_NOTES_FAILURE,
  ];

  return api.get(options);
};
