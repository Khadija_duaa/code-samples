import { api } from "../../services/api";
import * as types from "../Constants";

export const getTimeTracking = () => {
  const options = {
    url: `v1/branch/permissions/EC5F925B-BC8A-4BD2-B6D9-B298D71A864E`
  };
  options.types = [
    types.GET_TIME_TRACKINGS_SUCCESS,
    types.GET_TIME_TRACKINGS_FAILURE
  ];

  return api.get(options);
};

export const addTimeTracking = data => {
  const options = {
    url: "v1/timetracking"
  };
  options.types = [
    types.ADD_TIME_TRACKINGS_SUCCESS,
    types.ADD_TIME_TRACKINGS_FAILURE
  ];

  return api.post(options, data);
};
