import { api } from "../../services/api";
import * as types from "../Constants";
import { VISA_STATUS_REORDER_FAILURE, VISA_STATUS_REORDER_SUCCESS } from "../Constants";

var selectedBranchId = localStorage.getItem("selectedBranchId");
export const getVisaStatus = data => {
  var selectedBranchId = localStorage.getItem("selectedBranchId");
  const options = {
    url: "v1/company/visastatus/All/" + selectedBranchId
  };

  options.types = [types.VISA_STATUS_SUCCESS, types.VISA_STATUS_FAILURE];

  return api.get(options);
};

export const addVisaStatus = data => {
  const options = {
    url: "v1/company/visastatus"
  };

  options.types = [
    types.ADD_VISA_STATUS_SUCCESS,
    types.ADD_VISA_STATUS_FAILURE
  ];

  return api.post(options, data);
};

export const VisaStatusReOrder = data => {
  const options = {
    url: "v1/company/visastatus/VisaStatusReOrder"
  };

  options.types = [
    types.VISA_STATUS_REORDER_SUCCESS,
    types.VISA_STATUS_REORDER_FAILURE
  ];

  return api.post(options, data);
};


export const updVisaStatus = data => {
  const options = {
    url: "v1/company/visastatus"
  };

  options.types = [
    types.UPDATE_VISA_STATUS_SUCCESS,
    types.UPDATE_VISA_STATUS_SUCCESS
  ];

  return api.put(options, data);
};
