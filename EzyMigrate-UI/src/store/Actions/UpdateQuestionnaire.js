import { api } from "../../services/api";
import * as types from "../Constants";

export const updateQuestionnaire = data => {
  const options = {
    url: "/v1/questionnaire"
  };

  options.types = [
    types.UPDATE_QUESTIONNAIRE_SUCCESS,
    types.UPDATE_QUESTIONNAIRE_FAILURE
  ];

  return api.put(options, data);
};
