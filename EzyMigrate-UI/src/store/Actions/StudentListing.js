import { api } from "../../services/api";
import * as types from "../Constants";

export const getStudentListing = data => {
  let selectedBranchId = localStorage.getItem("selectedBranchId");
   
  let _data = {
    schoolType: data.schoolType,
    schoolId: data.schoolId,
    status: data.status,
    clientTags: data.tags,
    pageSize: data.pageSize,
    pageNumber: data.pageNumber,
    branchId: data.branchId || selectedBranchId
  };
  const options = {
    url: "v1/school/studentList"
  };

  options.types = [
    types.GET_STUDENT_LISTING_SUCCESS,
    types.GET_STUDENT_LISTING_FAILURE
  ];
  return api.post(options, _data);
};
