import { api } from "../../services/api";
import * as types from "../Constants";

export const getEmailSetting = () => {
  const options = {
    url: `/v1/users/UserEmailSetting/4C08DFD5-DD40-C6CC-C58C-D6A9D19D0192/86209a03-f000-42be-b890-9a2ebe637b5b`
  };
  options.types = [
    types.GET_EMAIL_SETTINGS_SUCCESS,
    types.GET_EMAIL_SETTINGS_FAILURE
  ];

  return api.get(options);
};

export const updateEmailSetting = data => {
  const options = {
    url: "v1/users/UserEmailSetting"
  };
  options.types = [
    types.PUT_EMAIL_SETTINGS_SUCCESS,
    types.PUT_EMAIL_SETTINGS_FAILURE
  ];

  return api.put(options, data);
};
