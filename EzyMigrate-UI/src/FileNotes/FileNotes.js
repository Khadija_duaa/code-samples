import React from "react";
import HeaderBar from "../Components/Header/HeaderBar";
import rightNav from "../images/log-right-nav-1.png";
import rightNavTwo from "../images/log-right-nav-2.png";
import rightNavThr from "../images/log-right-nav-3.png";
import rightNavFour from "../images/log-right-nav-4.png";
import rightNavFiv from "../images/log-right-nav-5.png";
import rightNavSix from "../images/log-right-nav-6.png";
import rightNavSev from "../images/log-right-nav-7.png";
import rightNavEight from "../images/log-right-nav-8.png";
import rightNavNine from "../images/log-right-nav-9.png";
import "./FileNotesStyles.css";
import { Link } from "react-router-dom";
import { Images } from "../Themes";
import Sidebar from "../Components/SideBar";
import HeaderBarTabs from "../Components/Header/HeaderTabs";

import activityData from "../Components/ClientActivity/ActivityData";
import ProfileSideBar from "../Components/ProfileSideBar";
import { apiRefresh } from "../services/api";
import * as types from "../store/Constants";
import {
  Select,
  message,
  Modal,
  Table,
  Spin,
  Form,
  DatePicker,
  Input,
  Button,
  Tag,
  Tooltip,
  Row,
  Col,
} from "antd";
import moment from "moment";
import renderHTML from "react-render-html";
import headOption from "../Components/Header/HeaderTabOptions";
// Require Editor JS files.
import "froala-editor/js/froala_editor.pkgd.min.js";
// Require Editor CSS files.
import "froala-editor/css/froala_style.min.css";
import "froala-editor/css/froala_editor.pkgd.min.css";
import FroalaEditorCom from "../Containers/FroalaEditorCom";

const { TextArea } = Input;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

// const headOption = [
//   {
//     tabName: "Client Profile",
//     imageName: Images.clientProfile,
//     linkName: "/profile"
//   },
//   { tabName: "Visas", imageName: Images.visas, linkName: "/visa-flow" },
//   { tabName: "Admission", imageName: Images.admission, linkName: "/admission" },
//   { tabName: "Documents", imageName: Images.documents, linkName: "/documents" },
//   { tabName: "Email", imageName: Images.email, linkName: "" },
//   {
//     tabName: "Activities",
//     imageName: Images.activities,
//     linkName: "/activities"
//   },
//   {
//     tabName: "File Notes",
//     imageName: Images.documents,
//     linkName: "/file-notes"
//   },
//   {
//     tabName: "Accounts",
//     imageName: Images.accounts,
//     linkName: "/client-account"
//   },
//   { tabName: "Questionnaire", imageName: Images.questionnare, linkName: "" },
//   {  tabName: "Chat", imageName: Images.supplier, linkName: "" },
//   { tabName: "Print Case", imageName: Images.print, linkName: "" }
// ];

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];
const visaColumns = [
  {
    title: "Visa",
    dataIndex: "branchVisaTypeName",
    key: "branchVisaTypeName",
  },
];

const { Option } = Select;

var userName = "";
class FileNotes extends React.Component {
  formRef = React.createRef();
  constructor(props) {
    super(props);
    this.state = {
      fileNoteString: "",
      selectedOption: null,
      headerOptions: [],
      showOnEdit: false,
      showIndex: null,
      template: "",
      editFileNotesString: "",
      profileData: null,
      LetterTemplatesRes: [],
      onCaseData: null,
      modalVisible: false,
      modal1Visible: false,
      selectedRowKeys: [],
      visaType: null,
      taskModal: false,
      addTaskLoader: false,
      usersData: [],
      selectedFileNote: null,
      loading: false,
      content: "",
      fileNotesTemplate: [],
      configCopy: {
        key:
          "YNB3fA3A7A8B6A4C3A-9UJHAEFZMUJOYGYQEa1c1ZJg1RAeF5C4C3G3E2C2A3D6B3E3==",
        height: "auto",
        // pluginsEnabled: ['image'],
        toolbarSticky: false,
        events: {
          "charCounter.update": function() {
            console.log("oops");
          },
          "image.beforeUpload": function(files) {
            let editor = this;
            if (files.length) {
              // Create a File Reader.
              let reader = new FileReader();

              // Set the reader to insert images when they are loaded.
              reader.onload = function(e) {
                let result = e.target.result;

                editor.image.insert(result, null, null, editor.image.get());
              };
              // Read image as base64.
              reader.readAsDataURL(files[0]);
            }
            editor.popups.hideAll();
            // Stop default upload chain.
            return false;
          },
        },
      },
    };
    this.props.onSetActiveKey(null);

    var selectedBranchId = localStorage.getItem("selectedBranchId");
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    this.props.onGetFileNotes(this.state.visaType);
    if (selectedBranchId) {
      this.props.onGetLetterTemplates(selectedBranchId).then((res) => {
        this.setState({
          LetterTemplatesRes: res.payload,
        });
      });
    }

    const VisaOpt = {
      url: `/v1/subject/case/All/dropdown/` + clientprofileid,
    };
    VisaOpt.types = [
      types.GET_DOCUMENT_TYPE_SUCCESS,
      types.GET_DOCUMENT_TYPE_FAILURE,
    ];

    apiRefresh.get(VisaOpt).then((res) => {
      res.items.map((data, index) => {
        if (data) {
          data.index = index;
          data.key = `${index + 1}`;
        }
        this.setState({ onCaseData: res.items });
      });
    });

    let selectedTab = {
      headName: "Client Profile",
      headPath: "/profile",
    };
    this.props.onSetClientTab(selectedTab);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.LetterTemplatesRes != this.props.LetterTemplatesRes) {
      var filterList = [];
      if (
        this.props.LetterTemplatesRes &&
        this.props.LetterTemplatesRes.items &&
        this.props.LetterTemplatesRes.items.length > 0
      ) {
        this.props.LetterTemplatesRes.items.map((data, index) => {
          if (data.type === "FILENOTE") {
            data.index = index;
            data.key = `${index + 1}`;
            filterList.push(data);
          }
        });
        this.setState({ fileNotesTemplate: filterList });
      }
    }
  }

  componentDidMount() {
    userName = localStorage.getItem("userName");
    this.getUsers();
    if (this.props.clientTab) {
      var options = headOption(this.props.clientTab);
      this.setState({ headerOptions: options });
    } else {
      this.setState({
        headerOptions: [
          {
            tabName: "Client Profile",
            imageName: Images.clientProfile,
            linkName: "/profile",
          },
          { tabName: "Visas", imageName: Images.visas, linkName: "/visa-flow" },
          {
            tabName: "Admission",
            imageName: Images.admission,
            linkName: "/admission",
          },
          {
            tabName: "Documents",
            imageName: Images.documents,
            linkName: "/documents",
          },
          {
            tabName: "Email",
            imageName: Images.email,
            linkName: "/client-email",
          },
          {
            tabName: "Activities",
            imageName: Images.activities,
            linkName: "/activities",
          },
          {
            tabName: "File Notes",
            imageName: Images.documents,
            linkName: "/file-notes",
          },
          {
            tabName: "Accounts",
            imageName: Images.accounts,
            linkName: "/client-account",
          },
          {
            tabName: "Open Case Management",
            imageName: Images.caseManagement,
            linkName: "/Case-management",
          },
          {
            tabName: "Questionnaire",
            imageName: Images.questionnare,
            linkName: "/client-questionnaire",
          },
          { tabName: "Chat", imageName: Images.supplier, linkName: "" },
          { tabName: "Print Case", imageName: Images.print, linkName: "" },
        ],
      });
    }
    var data = JSON.parse(localStorage.getItem("profileData"));
    this.setState({ profileData: data });
  }

  rowSelection = {
    fixed: "right",
    onChange: (selectedRowKeys, selectedRows) => {
      this.setState({ selectedRowKeys: selectedRows });
    },
  };
  handleCancel = () => {
    this.setState({ showOnEdit: false });
  };

  tagRender(props) {
    const { label, value, closable, onClose } = props;
    const onPreventMouseDown = (event) => {
      event.preventDefault();
      event.stopPropagation();
    };
    return (
      <Tag
        color={"cyan"}
        onMouseDown={onPreventMouseDown}
        closable={closable}
        onClose={onClose}
        style={{ marginRight: 3 }}
      >
        {label}
      </Tag>
    );
  }

  getUsers = () => {
    this.props.onGetAllUsers().then((res) => {
      const usersInfo = res.payload.users.map((item) => {
        return {
          label: item.fullName,
          value: item.id,
        };
      });
      this.setState({ usersData: usersInfo });
    });
  };

  onFinish = (values) => {
    this.setState({ addTaskLoader: true });
    let followers = [];
    const clientId = JSON.parse(localStorage.getItem("clientprofileid"));
    const userId = localStorage.getItem("userId");
    const branchId = localStorage.getItem("selectedBranchId");
    if (values.add_followers && values.add_followers.length)
      followers = values.add_followers.map((Item) => {
        return { userId: Item };
      });
    const data = {
      branchId: branchId,
      taskDate: values.select_date.format(),
      taskTitle: values && values.task_title,
      taskDescription: values && values.task_description,
      subjectId: clientId ? clientId : "00000000-0000-0000-0000-000000000000",
      taskUsers: followers.length
        ? [...followers, { userId: userId }]
        : [{ userId: userId }],
      isPontential: false,
      isCompleted: false,
      createdBy: userId,
    };

    this.props
      .onAddDailyTasks(data)
      .then((res) => {
        this.setState({ addTaskLoader: false });
        message.success("Successfully Added!");
        if (this.formRef && this.formRef.current)
          this.formRef.current.resetFields();
        this.setState({ taskModal: false });
        let userName = localStorage.getItem("userName");
        var profileData = JSON.parse(localStorage.getItem("profileData"));
        let myData = {
          clientName: profileData.fullName,
          logMessage:
            "\n client file note : " +
            values.task_description +
            "Moved to Task titled " +
            data.taskTitle +
            "  by " +
            userName,
          date: moment(new Date()).format("DD/MM/YYYY"),
          logType: "Client Tasks",
          invoiceId: "0",
        };
        activityData(myData);
      })
      .catch((error) => {
        if (this.formRef && this.formRef.current)
          this.formRef.current.resetFields();
        this.setState({ taskModal: false });
        message.error("Unable to Add Task!");
      });
  };

  addFileNote = () => {
    var userId = localStorage.getItem("userId");
    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    if (clientprofileid) {
      let data = {
        clientId: clientprofileid,
        fileNote: this.state.fileNoteString,
        createdBy: userId,
      };
      this.props
        .onAddFileNotes(data)
        .then(() => {
          message.success("File note create successfully");
          this.props.onGetFileNotes(this.state.visaType);
          let userName = localStorage.getItem("userName");
          var profileData = JSON.parse(localStorage.getItem("profileData"));
          let myData = {
            clientName: profileData.fullName,
            logMessage:
              "client file note: " +
              this.state.fileNoteString +
              " Added by " +
              userName,
            date: moment(new Date()).format("DD/MM/YYYY"),
            logType: "Client FileNote",
            invoiceId: "0",
          };
          activityData(myData);
          this.setState({ fileNoteString: "" });
        })
        .catch(() => {
          message.error("Failed to create file notes");
        });
    } else {
      message.error("Please search and select client first!");
    }
  };

  onRemoveFileNote = (fileNote) => {
    this.setState({ loading: true });
    var userId = localStorage.getItem("userId");
    let data = {
      id: fileNote.id,
      delete: true,
      modifiedBy: userId,
    };
    this.props.onDeleteFileNotes(data).then(() => {
      message.success("File Note deleted successfully");
      this.props.onGetFileNotes(this.state.visaType);
      let userName = localStorage.getItem("userName");
      var profileData = JSON.parse(localStorage.getItem("profileData"));
      let myData = {
        clientName: profileData.fullName,
        logMessage:
          "client file note: " + fileNote.fileNote + " Deleted by " + userName,
        date: moment(new Date()).format("DD/MM/YYYY"),
        logType: "Client FileNote",
        invoiceId: "0",
      };
      activityData(myData);
      this.setState({ loading: false });
    });
  };
  LinkFileNote = () => {
    var id = this.props.fileNotesData.items[this.state.showIndex].id;
    const docTypeOpt = {
      url: `v1/client/filenote/linkvisa`,
    };

    docTypeOpt.types = [
      types.GET_DOCUMENT_TYPE_SUCCESS,
      types.GET_DOCUMENT_TYPE_FAILURE,
    ];

    this.state.selectedRowKeys.map((item, index) => {
      var data = new Object();
      data.fileNotes = [];
      data.fileNotes.push(id);
      data.subjectCaseId = item.id;

      apiRefresh.post(docTypeOpt, data).then((res) => {
        data.fileNote = [];
      });
    });
    this.setState({ modal1Visible: false });
  };
  updFileNote = () => {
    var id = this.state.selectedFileNote.id;

    let clientprofileid = JSON.parse(
      window.localStorage.getItem("clientprofileid")
    );
    let userId = localStorage.getItem("userId");
    let data = {
      id: id,
      clientId: clientprofileid,
      fileNote: this.state.editFileNotesString,
      modifiedBy: userId,
    };

    this.props
      .onUpdateFileNotes(data)
      .then(() => {
        message.success("File notes updated successfully");
        this.props.onGetFileNotes(this.state.visaType);
        this.handleCancel();
        let userName = localStorage.getItem("userName");
        var profileData = JSON.parse(localStorage.getItem("profileData"));
        let myData = {
          clientName: profileData.fullName,
          logMessage:
            "CLIENT FILE NOTE: " +
            this.state.fileNoteString +
            " updated by " +
            userName,
          date: moment(new Date()).format("DD/MM/YYYY"),
          logType: "Client FileNote",
          invoiceId: "0",
        };
        this.setState({ modalVisible: false });
        activityData(myData);
      })
      .catch(() => {
        this.setState({ modalVisible: false });
        message.error("Failed to update file notes");
      });
  };

  onContentChange = (model) => {
    this.setState({ content: model });
  };

  onChangeTemplate = (value) => {
    this.setState({ template: value });

    const getContractOpt = {
      url: `v1/template/${value}`,
    };

    getContractOpt.types = [
      types.GET_DOCUMENT_TYPE_SUCCESS,
      types.GET_DOCUMENT_TYPE_FAILURE,
    ];
    apiRefresh.get(getContractOpt).then((resp) => {
      if (resp) {
        let clientprofileid = JSON.parse(
          window.localStorage.getItem("clientprofileid")
        );
        var paramArray = [];
        var param1 = new Object();
        param1.key = "ClientId";
        param1.value = clientprofileid;
        paramArray.push(param1);
        var param2 = new Object();
        param2.key = "UserId";
        param2.value = localStorage.getItem("userId");
        paramArray.push(param2);
        var data = new Object();
        data.templateName = resp.content
          ? resp.content.replace(
              "@CurrentDate",
              moment(new Date()).format("DD/MM/YYYY")
            )
          : "";
        data.parameters = paramArray;
        const docTypeOpt = {
          url: `v1/HtmlTemplate/SetAnyTemplate`,
        };

        docTypeOpt.types = [
          types.GET_DOCUMENT_TYPE_SUCCESS,
          types.GET_DOCUMENT_TYPE_FAILURE,
        ];
        apiRefresh.post(docTypeOpt, data).then((res) => {
          this.setState({ fileNoteString: res });
        });
      }
    });
  };

  onVisaTypeTemplate = (value) => {
    this.setState({ visaType: value });
    if (value == 0) {
      value = null;
      this.setState({ visaType: null });
    }
    this.props.onGetFileNotes(value);
  };

  renderAddTaskModal = () => (
    <Modal
      title="Add Task"
      visible={this.state.taskModal}
      onCancel={() => this.setState({ taskModal: false })}
      footer={false}
      bodyStyle={{ backgroundColor: "#f6f7f9" }}
    >
      <Spin size="large" spinning={this.state.addTaskLoader}>
        <Form
          name="basic"
          // initialValues={{
          //   task_description:
          //     this.state.selectedFileNote != null
          //       ? this.state.selectedFileNote.fileNote
          //       : "",
          // }}
          ref={this.formRef}
          {...layout}
          onFinish={this.onFinish}
        >
          <div className="add-tasks">
            <div>
              <div className="mdtr-modal-gray-cont">
                <div className="modal-parts">
                  <Form.Item
                    className="form-parts"
                    label="Task Title"
                    name="task_title"
                    rules={[{ required: true, message: "Required!" }]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    className="form-parts"
                    label="Task Description"
                    name="task_description"
                    rules={[
                      {
                        required: true,
                        message: "Required!",
                      },
                    ]}
                  >
                    <Input.TextArea rows={6} showCount maxLength={250} />
                    {/* <FroalaEditor
                      ref={(el) => {
                        this.state.configCopy = el;
                      }}
                      config={this.state.configCopy}
                      tag="textarea"
                      model={this.state.content}
                      style={{ width: 300 }}
                      onModelChange={(e) => this.setState({ content: e })}
                    /> */}
                  </Form.Item>
                  <Form.Item
                    className="form-parts"
                    label="Select Date"
                    name="select_date"
                    rules={[
                      {
                        required: true,
                        message: "Required!",
                      },
                    ]}
                  >
                    <DatePicker format={"DD/MM/YYYY"} />
                  </Form.Item>
                  <Form.Item
                    className="form-parts multi-select-option"
                    label="Add Followers"
                    name="add_followers"
                    // rules={[{ message: "Please Add Followers!" }]}
                  >
                    <Select
                      mode="multiple"
                      showArrow
                      tagRender={this.tagRender}
                      style={{ width: "100%" }}
                      options={this.state.usersData}
                      filterOption={(input, option) => {
                        return (
                          option.label
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        );
                      }}
                    />
                  </Form.Item>
                </div>
              </div>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="button-blue"
                >
                  Save
                </Button>
              </Form.Item>
            </div>
          </div>
        </Form>
      </Spin>
    </Modal>
  );

  render() {
    const {
      selectedOption,
      fileNoteString,
      headerOptions,
      editFileNotesString,
      showOnEdit,
      showIndex,
      profileData,
      content,
      config,
      fileNotesTemplate,
    } = this.state;
    const {
      fileNotesData,
      LetterTemplatesRes,
      visaStatusData,
      onUpdateCaseStatus,
      onGetVisaApplication,
      visaAppData,
      employerJobHistoryCurrentRes,
      onGetClientJobHistoryCurrent,
      onSetActiveKey,
      activeKey,
      onGetReminder,
      onGetVisaStatus,
      onGetAdmissionProgram,
      onGetAdmissionStatuses,
      onGetVisaType,
      onGetVisaTypeByCountry,
    } = this.props;
    return (
      <div>
        <Spin spinning={this.state.loading}>
          <div style={{ display: "flex" }}>
            <div className="page-container">
              {headerOptions.length > 0 && (
                <HeaderBarTabs data={headerOptions} activeTab="File Notes" />
              )}
              <div style={{ display: "flex", margin: 10 }}>
                <div
                  className={
                    activeKey
                      ? "content-width-open-sidebar"
                      : "content-width-close-sidebar"
                  }
                  style={{ marginTop: 13 }}
                >
                  {profileData ? (
                    <div style={{ width: "100%", marginTop: -10 }}>
                      <p>Client Name: {profileData && profileData.fullName}</p>
                    </div>
                  ) : (
                    <div style={{ height: 30 }} />
                  )}
                  <div
                    className={"file-notes-container"}
                    style={{ marginLeft: 0, marginRight: 0 }}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        marginBottom: 10,
                      }}
                    >
                      <div
                        class="select-options"
                        style={{ width: "64%", border: 0, display: "flex" }}
                      >
                        <div style={{ width: "50%" }}>
                          <Select
                            style={{ width: "100%" }}
                            onChange={this.onChangeTemplate}
                            placeholder="Templates"
                          >
                            {fileNotesTemplate &&
                              fileNotesTemplate.map((template, index) => {
                                return (
                                  <Option value={template.id}>
                                    {template.name}
                                  </Option>
                                );
                              })}
                          </Select>
                        </div>
                        <div style={{ width: "50%", marginLeft: "10px" }}>
                          <Select
                            style={{ width: "100%" }}
                            onChange={this.onVisaTypeTemplate}
                            placeholder="Visa Types"
                          >
                            <Option value={0}>All</Option>
                            {this.state.onCaseData &&
                              this.state.onCaseData.map((template, index) => (
                                <Option value={template.id}>
                                  {template.branchVisaTypeName}
                                </Option>
                              ))}
                          </Select>
                        </div>
                      </div>

                      <div style={{ display: "flex" }}>
                        <div className="profile-print-box">
                          <img
                            src={Images.printWhite}
                            className="profile-print-icon"
                          />
                        </div>
                      </div>
                    </div>
                    <Row style={{ marginTop: "3vh", marginBottom: "3vh" }}>
                      <Col>
                        <FroalaEditorCom
                          model={fileNoteString}
                          onModelChange={(e) =>
                            this.setState({ fileNoteString: e })
                          }
                        />
                      </Col>
                    </Row>

                    <div
                      className="button-container"
                      style={{ paddingLeft: 0, paddingTop: 0 }}
                    >
                      <Button
                        onClick={this.addFileNote}
                        className="add-file-note-btn margin-top-12 button-blue"
                        style={{ cursor: "pointer" }}
                      >
                        <span style={{ color: "#FFFFFF", fontSize: 12 }}>
                          Save
                        </span>
                      </Button>
                    </div>

                    <div className="file-notes-content">
                      {fileNotesData &&
                        fileNotesData.items.map((fileNote, index) => {
                          return (
                            <div>
                              <div
                                key={index}
                                className="file-content-index-cont"
                                style={{
                                  paddingTop: 8,
                                  paddingBottom: 8,
                                  marginBottom: 20,
                                }}
                              >
                                <div className="file-content-index-row">
                                  <div style={{ width: `calc(100% - 140px)` }}>
                                    <div>
                                      <span className="date-time">
                                        {moment(fileNote.modifiedDate).format(
                                          "DD MMM YYYY hh:mm A"
                                        ) + " "}
                                      </span>
                                      <span className="title">
                                        {fileNote.fullName}
                                      </span>
                                    </div>
                                    <div>{renderHTML(fileNote.fileNote)}</div>
                                    {/*<div*/}
                                    {/*  style={{*/}
                                    {/*    display: "flex",*/}
                                    {/*    alignItems: "center",*/}
                                    {/*  }}*/}
                                    {/*>*/}
                                    {/*  <div*/}
                                    {/*    dangerouslySetInnerHTML={{*/}
                                    {/*      __html: fileNote.fileNote,*/}
                                    {/*    }}*/}
                                    {/*  />*/}
                                    {/*</div>*/}
                                  </div>
                                  <div>
                                    <div className="delete-cont">
                                      <Tooltip
                                        placement="topLeft"
                                        title={`Update`}
                                      >
                                        <div
                                          className="delete-icon"
                                          style={{
                                            cursor: "pointer",
                                            marginTop: 5,
                                          }}
                                          onClick={() => {
                                            this.setState({
                                              modalVisible: true,
                                              showIndex: index,
                                              editFileNotesString:
                                                fileNote.fileNote,
                                              selectedFileNote: fileNote,
                                            });
                                          }}
                                        >
                                          <img
                                            src={Images.notesBlue}
                                            style={{ width: 15, height: 15 }}
                                          />
                                        </div>
                                      </Tooltip>
                                      <Tooltip
                                        placement="topLeft"
                                        title={`Link Visa`}
                                      >
                                        <div
                                          className="delete-icon"
                                          style={{
                                            cursor: "pointer",
                                            marginTop: 5,
                                          }}
                                          onClick={() => {
                                            this.setState({
                                              modal1Visible: true,
                                              showIndex: index,
                                              editFileNotesString:
                                                fileNote.fileNote,
                                              selectedFileNote: fileNote,
                                            });
                                          }}
                                        >
                                          <img
                                            src={Images.linkvisa}
                                            style={{ width: 15, height: 15 }}
                                          />
                                        </div>
                                      </Tooltip>
                                      <Tooltip
                                        placement="topLeft"
                                        title={`Add Tasks`}
                                      >
                                        <div
                                          className="delete-icon"
                                          style={{
                                            cursor: "pointer",
                                            marginTop: 5,
                                          }}
                                          onClick={() => {
                                            this.setState(
                                              {
                                                showIndex: index,
                                                content: fileNote.fileNote,
                                                selectedFileNote: fileNote,
                                                taskModal: true,
                                              },
                                              () => {
                                                let html = fileNote.fileNote;
                                                let div = document.createElement(
                                                  "div"
                                                );
                                                div.innerHTML = html;
                                                let text =
                                                  div.textContent ||
                                                  div.innerText ||
                                                  "";
                                                this.formRef.current.setFieldsValue(
                                                  {
                                                    task_description: text,
                                                  }
                                                );
                                              }
                                            );
                                          }}
                                        >
                                          <img
                                            src={Images.blueAdd}
                                            style={{ width: 15, height: 15 }}
                                          />
                                        </div>
                                      </Tooltip>
                                      <Tooltip
                                        placement="topLeft"
                                        title={`Delete`}
                                      >
                                        <div
                                          className="delete-icon"
                                          style={{
                                            cursor: "pointer",
                                            marginTop: 5,
                                          }}
                                          onClick={() =>
                                            this.onRemoveFileNote(fileNote)
                                          }
                                        >
                                          <img
                                            src={Images.deleteIcon}
                                            style={{ width: 15, height: 15 }}
                                          />
                                        </div>
                                      </Tooltip>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                    </div>
                  </div>
                </div>
                <div
                  className=""
                  style={{
                    width: activeKey ? "438px" : "115px",
                    marginLeft: 15,
                  }}
                >
                  <ProfileSideBar
                    onGetClientTag={this.props.onGetClientTag}
                    clientTagRes={this.props.clientTagRes}
                    getClientRes={this.props.getClientRes}
                    onGetProfileClientTag={this.props.onGetProfileClientTag}
                    onAddProfileClientTag={this.props.onAddProfileClientTag}
                    addProfielTagRes={this.props.addProfielTagRes}
                    onRemoveProfileClientTag={
                      this.props.onRemoveProfileClientTag
                    }
                    removeProfileTagRes={this.props.removeProfileTagRes}
                    visaStatusData={visaStatusData}
                    onUpdateCaseStatus={onUpdateCaseStatus}
                    visaAppData={visaAppData}
                    onGetVisaApplication={onGetVisaApplication}
                    onGetClientJobHistoryCurrent={onGetClientJobHistoryCurrent}
                    employerJobHistoryCurrentRes={employerJobHistoryCurrentRes}
                    onSetActiveKey={onSetActiveKey}
                    activeKey={activeKey}
                    onGetClientTask={this.props.onGetClientTask}
                    onUpdateTask={this.props.onUpdateTask}
                    onGetAllUsers={this.props.onGetAllUsers}
                    onAddTaskFollower={this.props.onAddTaskFollower}
                    onRemoveTasks={this.props.onRemoveTasks}
                    onAddDailyTasks={this.props.onAddDailyTasks}
                    onGetClientFamily={this.props.onGetClientFamily}
                    onUpdateCompletedTask={this.props.onUpdateCompletedTask}
                    onAddTaskFileNote={this.props.onAddTaskFileNote}
                    onAddTaskComment={this.props.onAddTaskComment}
                    onGetTaskComments={this.props.onGetTaskComments}
                    onGetTaskFollowers={this.props.onGetTaskFollowers}
                    onGetReminder={onGetReminder}
                    onGetVisaStatus={onGetVisaStatus}
                    onGetAdmissionProgram={onGetAdmissionProgram}
                    onGetAdmissionStatuses={onGetAdmissionStatuses}
                    onGetVisaType={onGetVisaType}
                    onGetVisaTypeByCountry={onGetVisaTypeByCountry}
                  />
                </div>
              </div>
            </div>
          </div>
          <Modal
            title={false}
            style={{ top: 20 }}
            visible={this.state.modalVisible}
            onOk={() => this.updFileNote()}
            onCancel={() => this.setState({ modalVisible: false })}
            Header={false}
          >
            <Row style={{ marginTop: "3vh", marginBottom: "3vh" }}>
              <Col>
                <FroalaEditorCom
                  model={this.state.editFileNotesString}
                  onModelChange={(e) =>
                    this.setState({ editFileNotesString: e })
                  }
                />
              </Col>
            </Row>
            {/* <div>
                                <div
                                  style={{ marginTop: 20, marginBottom: 20 }}
                                >
                                  <FroalaEditor
                                    ref={(el) => {
                                      config = el;
                                    }}
                                    config={config}
                                    tag="textarea"
                                    model={editFileNotesString}
                                    onModelChange={(e) =>
                                      this.setState({ editFileNotesString: e })
                                    }
                                  />
                                </div>
                                <div
                                  className="button-container-cont"
                                  style={{
                                    paddingLeft: 0,
                                    paddingTop: 0,
                                  }}
                                >
                                </div>
                              </div> */}
          </Modal>
          {this.state.modal1Visible ? (
            <Modal
              title={false}
              style={{ top: 20 }}
              visible={this.state.modal1Visible}
              onOk={this.LinkFileNote}
              onCancel={() => this.setState({ modal1Visible: false })}
              Header={false}
            >
              <Table
                className="border-3 "
                rowSelection={this.rowSelection}
                showCount={true}
                columns={visaColumns}
                dataSource={this.state.onCaseData}
                pagination={false}
              />
            </Modal>
          ) : null}
        </Spin>
        {this.renderAddTaskModal()}
      </div>
    );
  }
}

export default FileNotes;
