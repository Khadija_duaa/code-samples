import React, { Fragment, useState, useEffect, useRef } from "react";
// import EmployerManagementTable from "./EmployerManagementTable";
import {
  Form,
  Input,
  Button,
  Select,
  Col,
  Row,
  DatePicker,
  Spin,
  message,
} from "antd";
import ImapEmailTable from "./ImapEmailTable";
import { apiRefresh } from "../../services/api";

let selectedBranchId = localStorage.getItem("selectedBranchId");

const dateFormat = "DD/MM/YYYY";

var notSearch = true;
const MapEmail = ({
  labels,
  onSearchImapEmail,
  searchMailData,
  onSearchClient,
  mainClientsData,
  onImportManualEmail,
  onSearchPotentialClient,
  potentialClientsData,
  onSearchEmployer,
  searchEmployerRes,
  onImportManualPotentialEmail,
  onImportManualEmployerEmail,
  onAutoImportEmailClient,
}) => {
  const [isSelectType, setIsSelectType] = useState();
  const [isSelector, setIsSelector] = useState();
  const [mailList, setMailList] = useState();
  const [buttonClick, setButtonClick] = useState("");
  const [selectedClient, setSelectedClient] = useState("");
  const [searchedClients, setSearchedClients] = useState(null);
  const [checkedEmails, setCheckedEmails] = useState([]);
  const [label, setLabel] = useState("[Gmail]/All Mail");
  const [id, setId] = useState([]);
  const [pageNo, setPageNo] = useState();
  const [pageSize, setPageSize] = useState(10);
  const [defaultCurrent, setDefaultCurrent] = useState(1);
  const [selectedPotentialClient, setSelectedPotentialClient] = useState("");
  const [searchedPotentialClients, setSearchedPotentialClients] = useState(
    null
  );
  const [selectedEmployer, setSelectedEmployer] = useState("");
  const [searchedEmployer, setSearchedEmployer] = useState(null);
  const [loading, setLoading] = useState(false);
  const [payloadData, setPayloadData] = useState(null);
  const [uncheckCheckbox, setUncheckCheckbox] = useState(false);

  // const usePrevious = (value) => {
  //   const ref = useRef();
  //   useEffect(() => {
  //     ref.current = value;
  //   });
  //   return ref.current;
  // };
  //
  // const prevBranchId = usePrevious(selectedBranchId);
  // useEffect(() => {
  //   let data = null;
  //   let userId = localStorage.getItem("userId");
  //   let selectedBranchId = localStorage.getItem("selectedBranchId");
  //   if (prevBranchId !== selectedBranchId) {
  //     data = {
  //       branchId: selectedBranchId,
  //       clientName: "",
  //       clientNumber: "",
  //       dateFrom: "1900-01-01T00:00:00+00",
  //       email: "",
  //       mailBox: "INBOX",
  //       pageSize: 10,
  //       pageNumber: 1,
  //       userId: userId,
  //     };
  //
  //     setPayloadData(data);
  //     setLoading(true);
  //     onSearchImapEmail(data)
  //       .then((res) => {
  //         // form.resetFields();
  //         setDefaultCurrent(1);
  //         setLoading(false);
  //       })
  //       .catch((err) => {
  //         setLoading(false);
  //       });
  //   }
  // });

  useEffect(() => {
    if (
      searchMailData &&
      searchMailData.mails &&
      searchMailData.mails.length > 0
    ) {
      searchMailData &&
        searchMailData.mails &&
        searchMailData.mails.map((data, index) => {
          if (data) {
            data.index = index;
            data.key = `${index + 1}`;
          }
        });
      setMailList(searchMailData.mails);
    }
  });
  useEffect(() => {
    if (mainClientsData) {
      setSearchedClients(mainClientsData);
    }
    if (potentialClientsData) {
      setSearchedPotentialClients(potentialClientsData);
    }
    if (searchEmployerRes) {
      setSearchedEmployer(searchEmployerRes);
    }
  }, [mainClientsData, potentialClientsData, searchEmployerRes]);
  const [form] = Form.useForm();
  const { Option } = Select;
  const clientsList = [];
  const potentialClientsList = [];
  const employersList = [];

  if (
    searchedClients &&
    searchedClients.clients &&
    searchedClients.clients.length > 0
  ) {
    for (let i = 0; i < searchedClients.clients.length; i++) {
      clientsList.push(
        <Option key={searchedClients.clients[i].id}>
          {searchedClients.clients[i].firstName +
            " " +
            searchedClients.clients[i].lastName}
        </Option>
      );
    }
  }
  if (
    searchedPotentialClients &&
    searchedPotentialClients.potentialClients &&
    searchedPotentialClients.potentialClients.length > 0
  ) {
    for (let i = 0; i < searchedPotentialClients.potentialClients.length; i++) {
      potentialClientsList.push(
        <Option key={searchedPotentialClients.potentialClients[i].id}>
          {searchedPotentialClients.potentialClients[i].label}
        </Option>
      );
    }
  }

  if (
    searchEmployerRes &&
    searchEmployerRes.items &&
    searchEmployerRes.items.length > 0
  ) {
    for (let i = 0; i < searchEmployerRes.items.length; i++) {
      employersList.push(
        <Option key={searchEmployerRes.items[i].id}>
          {searchEmployerRes.items[i].name}
        </Option>
      );
    }
  }

  var d = new Date();
  var byDate = d.setMonth(d.getMonth() - 1);

  const onSearch = (values) => {
    var data = null;
    let userId = localStorage.getItem("userId");
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    // setIsSearch(true);
    setLoading(true);
    //
    // console.log("valuesvaluesvalues", values);
    if (buttonClick === "showAll") {
      data = {
        branchId: selectedBranchId,
        clientName: "",
        clientNumber: "",
        dateFrom: values.sentSince || "1900-01-01T00:00:00+00",
        email: "",
        mailBox: values.mailBox || "INBOX",
        pageSize: 10,
        pageNumber: 1,
        userId: userId,
      };
    } else {
      data = {
        branchId: selectedBranchId,
        clientNumber: values.clientNumber || "",
        clientName: values.clientName || "",
        dateFrom: values.sentSince || "1900-01-01T00:00:00+00",
        email: values.email || "",
        mailBox: values.mailBox || "INBOX",
        pageSize: pageSize,
        pageNumber: 1,
        userId: userId,
      };
    }

    setPayloadData(data);

    onSearchImapEmail(data)
      .then(() => {
        // form.resetFields();
        setDefaultCurrent(1);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });
  };

  const handleChange = (value) => {
    console.log(`selected ${value}`);
    setPageSize(value);
  };

  const handleChangeLabel = (value) => {
    console.log(`selected ${value}`);
    setLabel(value);
  };

  const handleSectorChange = (value) => {
    console.log(`selected ${value}`);
    setIsSelector(value);
  };

  const onChangeHandler = (value) => {
    setSelectedClient(value);
  };

  const onChangePotentialHandler = (value) => {
    setSelectedPotentialClient(value);
  };

  const onChangeEmployer = (value) => {
    setSelectedEmployer(value);
  };

  const onCheckEmail = (data, pageNumber, pageSize) => {
    let tempData;
    let _data = [];
    if (data && data.length > 0) {
      tempData = data.filter((item, i, ar) => ar.indexOf(item) === i);
    }
    if (tempData && tempData.length > 0) {
      for (let ind = 0; ind < tempData.length; ind++) {
        _data.push(tempData[ind].id);
      }
    }

    setPageNo(pageNumber);
    setPageSize(pageSize);
    setId(_data);
    setCheckedEmails(tempData);
  };

  const searchClient = (val) => {
    console.log("search:", val);
    if (val.length > 2 && notSearch) {
      notSearch = false;
      setTimeout(() => {
        // setLoading(true);
        onSearchClient(val)
          .then((res) => {
            setLoading(false);
            notSearch = true;
          })
          .catch((err) => {
            notSearch = true;
          });
      }, 1500);
    }
  };

  const searchPotentialClient = (val) => {
    console.log("search:", val);
    if (val.length > 2 && notSearch) {
      notSearch = false;
      setTimeout(() => {
        // setLoading(true);
        onSearchPotentialClient(val)
          .then((res) => {
            notSearch = true;
            setLoading(false);
          })
          .catch((err) => {
            notSearch = true;
            setLoading(false);
          });
      }, 1500);
    }
  };

  const searchEmployer = (val) => {
    console.log("search:", val);
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    if (val.length > 2 && notSearch) {
      notSearch = false;
      setTimeout(() => {
        // setLoading(true);
        let data = {
          branchId: selectedBranchId,
          name: val,
          business: "",
          city: "",
          jobSector: "",
          occupation: "",
          yearsOfBusiness: "",
          isPotential: false,
          employerType: "",
          pageSize: 10,
          pageNumber: 1,
        };
        onSearchEmployer(data)
          .then((res) => {
            notSearch = true;
            setLoading(false);
          })
          .catch((err) => {
            notSearch = true;
            setLoading(false);
          });
      });
    }
  };

  const onImportClientEmail = () => {
    var userId = localStorage.getItem("userId");
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    let count = 0;
    if (checkedEmails && checkedEmails.length > 0) {
      setLoading(true);
      for (let i = 0; i < checkedEmails.length; i++) {
        let selectedBranchId = localStorage.getItem("selectedBranchId");
        let data = {
          uids: [checkedEmails[i].id],
          branchId: selectedBranchId,
          clientName: payloadData.clientName,
          clientNumber: payloadData.clientNumber,
          dateFrom: payloadData.dateFrom,
          email: payloadData.email,
          mailBox: payloadData.mailBox,
          pageSize: 10,
          pageNumber: 1,
          userId: payloadData.userId,
        };
        const opt = {
          url: "v1/emailimport/SpecificEmailImportSettings",
        };
        opt.types = [
          "SPECIFIC_EMAIL_IMPORT_SUCCESS",
          "SPECIFIC_EMAIL_IMPORT_FAILURE",
        ];

        apiRefresh.post(opt, data).then((res) => {
          let importData = {
            id: 0,
            emailMessage: res.mails[0].emailMessage,
            isRead: false,
            attachmentName:
              res.mails[0] &&
              res.mails[0].attachments &&
              res.mails[0].attachments.length > 0
                ? res.mails[0].attachments[0].fileName
                : "",
            attachmentUrl:
              res.mails[0] &&
              res.mails[0].attachments &&
              res.mails[0].attachments.length > 0
                ? res.mails[0].attachments[0].fileURL
                : "",
            sizeInKB: checkedEmails[i].sizeInKB,
            subject: checkedEmails[i].subject,
            from: checkedEmails[i].from,
            import: checkedEmails[i].import,
            to: checkedEmails[i].to,
            cc: checkedEmails[i].cc,
            bcc: checkedEmails[i].bcc,
            notClients: false,
            clientReply: false,
            clientReplyMail: 0,
            cUserId: userId,
            importMessageId: checkedEmails[i].importMessageId,
            draft: false,
            emailType: 0,
            importText: "",
            clientId: selectedClient,
            importedDate: checkedEmails[i].date,
            autoImport: true,
            isSent: false,
            clientEmailType: 0,
            timeStamp: new Date(),
          };

          onImportManualEmail(importData)
            .then((res) => {
              message.success("Imported");
              setSelectedClient("");
              setUncheckCheckbox(true);
              count++;
              if (count === checkedEmails.length) {
                let data = {
                  uids: id,
                  branchId: selectedBranchId,
                  clientName: "",
                  clientNumber: "",
                  dateFrom: "1900-01-01T00:00:00+00",
                  email: "",
                  mailBox: "INBOX",
                  pageSize: pageSize,
                  pageNumber: pageNo,
                  userId: userId,
                };
                const opt = {
                  url: "v1/emailimport/SpecificEmailImportSettings",
                };
                opt.types = [
                  "SPECIFIC_EMAIL_IMPORT_SUCCESS",
                  "SPECIFIC_EMAIL_IMPORT_FAILURE",
                ];

                apiRefresh
                  .post(opt, data)
                  .then((res) => {
                    let _data = {
                      branchId: selectedBranchId,
                      clientName: "",
                      clientNumber: "",
                      dateFrom: "1900-01-01T00:00:00+00",
                      email: "",
                      mailBox: "INBOX",
                      pageSize: pageSize,
                      pageNumber: pageNo,
                      userId: userId,
                    };

                    onSearchImapEmail(_data)
                      .then(() => {
                        // form.resetFields();
                        setDefaultCurrent(pageNo);
                        setLoading(false);
                      })
                      .catch((err) => {
                        setLoading(false);
                      });
                  })
                  .catch((res) => {
                    setLoading(false);
                  });
              }
            })
            .catch((err) => {
              message.error("Failed!");
              setLoading(false);
            });
        });
      }
    }
  };

  const onUncheckCheckbox = () => {
    setLoading(true);
    let userId = localStorage.getItem("userId");
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    setUncheckCheckbox(false);
    let data = {
      branchId: selectedBranchId,
      clientName: "",
      clientNumber: "",
      dateFrom: "1900-01-01T00:00:00+00",
      email: "",
      mailBox: "INBOX",
      pageSize: 10,
      pageNumber: 0,
      userId: userId,
    };
    onSearchImapEmail(data)
      .then(() => {
        setLoading(false);
      })
      .then(() => {
        setLoading(false);
      });
  };

  const onImportPotentialClientEmail = () => {
    var userId = localStorage.getItem("userId");
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    let count = 0;
    if (checkedEmails && checkedEmails.length > 0) {
      setLoading(true);
      for (let i = 0; i < checkedEmails.length; i++) {
        let selectedBranchId = localStorage.getItem("selectedBranchId");
        let data = {
          uids: [checkedEmails[i].id],
          branchId: selectedBranchId,
          clientName: payloadData.clientName,
          clientNumber: payloadData.clientNumber,
          dateFrom: payloadData.dateFrom,
          email: payloadData.email,
          mailBox: payloadData.mailBox,
          pageSize: 10,
          pageNumber: 1,
          userId: payloadData.userId,
        };
        const opt = {
          url: "v1/emailimport/SpecificEmailImportSettings",
        };
        opt.types = [
          "SPECIFIC_EMAIL_IMPORT_SUCCESS",
          "SPECIFIC_EMAIL_IMPORT_FAILURE",
        ];

        apiRefresh.post(opt, data).then((res) => {
          let importData = {
            id: 0,
            emailMessage: res.mails[0].emailMessage,
            isRead: false,
            attachmentName:
              res.mails[0] &&
              res.mails[0].attachments &&
              res.mails[0].attachments.length > 0
                ? res.mails[0].attachments[0].fileName
                : "",
            attachmentUrl:
              res.mails[0] &&
              res.mails[0].attachments &&
              res.mails[0].attachments.length > 0
                ? res.mails[0].attachments[0].fileURL
                : "",
            sizeInKB: checkedEmails[i].sizeInKB,
            subject: checkedEmails[i].subject,
            from: checkedEmails[i].from,
            import: checkedEmails[i].import,
            to: checkedEmails[i].to,
            cc: checkedEmails[i].cc,
            bcc: checkedEmails[i].bcc,
            notClients: false,
            clientReply: false,
            clientReplyMail: 0,
            cUserId: userId,
            importMessageId: checkedEmails[i].importMessageId,
            draft: false,
            emailType: 0,
            importText: "",
            potentialClientId: selectedPotentialClient,
            importedDate: checkedEmails[i].date,
            autoImport: true,
            isSent: false,
            clientEmailType: 0,
            timeStamp: new Date(),
          };

          onImportManualPotentialEmail(importData)
            .then((res) => {
              message.success("Imported");
              setSelectedPotentialClient("");
              setUncheckCheckbox(true);
              count++;
              if (count === checkedEmails.length) {
                let data = {
                  uids: id,
                  branchId: selectedBranchId,
                  clientName: "",
                  clientNumber: "",
                  dateFrom: "1900-01-01T00:00:00+00",
                  email: "",
                  mailBox: "INBOX",
                  pageSize: pageSize,
                  pageNumber: pageNo,
                  userId: userId,
                };
                const opt = {
                  url: "v1/emailimport/SpecificEmailImportSettings",
                };
                opt.types = [
                  "SPECIFIC_EMAIL_IMPORT_SUCCESS",
                  "SPECIFIC_EMAIL_IMPORT_FAILURE",
                ];

                apiRefresh
                  .post(opt, data)
                  .then((res) => {
                    let _data = {
                      branchId: selectedBranchId,
                      clientName: "",
                      clientNumber: "",
                      dateFrom: "1900-01-01T00:00:00+00",
                      email: "",
                      mailBox: "INBOX",
                      pageSize: pageSize,
                      pageNumber: pageNo,
                      userId: userId,
                    };

                    onSearchImapEmail(_data)
                      .then(() => {
                        // form.resetFields();
                        setDefaultCurrent(pageNo);
                        setLoading(false);
                      })
                      .catch((err) => {
                        setLoading(false);
                      });
                  })
                  .catch((res) => {
                    setLoading(false);
                  });
              }
            })
            .catch((error) => {
              message.error("Failed!");
              setLoading(false);
            });
        });
      }
    }
  };

  const onImportEmployerEmail = () => {
    var userId = localStorage.getItem("userId");
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    let count = 0;
    if (checkedEmails && checkedEmails.length > 0) {
      setLoading(true);
      for (let i = 0; i < checkedEmails.length; i++) {
        let selectedBranchId = localStorage.getItem("selectedBranchId");
        let data = {
          uids: [checkedEmails[i].id],
          branchId: selectedBranchId,
          clientName: payloadData.clientName,
          clientNumber: payloadData.clientNumber,
          dateFrom: payloadData.dateFrom,
          email: payloadData.email,
          mailBox: payloadData.mailBox,
          pageSize: 10,
          pageNumber: 1,
          userId: payloadData.userId,
        };
        const opt = {
          url: "v1/emailimport/SpecificEmailImportSettings",
        };
        opt.types = [
          "SPECIFIC_EMAIL_IMPORT_SUCCESS",
          "SPECIFIC_EMAIL_IMPORT_FAILURE",
        ];

        apiRefresh.post(opt, data).then((res) => {
          let importData = {
            id: 0,
            emailMessage: res.mails[0].emailMessage,
            isRead: false,
            attachmentName:
              res.mails[0] &&
              res.mails[0].attachments &&
              res.mails[0].attachments.length > 0
                ? res.mails[0].attachments[0].fileName
                : "",
            attachmentUrl:
              res.mails[0] &&
              res.mails[0].attachments &&
              res.mails[0].attachments.length > 0
                ? res.mails[0].attachments[0].fileURL
                : "",
            sizeInKB: checkedEmails[i].sizeInKB,
            subject: checkedEmails[i].subject,
            from: checkedEmails[i].from,
            import: checkedEmails[i].import,
            to: checkedEmails[i].to,
            cc: checkedEmails[i].cc,
            bcc: checkedEmails[i].bcc,
            notClients: false,
            clientReply: false,
            clientReplyMail: 0,
            cUserId: userId,
            importMessageId: checkedEmails[i].importMessageId,
            draft: false,
            emailType: 0,
            importText: "",
            employerId: selectedEmployer,
            importedDate: checkedEmails[i].date,
            autoImport: true,
            isSent: false,
            clientEmailType: 0,
            timeStamp: new Date(),
          };

          onImportManualEmployerEmail(importData)
            .then((res) => {
              message.success("Imported");
              setSelectedEmployer("");
              setUncheckCheckbox(true);
              count++;

              if (count === checkedEmails.length) {
                let data = {
                  uids: id,
                  branchId: selectedBranchId,
                  clientName: "",
                  clientNumber: "",
                  dateFrom: "1900-01-01T00:00:00+00",
                  email: "",
                  mailBox: "INBOX",
                  pageSize: pageSize,
                  pageNumber: pageNo,
                  userId: userId,
                };
                const opt = {
                  url: "v1/emailimport/SpecificEmailImportSettings",
                };
                opt.types = [
                  "SPECIFIC_EMAIL_IMPORT_SUCCESS",
                  "SPECIFIC_EMAIL_IMPORT_FAILURE",
                ];

                apiRefresh
                  .post(opt, data)
                  .then((res) => {
                    let _data = {
                      branchId: selectedBranchId,
                      clientName: "",
                      clientNumber: "",
                      dateFrom: "1900-01-01T00:00:00+00",
                      email: "",
                      mailBox: "INBOX",
                      pageSize: pageSize,
                      pageNumber: pageNo,
                      userId: userId,
                    };

                    onSearchImapEmail(_data)
                      .then(() => {
                        // form.resetFields();
                        setDefaultCurrent(pageNo);
                        setLoading(false);
                      })
                      .catch((err) => {
                        setLoading(false);
                      });
                  })
                  .catch((res) => {
                    setLoading(false);
                  });
              }
            })
            .catch((err) => {
              message.error("Failed!");
              setLoading(false);
            });
        });
      }
    }
  };

  return (
    <Fragment>
      {loading ? (
        <div className={"spinner"}>
          <Spin size="large" />
        </div>
      ) : (
        <div>
          <div style={{ display: "flex" }}>
            <div className="client-tag">
              <div className="employer-manag">
                <div className="client-tag-form"></div>
                <div className="">
                  <div>
                    <div>
                      <Form
                        form={form}
                        onFinish={onSearch}
                        // {...layout}
                        name="employer-form"
                        initialValues={{ remember: true }}
                        colon={false}
                        labelCol={{
                          span: 8,
                        }}
                        wrapperCol={{
                          span: 16,
                        }}
                      >
                        {" "}
                        <div className="imap-set-form text-style margin-top-34">
                          <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                            <Col span={12}>
                              <Form.Item
                                label="MAIL BOX"
                                name="mailBox"

                                // rules={[
                                //   {
                                //     required: false,
                                //     message: "Please input your username!",
                                //   },
                                // ]}
                              >
                                {/*<Select  onChange={handleChangeLabel} defaultValue={"All Mail"}>*/}
                                {/*  {labels.map((data) => {*/}
                                {/*    return (*/}
                                {/*      <Option value={data.value}>{data.text}</Option>*/}
                                {/*    )*/}
                                {/*  })}*/}
                                {/*</Select>*/}
                                <Select defaultValue="INBOX">
                                  <Option value="INBOX">INBOX</Option>
                                  <Option value="[Gmail]">[Gmail]</Option>
                                  <Option value="[Gmail]/All Mail">
                                    All Mail
                                  </Option>
                                  <Option value="[Gmail]/Drafts">Drafts</Option>
                                  <Option value="[Gmail]/Important">
                                    Important
                                  </Option>
                                  <Option value="[Gmail]/Sent Mail">
                                    Sent Mail
                                  </Option>
                                  <Option value="[Gmail]/Spam">Spam</Option>
                                  <Option value="[Gmail]/Starred">
                                    Starred
                                  </Option>
                                  <Option value="[Gmail]/Trash">Trash</Option>
                                </Select>
                              </Form.Item>
                            </Col>
                            <Col span={12}>
                              <Form.Item
                                label="DATE FILTER"
                                name="dateFilter"
                                className="d-flex"
                              >
                                <Select
                                //   onChange={handleChange}
                                >
                                  <option value="">Please select</option>
                                  <option value="1">1 Day</option>
                                  <option value="2">2 Days</option>
                                  <option value="4">4 Days</option>
                                  <option value="7">1 Week</option>
                                  <option value="14">2 Weeks</option>
                                  <option value="21">3 Weeks</option>
                                  <option value="30">1 Month</option>
                                </Select>
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                            <Col span={12}>
                              <Form.Item
                                label="SENT SINCE"
                                name="sentSince"
                                className="d-flex"
                              >
                                <DatePicker format={dateFormat} />
                              </Form.Item>
                            </Col>
                            <Col span={12}>
                              <Form.Item
                                label="CLIENT NUMBER"
                                name="clientNumber"
                                rules={[
                                  {
                                    required: false,
                                    message: "Required!",
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                            <Col span={12}>
                              <Form.Item
                                label="EMAIL"
                                name="email"
                                className="d-flex"
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                            <Col span={12}>
                              <Form.Item
                                label="CLIENT NAME"
                                name="clientName"
                                rules={[
                                  {
                                    required: false,
                                    message: "Required!",
                                  },
                                ]}
                              >
                                <Input />
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                            <Col span={12}>
                              <Form.Item
                                label="PAGE SIZE"
                                name="pageSize"
                                rules={[
                                  {
                                    required: false,
                                    message: "Required!",
                                  },
                                ]}
                              >
                                <Select
                                  defaultValue={pageSize}
                                  onChange={handleChange}
                                >
                                  <option value={5}>5</option>
                                  <option value={10}>10</option>
                                  <option value={20}>20</option>
                                </Select>
                              </Form.Item>
                            </Col>
                            <Col span={6}></Col>
                            <Col style={{ paddingLeft: 0, paddingRight: 5 }}>
                              <Form.Item>
                                <Button
                                  onClick={() => setButtonClick("showAll")}
                                  className="employer-btn button-blue"
                                  type="primary"
                                  htmlType="submit"
                                >
                                  Show All
                                </Button>
                              </Form.Item>
                            </Col>
                            <Col style={{ paddingLeft: 5, paddingRight: 0 }}>
                              <Form.Item>
                                <Button
                                  onClick={() => setButtonClick("search")}
                                  className="employer-btn button-blue"
                                  htmlType="submit"
                                  type="primary"
                                >
                                  Search
                                </Button>
                              </Form.Item>
                            </Col>
                          </Row>
                        </div>
                      </Form>
                    </div>
                    <div className="reminder-set-form text-style margin-top-12">
                      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col span={8}>
                          <Form.Item label="CLIENTS">
                            <Select
                              showSearch
                              style={{ width: "100%" }}
                              placeholder="Search a client"
                              optionFilterProp="children"
                              onChange={onChangeHandler}
                              onSearch={searchClient}
                              filterOption={(input, option) =>
                                option.children
                                  .toLowerCase()
                                  .indexOf(input.toLowerCase()) >= 0
                              }
                              value={selectedClient}
                            >
                              <Option key="">Select</Option>
                              {clientsList}
                            </Select>
                          </Form.Item>
                          <Button
                            onClick={onImportClientEmail}
                            className="employer-btn button-blue"
                            type="primary"
                          >
                            Import
                          </Button>
                        </Col>
                        <Col span={8}>
                          <Form.Item
                            label="POTENTIAL CLIENTS"
                            rules={[
                              {
                                required: false,
                                message: "Required!",
                              },
                            ]}
                          >
                            <Select
                              showSearch
                              style={{ width: "100%" }}
                              placeholder="Search a potential client"
                              optionFilterProp="children"
                              onChange={onChangePotentialHandler}
                              onSearch={searchPotentialClient}
                              filterOption={(input, option) =>
                                option.children
                                  .toLowerCase()
                                  .indexOf(input.toLowerCase()) >= 0
                              }
                              value={selectedPotentialClient}
                            >
                              <Option key="">Select</Option>
                              {potentialClientsList}
                            </Select>
                          </Form.Item>
                          <Button
                            onClick={onImportPotentialClientEmail}
                            className="employer-btn button-blue"
                            type="primary"
                          >
                            Import
                          </Button>
                        </Col>
                        <Col span={8}>
                          <Form.Item
                            label="EMPLOYER"
                            rules={[
                              {
                                required: false,
                                message: "Required!",
                              },
                            ]}
                          >
                            <Select
                              showSearch
                              style={{ width: "100%" }}
                              placeholder="Search a employer"
                              optionFilterProp="children"
                              onChange={onChangeEmployer}
                              onSearch={searchEmployer}
                              filterOption={(input, option) =>
                                option.children
                                  .toLowerCase()
                                  .indexOf(input.toLowerCase()) >= 0
                              }
                              value={selectedEmployer}
                            >
                              <Option key="">Select</Option>
                              {employersList}
                            </Select>
                          </Form.Item>
                          <Button
                            onClick={onImportEmployerEmail}
                            className="employer-btn button-blue"
                            type="primary"
                          >
                            Import
                          </Button>
                        </Col>
                      </Row>
                    </div>
                    <ImapEmailTable
                      pageSize={pageSize}
                      defaultCurrent={defaultCurrent}
                      searchMailData={mailList}
                      onCheckEmail={onCheckEmail}
                      totalEmails={
                        searchMailData &&
                        searchMailData.total &&
                        searchMailData.total
                      }
                      payloadData={payloadData}
                      onSearchImapEmail={onSearchImapEmail}
                      uncheckCheckbox={uncheckCheckbox}
                      setUncheckCheckbox={onUncheckCheckbox}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};
export default MapEmail;
