import React, { Fragment, useState, useEffect } from "react";
import { Tabs, Modal, Spin } from "antd";
import MapEmail from "./MapEmail";
import BulkEmail from "../BulkEmail/BulkEmail";
// import AddNewEmployerForm from "./AddNewEmployerForm";
// import EmployerManagementTabs from "./EmployerManagementTabs";
// import Invoice from "./Invoice/Invoice";

var userId = localStorage.getItem("userId");
const selectedBranchId = localStorage.getItem("selectedBranchId");

const { TabPane } = Tabs;

const MailHead = ({
  onGetMailBox,
  mailBoxData,
  onSearchImapEmail,
  searchMailData,
  onSearchClient,
  mainClientsData,
  onImportManualEmail,
  onSearchPotentialClient,
  potentialClientsData,
  onSearchEmployer,
  searchEmployerRes,
  onImportManualPotentialEmail,
  onImportManualEmployerEmail,
  onAutoImportEmailClient,
  onGetLetterTemplates,
  LetterTemplatesRes,
  onAddDraftClient,
  onAddEmailDocument,
  emailDocumentRes,
  onGetSignature,
  signatureRes,
  onGetPdf,
  onGetDocuments,
  documentRes,
  onGetCountries,
  countriesData,
  visaTypeData,
  visaStatusData,
  onGetVisaType,
  onGetVisaStatus,
  onGetClientTag,
  clientTagRes,
}) => {
  const [addNew, setAddNew] = useState("");
  const [addEmployerModel, setAddEmployerModel] = useState(false);
  const [loading, setLoading] = useState(false);
  const [employerTabble, setEmployerTabble] = useState(true);
  const [userDataEmp, setUserDataEmp] = useState({});
  const [handleKey, setHandleKey] = useState("1");
  const [isGetEmployers, SetIsGetEmployers] = useState(false);
  const [isSearch, setIsSearch] = useState(false);
  const [labels, setLabels] =  useState([])

  const showModal = (modalTypeName) => {
    setAddNew(modalTypeName);
    setAddEmployerModel(true);
  };

  const handleCancel = () => {
    setAddEmployerModel(false);
  };

  const singleEmployeFuc = (userData) => {
    setEmployerTabble(!employerTabble);
    setUserDataEmp(userData);
  };

  const singleEmployeIcons = (userData) => {
    setUserDataEmp(userData);
  };

  const handleSetDefault = () => {
    setIsSearch(false);
  };

  useEffect(() => {
    let userId = localStorage.getItem("userId");
    onGetMailBox(userId).then((res) => {
      setLabels(res.payload.mailBoxes)
    });
  }, []);

  useEffect(() => {
    // if (!isGetEmployers) {
    //   setLoading(true);
    //
    //   SetIsGetEmployers(true);
    //   //   onGetEmployerManag(selectedBranchId).then(() => setLoading(false));
    // }
  }, [isGetEmployers]);

  const callback = (key) => {
    setEmployerTabble(true);
    // onGetEmployerManag(selectedBranchId);
    // window.location.reload();
  };

  return (
    <Fragment>
      <div className="reminder-tabs-header employer-Header">
        <Tabs onChange={(key) => callback(key)} defaultActiveKey="1">
          <TabPane tab="IMAP EMAILS" key="1">
            {labels && labels ?
              <MapEmail
              labels={labels}
              onSearchImapEmail={onSearchImapEmail}
              searchMailData={searchMailData}
              onSearchClient={onSearchClient}
              mainClientsData={mainClientsData}
              onImportManualEmail={onImportManualEmail}
              onSearchPotentialClient={onSearchPotentialClient}
              potentialClientsData={potentialClientsData}
              onSearchEmployer={onSearchEmployer}
              searchEmployerRes={searchEmployerRes}
              onImportManualPotentialEmail={onImportManualPotentialEmail}
              onImportManualEmployerEmail={onImportManualEmployerEmail}
              onAutoImportEmailClient={onAutoImportEmailClient}
            /> :  null}
          </TabPane>
          <TabPane tab="EMAIL" key="2">
            {/*  <Invoice showModal={showModal} />*/}
          </TabPane>
          <TabPane tab="MAIL CHIMP" key="3">
            {/*  <Invoice showModal={showModal} />*/}
          </TabPane>
          <TabPane tab="BULK EMAIL" key="4">
            <BulkEmail
              onGetLetterTemplates={onGetLetterTemplates}
              LetterTemplatesRes={LetterTemplatesRes}
              onAddDraftClient={onAddDraftClient}
              onAddEmailDocument={onAddEmailDocument}
              emailDocumentRes={emailDocumentRes}
              onGetSignature={onGetSignature}
              signatureRes={signatureRes}
              onGetPdf={onGetPdf}
              onGetDocuments={onGetDocuments}
              documentRes={documentRes}
              onGetCountries={onGetCountries}
              countriesData={countriesData}
              visaTypeData={visaTypeData}
              visaStatusData={visaStatusData}
              onGetVisaType={onGetVisaType}
              onGetVisaStatus={onGetVisaStatus}
              onGetClientTag={onGetClientTag}
              clientTagRes={clientTagRes}
            />
          </TabPane>
        </Tabs>
      </div>
    </Fragment>
  );
};
export default MailHead;
