import React, { Fragment } from "react";
// import { Images } from "./../../Themes";
import { Spin, message } from "antd";
import ImapEmail from "./ImapEmail/index";

let clientprofileid = JSON.parse(
  window.localStorage.getItem("clientprofileid")
);

const MailMain = () => {
  return (
    <Fragment>
      <div className="width-100">
        <ImapEmail />
      </div>
    </Fragment>
  );
};

export default MailMain;
