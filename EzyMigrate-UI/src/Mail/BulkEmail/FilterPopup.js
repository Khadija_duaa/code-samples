import React, { Fragment, useEffect, useState } from "react";
import { Tabs, Modal, Form, Input, Select, Table, Button } from "antd";

const { TabPane } = Tabs;

const { Option } = Select;

const FilterPopup = ({
  countriesData,
  allUsers,
  setClientRows,
  setPotClientRows,
  setEmployerRows,
}) => {
  const [handleKey, setHandleKey] = useState("1");
  const [clients, setClients] = useState([]);
  const [potentialClients, setPotentialClients] = useState([]);
  const [employers, setEmployers] = useState([]);
  const [processingPersons, setProcessingPersons] = useState([]);
  const [clientTags, setClientTags] = useState([]);
  const [visaStatuses, setVisaStatuses] = useState([]);
  const [potentialClientStatus, setPotentialClientStatus] = useState([]);
  const [visaTypes, setVisaTypes] = useState([]);
  const [selectedRowKeys1, setSelectedRowKeys1] = useState([]);
  const [visaCountry, setVisaCountry] = useState("");
  const [clientStatus, setClientStatus] = useState("");
  const [visaType, setVisaType] = useState("");
  const [visaStatus, setVisaStatus] = useState("");
  const [clientTag, setClientTag] = useState("");
  const [processingPerValue, setProcessingPerValue] = useState("");
  const [passportCountryVal, setPassportCountryVal] = useState("");
  const [pcStatusValue, setPcStatusValue] = useState("");

  const callback = (key) => {
    // console.log(key);

    setHandleKey(key);
  };

  useEffect(() => {
    if (allUsers) {
      allUsers.clientEmail.map((data, index) => {
        data.index = index;
        data.key = `${index + 1}`;
        data.chosen = false;
      });
      setClients(allUsers.clientEmail || []);
      allUsers.potentialClientEmail.map((data, index) => {
        data.index = index;
        data.key = `${index + 1}`;
        data.chosen = false;
      });
      setPotentialClients(allUsers.potentialClientEmail || []);
      allUsers.employerEmail.map((data, index) => {
        data.index = index;
        data.key = `${index + 1}`;
        data.chosen = false;
      });
      setEmployers(allUsers.employerEmail || []);
      setProcessingPersons(
        (allUsers.getAllUsers && allUsers.getAllUsers.users) || []
      );
      setClientTags(
        (allUsers.getClientTag && allUsers.getClientTag.items) || []
      );
      setVisaStatuses(
        (allUsers.getAllVisaStatus && allUsers.getAllVisaStatus.items) || []
      );
      setVisaTypes(
        (allUsers.getAllVisaTypes && allUsers.getAllVisaTypes.items) || []
      );
      setPotentialClientStatus(
        (allUsers.getPotentialClientStatus &&
          allUsers.getPotentialClientStatus.items) ||
          []
      );
    }
  }, [allUsers]);

  const clientColumns = [
    {
      title: "Name",
      dataIndex: "fullName",
      // defaultSortOrder: "descend",
      ellipsis: true,
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>
              {record && record.fullName}
            </span>
          </div>
        );
      },
    },

    {
      title: "Email",
      dataIndex: "email",
      key: "index",
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>{record && record.email}</span>
          </div>
        );
      },
    },
    {
      title: "Passport Country",
      dataIndex: "passportCountry",
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>
              {record && record.passportCountry}
            </span>
          </div>
        );
      },
    },
    {
      title: "Visa Type, Visa Status",
      dataIndex: "visaTypeStatus",
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>
              {record && record.visaTypeStatus}
            </span>
          </div>
        );
      },
    },
    {
      title: "Client Tags",
      dataIndex: "tags",
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>{record && record.tags}</span>
          </div>
        );
      },
    },
    {
      title: "Processing Person",
      dataIndex: "processingPerson",
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>
              {record && record.processingPerson}
            </span>
          </div>
        );
      },
    },
  ];
  const potentialClientColumns = [
    {
      title: "Name",
      dataIndex: "fullName",
      // defaultSortOrder: "descend",
      ellipsis: true,
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>
              {record && record.fullName}
            </span>
          </div>
        );
      },
    },

    {
      title: "Email",
      dataIndex: "email",
      key: "index",
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>{record && record.email}</span>
          </div>
        );
      },
    },
    {
      title: "Client Status",
      dataIndex: "clientStatus",
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>
              {record && record.clientStatus}
            </span>
          </div>
        );
      },
    },
  ];
  const employerColumns = [
    {
      title: "Name",
      dataIndex: "fullName",
      // defaultSortOrder: "descend",
      ellipsis: true,
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>
              {record && record.fullName}
            </span>
          </div>
        );
      },
    },

    {
      title: "Email",
      dataIndex: "email",
      key: "index",
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>{record && record.email}</span>
          </div>
        );
      },
    },
    {
      title: "Client Status",
      dataIndex: "contactPerson",
      width: "50px",
      render: (text, record) => {
        return (
          <div>
            <span style={{ fontSize: "12px" }}>
              {record && record.contactPerson}
            </span>
          </div>
        );
      },
    },
  ];

  const rowSelection = {
    fixed: "left",
    onChange: (selectedRowKeys, selectedRows) => {
      // setSelectedRowKeys(selectedRows);
      setClientRows(selectedRows);
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
    },
  };

  const rowSelection1 = {
    fixed: "left",
    onChange: (selectedRowKeys, selectedRows) => {
      // setSelectedRowKeys(selectedRows);
      setPotClientRows(selectedRows);
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
    },
  };

  const rowSelection2 = {
    fixed: "left",
    onChange: (selectedRowKeys, selectedRows) => {
      // setSelectedRowKeys(selectedRows);
      setEmployerRows(selectedRows);
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
    },
  };

  const onChangeVisaCountry = (value) => {
    var filter = clients.filter(
      (obj) =>
        obj.visaTypeStatus.includes(visaType || visaStatus) ||
        obj.tags.includes(clientTag) ||
        obj.processingPerson.includes(processingPerValue) ||
        obj.passportCountry == passportCountryVal
    );
    setVisaCountry(value);
    setClients(filter);
  };

  const onChangeClientStatus = (value) => {
    var filter = clients.filter(
      (obj) =>
        obj.visaTypeStatus.includes(visaType || visaStatus) ||
        obj.tags.includes(clientTag) ||
        obj.processingPerson.includes(processingPerValue) ||
        obj.passportCountry == passportCountryVal
    );
    setClientStatus(value);
    setClients(filter);
  };

  const onChangeVisaType = (value) => {
    var findValue = visaTypes.find((obj) => obj.id == value);
    var filter = clients.filter(
      (obj) =>
        obj.visaTypeStatus.includes(value || visaStatus) ||
        (obj.tags.includes(clientTag) && clientTag != "") ||
        (obj.processingPerson.includes(processingPerValue) &&
          processingPerValue != "") ||
        (obj.passportCountry == passportCountryVal && passportCountryVal != "")
    );
    setVisaType(findValue.visaTypeName);
    setClients(filter);
  };

  const onChangeVisaStatus = (value) => {
    var findValue = visaStatuses.find((obj) => obj.id == value);
    var filter = clients.filter(
      (obj) =>
        obj.visaTypeStatus.includes(visaType || findValue.name) ||
        (obj.tags.includes(clientTag) && clientTag != "") ||
        (obj.processingPerson.includes(processingPerValue) &&
          processingPerValue != "") ||
        (obj.passportCountry == passportCountryVal && passportCountryVal != "")
    );
    setVisaStatus(findValue.name);
    setClients(filter);
  };

  const onChangeClientTag = (value) => {
    var findValue = clientTags.find((obj) => obj.id == value);
    var filter = clients.filter(
      (obj) =>
        (obj.visaTypeStatus.includes(visaType || visaStatus) &&
          visaType != "" &&
          visaStatus != "") ||
        obj.tags.includes(findValue.name) ||
        (obj.processingPerson.includes(processingPerValue) &&
          processingPerValue != "") ||
        (obj.passportCountry == passportCountryVal && passportCountryVal != "")
    );
    setClientTag(findValue.name);
    setClients(filter);
  };

  const onChangeProcessingPerson = (value) => {
    var findValue = processingPersons.find((obj) => obj.id == value);
    var filter = clients.filter(
      (obj) =>
        (obj.visaTypeStatus.includes(visaType || visaStatus) &&
          visaType != "" &&
          visaStatus != "") ||
        obj.processingPerson.includes(findValue.fullName) ||
        (obj.tags.includes(clientTag) && clientTag != "") ||
        (obj.passportCountry == passportCountryVal && passportCountryVal != "")
    );
    setProcessingPerValue(findValue.fullName);
    setClients(filter);
  };

  const onChangePassportCountry = (value) => {
    var findValue =
      countriesData && countriesData.items.find((obj) => obj.id == value);
    var filter = clients.filter(
      (obj) =>
        (obj.visaTypeStatus.includes(visaType || visaStatus) &&
          visaType != "" &&
          visaStatus != "") ||
        (obj.tags.includes(clientTag) && clientTag != "") ||
        (obj.processingPerson.includes(processingPerValue) &&
          processingPerValue != "") ||
        obj.passportCountry == findValue.name
    );
    setPassportCountryVal(findValue.name);
    setClients(filter);
  };

  const onChangePCStatus = (value) => {
    var findValue = potentialClientStatus.find((obj) => obj.id == value);
    var filter = clients.filter((obj) =>
      obj.clientStatus.includes(findValue.name)
    );
    setPcStatusValue(findValue.name);
    setPotentialClients(filter);
  };

  const clearState = () => {
    setVisaCountry("");
    setClientStatus("");
    setVisaType("");
    setVisaStatus("");
    setClientTag("");
    setProcessingPerValue("");
    setPassportCountryVal("");
    setPcStatusValue("");

    allUsers.clientEmail.map((data, index) => {
      data.index = index;
      data.key = `${index + 1}`;
      data.chosen = false;
    });
    setClients(allUsers.clientEmail || []);
  };

  return (
    <Fragment>
      <div style={{ display: "flex", margin: 10 }}>
        <div style={{ width: "100%" }}>
          <div
            className={"employerin-box employer-manag-tabs"}
            style={{
              margin: 10,
              marginTop: 0,
              marginLeft: 0,
            }}
          >
            <div className="bg-white ">
              <Tabs
                activeKey={handleKey}
                type="card"
                size={"small"}
                className="mar-r employer-doc-tab"
                onChange={(key) => callback(key)}
              >
                <TabPane tab="Client" key="1">
                  <div
                    style={{
                      border: "2px solid #c9c9ca",
                      padding: "20px",
                      backgroundColor: "#f0f2f5b8",
                    }}
                  >
                    <Form
                      // onFinish={this.onFinish}
                      // ref={this.formRef}
                      style={{
                        justifyContent: "space-around",
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <div className="client-form" style={{ width: "30%" }}>
                          <Form.Item
                            className="margin-top-0"
                            style={{ paddingBottom: 0 }}
                            name="clientNumber"
                            colon={false}
                          >
                            <Select
                              placeholder="All"
                              showSearch
                              optionFilterProp="children"
                              value={visaCountry}
                              onChange={(e) => onChangeVisaCountry(e)}
                            >
                              <Option value="">Select Visa Country</Option>
                              {countriesData &&
                                countriesData.items
                                  .filter(
                                    (obj) =>
                                      obj.name.toLowerCase() ===
                                        "new zealand" ||
                                      obj.name.toLowerCase() === "australia" ||
                                      obj.name.toLowerCase() === "canada"
                                  )
                                  .map((data) => {
                                    // eslint-disable-next-line react/jsx-no-undef
                                    return (
                                      <Option value={data.id}>
                                        {data.name}
                                      </Option>
                                    );
                                  })}
                            </Select>
                          </Form.Item>
                        </div>
                        <div className="client-form" style={{ width: "30%" }}>
                          <Form.Item
                            className="margin-top-0"
                            style={{ paddingBottom: 0 }}
                            colon={false}
                          >
                            <Select
                              placeholder="Select Client Status"
                              showSearch
                              optionFilterProp="children"
                              value={clientStatus}
                              onChange={(e) => onChangeClientStatus(e)}
                            >
                              <Option value="">Select Client Status</Option>
                              <Option value="True">Active</Option>
                              <Option value="False">InActive</Option>
                            </Select>
                          </Form.Item>
                        </div>
                        <div className="client-form" style={{ width: "30%" }}>
                          <Form.Item
                            className="margin-top-0"
                            style={{ paddingBottom: 0 }}
                            colon={false}
                          >
                            <Select
                              placeholder="Select Visa Type"
                              showSearch
                              optionFilterProp="children"
                              value={visaType}
                              onChange={(e) => onChangeVisaType(e)}
                            >
                              <Option value="">Select Visa Type</Option>
                              {visaTypes &&
                                visaTypes.map((visaType, ind) => (
                                  <Option value={visaType.id}>
                                    {visaType.visaTypeName}
                                  </Option>
                                ))}
                            </Select>
                          </Form.Item>
                        </div>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <div className="client-form" style={{ width: "30%" }}>
                          <Form.Item
                            className="margin-top-0"
                            style={{ paddingBottom: 0 }}
                            colon={false}
                          >
                            <Select
                              placeholder="Select Visa Status"
                              showSearch
                              optionFilterProp="children"
                              value={visaStatus}
                              onChange={(e) => onChangeVisaStatus(e)}
                            >
                              <Option value="">Select Visa Status</Option>
                              {visaStatuses &&
                                visaStatuses.map((visaStatus, ind) => (
                                  <Option value={visaStatus.id}>
                                    {visaStatus.name}
                                  </Option>
                                ))}
                            </Select>
                          </Form.Item>
                        </div>
                        <div className="client-form" style={{ width: "30%" }}>
                          <Form.Item
                            className="margin-top-0"
                            style={{ paddingBottom: 0 }}
                            colon={false}
                          >
                            <Select
                              placeholder="Select Client Tag"
                              showSearch
                              optionFilterProp="children"
                              value={clientTag}
                              onChange={(e) => onChangeClientTag(e)}
                            >
                              <Option value="">Select Client Tags</Option>
                              {clientTags &&
                                clientTags.map((tag, ind) => (
                                  <Option value={tag.id}>{tag.name}</Option>
                                ))}
                            </Select>
                          </Form.Item>
                        </div>
                        <div className="client-form" style={{ width: "30%" }}>
                          <Form.Item
                            className="margin-top-0"
                            style={{ paddingBottom: 0 }}
                            colon={false}
                          >
                            <Select
                              placeholder="Processing Persons"
                              showSearch
                              optionFilterProp="children"
                              value={processingPerValue}
                              onChange={(e) => onChangeProcessingPerson(e)}
                            >
                              <Option value="">Select Processing Person</Option>
                              {processingPersons &&
                                processingPersons.map((user, ind) => (
                                  <Option value={user.id}>
                                    {user.fullName}
                                  </Option>
                                ))}
                            </Select>
                          </Form.Item>
                        </div>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <div className="client-form" style={{ width: "30%" }}>
                          <Form.Item className="margin-top-0" colon={false}>
                            <Select
                              placeholder="Passport Country"
                              showSearch
                              optionFilterProp="children"
                              value={passportCountryVal}
                              onChange={(e) => onChangePassportCountry(e)}
                            >
                              <Option value="">Select Passport Country</Option>
                              {countriesData &&
                                countriesData.items &&
                                countriesData.items.map((country, ind) => (
                                  <Option value={country.id}>
                                    {country.name}
                                  </Option>
                                ))}
                            </Select>
                          </Form.Item>
                        </div>
                        <div style={{ width: "30%" }}></div>
                        <div
                          style={{
                            width: "30%",
                            display: "flex",
                            justifyContent: "flex-end",
                          }}
                        >
                          <Button
                            type="primary"
                            className="login-form-button save-btn button-blue"
                            onClick={clearState}
                          >
                            Clear
                          </Button>
                        </div>
                      </div>
                    </Form>
                    <Table
                      className="border-3"
                      rowSelection={rowSelection}
                      showCount={true}
                      columns={clientColumns}
                      dataSource={clients}
                      pagination={false}
                    />
                  </div>
                </TabPane>
                <TabPane tab="Potential Client" key="2">
                  {handleKey === "1" || handleKey === "3" ? null : (
                    <div
                      style={{
                        border: "2px solid #c9c9ca",
                        padding: "20px",
                        backgroundColor: "#f0f2f5b8",
                      }}
                    >
                      <Form
                        // onFinish={this.onFinish}
                        // ref={this.formRef}
                        style={{
                          justifyContent: "space-around",
                        }}
                      >
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                        >
                          <div className="client-form" style={{ width: "30%" }}>
                            <Form.Item className="margin-top-0" colon={false}>
                              <Select
                                placeholder="Potential Client Status"
                                onChange={(e) => onChangePCStatus(e)}
                              >
                                <Option value="">
                                  Potential Client Status
                                </Option>
                                {potentialClientStatus &&
                                  potentialClientStatus.map((status, ind) => (
                                    <Option value={status.id}>
                                      {status.name}
                                    </Option>
                                  ))}
                              </Select>
                            </Form.Item>
                          </div>
                        </div>
                      </Form>
                      <Table
                        className="border-3"
                        rowSelection={rowSelection1}
                        showCount={true}
                        columns={potentialClientColumns}
                        dataSource={potentialClients}
                        pagination={false}
                      />
                    </div>
                  )}
                </TabPane>
                <TabPane tab="Employer" key="3">
                  {handleKey === "1" || handleKey === "2" ? null : (
                    <div
                      style={{
                        border: "2px solid #c9c9ca",
                        padding: "20px",
                        backgroundColor: "#f0f2f5b8",
                      }}
                    >
                      {/* <Form
                        // onFinish={this.onFinish}
                        // ref={this.formRef}
                        style={{
                          justifyContent: "space-around",
                        }}
                      >
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                        >
                          <div className="client-form" style={{ width: "30%" }}>
                            <Form.Item
                              className="margin-top-0"
                              colon={false}
                            >
                              <Select
                                placeholder="Potential Client Status"
                                onChange={(e) => onChangePCStatus(e)}
                              >
                                {potentialClientStatus &&
                                  potentialClientStatus.map((status, ind) => (
                                    <Option value={status.name}>
                                      {status.name}
                                    </Option>
                                  ))}
                              </Select>
                            </Form.Item>
                          </div>
                          <div className="client-form" style={{ width: "30%" }}>
                            <Form.Item
                              className="margin-top-0"
                              colon={false}
                            >
                              <Select
                                placeholder="Potential Client Status"
                                onChange={(e) => onChangePCStatus(e)}
                              >
                                {potentialClientStatus &&
                                  potentialClientStatus.map((status, ind) => (
                                    <Option value={status.name}>
                                      {status.name}
                                    </Option>
                                  ))}
                              </Select>
                            </Form.Item>
                          </div>
                        </div>
                      </Form> */}
                      <Table
                        className="border-3"
                        rowSelection={rowSelection2}
                        showCount={true}
                        columns={employerColumns}
                        dataSource={employers}
                        pagination={false}
                      />
                    </div>
                  )}
                </TabPane>
              </Tabs>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default FilterPopup;
