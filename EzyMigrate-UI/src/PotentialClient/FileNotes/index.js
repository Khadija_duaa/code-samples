import React, { useState } from "react";
import FileNotes from "./FileNotes";
import { Spin } from "antd";

const FileNotesMain = ({
  onGetEmployerFile,
  employerFileRes,
  onAddEmployerFile,

  onGetLetterTemplates,
  LetterTemplatesRes,

  potentialClientInfo,

  onRemoveEmployerFile,

  handleCancel,

  onUpdateEmployerFile,

  onGetClientFile,
  clientFileRes,
  employerDocumentRes,

  onAddClientFile,

  onUpdateClientFile,

  onDeleteClientFile,
  onGetAllUsers,
  onAddDailyTasks,
}) => {
  const [loading, setLoading] = useState(false);
  return (
    <div className="border-box emp-profile-box">
      <FileNotes
        onGetEmployerFile={onGetEmployerFile}
        employerFileRes={employerFileRes}
        onAddEmployerFile={onAddEmployerFile}
        onGetLetterTemplates={onGetLetterTemplates}
        LetterTemplatesRes={LetterTemplatesRes}
        potentialClientInfo={potentialClientInfo}
        onRemoveEmployerFile={onRemoveEmployerFile}
        handleCancel={handleCancel}
        onUpdateEmployerFile={onUpdateEmployerFile}
        onGetClientFile={onGetClientFile}
        clientFileRes={clientFileRes}
        employerDocumentRes={employerDocumentRes}
        onAddClientFile={onAddClientFile}
        onUpdateClientFile={onUpdateClientFile}
        onDeleteClientFile={onDeleteClientFile}
        setLoading={setLoading}
        loading={loading}
        onGetAllUsers={onGetAllUsers}
        onAddDailyTasks={onAddDailyTasks}
      />
    </div>
  );
};

export default FileNotesMain;
