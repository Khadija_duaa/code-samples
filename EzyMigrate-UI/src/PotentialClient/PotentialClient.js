import React, { Fragment } from "react";
import { connect } from "react-redux";
import {
  Button,
  Checkbox,
  Col,
  Input,
  Row,
  Space,
  Select,
  Table,
  DatePicker,
  Modal,
  message,
  Tooltip,
  Spin,
} from "antd";
import { bindActionCreators } from "redux";
import {
  getClientTag,
  getPotentialClientListing,
  getVisaType,
  deletePotentialClient,
  getPotentialClientInfo,
  getTeamMember,
  getPotentialClientUpdateBol,
  addNewClientNull,
  getPotentialClientStatus,
  movePClientToClient,
  getPClientEmailHistory,
} from "../store/Actions";
import { withRouter } from "react-router-dom";
import {
  DeleteOutlined,
  EditOutlined,
  PlusCircleOutlined,
  InfoCircleOutlined,
  TagsOutlined,
} from "@ant-design/icons";
import moment from "moment";
import AddNewPotentialClient from "./AddNewPotentialClient";
const { Option } = Select;
const dateFormat = "DD/MM/YYYY";

class PotentialClient extends React.Component {
  columnsClientTagsTable = [
    {
      title: "Tag Name",
      dataIndex: "tag",
      key: "tag",
      ellipsis: true,
      // width: "5%",
    },
  ];

  columnsProcessingPersonsTable = [
    {
      title: "Name",
      dataIndex: "userName",
      key: "userName",
      ellipsis: true,
      // width: "5%",
    },
  ];

  columns = [
    {
      title: "Contact Date",
      dataIndex: "contactDate",
      key: "contactDate",
      ellipsis: true,
      width: "150px",
      render: (text, record) => (
        <span style={{ fontSize: "14px", color: "black" }}>
          {record.contactDate === "1900-01-01T00:00:00+00:00"
            ? ""
            : moment(record.contactDate).format("DD/MM/YYYY")}
        </span>
      ),
    },
    {
      title: "Full Name",
      dataIndex: "fullName",
      key: "fullName",
      ellipsis: true,
      render: (text, record) => (
        <span>
          <a
            href="javascript:"
            onClick={() => {
              this.props.getPotentialClientInfo(record.id).then(() => {
                this.props.getPotentialClientUpdateBol(true);
                this.props.history.push("/update-potential-client");
              });
            }}
          >
            <span style={{ fontSize: "12px", color: "black" }}>
              {record.fullName}
            </span>
          </a>
        </span>
      ),
    },
    {
      title: "Mobile",
      dataIndex: "mobile",
      key: "mobile",
      ellipsis: true,
    },
    {
      title: "Status",
      dataIndex: "clientStatus",
      key: "clientStatus",
      ellipsis: true,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      ellipsis: true,
      width: "200px",
    },
    {
      title: "Interested Visa",
      dataIndex: "interestedVisaName",
      key: "interestedVisaName",
      ellipsis: true,
      width: "150px",
    },
    {
      title: "Worth",
      dataIndex: "worth",
      key: "worth",
      ellipsis: true,
    },
    {
      title: "Action",
      dataIndex: "id",
      width: "150px",
      render: (text, record) => (
        <span>
        {
          JSON.parse(
            decodeURIComponent(
              escape(window.atob(localStorage.getItem("userSystemPermissions")))
            )
          ).find((x) => x.role.toLowerCase() == "client database")
            .status == 1 ?
            <Tooltip title="Add to Client">
              <a
                href="javascript:"
                style={{ marginRight: "10px" }}
                onClick={() => {
                  this.showModalMovePClient(record.id);
                }}
              >
                <PlusCircleOutlined />
              </a>
            </Tooltip> :  null
        }
          <Tooltip title="Edit">
            <a
              href="javascript:"
              style={{ marginRight: "10px" }}
              onClick={() => {
                this.props.getPotentialClientInfo(record.id).then(() => {
                  this.props.getPotentialClientUpdateBol(true);
                  this.props.history.push("/update-potential-client");
                });
              }}
            >
              <EditOutlined />
            </a>
          </Tooltip>
          <Tooltip title="Client Tags">
            <a
              href="javascript:"
              style={{ marginRight: "10px" }}
              onClick={() => {
                this.showModalClientTags(record);
              }}
            >
              <TagsOutlined />
            </a>
          </Tooltip>
          <Tooltip title="Processing Persons">
            <a
              href="javascript:"
              style={{ marginRight: "10px" }}
              onClick={(e) => {
                e.stopPropagation();
                this.showModalProcessingPersons(record);
              }}
            >
              <InfoCircleOutlined />
            </a>
          </Tooltip>

          <Tooltip title="Delete">
            <a
              href="javascript:"
              onClick={(e) => {
                e.stopPropagation();
                this.showModalDeleteClient(record.id);
              }}
            >
              <DeleteOutlined />
            </a>
          </Tooltip>
        </span>
      ),
    },
  ];

  constructor(props) {
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    super(props);
    this.state = {
      isVisibleAddNew: false,
      visibleDeleteModalClient: false,
      visibleModalClientTags: false,
      visibleModalProcessingPersons: false,
      toDate: "1753-01-01T12:00:00.000Z",
      fromDate: "1753-01-01T12:00:00.000Z",
      email: "",
      firstName: "",
      lastName: "",
      address: "",
      occupation: "",
      clientStatus: "",
      interestedVisa: "",
      salePerson: "",
      exclude: false,
      clientTag: "",
      priority: 0,
      pageNumber: 1,
      pageSize: 5,
      totalPages: 0,
      defaultCurrent: 1,
      branchId: selectedBranchId,
      potentialClientList: [],
      potentialClientDataStatuses: [],
      loading: false,
      visibleModalMovePClient: false,
      processingPerson: localStorage.getItem("userOwner") === "True" ?
        "00000000-0000-0000-0000-000000000000" :
        localStorage.getItem("userManager") === "true" ?
          "00000000-0000-0000-0000-000000000000" :
          (localStorage.getItem("userOwner") !== "True" &&
            localStorage.getItem("userManager") !== "true" &&
            JSON.parse(
              decodeURIComponent(
                escape(window.atob(localStorage.getItem("userSystemPermissions")))
              )
            ).find((x) => x.role.toLowerCase() === "client database")
              .status === 1) ?
            "00000000-0000-0000-0000-000000000000" :
            localStorage.getItem("userId")
    };
  }

  componentDidMount() {
    this.props.getPotentialClientListing(this.state).then(() => {
      let _potentialClientListingCount =
        this.props &&
        this.props.potentialClientListingCount &&
        this.props.potentialClientListingCount;

      this.setState({ totalPages: _potentialClientListingCount });
    });
    this.props.getClientTag();
    this.props.onGetVisaType();
    this.props.getTeamMember();
    this.props.getPotentialClientStatus();
  }

  componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any) {
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    let _potentialClientList =
      nextProps &&
      nextProps.potentialClientListing &&
      nextProps.potentialClientListing;

    // if (_potentialClientList && _potentialClientList) {
    //   for (let index = 0; index < _potentialClientList.length; index++) {
    //     _potentialClientList[index].contactDate = moment(
    //       _potentialClientList[index].contactDate
    //     ).format("DD-MM-YYYY");
    //   }
    // }
    let _potentialClientDataStatuses =
      nextProps && nextProps.potentialClientData;
    this.setState(
      {
        potentialClientList: _potentialClientList,
        potentialClientDataStatuses: _potentialClientDataStatuses,
      },
      () => {
        console.log("state", this.state);
      }
    );

    let _selectedBranchId = nextProps && nextProps.selectedBranch;
    if (this.state.branchId !== _selectedBranchId) {
      this.setState({ branchId: _selectedBranchId }, () => {
        let _state = {
          isVisibleAddNew: false,
          visibleDeleteModalClient: false,
          visibleModalClientTags: false,
          visibleModalProcessingPersons: false,
          toDate: "1753-01-01T12:00:00.000Z",
          fromDate: "1753-01-01T12:00:00.000Z",
          email: "",
          firstName: "",
          lastName: "",
          address: "",
          occupation: "",
          clientStatus: "",
          interestedVisa: "",
          salePerson: "",
          exclude: false,
          clientTag: "",
          priority: 0,
          pageNumber: 1,
          pageSize: 5,
          totalPages: 0,
          defaultCurrent: 1,
          branchId: selectedBranchId,
          potentialClientList: [],
          potentialClientDataStatuses: [],
          processingPerson: localStorage.getItem("userOwner") === "True" ?
            "00000000-0000-0000-0000-000000000000" :
            localStorage.getItem("userManager") === "true" ?
              "00000000-0000-0000-0000-000000000000" :
              (localStorage.getItem("userOwner") !== "True" &&
                localStorage.getItem("userManager") !== "true" &&
                JSON.parse(
                  decodeURIComponent(
                    escape(window.atob(localStorage.getItem("userSystemPermissions")))
                  )
                ).find((x) => x.role.toLowerCase() === "client database")
                  .status === 1) ?
                "00000000-0000-0000-0000-000000000000" :
                localStorage.getItem("userId")
        };
        this.props.getPotentialClientListing(_state).then(() => {
          let _potentialClientListingCount;
          // if (this.state.exclude === true) {
          //   _potentialClientListingCount =
          //   this.props &&
          //   this.props.potentialClientListingCountTemp &&
          //   this.props.potentialClientListingCountTemp;
          //
          // } else {
          _potentialClientListingCount =
            this.props &&
            this.props.potentialClientListingCount &&
            this.props.potentialClientListingCount;

          // }

          this.setState({ totalPages: _potentialClientListingCount });
        });
        this.props.getPotentialClientStatus().then(() => {
          let _potentialClientDataStatuses =
            nextProps && nextProps.potentialClientData;

          this.setState({
            potentialClientDataStatuses: _potentialClientDataStatuses,
          });
        });
      });
    }
  }

  getHeaderButtons = () => {
    return (
      <Row>
        <Col>
          <Space size={8} className={"buttons-container-potential-clients"}>
            <Button
              size="small"
              type="primary"
              onClick={() => {
                this.props.addNewClientNull();
                this.props.getPotentialClientUpdateBol(true);

                this.props.history.push("/add-potential-client");
              }}
              className="button-blue"
            >
              <span className={"buttons-potential-clients"}>
                ADD POTENTIAL CLIENT
              </span>
            </Button>
            <Button
              className={"buttons-potential-clients button-blue"}
              size="small"
              type="primary"
              onClick={() => {
                this.props.history.push("/import-potential-client");
              }}
            >
              <span className={"buttons-potential-clients"}>
                IMPORT POTENTIAL CLIENT
              </span>
            </Button>
          </Space>
        </Col>
      </Row>
    );
  };

  handleChangeFromDate = (date) => {
    let _fromDate =
      date === null
        ? ""
        : moment(moment(date).format()).format("YYYY-MM-DDT00:00:00+00:00");

    this.setState({ fromDate: _fromDate });
  };

  handleChangeToDate = (date) => {
    let _toDate =
      date === null
        ? ""
        : moment(moment(date).format()).format("YYYY-MM-DDT00:00:00+00:00");

    this.setState({ toDate: _toDate });
  };

  getDateFilters = () => {
    return (
      <Row>
        <Col xs={24} style={{ display: "flex" }}>
          <div className={"date-from-container"}>
            <p className={"date-text"}>Date From:</p>
            <DatePicker
              format={dateFormat}
              size="large"
              onChange={this.handleChangeFromDate}
            />
          </div>
          <div className={"date-to-container"}>
            <p className={"date-text"}>Date To:</p>
            <DatePicker
              format={dateFormat}
              size="large"
              onChange={this.handleChangeToDate}
            />
          </div>
        </Col>
      </Row>
    );
  };

  handleChangeEmail = (e) => {
    this.setState({ email: e.target.value }, () => {
      console.log("state", this.state);
    });
  };

  handleChangeFirstName = (e) => {
    this.setState({ firstName: e.target.value }, () => {
      console.log("state", this.state);
    });
  };

  handleChangeLastName = (e) => {
    this.setState({ lastName: e.target.value }, () => {
      console.log("state", this.state);
    });
  };

  handleChangeAddress = (e) => {
    this.setState({ address: e.target.value }, () => {
      console.log("state", this.state);
    });
  };

  handleChangeOccupation = (e) => {
    this.setState({ occupation: e.target.value }, () => {
      console.log("state", this.state);
    });
  };

  getInputFilters = () => {
    return (
      <Row style={{ marginTop: "18px" }}>
        <Col xs={24} style={{ display: "flex" }}>
          <div className={"inputs-filters-container"}>
            <Input
              placeholder="Email"
              size="large"
              onChange={this.handleChangeEmail}
            />
          </div>
          <div className={"inputs-filters-container"}>
            <Input
              placeholder="First Name"
              size="large"
              onChange={this.handleChangeFirstName}
            />
          </div>
          <div className={"inputs-filters-container"}>
            <Input
              placeholder="Last Name"
              size="large"
              onChange={this.handleChangeLastName}
            />
          </div>
          <div className={"inputs-filters-container"}>
            <Input
              placeholder="Occupation"
              size="large"
              onChange={this.handleChangeOccupation}
            />
          </div>
          <div className={"inputs-filters-container"}>
            <Input
              placeholder="Address"
              size="large"
              onChange={this.handleChangeAddress}
            />
          </div>
        </Col>
      </Row>
    );
  };

  handleChangeClientStatus = (value) => {
    this.setState({ clientStatus: value });
  };

  handleChangeVisaTypes = (value) => {
    this.setState({ interestedVisa: value });
  };

  handleChangeClientTags = (value) => {
    this.setState({ clientTag: value.toString() });
  };

  handleChangePriority = (value) => {
    this.setState({ priority: value });
  };

  handleChangeSalesPerson = (value) => {
    this.setState({ salePerson: value });
  };

  getSelectFilters = () => {
    return (
      <Row style={{ marginTop: "18px" }}>
        <Col xs={24} style={{ display: "flex" }}>
          <div className={"filters-container-main"}>
            <Select
              className={"width-selects-filters"}
              placeholder="Please select client status"
              virtual={false}
              size="middle"
              onChange={this.handleChangeClientStatus}
            >
              <Option value={""}>{"Select"}</Option>
              {this.state &&
                this.state.potentialClientDataStatuses &&
                this.state.potentialClientDataStatuses.map((data) => {
                  // eslint-disable-next-line react/jsx-no-undef
                  return <Option value={data.name}>{data.name}</Option>;
                })}
            </Select>
          </div>
          <div className={"filters-container-sub"}>
            <Select
              virtual={false}
              className={"width-selects-filters"}
              placeholder="Please select interested visa"
              size="middle"
              onChange={this.handleChangeVisaTypes}
            >
              <Option value={""}>{"Select"}</Option>
              {this.props &&
                this.props.visaTypeData &&
                this.props.visaTypeData.items &&
                this.props.visaTypeData.items.map((data) => {
                  // eslint-disable-next-line react/jsx-no-undef
                  return (
                    <Option value={data.id.toString()}>
                      {data.visaTypeName}
                    </Option>
                  );
                })}
            </Select>
          </div>
          <div className={"filters-container-main"}>
            <Select
              virtual={false}
              className={"width-selects-filters"}
              placeholder="Please select client tags"
              size="middle"
              onChange={this.handleChangeClientTags}
            >
              <Option value={""}>{"Select"}</Option>
              {this.props &&
                this.props.clientTagRes &&
                this.props.clientTagRes.items &&
                this.props.clientTagRes.items.map((data) => {
                  // eslint-disable-next-line react/jsx-no-undef
                  return <Option value={data.id}>{data.name}</Option>;
                })}
            </Select>
          </div>
          <div className={"filters-container-sub"}>
            <Select
              virtual={false}
              className={"width-selects-filters"}
              size="middle"
              placeholder={"Select priority"}
              onChange={this.handleChangePriority}
            >
              <Option value={1}>{"Oldest to Newest"}</Option>
              <Option value={0}>{"Newest to Oldest"}</Option>
            </Select>
          </div>
        </Col>
      </Row>
    );
  };

  onClickSearch = () => {
    this.setState({ loading: true });
    let _data = {
      fromDate: this.state.fromDate,
      toDate: this.state.toDate,
      email: this.state.email,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      address: this.state.address,
      occupation: this.state.occupation,
      clientStatus: this.state.clientStatus,
      interestedVisa: this.state.interestedVisa,
      clientTag: this.state.clientTag,
      priority: this.state.priority,
      salePerson: this.state.salePerson,
      exclude: this.state.exclude,
      pageNumber: 1,
      pageSize: 5,
      branchId: this.state.branchId,
      processingPerson: localStorage.getItem("userOwner") === "True" ?
        "00000000-0000-0000-0000-000000000000" :
        localStorage.getItem("userManager") === "true" ?
          "00000000-0000-0000-0000-000000000000" :
          (localStorage.getItem("userOwner") !== "True" &&
            localStorage.getItem("userManager") !== "true" &&
            JSON.parse(
              decodeURIComponent(
                escape(window.atob(localStorage.getItem("userSystemPermissions")))
              )
            ).find((x) => x.role.toLowerCase() === "client database")
              .status === 1) ?
            "00000000-0000-0000-0000-000000000000" :
            localStorage.getItem("userId")
    };

    this.props
      .getPotentialClientListing(_data)
      .then(() => {
        this.setState({ loading: false });
        let _length;
        // if (this.state.exclude === true) {
        //   _length =
        //     this.props &&
        //     this.props.potentialClientListingCountTemp &&
        //     this.props.potentialClientListingCountTemp;
        //
        // } else {
        _length =
          this.props &&
          this.props.potentialClientListingCount &&
          this.props.potentialClientListingCount;

        // }

        this.setState(
          {
            defaultCurrent: 1,
            totalPages: _length,
            branchId: this.state.branchId,
          },
          () => {
            console.log("state", this.state);
          }
        );
      })
      .then(() => {
        this.setState({ loading: false });
      });
  };

  onClickShowAll = () => {
    window.location.reload();
  };

  getFilterButtons = () => {
    return (
      <Row style={{ marginTop: "35px" }}>
        <Col xs={6} offset={18} style={{ display: "flex" }}>
          <Button
            type="primary"
            style={{ marginRight: "4px" }}
            className="employer-btn"
            onClick={() => this.onClickShowAll()}
            className="button-blue"
          >
            Show All
          </Button>
          <Button
            type="primary"
            className="employer-btn"
            onClick={() => this.onClickSearch()}
            className="button-blue"
          >
            Search
          </Button>
        </Col>
      </Row>
    );
  };

  showModalDeleteClient = (id) => {
    this.setState({
      visibleDeleteModalClient: true,
      index: id,
    });
  };

  handleCancelDeleteModalClient = (e) => {
    this.setState({
      visibleDeleteModalClient: false,
    });
  };

  showModalMovePClient = (id) => {
    this.setState({
      visibleModalMovePClient: true,
      movePClientIndex: id,
    });
  };

  handleCancelModalMovePClient = (e) => {
    this.setState({
      visibleModalMovePClient: false,
    });
  };

  showModalClientTags = (data) => {
    let _clientTagsList = [];
    if (data.clientTags.length > 0) {
      for (let _ind = 0; _ind < data.clientTags.length; _ind++) {
        _clientTagsList.push({ tag: data.clientTags[_ind] });
      }
    }
    this.setState({
      visibleModalClientTags: true,
      dataClientTags: _clientTagsList,
    });
  };

  handleCancelShowModalClientTags = (e) => {
    this.setState({
      visibleModalClientTags: false,
    });
  };

  paginate = (value) => {
    this.setState(
      { pageSize: value.pageSize, pageNumber: value.current },
      () => {
        this.props.getPotentialClientListing(this.state).then(() => {
          this.setState(
            {
              firstName: this.state.firstName,
              lastName: this.state.lastName,
              address: this.state.address,
              occupation: this.state.occupation,
              defaultCurrent: 1,
              branchId: this.state.branchId,
            },
            () => {
              console.log("state", this.state);
            }
          );
        });
      }
    );
  };

  showModalProcessingPersons = (data) => {
    let _pPersonsList = [];
    if (data.processingPersons.length > 0) {
      for (let _ind = 0; _ind < data.processingPersons.length; _ind++) {
        _pPersonsList.push({ userName: data.processingPersons[_ind] });
      }
    }
    this.setState({
      visibleModalProcessingPersons: true,
      dataProcessingPersons: _pPersonsList,
    });
  };

  handleCancelShowModalProcessingPersons = (e) => {
    this.setState({
      visibleModalProcessingPersons: false,
    });
  };

  getModalClientTags = () => {
    return (
      <div>
        <Modal
          title="Client Tags"
          visible={this.state.visibleModalClientTags}
          onCancel={this.handleCancelShowModalClientTags}
          footer={null}
          maskClosable={false}
        >
          <div className={"school-table"}>
            <Table
              dataSource={
                this.state.dataClientTags ? this.state.dataClientTags : null
              }
              columns={this.columnsClientTagsTable}
              pagination={{
                defaultPageSize: 5,
              }}
            />
          </div>
        </Modal>
      </div>
    );
  };

  onChangeExcludeCheckBox = (e) => {
    this.setState({ exclude: e.target.checked });
  };

  getExcludeFilter = () => {
    return (
      <Row style={{ marginTop: "27px" }}>
        <Col xs={24}>
          <div
            className={"processing-persons-container"}
            style={{ display: "flex" }}
          >
            <div className={"filters-container-main"}>
              <Select
                virtual={false}
                placeholder="Select sales person"
                className={"width-selects-filters"}
                size="middle"
                onChange={this.handleChangeSalesPerson}
              >
                <Option value={""}>{"Select"}</Option>
                {this.props &&
                  this.props.teamMembers &&
                  this.props.teamMembers.map((data) => {
                    // eslint-disable-next-line react/jsx-no-undef
                    return <Option value={data.id}>{data.fullName}</Option>;
                  })}
              </Select>
            </div>
            <div className={"checkbox"}>
              <Checkbox onChange={(e) => this.onChangeExcludeCheckBox(e)}>
                {<span className={"filter-text"}>Exclude</span>}
              </Checkbox>
            </div>
          </div>
        </Col>
      </Row>
    );
  };

  getModalProcessingPersons = () => {
    return (
      <div>
        <Modal
          title="Processing Persons"
          visible={this.state.visibleModalProcessingPersons}
          onCancel={this.handleCancelShowModalProcessingPersons}
          footer={null}
          maskClosable={false}
        >
          <div className={"school-table"}>
            <Table
              dataSource={
                this.state.dataProcessingPersons
                  ? this.state.dataProcessingPersons
                  : null
              }
              columns={this.columnsProcessingPersonsTable}
              pagination={{
                defaultPageSize: 5,
              }}
            />
          </div>
        </Modal>
      </div>
    );
  };

  getModalDeleteClient = () => {
    return (
      <div>
        <Modal
          title="Delete"
          visible={this.state.visibleDeleteModalClient}
          onCancel={this.handleCancelDeleteModalClient}
          footer={null}
          maskClosable={false}
        >
          <Row>
            <Col span={24}>
              <Row> Are you sure, you want to delete potential client?</Row>
            </Col>
          </Row>
          <Row style={{ display: "flex", marginTop: "40px" }}>
            <Col span={4} offset={16}>
              <Button
                onClick={() => {
                  this.handleCancelDeleteModalClient();
                }}
              >
                Cancel
              </Button>
            </Col>
            <Col span={4}>
              <Button
                className={"button"}
                onClick={() => {
                  this.props
                    .deletePotentialClient(this.state.index)
                    .then(() => {
                      message.success("Client deleted successfully!");
                      this.setState({ visibleDeleteModalClient: false });
                      window.location.assign(
                        "/potential-client/potential-clients"
                      );
                      // this.props
                      //   .getPotentialClientListing(this.state)
                      //   .then(() => {
                      //
                      //     let _potentialClientListingCount
                      //     if(this.state.exclude === true)
                      //     {
                      //       _potentialClientListingCount =
                      //           this.props &&
                      //           this.props.potentialClientListingCountTemp &&
                      //           this.props.potentialClientListingCountTemp;
                      //
                      //     }
                      //     else
                      //     {
                      //       _potentialClientListingCount =
                      //           this.props &&
                      //           this.props.potentialClientListingCount &&
                      //           this.props.potentialClientListingCount;
                      //
                      //     }
                      //
                      //     this.setState({
                      //       totalPages: _potentialClientListingCount
                      //     });
                      //   });
                    });
                }}
              >
                Delete
              </Button>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  };

  getModalMovePClient = () => {
    return (
      <div>
        <Modal
          title="Confirmation"
          visible={this.state.visibleModalMovePClient}
          onCancel={this.handleCancelModalMovePClient}
          footer={null}
          maskClosable={false}
        >
          <Row>
            <Col span={24}>
              <Row>
                {" "}
                <span style={{ fontSize: 12 }}>
                  Are you sure, you want to move potential client to client?
                </span>
              </Row>
            </Col>
          </Row>
          <Row style={{ display: "flex", marginTop: "40px" }}>
            <Col span={4} offset={15} style={{ marginRight: "10px" }}>
              <Button
                onClick={() => {
                  this.handleCancelModalMovePClient();
                }}
                style={{ borderRadius: 5 }}
              >
                Cancel
              </Button>
            </Col>
            <Col span={4}>
              <Button
                className={"button button-blue"}
                onClick={() => {
                  this.props
                    .movePClientToClient(this.state.movePClientIndex)
                    .then(() => {
                      message.success(
                        "Potential Client is successfully added to client"
                      );
                      setTimeout(() => {
                        window.location.assign(
                          "/potential-client/potential-clients"
                        );
                      }, 1000);
                    })
                    .catch(() => {
                      message.error("An error occurred!");
                    });
                }}
              >
                OK
              </Button>
            </Col>
          </Row>
        </Modal>
      </div>
    );
  };

  getTable = () => {
    return (
      <Row style={{ marginTop: "8px" }}>
        <Col className={"school-table"}>
          <Table
            dataSource={
              this.state.potentialClientList
                ? this.state.potentialClientList
                : null
            }
            columns={this.columns}
            onChange={this.paginate}
            pagination={{
              defaultCurrent: this.state.defaultCurrent,
              total: this.state.totalPages,
              showSizeChanger: true,
              defaultPageSize: 5,
              pageSizeOptions: ["5", "10", "15", "20"],
            }}
          />
        </Col>
      </Row>
    );
  };

  render() {
    return (
      <div>
        <div>
          {this.getHeaderButtons()}
          <div style={{ display: "flex" }}>
            <div className="page-container">
              <div className="ts-container-inquiry">
                {this.getDateFilters()}
                {this.getInputFilters()}
                {this.getSelectFilters()}
                {this.getExcludeFilter()}
                {this.getFilterButtons()}
                <Spin size="large" spinning={this.state.loading}>
                  {this.getTable()}
                </Spin>
                {this.getModalMovePClient()}
                {this.getModalDeleteClient()}
                {this.getModalClientTags()}
                {this.getModalProcessingPersons()}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  let selectedBranch =
    state &&
    state.potentialClientReducer &&
    state.potentialClientReducer.potentialClientData;

  return {
    potentialClientListing:
      state &&
      state.potentialClientReducer &&
      state.potentialClientReducer.potentialClientListing &&
      state.potentialClientReducer.potentialClientListing.taskIds &&
      state.potentialClientReducer.potentialClientListing.taskIds,
    potentialClientListingCount:
      state &&
      state.potentialClientReducer &&
      state.potentialClientReducer.potentialClientListing &&
      state.potentialClientReducer.potentialClientListing &&
      state.potentialClientReducer.potentialClientListing.count,
    potentialClientListingCountTemp:
      state &&
      state.potentialClientReducer &&
      state.potentialClientReducer.potentialClientListing &&
      state.potentialClientReducer.potentialClientListing &&
      state.potentialClientReducer.potentialClientListing.searchCount,
    teamMembers:
      state &&
      state.teamMemberReducer &&
      state.teamMemberReducer.teamMembers &&
      state.teamMemberReducer.teamMembers.users &&
      state.teamMemberReducer.teamMembers.users,
    clientTagRes:
      state &&
      state.accountSetReducer &&
      state.accountSetReducer.clientTagRes &&
      state.accountSetReducer.clientTagRes,
    visaTypeData:
      state && state.visaTypeReducer && state.visaTypeReducer.visaTypeData,
    selectedBranch:
      state && state.branchReducer && state.branchReducer.selectedBranch,
    potentialClientData:
      state &&
      state.potentialClientReducer &&
      state.potentialClientReducer.potentialClientData &&
      state.potentialClientReducer.potentialClientData.items &&
      state.potentialClientReducer.potentialClientData.items,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getPotentialClientListing: bindActionCreators(
    getPotentialClientListing,
    dispatch
  ),
  getClientTag: bindActionCreators(getClientTag, dispatch),
  onGetVisaType: bindActionCreators(getVisaType, dispatch),
  deletePotentialClient: bindActionCreators(deletePotentialClient, dispatch),
  getPotentialClientInfo: bindActionCreators(getPotentialClientInfo, dispatch),
  getTeamMember: bindActionCreators(getTeamMember, dispatch),
  getPotentialClientUpdateBol: bindActionCreators(
    getPotentialClientUpdateBol,
    dispatch
  ),
  addNewClientNull: bindActionCreators(addNewClientNull, dispatch),
  getPotentialClientStatus: bindActionCreators(
    getPotentialClientStatus,
    dispatch
  ),
  movePClientToClient: bindActionCreators(movePClientToClient, dispatch),
  onGetPClientHistory: bindActionCreators(getPClientEmailHistory, dispatch),
});
PotentialClient = connect(mapStateToProps, mapDispatchToProps)(PotentialClient);

export default withRouter(PotentialClient);
