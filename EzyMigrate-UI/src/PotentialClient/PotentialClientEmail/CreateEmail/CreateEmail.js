import React, { Fragment } from "react";
import CreateEmailForm from "./CreateEmailForm";

const createEmail = ({
  onGetLetterTemplates,
  LetterTemplatesRes,

  onSendEmailLink,

  onAddEmployerEmail,

  potentialClientInfo,
  onGetSignature,
  onGetDocuments,
  onAddPotentialEmail,
  setHandleKey,
  onGetPClientHistory,
  onGetPdf,
  onAddEmailDocument,
  documentRes,
  onGetEmployerDocument,
  employerDocumentRes,
  onGetDocumentDownload,
}) => {
  // useEffect(() => {
  //   setLoading(true);
  //
  //   onGetEmployerCheckList(selectedBranchId).then((res) => {
  //     setChecklistItem(res.payload.items);
  //     setLoading(false);
  //   });
  // }, [onGetEmployerCheckList]);

  // const handleCancel = () => {
  //   setIsModalVisible(false);
  // };

  return (
    <Fragment>
      <CreateEmailForm
        onGetLetterTemplates={onGetLetterTemplates}
        LetterTemplatesRes={LetterTemplatesRes}
        onSendEmailLink={onSendEmailLink}
        onAddEmployerEmail={onAddEmployerEmail}
        potentialClientInfo={potentialClientInfo}
        onGetSignature={onGetSignature}
        onGetDocuments={onGetDocuments}
        onAddPotentialEmail={onAddPotentialEmail}
        setHandleKey={setHandleKey}
        onGetPClientHistory={onGetPClientHistory}
        onGetPdf={onGetPdf}
        onAddEmailDocument={onAddEmailDocument}
        documentRes={documentRes}
        onGetEmployerDocument={onGetEmployerDocument}
        employerDocumentRes={employerDocumentRes}
        onGetDocumentDownload={onGetDocumentDownload}
      />
    </Fragment>
  );
};

export default createEmail;
