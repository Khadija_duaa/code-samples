import React, { Fragment, useEffect, useState } from "react";
import { Tabs, Modal } from "antd";
import EmployerEmailHistory from "./PotentialClientHistory/PotentialClientHistory";
// import AddEmployerDocuments from "./EmployerEmailHistory/AddEmployerDocuments";
// import UpdateEmployerDocuments from "./EmployerEmailHistory/UpdateEmployerDocuments";
import CreateEmail from "./CreateEmail/CreateEmail";

const { TabPane } = Tabs;

const PotentialClientEmail = ({
  userDataEmp,

  onGetDocumentChecklist,
  docChecklistRes,

  onGetEmployerCheckList,
  employerCheckListRes,

  onAddEmployerCheckList,

  location,
  history,

  onSendEmailLink,

  onGetLetterTemplates,
  LetterTemplatesRes,

  onAddEmployerEmail,

  // APIS START FROM

  onGetEmployerHistory,
  employerHistoryRes,

  onGetAutoEmailImport,
  emailImportRes,

  onAddAutoEmailImport,

  potentialClientInfo,

  onGetSignature,
  onGetDocuments,

  onAddPotentialEmail,
  onGetPClientHistory,
  potentialClientHistory,
  onGetPdf,
  onGetImapForAll,
  imapForAllRes,
  signatureRes,
  onAddEmailDocument,
  documentRes,
  onGetEmployerDocument,
  employerDocumentRes,
  onGetDocumentDownload,
}) => {
  const [modalType, setModalType] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [handleKey, setHandleKey] = useState("1");

  const showModal = (modalTypeName) => {
    setModalType(modalTypeName);
    setIsModalVisible(true);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const callback = (key) => {
    // console.log(key);

    setHandleKey(key);
  };

  return (
    <Fragment>
      <div style={{ display: "flex" }}>
        <div className="ts-container-Pclient width-100">
          <div className="bg-white employer-manag-tabs">
            <Tabs
              activeKey={handleKey}
              type="card"
              size={"small"}
              className="mar-r employer-doc-tab"
              onChange={(key) => callback(key)}
            >
              <TabPane tab="HISTORY" key="1">
                <div
                  style={{
                    border: "2px solid #c9c9ca",
                    padding: "20px",
                    backgroundColor: "#f0f2f5b8",
                  }}
                >
                  <EmployerEmailHistory
                    potentialClientInfo={potentialClientInfo}
                    showModal={showModal}
                    onGetEmployerHistory={onGetEmployerHistory}
                    employerHistoryRes={employerHistoryRes}
                    onGetAutoEmailImport={onGetAutoEmailImport}
                    emailImportRes={emailImportRes}
                    onAddAutoEmailImport={onAddAutoEmailImport}
                    onGetPClientHistory={onGetPClientHistory}
                    potentialClientHistory={potentialClientHistory}
                    onGetLetterTemplates={onGetLetterTemplates}
                    LetterTemplatesRes={LetterTemplatesRes}
                    onSendEmailLink={onSendEmailLink}
                    onGetImapForAll={onGetImapForAll}
                    imapForAllRes={imapForAllRes}
                    onGetSignature={onGetSignature}
                    signatureRes={signatureRes}
                    onGetDocuments={onGetDocuments}
                    documentRes={documentRes}
                    // onGetEmployerDocument={onGetEmployerDocument}
                    onGetPdf={onGetPdf}
                    onAddEmailDocument={onAddEmailDocument}
                    onGetEmployerDocument={onGetEmployerDocument}
                    employerDocumentRes={employerDocumentRes}
                    onGetDocumentDownload={onGetDocumentDownload}
                  />
                </div>
              </TabPane>
              <TabPane tab="CREATE" key="2">
                {handleKey === "1" ?
                  null:
                <div
                  style={{
                    border: "2px solid #c9c9ca",
                    padding: "20px",
                    backgroundColor: "#f0f2f5b8",
                  }}
                >
                  <CreateEmail
                    onSendEmailLink={onSendEmailLink}
                    onGetLetterTemplates={onGetLetterTemplates}
                    LetterTemplatesRes={LetterTemplatesRes}
                    onAddEmployerEmail={onAddEmployerEmail}
                    potentialClientInfo={potentialClientInfo}
                    onGetSignature={onGetSignature}
                    onGetDocuments={onGetDocuments}
                    onAddPotentialEmail={onAddPotentialEmail}
                    setHandleKey={setHandleKey}
                    onGetPClientHistory={onGetPClientHistory}
                    onGetPdf={onGetPdf}
                    signatureRes={signatureRes}
                    onAddEmailDocument={onAddEmailDocument}
                    documentRes={documentRes}
                    onGetEmployerDocument={onGetEmployerDocument}
                    employerDocumentRes={employerDocumentRes}
                    onGetDocumentDownload={onGetDocumentDownload}
                  />
                </div>}
              </TabPane>
            </Tabs>
          </div>
        </div>
      </div>

      {/* {isModalVisible && (
        <div className="reminder-model">
          <Modal
            className="reminder-model-main"
            title={
              (modalType === "add-documents" && "ADD DOCUMENTS") ||
              (modalType === "update-documents" && "UPDATE DOCUMENTS")
            }
            visible={isModalVisible}
            onCancel={handleCancel}
            // Header={true}
            footer={false}
          >
            {modalType === "add-documents" && (
              <AddEmployerDocuments
                onAddEmployerDocument={onAddEmployerDocument}
                onUploadAvatar={onUploadAvatar}
                imageUploadSuccess={imageUploadSuccess}
              />
            )}

            {modalType === "update-documents" && (
              <UpdateEmployerDocuments
                onAddEmployerDocument={onAddEmployerDocument}
              />
            )}
          </Modal>
        </div>
      )} */}
    </Fragment>
  );
};

export default PotentialClientEmail;
