import React, { useState } from "react";
import { Upload, Button, message, Row, Col, Modal, Form } from "antd";
import {
  LoadingOutlined,
  PaperClipOutlined,
  PictureTwoTone,
  FilePdfTwoTone,
  FileWordTwoTone,
  FileExcelTwoTone,
  PlusOutlined,
} from "@ant-design/icons";
import { DeviceHub } from "@material-ui/icons";

var fileList = [
  {
    uid: "-2",
    name: "pdf.pdf",
    status: "done",
    url: "http://cdn07.foxitsoftware.cn/pub/foxit/cpdf/FoxitCompanyProfile.pdf",
  },
  {
    uid: "-3",
    name: "doc.doc",
    status: "done",
    url:
      "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.doc",
  },
];

const AddEmployerDocuments = ({
  onAddEmployerDocument,

  onUploadAvatar,
  imageUploadSuccess,
}) => {
  const [fileListData, setFileListData] = useState([]);
  const [previewVisible, SetPreviewVisible] = useState(false);
  const [previewImage, SetPreviewImage] = useState("");
  const [previewTitle, SetPreviewTitle] = useState("");

  const handleCancel = () => SetPreviewVisible(false);

  const handleChange = ({ fileList }) => {
    setFileListData({ fileList });
  };

  const getBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  };

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    SetPreviewVisible(true);
    SetPreviewImage(file.url || file.preview);

    SetPreviewTitle(
      file.name || file.url.substring(file.url.lastIndexOf("/") + 1)
    );
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  return (
    <Row>
      <Col span={24}>
        <div className="ts-upload-file-cont">
          <Upload
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            listType="picture-card"
            fileList={fileList}
            onPreview={handlePreview}
            onChange={handleChange}
          >
            {fileListData.length >= 8 ? null : uploadButton}
          </Upload>
          <Form>
            <Form.Item name="username"></Form.Item>
            <Button
              // onClick={handleSubmit}
              className="float-right button-blue"
              htmlType="submit"
              type="primary"
            >
              Upload
            </Button>
          </Form>

          <Modal
            visible={previewVisible}
            title={previewTitle}
            footer={null}
            onCancel={handleCancel}
          >
            <img alt="example" style={{ width: "100%" }} src={previewImage} />
          </Modal>
        </div>
      </Col>
    </Row>
  );
};

export default AddEmployerDocuments;
