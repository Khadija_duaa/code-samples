import React, { useState } from "react";
import { Upload, Button, message, Form } from "antd";
import { FileDoneOutlined, PlusOutlined } from "@ant-design/icons";

const AddEmployerDocuments = ({ onAddEmployerDocument }) => {
  const [fileList, setFileList] = useState([]);

  const onChange = ({ fileList: newFileList }) => {
     
    setFileList(newFileList);
  };

  const onPreview = async (file) => {
    let src = file.url;
    if (!src) {
      src = await new Promise((resolve) => {
         
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  const uploadService = () => {
    return Promise.resolve();
  };

  const customRequest = async (options) => {
    options.onProgress({ percent: 0 });
    const url = await uploadService();
    options.onProgress({ percent: 100 });
    options.onSuccess({}, { ...options.file, url });
    console.log(options);
  };

  const [form] = Form.useForm();
  const onFinish = (values) => {
    // setLoading(true);
    console.log("valuesvaluesvalues ", values);
     
    const JsonData = {
      id: 0,
      subjectId: "86209A03-F000-42BE-B890-9A2EBE637B5B",
      familyId: "00000000-0000-0000-0000-000000000000",
      documentTypeId: 0,
      title: "string",
      sizeInKB: 0,
      docuementExtension: "string",
      docuementBlobUrl: "string",
      blobFileName: "string",
      isAgent: true,
      isClient: true,
      showAgent: true,
      showClient: true,
      questionnaireId: 0,
      createdBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      createdDate: "2021-04-15T07:25:59.887Z",
      modifiedBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      modifiedDate: "2021-04-15T07:25:59.887Z",
      subjectTypeId: 0,
    };
     
    var formdata = new FormData();
    if (fileList) {
      // for (let index = 0; index < fileList.length; index++) {
      //   let selectedFile =
      //     fileList && fileList[index] && fileList[index].originFileObj;
      //   formData.append("File", selectedFile);
      //   if (selectedFile) {
      //     this.getUploadedFileData(formData, selectedFile);
      //   }
      // }
    }
    formdata.append("Data", JSON.stringify(JsonData));
     
    onAddEmployerDocument(formdata);
    // .then(() => onGetDocuments());
    // setLoading(false);
    message.success("Uploaded Successfully!");
  };

  console.log("fileListfileListfileList", fileList);

  return (
    <div className="promotional-banner upload-csv-file-sec">
      <Form onFinish={onFinish} form={form}>
        <Form.Item>
          <Upload
            // action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            listType="picture-card"
            fileList={fileList}
            onPreview={onPreview}
            onChange={onChange}
            customRequest={customRequest}
          >
            {fileList.length < 10 && (
              <div className="import-file-button-sec">
                <div>
                  <PlusOutlined />
                  <div style={{ marginTop: 8 }}>Upload</div>
                </div>
              </div>
            )}
          </Upload>
          <Button className="float-right" htmlType="submit" type="primary">
            Upload
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default AddEmployerDocuments;
