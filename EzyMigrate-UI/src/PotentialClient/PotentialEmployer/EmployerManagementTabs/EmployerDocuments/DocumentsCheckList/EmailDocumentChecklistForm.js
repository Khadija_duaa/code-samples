import React, { Fragment, useState } from "react";
import { Form, Input, Button, message, Row, Col } from "antd";
import AddDocumentsCheckList from "./AddDocumentsCheckList";
import FroalaEditorCom from "../../../../../Containers/FroalaEditorCom";

function onRequiredChange(e) {
  console.log(`checked = ${e.target.checked}`);
}

const DocumentChecklistForm = ({
  handleCancel,

  onSendEmailLink,
}) => {
  const [letterString, setLetterString] = useState("");
  const [to, setTo] = useState("");
  const [ccc, setCcc] = useState("");

  // useEffect(() => {
  //   form.setFieldsValue({
  //     name: updatedata.name
  //   });
  // }, [form, updatedata.name]);
  const [form] = Form.useForm();
  const onFinish = (values) => {
    // setLoading(true);

    console.log("Received values of form:", values);

    const formData = new FormData();
    formData.append("Message", letterString);
    formData.append("Subject", values.subject);
    formData.append("From", "usamamateen079@gmail.com");
    formData.append("Priority", "10");
    formData.append("FrequencyCode", "employer");
    formData.append("ModuleId", "10");
    formData.append(`Recipients[0].name`, to);
    formData.append(`Recipients[0].type`, "To");
    formData.append(`Recipients[1].name`, ccc);
    formData.append(`Recipients[1].type`, "CC");
    formData.append("Attachments", []);

    onSendEmailLink(formData)
      .then(() => handleCancel())
      // .then(() => onGetDocumentChecklist())
      .then(() => {
        // setLoading(false);
        message.success("Successfully Added!");
      });
  };


  return (
    <Fragment>
      <Form onFinish={onFinish} form={form} className="width-100" name="main">
        <div className="border-box-checklist add-employer-para">
          <Form.Item name="to" required={false}>
            <Row gutter={8}>
              <Col span={12}>
                <p>To</p>
              </Col>
              <Col span={12}>
                <Input
                  placeholder="To"
                  value={to}
                  onChange={(e) => setTo(e.target.value)}
                />
              </Col>
            </Row>
          </Form.Item>
          <Form.Item style={{ marginTop: "12px" }} name="ccc" required={false}>
            <Row gutter={8}>
              <Col span={12}>
                <p>Ccc</p>
              </Col>
              <Col span={12}>
                <Input
                  placeholder="Ccc"
                  value={ccc}
                  onChange={(e) => setCcc(e.target.value)}
                />
              </Col>
            </Row>
          </Form.Item>
          <Form.Item
            style={{ marginTop: "12px" }}
            name="subject"
            required={false}
          >
            <Row gutter={8}>
              <Col span={12}>
                <p>Subject</p>
              </Col>
              <Col span={12}>
                <Input />
              </Col>
            </Row>
          </Form.Item>
          <div className="margin-top-20 letter-froala">
            <FroalaEditorCom
              setLoading={(value) => setLoading(value)}
              model={letterString}
              onModelChange={(value) => setLetterString(value)}
            />
          </div>
          <div className="margin-top-20">
            <AddDocumentsCheckList />
          </div>
        </div>

        <Row className="flex-end margin-top-12" gutter={10}>
          <Col>
            <Form.Item>
              <Button
                type="primary"
                className="login-form-button save-btn"
                htmlType="submit"
              >
                SEND
              </Button>
            </Form.Item>
          </Col>
          <Col>
            <Form.Item>
              <Button
                onClick={() => handleCancel()}
                type="primary"
                className="login-form-button save-btn"
              >
                CLOSE
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Fragment>
  );
};
export default DocumentChecklistForm;
