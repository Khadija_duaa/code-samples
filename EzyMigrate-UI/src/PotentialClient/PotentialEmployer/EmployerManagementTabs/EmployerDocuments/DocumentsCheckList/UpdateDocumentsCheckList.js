import React, { Fragment, useState } from "react";
import { Upload, Button, message, Form } from "antd";
import { FileDoneOutlined, PlusOutlined } from "@ant-design/icons";

const UpdateEmployerDocuments = ({}) => {
  const [fileList, setFileList] = useState([]);

  return (
    <Fragment>
      <div className="promotional-banner upload-csv-file-sec">
        <p>Do you want to update title of this document ?</p>
      </div>
      <Button htmlType="submit" type="primary">
        OK
      </Button>
    </Fragment>
  );
};

export default UpdateEmployerDocuments;
