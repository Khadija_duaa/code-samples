import React, { Fragment, useState } from "react";
import {
  Form,
  Input,
  Button,
  message,
  Row,
  Col,
  Checkbox,
  Switch,
  Space,
} from "antd";
import { Images } from "./../../../Themes";
import { CloseCircleOutlined } from "@ant-design/icons";

const DocumentChecklistForm = ({
  handleCancel,

  showModal,

  onGetEmployerCheckList,

  onAddEmployerCheckList,

  selectedBranchId,

  selectdata,
}) => {
  const [required, SetRequired] = useState(false);

  const [form] = Form.useForm();

  const onFinish = (values) => {
    // setLoading(true);

    var checkListItemsData = [];

    if (values.checkListItems && values.checkListItems.length > 0) {
      for (var i = 0; i < values.checkListItems.length; i++) {
        checkListItemsData.push({
          id: 0,
          checkListId: 0,
          name: values && values.checkListItems[i].name,
          required: required ? required : false,
          createdBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          createdDate: "2021-04-22T18:27:50.776Z",
          modifiedBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          modifiedDate: "2021-04-22T18:27:50.776Z",
          priority: 0,
        });
      }
    }

    console.log("Received values of form:", values);

    const data = {
      branchId: selectedBranchId && selectedBranchId,
      name: values.name,
      description: values.description,
      createdBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      checkListItems: checkListItemsData,
    };

    onAddEmployerCheckList(data)
      .then(() => handleCancel())
      .then(() => onGetEmployerCheckList(selectedBranchId))
      .then(() => {
        // setLoading(false);
        message.success("Successfully Added!");
        showModal("send-email");
      });
  };

  function onRequiredChange(e) {
    console.log(`checked = ${e.target.checked}`);

    SetRequired(e.target.checked);
  }

  return (
    <Fragment>
      {selectdata && (
        <Form
          onFinish={onFinish}
          form={form}
          initialValues={selectdata}
          className="width-100"
          name="main"
        >
          <div className="border-box-checklist add-employer-para">
            <Form.Item name="name" required={false}>
              <Row gutter={8}>
                <Col span={12}>
                  <p>Name</p>
                </Col>
                <Col span={12}>
                  <Input placeholder="Name" />
                </Col>
              </Row>
            </Form.Item>
            <Form.Item
              style={{ marginTop: "12px" }}
              name="description"
              required={false}
            >
              <Row gutter={8}>
                <Col span={12}>
                  <p>Discription</p>
                </Col>
                <Col span={12}>
                  <Input placeholder="Discription" />
                </Col>
              </Row>
            </Form.Item>
          </div>

          <div
            style={{ marginTop: "12px" }}
            className="border-box-checklist add-employer-para"
          >
            <Form.List name="checkListItems">
              {(checkListItems, { add, remove }) => (
                <div className="">
                  <Row className=" margin-contact-container">
                    <Col>
                      <span style={{ fontSize: "15px", color: "gray" }}>
                        Create a list of documents that you require from your
                        client, for example you can create a work visa document
                        list. Please note if you make a document “Required” this
                        means the client will not be able to update any
                        documents until the required document has been added.
                      </span>
                    </Col>
                    <Col>
                      <Form.Item className="form-add-btn">
                        <img
                          src={Images.addIcon}
                          className="icons-client "
                          type="primary"
                          onClick={() => add()}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={24}>
                      {checkListItems.map((field) => (
                        <Row className="file-delete-head">
                          <Col xs={24}>
                            <Space
                              key={field.key}
                              style={{ display: "block", marginBottom: 8 }}
                              align="baseline"
                            >
                              <Row gutter={12} style={{ lineHeight: "32px" }}>
                                <Col span={14}>
                                  <Form.Item
                                    {...field}
                                    fieldKey={[field.fieldKey, "first"]}
                                    name={[field.name, "name"]}
                                    required={false}
                                  >
                                    <Input.TextArea rows={6} />
                                  </Form.Item>
                                </Col>
                                <div
                                  style={{ width: "41%" }}
                                  className="d-flex space-between"
                                >
                                  <div>
                                    <Col>
                                      <Checkbox
                                        style={{
                                          color: "#0c55da",
                                          fontWeight: 500,
                                        }}
                                        onChange={onRequiredChange}
                                        defaultChecked={required}
                                      >
                                        Required
                                      </Checkbox>
                                      <Row
                                        style={{
                                          color: "rgb(107 107 107)",
                                          fontWeight: 500,
                                        }}
                                      >
                                        Priority
                                      </Row>
                                      <Row gutter={10}>
                                        <Col>
                                          <Form.Item
                                            colon={false}
                                            className="d-block checklist-switch"
                                            label="High"
                                          >
                                            <Switch />
                                          </Form.Item>
                                        </Col>
                                        <Col>
                                          <Form.Item
                                            colon={false}
                                            className="d-block checklist-switch-low"
                                            label="Low"
                                          >
                                            <Switch />
                                          </Form.Item>
                                        </Col>
                                      </Row>
                                    </Col>
                                  </div>
                                  <div>
                                    <Col>
                                      <a
                                        href="javascript:"
                                        onClick={() => {
                                          remove(field.name);
                                        }}
                                      >
                                        <CloseCircleOutlined
                                          style={{
                                            color: "red",
                                            fontSize: "15px",
                                          }}
                                        />
                                      </a>
                                    </Col>
                                  </div>
                                </div>
                              </Row>
                            </Space>
                          </Col>
                        </Row>
                      ))}
                    </Col>
                  </Row>
                </div>
              )}
            </Form.List>
          </div>

          <Row className="flex-end margin-top-12" gutter={10}>
            <Col>
              <Form.Item>
                <Button
                  type="primary"
                  className="login-form-button save-btn"
                  htmlType="submit"
                  // onClick={() => onSave()}
                >
                  SAVE
                </Button>
              </Form.Item>
            </Col>
            <Col>
              <Form.Item>
                <Button
                  onClick={() => handleCancel()}
                  type="primary"
                  className="login-form-button save-btn"
                >
                  CLOSE
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      )}
    </Fragment>
  );
};
export default DocumentChecklistForm;
