import React, { useEffect, useState } from "react";
import { Table, Button, Space, Spin } from "antd";

const DocumentCheckList = ({}) => {
  const [loading, setLoading] = useState(false);
  // const [linkId, setLinkId] = useState("");

  // useEffect(() => {
  //   setLoading(true);
  //   onGetDocumentCheckList()
  //     .then((res) => {
  //        

  //       onGetDocumentLink(
  //         res &&
  //           res.payload &&
  //           res.payload.items &&
  //           res.payload.items.map((data) => data && data.link)
  //       );
  //     })
  //     .then(() => {
  //       setLoading(false);
  //     });
  // }, [onGetDocumentCheckList]);
  const columns = [
    {
      title: "Title",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "Link Client",
      dataIndex: "linkClient",
      key: "Link Client",
      render: (text, record) => (
        <div>
          Please{" "}
          <Button className="click-here" type="link">
            Click Here
          </Button>{" "}
          to open
        </div>
      )
    },
    {
      title: "Link",
      dataIndex: "link",
      key: "link",
      render: (text, record) => (
        <Space size="middle">
          {text}
          <Button className="copy-link" type="default">
            Copy Link
          </Button>
        </Space>
      )
    }
  ];
  const data = [
    {
      key: "1",
      title: "Attachment 3.pdf",
      linkClient: "Click Here",
      link: "https://checkmyvisa.co.nz/"
    },
    {
      key: "2",
      title: "Attachment 3.pdf",
      linkClient: "Click Here",
      link: "https://checkmyvisa.co.nz/"
    }
  ];

  return (
    <div className="mar-top-1 mb-6rem document-table">
      <Spin size="large" spinning={loading}>
        <Table
          rowClassName={(record, index) =>
            index % 2 === 0 ? "table-row-light" : "table-row-dark"
          }
          bordered
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      </Spin>
    </div>
  );
};

export default DocumentCheckList;
