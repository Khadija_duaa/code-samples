import React, { Fragment, useState } from "react";
import { Button, Row, Col, Form, message } from "antd";
import moment from "moment";
import activityData from "../../../Components/ClientActivity/ActivityData";

const UpdateEmployerDocuments = ({
  onUpdateEmployerDocument,
  onUpdateTitle,
}) => {
  const [loading, setLoading] = useState("");

  const [form] = Form.useForm();
  const onFinish = (values) => {
    setLoading(true);
    console.log("Received values of form: ", values);

    const data = {
      documentTitle: onUpdateTitle && onUpdateTitle,
      id: 0,
    };

    onUpdateEmployerDocument(data)
      .then(() => {
        // onGetClientProfile();
        setLoading(false);
        message.success("Successfully Updated!");
        let userName = localStorage.getItem("userName");
        var profileData = JSON.parse(localStorage.getItem("profileData"));
        let myData = {
          clientName: profileData.fullName,
          logMessage:
            "\n Document " +
            data.documentTitle +
            " renamed for " +
            profileData.fullName +
            userName,
          date: moment(new Date()).format("DD/MM/YYYY"),
          logType: "Client Documents",
          invoiceId: "0",
        };
        activityData(myData);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  return (
    <Fragment>
      <Form onFinish={onFinish} form={form}>
        <div className="promotional-banner upload-csv-file-sec">
          <Row>
            <span style={{ fontSize: "14px", fontWeight: "500" }}>
              Do you want to update title of this document ?
            </span>
          </Row>
        </div>
        <Row style={{ justifyContent: "flex-end" }}>
          <Col>
            <Form.Item>
              <Button
                style={{ marginTop: "10px" }}
                htmlType="submit"
                type="primary"
              >
                OK
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Fragment>
  );
};

export default UpdateEmployerDocuments;
