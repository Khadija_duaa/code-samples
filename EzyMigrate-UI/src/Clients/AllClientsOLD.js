import React from "react";
import Select from "react-select";
import HeaderBar from "../Components/Header/HeaderBar";
import "./ClientsStyles.css";
import partner from "../images/partners-icon.png";
import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import HeaderBarTabs from "../Components/Header/HeaderTabs";
import { Images } from "../Themes";
import { Table, Tag, Space } from "antd";
// import { Option } from "antd";

const { Option } = Select;

const options2 = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const headOption = [{ tabName: "All Clients" }];

class AllClients extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      selectedOption: null,
      newClientTags: [],
    };
  }

  searchData = () => {
    let data = {
      firstName: "string",
      lastName: "string",
      email: "string",
      occupation: "string",
      pageSize: 0,
      pageNumber: 0,
      branchId: "fe9506ec-7cdf-4c42-81c4-345bd51a12ef",
      mobile: "string",
      company: "string",
      activeInactive: true,
      clientSort: "string",
      passportNo: "string",
      surnameStart: "string",
      status: 0,
      dateOfBirth: "2021-02-15",
      jobSector: "string",
      includeDeleted: true,
      internalId: "string",
      familyMember: "string",
      visaType: 0,
      salePerson: "string",
      clientType: "string",
      inzClient: "string",
      clientTags: "string",
      clientNumber: "string",
    };

    this.props.onSearchConnection(data);
  };

  componentDidMount() {
    console.log("this.props", this.props);
    this.props.onGetClientTag();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.clientTagRes !== this.props.clientTagRes) {
      this.gettingClientTags(this.props.clientTagRes);
    }
    // if (prevProps.searchConnectionData !== this.props.searchConnectionData) {
    //   this.gettingClientTags(this.props.searchConnectionData);
    // }
  }

  gettingClientTags = (arr) => {
    let newArr = [];
    if (arr && arr.items && arr.items.length > 0) {
      arr.items.map((data) =>
        newArr.push({
          value: data.id,
          label: data.name,
        })
      );
    }

    this.setState({ newClientTags: newArr });
  };

  myChangeHandler = (text) => {
    this.setState({ username: text });
  };

  onChange = (value) => {
    console.log(`selected ${value}`);
  };

  onBlur = () => {
    console.log("blur");
  };

  onFocus = () => {
    console.log("focus");
  };

  onSearch = (val) => {
    console.log("search:", val);
  };

  handleChange = (selectedOption) => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };

  render() {
    const { selectedOption, newClientTags } = this.state;
    const { clientTagRes } = this.props;
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div class="page-container">
            <HeaderBarTabs data={headOption} activeTab="Client Profile" />
            <div className="container">
              <div className="header-title">
                <span>Clients Detail</span>
              </div>
              <div className="form-container">
                <div style={{ flexDirection: "row", display: "flex" }}>
                  <div className="all-client-form">
                    <form style={{ paddingLeft: 40 }}>
                      <div style={{ display: "flex" }}>
                        <div>
                          <div>
                            <p>Client Number</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Last Name</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Mobile</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Company</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Active / Inactive</p>
                            <div class="select-options">
                              <Select
                                value={selectedOption}
                                onChange={this.handleChange}
                                options={options2}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Client Sort</p>
                            <div class="select-options">
                              <Select
                                value={selectedOption}
                                onChange={this.handleChange}
                                options={options2}
                              />
                            </div>
                          </div>
                        </div>
                        <div class="input-div">
                          <div>
                            <p>First Name</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Email</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Passport #</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Surname Stat's With</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Status</p>
                            <div
                              class="select-options"
                              style={{ paddingLeft: 1 }}
                            >
                              <Select
                                value={selectedOption}
                                onChange={this.handleChange}
                                options={options2}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Date Of Birth</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                        </div>
                        <div class="input-div">
                          <div>
                            <p>Job Sector</p>
                            <div class="select-options">
                              <Select
                                value={selectedOption}
                                onChange={this.handleChange}
                                options={options2}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Occupation</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Include Deleted</p>
                            <div class="select-options">
                              <Select
                                value={selectedOption}
                                onChange={this.handleChange}
                                options={options2}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Internal ID</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Family Member</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Visa Type</p>
                            <div class="select-options">
                              <Select
                                value={selectedOption}
                                onChange={this.handleChange}
                                options={options2}
                              />
                            </div>
                          </div>
                        </div>
                        <div class="input-div">
                          <div>
                            <p>Sales Person</p>
                            <div
                              class="select-options"
                              style={{ paddingRight: 1 }}
                            >
                              <Select
                                value={selectedOption}
                                onChange={this.handleChange}
                                options={options2}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Client Type</p>
                            <div
                              class="select-options"
                              style={{ paddingTop: 1, paddingRight: 1 }}
                            >
                              <Select
                                value={selectedOption}
                                onChange={this.handleChange}
                                options={options2}
                              />
                            </div>
                          </div>
                          <div>
                            <p>INZ Client</p>
                            <div class="input-border">
                              <input
                                className="input"
                                placeholder=""
                                type="text"
                                onChange={this.myChangeHandler}
                              />
                            </div>
                          </div>
                          <div>
                            <p>Client Tags</p>
                            <div
                              class="select-options"
                              style={{ paddingBottom: 1, paddingRight: 1 }}
                            >
                              <Select options={newClientTags} />
                              {/* <Select
                                mode="multiple"
                                showArrow
                                placeholder="Select Client Tag"
                                style={{ width: "100%" }}
                              >
                                {clientTagRes &&
                                  clientTagRes.items &&
                                  clientTagRes.items.length > 0 &&
                                  clientTagRes.items.map((item, index) => (
                                    <Option key={index}>{item.name}</Option>
                                  ))} */}
                              {/* </Select> */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div className="button-container">
                <Link style={{ textDecoration: "none", color: "#555555" }}>
                  <div style={{ display: "flex" }}>
                    <div>
                      <span
                        className="all-show-button"
                        style={{ color: "#FFFFFF" }}
                      >
                        SHOW ALL
                      </span>
                    </div>
                    <div>
                      <span
                        onClick={this.searchData}
                        className="button-search"
                        style={{ color: "#FFFFFF" }}
                      >
                        SEARCH
                      </span>
                    </div>
                  </div>
                </Link>
              </div>

              <div className="all-clients-table">
                <div class="main-table-cont">
                  <div className="header-table">
                    <div className="header-tab-10">
                      <span class="header-font">Number</span>
                    </div>
                    <div className="header-tab-15">
                      <span class="header-font">Name</span>
                    </div>
                    <div className="header-tab-15">
                      <span class="header-font">Internal ID</span>
                    </div>
                    <div className="header-tab-15">
                      <span class="header-font">Mobile #</span>
                    </div>
                    <div className="header-tab-20">
                      <span class="header-font">Email</span>
                    </div>
                    <div className="header-tab-20">
                      <span class="header-font">Processing Person</span>
                    </div>
                    <div className="header-tab-10">
                      <span class="header-font">Client Tag</span>
                    </div>
                    <div className="header-tab-5">
                      <span></span>
                    </div>
                  </div>
                  <div className="table-content">
                    <div className="content-index-10">
                      <span class="content-text">111356</span>
                    </div>
                    <div className="content-index-15">
                      <span class="content-text">fiaza chaudhry</span>
                    </div>
                    <div className="content-index-15">
                      <span class="content-text"></span>
                    </div>
                    <div className="content-index-15">
                      <span class="content-text"></span>
                    </div>
                    <div className="content-index-20">
                      <span class="content-text">fiaza@ezymigrate.com</span>
                    </div>
                    <div className="content-index-20">
                      <span class="content-text"></span>
                    </div>
                    <div className="content-index-10">
                      <span class="content-text"></span>
                    </div>
                    <div className="delete-container">
                      <div className="delete-icon">
                        <img src={Images.deleteIcon} />
                      </div>
                    </div>
                  </div>
                  <div className="table-content">
                    <div className="content-index-10">
                      <span class="content-text">111356</span>
                    </div>
                    <div className="content-index-15">
                      <span class="content-text">fiaza chaudhry</span>
                    </div>
                    <div className="content-index-15">
                      <span class="content-text"></span>
                    </div>
                    <div className="content-index-15">
                      <span class="content-text"></span>
                    </div>
                    <div className="content-index-20">
                      <span class="content-text">fiaza@ezymigrate.com</span>
                    </div>
                    <div className="content-index-20">
                      <span class="content-text"></span>
                    </div>
                    <div className="content-index-10">
                      <span class="content-text"></span>
                    </div>
                    <div className="delete-container">
                      <div className="delete-icon">
                        <img src={Images.deleteIcon} />
                      </div>
                    </div>
                  </div>
                  <div className="table-content">
                    <div className="content-index-10">
                      <span class="content-text">111356</span>
                    </div>
                    <div className="content-index-15">
                      <span class="content-text">fiaza chaudhry</span>
                    </div>
                    <div className="content-index-15">
                      <span class="content-text"></span>
                    </div>
                    <div className="content-index-15">
                      <span class="content-text"></span>
                    </div>
                    <div className="content-index-20">
                      <span class="content-text">fiaza@ezymigrate.com</span>
                    </div>
                    <div className="content-index-20">
                      <span class="content-text"></span>
                    </div>
                    <div className="content-index-10">
                      <span class="content-text"></span>
                    </div>
                    <div className="delete-container">
                      <div className="delete-icon">
                        <img src={Images.deleteIcon} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AllClients;
