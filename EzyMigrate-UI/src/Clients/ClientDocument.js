import React from "react";
import "./ClientsStyles.css";
import HeaderBar from "../Components/Header/HeaderBar";
import { EyeFilled } from "@ant-design/icons";

import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import { Images } from "../Themes";
import HeaderBarTabs from "../Components/Header/HeaderTabs";

import ProgressBar from "../Components/Shared/Progressbar";

import headOption from "../Components/Header/HeaderTabOptions";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

class ClientDocument extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeMainTab: "document",
      activeTab: "studentVisa",
      extend: false,
      headerOptions: [],
    };
  }
  componentDidMount() {
    if (this.props.clientTab) {
      var options = headOption(this.props.clientTab);
      this.setState({ headerOptions: options });
    } else {
      this.setState({
        headerOptions: [
          {
            tabName: "Client Profile",
            imageName: Images.clientProfile,
            linkName: "/profile",
          },
          { tabName: "Visas", imageName: Images.visas, linkName: "/visa-flow" },
          {
            tabName: "Admission",
            imageName: Images.admission,
            linkName: "/admission",
          },
          {
            tabName: "Documents",
            imageName: Images.documents,
            linkName: "/documents",
          },
          {
            tabName: "Email",
            imageName: Images.email,
            linkName: "/client-email",
          },
          {
            tabName: "Activities",
            imageName: Images.activities,
            linkName: "/activities",
          },
          {
            tabName: "File Notes",
            imageName: Images.documents,
            linkName: "/file-notes",
          },
          {
            tabName: "Accounts",
            imageName: Images.accounts,
            linkName: "/client-account",
          },
          {
            tabName: "Open Case Management",
            imageName: Images.caseManagement,
            linkName: "/Case-management",
          },
          {
            tabName: "Questionnaire",
            imageName: Images.questionnare,
            linkName: "/client-questionnaire",
          },
          { tabName: "Chat", imageName: Images.supplier, linkName: "" },
          { tabName: "Print Case", imageName: Images.print, linkName: "" },
        ],
      });
    }
  }

  myChangeHandler = (text) => {
    this.setState({ username: text });
  };

  onChange = (value) => {
    console.log(`selected ${value}`);
  };

  onBlur = () => {
    console.log("blur");
  };

  onFocus = () => {
    console.log("focus");
  };

  onSearch = (val) => {
    console.log("search:", val);
  };

  onChangeMainTab = (value) => {
    this.setState({ activeMainTab: value });
  };

  onChangeTab = (value) => {
    this.setState({ activeTab: value });
  };

  handleChange = (selectedOption) => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };

  handleCheck = () => {
    this.setState({ checked: !this.state.checked });
  };

  render() {
    const { selectedOption, headerOptions } = this.state;
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            {headerOptions.length > 0 && (
              <HeaderBarTabs data={headerOptions} activeTab="Documents" />
            )}
            <div className="cd-container">
              <div>
                {/* ===== Design from Client Document Screen ===== */}

                {true && (
                  <div>
                    <div className="cv-gray-cont" style={{ paddingBottom: 15 }}>
                      <div className="cv-row">
                        <div style={{ margin: 10 }}>
                          <div className="cv-assist-cont">
                            <img
                              src={Images.officeMatereial}
                              style={{ width: 15, height: 15 }}
                            />
                            <span
                              className="cv-bold-text"
                              style={{ color: "#19267C", marginLeft: 10 }}
                            >
                              Unparented
                            </span>
                          </div>
                        </div>
                      </div>

                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <div />
                        <div
                          style={{
                            display: "flex",
                            marginBottom: 10,
                            alignItems: "center",
                          }}
                        >
                          <div>
                            <span
                              className="cv-bold-text"
                              style={{ color: "#75868F" }}
                            >
                              Visible To:
                            </span>
                          </div>
                          <div className="cv-circle-lable-cont">
                            <div className="cv-client-circle"></div>
                            <span
                              className="cv-normal-text"
                              style={{ marginLeft: 5 }}
                            >
                              Client
                            </span>
                          </div>
                          {/*<div className="cv-circle-lable-cont">*/}
                          {/*  <div className="cv-agent-circle"></div>*/}
                          {/*  <span*/}
                          {/*    className="cv-normal-text"*/}
                          {/*    style={{ marginLeft: 5 }}*/}
                          {/*  >*/}
                          {/*    Agent*/}
                          {/*  </span>*/}
                          {/*</div>*/}
                          {/*<div className="cv-circle-lable-cont">*/}
                          {/*  <div className="cv-both-circle"></div>*/}
                          {/*  <span*/}
                          {/*    className="cv-normal-text"*/}
                          {/*    style={{ marginLeft: 5 }}*/}
                          {/*  >*/}
                          {/*    Both*/}
                          {/*  </span>*/}
                          {/*</div>*/}
                          <div>
                            <div
                              className="cd-blue-plus-btn-cont"
                              style={{ marginLeft: 15 }}
                            >
                              <div className="cv-plus-cont">
                                <span className="cd-blue-plus-text">+</span>
                              </div>
                              <span className="cv-org-btn-text">
                                ADD DOCUMENTS
                              </span>
                            </div>
                          </div>
                          <div>
                            <div
                              className="cd-blue-plus-btn-cont"
                              style={{ marginLeft: 10 }}
                            >
                              <div style={{ marginRight: 5, display: "flex" }}>
                                <img
                                  src={Images.move}
                                  style={{ width: 15, height: 14 }}
                                />
                              </div>
                              <span className="cv-org-btn-text">
                                MOVE DOCUMENTS
                              </span>
                            </div>
                          </div>
                          <div>
                            <div
                              className="cd-blue-plus-btn-cont"
                              style={{ marginLeft: 10 }}
                            >
                              <div style={{ marginRight: 5, display: "flex" }}>
                                <img
                                  src={Images.backToDoc}
                                  style={{ width: 14, height: 14 }}
                                />
                              </div>
                              <span className="cv-org-btn-text">
                                BACK TO DOCUMENTS
                              </span>
                            </div>
                          </div>
                          <div>
                            <div className="cv-actions-cont">
                              <span
                                className="cv-normal-text"
                                style={{ fontWeight: "500", marginLeft: 0 }}
                              >
                                Actions
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="margin-client">
                        <div
                          onClick={() => this.onChangeTopTab("studentVisa")}
                          className={
                            this.state.activeTab == "studentVisa"
                              ? "cv-active-tab"
                              : "cd-inactive-tab"
                          }
                        >
                          <span
                            className={
                              this.state.activeTab == "studentVisa"
                                ? "cv-active-tab-text"
                                : "cv-inactive-tab-text"
                            }
                          >
                            STUDENT VISA - GENERAL
                          </span>
                        </div>
                        <div
                          onClick={() => this.onChangeTopTab("intermVisa")}
                          className={
                            this.state.activeTab == "intermVisa"
                              ? "cv-active-tab"
                              : "cd-inactive-tab"
                          }
                        >
                          <span
                            className={
                              this.state.activeTab == "intermVisa"
                                ? "cv-active-tab-text"
                                : "cv-inactive-tab-text"
                            }
                          >
                            INTERM VISA
                          </span>
                        </div>
                        <div
                          onClick={() => this.onChangeTopTab("employeVisa")}
                          className={
                            this.state.activeTab == "employeVisa"
                              ? "cv-active-tab"
                              : "cd-inactive-tab"
                          }
                        >
                          <span
                            className={
                              this.state.activeTab == "employeVisa"
                                ? "cv-active-tab-text"
                                : "cv-inactive-tab-text"
                            }
                          >
                            EMPLOYER VISA
                          </span>
                        </div>
                      </div>

                      <div className="cd-white-cont">
                        <div className="cd-doc-head-row">
                          <div
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <div>
                              <input
                                type="checkbox"
                                className="sus-checkbox"
                                onChange={this.handleCheck}
                                defaultChecked={this.state.checked}
                              />
                            </div>
                            <div className="cv-doc-inner-index">
                              <span className="cv-doc-head-text">#</span>
                            </div>
                            <div className="cv-doc-width">
                              <span className="cv-doc-head-text">Document</span>
                            </div>
                          </div>
                          <div>
                            <span className="cv-doc-head-text">Title</span>
                          </div>
                          <div className="cv-width-15">
                            <span className="cv-doc-head-text">Action</span>
                          </div>
                        </div>

                        <div className="cd-table-row"></div>

                        <div
                          className="cv-doc-row"
                          style={{
                            justifyContent: "space-between",
                            paddingRight: 50,
                          }}
                        >
                          <div
                            className="cv-width-52"
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <div>
                              <input
                                type="checkbox"
                                className="sus-checkbox"
                                onChange={this.handleCheck}
                                defaultChecked={this.state.checked}
                              />
                            </div>
                            <div className="cv-doc-inner-index">
                              <span
                                className="cv-doc-text"
                                style={{ color: "#5B5B5B" }}
                              >
                                1
                              </span>
                            </div>
                            <div className="cv-doc-width">
                              <div>
                                <span
                                  className="cv-doc-text"
                                  style={{ color: "#1081B8" }}
                                >
                                  Merge-66598939340573894-PDF.pdf
                                </span>
                              </div>
                              <div
                                className="cv-doc-date-text-cont"
                                style={{ height: 10 }}
                              >
                                <span className="cv-doc-date-text"></span>
                              </div>
                            </div>
                          </div>

                          <div style={{ display: "block" }}>
                            <div style={{ display: "flex" }}>
                              <div className="cv-action-icons-border">
                                <img
                                  src={Images.download}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.visibility}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.download}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div className="cv-action-icons-border">
                                <img
                                  src={Images.edit}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.multimediaBlue}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.deleteIcon}
                                  className="cv-action-icon"
                                />
                              </div>
                            </div>
                          </div>
                        </div>

                        <div
                          className="cv-doc-row"
                          style={{
                            justifyContent: "space-between",
                            paddingRight: 50,
                          }}
                        >
                          <div
                            className="cv-width-52"
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <div>
                              <input
                                type="checkbox"
                                className="sus-checkbox"
                                onChange={this.handleCheck}
                                defaultChecked={this.state.checked}
                              />
                            </div>
                            <div className="cv-doc-inner-index">
                              <span
                                className="cv-doc-text"
                                style={{ color: "#5B5B5B" }}
                              >
                                2
                              </span>
                            </div>
                            <div className="cv-doc-width">
                              <div>
                                <span
                                  className="cv-doc-text"
                                  style={{ color: "#1081B8" }}
                                >
                                  Merge-66598939340573894-PDF.pdf
                                </span>
                              </div>
                              <div className="cv-doc-date-text-cont">
                                <span className="cv-doc-date-text">
                                  12/02/2019 | 20 KB
                                </span>
                              </div>
                            </div>
                          </div>

                          <div style={{ display: "block" }}>
                            <div style={{ display: "flex" }}>
                              <div className="cv-action-icons-border">
                                <img
                                  src={Images.download}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.visibility}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.download}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.edit}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.multimediaBlue}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.deleteIcon}
                                  className="cv-action-icon"
                                />
                              </div>
                            </div>
                          </div>
                        </div>

                        <div
                          className="cv-doc-row"
                          style={{
                            justifyContent: "space-between",
                            paddingRight: 50,
                          }}
                        >
                          <div
                            className="cv-width-52"
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <div>
                              <input
                                type="checkbox"
                                className="sus-checkbox"
                                onChange={this.handleCheck}
                                defaultChecked={this.state.checked}
                              />
                            </div>
                            <div className="cv-doc-inner-index">
                              <span
                                className="cv-doc-text"
                                style={{ color: "#5B5B5B" }}
                              >
                                3
                              </span>
                            </div>
                            <div className="cv-doc-width">
                              <div>
                                <span
                                  className="cv-doc-text"
                                  style={{ color: "#1081B8" }}
                                >
                                  Merge-66598939340573894-PDF.pdf
                                </span>
                              </div>
                              <div className="cv-doc-date-text-cont">
                                <span className="cv-doc-date-text">
                                  12/02/2019 | 20 KB
                                </span>
                              </div>
                            </div>
                          </div>

                          <div style={{ display: "block" }}>
                            <div style={{ display: "flex" }}>
                              <div className="cv-action-icons-border">
                                <img
                                  src={Images.download}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.visibility}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.download}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.edit}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.multimediaBlue}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.deleteIcon}
                                  className="cv-action-icon"
                                />
                              </div>
                            </div>
                          </div>
                        </div>

                        <div
                          className="cv-doc-row"
                          style={{
                            justifyContent: "space-between",
                            paddingRight: 50,
                          }}
                        >
                          <div
                            className="cv-width-52"
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <div>
                              <input
                                type="checkbox"
                                className="sus-checkbox"
                                onChange={this.handleCheck}
                                defaultChecked={this.state.checked}
                              />
                            </div>
                            <div className="cv-doc-inner-index">
                              <span
                                className="cv-doc-text"
                                style={{ color: "#5B5B5B" }}
                              >
                                3
                              </span>
                            </div>
                            <div className="cv-doc-width">
                              <div>
                                <span
                                  className="cv-doc-text"
                                  style={{ color: "#1081B8" }}
                                >
                                  Merge-66598939340573894-PDF.pdf
                                </span>
                              </div>
                              <div className="cv-doc-date-text-cont">
                                <span className="cv-doc-date-text">
                                  12/02/2019 | 20 KB
                                </span>
                              </div>
                            </div>
                          </div>

                          <div style={{ display: "block" }}>
                            <div style={{ display: "flex" }}>
                              <div className="cv-action-icons-border">
                                <img
                                  src={Images.download}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.visibility}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.download}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.edit}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.multimediaBlue}
                                  className="cv-action-icon"
                                />
                              </div>
                              <div
                                className="cv-action-icons-border"
                                style={{ marginLeft: 5 }}
                              >
                                <img
                                  src={Images.deleteIcon}
                                  className="cv-action-icon"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}

                {/* ===== Design from Client Document tab Screen ===== */}

                {false && (
                  <div>
                    <div className="cv-gray-cont" style={{ paddingBottom: 15 }}>
                      <div className="sus-tab-container">
                        <div
                          onClick={() => this.onChangeMainTab("document")}
                          className={
                            this.state.activeMainTab == "document"
                              ? "cd-doc-active-tab"
                              : "cd-doc-inactive-tab"
                          }
                        >
                          <span
                            className={
                              this.state.activeMainTab == "document"
                                ? "cd-doc-active-tab-text"
                                : "cd-doc-inactive-tab-text"
                            }
                          >
                            DOCUMENT
                          </span>
                        </div>
                        <div
                          onClick={() =>
                            this.onChangeMainTab("documentChecklist")
                          }
                          className={
                            this.state.activeMainTab == "documentChecklist"
                              ? "cd-doc-active-tab"
                              : "cd-doc-inactive-tab"
                          }
                        >
                          <span
                            className={
                              this.state.activeMainTab == "documentChecklist"
                                ? "cd-doc-active-tab-text"
                                : "cd-doc-inactive-tab-text"
                            }
                          >
                            DOCUMENT CHECKLIST
                          </span>
                        </div>
                      </div>
                      {this.state.activeMainTab == "document" && (
                        <div className="cd-blue-border-cont">
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                              marginRight: 20,
                            }}
                          >
                            <div />
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                              }}
                            >
                              <div />
                              <div
                                style={{
                                  display: "flex",
                                  marginBottom: 10,
                                  alignItems: "center",
                                }}
                              >
                                <div>
                                  <span
                                    className="cv-bold-text"
                                    style={{ color: "#75868F" }}
                                  >
                                    Visible To:
                                  </span>
                                </div>
                                <div className="cv-circle-lable-cont">
                                  <div className="cv-client-circle"></div>
                                  <span
                                    className="cv-normal-text"
                                    style={{ marginLeft: 5 }}
                                  >
                                    Client
                                  </span>
                                </div>
                                {/*<div className="cv-circle-lable-cont">*/}
                                {/*  <div className="cv-agent-circle"></div>*/}
                                {/*  <span*/}
                                {/*    className="cv-normal-text"*/}
                                {/*    style={{ marginLeft: 5 }}*/}
                                {/*  >*/}
                                {/*    Agent*/}
                                {/*  </span>*/}
                                {/*</div>*/}
                                {/*<div className="cv-circle-lable-cont">*/}
                                {/*  <div className="cv-both-circle"></div>*/}
                                {/*  <span*/}
                                {/*    className="cv-normal-text"*/}
                                {/*    style={{ marginLeft: 5 }}*/}
                                {/*  >*/}
                                {/*    Both*/}
                                {/*  </span>*/}
                                {/*</div>*/}
                                <div>
                                  <div
                                    className="cv-org-btn-cont padding-btn"
                                    style={{ marginLeft: 25 }}
                                  >
                                    <div className="cv-plus-cont">
                                      <span className="cv-plus-icon">+</span>
                                    </div>
                                    <span className="cv-org-btn-text">
                                      ADD DOCUMENTS
                                    </span>
                                  </div>
                                </div>
                                <div>
                                  <div className="cv-actions-cont">
                                    <span
                                      className="cv-normal-text"
                                      style={{
                                        fontWeight: "500",
                                        marginLeft: 0,
                                      }}
                                    >
                                      Actions
                                    </span>
                                  </div>
                                </div>
                                <div
                                  className="cv-print-icon-cont"
                                  style={{ marginLeft: 10 }}
                                >
                                  <img
                                    src={Images.printBlue}
                                    className="profile-print-icon"
                                  />
                                </div>
                                <div className="cv-extend-icon-cont">
                                  <img
                                    src={Images.extendIcon}
                                    className="cv-extend-icon"
                                    style={{ transform: `rotate(90deg)` }}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="sus-tab-container">
                            <div
                              onClick={() => this.onChangeTopTab("studentVisa")}
                              className={
                                this.state.activeTab == "studentVisa"
                                  ? "cd-doc-active-tab"
                                  : "cd-doc-sub-inactive-tab"
                              }
                            >
                              <span
                                className={
                                  this.state.activeTab == "studentVisa"
                                    ? "cd-doc-active-tab-text"
                                    : "cv-inactive-tab-text"
                                }
                              >
                                Student Visa - Gen
                              </span>
                            </div>
                            <div
                              onClick={() => this.onChangeTopTab("intermVisa")}
                              className={
                                this.state.activeTab == "intermVisa"
                                  ? "cd-doc-active-tab"
                                  : "cd-doc-sub-inactive-tab"
                              }
                            >
                              <span
                                className={
                                  this.state.activeTab == "intermVisa"
                                    ? "cd-doc-active-tab-text"
                                    : "cv-inactive-tab-text"
                                }
                              >
                                Interim Visa
                              </span>
                            </div>
                            <div
                              onClick={() => this.onChangeTopTab("investor")}
                              className={
                                this.state.activeTab == "investor"
                                  ? "cd-doc-active-tab"
                                  : "cd-doc-sub-inactive-tab"
                              }
                            >
                              <span
                                className={
                                  this.state.activeTab == "investor"
                                    ? "cd-doc-active-tab-text"
                                    : "cv-inactive-tab-text"
                                }
                              >
                                Investor
                              </span>
                            </div>
                            <div
                              onClick={() => this.onChangeTopTab("citizenShip")}
                              className={
                                this.state.activeTab == "citizenShip"
                                  ? "cd-doc-active-tab"
                                  : "cd-doc-sub-inactive-tab"
                              }
                            >
                              <span
                                className={
                                  this.state.activeTab == "citizenShip"
                                    ? "cd-doc-active-tab-text"
                                    : "cv-inactive-tab-text"
                                }
                              >
                                Citizenship
                              </span>
                            </div>
                            <div
                              onClick={() => this.onChangeTopTab("residence")}
                              className={
                                this.state.activeTab == "residence"
                                  ? "cd-doc-active-tab"
                                  : "cd-doc-sub-inactive-tab"
                              }
                            >
                              <span
                                className={
                                  this.state.activeTab == "residence"
                                    ? "cd-doc-active-tab-text"
                                    : "cv-inactive-tab-text"
                                }
                              >
                                Residence
                              </span>
                            </div>
                          </div>

                          <div className="cd-doc-checklist-cont">
                            <div className="cv-doc-head-row">
                              <div
                                className="cv-width-55"
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <div>
                                  <input
                                    type="checkbox"
                                    className="sus-checkbox"
                                    onChange={this.handleCheck}
                                    defaultChecked={this.state.checked}
                                  />
                                </div>
                                <div className="cv-doc-inner-index">
                                  <span className="cv-doc-head-text">#</span>
                                </div>
                                <div className="cv-doc-width">
                                  <span className="cv-doc-head-text">
                                    Document
                                  </span>
                                </div>
                              </div>
                              <div className="cv-width-17">
                                <span className="cv-doc-head-text">Title</span>
                              </div>
                              <div className="cv-width-13">
                                <span className="cv-doc-head-text">Type</span>
                              </div>
                              <div className="cv-width-15">
                                <span className="cv-doc-head-text">Action</span>
                              </div>
                            </div>

                            <div className="cd-table-row"></div>

                            <div className="cv-doc-row">
                              <div
                                className="cv-width-52"
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <div>
                                  <input
                                    type="checkbox"
                                    className="sus-checkbox"
                                    onChange={this.handleCheck}
                                    defaultChecked={this.state.checked}
                                  />
                                </div>
                                <div className="cv-doc-inner-index">
                                  <span
                                    className="cv-doc-text"
                                    style={{ color: "#5B5B5B" }}
                                  >
                                    1
                                  </span>
                                </div>
                                <div className="cv-doc-width">
                                  <div>
                                    <span
                                      className="cv-doc-text"
                                      style={{ color: "#1081B8" }}
                                    >
                                      Merge-66598939340573894-PDF.pdf
                                    </span>
                                  </div>
                                  <div className="cv-doc-date-text-cont">
                                    <span className="cv-doc-date-text">
                                      12/02/2019 | 20 KB
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="cv-width-20"
                                style={{ padding: 1 }}
                              >
                                <div className="cv-title-box">
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontSize: 13, marginLeft: 5 }}
                                  >
                                    show data
                                  </span>
                                </div>
                              </div>
                              <div
                                className="cv-width-13"
                                style={{ padding: 1 }}
                              >
                                <div
                                  className="cv-title-box"
                                  style={{ width: "80%" }}
                                >
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontSize: 13, marginLeft: 5 }}
                                  >
                                    show data
                                  </span>
                                </div>
                              </div>
                              <div style={{ display: "block" }}>
                                <div style={{ display: "flex" }}>
                                  <div className="cv-action-icons-border">
                                    <img
                                      src={Images.download}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.visibility}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.download}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.edit}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.multimediaBlue}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.deleteIcon}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className="cv-doc-row">
                              <div
                                className="cv-width-52"
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <div>
                                  <input
                                    type="checkbox"
                                    className="sus-checkbox"
                                    onChange={this.handleCheck}
                                    defaultChecked={this.state.checked}
                                  />
                                </div>
                                <div className="cv-doc-inner-index">
                                  <span
                                    className="cv-doc-text"
                                    style={{ color: "#5B5B5B" }}
                                  >
                                    2
                                  </span>
                                </div>
                                <div className="cv-doc-width">
                                  <div>
                                    <span
                                      className="cv-doc-text"
                                      style={{ color: "#1081B8" }}
                                    >
                                      Merge-66598939340573894-PDF.pdf
                                    </span>
                                  </div>
                                  <div className="cv-doc-date-text-cont">
                                    <span className="cv-doc-date-text">
                                      12/02/2019 | 20 KB
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="cv-width-20"
                                style={{ padding: 1 }}
                              >
                                <div className="cv-title-box">
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontSize: 13, marginLeft: 5 }}
                                  >
                                    show data
                                  </span>
                                </div>
                              </div>
                              <div
                                className="cv-width-13"
                                style={{ padding: 1 }}
                              >
                                <div
                                  className="cv-title-box"
                                  style={{ width: "80%" }}
                                >
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontSize: 13, marginLeft: 5 }}
                                  >
                                    show data
                                  </span>
                                </div>
                              </div>
                              <div style={{ display: "block" }}>
                                <div style={{ display: "flex" }}>
                                  <div className="cv-action-icons-border">
                                    <img
                                      src={Images.download}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <EyeFilled style={{ color: "#4BC5FF" }} />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.download}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.edit}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.multimediaBlue}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.deleteIcon}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className="cv-doc-row">
                              <div
                                className="cv-width-52"
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <div>
                                  <input
                                    type="checkbox"
                                    className="sus-checkbox"
                                    onChange={this.handleCheck}
                                    defaultChecked={this.state.checked}
                                  />
                                </div>
                                <div className="cv-doc-inner-index">
                                  <span
                                    className="cv-doc-text"
                                    style={{ color: "#5B5B5B" }}
                                  >
                                    3
                                  </span>
                                </div>
                                <div className="cv-doc-width">
                                  <div>
                                    <span
                                      className="cv-doc-text"
                                      style={{ color: "#1081B8" }}
                                    >
                                      Merge-66598939340573894-PDF.pdf
                                    </span>
                                  </div>
                                  <div className="cv-doc-date-text-cont">
                                    <span className="cv-doc-date-text">
                                      12/02/2019 | 20 KB
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="cv-width-20"
                                style={{ padding: 1 }}
                              >
                                <div className="cv-title-box">
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontSize: 13, marginLeft: 5 }}
                                  >
                                    show data
                                  </span>
                                </div>
                              </div>
                              <div
                                className="cv-width-13"
                                style={{ padding: 1 }}
                              >
                                <div
                                  className="cv-title-box"
                                  style={{ width: "80%" }}
                                >
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontSize: 13, marginLeft: 5 }}
                                  >
                                    show data
                                  </span>
                                </div>
                              </div>
                              <div style={{ display: "block" }}>
                                <div style={{ display: "flex" }}>
                                  <div className="cv-action-icons-border">
                                    <img
                                      src={Images.download}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.visibility}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.download}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.edit}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.multimediaBlue}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.deleteIcon}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className="cv-doc-row">
                              <div
                                className="cv-width-52"
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <div>
                                  <input
                                    type="checkbox"
                                    className="sus-checkbox"
                                    onChange={this.handleCheck}
                                    defaultChecked={this.state.checked}
                                  />
                                </div>
                                <div className="cv-doc-inner-index">
                                  <span
                                    className="cv-doc-text"
                                    style={{ color: "#5B5B5B" }}
                                  >
                                    1
                                  </span>
                                </div>
                                <div className="cv-doc-width">
                                  <div>
                                    <span
                                      className="cv-doc-text"
                                      style={{ color: "#1081B8" }}
                                    >
                                      Merge-66598939340573894-PDF.pdf
                                    </span>
                                  </div>
                                  <div className="cv-doc-date-text-cont">
                                    <span className="cv-doc-date-text">
                                      12/02/2019 | 20 KB
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="cv-width-20"
                                style={{ padding: 1 }}
                              >
                                <div className="cv-title-box">
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontSize: 13, marginLeft: 5 }}
                                  >
                                    show data
                                  </span>
                                </div>
                              </div>
                              <div
                                className="cv-width-13"
                                style={{ padding: 1 }}
                              >
                                <div
                                  className="cv-title-box"
                                  style={{ width: "80%" }}
                                >
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontSize: 13, marginLeft: 5 }}
                                  >
                                    show data
                                  </span>
                                </div>
                              </div>
                              <div style={{ display: "block" }}>
                                <div style={{ display: "flex" }}>
                                  <div className="cv-action-icons-border">
                                    <img
                                      src={Images.download}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.visibility}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.download}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.edit}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.multimediaBlue}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.deleteIcon}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className="cv-doc-row">
                              <div
                                className="cv-width-52"
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <div>
                                  <input
                                    type="checkbox"
                                    className="sus-checkbox"
                                    onChange={this.handleCheck}
                                    defaultChecked={this.state.checked}
                                  />
                                </div>
                                <div className="cv-doc-inner-index">
                                  <span
                                    className="cv-doc-text"
                                    style={{ color: "#5B5B5B" }}
                                  >
                                    4
                                  </span>
                                </div>
                                <div className="cv-doc-width">
                                  <div>
                                    <span
                                      className="cv-doc-text"
                                      style={{ color: "#1081B8" }}
                                    >
                                      Merge-66598939340573894-PDF.pdf
                                    </span>
                                  </div>
                                  <div className="cv-doc-date-text-cont">
                                    <span className="cv-doc-date-text">
                                      12/02/2019 | 20 KB
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="cv-width-20"
                                style={{ padding: 1 }}
                              >
                                <div className="cv-title-box">
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontSize: 13, marginLeft: 5 }}
                                  >
                                    show data
                                  </span>
                                </div>
                              </div>
                              <div
                                className="cv-width-13"
                                style={{ padding: 1 }}
                              >
                                <div
                                  className="cv-title-box"
                                  style={{ width: "80%" }}
                                >
                                  <span
                                    className="cv-normal-text"
                                    style={{ fontSize: 13, marginLeft: 5 }}
                                  >
                                    show data
                                  </span>
                                </div>
                              </div>
                              <div style={{ display: "block" }}>
                                <div style={{ display: "flex" }}>
                                  <div className="cv-action-icons-border">
                                    <img
                                      src={Images.download}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.visibility}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.download}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.edit}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.multimediaBlue}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                  <div
                                    className="cv-action-icons-border"
                                    style={{ marginLeft: 5 }}
                                  >
                                    <img
                                      src={Images.deleteIcon}
                                      className="cv-action-icon"
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      )}

                      {this.state.activeMainTab == "documentChecklist" && (
                        <div className="cd-blue-border-cont">
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                              marginRight: 20,
                            }}
                          >
                            <div />
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                              }}
                            >
                              <div />
                              <div
                                style={{
                                  display: "flex",
                                  marginBottom: 10,
                                  alignItems: "center",
                                }}
                              >
                                <div>
                                  <div className="cv-actions-cont">
                                    <span
                                      className="cv-normal-text"
                                      style={{
                                        fontWeight: "500",
                                        marginLeft: 0,
                                      }}
                                    >
                                      Actions
                                    </span>
                                  </div>
                                </div>
                                <div
                                  className="cv-print-icon-cont"
                                  style={{ marginLeft: 10 }}
                                >
                                  <img
                                    src={Images.printBlue}
                                    className="profile-print-icon"
                                  />
                                </div>
                                <div className="cv-extend-icon-cont">
                                  <img
                                    src={Images.extendIcon}
                                    className="cv-extend-icon"
                                    style={{ transform: `rotate(90deg)` }}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="cd-doc-checklist-cont">
                            <div
                              className="cv-doc-head-row"
                              style={{
                                paddingTop: 5,
                                paddingBottom: 5,
                                borderBottom: 0,
                                justifyContent: "space-between",
                              }}
                            >
                              <div
                                className="cd-header-title-index"
                                style={{ paddingLeft: 40 }}
                              >
                                <span className="cv-doc-head-text">Title</span>
                              </div>
                              <div
                                className="cd-header-title-index"
                                style={{ justifyContent: "center" }}
                              >
                                <span className="cv-doc-head-text">
                                  Link Client
                                </span>
                              </div>
                              <div
                                className="cd-header-title-index"
                                style={{
                                  borderRight: 0,
                                  justifyContent: "center",
                                }}
                              >
                                <span className="cv-doc-head-text">Action</span>
                              </div>
                            </div>

                            <div
                              className="cd-top-row-table"
                              style={{
                                paddingTop: 5,
                                height: 20,
                                paddingBottom: 5,
                                justifyContent: "space-between",
                              }}
                            >
                              <div
                                className="cd-header-title-index"
                                style={{ paddingLeft: 40 }}
                              ></div>
                              <div
                                className="cd-header-title-index"
                                style={{ justifyContent: "center" }}
                              ></div>
                              <div
                                className="cd-header-title-index"
                                style={{
                                  borderRight: 0,
                                  justifyContent: "center",
                                }}
                              ></div>
                            </div>

                            <div
                              className="cd-top-row-table"
                              style={{
                                paddingTop: 5,
                                paddingBottom: 5,
                                borderTop: 0,
                                justifyContent: "space-between",
                              }}
                            >
                              <div
                                className="cd-header-title-index"
                                style={{ paddingLeft: 40 }}
                              >
                                <span className="cv-normal-text">
                                  WORK VISA
                                </span>
                              </div>
                              <div
                                className="cd-header-title-index"
                                style={{ justifyContent: "center" }}
                              >
                                <span className="cv-normal-text">
                                  Please{" "}
                                  <span style={{ color: "#0172BB" }}>
                                    click here
                                  </span>{" "}
                                  to open
                                </span>
                              </div>
                              <div
                                className="cd-header-title-index"
                                style={{
                                  borderRight: 0,
                                  justifyContent: "center",
                                }}
                              >
                                <div className="cd-send-btn-cont">
                                  <span className="cd-send-btn-text">SEND</span>
                                </div>
                              </div>
                            </div>

                            <div
                              className="cd-top-row-table"
                              style={{
                                paddingTop: 5,
                                paddingBottom: 5,
                                borderTop: 0,
                                justifyContent: "space-between",
                              }}
                            >
                              <div
                                className="cd-header-title-index"
                                style={{ paddingLeft: 40 }}
                              >
                                <span className="cv-normal-text">
                                  WORK VISA
                                </span>
                              </div>
                              <div
                                className="cd-header-title-index"
                                style={{ justifyContent: "center" }}
                              >
                                <span className="cv-normal-text">
                                  Please{" "}
                                  <span style={{ color: "#0172BB" }}>
                                    click here
                                  </span>{" "}
                                  to open
                                </span>
                              </div>
                              <div
                                className="cd-header-title-index"
                                style={{
                                  borderRight: 0,
                                  justifyContent: "center",
                                }}
                              >
                                <div className="cd-send-btn-cont">
                                  <span className="cd-send-btn-text">SEND</span>
                                </div>
                              </div>
                            </div>

                            <div
                              className="cd-top-row-table"
                              style={{
                                paddingTop: 5,
                                paddingBottom: 5,
                                borderTop: 0,
                                justifyContent: "space-between",
                              }}
                            >
                              <div
                                className="cd-header-title-index"
                                style={{ paddingLeft: 40 }}
                              >
                                <span className="cv-normal-text">
                                  WORK VISA
                                </span>
                              </div>
                              <div
                                className="cd-header-title-index"
                                style={{ justifyContent: "center" }}
                              >
                                <span className="cv-normal-text">
                                  Please{" "}
                                  <span style={{ color: "#0172BB" }}>
                                    click here
                                  </span>{" "}
                                  to open
                                </span>
                              </div>
                              <div
                                className="cd-header-title-index"
                                style={{
                                  borderRight: 0,
                                  justifyContent: "center",
                                }}
                              >
                                <div className="cd-send-btn-cont">
                                  <span className="cd-send-btn-text">SEND</span>
                                </div>
                              </div>
                            </div>

                            <div
                              className="cd-top-row-table"
                              style={{
                                paddingTop: 5,
                                paddingBottom: 5,
                                borderTop: 0,
                                justifyContent: "space-between",
                              }}
                            >
                              <div
                                className="cd-header-title-index"
                                style={{ paddingLeft: 40 }}
                              >
                                <span className="cv-normal-text">
                                  WORK VISA
                                </span>
                              </div>
                              <div
                                className="cd-header-title-index"
                                style={{ justifyContent: "center" }}
                              >
                                <span className="cv-normal-text">
                                  Please{" "}
                                  <span style={{ color: "#0172BB" }}>
                                    click here
                                  </span>{" "}
                                  to open
                                </span>
                              </div>
                              <div
                                className="cd-header-title-index"
                                style={{
                                  borderRight: 0,
                                  justifyContent: "center",
                                }}
                              >
                                <div className="cd-send-btn-cont">
                                  <span className="cd-send-btn-text">SEND</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ClientDocument;
