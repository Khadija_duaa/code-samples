import React, { Fragment } from "react";
import CreateEmailForm from "./CreateEmailForm";

const createEmail = ({
  onGetLetterTemplates,
  LetterTemplatesRes,

  onSendEmailLink,
  handleKey,
  onAddEmployerEmail,
  onAddDraftClient,

  onGetClientProfile,
  clientProfileData,

  setLoading,
  loading,

  onGetClientHistory,

  onUploadAvatar,

  onAddEmailDocument,
  emailDocumentRes,

  setHandleKey,

  onGetClientFamilyEmails,
  clientEmailRes,

  onGetEmployerDocument,
  employerDocumentRes,

  onGetImapSettingss,
  imapSettingRes,

  onGetSignature,
  signatureRes,

  onGetPdf,

  onGetLink,
  onGetDocuments,

  documentRes,
  onGetDocumentDownload,

  onDeleteClientEmail,
}) => {
  // useEffect(() => {
  //   setLoading(true);
  //
  //   onGetEmployerCheckList(selectedBranchId).then((res) => {
  //     setChecklistItem(res.payload.items);
  //     setLoading(false);
  //   });
  // }, [onGetEmployerCheckList]);

  // const handleCancel = () => {
  //   setIsModalVisible(false);
  // };

  return (
    <Fragment>
      <CreateEmailForm
        onGetLetterTemplates={onGetLetterTemplates}
        LetterTemplatesRes={LetterTemplatesRes}
        onSendEmailLink={onSendEmailLink}
        onAddEmployerEmail={onAddEmployerEmail}
        onAddDraftClient={onAddDraftClient}
        onGetClientProfile={onGetClientProfile}
        clientProfileData={clientProfileData}
        setLoading={setLoading}
        loading={loading}
        onGetClientHistory={onGetClientHistory}
        onUploadAvatar={onUploadAvatar}
        onAddEmailDocument={onAddEmailDocument}
        emailDocumentRes={emailDocumentRes}
        setHandleKey={setHandleKey}
        onGetClientFamilyEmails={onGetClientFamilyEmails}
        clientEmailRes={clientEmailRes}
        onGetEmployerDocument={onGetEmployerDocument}
        employerDocumentRes={employerDocumentRes}
        onGetImapSettingss={onGetImapSettingss}
        imapSettingRes={imapSettingRes}
        onGetSignature={onGetSignature}
        signatureRes={signatureRes}
        onGetPdf={onGetPdf}
        onGetLink={onGetLink}
        onGetDocuments={onGetDocuments}
        documentRes={documentRes}
        onGetDocumentDownload={onGetDocumentDownload}
        onDeleteClientEmail={onDeleteClientEmail}
        handleKey={handleKey}
      />
    </Fragment>
  );
};

export default createEmail;
