import React, { useState, useEffect } from "react";
import { Form, Input, Button, Layout, Image, Row, Col, message } from "antd";
import { Link } from "react-router-dom";
import { Images } from "./../../Themes";
import navMenu from "./../../images/nav-collaps.png";
import SignUpEmail from "./SignUp";

const { Header, Footer, Content } = Layout;

const formData = new FormData();

const SignUp = ({
  onAddSignUpEmail,
  addSignUpRes,
  onGetTextEncoding,
  textEncodingRes,

  onSendEmailLink
}) => {
  return (
    <div className="bg-white">
      <Layout className="signup-header">
        <Header>
          <div className="d-flex sign-up-head">
            <div>
              <Image src={Images.logo} preview={false} />
            </div>
            <div className="d-flex">
              <div
                className="d-flex align-items"
                style={{ marginRight: "20px" }}
              >
                <div className="head-link">
                  <Image src={navMenu} preview={false} />
                </div>
                <div className="head-text">
                  <Link to="/login">
                    <h1>MENU</h1>
                  </Link>
                </div>
              </div>

              <div className="d-flex align-items">
                <div className="head-link">
                  <Image src={navMenu} preview={false} />
                </div>
                <div className="head-text">
                  <Link to="/login">
                    <h1>Sign In</h1>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </Header>
        <Content className="">
          <SignUpEmail
            onAddSignUpEmail={onAddSignUpEmail}
            addSignUpRes={addSignUpRes}
          />
        </Content>
      </Layout>
    </div>
  );
};

export default SignUp;
