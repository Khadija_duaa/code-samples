import React, { useState, useEffect } from "react";
import { Form, Input, Button, Layout, Image, Row, Col, message } from "antd";
import { Link } from "react-router-dom";
import { Images } from "./../../Themes";
import navMenu from "./../../images/nav-collaps.png";
import { Fragment } from "react";
import { VideoCameraOutlined } from "@ant-design/icons";
import images from "../../Themes/Images";
import svgsSignUp from "./../../svgs/sign-up-feature.svg";

const { Header, Footer, Content } = Layout;

const formData = new FormData();

const SignUpEmail = ({
  onAddSignUpEmail,
  addSignUpRes,
  onGetTextEncoding,
  textEncodingRes,

  onSendEmailLink,
}) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    // setLoading(true);
    console.log("valuesvaluesvalues", values);

    const data = {
      email: values && values.email,
      uri:
        // "http://localhost:3000/sign-up-form?params=",
        "https://app-stage.ezymigrate.co.nz/sign-up-form?params=",
    };

    onAddSignUpEmail(data);
  };

  return (
    <Fragment>
      <div className="signup-contaienr">
        <div className="signup-email ">
          <Form
            style={{ marginTop: "135px" }}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            form={form}
          >
            <Row gutter={10}>
              <Col className="email-input">
                <Form.Item
                  label=""
                  name="email"
                  rules={[{ required: true, message: "Required!" }]}
                >
                  <Input
                    placeholder="Email Address"
                    style={{ width: "350px" }}
                  />
                </Form.Item>
              </Col>
              <div className="sign-up-btn">
                <Col>
                  <Form.Item>
                    <Button
                      style={{ width: "150px" }}
                      type="primary"
                      htmlType="submit"
                    >
                      Sign Up Now
                    </Button>
                  </Form.Item>
                </Col>
              </div>
            </Row>
          </Form>
        </div>
        <div className="video-btn">
          <Button
            icon={<VideoCameraOutlined />}
            style={{ width: "250px" }}
            type="primary"
            htmlType="submit"
          >
            Watch Video
          </Button>
        </div>
      </div>
      <div className="quick-box">
        <Row>
          <Col>
            <span className="feature-content"> Quick Breakdown </span>
            <span className="feature-content-lg"> of Our Features </span>
          </Col>
        </Row>
        <Row>
          <Col>
            <p>Start making your Immigration Business more efficient today.</p>
          </Col>
        </Row>

        <div className="feature-btn ">
          <Button type="primary" htmlType="submit">
            More Feature
          </Button>
        </div>
        <div className="features-img">
          <Image src={images.Featuresimg} preview={false} />
        </div>
      </div>
      <div className="amzing-feature">
        <Row>
          <Col>
            <span className="feature-content-lg">
              Amazing <svgsSignUp />
            </span>{" "}
            <span className="feature-content">Features</span>
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};

export default SignUpEmail;
