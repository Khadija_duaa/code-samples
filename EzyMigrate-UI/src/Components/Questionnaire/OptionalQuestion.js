// @flow
import React, { useState, useEffect } from "react";
import { Images } from "../../Themes";
import {
  message,
  Spin,
  Button,
  Upload,
  Select,
  Form,
  Modal,
  Checkbox,
  Input,
  Radio
} from "antd";

function OptionalQuestion(Props) {
  const [show, setShow] = useState(false);
  // const [name, setName] = useState(Props.manager.fullName);
  // const [email, setEmail] = useState(Props.manager.alternateEmail);

  const [fieldName, setFieldName] = useState("");
  const [fieldKey, setFieldKey] = useState("");
  const [connectionsData, setConnectionsData] = useState("");
  const [relation, setRelation] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);

  useEffect(() => {
    setFieldName(Props.field.name);
    setFieldKey(Props.field.fieldKey);
  }, [Props]);

  const userOwner = localStorage.getItem("userOwner");
  const userManager = localStorage.getItem("userManager");
  const userId = localStorage.getItem("userId");

  const { Option } = Select;
  console.log(Props.options);
  //  

  return (
    <div>
      <div
        class="cq-form-cont"
        style={{
          width: "100%",
          marginLeft: 0,
          marginTop: 20,
          padding: 30,
          backgroundColor: "#FFF7EC"
        }}
      >
        {Props.options && (
          <div>
            <Form.Item
              {...Props.field}
              name={[fieldName, "optionSelected"]}
              fieldKey={[fieldKey, "optionSelected"]}
            >
              <Radio.Group>
                {Props.options.map((option, index) => {
                   
                  return <Radio value={option}>{option}</Radio>;
                })}
              </Radio.Group>
            </Form.Item>
          </div>
        )}
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            width: "80%"
          }}
        >
          <p className="cq-label-text">Question</p>
          <div class="profile-input-border" style={{ width: "60%" }}>
            <Form.Item
              {...Props.field}
              name={[fieldName, "questionName"]}
              fieldKey={[fieldKey, "questionName"]}
            >
              <Input className="profile-input" />
            </Form.Item>
          </div>
        </div>
        <div>
          <Form.Item
            {...Props.field}
            name={[fieldName, "isRequired"]}
            fieldKey={[fieldKey, "isRequired"]}
            valuePropName="checked"
          >
            <Checkbox>Is Required</Checkbox>
          </Form.Item>
        </div>
        <div
          style={{
            display: "flex",
            marginTop: 50,
            justifyContent: "space-between",
            width: "80%"
          }}
        >
          <p class="cq-label-text">Answer Type</p>
          <div style={{ width: "60%", paddingLeft: 1 }}>
            <div style={{ paddingLeft: 7 }}>
              <Form.Item
                {...Props.field}
                name={[fieldName, "answerType"]}
                fieldKey={[fieldKey, "answerType"]}
              >
                <Select>
                  <Option value="1">Textbox</Option>
                  <Option value="2">Date </Option>
                  <Option value="3">Drop down list</Option>
                  <Option value="4">Radio button</Option>
                  <Option value="5">Checkbox</Option>
                  <Option value="6">Month</Option>
                  <Option value="7">Textarea</Option>
                  <Option value="9">File</Option>
                </Select>
              </Form.Item>
            </div>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            marginTop: 40,
            justifyContent: "space-between",
            width: "90%"
          }}
        >
          <div style={{ width: "48%" }}>
            <p class="cq-label-text">Mapping Parent</p>
            <div style={{ width: "100%", paddingLeft: 1 }}>
              <div>
                <Form.Item
                  {...Props.field}
                  name={[fieldName, "mappingParent"]}
                  fieldKey={[fieldKey, "mappingParent"]}
                >
                  <Select>
                    <Option value="A">A</Option>
                  </Select>
                </Form.Item>
              </div>
            </div>
          </div>
          <div style={{ width: "48%" }}>
            <p class="cq-label-text">Mapping Property</p>
            <div style={{ width: "100%", paddingLeft: 1 }}>
              <div>
                <Form.Item
                  {...Props.field}
                  name={[fieldName, "mappingProperty"]}
                  fieldKey={[fieldKey, "mappingProperty"]}
                >
                  <Select>
                    <Option value="A">A</Option>
                  </Select>
                </Form.Item>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default OptionalQuestion;
