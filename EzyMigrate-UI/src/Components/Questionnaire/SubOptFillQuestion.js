// @flow
import React, { useState, useEffect } from "react";
import { Images } from "../../Themes";
import {
  message,
  Spin,
  Button,
  Upload,
  Select,
  Form,
  Modal,
  Checkbox,
  Input,
  Radio,
  DatePicker
} from "antd";

import AnswerField from "./AnswerField";

function SubOptFillQuestion(Props) {
  const [show, setShow] = useState(false);
  const [optionName, setOptionName] = useState("");
  const [question, setQuestion] = useState(Props.question);

  useEffect(() => {
    setQuestion(Props.question);
  }, [Props]);

  const userOwner = localStorage.getItem("userOwner");
  const userManager = localStorage.getItem("userManager");
  const userId = localStorage.getItem("userId");

  const { Option } = Select;
  //  

  return (
    <div>
      <div style={{ width: "100%", marginTop: 15 }}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          {Props.questionData.answerType === 0 ? (
            <div
              dangerouslySetInnerHTML={{
                __html: Props.questionData.question
              }}
            />
          ) : (
            <p className="cq-label-text" style={{ width: "38%" }}>
              {Props.questionData.isRequired
                ? Props.questionData.question + " *"
                : Props.questionData.question}
            </p>
          )}

          <Form.List
            name={[Props.question.name, "filledAnswers"]}
            key={[Props.question.fieldKey, "filledAnswers"]}
          >
            {(filledAnswers, { add, remove }) => {
              if (filledAnswers.length === 0) {
                setTimeout(() => {
                  add();
                }, 500);
                // add();
              }
              return (
                <>
                  <div
                    style={{
                      width: Props.questionData.isMultiple ? "55%" : "60%"
                    }}
                  >
                    {filledAnswers.map((answer, index) => {
                      return (
                        <Form.Item required={false} key={answer.key}>
                          <AnswerField
                            answer={answer}
                            questionData={Props.questionData}
                            remove={remove}
                            answerIndex={index}
                            selectedOption={option => {
                               
                              setOptionName(option);
                            }}
                            countriesData={Props.countriesData}
                          />
                        </Form.Item>
                      );
                    })}
                  </div>

                  {/* {filledAnswers.length === 0 ? add() : null} */}
                  <Form.Item>
                    {Props.questionData.isMultiple && (
                      <div
                        className="cq-add-button"
                        onClick={() => add()}
                        style={{ marginLeft: 20 }}
                      >
                        <img src={Images.plusIcon} style={{ width: 20 }} />
                      </div>
                    )}
                  </Form.Item>
                </>
              );
            }}
          </Form.List>
        </div>
      </div>
    </div>
  );
}

export default SubOptFillQuestion;
