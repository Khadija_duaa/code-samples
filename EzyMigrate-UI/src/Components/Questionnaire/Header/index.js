import React, { Fragment } from "react";
import HeaderContact from "./ContactUs";

const Header = (branchData) => {
  return (
    <Fragment>
      <HeaderContact branchData={branchData} />
    </Fragment>
  );
};

export default Header;
