// @flow
import React, { useState, useEffect } from "react";
import { Images } from "../../Themes";
import {
  message,
  Spin,
  Button,
  Upload,
  Select,
  Form,
  Modal,
  Checkbox,
  Input,
  Radio,
  DatePicker
} from "antd";

import OptionalQuestion from "./OptionalQuestion";

import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";

function RadioOption(Props) {
  const [show, setShow] = useState(false);
  // const [name, setName] = useState(Props.manager.fullName);
  // const [email, setEmail] = useState(Props.manager.alternateEmail);

  const [fieldName, setFieldName] = useState("");
  const [fieldKey, setFieldKey] = useState("");
  const [connectionsData, setConnectionsData] = useState("");
  const [answerType, setAnswerType] = useState("");
  const [relation, setRelation] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [optionText, setOptionText] = useState("");
  const [value, setValue] = useState("");
  const [options, setOptions] = useState([]);

  useEffect(() => {
    setOptions(Props.option);
    // setFieldKey(Props.field.fieldKey)
  }, [Props]);

  const userOwner = localStorage.getItem("userOwner");
  const userManager = localStorage.getItem("userManager");
  const userId = localStorage.getItem("userId");

  const { Option } = Select;
  console.log(Props.field);
  //  

  const onChangeAnswerType = value => {
    setAnswerType(value);
  };

  const onChange = e => {
    setValue(e.target.value);
  };
  //  
  console.log(options);

  return (
    <div>
      <Radio onChange={onChange} value={Props.option}>
        {options}
      </Radio>
    </div>
  );
}

export default RadioOption;
