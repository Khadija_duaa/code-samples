// @flow
import React, { useState, useEffect } from "react";
import { Images } from "../../Themes";
import {
  message,
  Spin,
  Button,
  Upload,
  Select,
  Form,
  Modal,
  Checkbox,
  Input,
  Radio,
  DatePicker,
} from "antd";

import AnswerField from "./AnswerField";
import SubOptFillQuestion from "./SubOptFillQuestion";

function OptFillQuestion(Props) {
  const [show, setShow] = useState(false);
  const [optionName, setOptionName] = useState("");
  const [question, setQuestion] = useState(Props.question);
  const [checkboxOptions, setCheckboxOptions] = useState([]);
  const [hideSection, setHideSection] = useState(false);

  var quesitonsDuplicate = [];

  var sectionId = "";

  useEffect(() => {
    setQuestion(Props.question);
  }, [Props]);

  const userOwner = localStorage.getItem("userOwner");
  const userManager = localStorage.getItem("userManager");
  const userId = localStorage.getItem("userId");

  const { Option } = Select;
  //  

  return (
    <div>
      <div style={{ width: "100%", marginTop: 15 }}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          {Props.questionData.answerType === 0 ? (
            <div
              dangerouslySetInnerHTML={{
                __html: Props.questionData.question,
              }}
            />
          ) : (
            <p className="cq-label-text" style={{ width: "38%" }}>
              {Props.questionData.isRequired
                ? Props.questionData.question + " *"
                : Props.questionData.question}
            </p>
          )}

          <Form.List
            name={[Props.question.name, "filledAnswer"]}
            key={[Props.question.fieldKey, "filledAnswer"]}
          >
            {(filledAnswer, { add, remove }) => {
              if (filledAnswer.length === 0) {
                setTimeout(() => {
                  add();
                }, 500);
                // add();
              }
              return (
                <>
                  <div
                    style={{
                      width: Props.questionData.isMultiple ? "55%" : "60%",
                    }}
                  >
                    {filledAnswer.map((answer, index) => {
                      return (
                        <Form.Item required={false} key={answer.key}>
                          <AnswerField
                            answer={answer}
                            questionData={Props.questionData}
                            remove={remove}
                            answerIndex={index}
                            selectedOption={(option, isCheckbox) => {
                              if (isCheckbox) {
                                setCheckboxOptions(option);
                              } else {
                                setOptionName(option);
                              }
                            }}
                            countriesData={Props.countriesData}
                          />
                        </Form.Item>
                      );
                    })}
                  </div>

                  {/* {filledAnswers.length === 0 ? add() : null} */}
                  <Form.Item>
                    {Props.questionData.isMultiple && (
                      <div
                        className="cq-add-button"
                        onClick={() => add()}
                        style={{ marginLeft: 20 }}
                      >
                        <img src={Images.plusIcon} style={{ width: 20 }} />
                      </div>
                    )}
                  </Form.Item>
                </>
              );
            }}
          </Form.List>
        </div>
      </div>
      <Form.List
        name={[Props.question.name, "questionOptions"]}
        key={[Props.question.fieldKey, "questionOptions"]}
      >
        {(questionOptions, { add, remove }) => {
          return (
            <div>
              {questionOptions.map((option, optInd) => (
                <div key={optInd}>
                  <Form.List
                    name={[option.name, "optionalQuestions"]}
                    key={[option.fieldKey, "optionalQuestions"]}
                  >
                    {(optionalQuestions, { add, remove }) => {
                      var checkOptionName = "";
                      if (checkboxOptions.length > 0) {
                        var findOption = checkboxOptions.find(
                          (obj) =>
                            obj ===
                            Props.questionData.questionOptions[optInd].option
                        );
                        if (findOption) {
                          checkOptionName = findOption;
                        }
                      } else {
                        checkOptionName = optionName;
                      }
                      var checkQuestions = "";

                      var hideQuestion = false;
                      for (
                        var a = 0;
                        a <
                        Props.questionData.questionOptions[optInd]
                          .optionalQuestions.length;
                        a++
                      ) {
                        for (
                          var b = 0;
                          b <
                          Props.questionData.questionOptions[optInd]
                            .optionalQuestions[a].questions.length;
                          b++
                        ) {
                          var findId = quesitonsDuplicate.find(
                            (obj) =>
                              obj.id ===
                              Props.questionData.questionOptions[optInd]
                                .optionalQuestions[a].questions[b].id
                          );
                          if (!findId) {
                            quesitonsDuplicate.push(
                              Props.questionData.questionOptions[optInd]
                                .optionalQuestions[a].questions[b]
                            );
                          }
                        }
                      }
                      // if (checkboxOptions.length > 1) {
                      //   for (var a = 0; a < checkboxOptions.length; a++) {
                      //     if (
                      //       checkboxOptions[a] ===
                      //       Props.questionData.questionOptions[optInd].option
                      //     ) {
                      //       if (
                      //         Props.questionData.questionOptions[optInd]
                      //           .optionalQuestions.length > 0
                      //       ) {
                      //         for (
                      //           var b = 0;
                      //           b <
                      //           Props.questionData.questionOptions[optInd]
                      //             .optionalQuestions.length;
                      //           b++
                      //         ) {
                      //           for (
                      //             var c = 0;
                      //             c <
                      //             Props.questionData.questionOptions[optInd]
                      //               .optionalQuestions[b].questions.length;
                      //             c++
                      //           ) {
                      //             var findId = quesitonsDuplicate.find(
                      //               (obj) =>
                      //                 obj.id ===
                      //                 Props.questionData.questionOptions[optInd]
                      //                   .optionalQuestions[b].questions[c].id
                      //             );
                      //             if (!findId) {
                      //               quesitonsDuplicate.push(
                      //                 Props.questionData.questionOptions[optInd]
                      //                   .optionalQuestions[b].questions[c]
                      //               );
                      //             }
                      //           }
                      //         }
                      //       }
                      //     }
                      //   }
                      // }

                      // for (
                      //   var a = 0;
                      //   a <
                      //   Props.questionData.questionOptions[optInd]
                      //     .optionalQuestions.length;
                      //   a++
                      // ) {
                      //   for (
                      //     var b = 0;
                      //     b <
                      //     Props.questionData.questionOptions[optInd]
                      //       .optionalQuestions[a].questions.length;
                      //     b++
                      //   ) {
                      //     var findId = quesitonsDuplicate.find(
                      //       (obj) =>
                      //         obj.id ===
                      //         Props.questionData.questionOptions[optInd]
                      //           .optionalQuestions[a].questions[b].id
                      //     );
                      //     if (!findId) {
                      //       quesitonsDuplicate.push(
                      //         Props.questionData.questionOptions[optInd]
                      //           .optionalQuestions[a].questions[b]
                      //       );
                      //     }
                      //   }
                      // }
                      // var hideQuestion = false;
                      if (checkboxOptions.length > 1) {
                        for (var z = 0; z < checkboxOptions.length; z++) {
                          if (
                            checkboxOptions[z] ===
                            Props.questionData.questionOptions[optInd].option
                          ) {
                            for (
                              var a = 0;
                              a <
                              Props.questionData.questionOptions[optInd]
                                .optionalQuestions.length;
                              a++
                            ) {
                              for (
                                var b = 0;
                                b <
                                Props.questionData.questionOptions[optInd]
                                  .optionalQuestions[a].questions.length;
                                b++
                              ) {
                                var findMultiple = quesitonsDuplicate.filter(
                                  (obj) =>
                                    obj.question ===
                                    Props.questionData.questionOptions[optInd]
                                      .optionalQuestions[a].questions[b]
                                      .question
                                );
                                if (findMultiple.length > 0) {
                                  hideQuestion = true;
                                  sectionId = findMultiple[0].sectionId;
                                }
                              }
                            }
                          }
                        }
                      }
                      console.log(quesitonsDuplicate, hideQuestion);
                      return (
                        <div>
                          {checkOptionName ===
                            Props.questionData.questionOptions[optInd].option &&
                            optionalQuestions.map((optQuestion, optqInd) => {
                              return (
                                <>
                                  {(!hideQuestion ||
                                    (checkboxOptions.length > 0 &&
                                      sectionId ===
                                        Props.questionData.questionOptions[
                                          optInd
                                        ].optionalQuestions[optqInd].id)) && (
                                    <div>
                                      <div
                                        className="section-heading-row"
                                        style={{
                                          paddingBottom: 10,
                                        }}
                                      >
                                        <div>
                                          <span className="heading-text">
                                            {
                                              Props.questionData
                                                .questionOptions[optInd]
                                                .optionalQuestions[optqInd].name
                                            }
                                          </span>
                                        </div>
                                        {hideSection ? (
                                          <div
                                            style={{ cursor: "pointer" }}
                                            onClick={() =>
                                              setHideSection(false)
                                            }
                                          >
                                            <span
                                              className="heading-text"
                                              style={{
                                                fontSize: 11,
                                              }}
                                            >
                                              SHOW
                                            </span>
                                          </div>
                                        ) : (
                                          <div
                                            style={{ cursor: "pointer" }}
                                            onClick={() => setHideSection(true)}
                                          >
                                            <span
                                              className="heading-text"
                                              style={{
                                                fontSize: 11,
                                              }}
                                            >
                                              HIDE
                                            </span>
                                          </div>
                                        )}
                                      </div>
                                      {!hideSection && (
                                        <div
                                          className={
                                            optQuestion &&
                                            "cq-optional-section-cont"
                                          }
                                          style={{
                                            backgroundColor:
                                              "rgb(238, 249, 255)",
                                          }}
                                        >
                                          <Form.List
                                            name={[
                                              optQuestion.name,
                                              "questions",
                                            ]}
                                            key={[
                                              optQuestion.fieldKey,
                                              "questions",
                                            ]}
                                          >
                                            {(questions, { add, remove }) => {
                                              return (
                                                <div>
                                                  {questions.map(
                                                    (optQuestion, quesInd) => {
                                                      return (
                                                        <div key={quesInd}>
                                                          <Form.Item
                                                            required={false}
                                                            key={question.key}
                                                          >
                                                            <SubOptFillQuestion
                                                              question={
                                                                optQuestion
                                                              }
                                                              questionData={
                                                                Props
                                                                  .questionData
                                                                  .questionOptions[
                                                                  optInd
                                                                ]
                                                                  .optionalQuestions[
                                                                  optqInd
                                                                ].questions[
                                                                  quesInd
                                                                ]
                                                              }
                                                              countriesData={
                                                                Props.countriesData
                                                              }
                                                            />
                                                          </Form.Item>
                                                        </div>
                                                      );
                                                    }
                                                  )}
                                                </div>
                                              );
                                            }}
                                          </Form.List>
                                        </div>
                                      )}
                                    </div>
                                  )}
                                </>
                              );
                            })}
                        </div>
                      );
                    }}
                  </Form.List>
                </div>
              ))}
            </div>
          );
        }}
      </Form.List>
    </div>
  );
}

export default OptFillQuestion;
