import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { Images } from "../Themes";
import { Link } from "react-router-dom";
import "./SidebarStyles.css";
import dashboardBI from "../svgs/menu-icons/dashboardBI.svg";
import dashboardClient from "../svgs/menu-icons/dashboard.svg";
import caseManagement from "../svgs/menu-icons/case-management.svg";
import meeting from "../svgs/menu-icons/meeting.svg";
import taskComplete from "../svgs/menu-icons/task-complete.svg";
import playButton from "../svgs/menu-icons/play-button.svg";
import userGroup from "../svgs/menu-icons/users-group.svg";
import addContact from "../svgs/menu-icons/add-contact.svg";
import group from "../svgs/menu-icons/group.svg";
import report from "../svgs/menu-icons/report.svg";
import account from "../svgs/menu-icons/calculator.svg";
import xero from "../svgs/menu-icons/xero.svg";
import email from "../svgs/menu-icons/black-back-closed-envelope-shape.svg";
import agent from "../svgs/menu-icons/insurance-agent.svg";
import schoolManagement from "../svgs/menu-icons/college-graduation.svg";
import supplierManagement from "../svgs/menu-icons/manager.svg";
import questionnaires from "../svgs/menu-icons/question-speech-bubble.svg";
import deals from "../svgs/menu-icons/handshake.svg";
import timeTracking from "../svgs/menu-icons/clock.svg";
import visaForms from "../svgs/menu-icons/forms.svg";
import techSupport from "../svgs/menu-icons/admin-with-cogwheels.svg";
import settings from "../svgs/menu-icons/settings.svg";
import superUserSettings from "../svgs/menu-icons/super_user_settings.svg";
import cpdPlan from "../svgs/menu-icons/CPD-plan.svg";
import usefulLink from "../svgs/menu-icons/usefull-link.svg";

const Sidebar = (Props) => {
  const userOwner = localStorage.getItem("userOwner");
  const userManager = localStorage.getItem("userManager");
  return (
    <div style={{ height: "100%", width: 220, backgroundColor: "#213037" }}>
      <List disablePadding dense>
        <Link
          to="/profile"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
              backgroundColor:
                Props.activeScreen == "Dashboard (BI)"
                  ? "#435056"
                  : "transparent",
            }}
          >
            <img
              src={dashboardBI}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Dashboard (BI)
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/dashboard"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
              backgroundColor:
                Props.activeScreen == "dashboardCl" ? "#435056" : "transparent",
            }}
          >
            <img
              src={dashboardClient}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Dashboard (Client)
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/case-management"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
            }}
          >
            <img
              src={caseManagement}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Case Management
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/meetings"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              backgroundColor:
                Props.activeScreen == "meetings" ? "#435056" : "transparent",
            }}
          >
            <img
              src={meeting}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Meetings
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/tasks-and-reminders/tasks-to-do"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              backgroundColor:
                Props.activeScreen == "taskAndReminders"
                  ? "#435056"
                  : "transparent",
            }}
          >
            <img
              src={taskComplete}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              My Daily Tasks / Reminders
            </ListItemText>
          </ListItem>
        </Link>
        <ListItem button>
          <img
            src={playButton}
            style={{ width: 20, height: 20, marginRight: 10 }}
          />
          <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
            Video Tutorials
          </ListItemText>
        </ListItem>
        <Link
          to="/all-clients"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem button>
            <img
              src={userGroup}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              All Clients
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/add-new-client"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem button>
            <img
              src={addContact}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Add New Client
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/potential-client/potential-clients"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              backgroundColor:
                Props.activeScreen == "potentialClients"
                  ? "#435056"
                  : "transparent",
            }}
          >
            <img
              src={group}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Potential Clients
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/reports"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              backgroundColor:
                Props.activeScreen == "reports" ? "#435056" : "transparent",
            }}
          >
            <img
              src={report}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Reports
            </ListItemText>
          </ListItem>
        </Link>
        <ListItem button>
          <img
            src={account}
            style={{ width: 20, height: 20, marginRight: 10 }}
          />
          <ListItemText style={{ color: "#FFFFFF" }}>Accounts</ListItemText>
        </ListItem>
        <ListItem button>
          <img src={xero} style={{ width: 20, height: 20, marginRight: 10 }} />
          <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
            XERO
          </ListItemText>
        </ListItem>
        <ListItem button>
          <img src={email} style={{ width: 20, height: 20, marginRight: 10 }} />
          <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
            Mail
          </ListItemText>
        </ListItem>
        <Link to="/agent" style={{ textDecoration: "none", color: "#FFFFFF" }}>
          <ListItem button>
            <img
              src={agent}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF" }}>Agents</ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/school-management/student-list"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              backgroundColor:
                Props.activeScreen == "School Management"
                  ? "#435056"
                  : "transparent",
            }}
          >
            <img
              src={schoolManagement}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              School Management
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/employer-management"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              backgroundColor:
                Props.activeScreen == "employerManagement"
                  ? "#435056"
                  : "transparent",
            }}
          >
            <img
              src={supplierManagement}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Employer Management
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/questionnaire"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              backgroundColor:
                Props.activeScreen === "customQuestionnaire"
                  ? "#435056"
                  : "transparent",
            }}
          >
            <img
              src={questionnaires}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Custom Questionnaires
            </ListItemText>
          </ListItem>
        </Link>
        <ListItem button>
          <img src={deals} style={{ width: 20, height: 20, marginRight: 10 }} />
          <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
            Deals
          </ListItemText>
        </ListItem>
        <Link
          to="/time-tracking"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              backgroundColor:
                Props.activeScreen == "timeTracking"
                  ? "#435056"
                  : "transparent",
            }}
          >
            <img
              src={questionnaires}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Time Tracking
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/nz-form-list"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem button>
            <img
              src={visaForms}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Visa Forms
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/technical-support"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem
            button
            style={{
              backgroundColor:
                Props.activeScreen == "technicalSupport"
                  ? "#435056"
                  : "transparent",
            }}
          >
            <img
              src={techSupport}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Technical Support
            </ListItemText>
          </ListItem>
        </Link>
        <Link
          to="/account-settings"
          style={{ textDecoration: "none", color: "#FFFFFF" }}
        >
          <ListItem button>
            <img
              src={settings}
              style={{ width: 20, height: 20, marginRight: 10 }}
            />
            <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
              Account Settings
            </ListItemText>
          </ListItem>
        </Link>
        <ListItem button>
          <img
            src={cpdPlan}
            style={{ width: 20, height: 20, marginRight: 10 }}
          />
          <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
            CPD Plan
          </ListItemText>
        </ListItem>
        <ListItem button>
          <img
            src={usefulLink}
            style={{ width: 20, height: 20, marginRight: 10 }}
          />
          <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
            Useful Links
          </ListItemText>
        </ListItem>
        {(userOwner === "True" || userManager === "true") && (
          <Link
            to="/super-user-setting?activeTab=account-settings"
            style={{ textDecoration: "none", color: "#FFFFFF" }}
          >
            <ListItem
              button
              style={{
                backgroundColor:
                  Props.activeScreen == "superUserSettings"
                    ? "#435056"
                    : "transparent",
              }}
            >
              <img
                src={superUserSettings}
                style={{ width: 20, height: 20, marginRight: 10 }}
              />
              <ListItemText style={{ color: "#FFFFFF", fontSize: 12 }}>
                Super User Settings
              </ListItemText>
            </ListItem>
          </Link>
        )}
      </List>
    </div>
  );
};

export default Sidebar;
