import React, { Fragment, useState, useEffect } from "react";
import { Select, message, Button, DatePicker } from "antd";
import moment from "moment";

const { Option } = Select;

const dateFormat = "DD/MM/YYYY";

const VisaCard = ({ visaTypeData }) => {
  const [showAppDates, setShowAppDates] = useState(false);
  const [showDocuments, setShowDocuments] = useState(false);
  const [linkFamily, setLinkFamily] = useState(false);

  const onChangeVisaType = value => {
    console.log(`selected ${value}`);
    // setVisaTypeId(value);
  };

  const onChangeCategory = value => {
    console.log(`selected ${value}`);
    // setCategory(value);
  };

  const onChangeDestination = value => {
    console.log(`selected ${value}`);
    // setDestination(value);
  };

  const onChange = (date, dateString) => {
    console.log(date, dateString);
    // setStartDate(date);
  };

  return (
    <Fragment>
      <div></div>
      {/* <div
        className="cv-gray-cont"
        style={{
          paddingBottom: showAppDates || showDocuments || linkFamily ? 0 : 20,
        }}
      >
        <div className="cv-row">
          <div>
            <div style={{ display: "flex" }}>
              <span className="cv-bold-text">
                {visaApp.branchVisaTypeName.toUpperCase()}
              </span>
              {visaApp.country !== "" && (
                <div className="cv-green-box">
                  <span className="cv-green-text">
                    {visaApp.country.toUpperCase()}
                  </span>
                </div>
              )}
            </div>
            <div>
              {visaApp.caseLinks &&
                visaApp.caseLinks.items.length > 0 &&
                visaApp.caseLinks.items.map((caseLink, linkIndex) => {
                  return (
                    <div
                      key={linkIndex}
                      className="linked-member-cont"
                      style={{
                        marginLeft: linkIndex > 0 ? 5 : 0,
                      }}
                    >
                      <div className="button-first-second-row">
                        <span
                          className="linked-member-text"
                          style={{ color: "#555555" }}
                        >
                          {caseLink.firstName + " " + caseLink.lastName}
                        </span>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
          <div style={{ display: "flex" }}>
            <div
              className="cv-preparing-box"
              style={{
                backgroundColor: visaApp.visaStatusColor,
              }}
            >
              <div
                className="cv-imm-cont"
                style={{ cursor: "pointer" }}
                onClick={() => this.onClickStatus(visaApp)}
              >
                <span className="cv-imm-text">
                  {visaApp.visaStatusName.toUpperCase()}
                </span>
              </div>
              <div className="cv-icons-row">
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                      setLinkFamily(true);
                      setShowDocuments(false);
                      setShowAppDates(false)
                  }
                    // this.setState({
                    //   linkFamily: true,
                    //   showDocuments: false,
                    //   showAppDates: false,
                    //   expendedView: true,
                    // })
                  }
                >
                  <img
                    src={Images.multimedia}
                    style={{ width: 15, height: 15 }}
                  />
                </div>
                <div>
                  <img
                    src={Images.notesWhite}
                    style={{ width: 15, height: 15 }}
                  />
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                      
                      setLinkFamily(false);
                      setShowDocuments(false);
                      setShowAppDates(false)
                  }
                  
                    // this.setState({
                    //   showEmail: true,
                    //   linkFamily: false,
                    //   showDocuments: false,
                    //   showAppDates: false,
                    //   expendedView: true,
                    // })
                  }
                
                >
                  <img
                    src={Images.emailWhite}
                    style={{ width: 15, height: 15 }}
                  />
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => this.getCaseDocuments(visaApp.id)}
                >
                  <img
                    src={Images.notesWhite}
                    style={{ width: 15, height: 15 }}
                  />
                </div>
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                      setLinkFamily(true);
                      setShowDocuments(false);
                      setShowAppDates(false)
                  }
                    // this.setState({
                    //   showAppDates: true,
                    //   linkFamily: false,
                    //   showDocuments: false,
                    //   showEmail: false,
                    //   expendedView: true,
                    // })
                  }
                >
                  <DownOutlined style={{ color: "#FFFFFF" }} />
                </div>
              </div>
            </div>
            <div
              className="cv-delete-cont"
              style={{ cursor: "pointer" }}
              onClick={() => this.removeSubject(visaApp.id)}
            >
              <img src={Images.deleteGray} className="cv-dlt-icon" />
            </div>
          </div>
        </div>
        {(showAppDates || linkFamily || showDocuments) && (
          <div className="cv-show-hide-cont">
            <div></div>
            <div
              className="sus-show-btn"
              style={{ marginRight: 30 }}
              onClick={() => 
                this.setState({
                  showAppDates: false,
                  linkFamily: false,
                  showDocuments: false,
                })
              }
            >
              <span className="sus-save-btn-text">HIDE</span>
            </div>
          </div>
        )}
        {showDocuments && (
          <div className="cv-gray-cont">
            <div className="sus-tab-container">
              <div
                onClick={() => this.onChangeTopTab("document")}
                className={
                  this.state.activeTab == "document"
                    ? "cv-active-tab"
                    : "cv-inactive-tab"
                }
              >
                <span
                  className={
                    this.state.activeTab == "document"
                      ? "cv-active-tab-text"
                      : "cv-inactive-tab-text"
                  }
                >
                  DOCUMENT
                </span>
              </div>
              <div
                onClick={() => this.onChangeTopTab("documentChecklist")}
                className={
                  this.state.activeTab == "documentChecklist"
                    ? "cv-active-tab"
                    : "cv-inactive-tab"
                }
              >
                <span
                  className={
                    this.state.activeTab == "documentChecklist"
                      ? "cv-active-tab-text"
                      : "cv-inactive-tab-text"
                  }
                >
                  SEND DOCUMENT CHECKLIST
                </span>
              </div>
            </div>

            <div className="cv-white-cont">
              <div className="cv-row" style={{ marginBottom: 10 }}>
                <div />
                <div style={{ display: "flex" }}>
                  <div className="cv-print-icon-cont">
                    <img
                      src={Images.printBlue}
                      className="profile-print-icon"
                    />
                  </div>
                  <div className="cv-extend-icon-cont">
                    <img
                      src={Images.extendIcon}
                      className="cv-extend-icon"
                      style={{
                        transform: `rotate(270deg)`,
                      }}
                    />
                  </div>
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <div />
                <div
                  style={{
                    display: "flex",
                    marginBottom: 10,
                    alignItems: "center",
                  }}
                >
                  <div>
                    <span className="cv-bold-text" style={{ color: "#75868F" }}>
                      Visible To:
                    </span>
                  </div>
                  <div className="cv-circle-lable-cont">
                    <div className="cv-client-circle"></div>
                    <span className="cv-normal-text" style={{ marginLeft: 5 }}>
                      Client
                    </span>
                  </div>
                  <div className="cv-circle-lable-cont">
                    <div className="cv-agent-circle"></div>
                    <span className="cv-normal-text" style={{ marginLeft: 5 }}>
                      Agent
                    </span>
                  </div>
                  <div className="cv-circle-lable-cont">
                    <div className="cv-both-circle"></div>
                    <span className="cv-normal-text" style={{ marginLeft: 5 }}>
                      Both
                    </span>
                  </div>
                  <div>
                    <div className="cv-org-btn-cont" style={{ marginLeft: 25 }}>
                      <div className="cv-plus-cont">
                        <span className="cv-plus-icon">+</span>
                      </div>
                      <span className="cv-org-btn-text">ADD DOCUMENTS</span>
                    </div>
                  </div>
                  <div>
                    <div className="cv-actions-cont">
                      <span
                        className="cv-normal-text"
                        style={{
                          fontWeight: "500",
                          marginLeft: 0,
                        }}
                      >
                        Actions
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="cv-doc-head-row">
                <div
                  className="cv-width-55"
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <div>
                    <input
                      type="checkbox"
                      className="sus-checkbox"
                      onChange={this.handleCheck}
                      defaultChecked={this.state.checked}
                    />
                  </div>
                  <div className="cv-doc-inner-index">
                    <span className="cv-doc-head-text">#</span>
                  </div>
                  <div className="cv-doc-width">
                    <span className="cv-doc-head-text">Document</span>
                  </div>
                </div>
                <div className="cv-width-17">
                  <span className="cv-doc-head-text">Title</span>
                </div>
                <div className="cv-width-13">
                  <span className="cv-doc-head-text">Type</span>
                </div>
                <div className="cv-width-15">
                  <span className="cv-doc-head-text">Action</span>
                </div>
              </div>

              {caseDocumentData &&
                caseDocumentData.items.map((document, index) => {
                  return (
                    <div className="cv-doc-row">
                      <div
                        className="cv-width-52"
                        style={{
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <div>
                          <input
                            type="checkbox"
                            className="sus-checkbox"
                            onChange={this.handleCheck}
                            defaultChecked={this.state.checked}
                          />
                        </div>
                        <div className="cv-doc-inner-index">
                          <span
                            className="cv-doc-text"
                            style={{ color: "#5B5B5B" }}
                          >
                            {document.id}
                          </span>
                        </div>
                        <div className="cv-doc-width">
                          <div>
                            <span
                              className="cv-doc-text"
                              style={{
                                color: "#1081B8",
                              }}
                            >
                              {document.title}
                            </span>
                          </div>
                          <div className="cv-doc-date-text-cont">
                            <span className="cv-doc-date-text"></span>
                          </div>
                        </div>
                      </div>
                      <div className="cv-width-20" style={{ padding: 1 }}>
                        <div className="cv-title-box">
                          <span
                            className="cv-normal-text"
                            style={{
                              fontSize: 9,
                              marginLeft: 5,
                            }}
                          >
                            {document.blobFileName}
                          </span>
                        </div>
                      </div>
                      <div className="cv-width-13" style={{ padding: 1 }}>
                        <div className="cv-title-box" style={{ width: "80%" }}>
                          <span
                            className="cv-normal-text"
                            style={{
                              fontSize: 9,
                              marginLeft: 5,
                            }}
                          >
                            show data
                          </span>
                        </div>
                      </div>
                      <div className="cv-width-15" style={{ display: "block" }}>
                        <div
                          style={{
                            display: "flex",
                            marginTop: 10,
                          }}
                        >
                          <div className="cv-action-icons-border">
                            <img
                              src={Images.download}
                              className="cv-action-icon"
                            />
                          </div>
                          <div
                            className="cv-action-icons-border"
                            style={{ marginLeft: 5 }}
                          >
                            <img
                              src={Images.visibility}
                              className="cv-action-icon"
                            />
                          </div>
                          {false && (
                            <div
                              className="cv-action-icons-border"
                              style={{ marginLeft: 5 }}
                            >
                              <img
                                src={Images.download}
                                className="cv-action-icon"
                              />
                            </div>
                          )}
                        </div>
                        {false && (
                          <div
                            style={{
                              display: "flex",
                              marginTop: 3,
                            }}
                          >
                            <div className="cv-action-icons-border">
                              <img
                                src={Images.fileNotes}
                                className="cv-action-icon"
                              />
                            </div>
                            <div
                              className="cv-action-icons-border"
                              style={{ marginLeft: 5 }}
                            >
                              <img
                                src={Images.multimediaBlue}
                                className="cv-action-icon"
                              />
                            </div>
                            <div
                              className="cv-action-icons-border"
                              style={{ marginLeft: 5 }}
                            >
                              <img
                                src={Images.deleteIcon}
                                className="cv-action-icon"
                              />
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        )}
      </div>

      <div className="cv-ass-rows-cont">
        {linkFamily && (
          <div className="cv-lf-white-cont">
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginTop: 20,
                marginLeft: 10,
              }}
            >
              {partnerProfileData && (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    marginBottom: 15,
                  }}
                >
                  <div class="cm-profile-fill-cont" style={{ marginLeft: 20 }}>
                    <img src={Images.personFill} class="cm-profile-fill" />
                  </div>
                  <div className="cv-lf-checkbox-label-cont">
                    <span className="sus-checkbox-label">
                      {partnerProfileData.firstName}
                    </span>
                  </div>
                  <div>
                    <Checkbox
                      checked={this.state.checked}
                      onChange={(e) =>
                        this.onChangeFamilyCheck(
                          e,
                          visaApp.id,
                          partnerProfileData.id
                        )
                      }
                    ></Checkbox>
                  </div>
                </div>
              )}

              
            </div>
          </div>
        )}

        {showAppDates &&
          visaApp.caseHistory.map((caseHistory, caseIndex) => {
            return (
              <div key={caseIndex}>
                <div
                  className="cv-row"
                  style={{
                    marginTop: 10,
                    alignItems: "center",
                  }}
                >
                  <div className="cv-ass-row">
                    <div
                      className="cv-blue-box"
                      style={{
                        backgroundColor: "#845FA8",
                      }}
                    ></div>
                    <div style={{ display: "flex" }}>
                      <span className="cv-normal-text">
                        {caseHistory.caseStatusName}
                      </span>
                    </div>
                    {/* disable detail button
                    {false && (
                      <div className="cv-org-cont">
                        <span className="cv-org-text">DETAILS</span>
                      </div>
                    )}
                  </div>

                  <div className="cv-row">
                    <span className="cv-normal-text">
                      {moment(caseHistory.startDate).format("DD/MM/yyyy")}
                    </span>
                    <div
                      className="cv-cross-cont"
                      style={{ cursor: "pointer" }}
                      onClick={() => this.removeStatus(caseHistory.id)}
                    >
                      <img src={Images.crossWhite} className="cv-svg-8" />
                    </div>
                  </div>
                </div>

              </div>
            );
          })}
      </div> */}
    </Fragment>
  );
};

export default VisaCard;
