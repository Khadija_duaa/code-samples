// @flow
import React, { useState, useEffect } from "react";
import styled from "styled-components";
import "./HeaderBarStyles.css";
import { Button, Select, Upload } from "antd";
import navMenu from "../../images/nav-collaps.png";
import search from "../../svgs/search.svg";
import { Images } from "../../Themes";
import { PoweroffOutlined } from "@ant-design/icons";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" }
];

function HeaderBar(Props) {
  const [selectedBranch, setSelectedBranch] = useState("");

  const { Option } = Select;
  const branchOptions = [];
  if (Props.branchData && Props.branchData.length > 0) {
    for (let i = 0; i < Props.branchData.length; i++) {
      branchOptions.push(
        <Option key={Props.branchData[i].branchId}>
          {Props.branchData[i].branchName}
        </Option>
      );
    }
  }

  const handleChangeBranch = selectedBranch => {
    setSelectedBranch(selectedBranch);

    var findBranch = Props.branchData.find(
      obj => obj.branchId === selectedBranch
    );

    if (findBranch) {
      localStorage.setItem("userManager", findBranch.isManager);
      localStorage.setItem("selectedBranchId", findBranch.branchId);
    }
  };

  const onLogout = () => {
    Props.onLogout();
  };

  return (
    <div>
      <div className="header-top-bar">
        <div style={{ display: "flex", width: "55%" }}>
          <div>
            <img
              src={Images.logo}
              className="logo-img"
              style={{ width: 140, height: "auto" }}
            />
          </div>
          <div className="menu-icon">
            <img src={navMenu} />
          </div>
          <div className="company-cont">
            <div className="company-box">
              <p className="company-text">Branch</p>
            </div>
            <div style={{ width: "100%" }}>
              <Select
                bordered={false}
                placeholder="Select Branch"
                defaultValue={selectedBranch}
                onChange={handleChangeBranch}
                style={{ width: "100%" }}
              >
                {branchOptions}
              </Select>
            </div>
          </div>
          <div className="search-client">
            <div className="search-box">
              <img src={search} style={{ width: 20, height: 20 }} />
            </div>
            <div className="search-input-cont">
              <input
                className="search-input"
                placeholder="Search Client..."
                type="text"
              />
            </div>
          </div>
        </div>
        <div style={{ display: "flex", width: "35%", paddingRight: 10 }}>
          <div className="select-country">
            <Select value={"selectedOption"} options={options} />
          </div>
          <div style={{ marginTop: 8, width: 300 }}>
            <p style={{ color: "#0974AC" }}>Hello Benedict Thomas !</p>
          </div>
          <div>
            <div className="btn-logout-cont">
              <div
                className="btn-logout"
                style={{ cursor: "pointer" }}
                onClick={onLogout}
              >
                <Button type="primary" icon={<PoweroffOutlined />}>
                  Logout
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HeaderBar;
