// @flow
import * as React from "react";
import styled from "styled-components";
import "./HeaderBarStyles.css";
import logo from "../../images/logo.png";
import Select from "react-select";
import navMenu from "../../images/nav-collaps.png";
import { Images } from "../../Themes";
import { Link } from "react-router-dom";

const PotentialHeaderBarTabs = Props => {
  return (
    <div>
      <div className="pc-header-bar">
        {Props.data &&
          Props.data.map((item, index) => {
            return (
              <Link
                to={item.linkName}
                params={{ id: Props.partnerId ? Props.partnerId : null }}
                style={{ textDecoration: "none", color: "#FFFFFF" }}
              >
                {/* eslint-disable-next-line no-undef */}
                <div
                  className="header-bar-text-div"
                  style={{
                    backgroundColor:
                      Props.activePath == item.linkName
                        ? "#33aada"
                        : "transparent",
                    paddingLeft: 26,
                    paddingRight: 19
                  }}
                >
                  <span
                    className="header-text"
                    style={{
                      fontWeight:
                        Props.activePath == item.linkName ? "600" : "400"
                    }}
                  >
                    {item.tabName}
                  </span>
                </div>
              </Link>
            );
          })}
      </div>
    </div>
  );
};

export default PotentialHeaderBarTabs;
