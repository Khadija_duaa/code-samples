import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { Link } from "react-router-dom";

function ProfileSidebar({ setShowClient, showClient }) {
  return (
    <div style={{ height: "auto", width: 140, backgroundColor: "#F3F4F6" }}>
      <List disablePadding dense>
        <ListItem button>
          <ListItemText>Update Visa Status</ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText>Update Admission</ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText>Send SMS</ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText>Tasks</ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText>Meetings</ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText>Reminders</ListItemText>
        </ListItem>
        <ListItem button onClick={() => setShowClient("client-tags")}>
          <ListItemText>Client Tags</ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText>Balance</ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText>Time Tracking</ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText>Deals</ListItemText>
        </ListItem>
      </List>
    </div>
  );
}

export default ProfileSidebar;
