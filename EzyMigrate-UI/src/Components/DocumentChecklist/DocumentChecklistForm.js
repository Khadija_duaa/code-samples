import React, { Fragment, useEffect, useState } from "react";
import {
  Form,
  Input,
  Button,
  message,
  Row,
  Col,
  Checkbox,
  Switch,
  Space,
  Select,
  Spin,
} from "antd";

import { Images } from "./../../Themes";
import { CloseCircleOutlined, DeleteOutlined } from "@ant-design/icons";

const layoutSendingReceipt = {
  labelCol: { span: 3, offset: 0 },
  wrapperCol: { span: 13, offset: 10 },
};

const layoutInner = {
  labelCol: { span: 2, offset: 0 },
  wrapperCol: { span: 13, offset: 11 },
};

const { Option } = Select;
const DocumentChecklistForm = ({
  loading,
  handleCancel,
  setLoading,
  checklistName,
  createMode,
  getUrl,
  updatedata,
  Mode,
  showModal,
  onAddDocumentChecklist,

  onGetDocumentChecklist,
  docChecklistRes,

  selectedBranchId,

  onUpdateDocumentChecklist,
  view,
  onGetChecklistItems,
  docChecklistItemRes,

  docuementResponse,
  clientprofileid,
}) => {
  const [form] = Form.useForm();

  // useEffect(() => {
  //   setLoading(true);
  //
  //   onGetChecklistItems(selectedBranchId).then(() => {
  //     setLoading(false);
  //   });
  // }, []);

  // useEffect(() => {
  //
  //   // Fields Set Form Start //
  //   form.setFieldsValue({
  //     name:
  //       docChecklistRes &&
  //       docChecklistRes.items[0] &&
  //       docChecklistRes.items[0].name,
  //     Discription: docChecklistRes && docChecklistRes.name,
  //   });
  // }, [docChecklistRes]);

  // const docChecklistitems = (docChecklistRes) => {

  // };

  const onFinish = (values) => {
    if (
      (values && values.checkListItems === undefined) ||
      (values && values.checkListItems && values.checkListItems.length === 0)
    ) {
      message.error("Please add atleast one item!");
    } else {
      setLoading(true);

      var checkListItemsData = [];
      var checkListItemsUpdate = [];

      if (values.checkListItems && values.checkListItems.length > 0) {
        for (var i = 0; i < values.checkListItems.length; i++) {
          checkListItemsData.push({
            id: 0,
            checkListId: view === "settings" ? 0 : Mode && Mode.id,
            name: values && values.checkListItems[i].name,
            required:
              values && values.checkListItems[i].required === undefined
                ? true
                : values && values.checkListItems[i].required,
            createdBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            createdDate: "2021-04-22T18:27:50.776Z",
            modifiedBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            modifiedDate: "2021-04-22T18:27:50.776Z",
            priority:
              values && values.checkListItems[i].priority === undefined
                ? "0"
                : values && values.checkListItems[i].priority,
          });
        }
      }
      if (values.checkListItems && values.checkListItems.length > 0) {
        for (var j = 0; j < values.checkListItems.length; j++) {
          checkListItemsUpdate.push({
            id:
              values && values.checkListItems[j].id
                ? values.checkListItems[j].id
                : 0,
            checkListId:
              values && values.checkListItems[j].checkListId
                ? values.checkListItems[j].checkListId
                : createMode.id,
            name: values && values.checkListItems[j].name,
            required:
              values && values.checkListItems[j].required === undefined
                ? true
                : values && values.checkListItems[j].required,
            createdBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            createdDate: "2021-04-27T21:26:36.593Z",
            modifiedBy: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            modifiedDate: "2021-04-27T21:26:36.593Z",
            priority:
              values && values.checkListItems[j].priority === undefined
                ? 0
                : values && values.checkListItems[j].priority,
          });
        }
      }

      // console.log("Received values of form:", values);

      if (createMode === "add-document") {
        let data;
        view === "settings"
          ? (data = {
              branchId: selectedBranchId && selectedBranchId,
              name: values.name,
              description:
                values.description === undefined ? "" : values.description,
              checkListItems: checkListItemsData,
            })
          : (data = {
              subjectId: clientprofileid,
              name: values.name,
              description:
                values.description === undefined ? "" : values.description,
              checkListItems: checkListItemsData,
            });

        onAddDocumentChecklist(data)
          .then((res) => {
            handleCancel();
            getUrl(res.payload);
          })
          .catch(() => {
            setLoading(false);
          })
          .then(() =>
            onGetDocumentChecklist(
              view === "settings" ? selectedBranchId : clientprofileid
            )
          )
          .catch(() => {
            setLoading(false);
          })
          .then((res) => {
            setLoading(false);
            message.success("Successfully Added!");
            if (view === "settings") {
            } else {
              var link = res.payload.items[res.payload.items.length - 1].link;
              showModal("send-email", link, values.name);
            }
          })
          .catch(() => {
            setLoading(false);
          });
      } else {
        const updata = {
          id: updatedata && updatedata.id,
          branchId: updatedata && updatedata.branchId,
          name: values.name,
          description:
            values.description === undefined ? "" : values.description,
          checkListItems: checkListItemsUpdate,
        };

        onUpdateDocumentChecklist(updata)
          .then(() => handleCancel())
          .catch(() => {
            setLoading(false);
          })
          .then(() => onGetDocumentChecklist(selectedBranchId))
          .catch(() => {
            setLoading(false);
          })
          .then(() => {
            setLoading(false);
            message.success("Successfully Updated!");
          })
          .catch(() => {
            setLoading(false);
          });
      }
    }
  };

  // console.log(
  //   "docChecklistitemsdocChecklistitemsdocChecklistitems",
  //   docChecklistitems
  // );

  return (
    <Spin size="large" spinning={loading}>
      <Fragment>
        {createMode && (
          <Form
            onFinish={onFinish}
            form={form}
            initialValues={Mode}
            className="width-100"
            name="main"
          >
            <div className="border-box-checklist">
              <Form.Item
                name="name"
                label="Name"
                colon={false}
                {...layoutInner}
                rules={[
                  {
                    required: true,
                    message: "Required!",
                  },
                ]}
              >
                <Input placeholder="Name" />
              </Form.Item>
              <Form.Item
                {...layoutSendingReceipt}
                style={{ marginTop: "12px" }}
                name="description"
                label="Description"
                colon={false}
              >
                <Input placeholder="Description" />
              </Form.Item>
            </div>

            {/* Hey this is testing Data */}

            <div style={{ marginTop: "12px" }} className="border-box-checklist">
              {/* THIS IS WORKING ORIGINAL Data */}

              <Form.List name="checkListItems">
                {(checkListItems, { add, remove }) => (
                  <div className="">
                    <Row className=" margin-contact-container">
                      <Col>
                        <span style={{ fontSize: "12px", color: "gray" }}>
                          Create a list of documents that you require from your
                          client, for example you can create a work visa
                          document list. Please note if you make a document
                          “Required” this means the client will not be able to
                          update any documents until the required document has
                          been added.
                        </span>
                      </Col>
                      <Col>
                        <Form.Item className="form-add-btn">
                          <img
                            style={{ marginBottom: "6px" }}
                            src={Images.addIcon}
                            className="icons-client "
                            type="primary"
                            onClick={() => add()}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={24}>
                        {checkListItems.map((field, index) => (
                          <Row className="file-delete-head" key={field.key}>
                            <Col xs={2} style={{ paddingTop: "15px" }}>
                              <p>{index + 1}</p>
                            </Col>
                            <Col xs={13}>
                              <Form.Item
                                {...field}
                                fieldKey={[field.fieldKey, "first"]}
                                name={[field.name, "name"]}
                                rules={[
                                  {
                                    required: true,
                                    message: "Required!",
                                  },
                                ]}
                              >
                                <Input.TextArea rows={6} />
                              </Form.Item>
                            </Col>
                            <Col xs={3} offset={1}>
                              <Row>
                                <Col xs={24}>
                                  <div style={{ display: "flex" }}>
                                    <div
                                      className="label-form"
                                      style={{
                                        marginTop: "19px",
                                        marginRight: "6px",
                                      }}
                                    >
                                      Required?
                                    </div>
                                    <Form.Item
                                      initialValue={true}
                                      {...field}
                                      fieldKey={[field.fieldKey, "required"]}
                                      name={[field.name, "required"]}
                                    >
                                      <Select>
                                        <Option value={true}>Yes</Option>
                                        <Option value={false}>No</Option>
                                      </Select>
                                    </Form.Item>
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={24}>
                                  <div style={{ display: "flex" }}>
                                    <div
                                      className="label-form"
                                      style={{
                                        marginTop: "19px",
                                        marginRight: "23px",
                                      }}
                                    >
                                      Priority
                                    </div>
                                    <Form.Item
                                      initialValue="0"
                                      {...field}
                                      fieldKey={[field.fieldKey, "priority"]}
                                      name={[field.name, "priority"]}
                                    >
                                      <Select>
                                        <Option value="0">Low</Option>
                                        <Option value="1">High</Option>
                                      </Select>
                                    </Form.Item>
                                  </div>
                                </Col>
                              </Row>
                            </Col>
                            <Col
                              xs={2}
                              offset={3}
                              style={{ paddingTop: "15px" }}
                            >
                              <a
                                href="javascript:"
                                onClick={() => {
                                  remove(field.name);
                                }}
                              >
                                <DeleteOutlined color="rgb(51, 170, 218)" />
                              </a>
                            </Col>
                          </Row>
                        ))}
                      </Col>
                    </Row>
                  </div>
                )}
              </Form.List>
            </div>

            <Row className="flex-end margin-top-12" gutter={10}>
              <Col>
                <Form.Item>
                  <Button
                    type="primary"
                    className="login-form-button save-btn"
                    htmlType="submit"
                    // onClick={() => showModal("send-email")}
                  >
                    SAVE
                  </Button>
                </Form.Item>
              </Col>
              <Col>
                <Form.Item>
                  <Button
                    onClick={() => handleCancel()}
                    type="primary"
                    className="login-form-button save-btn"
                  >
                    CLOSE
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        )}
      </Fragment>
    </Spin>
  );
};
export default DocumentChecklistForm;
