import React, { Fragment } from "react";
import { Table } from "antd";
import ReactToPrint, { PrintContextConsumer } from "react-to-print";

const columns = [
  {
    title: "EZM ID",
    dataIndex: "clientNumber",
    key: "clientNumber",
    render: (text, row, index) => {
      //   if (index == 0) {
      return <a>{text}</a>;
      //   }
      //   return {
      //     children: <a>{text}</a>,
      //     props: {
      //       colSpan: 6,
      //     },
      //   };
    },
  },
  {
    title: "EMPLOYER NAME",
    dataIndex: "employerName",
    key: "employerName",
  },
  {
    title: "LEGAL COMPANY Name",
    dataIndex: "business",
    key: "business",
  },
  {
    title: "CONTACT PERSON",
    dataIndex: "contactPerson",
    key: "contactPerson",
  },
  {
    title: "MOBILE",
    dataIndex: "mobile",
    key: "mobile",
  },
  {
    title: "PHONE",
    dataIndex: "dob",
    key: "dob",
  },
  {
    title: "EMAIL",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "CLIENT",
    dataIndex: "clientName",
    key: "clientName",
  },
  {
    title: "JOB SECTOR",
    dataIndex: "jobSector",
    key: "jobSector",
  },
  {
    title: "OCCUPATION",
    dataIndex: "occupation",
    key: "occupation",
  },
  {
    title: "PROCESSING PERSONS",
    dataIndex: "fullName",
    key: "fullName",
  },
];
// const SaleHistoryTable = ({ SaleHistoryReport, displayText }) => {
export class ClientEmployerTable extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      pageNumber: 1,
      pageSize: 10,
      totalPages: props.reportsCount,
      defaultCurrent: 1,
    };
  }

  paginate = (value) => {
     
    this.setState(
      { pageSize: value.pageSize, pageNumber: value.current },
      () => {
        this.props.onBirthdayReport().then(() => {
          this.setState(
            {
              defaultCurrent: 1,
              //   branchId: this.state.branchId,
            },
            () => {
              console.log("state", this.state);
            }
          );
        });
      }
    );
  };

  render() {
    const { birthdayReportRes, displayText } = this.props;
    return (
      <Fragment>
        <div className="report-table">
          <div className="rep-print-row-cont">
            <div
              className="pciq-top-div"
              style={{ marginBottom: 5, marginLeft: 10 }}
            >
              <span
                className="pc-top-div-text"
                style={{ color: "#0A3C5D", fontSize: 12 }}
              >
                {displayText}
              </span>
            </div>
          </div>
          <Table
            columns={columns}
            dataSource={birthdayReportRes && birthdayReportRes}
            pagination={false}
            className="border-3 table-head"
            bordered
            onChange={this.paginate}
            pagination={{
              defaultCurrent: this.state.defaultCurrent,
              total: this.state.totalPages,
              defaultPageSize: 10,
            }}
          />
        </div>
      </Fragment>
    );
  }
}
export default ClientEmployerTable;
