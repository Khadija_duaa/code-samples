import React, { Fragment } from "react";
import { Table } from "antd";
import ReactToPrint, { PrintContextConsumer } from "react-to-print";
import SaleHistoryTable from "./SaleHistoryTable";
import { Images } from "../../Themes";

export class SaleHistoryReport extends React.PureComponent {
  render() {
    const {
      saleHistoryRes,
      displayText,
      totalInvoiced,
      totalReceived,
      reportsCount,
      onSaleHistoryReport,
      requestData,
    } = this.props;
    return (
      <Fragment>
        <div className="report-table">
          <div className="rep-print-row-cont">
            <div />
            <div>
              <div className="rep-print-icon-cont">
                <ReactToPrint
                  trigger={() => {
                    // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
                    // to the root node of the returned component as it will be overwritten.
                    return (
                      <img
                        style={{ cursor: "pointer" }}
                        src={Images.printNBlue}
                        className="rep-print-icon"
                      />
                    );
                  }}
                  content={() => this.saleHistoryRef}
                />
              </div>
            </div>
          </div>
          <SaleHistoryTable
            saleHistoryRes={saleHistoryRes}
            ref={(el) => (this.saleHistoryRef = el)}
            displayText={displayText}
            totalInvoiced={totalInvoiced}
            totalReceived={totalReceived}
            reportsCount={reportsCount}
            onSaleHistoryReport={onSaleHistoryReport}
            requestData
          />
        </div>
      </Fragment>
    );
  }
}
export default SaleHistoryReport;
