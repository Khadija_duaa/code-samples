import React, { Fragment } from "react";
import { Table } from "antd";
import ReactToPrint, { PrintContextConsumer } from "react-to-print";
import { Images } from "../../Themes";
import EMedicalExpiryTable from "./EMedicalExpiryTable";
import PassportExpiryTable from "./PassportExpiryTable";

export class PassportExpiryReport extends React.PureComponent {
  render() {
    const {
      employerMedicalExpRes,
      displayText,
      reportsCount,
      onEmployerMedicalExpiry,
      requestData,
    } = this.props;
    return (
      <Fragment>
        <div className="report-table">
          <div className="rep-print-row-cont">
            <div />
            <div>
              <div className="rep-print-icon-cont">
                <ReactToPrint
                  trigger={() => {
                    // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
                    // to the root node of the returned component as it will be overwritten.
                    return (
                      <img
                        style={{ cursor: "pointer" }}
                        src={Images.printNBlue}
                        className="rep-print-icon"
                      />
                    );
                  }}
                  content={() => this.saleHistoryRef}
                />
              </div>
            </div>
          </div>
          <PassportExpiryTable
            saleHistoryRes={employerMedicalExpRes}
            ref={(el) => (this.saleHistoryRef = el)}
            displayText={displayText}
            reportsCount={reportsCount}
            onSaleHistoryReport={onEmployerMedicalExpiry}
            requestData={requestData}
          />
        </div>
      </Fragment>
    );
  }
}
export default PassportExpiryReport;
