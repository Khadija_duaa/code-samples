// @flow
import React, { useState, useEffect } from "react";
import { Images } from "../../Themes";
import { message, Spin, Button, Upload, Select, Input } from "antd";

function BranchUserCard(Props) {
  const [show, setShow] = useState(false);
  const [name, setName] = useState(Props.user.user.fullName);
  const [email, setEmail] = useState(Props.user.user.alternateEmail);
  const [isManager, setIsManager] = useState(Props.user.isManager);
  const [loading, setLoading] = useState(false);

  const [imageUrl, setImageUrl] = useState(
    Props.imageUrl ? Props.imageUrl : Props.user.user.image
  );
  const [isLock, setIsLock] = useState(Props.user.user.isLock);
  const [checkboxValue, setCheckboxValue] = useState("");

  let userId = localStorage.getItem("userId");

  useEffect(() => {
    setName(Props.user.user.fullName);
    setEmail(Props.user.user.alternateEmail);
    setIsManager(Props.user.isManager);
    setImageUrl(Props.imageUrl ? Props.imageUrl : Props.user.user.image);
    setIsLock(Props.user.user.isLock);
  }, [
    Props.user.user.fullName,
    Props.user.user.alternateEmail,
    Props.user.user.isManager,
    Props.user.user.isLock,
    Props.user.user.image,
    Props.user.isManager,
    Props.imageUrl,
  ]);

  const myChangeHandler = (value) => {
    console.log("show input value ", value);
    setCheckboxValue(value);
  };

  let userData = {
    Id: Props.user.user.id,
    fullName: name,
    alternateEmail: email,
    isManager: isManager,
    isOwner: Props.user.user.isOwner,
    RoleId: Props.user.user.roleId,
    Title: Props.user.user.title,
    Image: Props.imageUrl ? Props.imageUrl : imageUrl,
    ForgotPasswordBit: false,
    LastSignedIn: new Date(),
    Signature: "",
    TimeZone: "",
    comission: 0,
    agreementUrl: "",
    description: "",
    SignatureImage: "",
  };

  let disableData = {
    userId: Props.user.user.id,
    isLock: !isLock,
  };

  return (
    <div className="sus-member-card-bgcolor">
      <Spin spinning={loading}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <div>
            <div className="sus-profile-cont">
              <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action={(info) => Props.uploadImage(info, Props.user.userId)}
                onChange={Props.handleChangeImage}
              >
                {Props.imageUrl ? (
                  <img
                    src={Props.imageUrl}
                    alt="avatar"
                    style={{ width: 105, height: 105 }}
                  />
                ) : imageUrl ? (
                  <img
                    src={imageUrl}
                    alt="avatar"
                    style={{ width: 105, height: 105 }}
                  />
                ) : (
                  <img
                    src={Images.dummyUserImage}
                    className="sus-profile-img"
                  />
                )}
              </Upload>
            </div>
          </div>
          <div style={{ width: "56%" }}>
            <div className="profile-input-border">
              <Input
                className="profile-input"
                placeholder="Inderjeet"
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div className="profile-input-border" style={{ marginTop: 10 }}>
              <Input
                className="profile-input"
                placeholder="sarahayat123@gmail.com"
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div
              className="sus-checkbox-cont"
              style={{ width: "50%", marginTop: 15 }}
            >
              <Input
                type="checkbox"
                className="sus-checkbox"
                onChange={() => setIsManager(!isManager)}
                defaultChecked={isManager}
              />
              <span className="sus-disable-text" style={{ marginLeft: 10 }}>
                Branch Manager
              </span>
            </div>
            {Props.user.user.id != userId && (
              <div
                className="sus-disable-cont"
                style={{ cursor: "pointer" }}
                onClick={() => {
                  setLoading(true);
                   
                  Props.onDisable(disableData)
                    .then((res) => {
                      Props.onGetBranch()
                        .then((res) => {
                          setLoading(false);
                        })
                        .catch((err) => {
                          setLoading(false);
                        });
                    })
                    .catch((err) => {
                      setLoading(false);
                    });
                }}
              >
                <span className="sus-disable-text">
                  {Props.user.user.isLock
                    ? "Enable Team Member"
                    : "Disable Team Member"}
                </span>
                <img src={Images.disable} className="sus-disable-icon" />
              </div>
            )}
            <Button
              onClick={() => Props.onUpdate(userData, isManager)}
              type="primary"
              className="sus-add-btn"
              style={{ marginTop: 20 }}
            >
              SAVE
            </Button>
          </div>
        </div>
      </Spin>
    </div>
  );
}

export default BranchUserCard;
