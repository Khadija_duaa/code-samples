// @flow
import React, { useState } from "react";
import { Images } from "../../Themes";
import { message, Spin, Button, Upload, Select } from "antd";

function ManagerCard(Props) {
  const [show, setShow] = useState(false);
  const [name, setName] = useState(Props.manager.fullName);
  const [email, setEmail] = useState(Props.manager.alternateEmail);
  const [isManager, setIsManager] = useState(Props.manager.isManager);
  const [isOwner, setIsOwner] = useState(Props.manager.isOwner);
  const [telephoneNumber, setTelephone] = useState(
    Props.manager.userContact ? Props.manager.userContact.telephoneNumber : ""
  );
  const [mobileNumber, setMobileNum] = useState(
    Props.manager.userContact ? Props.manager.userContact.mobileNumber : ""
  );
  const [companyDesignation, setCompanyDesignation] = useState(
    Props.manager.title
  );
  const [city, setCity] = useState(
    Props.manager.userAddress ? Props.manager.userAddress.city : ""
  );
  const [country, setCountry] = useState(
    Props.manager.userAddress ? Props.manager.userAddress.country : ""
  );
  const [imageUrl, setImageUrl] = useState(
    Props.imageUrl ? Props.imageUrl : Props.manager.image
  );
  const [isLock, setIsLock] = useState(Props.manager.isLock);
  const [selectedOption, setSelectedOption] = useState(
    Props.manager.roleId == 3
      ? "User"
      : Props.manager.roleId === 2
      ? "Manager"
      : Props.manager.roleId === 1
      ? "Super User"
      : null
  );

  const [checkboxValue, setCheckboxValue] = useState("");

  const userOwner = localStorage.getItem("userOwner");
  const userManager = localStorage.getItem("userManager");

  const { Option } = Select;

  let userId = localStorage.getItem("userId");

  const myChangeHandler = (value) => {
    console.log("show input value ", value);
    setCheckboxValue(value);
  };

  const handleChange = (selectedOption) => {
    setSelectedOption(parseInt(selectedOption));
  };

  var postUserAddress = false;
  var postUserContact = false;

  if (Props.manager.userAddress) {
    postUserAddress = false;
  } else {
    postUserAddress = true;
  }

  if (Props.manager.userContact) {
    postUserContact = false;
  } else {
    postUserContact = true;
  }

  let userData = {
    Id: Props.manager.id,
    fullName: name,
    alternateEmail: email,
    companyDesignation: "",
    isManager: isManager,
    isOwner: isOwner,
    RoleId: 0,
    Title: companyDesignation,
    Image: Props.imageUrl ? Props.imageUrl : imageUrl,
    ForgotPasswordBit: false,
    LastSignedIn: new Date(),
    Signature: "",
    TimeZone: "",
    comission: 0,
    agreementUrl: "",
    description: "",
    SignatureImage: "",
  };

  let userContact = {
    fax1: "",
    fax2: "",
    mobileNumber: mobileNumber,
    mobileNumber2: "",
    telephoneNumber: telephoneNumber,
    telephoneNumber2: "",
    userId: Props.manager.id,
  };

  let userAddress = {
    country: country,
    city: city,
    address3: "",
    address2: "",
    address1: "",
    userId: Props.manager.id,
  };

  return (
    <div>
      <div
        className="sus-team-memb-setting"
        style={{ marginTop: 47, width: "100%" }}
      >
        {show && (
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              marginRight: -75,
              marginLeft: 45,
              marginTop: 20,
              width: "82%",
            }}
          >
            <div></div>
            <div
              className="sus-show-btn"
              onClick={() => {
                setShow(false);
              }}
            >
              <span className="sus-save-btn-text">HIDE</span>
            </div>
          </div>
        )}
        <div
          className="sus-member-card-cont"
          style={{
            height: "auto",
            marginTop: show ? 0 : 20,
          }}
        >
          <div
            className="form-cont"
            style={{
              justifyContent: "space-between",
              paddingTop: 20,
              paddingTop: 5,
              paddingBottom: 0,
              display: "block",
              width: "97%",
              marginLeft: -30,
              marginRight: -30,
            }}
          >
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <div>
                <div className="sus-profile-cont">
                  <Upload
                    name="avatar"
                    listType="picture-card"
                    className="avatar-uploader"
                    showUploadList={false}
                    action={(info) => Props.uploadImage(info, Props.manager.id)}
                    onChange={Props.handleChangeImage}
                  >
                    {Props.imageUrl ? (
                      <img
                        src={Props.imageUrl}
                        alt="avatar"
                        style={{ width: 105, height: 105 }}
                      />
                    ) : imageUrl ? (
                      <img
                        src={imageUrl}
                        alt="avatar"
                        style={{ width: 105, height: 105 }}
                      />
                    ) : (
                      <img
                        src={Images.dummyUserImage}
                        className="sus-profile-img"
                      />
                    )}
                  </Upload>
                </div>
              </div>
              <div style={{ width: "56%" }}>
                <div className="profile-input-border">
                  <input
                    className="profile-input"
                    placeholder="advisor"
                    type="text"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                </div>
                <div className="profile-input-border" style={{ marginTop: 10 }}>
                  <input
                    className="profile-input"
                    placeholder="testadvisor@ezymigrate.com"
                    type="text"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
                <div
                  className="select-options"
                  style={{ marginTop: 10, backgroundColor: "#FFFFFF" }}
                >
                  <Select
                    defaultValue={selectedOption}
                    bordered={false}
                    placeholder="Select Role"
                    onChange={handleChange}
                    style={{ width: "100%" }}
                  >
                    <Option key={1}>Super User</Option>
                    <Option key={2}>Manager</Option>
                    <Option key={3}>User</Option>
                  </Select>
                </div>

                {userOwner === "True" && (
                  <div
                    style={{
                      display: "flex",
                      marginTop: 20,
                      marginRight: -50,
                    }}
                  >
                    <div className="sus-checkbox-cont">
                      <input
                        type="checkbox"
                        className="sus-checkbox"
                        onChange={() => setIsManager(!isManager)}
                        defaultChecked={isManager}
                      />
                      <span className="sus-disable-text">Branch Manager</span>
                    </div>

                    <div className="sus-checkbox-cont">
                      <input
                        disabled={userOwner == "True" ? false : true}
                        type="checkbox"
                        className="sus-checkbox"
                        onChange={() => setIsOwner(!isOwner)}
                        defaultChecked={isOwner}
                      />
                      <span className="sus-disable-text">Owner</span>
                    </div>
                  </div>
                )}

                {Props.manager.id != userId && (
                  <div
                    className="sus-disable-cont"
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      let disableData = {
                        userId: Props.manager.id,
                        isLock: !isLock,
                      };

                      Props.onDisableUser(disableData)
                        .then((res) => {
                          setIsLock(!isLock);

                          if (disableData.isLock) {
                            message.success("User disabled successfully!");
                          } else {
                            message.success("User enabled successfully!");
                          }
                        })
                        .catch((err) => {});
                    }}
                  >
                    <span className="sus-disable-text">
                      {isLock ? "Enable Team Member" : "Disable Team Member"}
                    </span>
                    <img src={Images.disable} className="sus-disable-icon" />
                  </div>
                )}
              </div>
            </div>
            {show && (
              <div>
                <div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <p className="medical-label" style={{ fontSize: 11 }}>
                      Telephone No:
                    </p>
                    <div
                      className="profile-input-border"
                      style={{ width: "55%" }}
                    >
                      <input
                        className="profile-input"
                        placeholder=""
                        type="text"
                        value={telephoneNumber}
                        onChange={(e) => setTelephone(e.target.value)}
                      />
                    </div>
                  </div>

                  <div
                    style={{
                      display: "flex",
                      marginTop: 10,
                      justifyContent: "space-between",
                    }}
                  >
                    <p className="medical-label" style={{ fontSize: 11 }}>
                      Mobile No:
                    </p>
                    <div
                      className="profile-input-border"
                      style={{ width: "55%" }}
                    >
                      <input
                        className="profile-input"
                        placeholder=""
                        type="text"
                        value={mobileNumber}
                        onChange={(e) => setMobileNum(e.target.value)}
                      />
                    </div>
                  </div>

                  <div
                    style={{
                      display: "flex",
                      marginTop: 10,
                      justifyContent: "space-between",
                    }}
                  >
                    <p className="medical-label" style={{ fontSize: 11 }}>
                      Company Designation:
                    </p>
                    <div
                      className="profile-input-border"
                      style={{ width: "55%" }}
                    >
                      <input
                        className="profile-input"
                        placeholder=""
                        type="text"
                        value={companyDesignation}
                        onChange={(e) => setCompanyDesignation(e.target.value)}
                      />
                    </div>
                  </div>

                  <div
                    style={{
                      display: "flex",
                      marginTop: 10,
                      justifyContent: "space-between",
                    }}
                  >
                    <p className="medical-label" style={{ fontSize: 11 }}>
                      Country:
                    </p>
                    <div
                      className="profile-input-border"
                      style={{ width: "55%" }}
                    >
                      <input
                        className="profile-input"
                        placeholder=""
                        type="text"
                        value={country}
                        onChange={(e) => setCountry(e.target.value)}
                      />
                    </div>
                  </div>

                  <div
                    style={{
                      display: "flex",
                      marginTop: 10,
                      justifyContent: "space-between",
                    }}
                  >
                    <p className="medical-label" style={{ fontSize: 11 }}>
                      City:
                    </p>
                    <div
                      className="profile-input-border"
                      style={{ width: "55%" }}
                    >
                      <input
                        className="profile-input"
                        placeholder=""
                        type="text"
                        value={city}
                        onChange={(e) => setCity(e.target.value)}
                      />
                    </div>
                  </div>

                  <div
                    style={{
                      display: "flex",
                      marginTop: 15,
                      marginBottom: 15,
                    }}
                  >
                    <Button
                      onClick={() =>
                        Props.parentMethod(
                          userData,
                          userAddress,
                          userContact,
                          postUserAddress,
                          postUserContact
                        )
                      }
                      type="primary"
                      className="sus-add-btn"
                      style={{ marginTop: 15 }}
                    >
                      SAVE
                    </Button>
                  </div>
                </div>
              </div>
            )}

            {!show && (
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <div></div>
                <div
                  className="sus-show-btn"
                  onClick={() => setShow({ show: true })}
                >
                  <span className="sus-save-btn-text">SHOW</span>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default ManagerCard;
