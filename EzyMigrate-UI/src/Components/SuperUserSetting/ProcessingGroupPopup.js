// @flow
import React, { useState, useEffect } from "react";
import { Button, Select, Upload, Modal, Form, Space, Input } from "antd";
import { Images } from "../../Themes";

function processingGroupPopup(Props) {
  // const [show, setShow] = useState(false);

  // useEffect(() => {}, [Props]);

  return (
    <div>
      {/*<Modal
        visible={this.state.visible}
        width="500"
        height="300"
        effect="fadeInUp"
        onClickAway={() => this.closeModal()}
      >
        <div style={{ padding: 40 }}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div className="sus-modal-label">
              <span className="sus-modal-label-text">
                PROCESSING PERSON GROUPS
              </span>
            </div>
            <div onClick={this.closeModal}>
              <img src={Images.crossRed} style={{ width: 20 }} />
            </div>
          </div>
          <div className="sus-add-processing-person-form">
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <p class="medical-label" style={{ fontSize: 11, marginLeft: 15 }}>
                Group Name*:
              </p>
              <div class="profile-input-border" style={{ width: "60%" }}>
                <input
                  className="profile-input"
                  placeholder=""
                  type="text"
                  value={groupName}
                  onChange={e => this.myChangeHandler("groupName", e)}
                />
              </div>
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: 15
              }}
            >
              <p class="medical-label" style={{ fontSize: 11, marginLeft: 15 }}>
                Group Email*:
              </p>
              <div class="profile-input-border" style={{ width: "60%" }}>
                <input
                  className="profile-input"
                  placeholder=""
                  type="text"
                  value={groupEmail}
                  onChange={e => this.myChangeHandler("groupEmail", e)}
                />
              </div>
            </div>
          </div>

          <div className="sus-add-processing-person-form">
            <Form>
              <Form.List name="sights">
                {(fields, { add, remove }) => (
                  <>
                    {fields.map(field => (
                      <Space key={field.key} align="baseline">
                        <Form.Item
                          noStyle
                          shouldUpdate={(prevValues, curValues) =>
                            prevValues.area !== curValues.area ||
                            prevValues.sights !== curValues.sights
                          }
                        >
                          {() => (
                            <Form.Item
                              {...field}
                              label="Sight"
                              name={[field.name, "sight"]}
                              fieldKey={[field.fieldKey, "sight"]}
                              rules={[
                                { required: true, message: "Missing sight" }
                              ]}
                            >
                              <Input />
                            </Form.Item>
                          )}
                        </Form.Item>
                        <Form.Item
                          {...field}
                          label="Price"
                          name={[field.name, "price"]}
                          fieldKey={[field.fieldKey, "price"]}
                          rules={[{ required: true, message: "Missing price" }]}
                        >
                          <Input />
                        </Form.Item>

                        <MinusCircleOutlined
                          onClick={() => remove(field.name)}
                        />
                      </Space>
                    ))}
                    <Form.Item>
                      <Button
                        type="dashed"
                        onClick={() => add()}
                        block
                        icon={<PlusOutlined />}
                      >
                        Add sights
                      </Button>
                    </Form.Item>
                  </>
                )}
              </Form.List>
            </Form>
          </div>

          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              marginTop: 30
            }}
          >
            <div></div>
            <div style={{ display: "flex" }}>
              <Button
                onClick={
                  isUpdateGroup ? () => this.updateGroup() : this.addGroup
                }
                loading={addGroupLoading}
                type="primary"
                className="sus-add-btn"
              >
                SAVE
              </Button>
              <Button
                onClick={this.closeModal}
                type="primary"
                className="sus-add-btn"
                style={{ marginLeft: 10 }}
              >
                CLOSE
              </Button>
            </div>
          </div>
        </div>
      </Modal>*/}
    </div>
  );
}

export default processingGroupPopup;
