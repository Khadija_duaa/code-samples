import React, { Fragment } from "react";

const PreviewScreen = ({}) => {
   
  return (
    <Fragment>
      <iframe
        style={{ width: "100%", height: "100%" }}
        src="https://cs110032000b9dd1aa8.blob.core.windows.net/documentmanagement/04b4225c-ff4d-4562-adb7-61c7945d142aLogo.gif"
      ></iframe>
    </Fragment>
  );
};

export default PreviewScreen;
