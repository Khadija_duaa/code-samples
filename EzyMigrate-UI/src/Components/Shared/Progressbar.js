import React from "react";

const ProgressBar = (props) => {
  const { bgcolor, completed, color } = props;

  const containerStyles = {
    height: 20,
    width: '100%',
    backgroundColor: "#e0e0de",
    borderRadius: 5,
  }

  const fillerStyles = {
    height: '100%',
    width: `${completed}%`,
    backgroundColor: bgcolor,
    borderRadius: 'inherit',
    textAlign: 'center'
  }

  const labelStyles = {
    padding: 5,
    color: color,
    fontWeight: '600',
    fontSize: 12
  }

  return (
    <div style={containerStyles}>
      <div style={fillerStyles}>
        {color && <span style={labelStyles}>{`${completed}%`}</span>}
      </div>
    </div>
  );
};

export default ProgressBar;
