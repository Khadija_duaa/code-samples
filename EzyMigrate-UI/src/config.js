export const redirectOutlookUrl = `${window.location.origin}/outlook-integration`;
export const redirectGmailUrl = `${window.location.origin}/account-setting`;
export const redirectUrlXero = window.location.origin + "/xero-sync";
export const redirectUrl = "https://app-stage.ezymigrate.co.nz/document-sync";
export const messageConfig = { top: 50, duration: 1 };
export const forgotPasswordUrl = `${window.location.origin}/forgot-password/`;
