import React, { Component } from "react";
import {
  Router,
  Switch,
  Route,
  Redirect,
  BrowserRouter,
} from "react-router-dom";
import history from "./services/history";
import Routes from "./routes";
import { connect, Provider } from "react-redux";
import { initStore } from "./store";
import jwt_decode from "jwt-decode";
import {
  User,
  Theme,
  AdminLogin,
  LinkQuestionnaire,
  ForgotPassword,
  SupervisorLogin,
} from "./Containers";
import queryString from "query-string";
import SignUp from "./Containers/SignUp";
import SignUpForm from "./Containers/SignUpForm";
import "./firebase";
// import AdminLogin from "./Containers/AdminLogin";
import { PowerInputSharp } from "@material-ui/icons";
import { Spin } from "antd";
import { bindActionCreators } from "redux";
import { getXeroToken } from "./store/Actions";
import PrivacyPolicy from "./PrivacyPolicy/PrivacyPolicy";
import { message } from "antd";
import { messageConfig } from "./config";
const { top, duration } = messageConfig;
var jwtDecoded = null;

let token = localStorage.getItem("token");
let admintoken = localStorage.getItem("admintoken");

if (token && token != "undefined") {
  jwtDecoded = jwt_decode(token);
}

class App extends Component {
  constructor(props) {
    message.config({
      top: top,
      duration: duration,
    });
    super(props);
    this.state = {
      auth: token,
      adminauth: admintoken,
    };
  }

  componentWillMount() {
    // this.props.getXeroToken();
  }

  getThemeLayout = () => {
    var isSupervisor = localStorage.getItem("isSupervisor") === "True";

    let path = "";
    if (isSupervisor || history.location.pathname === "/login/supervisor") {
      path = "/supervisor/batches";
      window.history.pushState(
        { urlPath: "/supervisor/batches" },
        "",
        "/supervisor/batches"
      );
    } else if (
      history.location.pathname === "/" ||
      history.location.pathname === "/login" ||
      history.location.pathname === "/login/"
    ) {
      path = "/dashboardBI";
      window.history.pushState({ urlPath: "/dashboardBI" }, "", "/dashboardBI");
    } else if (history.location.pathname === "/admin-login") {
      window.history.pushState({ urlPath: "/companies" }, "", "/companies");
      path = "/companies";
    } else {
      if (this.state.adminauth) {
        path = "/companies";
      } else {
        path = history.location.pathname;
      }
    }
    return this.state.auth || this.state.adminauth ? (
      <>
        {localStorage.getItem("userSystemPermissions") ||
        localStorage.getItem("isSupervisor") === "True" ? (
          <Redirect to={path} />
        ) : // setTimeout(() => {
        //   this.getThemeLayout();
        // }, 500)
        null}
      </>
    ) : (
      <Redirect
        to={
          history.location.pathname == "/admin-login"
            ? "/admin-login"
            : history.location.pathname == "/CustomQuestionnaire/Survey"
            ? "/CustomQuestionnaire/Survey"
            : history.location.pathname == "/Home/PrivacyPolicy"
            ? "/Home/PrivacyPolicy"
            : history.location.pathname == "/sign-up-form"
            ? "/sign-up-form"
            : history.location.pathname.indexOf("forgot-password") > -1
            ? history.location.pathname
            : history.location.pathname == "/login/supervisor"
            ? "/login/supervisor"
            : "/login"
        }
      />
    );
  };
  getHomePage = () => {
    let jsx = null;
    if (
      this.state.auth ||
      this.state.adminauth
      // history.location.pathname !== "/admin-login"
    ) {
      jsx = <Route key={0} path="/" component={Theme} />;
    } else {
      // jsx = (
      //   <Route
      //     path={
      //       history.location.pathname == "/admin-login"
      //         ? "/admin-login"
      //         : "/login"
      //     }
      //     component={
      //       history.location.pathname === "/admin-login"
      //         ? AdminLogin
      //         : User
      //     }
      //   />
      // );
    }
    return jsx;
  };

  getToken = (token) => {
    this.setState({ auth: token });
  };

  getAdminToken = (admintoken) => {
    this.setState({ adminauth: admintoken });
  };

  authLink = () => {
    let token = localStorage.getItem("token");

    const { exp } = jwtDecoded(token);
    const expirationTime = exp * 1000 - 60000;
    if (Date.now() >= expirationTime) {
      localStorage.clear();
      window.location.reload();
      // set LocalStorage here based on response;
    }
    return this.state.auth ? <Redirect to={"/login"} /> : "";
  };

  checkXeroValidations = () => {
    if (
      this.state.auth &&
      history.location.pathname === "/xero-sync-add-account"
    ) {
      // this.props.history.push("/xero-sync")
      return <Redirect to={"/xero-sync"} />;
    } else if (this.state.auth && history.location.pathname === "/xero-sync") {
      return <Redirect to={"/xero-sync"} />;
    } else if (
      this.state.auth &&
      history.location.pathname === "/xero-sync-invoices"
    ) {
      return <Redirect to={"/xero-sync"} />;
    } else {
    }
  };

  render() {
    return (
      <Provider store={initStore()}>
        <BrowserRouter>
          <Switch>
            {history.location && (
              <>
                <Route
                  exact
                  path="/login"
                  render={(props) => (
                    <User {...props} getToken={this.getToken} />
                  )}
                />
                <Route
                  exact
                  path="/forgot-password/:token"
                  component={ForgotPassword}
                />
                <Route
                  exact
                  path="/admin-login"
                  render={(props) => (
                    <AdminLogin {...props} getAdminToken={this.getAdminToken} />
                  )}
                />
                <Route
                  exact
                  path="/login/supervisor"
                  component={SupervisorLogin}
                />
                <Route
                  exact
                  path="/CustomQuestionnaire/Survey"
                  render={(props) => <LinkQuestionnaire {...props} />}
                />
                <Route
                  exact
                  path="/Home/PrivacyPolicy"
                  render={(props) => <PrivacyPolicy {...props} />}
                />
                <Route path="/sign-up-form" component={SignUpForm} />
                {/* <Redirect to={"/sign-up-form"} /> */}

                {history.location.pathname == "/CustomQuestionnaire/Survey"
                  ? null
                  : history.location.pathname == "/Home/PrivacyPolicy"
                  ? null
                  : this.getHomePage()}

                {history.location.pathname == "/CustomQuestionnaire/Survey"
                  ? null
                  : history.location.pathname == "/Home/PrivacyPolicy"
                  ? null
                  : this.getThemeLayout()}
                {history.location.pathname != "/CustomQuestionnaire/Survey"
                  ? null
                  : history.location.pathname == "/Home/PrivacyPolicy"
                  ? null
                  : this.checkXeroValidations()}
              </>
            )}
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // userData: state.userReducer && state.userReducer.userData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    // getXeroToken: bindActionCreators(getXeroToken, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
