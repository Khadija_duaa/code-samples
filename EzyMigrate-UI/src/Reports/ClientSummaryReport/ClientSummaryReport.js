import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Table } from "antd";
//import { Test } from './PotentialClientDashboardReport.styles';

class ClientSummaryReport extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentWillMount = () => {
    console.log("PotentialClientDashboardReport will mount");
  };

  componentDidMount = () => {
     
    console.log("PotentialClientDashboardReport mounted");
  };

  componentWillReceiveProps = (nextProps) => {
    console.log("PotentialClientDashboardReport will receive props", nextProps);
  };

  componentWillUpdate = (nextProps, nextState) => {
    console.log(
      "PotentialClientDashboardReport will update",
      nextProps,
      nextState
    );
  };

  componentDidUpdate = () => {
    console.log("PotentialClientDashboardReport did update");
  };

  componentWillUnmount = () => {
    console.log("PotentialClientDashboardReport will unmount");
  };

  render() {
    const {
      Clients,
      pageSize,
      currentPage,
      totalPages,
      columns,
      paginate,
    } = this.props;

    return (
      <Table
        className="dashboard-summary-table"
        rowClassName={(record, index) =>
          index % 2 === 0 ? "table-row-light" : "table-row-dark"
        }
        columns={columns}
        dataSource={Clients}
        onChange={paginate}
        pagination={{
          defaultCurrent: currentPage,
          total: totalPages,
          showSizeChanger: true,
          defaultPageSize: pageSize,
          pageSizeOptions: ["5", "10", "15", "20", "50"],
        }}
        scroll={{ x: "auto", y: 540 }}
      />
    );
  }
}

export default ClientSummaryReport;
