import React from "react";
import Select from "react-select";
import "./ReportsStyles.css";
import HeaderBar from "../Components/Header/HeaderBar";

import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import { Images } from "../Themes";
import PotentialHeaderTabs from "../Components/Header/PotentialHeaderTabs";
import DatePicker from "react-date-picker";

import ProgressBar from "../Components/Shared/Progressbar";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" }
];

const headOption = [
  { tabName: "Reports", linkName: "/report" },
  { tabName: "Visa Reports", linkName: "/visa-report" },
  { tabName: "Case Management", linkName: "/" },
  { tabName: "Potential Clients", linkName: "/potential-clients-report" },
  { tabName: "Time Tracking", linkName: "/time-tracking" },
  { tabName: "Employer Report", linkName: "/" }
];

class TimeTracking extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeMainTab: "document",
      activeTab: "questionnaire",
      extend: false
    };
  }

  myChangeHandler = text => {
    this.setState({ username: text });
  };

  onChange = value => {
    console.log(`selected ${value}`);
  };

  onBlur = () => {
    console.log("blur");
  };

  onFocus = () => {
    console.log("focus");
  };

  onSearch = val => {
    console.log("search:", val);
  };

  onChangeMainTab = value => {
    this.setState({ activeMainTab: value });
  };

  onChangeTab = value => {
    this.setState({ activeTab: value });
  };

  handleChange = selectedOption => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };

  handleCheck = () => {
    this.setState({ checked: !this.state.checked });
  };

  render() {
    const { selectedOption } = this.state;
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            <PotentialHeaderTabs
              data={headOption}
              activeTab="Potential Clients"
            />

            <div className="report-container">
              <div>
                <div>
                  <div className="pciq-top-div">
                    <span
                      className="pc-top-div-text"
                      style={{ color: "#0A3C5D" }}
                    >
                      VISA REPORT FILTERS
                    </span>
                  </div>

                  <div className="ca-gray-cont" style={{ border: 0 }}>
                    <div style={{ display: "flex", marginLeft: 20 }}>
                      <div style={{ display: "flex", width: "48%" }}>
                        <div
                          className="profile-input-border"
                          style={{
                            width: "100%",
                            overflow: "inherit",
                            paddingTop: 6,
                            paddingBottom: 6,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "space-between",
                            paddingRight: 5
                          }}
                        >
                          <DatePicker
                            calendarIcon={null}
                            clearIcon={null}
                            onChange={this.onChangeDate}
                            value={this.state.date}
                          />
                          <img
                            src={Images.calendar}
                            className="profile-calendar-icon"
                          />
                        </div>
                      </div>
                      <div
                        style={{
                          marginLeft: 20,
                          display: "flex",
                          width: "48%"
                        }}
                      >
                        <div
                          className="profile-input-border"
                          style={{
                            width: "100%",
                            overflow: "inherit",
                            paddingTop: 6,
                            paddingBottom: 6,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "space-between",
                            paddingRight: 5
                          }}
                        >
                          <DatePicker
                            calendarIcon={null}
                            clearIcon={null}
                            onChange={this.onChangeDate}
                            value={this.state.date}
                          />
                          <img
                            src={Images.calendar}
                            className="profile-calendar-icon"
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      style={{ display: "flex", marginLeft: 20, marginTop: 10 }}
                    >
                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 0, width: "48%" }}
                      >
                        <div className="select-options">
                          <Select
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={options}
                          />
                        </div>
                      </div>

                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 20, width: "48%" }}
                      >
                        <div className="select-options">
                          <Select
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={options}
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      style={{ display: "flex", marginLeft: 20, marginTop: 10 }}
                    >
                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 0, width: "48%" }}
                      >
                        <div className="select-options">
                          <Select
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={options}
                          />
                        </div>
                      </div>

                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 20, width: "48%" }}
                      >
                        <div className="select-options">
                          <Select
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={options}
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      style={{
                        display: "flex",
                        marginRight: 20,
                        justifyContent: "space-between",
                        marginLeft: 20,
                        marginTop: 10
                      }}
                    >
                      <div />

                      <div style={{ display: "flex" }}>
                        <div
                          className="pc-btn-cont"
                          style={{
                            paddingTop: 5,
                            paddingBottom: 5,
                            borderRadius: 3,
                            marginLeft: 15
                          }}
                        >
                          <span className="pc-btn-text">SHOW</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div
                    className="rep-print-row-cont"
                    style={{ marginRight: 20, marginTop: 20 }}
                  >
                    <div
                      className="pciq-top-div"
                      style={{ marginBottom: 5, marginTop: 0 }}
                    >
                      <span
                        className="pc-top-div-text"
                        style={{ color: "#0A3C5D", fontSize: 12 }}
                      >
                        CASE DETAIL
                      </span>
                    </div>
                    <div style={{ display: "flex" }}>
                      <div
                        className="pc-btn-cont"
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5,
                          borderRadius: 3,
                          marginLeft: 15
                        }}
                      >
                        <span className="pc-btn-text">EXPORT</span>
                      </div>
                    </div>
                  </div>

                  <table
                    className="ca-invoice-table-cont"
                    style={{ borderSpacing: 1, border: 0, marginTop: 10 }}
                  >
                    <tbody>
                      <tr style={{ backgroundColor: "#F8F9FB" }}>
                        <th className="ca-table-heading">Sn</th>
                        <th className="ca-table-heading">Client</th>
                        <th className="ca-table-heading">Visa Type</th>
                        <th className="ca-table-heading">Service Type</th>
                        <th className="ca-table-heading">User</th>
                        <th className="ca-table-heading">Time Spent(min)</th>
                        <th className="ca-table-heading">Description</th>
                        <th className="ca-table-heading">Work Type</th>
                        <th className="ca-table-heading">Visa Status</th>
                      </tr>
                      <tr style={{ backgroundColor: "#FFFFFF" }}>
                        <td className="report-table-content-text">1</td>
                        <td className="report-table-content-text">asdads</td>
                        <td className="report-table-content-text">Work Visa</td>
                        <td className="report-table-content-text">asdasf</td>
                        <td className="report-table-content-text">asdfss</td>
                        <td className="report-table-content-text">
                          0 hour(s) and 0 minute(s)
                        </td>
                        <td className="report-table-content-text">asdf</td>
                        <td className="report-table-content-text">ewfd</td>
                        <td className="report-table-content-text">Active</td>
                      </tr>
                    </tbody>
                  </table>

                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <div />
                    <div style={{ display: "flex" }}>
                      <div
                        className="pciq-top-div"
                        style={{ marginBottom: 5, marginTop: 0 }}
                      >
                        <span
                          className="pc-top-div-text"
                          style={{ color: "#0A3C5D", fontSize: 12 }}
                        >
                          Total Time Spent:{" "}
                        </span>
                      </div>

                      <div
                        className="pciq-top-div"
                        style={{ marginBottom: 5, marginTop: 0 }}
                      >
                        <span
                          className="pc-top-div-text"
                          style={{ color: "#0A3C5D", fontSize: 12 }}
                        >
                          0 hour(s) and 0 minute(s)
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {false && (
              <div
                className="report-container"
                style={{ paddingTop: 30, paddingBottom: 30 }}
              >
                <div className="pciq-top-div">
                  <span
                    className="pc-top-div-text"
                    style={{ color: "#0A3C5D" }}
                  >
                    VISA DETAIL
                  </span>
                </div>

                <div className="pc-text-field-row" style={{ marginBottom: 20 }}>
                  <div>
                    <span className="cv-normal-text">Show</span>
                  </div>
                  <div
                    className="select-options"
                    style={{ width: 100, marginLeft: 15 }}
                  >
                    <Select
                      value={selectedOption}
                      onChange={this.handleChange}
                      options={options}
                    />
                  </div>

                  <div>
                    <span className="cv-normal-text">entries</span>
                  </div>
                </div>

                <table
                  className="ca-invoice-table-cont"
                  style={{
                    borderSpacing: 1,
                    border: 0,
                    backgroundColor: "#BFBFBF",
                    borderRadius: 0
                  }}
                >
                  <tbody>
                    <tr style={{ backgroundColor: "#F8F9FB" }}>
                      <th className="ca-table-heading">Sn</th>
                      <th className="ca-table-heading">Name</th>
                      <th className="ca-table-heading">Email</th>
                      <th className="ca-table-heading">Visa Type</th>
                      <th className="ca-table-heading">Visa Status</th>
                      <th className="ca-table-heading">Visa Modified Date</th>
                      <th className="ca-table-heading">Filter Status</th>
                    </tr>
                    <tr style={{ backgroundColor: "#FFFFFF" }}>
                      <td className="report-table-content-text">1</td>
                      <td className="report-table-content-text">
                        Benedict Thomas
                      </td>
                      <td className="report-table-content-text">
                        vshgh@gmail.com
                      </td>
                      <td className="report-table-content-text">
                        visitor visa
                      </td>
                      <td className="report-table-content-text">Active</td>
                      <td className="report-table-content-text">13/01/2020</td>
                      <td className="report-table-content-text"></td>
                    </tr>
                    <tr style={{ backgroundColor: "#FFFFFF" }}>
                      <td className="report-table-content-text">2</td>
                      <td className="report-table-content-text">
                        Benedict Thomas
                      </td>
                      <td className="report-table-content-text">
                        vshgh@gmail.com
                      </td>
                      <td className="report-table-content-text">
                        visitor visa
                      </td>
                      <td className="report-table-content-text">Active</td>
                      <td className="report-table-content-text">13/01/2020</td>
                      <td className="report-table-content-text"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default TimeTracking;
