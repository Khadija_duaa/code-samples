import React from "react";
import Select from "react-select";
import "./ReportsStyles.css";
import HeaderBar from "../Components/Header/HeaderBar";

import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import { Images } from "../Themes";
import PotentialHeaderTabs from "../Components/Header/PotentialHeaderTabs";
import DatePicker from "react-date-picker";

import ProgressBar from "../Components/Shared/Progressbar";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const headOption = [
  { tabName: "Reports", linkName: "/report" },
  { tabName: "Visa Reports", linkName: "/" },
  { tabName: "Case Management", linkName: "/" },
  { tabName: "Potential Clients", linkName: "/potential-clients-report" },
  { tabName: "Time Tracking", linkName: "/" },
  { tabName: "Employer Report", linkName: "/" },
];

class EmployerReport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeMainTab: "document",
      activeTab: "questionnaire",
      extend: false,
    };
  }

  myChangeHandler = (text) => {
    this.setState({ username: text });
  };

  onChange = (value) => {
    console.log(`selected ${value}`);
  };

  onBlur = () => {
    console.log("blur");
  };

  onFocus = () => {
    console.log("focus");
  };

  onSearch = (val) => {
    console.log("search:", val);
  };

  onChangeMainTab = (value) => {
    this.setState({ activeMainTab: value });
  };

  onChangeTab = (value) => {
    this.setState({ activeTab: value });
  };

  handleChange = (selectedOption) => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };

  handleCheck = () => {
    this.setState({ checked: !this.state.checked });
  };

  render() {
    const { selectedOption } = this.state;
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            <PotentialHeaderTabs
              data={headOption}
              activeTab="Potential Clients"
            />

            <div className="report-container">
              <div>
                <div>
                  <div className="pciq-top-div">
                    <span
                      className="pc-top-div-text"
                      style={{ color: "#0A3C5D" }}
                    >
                      Report Filter
                    </span>
                  </div>

                  <div className="ca-gray-cont" style={{ border: 0 }}>
                    <div style={{ display: "flex", marginLeft: 20 }}>
                      <div style={{ display: "flex", width: "48%" }}>
                        <div
                          className="profile-input-border"
                          style={{
                            width: "100%",
                            overflow: "inherit",
                            paddingTop: 6,
                            paddingBottom: 6,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "space-between",
                            paddingRight: 5,
                          }}
                        >
                          <DatePicker
                            calendarIcon={null}
                            clearIcon={null}
                            onChange={this.onChangeDate}
                            value={this.state.date}
                          />
                          <img
                            src={Images.calendar}
                            className="profile-calendar-icon"
                          />
                        </div>
                      </div>
                      <div
                        style={{
                          marginLeft: 20,
                          display: "flex",
                          width: "48%",
                        }}
                      >
                        <div
                          className="profile-input-border"
                          style={{
                            width: "100%",
                            overflow: "inherit",
                            paddingTop: 6,
                            paddingBottom: 6,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "space-between",
                            paddingRight: 5,
                          }}
                        >
                          <DatePicker
                            calendarIcon={null}
                            clearIcon={null}
                            onChange={this.onChangeDate}
                            value={this.state.date}
                          />
                          <img
                            src={Images.calendar}
                            className="profile-calendar-icon"
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      style={{ display: "flex", marginLeft: 20, marginTop: 10 }}
                    >
                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 0, width: "48%" }}
                      >
                        <div className="select-options">
                          <Select
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={options}
                          />
                        </div>
                      </div>

                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 20, width: "48%" }}
                      >
                        <div className="select-options">
                          <Select
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={options}
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="report-btn-tab-row">
                    <div className="pcr-active-btn-tab">
                      <span className="pc-btn-text">SALES HISTORY</span>
                    </div>

                    <div
                      className="pcr-inactive-btn-tab"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="pc-btn-text">CLIENT SOURCE</span>
                    </div>

                    <div
                      className="pcr-inactive-btn-tab"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="pc-btn-text">PROCESSING PERSONS</span>
                    </div>

                    <div
                      className="pcr-inactive-btn-tab"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="pc-btn-text">FILE NOTES</span>
                    </div>

                    <div
                      className="pcr-inactive-btn-tab"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="pc-btn-text">
                        POTENTIAL CLIENT STATUS
                      </span>
                    </div>
                  </div>

                  {/* ===== Design from Medical Expiring Employer report Screen ===== */}

                  {true && (
                    <div>
                      <div className="rep-print-row-cont">
                        <div
                          className="pciq-top-div"
                          style={{ marginBottom: 5 }}
                        >
                          <span
                            className="pc-top-div-text"
                            style={{ color: "#0A3C5D", fontSize: 12 }}
                          >
                            SALE'S PERSON
                          </span>
                        </div>
                        <div>
                          <div className="rep-print-icon-cont">
                            <img
                              src={Images.printNBlue}
                              className="rep-print-icon"
                            />
                          </div>
                        </div>
                      </div>

                      <table
                        className="ca-invoice-table-cont"
                        style={{ borderSpacing: 1, border: 0 }}
                      >
                        <tbody>
                          <tr style={{ backgroundColor: "#F8F9FB" }}>
                            <th className="ca-table-heading">Sn</th>
                            <th className="ca-table-heading">First Name</th>
                            <th className="ca-table-heading">Last Name</th>
                            <th className="ca-table-heading">Client Tags</th>
                            <th className="ca-table-heading">
                              Visa Type, Visa Status
                            </th>
                            <th className="ca-table-heading">Employer Name</th>
                            <th className="ca-table-heading">Issue Date</th>
                            <th className="ca-table-heading">Expiry Date</th>
                          </tr>
                          <tr style={{ backgroundColor: "#FFFFFF" }}>
                            <td
                              className="report-table-content-text"
                              style={{
                                textAlign: "left",
                                paddingTop: 10,
                                paddingBottom: 10,
                              }}
                              colSpan={6}
                            >
                              <div className="report-table-first-row">
                                <div>
                                  <span>Sales Person :</span>
                                </div>
                                <div className="report-column-btn">
                                  <span style={{ color: "#FFFFFF" }}>
                                    Sam Bam
                                  </span>
                                </div>
                                <div className="report-column-btn">
                                  <span style={{ color: "#FFFFFF" }}>
                                    advisor
                                  </span>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr style={{ backgroundColor: "#FFFFFF" }}>
                            <td className="report-table-content-text">1</td>
                            <td className="report-table-content-text">
                              jaspreet
                            </td>
                            <td className="report-table-content-text">kaur</td>
                            <td className="report-table-content-text">
                              vshgh@gmail.com
                            </td>
                            <td className="report-table-content-text">0.00</td>
                            <td className="report-table-content-text">0.00</td>
                          </tr>
                          <tr style={{ backgroundColor: "#FFFFFF" }}>
                            <td className="report-table-content-text">2</td>
                            <td className="report-table-content-text">
                              potential client
                            </td>
                            <td className="report-table-content-text">kaur</td>
                            <td className="report-table-content-text">
                              vshgh@gmail.com
                            </td>
                            <td className="report-table-content-text">
                              300.00
                            </td>
                            <td className="report-table-content-text">
                              300.00
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <div className="report-total-main-cont">
                        <div />

                        <div className="report-total-cont">
                          <div className="report-total-text-div">
                            <span
                              className="cv-bold-text"
                              style={{ marginRight: 20, fontSize: 10 }}
                            >
                              Total
                            </span>
                          </div>
                          <div
                            className="rep-rec-am-total-cont"
                            style={{ width: "33%" }}
                          >
                            <span
                              className="cv-bold-text"
                              style={{ fontSize: 10 }}
                            >
                              300.00
                            </span>
                          </div>
                          <div className="rep-rec-am-total-cont">
                            <span
                              className="cv-bold-text"
                              style={{ fontSize: 10 }}
                            >
                              300.00
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EmployerReport;
