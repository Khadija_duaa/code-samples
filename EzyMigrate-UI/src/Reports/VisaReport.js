import React from "react";
import "./ReportsStyles.css";
import { Select, DatePicker, Table, Tag, Space, Spin } from "antd";

import { CSVLink } from "react-csv";

import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import { Images } from "../Themes";
import PotentialHeaderTabs from "../Components/Header/PotentialHeaderTabs";

import ProgressBar from "../Components/Shared/Progressbar";
import VisaReportChild from "../Components/Reports/VisaReport";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const { Option } = Select;

const headOption = [
  { tabName: "Reports", linkName: "/report" },
  { tabName: "Visa Reports", linkName: "/visa-report" },
  { tabName: "Case Management", linkName: "/" },
  { tabName: "Potential Clients", linkName: "/potential-clients-report" },
  { tabName: "Time Tracking", linkName: "/" },
  { tabName: "Employer Report", linkName: "/" },
];
const dateFormat = "DD/MM/YYYY";

const headers = [
  { label: "First Name", key: "firstName" },
  { label: "Last Name", key: "lastName" },
  { label: "Email", key: "email" },
  { label: "Age", key: "age" },
];

const data = [
  {
    firstName: "Warren",
    lastName: "Morrow",
    email: "sokyt@mailinator.com",
    age: "36",
  },
  {
    firstName: "Gwendolyn",
    lastName: "Galloway",
    email: "weciz@mailinator.com",
    age: "76",
  },
  {
    firstName: "Astra",
    lastName: "Wyatt",
    email: "quvyn@mailinator.com",
    age: "57",
  },
  {
    firstName: "Jasmine",
    lastName: "Wong",
    email: "toxazoc@mailinator.com",
    age: "42",
  },
  {
    firstName: "Brooke",
    lastName: "Mcconnell",
    email: "vyry@mailinator.com",
    age: "56",
  },
  {
    firstName: "Christen",
    lastName: "Haney",
    email: "pagevolal@mailinator.com",
    age: "23",
  },
  {
    firstName: "Tate",
    lastName: "Vega",
    email: "dycubo@mailinator.com",
    age: "87",
  },
  {
    firstName: "Amber",
    lastName: "Brady",
    email: "vyconixy@mailinator.com",
    age: "78",
  },
  {
    firstName: "Philip",
    lastName: "Whitfield",
    email: "velyfi@mailinator.com",
    age: "22",
  },
  {
    firstName: "Kitra",
    lastName: "Hammond",
    email: "fiwiloqu@mailinator.com",
    age: "35",
  },
  {
    firstName: "Charity",
    lastName: "Mathews",
    email: "fubigonero@mailinator.com",
    age: "63",
  },
];

const csvReport = {
  data: data,
  headers: headers,
  filename: "Clue_Mediator_Report.csv",
};

class VisaReport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeMainTab: "",
      activeTab: "",
      extend: false,
      dateFrom: "",
      dateTo: "",
      processingPerson: [],
      loadReports: false,
      pageNumber: 1,
      pageSize: 10,
      processingIds: "",
      visaTypeId: null,
      caseStatusId: [],
      ausVisaTypeId: null,
      ausSubVisaId: null,
      destination: null,
      liaId: null,
      allClients: null,
    };
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    this.props.onGetTeamMember();
    this.props.onGetVisaType();
    this.props.onGetVisaStatus();
    this.props.onGetVisaTypeByCountry("28");
    this.props.onGetDestination();
    this.props.onGetAllClient(selectedBranchId);
  }

  myChangeHandler = (text) => {
    this.setState({ username: text });
  };

  onChange = (value) => {
    console.log(`selected ${value}`);
  };

  onBlur = () => {
    console.log("blur");
  };

  onFocus = () => {
    console.log("focus");
  };

  onSearch = (val) => {
    console.log("search:", val);
  };

  onChangeMainTab = (value) => {
    this.setState({ activeMainTab: value });
  };

  onChangeTab = (value) => {
    this.setState({ activeTab: value });
  };

  handleChangeVisaType = (selectedOption) => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };

  handleChangeVisaStatus = (selectedOption) => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };

  handleCheck = () => {
    this.setState({ checked: !this.state.checked });
  };

  onChangeDateFrom = (date, dateString) => {
    this.setState({ dateFrom: date });
  };

  onChangeDateTo = (date, dateString) => {
    this.setState({ dateTo: date });
  };

  changePageSize = (value) => {
    this.setState({ pageSize: value });
  };

  onVisaReport = () => {
    let selectedBranchId = localStorage.getItem("selectedBranchId");
    var processingIds = "";
    var statuses = "";
    this.setState({ loadReports: true });
    if (this.state.processingPerson.length > 0) {
      for (var i = 0; i < this.state.processingPerson.length; i++) {
        if (i === 0) {
          processingIds = this.state.processingPerson[i];
        } else {
          processingIds += "," + this.state.processingPerson[i];
        }
      }
    }
    if (this.state.caseStatusId.length > 0) {
      for (var i = 0; i < this.state.caseStatusId.length; i++) {
        if (i === 0) {
          statuses = this.state.caseStatusId[i];
        } else {
          statuses += "," + this.state.caseStatusId[i];
        }
      }
    }
    this.setState({ processingIds: processingIds });
    let data = {
      branchId: selectedBranchId,
      processingPerson: processingIds || "00000000-0000-0000-0000-000000000000",
      dateFrom: this.state.dateFrom || "1900-01-01T00:00:00.000Z",
      dateTo: this.state.dateTo || "1900-01-01T00:00:00.000Z",
      pageSize: this.state.pageSize,
      pageNumber: this.state.pageNumber,
      visaTypeId: this.state.visaTypeId || 0,
      caseStatusId: statuses,
      subId: this.ausSubVisaId || 0,
      destination: this.state.destination || "",
      liaId: this.state.liaId || "00000000-0000-0000-0000-000000000000",
    };
    this.props.onVisaReport(data).then(() => {
      this.setState({ loadReports: false });
    });
  };

  render() {
    const {
      selectedOption,
      dateFrom,
      dateTo,
      visaTypeId,
      caseStatusId,
      ausVisaTypeId,
      ausSubVisaId,
      destination,
      liaId,
      processingPerson,
      allClients,
      pageSize,
    } = this.state;
    const {
      teamMembers,
      visaTypeData,
      visaStatusData,
      countryVisaTypeData,
      visaCategoryData,
      destinationRes,
      allClientData,
      visaReportRes,
    } = this.props;
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            <PotentialHeaderTabs
              data={headOption}
              activeTab="Potential Clients"
            />

            <div className="report-container">
              <div>
                <div>
                  <div className="pciq-top-div">
                    <span
                      className="pc-top-div-text"
                      style={{ color: "#0A3C5D" }}
                    >
                      VISA REPORT FILTERS
                    </span>
                  </div>

                  <div className="ca-gray-cont" style={{ border: 0 }}>
                    <div style={{ display: "flex", marginLeft: 20 }}>
                      <div style={{ display: "flex", width: "47%" }}>
                        <div
                          style={{
                            width: "100%",
                          }}
                        >
                          <DatePicker
                            onChange={this.onChangeDateFrom}
                            value={dateFrom}
                            format={dateFormat}
                          />
                        </div>
                      </div>
                      <div
                        style={{
                          marginLeft: 20,
                          display: "flex",
                          width: "47%",
                        }}
                      >
                        <div
                          style={{
                            width: "100%",
                          }}
                        >
                          <DatePicker
                            onChange={this.onChangeDateTo}
                            value={dateTo}
                            format={dateFormat}
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      style={{ display: "flex", marginLeft: 20, marginTop: 10 }}
                    >
                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 0, width: "31.65%" }}
                      >
                        <div>
                          <Select
                            placeholder="Select Visa Type"
                            value={visaTypeId}
                            onChange={(value) => {
                              this.setState({ visaTypeId: value });
                            }}
                          >
                            {visaTypeData &&
                              visaTypeData.items.map((visa, index) => (
                                <Option value={visa.id}>
                                  {visa.visaTypeName}
                                </Option>
                              ))}
                          </Select>
                        </div>
                      </div>

                      <div
                        className="pc-select-width multi-select-option"
                        style={{ marginLeft: 15, width: "31.65%" }}
                      >
                        <div>
                          <Select
                            placeholder="Select Visa Status"
                            mode="multiple"
                            value={caseStatusId}
                            onChange={(value) =>
                              this.setState({ caseStatusId: value })
                            }
                          >
                            {visaStatusData &&
                              visaStatusData.items.map((visaStatus, index) => (
                                <Option value={visaStatus.id}>
                                  {visaStatus.name}
                                </Option>
                              ))}
                          </Select>
                        </div>
                      </div>

                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 15, width: "31.65%" }}
                      >
                        <div>
                          <Select
                            placeholder="Australian Visa Type"
                            value={ausVisaTypeId}
                            onChange={(value) => {
                              this.setState({ ausVisaTypeId: value });
                              this.props.onGetVisaCategory(value);
                            }}
                          >
                            {countryVisaTypeData &&
                              countryVisaTypeData.items.map(
                                (countryVisa, index) => (
                                  <Option value={countryVisa.id}>
                                    {countryVisa.visaTypeName}
                                  </Option>
                                )
                              )}
                          </Select>
                        </div>
                      </div>
                    </div>

                    <div
                      style={{ display: "flex", marginLeft: 20, marginTop: 10 }}
                    >
                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 0, width: "31.65%" }}
                      >
                        <div>
                          <Select
                            placeholder="Australian Visa Sub Types"
                            value={selectedOption}
                            disabled={
                              visaCategoryData &&
                              visaCategoryData.items.length > 0
                                ? false
                                : true
                            }
                            onChange={(value) =>
                              this.setState({ ausSubVisaId: value })
                            }
                          >
                            {visaCategoryData &&
                              visaCategoryData.items.map((category, index) => (
                                <Option value={category.id}>
                                  {category.name}
                                </Option>
                              ))}
                          </Select>
                        </div>
                      </div>

                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 15, width: "31.65%" }}
                      >
                        <div>
                          <Select
                            placeholder="Destination"
                            value={destination}
                            onChange={(value) =>
                              this.setState({ destination: value })
                            }
                          >
                            {destinationRes &&
                              destinationRes.items.map((destination, index) => (
                                <Option value={destination.id}>
                                  {destination.name}
                                </Option>
                              ))}
                          </Select>
                        </div>
                      </div>

                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 15, width: "31.65%" }}
                      >
                        <div>
                          <Select
                            placeholder="LIA"
                            value={liaId}
                            onChange={(value) =>
                              this.setState({ liaId: value })
                            }
                          >
                            {teamMembers &&
                              teamMembers.users.map((user, userInd) => (
                                <Option value={user.id}>{user.fullName}</Option>
                              ))}
                          </Select>
                        </div>
                      </div>
                    </div>

                    <div
                      style={{ display: "flex", marginLeft: 20, marginTop: 10 }}
                    >
                      <div
                        className="pc-select-width"
                        style={{ marginLeft: 0, width: "31.65%" }}
                      >
                        <div>
                          <Select
                            placeholder="All Clients"
                            value={allClients}
                            onChange={(value) =>
                              this.setState({ allClients: value })
                            }
                          >
                            <Option value="0">All Clients</Option>
                            <Option value="1">Active</Option>
                            <Option value="2">Inactive</Option>
                          </Select>
                        </div>
                      </div>

                      <div
                        className="pc-select-width multi-select-option"
                        style={{ marginLeft: 15, width: "31.65%" }}
                      >
                        <div>
                          <Select
                            placeholder="Processing Person"
                            mode="multiple"
                            value={processingPerson}
                            onChange={(value) =>
                              this.setState({ processingPerson: value })
                            }
                          >
                            {teamMembers &&
                              teamMembers.users.map((user, userInd) => (
                                <Option value={user.id}>{user.fullName}</Option>
                              ))}
                          </Select>
                        </div>
                      </div>

                      <div style={{ display: "flex", marginLeft: 15 }}>
                        {visaReportRes && visaReportRes.items.length > 0 && (
                          <CSVLink
                            data={visaReportRes && visaReportRes.items}
                            headers={headers}
                          >
                            <div
                              className="pc-btn-cont"
                              style={{
                                paddingTop: 5,
                                paddingBottom: 5,
                                borderRadius: 3,
                                cursor: "pointer",
                                marginRight: 15,
                              }}
                            >
                              <span className="pc-btn-text">EXPORT</span>
                            </div>
                          </CSVLink>
                        )}
                        <div
                          className="pc-btn-cont"
                          style={{
                            paddingTop: 5,
                            paddingBottom: 5,
                            borderRadius: 3,
                            cursor: "pointer",
                          }}
                          onClick={this.onVisaReport}
                        >
                          <span className="pc-btn-text">SHOW</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* ===== Design from potential client's client source report Screen ===== */}

            <div
              className="report-container"
              style={{ paddingTop: 30, paddingBottom: 30 }}
            >
              <div className="pciq-top-div">
                <span className="pc-top-div-text" style={{ color: "#0A3C5D" }}>
                  VISA DETAIL
                </span>
              </div>

              <div className="pc-text-field-row" style={{ marginBottom: 20 }}>
                <div>
                  <span className="cv-normal-text">Show</span>
                </div>
                <div style={{ width: 100, marginLeft: 15 }}>
                  <Select value={pageSize} onChange={this.changePageSize}>
                    <Option value="10">10</Option>
                    <Option value="25">25</Option>
                    <Option value="50">50</Option>
                    <Option value="100">100</Option>
                    <Option value="500">500</Option>
                  </Select>
                </div>

                <div>
                  <span className="cv-normal-text">entries</span>
                </div>
              </div>

              {visaReportRes && visaReportRes.list.length > 0 && (
                <VisaReportChild
                  fileNotesReportRes={visaReportRes && visaReportRes.list}
                  displayText={"Visa Report"}
                  reportsCount={visaReportRes && visaReportRes.count}
                  onFileNotesReport={this.onVisaReport}
                  requestData={this.state}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VisaReport;
