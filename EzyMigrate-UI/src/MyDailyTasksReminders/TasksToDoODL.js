import React from "react";
import Select from "react-select";
import "./MyDailyTasksReminderStyles.css";
import HeaderBar from "../Components/Header/HeaderBar";

import { Link } from "react-router-dom";
import Sidebar from "../Components/SideBar";
import { Images } from "../Themes";
import PotentialHeaderTabs from "../Components/Header/PotentialHeaderTabs";
import Modal from "react-awesome-modal";
import Calendar from "react-calendar";

const headOption = [
  { tabName: "TASKS TO DO", linkName: "/tasks-and-reminders/tasks-to-do" },
  {
    tabName: "COMPLETED TASKS",
    linkName: "/tasks-and-reminders/completed-tasks"
  },
  {
    tabName: "COMMISSION REMINDERS",
    linkName: "/tasks-and-reminders/commission-reminders"
  }
];

class TasksToDo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      visibleFileNote: false,
      addTaskVisible: false,
      addReminderVisible: false,
      updPopupVisible: false
    };
  }

  onChange = date => this.setState({ date });

  openFNModal = () => {
    this.setState({
      visibleFileNote: true
    });
  };

  closeFNModal = () => {
    this.setState({
      visibleFileNote: false
    });
  };

  openAddTaskModal = () => {
    this.setState({
      addTaskVisible: true
    });
  };

  closeAddTaskModal = () => {
    this.setState({
      addTaskVisible: false
    });
  };

  openAddReminderModal = () => {
    this.setState({
      addReminderVisible: true
    });
  };

  closeAddReminderModal = () => {
    this.setState({
      addReminderVisible: false
    });
  };

  render() {
    return (
      <div>
        <div style={{ display: "flex" }}>
          <div className="page-container">
            <PotentialHeaderTabs data={headOption} activeTab="TASKS TO DO" />
            <div style={{ display: "flex" }}>
              <div style={{ width: "65%", marginTop: 20 }}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "flex-end",
                    marginRight: 20
                  }}
                >
                  <div className="profile-print-box">
                    <img
                      src={Images.printWhite}
                      className="profile-print-icon"
                    />
                  </div>
                </div>

                <div className="mdtr-to-do-tasks-white-cont">
                  <div>
                    <span className="cv-bold-text" style={{ marginLeft: 10 }}>
                      TODAY's TASKS
                    </span>
                  </div>

                  <div className="cv-gray-cont">
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        paddingBottom: 20
                      }}
                    >
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <div>
                          <input
                            type="checkbox"
                            className="sus-checkbox"
                            onChange={this.handleCheck}
                            defaultChecked={this.state.checked}
                          />
                        </div>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <span className="cv-normal-text">Ghbbjnm</span>
                        </div>
                        <div className="mdtr-org-cont">
                          <span className="mdtr-org-text-color">
                            Julie Smith
                          </span>
                        </div>
                      </div>
                      <div style={{ display: "flex" }}>
                        <div className="mdtr-user-cont">
                          <img
                            src={Images.userCircleGray}
                            style={{ width: 15, height: 15 }}
                          />
                          <img
                            src={Images.nextGray}
                            style={{
                              marginLeft: 5,
                              transform: `rotate(90deg)`,
                              width: 12,
                              height: 12
                            }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.calendarBlue}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.fileNotes}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.deleteIcon}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        paddingBottom: 20
                      }}
                    >
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <div>
                          <input
                            type="checkbox"
                            className="sus-checkbox"
                            onChange={this.handleCheck}
                            defaultChecked={this.state.checked}
                          />
                        </div>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <span className="cv-normal-text">Ghbbjnm</span>
                        </div>
                        <div className="mdtr-org-cont">
                          <span className="mdtr-org-text-color">
                            Julie Smith
                          </span>
                        </div>
                      </div>
                      <div style={{ display: "flex" }}>
                        <div className="mdtr-user-cont">
                          <img
                            src={Images.userCircleGray}
                            style={{ width: 15, height: 15 }}
                          />
                          <img
                            src={Images.nextGray}
                            style={{
                              marginLeft: 5,
                              transform: `rotate(90deg)`,
                              width: 12,
                              height: 12
                            }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.calendarBlue}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.fileNotes}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.deleteIcon}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="mdtr-to-do-tasks-white-cont">
                  <div>
                    <span className="cv-bold-text">PENDING TASKS</span>
                  </div>

                  <div className="cv-gray-cont">
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        paddingBottom: 20
                      }}
                    >
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <div>
                          <input
                            type="checkbox"
                            className="sus-checkbox"
                            onChange={this.handleCheck}
                            defaultChecked={this.state.checked}
                          />
                        </div>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <span className="cv-normal-text">Ghbbjnm</span>
                        </div>
                        <div className="mdtr-org-cont">
                          <span className="mdtr-org-text-color">
                            Julie Smith
                          </span>
                        </div>
                      </div>
                      <div style={{ display: "flex" }}>
                        <div className="mdtr-user-cont">
                          <img
                            src={Images.userCircleGray}
                            style={{ width: 15, height: 15 }}
                          />
                          <img
                            src={Images.nextGray}
                            style={{
                              marginLeft: 5,
                              transform: `rotate(90deg)`,
                              width: 12,
                              height: 12
                            }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.calendarBlue}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.fileNotes}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.deleteIcon}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                      </div>
                    </div>

                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        paddingBottom: 20
                      }}
                    >
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <div>
                          <input
                            type="checkbox"
                            className="sus-checkbox"
                            onChange={this.handleCheck}
                            defaultChecked={this.state.checked}
                          />
                        </div>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <span className="cv-normal-text">Ghbbjnm</span>
                        </div>
                        <div className="mdtr-org-cont">
                          <span className="mdtr-org-text-color">
                            Julie Smith
                          </span>
                        </div>
                      </div>
                      <div style={{ display: "flex" }}>
                        <div className="mdtr-user-cont">
                          <img
                            src={Images.userCircleGray}
                            style={{ width: 15, height: 15 }}
                          />
                          <img
                            src={Images.nextGray}
                            style={{
                              marginLeft: 5,
                              transform: `rotate(90deg)`,
                              width: 12,
                              height: 12
                            }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.calendarBlue}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.fileNotes}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                        <div
                          className="mdtr-user-cont"
                          style={{ backgroundColor: "#FFFFFF", marginLeft: 5 }}
                        >
                          <img
                            src={Images.deleteIcon}
                            style={{ width: 15, height: 15 }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="mdtr-right-cont">
                <div className="mdtr-btn-row">
                  <div
                    className="mdtr-add-tasks"
                    onClick={this.openAddTaskModal}
                    style={{ width: 115 }}
                  >
                    <img
                      src={Images.interfaceIcon}
                      style={{ width: 12, height: 12 }}
                    />
                    <span className="mdtr-btn-text">ADD TASKS</span>
                  </div>
                  <div
                    className="mdtr-add-tasks"
                    onClick={this.openAddReminderModal}
                    style={{ backgroundColor: "#FF9D36", width: 115 }}
                  >
                    <img
                      src={Images.bellIcon}
                      style={{ width: 12, height: 12 }}
                    />
                    <span className="mdtr-btn-text">ADD REMINDERS</span>
                  </div>
                </div>
                <div style={{ marginTop: 20 }}>
                  <Calendar onChange={this.onChange} value={this.state.date} />
                </div>

                <div className="mdtr-reminder-tasks-cont">
                  <div className="mdtr-reminder-tasks-header-cont">
                    <div className="mdtr-reminder-tasks-head-text-cont">
                      <img
                        src={Images.bellRed}
                        style={{ width: 15, height: 15 }}
                      />
                      <span className="cv-bold-text" style={{ marginLeft: 5 }}>
                        REMINDERS
                      </span>
                    </div>
                    <div className="mdtr-reminder-tasks-head-text-cont">
                      <div
                        className="mdtr-user-cont"
                        style={{ borderRadius: "50%", padding: 3 }}
                      >
                        <img
                          src={Images.nextGray}
                          style={{
                            width: 8,
                            height: 8,
                            transform: `rotate(90deg)`
                          }}
                        />
                      </div>
                      <span
                        className="cv-normal-text"
                        style={{ marginLeft: 5, color: "#A9ACB1" }}
                      >
                        Hide
                      </span>
                    </div>
                  </div>

                  <div>
                    <div className="mdtr-rem-row-cont">
                      <div style={{ display: "flex" }}>
                        <div className="mdtr-org-sm-box" />
                        <div style={{ marginLeft: 10 }}>
                          <div>
                            <span
                              className="cv-bold-text"
                              style={{ marginRight: 5, color: "#1281BC" }}
                            >
                              Passport Reminder
                            </span>
                          </div>
                          <div style={{ display: "flex" }}>
                            <span className="mdtr-rem-normal-text">
                              For Constance Grace Mhlanga Expires On 12/03/2020
                            </span>
                          </div>
                          <div style={{ display: "flex" }}>
                            <span
                              className="cv-normal-text"
                              style={{
                                fontSize: 10,
                                fontWeight: "600",
                                marginLeft: 0
                              }}
                            >
                              26 Jan 2017
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="mdtr-rem-row-cont">
                      <div style={{ display: "flex" }}>
                        <div className="mdtr-org-sm-box" />
                        <div style={{ marginLeft: 10 }}>
                          <div>
                            <span
                              className="cv-bold-text"
                              style={{ marginRight: 5, color: "#1281BC" }}
                            >
                              Passport Reminder
                            </span>
                          </div>
                          <div style={{ display: "flex" }}>
                            <span className="mdtr-rem-normal-text">
                              For Constance Grace Mhlanga Expires On 12/03/2020
                            </span>
                          </div>
                          <div style={{ display: "flex" }}>
                            <span
                              className="cv-normal-text"
                              style={{
                                fontSize: 10,
                                fontWeight: "600",
                                marginLeft: 0
                              }}
                            >
                              26 Jan 2017
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="mdtr-reminder-tasks-cont">
                  <div className="mdtr-reminder-tasks-header-cont">
                    <div className="mdtr-reminder-tasks-head-text-cont">
                      <span className="cv-bold-text">AGENT COMMISSION</span>
                    </div>
                    <div className="mdtr-reminder-tasks-head-text-cont">
                      <span
                        className="cv-normal-text"
                        style={{ marginLeft: 5, color: "#A9ACB1" }}
                      >
                        Hide
                      </span>
                    </div>
                  </div>

                  <div>
                    <div className="mdtr-rem-row-cont">
                      <div style={{ display: "flex" }}>
                        <div className="mdtr-org-sm-box" />
                        <div style={{ marginLeft: 10 }}>
                          <div>
                            <span
                              className="cv-bold-text"
                              style={{ marginRight: 5, color: "#1281BC" }}
                            >
                              Passport Reminder
                            </span>
                          </div>
                          <div style={{ display: "flex" }}>
                            <span className="mdtr-rem-normal-text">
                              For Constance Grace Mhlanga Expires On 12/03/2020
                            </span>
                          </div>
                          <div style={{ display: "flex" }}>
                            <span
                              className="cv-normal-text"
                              style={{
                                fontSize: 10,
                                fontWeight: "600",
                                marginLeft: 0
                              }}
                            >
                              26 Jan 2017
                            </span>
                          </div>
                        </div>
                      </div>

                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          marginLeft: 25,
                          marginRight: 30
                        }}
                      >
                        <div>
                          <span
                            className="cv-bold-text"
                            style={{ fontSize: 8 }}
                          >
                            Amount:{" "}
                          </span>
                          <span
                            className="cv-normal-text"
                            style={{ fontSize: 8, marginLeft: 0 }}
                          >
                            $200.00
                          </span>
                        </div>

                        <div>
                          <span
                            className="cv-bold-text"
                            style={{ fontSize: 8 }}
                          >
                            Client:{" "}
                          </span>
                          <span
                            className="cv-normal-text"
                            style={{ fontSize: 8, marginLeft: 0 }}
                          >
                            Adam Hyyat
                          </span>
                        </div>

                        <div>
                          <span
                            className="cv-bold-text"
                            style={{ fontSize: 8 }}
                          >
                            Agent:{" "}
                          </span>
                          <span
                            className="cv-normal-text"
                            style={{ fontSize: 8, marginLeft: 0 }}
                          >
                            Sam Bam
                          </span>
                        </div>
                      </div>

                      <div
                        style={{
                          display: "flex",
                          justifyContent: "flex-end",
                          marginTop: 10
                        }}
                      >
                        <div
                          className="mdtr-add-tasks"
                          style={{
                            backgroundColor: "#45BD57",
                            paddingLeft: 10,
                            paddingRight: 10
                          }}
                        >
                          <span className="mdtr-btn-text">PAY NOW</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* ======== Add file note design ======== */}

            <Modal
              visible={this.state.visibleFileNote}
              width="500"
              height="230"
              effect="fadeInUp"
              onClickAway={() => this.closeFNModal()}
            >
              <div style={{ padding: 20 }}>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div className="sus-modal-label" style={{ border: 0 }}>
                    <span className="sus-modal-label-text">ADD FILE NOTE</span>
                  </div>
                  <div onClick={this.closeModal}>
                    <img src={Images.grayCross} style={{ width: 15 }} />
                  </div>
                </div>
                <div
                  className="ca-gray-cont"
                  style={{ borderColor: "#BFBFBF", padding: 10, margin: 0 }}
                >
                  <p
                    class="medical-label"
                    style={{ fontSize: 11, marginLeft: 15 }}
                  >
                    Follow up on Visa
                  </p>
                  <p
                    class="medical-label"
                    style={{ fontSize: 11, marginLeft: 15 }}
                  >
                    Bandict Thomas: norjoisf3sdk
                  </p>
                  <p
                    class="medical-label"
                    style={{ fontSize: 11, marginLeft: 15 }}
                  >
                    Bandict Thomas: jkjjjkjhkjgf
                  </p>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: 15
                  }}
                >
                  <div></div>
                  <div style={{ display: "flex" }}>
                    <div
                      onClick={this.closeFNModal}
                      className="sus-modal-button"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="sus-modal-button-text">SAVE</span>
                    </div>
                    <div
                      onClick={this.closeFNModal}
                      className="sus-modal-button"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="sus-modal-button-text">CLOSE</span>
                    </div>
                  </div>
                </div>
              </div>
            </Modal>

            {/* ======== Add Daily Task design ====== */}

            <Modal
              visible={this.state.addTaskVisible}
              width="500"
              height="570"
              effect="fadeInUp"
              onClickAway={() => this.closeAddTaskModal()}
            >
              <div style={{ padding: 20 }}>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div className="sus-modal-label" style={{ border: 0 }}>
                    <span className="sus-modal-label-text">ADD TASK</span>
                  </div>
                  <div onClick={this.closeAddTaskModal}>
                    <img src={Images.grayCross} style={{ width: 15 }} />
                  </div>
                </div>
                <div>
                  <div>
                    <span className="cv-bold-text">
                      SELECT CLIENT (OPTIONAL)
                    </span>
                  </div>

                  <div className="mdtr-modal-gray-cont">
                    <div style={{ width: "100%" }}>
                      <div className="mdtr-checkbox-cont">
                        <input
                          type="checkbox"
                          className="sus-checkbox"
                          onChange={this.handleCheck}
                          defaultChecked={this.state.checked}
                        />
                        <span className="cv-normal-text">Potential Client</span>
                      </div>

                      <div style={{ width: "100%", marginLeft: 20 }}>
                        <div
                          class="emp-ae-input-width"
                          style={{ marginTop: 25 }}
                        >
                          <p style={{ width: "30%" }}>Link Client</p>
                          <div
                            class="profile-input-border"
                            style={{ width: "65%" }}
                          >
                            <input
                              className="profile-input"
                              placeholder=""
                              type="text"
                              onChange={this.myChangeHandler}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div>
                    <span className="cv-bold-text">ADD TASK</span>
                  </div>

                  <div className="mdtr-modal-gray-cont">
                    <div style={{ width: "100%" }}>
                      <div style={{ width: "100%", marginLeft: 20 }}>
                        <div
                          class="emp-ae-input-width"
                          style={{ marginTop: 25 }}
                        >
                          <p style={{ width: "30%" }}>Task Title</p>
                          <div
                            class="profile-input-border"
                            style={{ width: "65%" }}
                          >
                            <input
                              className="profile-input"
                              placeholder=""
                              type="text"
                              onChange={this.myChangeHandler}
                            />
                          </div>
                        </div>
                      </div>

                      <div style={{ width: "100%", marginLeft: 20 }}>
                        <div
                          class="emp-ae-input-width"
                          style={{ marginTop: 25, marginRight: 32 }}
                        >
                          <p style={{ width: "30%" }}>Task Description</p>
                          <div style={{ width: "65%" }}>
                            <textarea
                              value={this.state.value}
                              onChange={this.handleChange}
                              rows={4}
                              className="ca-text-area"
                            />
                          </div>
                        </div>
                      </div>

                      <div style={{ width: "100%", marginLeft: 20 }}>
                        <div
                          class="emp-ae-input-width"
                          style={{ marginTop: 25 }}
                        >
                          <p style={{ width: "30%" }}>Add Followers</p>
                          <div
                            class="profile-input-border"
                            style={{ width: "65%" }}
                          >
                            <input
                              className="profile-input"
                              placeholder=""
                              type="text"
                              onChange={this.myChangeHandler}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: 15
                  }}
                >
                  <div></div>
                  <div style={{ display: "flex" }}>
                    <div className="mdtr-add-tasks">
                      <span className="sus-modal-button-text">ADD TASK</span>
                    </div>
                  </div>
                </div>
              </div>
            </Modal>

            {/* ======== Add Reminder design ====== */}

            <Modal
              visible={this.state.addReminderVisible}
              width="500"
              height="520"
              effect="fadeInUp"
              onClickAway={() => this.closeAddReminderModal()}
            >
              <div style={{ padding: 20 }}>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div className="sus-modal-label" style={{ border: 0 }}>
                    <span className="sus-modal-label-text">ADD REMINDER</span>
                  </div>
                  <div onClick={this.closeAddTaskModal}>
                    <img src={Images.grayCross} style={{ width: 15 }} />
                  </div>
                </div>
                <div>
                  <div>
                    <span className="cv-bold-text">
                      SELECT CLIENT (OPTIONAL)
                    </span>
                  </div>

                  <div className="mdtr-modal-gray-cont">
                    <div style={{ width: "100%" }}>
                      <div className="mdtr-checkbox-cont">
                        <input
                          type="checkbox"
                          className="sus-checkbox"
                          onChange={this.handleCheck}
                          defaultChecked={this.state.checked}
                        />
                        <span className="cv-normal-text">Potential Client</span>
                      </div>

                      <div style={{ width: "100%", marginLeft: 20 }}>
                        <div
                          class="emp-ae-input-width"
                          style={{ marginTop: 25 }}
                        >
                          <p style={{ width: "30%" }}>Link Client</p>
                          <div
                            class="profile-input-border"
                            style={{ width: "65%" }}
                          >
                            <input
                              className="profile-input"
                              placeholder=""
                              type="text"
                              onChange={this.myChangeHandler}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div>
                    <span className="cv-bold-text">ADD REMINDER</span>
                  </div>

                  <div className="mdtr-modal-gray-cont">
                    <div style={{ width: "100%" }}>
                      <div style={{ width: "100%", marginLeft: 20 }}>
                        <div
                          class="emp-ae-input-width"
                          style={{ marginTop: 25 }}
                        >
                          <p style={{ width: "30%" }}>Reminder Title</p>
                          <div
                            class="profile-input-border"
                            style={{ width: "65%" }}
                          >
                            <input
                              className="profile-input"
                              placeholder=""
                              type="text"
                              onChange={this.myChangeHandler}
                            />
                          </div>
                        </div>
                      </div>

                      <div style={{ width: "100%", marginLeft: 20 }}>
                        <div
                          class="emp-ae-input-width"
                          style={{ marginTop: 25, marginRight: 32 }}
                        >
                          <p style={{ width: "30%" }}>Reminder Description</p>
                          <div style={{ width: "65%" }}>
                            <textarea
                              value={this.state.value}
                              onChange={this.handleChange}
                              rows={4}
                              className="ca-text-area"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: 15
                  }}
                >
                  <div></div>
                  <div style={{ display: "flex" }}>
                    <div className="mdtr-add-tasks">
                      <span className="sus-modal-button-text">
                        ADD REMINDER
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </Modal>

            {/* ======== Edit popup ======== */}

            <Modal
              visible={this.state.updPopupVisible}
              width="500"
              height="200"
              effect="fadeInUp"
              onClickAway={() => this.closeFNModal()}
            >
              <div style={{ padding: 20 }}>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div className="sus-modal-label" style={{ border: 0 }}>
                    <span className="sus-modal-label-text">UPDATE</span>
                  </div>
                  <div onClick={this.closeModal}>
                    <img src={Images.grayCross} style={{ width: 15 }} />
                  </div>
                </div>
                <div
                  className="ca-gray-cont"
                  style={{ borderColor: "#BFBFBF", padding: 20, margin: 0 }}
                >
                  <div style={{ width: "100%", marginLeft: 20 }}>
                    <div class="emp-ae-input-width">
                      <p style={{ width: "30%" }}>Title</p>
                      <div
                        class="profile-input-border"
                        style={{ width: "65%" }}
                      >
                        <input
                          className="profile-input"
                          placeholder=""
                          type="text"
                          onChange={this.myChangeHandler}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: 15
                  }}
                >
                  <div></div>
                  <div style={{ display: "flex" }}>
                    <div
                      onClick={this.closeFNModal}
                      className="sus-modal-button"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="sus-modal-button-text">SAVE</span>
                    </div>
                    <div
                      onClick={this.closeFNModal}
                      className="sus-modal-button"
                      style={{ marginLeft: 10 }}
                    >
                      <span className="sus-modal-button-text">CLOSE</span>
                    </div>
                  </div>
                </div>
              </div>
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}

export default TasksToDo;
