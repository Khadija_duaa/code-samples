// import React from "react";
// import Select from "react-select";
// import "./MyDailyTasksReminderStyles.css";
// import HeaderBar from "../Components/Header/HeaderBar";

// import { Link } from "react-router-dom";
// import Sidebar from "../Components/SideBar";
// import { Images } from "../Themes";
// import PotentialHeaderTabs from "../Components/Header/PotentialHeaderTabs";
// import Modal from "react-awesome-modal";
// import Calendar from "react-calendar";

// const headOption = [
//   { tabName: "TASKS TO DO", linkName: "/tasks-and-reminders/tasks-to-do" },
//   { tabName: "COMPLETED TASKS", linkName: "/" },
//   { tabName: "COMMISSION REMINDERS", linkName: "/" }
// ];

// class CommissionReminder extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       date: new Date()
//     };
//   }

//   onChange = date => this.setState({ date });

//   render() {
//     return (
//       <div style={{ width: "100vw" }}>
//         <div>
//           <HeaderBar />
//         </div>
//         <div style={{ display: "flex" }}>
//           <div>
//             <Sidebar activeScreen="taskAndReminders" />
//           </div>
//           <div className="page-container">
//             <PotentialHeaderTabs
//               data={headOption}
//               activeTab="COMMISSION REMINDERS"
//             />
//             <div style={{ display: "flex" }}>
//               <div style={{ width: "65%", marginTop: 20 }}>
//                 <div className="mdtr-to-do-tasks-white-cont">
//                   <div>
//                     <span className="cv-bold-text" style={{ marginLeft: 10 }}>
//                       COMPLETED TASKS
//                     </span>
//                   </div>

//                   <div className="cv-gray-cont" style={{ padding: 0 }}>
//                     <div className="mdtr-com-rem-cont">
//                       <div style={{ display: "flex" }}>
//                         <div className="mdtr-org-sm-box" />
//                         <div style={{ marginLeft: 10 }}>
//                           <div>
//                             <span
//                               className="cv-bold-text"
//                               style={{ marginRight: 5, color: "#1281BC" }}
//                             >
//                               School Commission Reminder
//                             </span>
//                           </div>
//                           <div style={{ display: "flex" }}>
//                             <span className="mdtr-rem-normal-text">
//                               18 Mar 2018
//                             </span>
//                           </div>
//                           <div style={{ display: "flex" }}>
//                             <span className="mdtr-rem-normal-text">
//                               1st Payment
//                             </span>
//                           </div>

//                           <div
//                             style={{
//                               display: "flex",
//                               justifyContent: "space-between"
//                             }}
//                           >
//                             <div>
//                               <span
//                                 className="cv-bold-text"
//                                 style={{ fontSize: 8 }}
//                               >
//                                 Amount:{" "}
//                               </span>
//                               <span
//                                 className="cv-normal-text"
//                                 style={{ fontSize: 8, marginLeft: 0 }}
//                               >
//                                 $200.00
//                               </span>
//                             </div>

//                             <div style={{ marginLeft: 20 }}>
//                               <span
//                                 className="cv-bold-text"
//                                 style={{ fontSize: 8 }}
//                               >
//                                 Client:{" "}
//                               </span>
//                               <span
//                                 className="cv-normal-text"
//                                 style={{ fontSize: 8, marginLeft: 0 }}
//                               >
//                                 Adam Hyyat
//                               </span>
//                             </div>

//                             <div style={{ marginLeft: 20 }}>
//                               <span
//                                 className="cv-bold-text"
//                                 style={{ fontSize: 8 }}
//                               >
//                                 School:{" "}
//                               </span>
//                               <span
//                                 className="cv-normal-text"
//                                 style={{ fontSize: 8, marginLeft: 0 }}
//                               >
//                                 aut
//                               </span>
//                             </div>
//                           </div>
//                         </div>
//                       </div>
//                     </div>

//                     <div className="mdtr-com-rem-cont">
//                       <div style={{ display: "flex" }}>
//                         <div className="mdtr-org-sm-box" />
//                         <div style={{ marginLeft: 10 }}>
//                           <div>
//                             <span
//                               className="cv-bold-text"
//                               style={{ marginRight: 5, color: "#1281BC" }}
//                             >
//                               School Commission Reminder
//                             </span>
//                           </div>
//                           <div style={{ display: "flex" }}>
//                             <span className="mdtr-rem-normal-text">
//                               18 Mar 2018
//                             </span>
//                           </div>
//                           <div style={{ display: "flex" }}>
//                             <span className="mdtr-rem-normal-text">
//                               1st Payment
//                             </span>
//                           </div>

//                           <div
//                             style={{
//                               display: "flex",
//                               justifyContent: "space-between"
//                             }}
//                           >
//                             <div>
//                               <span
//                                 className="cv-bold-text"
//                                 style={{ fontSize: 8 }}
//                               >
//                                 Amount:{" "}
//                               </span>
//                               <span
//                                 className="cv-normal-text"
//                                 style={{ fontSize: 8, marginLeft: 0 }}
//                               >
//                                 $200.00
//                               </span>
//                             </div>

//                             <div style={{ marginLeft: 20 }}>
//                               <span
//                                 className="cv-bold-text"
//                                 style={{ fontSize: 8 }}
//                               >
//                                 Client:{" "}
//                               </span>
//                               <span
//                                 className="cv-normal-text"
//                                 style={{ fontSize: 8, marginLeft: 0 }}
//                               >
//                                 Adam Hyyat
//                               </span>
//                             </div>

//                             <div style={{ marginLeft: 20 }}>
//                               <span
//                                 className="cv-bold-text"
//                                 style={{ fontSize: 8 }}
//                               >
//                                 School:{" "}
//                               </span>
//                               <span
//                                 className="cv-normal-text"
//                                 style={{ fontSize: 8, marginLeft: 0 }}
//                               >
//                                 aut
//                               </span>
//                             </div>
//                           </div>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>

//               <div className="mdtr-right-cont" style={{ marginTop: 40 }}>
//                 <div className="mdtr-btn-row">
//                   <div className="mdtr-add-tasks" style={{ width: 115 }}>
//                     <img
//                       src={Images.interfaceIcon}
//                       style={{ width: 12, height: 12 }}
//                     />
//                     <span className="mdtr-btn-text">ADD TASKS</span>
//                   </div>
//                   <div
//                     className="mdtr-add-tasks"
//                     style={{ backgroundColor: "#FF9D36", width: 115 }}
//                   >
//                     <img
//                       src={Images.bellIcon}
//                       style={{ width: 12, height: 12 }}
//                     />
//                     <span className="mdtr-btn-text">ADD REMINDERS</span>
//                   </div>
//                 </div>
//                 <div style={{ marginTop: 20 }}>
//                   <Calendar onChange={this.onChange} value={this.state.date} />
//                 </div>

//                 <div className="mdtr-reminder-tasks-cont">
//                   <div className="mdtr-reminder-tasks-header-cont">
//                     <div className="mdtr-reminder-tasks-head-text-cont">
//                       <img
//                         src={Images.bellRed}
//                         style={{ width: 15, height: 15 }}
//                       />
//                       <span className="cv-bold-text" style={{ marginLeft: 5 }}>
//                         REMINDERS
//                       </span>
//                     </div>
//                     <div className="mdtr-reminder-tasks-head-text-cont">
//                       <div
//                         className="mdtr-user-cont"
//                         style={{ borderRadius: "50%", padding: 3 }}
//                       >
//                         <img
//                           src={Images.nextGray}
//                           style={{
//                             width: 8,
//                             height: 8,
//                             transform: `rotate(90deg)`
//                           }}
//                         />
//                       </div>
//                       <span
//                         className="cv-normal-text"
//                         style={{ marginLeft: 5, color: "#A9ACB1" }}
//                       >
//                         Hide
//                       </span>
//                     </div>
//                   </div>

//                   <div>
//                     <div className="mdtr-rem-row-cont">
//                       <div style={{ display: "flex" }}>
//                         <div className="mdtr-org-sm-box" />
//                         <div style={{ marginLeft: 10 }}>
//                           <div>
//                             <span
//                               className="cv-bold-text"
//                               style={{ marginRight: 5, color: "#1281BC" }}
//                             >
//                               Passport Reminder
//                             </span>
//                           </div>
//                           <div style={{ display: "flex" }}>
//                             <span className="mdtr-rem-normal-text">
//                               For Constance Grace Mhlanga Expires On 12/03/2020
//                             </span>
//                           </div>
//                           <div style={{ display: "flex" }}>
//                             <span
//                               className="cv-normal-text"
//                               style={{
//                                 fontSize: 10,
//                                 fontWeight: "600",
//                                 marginLeft: 0
//                               }}
//                             >
//                               26 Jan 2017
//                             </span>
//                           </div>
//                         </div>
//                       </div>
//                     </div>

//                     <div className="mdtr-rem-row-cont">
//                       <div style={{ display: "flex" }}>
//                         <div className="mdtr-org-sm-box" />
//                         <div style={{ marginLeft: 10 }}>
//                           <div>
//                             <span
//                               className="cv-bold-text"
//                               style={{ marginRight: 5, color: "#1281BC" }}
//                             >
//                               Passport Reminder
//                             </span>
//                           </div>
//                           <div style={{ display: "flex" }}>
//                             <span className="mdtr-rem-normal-text">
//                               For Constance Grace Mhlanga Expires On 12/03/2020
//                             </span>
//                           </div>
//                           <div style={{ display: "flex" }}>
//                             <span
//                               className="cv-normal-text"
//                               style={{
//                                 fontSize: 10,
//                                 fontWeight: "600",
//                                 marginLeft: 0
//                               }}
//                             >
//                               26 Jan 2017
//                             </span>
//                           </div>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                 </div>

//                 <div className="mdtr-reminder-tasks-cont">
//                   <div className="mdtr-reminder-tasks-header-cont">
//                     <div className="mdtr-reminder-tasks-head-text-cont">
//                       <span className="cv-bold-text">AGENT COMMISSION</span>
//                     </div>
//                     <div className="mdtr-reminder-tasks-head-text-cont">
//                       <span
//                         className="cv-normal-text"
//                         style={{ marginLeft: 5, color: "#A9ACB1" }}
//                       >
//                         Hide
//                       </span>
//                     </div>
//                   </div>

//                   <div>
//                     <div className="mdtr-rem-row-cont">
//                       <div style={{ display: "flex" }}>
//                         <div className="mdtr-org-sm-box" />
//                         <div style={{ marginLeft: 10 }}>
//                           <div>
//                             <span
//                               className="cv-bold-text"
//                               style={{ marginRight: 5, color: "#1281BC" }}
//                             >
//                               Passport Reminder
//                             </span>
//                           </div>
//                           <div style={{ display: "flex" }}>
//                             <span className="mdtr-rem-normal-text">
//                               For Constance Grace Mhlanga Expires On 12/03/2020
//                             </span>
//                           </div>
//                           <div style={{ display: "flex" }}>
//                             <span
//                               className="cv-normal-text"
//                               style={{
//                                 fontSize: 10,
//                                 fontWeight: "600",
//                                 marginLeft: 0
//                               }}
//                             >
//                               26 Jan 2017
//                             </span>
//                           </div>
//                         </div>
//                       </div>

//                       <div
//                         style={{
//                           display: "flex",
//                           justifyContent: "space-between",
//                           marginLeft: 25,
//                           marginRight: 30
//                         }}
//                       >
//                         <div>
//                           <span
//                             className="cv-bold-text"
//                             style={{ fontSize: 8 }}
//                           >
//                             Amount:{" "}
//                           </span>
//                           <span
//                             className="cv-normal-text"
//                             style={{ fontSize: 8, marginLeft: 0 }}
//                           >
//                             $200.00
//                           </span>
//                         </div>

//                         <div>
//                           <span
//                             className="cv-bold-text"
//                             style={{ fontSize: 8 }}
//                           >
//                             Client:{" "}
//                           </span>
//                           <span
//                             className="cv-normal-text"
//                             style={{ fontSize: 8, marginLeft: 0 }}
//                           >
//                             Adam Hyyat
//                           </span>
//                         </div>

//                         <div>
//                           <span
//                             className="cv-bold-text"
//                             style={{ fontSize: 8 }}
//                           >
//                             Agent:{" "}
//                           </span>
//                           <span
//                             className="cv-normal-text"
//                             style={{ fontSize: 8, marginLeft: 0 }}
//                           >
//                             Sam Bam
//                           </span>
//                         </div>
//                       </div>

//                       <div
//                         style={{
//                           display: "flex",
//                           justifyContent: "flex-end",
//                           marginTop: 10
//                         }}
//                       >
//                         <div
//                           className="mdtr-add-tasks"
//                           style={{
//                             backgroundColor: "#45BD57",
//                             paddingLeft: 10,
//                             paddingRight: 10
//                           }}
//                         >
//                           <span className="mdtr-btn-text">PAY NOW</span>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default CommissionReminder;
