import React, { useState, useRef } from "react";
import { Form, Input, Button, DatePicker, Spin, message } from "antd";
import "./comment.css";
import moment from "moment";

let clientprofileid = JSON.parse(
  window.localStorage.getItem("clientprofileid")
);

const UpdateDateModal = ({
  onGetDailyTasks,
  dailyTaskRes,

  onUpdetaTasks,

  updateTitle,
  updateData,
  reloadFlag,
  setReload,
  handleCancel,
}) => {
  const [loading, setLoading] = useState(false);
  const formRef = useRef();
  const onFinish = (values) => {
    setLoading(true);
    console.log("Received values of form:", values);

    const updatedate = {
      id: updateTitle,
      branchId: updateData.branchId,
      taskDate: values.reschedule_date.format(),
      taskTitle: updateData.taskTitle,
      taskDescription: updateData.taskDescription,
      subjectId: updateData.subjectId,
      isPontential: updateData.isPontential,
      isCompleted: updateData.isCompleted,
      completedOn: updateData.completedOn,
      modifiedBy: updateData.modifiedBy,
    };
    onUpdetaTasks(updatedate)
      .then(() => {
        setLoading(false);
        if (formRef && formRef.current) formRef.current.resetFields();
        message.success("Successfully Updated!");
        handleCancel();
        setReload(!reloadFlag);
        let userName = localStorage.getItem("userName");
        var profileData = JSON.parse(localStorage.getItem("profileData"));
        let myData = {
          clientName: profileData.fullName,
          logMessage:
            "\nlOG FOR " +
            profileData.fullName +
            " Task rescheduled by " +
            userName,
          date: moment(new Date()).format("DD/MM/YYYY"),
          logType: "Client Tasks",
          invoiceId: "0",
        };
        // activityData(myData);
        // window.location.reload();
      })
      .catch((error) => {
        setLoading(false);
        if (formRef && formRef.current) formRef.current.resetFields();
        message.error("Error in Update!");
      });
  };

  return (
    <Spin size="large" spinning={loading}>
      <Form
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        ref={formRef}
      >
        <div className="mdtr-modal-gray-cont">
          <div className="modal-parts">
            <Form.Item
              className="add-reminder-form"
              className="form-parts"
              label="Reschedule Date"
              name="reschedule_date"
              required={false}
              rules={[{ required: false }]}
            >
              <DatePicker format={"DD/MM/YYYY"} />
            </Form.Item>
          </div>
        </div>
        <Form.Item>
          <Button type="primary" className="task-blue" htmlType="submit">
            SAVE
          </Button>
        </Form.Item>
      </Form>
    </Spin>
  );
};

export default UpdateDateModal;
