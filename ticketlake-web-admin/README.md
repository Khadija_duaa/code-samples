# Ticketlake Admin Panel

Ticketlake is an event management website. User can buy tickets online from the website.
Ticketlake website is developed using React.js and Redux. Also Ticketlake admin panel is developed using
React.js and Redux

## Setup development Server
 

```bash
git clone http://58.27.238.210:8060/scm/tic/ticketlake-web-admin.git
cd ticketlake-web-admin
git checkout develop
npm install
```

When installation are done, use the following command to start the website server

## Start the server
``` 
 npm start
 ```

