import React, {Component} from 'react';
import { Row, Col, Input, Button, FormText } from 'reactstrap'
import { connect } from 'react-redux'
import { saveTicketClasses } from '../../redux/actions/events'
import Loader from '../loader'
import swal from 'sweetalert';
import _ from 'lodash'
import {NotificationManager} from "react-notifications";
class TicketClasses extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ticketClasses: [{
                ticketClassName: "BUSINESS", ticketClassColor: '#008080',
                ticketClassType: 'REGULAR'
            },{
                ticketClassName: "STANDARD",  ticketClassColor: '#00FF00', ticketClassType: 'REGULAR'},
            {
                ticketClassName: "VIP",  ticketClassColor: '#99ccff', ticketClassType: 'REGULAR'
            }],
            ticketNameError: false,
            errorMessage: 'Class Name is required field',
            parentEventUUID: "",
            ticketColorError: false,
            eventType: '',
            errors: {ticketClassName: [{ticketClassName: false, ticketClassNameMsg: ""}],
            ticketClassColor: [{ticketClassColor: false, ticketClassColorMsg: ""}]
            }
        }
    }
    componentWillReceiveProps (nextProps) {
        if (nextProps.StandardEvent.ticketClassesSaved !== this.props.StandardEvent.ticketClassesSaved) {
            this.props.showNext(true)
            NotificationManager.success("Ticket classes saved", "Success", 3000)
        }
        if(this.props.StandardEvent.error !== nextProps.StandardEvent.error) {
			if(Array.isArray(nextProps.StandardEvent.errors)) {
				nextProps.StandardEvent.errors.map(error => {
					NotificationManager.error(error, "Error")
				})
			} else {
				NotificationManager.error(nextProps.StandardEvent.errors, "Error")
			}
		}
    }
    componentDidMount(){
        if (this.props.eventType) this.setState({eventType: this.props.eventType})
        if(this.props.eventSlotDetail) {
            this.setState({
                ticketClasses: this.props.eventSlotDetail.parentEventInfo.ticketClassesConfig,
                parentEventUUID: this.props.eventSlotDetail.parentEventInfo.parentEvent,
                eventType: this.props.eventSlotDetail.eventType
            })
        }
        if(this.props.StandardEvent.eventSaved) {
            this.setState({parentEventUUID: this.props.StandardEvent.eventUUID})
        }
    }
    handleTicketChange = idx => evt => {
        const newTicketClasses = this.state.ticketClasses.map((ticket, sidx) => {
            if (idx !== sidx) return ticket;
            if(evt.target.name === 'ticketClassName' && _.find(this.state.ticketClasses, { 'ticketClassName': (evt.target.value).toUpperCase()})) {
                this.setState({
                    ticketNameError: true,
                    errorMessage: 'Class Name cannot be same'
                })
                return { ...ticket, [evt.target.name]: "" } 
            }

            return { ...ticket, [evt.target.name]: (evt.target.value).toUpperCase()};
        });

        let _state = {...this.state}
        _state.ticketClasses = newTicketClasses
        if(evt.target.name === 'ticketClassName') _state.errors[evt.target.name][idx] = false
        this.setState(_state);
    };

    addNewTicketClass = () => {
        const { ticketClasses } = this.state
        let flag = false
        for (let key in ticketClasses) {
            if(ticketClasses[key].ticketClassName === "") {
                flag = true
                break;
            }
        }
        if (flag) {
            swal("Error", "Please fill data to add more", "error")
            return
        }
        this.setState({
            ticketClasses: this.state.ticketClasses.concat([{ ticketClassName: "", ticketClassColor: "", ticketClassType: "REGULAR" }])
        });
    };

    removeTicketClasses = idx => () => {
        this.setState({
            ticketClasses: this.state.ticketClasses.filter((s, sidx) => {
                return sidx != idx
            })
        });
    };

    saveTicketClasses  = () => {
        const { ticketClasses } = this.state
        let _state = {...this.state}
        let flag = true
        if(ticketClasses.length<1) {
            swal("Error", "Add atleast one ticket class", "error")
            flag = false
            return
        }
        for (let key in ticketClasses) {
            if(ticketClasses[key].ticketClassName === "" || ticketClasses[key].ticketClassName === null) {
                flag = false
                _state.errors.ticketClassName[key] = {ticketClassName: true, ticketClassNameMsg: "ClassName cannot be empty"}
                break
            }
            else if(ticketClasses[key].ticketClassColor === "" || ticketClasses[key].ticketClassColor === null) {
                flag = false
                _state.errors.ticketClassColor[key] = {ticketClassColor: true, ticketClassColorMsg: "ticketClassColor cannot be empty"}
                break
            }
        }
        this.setState(_state)
        if(!flag) return swal("Error", "Required information missing", "error")
        if (_state.parentEventUUID === "")  return swal("Error", "Create Event First", "error")
        this.setState({ticketNameError: false, ticketColorError: false})
        let _ticketClasses = {
            ticketClassesConfig: ticketClasses,
            parentEvent: _state.parentEventUUID
        }
        this.props.showNext(true)
        this.props.saveTicketClasses(_ticketClasses)
        
    }

    render() {
        const {errors} = this.state
        return (
            <div>
                {this.props.StandardEvent.loading || this.props.loader ? <Loader/> : null}
                <Row style={styles.margin} className='mt-5'>
                <Col md={{size:8, offset: 3}}>
                            <Row >
                    <Col md="4">
                        <p><b>Ticket Class Name  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></b></p> 
                    </Col>
                    <Col md="2">
                        <p><b>Class Color</b></p>
                    </Col>
                  {this.state.eventType !== 'STANDARD'?   <Col md="4">
                        <p><b>Type</b></p>
                    </Col>: null}
                    </Row>
                    </Col>
                </Row>

                {
                    this.state.ticketClasses.map((share, id) => {
                        return (
                            <Row key={id} style={styles.margin} >
                            <Col md={{size:8, offset: 3}}>
                            <Row >
                                <Col md="4">
                                    <Input type="text" value={share.ticketClassName} name="ticketClassName" onChange={this.handleTicketChange(id)}
                                        disabled={this.props.StandardEvent.ticketClassesSaved}  />
                                        <FormText>{typeof errors.ticketClassName[id] != "undefined" && errors.ticketClassName[id].ticketClassName?
                                        <p style = {styles.error}>{this.state.errorMessage}</p>: null}</FormText>
                                </Col>
                                <Col md="2">
                                    <Input type="color" name="ticketClassColor"
                                        id = "ticketClassColor"
                                        onChange = {this.handleTicketChange(id)}
                                        value = {share.ticketClassColor}
                                        disabled={this.props.StandardEvent.ticketClassesSaved}
                                        style={{"borderRadius":"18px","padding":"15px","width":"15px","height":"15px", background: share.ticketClassColor}}
                                        defaultValue = {share.ticketClassColor}  />
                                        <FormText>{typeof errors.ticketClassColor[id] != "undefined" && errors.ticketClassColor[id].ticketClassColor?
                                        <p style = {styles.error}>Class Color is required</p>: null}</FormText>
                                </Col>
                                {this.state.eventType !== 'STANDARD' ?<Col md = "4" >
                                    
                                    <Input type="select" onChange = {this.handleTicketChange(id)} 
                                    value = {share.ticketClassType} defaultValue = {share.ticketClassType} name = "ticketClassType" disabled = {this.props.StandardEvent.ticketClassesSaved}>
                                        <option value = "REGULAR">REGULAR</option>
                                        <option value = "PASS">PASS</option>
                                    </Input>
                                </Col>: null }
                                <Col md = "2">
                                    <Button disabled = {this.props.StandardEvent.ticketClassesSaved}
                                    onClick={this.removeTicketClasses(id)} style={styles.button}>
                                        <i className="fas fa-trash-alt"></i>
                                    </Button>

                                </Col>
                                </Row>
                                </Col>
                            </Row>
                        )
                    })
                }
                <Row style={styles.margin}>
                    <Col md={{size:6, offset: 3}} className='mt-2'>
                            <Button 
                            disabled = {this.props.StandardEvent.ticketClassesSaved}
                            onClick={this.addNewTicketClass}>
                               <i className='fas fa-plus'/> Add More
						    </Button> 
                    </Col>
                </Row>

                <Row >
                <Col md={{size:6, offset: 3}} className='mt-2'>
                        <Button 
                        disabled = {this.props.StandardEvent.ticketClassesSaved}
                         onClick = {this.saveTicketClasses} style={{padding:'10px 30px'}}>
                            {this.props.StandardEvent.ticketClassesSaved ? 'Ticket Classes Saved': 'Save'}
                        </Button>
                    </Col>

                </Row>


            </div>


        );
    }
}

const styles = {
    margin: {
        marginTop: 10,
        marginBottom: 10
    },
    error: {
        marginTop: 10,
        color: 'red'
    }
}
const mapStateToProps = (state) => {
    return state;
} 
export default connect(mapStateToProps, {saveTicketClasses})(TicketClasses)