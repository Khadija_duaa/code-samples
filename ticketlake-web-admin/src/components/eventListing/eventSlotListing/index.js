import React, { Component } from 'react'
import { connect } from "react-redux";
import { getAllEventsSlots } from '../../../redux/actions/passes'
import { Popconfirm, message } from 'antd';
import { loadCategories } from "../../../redux/actions/eventCategory";
import { deleteEventSlot, resetDeleteState, publishEvent, resetPublishState } from '../../../redux/actions/events/eventActions'
import { resetEventBasicInfo, resetEventSlotState, cancelEventSlot, resetCancelState } from '../../../redux/actions/events'
import Loader from '../../../utils/loader';
import SeriesConfig from '../../../views/events/standard/Details'
import RecurrConfig from '../../../views/events/recurring/Details'
import Stats from '../../stats'
import axios from '../../../utils/axios'
import moment from "moment";

import {
    Card,
    Table,
    Modal,
    ModalBody,
    ModalHeader,
    ModalFooter,
    Row,
    CardTitle,
    CardBody,
    Col, Button
} from 'reactstrap';
import TableHeads from '../../../utils/Headers';
const header = ["Event ID", "Name.", "Start Date", "End Date", "Available Tickets", "Venue", "Address", "Seating", "City", "Country", "Event Type", "Status", "Action"];

class EventSlotList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            ticketClasses: [],
            ticketClassesConfig: [],
            eventType: '',
            _id: ''
        };
    }

    componentWillMount() {
        this.props.resetEventBasicInfo()
        this.props.resetEventSlotState()
        if (this.props.match.params.parentEventId) {
            this.props.getAllEventsSlots(this.props.match.params.parentEventId);
        }
    }
    componentDidMount() {
        window.scrollTo(0, 0)
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.CancelEvent.canceled) {
            message.success("Event Canceled Successfully")
            this.props.resetCancelState()
            this.props.getAllEventsSlots(this.props.match.params.parentEventId);
        }

        if (nextProps.DeleteEvent.deleted) {
            message.success("Event Deleted Successfully")
            if(this.props.match.params.eventType === 'STANDARD') 
                this.props.history.push(`/events`)
            this.props.resetDeleteState()
            this.props.getAllEventsSlots(this.props.match.params.parentEventId);
        }
        if (nextProps.CancelEvent.error) {
            message.error("Error canceling Event")
            this.props.resetCancelState()
        }
        if (nextProps.DeleteEvent.error) {
            message.error(nextProps.DeleteEvent.errors)
            this.props.resetDeleteState()
        }
        if (nextProps.PublishEvent.error) {
            message.error(nextProps.PublishEvent.errors)
            this.props.resetPublishState()
        }
        if (nextProps.PublishEvent.published) {
            message.success(nextProps.PublishEvent.data)
            this.props.getAllEventsSlots(this.props.match.params.parentEventId);
            this.props.resetPublishState()
        }
    }
    getTableHeaders = () => {
        return (
            <TableHeads>
                {header}
            </TableHeads>
        );
    };
    editEvent = (event, slotId, eventType) => {
        if (eventType === "STANDARD") this.props.history.push(`/events/standard/?${slotId}`)
        else this.props.history.push(`/events/passes/?${slotId}`)
    }
    loadConfiguration = (eventId, eventType) => {
        const URL = `/events/get-passes`
        axios.post(URL, {
            parentEventId: this.props.match.params.parentEventId
        }).then(ticketClasses => {
            if(ticketClasses.data.data.ticketClassesConfig.length > 0) {
                this.setState({
                    ticketClassesConfig: ticketClasses.data.data.ticketClassesConfig
                }, () => {this.toggle()})
            } else {
                message.error("Ticket classes not found")
            }
        }).catch(err => {
            message.error("Server Error")  
        })
    }

    toggle = (flag = false) => {
        this.setState(prevState => ({
            modal: !prevState.modal,
            eventType: this.props.match.params.eventType
        }), () => {
            if (flag) this.props.getAllEventsSlots(this.props.match.params.parentEventId);
        });
    }
    confirm = (action, eventSlotId) => {
        if (action === 'delete') return this.props.deleteEventSlot(eventSlotId)
        if (action === 'publish') return this.props.publishEvent(eventSlotId)
        this.props.cancelEventSlot(eventSlotId)
    }

    cancel = (e) => { }

    render() {

        const { PassConfiguration } = this.props
        return <div>

            {PassConfiguration.loading ? <Loader /> :
            <React.Fragment>
                <div style = {{marginTop: 10, marginBottom: 30}}>
                        <h3>
                            {this.props.match.params ? this.props.match.params.title : null}
                        </h3>
                </div>
                <Stats/>
                
                <Row>
                
                    <Col>
                        <Row>
                            
                            <Col md = {{size: 12}}>
                                {this.props.match.params.eventType !== 'STANDARD'? <Button style = {{float: 'right'}} onClick = {this.loadConfiguration}>Add New Slot</Button>: null}
                            </Col>

                        </Row>
                        <Row>
                            <Col md="12" className="mt-3">
                                <Card>
                                    <Table bordered hover striped responsive>
                                        <thead>
                                            {this.getTableHeaders()}
                                        </thead>
                                        <tbody>
                                            {
                                                typeof PassConfiguration !== 'undefined' && PassConfiguration.eventSlots.length > 0 ? PassConfiguration.eventSlots.map((event, i) => {
                                                    return (
                                                        <tr key={event.eventSlotId}>
                                                            <td>
                                                                {event.eventSlotId? event.eventSlotId.split("-")[0]: null}
                                                            </td>

                                                            <td>
                                                                {event.eventTitle ? event.eventTitle: null}
                                                            </td>
                                                            <td>
                                                                {event.eventDateTimeSlot.eventStartTime ? 
                                                                    moment(event.eventDateTimeSlot.eventStartTime).local().format('YYYY-MM-DD HH:mm:ss'): null}
                                                            </td>
                                                            <td>
                                                                {
                                                                    event.eventDateTimeSlot.eventEndTime? 
                                                                    moment(event.eventDateTimeSlot.eventEndTime).local().format('YYYY-MM-DD HH:mm:ss'):null
                                                                }
                                                            </td>
                                                            <td>{event.totalAvailableTickets? event.totalAvailableTickets: null}</td>
                                                            <td>{event.venue ? event.venue.name : null}</td>
                                                            <td>{event.venue ? event.venue.address : null}</td>
                                                            <td>{event.venue ? event.venue.seatingCapacity : null}</td>
                                                            <td>{event.venue ? event.venue.city : null}</td>
                                                            <td>{event.venue ? event.venue.country : null}</td>
                                                            <td>{event.eventType ? event.eventType: null}</td>
                                                            <td>{event.slotStatus? event.slotStatus: null}</td>
                                                            <td>
                                                                <Button onClick={() => this.editEvent(event, event.eventSlotId, event.eventType)}disabled={event.slotStatus === 'CANCELLED' ? true : false}><i className="fas fa-edit" /></Button>
                                                                <Popconfirm
                                                                    title="Are you sure you want to cancel this event?"
                                                                    onConfirm={() => this.confirm('cancel', event.eventSlotId)}
                                                                    onCancel={this.cancel}
                                                                    okText="Yes"
                                                                    cancelText="No"
                                                                >
                                                                    <Button disabled={event.slotStatus === 'CANCELLED' ? true : false}><i className="fas fa-ban" /></Button>
                                                                </Popconfirm>
                                                                <Popconfirm
                                                                    title="Are you sure you want to delete this event?"
                                                                    onConfirm={() => this.confirm('delete', event.eventSlotId)}
                                                                    onCancel={this.cancel}
                                                                    okText="Yes"
                                                                    cancelText="No"
                                                                >
                                                                    <Button disabled={event.slotStatus === 'CANCELLED' ? true : false}><i className="fa fa-trash" /></Button>
                                                                </Popconfirm>

                                                                <Popconfirm
                                                                    title="Are you sure you want to publish this event?"
                                                                    onConfirm={() => this.confirm('publish', event.eventSlotId)}
                                                                    onCancel={this.cancel}
                                                                    okText="Yes"
                                                                    cancelText="No"
                                                                >
                                                                    <Button disabled={event.slotStatus === 'CANCELLED' ? true : false}>{!event.isPublished ? "Publish": "Unpublish"}</Button>
                                                                </Popconfirm>

                                                                {/* <Button onClick={() => this.loadConfiguration(event._id, event.eventType)} style={event.eventType === 'STANDARD' ? styles.displayBlock : null}><i className='fa fa-plus' /></Button> */}
                                                            </td>
                                                        </tr>)
                                                }) : null
                                            }
                                        </tbody>
                                    </Table>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                </React.Fragment>
            }
            <div>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} size="lg" style={styles.modal}>
                    <ModalHeader toggle={this.toggle}>Add New Event Slot</ModalHeader>
                    <ModalBody style={{ width: '1200px' }}>
                        {this.state.eventType == "SERIES" ? <SeriesConfig ticketClassesConfig={this.state.ticketClassesConfig}
                            parentEventId={this.props.match.params.parentEventId} toggle={this.toggle} newSlot={true} eventType={this.state.eventType} /> :
                            <RecurrConfig ticketClassesConfig={this.state.ticketClassesConfig} eventType={this.state.eventType}
                                parentEventId={this.props.match.params.parentEventId} toggle={this.toggle} newSlot={true} />}
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Close</Button>
                    </ModalFooter>
                </Modal>
            </div>
            <div>
            </div>
        </div>
    }
}

const styles = {
    displayBlock: {
        display: 'none'
    },
    modal: {
        width: '50vw',
        maxWidth: '50vw'
    }
}
const mapStateToProps = (state) => {
    return {
        PassConfiguration: state.PassConfiguration,
        CancelEvent: state.CancelEvent,
        DeleteEvent: state.DeleteEvent,
        PublishEvent: state.PublishEvent
    }
};

export default connect(mapStateToProps, {
    getAllEventsSlots, loadCategories,
    resetEventBasicInfo, resetEventSlotState, cancelEventSlot, resetCancelState,
    deleteEventSlot, resetDeleteState, publishEvent, resetPublishState
})(EventSlotList);
