import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getAllEvents} from '../../../redux/actions/events'
import {loadCategories} from "../../../redux/actions/eventCategory/index";
import {Row, Col, Table, Form, FormGroup, Label, Button, Input, Card, InputGroup, InputGroupAddon} from 'reactstrap'
import moment from 'moment'
import TableHeads from '../../../utils/Headers';
const header = ["ID", "Name", "Location", "Date"];

class AllPublicEvents extends Component {
    constructor(props) {
        super(props)
        this.state = {
            "paginate": false,
            "page": 0,
            "skip": 0,
            "pageSize": 10,
            "country": "",
            "city": "",
            "celebrities": [],
            "categories": [],
            "isFeatured": false,
            "isPublished": true,
            "to": "",
            "from": "",
            "search": "",
            "status":"CANCELLED",
            "searchString": ""
        }
    }
    componentDidMount() {
        this.props.getAllEvents(this.state)
        this.props.loadCategories();
    }
    hanldeSearchField = (event) => {
        console.log('hanldeSearchField')
        this.setState({
            searchString: event.target.value
        })
    }
    searchField = () => {
        return (
            <Form className="form-horizontal" style={{float:'right'}}>
                <FormGroup>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="submit" disabled color="danger"><i className="fa fa-search"/> Search</Button>
                        </InputGroupAddon>
                        <Input type="text" id="input1-group2" name="input1-group2" placeholder="Type here..."
                              onChange = {this.hanldeSearchField} />
                    </InputGroup>
                </FormGroup>
            </Form>
        );
    };
    dateFilter = (e)  => {
        const today = new Date();
        let tomorrow = new Date();
        tomorrow.setDate(today.getDate()+1);
        const _state = {...this.state}
        const {target} = e;
        if(target.value === "today") {
            _state.from = today.toString()
            _state.to = today.toString()
        }
        else if (target.value === "tomorrow") {
            _state.from = tomorrow.toString()
            _state.to = tomorrow.toString()
        } else {
            _state.from = ""
            _state.to = ""
        }
        _state.eventDay = target.value
        this.setState(_state, function() {
            this.props.getAllEvents(this.state);
        });
        

        
    }
    onCategoryChange = (e) => {
        let {target} = e;
        let state = {...this.state};
        state.categories = []
        if (target.value === "all") state.categories = []
        else state.categories.push(target.value);
        state.category = target.value
        this.setState({...state});
        this.getCategoryFilters(state);

    };
    getCategoryFilters = (state) => {

        let _state ={...state};
        if (state.categories !== []) {
            this.props.getAllEvents(_state)
        } else {
            return null;
        }

    };

    getTableHeaders = () => {
        return (
            <TableHeads>
                {header}
            </TableHeads>
        );
    };
    handleClick = (eventId)  => {
        this.props.callback(eventId)
    }
    renderAllEventData = () => {
        const {searchString} = this.state
        const filter = searchString.toLowerCase()
        if(!this.props.events) return null;
        return (
            Array.isArray(this.props.events )&& this.props.events.map((event, i) => {
                if (event.eventTitle.toLowerCase().includes(filter) || filter === "")
                return (
                    <tr key={event.eventTitle}>
                        <td onClick = {() => this.handleClick(event._id)}>
                                {event.eventSlotId.split("-")[0]}
                        </td>
                        <td onClick = {() => this.handleClick(event._id)}>{event.eventTitle}
                        </td>
                        <td onClick = {() => this.handleClick(event._id)}>
                                {typeof event.venue !== 'undefined' ? event.venue.city : null}
                        </td>
                        <td onClick = {() => this.handleClick(event._id)}>
                                {moment(event.eventDateTimeSlot.eventStartTime).local().format('YYYY-MM-DD')}
                        </td>
                    </tr>
                )
            })

        );
    };
    render () {
        return (
            <Row>

                    <Col>
                        <Row>
                            <Col md={2} >
                            {
                                    this.props.categories.length > 0 ? 
                                    <FormGroup>
                                        <Label for="categories">Event Category</Label>
                                        <Input type="select" name="categories" id="categories" onChange = {this.onCategoryChange} value = {this.state.category}>
                                            <option key={"all"} value = "all">All</option>
                                            {
                                                this.props.categories.map(category => {
                                                    return (
                                                        <React.Fragment>
                                                            
                                                            <option key={category._id} value = {category._id}>{category.categoryName}</option>
                                                        </React.Fragment>
                                                        
                                                    )
                                                })
                                            }
                                        </Input>
                                    </FormGroup> : null
                                }
                            </Col>
                            <Col md={2} >
                            {
                                <FormGroup>
                                    <Label for="categories">Event Day</Label>
                                    <Input type="select" name="eventDay" id="eventDay" onChange={this.dateFilter} value={this.state.eventDay}>
                                        <option key={"all"} value="all">All</option>
                                        <option key={"today"} value="today">Today</option>
                                        <option key="tomorrow" value = "tomorrow">Tomorrow</option>
                                    </Input>
                                </FormGroup> 
                                }
                            </Col>
                            <Col md = {2}>
                                <FormGroup>
                                    <Label>Event Type</Label>
                                    <Input type="select">
                                        <option key={"all"} value="all">All</option>
                                        <option key={"today"} value="today">Standard</option>
                                        <option key="tomorrow" value = "tomorrow">Series</option>
                                        <option key="tomorrow" value = "tomorrow">Recurring</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col sm={12}>
                                {this.searchField()}
                            </Col>

                        </Row>
                        <Row>
                            <Col md="12">
                                <Card>
                                    <Table bordered hover striped responsive>
                                        <thead>
                                        {this.getTableHeaders()}
                                        </thead>
                                        <tbody>
                                        {this.renderAllEventData()}
                                        </tbody>
                                    </Table>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        events: state.Events.allEvents,
        categories: state.eventCategories.categories
    }
}
export default connect(mapStateToProps, {getAllEvents, loadCategories})(AllPublicEvents)