import React from 'react';
import {
  Table,
  Row,
  Col,
  Card,
  Label,Button,
    Form,FormGroup,InputGroup,InputGroupAddon,Input
} from 'reactstrap';
import {connect} from "react-redux";
import {searchEvents,setEvent, getAllParentEvents, resetEventBasicInfo, resetEventSlotState} from "../../redux/actions/events";
import {getEventSlots} from '../../redux/actions/passes'
import {loadCategories} from "../../redux/actions/eventCategory/index";
import {withRouter, Link} from "react-router-dom";
import TableHeads from '../../utils/Headers';
import Loader from '../../utils/loader';
import moment from "moment";
import {organizationId} from '../../utils/CONSTANTS'
import {
	OrderStatus,Visits,SalesRatio
}
	from 'components/dashboard-components';
const header = ["Event ID", "Name.", "Start Date", "End Date","Available Tickets", "Action"];
const parentEventHeader = ['Event ID', 'Title', 'Event Type', 'Created By', 'Contact Email']
const searchFields = ['eventTitle', 'eventTitle', 'eventTitle', 'eventDateTimeSlot.eventStartTime', 'eventDateTimeSlot.eventEndTime'];

let date = Date(Date.now());

let currentDate = date.toString();

let todayDate= moment(currentDate).format('YYYY-MM-DD hh:mm:ss');

const country = [
    {
        name: "All"
    },
    {
        name: "Pakistan"
    },
    {
        name: "India"
    },
    {
        name: "China"
    },
    {
        name: "Russia"
    },
];


class Event extends React.Component {


    state = {
        paginate: true,
        page:1,
        skip:0,
        pageSize:10,
        country:"",
        city:"",
        celebrities: [

        ],
        categories: [
        ],
        isFeatured: false,
        isPublished:false,
        from: "",
        to: "",
        searchString: "",
        value:'',
    };

    componentDidMount() {
        this.props.getAllParentEvents(organizationId)
        this.props.resetEventBasicInfo()
        this.props.resetEventSlotState()
        this.props.loadCategories();
        if(this.props.match.params.parentEventId) {
            this.props.getEventSlots(this.props.match.params.parentEventId, false);
        }
    };

    getFilters = (state) => {
        let _state ={...state};

        if (state.country === 'All') {
            this.props.getAllEvents(_state)

        }
        else {
            this.props.getAllEvents(_state,state.country)
        }

};
    getCategoryFilters = (state) => {

        let _state ={...state};
        if (state.categories !== []) {
            this.props.getAllEvents(_state)
        } else {
            return null;
        }

    };


    onInputChange = (e) => {
        let {target} = e;
        let state = {...this.state};
        state[target.name] = target.value;
        this.setState(state);
        this.getFilters(state);
    };

    onCategoryChange = (e) => {

        let {target} = e;
        let state = {...this.state};
        state.categories = []
        if (target.value === "all") state.categories = []
        else state.categories.push(target.value);
        state.category = target.value
        this.setState({...state});
        this.getCategoryFilters(state);

    };
    
    handleSearchString = ({target}) => {
        this.setState({
            searchString: target.value
        })
    }

    searchField = () => {
        return (
            <Form className="form-horizontal" style={{float:'right'}}>
                <FormGroup>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="submit" disabled color="danger"><i className="fa fa-search"/> Search</Button>
                        </InputGroupAddon>
                        <Input type="text" id="input1-group2" name="input1-group2" placeholder="Type here..."
                            onChange = {this.handleSearchString}/>
                    </InputGroup>
                </FormGroup>
            </Form>
        );
    };

    categoryFilter = () => {
        return (
            <select data-placeholder="Category" value={this.state.categories}
                    name="categories"
                    onChange={this.onCategoryChange}
                    style={{
                        float: 'left',
                        height: '30px',
                        width: '200px',
                        marginBottom: '10px',
                        paddingLeft: '10px',
                        borderRadius: '4px'
                    }}
                    className="chosen-select no-search-select">
                <option value = "all">All</option>
                {
                    Array.isArray(this.props.categories) && this.props.categories.map((x, i) => {
                        return (
                            <option key={i} value={x._id}>{x.categoryName}</option>
                        )
                    })
                }
            </select>

        );
    };

    countryFilter = () => {
        return (
            <select name="country" value={this.state.country} onChange={this.onInputChange}
                    style={{
                        float: 'left',
                        height: '30px',
                        width: '200px',
                        marginBottom: '10px',
                        paddingLeft: '10px',
                        borderRadius: '4px'
                    }}
                    className="chosen-select no-search-select">
                <option disabled selected >Select Country</option>
                {
                    Array.isArray(country) && country.map((data, i) => {
                        return (
                            <option key={i} value={data.name}>{data.name}</option>
                        )
                    })
                }
            </select>

        );
    };


    getTableHeaders = () => {
        return (
            <TableHeads>
                {header}
            </TableHeads>
        );
    };

    getParentEventTableHeaders = () => {
        return (
            <TableHeads>
                {parentEventHeader}
            </TableHeads>
        );
    }
    renderParentList = () => {
        const { ParentEventsList } = this.props
        if (typeof ParentEventsList !== 'undefined' && ParentEventsList.data.length > 0) {
            const { searchString } = this.state
            const lowercasedFilter = searchString.toLowerCase();
        return <div>
            {ParentEventsList.loading ? <Loader/> :
                <React.Fragment>
                    <Row>
					<Col sm={12} lg={4}>
						<OrderStatus subName = {"Events Status"} mainName = {"Events Progress"}/>
					</Col>
                    <Col sm={12} md={4}>
						<Visits />
					</Col>
                    <Col sm={12} md={4}>
						<SalesRatio />
					</Col>
				</Row>
                
                <Row>
                    <Col>
                        <Row>
                            <Col md={2} >
                                <Label>Select Category</Label>
                                {this.categoryFilter()}
                            </Col>
                            {/* <Col md={2} >
                                {this.countryFilter()}
                            </Col> */}

                            <Col md = {{size:4, offset: 4}}>
                                {this.searchField()}
                            </Col>
                            <Col md = {2}>
                            <Button onClick = {() => {this.props.history.push('/events/create')}}>Add New Event</Button>
                            </Col>

                        </Row>
                        <Row>
                            <Col md="12">
                                <Card>
                                    <Table bordered hover striped responsive>
                                        <thead>
                                        {this.getParentEventTableHeaders()}
                                        </thead>
                                        <tbody>
                                            {
                                              ParentEventsList.data.map((event, index) => {
                                                  if(event.title.toLowerCase().includes(lowercasedFilter) || 
                                                    event.eventType.toLowerCase().includes(lowercasedFilter) || event.createdBy.toLowerCase().includes(lowercasedFilter) ||lowercasedFilter === "")
                                                    return (
                                                        <tr key={event._id}>                
                                                            <td>
                                                            <Link to ={`/events/detail/${event._id}/${event.eventType}/${event.title}`}>
                                                                {event.parentEvent.split("-")[0]}
                                                            </Link>
                                                            </td>
                                    
                                                            <td>
                                                            <Link to ={`/events/detail/${event._id}/${event.eventType}/${event.title}`}>
                                                                {event.title}
                                                            </Link>
                                                            </td>
                                                            <td>
                                                            <Link to ={`/events/detail/${event._id}/${event.eventType}/${event.title}`}>
                                                                {event.eventType}
                                                            </Link>
                                                            </td>
                                                            <td>
                                                            <Link to ={`/events/detail/${event._id}/${event.eventType}/${event.title}`}>    
                                                                {event.createdBy}
                                                            </Link>
                                                            </td>
                                                            <td>
                                                            <Link to ={`/events/detail/${event._id}/${event.eventType}/${event.title}`}>
                                                                {event.contactPersonInfo.email}
                                                            </Link>
                                                            </td>
                                                            
                                                        </tr>)
                                                    })
                                                }
                                        </tbody>
                                    </Table>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                </React.Fragment>
            }

        </div>
        } else {
            return (<div><h4>{this.props.PassConfiguration.data.length < 1 ?"Loading Events ...": null }</h4></div>)
        }
        
    }
    renderEventDetails = () => {
        const {PassConfiguration} = this.props
        return <div>
            {PassConfiguration.loading ? <Loader/> :
                <Row>
                    <Col>
                        <Row>
                            <Col md={{size:2, offset: 10}}>
                                <Button onClick={() => { this.props.history.push('/events/create') }}>Add New Configuration</Button>
                            </Col>

                        </Row>
                        <Row>
                            <Col md="12" className = "mt-3">
                                <Card>
                                    <Table bordered hover striped responsive>
                                        <thead>
                                        {this.getTableHeaders()}
                                        </thead>
                                        <tbody>
                                            {
                                              typeof PassConfiguration !== 'undefined' && PassConfiguration.slots.length > 0 ? PassConfiguration.slots.map((event, i) => {
                                                    return (
                                                        <tr key={event.eventSlotId}>
                                                            <td>
                                                                {event.eventSlotId.split("-")[0]}
                                                            </td>
                                    
                                                            <td>
                                                                {event.eventTitle}
                                                            </td>
                                                            <td>
                                                                {moment(event.eventDateTimeSlot.eventStartTime).local().format('YYYY-MM-DD HH:mm:ss')}
                                                            </td>
                                                            <td>
                                                                {
                                                                    moment(event.eventDateTimeSlot.eventEndTime).local().format('YYYY-MM-DD HH:mm:ss')
                                                                }
                                                            </td>
                                                            <td>{event.totalAvailableTickets}</td>
                                                            <td>
                                                                <Button onClick = {() => this.editEvent(event, event.eventSlotId)}><i className='fa fa-pencil'/>Edit</Button> 
                                                                {/* <Button><i className='fa fa-pencil-square-o'/>{event.isPublished ? 'UnPublish': 'Publish'}</Button>  */}
                                                            </td>
                                                        </tr>)
                                                    }) : null
                                                }
                                        </tbody>
                                    </Table>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            }

        </div>
    }

    render() {

        return typeof this.props.match.params.parentEventId === 'undefined' ? this.renderParentList() : this.renderEventDetails()
  }
}

const mapStateToProps = (state) => {
    return {
        processing: state.Events.processing,
        events: state.Events.allEvents,
        categories: state.eventCategories.categories,
        message: state.Events.message,
        error: state.Events.error,
        ParentEventsList: state.ParentEventsList,
        PassConfiguration: state.PassConfiguration
    }
};

const connectedComponent = connect(mapStateToProps, {getEventSlots,searchEvents,setEvent,loadCategories, getAllParentEvents, resetEventBasicInfo, resetEventSlotState})(Event);
export default withRouter(connectedComponent);
