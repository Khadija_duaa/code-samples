import React from 'react';
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label
} from 'reactstrap';

const DropDown = (props) => {
    return (
        <Row>
            <Col sm="12" md="6" lg="4">
                <Form className="mt-3">
                    <FormGroup>
                        <Label for="exampleCustomSelect">Select Value</Label>
                        <Input type="select">
                            <option value="">Select</option>
                            <option>Value 1</option>
                            <option>Value 2</option>
                            <option>Value 3</option>
                            <option>Value 4</option>
                            <option>Value 5</option>
                        </Input>
                    </FormGroup>
                </Form>
            </Col>
        </Row>
    );
}

export default DropDown;