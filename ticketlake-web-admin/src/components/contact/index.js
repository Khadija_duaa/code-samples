import React from 'react'
import { Col, Row, FormGroup, Label, Input, Button, FormText } from 'reactstrap';
const styles = {
    marginTop: 10,
    error: {
		marginTop: '10px',
		fontSize: '10px',
		color: 'red',
		fontWeight: 'bold'
	}
}
const Contact = (props) => {
    return (
        <React.Fragment>
            <Row>
                <Col md="6">
                    <h5>
                        Event Contact Details
                        </h5>
                </Col>
            </Row>
            <Row>
                <Col md="6">
                    <FormGroup>
                        <Label>
                            Email <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
                        </Label>
                        <Input type="email" defaultValue={props.email} onChange={props.onChange} disabled = {props.isSaved}/>
                        <FormText>{props.errors.email ? <p style = {styles.error}>{props.errors.emailMsg}</p>: null}</FormText>
                    </FormGroup>
                </Col>
            </Row>
            {

                props.landLine.map((landPhone, index) => {
                    return (
                        <Row key={index}>
                            <Col md = "6">
                                <FormGroup>
                                    <Label> Landline</Label>
                                    <Input type="text" disabled = {props.isSaved} placeholder = "+923004441180" value={landPhone.landline} name="landline" placeholder="94487424" key={index} onChange={props.handleLandLineChange(index)} />
                                <FormText>{typeof props.errors.landline[index] != 'undefined' ?
                                <p style = {styles.error}>{props.errors.landline[index].landlineMsg}</p>: null}</FormText>
                                </FormGroup>
                            </Col>
                            <Col md = "6" style = {{marginTop: '30px'}}>
                                <FormGroup>
                                    <Button onClick={props.removeLandline(index)} style={styles.button} disabled ={props.isSaved}>
                                        <i className="fas fa-trash-alt"></i>
                                    </Button>
                                </FormGroup>

                            </Col>


                        </Row>

                    )
                })
            }
            <Row>
                <Col md="6">
                    <FormGroup>
                        <Button onClick={props.addLandline} >
                            Add Landline
                            </Button>
                    </FormGroup>
                </Col>
            </Row>

            {

                props.mobile.map((mobile, index) => {
                    return (
                        <Row key={index}>
                            <Col md = "6">
                                <FormGroup>
                                    <Label> Mobile</Label>
                                    <Input type="text" disabled = {props.isSaved} placeholder = "+923004441180" value={mobile.mobile} name="mobile" placeholder="0344324324" key={index} onChange={props.handleMobileChange(index)} />
                                    <FormText>{typeof props.errors.mobile[index] != 'undefined' ?
                                <p style = {styles.error}>{props.errors.mobile[index].mobileMsg}</p>: null}</FormText>
                                </FormGroup>
                            </Col>
                            <Col md = "6" style = {{marginTop: '30px'}}>
                                <FormGroup>
                                    <Button onClick={props.removeMobile(index)} style={styles.button} disabled ={props.isSaved}>
                                        <i className="fas fa-trash-alt"></i>
                                    </Button>
                                </FormGroup>

                            </Col>


                        </Row>

                    )
                })
            }
            <Row>
                <Col md="6">
                    <FormGroup>
                        <Button onClick={props.addMobile} style={styles.button} disabled ={props.isSaved}>
                            Add Mobile
        </Button>
                    </FormGroup>
                </Col>
            </Row>
        </React.Fragment>
    )
}
export default Contact