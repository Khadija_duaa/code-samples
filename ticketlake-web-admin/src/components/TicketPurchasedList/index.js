import React, {Component} from 'react'
import {Row, Col, Input, Table, Button,FormText, Modal, ModalHeader, 
    FormGroup,ModalBody, ModalFooter, Label} from 'reactstrap'
import {connect} from 'react-redux'
import {getEventPurchasedTickets, refundTicket, resetState} from '../../redux/actions/tickets'
import Loader from '../../utils/loader';
import moment from 'moment'
import {NotificationManager} from "react-notifications";
class TicketPurchasedList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal: false,
            ticketId: '',
            loading: false,
            error: false,
            saved: false,
            "refundValue": 0,
            "caption": "",
            "refundDescription": "",
            errors: {}
        };
        this.toggle = this.toggle.bind(this);
    }
    componentDidMount () {
        if(typeof this.props.match !== 'undefined' && this.props.match.params.eventId)
            this.props.getEventPurchasedTickets(this.props.match.params.eventId, 'CANCELLED')
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.Refunds.saved) {
            this.props.resetState()
            NotificationManager.success("Ticket Refund Successfully", 1000)
            
        }
        if(nextProps.Refunds.error) {
            this.props.resetState()
            NotificationManager.error("Ticket Not found", "Error", 1000)
            
        }
    }
    renderProps = () => {
        return <>
            <thead>
            <tr>
                <th>
                    Ticket Id
                            </th>
                <th>
                    Purchased By
                            </th>
                <th>
                    Email
                </th>
                <th>
                    Ticket Class
                </th>
                <th>Ticket Type</th>
                <th>Ticket Status</th>
                <th>
                    Mode
                </th>
                <th>Purchased On</th>
                <th>
                    Scan Count
                </th>
                <th>
                    Last Scan
                </th>
                <th>
                    Last Scan By
                </th>
            </tr>
            </thead>
            <tbody>
                {
                    this.props.data.map((ticket, index) => {
                        return (
                            this. props.searchString === "" || ticket.srNo.includes(this.props.searchString)||
                            ticket.purchaseDetails.name.toLowerCase().includes(this.props.searchString) || ticket.lastScanBy && ticket.lastScanBy.toLowerCase().includes(this.props.searchString) ?
                            <tr>
                                <td>
                                    {ticket.srNo}
                                </td>
                                <td>
                                    {ticket.purchaseDetails.name}
                                </td>
                                <td>
                                    {ticket.purchaseDetails.email || null}
                                </td>
                                <td>
                                    {ticket.ticketClassInfo.ticketClassName || null}
                                </td>
                                <td>
                                    {ticket.ticketType || null}
                                </td>
                                <td>
                                    {ticket.ticketStatus || null}
                                </td>
                                <td>
                                    {ticket.isOffline ? "Offline" : "Online"}
                                </td>
                                <td>
                                    {
                                        moment(ticket.purchaseDetails.purchasedDate).format("YYYY-MM-DD HH:mm") || null}
                                </td>
                                <td>
                                    {ticket.scantCount || 0}
                                </td>
                                <td>
                                    {ticket.lastScanAt || null}
                                </td>
                                <td>
                                    {ticket.lastScanBy || null}
                                </td>
                            </tr> : null
                        )
                    })
                }
            </tbody>
        </>
        
    }
    renderState = () => {
        return <>
            <thead>
                <tr>
                    <th>
                        Ticket Id
                            </th>
                    <th>
                        Purchased By
                            </th>
                    <th>
                        Email
                            </th>
                    <th>
                        Action
                            </th>

                </tr>
            </thead>
            <tbody>
                {this.props.Tickets.purchasedTickets.map((ticket, index) => {
                    return (
                        this.props.searchString === "" || typeof this.props.searchString === 'undefined' || ticket.srNo.includes(this.props.searchString) ||
                            ticket.purchaseDetails.name.toLowerCase().includes(this.props.searchString) || ticket.lastScanBy && ticket.lastScanBy.toLowerCase().includes(this.props.searchString) ?
                            <tr>
                                <td>
                                    {ticket.srNo}
                                </td>
                                <td>
                                    {ticket.purchaseDetails.name}
                                </td>
                                <td>
                                    {ticket.purchaseDetails.email || null}
                                </td>
                                <td><Button onClick = { () =>this.refundTicketView(ticket.uuid)}>Refund</Button></td>

                            </tr> : null
                    )
                })
                }
            </tbody>
        </>
    }
    refundTicketView = (ticketId) => {
        this.setState({ticketId}, () => {
            this.toggle()
        })
    }
    refundTicket = () => {
        let flag = false
        const _state = {...this.state}

        if(this.state.caption === "") {
            flag = true
            _state.errors.caption = true
        }
        
        if (this.state.refundDescription === "") {
            flag = true
            _state.errors.refundDescription = true
        }
        if (parseInt(this.state.refundValue) < 1) {
            flag = true
            _state.errors.refundValue = true
        }
        if (flag) {
            return this.setState(_state)
        }
        const obj = {
            "ticketId" : this.state.ticketId,
            "refundInformation" : {
                "refundValue" : this.state.refundValue,
                "caption" : this.state.caption,
                "refundDescription" : this.state.refundDescription
            }
        }
        this.props.refundTicket(obj)
        this.toggle()
    }
    toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
    }
    handleInputChange = ({target}) => {
        const _state = {...this.state}
        _state[target.name] = target.value
        _state.errors[target.name] = false
        this.setState(_state)
    }
    renderRefundView = () => {
        return <Row md>
            <Col md={{size:10, offset: 2}}>
                <Row>
                    <Col md = {8}>
                        <FormGroup>
                            <Label>Refund Value</Label>
                            <Input type = "number" min = {0} name = "refundValue" onChange = {this.handleInputChange}/>
                            <FormText>{this.state.errors.refundValue ? <p style = {styles.error}>{"Refund value should greater than 0"}</p>: null}</FormText>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md = {8}>
                        <FormGroup>
                            <Label>Caption</Label>
                            <Input type = "text"  name = "caption" onChange = {this.handleInputChange}/>
                            <FormText>{this.state.errors.caption ? <p style = {styles.error}>{"Caption is required field"}</p>: null}</FormText>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md = {8}>
                        <FormGroup>
                            <Label>Description</Label>
                            <Input type = "text"  name = "refundDescription" onChange = {this.handleInputChange}/>
                            <FormText>{this.state.errors.refundDescription ? <p style = {styles.error}>{"Description is required field"}</p>: null}</FormText>
                        </FormGroup>
                    </Col>
                </Row>
            </Col>
        </Row>

    }
    render() {
        const {Tickets, Refunds} = this.props
        return (
            <div>
                {/* {this.state.error ? NotificationManager.error("Ticke not found", "Error", 3000) : null}
                {this.state.saved ? NotificationManager.success("Refunded Successfully", "Sucess", 3000) :null} */}
                {Tickets.loading ? <Loader /> : null}
                <Row>
                    <Col md={{ size: 4 }}>
                        <h2>Purchased Tickets</h2>
                    </Col>
                    <Col md={{ size: 2, offset: 6 }}>
                        <Input type="text" className="search-r" placeholder="Search" onChange={this.props.handleSearchString} />
                    </Col>
                </Row>
                <Row>
                    <Col md="12">
                        <Table bordered striped responsive className="white-bg">
                            {this.props.data && this.props.data.length > 0 ? this.renderProps() : this.renderState()}
                        </Table>
                    </Col>
                </Row>
                <div>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                        <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
                        <ModalBody>
                            {this.renderRefundView()}
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={this.refundTicket}>Refund</Button>{' '}
                            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                        </ModalFooter>
                    </Modal>
                </div>
            </div>
            
        )
    }
    
}
const styles = {
    error: {
        color: 'red',
        fontWeight: 'bold'
    }
}
const mapStateToProps = (state) => {
    return state
}

export default connect(mapStateToProps, {getEventPurchasedTickets,refundTicket, resetState})(TicketPurchasedList)