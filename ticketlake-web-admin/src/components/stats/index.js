import React, { PureComponent } from 'react';
import {
  BarChart, ResponsiveContainer, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import Chart from 'react-c3-component';
import 'c3/c3.css';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,  Label,
  Row,
} from 'reactstrap';

import { Table, Divider, Tag } from 'antd';

// import {GoogleComponent} from "react-google-location";
// const API_KEY = 'AIzaSyD3saUQV3v1ubgCWYdtoBVbVvZ-AVGlgl0';

const data = [
  {
    name: 'Page A', uv: 10,
  },
  {
    name: 'Page B', uv: 20,
  },
  {
    name: 'Page C', uv: 30,
  },
  {
    name: 'Page D', uv: 40,
  },
  {
    name: 'Page E', uv: 30,
  },
  {
    name: 'Page F', uv: 40,
  },
  {
    name: 'Page G', uv: 50,
  },
  {
    name: 'Page I', uv: 70,
  },
  {
    name: 'Page J', uv: 90,
  },
  {
    name: 'Page K', uv: 50,
  },
  {
    name: 'Page K', uv: 80,
  },
  {
    name: 'Page L', uv: 50,
  },
  {
    name: 'Page M', uv: 30,
  },
  {
    name: 'Page N', uv: 50,
  },
  {
    name: 'Page O', uv: 20,
  },
  {
    name: 'Page P', uv: 25,
  },
  {
    name: 'Page Q', uv: 50,
  },
  {
    name: 'Page R', uv: 10,
  },
  {
    name: 'Page S', uv: 10,
  },
  {
    name: 'Page T', uv: 50,
  },
  {
    name: 'Page U', uv: 20,
  },
  {
    name: 'Page V', uv: 30,
  },
  {
    name: 'Page X', uv: 50,
  },
  {
    name: 'Page Y', uv: 40,
  },
  {
    name: 'Page Z', uv: 30,
  },
];


const columns = [
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
    text: 'text',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    text: 'text',
  },
  {
    title: 'Price',
    dataIndex: 'price',
    key: 'price',
    text: 'text',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
    text: 'text',
  }
];

const tableData = [
  {
    key: '1',
    date:'9-Sep-2019',
    name: 'John Brown',
    price: 32+'$',
    address: 'New York No. 1 Lake Park',
  },
  {
    key: '2',
    date:'9-Aug-2019',
    name: 'Jim Green',
    price: 42+'$',
    address: 'London No. 1 Lake Park',
  },
  {
    key: '3',
    date:'9-Aug-2019',
    name: 'Joe Black',
    price: 32+'$',
    address: 'Sidney No. 1 Lake Park',
  },
];
class VenuesStats extends PureComponent {


  constructor(props) {
    super(props);
    this.state = {
    };
  }

  
  render() {
    return (
      <div className="custom-section-design">
        <Row>
          <Col md="3">
            <Card className="stats-box">
                  <Row>
                    <Col>{this.props.chart1Name || "Name here"}</Col>
                    <Col className="right percentage"><i className="fas fa-caret-up"></i> 20%</Col>
                  </Row>
                  <Row>
                    <Col>
                      <span className="red-title">{this.props.chart1Data || "62,000"}</span>
                    </Col>
                  </Row>
                  <Row>
                      <Col style={{ width: '100%', height: 150 }}>
                        <ResponsiveContainer>
                          <BarChart className="bar-charts" width={100} height={400} data={data}>
                            <Bar dataKey="uv" fill="#636363"/>
                          </BarChart>
                        </ResponsiveContainer>
                      </Col>
                  </Row>
              </Card>
            </Col>

            <Col md="3">
            <Card className="stats-box">
                  <Row>
                    <Col>Name here</Col>
                    <Col className="right percentage"><i className="fas fa-caret-up"></i> 90%</Col>
                  </Row>
                  <Row>
                    <Col>
                      <span className="red-title">62,000</span>
                    </Col>
                  </Row>
                  <Row>
                      <Col style={{ width: '100%', height: 150 }}>
                        <ResponsiveContainer>
                          <BarChart className="bar-charts" width={100} height={400} data={data}>
                            <Bar dataKey="uv" fill="#636363"/>
                          </BarChart>
                        </ResponsiveContainer>
                      </Col>
                  </Row>
              </Card>
            </Col>

            <Col md="3">
            <Card className="stats-box">
                  <Row>
                    <Col>Name here</Col>
                    <Col className="right percentage"><i className="fas fa-caret-up"></i> 500%</Col>
                  </Row>
                  <Row>
                    <Col>
                      <span className="red-title">62,000</span>
                    </Col>
                  </Row>
                  <Row>
                      <Col style={{ width: '100%', height: 150 }}>
                        <ResponsiveContainer>
                          <BarChart className="bar-charts" width={100} height={400} data={data}>
                            <Bar dataKey="uv" fill="#636363"/>
                          </BarChart>
                        </ResponsiveContainer>
                      </Col>
                  </Row>
              </Card>
            </Col>
            <Col md="3">
            <Card className="stats-box">
                  <Row>
                    <Col>Name here</Col>
                    <Col className="right percentage"><i className="fas fa-caret-up"></i> 70%</Col>
                  </Row>
                  <Row>
                    <Col>
                      <span className="red-title">62,000</span>
                    </Col>
                  </Row>
                  <Row>
                      <Col style={{ width: '100%', height: 150 }}>
                        <ResponsiveContainer>
                          <BarChart className="bar-charts" width={100} height={400} data={data}>
                            <Bar dataKey="uv" fill="#636363"/>
                          </BarChart>
                        </ResponsiveContainer>
                      </Col>
                  </Row>
              </Card>
            </Col>
        </Row>

        <Row>
          <Col md="4">
            <Card className="stats-box">
                  <Row>
                    <Col>
                      <span className="red-sub-title">Details</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="red-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry</Col>
                  </Row>
                  <Row>
                    <Table classname="full-width" columns={columns} dataSource={tableData} pagination={false}/>
                  </Row>
            </Card>
          </Col>
          <Col md="4">
            <Card className="stats-box">
                  <Row>
                    <Col>
                      <span className="red-sub-title">Details</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="red-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry</Col>
                  </Row>
                  <Row>
                    <Table classname="full-width" columns={columns} dataSource={tableData} pagination={false}/>
                  </Row>
            </Card>
          </Col>
          <Col md="4">
					<Card>
						<CardBody>
							<CardTitle>Donut Chart</CardTitle>
							<Chart
								config={{
									data: {
										columns: [
											["option1", 30],
											["option2", 120]
										],
										type: "donut",
										onclick: function (o, n) { console.log("onclick", o, n) },
										onmouseover: function (o, n) { console.log("onmouseover", o, n) },
										onmouseout: function (o, n) { console.log("onmouseout", o, n) }
									},
									donut: { title: "Total Sale" },
									color: { pattern: ["#2962FF", "#4fc3f7", "#f62d51"] }
								}}
							/>
						</CardBody>
					</Card>
				</Col>
        </Row>
      </div>
    );
  }
}



export default VenuesStats;
