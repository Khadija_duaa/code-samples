import React from 'react';
import Datetime from "react-datetime";
import 'react-datetime/css/react-datetime.css';
const yesterday = Datetime.moment().subtract(1, 'day');
const styles = {
	error: {
		fontWeight: 'bold',
		marginTop: 2,
		color: 'red'
	}
}
const EventSchedule = (props) => {
	
	const {errors} = props
	const valid = function( current ){
		return current.isAfter( yesterday );
	};
	return (
		<div className="step step2 mt-5">
			<div>
				<h2>Schedule</h2>
			</div>
			<div>
				<h6>
					Schedule Event Days & Slots
				</h6>
			</div>
			<div className="row">
				<div className="col-lg-8">
					<form>
						<div className="form-group row">
							<label htmlFor="staticEmail" className="col-md-4 col-form-label">
								<i className="mdi mdi-calendar-clock mr-3"></i>
								Event Starting Date Time  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
							</label>
							<div className="col-md-5">
								<Datetime
									timeFormat={true}
									locale="en-gb"
									isValidDate = {valid}
									value = {props.eventDateTime.eventStartTime}
									defaultValue = {props.eventDateTime.eventStartTime}
									onChange = {props.startTime}
									inputProps={{ placeholder: "Datetime Picker Here", disabled: props.isSaved}}
								/>
								<div style = {styles.error}>{errors.eventStartTime?<p>{errors.eventStartTimeMsg}</p>: null}</div>
							</div>
						</div>
						<div className="form-group row">
							<label htmlFor="staticEmail" className="col-md-4 col-form-label">
								<i className="mdi mdi-calendar-clock mr-2"></i>Event Ending Date Time  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
							</label>
							<div className="col-md-5">
								<Datetime
									timeFormat={true}
									locale="en-gb"
									isValidDate = {valid}
									value = {props.eventDateTime.eventEndTime}
									defaultValue = {props.eventDateTime.eventEndTime}
									onChange = {props.endTime}
									inputProps={{ placeholder: "Datetime Picker Here", disabled: props.isSaved }}
								/>
								<div style = {styles.error}>{errors.eventEndTime?<p>{errors.eventEndTimeMsg}</p>: null}</div>
							</div>
						</div>
						<div className="form-group row">
							
							<label htmlFor="staticEmail" className="col-md-4 col-form-label">
								<i className="mdi mdi-calendar-clock mr-2"></i>Entry Starting Date Time  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
							</label>
							<div className="col-md-5">
								<Datetime
									locale="en-gb"
									onChange = {props.eventEntry}
									isValidDate={valid}
									value = {props.eventEntryTime.entryStartTime}
									defaultValue = {props.eventEntryTime.entryStartTime}
									inputProps={{ placeholder: "Datetime Picker Here" ,  disabled: props.isSaved}}
								/>
								<div style = {styles.error}>{errors.entryStartTime?<p>{errors.entryStartTimeMsg}</p>: null}</div>
							</div>
						</div>
						<div className = "form-group row">
						<label htmlFor="staticEmail" className="col-md-4 col-form-label">
								<i className="mdi mdi-calendar-clock mr-2"></i>Entry Closing Date Time  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
							</label>
							<div className="col-md-5">
								<Datetime
									locale="en-gb"
									isValidDate={valid}
									onChange = {props.eventEntryClose}
									value = {props.eventEntryTime.entryEndTime}
									defaultValue = {props.eventEntryTime.entryEndTime}
									inputProps={{ placeholder: "Datetime Picker Here",  disabled: props.isSaved }}
								/>
								<div style = {styles.error}>{errors.entryEndTime?<p>{errors.entryEndTimeMsg}</p>: null}</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	)
}

export default EventSchedule