import React, {Component} from 'react';
import Datetime from "react-datetime";
import 'react-datetime/css/react-datetime.css';
import {Row, Col,FormGroup, Label, FormText, Button} from 'reactstrap'
import swal from 'sweetalert';
import moment from 'moment'
const yesterday = Datetime.moment().subtract(1, 'day');
export default class EventSchedule extends Component {
    constructor(props) {
        super(props)
        this.state = {
            eventStartTime: '',
            eventEndTime: '',
            entryStartTime: '',
            entryEndTime: '',
            errors: {},
        }
    }
    onChange = (dateObject, fieldName) => {
        this.setState({
            errors: {...this.state.errors, [fieldName]: false},
            [fieldName]: dateObject._d
        })
    }
    validateTime = (current) => {
        return current.isAfter( yesterday );
    }
    validate = () => {
        let _state = {...this.state}
        let flag = true


        if (_state.eventStartTime === "")	{
			flag = false
			_state.errors.eventStartTime = true
			_state.errors.eventStartTimeMsg = "Event Start time is required"
		}
		if (_state.eventEndTime === "") {
			flag = false
			_state.errors.eventEndTime = true
			_state.errors.eventEndTimeMsg = "Event End time is required"
		}
		if(_state.entryStartTime === "") {
			flag = false
			_state.errors.entryStartTime = true
			_state.errors.entryStartTimeMsg = "Event Entry time is required"
		}
		if(_state.entryEndTime === "") {
			flag = false
			_state.errors.entryEndTime = true
			_state.errors.entryEndTimeMsg = "Event Entry Close time is required"
		}
		if(_state.eventStartTime != "" && _state.eventEndTime != "" &&
            _state.eventEndTime <= _state.eventStartTime ) {
			flag = false
			_state.errors.eventEndTime = true
			const now = moment(_state.eventEndTime);
			const duration = moment.duration(now.diff(_state.eventStartTime));
			_state.errors.eventEndTimeMsg = "Event end time should be greater than start time"
			if (duration.asHours() <1) _state.errors.eventEndTimeMsg = "Even Slot can be minimum of 1 hour"
			
		}
		if(_state.entryStartTime != "" && _state.eventStartTime != "" &&
            _state.entryStartTime >= _state.eventStartTime) {
			flag = false
			_state.errors.entryStartTime = true
			_state.errors.entryStartTimeMsg = 'Event Entry time cannot be greater than start time'
		}

		if(_state.entryEndTime > _state.eventEndTime) {
			flag = false
			_state.errors.entryEndTime = true
			_state.errors.entryEndTimeMsg = 'Event Entry time cannot be greater than start time'
		}
 
        this.setState(_state)
        if(!flag) {
            swal("Error", "Please provide all provided fields", "error")
            return
        }
        this.props.addSlots(this.state)
    }
    render() {
        const { errors } = this.state
        return (
            <Row form>
                <Col md={6}>
                    <FormGroup>
                        <Label for="exampleEmail">Start Date Time</Label>
                        <Datetime
                            onChange={moment => this.onChange(moment, 'eventStartTime')}
                            timeFormat={true}
                            isValidDate = {this.validateTime}
                            locale="en-gb"
                            value = {this.state.eventStartTime}
                            inputProps={{ placeholder: "Datetime Picker" }}
                        />
                        <FormText>
                            {errors.eventStartTime ? <p style = {style.errorMsg}>{errors.eventStartTimeMsg}</p>: null}
                        </FormText>
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                        <Label for="examplePassword">End Date Time</Label>
                        <Datetime
                            timeFormat={true}
                            locale="en-gb"
                            isValidDate = {this.validateTime}
                            onChange={moment => this.onChange(moment, 'eventEndTime')}
                            value = {this.state.eventEndTime}
                            inputProps={{ placeholder: "Datetime Picker"}}
                        />
                        <FormText>
                            {errors.eventEndTime ? <p style = {style.errorMsg}>{errors.eventEndTimeMsg}</p>: null}
                        </FormText>
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                        <Label for="exampleEmail">Entry Start</Label>
                        <Datetime
                            timeFormat={true}
                            locale="en-gb"
                            isValidDate = {this.validateTime}
                            onChange={moment => this.onChange(moment, 'entryStartTime')}
                            value = {this.state.entryStartTime}
                            inputProps={{ placeholder: "Datetime Picker" }}
                        />
                        <FormText>
                            {errors.entryStartTime ? <p style = {style.errorMsg}>{errors.entryStartTimeMsg}</p>: null}
                        </FormText>
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                        <Label for="examplePassword">Entry Close</Label>
                        <Datetime
                            timeFormat={true}
                            isValidDate = {this.validateTime}
                            locale="en-gb"
                            onChange={moment => this.onChange(moment, 'entryEndTime')}
                            value = {this.state.entryEndTime}
                            inputProps={{ placeholder: "Datetime Picker" }}
                        />
                        <FormText>
                            {errors.entryEndTime ? <p style = {style.errorMsg}>{errors.entryEndTimeMsg}</p>: null}
                        </FormText>
                    </FormGroup>
                </Col>
                    <Col md = {6}>
                    <Button onClick = {this.validate} color="primary" size="lg">Add Slot</Button>
                    </Col>
            </Row>
        )
    }
}

const style = {
    errorMsg: {
        fontWeight: 'bold',
        color: 'red'
    }
}