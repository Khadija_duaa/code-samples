import React from 'react';


class Footer extends React.Component {
  render() {
    return (
      <footer className="footer text-center">
        All Rights Reserved by ticketlake. Designed and Developed by Synavos
      </footer>
    );
  }
}
export default Footer;
