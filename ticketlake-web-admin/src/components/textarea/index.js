import React from 'react';
import {
    Form,
    FormGroup,
    Input
} from 'reactstrap';

const TextArea = (props) => {
    return (
        <Form>
            <FormGroup>
                <Input type="textarea" rows="3" />
            </FormGroup>
        </Form>
    );
}

export default TextArea;
