import React, { Component } from 'react';
import Select from 'react-select';
import TagsInput from "react-tagsinput";
import { connect } from 'react-redux';
import {getAllCategories} from '../../redux/actions/categories'
import { saveEventBasicInfo } from '../../redux/actions/events'
import Loader from '../loader'
import { Button, Col } from 'reactstrap'
import swal from 'sweetalert';
import Contact from '../contact'
import {NotificationManager} from 'react-notifications';


class BasicInfo extends Component {
	constructor(props) {
		super(props);

		this.state = {
			eventBasicInfo: {
				title: "",
				organizationId: "5cad067ce5d2dd0016d33790",
				eventType : this.props.eventType,
				eventCategories: [],
				currency : "$",
				description: "",
				parentEvent: null,
				isSave: false,
				tags: [],
				contactPersonInfo: {
					email: '',
					landLine: [],
					phoneNumber: []
				}
			},
			email: '',
			landLine: [{
                landline: ''
            }],
            mobile: [{
                mobile: ''
			}],
			errors: {landline: [], mobile: []}
		};
	}
	componentDidMount() {
		
		this.props.getAllCategories()
	}
	handleDropDownChange = (events) => {
		let _state = this.state
		_state.events = events
		_state.errors.category = false
		if(events.length > 0) {
			let _eventIds = []
			events.forEach(event => {
				_eventIds.push(event.value)
			})
			_state.eventBasicInfo.eventCategories = _eventIds
		} else _state.eventBasicInfo.eventCategories = []
		this.setState (_state)
		
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.eventSlotDetail !== this.props.eventSlotDetail && typeof nextProps.eventSlotDetail.eventSlotId !== 'undefined') {
			let _state  = {...this.state}
			_state.eventSlotId = nextProps.eventSlotDetail.eventSlotId || null
			_state.eventBasicInfo.title = nextProps.eventSlotDetail.parentEventInfo.title || null
			_state.eventBasicInfo.currency = nextProps.eventSlotDetail.parentEventInfo.currency || null
			_state.eventBasicInfo.description = nextProps.eventSlotDetail.parentEventInfo.description || null
			_state.eventBasicInfo.tags = nextProps.eventSlotDetail.parentEventInfo.tags || []
			_state.eventBasicInfo.parentEvent = nextProps.eventSlotDetail.parentEventInfo.parentEvent || null
			_state.email = nextProps.eventSlotDetail.parentEventInfo.contactPersonInfo.email || null
			_state.eventBasicInfo.eventType = nextProps.eventSlotDetail.parentEventInfo.eventType || null
			if(nextProps.eventSlotDetail.categories.length > 0) {
				let events = []
				nextProps.eventSlotDetail.categories.map(category => {
					events.push({
						"label": category.name,
						"value": category._id
					})
					_state.eventBasicInfo.eventCategories.push(category._id)
				})
				_state.events = events
			}
			if(typeof nextProps.eventSlotDetail.parentEventInfo.contactPersonInfo.landLine != 'undefined' && 
				nextProps.eventSlotDetail.parentEventInfo.contactPersonInfo.landLine.length > 0) {
				let landline = []
				nextProps.eventSlotDetail.parentEventInfo.contactPersonInfo.landLine.map(phone => {
					landline.push({
						landline: phone
					})
				})
				_state.landLine = landline
			}
			if(typeof nextProps.eventSlotDetail.parentEventInfo.contactPersonInfo.phoneNumber != 'undefined'
				&& nextProps.eventSlotDetail.parentEventInfo.contactPersonInfo.phoneNumber.length > 0) {
				let mobile = []
				nextProps.eventSlotDetail.parentEventInfo.contactPersonInfo.phoneNumber.map(phone => {
					mobile.push({
						mobile: phone
					})
				})
				_state.mobile = mobile
			}
			this.setState(_state)
		}
		if(this.props.StandardEvent.error !== nextProps.StandardEvent.error) {
			if(Array.isArray(nextProps.StandardEvent.errors)) {
				nextProps.StandardEvent.errors.map(error => {
					NotificationManager.error(error, "Error")
				})
			} else {
				NotificationManager.error(nextProps.StandardEvent.errors, "Error")
			}

		}
		if (nextProps.StandardEvent.eventSaved !== this.props.StandardEvent.eventSaved) {
			this.setState({
				isSave: nextProps.StandardEvent.eventSaved
			}, function() {
				NotificationManager.success('Event basic information saved', 'Success', 3000);
			})
			this.props.showNext(true)
		}
	}
	handleRegularTagsChange = (tags) =>{
		let _state = {...this.state}
		_state.eventBasicInfo.tags = tags
		
		this.setState(_state);
	}
	saveEventBasicInfo = (event) => {
		event.preventDefault();
		const {errors, eventBasicInfo, landLine, mobile, email } = this.state
		errors.landline = [];
		errors.mobile = [];
		let _landline = []
		let _mobile = []
		let flag = true;
		let _state = {...this.state}

		if (eventBasicInfo.eventCategories.length < 1) {
			flag = false
			errors.category = true
			errors.categoryMsg = "One or more than one category to be added"
		}
		if (eventBasicInfo.description === "") {
			flag = false
			errors.description = true
			errors.descriptionMsg = "Description is required field"
		}
		if (eventBasicInfo.title === "") {
			flag = false
			errors.title = true
			errors.titleMsg = "Event title is required field"
		}
		
        if(email === "" || email === null) {
			errors.email = true
			errors.emailMsg = "Email is required field"
			flag = false;
		}
		else if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email)) {
			errors.email = true
			errors.emailMsg = "Email format is not correct"
			flag = false;
		}
        for (let key in landLine) {
			if (landLine[key].landline === "" || landLine[key].landline === null) {
				errors.landline[key]  = {landline: true}
				errors.landline[key] = {landlineMsg: "Landline is required"}
				flag = false
			}
			else if(!/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(landLine[key].landline)) {
				errors.landline[key]  = {landline: true}
				errors.landline[key] = {landlineMsg: "Landline format is not correct"}
				flag = false
			}
            _landline.push(landLine[key].landline)
                
        }

        for (let key in mobile) {
			if (mobile[key].mobile === "" || mobile[key].mobile === null) {
				errors.mobile[key] = {mobile: true}
				errors.mobile[key] = {mobileMsg: "Mobile is required"}
				flag = false
				break
			}
			else if(!/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(mobile[key].mobile)) {
				errors.mobile[key] = {mobile: true}
				errors.mobile[key] = {mobileMsg: "Mobile format is not correct"}
				flag = false
				break
			}
            _mobile.push(mobile[key].mobile)
        }
		if (!flag) {
			_mobile = []
			_landline = []
			this.setState({errors: errors})
			swal("Error", "Some required information is missing!", "error");
			return
		}
		
		_state.eventBasicInfo.contactPersonInfo = {
			landLine: _landline,
			email: email,
			phoneNumber: _mobile
		}
		this.setState(_state, function() {
			this.props.saveEventBasicInfo(this.state.eventBasicInfo)
		})
		
	}
	handleInputChange = (event) => {
		event.preventDefault()
		const {target} = event;
		let _state = {...this.state};
		if(target.name === "title") _state.errors.title = false
		else if (target.name === "description") _state.errors.description = false
		
		_state.eventBasicInfo[target.name] = target.value
		this.setState(_state, function() {
		})
	}
    handleLandLineChange = idx => evt => {
		let _state = {...this.state}

        const _landline = this.state.landLine.map((line, sidx) => {
            if (idx !== sidx) return line;
            return { ...line, landline: evt.target.value };
		});
		
		_state.landLine = _landline
		_state.errors.landline[idx] = {landline: false}
		this.setState(_state);
    };

    handleMobileChange = idx => evt => {
		let _state = {...this.state}
        const _mobile = this.state.mobile.map((mobile, sidx) => {
            if (idx !== sidx) return mobile;
            return { ...mobile, mobile: evt.target.value };
		});
		
		_state.mobile = _mobile
		_state.errors.mobile[idx] = {mobile: false}
		this.setState(_state);
    };

    removeMobile  = idx => () => {
        this.setState({
            mobile: this.state.mobile.filter((s, sidx) => {
                return sidx != idx
            })
        });
    }

    removeLandline = idx => () => {
        this.setState({
            landLine: this.state.landLine.filter((s, sidx) => {
                return sidx != idx
            })
        });
    };

    addLandline = () => {
        let flag = true;
        const { landLine } = this.state
        for (let key in landLine) {
            if (landLine[key].landline === "" || landLine[key].landline === null) {
                flag = false
                break
            }
        }
        if (flag) {
            this.setState({
				landLine: this.state.landLine.concat([{ landline: "" }]),
				landlineError: false
            });
        }
    };

    addMobile = () => {
        let flag = true;
        const { mobile } = this.state
        for (let key in mobile) {
            if (mobile[key].mobile === "" || mobile[key].mobile === null) {
                flag = false
                break
            }
        }
        if (flag) {
            this.setState({
				mobile: this.state.mobile.concat([{ mobile: "" }]),
				mobileError: false,
            });
        }

    }

    setEmailAddress = (event) => {
        this.setState({
			email: event.target.value,
			errors: {...this.state.errors, email: false}
		})
    }
	render() {
		const { eventBasicInfo, errors } = this.state
		const { StandardEvent, Categories } = this.props
		let _options = [];
		if(this.props.Categories.data && this.props.Categories.data.length) {
			this.props.Categories.data.map(category => {
				return (
					_options.push({
						'value': category._id,
						'label': category.name
					})
				)
			})
		}
		return (
			<div className="step">
				{Categories.loading || StandardEvent.loading || this.props.loader? <Loader/> : null}
				<div className="row">
				<Col md={{size:6, offset: 3}} className="mt-5	">
					
						<div className="">
							<h4>Basic information</h4>
							<small>Event Overview and general information</small>
							<div className="form-group content form-block-holder mt-2">
									<label className="control-label ">
										Title <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
									</label>
									<div >
										<input
										className='form-control'
											autoComplete="off"
											type="text"
											placeholder="Event Title"
											required
											disabled = {this.props.StandardEvent.eventSaved}
											name = "title"
											onChange = {this.handleInputChange}
											defaultValue={eventBasicInfo.title} />
									</div>
									<div>{errors.title ? <p style = {styles.error}>{errors.titleMsg}</p>: null }</div>
								</div>
								<div className="form-group content form-block-holder">
									<label className="control-label">
										Currecy
									</label>
									<div>
										<select
										className='form-control'
											autoComplete="off"
											required
											name = "currency"
											disabled = {this.props.StandardEvent.eventSaved}
											value = {eventBasicInfo.currency}
											onChange = {this.handleInputChange}
											defaultValue={eventBasicInfo.currency}
											onBlur={this.validationCheck}>
											<option value="$">USD</option>
											<option value="PKR">PKR</option>
											<option value="INR">INR</option>
										</select>
									</div>
								</div>
								<div className="form-group content form-block-holder">
									<label className="control-label ">
										Category <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
									</label>
									<div >
										<Select
											isDisabled = {this.props.StandardEvent.eventSaved}
											value={this.state.events}
											onChange={this.handleDropDownChange}
											options={_options}
											autoFocus = {true}
											isMulti = {true}
											className="height-auto"
										/>
									</div>
									<div>{errors.category ? <p style = {styles.error}>{errors.categoryMsg}</p>: null }</div>
								</div>
								<div className="form-group content form-block-holder">
									<label className="control-label ">
										Descrption <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
									</label>
									<div >
										<textarea
										disabled = {this.props.StandardEvent.eventSaved}
										className='form-control'
											placeholder="Event Description"
											required
											onChange = {this.handleInputChange}
											name = "description"
											value={eventBasicInfo.description}
											onBlur={this.validationCheck} />
									</div>
									<div>{errors.description ? <p style = {styles.error}>Event Description is required</p>: null }</div>
								</div>
								<div className="form-group content form-block-holder tags-container">
									<label className="control-label ">
										Tags
									</label>
									<div >
										<TagsInput
										className='height-auto form-control'
											value={this.state.eventBasicInfo.tags}
											disabled = {this.props.StandardEvent.eventSaved}
											preventSubmit = {true}
											name = "tags"
											onChange={this.handleRegularTagsChange}
											tagProps={{removeKeys:"x", className: "tags-bg react-tagsinput-tag bg-info text-white rounded", }}
										/>
									</div>
								</div>
										<Contact email = {this.state.email} onChange = {this.setEmailAddress}
										landLine = {this.state.landLine} handleLandLineChange = {this.handleLandLineChange}
										removeLandline = {this.removeLandline}
										addLandline = {this.addLandline}
										mobile = {this.state.mobile}
										handleMobileChange = {this.handleMobileChange}
										removeMobile = {this.removeMobile}
										isSaved = {this.props.StandardEvent.eventSaved}
										errors = {this.state.errors}
										addMobile = {this.addMobile}
										/>
								<div className="row mt-1">
									<div className="col-md-4">
										<Button onClick={this.saveEventBasicInfo} disabled = {this.props.StandardEvent.eventSaved || this.props.StandardEvent.loading}
										className='btn-lg' style={{padding: '10px 30px'}}>
											{this.state.isSave ? 'Event Saved': this.props.StandardEvent.loading ? 'Saving': 'Save'}
										</Button>
									</div>

								</div>
						</div>
						
						</Col>

				</div>
                
			</div>
		)
	}
}
const styles = {
	error: {
		marginTop: '10px',
		fontSize: '10px',
		color: 'red',
		fontWeight: 'bold'
	}
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, { getAllCategories, saveEventBasicInfo })(BasicInfo);
