import React from 'react'
import { Row, Col, Input, FormText} from 'reactstrap'
import { checkPropTypes } from 'prop-types';
const ListTicketClasses = ({isSaved, ticketClasses,ticketClassesCount, handleTicketChange}) => {
    return (
        ticketClasses ? ticketClasses.map((share, id) => {
            return (
                <div key={id}>
                    
                <Row key={id} style={styles.margin}>
                    <Col md="3">
                        <Input type="text" value={share.ticketClassName} name="ticketClassName" 
                            disabled={true} />
                    </Col>
                    <Col md="3">
                        <Input type="number" value={share.availableTickets} name="availableTickets"
                            min = "1"
                            value = {ticketClassesCount[share.ticketClassId]}
                            disabled={true}/>

                    </Col>
                    <Col md="3">
                        <Input type="number" value={share.ticketClassPrice} name="ticketClassPrice"
                            min = "1"
                            disabled={isSaved} onChange = {handleTicketChange(id)}/>
                    </Col>
                    <Col md="2">
                        <Input type="text"  name="ticketClassColor"
                            disabled={true}  style = {{borderRadius: 18, padding: 15, width: 15, height: 15, 
                            backgroundColor: share.ticketClassColor}}/>
                    </Col>
        
                </Row>
                </div>
                
            )
        }): <p>Add Ticket classes first</p>
    )
    
}

function ticketListJSX(ticketClasses) {
        

}
const styles = {
    margin: {
        marginTop: 10,
        marginBottom: 10
	},
}

export default ListTicketClasses
