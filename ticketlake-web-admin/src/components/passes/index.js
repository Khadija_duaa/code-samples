import React, { Component } from 'react'
import {Row, Col, Label, FormGroup, Input, Card, CardBody,FormText, Button} from 'reactstrap'
import {connect} from 'react-redux'
import Select from 'react-select';
import { createPassConfig,resetState,getEventSlots} from '../../redux/actions/passes'
import { NotificationManager } from 'react-notifications';

class Pass  extends Component{
    constructor(props) {
        super(props)
        this.state = {
            "parentEventId": "",
            passConfigs: {
                "passTitle": "",
                "passPrice": 0,
                "availablePassCount": 0,
                "ticketClassId": "",
                "eventSlotIds": [],
                "passId": ""
            },
            events: [],
            isPassSaved: false,
            errors: {},
            ticketClasses: []
        }
    }
    componentDidMount() {
        window.scrollTo(0, 0)
        if (this.props.StandardEvent.ticketClasses.length > 0) {
            this.setState({
                ticketClasses: this.props.StandardEvent.ticketClasses,
                passConfigs: {...this.state.passConfigs, ticketClassId: this.props.StandardEvent.ticketClasses[0]._id}
            })
        }
        this.props.resetState()
        this.setState({
            parentEventId: this.props.StandardEvent.eventId
        }, () => {
            this.props.getEventSlots(this.state.parentEventId, true)
        })
        if (this.props.eventData) {
            this.setState({
                parentEventId: this.props.eventData.parentEventId,
                ticketClasses: this.props.eventData.parentEventInfo.ticketClassesConfig,
                passConfigs: {...this.state.passConfigs, ticketClassId: this.props.eventData.parentEventInfo.ticketClassesConfig[0]._id}
            })
        }
        if (this.props.passData) {
            this.setState({
                passConfigs: {...this.state.passConfigs, 
                    ticketClassId: this.props.passData.ticketClassId,
                    passTitle: this.props.passData.passTitle,
                    availablePassCount: this.props.passData.availablePassCount,
                    passPrice: this.props.passData.passPrice,
                    passId: this.props.passData._id
                }
            })
        }
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.PassConfiguration.saved && nextProps.PassConfiguration.id === this.props.id) {
                NotificationManager.success("Pass configured successfully", "Success")
            this.setState({isPassSaved: true}, () => {
                this.props.resetState()
                this.props.getEventSlots(this.state.parentEventId, true)
            })
        }
        if(nextProps.PassConfiguration.error && nextProps.PassConfiguration.id === this.props.id) {
                NotificationManager.error(nextProps.PassConfiguration.data, "Error")
                this.props.resetState()
                this.props.getEventSlots(this.state.parentEventId, true)
        }

    }
    handleInputChange = ({target}) => {
        const value = target.value
        const name = target.name
        const _state = {...this.state}
        _state.passConfigs[name] = value
        _state.errors[name] = false
        this.setState(_state)
    }
    handleDropDownChange = (events) => {
        const _state = {...this.state}
        _state.events = events
        if(events.length > 0) {
            let _eventIds = []
            events.forEach(event => {
                _eventIds.push(event.value)
            })
            _state.passConfigs.eventSlotIds = _eventIds
        } else _state.passConfigs.eventSlotIds = []
        _state.errors.eventSlotIds = false
        this.setState (_state)
        
    }
    validateInput = () => {
        const _state = {...this.state}
        const {passConfigs, errors } = _state
        let isValidated = true
        if(passConfigs.passTitle === "") {
            isValidated = false
            errors.passTitle = true
            errors.passTitleMsg = "Pass Title Cannot be empty"
        }
        if (passConfigs.availablePassCount < 1) {
            isValidated = false
            errors.availablePassCount = true
            errors.availablePassCountMsg = "Available Passes cannot be 0"
        }
        if (passConfigs.eventSlotIds.length < 1) {
            isValidated = false
            errors.eventSlotIds = true
            errors.eventSlotIdsMsg = "Select atleast one event slot"
        }
        if (passConfigs.ticketClassId === "") {
            isValidated = false
            errors.ticketClassId = true
            errors.ticketClassIdMsg = "Select Ticket Class"
        }
        if (passConfigs.passPrice < 1) {
            isValidated = false
            errors.passPrice = true
            errors.passPriceMsg = "Pass Price cannot be 0"
        }
        if (!isValidated) this.setState({errors: errors})
        return isValidated
    }
    savePassConfig = () => {
        if(!this.validateInput()) return
        let passes = {
            parentEventId: this.state.parentEventId,
            passConfigs: this.state.passConfigs
        }
        if(this.state.passConfigs.passId && this.state.passConfigs.passId !== "")
            passes.passConfigs._id = this.state.passConfigs.passId
        
        this.props.createPassConfig(passes, this.props.id) 
        
    }
    render() {
        const {errors, passConfigs} = this.state
        return (
            <Row>
                <Col md={{ size: 12 }}>
                    <Card>
                        <CardBody>
                            <FormGroup>
                                <Label>Name *</Label>
                                <Input type="text" name="passTitle" onChange = {this.handleInputChange} disabled = {this.state.isPassSaved} value = {passConfigs.passTitle}/>
                                <FormText>
                                    {errors.passTitle ? <p style = {styles.error}>{errors.passTitleMsg}</p>: null}
                                </FormText>
                            </FormGroup>
                            <FormGroup>
                                <Label>Available Passes Count *</Label>
                                <Input type="number" name="availablePassCount" value = {passConfigs.availablePassCount} onChange = {this.handleInputChange} disabled = {this.state.isPassSaved} min = {0}/>
                                <FormText>
                                    {errors.availablePassCount ? <p style = {styles.error}>{errors.availablePassCountMsg}</p>: null}
                                </FormText>
                            </FormGroup>
                            <FormGroup>
                                <Label>Select Slot</Label>
                                <Select
                                    isDisabled = {this.state.isPassSaved}
                                    value={this.state.events}
                                    onChange={this.handleDropDownChange}
                                    options={this.props.PassConfiguration.slots}
                                    autoFocus={true}
                                    isMulti={true}
                                />
                                <FormText>
                                    {errors.eventSlotIds ? <p style = {styles.error}>{errors.eventSlotIdsMsg}</p>: null}
                                </FormText>
                            </FormGroup>
                            <FormGroup>
                                <Label>Class *</Label>
                                <Input type="select" name="ticketClassId" onChange = {this.handleInputChange} 
                                    disabled = {this.state.isPassSaved} value = {passConfigs.ticketClassId}>
                                    {
                                        this.state.ticketClasses.length > 0 ? 
                                        this.state.ticketClasses.map(ticket => {
                                            if (ticket.ticketClassType === 'PASS') {
                                                return (
                                                    <option value = {ticket._id}>{ticket.ticketClassName}</option>
                                                )
                                            }
                                        }): null
                                    }
                                </Input>
                                <FormText>
                                    {errors.ticketClassId ? <p style = {styles.error}>{errors.ticketClassIdMsg}</p>: null}
                                </FormText>
                            </FormGroup>
                            <FormGroup>
                                <Label>Pass Price *</Label>
                                <Input type="text" name="passPrice" value = {passConfigs.passPrice} onChange = {this.handleInputChange} disabled = {this.state.isPassSaved} />
                                <FormText>
                                    {errors.passPrice ? <p style = {styles.error}>{errors.passPriceMsg}</p>: null}
                                </FormText>
                            </FormGroup>
                            <FormGroup>
                                <Button onClick = {this.savePassConfig} disabled = {this.state.isPassSaved}>Save Pass</Button>
                            </FormGroup>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        )
    }

}

const mapStateToProps = (state) => {
    return state
}
const styles = {
    error: {
        fontWeight: 'bold',
        color: 'red'
    }
}
export default connect(mapStateToProps, {createPassConfig,resetState,getEventSlots})(Pass)