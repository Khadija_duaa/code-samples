
import React, { Component } from 'react';
import {
    Card, CardBody, FormGroup, Input, Label, Row, Button, Col,
    Modal, ModalBody, ModalHeader, ModalFooter, FormText
} from 'reactstrap';
import UserIcon from '../../assets/images/user_icon.png'
import DropImage from '../imageUpload'
import { uploadImage } from '../../redux/actions/images'
import { connect } from 'react-redux'
import swal from 'sweetalert'
import Loader from '../loader'
import uuid from "uuid";
import { post } from 'axios'
import { BASE_URL, XAUTH } from '../../utils/CONSTANTS';
class EventSpeakers extends Component {
    constructor(props) {
        super(props)
        this.state = {
            speakers: [
                {
                    "sectionLabel": "",
                    "sectionId": "", // for same sectionLabel, it should be same, for different sectionLabel, it should be unique 
                    "celebrityName": "",
                    "celebrityTitle": "",
                    "celebrityDescription": "",
                    "celebrityImageKey": {
                        "imageKey": "",
                        "imageUrl": ""
                    },
                    "links": {
                        "linkedin": "",
                        "fb": "",
                        "twitter": "",
                        "instagram": "",
                        "google": "",
                        "other": ""
                    },
                    "filePreview": ""
                }],
            sectionLabel: "",
            uniqueSections: [],
            imageUploading: false,
            errors: {
                sectionId: [{ sectionId: false, sectionIdMsg: "" }],
                celebrityName: [{ celebrityName: true, celebrityNameMsg: "" }],
                celebrityTitle: [{ celebrityTitle: true, celebrityTitleMsg: "" }],
                celebrityDescription: [{ celebrityDescription: true, celebrityDescriptionMsg: "" }],
                celebrityImageKey: [{ celebrityImageKey: true, celebrityImageKeyMsg: "" }]
            }
        }
    }
    handleImageDrop = idx => file => {
        let _state = { ...this.state }
        const URL = `${BASE_URL}/events/image-upload`
        let formData = new FormData();
        formData.append('image', file[0]);
        this.setState({ imageUploading: true })
        return post(URL, formData, {
            headers: {
                'Content-Type': 'application/json',
                'X-AUTH': XAUTH
            }
        }).then(image => {
            _state.speakers[idx].celebrityImageKey = image.data
            _state.imageUploading = false
            this.setState(_state)
        }).catch(err => {
        })
    }
    addAnotherSpeaker = () => {
        const { speakers } = this.state
        let flag = false
        for (let key in speakers) {
            if (speakers[key].celebrityName === ""
                || speakers[key].celebrityDescription === "" || speakers[key].celebrityTitle === "") {
                flag = true
                break;
            }
        }
        if (flag) {
            swal("Error", "Please fill data to add more", "error")
            return
        }
        let _state = { ...this.state }
        let _speaker = {
            celebrityName: "", celebrityDescription: "", celebrityTitle: "",
            "sectionLabel": "speaker", "sectionId": "1234", "celebrityImageKey": {
                "imageKey": "",
                "imageUrl": ""
            },
            "links": {
                "linkedin": "",
                "fb": "",
                "twitter": "",
                "instagram": "",
                "google": "",
                "other": ""
            },
        }
        _state.speakers = speakers.concat([_speaker])
        this.setState(_state);
    };
    handleSpeakerChange = idx => evt => {
        const newSpeaker = this.state.speakers.map((speaker, sidx) => {
            if (idx !== sidx) return speaker;
            return { ...speaker, [evt.target.name]: evt.target.value };
        });
        let _state = { ...this.state }
        _state.speakers = newSpeaker
        _state.errors[evt.target.name][idx] = false

        this.setState(_state);
    };
    handleLinks = idx => evt => {
        let _speakers  = {...this.state}
        const newSpeaker = _speakers.speakers.map((speaker, sidx) => {
            if (idx !== sidx) return speaker;
            speaker.links[evt.target.name] = evt.target.value
            return speaker
                        
        });
        let _state = { ...this.state }
        _state.speakers = newSpeaker

        this.setState(_state);

    }
    componentWillReceiveProps(nextProps) {
        const { Images } = nextProps;
        if (typeof Images.bannerImage.imageKey != "undefined") {
            this.setState({
                images: this.state.images.concat([Images.bannerImage])
            })
        }

    }
    sendSpeakers = () => {
        let _state = { ...this.state };
        let flag = true
        if (this.state.sectionLabel === "" || this.state.sectionLabel === null) {
            _state.errors.section = true
            _state.errors.sectionMsg = "Section label is required"
            flag = false
        }
        _state.speakers.map((speaker, index) => {
            if (speaker.celebrityDescription === "") {
                _state.errors.celebrityDescription[index] = { celebrityDescription: true, celebrityDescriptionMsg: "Description is required" }
                flag = false
            }
            if (speaker.celebrityName === "") {
                _state.errors.celebrityName[index] = { celebrityName: true, celebrityNameMsg: "Name is required" }
                flag = false
            }
            if (speaker.celebrityTitle === "") {
                _state.errors.celebrityTitle[index] = { celebrityTitle: true, celebrityTitleMsg: "Title is required" }
                flag = false
            }
            if (speaker.celebrityImageKey.imageUrl === "") {
                _state.errors.celebrityImageKey[index] = { celebrityImage: true, celebrityImageMsg: "Image is require" }
                flag = false
            }
            speaker.sectionLabel = _state.sectionLabel
        })
        this.setState(_state)
        if (!flag) return
        this.props.saveSpeakers(this.state.speakers)
    }
    setSectionLabel = (event) => {
        let _state = { ...this.state };
        _state.sectionLabel = event.target.value
        _state.errors.section = false
        this.setState(_state);
    }
    render() {
        const { errors } = this.state
        return (
            <Modal toggle={this.props.toggle} isOpen={this.props.isOpen} className="modal-lg">
                <ModalHeader toggle={this.toggle}><h3>Event Speakers</h3></ModalHeader>
                <ModalBody className="modal-lg">
                    {this.state.imageUploading ? <Loader /> : null}
                    <Row>
                        <Col md="4">
                            <p>
                                Please enter following parameters to add a custom section
                        </p>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4">
                            <FormGroup>
                                <Label>
                                    Section Label  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
                            </Label>
                                <Input type="text" name="sectionLabel" value={this.state.sectionLabel}
                                    onChange={this.setSectionLabel} />
                                <FormText>{errors.section ? <p style={styles.error}>{errors.sectionMsg}</p> : null}</FormText>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        {
                            this.state.speakers.map((speaker, id) => {
                                return (
                                    <>
                                    <Row style={{width:'96%', marginLeft:'10px', marginBottom:'15px'}}>
                                        <Col md="12" className="border  m-1" style={styles.border}>
                                            <FormGroup>
                                                <Label>Section Name  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
                                                <Input type="text" disabled={this.props.isSaved} placeholder="Name" value={speaker.celebrityName} name="celebrityName" onChange={this.handleSpeakerChange(id)} />
                                                <FormText>{typeof errors.celebrityName[id] != "undefined" && errors.celebrityName[id].celebrityName ? <p style={styles.error}>{errors.celebrityName[id].celebrityNameMsg}</p> : null}</FormText>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>Celebrity  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
                                                <Input type="text" disabled={this.props.isSaved} placeholder="Title" value={speaker.celebrityTitle} name="celebrityTitle" onChange={this.handleSpeakerChange(id)} />
                                                <FormText>{typeof errors.celebrityTitle[id] != "undefined" && errors.celebrityTitle[id].celebrityTitle ? <p style={styles.error}>{errors.celebrityTitle[id].celebrityTitleMsg}</p> : null}</FormText>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>Description  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
                                                <Input type="textarea" placeholder="description" value={speaker.celebrityDescription} name="celebrityDescription"
                                                    onChange={this.handleSpeakerChange(id)} />
                                                <FormText>{typeof errors.celebrityDescription[id] != "undefined" && errors.celebrityDescription[id].celebrityDescription ?
                                                    <p style={styles.error}>{errors.celebrityDescription[id].celebrityDescriptionMsg}</p> : null}</FormText>
                                            </FormGroup>
                                            <FormGroup>
                                                <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
                                                <div style={{ display: 'flex'}}>
                                                    <div>
                                                        {speaker.celebrityImageKey.imageUrl != "" ? <img src={speaker.celebrityImageKey.imageUrl} style={{ width: 145, height: 150, marginTop: 28 }} /> : null}
                                                    </div>
                                                    <div>
                                                        <DropImage float='none' images={speaker.speakerImage} multi={false} handleImageDrop={this.handleImageDrop(id)} />
                                                        <FormText>{typeof errors.celebrityImageKey[id] != "undefined" && errors.celebrityImageKey[id].celebrityImageKey ? <p style={styles.error}>{errors.celebrityImageMsg}</p> : null}</FormText>
                                                    </div>
                                                </div>


                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row style={{width:'96%', paddingBottom:'40px', paddingTop:'40px', marginLeft:'10px'}}>
                                    <Col md="2">Links</Col>
                                    <Col md="5">
                                       <Row style = {styles.spacing}>
                                            <Col md="2">
                                                <i className="fab fa-facebook-f"></i>
                                            </Col>
                                            <Col md="10">
                                                <Input type="text" onChange = {this.handleLinks(id)} value = {speaker.links.fb} name = "fb"/>
                                            </Col>
                                        </Row>
                                        <Row style = {styles.spacing}>
                                            <Col md="2">
                                                <i className="fab fa-instagram"></i>
                                            </Col>
                                            <Col md="10">
                                                <Input type="text" onChange = {this.handleLinks(id)} value = {speaker.links.instagram} name = "instagram"/>
                                            </Col>
                                        </Row>
                                        <Row style = {styles.spacing}>
                                            <Col md="2">
                                                <i className="fab fa-linkedin-in"></i>
                                            </Col>
                                            <Col md="10">
                                                <Input type="text" onChange = {this.handleLinks(id)} value = {speaker.links.linkedin} name = "linkedin"/>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col md="5">
                                        <Row style = {styles.spacing}>
                                            <Col md="2">
                                                <i className="fab fa-twitter"></i>
                                            </Col>
                                            <Col md="10">
                                                <Input type="text" onChange = {this.handleLinks(id)} value = {speaker.links.twitter} name = "twitter"/>
                                            </Col>
                                        </Row>
                                        <Row style = {styles.spacing}>
                                            <Col md="2">
                                                <i className="fab fa-google"></i>
                                            </Col>
                                            <Col md="10">
                                                <Input type="text" onChange = {this.handleLinks(id)} value = {speaker.links.google} name = "google"/>
                                            </Col>
                                        </Row>
                                        <Row style = {styles.spacing}>
                                            <Col md="2">
                                                <i className="fas fa-link"></i>
                                            </Col>
                                            <Col md="10">
                                                <Input type="text" onChange = {this.handleLinks(id)} value = {speaker.links.other} name ="other" />
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                                    
                                </>
                                )

                            })
                        }
                        
                                
                          {
                              !this.props.editMode ? <Row style={{width:'96%', marginLeft:'10px'}} className="text-center">
                              <Col md="12" >
                                  <Row style = {styles.spacing}>
                                      <Col>
                                          <img src={UserIcon} style={{ width: 100, height: 100 }} />
                                      </Col>
                                  </Row>
                                  <Row style = {styles.spacing}>
                                      <Col>
                                          <Button disabled={this.state.imageUploading ? true : false}
                                              onClick={this.addAnotherSpeaker}><i className="fas fa-plus"></i>Add another Person</Button>
                                      </Col>
                                  </Row>

                              </Col>
                          </Row>: null
                          }      
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.sendSpeakers}>
                        Save
                        </Button>
                    <Button color="secondary" onClick={this.props.toggle}>
                        Close
                        </Button>
                </ModalFooter>
            </Modal>

        )
    }

}
const styles = {
    border: {
        borderColor: '#d8dcdc',
        border: 'solid 1px',
        borderRadius: '2%',
    },
    spacing: {
        marginTop: 5,
        marginBotton: 5
    },
    error: {
        color: 'red',
        textAlign: 'left',
        fontWeight: 'bold'
    }
}
const mapStateToProps = (state) => {
    return state
}

export default connect(mapStateToProps, { uploadImage })(EventSpeakers)