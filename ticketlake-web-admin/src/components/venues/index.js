import React from 'react';
import {FormGroup, Label, Input, Row, Col} from 'reactstrap'
const venuList = (props) => {
    return (
        <Row>
            <Col>
                <FormGroup>
                    <Label>
                        <h3>Venue</h3>
                    </Label>
                    <Input type="select" onChange = {props.handleVenueChange} name = "venueId"  value = {props.venueId} disabled = {props.isSaved}>
                    {
                        Array.isArray(props.venues) && props.venues.length > 0 ? props.venues.map(venu => {
                            
                            return (
                                <option value={venu._id + " " + venu.seatingCapacity} key={venu._id}>{venu.name}</option>
                            );
                        }) : null
                    }
                    </Input>

                </FormGroup>
                <FormGroup>
                    <Label>
                        Seating Capacity
                    </Label>
                    <Input type = "text" value = {props.totalSeating} disabled = {true}/>
                </FormGroup>
                <FormGroup>
                    <Label>
                        Total Tickets
                    </Label>
                    <Input type = "text" name = "availableSeats" value = {props.availableSeats} 
                        onChange = {props.availableSeatsCallback} disabled = {props.isSaved}/>
                </FormGroup>
            </Col>
        </Row>
    )

}

export default venuList;