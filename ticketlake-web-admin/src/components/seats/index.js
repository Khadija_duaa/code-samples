import React from 'react';
import { Card, CardBody, Col, Row } from 'reactstrap';

const style = {
    display: 'flex',
    flexWrap: 'wrap'
}

const generateSeats = (prop) => {
    let divs = []
    for (let i = 0; i < prop.totalSeats; i++) {
        if (typeof prop.reservedSeats[0][i] === 'undefined') {
            prop.reservedSeats[0].push({
                seatNumber: i,
                purchased: false, 
                ticketClassColor: null,
                ticketClassId: null,
                ticketClassName: null
            })

        }
        divs.push(
            <div className = "text-center" key={i} md="1" onClick={() => !prop.isSaved && prop.handleClick(i)}
                style={{
                    border: '0.5px solid', height: '50px', width: '50px',
                    marginLeft: '5px', marginRight: '5px', marginTop: '5px', marginBottom: '5px',
                    backgroundColor: prop.reservedSeats[0][i] !== null  ? prop.reservedSeats[0][i].ticketClassColor: null
                }}>
                <p className="text-center">{i}</p>
            </div>
        )
    }
    return divs;
}

const Seats = (props) => {
    console.log('Seats ',props)
    return (
        props.totalSeats > 0 ?
        <Col >
        <Card>
            <CardBody>
                <div style = {style}>
                    {generateSeats(props)}
                </div>
            </CardBody>
        </Card>
    </Col>
            : null
    )
}

export default Seats;

