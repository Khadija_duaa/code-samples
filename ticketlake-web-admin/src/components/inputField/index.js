import React from 'react';
import {
    Form,
    FormGroup,
    Input
} from 'reactstrap'

const InputField = (props) => {
    return (
        <Form>
            <FormGroup>
                <Input type="text" defaultValue="Text Here" />
            </FormGroup>
        </Form>
    );
}

export default InputField;