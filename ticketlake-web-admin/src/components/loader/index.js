import React from 'react'
import LoaderGif from '../../assets/images/loader.gif'

const style = {
	loader: {
		position: 'fixed',
		zIndex: 999,
		height: '50px',
		width: '50px',
		overflow: 'visible',
		margin: 'auto',
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		backgroundColor: 'rgba(0,0,0,0.3)'
	}
}
const Loader = () => {
    return (
		<div>
			<img src = {LoaderGif} style = {style.loader} />

		</div>
        
    )
}

export default Loader