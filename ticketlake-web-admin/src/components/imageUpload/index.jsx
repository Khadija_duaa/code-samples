import React from 'react'
import Dropzone from "react-dropzone";
import dragDrop from '../../assets/images/drag-drop.png';
const style = {
    imageDiv: {
		padding: '21px 49px 26px 49px'
	}
}

const ImageUpload = (props) => {
    return (
        <Dropzone
            style={{
                float: props.float || 'left',
                width: "150px",
                height: "150px",
                borderRadius: "2%",
                objectFit: "cover",
                objectPosition: "center",
                border: "0.5px solid",
                borderColor: 'rgb(228, 226, 226)',
                float: 'left',
                background: '#f5f6fa',
                padding: '15px',
                zIndex:2,
            }}
            multiple={false}
            disabled = {props.isSaved}
            accept="image/*"
            onDrop={props.handleImageDrop}
        >
        <div style = {{float: 'left'}}> 
            <img src={dragDrop} />
            {/* // <div style = {style.imageDiv}>
            // <i className="fas fa-upload fa-3x"></i>
            // </div>
            // <div>
            //     Drop or Drag files to upload
            // </div> */}
        </div>
        {/* <div style={{textAlign:'center'}}>1024 x 768</div> */}
        </Dropzone>
    )
}

export default ImageUpload;