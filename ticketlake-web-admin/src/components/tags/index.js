import React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  Row,
  Col
} from 'reactstrap';
import TagsInput from "react-tagsinput";

const Tags = (props) => {
    return (
        <Row>
        <Col md="12">
          <Card>
            <CardTitle className="bg-light border-bottom p-3 mb-0">
              <i className="mdi mdi-tag-multiple mr-2"></i>Tags
            </CardTitle>
            <CardBody className="border-top">
              <TagsInput
                value={this.props.regularTags}
                onChange={this.props.handleRegularTags}
                tagProps={{ className: "react-tagsinput-tag bg-info text-white rounded" }}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
}

export default Tags;