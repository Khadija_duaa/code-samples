import * as actions from './organizations-actions';

const initState = () => {
    return {
        message: null,
        error: null,
        organizations:[],
        venue:null,
    };
};


//----- reducers-----/

const reducer = (state = initState(), action) => {
    let newState = {...state};

    switch (action.type) {

        case actions.ORGANIZATIONS:
            setOrganizations(newState, action.payload);
            break;
        case actions.VENUE:
            setOrganization(newState, action.payload);
            break;
        case actions.RESET_VENUE:
            RESET_VENUE(newState, action.payload);
            break;
        case actions.ADD_VENUE:
            addVenue(newState, action.payload);
            break;
        case actions.DELETE_VENUE:
            return newState
            break;
        default :
            break;
    }

    return newState;
};

const _deleteOrganizations = (state, data) => {
    let newVenues = [...state.organizations];
    newVenues.splice(data.i, 1);
    state.organizations = newVenues;

};

const addVenue = (state, data) => {
    state.message = data.message;
    state.error = data.error;
};


const setOrganization = (state, data) => {
    state.organizations = data;
};

const RESET_VENUE = (state ) => {
    state.organizations = null;
};

const setOrganizations = (state, data) => {
    state.organizations = data;
    state.error = data.error;
    state.message = data.message;
};

export default reducer;
