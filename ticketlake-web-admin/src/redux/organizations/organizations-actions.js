import axios from '../../utils/axios';
export const ORGANIZATIONS = 'ACTION_ORGANIZATIONS';
export const PROCESSING_VENUE = 'ACTION_VENUE_PROCESSING';
export const VENUE = 'ACTION_FETCH_VENUE';
export const DELETE_VENUE = 'ACTION_FETCH_DELETE_VENUE';
export const RESET_VENUE = 'ACTION_RESET_VENUE_DATA';
export const ADD_VENUE = 'ACTION_ADD_VENUE';

//----- actions-----/

export const getAllOrganizations = (id) => {
    console.log('wao id:', id)
    return (dispatch) => {
        axios.get('/organizations/get-organizations')
            .then((response) => {
                let data = response.data.data
                dispatch(getOrganizations(data))
            })
            .catch(err => {
                dispatch(getOrganizations({error: err}));
            });
    };
};

const getOrganizations= (org) => {
    return {
        type: ORGANIZATIONS,
        payload: org,
    }
};

export const setOrganizations = (venue) => {
    return {
        type: VENUE,
        payload: venue,

    }
};

const addVenue = (venue) => {
    return {
        type: ADD_VENUE,
        payload: venue,

    }
};

export const resetRedux = () => {
    return {
        type: RESET_VENUE
    };
};

const _deleteCategory = (index) => {
    return {
        type: DELETE_VENUE,
        payload: index,
    }
};

export const deleteOrganizations = (organization, index, callback, errorcallback) => {
    return (dispatch) => {
        axios.delete(`/organizations/delete-organization/${organization.venueDeleteInfo.organizationId}`)
            .then((response) => {
                dispatch(_deleteCategory({index}));
                callback && callback();
            })
            .catch(err => {
                dispatch(_deleteCategory({error: err}));
                errorcallback && errorcallback();
            });
    };
};

export const setOrganization = (name,description,email,phone,callback,cb,cback,errorcallback) => {
    // return (dispatch,getState) => {
    //     let urlUpdate = `/organizations/update-organization`;
    //     let axiosPromise = null;
    //     let existingId = getState().eventVenues.venue ? getState().eventVenues.venue.venueId : null;
    //     if (existingId) {
    //         axiosPromise = axios.put(urlUpdate,{name,description,email,phone});
    //     }
    //     axiosPromise.then((response) => {
    //         let {data} = response;
    //         dispatch(addVenue({message: data._message}));
    //         callback && callback();
    //         cb && cb();
    //         cback() && cback();
    //     }).catch(err => {
    //         dispatch(addVenue({error: err}));
    //         errorcallback && errorcallback()
    //     });
    // };
};

