import { UPLOADING_IMAGE, IMAGE_STATE_RESET ,GALLERY_IMAGE_UPLOADED, BANNER_IMAGE_UPLOADED, IMAGE_UPLOAD_FAILED  } from '../../actions/images'

const initialState = {
    loading: false,
    message: null,
    bannerImage: '',
    galleryImages: '',
    data: '',
    slotId: '',
    type: ''
}
const imageReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPLOADING_IMAGE:
            return {...state,...{loading:true, galleryImages: '', bannerImage: '', slotId: ''}};
        case IMAGE_STATE_RESET:
            return {...state,...{loading:false, galleryImages: '', bannerImage: '', slotId: ''}};
        case BANNER_IMAGE_UPLOADED:
            return {...state,...{loading:false, bannerImage: action.payload.data, slotId: action.slotId}};
        case GALLERY_IMAGE_UPLOADED:
            return {...state,...{loading:false, 
                galleryImages: action.payload.data, slotId: action.slotId}};
        case IMAGE_UPLOAD_FAILED:
            return {...state,...{loading:false, data: action.payload, slotId: action.slotId}};
        default:
            return state;
    }
}

export default imageReducer

  