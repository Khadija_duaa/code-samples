import { 
    RESET_PUBLISH_STATE, EVENT_PUBLISHED, EVENT_PUBLISHING, ERROR_EVENT_PUBLISH
 } from '../../actions/events/eventActions'
const initialState = {
    loading: false,
    message: null,
    data: null,
    published: false,
    error: false,
    errors: []
}
const PublishEvent = (state = initialState, action) => {
    switch (action.type) {
        case EVENT_PUBLISHING:
            return {...state,...{loading:true, error: false, published: false}};
        
        case EVENT_PUBLISHED: 
            return {...state,...{loading:false, error: false, published: true, data: action.payload}};

        case ERROR_EVENT_PUBLISH: 
            return {...state,...{loading:false, error: true, published: false, errors: action.payload}};

        case RESET_PUBLISH_STATE:
            return {...state,...{loading:false, error: false, published: false, data: null, errors: []}};
        default:
            return state;
    }
}

export default PublishEvent

  