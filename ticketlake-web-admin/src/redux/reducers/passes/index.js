import { CREATING_PASS, PASS_CREATED, ERROR_PASS_CREATION, FETCHING_EVENT_SLOT,
    RESET_PASS_CONFIG, ERROR_FETCHING_SLOT, EVENT_SLOTS_FETCHED, ALL_EVENT_SLOTS_FETCHED, FETCHING_ALL_EVENT_SLOT, ERROR_FETCHING_ALL_SLOT  } from '../../actions/passes/index'

const initialState = {
    loading: false,
    saved: false,
    error: false,
    data: [],
    slots: [],
    passData: [],
    eventSlots: [],
    id: ''
}
const PassConfiguration = (state = initialState, action) => {
    switch (action.type) {
        case CREATING_PASS:
            return {...state,...{loading:true, saved: false, error: false, id: action.id}};
        case PASS_CREATED:
            return {...state,...{loading:false, saved: true, error: false, passData: action.payload, id: action.id}};
        case ERROR_PASS_CREATION:
            return {...state,...{loading:false, passData: action.payload, saved: false, error: true, id: action.id}};
            case FETCHING_EVENT_SLOT:
            return {...state,...{loading:true, saved: false, error: false, id: action.id}};
        case EVENT_SLOTS_FETCHED:
            return {...state,...{loading:false, error: false, slots: action.payload}};
        case ERROR_FETCHING_SLOT:
            return {...state,...{loading:false, data: action.payload, saved: false, error: true, id: action.id}};
        case RESET_PASS_CONFIG: 
            return {...state,...{loading:false, saved: false, error: false, id: '', passData: []}};
        case FETCHING_ALL_EVENT_SLOT: 
            return {...state,...{loading:true, saved: false, error: false}};
        case ALL_EVENT_SLOTS_FETCHED: 
            return {...state,...{loading:false, error: false, eventSlots: action.payload}};
        case ERROR_FETCHING_ALL_SLOT: 
            return {...state,...{loading:false, error: true, eventSlots: action.payload}};
        default:
            return state;
    }
}

export default PassConfiguration

  