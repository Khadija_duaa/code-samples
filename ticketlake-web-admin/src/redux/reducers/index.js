import { combineReducers } from 'redux';
import Categories from './categories'
import Venues from './venues'
import Images from './images'
import StandardEvent from './event'
import Users from './users'
import Events from './events'
import Admins from './admins'
import Tickets from './tickets'
import EventSlot from './eventSlots'
import eventCategories from './eventCategory'
import eventVenues from './eventVenues'
import userReducer from '../user/user-reducer';
import organizations from '../organizations/organizations-reducer';
import PassConfiguration from './passes'
import RecurringEvent from './recurrEvent'
import ParentEventsList from './ParentEvent'
import CancelEvent from './eventCancel'
import Refunds from './refunds'
import DeleteEvent from './eventDelete'
import PublishEvent from './eventPublish'
export default combineReducers({
    Categories,
    Venues,
    StandardEvent,
    Images,
    Users: Users,
    Events: Events,
    Admins: Admins,
    Tickets: Tickets,
    EventSlot,
    eventCategories:eventCategories,
    eventVenues:eventVenues,
    userReducer,
    organizations,
    PassConfiguration,
    RecurringEvent,
    ParentEventsList,
    Refunds,
    CancelEvent,
    DeleteEvent,
    PublishEvent
});
