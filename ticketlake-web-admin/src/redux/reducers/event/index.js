import {
    EVENT_SAVING, EVENT_CREATION_FAILED, EVENT_SAVED, EVENT_CONTACT_CREATION_FAILED,
    EVENT_CONTACT_SAVED, EVENT_CONTACT_SAVING, EVENT_TICKETS_SAVED,
    EVENT_TICKETS_CREATION_FAILED, EVENT_TICKETS_SAVING, RESET_EVENT_BASIC_INFO
} from '../../actions/events'

const initialState = {
    eventId: '',
    eventUUID: '',
    eventSaved: false,
    ticketClassesSaved: false,
    contactSaved: false,
    loading: false,
    error: false,
    data: [],
    ticketClasses: [],
    contactData: [],
    errors: [],
    message: null
}
const eventReducer = (state = initialState, action) => {
    switch (action.type) {
        case EVENT_SAVING:
            return { ...state, ...{ loading: true, error: false, errors: [] } };
        case EVENT_SAVED:
            return {
                ...state, ...{
                    loading: false, eventSaved: true, eventId: action.payload._id, errors: [],
                    eventUUID: action.payload.parentEvent, ticketClassesSaved: false, error: false
                }
            };
        case RESET_EVENT_BASIC_INFO: 
        return {
            ...state, ...{
                loading: false, eventSaved: false, eventId: '',
                eventUUID: '', ticketClassesSaved: false, error: false, errors: []
            }
        }

        case EVENT_CREATION_FAILED:
            return { ...state, ...{ loading: false, errors: action.payload, error: true } };
        case EVENT_CONTACT_SAVING:
            return { ...state, ...{ loading: true,error: false } };
        case EVENT_CONTACT_SAVED:
            return { ...state, ...{ loading: false, contactSaved: true, contactData: action.payload,error: false } };
        case EVENT_CONTACT_CREATION_FAILED:
            return { ...state, ...{ loading: false, data: action.payload,error: false } };
        case EVENT_TICKETS_SAVING:
            return { ...state, ...{ loading: true,error: false } };
        case EVENT_TICKETS_SAVED:
            return { ...state, ...{ loading: false, ticketClassesSaved: true, ticketClasses: action.payload.ticketClassesConfig,error: false } };
        case EVENT_TICKETS_CREATION_FAILED:
            return { ...state, ...{ loading: false, data: null, error: true, errors: action.payload } };
        default:
            return state;
    }
}



export default eventReducer

