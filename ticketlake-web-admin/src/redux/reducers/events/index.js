import * as actions from '../../actions/events/index';

const initState = {
    event: null,
    _events:[],
    allEvents: [],
    message: null,
    error: null,
    processing: true,
    ticketClass:[],

};


const events = (state = initState, action) => {

    let newState = {...state};

    switch (action.type) {
        case actions.ALL_EVENTS:
            setAllEvents(newState, action.payload);
            break;
        case actions.PROCESSING:
            setProcessing(newState, action.payload);
            break;
        case actions.EVENT:
            setEvent(newState, action.payload);
            break;
        case actions.EVENT_DETAIL:
            setEventDetail(newState, action.payload);
            break;
        default : {
            // do nothing
        }
    }
    return newState;

};

const setEvent = (state, data) => {
    state.event = data;
};
const setEventDetail = (state, data) => {
    state.ticketClass = data.ticketClass;
};

const setAllEvents = (state, data) => {
    state._events = data._events;
    state.allEvents = data.allEvents;
    state.error = data._error;
};


const setProcessing = (state, processing) => {
    state.processing = processing;
};


export default events;