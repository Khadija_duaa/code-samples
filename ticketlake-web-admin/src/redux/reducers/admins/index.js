import * as actions from '../../actions/admins/index';

const initState = () => {
    return {
        processing: false,
        message: null,
        error: null,
        admin:null,
        admins:[],
        roles:[],

    };
};

const user = (state = initState(), action) => {
    let newState = {...state};

    switch (action.type) {

        case actions.PROCESSING_ADMIN:
            setProcessing(newState, action.payload);
            break;
        case actions.ADMINS:
            setAdmins(newState, action.payload);
            break;
        case actions.ROLES:
            setRoles(newState, action.payload);
            break;
        case actions.ADD_ADMIN:
            addAdmin(newState, action.payload);
            break;
        case actions.DELETE_ADMIN:
            _deleteAdmin(newState, action.payload);
            break;
        case actions.ADMIN:
            setAdmin(newState, action.payload);
            break;
        case actions.RESET_ADMIN:
            RESET_ADMIN(newState, action.payload);
            break;
        default :
            break;
    }

    return newState;
};

const setAdmin = (state, data) => {
    state.admin = data;
};

const _deleteAdmin = (state, data) => {
    // let newAdmins = [...state.admins];
    // newAdmins.splice(data.index, 1);
    // state.admins = newAdmins;
    state.message= data.message;
    state.error =data.error;
};

const setAdmins = (state, data) => {
    state.admins = data;
    state.error = data.error;
};

const addAdmin = (state, data) => {
    state.message = data.message;
    state.error = data.error;
};

const setRoles = (state, data) => {
    state.roles = data;
    state.error = data.error;
};

const RESET_ADMIN = (state ) => {
    state.admin = null;
};


const setProcessing = (state, processing) => {
    state.processing = processing;
};

export default user;

