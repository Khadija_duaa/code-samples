import {  FETCHING_CATEGORIES, FETCHING_ERROR, CATEGORIES_FETCHED } from '../../actions/categories'

const initialState = {
    loading: false,
    data: [],
    message: null,
    processing: false,
    error: null,
    category:null,
    categories:[],
};
const categoriesReducer = (state = initialState, action) => {
    let newState = {...state};

    switch (action.type) {
        case FETCHING_CATEGORIES:
            return {...state,...{loading:false}};
        case CATEGORIES_FETCHED:
            return {...state,...{loading:false, data: action.payload.data}};
        case FETCHING_ERROR:
            return {...state,...{loading:false, data: action.payload.data}};

      default:
        return state;
    }
}



export default categoriesReducer

  