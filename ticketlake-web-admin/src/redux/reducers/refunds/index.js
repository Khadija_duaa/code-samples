import {REFUND_PROCESSED, PROCESSING_REFUND, ERROR_PROCESSING_REFUND, RESET_REFUD} from '../../actions/tickets'
const initialState = {
    loading: false,
    message: null,
    data: null,
    error: false,
    saved: false
}
const Refunds = (state = initialState, action) => {
    switch (action.type) {
        case PROCESSING_REFUND:
            return {...state,...{loading:true, error: false, saved: false}};
        case REFUND_PROCESSED:
            return {...state,...{loading:false,saved: true, data: action.payload, error: false}};
        case ERROR_PROCESSING_REFUND:
            return {...state,...{loading:false, data: action.payload, saved: false, error: true}};  
            case RESET_REFUD:
            return {...state,...{loading:false, data: null, saved: false, error: false}};           
        default:
            return state;
    }
}

export default Refunds

  