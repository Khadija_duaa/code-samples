import {  FETCHING_VENUES, FETCHING_ERROR, VENUES_FETCHED } from '../../actions/venues'

const initialState = {
    loading: true,
    data: [],
    message: null
}
const venuesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCHING_VENUES:
            return {...state,...{loading:false}};
        case VENUES_FETCHED:
            return {...state,...{loading:false, data: action.payload.data}};
        case FETCHING_ERROR:
            return {...state,...{loading:false, data: action.payload}};
      default:
        return state;
    }
}

export default venuesReducer

  