import * as actions from '../../actions/eventVenues/index';

const initState = () => {
    return {
        message: null,
        error: null,
        venues:[],
        venue:null,
    };
};

const reducer = (state = initState(), action) => {
    let newState = {...state};

    switch (action.type) {

        case actions.VENUES:
            setVenues(newState, action.payload);
            break;
        case actions.VENUE:
            setVenue(newState, action.payload);
            break;
        case actions.RESET_VENUE:
            RESET_VENUE(newState, action.payload);
            break;
        case actions.ADD_VENUE:
            addVenue(newState, action.payload);
            break;
        case actions.DELETE_VENUE:
            _deleteVenue(newState, action.payload);
            break;
        default :
            break;
    }

    return newState;
};

const _deleteVenue = (state, data) => {
    let newVenues = [...state.venues];
    newVenues.splice(data.i, 1);
    state.venues =newVenues;

};

const addVenue = (state, data) => {
    state.message = data.message;
    state.error = data.error;
};


const setVenue = (state, data) => {
    state.venue = data;
};

const RESET_VENUE = (state ) => {
    state.venue = null;
};

const setVenues = (state, data) => {
    state.venues = data;
    state.error = data.error;
    state.message = data.message;
};

export default reducer;
