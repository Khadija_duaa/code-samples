import { FETCHING_ALL_PARENT_EVENTS, PARENTS_EVENT_FETCHED, ERROR_FETCHING_PARENT_EVENTS } from '../../actions/events'

const initialState = {
    loading: false,
    error: false,
    data: []
}
const parentsEventList = (state = initialState, action) => {
    switch (action.type) {
        case FETCHING_ALL_PARENT_EVENTS:
            return {...state,...{loading:true, error: false}};
        case PARENTS_EVENT_FETCHED:
            return {...state,...{loading:false, error: false, data: action.payload}};
        case ERROR_FETCHING_PARENT_EVENTS:
            return {...state,...{loading:false, error: true, data: action.payload}};
        default:
            return state;
    }
}

export default parentsEventList

  