import { CANCEL_EVENT_STARTED, EVENT_CANCELLED, EVENT_CANCEL_FAILED, RESET_CANCEL_EVENT } from '../../actions/events'
const initialState = {
    loading: false,
    message: null,
    data: null,
    canceled: false,
    error: false,
    errors: []
}
const CancelEvent = (state = initialState, action) => {
    switch (action.type) {
        case CANCEL_EVENT_STARTED:
            return {...state,...{loading:true, error: false, canceled: false}};
        
        case EVENT_CANCELLED: 
            return {...state,...{loading:false, error: false, canceled: true}};

        case EVENT_CANCEL_FAILED: 
            return {...state,...{loading:false, error: true, canceled: false, errors: action.payload}};

        case RESET_CANCEL_EVENT:
            return {...state,...{loading:false, error: false, canceled: false}};
        default:
            return state;
    }
}

export default CancelEvent

  