import {SAVING_TICKET, ERROR_TICKET_SAVING, TICKET_SAVED, FETCHING_PURCHASED_TICKET,
ERROR_FETCHING_PURCHASED_TICKET, PURCHASED_TICKET_FETCHED, RESET_TICKET_STATE} from '../../actions/tickets';

const initState = () => {
    return {
        errorMsg:null,
        data: null,
        loading: false,
        error: false,
        success: false,
        purchasedTickets: [],
        fetchError: false,
        fetching: false,
        errors: []
    };
};

const ticket = (state = initState(), action) => {
    let newState = {...state};

    switch (action.type) {

        case SAVING_TICKET:
            return { ...state, ...{ loading: true, error: false, success: false } };
        case TICKET_SAVED:
            return { ...state, ...{ loading: false, data: action.payload, error: false, success: true } };
        case ERROR_TICKET_SAVING:
            return { ...state, ...{ loading: false, data: action.payload, error: true, success: false } };
        case FETCHING_PURCHASED_TICKET:
            return {...state, ...{fetching: true, fetchError: false}}
        case PURCHASED_TICKET_FETCHED: 
            return {...state, ...{fetching: false, fetchError: false, purchasedTickets: action.payload.docs}}
        case ERROR_FETCHING_PURCHASED_TICKET: 
            return {...state, ...{fetching: false, fetchError: true, errors: action.payload}}
        case RESET_TICKET_STATE: 
            return {...state, ...{loading: false, error: false, success: false, errors: [], data: null}}
        default :
            return newState;
    }
};



export default ticket;

