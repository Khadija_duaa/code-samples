
import * as actions from '../../actions/eventCategory/index';

const initState = () => {
    return {
        processing: false,
        message: null,
        error: null,
        category:null,
        categories:[],

    };
};

const category = (state = initState(), action) => {
    let newState = {...state};

    switch (action.type) {

        case actions.PROCESSING_CATEGORY:
            setProcessing(newState, action.payload);
            break;
        case actions.CATEGORIES:
            setCategories(newState, action.payload);
            break;
        case actions.ADD_CATEGORY:
            addCategory(newState, action.payload);
            break;
        case actions.CATEGORY:
            setCategory(newState, action.payload);
            break;
        case actions.DELETE_CATEGORY:
            _deleteCategory(newState, action.payload);
            break;
        case actions.RESET_CATEGORY:
            RESET_CATEGORY(newState, action.payload);
            break;
        default :
            break;
    }
    return newState;
};

const setCategory = (state, data) => {
    state.category = data;
};

const _deleteCategory = (state, data) => {
    let newCategories = [...state.categories];
    newCategories.splice(data.index, 1);
    state.categories =newCategories;
    state.message= data.message;
    state.error =data.error;

};

const setCategories = (state, data) => {
    state.categories = data;
    state.error = data.error;
};

const addCategory = (state, data) => {
    state.message = data.message;
    state.error = data.error;
};



const RESET_CATEGORY = (state ) => {
    state.category = null;
};


const setProcessing = (state, processing) => {
    state.processing = processing;
};

export default category;



  