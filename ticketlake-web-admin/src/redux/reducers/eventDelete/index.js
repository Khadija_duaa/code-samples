import { DELETING_EVENT, EVENT_DELETED, ERROR_DELETING_EVENT, RESET_DELETE } from '../../actions/events/eventActions'
const initialState = {
    loading: false,
    message: null,
    data: null,
    deleted: false,
    error: false,
    errors: []
}
const CancelEvent = (state = initialState, action) => {
    switch (action.type) {
        case DELETING_EVENT:
            return {...state,...{loading:true, error: false, deleted: false}};
        
        case EVENT_DELETED: 
            return {...state,...{loading:false, error: false, deleted: true}};

        case ERROR_DELETING_EVENT: 
            return {...state,...{loading:false, error: true, deleted: false, errors: action.payload}};

        case RESET_DELETE:
            return {...state,...{loading:false, error: false, deleted: false}};
        default:
            return state;
    }
}

export default CancelEvent

  