import {SAVING_RECURR_EVENT, RECURR_EVENT_SAVED, ERROR_SAVING_RECURR_EVENT, SLOT_RESET} from '../../actions/recurringEvent'
const initialState = {
    loading: false,
    message: null,
    data: null,
    saved: false,
    slotId: null,
    error: false,
    errors: []
}
const RecurringEvent = (state = initialState, action) => {
    switch (action.type) {
        case SAVING_RECURR_EVENT:
            return {...state,...{loading:true, error: false}};
        case RECURR_EVENT_SAVED:
            return {...state,...{loading:false,saved: true, bannerImage: action.payload, slotId: action.slotId, errors: [],error: false}};
        case ERROR_SAVING_RECURR_EVENT:
            return {...state,...{loading:false, errors: action.payload, saved: false, error: true, slotId: action.slotId}};
        case SLOT_RESET: 
            return {...state, ... {loading: false, data: null, saved: false, slotId: null, error: false, errors: []}}
           
        default:
            return state;
    }
}

export default RecurringEvent

  