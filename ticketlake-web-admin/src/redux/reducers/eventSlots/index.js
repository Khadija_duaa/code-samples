import { SAVING_EVENT_SLOT, SLOT_ERROR, SLOT_SAVED, RESET_STATE } from '../../actions/events'
import {FETCHING_EVENT_DETAIL, EVENT_DETAIL_FETCHED, ERROR_FETCHING_EVENT_DETAIL, SLOT_RESET} from '../../actions/eventSlot'
const initialState = {
    loading: false,
    message: null,
    data: null,
    saved: false,
    slotId: null,
    error: false,
    errors: []
}
const slotReducer = (state = initialState, action) => {
    switch (action.type) {
        case SAVING_EVENT_SLOT:
            return {...state,...{loading:true, error: false}};
        case SLOT_SAVED:
            return {...state,...{loading:false,saved: true, bannerImage: action.payload, slotId: action.slotId, error: false, errors: []}};
        case SLOT_ERROR:
            return {...state,...{loading:false, errors: action.payload, saved: false, error: true, slotId: action.slotId}};
        case SLOT_RESET:
            return {...state, ... {loading: false, data: null, saved: false, slotId: null, error: false}}
        case RESET_STATE: 
            return {...state, ... {loading: false, data: null, saved: false, slotId: null, error: false}}
        case FETCHING_EVENT_DETAIL:
        return {...state,...{loading:true,error: false}};
        case EVENT_DETAIL_FETCHED:
            return {...state,...{loading:false, data: action.payload.data,error: false}};
        case ERROR_FETCHING_EVENT_DETAIL:
            return {...state,...{loading:false, data: action.payload, error: false}};
           
        default:
            return state;
    }
}

export default slotReducer

  