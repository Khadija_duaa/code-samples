import axios from '../../../utils/axios';
export const FETCHING_VENUES = 'FETCHING_VENUES';
export const VENUES_FETCHED = 'VENUES_FETCHED';
export const FETCHING_ERROR = 'FETCHING_ERROR'
const URL = `/venues/get-all-venues`

export function getAllVenues() {
    return (dispatch => {
        dispatch({
            type: FETCHING_VENUES,
            payload: null
        });
        axios.get(URL).then(venues => {
            dispatch({
                type: VENUES_FETCHED,
                payload: venues.data
            });
        }).catch (err => {
            dispatch({
                type: FETCHING_ERROR,
                payload: err.message
            });
        });
    });
}