import axios from '../../../utils/axios';
export const PROCESSING_ADMIN = 'ACTION_ADMIN_PROCESSING';
export const ADMINS = 'ACTION_FETCH_ADMIN_LIST';
export const ROLES = 'ACTION_FETCH_ROLES_LIST';
export const ADMIN = 'ACTION_FETCH_ADMIN';
export const DELETE_ADMIN = 'ACTION_FETCH_DELETE_ADMIN';
export const RESET_ADMIN = 'ACTION_RESET_ADMIN_DATA';
export const ADD_ADMIN = 'ACTION_ADD_ADMIN';

const setProcessing = (processing) => {
    return {
        type: PROCESSING_ADMIN,
        payload: processing
    };
};

const setRoles = (roles) => {
    return {
        type: ROLES,
        payload: roles,

    }
};

const _deleteAdmin = (index) => {
    return {
        type: DELETE_ADMIN,
        payload: index,
    }
};
const setAdmins = (admins) => {
    return {
        type: ADMINS,
        payload: admins,

    }
};

export const setAdmin = (admin) => {
    return {
        type: ADMIN,
        payload: admin,

    }
};

const addAdmin = (admin) => {
    return {
        type: ADD_ADMIN,
        payload: admin,

    }
};

export const resetRedux = () => {
    return {
        type: RESET_ADMIN
    };
};


export const loadRoles = () => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));
        axios.get(`/roles/get-all-roles`)
            .then((response) => {
                const {data} = response.data;
                dispatch(setRoles(data));
                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setRoles({error: err}));
                dispatch(setProcessing(false));
            });
    };
};

export const deleteAdmin = (id, index,callback,errorcallback) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.delete(`/admins/delete-admin/${id}`)
            .then((response) => {
                const {data} = response;

                dispatch(_deleteAdmin({index,message: data._message}));
                dispatch(setProcessing(false));
                callback && callback();
            })
            .catch(err => {
                dispatch(_deleteAdmin({error: err}));
                errorcallback && errorcallback();
                dispatch(setProcessing(false));
            });
    };
};

export const loadAdmins = () => {
    return (dispatch) => {
        axios.get('/admins/get-all-admins')
            .then((response) => {
                const {data} = response.data;
                dispatch(setAdmins(data.reverse()));
            })
            .catch(err => {
                dispatch(setAdmins({error: err}));
            });
    };
};


export const saveAdmin = (profile_picture_key, role, username, password,email,name,gender,cnic,dob,dateOfJoining,contactNumber,address,callback,cb,cback,errorcallback) => {

    return (dispatch,getState) => {
        let url = `/admins/create-admin`;
        let urlUpdate = `/admins/update-admin-profile`;

        let axiosPromise = null;

        let existingId = getState().Admins.admin ? getState().Admins.admin.uuid : null;

        if (existingId) {
            urlUpdate += `/${existingId}`;
            axiosPromise = axios.put(urlUpdate, { role,email,name,username,gender,cnic,dob,dateOfJoining,contactNumber,address,});
        }
        else {

            axiosPromise = axios.post(url,{profile_picture_key, role, username, password,email,name,gender,cnic,dob,dateOfJoining,contactNumber,address,});
        }

        axiosPromise.then((response) => {
            const {data} = response;

            dispatch(addAdmin({message: data._message}));

            callback && callback();
            cb && cb();
            cback && cback();

        }).catch(err => {
            // console.log('errrrrrrr', err.response.data._error)
            dispatch(addAdmin({error: err}));
            errorcallback && errorcallback()
        });
    };
};


export const updateAdmin = (uuid, profile_picture_key, role, username, password,email,name,callback,cb,cback,errorcallback) => {
    console.log('updateAdmin action', uuid )
    return (dispatch) => {
        let urlUpdate = `/admins/update-admin-profile/${uuid}`;
        axios.put(urlUpdate, {role,email,name,password,username}
            ).then((response) => {
            console.log('edit action response', response)
            dispatch(addAdmin({message: response._message}));
            callback && callback();
            cb && cb();
            cback && cback();
        }).catch(err => {
            console.log('edit action error', err)
            dispatch(addAdmin({error: err}));
            errorcallback && errorcallback()
        });
    };
};