import axios from '../../../utils/axios';
export const UPLOADING_IMAGE = 'UPLOADING_IMAGE'
export const BANNER_IMAGE_UPLOADED = 'BANNER_IMAGE_UPLOADED'
export const GALLERY_IMAGE_UPLOADED = 'GALLERY_IMAGE_UPLOADED'
export const IMAGE_UPLOAD_FAILED = 'IMAGE_UPLOAD_FAILED'
export const IMAGE_STATE_RESET = 'IMAGE_STATE_RESET'
export function uploadImage(imageType, imageBuffer, slotId) {
    const URL = `/events/image-upload`
    return (dispatch => {
        dispatch({
            type: UPLOADING_IMAGE,
            payload: null,
            slotId: slotId
        })
        axios.post(URL, imageBuffer).then(image => {
            imageType === 'banner' ? 
            dispatch({
                type: BANNER_IMAGE_UPLOADED,
                payload: image,
                slotId: slotId
            }): dispatch({
                type: GALLERY_IMAGE_UPLOADED,
                payload: image,
                slotId: slotId
            })

        }).catch (err => {
            dispatch({
                type: IMAGE_UPLOAD_FAILED,
                payload: err.message,
                slotId: slotId
            })
        })
    });
}

export function resetImageState () {
    return (dispatch => {
        dispatch({
            type: IMAGE_STATE_RESET,
            payload: null
        })
    })
}