import axios from '../../../utils/axios';
export const SAVING_RECURR_EVENT = 'SAVING_RECURR_EVENT'
export const RECURR_EVENT_SAVED = 'RECURR_EVENT_SAVED'
export const ERROR_SAVING_RECURR_EVENT = 'ERROR_SAVING_RECURR_EVENT'
export const SLOT_RESET = 'SLOT_RESET'

export function saveRecurringEvent (event) {
    const URL = `/slots/add-recurring-event-slots`
    return (dispatch => {
        dispatch({
            type: SAVING_RECURR_EVENT,
            payload: null
        })
        axios.post(URL, event).then(event => {
            dispatch({
                type: RECURR_EVENT_SAVED,
                payload: event.data.data
            })

        }).catch (err => {
            dispatch({
                type: ERROR_SAVING_RECURR_EVENT,
                payload: err.response.data._error
            })
        })
    });

}

export function resetSlot () {
    return (dispatch) => {
        dispatch({
            type: SLOT_RESET,
            payload: null
        })
    }
}