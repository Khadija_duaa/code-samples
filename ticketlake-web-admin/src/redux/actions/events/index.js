import {post} from 'axios';
import axios from '../../../utils/axios';
import {getObjectValue} from '../../../utils/common-utils';
export const EVENT_SAVED = 'EVENT_SAVED'
export const EVENT_SAVING = 'EVENT_SAVING'
export const EVENT_CREATION_FAILED = 'EVENT_CREATION_FAILED'
export const EVENT_TICKETS_SAVED = 'EVENT_TICKETS_SAVED'
export const EVENT_TICKETS_SAVING = 'EVENT_TICKETS_SAVING'
export const EVENT_TICKETS_CREATION_FAILED = 'EVENT_TICKETS_CREATION_FAILED'
export const EVENT_CONTACT_CREATION_FAILED = 'EVENT_CONTACT_CREATION_FAILED'
export const EVENT_CONTACT_SAVED = 'EVENT_CONTACT_SAVED'
export const EVENT_CONTACT_SAVING = 'EVENT_CONTACT_SAVING'
export const ALL_EVENTS = 'ACTION_ALL_EVENTS';
export const SET_EVENT_HISTORY = 'ACTION_SET_EVENT_HISTORY';
export const SAVE_EVENT = 'ACTION_SAVE_EVENT';
export const EVENT_DATA = 'ACTION_EVENT_DATA';
export const PROCESSING = 'ACTION_EVENT_PROCESSING';
export const EVENT = 'ACTION_EVENT';
export const SAVING_EVENT_SLOT = 'SAVING_EVENT_SLOT'
export const SLOT_SAVED = 'SLOT_SAVED'
export const SLOT_ERROR = 'SLOT_ERROR'
export const EVENT_DETAIL ='EVENT_DETAIL'
export const RESET_STATE = 'RESET_STATE'
export const RESET_EVENT_BASIC_INFO = 'RESET_EVENT_BASIC_INFO'
export const FETCHING_ALL_PARENT_EVENTS = 'FETCHING_ALL_PARENT_EVENTS'
export const PARENTS_EVENT_FETCHED = 'PARENTS_EVENT_FETCHED'
export const ERROR_FETCHING_PARENT_EVENTS = 'ERROR_FETCHING_PARENT_EVENTS'
export const CANCEL_EVENT_STARTED = 'CANCEL_EVENT_STARTED'
export const EVENT_CANCELLED = 'EVENT_CANCELLED'
export const EVENT_CANCEL_FAILED = 'EVENT_CANCEL_FAILED'
export const RESET_CANCEL_EVENT = 'RESET_CANCEL_EVENT'
export function saveEventBasicInfo(basicInfo) {
    const URL = `/events/manage-parent-event`
    return (dispatch => {
        dispatch({
            type: EVENT_SAVING,
            payload: null
        })
        axios.post(URL, basicInfo).then(event => {
            dispatch({
                type: EVENT_SAVED,
                payload: event.data.data
            })

        }).catch (err => {
            dispatch({
                type: EVENT_CREATION_FAILED,
                payload: err.response.data._error
            })
        })
    });
}

export function cancelEventSlot(eventSlotId) {
    const URL = `/events/cancel-event-slot`
    return (dispatch => {
        dispatch({
            type: CANCEL_EVENT_STARTED,
            payload: null
        })
        axios.post(URL, {eventSlotId}).then(event => {
            dispatch({
                type: EVENT_CANCELLED,
                payload: event.data.data
            })

        }).catch (err => {
            dispatch({
                type: EVENT_CANCEL_FAILED,
                payload: err.message
            })
        })
    });
}

export function saveEventContactInfo (contactInfo) {
    const URL = `/events/manage-parent-event`
    return (dispatch => {
        dispatch({
            type: EVENT_CONTACT_SAVING,
            payload: null
        })
        axios.post(URL, contactInfo).then(event => {
            dispatch({
                type: EVENT_CONTACT_SAVED,
                payload: event.data.data
            })

        }).catch (err => {
            dispatch({
                type: EVENT_CONTACT_CREATION_FAILED,
                payload: err.message
            })
        })
    });

}

export function saveEventSlot (eventSlot, slotId)  {
    const URL = `/slots/manage-event-slot`
    return (dispatch => {
        dispatch({
            type: SAVING_EVENT_SLOT,
            payload: null
        })
        axios.post(URL, eventSlot).then(event => {
            dispatch({
                type: SLOT_SAVED,
                payload: event.data.data,
                slotId: slotId
            })

        }).catch (err => {
            dispatch({
                type: SLOT_ERROR,
                slotId: slotId,
                payload: err.response.data._error
            })
        })
    });

}

export function saveTicketClasses (ticketClasses) {
    const URL = `/events/manage-parent-event`
    return (dispatch => {
        dispatch({
            type: EVENT_TICKETS_SAVING,
            payload: null
        })
        axios.post(URL, ticketClasses).then(event => {
            dispatch({
                type: EVENT_TICKETS_SAVED,
                payload: event.data.data
            })

        }).catch (err => {
            dispatch({
                type: EVENT_TICKETS_CREATION_FAILED,
                payload: err.response.data._error
            })
        })
    });
}

export function getAllParentEvents(organizationId) {
    const URL = `/events/get-parent-events/`
    const requestData = {
        "orgId": organizationId,	
        "paginate": false,
        "page": 1,
        "skip": 0,
        "pageSize": 10
    }
    return (dispatch => {
        dispatch({
            type: FETCHING_ALL_PARENT_EVENTS,
            payload: null
        })
        axios.post(URL, requestData).then(event => {
            dispatch({
                type: PARENTS_EVENT_FETCHED,
                payload: event.data.data
            })

        }).catch (err => {
            dispatch({
                type: ERROR_FETCHING_PARENT_EVENTS,
                payload: err.response.data._error
            })
        })
    });

}


const setAllEvents = (payload) => {
    return {
        type: ALL_EVENTS,
        payload
    }
};

export const setEvent = (event) => {
    return {
        type:EVENT ,
        payload: event,

    }
};

export const setEventDetail = (eventDetail) => {
    return {
        type:EVENT_DETAIL ,
        payload: eventDetail,

    }
};


const setProcessing = (processing) => {
    return {
        type: PROCESSING,
        payload: processing
    };
};

export const searchEvents = (searchStr, searchParams = []) => {
    return (dispatch, getState) => {
        const eventsState = getState().Events;

        let events = eventsState._events || eventsState.allEvents;

        events = [...events];

        if (searchStr) {
            let filteredEvents = [];
            events.forEach(event => {

                for (let index = 0; index < searchParams.length; index++) {
                    let param = searchParams[index];

                    const _val = getObjectValue(event, param);
                    if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                        filteredEvents.push(event);
                        break;
                    }
                }
            });
            dispatch(setAllEvents({_events: events, allEvents: filteredEvents}));
        }
        else {
            dispatch(setAllEvents({_events: undefined, allEvents:events}));
        }
    };

};

export const getAllEvents = (searchObject) => {
    return (dispatch) => {
        dispatch(setProcessing(true));
        let axiosPromise = null;
        axiosPromise = axios.post('/events/get-all-events-public', searchObject)


        axiosPromise.then(response => {
            dispatch(setAllEvents({ allEvents: response.data.data }));
            dispatch(setProcessing(false));

        }).catch((err) => {
            dispatch(setAllEvents({ error: err }));
            dispatch(setProcessing(false));
        });
    }
};

export const getEventDetail = (id) => {
    return (dispatch) => {
        axios.get(`/events/event-detail/${id}`)
            .then((response) => {
                const {data} = response.data;
                dispatch(setEventDetail({ticketClass:data}));
            })
            .catch(err => {
                dispatch(setEventDetail({error: err}));
            });
    };
};

export function resetEventSlotState () {
    return (dispatch) => {
        dispatch({
            type: RESET_STATE,
            payload: null
        })
    }
}


export function resetEventBasicInfo() {
    return (dispatch) => {
        dispatch({
            type: RESET_EVENT_BASIC_INFO,
            payload: null
        })
    }

}

export function resetCancelState () {
    return (dispatch) => {
        dispatch({
            type: RESET_CANCEL_EVENT,
            payload: null
        })
    }
}