import axios from '../../../../utils/axios';
export const DELETING_EVENT = 'DELETING_EVENT'
export const EVENT_DELETED = 'EVENT_DELETED'
export const ERROR_DELETING_EVENT = 'ERROR_DELETING_EVENT'
export const RESET_DELETE = 'RESET_DELETE'
export const EVENT_PUBLISHING = 'EVENT_PUBLISHING'
export const EVENT_PUBLISHED = 'EVENT_PUBLISHED'
export const ERROR_EVENT_PUBLISH = 'ERROR_EVENT_PUBLISH'
export const RESET_PUBLISH_STATE = 'RESET_PUBLISH_STATE'
export function publishEvent(slotId) {
    return (dispatch) => {
        dispatch({
            type: EVENT_PUBLISHING,
            payload: null
        })
        axios.post(`/events/publish-unPublish-event`, {
            eventSlotId: slotId
        }).then((response) => {
            dispatch({
                type: EVENT_PUBLISHED,
                payload: response.data._message
            })

        }).catch(err => {
            dispatch({
                type: ERROR_EVENT_PUBLISH,
                payload: err.response.data._error
            })
        })
    }
}

export function deleteEventSlot(slotId) {
    return (dispatch) => {
        dispatch({
            type: DELETING_EVENT,
            payload: null
        })
        axios.delete(`/events/delete-event-slot/${slotId}`).then(() => {
            dispatch({
                type: EVENT_DELETED,
                payload: null
            })

        }).catch(err => {
            dispatch({
                type: ERROR_DELETING_EVENT,
                payload: err.response.data._error
            })
        })
    }
}

export function resetDeleteState () {
    return (dispatch) => {
        dispatch({
            type: RESET_DELETE,
            payload: null
        })
    }
}

export function resetPublishState () {
    return (dispatch) => {
        dispatch({
            type: RESET_PUBLISH_STATE,
            payload: null
        })
    }
}