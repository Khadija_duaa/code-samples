import axios from '../../../utils/axios';
export const FETCHING_EVENT_DETAIL = 'FETCHING_EVENT_DETAIL'
export const EVENT_DETAIL_FETCHED = 'EVENT_DETAIL_FETCHED'
export const ERROR_FETCHING_EVENT_DETAIL = 'ERROR_FETCHING_EVENT_DETAIL'
export const SLOT_RESET = 'SLOT_RESET'

export function getEventDetail(slotId) {
    const url = `/events/event-detail/${slotId}`
    return (dispatch) => {
        dispatch({
            type: FETCHING_EVENT_DETAIL,
            payload: null
        })
        axios.get(url).then(response => {
            dispatch({
                type: EVENT_DETAIL_FETCHED,
                payload: response.data
            })
        }).catch(err => {
            dispatch({
                type: ERROR_FETCHING_EVENT_DETAIL,
                payload: err.message
            })
        })
    }
}

export function resetSlot () {
    return (dispatch) => {
        dispatch({
            type: SLOT_RESET,
            payload: null
        })
    }
}