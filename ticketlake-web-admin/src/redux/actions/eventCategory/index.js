
import axios from '../../../utils/axios';
export const PROCESSING_CATEGORY = 'ACTION_CATEGORY_PROCESSING';
export const CATEGORIES = 'ACTION_FETCH_CATEGORY_LIST';
export const CATEGORY = 'ACTION_FETCH_CATEGORY';
export const DELETE_CATEGORY = 'ACTION_FETCH_DELETE_CATEGORY';
export const RESET_CATEGORY = 'ACTION_RESET_CATEGORY_DATA';
export const ADD_CATEGORY = 'ACTION_ADD_CATEGORY';


const setProcessing = (processing) => {
    return {
        type: PROCESSING_CATEGORY,
        payload: processing
    };
};

const _deleteCategory = (index) => {
    return {
        type:DELETE_CATEGORY,
        payload: index,
    }
};
const setCategories= (categories) => {
    return {
        type: CATEGORIES,
        payload: categories,

    }
};

export const setCategory = (category) => {
    return {
        type: CATEGORY,
        payload: category,

    }
};

const addCategory = (category) => {
    return {
        type: ADD_CATEGORY,
        payload: category,

    }
};

export const resetRedux = () => {
    return {
        type: RESET_CATEGORY
    };
};

export const deleteCategory = (uuid, index,callback,errorcallback) => {
    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.delete(`/categories/delete-category/${uuid}`)
            .then((response) => {
                let {data} = response;
                dispatch(_deleteCategory({index,message:data._message}));
                dispatch(setProcessing(false));
                callback && callback();
            })
            .catch(err => {
                dispatch(_deleteCategory({error: err}));
                errorcallback && errorcallback();
                dispatch(setProcessing(false));
            });
    };
};

export const loadCategories = () => {
    return (dispatch) => {
        axios.get('/categories/get-all-categories')
            .then((response) => {

                let {data} = response.data;

                const categories =[];
                data.forEach(category => {
                    categories.push({tags:category.tags, iconImageKey:category.iconImageKey?category.iconImageKey:null, imageKey:category.imageKey?category.imageKey:null, categoryName:category.name, uuid:category.uuid, _id:category._id});

                });

                dispatch(setCategories(categories.reverse()));
            })
            .catch(err => {
                dispatch(setCategories({error: err}));
            });
    };
};


export const saveCategory = (name,tags,imageKey,imageUrl,iconKey,iconUrl, callback,cb,cback,errorcallback) => {
    return (dispatch,getState) => {
        let url = `/categories/create-category`;
        let urlUpdate = `/categories/update-category`;

        let axiosPromise = null;

        let existingId = getState().eventCategories.category ? getState().eventCategories.category.uuid : null;

        if (existingId) {
            urlUpdate += `/${existingId}`;
            axiosPromise = axios.put(urlUpdate, {
                name,
                tags,
                iconImageKey:{
                    imageKey:iconKey,
                    imageUrl:iconUrl,
                },
                imageKey:{
                    imageKey:imageKey,
                    imageUrl:imageUrl,
                },
        });
        }
        else {

            axiosPromise = axios.post(url,{
                name,
                tags,
                iconImageKey:{
                    imageKey:iconKey,
                    imageUrl:iconUrl,
                },
                imageKey:{
                    imageKey:imageKey,
                    imageUrl:imageUrl,
                },
            });
        }

        axiosPromise.then((response) => {
            let {data} = response;
            dispatch(addCategory({message: data._message}));

            callback && callback();
            cb && cb();
            cback && cback();

        }).catch(err => {
            dispatch(addCategory({error: err}));
            errorcallback && errorcallback()
        });
    };
};
