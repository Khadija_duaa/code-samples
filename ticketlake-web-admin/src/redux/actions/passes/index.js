
import axios from '../../../utils/axios'
import moment from 'moment'
export const CREATING_PASS = 'CREATING_PASS';
export const PASS_CREATED = 'PASS_CREATED';
export const ERROR_PASS_CREATION = 'ERROR_PASS_CREATION';
export const RESET_PASS_CONFIG = 'RESET_PASS_CONFIG'
export const FETCHING_EVENT_SLOT = 'FETCHING_EVENT_SLOT'
export const EVENT_SLOTS_FETCHED = 'EVENT_SLOTS_FETCHED'
export const ERROR_FETCHING_SLOT = 'ERROR_FETCHING_SLOT'
export const FETCHING_ALL_EVENT_SLOT = 'FETCHING_ALL_EVENT_SLOT'
export const ALL_EVENT_SLOTS_FETCHED = 'ALL_EVENT_SLOTS_FETCHED'
export const ERROR_FETCHING_ALL_SLOT = 'ERROR_FETCHING_ALL_SLOT'
export const createPassConfig = (pass, id) => {
    const create_url = `/events/add-passes`
    const update_url = `/events/edit-pass`
    let url = ""
    if(pass.passConfigs._id && pass.passConfigs._id !== "") url = update_url
    else url = create_url
    return (dispatch) => {
        dispatch({
            type: CREATING_PASS,
            payload: null
        })
        axios.post(url, pass).then(response => {
            dispatch({
                type: PASS_CREATED,
                payload: response.data.data,
                id: id
            })
        }).catch(err => {
            console.log('response ->',err.response.data._error)
            dispatch({
                type: ERROR_PASS_CREATION,
                payload: err.response.data._error,
                id: id
            })
        });
    };
};

export const updatePass = (pass, id) => {
    const url = `/events/edit-pass`
    return (dispatch) => {
        dispatch({
            type: CREATING_PASS,
            payload: null
        })
        axios.post(url, pass).then(response => {
            dispatch({
                type: PASS_CREATED,
                payload: response.data
            })
        }).catch (err => {
            dispatch({
                type: ERROR_PASS_CREATION,
                payload: err.response.data._error
            })
        })
    }

}

export const getEventSlots = (eventId, process = false) => {
    console.log('getEventSlots', process)
    const url = `/events/get-event-slots/${eventId}`
    return (dispatch) => {
        dispatch({
            type: FETCHING_EVENT_SLOT,
            payload: null
        })
        axios.get(url).then(response => {
            if (response.data.data.length > 0) {
                let eventData = []
                response.data.data.map(event => {
                    eventData.push({
                        'label': moment(event.eventDateTimeSlot.eventStartTime).local().format('YYYY-MM-DD HH:mm:ss'),
                        'value': event._id
                    })
                })
                dispatch({
                    type: EVENT_SLOTS_FETCHED,
                    payload: process ? eventData: response.data.data
                })
            } else {
                dispatch({
                    type: EVENT_SLOTS_FETCHED,
                    payload: []
                })
            }
            
            
        }).catch(err => {
            dispatch({
                type: ERROR_FETCHING_SLOT,
                payload: err.response
            })
        });
    }
}

export const getAllEventsSlots = (eventId) => {
    const url = `/events/get-event-slots/${eventId}`
    return (dispatch) => {
        dispatch({
            type: FETCHING_ALL_EVENT_SLOT,
            payload: null
        })
        axios.get(url).then(response => {
            dispatch({
                type: ALL_EVENT_SLOTS_FETCHED,
                payload: response.data.data
            })
            
        }).catch(err => {
            dispatch({
                type: ERROR_FETCHING_ALL_SLOT,
                payload: err.response
            })
        });
    }

}

export const resetState = () => {
    return (dispatch) => {
        dispatch({
            type: RESET_PASS_CONFIG,
            payload:  null
        })
    }
}