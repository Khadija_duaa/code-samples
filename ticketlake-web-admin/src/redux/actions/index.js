import { categoriesReducer }  from './categories';
import VenuesReducer from './venues'
export default {
    Categories: categoriesReducer,
    Venues: VenuesReducer
}