import axios from '../../../utils/axios'
export const SAVING_TICKET = 'SAVING_TICKET';
export const TICKET_SAVED = 'TICKET_SAVED';
export const ERROR_TICKET_SAVING = 'ERROR_TICKET_SAVING'
export const FETCHING_PURCHASED_TICKET = 'FETCHING_PURCHASED_TICKET'
export const PURCHASED_TICKET_FETCHED = 'PURCHASED_TICKET_FETCHED'
export const ERROR_FETCHING_PURCHASED_TICKET = 'ERROR_FETCHING_PURCHASED_TICKET'
export const REFUND_PROCESSED = 'REFUND_PROCESSED'
export const PROCESSING_REFUND = 'PROCESSING_REFUND'
export const ERROR_PROCESSING_REFUND = 'ERROR_PROCESSING_REFUND'
export const RESET_REFUD = 'RESET_REFUND'
export const RESET_TICKET_STATE = 'RESET_TICKET_STATE'
export function generateTicket (ticket) {
    const url = `/tickets/purchase-ticket-admin`
    return (dispatch) => {
        dispatch({
            type: SAVING_TICKET,
            payload: null
        })
        axios.post(url, ticket).then(ticket => {
            dispatch({
                type: TICKET_SAVED,
                payload: ticket
            })
        }).catch(err => {
            dispatch({
                type: ERROR_TICKET_SAVING,
                payload: err.response.data._error
            })
        })
    }
}

export function getEventPurchasedTickets(eventId, queryString = null) {
    let url = `/tickets/get-event-purchased-tickets-admin?paginate=false&eventId=${eventId}`
    if (queryString) url = url + '&ticketStatus='+queryString
    return (dispatch) => {
        dispatch({
            type: FETCHING_PURCHASED_TICKET,
            payload: null
        })
        axios.get(url).then(response => {
            dispatch({
                type: PURCHASED_TICKET_FETCHED,
                payload: response.data.data
            })
        }).catch(err => {
            dispatch({
                type: ERROR_FETCHING_PURCHASED_TICKET,
                payload: err.response.data._errors
            })
        })
    }
}

export function refundTicket(ticket) {
    const url = `/tickets/refund-ticket`
    return (dispatch) => {
        dispatch({
            type: PROCESSING_REFUND,
            payload: null
        })
        axios.post(url, ticket).then(response => {
            dispatch({
                type: REFUND_PROCESSED,
                payload: response.data.data
            })
        }).catch(err => {
            dispatch({
                type: ERROR_PROCESSING_REFUND,
                payload: err.response.data._errors
            })
        })
    }

}

export function resetState() {
    return (dispatch) => {
        dispatch({
            type: RESET_REFUD,
            payload: null
        })
    }
}

export function resetTicketState () {
    return (dispatch) => {
        dispatch({
            type: RESET_TICKET_STATE,
            payload: null
        })
    }
}