import axios from '../../../utils/axios';
export const VENUES = 'ACTION_VENUES';
export const PROCESSING_VENUE = 'ACTION_VENUE_PROCESSING';
export const VENUE = 'ACTION_FETCH_VENUE';
export const DELETE_VENUE = 'ACTION_FETCH_DELETE_VENUE';
export const RESET_VENUE = 'ACTION_RESET_VENUE_DATA';
export const ADD_VENUE = 'ACTION_ADD_VENUE';


export const getAllVenues = () => {
    return (dispatch) => {
        axios.get('/venues/get-all-venues')
            .then((response) => {
                let {data} = response.data;
                dispatch(getVenues(data.reverse()));
            })
            .catch(err => {
                dispatch(getVenues({error: err}));
            });
    };
};

const getVenues= (categories) => {
    return {
        type: VENUES,
        payload: categories,
    }
};

export const setVenue = (venue) => {
    return {
        type: VENUE,
        payload: venue,

    }
};

const addVenue = (venue) => {
    return {
        type: ADD_VENUE,
        payload: venue,

    }
};

export const resetRedux = () => {
    return {
        type: RESET_VENUE
    };
};

const _deleteCategory = (index) => {
    return {
        type: DELETE_VENUE,
        payload: index,
    }
};

export const deleteVenue = (venueId, i,callback,errorcallback) => {
    return (dispatch) => {

        axios.delete(`/venues/delete-venue/${venueId}`)
            .then((response) => {
                dispatch(_deleteCategory({i}));
                callback && callback();
            })
            .catch(err => {
                dispatch(_deleteCategory({error: err}));
                errorcallback && errorcallback();
            });
    };
};

export const saveVenue = (name,city,country,latitude,longitude,address, seatingCapacity,postalCode,callback,cb,cback,errorcallback) => {
    
    return (dispatch,getState) => {
        let url = `/venues/create-venue`;
        let urlUpdate = `/venues/update-venue`;

        let axiosPromise = null;

        let existingId = getState().eventVenues.venue ? getState().eventVenues.venue.venueId : null;

        if (existingId) {
            urlUpdate += `/${existingId}`;
            axiosPromise = axios.put(urlUpdate, { name,city,country,latitude,longitude,address, seatingCapacity, postalCode});
        }
        else {

            axiosPromise = axios.post(url,{name,city,country,latitude,longitude,address, seatingCapacity, postalCode});
        }
        return axiosPromise.then((response) => {
            let {data} = response;

            dispatch(addVenue({message: data}));

            callback && callback();
            cb && cb();
            cback() && cback();

        }).catch(err => {
            dispatch(addVenue({error: err}));
            errorcallback && errorcallback()
        });
    };
};

