import axios from '../../../utils/axios';
import {BASE_URL, XAUTH} from '../../../utils/CONSTANTS';
export const FETCHING_CATEGORIES = 'FETCHING_CATEGORIES';
export const CATEGORIES_FETCHED = 'CATEGORIES_FETCHED';
export const FETCHING_ERROR = 'FETCHING_ERROR'
const URL = `/categories/get-all-categories`

export function getAllCategories() {
    return (dispatch => {
        dispatch({
            type: FETCHING_CATEGORIES,
            payload: null
        });
        axios.get(URL).then(categories => {
            dispatch({
                type: CATEGORIES_FETCHED,
                payload: categories.data
            });
        }).catch (err => {
            dispatch({
                type: FETCHING_ERROR,
                payload: err.message
            });
        });
    });
}