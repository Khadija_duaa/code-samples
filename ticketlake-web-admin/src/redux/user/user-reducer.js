import * as actions from './user-action-types';

const loadAuthStateFromLocalStorage = () => {
    let authState = sessionStorage.getItem("authState");

    if (authState && authState.length > 0) {
        return JSON.parse(authState);
    }

    return null;
};




const _initState = {
    activeUser: null,
    authenticated: false,
    error:false,
};

const initState = () => {
    const prevState = loadAuthStateFromLocalStorage();
    if (!prevState) {
        return {..._initState};
    }
    return prevState;

};


const user_reducer = (state = initState(), action) => {
    let newState = {...state};
    switch (action.type) {
        case actions.SET_ACTIVE_USER:
            login(newState, action.payload, action.token);
            break;

        case actions.LOGOUT_USER:
            logout(newState);
            break;

        case actions.ERROR_USER:
            error(newState, action.payload);
            break;

        default:
            break;
    }

    return newState;
};


const login = (state, data, token) => {
    state.activeUser = data;
    state.authenticated = true;
    state.token = token;
    sessionStorage.setItem('authState', JSON.stringify(state));
};

const logout = (state) => {
    state.activeUser = {};
    state.authenticated = false;
    sessionStorage.removeItem("authState");

};

const error = (state) => {
    state.activeUser = {};
    state.authenticated = false;
    state.error = true;

};

export default user_reducer;