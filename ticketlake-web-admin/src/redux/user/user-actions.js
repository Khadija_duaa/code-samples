import * as actions from './user-action-types';
import axios from '../../utils/axios';

const setActiveUser = (data, token) => {
    return {
        type: actions.SET_ACTIVE_USER,
        payload: data,
        token: token,
    }
};



export const logout = () => {
    return {
        type: actions.LOGOUT_USER,
    }
};

export const setError = (data) => {
    return {
        type: actions.ERROR_USER,
        payload:data
    }
};

export const logoutUser = (cb) => {
    return (dispatch, getState) => {
        setTimeout(() => {
            dispatch(logout());
            cb && cb();
        }, 1000);
    }
};


export const loginUser = (loginData, error) => {
    return (dispatch, getState) => {
        axios.post('/admins/login', loginData)
            .then((response)=> {
                setTimeout(() => {
                    const {data} = response;
                    let token = response.headers["access-token"];
                    dispatch(setActiveUser(data, token));
                }, 1000);
            })
            .catch(error => {
                dispatch(setError(error));
            })
    }
};

