import React from 'react';
import { Row, Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import UserRecord from '../userRecord';
import QrReader from 'react-qr-reader'
import axios from '../../../utils/axios';

export default class qrCode extends React.Component {
    constructor(props){
        super(props)
        this.state={
            eventId:'',
            srNo:'',
            isRecord:false,
            result: null,
            error:null,
            scannedData:[],
            delay: 100,
            // result:'No result',
        }
    }

    // handleScan(data){
    //     this.setState({
    //       result: data,
    //     })
    //   }
    //   handleError(err){
    //     console.error('err in qr code', err)
    //   }

    // etCodeClear=()=>{
    //     this.setState({
    //         qrCode:''
    //     })
    //     this.props.history.push('/scanning')
    // }

    handleScan = data => {
        if (data) {
            this.setState({
                srNo:data,
            })
            axios.post('/tickets/validate-qrcode', {
                "eventId": this.props.location.eventId,      //this time its static 
                "srNo": data,            //this time its static 
            })
            .then((response)=>{
                this.setState({
                    scannedData:response.data,
                    isRecord:true,
                })
            })
            .catch((error)=>{
                console.log('error', error);
                console.log('error2', error.response.data._error);
                this.setState({
                    error:error.response.data._error||'error on scanning'
                })
            });
        }
      }
      handleError = err => {
        console.error('err in qr code 29', err)
      }
      
    render() {
        // const previewStyle = {
        //     height: 240,
        //     width: 320,
        //   }
        console.log('this.props in qr code', this.props.location.eventId, this.state.isRecord, this.state.result, this.state.srNo)
        return (
            <>
                {/* while input data is true */}
                {this.state.isRecord===false ?
                    <>
                        <Row>
                            <Col md="3">
                                <h3><b>QR Code Scanning</b></h3>
                            </Col>
                        </Row>
                        <Row className = "mt-3">
                            <Col md="12">
                                <p className="space-bw">Please adjust the QR Code in scanning area</p>
                                {/* <p>{this.props.location.eventId}</p> */}
                            </Col>
                            <Col md="4">
                                <div className="qr-scan-screen1">
                                <QrReader
                                    delay={300}
                                    onError={this.handleError}
                                    onScan={this.handleScan}
                                    style={{ width: '100%' }}
                                    />
                                </div>
                                <br/>
                                <p className="rd">{this.state.error}</p>
                                {/* <QrReader
                                    delay={this.state.delay}
                                    style={previewStyle}
                                    onError={this.handleError}
                                    onScan={this.handleScan}
                                    />
                                    <p>{this.state.result}</p> */}
                            </Col>
                        </Row>
                    </> 
                    :
                    <>
                        <UserRecord srNo={this.state.srNo} eventData={this.props.location.eventData} eventId={this.props.location.eventId} scannedData={this.state.scannedData}/>
                    </>
                }

            </>
        )
    }
}