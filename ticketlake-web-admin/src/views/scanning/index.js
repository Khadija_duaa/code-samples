import React, {Component} from 'react';
import {
  Row,
  Col,
  CardTitle,
  Card,
  CardBody
} from 'reactstrap';
import { Route, Link } from 'react-router-dom';
import QrCode from './qrCode';
import EtCode from './etCode';
import AllTickets from './allTickets';

const cards = (e) => {
    console.log('||et code + qr code scan screen||', e)
    return (
    <div>
        {/* <Row>
            <Col md="3">
                <h3><b>Tickets Scanning</b></h3>
            </Col>
        </Row> */}
        <Row className = "mt-3">
        <Col md="12">
            <p>Please select a method ticket!</p>
        </Col>
            {
                eventSelectionScreens.map((screen, key) => {
                    return (
                        screen.name != 'default' ? 
                        <Col md="3" key={key}>
                            <Link 
                                    to={{
                                        pathname: screen.path, 
                                        eventId: e.location.eventId,
                                        eventData: e.location.eventData,
                                    }} 
                                    key={key} 
                                    >
                                <Card className="flex-box" style={{padding:'0px'}}>
                                    <CardBody>
                                        <CardTitle className="flex-box-heading">
                                            <i className={screen.icon}></i>
                                            <br/>
                                            {screen.name}
                                        </CardTitle>
                                    </CardBody>
                                </Card>
                            </Link>
                            {/*  {e.location.eventId}    delete after done */}
                        </Col> : null
                    )
                })
            }
        </Row> 
        </div>
    )
}
const all = () => {
    return (
        <Row>
            <Col md="12">
                <AllTickets />
            </Col>
        </Row>
    )
}

const eventSelectionScreens = [{
    name: 'default',
    path: '/alltickets',
    description: '',
    exact: true,
    component: all
},{
    name: 'default',
    path: '/alltickets/scanning',
    description: '',
    exact: true,
    component: cards
},{
    name: 'et code',
    path: '/alltickets/scanning/etcode',
    exact: false,
    component: EtCode,
    icon:'fas fa-keyboard',
    // eventId:this.props.location.eventId
},{
    name: 'qr code',
    path: '/alltickets/scanning/qrcode',
    exact: false,
    component: QrCode,
    icon:'fas fa-qrcode',
    // eventId:this.props.location.eventId
}]


export default class Scanning extends Component {
    render() {
        const routes = eventSelectionScreens.map((prop, key) => {
            return <Route exact = {prop.exact} path={prop.path} key={key} component = {prop.component} />;
        });
        return(
            <div>
                {routes}
            </div>
            
        )
    }
}


