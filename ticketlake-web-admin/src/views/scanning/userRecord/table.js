import React from 'react';
import { Table } from 'reactstrap';
import Moment from 'react-moment';
import moment from 'moment';
export default class recordTable extends React.Component {
    constructor(props){
        super(props)
    }
    
    render() {
        console.log('details table', this.props)
        const {eventName, eventType, phone, venue, ticketClass, dateStart, dateEnd, lastScanned, lastScannedBy, scanCount, scannedTickets} = this.props
        return (
                    <Table bordered className="m-custom-table">
                                            <tbody>
                                                <tr>
                                                    <th>Event Name</th>
                                                    <td>{eventName}</td>
                                                </tr>
                                                <tr>
                                                    <th>Event ID</th>
                                                    <td>#909091</td>
                                                </tr>
                                                <tr>
                                                    <th>Venue</th>
                                                    <td>{venue}</td>
                                                </tr>
                                                <tr>
                                                    <th>Event Type</th>
                                                    <td>{eventType}</td>
                                                </tr>
                                                <tr>
                                                    <th>Scanned Tickets</th>
                                                    <td>{scannedTickets}</td>
                                                </tr>
                                                <tr>
                                                    <th>Date</th>
                                                    <td>
                                                        {moment(dateStart).utc().format("MMM DD")}
                                                            {' '} - {' '}
                                                        {moment(dateEnd).utc().format("MMM DD")}
                                                    </td>
                                                </tr>
                                                {/* do no need if scan is more than 1000 than? */}
                                                {/* <tr>
                                                    <th>Last Scanned</th>
                                                    <td>{lastScanned}</td>
                                                </tr>
                                                <tr>
                                                    <th>Last Scanned by</th>
                                                    <td>{lastScannedBy}</td>
                                                </tr>
                                                <tr>
                                                    <th>Scan Count</th>
                                                    <td className="link">{scanCount} Scans</td>
                                                </tr> */}
                                            </tbody>
                                </Table>   
        )
    }
}