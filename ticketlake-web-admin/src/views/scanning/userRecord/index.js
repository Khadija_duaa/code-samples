import React, {Component} from 'react';
import { Row, Col, Button, Form, FormGroup, Label, Input, Table, Modal, ModalHeader, ModalBody, ModalFooter  } from 'reactstrap';
import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import axios from '../../../utils/axios';
import Moment from 'react-moment';
import RecordTable from './table';
// import base64ToImage from 'base64-to-image';
import moment from 'moment';
// import base64Img from 'base64-img';
// var request = require('ajax-request');
// var fs = require('file-system');

function dataURItoBlob (dataURI) {
    let byteString = atob(dataURI.split(',')[1]);
  
    // separate out the mime component
    let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
  
    let ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    let blob = new Blob([ab], {type: mimeString});
    return blob;
  }
  
  function padWithZeroNumber (number, width) {
    number = number + '';
    return number.length >= width
      ? number
      : new Array(width - number.length + 1).join('0') + number;
  }
  
  function getFileExtention (blobType) {
    // by default the extention is .png
    let extention = IMAGE_TYPES.PNG;
  
    if (blobType === 'image/jpeg') {
      extention = IMAGE_TYPES.JPG;
    }
    return extention;
  }
  
  function getFileName (imageNumber, blobType) {
    const prefix = 'photo';
    const photoNumber = padWithZeroNumber(imageNumber, 4);
    const extention = getFileExtention(blobType);
  
    return `${prefix}-${photoNumber}.${extention}`;
  }
  
  function downloadImageFileFomBlob (blob, imageNumber) {
    window.URL = window.webkitURL || window.URL;
  
    let anchor = document.createElement('a');
    anchor.download = getFileName(imageNumber, blob.type);
    anchor.href = window.URL.createObjectURL(blob);
    let mouseEvent = document.createEvent('MouseEvents');
    mouseEvent.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
    localStorage.setItem("message", anchor.href)
    console.log('blob, imageNumber2.4',anchor.href)
    anchor.dispatchEvent(mouseEvent);
  }
  
  function downloadImageFile (dataUri, imageNumber) {
    let blob = dataURItoBlob(dataUri);
    downloadImageFileFomBlob(blob, imageNumber);
  }

  
export default class userRecord extends Component {
    constructor(props){
        super(props)
        this.state={
            isProtected:false,
            pass:'',
            isPic:false,
            camPic:'',
            isScanned: false, //this.props.scannedData.isScanned,
            rejectedValue:'',
            modal:false,
            eventId:'',
            srNo:'',
            deviceId:'Xdf432gh',
            verifySuccessfully:'',
            error:'',
            successImage:'',
            modalOpen: false,
            isClicked: false,
            venueHistoryInfo:[],
            inValidPassword:null,
            isPasswordProtected:false, // get prop from api and set here
            scanCount:this.props.scannedData.data.scanCount,
            file: '',
            imagePreviewUrl: '',
        }
        this.imageNumber = 0;
    }
    componentDidMount=()=>{
        console.log('c bug', this.props)
        if(this.props.scannedData.data.scanCount===0){
            if(this.props.eventData.isTicketsPasswordProtected===true){
                this.setState({
                    isPasswordProtected:true,
                })
            }else{
                this.setState({
                    isPasswordProtected:false,
                })
            }
            
        }
    }
    changePassword=(e)=>{
        console.log(e.target.value)
        this.setState({
            pass:e.target.value,
        })
    }
    etCodePasswordSubmit=()=>{
        // console.log('submited password', this.props, this.state.pass)
        axios.post('/tickets/validate-password', {
            "eventId": this.props.scannedData.data.eventInfo._id,
            "srNo": this.props.srNo,
            "ticketPassword": this.state.pass,
        })
        .then((response)=>{
            this.setState({
                inValidPassword:false,
                isPasswordProtected:false,
            })
        })
        .catch((error)=>{
            // console.log('password error', error.response.data._error);
             this.setState({
                inValidPassword:error.response.data._error?error.response.data._error : null
            })
        });
    }

    toggle=()=>{
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
    }
    onRejectedValue=(e)=>{
        // console.log('rejectedValue', e.target.value)
        this.setState({
            rejectedValue: e.target.value
        })
    }

    openDeleteModal = (data) => this.setState({modalOpen: !this.state.modalOpen,venueHistoryInfo: data});

    getModalContent = () => {
        // console.log('venueHistoryInfo', this.state.venueHistoryInfo.scanningDetails?this.state.venueHistoryInfo.scanningDetails:'no data')
      return (<Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal}
                     className={'modal-danger ' + this.props.className}>
  
          <ModalHeader toggle={this.openDeleteModal}>
            History
          </ModalHeader>
  
          <ModalBody>
              {/* History not found... */}
                    <Col md="col-sm-12 col-md-12" className="space-bw">
                                <Table bordered className="m-custom-table">
                                <tbody>
                                    <tr>
                                        <th>Device Id</th>
                                        <th>Name</th>
                                        <th>Scan Date</th>
                                    </tr>
                                    
                                        {this.state.venueHistoryInfo.scanningDetails?this.state.venueHistoryInfo.scanningDetails.map(data => {
                                            // console.log('daaaaata', data.scanningHistory.device))
                                            return  (   
                                                        <tr>
                                                            <td>{data.scanningHistory.device}</td>
                                                            <td>{data.scanningHistory.staffInfo.name}</td>
                                                            <td>{moment(data.scanningHistory.scanDateTime).utc().format("MMM DD YYYY")}</td>
                                                        </tr>
                                                    )
                                            })
                                            :null
                                        }
                                    

                                    
                                    
                                </tbody>
                    </Table>  
            </Col>
          </ModalBody>
  
          <ModalFooter>
  
            <Button color="primary" onClick={this.openDeleteModal}>
              Cancel
            </Button>
  
          </ModalFooter>
        </Modal>
      );
    };
    // verfiy ticket
    verifyTicket=()=>{
        // console.log('verify btn click', this.props.scannedData.data.eventInfo._id , this.props.srNo)
        axios.put('/tickets/add-scan-detail', {
            "eventId": this.props.scannedData.data.eventInfo._id,
            "srNo": this.props.srNo,
            "deviceId": this.state.deviceId,
        })
        .then((response)=>{
            // console.log('response ticket verify', this);
            // this.props.history.push('/scanning')
            this.setState({
                verifySuccessfully: response.data.message? response.data.message : '---'
            })
        })
        .catch((error)=>{
            // console.log('error', error.response._error);
            this.setState({
                error:'Ticket not scan, uplaod image than try again!'
            })
        });
    }
    // verfiy ticket
    

    uploadFile=(e)=>{
        e.preventDefault();
        console.log('ticket scan image upload', this.props.scannedData)

        let reader = new FileReader();
        let file = e.target.files[0];
        let formData = new FormData()
        formData.append("image", file)
        formData.append("eventId", this.props.scannedData.data.eventInfo._id)
        formData.append("srNo", this.props.srNo)
        formData.append("ticketPassword", this.props.srNo?this.props.srNo:null)
        // console.log('file is here => ', file)
        this.setState({
            file: file,
        });

        axios.post('/tickets/add-ticket-owner-picture', formData)
        .then((response)=>{
            // console.log('upload images response', response);
            this.setState({
                successImage: response.data.message? response.data.message : '---',
                isPic:true,
                isScanned:true,
                camPic:file,
                modal: false
            })
        })
        .catch((error)=>{
            console.log('error on image uplaod', error)
            this.setState({
                error:error.response.data._error,
                isPic:false,
                isScanned:false,
            })
        });
    }

    // -------------------------------------------------------- camera functions --------------------------------------------------------//
    onTakePhoto (dataUri) {
        downloadImageFile(dataUri, this.imageNumber);
        this.imageNumber += 1;
    }
  
    onCameraError (error) {
        console.error('onCameraError', error);
    }
    
    onCameraStart (stream) {
        console.log('onCameraStart');
    }
    
    onCameraStop () {
        console.log('onCameraStop');
    }
      // -------------------------------------------------------- camera functions --------------------------------------------------------//


    render() {
        // console.log('et_code', this.props.scannedData.data.purchaseDetails.name)
        // console.log('index.js 90000', this.props.scannedData.data)
        // console.log('index.js scanCount', this.props.scannedData.data.scanCount)
        // console.log('localstorage render', localStorage.message)
        // var localDate = new Date(this.props.scannedData.data.eventInfo.eventDateTimeSlot.eventStartTime);
        console.log('ticket scan image upload 2', this.props)
        return (
            <>          {this.state.isPasswordProtected?
                        <>
                            <Row>
                                <Col md="3">
                                    <h3><b>Password</b></h3>
                                </Col>
                            </Row>
                            <Row className = "mt-3">
                                <Col md="12">
                                    <p className="space-bw">Code has been scanned successfully, now enter the password to check in.</p>
                                </Col>
                                <Col md="4">
                                    <Form inline>
                                        <FormGroup className="mb-4 mr-sm-2 mb-sm-0">
                                            <Input type="password" name="et-password" id="etText" placeholder="Password" onChange={(value)=>this.changePassword(value)}/>
                                        </FormGroup>
                                        
                                    </Form>
                                    <FormGroup md="12" className="mb-2 mr-sm-2 mb-sm-0 space-bw">
                                            <Button className="submit" onClick={this.etCodePasswordSubmit}>Submit</Button>{' '}
                                    </FormGroup>
                                </Col>
                            </Row>
                            <p>{this.state.inValidPassword}</p>
                        </>
                        :
                        <>
                        {this.getModalContent()}
                        <Row className = "mt-3">
                            <Col md="4 ">
                                <p>Please take picture for user record</p>
                                <Row>
                                    {this.props.scannedData.data.userImage?
                                    <Col md="12" className="m-r-camera">
                                        {this.props.scannedData.data.userImage.imageUrl?<img src={this.props.scannedData.data.userImage.imageUrl}/>:null}
                                    </Col>
                                    :
                                    <Col md="12" className="m-r-camera">
                                    {this.state.isScanned===false?
                                        <Camera
                                            onTakePhoto = { (dataUri) => { this.onTakePhoto(dataUri); } }
                                            onCameraError = { (error) => { this.onCameraError(error); } }
                                            idealFacingMode = {FACING_MODES.ENVIRONMENT}
                                            idealResolution = {{width: 200, height: 180}}
                                            imageType = {IMAGE_TYPES.PNG}
                                            imageCompression = {2.57}
                                            isMaxResolution = {false}
                                            isImageMirror = {false}
                                            isSilentMode = {true}
                                            isDisplayStartCameraError = {true}
                                            isFullscreen = {true}
                                            sizeFactor = {1}
                                            onCameraStart = { (stream) => { this.onCameraStart(stream); } }
                                            onCameraStop = { () => { this.onCameraStop(); } }
                                        />
                                        :
                                            null
                                            // <img src={localStorage.message}/>
                                    }
                                    </Col>
                                    }
                                    <Col md="12" className="m-r-camera" style={{marginTop:'30px'}}>
                                        or Uplaod File: <input type="file" name="fileToUpload" id="fileToUpload" onChange={(e)=>{this.uploadFile(e)}}></input>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md="col-sm-12 col-md-6" className="space-bw">
                                <Table bordered className="m-custom-table">
                                            <tbody>
                                                <tr>
                                                    <th>Name</th>
                                                    <td>{this.props.scannedData.data.purchaseDetails.name}</td>
                                                </tr>
                                                <tr>
                                                    <th>Email</th>
                                                    <td>{this.props.scannedData.data.purchaseDetails.email}</td>
                                                </tr>
                                                {/* <tr>
                                                    <th>Phone</th>
                                                    <td>{this.props.scannedData.data.purchaseDetails.phoneNumber? this.props.scannedData.data.purchaseDetails.phoneNumber: '-'}</td>
                                                </tr> */}
                                                <tr>
                                                    <th>Venue</th>
                                                    <td>{this.props.scannedData.data.eventInfo.venue.address}</td>
                                                </tr>
                                                <tr>
                                                    <th>Ticket Class</th>
                                                    <td>{this.props.scannedData.data.eventInfo.ticketClassInfo.ticketClassName}</td>
                                                </tr>
                                                <tr>
                                                    <th>Date</th>
                                                    <td>
                                                        {/* <Moment format="YYYY-MM-D"> {this.props.scannedData.data.eventInfo.eventDateTimeSlot.eventStartTime} </Moment> */}
                                                        {moment(this.props.scannedData.data.eventInfo.eventDateTimeSlot.eventStartTime).utc().format("MMM DD")}
                                                            {' '} - {' '}
                                                        {moment(this.props.scannedData.data.eventInfo.eventDateTimeSlot.eventEndTime).utc().format("MMM DD")}
                                                        {/* <Moment format="D MMM YYYY" withTitle> {this.props.scannedData.data.eventInfo.eventDateTimeSlot.eventEndTime} </Moment> */}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Last Scanned</th>
                                                    <td>{this.props.scannedData.data.lastScanAt?this.props.scannedData.data.lastScanBy:'Null'}</td>
                                                </tr>
                                                <tr>
                                                    <th>Last Sccanned by</th>
                                                    <td>{this.props.scannedData.data.lastScanBy?this.props.scannedData.data.lastScanBy:'Null'}</td>
                                                </tr>
                                                <tr>
                                                    <th>Scan Count</th>
                                                    <td className="link" onClick={() => {
                                    this.openDeleteModal(this.props.scannedData.data);
                                  }}>{this.props.scannedData.data.scanCount} Scans</td>
                                                </tr>
                                            </tbody>
                                </Table>   



                                {/* scan ticket actions */}
                                <Col md="col-sm-12 col-md-6" className="space-bw">
                                    <Button className="submit" onClick={this.verifyTicket}>Verify</Button>{' '}
                                    <Button color="danger" onClick={this.toggle}>Rejected</Button>
                                </Col>
                                {/* scan ticket actions */}


                                {/* verify msg */}
                                <Col md="col-sm-12 col-md-6" className="space-bw">
                                    <h4 className="red">{this.state.error}{this.state.successImage}</h4>
                                    <h4 className="red">{this.state.verifySuccessfully}</h4>
                                </Col>
                                {/* verify msg */}

                                {/* rejected sacn model */}
                                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                                    <ModalHeader toggle={this.toggle}>Ticket Rejected</ModalHeader>
                                    <ModalBody>
                                        <p>Please write reason of rejection!</p>
                                        <Input type="textarea" name="text" id="exampleText" value={this.state.rejectedValue} onChange={(value)=>this.onRejectedValue(value)}/>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="primary" onClick={this.toggle}>Send</Button>{' '}
                                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                                {/* rejected sacn model */}
                            </Col>
                        </Row>
                        </>
                        }
                        
            </>
        )
    }
}