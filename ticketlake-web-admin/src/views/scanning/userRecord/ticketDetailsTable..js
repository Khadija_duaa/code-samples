import React from 'react';
import { Table, Button } from 'reactstrap';
import {withRouter, Link, Switch, Route} from "react-router-dom";
export default class ticketDetailsTable extends React.Component {
    constructor(props){
        super(props)
    }
    
    render() {
        console.log('props.ticketClassesConfig eventSlotId', this.props,  this.props.ticketClassesConfig.ticketClassName)
        console.log('this.props.ticketClasses', this.props.ticketClasses)
        console.log('this.props.ticketClassesConfig.length===0', this.props.ticketClassesConfig.length)


        var _ticketClassesConfig = this.props.ticketClassesConfig;
        var _ticketClasses = this.props.ticketClasses.ticketClasses;

            // var result = [];
            // for(var i=0;i<_ticketClassesConfig.length;i++){
            //     console.log('for _ticketClassesConfig', _ticketClassesConfig)
            //     console.log('for _ticketClasses', _ticketClasses)
            //     // _ticketClassesConfig.map((list, i)=>{
            //     //     console.log('map list', list._id)
            //     //     list.push(list[i], _ticketClasses[i])
            //     // })
            //     result.push(_ticketClassesConfig[i],_ticketClasses[i]);
            // }

            // var hash = new Map();
            // _ticketClassesConfig.concat(_ticketClasses).forEach(function(obj) {
            //     hash.set(obj._id===obj.ticketClassId, Object.assign(hash.get(obj.ticketClassId) || {}, obj))
            // });
            // var a3 = Array.from(hash.values())
            let availableTickets =[]
            _ticketClassesConfig.map((ticket, index)=> {
                if(_ticketClasses[index]){
                    if(ticket._id === _ticketClasses[index].ticketClassId) 
                            availableTickets.push(
                                {...ticket, ..._ticketClasses[index]}
                            ) 
                }
            })

        console.log('sssss',availableTickets)

        console.log('_ticketClassesConfig', _ticketClassesConfig)
        console.log('_ticketClasses', _ticketClasses)
        // console.log('result', result.map((list,i)=>{return list?list:null}))
        // console.log('result2', result)

        // const {userName, email, phone, venue, ticketClass, dateStart, dateEnd, lastScanned, lastScannedBy, scanCount, scannedTickets} = this.props
        return (
            <>

                {/* {this.props.ticketClassesConfig.length===0?
                                        "LOADING..."
                                    :
                                        <>
                                            <p>not loading</p>
                                        </>
                } */}
                <h3 className="m-t-30"><b>Available Tickets</b></h3>
                <Table bordered className="m-custom-table" responsive>
                        <thead>
                            <tr>
                                <th>Event Class</th>
                                <th>Price</th>
                                <th>Available Tickets</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                                        {availableTickets.map((ticket,i) => {
                                            console.log('ssssssssssdddd', ticket)
                                                return (
                                                    <tr key={i}>
                                                        <td>{ticket.ticketClassName}</td>
                                                        <td>{ticket.ticketClassPrice} $</td>
                                                        <td>{ticket.availableTickets}</td>
                                                        <td><Link to={{ 
                                                            pathname: `/tickets/events/detail/${this.props.eventSlotId}/${ticket._id}`,
                                                            ticket: ticket,
                                                            }} ><Button outline color="primary">Purchase</Button></Link></td>
                                                    </tr>

                                                )
                                            })
                                        }
                        </tbody>
                </Table>`
            </>
        )
    }
}