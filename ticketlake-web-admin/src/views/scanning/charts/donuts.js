import React from 'react';
import Chart from 'react-c3-component';
import 'c3/c3.css';
import { Link } from "react-router-dom";

import {Row, Col,Button} from 'reactstrap';

class DonutsCharts extends React.Component {


	render() {
		console.log('this.props.donut', this.props)
		return <div>
			<Row>
                <Col md="12">
							{/* <CardTitle>Donut Chart</CardTitle> */}
							<Chart
								config={{
									data: {
										columns: [
											["Scanned: " + this.props.scannedTickets, this.props.scannedTickets],
											["Unscanned: " + this.props.totalAvailableTickets, this.props.totalAvailableTickets]
										],
										type: "donut",
										onclick: function (o, n) { },
										onmouseover: function (o, n) { },
										onmouseout: function (o, n) { }
									},
									donut: { title: "Total Scanned" },
									color: { pattern: ["#ff0000", "#bdbdbd", "#f62d51"] }
								}}
							/>
				</Col>
                <Col md="12" className="scan-btn-wrap">
                             <Button color="danger">
                                <Link
                                    to={{
                                    pathname: '/alltickets/scanning/', 
                                    eventId: this.props.eventId.iv._id,
                                  }}
                                >Scan Ticket </Link></Button>{' '}
                             {/* <Button color="secondary">View Scanned Tickets</Button> */}
                </Col>
			</Row>
		</div>
	}
}

export default DonutsCharts;
