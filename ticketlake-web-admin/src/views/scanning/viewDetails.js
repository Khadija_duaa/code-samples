import React from 'react';
import { Row, Col } from 'reactstrap';
import RecordTable from './userRecord/table';
import DonutsCharts from './charts/donuts';
import TicketDetailsTable from './userRecord/ticketDetailsTable.';
import Axios from '../../utils/axios';
class viewDetails extends React.Component {
    constructor(props){
        super(props)
        this.state={
            detailsPage:'',
            parentEventInfo:'',
            eventDateTimeSlot:'',
            venue:'',
            ticketClasses:'',
        }
    }
    componentDidMount(){
        Axios.get(`/events/event-slot-detail/${this.props.location.event}`)
                .then((response) => {
                    this.setState({
                        detailsPage:response.data.data,
                        parentEventInfo:response.data.data.parentEventInfo,
                        eventDateTimeSlot:response.data.data.eventDateTimeSlot,
                        venue:response.data.data.venue,
                        ticketClasses:response.data.data,
                    })
                })
                .catch(err => {
                });
        if(!this.props.location.event){
            this.props.history.push('/alltickets')
        }

       

    }
    render() {
        console.log('this.props.location.event', this.props.location.event)
        return (
                 <>   
            {this.props.location.event?
                    <>
                    {this.state.parentEventInfo.length===0?
                        "LOADING..."
                    :
                        <>
                            <Row>
                                <Col md="12">
                                    
                                    <h3><b>{this.state.parentEventInfo.title} Detail for Scanning</b></h3>
                                </Col>
                            </Row>
                            <Row className="space-bw">
                                <Col md="7">
                                    <RecordTable
                                        eventName={this.state.parentEventInfo.title}
                                        venue={this.state.venue.address}
                                        eventType={this.state.parentEventInfo.eventType}
                                        dateStart={this.state.eventDateTimeSlot.eventStartTime}
                                        dateEnd={this.state.eventDateTimeSlot.eventEndTime}  
                                        scannedTickets={~~((this.state.detailsPage.scannedTicketsCount*100)/this.state.detailsPage.totalAvailableTickets) + '% ('+ this.state.detailsPage.scannedTicketsCount + '/' + this.state.detailsPage.totalAvailableTickets + ')'}
                                        lastScanned='-'
                                        lastScannedBy='-'
                                        scanCount={this.state.detailsPage.scannedTicketsCount}
                                    />
                                </Col>
                                <Col md="5">
                                    <Row>
                                        <Col md="12">
                                            <DonutsCharts 
                                                eventId={this.props.location} 
                                                scannedTickets={this.state.detailsPage.scannedTicketsCount}
                                                totalAvailableTickets={this.state.detailsPage.totalAvailableTickets}
                                                />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>

                            <Row>
                                <Col md="12">
                                    <TicketDetailsTable 
                                        ticketClasses={this.state.ticketClasses} 
                                        ticketClassesConfig={this.state.parentEventInfo.ticketClassesConfig}
                                        eventSlotId={this.props.location.iv.eventSlotId}
                                    />
                                </Col>
                            </Row>
                        </>
                    }
                        
                    </>
                : null
            }
            </>
               
        )
    }
}

export default viewDetails;