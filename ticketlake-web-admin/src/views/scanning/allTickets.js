import React from 'react';
import {
  Table,
  Row,
  Col,
  Card,
//   Button,
//   Form,FormGroup,InputGroup,InputGroupAddon,Input
} from 'reactstrap';
import {connect} from "react-redux";
import {getAllEvents,searchEvents,setEvent,getEventDetail} from "../../redux/actions/events";
import {loadCategories} from "../../redux/actions/eventCategory/index";
import {withRouter, Link, Switch, Route} from "react-router-dom";
import TableHeads from '../../utils/Headers';
import Loader from '../../utils/loader';
import Scanning from '.';
import viewDetails from './viewDetails';
import axios from '../../utils/axios';

const header = ["Name", "Category", "Location", "Action"];
// const searchFields = ['eventTitle', '_id','eventTitle', 'eventTitle', 'eventDateTimeSlot.eventStartTime', 'eventDateTimeSlot.eventEndTime'];
// const country = [
//     {
//         name: "All"
//     },
//     {
//         name: "Pakistan"
//     }
// ];

class AllTickets extends React.Component {
    state = {
        paginate: false,
        page:0,
        skip:0,
        pageSize:10,
        country:"",
        city:"",
        celebrities: [],
        categories: [],
        isFeatured: false,
        isPublished:true,
        isDraft: false,
        from: "",
        to: "",
        search: "",
        value:'',
        error:'',
        allEventsForScanning:[],
        processing:true,
    };

    componentDidMount() {
        axios.post('/events/get-staff-events/today')
        .then((response)=>{
            console.log('/events/get-staff-events/today', response.data.data)
            this.setState({
                allEventsForScanning:response.data.data,
                processing:false,
            })
        })
        .catch((error)=>{
             this.setState({
                // error:error.response.data._error?error.response.data._error : null
            })
        });
        // let s ={...this.state};
        // this.props.getAllEvents(s);
        // this.props.loadCategories();
    }

    // getFilters = (state) => {
    //     let _state ={...state};
    //     if (state.country === 'All') {
    //         this.props.getAllEvents(_state)
    //     }
    //     else {
    //         this.props.getAllEvents(_state,state.country)
    //     }
    // };  
    // getCategoryFilters = (state) => {

    //     let _state ={...state};
    //     console.log('its sttae before data',_state);
    //     if (state.categories !== []) {
    //         this.props.getAllEvents(_state, '', state.categories)
    //     } else {
    //         return null;
    //     }
    // };


    // onInputChange = (e) => {
    //     let {target} = e;
    //     let state = {...this.state};
    //     state[target.name] = target.value;
    //     console.log('its sttae data',state)
    //     this.setState(state);
    //     this.getFilters(state);
    // };

    // onCategoryChange = (e) => {
    //     let {target} = e;
    //     let state = {...this.state};
    //     let data = [];
    //     data.push(target.value);
    //     state[target.name] = data;
    //     this.setState({...state});
    //     this.getCategoryFilters(state);
    // };

    renderAllEventData = () => {
        // if(!this.props.events) return null;
        return (
            this.state.allEventsForScanning.map((event, i) => {
                // console.log('all event in sccanning', event)
                return (
                    <tr key={i}>
                        <td>
                            {event.eventTitle} 
                            {/* - {event._id} */}
                        </td>
                        <td>
                                {event.categories?event.categories.map((cat,i)=>{
                                    return <span key={i}>{cat.name?cat.name:'Category not assign'} </span>
                                    }):''
                                }
                        </td>
                        <td>
                            {event.venue?event.venue.address:''}
                        </td>
                        <td>
                            <span className="view p-r-20" key="view">
                                <Link to={{ 
                                         pathname: `/alltickets/detailsview/${event.eventSlotId}`,
                                         event: event.eventSlotId,
                                         iv:event
                                        }} >View Details</Link>
                            </span>
                            <span className="scan">
                                <Link
                                    to={{
                                    pathname: '/alltickets/scanning/', 
                                    eventId: event._id,
                                    eventData:event,
                                  }}
                                ><i className="fas fa-address-card"></i> Scans</Link>
                            </span>
                        </td>
                    </tr>)
            })

        );
    };



    // searchField = () => {
    //     return (
    //         <Form className="form-horizontal" style={{float:'right'}}>
    //             <FormGroup>
    //                 <InputGroup>
    //                     <Input type="text" id="input1-group2" name="input1-group2" placeholder="Type here..."
    //                            onChange={(e) => this.props.searchEvents(e.target.value, searchFields)}/>
    //                     <span className="search_icon"><i className="fa fa-search"/></span>
    //                 </InputGroup>
    //             </FormGroup>
    //         </Form>
    //     );
    // };

    // categoryFilter = () => {
    //     return (
    //         <select data-placeholder="Category" value={this.state.categories}
    //                 name="categories"
    //                 onChange={this.onCategoryChange}
    //                 style={{
    //                     float: 'left',
    //                     height: '30px',
    //                     width: '150px',
    //                     marginBottom: '10px',
    //                     paddingLeft: '10px',
    //                     borderRadius: '4px'
    //                 }}
    //                 className="chosen-select no-search-select">
    //             <option disabled selected>Select Category</option>
    //             <option>All</option>
    //             {
    //                 Array.isArray(this.props.categories) && this.props.categories.map((x, i) => {
    //                     return (
    //                         <option key={i} value={x._id}>{x.categoryName}</option>
    //                     )
    //                 })
    //             }
    //         </select>

    //     );
    // };

    // countryFilter = () => {
    //     return (
    //         <select name="country" value={this.state.country} onChange={this.onInputChange}
    //                 style={{
    //                     float: 'left',
    //                     height: '30px',
    //                     width: '150px',
    //                     marginBottom: '10px',
    //                     paddingLeft: '10px',
    //                     borderRadius: '4px'
    //                 }}
    //                 className="chosen-select no-search-select">
    //             <option disabled selected >Select Country</option>
    //             {
    //                 Array.isArray(country) && country.map((data, i) => {
    //                     return (
    //                         <option key={i} value={data.name}>{data.name}</option>
    //                     )
    //                 })
    //             }
    //         </select>

    //     );
    // };


    getTableHeaders = () => {
        return (
            <TableHeads>
                {header}
            </TableHeads>
        );
    };


    render() {
        return <div>
            {this.state.processing==='true' ? <Loader/> :
                <Switch>
                    <Route path={'/alltickets/scanning'} component={Scanning}/>
                    <Route path={'/alltickets/detailsview'} component={viewDetails}/>
                <Row key={+1}>
                <Col md="12">
                    <h3><b>Tickets Scanning</b></h3>
                </Col>
                    <Col>
                        <Row>
                            <Col md={2} >
                                {/* {this.categoryFilter()} */}
                            </Col>
                            <Col md={2} >
                                {/* {this.countryFilter()} */}
                            </Col>
                            <Col sm={8}>
                                {/* {this.searchField()} */}
                            </Col>

                        </Row>
                        <Row>
                            <Col md="12">
                                <Card>
                                    <Table bordered hover striped responsive>
                                        <thead>
                                        {this.getTableHeaders()}
                                        </thead>
                                        <tbody>
                                        {this.renderAllEventData()}
                                        </tbody>
                                    </Table>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                </Switch>
            }

        </div>
  }
}

const mapStateToProps = (state) => {
    return {
        // processing: state.Events.processing,
        // events: state.Events.allEvents,
        // categories: state.eventCategories.categories,
        // message: state.Events.message,
        // error: state.Events.error,
    }
};

const connectedComponent = connect(mapStateToProps, {getAllEvents,searchEvents,setEvent,getEventDetail,loadCategories})(AllTickets);
export default withRouter(connectedComponent);
