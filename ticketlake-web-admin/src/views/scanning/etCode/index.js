import React from 'react';
import { Row, Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import UserRecord from '../userRecord';
import axios from '../../../utils/axios';

export default class etCode extends React.Component {
    constructor(props){
        super(props)
        this.state={
            eventId:'',
            srNo:'',
            isRecord:false,
            error:null,
            scannedData:[],
        }
    }
    changeETCode=(e)=>{
        console.log(e.target.value)
        this.setState({
            srNo:e.target.value,
        })
    }
    componentDidMount(){
        console.log('this.props.location.eventId', this.props.location.eventId)
        console.log('this.props.location.eventData', this.props.location.eventData)
        if(this.props.location.eventId===undefined){
            this.props.history.push('/alltickets')
        }
    }
    etCodeSubmit=(e)=>{
        e.preventDefault();
        console.log('submited et code', this.state.srNo)
        axios.post('/tickets/validate-qrcode', {
            "eventId": this.props.location.eventId,      //this time its static 
            "srNo": this.state.srNo,            //this time its static 
        })
        .then((response)=>{
            console.log('response', response);
            this.setState({
                isRecord:true,
                scannedData:response.data
            })
        })
        .catch((error)=>{
            // console.log('error', error.response.data._error);
            this.setState({
                error:error?error.response.data._error:'error occured',
            })
        });
        
    }
    etCodeClear=()=>{
        this.setState({
            etCode:''
        })
        this.props.history.push('/alltickets')
    }
    render() {
        console.log('this.props.location.eventId', this.props.location.eventId)
        console.log('this.state.isRecord', this.state.isRecord)
        return (
            <>
                {/* while input data is true */}
                {this.state.isRecord===false ?
                    <>
                        <Row>
                            <Col md="3">
                                <h3><b>ET-Code</b></h3>
                            </Col>
                        </Row>
                        <Row className = "mt-3">
                            <Col md="12">
                                <p className="space-bw">Please Adjust the ET Code in scanning area</p>
                            </Col>
                            <Col md="4">
                                <Form inline onSubmit={this.etCodeSubmit}>
                                    <FormGroup className="mb-4 mr-sm-2 mb-sm-0">
                                        <Label for="exampleEmail" className="mr-sm-2">Code</Label>
                                        <Input type="text" name="et-text" id="etText" placeholder="i.e, 090710X" value={this.state.srNo} onChange={(value)=>this.changeETCode(value)}/>
                                    </FormGroup>
                                </Form>
                                
                                <FormGroup md="12" className="mb-2 mr-sm-2 mb-sm-0 space-bw rd">
                                            {this.state.error}
                                </FormGroup>
                                <FormGroup md="12" className="mb-2 mr-sm-2 mb-sm-0 space-bw">
                                        <Button className="submit" onClick={this.etCodeSubmit}>Submit</Button>{' '}
                                        <Button outline color="secondary" onClick={this.etCodeClear}>Cancel</Button>
                                </FormGroup>
                            </Col>
                        </Row>
                    </> 
                    :
                    <>
                        <UserRecord eventId={this.state.eventId} eventData={this.props.location.eventData} srNo={this.state.srNo} scannedData={this.state.scannedData}/>
                    </>
                }

            </>
        )
    }
}