import React from 'react';
import {
    Table,
    Row,
    Col,
    Card,
    CardBody,CardHeader,
    CardTitle,
    CardSubtitle,Button,
    Form,FormGroup,InputGroup,InputGroupAddon,Input
} from 'reactstrap';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import TableHeads from '../../utils/Headers';
import Loader from '../../utils/loader';
import TicketGeneration from "../TicketGeneration";

const header = ["Ticket Id", "Purchased by", "Purchased Date", "Scan Count", "Last Scan", "Scanned by"];
const data=[
    {
        "ticketId":'123',
        "purchasedBy":'Harry O Enstain',
        "purchasedDate":'03-08-19',
        "ScanCount":'20',
        "LastScan":'05-08-19',
        "ScannedBy":'lawrance O Vivo'
    },
    {
        "ticketId":'123',
        "purchasedBy":'Harry O Enstain',
        "purchasedDate":'03-08-19',
        "ScanCount":'20',
        "LastScan":'05-08-19',
        "ScannedBy":'lawrance O Vivo'
    },
    {
        "ticketId":'123',
        "purchasedBy":'Harry O Enstain',
        "purchasedDate":'03-08-19',
        "ScanCount":'20',
        "LastScan":'05-08-19',
        "ScannedBy":'lawrance O Vivo'
    },
    {
        "ticketId":'123',
        "purchasedBy":'Harry O Enstain',
        "purchasedDate":'03-08-19',
        "ScanCount":'20',
        "LastScan":'05-08-19',
        "ScannedBy":'lawrance O Vivo'
    },
    {
        "ticketId":'123',
        "purchasedBy":'Harry O Enstain',
        "purchasedDate":'03-08-19',
        "ScanCount":'20',
        "LastScan":'05-08-19',
        "ScannedBy":'lawrance O Vivo'
    },
];

const Type = [{name: 'Ticket', value: 'Ticket'}, {name: ' Pass', value : 'Pass'}];

const TicketState = [{name: 'Scanned', value: 'Scanned'}, {name: 'Unscanned', value : 'Unscanned'}];

class Tickets extends React.Component {


    state = {
    };

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    handleInput =(e) => {
        this.setState({to: e.target.value, from : e.target.value});
    };


    renderAllTicketsData = () => {
        return (
            data.map((ticket, i) => {
                return (
                    <tr key={ticket._id}>
                        <td>
                            {ticket.ticketId}
                        </td>

                        <td>
                            {ticket.purchasedBy}
                        </td>

                        <td>
                            {ticket.purchasedDate}
                        </td>
                        <td>
                            {ticket.ScanCount}
                        </td>
                        <td>
                            {ticket.LastScan}
                        </td>
                        <td>
                            {ticket.ScannedBy}
                        </td>
                    </tr>)
            })

        );
    };



    searchField = () => {
        return (
            <Form className="form-horizontal" style={{float:'right'}}>
                <FormGroup>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="submit" disabled color="danger"><i className="fa fa-search"/> Search</Button>
                        </InputGroupAddon>
                        <Input type="text" id="input1-group2" name="input1-group2" placeholder="Type here..."/>
                    </InputGroup>
                </FormGroup>
            </Form>
        );
    };

    typeFilter = () => {
        return (
            <select   style={{float:'left',height:'30px',width:'120px', marginBottom : '10px',marginRight:'10px' ,paddingLeft:'10px'}}>
                {
                    Type.map(type => {
                        return (
                            <option key={type.name} value={type.value}  >
                                {type.name}
                            </option>
                        )
                    })
                }
            </select>
        );
    };


    stateFilter = () => {
        return (
            <select   style={{float:'left',height:'30px',width:'120px', marginBottom : '10px', paddingLeft:'10px'}}>
                {
                    TicketState.map(state => {
                        return (
                            <option key={state.name} value={state.value}  >
                                {state.name}
                            </option>
                        )
                    })
                }
            </select>
        );
    };


    getTableHeaders = () => {
        return (
            <TableHeads>
                {header}
            </TableHeads>
        );
    };


    render() {
        return <div>
            {this.props.processing ? <Loader/> :
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader >
                                <Row style={{display:'flex'}}>
                                    <Col>
                                        <h3>Purchased | Scanned Tickets</h3>
                                    </Col>
                                    <Col>
                                <FormGroup style={{float:'right'}}>
                                    <Button style={{backgroundColor:'red', border:'none'}} className="button" onClick={() => this.props.history.push('/tickets/events/ticket-list/ticket-generation')}>
                                        Generate Ticket
                                    </Button>
                                </FormGroup>
                                    </Col>
                                </Row>
                            </CardHeader>
                            <CardBody>
                                {this.typeFilter()}
                                {this.stateFilter()}
                                {this.searchField()}

                                <Table bordered hover striped responsive>
                                    <thead>
                                    {this.getTableHeaders()}
                                    </thead>
                                    <tbody>
                                    {this.renderAllTicketsData()}
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            }

        </div>
    }
}

const mapStateToProps = (state) => {
    return {

        ticketClass :state.Events && state.Events.ticketClass && state.Events.ticketClass.ticketClasses,
        event:state.Events.event,
    }
};

const connectedComponent = connect(mapStateToProps, null)(Tickets);
export default withRouter(connectedComponent);
