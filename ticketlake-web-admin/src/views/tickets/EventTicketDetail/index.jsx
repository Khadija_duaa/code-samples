import React from 'react'
import { getEventDetail } from '../../../redux/actions/eventSlot'
import {generateTicket, getEventPurchasedTickets, resetTicketState} from '../../../redux/actions/tickets'
import { connect } from 'react-redux'
import { Row, Col, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input, FormText, Card,CardBody,CardTitle} from 'reactstrap'
import _ from 'lodash'
import {NotificationManager} from 'react-notifications';
import TicketPurchasedList from '../../../components/TicketPurchasedList'
import Chart from 'react-c3-component';
import 'c3/c3.css';
class EventTicketDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            isOffline: 0,
            email: "",
            phoneNumber: "",
            name: "",
            DOB: "",
            ticketClassId: "",
            eventId: "",
            mainEventId: "",
            userId: "",
            errors: {},
            seatNumber: "",
            searchString: "",
            availableTickets: [],
            isDisabled: false,
            error: false
        };
    
        this.toggle = this.toggle.bind(this);
      }
    
    toggle() {
        if(this.props.Slot && new Date(this.props.Slot.eventDateTimeSlot.eventStartTime) < new Date()) {
            return NotificationManager.error('Event Date passed, ticket cannot be generated', 'Error', 3000);
        }
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    handleInputChange = (event) => {
        const {target} = event;
        this.setState({
            [target.name]: target.value,
            errors: {...this.state.errors, [target.name]: false}
        })
    }
    refreshEventDetail = () => {
        if (this.props.match.params.eventSlotId !== "") this.props.getEventDetail(this.props.match.params.eventSlotId)
        if (this.props.match.params.eventId !== "") this.props.getEventPurchasedTickets(this.props.match.params.eventId)
    }
    componentDidMount() {
        window.scrollTo(0, 0)
        this.refreshEventDetail()
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.Tickets.success) {
            NotificationManager.success("Ticket purchased successfully","Success",3000)
            this.props.resetTicketState()
            this.toggle()
            this.refreshEventDetail()
         }
        else if(nextProps.Tickets.error) {
            NotificationManager.error(nextProps.Tickets.data,"Error",3000)
            this.props.resetTicketState()
            this.toggle()
            this.refreshEventDetail()
         } 
       if(this.props.Slot !== nextProps.Slot) {
           const {ticketClasses, parentEventInfo} = nextProps.Slot
           const _state = {...this.state}
           if(typeof ticketClasses !== 'undefined' && ticketClasses.length >0) {
              ticketClasses.map((ticket, index) => {
                  if (typeof parentEventInfo.ticketClassesConfig[index] !== 'undefined' && parentEventInfo.ticketClassesConfig[index]._id === ticket.ticketClassId) {
                      _state.availableTickets[ticket.ticketClassId] = ticket.availableTickets
                  }
              }) 
              this.setState(_state, function() {
              })
           }
       }

    }
    checkSeatAvailability = () => {
        const result = _.find(this.props.Slot.ticketClasses, ['ticketClassId', this.state.ticketClassId])
        if (typeof result != 'undefined') {
            this.setState({
                seatNumber: result.seatNumber
            })
            return true
        }
        return false
    }
    generateTIcket = () => {
        let flag = true
        const _state = {...this.state}
        if (this.state.email === "") {
            _state.errors.email = true
            _state.errors.emailMsg = "Email is required field"
        }
        else if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email)) {
			_state.errors.email = true
			_state.errors.emailMsg = "Email format is not correct"
			flag = false;
        }
        if (_state.name === "") {
            _state.errors.name = true
            flag = false
        }
        if(_state.ticketClassId === "") {
            flag = false
            _state.errors.ticketClassId = true
            _state.errors.ticketClassIdMsg = "Ticket Selection is required"
        }
        else if(_state.ticketClassId !== "") {
            const result = this.checkSeatAvailability()
            if(typeof result === 'undefined') {
                _state.errors.ticketClassId = true
                _state.errors.ticketClassIdMsg = "Ticket Class not found"
                flag = false
            } else {
                if (result.availableTickets < 0) {
                    _state.errors.ticketClassId = true
                    _state.errors.ticketClassIdMsg = "Seats not available"
                    flag = false
                }
            }
        }

        if(!/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(this.state.phoneNumber)) {
            _state.errors.phoneNumber = true
            _state.errors.phoneNumberMsg = "Phone number is required"
            flag = false
        }
        this.setState(_state)
        if(!flag || !this.props.Slot) return
        const seat = this.generateRandomSeat()
        const ticketGenerate = {
            "mainEventId": this.props.Slot.parentEventInfo._id,
            "eventId": this.props.Slot._id,
            "eventSlotId": this.props.match.params.id,
            "userId": "", 
            "tickets": [{
                "sectionId": "abc",
                "sectionName":"X",
                "rowNumber": 0,
                "seatNumber": seat,
                "userInfo": {
                   "name": this.state.name,
                   "phoneNumber": this.state.phoneNumber,
                   "email": this.state.email,
                   "DOB": ""
               },
               "ticketClassId": this.state.ticketClassId
            }],
            
            "purchaseType": "REGULAR",
            "isOffline": this.state.isOffline
        }
        this.props.generateTicket(ticketGenerate)
        
    }
    
    generateRandomSeat = () => {
        if(this.props.Slot) {
            const result = _.findIndex(this.props.Slot.seats.seats[0].seats[0], { 'purchased': false, 'ticketClassId':this.state.ticketClassId});
            return result
        }
    }
    onRadioBtnClick = (isOffline) => {
        this.setState({ isOffline });
    }
    handleSearchString = (event) => {
        this.setState({
            searchString: event.target.value.toLowerCase()
        })
    }
    handleDropDownChange = (event) => {
        const ticketClassId = event.target.value;
        const count = this.state.availableTickets[ticketClassId]
        let flag = true
        if (count >= 1) flag = false 
        this.setState({
            ticketClassId: ticketClassId,
            isDisabled: flag,
            error: flag
        })

    }
    renderTicketGenerationView = () => {
        const {errors} = this.state
        return (
            this.props.Slot ? 
            <Row>
                <Col md = {8}>
                    <FormGroup>
                        <Label>Name</Label>
                        <Input type="text" name ="name" onChange = {this.handleInputChange}></Input>
                        <FormText>{errors.name ? <p style = {styles.error}>Name is required field</p>: null}</FormText>
                    </FormGroup>
                    <FormGroup>
                        <Label>Phone</Label>
                        <Input type="text" name ="phoneNumber" onChange = {this.handleInputChange}></Input>
                        <FormText>{errors.phoneNumber? <p style = {styles.error}>{errors.phoneNumberMsg}</p> : null}</FormText>
                    </FormGroup>
                    <FormGroup>
                        <Label>Email</Label>
                        <Input type="text" name ="email" onChange = {this.handleInputChange}></Input>
                        <FormText>{errors.email? <p style = {styles.error}>{errors.emailMsg}</p> : null}</FormText>
                    </FormGroup>
                    <FormGroup>
                        <Label>Class</Label>
                        <Input type="select" name ="ticketClassId"  onChange = {this.handleDropDownChange} value = {this.state.ticketClassId}>
                            <option>Select Class</option>
                            {
                                this.props.Slot.parentEventInfo.ticketClassesConfig.length > 0 ? 
                                this.props.Slot.parentEventInfo.ticketClassesConfig.map(ticket => {
                                        return ( <option key = {ticket._id} value = {ticket._id}>{ticket.ticketClassName}</option>)
                                    })
                                : null
                            }
                        </Input>
                        <FormText>{errors.ticketClassId? <p style = {styles.error}>{errors.ticketClassIdMsg}</p> : null}</FormText>
                    </FormGroup>
                    <FormGroup>
                        <Button onClick = {this.generateTIcket} disabled = {this.state.isDisabled}>
                            Generate Ticket
                        </Button>
                        <FormText>{this.state.error ? <p style = {styles.error}>"Tickets not available for the selected class"</p>: null}</FormText>
                    </FormGroup>
                </Col>
            </Row> : null
        )
    }
    render() {
        return (
            <React.Fragment>
                <Row>
                <Col md="6">
					<Card>
						<CardBody>
							<CardTitle>Sales Chart</CardTitle>
							<Chart
								config={{
									data: {
										columns: [
											["option1", 30],
											["option2", 120]
										],
										type: "donut",
										onclick: function (o, n) { console.log("onclick", o, n) },
										onmouseover: function (o, n) { console.log("onmouseover", o, n) },
										onmouseout: function (o, n) { console.log("onmouseout", o, n) }
									},
									donut: { title: "Total Sale" },
									color: { pattern: ["#2962FF", "#4fc3f7", "#f62d51"] }
								}}
							/>
						</CardBody>
					</Card>
				</Col>
                <Col md="6">
					<Card>
						<CardBody>
							<CardTitle>Total Ticket</CardTitle>
							<Chart
								config={{
									data: {
										columns: [
											["option1", 50],
											["option2", 100]
										],
										type: "pie",
										onclick: function (o, n) { console.log("onclick", o, n) },
										onmouseover: function (o, n) { console.log("onmouseover", o, n) },
										onmouseout: function (o, n) { console.log("onmouseout", o, n) }
									},
									color: { pattern: ["#2962FF", "#4fc3f7", "#a1aab2"] }
								}}
							/>
						</CardBody>
					</Card>
				</Col>
                </Row>
                <Row style={{marginBottom:'40px'}}>
                    <Col md={{ size: 8}}>
                        <h2>Ticket Details for {this.props.Slot?this.props.Slot.eventTitle:''}</h2>
                    </Col>
                    <Col md={{ size: 2, offset: 2 }} className="topheadings">
                        <Button onClick={() => this.toggle()}>Generate Ticket</Button>
                    </Col>
                    <Col md="12">
                        {this.props.Slot ?
                            <Table bordered striped responsive className="white-bg">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Event Date</th>
                                        <th>Class Name</th>
                                        <th>Available Ticket</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            {this.props.Slot.eventTitle}
                                        </td>
                                        <td>
                                            {this.props.Slot.eventDateTimeSlot.eventStartTime}
                                        </td>
                                        <td>
                                        {
                                            this.props.Slot.parentEventInfo.ticketClassesConfig.map(ticket => {
                                                return (
                                                    <p>{ticket.ticketClassName}</p>
                                                )
                                            })
                                        }
                                        </td>
                                        <td>
                                            {
                                                this.props.Slot.ticketClasses.map(ticket => {
                                                    return (
                                                        <p>{ticket.availableTickets}</p>
                                                    )
                                                })
                                            }
                                        </td>

                                    </tr>
                                </tbody>
                            </Table>
                            : <h4>Data not found</h4>}
                    </Col>
                    
                    <div>
                        {/* <Button color="danger" onClick={this.toggle}>{this.props.buttonLabel}</Button> */}
                        <Modal isOpen={this.state.modal} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
                            toggle={this.toggle} className={this.props.className}>
                            <ModalHeader toggle={this.toggle}>Generate Ticket</ModalHeader>
                            <ModalBody>
                                {this.renderTicketGenerationView()}
                            </ModalBody>
                            <ModalFooter>
                                <Button color="primary" onClick={this.toggle}>Close</Button>{' '}
                            </ModalFooter>
                        </Modal>
                    </div>
                </Row>
                    <TicketPurchasedList data = {this.props.Tickets.purchasedTickets} handleSearchString = {this.handleSearchString} searchString = {this.state.searchString}/>
                
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        Slot: state.EventSlot.data,
        Tickets: state.Tickets
    }
}
const styles = {
    error: {
        color: 'red',
        fontWeight: 'bold'
    }
}
export default connect(mapStateToProps, { getEventDetail, generateTicket, resetTicketState, getEventPurchasedTickets })(EventTicketDetail)