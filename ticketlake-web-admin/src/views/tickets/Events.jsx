import React from 'react';
import {
  Table,
  Row,
  Col,
  Card,
  Label,Button,
    Form,FormGroup,InputGroup,InputGroupAddon,Input
} from 'reactstrap';
import {connect} from "react-redux";
import {getAllEvents,searchEvents,setEvent,getEventDetail} from "../../redux/actions/events";
import {loadCategories} from "../../redux/actions/eventCategory/index";
import {withRouter, Link, Switch, Route} from "react-router-dom";
import TableHeads from '../../utils/Headers';
import Loader from '../../utils/loader';
import moment from "moment";
import ticketGeneration from "../TicketGeneration";
import EventTicketDetail from './EventTicketDetail'
const header = ["Name", "Ticket Avail.", "Pass Avail.", "Start Date", "End Date"];
const searchFields = ['eventTitle', 'eventTitle', 'eventTitle', 'eventDateTimeSlot.eventStartTime', 'eventDateTimeSlot.eventEndTime'];

class Event extends React.Component {


    state = {
        paginate: false,
        page:0,
        skip:0,
        pageSize:10,
        country:"",
        city:"",
        celebrities: [

        ],
        categories: [
        ],
        isFeatured: false,
        isPublished:true,
        isDraft: false,
        from: "",
        to: "",
        search: "",
        value:'',
        eventDay: '',
        status: "ALL",
        category: ''
    };

    componentDidMount() {
        window.scrollTo(0, 0)
        let s ={...this.state};
        this.props.getAllEvents(s);
        this.props.loadCategories();
    };

    getFilters = (state) => {
        let _state ={...state};

        if (state.country === 'All') {
            this.props.getAllEvents(_state)

        }
        else {
            this.props.getAllEvents(_state,state.country)
        }

};
    getCategoryFilters = (state) => {

        let _state ={...state};
        if (state.categories !== []) {
            this.props.getAllEvents(_state)
        } else {
            return null;
        }

    };

    dateFilter = (e)  => {
        const today = new Date();
        let tomorrow = new Date();
        tomorrow.setDate(today.getDate()+1);
        const _state = {...this.state}
        const {target} = e;
        if(target.value === "today") {
            _state.from = today.toString()
            _state.to = today.toString()
        }
        else if (target.value === "tomorrow") {
            _state.from = tomorrow.toString()
            _state.to = tomorrow.toString()
        } else {
            _state.from = ""
            _state.to = ""
        }
        _state.eventDay = target.value
        this.setState(_state, function() {
            this.props.getAllEvents(this.state);
        });
        

        
    }
    onCategoryChange = (e) => {
        let {target} = e;
        let state = {...this.state};
        state.categories = []
        if (target.value === "all") state.categories = []
        else state.categories.push(target.value);
        state.category = target.value
        this.setState({...state});
        this.getCategoryFilters(state);

    };

    renderAllEventData = () => {
        if(!this.props.events) return null;
        return (
            Array.isArray(this.props.events )&& this.props.events.map((event, i) => {
                return (
                    <tr key={event.eventTitle}>
                        <td>
                            <Link to={'/tickets/events/detail/'+event.eventSlotId+'/'+event._id} onClick={() => {
                                this.props.setEvent(event);
                                this.props.getEventDetail(event.eventSlotId);

                            }}>{event.eventTitle}</Link>
                        </td>

                        <td>
                            {event.eventTitle}
                        </td>

                        <td>
                            {event.eventTitle}
                        </td>
                        <td>
                            { moment(event.eventDateTimeSlot.eventStartTime).local().format('YYYY-MM-DD HH:mm:ss')}
                        </td>
                        <td>
                            { moment(event.eventDateTimeSlot.eventEndTime).local().format('YYYY-MM-DD HH:mm:ss')}
                        </td>
                    </tr>)
            })

        );
    };



    searchField = () => {
        return (
            <Form className="form-horizontal" style={{float:'right'}}>
                <FormGroup>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="submit" disabled color="danger"><i className="fa fa-search"/> Search</Button>
                        </InputGroupAddon>
                        <Input type="text" id="input1-group2" name="input1-group2" placeholder="Type here..."
                               onChange={(e) => this.props.searchEvents(e.target.value, searchFields)}/>
                    </InputGroup>
                </FormGroup>
            </Form>
        );
    };




    getTableHeaders = () => {
        return (
            <TableHeads>
                {header}
            </TableHeads>
        );
    };


    render() {
        return <div>
            {this.props.processing ? <Loader/> :
                <Switch>
                    <Route exact = {true} path={'/tickets/events/detail/:eventSlotId/:eventId'} component={EventTicketDetail}/>
                    <Route exact = {true} path={'/tickets/events/buy/:mainEvent/:slotId'} component={ticketGeneration}/>
                <Row>

                    <Col>
                        <Row>
                            <Col md={2} >
                            {
                                    this.props.categories.length > 0 ? 
                                    <FormGroup>
                                        <Label for="categories">Event Category</Label>
                                        <Input type="select" name="categories" id="categories" onChange = {this.onCategoryChange} value = {this.state.category}>
                                            <option key={"all"} value = "all">All</option>
                                            {
                                                this.props.categories.map(category => {
                                                    return (
                                                        <React.Fragment>
                                                            
                                                            <option key={category._id} value = {category._id}>{category.categoryName}</option>
                                                        </React.Fragment>
                                                        
                                                    )
                                                })
                                            }
                                        </Input>
                                    </FormGroup> : null
                                }
                            </Col>
                            <Col md={2} >
                            {
                                <FormGroup>
                                    <Label for="categories">Event Day</Label>
                                    <Input type="select" name="eventDay" id="eventDay" onChange={this.dateFilter} value={this.state.eventDay}>
                                        <option key={"all"} value="all">All</option>
                                        <option key={"today"} value="today">Today</option>
                                        <option key="tomorrow" value = "tomorrow">Tomorrow</option>
                                    </Input>
                                </FormGroup> 
                                }
                            </Col>
                            <Col md = {2}>
                                <FormGroup>
                                    <Label>Event Type</Label>
                                    <Input type="select">
                                        <option key={"all"} value="all">All</option>
                                        <option key={"today"} value="today">Standard</option>
                                        <option key="tomorrow" value = "tomorrow">Series</option>
                                        <option key="tomorrow" value = "tomorrow">Recurring</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col sm={8} style = {styles.searchField}>
                                {this.searchField()}
                            </Col>

                        </Row>
                        <Row>
                            <Col md="12">
                                <Card>
                                    <Table bordered hover striped responsive>
                                        <thead>
                                        {this.getTableHeaders()}
                                        </thead>
                                        <tbody>
                                        {this.renderAllEventData()}
                                        </tbody>
                                    </Table>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                </Switch>
            }

        </div>
  }
}
const styles = {
    searchField: {
        right: '10px',
        position: 'absolute',
        marginTop: '28px'
    }
}
const mapStateToProps = (state) => {
    return {
        processing: state.Events.processing,
        events: state.Events.allEvents,
        categories: state.eventCategories.categories,
        message: state.Events.message,
        error: state.Events.error,
    }
};

const connectedComponent = connect(mapStateToProps, {getAllEvents,searchEvents,setEvent,getEventDetail,loadCategories})(Event);
export default withRouter(connectedComponent);
