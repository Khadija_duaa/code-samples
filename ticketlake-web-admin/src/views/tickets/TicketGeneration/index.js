import React, {Component} from './node_modules/react'
import {Col, Row, Input,Button,ButtonGroup,Label, FormGroup, FormText} from './node_modules/reactstrap'
import {connect} from './node_modules/react-redux'
import {generateTicket} from '../../../redux/actions/tickets'
import _ from 'lodash'
class TicketGeneration extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal: false,
            isOffline: 0,
            email: "",
            phoneNumber: "",
            name: "",
            DOB: "",
            ticketClassId: "",
            eventId: "",
            mainEventId: "",
            userId: "",
            errors: {},
            seatNumber: "",
            speakers: [
                {
                  "sectionLabel": "",
                  "sectionId": "", // for same sectionLabel, it should be same, for different sectionLabel, it should be unique 
                  "celebrityName": "",
                  "celebrityTitle": "",
                  "celebrityDescription": "",
                  "celebrityImageKey": {
                    "imageKey": "",
                    "imageUrl": ""
                  },
                  "links": {
                    "linkedin": "String",
                    "fb": "String",
                    "twitter": "String",
                    "instagram": "String",
                    "google": "String",
                    "other": "String"
                    },
                  "filePreview": ""
            }]
        };

    }

    addAnotherSpeaker = () => {
        const { speakers } = this.state
        let flag = false
        for (let key in speakers) {
			if(speakers[key].celebrityName === "" 
			|| speakers[key].celebrityDescription === "" || speakers[key].celebrityTitle === "" ) {
                flag = true
                break;
            }
        }
        if (flag) {
            swal("Error", "Please fill data to add more", "error")
            return
		}
        let _state = {...this.state}
        let _speaker = {
            celebrityName: "", celebrityDescription: "", celebrityTitle: "",
            "sectionLabel": "speaker", "sectionId": "1234","celebrityImageKey": {
                "imageKey": "",
                "imageUrl": ""
            }
        }
		_state.speakers = speakers.concat([_speaker])
        this.setState(_state);
    };
    handleSpeakerChange = idx => evt => {
        const newSpeaker = this.state.speakers.map((speaker, sidx) => {
            if (idx !== sidx) return speaker;
            return { ...speaker, [evt.target.name]: evt.target.value };
		});
		let _state = {...this.state}
        _state.speakers = newSpeaker
        _state.errors[evt.target.name][idx] = false

        this.setState(_state, function() {
        });
    };
    checkSeatAvailability = () => {
        const result = _.find(this.props.Slot.ticketClasses, ['ticketClassId', this.state.ticketClassId])
        if (typeof result != 'undefined') {
            this.setState({
                seatNumber: result.seatNumber
            })
            return true
        }
        return false
    }
    generateTIcket = () => {
        let flag = true
        const _state = {...this.state}
        if (this.state.email === "") {
            _state.errors.email = true
            _state.errors.emailMsg = "Email is required field"
        }
        else if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email)) {
			_state.errors.email = true
			_state.errors.emailMsg = "Email format is not correct"
			flag = false;
        }
        if (_state.name === "") {
            _state.errors.name = true
            flag = false
        }
        if(_state.ticketClassId === "") {
            flag = false
            _state.errors.ticketClassId = true
            _state.errors.ticketClassIdMsg = "Ticket Selection is required"
        }
        else if(_state.ticketClassId !== "") {
            const result = this.checkSeatAvailability()
            if(typeof result === 'undefined') {
                _state.errors.ticketClassId = true
                _state.errors.ticketClassIdMsg = "Ticket Class not found"
                flag = false
            } else {
                if (result.availableTickets < 0) {
                    _state.errors.ticketClassId = true
                    _state.errors.ticketClassIdMsg = "Seats not available"
                    flag = false
                }
            }
        }

        if(!/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(this.state.phoneNumber)) {
            _state.errors.phoneNumber = true
            _state.errors.phoneNumberMsg = "Phone number is required"
            flag = false
        }
        this.setState(_state)
        if(!flag || !this.props.Slot) return
        const ticketGenerate = {
            "mainEventId": this.props.Slot.parentEventInfo._id,
            "eventId": this.props.Slot._id,
            "userId": "", 
            "tickets": [{
                "sectionId": "abc",
                "sectionName":"X",
                "rowNumber": 0,
                "seatNumber": 1,
                "userInfo": {
                   "name": this.state.name,
                   "phoneNumber": this.state.phoneNumber,
                   "email": this.state.email,
                   "DOB": ""
               },
               "ticketClassId": this.state.ticketClassId
            }],
            
            "purchaseType": "REGULAR",
            "isOffline": this.state.isOffline
        }
        this.props.generateTicket(ticketGenerate)
        
    }
    
    generateRandomSeat = () => {
        if(this.props.Slot) {
            return _.find(this.props.Slot.seats.seats[0].seats[0], ['purchased', false])
        }
    }
    onRadioBtnClick = (isOffline) => {
        this.setState({ isOffline });
    }
    renderTicketGenerationView = () => {
        const {errors} = this.state
        return (
            this.props.Slot ? 
            <Row>
                <Col>
                    <FormGroup>
                        <Label>Name</Label>
                        <Input type="text" name ="name" onChange = {this.handleInputChange}></Input>
                        <FormText>{errors.name ? <p style = {styles.error}>Name is required field</p>: null}</FormText>
                    </FormGroup>
                    <FormGroup>
                        <Label>Phone</Label>
                        <Input type="text" name ="phoneNumber" onChange = {this.handleInputChange}></Input>
                        <FormText>{errors.phoneNumber? <p style = {styles.error}>{errors.phoneNumberMsg}</p> : null}</FormText>
                    </FormGroup>
                    <FormGroup>
                        <Label>Email</Label>
                        <Input type="text" name ="email" onChange = {this.handleInputChange}></Input>
                        <FormText>{errors.email? <p style = {styles.error}>{errors.emailMsg}</p> : null}</FormText>
                    </FormGroup>
                    <FormGroup>
                        <Label>Class</Label>
                        <Input type="select" name ="ticketClassId"  onChange = {this.handleInputChange} value = {this.state.ticketClassId}>
                            <option>Select Class</option>
                            {
                                this.props.Slot.parentEventInfo.ticketClassesConfig.length > 0 ? 
                                this.props.Slot.parentEventInfo.ticketClassesConfig.map(ticket => {
                                        return ( <option key = {ticket._id} value = {ticket._id}>{ticket.ticketClassName}</option>)
                                    })
                                : null
                            }
                        </Input>
                        <FormText>{errors.ticketClassId? <p style = {styles.error}>{errors.ticketClassIdMsg}</p> : null}</FormText>
                    </FormGroup>
                    <FormGroup>
                        <Button onClick = {this.generateTIcket} disabled = {this.props.Slot && new Date(this.props.Slot.eventDateTimeSlot.startTime) >= new Date()? true: false}>
                            Generate Ticket
                        </Button>
                    </FormGroup>
                </Col>
            </Row> : null
        )
    }
    render() {
        return(<div>Hi</div>)
    }
}

const mapStateToProps = (state) => {
    return state
}
const styles = {
    error: {
        color: 'red',
        fontWeight: 'bold'
    }
}
export default connect(mapStateToProps, { generateTicket })(TicketGeneration)