import React from 'react';
import {Button,Card,CardBody,CardFooter,CardHeader,Col,Row,} from 'reactstrap';
import {
    Form,
    Input,
    InputNumber,
  } from 'antd';
  
import {getAllOrganizations} from "../../redux/organizations/organizations-actions";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import Moment from 'react-moment';
import Axios from '../../utils/axios';


class OrgAddFormComponnet extends React.Component {
    constructor(props){
        super(props)
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                Axios.post(`/organizations/create-organization`,{
                    name : values.name,
                    email : values.email,
                    phoneNumber : values.phone,
                    description : values.description,
                    uan : values.uan,
                    ntn : values.ntn,
                    address : {
                        city : values.city,
                        country : values.country,
                        zipCode : values.zipcode,
                        latitude : values.latitude,
                        longitude : values.longitude,
                    }
                })
                .then((response) => {
                    this.props.history.push('/organizations')
                    this.props.getAllOrganizations()
                })
                .catch(err => {
                });
            }
        })
    }


    cancelOrganizations=()=>{
        this.props.history.push('/organizations')
    }


    render() {
        const { getFieldDecorator } = this.props.form;
        return (
                   <>   
                        <div>
                            <Card>
                                <Form onSubmit={this.handleSubmit} id="2ndform">
                                    <CardHeader>
                                        <p style={{fontSize: '30px'}}>Add Organization</p>
                                    </CardHeader>
                                    <CardBody>
                                    <Row>
                                        <Col md="12">
                                            <p style={{color:'black', fontSize:'15px'}}> Please fill the following input fields to add a organization</p>
                                        </Col>
                                        <Col md="6">
                                            <Form.Item
                                                label="Name"
                                                >
                                                {getFieldDecorator('name', {
                                                    rules: [{ required: true, message: 'Please input your name', whitespace: true }],
                                                })(<Input />)}
                                            </Form.Item>

                                            <Form.Item label="E-mail">
                                                {getFieldDecorator('email', {
                                                    rules: [
                                                    {
                                                        type: 'email',
                                                        message: 'The input is not valid E-mail',
                                                    },
                                                    {
                                                        required: true,
                                                        message: 'Please input your E-mail',
                                                    },
                                                    ],
                                                })(<Input />)}
                                            </Form.Item>

                                            <Form.Item label="Phone Number">
                                                {getFieldDecorator('phone', {
                                                    rules: [{
                                                            type: 'number',
                                                            message: ' ',
                                                            },
                                                    { required: true, message: 'Please input your phone number' }
                                                    ],
                                                })(<InputNumber min={1}  style={{ width: '100%' }} />)}
                                            </Form.Item>

                                            
                                                

                                            <Form.Item
                                                label="Description"
                                                >
                                                {getFieldDecorator('description', {
                                                    rules: [{ required: true, message: 'Please input your description', whitespace: true }],
                                                })(<Input />)}
                                            </Form.Item>

                                            <Form.Item
                                                label="UAN"
                                                >
                                                {getFieldDecorator('uan', {
                                                    rules: [{ required: true, message: 'Please input your UAN', whitespace: true }],
                                                })(<Input />)}
                                            </Form.Item>

                                            </Col>

                                            <Col md="6">
                                            
                                            <Form.Item
                                                label="NTN"
                                                >
                                                {getFieldDecorator('ntn', {
                                                    rules: [{ required: true, message: 'Please input your NTN', whitespace: true }],
                                                })(<Input />)}
                                            </Form.Item>
                                            
                                            <Form.Item label="City">
                                                {getFieldDecorator('city', {
                                                    rules: [{ required: true, message: 'Please input city' }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>

                                            <Form.Item label="Country">
                                                {getFieldDecorator('country', {
                                                    rules: [{ required: true, message: 'Please input country' }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>

                                            <Form.Item label="Zipcode">
                                                {getFieldDecorator('zipcode', {
                                                    rules: [{ required: true, message: 'Please input zipcode' }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>

                                            <Form.Item label="Latitude">
                                                {getFieldDecorator('latitude', {
                                                    rules: [{ required: true, message: 'Please input Latitude' }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>

                                            </Col>
                                            
                                            <Col md="12">

                                            <Form.Item label="Longitude">
                                                {getFieldDecorator('longitude', {
                                                    rules: [{ required: true, message: 'Please input Longitude' }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>    
                                        </Col>
                                        </Row>
                                    </CardBody>
                                    <CardFooter>
                                        <Row>
                                            <Col className={"col-md-1 offset-md-1"}>
                                                <Button type="submit" size="md" color="primary" >
                                                    Submit
                                                </Button>
                                                </Col>
                                                <Col md="1">
                                                <Button size="md" color="secondary" onClick={this.cancelOrganizations}>
                                                    Cancel
                                                </Button>
                                            </Col>
                                        </Row>
                                    </CardFooter>   
                                </Form>
                            </Card>
                        </div>
                   </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        organizations: state.organizations.organizations
    }
};


const OrgAddForm = Form.create()(OrgAddFormComponnet);
const connectedComponent = connect(mapStateToProps, {getAllOrganizations})(OrgAddForm);
export default withRouter(connectedComponent);