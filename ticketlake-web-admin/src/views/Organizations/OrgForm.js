import React from 'react';
import {Button,Card,CardBody,CardFooter,CardHeader,Col,Row,} from 'reactstrap';
import {
    Form,
    Input,
    Tooltip,
    Icon,
    Cascader,
    Select,
    Checkbox,
    AutoComplete,
    InputNumber,
  } from 'antd';
  
  
import Moment from 'react-moment';
import Axios from '../../utils/axios';


class OrgFormComponnet extends React.Component {
    constructor(props){
        super(props)
    }
    componentDidMount(){
        if(this.props.editValues===null){
            // this.props.history.push('/organizations')
        }
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                Axios.put(`/organizations/update-organization/${values.organizationId}`,{
                    name : values.name,
                    email : values.email,
                    phoneNumber : values.phone,
                    description : values.description,
                    uan : values.uan,
                    ntn : values.ntn,
                    address : {
                        city : values.city,
                        country : values.country,
                        zipCode : values.zipcode,
                        latitude : values.latitude,
                        longitude : values.longitude,
                    }
                })
                .then((response) => {
                    this.props.getAllOrganizations()
                    this.props.visit()
                })
                .catch(err => {
                });

                // Axios.post(`/organizations/create-organization`,{
                //     name : values.name,
                //     email : values.email,
                //     phoneNumber : values.phone,
                //     description : values.description,
                //     uan : values.uan,
                //     ntn : values.ntn,
                //     address : {
                //         city : values.city,
                //         country : values.country,
                //         zipCode : values.zipcode,
                //         latitude : values.latitude,
                //         longitude : values.longitude,
                //     }
                // })
                // .then((response) => {
                //     console.log('organizations response', response)
                //     this.props.getAllOrganizations()
                //     this.props.visit()
                // })
                // .catch(err => {
                //     console.log('err in add organization', err)
                // });
            }
        })
    }
    handleUpdate=(e)=>{
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('values')
            }
        })
    }

    cancelOrganizations=()=>{
        this.props.visit()
        // this.props.history.push('/organizations')
    }


    render() {
        // this.props.editValues?this.props.editValues:null
        const { getFieldDecorator } = this.props.form;
        // const {name, email, description, phoneNumber, uan, address, ntn,} = this.props.editValues
        const {editValues} = this.props
        return (
                   <>   
                        <div>
                            <Card>
                                <Form onSubmit={this.handleSubmit}>
                                    <CardHeader>
                                        <p style={{fontSize: '30px'}}>Update Organization</p>
                                    </CardHeader>
                                    <CardBody>
                                        <Row>
                                        <Col md="12">
                                            <p style={{color:'black', fontSize:'15px'}}> Please fill the following input fields to add a organization</p>
                                        </Col>
                                        <Col md="6">
                                            <Form.Item
                                                label="Name"
                                                >
                                                {getFieldDecorator('name', {
                                                    initialValue: editValues?editValues.name:null,
                                                    rules: [{ required: true, message: 'Please input your name', whitespace: true }],
                                                })(<Input />)}
                                            </Form.Item>

                                            <Form.Item style={{display:'none'}}>
                                                {getFieldDecorator('organizationId', {
                                                    initialValue: editValues?editValues.organizationId:null,
                                                })(<Input values={editValues?editValues.organizationId:null} type="hidden" />)}
                                            </Form.Item>

                                            <Form.Item label="E-mail">
                                                {getFieldDecorator('email', {
                                                    initialValue: editValues?editValues.email:null,
                                                    rules: [
                                                    {
                                                        type: 'email',
                                                        message: 'The input is not valid E-mail',
                                                    },
                                                    {
                                                        required: true,
                                                        message: 'Please input your E-mail',
                                                    },
                                                    ],
                                                })(<Input />)}
                                            </Form.Item>

                                            <Form.Item label="Phone Number">
                                                {getFieldDecorator('phone', {
                                                    initialValue: editValues?editValues.phoneNumber:null,
                                                    rules: [{ required: true, message: 'Please input your phone number' }],
                                                })(<InputNumber min={1} style={{ width: '100%' }} />)}
                                            </Form.Item>

                                            <Form.Item
                                                label="Description"
                                                >
                                                {getFieldDecorator('description', {
                                                    initialValue: editValues?editValues.description:null,
                                                    rules: [{ required: true, message: 'Please input your description', whitespace: true }],
                                                })(<Input />)}
                                            </Form.Item>
                                            

                                            <Form.Item
                                                label="UAN"
                                                >
                                                {getFieldDecorator('uan', {
                                                    initialValue: editValues?editValues.uan:null,
                                                    rules: [{ required: true, message: 'Please input your UAN', whitespace: true }],
                                                })(<Input />)}
                                            </Form.Item>

                                            </Col>

                                            <Col md="6">
                                            
                                            <Form.Item
                                                label="NTN"
                                                >
                                                {getFieldDecorator('ntn', {
                                                    initialValue: editValues?editValues.ntn:null,
                                                    rules: [{ required: true, message: 'Please input your NTN', whitespace: true }],
                                                })(<Input />)}
                                            </Form.Item>
                                            
                                            <Form.Item label="City">
                                                {getFieldDecorator('city', {
                                                    initialValue: editValues?editValues.address.city:null,
                                                    rules: [{ required: true, message: 'Please input city' }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>

                                            <Form.Item label="Country">
                                                {getFieldDecorator('country', {
                                                    initialValue: editValues?editValues.address.country:null,
                                                    rules: [{ required: true, message: 'Please input country' }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>

                                            <Form.Item label="Zipcode">
                                                {getFieldDecorator('zipcode', {
                                                    initialValue: editValues?editValues.address.zipCode:null,
                                                    rules: [{ required: true, message: 'Please input zipcode' }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>

                                            <Form.Item label="Latitude">
                                                {getFieldDecorator('latitude', {
                                                    initialValue: editValues?editValues.address.latitude:null,
                                                    rules: [{ required: true, message: 'Please input Latitude' }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>

                                            </Col>
                                            <Col md="12">

                                            <Form.Item label="Longitude">
                                                {getFieldDecorator('longitude', {
                                                    initialValue: editValues?editValues.address.longitude:null,
                                                    rules: [{ required: true, message: 'Please input Longitude' }],
                                                })(
                                                    <Input />
                                                )}
                                            </Form.Item>    
                                        </Col>
                                        </Row>
                                    </CardBody>
                                    <CardFooter>
                                        <Row>
                                            <Col className={"col-md-1 offset-md-1"}>
                                                <Button type="submit" size="md" color="primary" >
                                                    Update
                                                </Button>
                                                </Col>
                                                <Col md="1">
                                                {/* <Button size="md" color="secondary" onClick={this.cancelOrganizations}>
                                                    Cancel
                                                </Button> */}
                                            </Col>
                                        </Row>
                                    </CardFooter>   
                                </Form>
                            </Card>
                        </div>
                   </>
        )
    }
}

const OrgForm = Form.create()(OrgFormComponnet);
export default OrgForm;
