import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Container, Modal, ModalBody, ModalFooter, ModalHeader,
  Row,
  Table,
  Button,
} from "reactstrap";
import {Route, Switch, withRouter, Link} from "react-router-dom";
import TableHeads from "../../../utils/Headers";
import {NotificationManager} from "react-notifications";


//------connect store and import store action----//
import connect from "react-redux/es/connect/connect";
// import TicketlakeAdmins from "./TicketlakeAdmins";
import OrganizerEventsUsers from "./OrganizerEventsUsers";
// import OrganizerEvents from "./OrganizerEvents";
// import assignAdmins from "./mangeRole/assignAdmins";
import EditUserToOrganisation from "./EditUserToOrganisation";
import AddUserToOrganisation from "./AddUserToOrganisation";
// import EditUserOrgForm fro./_EditUserOrgFormorm";
import ManageRole from "./ManageRole";
//------connect store and import store action----//


const header = ["Name", "Description", "Email", "Phone","Actions"];

class ViewOrganizations extends React.Component {
  constructor(props){
    super(props)
    this.state={
        modalOpen: false,
        isClicked: false,
        title:'',
        editValues:null,
    }
}
  componentDidMount=()=>{
    if(this.props.location.OrganizerEventsUsers){
      console.log('Yes',  this.props.location.OrganizerEventsUsers.name)
      this.setState({
        OrganizerEventsUsersName:this.props.location.OrganizerEventsUsers.name
      })
    }else{
      // this.props.history.push('/organizations')
      console.log('No')
    }
  }
  
  openDeleteModal = (venue) => this.setState({modalOpen: !this.state.modalOpen,venueDeleteInfo: venue});

  getModalContent = () => {
    return (<Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal}
                   className={'modal-danger ' + this.props.className}>

        <ModalHeader toggle={this.openDeleteModal}>
          Are you sure ?
        </ModalHeader>

        <ModalBody>
          Are you sure you want to delete this Venue ?
        </ModalBody>

        <ModalFooter>
          <Button color="danger" onClick={() => { this.openDeleteModal(null); }}>
            Delete
          </Button>{' '}

          <Button color="primary" onClick={this.openDeleteModal}>
            Cancel
          </Button>

        </ModalFooter>
      </Modal>
    );
  };



  getVenueData = () => {
    // console.log('this is organization list', this.props.organizations.organizations.map(list=>{ return list.name }))
    console.log('this is organization list', this.props.organizations)
    return (
    //   this.props.organizations.organizations!==null?this.props.organizations.organizations.map((org, i) => {
    //         return (
                      <tr key={'org._id'}>
                        <td style={{width:'20%'}}> asdad </td>
                        <td style={{width:'30%'}}> asdad </td>
                        <td> asdad</td>
                        <td> asdsad </td>
                        <td> 
                            <Container>
                              <Row>
                                <Col xs={12} xl={'auto'}>
                                  <Button type="submit" size="md" color="primary" 
                                    onClick={
                                            ()=>{
                                              this.editOrganizations('org')
                                          }
                                    }> Edit </Button>
                                </Col>

                                <Col xs={12} xl={'auto'}>
                                  <Button type="submit" size="md" color="danger"  onClick={() => {
                                    this.openDeleteModal('org,i');
                                  }}>
                                    Delete
                                  </Button>
                                </Col>
                              </Row>
                            </Container>
                        </td>
                      </tr>
        //            )
        //   }):null
    );
  };

  getTableHeaders = () => {
    return (
      <TableHeads>
        {header}
      </TableHeads>
    );
  };


  getTable = () => {
    return (
      <Table bordered hover striped responsive>
        <thead>
        {this.getTableHeaders()}
        </thead>

        <tbody>
        {this.getVenueData()}
        </tbody>

      </Table>
    );
  };


  getCardBody = () => {
    return (
        <Row style={{marginTop: '10px'}}>
          <Col md={12}>
            {this.getTable()}
          </Col>
        </Row>
    );
  };


  render() {
    // console.log('this.state.isClicked', this.state.isClicked)
    // console.log('viewOrganizations index', this.props)
    // console.log('viewOrganizations index params', this.props.match.params.id)
    return (
          <>
            {this.state.isClicked?
              <div className="animated fadeIn">
               ....
              </div>
            :
            <div className="animated fadeIn">
              {this.getModalContent()}
              <Row>
                    <Switch>
                      <Route path={'/organizations/view-organization/Add-User-to-Organisation'} component={AddUserToOrganisation}/>
                      <Route path={'/organizations/view-organization/Edit-User-to-Organisation'} component={EditUserToOrganisation}/>
                      {/* <<Route path={'/organizations/view-organization/EditUserOrgForm'} component={EditUserOrgForm}/> */}
                      <Route path={'/organizations/view-organization/manage-role'} component={ManageRole}/>
                      <Col>
                        <Row>
                          <Col className="border-bottom p-b-30 m-b-20">
                            <span style={{fontSize: '25px'}}>{this.state.OrganizerEventsUsersName}</span>
                          </Col>
                        </Row>
                        <OrganizerEventsUsers organizationsId={this.props.location.OrganizerEventsUsers}/>
                      </Col>
                      
                    </Switch>
              </Row>
            </div>
          }
        </>
    );
  }
}


//connect store in footer
const connectedComponent = connect()(ViewOrganizations);
export default withRouter(connectedComponent);
