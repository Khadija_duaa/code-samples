import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
} from 'reactstrap';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {loadRoles,saveAdmin,loadAdmins} from "../../../redux/actions/admins/index";
import {NotificationManager} from "react-notifications";
import axios from '../../../utils/axios';


class AdminForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    formName: 'Add User',
    profile_picture_key:'',
    role:'',
    username:'',
    password:'',
    email:'',
    name: '',
    isError:false,
    passwordError:'',
    usernameError:'',
    isRole:false,
    isName:false,
  }
}


  componentDidMount() {
    console.log('+ admin add', this.props)
    this.props.loadRoles();
    if (this.props.admin) {
      let {profile_picture_key, password,email,name,username} = this.props.admin;
      let role =  this.props.admin.role && this.props.admin.role['_id'];
      this.state.role = role;
      this.setState({profile_picture_key,role, password,email,name,username, formName: 'Update User'});
    }
    console.log('this.props.admin form data', this.props.admin)

    if(!this.props.location.OrganizerEventsUsers){
      this.props.history.push('/organizations/')
    }
  }

  onInputChange = (e) => {
    let {target} = e;
    let state = {...this.state};
    state[target.name] = target.value;
    this.setState(state);
    if (e.target.name==='username'){
      if(e.target.value.match('^[a-zA-Z0-9]*$')){
        console.log('match')
        this.setState({
          usernameError:'',
        });
      }
      else{
        console.log('not match')
        this.setState({
            usernameError:'UserName contain only strings and numbers',
        });
      }
    }
    if (e.target.name==='password'){
      // console.log('adasd', e.target.value.length===6)
      if(e.target.value.length<6){
        this.setState({
           passwordError:'Password length sholud be at least 6 digit',
        });
      }
      else{
        this.setState({
          passwordError:'',
       });
      }
      // alert('username')
      // [A-Za-z0-9]+'
      console.log('username', e.target.value.length)
    }
    
  };

  onSaveChanges = () => {
    // console.log('lead bug', this.state.role.length)
    if(this.state.name.length===0){
      this.setState({
        isName:true
      })
    }else if(this.state.role.length===0){
        this.setState({
            isRole:true
          })
    }else if(this.state.email.length===0){
        this.setState({
            isError:true
          })
    }else{
      this.props.saveAdmin(this.state.profile_picture_key,this.state.role,this.state.username,this.state.password,this.state.email,this.state.name, () => {
          this.props.history.push(`/organizations/view-organization/${this.props.location.OrganizerEventsUsers.organizationId}`);
        },
        () => {this.props.loadAdmins()},
        () => {NotificationManager.success(this.props.admin ? this.props.message :'Created Successfully')},   
        () => {NotificationManager.error(this.props.error.response.data._error)},
        // () => {NotificationManager.error(this.props.error.response?this.props.error.response.data._error:this.props.error)},
        // console.log('save admin error', this.props.error)
        )
        // if(this.state.error){
        //       this.setState({
        //           error:this.props.error.error
        //       })
        //         // console.log('request 000',this.props.error.error.response.data._error);
        //         console.log("response error", this.state.error.response.data._error?this.state.error.response.data._error:this.state.error)
        // }else{
        //       this.setState({
        //           error:this.props.error.error
        //       })
        //     console.log("else error", this.state.error)
        // }
    }
    
  };
  onCancel=()=>{
    this.props.history.goBack()
  }
  onDrop = (e) => {
    if (e.target.files.length) {
      e.preventDefault();
      let file = e.target.files[0];
      let formData = new FormData()
      formData.append("image", file)

      axios.patch('/admins/update-profile-picture', formData)
      .then((response)=>{
          console.log('upload profile image response', response);
          // this.setState({
          //   profile_picture_key: response.data.imageKey,
          //   iconImageUrl: response.data.imageUrl
          // });
      })
      .catch((error)=>{
          console.log('upload profile image error', error);
      });
    } else {
      return null
    }
  };


  getForm = () => {
    console.log('this.state.profile_picture_key', this.state.formName==='Update Admin')
    return (
      <Form action="" method="post" className="form-horizontal">

        {this.state.formName==='Update Admin'?
            <>
                {this.props.admin?
                    <>
                          {this.props.admin.profileImage?
                                  <FormGroup row>
                                        <Col md="1">
                                              <Label style={{fontSize:'15px'}}>Profile Display</Label>
                                        </Col>
                                        <Col md="1">
                                              <img alt="FF" src={this.props.admin.profileImage?this.props.admin.profileImage.imageUrl:null}/>
                                        </Col>
                                        <Col md="1">
                                              <input type="file"
                                                    ref={uploadElement => this.uploadElement = uploadElement}
                                                    accept="image/*"
                                                    hidden
                                                    onChange={this.onDrop}
                                                    />
                                              <Button type="submit" size="md" color={this.props.admin?'light':'primary'}
                                                      onClick={e => {
                                                        e.preventDefault();
                                                        this.uploadElement.click()
                                                      }}>
                                                Upload
                                              </Button>
                                              <br />
                                        </Col>
                                  </FormGroup>
                                  :null
                            }
                      </>
                  :null
                  }
              </>
        :null
        }

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Role</Label>
          </Col>
          <Col md="11">
            <Input type="select"
                   name="role"
                   value={this.state.role}
                   autoFocus
                   id="role-select"
                   onChange={this.onInputChange}>
              <option value="" disabled selected>Select your option</option>

              {this.props.roles.length >0 ? this.props.roles.map(role => {
                  return (
                    <option key={role._id} value={role._id}>
                      {role.roleName}
                    </option>
                  )
                }
              ): null}
            </Input>
          <p className="red">{this.state.isRole===false? null :'Please select role'}</p>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Username</Label>
          </Col>
          <Col md="11">
            <Input type="text" id="username" name="username" placeholder="Enter Username" onChange={this.onInputChange}
                   required value={this.state.username}/>
            <p className="red">{this.state.usernameError}</p>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Password</Label>
          </Col>
          <Col md="11">
            <Input type="password" id="password" minLength={6} min={6} name="password" placeholder={'Enter Password'} onChange={this.onInputChange}
                   required value={this.state.password} />
            <p className="red">{this.state.passwordError}</p>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Email</Label>
          </Col>
          <Col md="11">
            <Input type="email" id="email" name="email" placeholder="Enter Email" onChange={this.onInputChange}
                   required value={this.state.email} />
            <p className="red">{this.state.isError===false? null :'Email required'}</p>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Name</Label>
          </Col>
          <Col md="11">
            <Input type="text" id="name" name="name" placeholder="Enter Name" onChange={this.onInputChange}
                   required value={this.state.name} />
            <p className="red">{this.state.isName===false? null :'Name required'}</p>
          </Col>
        </FormGroup>
      </Form>
    );
  };

  getCardFooter = () => {
    return (
      <Row>
        <Col xs="1">
          <Button type="submit" size="md" color="primary" onClick={this.onSaveChanges}>
            Submit
          </Button>
        </Col>
        <Col xs="1">
          <Button type="submit" size="md" color="secondary" onClick={this.onCancel}>
            Cancel
          </Button>
        </Col>
      </Row>
    );
  };


  render() {
    
    return (
      <div style={{width:"100%"}}>
        <Card>
          <CardHeader>
            <p style={{fontSize: '30px'}}>{this.state.formName}</p>

          </CardHeader>

          <CardBody>
            {this.getForm()}
          </CardBody>

          <CardFooter>
            {this.getCardFooter()}
          </CardFooter>
        </Card>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
    console.log('er', state.Admins.error)
  return {
    roles: state.Admins.roles,
    admin: state.Admins.admin,
    error: state.Admins.error,
  }
}

const AddUserToOrganisation = connect(mapStateToProps, {loadRoles,saveAdmin,loadAdmins})(AdminForm);
export default withRouter(AddUserToOrganisation);
