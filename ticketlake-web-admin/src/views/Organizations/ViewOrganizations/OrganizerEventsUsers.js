import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Container, Modal, ModalBody, ModalFooter, ModalHeader,
  Form,FormGroup,InputGroup,
  Row,
  Table,
  Button,
} from "reactstrap";
import { Input } from 'antd';
import {Route, Switch, withRouter, Link} from "react-router-dom";
import TableHeads from "../../../utils/Headers";
import {loadAdmins} from "../../../redux/actions/admins/index";


//------connect store and import store action----//
import connect from "react-redux/es/connect/connect";
import { getObjectValue } from "../../../utils/common-utils";
// import AddUserOrgForm from "./AddUserOrgForm";
//------connect store and import store action----//


const header = ["User Name", "Name", "Role", "Email"];
const searchFields = ['name',];

class OrganizerEventsUsers extends React.Component {
  constructor(props){
    super(props)
    this.state={
        modalOpen: false,
        isClicked: false,
        loading:true,
        title:'',
        editValues:null,
        search: "",
        value:'',
        admins:[],
        allAdmins:this.props.admins
    }
}
  componentDidMount(){
    this.props.loadAdmins()
    this.setState({
      admins:this.props.admins,
      loading:false,
    })
  }
  onInputChange = (e) => {
    let {target} = e;
    let state = {...this.state};
    state[target.name] = target.value;
    this.setState(state);
    this.getFilters(state);
};
  
  openDeleteModal = (venue) => this.setState({modalOpen: !this.state.modalOpen,venueDeleteInfo: venue});
  searchEvents = (searchStr, searchFields) =>{
      if (searchStr, searchFields) {
        let filteredEvents = [];
        this.state.admins.forEach(event => {

            for (let index = 0; index < searchFields.length; index++) {
                let param = searchFields[index];

                const _val = getObjectValue(event, param);
                if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
                    filteredEvents.push(event);
                    break;
                }
            }
        });
        this.setState({
          admins:filteredEvents
        })
    }
    else {
      this.setState({
        admins:this.state.allAdmins
      })
    }
  }
  searchField = () => {
    return (
        <Form className="form-horizontal">
            <FormGroup>
                <InputGroup>
                    <Input type="text" id="input1-group2" name="input1-group2" placeholder="Type here..."
                           onChange={(e) => this.searchEvents(e.target.value, searchFields)}/>
                    <span className="search_icon"><i className="fa fa-search"/></span>
                </InputGroup>
            </FormGroup>
        </Form>
    );
};


  manageRole = () =>{
    // alert('manageRole')
  }

  // addUsers= () =>{
  //   this.props.history.push('/organizations/view-organization/AddUserOrgForm')
  // }
  // editUsers = (data) =>{
  //   console.log('editUsers', data)
  // }

  // getAdminActions = (admin, i) => {
  //   return (
  //     <Container>
  //       <Row>
          
  //           <span className="icon-btns-1" onClick={() => {
  //             this.props.setAdmin(admin);
  //             this.props.history.push('/admins/admin-form');
  //           }}><i className="far fa-edit"></i></span>
            
  //           <span className="icon-btns-2"onClick={() => {
  //             this.openDeleteModal({_id: admin._id, i});
  //           }}><i className="far fa-trash-alt"></i></span>
  //       </Row>
  //     </Container>
  //   );
  // };

  render() {
    console.log('rizwan, organizationsId admins', this.props.admins)
    return (
          <>
            {this.state.isClicked?
              <div className="animated fadeIn">
               Loading...
              </div>
            :
            <div className="animated fadeIn m-b-20">
                <Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal} className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.openDeleteModal}>
                      Are you sure ?
                    </ModalHeader>
                    <ModalBody>
                      Are you sure you want to delete this Venue ?
                    </ModalBody>
                    <ModalFooter>
                      <Button color="danger" onClick={() => { this.openDeleteModal(null); }}>
                        Delete
                      </Button>{' '}
                      <Button color="primary" onClick={this.openDeleteModal}>
                        Cancel
                      </Button>
                    </ModalFooter>
                </Modal>
              {this.state.loading? 'loading..'
              :
              <Row>
                <Col xs="12">
                    {/* <Switch>
                      <Route path={'/organizations/AddUserOrgForm'} component={AddUserOrgForm}/> */}
                      {/* <Col md={12} style={{float: 'left'}} className="m-b-20">
                        <span style={{fontSize: '20px'}}>Events Organiser's Users</span>
                      </Col> */}
                      <Col md={12} style={{float: 'left'}}>
                        {/* <div style={{float: 'left'}}>{this.searchField()}</div> */}
                        <div style={{float: 'right'}}>
                            {/* <Link to={{
                                      pathname: `/organizations/view-organization/manage-role/`, 
                                      OrganizerEventsUsers: this.props.organizationsId,
                                      params:{id: this.props.organizationsId}
                                    }}>
                                    <Button color="primary" onClick={() => { this.manageRole() }}>
                                        Manage Role
                                    </Button>
                              </Link> {' '}
                              <Link to={{ 
                                        pathname: '/organizations/view-organization/Add-User-to-Organisation/',
                                        OrganizerEventsUsers: this.props.organizationsId,
                                        params:{id: this.props.organizationsId}
                                        }} >
                                        <Button color="success">
                                          
                                              + Add User
                                        </Button>
                                </Link> */}
                            
                        </div>
                      </Col>
                      <Col md={12} style={{float: 'left'}}>
                        <Row style={{marginTop: '10px'}}>
                          <Col md={12}>
                            <Table bordered hover striped responsive className="white-bg">
                              <thead>
                                <TableHeads>
                                  {header}
                                </TableHeads>
                              </thead>
                              <tbody>
                              {!this.props.admins.error?this.props.admins.map((data,i)=>{
                                          return(
                                            <tr key={i}>
                                              <td style={{width:'20%'}}> {data.username} </td>
                                              <td style={{width:'20%'}}> {data.name}</td>
                                              <td style={{width:'20%'}}> {data.role===null ? null : data.role.roleName}</td>
                                              <td> {data.email} </td>
                                              {/* <td style={{width:'210px'}}> 
                                                  <div style={{float: 'right'}}>
                                                        <Link to={{ 
                                                            pathname: `/organizations/view-organization/Edit-User-to-Organisation/`,
                                                            admin:data,
                                                            }} >
                                                            <span className="icon-btns-1"><i className="far fa-edit"></i></span>
                                                        </Link>
                                                        <span className="icon-btns-2" onClick={() => {
                                                              this.openDeleteModal(data);
                                                            }}>
                                                            <i className="far fa-trash-alt"></i>
                                                        </span>
                                                  </div>  
                                              </td> */}
                                          </tr>
                                          )
                                }):null
                              }
                                    
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Col>  
                    {/* </Switch> */}
                </Col>
              </Row>
              }
            </div>
          }
        </>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('OrganizerEventsUsers loadAdmins', state)
  return {
    admins: state.Admins.admins,
    message: state.Admins.message,
    error: state.Admins.error,
  }
};

//connect store in footer
const connectedComponent = connect(mapStateToProps, {loadAdmins})(OrganizerEventsUsers);
export default withRouter(connectedComponent);
