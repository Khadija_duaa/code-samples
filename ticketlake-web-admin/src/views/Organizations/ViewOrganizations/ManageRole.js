import React from 'react';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import { TabContent, Nav, NavItem, NavLink, Card,CardHeader, CardBody, Button, CardFooter, CardTitle, CardText, Row, Col } from 'reactstrap';
import Axios from '../../../utils/axios';
import {
    Form,
    Input,
    Checkbox,
    Collapse,
    Tabs,
    Modal,
    Popconfirm
} from 'antd';
import { NotificationManager } from 'react-notifications';
const { TabPane } = Tabs;
const Panel = Collapse.Panel;


class ManageRoleComponnet extends React.Component {
    constructor(props) {
        super(props);
        this.newTabIndex = 0;
        const panes = [];
        //tabs name

        //all roles content
        
        //all role content
        this.state = {
          loading:true ,
          isloading:true ,
          checked: true,
        //   activeKey: panes[0].key,
          panes,
          activeKey:'',
          modules:[],
          roleDetails:[],
          checkedr:[],
          chk:false,
          newId:'5d0f80e652f4ce069bf756c4',
          modal1Visible: false,
          checkedValues:null,
        };
      }
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    
      onChangez=(e)=>{        
        console.log('checked = ', e.target.value);
        // var numArray = [];
        var val = e.target.value
        var newArray = this.state.checkedr.slice();    
        newArray.push(val);   
        console.log('checked2222 = ', newArray);
        this.setState({checkedr:newArray,})
        // this.setState({ numArray });
      }
      handleSubmit = (e, values, checkedValues)=> {
        e.preventDefault();
        this.props.form.validateFields((err, values, checkedValues) => {
          if (!err) {
            console.log('Received values of form: ', values, this.state.checkedValues);
            this.getRoleTab()
            // add ne roles ---------------------------------------------------//
            Axios.post('/roles/create-role',{
                roleName: values.TitleNewRole,
                permissions: this.state.checkedValues
            })
            .then((response) => {
                console.log('/add ne role', response)
                NotificationManager.success('Role Created Successfully')
                this.getRoleTab()
                this.setModal1Visible(false)
            })
            .catch(err => {
                console.log('/add ne role errr', err)
            });
            // get all roles ---------------------------------------------------//
          }
        });
      };
      onSelectRole=(checkedValues)=>{
        console.log('checked = ', checkedValues);
        var val = checkedValues;
        this.setState({
            checkedValues:val
        })
      }
      deleteRole=(delId)=>{
          console.log('deleteRole', delId)
            Axios.delete(`/roles/delete-role/${delId}`)
                .then((response) => {
                    NotificationManager.success('Role Deleted Successfully')
                    this.getRoleTab()
                })
                .catch(err => {
                    console.log('Role Deleted Errr', err.response.data._error)
                    NotificationManager.error(err.response.data._error)
                });
        }      
    

      componentDidMount(){
                
                this.getRoleTab()
      }

    confirmDelete(e){
        this.deleteRole(e)
        console.log(e);
    }
            
    cancelConfirm(e){
            console.log(e);
    }
    

      // get all roles ---------------------------------------------------//
      getRoleTab=()=>{
        Axios.get('/roles/get-all-roles')
        .then((response) => {
            console.log('/roles/get-all-roles', response.data.data)
            this.setState({
                panes:response.data.data,
                activeKey:response.data.data[0]._id,
                loading:false,
            })
            this.getRoleDetails(response.data.data[0]._id)
        })
        .catch(err => {
            console.log('roles/get-all-roles errr', err)
        });
      }
      // get all roles ---------------------------------------------------//

      // get role details---------------------------------------------------//
      getRoleDetails=(activeKey)=>{
          console.log('111', activeKey )
          Axios.get(`/roles/get-role-detail/${activeKey}`)
            .then((response) => {
                console.log('get-role-detail', response.data.data)
                this.setState({
                    roleDetails:response.data.data,
                    isloading:false,
                })
            })
            .catch(err => {
                console.log('get-role-detail errr', err)
            });
        }
      // get role details ---------------------------------------------------//

    
      onChange = activeKey => {
          console.log('activeKey is', activeKey)
        this.setState({ activeKey });
        this.getRoleDetails(activeKey)
      };
    
      onEdit = (targetKey, action) => {
        this[action](targetKey);
      };
    
      add = () => {
          console.log('add new roles')
      };

      
    
      remove = targetKey => {
        let activeKey = this.state.activeKey;
        let lastIndex;
        this.state.panes.forEach((pane, i) => {
          if (pane.key === targetKey) {
            lastIndex = i - 1;
          }
        });
        const panes = this.state.panes.filter(pane => pane.key !== targetKey);
        if (panes.length && activeKey === targetKey) {
          if (lastIndex >= 0) {
            activeKey = panes[lastIndex].key;
          } else {
            activeKey = panes[0].key;
          }
        }
        this.setState({ panes, activeKey });
      };


    cancelOrganizations=()=>{
        this.props.history.push(`/organizations/view-organization/${this.props.match.params.id}`)
    }

    createTabs=()=>{
        return(
          <>
          <Input placeholder="Title of Privileges" className="margin-bottom-15"/>
                  <Form.Item>
                      <Checkbox.Group style={{ width: '100%' }}>
                      <Row>
                          <Col span={24}>
                              <Checkbox value="Create">Create</Checkbox>
                          </Col>
                      </Row>
                      <Row>
                          <Col span={24}>
                              <Checkbox value="Read">Read</Checkbox>
                          </Col>
                      </Row>
                      <Row>
                          <Col span={24}>
                              <Checkbox value="Update">Update</Checkbox>
                          </Col>
                      </Row>
                      <Row>
                          <Col span={24}>
                              <Checkbox value="Delete">Delete</Checkbox>
                          </Col>
                      </Row>
                      </Checkbox.Group>
                  </Form.Item>
                  <Row className="margin-top-40">
                        <Col className={"col-md-5"}>
                            <Button type="submit" size="md" color="primary">
                                Save
                            </Button>{' '}
                            <Button size="md" color="secondary">
                                Cancel
                            </Button>
                        </Col>
                  </Row>
          </>
        )
    }

    render() {
        
        const { getFieldDecorator } = this.props.form;
        console.log('match param', this.props.match.params.id)
        console.log('checkedValues render', this.state.checkedValues)
        return (
                   <>   
                            <Card style={{width:'100%'}}>
                                    <CardHeader>
                                        <p style={{fontSize: '30px', float:'left', marginBottom:'0px'}}>Manage Role</p>
                                        <div style={{float:'right', marginTop:'5px'}}>
                                             <Button onClick={() => this.setModal1Visible(true)} className="btn-danger">+ Add</Button>
                                        </div>

                                        <Modal
                                            title="Create New Role"
                                            style={{ top: 40 }}
                                            className="model-design"
                                            visible={this.state.modal1Visible}
                                            onOk={() => this.setModal1Visible(false)}
                                            onCancel={() => this.setModal1Visible(false)}
                                            destroyOnClose={true}
                                            footer={false}
                                            >
                                            <Form onSubmit={this.handleSubmit}>
                                                <Row>
                                                    <Col span={24}>
                                                        <Form.Item>
                                                        {getFieldDecorator('TitleNewRole', {
                                                                rules: [{ required: true, message: 'Please input your role name!' }],
                                                            })(
                                                            <Input placeholder="Title of New Role" className="margin-bottom-15"/>
                                                            )
                                                        }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                                <Checkbox.Group style={{ width: '100%' }} onChange={this.onSelectRole}>
                                                <Row>
                                                <Col span={12}>
                                                <Row>
                                                    <Col span={24}>
                                                    <strong>Admin:</strong>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0f3b29ac5f17bcf1674f3a' >Get Admin Details</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0f3b29ac5f17bcf1674f39' >Update Admin</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0f3b29ac5f17bcf1674f3b' >Get All Admins</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0f3b29ac5f17bcf1674f38' >Create Admin</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0f3b29ac5f17bcf1674f3d' >Assign Events to staff</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0f3b29ac5f17bcf1674f3c' >Update Admin Password</Checkbox>
                                                    </Col>
                                                </Row>


                                                <Row>
                                                    <Col span={24}>
                                                    <strong>Categories:</strong>
                                                    </Col>
                                                </Row>

                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0faf08ac5f17bcf1674f40' >Update Category</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0faf08ac5f17bcf1674f41' >All Categories</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0faf08ac5f17bcf1674f3f' >Create Category</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0faf08ac5f17bcf1674f42' >Delete Category</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0faf08ac5f17bcf1674f43' >Category Detail</Checkbox>
                                                    </Col>
                                                </Row>



                                                <Row>
                                                    <Col span={24}>
                                                    <strong>Organization:</strong>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb3d4ac5f17bcf1674f47' >All Organization</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb3d4ac5f17bcf1674f48' >Delete Organization</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb3d4ac5f17bcf1674f46' >Update Organization</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb3d4ac5f17bcf1674f45' >Create Organization</Checkbox>
                                                    </Col>
                                                </Row>



                                                <Row>
                                                    <Col span={24}>
                                                    <strong>Roles:</strong>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb822ac5f17bcf1674f4f' >All Modules</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb822ac5f17bcf1674f4b' >Update Role</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb822ac5f17bcf1674f4c' >All Roles</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb822ac5f17bcf1674f4d' >Delete Role</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb822ac5f17bcf1674f4e' >Role Detail</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb822ac5f17bcf1674f4a' >Create Role</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fb822ac5f17bcf1674f50' >Create Organizational Admin Role</Checkbox>
                                                    </Col>
                                                </Row>

                                                <Row>
                                                    <Col span={24}>
                                                    <strong>Seats:</strong>
                                                    </Col>
                                                </Row>

                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fba9cac5f17bcf1674f52' >Manage/Create Slot Seats</Checkbox>
                                                    </Col>
                                                </Row>



                                                

                                                </Col>
                                                

                                                <Col span={12}>
                                                <Row>
                                                    <Col span={24}>
                                                    <strong>Event Slots:</strong>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fbc0bac5f17bcf1674f55' >Add Recurring Slot</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d0fbc0bac5f17bcf1674f54' >Manage/ Create Slot</Checkbox>
                                                    </Col>
                                                </Row>

                                                <Row>
                                                    <Col span={24}>
                                                    <strong>Ticket:</strong>
                                                    </Col>
                                                </Row>

                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105bbdac5f17bcf1674f57' >Refund Ticket</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105bbdac5f17bcf1674f5c' >Scanning</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105bbdac5f17bcf1674f5a' >List of Tickets</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105bbdac5f17bcf1674f5b' >List of Ticket for Staff</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105bbdac5f17bcf1674f58' >Purchase Ticket</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105bbdac5f17bcf1674f59' >Ticket Detail</Checkbox>
                                                    </Col>
                                                </Row>


                                                
                                                <Row>
                                                    <Col span={24}>
                                                    <strong>Venues:</strong>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105fccac5f17bcf1674f5f' >Update Venue</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105fccac5f17bcf1674f60' >All Venues</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105fccac5f17bcf1674f5e' >Create Venue</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105fccac5f17bcf1674f62' >Delete Venue</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d105fccac5f17bcf1674f61' >Venue Detail</Checkbox>
                                                    </Col>
                                                </Row>



                                                <Row>
                                                    <Col span={24}>
                                                    <strong>Events:</strong>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f65' >Get Draft Event</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f6c' >Edit Passes</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f6f' >Parent Event Detail</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f6e' >Create Event</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f69' >Unpublish Event</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f6d' >Get All Passes</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f71' >Get Staff Events</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f66' >Get Events(Parent Events)</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f67' >Cancel Event Slot</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f70' >Event Slot Detail</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f6a' >Create Parent Event</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f68' >Delete Event Slot</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col span={24}>
                                                        <Checkbox value='5d109e02ac5f17bcf1674f6b' >Create Passes</Checkbox>
                                                    </Col>
                                                </Row>
                                                </Col>
                                                </Row>
                                                </Checkbox.Group>

                                                <Row style={{marginTop:'40px'}}>
                                                    <Col span={24}>
                                                        <Button type="primary" className="red-btn" htmlType="submit">
                                                            Create
                                                        </Button>
                                                    </Col>
                                                </Row>

                                            </Form>
                                        </Modal>

                                    </CardHeader>
                                    <CardBody>
                                    {this.state.loading? "loading...":
                                        <div>
                                            {/* {this.state.activeKey} */}
                                            
                                            {/* <Form onSubmit={this.handleSubmit}> */}
                                                <Tabs
                                                hideAdd
                                                onChange={this.onChange}
                                                // onClick={this.clikedPane}
                                                activeKey={this.state.activeKey}
                                                type="editable-card"
                                                className="custom-tabs-design"
                                                // onEdit={this.onEdit}
                                                >
                                                        {this.state.panes.map((pane,i) => (
                                                            <TabPane tab={pane.roleName} key={pane._id}>
                                                                <Form>
                                                                    {/* <Collapse accordion defaultActiveKey={['0']}> */}
                                                                        {this.state.isloading?'loading...':
                                                                                        <>
                                                                                            <h3>{this.state.roleDetails.roleName}</h3> 
                                                                                            <Popconfirm 
                                                                                                    onConfirm={()=>this.confirmDelete(this.state.activeKey)}
                                                                                                    onCancel={this.cancelConfirm}
                                                                                                    okText="Yes"
                                                                                                    cancelText="No"
                                                                                                    placement="left"
                                                                                                    title="Are you sure to want delete Role？"
                                                                                            >
                                                                                                <span className="delete-btn"><i className="far fa-trash-alt"></i> Delete Role</span>
                                                                                            </Popconfirm>
                                                                                            {this.state.activeKey}
                                                                                        {this.state.roleDetails.moduleListWithPermissions.map((module,i) => (
                                                                                            // <Panel header={module.moduleName} key={i}>
                                                                                                <div style={{ width: '100%', padding:'30px 30px 0 30px' }} key={i}>
                                                                                                    <p><strong>{module.moduleName}:</strong></p>
                                                                                                        {module.permissions.map((permission,i) => (                               
                                                                                                            <Row key={i}>
                                                                                                                <Col span={24}>
                                                                                                                    <Checkbox value={permission._id} checked={permission.check?true:false}>{permission.moduleName}</Checkbox>
                                                                                                                </Col>
                                                                                                            </Row>
                                                                                                        ))}
                                                                                                </div>
                                                                                            // </Panel>
                                                                                        ))}
                                                                                        </>
                                                                         }
                                                                    {/* </Collapse> */}
                                                                    <Row className="margin-top-40">
                                                                        <Col className={"col-md-5"}>
                                                                            {/* <Button type="submit" size="md" color="primary">
                                                                                Save
                                                                            </Button>{' '} */}
                                                                            {/* <Button size="md" color="secondary">
                                                                                Cancel
                                                                            </Button> */}
                                                                        </Col>
                                                                    </Row>
                                                                    </Form>
                                                            </TabPane>
                                                        ))}
                                                </Tabs>
                                            {/* </Form> */}
                                        </div>
                                     }
                                        
                                    </CardBody>
                            </Card>
                   </>
        )
    }
}


const ManageRole = Form.create()(ManageRoleComponnet);
const connectedComponent = connect()(ManageRole);
export default withRouter(connectedComponent);