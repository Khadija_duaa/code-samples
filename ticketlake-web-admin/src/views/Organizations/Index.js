import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Container, Modal, ModalBody, ModalFooter, ModalHeader,
  Row,
  Table,
  Button,
} from "reactstrap";
import {Route, Switch, withRouter, Link} from "react-router-dom";
import TableHeads from "../../utils/Headers";
import {NotificationManager} from "react-notifications";
import OrganizationsForm from "./OrganizationsForm";


//------connect store and import store action----//
import connect from "react-redux/es/connect/connect";
import {getAllOrganizations, setOrganizations, deleteOrganizations,resetRedux} from "../../redux/organizations/organizations-actions";
import OrgForm from "./OrgForm";
import OrgAddForm from "./OrgAddForm";
import ViewOrganizations from "./ViewOrganizations/index";
// import AssignAdmins from "./ViewOrganizations/mangeRole/assignAdmins";
//------connect store and import store action----//


const header = ["Name", "Description", "Email", "Phone","Actions"];

class Organizations extends React.Component {
  constructor(props){
    super(props)
    this.state={
        modalOpen: false,
        isClicked: false,
        title:'',
        editValues:null,
    }
}

  componentDidMount() {
    // this.props.resetRedux();
    this.props.getAllOrganizations('65e609c1-9087-4a3f-a350-03fd2c31500d');
  }
  // viewOrganizations = () => {
  //   this.props.history.push('/organizations/view-organization')
  // }
  addOrganizations = (org) => {this.props.history.push(`/organizations/OrgAddForm`)};

  editOrganizations = (org, i) => { 
    console.log('edit org', org)
                                    this.setState({isClicked: !this.state.isClicked}) 
                                    this.props.history.push(`/organizations/orgform/${org.organizationId}`)
                                  };

  cancelOrganizations = () => {this.setState({isClicked: !this.state.isClicked, isClickedAdd:!this.state.isClickedAdd}) 
                            this.props.history.push('/organizations/')};

  openDeleteModal = (venue) => this.setState({modalOpen: !this.state.modalOpen,venueDeleteInfo: venue});

  getModalContent = () => {
    return (<Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal}
                   className={'modal-danger ' + this.props.className}>

        <ModalHeader toggle={this.openDeleteModal}>
          Are you sure ?
        </ModalHeader>

        <ModalBody>
          Are you sure you want to delete this Organisation ?
        </ModalBody>

        <ModalFooter>
          <Button color="danger" onClick={() => {
            let organization = this.state;
            this.props.deleteOrganizations(organization,  
                        () =>{   
                          NotificationManager.success('Deleted Successfully')
                        },
                        () =>{ 
                          NotificationManager.success('Deleted Successfully')                          
                          this.props.getAllOrganizations();
                          // NotificationManager.error('Error');
                        }
                        );
            this.openDeleteModal(null);
          }}>
            Delete
          </Button>{' '}

          <Button color="primary" onClick={this.openDeleteModal}>
            Cancel
          </Button>

        </ModalFooter>
      </Modal>
    );
  };



  getVenueData = () => {
    console.log('orgdddd', !this.props.organizations.organizations.error?"good":"error")
    return (
      !this.props.organizations.organizations.error?this.props.organizations.organizations.map((org, i) => {
        console.log('org', org.organizationId)
            return (
                      <tr key={org._id}>
                        <td style={{width:'25%'}}> {org.name} </td>
                        <td style={{width:'25%'}}> {org.description} </td>
                        <td> {org.email} </td>
                        <td> {org.phoneNumber} </td>
                        <td style={{width:'30%'}}> 
                            <Container>
                              <Row style={{float:'right'}}>
                                <div style={{marginRight: '10px'}}>
                                    <Link to={{
                                          pathname: `/organizations/view-organization/${org.organizationId}`, 
                                          OrganizerEventsUsers: org,
                                        }}>
                                          <span style={{lineHeight:'35px'}}
                                            onClick={
                                                    ()=>{
                                                      this.setState({editValues:org})
                                                  }
                                            }>Details View </span>
                                    </Link>
                                </div>
                                <span className="icon-btns-1" size="md"
                                    onClick={
                                            ()=>{
                                              this.editOrganizations(org)
                                              this.setState({editValues:org})
                                          }
                                    }><i className="far fa-edit"></i></span>
                                <span className="icon-btns-2"  onClick={() => {
                                      this.openDeleteModal(org,i);
                                    }}>
                                      <i className="far fa-trash-alt"></i>
                                      </span>
                              </Row>
                            </Container>
                        </td>
                      </tr>
                   )
          }):null
    );
  };

  // getVenueActions = (org, i) => {
  //   return (
  //     <Container>
  //       <Row>
  //         <Col xs={12} xl={'auto'} style={{marginBottom: '12px'}}>
  //           <Button type="submit" size="md" color="primary" onClick={(org,i)=>this.editOrganizations(org,i)}> Edit </Button>
  //         </Col>

  //         <Col xs={12} xl={'auto'}>
  //           <Button type="submit" size="md" color="danger"  onClick={() => {
  //             this.openDeleteModal(org,i);
  //           }}>
  //             Delete
  //           </Button>
  //         </Col>
  //       </Row>
  //     </Container>
  //   );
  // };
  getTableHeaders = () => {
    return (
      <TableHeads>
        {header}
      </TableHeads>
    );
  };


  getTable = () => {
    return (
      <Table bordered hover striped responsive>
        <thead>
        {this.getTableHeaders()}
        </thead>

        <tbody>
        {this.getVenueData()}
        </tbody>

      </Table>
    );
  };


  getCardBody = () => {
    return (
      <div>
        <Row style={{marginTop: '10px'}}>
          <Col sm={12}>
            {this.getTable()}
          </Col>
        </Row>
      </div>
    );
  };


  render() {
    return (
          <>
            {this.state.isClicked?
              <div className="animated fadeIn">
                <OrgForm
                    visit={this.cancelOrganizations} 
                    title={this.state.title}
                    editValues={this.state.editValues}
                    getAllOrganizations={this.props.getAllOrganizations}
                    />
              </div>
            :
            <div className="animated fadeIn">
              {this.getModalContent()}
              <Row>
                <Col xs="12">
                    <Switch>
                        <Route path={'/organizations/OrgForm'} component={OrgForm}/>
                        <Route path={'/organizations/OrgAddForm'} component={OrgAddForm}/>
                        <Route exact path={'/organizations/view-organization/:id'} component={ViewOrganizations}/>
                        
                        {/* <Route path={'/organizations/view-organization/assign-admin'} component={AssignAdmins}/> */}
                  <Card>
                    <CardHeader>
                      <Col md={7} style={{float: 'left'}}>
                        <span style={{fontSize: '25px'}}>Organizations</span>
                      </Col>
                        
                      <Col md={11}  style={{textAlign: 'right', marginTop:'5px'}}>
                        {/* <Button color="primary" size="md" onClick={this.addOrganizations}>
                                        Add Organization
                        </Button> */}
                      </Col>
                    </CardHeader>

                    <CardBody>
                      {this.getCardBody()}
                    </CardBody>
                  </Card>
                    </Switch>
                </Col>
              </Row>
            </div>
          }
        </>
    );
  }
}

//map store states in footer
const mapStateToProps = (state) => {
  return {
    organizations: state.organizations
    // message: state.eventVenues.message,
    // error: state.eventVenues.error,
  }
};

//connect store in footer
const connectedComponent = connect(mapStateToProps, {getAllOrganizations,setOrganizations,deleteOrganizations,resetRedux})(Organizations);
export default withRouter(connectedComponent);
