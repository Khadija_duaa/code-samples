import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,  Label,
  Row,
} from 'reactstrap';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import Message from '../../utils/Message';
import {setOrganization, getAllOrganizations} from "../../redux/organizations/organizations-actions";
import {NotificationManager} from "react-notifications";
// import {GoogleComponent} from "react-google-location";
// const API_KEY = 'AIzaSyD3saUQV3v1ubgCWYdtoBVbVvZ-AVGlgl0';


class OrganizationsForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      formName: 'Add Organization',
      name:this.props.organizations.name,
      description:this.props.organizations.description,
      email:this.props.organizations.email,
      phone:this.props.organizations.phoneNumber,
    };
  }

  componentDidMount() {
    if (this.props.venue) {
      let {name,description,email,phone} = this.props.venue;
      this.setState({name,description,email,phone, formName: 'Update Venue'});
    }
  }

  onSaveChanges = () => {
    // this.props.setOrganization(this.state.name,this.state.description,this.state.email,this.state.phone)
    this.props.history.push('/organizations');
    this.props.getAllOrganizations();
    // ,() => {
    //     this.props.getAllOrganizations()},() =>{   NotificationManager.success(this.props.venue ? 'updated Successfully' :'Created Successfully')},   () =>{NotificationManager.error( 'Error')})
  };
  onInputChange = (e) => {
      let {target} = e;
      let state = {...this.state};
      state[target.name] = target.value;
      this.setState(state);
  };

  cancelOrganizations=()=>{
      this.props.getAllOrganizations()
      this.props.history.push('/organizations')
  }

  onKeyPress = (e) => {
      const characterCode = e.key;
      if (characterCode === 'Backspace') return;
      const characterNumber = Number(characterCode);
      if (characterNumber >= 0 && characterNumber <= 9) {
        if (e.currentTarget.value && e.currentTarget.value.length) {
          return;
        }
        else if (characterNumber === 0) {
          e.preventDefault()
        }
      } else {
        e.preventDefault()
      }
  };


  getName = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>
        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Name</Label>
        </Col>
        <Col md="11" >
          <Input type="text" id="name" name="name"  value={this.state.name} onChange={this.onInputChange} placeholder='Enter Name' />
        </Col>
      </FormGroup>
    )
  };


  getDescription = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>
        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Description</Label>
        </Col>
        <Col md="11" >
          <Input type="text" id="description" name="description"  value={this.state.description} onChange={this.onInputChange} placeholder='Enter description' />
        </Col>
      </FormGroup>
    )
  };

  getEmail = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>
        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Email</Label>
        </Col>
        <Col md="11" >
          <Input type="email" id="email" name="email"  placeholder='Enter email' value={this.state.email} onChange={this.onInputChange}/>
        </Col>
      </FormGroup>
    )
  };

  getPhone = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>
        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Phone</Label>
        </Col>
        <Col md="11" >
          <Input type="number" id="phone" name="phone" placeholder='Enter Phone' value={this.state.phone} onKeyPress={this.onKeyPress}/>
        </Col>
      </FormGroup>
    )
  };

  getForm = () => {
    return (
      <Form action="" method="post" className="form-horizontal">
        <FormGroup row style={{marginTop:'10px'}}>
          <Col md="12">
            <Message style={{color:'black', fontSize:'15px'}}> Please fill the following input fields to add a venue</Message>
          </Col>
        </FormGroup>
            {this.getName()}
            {this.getDescription()}
            {this.getEmail()}
            {this.getPhone()}
      </Form>
    );
  };

  getCardFooter = () => {
    return (
      <Row>
        <Col className={"col-md-1 offset-md-1"}>
          <Button type="submit" size="md" color="primary" onClick={this.onSaveChanges}>
            Submit
          </Button>
        </Col>
        <Col md="1">
          <Button type="submit" size="md" color="secondary" onClick={this.cancelOrganizations}>
            Cancel
          </Button>
        </Col>
      </Row>
    );
  };


  render() {
    return (
      <div>
        <Card>
          <CardHeader>
            <p style={{fontSize: '30px'}}>Add Organization</p>

          </CardHeader>

          <CardBody>
            {this.getForm()}
          </CardBody>

          <CardFooter>
            {this.getCardFooter()}
          </CardFooter>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    organizations: state.organizations.organizations
  }
};


const connectedComponent = connect(mapStateToProps, {setOrganization,getAllOrganizations})(OrganizationsForm);
export default withRouter(connectedComponent);
