import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
} from 'reactstrap';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {loadRoles,saveAdmin,loadAdmins} from "../../redux/actions/admins/index";
import setActiveUser from '../../redux/user/user-actions'
import {NotificationManager} from "react-notifications";
import user from '../../assets/images/user.jpg'
import './admin.css';
import axios from '../../utils/axios';


class AdminForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    formName: 'Add Admin',
    profile_picture_key:'',
    role:'',
    username:'',
    password:'',
    email:'',
    name: '',
    gender:'',
    cnic:'',
    dob:'',
    dateOfJoining: '',
    contactNumber: '',
    address: '',
    reportingTo: '5d1228dd40d1ab1683478624',
    isError:false,
    passwordError:'',
    usernameError:'',

    isName:false,
    isEmail:false,
    isGender:false,
    isCnic:false,
    isDob:false,
    isDateOfJoining:false,
    isAddress:false,
    isContactNumber:false,
    isUsername:false,
    isRole:false,
    isPassword:false,
  }
}


  componentDidMount() {
    // console.log('admin update', this.props.admin?this.props.admin.profileImage.imageUrl:null)
    this.props.loadRoles();
    if (this.props.admin) {
      let {profile_picture_key, password,email,name,username,gender,cnic,dob,dateOfJoining,contactNumber,address} = this.props.admin;
      let role =  this.props.admin.role && this.props.admin.role['_id'];
      this.state.role = role;
      this.setState({profile_picture_key,role, password,email,name,username, gender,cnic,dob,dateOfJoining,contactNumber,address, formName: 'Update Admin'});
    }
    console.log('this.props.admin form data', this.props.admin)

  }

  onInputChange = (e) => {
    let {target} = e;
    let state = {...this.state};
    state[target.name] = target.value;
    this.setState(state);
    if (e.target.name==='username'){
      if(e.target.value.match('^[a-zA-Z0-9]*$')){
        console.log('match')
        this.setState({
          usernameError:'',
        });
      }
      else{
        console.log('not match')
        this.setState({
            usernameError:'UserName contain only strings and numbers',
        });
      }
    }
    if (e.target.name==='password'){
      if(e.target.value.length<6){
        this.setState({
           passwordError:'Password length sholud be at least 6 digit',
        });
      }
      else{
        this.setState({
          passwordError:'',
       });
      }
      console.log('username', e.target.value.length)
    }
    
  };

  onSaveChanges = () => {
    console.log('error handling', this.props)
    if(this.state.name.length===0){
        this.setState({
          isName:true
        })
    }else if(this.state.role.length===0){
        this.setState({
            isRole:true
          })
    }else if(this.state.email.length===0){
        this.setState({
            isEmail:true
          })
    }else if(this.state.gender.length===0){
        this.setState({
            isGender:true
          })
    }else if(this.state.cnic.length===0){
        this.setState({
            isCnic:true
          })
    }else if(this.state.dob.length===0){
        this.setState({
            isDob:true
          })
    }else if(this.state.dateOfJoining.length===0){
        this.setState({
            isDateOfJoining:true
          })
    }else if(this.state.contactNumber.length===0){
        this.setState({
            isContactNumber:true
          })
    }else if(this.state.address.length===0){
        this.setState({
            isAddress:true
          })
    }else{
      this.props.saveAdmin(
        this.state.profile_picture_key,
        this.state.role,this.state.username,
        this.state.password,
        this.state.email,
        this.state.name,
        this.state.gender, 
        this.state.cnic,
        this.state.dob,
        this.state.dateOfJoining,
        this.state.contactNumber,
        this.state.address,
        () => {
        this.props.history.push('/admins');
      },() => { this.props.loadAdmins()},
        () => { NotificationManager.success(this.props.admin ? this.props.message :'Created Successfully')},   
        () => { NotificationManager.error(this.props.error.message, 'Error')})
    }
    
  };

  onDrop = (e) => {
    if (e.target.files.length) {
      e.preventDefault();
      let file = e.target.files[0];
      let formData = new FormData()
      formData.append("image", file)

      axios.patch('/admins/update-profile-picture', formData)
      .then((response)=>{
          console.log('upload profile image response', response);
          // this.setState({
          //   profile_picture_key: response.data.imageKey,
          //   iconImageUrl: response.data.imageUrl
          // });
      })
      .catch((error)=>{
          console.log('upload profile image error', error);
      });
    } else {
      return null
    }
  };


  getForm = () => {
    console.log('this.state.profile_picture_key', this.state.formName==='Update Admin')
    console.log('this.props.userReducer.activeUser.username===this.props.admin.username', this.props.allData?this.props.allData.userReducer.activeUser.data.username:null)
    // console.log('this.props.userReducer.activeUser.username===this.props.admin.username2', this.props.admin.username)
    return (
      <Form action="" method="post" className="form-horizontal">
        {this.props.admin?
            <>
                {this.props.allData.userReducer.activeUser.data.username===this.props.admin.username?
                    <>
                        <FormGroup row>
                              <Col md="1">
                                    <Label style={{fontSize:'15px'}}>Profile Display</Label>
                              </Col>
                              <Col md="1">
                                    <img alt="FF" style={{height:'100px', width:'100px'}} src={this.props.admin.profileImage?this.props.admin.profileImage.imageUrl:'https://via.placeholder.com/100'}/>
                              </Col>
                              <Col md="1">
                                    <input type="file"
                                          ref={uploadElement => this.uploadElement = uploadElement}
                                          accept="image/*"
                                          hidden
                                          onChange={this.onDrop}
                                          />
                                    <Button type="submit" size="md" color={this.props.admin?'light':'primary'}
                                            onClick={e => {
                                              e.preventDefault();
                                              this.uploadElement.click()
                                            }}>
                                      Upload
                                    </Button>
                                    <br />
                              </Col>
                        </FormGroup>
                    </>
                :null
                }
            </>
        :
        null
      }

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Role</Label>
          </Col>
          <Col md="11">
            <Input type="select"
                   name="role"
                   value={this.state.role}
                   autoFocus
                   id="role-select"
                   onChange={this.onInputChange}>
              <option value="" disabled selected>Select your option</option>

              {this.props.roles.length >0 ? this.props.roles.map(role => {
                // console.log('this.props.roles', this.props.roles)
                  return (
                    <option key={role._id} value={role._id}>
                      {role.roleName}
                    </option>
                  )
                }
              ): null}
            </Input>
          <p className="red">{this.state.isRole===false? null :'Please select role'}</p>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Username</Label>
          </Col>
          <Col md="11">
            <Input type="text" id="username" name="username" placeholder="Enter Username" onChange={this.onInputChange}
                   required value={this.state.username}/>
            <p className="red">{this.state.usernameError}</p>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Password</Label>
          </Col>
          <Col md="11">
            <Input type="password" id="password" minLength={6} min={6} name="password" placeholder={'Enter Password'} onChange={this.onInputChange}
                   required value={this.state.password} />
            <p className="red">{this.state.passwordError}</p>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Email</Label>
          </Col>
          <Col md="11">
            <Input type="email" id="email" name="email" placeholder="Enter Email" onChange={this.onInputChange}
                   required value={this.state.email} />
            <p className="red">{this.state.isEmail===false? null :'Email required'}</p>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Name</Label>
          </Col>
          <Col md="11">
            <Input type="text" id="name" name="name" placeholder="Enter Name" onChange={this.onInputChange}
                   required value={this.state.name} />
            <p className="red">{this.state.isName===false? null :'Name required'}</p>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Gender</Label>
          </Col>
          <Col md="11">
            <Input type="select"
                    name="gender"
                    value={this.state.gender}
                    id="role-gender"
                    onChange={this.onInputChange}>
                      <option value="" disabled selected>Select your Gender</option>
                      <option key={1} value='MALE'>Male</option>
                      <option key={2} value='FEMALE'>Female</option>
              </Input>
            <p className="red">{this.state.isGender===false? null :'Gender required'}</p>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>CNIC</Label>
          </Col>
          <Col md="11">
            <Input type="number" id="cnic" name="cnic" placeholder="Enter cnic" onChange={this.onInputChange}
                   required value={this.state.cnic} />
            <p className="red">{this.state.isCnic===false? null :'CNIC required'}</p>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1"><Label for="dob">DOB</Label>
          </Col>
            <Col md="11">
              <Input
                type="date"
                name="dob"
                id="dob"
                required
                value={this.state.dob}
                onChange={this.onInputChange}
                placeholder="date placeholder"
              />
          <p className="red">{this.state.isDob===false? null :'DOB required'}</p>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1"><Label for="exampleDate">Date Of Joining</Label>
          </Col>
            <Col md="11">
              <Input
                type="date"
                name="dateOfJoining"
                required
                id="dateOfJoining"
                value={this.state.dateOfJoining}
                onChange={this.onInputChange}
                placeholder="date placeholder"
              />
          <p className="red">{this.state.isDateOfJoining===false? null :'Date of joining required'}</p>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Contact</Label>
          </Col>
          <Col md="11">
            <Input type="number" id="contactNumber" name="contactNumber" placeholder="Enter Contact Number" onChange={this.onInputChange}
                   required value={this.state.contactNumber} />
            <p className="red">{this.state.isContactNumber===false? null :'Contact number required'}</p>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Address</Label>
          </Col>
          <Col md="11">
            <Input type="text" id="address" name="address" placeholder="Enter Address" onChange={this.onInputChange}
                   required value={this.state.address} />
            <p className="red">{this.state.isAddress===false? null :'Address required'}</p>
          </Col>
        </FormGroup>
      </Form>
    );
  };

  getCardFooter = () => {
    return (
      <Row>
        <Col xs="1">
          <Button type="submit" size="md" color="primary" onClick={this.onSaveChanges}>
            Submit
          </Button>
        </Col>
        <Col xs="1">
          <Button type="submit" size="md" color="secondary" onClick={() => this.props.history.push('/admins')}>
            Cancel
          </Button>
        </Col>
      </Row>
    );
  };


  render() {
    return (
      <div>
        <Card>
          <CardHeader>
            <p style={{fontSize: '30px'}}>{this.state.formName}</p>

          </CardHeader>

          <CardBody>
            {this.getForm()}
          </CardBody>

          <CardFooter>
            {this.getCardFooter()}
          </CardFooter>
        </Card>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  console.log('mapProps error handling', state)
  return {
    roles: state.Admins.roles,
    admin: state.Admins.admin,
    error: state.Admins.error,
    allData:state,
  }
}

const connectedComponent = connect(mapStateToProps, {loadRoles,saveAdmin,loadAdmins})(AdminForm);
export default withRouter(connectedComponent);
