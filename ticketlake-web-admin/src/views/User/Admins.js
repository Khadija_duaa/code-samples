import React from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button,
  Container,
  Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';
import {Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import TableHeads from '../../utils/Headers';
import {loadAdmins, deleteAdmin, setAdmin, resetRedux} from "../../redux/actions/admins/index";
import {NotificationManager} from "react-notifications";
import {capitalize} from "../../utils/common-utils";
import CategoryForm from "../EventCategory/CategoryForm";
import AdminForm from "./AdminForm";

const header = ["Username", "Name", "Role", "Actions"];

class Admins extends React.Component {

  state = {
    modalOpen: false
  };

  openDeleteModal = (admin) => this.setState({modalOpen: !this.state.modalOpen, adminDeleteInfo: admin});

  getModalContent = () => {
    return (<Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal}
                   className={'modal-danger ' + this.props.className}>

        <ModalHeader toggle={this.openDeleteModal}>
          Are you sure ?
        </ModalHeader>

        <ModalBody>
          Are you sure you want to delete this Admin ?
        </ModalBody>

        <ModalFooter>
          <Button color="danger" onClick={() => {
            let {adminDeleteInfo} = this.state;
            this.props.deleteAdmin(adminDeleteInfo._id, adminDeleteInfo.index,  
              () =>{ NotificationManager.success('Deleted Successfully')},
              () =>{ NotificationManager.error('Error');}
            );
            this.openDeleteModal(null);
            setTimeout(() => {
              this.props.resetRedux()
              this.props.loadAdmins()
            }, 2000);
          }}>
            Delete
          </Button>{' '}

          <Button color="primary" onClick={this.openDeleteModal}>
            Cancel
          </Button>

        </ModalFooter>
      </Modal>
    );
  };


  componentDidMount() {
    this.props.resetRedux();
    this.props.loadAdmins();
  };

  getAdminData = () => {
    console.log('getAdminData',this.props.admins)
    if(!this.props.admins) return null;
    return (
      this.props.admins.length > 0 ? this.props.admins.map((admin, i) => {
        return (
          <tr key={admin._id}>
            <td>
              {capitalize(admin.username)}
            </td>

            <td>
              {capitalize(admin.name)}
            </td>

            <td>
              {admin.role? admin.role['roleName'] : "N/A"}
            </td>
            <td>
              {this.getAdminActions(admin, i)}
            </td>
          </tr>)
      }): null

    );
  };

  getAdminActions = (admin, i) => {
    return (
      <Container>
        <Row>
          
            <span className="icon-btns-1" onClick={() => {
              this.props.setAdmin(admin);
              this.props.history.push('/admins/admin-form');
            }}><i className="far fa-edit"></i></span>
            
            <span className="icon-btns-2"onClick={() => {
              this.openDeleteModal({_id: admin._id, i});
            }}><i className="far fa-trash-alt"></i></span>
        </Row>
      </Container>
    );
  };
  getTableHeaders = () => {
    return (
      <TableHeads>
        {header}
      </TableHeads>
    );
  };


  getTable = () => {
    return (
      <Table bordered hover striped responsive>
        <thead>
        {this.getTableHeaders()}
        </thead>

        <tbody>
        {this.getAdminData()}
        </tbody>

      </Table>
    );
  };


  getCardBody = () => {
    return (
      <div>
        <Row style={{marginTop: '10px'}}>
          <Col sm={12}>
            {this.getTable()}
          </Col>
        </Row>
      </div>
    );
  };


  render() {
    return (
      <div className="animated fadeIn">
        {this.getModalContent()}
        <Row>
          <Col xs="12">
              <Switch>
                  <Route path={'/admins/admin-form'} component={AdminForm}/>
            <Card>
              <CardHeader>
                <Col md={2} style={{float: 'left'}}>
                  <p style={{fontSize: '25px'}}>Admins</p>
                </Col>
                <Col md={11}  style={{textAlign: 'right', marginTop:'5px'}}>
                  <Button color="primary" size="md"
                          onClick={() => {
                            this.props.history.push('/admins/admin-form')}}>
                    Add Admin
                  </Button>
                </Col>
              </CardHeader>

              <CardBody>
                {this.getCardBody()}
              </CardBody>
            </Card>
              </Switch>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    admins: state.Admins.admins,
    message: state.Admins.message,
    error: state.Admins.error,
  }
};


const connectedComponent = connect(mapStateToProps, {loadAdmins,  deleteAdmin, setAdmin, resetRedux})(Admins);
export default withRouter(connectedComponent);
