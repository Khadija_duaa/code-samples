import React from 'react';
import * as data from './data-bootstrap-table';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';


//This is for the Search item
function afterSearch(searchText, result) {
  console.log('Your search text is ' + searchText);
  console.log('Result is:');
  for (let i = 0; i < result.length; i++) {
    console.log('Fruit: ' + result[i].id + ', ' + result[i].name + ', ' + result[i].price);
  }
}
const options = {
  // A hook for after droping rows.
  afterSearch: afterSearch  // define a after search hook
};


class Events extends React.Component {

  render() {
    return (<div>
      <Row>
        <Col md="12">
          <Card>
            <CardBody>
              <BootstrapTable striped hover
                condensed search={true}
                data={data.jsondata}
                pagination
                options={options}
                tableHeaderClass='mb-1'
              >
                <TableHeaderColumn width='100' dataField='name' isKey>Event ID</TableHeaderColumn>
                <TableHeaderColumn width='100' dataField='gender'>Name</TableHeaderColumn>
                <TableHeaderColumn width='100' dataField='company'>Ticket Avail.</TableHeaderColumn>
                <TableHeaderColumn width='100' dataField='company'>Pass Avail.</TableHeaderColumn>
                <TableHeaderColumn width='100' dataField='company'>Date</TableHeaderColumn>
              </BootstrapTable>
            </CardBody>
          </Card>


        </Col>
      </Row>
    </div>
    );
  }
}
export default Events;