const jsondata = [
    {
        "eventCategories": [
            "5c64fd993158cf6eb745bb46",
            "5c64fda13158cf6eb745bb4c"
        ],
        "tags": [],
        "eventImagesKeys": [],
        "category": [],
        "_id": "5c80a44a0c4b02184c21615c",
        "isActive": false,
        "isFeatured": false,
        "createdAt": "2019-03-07T04:52:08.256Z",
        "title": "Lums Event",
        "description": "Luminites",
        "freeText": "Maybe an html page stored in db",
        "eventTimesSlots": {
            "tags": [
                "#yourEvent",
                "#QB",
                "#lhr"
            ],
            "_id": "5c80a44a0c4b02184c216161",
            "event_start_time": "2019-01-17 12:30:41",
            "event_end_time": "2019-01-17 14:30:41",
            "ticketClasses": [
                {
                    "_id": "5c80a44a0c4b02184c216162",
                    "ticketClassName": "VIP",
                    "ticketType": "PASS",
                    "basePrice": 20,
                    "salePrice": 22,
                    "currency": "PKR",
                    "availableTickets": 6,
                    "reservedTickets": 10
                },
                {
                    "_id": "5c80a44a0c4b02184c216163",
                    "ticketClassName": "Ecnomic",
                    "ticketType": "REGULAR",
                    "basePrice": 20,
                    "salePrice": 22,
                    "currency": "PKR",
                    "availableTickets": 69,
                    "reservedTickets": 10
                }
            ],
            "celebrities": [
                {
                    "_id": "5c80a44a0c4b02184c216163",
                    "name": "Ataib Zaidi"
                }
            ],
            "guests": [
                {
                    "_id": "5c80a44a0c4b02184c216164",
                    "name": "Musharaf",
                    "guestCategory": "5c64fd993158cf6eb745bb46"
                }
            ]
        },
        "contactPersonInfo": {
            "name": "Syed Ataib Zaidi",
            "phoneNumber": "+923434036174"
        },
        "createdBy": "postman admin",
        "uuid": "7cedb26b-114b-445d-9e97-7a1648428c8f",
        "categories": []
    },
    ]

export { jsondata };