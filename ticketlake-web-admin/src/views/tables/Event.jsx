import React from 'react';
import {
  Table,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,Button,
    Form,FormGroup,InputGroup,InputGroupAddon,Input
} from 'reactstrap';
import connect from "react-redux/es/connect/connect";
import {getAllEvents,searchEvents} from "../../redux/actions/events";
import {withRouter} from "react-router-dom";
import TableHeads from '../../utils/Headers';
import Loader from '../../utils/loader';

const header = ["Name", "Ticket Avail.", "Pass Avail.", "Start Date", "End Date"];

const searchFields = ['title', 'title', 'title', 'eventTimesSlots.event_start_time', 'eventTimesSlots.event_end_time'];

class Event extends React.Component {


    getEventData = () => {
        if(!this.props.events) return null;
        return (
            this.props.events.map((event, i) => {
                return (
                    <tr key={event.name}>
                        <td>
                            {event.title}
                        </td>

                        <td>
                            {event.title}
                        </td>

                        <td>
                            {event.title}
                        </td>
                        <td>
                            {event.eventTimesSlots.event_start_time}
                        </td>
                        <td>
                            {event.eventTimesSlots.event_end_time}
                        </td>
                    </tr>)
            })

        );
    };

    searchField = () => {
        return (
            <Form className="form-horizontal" style={{float:'right'}}>
                <FormGroup>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="submit" disabled color="danger"><i className="fa fa-search"/> Search</Button>
                        </InputGroupAddon>
                        <Input type="text" id="input1-group2" name="input1-group2" placeholder="Type here..."
                               onChange={(e) => this.props.searchEvents(e.target.value, searchFields)}/>
                    </InputGroup>
                </FormGroup>
            </Form>
        );
    };

    getTableHeaders = () => {
        return (
            <TableHeads>
                {header}
            </TableHeads>
        );
    };


    render() {
        return <div>
            {this.props.processing ? <Loader/> :
                <Row>
                    <Col>
                        <Row>
                            <Col sm={12}>
                                {this.searchField()}
                            </Col>
                        </Row>
                        <Row>
                            <Col md="12">
                                <Card>
                                    <Table bordered hover striped responsive>
                                        <thead>
                                        {this.getTableHeaders()}
                                        </thead>
                                        <tbody>
                                        {this.getEventData()}
                                        </tbody>
                                    </Table>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            }

        </div>
  }
}

const mapStateToProps = (state) => {
    console.log('its event data', state.Events.processing);
    return {
        processing: state.Events.processing,
        events: state.Events.allEvents,
        message: state.Events.message,
        error: state.Events.error,
    }
};

const connectedComponent = connect(mapStateToProps, {getAllEvents,searchEvents})(Event);
export default withRouter(connectedComponent);
