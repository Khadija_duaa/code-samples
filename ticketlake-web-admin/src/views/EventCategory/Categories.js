import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container, Modal, ModalBody, ModalFooter, ModalHeader,
  Row,
  Table,
} from "reactstrap";
import connect from "react-redux/es/connect/connect";
import {loadCategories,setCategory,resetRedux,deleteCategory} from "../../redux/actions/eventCategory/index";
import {withRouter, Route,Switch} from "react-router-dom";
import {NotificationManager} from "react-notifications";
import TableHeads from "../../utils/Headers";
import { Badge } from 'reactstrap';
import CategoryForm from './CategoryForm';

const header = ["Name", "Icon", "Image", "Related Tags", "Actions"];

class Categories extends React.Component {

  state = {
    modalOpen: false
  };

  componentDidMount() {
      this.props.resetRedux();
    this.props.loadCategories();
  };

  openDeleteModal = (category) => this.setState({modalOpen: !this.state.modalOpen, categoryDeleteInfo: category});

  getModalContent = () => {
    return (<Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal}
                   className={'modal-danger ' + this.props.className}>

        <ModalHeader toggle={this.openDeleteModal}>
          Are you sure ?
        </ModalHeader>

        <ModalBody>
          Are you sure you want to delete this Category ?
        </ModalBody>

        <ModalFooter>
          <Button color="danger" onClick={() => {
            let {categoryDeleteInfo} = this.state;
            this.props.deleteCategory(categoryDeleteInfo.uuid, categoryDeleteInfo.index,  () =>{   NotificationManager.success('Deleted Successfully')},() =>{ NotificationManager.error(this.props.error);});
            this.openDeleteModal(null);
          }}>
            Delete
          </Button>{' '}

          <Button color="primary" onClick={this.openDeleteModal}>
            Cancel
          </Button>

        </ModalFooter>
      </Modal>
    );
  };


  getCategoryData = () => {
    if(!this.props.categories) return null;
    return (
      this.props.categories.length > 0 ? this.props.categories.map((category, index) => {
        console.log('category', category)
        return (
          <tr key={index}>
            <td>
              {category.categoryName}
            </td>
            <td>
              <img style={{marginBottom:'0px', marginRight:'0px'}} src={category.iconImageKey?category.iconImageKey.imageUrl:null} className="category-icon"/>
            </td>
            <td>
              <img style={{marginBottom:'0px', marginRight:'0px'}} src={category.iconImageKey?category.imageKey.imageUrl:null} className="category-icon"/>
            </td>
            <td>
              <div style={{display:'flex'}}>
              {category.tags.map((n,i) =>{ return (<p style={{fontSize:'20px'}} key={i}><Badge style={{marginRight:'3px'}}>{n.name}</Badge></p>) })}
              </div>
            </td>

            <td style={{width:'30%'}}>
              {this.getCategoryActions(category, index)}
            </td>
          </tr>)
      }) : null


    );
  };

  getCategoryActions = (category, index) => {
    // console.log('cat', category)
    return (
      <Container>
        <Row>
            <span className="icon-btns-1" size="md" color="primary" onClick={() => {
              this.props.setCategory(category);
              this.props.history.push('/categories/category-form');
            }}>
              <i className="far fa-edit"></i>
            </span>
            <span className="icon-btns-2" color="danger"  onClick={() => {
              this.openDeleteModal({uuid: category.uuid, index});
            }}>
              <i className="far fa-trash-alt"></i>
            </span>
        </Row>
      </Container>
    );
  };
  getTableHeaders = () => {
    return (
      <TableHeads>
        {header}
      </TableHeads>
    );
  };


  getTable = () => {
    return (
      <Table bordered hover striped responsive>
        <thead>
        {this.getTableHeaders()}
        </thead>

        <tbody>
        {this.getCategoryData()}
        </tbody>

      </Table>
    );
  };


  getCardBody = () => {
    return (
      <div>
        <Row style={{marginTop: '10px'}}>
          <Col sm={12}>
            {this.getTable()}
          </Col>
        </Row>
      </div>
    );
  };


  render() {
    return (
      <div className="animated fadeIn">
        {this.getModalContent()}
        <Row>
          <Col xs="12">
              <Switch>
              <Route path={'/categories/category-form'} component={CategoryForm}/>
            <Card>

                    <CardHeader>
                        <Col md={2} style={{float: 'left'}}>
                            <p style={{fontSize: '25px'}}>Categories</p>
                        </Col>
                        <Col md={11}  style={{textAlign: 'right', marginTop:'5px'}}>
                            <Button color="primary" size="md"
                                    onClick={() => {this.props.history.push('/categories/category-form')}}>
                                Add Category
                            </Button>
                        </Col>
                    </CardHeader>

                    <CardBody>

                        {this.getCardBody()}
                    </CardBody>

            </Card>
              </Switch>
          </Col>
        </Row>

      </div>
    );
  }
}
const mapStateToProps = (state) => {
  console.log('category mapstate error', )
  return {
    categories: state.eventCategories.categories,
    message :state.eventCategories.message,
    error :state.eventCategories.error? state.eventCategories.error.response.data._error && state.eventCategories.error.response.data._error && state.eventCategories.error.response.data._error: '',
  }
};


const connectedComponent = connect(mapStateToProps, {loadCategories,setCategory,resetRedux,deleteCategory})(Categories);
export default withRouter(connectedComponent);
