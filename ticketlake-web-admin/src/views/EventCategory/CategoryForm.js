import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  FormText,
  Label,
} from 'reactstrap';
import { Upload,  Modal, message} from 'antd';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {saveCategory,loadCategories, resetRedux} from "../../redux/actions/eventCategory/index";
import {NotificationManager} from "react-notifications";
import { WithContext as ReactTags } from 'react-tag-input';
import './forms.css';
import axios from '../../utils/axios';

const Dragger = Upload.Dragger;
class CategoryForm extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      formName: 'Add Category',
      name: '',
      tags: [],
      icon:'',
      iconPreviewUrl:'',
      image: '',
      imagePreviewUrl: '',
      imageKey: '',
      imageUrl: '',
      iconImageKey: '',
      iconImageUrl: '',
      isLoadingImage:true,
      isLoadingIcon:true,
    };
    this.handleDelete = this.handleDelete.bind(this);
    this.handleAddition = this.handleAddition.bind(this);
  }
  

  uploadImage=(e)=>{
    e.preventDefault();
    let file = e.target.files[0];
    let formData = new FormData()
    formData.append("image", file)
    this.setState({
      isLoadingImage:true,
    });
    axios.post('/events/image-upload', formData)
    .then((response)=>{
        console.log('upload images response', response);
        this.setState({
          imageKey: response.data.imageKey,
          imageUrl: response.data.imageUrl,
          isLoadingImage:false,
        });
    })
    .catch((error)=>{
        console.log('upload images error', error);
    });
}


    uploadIcon=(e)=>{
      e.preventDefault();
      let file = e.target.files[0];
      let formData = new FormData()
      formData.append("image", file)
      this.setState({
        isLoadingIcon:true,
      });
      axios.post('/events/image-upload', formData)
      .then((response)=>{
          console.log('upload icon response', response);
          this.setState({
            iconImageKey: response.data.imageKey,
            iconImageUrl: response.data.imageUrl,
            isLoadingIcon:false,
          });
      })
      .catch((error)=>{
          console.log('upload icon error', error);
      });
    }

  componentDidMount() {
    // console.log('Current Category this.props.category', this.props.category?this.props.category.iconImageKey.imageUrl:null)
    if (this.props.category) {
      let {categoryName,tags, imageKey, iconImageKey}= this.props.category;
      this.setState({
        name:categoryName,
        tags: tags,  
        imageKey:imageKey?imageKey.imageKey:null,
        imageUrl:imageKey?imageKey.imageUrl:null,
        iconImageKey:iconImageKey?iconImageKey.imageKey:null,
        iconImageUrl:iconImageKey?iconImageKey.imageUrl:null,
        formName: 'Update Category'  
      });
      if(iconImageKey.imageUrl){
        this.setState({
          isLoadingIcon:false,
        });
      }
      if(imageKey.imageUrl){
        this.setState({
          isLoadingImage:false,
        });
      }
    }

  }

  onInputChange = (e) => {
    let {target} = e;
    let state = {...this.state};
    state[target.name] = target.value;
    this.setState(state);
  };

  handleDelete(i) {
    const { tags } = this.state;
    this.setState({
      tags: tags.filter((tag, index) => index !== i),
    });
  }

  handleAddition(tag) {
    this.setState(state => ({ tags: [...state.tags, tag] }))};


  onSaveChanges = () => {
    if(this.state.name.length===0 || this.state.tags.length===0 || this.state.imageKey.length===0 || this.state.iconImageKey.length===0){
      NotificationManager.error('Required all field')
    }else{
      this.props.saveCategory(this.state.name ,this.state.tags, this.state.imageKey, this.state.imageUrl, this.state.iconImageKey, this.state.iconImageUrl, () => {
        this.props.history.push('/categories');
      },() => {
          this.props.loadCategories()
          this.props.resetRedux()
      },() =>{   NotificationManager.success(this.props.category ? 'Updated Successfully' :'Created Successfully')},
        // () =>{NotificationManager.error( "Category Already Exist", "Error")}
      )
    }
  };

  cancelForm = () => {
    this.props.resetRedux(); 
    this.props.history.push('/categories')
  }

  getForm = () => {
    console.log('Current Category function imageKey in render', this.props.category)


    return (
      <Form  className="form-horizontal">

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Name</Label>
          </Col>
          <Col md="11">
            <Input type="text" id="name" name="name" placeholder="Enter Name" onChange={this.onInputChange}
                   required value={this.state.name}/>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Tags</Label>
          </Col>
          <Col md="11">
            <ReactTags tags={this.state.tags}
                       className="reacttags__tags"
                       inline={true}
                       labelField={'name'}
                       handleDelete={this.handleDelete}
                       handleAddition={this.handleAddition}
                       placeholder={"Type Tags"}/>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Icon</Label>
          </Col>
          <Col md="11">
          {this.props.category?
              <img src={this.props.category.iconImageKey?this.props.category.iconImageKey.imageUrl:null} className="category-icon"/>
              : null
            }
            <input type="file" name="fileToUpload" id="fileToUpload" onChange={(e)=>{this.uploadIcon(e)}}></input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="1">
            <Label style={{fontSize:'15px'}}>Image</Label>
          </Col>
          <Col md="11">
            {this.props.category?
              <img src={this.props.category.imageKey?this.props.category.imageKey.imageUrl:null} className="category-image"/>
              : null
            }
            <input type="file" name="fileToUpload" id="fileToUpload" onChange={(e)=>{this.uploadImage(e)}}></input>
          </Col>
        </FormGroup>
      </Form>
    );
  };

  getCardFooter = () => {
    return (
      <Row>
        
        <Col xs="1">
          {this.state.isLoadingIcon===false && this.state.isLoadingImage===false?
              <Button type="submit" size="md" color="primary" onClick={this.onSaveChanges}>
                Submit
              </Button>
              :
              <Button type="submit" size="md" color="primary" disabled>
                Submit
              </Button>
          }
        </Col>
        <Col xs="1">
          <Button type="submit" size="md" color="secondary" onClick={this.cancelForm}>
            Cancel
          </Button>
        </Col>
      </Row>
    );
  };

  render() {
    return (
      <div>
        <Card>
          <CardHeader>
            <p style={{fontSize: '30px'}}>{this.state.formName}</p>

          </CardHeader>

          <CardBody>
            {this.getForm()}
          </CardBody>

          <CardFooter>
            {this.getCardFooter()}
          </CardFooter>
        </Card>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    category: state.eventCategories.category,
    message: state.eventCategories.message,
    error: state.eventCategories.error,
  }
};

const connectedComponent = connect(mapStateToProps, {saveCategory,loadCategories, resetRedux})(CategoryForm);
export default withRouter(connectedComponent);
