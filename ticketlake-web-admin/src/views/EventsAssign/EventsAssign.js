import React from "react";
import {
  Form,
  Select,
  InputNumber,
  Radio,
  Slider,
  Upload,
  Icon,
  Rate,
  Checkbox,
} from 'antd';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
} from "reactstrap";
import connect from "react-redux/es/connect/connect";
import {withRouter, Route,Switch} from "react-router-dom";
import {NotificationManager} from "react-notifications";
import Axios from '../../utils/axios';
import {loadAdmins} from "../../redux/actions/admins/index";
import {getAllParentEvents} from "../../redux/actions/events";
import {organizationId} from '../../utils/CONSTANTS';

const { Option } = Select;


class EventsAssign extends React.Component {

  componentDidMount(){
    this.props.loadAdmins()
    this.props.getAllParentEvents(organizationId)
    // console.log('assign event didmount', this.props)
        // Axios.post(`/admins/assign-events`,{
        //       uuid : "f36669ce-5e98-4d7b-9963-98fa8870e3d6",
        //       eventSlotIds : ["8624a4d0-ce6d-4adb-9b5d-a2467176b112"]
        //     }
        // )
        // .then((response) => {
        //   console.log('assign event response', response)
        // })
        // .catch(err => {
        //   console.log('assign event error', err)
        // });
  }

  handleSubmit = e => {
    e.preventDefault();
        this.props.form.validateFields((err, values) => {
          console.log('assign event values', values)
            if (!err) {
                Axios.post(`/admins/assign-events`,{
                        uuid : values.uuid,
                        eventSlotIds : values.eventSlotIds,
                    }
                )
                .then((response) => {
                  console.log('assign event response', response)
                  NotificationManager.success('Event Assign Successfully')
                })
                .catch(err => {
                  console.log('assign event error', err?err.response.data._error:'')
                  NotificationManager.error(err?err.response.data._error:'error')
                  // state.eventCategories.error? state.eventCategories.error.response.data._error && state.eventCategories.error.response.data._error && state.eventCategories.error.response.data._error: '',
                });
            }
        })
  };


  render() {
    console.log('assign event render', this.props.ParentEventsList.data)
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
                    <CardBody>
                        <Col md={12} style={{float: 'left'}}>
                            <p style={{fontSize: '25px'}}>Events Assign to Users</p>
                        </Col>

                        <Col md={12} style={{float: 'left'}}>

                                <Form onSubmit={this.handleSubmit}>
                                      <Form.Item label="Select User">
                                          {getFieldDecorator('uuid', {
                                            rules: [{ required: true, message: 'Please select user' }],
                                          })(
                                            <Select placeholder="Please select user">
                                                  {this.props.admins.map((admin,i)=>{
                                                      return(
                                                        <Option value={admin.uuid} key={i}>{admin.username}</Option>
                                                      )
                                                    })
                                                  }
                                            </Select>
                                          )}
                                      </Form.Item>
                                      <Form.Item label="Select Event" className="multiple-tags-design">
                                          {getFieldDecorator('eventSlotIds', {
                                            rules: [{ required: true, message: 'Please select event', type: 'array' }],
                                          })(
                                            <Select mode="multiple" placeholder="Please select event">
                                                  {this.props.ParentEventsList.data.map((admin,i)=>{
                                                      return(
                                                        <Option value={admin.parentEvent} key={i}>{admin.title}</Option>
                                                      )
                                                    })
                                                  }
                                            </Select>,
                                          )}
                                      </Form.Item>
                                      <Form.Item style={{marginTop:'40px'}}>
                                          <Button className="red-btn" type="primary" htmlType="submit">
                                              Assign Event to User
                                          </Button>
                                      </Form.Item>
                                </Form>

                        </Col>

                    </CardBody>
            </Card>
          </Col>
        </Row>

      </div>
    );
  }
}
const mapStateToProps = (state) => {
  console.log('assign event didmount2', state)
  return {
    admins: state.Admins.admins,
    ParentEventsList: state.ParentEventsList,
    // error :state.eventCategories.error? state.eventCategories.error.response.data._error && state.eventCategories.error.response.data._error && state.eventCategories.error.response.data._error: '',
  }
};



const EventsAssignComponent = Form.create()(EventsAssign);
export default withRouter(connect(mapStateToProps, {loadAdmins,getAllParentEvents})(EventsAssignComponent));
