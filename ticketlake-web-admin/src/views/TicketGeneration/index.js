import React from 'react';
import {
    Row,
    Col,
    Button,
    FormGroup,CardHeader,Input,
    Label,
    Card,
    CardBody,Form,
} from 'reactstrap';
import ModalPopup from '../../CommonComponents/ModalPopUp';
//import {saveTicket} from "../../redux/actions/tickets/index";

import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {NotificationManager} from "react-notifications";


class TicketGeneration extends React.Component {


        state = {
            modalOpen: false,
            eventId:'',
            userId:'454654646',
            mainEventId:'12345',
            formName: 'Generate Ticket',
            purchaseType:'',
            ticketClassId:'',
            name: '',
            phoneNumber: '',
            email: '',
            DOB: '',
            sectionId:'',
            rowNumber:'',
            seatNumber:''
        };

    componentDidMount() {
        window.scrollTo(0, 0)
        let eventId =  this.props.eventId;
        this.state.eventId = eventId;
            this.setState({eventId});

    }

    openDeleteModal = (props) => this.setState({modalOpen: !this.state.modalOpen,DeleteInfo: props});


    onChange = (e) => {

        let {target} = e;
        let state = {...this.state};

        state[target.name] = target.value;

        this.setState(state);
    };
    handleClick = () => {
        this.props.saveTicket(this.state.mainEventId,this.state.eventId,this.state.userId,this.state.purchaseType,this.state.ticketClassId,this.state.name,
            this.state.phoneNumber,this.state.email, this.state.DOB,
            this.state.sectionId,this.state.rowNumber,this.state.seatNumber,
            () =>{   NotificationManager.success('Created Successfully')},   () =>{NotificationManager.error( this.props.error,'Error')});


    }
    getForm = () => {
        return (
            <Form action="" method="post" className="form-horizontal">

                <FormGroup row>
                    <Col md="2">
                        <Label style={{fontSize:'15px'}}>Username</Label>
                    </Col>
                    <Col md="11">
                        <Input type="text"  name="name" placeholder="Enter Username" onChange={this.onChange}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col md="2">
                        <Label style={{fontSize:'15px'}}>Phone</Label>
                    </Col>
                    <Col md="11">
                        <Input type="number"  name="phoneNumber" placeholder="Enter Phone" onChange={this.onChange}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col md="2">
                        <Label style={{fontSize:'15px'}}>Email</Label>
                    </Col>
                    <Col md="11">
                        <Input type="text"  name="email" placeholder="Enter Email" onChange={this.onChange}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col md="2">
                        <Label style={{fontSize:'15px'}}>DOB</Label>
                    </Col>
                    <Col md="11">
                        <Input type="date"  name="DOB" placeholder="Enter DOB" onChange={this.onChange}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col md="2">
                        <Label style={{fontSize:'15px'}}>Ticket Type</Label>
                    </Col>
                    <Col md="11">
                        <Input type="text"  name="purchaseType" placeholder="Enter Type" onChange={this.onChange}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col md="2">
                        <Label style={{fontSize:'15px'}}>Ticket Class</Label>
                    </Col>
                    <Col md="11">
                        <Input type="select" name="ticketClassId" onChange={this.onChange}>>
                            <option value="" disabled selected >Select your option</option>

                            {this.props.ticketClass && this.props.ticketClass.map(ticket => {
                                    return (
                                        <option key={ticket._id} value={ticket.ticketClassId}>
                                            {ticket.ticketClassId} - {ticket.ticketClassPrice + '/ticket'}
                                        </option>
                                    )
                                }
                            )}
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col md="2">
                        <Label style={{fontSize:'15px'}}>Section</Label>
                    </Col>
                    <Col md="11">
                        <Input type="text"  name="sectionId" placeholder="Enter section" onChange={this.onChange}/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col md="2">
                        <Label style={{fontSize:'15px'}}>Row no</Label>
                    </Col>
                    <Col md="11">
                        <Input type="number"  name="rowNumber" placeholder="Enter section" onChange={this.onChange}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col md="2">
                        <Label style={{fontSize:'15px'}}>Seat no</Label>
                    </Col>
                    <Col md="11">
                        <Input type="number"  name="seatNumber" placeholder="Enter section" onChange={this.onChange}/>
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Button style={{backgroundColor:'red', border:'none'}} className="button" onClick={this.handleClick}>
                        Generate
                    </Button>
                </FormGroup>



            </Form>
        );
    };



    render() {
        const {isPassButtonSelection} = this.props;
        return (
            <Row>
                < Col sm="12">
                    <Card>
                        <CardHeader className="p-3 border-bottom mb-0">
                            <Row>
                                <Col>{isPassButtonSelection ?  "Generate Pass" :  this.state.formName + ' for ' + this.props.event } </Col>
                                <Col>
                            <Button style={{float:"right",backgroundColor:'red',border:'none'}} className="button"  onClick={() => {
                                this.openDeleteModal()}}>
                                Add another person
                            </Button>
                                </Col>
                                </Row>
                        </CardHeader>
                        <CardBody>
                            {this.getForm()}
                        </CardBody>

                    </Card>
                </Col>
                <ModalPopup isFormEnabled={true} deleteText = "venue" onClick={() => {
                    this.openDeleteModal(null);
                }} openDeleteModal ={this.openDeleteModal} modalOpen ={this.state.modalOpen}/>

            </Row>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        eventId :state.Events && state.Events.event && state.Events.event._id,
        ticketClass :state.Events && state.Events.ticketClass && state.Events.ticketClass.ticketClasses,
        event:state.Events && state.Events.event && state.Events.event.eventTitle,
        error:state.Tickets && state.Tickets.error,
    }
};
const connectedComponent = connect(mapStateToProps, {})(TicketGeneration);
export default withRouter(connectedComponent);
