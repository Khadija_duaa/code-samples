import React, { Component } from 'react'
import { Row, Col, Button } from 'reactstrap'
import Pass from '../../components/passes'
import { type } from 'os';
export default class Passes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            passes: [""],
            passesData: [],
            parentEventId: ""
        }
    }
    componentWillMount() {
        if(this.props.eventSlotDetail) {
            if(this.props.eventSlotDetail.parentEventInfo.passConfigs.length > 0) {
                this.setState({
                    passesData: this.props.eventSlotDetail.parentEventInfo.passConfigs,
                    parentEventId: this.props.eventSlotDetail.parentEventId
                })
                let _passes = []
                for(let i =0; i< this.props.eventSlotDetail.parentEventInfo.passConfigs.length -1; i++)
                    _passes.push("")
                this.setState({passes: this.state.passes.concat(_passes)}) 
            }
        }
    }

    addMorePass = () => {
        this.setState({ passes: this.state.passes.concat([""]) })
    }

    deletePass = (index) => {
        const _state = { ...this.state }
        delete _state.passes[index]
        this.setState(_state)
    }

    render() {
        let data;
        return (
            <div className="mt-3">
                <Row>
                    <Col md={{ size: 8, offset: 2 }}>
                        <Row>



                            <Col md={{ size: 12 }}>
                                <h3>Pass Configuration</h3>


                                {
                                    this.state.passes.map((pass, index) => {
                                        if(typeof this.state.passesData[index] !== 'undefined') data = this.state.passesData[index]
                                        console.log('sending data to component ', data)
                                        return (
                                            <Row>
                                                <Col md={{ size: 10 }}>
                                                    <Pass key={index} id={index} passData = {data} parentEventId = {this.state.parentEventId} eventData = {this.props.eventSlotDetail}/>
                                                </Col>
                                                <Col md={{ size: 2 }}>
                                                    {index > 0 ? <Button onClick={() => this.deletePass(index)}><i className="fas fa-trash-alt"></i></Button> : null}
                                                </Col>
                                            </Row>

                                        )
                                    })
                                }
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col md={{ size: 8, offset: 2 }}><Button onClick={this.addMorePass}>Add More Pass</Button></Col>

                </Row>

            </div>

        )
    }
}