import React from 'react';
import { Row, Col, Button } from 'reactstrap';
import { Form, Tabs, Select, Input, InputNumber, DatePicker, Checkbox} from 'antd';
import Axios from '../../utils/axios';
import TagsInput from "react-tagsinput";

const { TextArea } = Input;
const { TabPane } = Tabs;
const { Option } = Select;
const { RangePicker } = DatePicker;


class addCouponsComponnet extends React.Component {
    constructor(props){
        super(props)
        this.state={
            defaultActiveKey:1,
            tags: []
        }
    }
    handleChange=(tags)=>{
        this.setState({
            tags:tags,
        })
      }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
          }
        });
      }
    helloWorld = e => {
        console.log('continue')
        // this.update(number)
        var numberIs = this.state.defaultActiveKey+1
        console.log('numberIs', numberIs)
        this.setState({
                    defaultActiveKey:numberIs,
        })
        // console.log('this.state.defaultActiveKey',this.state.defaultActiveKey)
    }
    cancelBtn = e => {
        this.setState({
            defaultActiveKey: this.state.defaultActiveKey-1,
        })
    }
    // update=()=>{
    //     this.setState({
    //         defaultActiveKey:'2',
    //     })
    // }
    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 4},
            wrapperCol: { span: 20 },
          };
          const rangeConfig = {
            rules: [{ type: 'array', required: true, message: 'Please select date!' }],
          };
        return (
                 <>   
                    <Col md="12" className="margin-bottom-40">
                        <h3><b>Create Coupon</b></h3>
                    </Col>
                    <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <Col md="12">
                        <div className="card-container coupon-tabs-design">
                            <Tabs type="card" activeKey={JSON.stringify(this.state.defaultActiveKey)} defaultActiveKey={JSON.stringify(this.state.defaultActiveKey)}>
                                <TabPane tab="1. General" key={"1"}>
                                    <Row>
                                        <Col md="12" className="margin-top-40">
                                            <h4><b>General</b></h4>
                                            <p><b>Overview of Coupon</b></p>
                                        </Col>
                                        <Col md="8">
                                            <Form.Item label="Applicable to" className="custom-width">
                                                {getFieldDecorator('applicableTo', {
                                                    rules: [{ required: true, message: 'Please select any field!' }],
                                                })(
                                                    <Select placeholder="Please select any field">
                                                        <Option value="event">Event</Option>
                                                        <Option value="interest">Interest</Option>
                                                    </Select>,
                                                )}
                                            </Form.Item>
                                        </Col>
                                        <Col md="4" style={{lineHeight: '37px', color: "#929292"}}>Event, Interest, Organiser, All Users</Col>
                                        <Col md="8">
                                            <Form.Item label="Event(s)" >
                                                {getFieldDecorator('events', {
                                                    rules: [{ required: true, message: 'Field can not empty' }],
                                                })(
                                                    <Input />,
                                                )}
                                            </Form.Item>
                                            <Form.Item label="Event(s)" >
                                                <TagsInput
                                                    className='border height-auto form-control'
                                                    value={this.state.tags} 
                                                    onChange={this.handleChange}
                                                    preventSubmit = {true}
                                                    name = "tags"
                                                    // onChange={this.handleRegularTagsChange}
                                                    tagProps={{removeKeys:"x", placeholder: '', className: "tags-bg react-tagsinput-tag bg-info text-white rounded", }}
                                                />
                                            
                                            </Form.Item>
                                            <Form.Item label="Discount" >
                                                {getFieldDecorator('discount', {
                                                    rules: [{ required: true, message: 'Field can not empty' }],
                                                })(
                                                    <InputNumber addonAfter="$" />,
                                                )}
                                            </Form.Item>
                                            <Form.Item label="Quantity" >
                                                {getFieldDecorator('quantity', {
                                                    rules: [{ required: true, message: 'Field can not empty' }],
                                                })(
                                                    <InputNumber />,
                                                )}
                                            </Form.Item>
                                            <Form.Item label="Active Now" style={{marginBottom:'0px'}}>
                                                {getFieldDecorator('activeNow', {
                                                    valuePropName: 'checked',
                                                    initialValue: true,
                                                })(<Checkbox>Coupon will be instantly avaibale for use</Checkbox>)}
                                            </Form.Item>
                                            <Form.Item label="Unique Code" style={{marginBottom:'0px'}}>
                                                {getFieldDecorator('uniqueCode', {
                                                    valuePropName: 'checked',
                                                    initialValue: false,
                                                })(<Checkbox>Unique coupons will be send to each user</Checkbox>)}
                                            </Form.Item>
                                            <Form.Item label="Code Series" style={{marginBottom:'0px'}}>
                                                {getFieldDecorator('codeSeries', {
                                                    valuePropName: 'checked',
                                                    initialValue: false,
                                                })(<Checkbox>All Coupons with series format i.e CA***</Checkbox>)}
                                            </Form.Item>
                                            <Form.Item label="Format" >
                                                {getFieldDecorator('format', {
                                                    rules: [{ required: true, message: 'Field can not empty'}],
                                                })(
                                                    <Input placeholder="CX-XXXXXX"/>,
                                                )}
                                            </Form.Item>
                                                <span className="red-btn" onClick={(e)=>this.helloWorld(e)}>
                                                    Continue
                                                </span>{' '}
                                                {/* <a className="border-btn" onClick={(e)=>this.cancelBtn(e)}>
                                                    Cancel
                                                </a> */}
                                        </Col>
                                    </Row>
                                </TabPane>
                                <TabPane tab="2. Usage Limits" key="2">
                                    <Row>
                                        <Col md="12" className="margin-top-40">
                                            <h4><b>Usage Limits</b></h4>
                                            <p><b>Overview of Coupon</b></p>
                                        </Col>
                                        <Col md="6">
                                            <Form.Item label="Valid Duration" className="custom-width">
                                                {getFieldDecorator('range-picker', rangeConfig)(<RangePicker />)}
                                            </Form.Item>
                                        </Col>
                                        <Col md="4" style={{lineHeight: '37px', color: "#929292"}}></Col>
                                        <Col md="6">
                                            <Form.Item label="Usage Limit/user" className="custom-width">
                                                {getFieldDecorator('vaidDuration', {
                                                    rules: [{ required: true, message: 'Please select any field!' }],
                                                })(
                                                    <Select placeholder="Please select any field">
                                                        <Option value="once">Onece</Option>
                                                        <Option value="unlimited">Unlimited</Option>
                                                        <Option value="specificaccount">Specific Account</Option>
                                                    </Select>,
                                                )}
                                            </Form.Item>
                                            <Form.Item>
                                                <span className="red-btn" onClick={(e)=>this.helloWorld(e)}>
                                                    Continue
                                                </span>{' '}
                                                <span className="border-btn" onClick={(e)=>this.cancelBtn(e)}>
                                                    Back
                                                </span>
                                            </Form.Item>
                                        </Col>
                                        <Col md="4" style={{lineHeight: '37px', color: "#929292"}}>Once, Unlimied or Specific Account</Col>
                                    </Row>
                                </TabPane>
                                <TabPane tab="3. Target Audience" key="3">
                                    <Row>
                                        <Col md="12" className="margin-top-40">
                                            <h4><b>Select an Audience to distribute</b></h4>
                                            <p><b>Audience Selection</b></p>
                                        </Col>
                                        <Col md="6">
                                            <Form.Item label="Audience" className="custom-width">
                                                {getFieldDecorator('audience', {
                                                    rules: [{ required: true, message: 'Please select any field!' }],
                                                })(
                                                    <Select placeholder="Audience Name #">
                                                        <Option value="once">Onece</Option>
                                                        <Option value="unlimited">Unlimited</Option>
                                                        <Option value="specificaccount">Specific Account</Option>
                                                    </Select>,
                                                )}
                                            </Form.Item>
                                        </Col>
                                        <Col md="4" style={{lineHeight: '37px', color: "#929292"}}></Col>
                                        <Col md="6">
                                            <Form.Item label="Audience Count" className="custom-width">
                                                {getFieldDecorator('audienceCount', {
                                                    rules: [{ required: true, message: 'Please select any field!' }],
                                                })(
                                                    <Input />,
                                                )}
                                            </Form.Item>
                                        </Col>
                                        <Col md="4" style={{lineHeight: '37px', color: "#929292"}}></Col>
                                        <Col md="6" className="margin-bottom-40">
                                            <Form.Item label="Notification" style={{marginBottom:'0px'}}>
                                                {getFieldDecorator('notification', {
                                                    valuePropName: 'checked',
                                                    initialValue: true,
                                                })(<Checkbox>In-app notiication will be sent to selected audience</Checkbox>)}
                                            </Form.Item>
                                            <Form.Item label="SMS" style={{marginBottom:'0px'}}>
                                                {getFieldDecorator('sms', {
                                                    valuePropName: 'checked',
                                                    initialValue: false,
                                                })(<Checkbox>SMS will be sent to selected audience</Checkbox>)}
                                            </Form.Item>
                                            <Form.Item label="Email" style={{marginBottom:'0px'}}>
                                                {getFieldDecorator('email', {
                                                    valuePropName: 'checked',
                                                    initialValue: false,
                                                })(<Checkbox>An email will be sent to selected audience</Checkbox>)}
                                            </Form.Item>
                                        </Col>
                                        <Col md="4" style={{lineHeight: '37px', color: "#929292"}}></Col>

                                        <Col md="6">
                                            <Form.Item label="Caption" className="custom-width">
                                                {getFieldDecorator('caption', {
                                                    rules: [{ required: true, message: 'Please select any field!' }],
                                                })(
                                                    <Input />,
                                                )}
                                            </Form.Item>
                                        </Col>
                                        <Col md="4" style={{lineHeight: '37px', color: "#929292"}}></Col>

                                        <Col md="6">
                                            <Form.Item label="Description" className="custom-width">
                                                {getFieldDecorator('description', {
                                                    rules: [{ required: true, message: 'Please select any field!' }],
                                                })(
                                                    <TextArea autosize={{ minRows: 6, maxRows: 6 }}/>,
                                                )}
                                            </Form.Item>
                                        </Col>
                                        <Col md="4" style={{lineHeight: '37px', color: "#929292"}}></Col>
                                    </Row>
                                    <Form.Item>
                                                <Button className="red-btn" htmlType="submit">
                                                    Save
                                                </Button> {' '}
                                                <span className="border-btn" onClick={(e)=>this.cancelBtn(e)}>
                                                    Back
                                                </span>
                                    </Form.Item>
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                    </Form>
                 </>
               
        )
    }
}
const addCoupons = Form.create()(addCouponsComponnet);
export default addCoupons;