import React from 'react';
import {
  Table,
  Row,
  Col,
  Card,
  Button,
  Modal, ModalBody, ModalFooter, ModalHeader,
  Form,FormGroup,InputGroup,Input
} from 'reactstrap';
import {connect} from "react-redux";
import {getAllEvents,searchEvents,setEvent,getEventDetail} from "../../redux/actions/events";
import {loadCategories} from "../../redux/actions/eventCategory/index";
import {withRouter, Link, Switch, Route} from "react-router-dom";
import TableHeads from '../../utils/Headers';
import Loader from '../../utils/loader';
import addCoupons from './addCoupons';

const header = ["Coupon Code", "Value", "Sent to", "Used by", "Status"];
const searchFields = ['eventTitle', '_id', 'eventTitle', 'eventTitle', 'eventDateTimeSlot.eventStartTime', 'eventDateTimeSlot.eventEndTime'];

class Coupons extends React.Component {
    state = {
        paginate: false,
        page:0,
        skip:0,
        pageSize:10,
        search: "",
        value:'',
    };

    componentDidMount() {
        let s ={...this.state};
        this.props.getAllEvents(s);
    }





    onInputChange = (e) => {
        let {target} = e;
        let state = {...this.state};
        state[target.name] = target.value;
        this.setState(state);
        this.getFilters(state);
    };

openActivateModal = (venue) => this.setState({modalOpen: !this.state.modalOpen});

getModalContent = () => {
    return (
    <Modal isOpen={this.state.modalOpen} toggle={this.openActivateModal} className={'modal-danger ' + this.props.className}>
        <ModalHeader toggle={this.openActivateModal}> Activate Coupon </ModalHeader>
        <ModalBody>Are you sure you want to Activate this Coupons ?</ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={() => { this.openActivateModal(null); }}>Activate</Button>{' '}
          <Button color="primary" onClick={this.openActivateModal}> Cancel </Button>
        </ModalFooter>
    </Modal>
    );
  };


    renderAllEventData = () => {
        if(!this.props.events) return null;
        return (
            Array.isArray(this.props.events )&& this.props.events.map((event, i) => {
                return (
                    <tr key={i}>
                        <td>
                            X8878001
                        </td>
                        <td>40% off</td>
                        <td>
                                {event.categories?event.categories.map((cat,i)=>{
                                    return <span key={i}>{cat.name?cat.name:'Category not assign'} </span>
                                    }):''
                                }
                        </td>
                        <td>
                            {event.venue?event.venue.address:''}
                        </td>
                        <td>
                            <span className="view p-r-20 link" key="view"  onClick={this.openActivateModal}>
                                Activate
                            </span>
                        </td>
                    </tr>)
            })

        );
    };



    searchField = () => {
        return (
            <Form className="form-horizontal" style={{float:'left'}}>
                <FormGroup>
                    <InputGroup>
                        <Input type="text" id="input1-group2" name="input1-group2" placeholder="Type here..."
                               onChange={(e) => this.props.searchEvents(e.target.value, searchFields)}/>
                        <span className="search_icon"><i className="fa fa-search"/></span>
                    </InputGroup>
                </FormGroup>
            </Form>
        );
    };

    getTableHeaders = () => {
        return (
            <TableHeads>
                {header}
            </TableHeads>
        );
    };


    render() {
        return <div>
            {this.props.processing ? <Loader/> :
                <Switch>
                    <Route path={'/coupons/addCoupons'} component={addCoupons}/>
                <Row key="new key">
                    {this.getModalContent()}
                <Col md="12" className="margin-bottom-15">
                    <h3><b>Coupons</b></h3>
                </Col>
                    <Col>
                        <Row>
                            <Col sm={8}>
                                {this.searchField()}
                            </Col>
                            <Col sm={4}>
                                <div style={{float: 'right'}}>
                                    <Link to="/coupons/addCoupons">
                                        <Button color="primary">
                                            + Add New Coupon
                                        </Button>
                                    </Link>
                                </div>
                            </Col>
                            
                        </Row>
                        <Row>
                            <Col md="12">
                                <Card>
                                    <Table bordered hover striped responsive>
                                        <thead>
                                        {this.getTableHeaders()}
                                        </thead>
                                        <tbody>
                                        {this.renderAllEventData()}
                                        </tbody>
                                    </Table>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                    </Switch>
            }

        </div>
  }
}

const mapStateToProps = (state) => {
    return {
        processing: state.Events.processing,
        events: state.Events.allEvents,
        message: state.Events.message,
        error: state.Events.error,
    }
};
// const mapDispatchToProps = (data) =>{
//     console.log('data')
//     // return{
//     //     getAllEvents : (data)=>this.dispatch(getAllEvents(data))
//     // }
// }

const connectedComponent = connect(mapStateToProps, {getAllEvents,searchEvents,setEvent,getEventDetail,loadCategories})(Coupons);
export default withRouter(connectedComponent);
