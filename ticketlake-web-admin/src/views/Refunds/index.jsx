import React, {Component} from 'react'
import {connect} from 'react-redux'
import EventListing from '../../components/eventListing/allPublicEvents'
import PurchaseTicketsList from '../../components/TicketPurchasedList'
import {Route, Switch} from 'react-router-dom'
class Refunds extends Component {
    constructor(props) {
        super(props)
        this.state = {
            purchasedTickets: []
        }
    }
    componentDidMount() {
        window.scrollTo(0, 0)
    }
    handleEventClick = (eventId) => {
        this.props.history.push(`/refunds/event/ticket/${eventId}`)
    }
    render() {
        return (
            <React.Fragment>
                <Switch>
                <Route exact = {true} path={'/refunds/event/ticket/:eventId'} component={PurchaseTicketsList}/>
                <EventListing callback = {this.handleEventClick}/>
                </Switch>
                
            </React.Fragment>
            
        )
    }
}

const mapStateToProps = (state) => {
    return state
}

export default connect(mapStateToProps, {})(Refunds)