import React, {Component} from 'react';
import {
  Row,
  Col,
  CardTitle,
  CardText,
  Card,
  CardBody
} from 'reactstrap';
import { Route, Link, Switch } from 'react-router-dom';
import StandardEvent from '../../views/events/standard'
import SeriesEvent from '../../views/events/series'
import RecurringEvent from '../../views/events/recurring';
import EventListing from '../../components/eventListing'
import EventSlotListing from '../../components/eventListing/eventSlotListing'
import EventWithPasses from '../../views/events/standard/formSteps'
import { resetEventBasicInfo } from '../../redux/actions/events'
import { resetSlot } from '../../redux/actions/eventSlot'
import {connect} from 'react-redux'
// Style
const cardStyle = {
    minHeight: '210px',
    height: 210,
    padding: '0 0',
    width: '100%',
    backgroundColor: 'rgb(249, 249, 249)',
    boxShadow: '0 0 8px #bdbdbd'
};
const cardHeadingStyle = {
    textAlign: 'center',
    fontWeight: 700,
    fontSize : '25px',
    color: '#000'
};
const textStyle = {
    color: '#000',
    textAlign: 'center',
    fontSize: '15px'
};

class EventTypeSelection extends Component {
    render() {
        return (
            <div style = {{marginTop: 5}}>
                <Row className = "mt-1">
                    <Col md="6">
                        <h3><b>Create An Event</b></h3>
                    </Col>
                </Row>
                <Row>
                    <Col md="6">
                        <h6><b>Select occurrence of your event</b></h6>
                    </Col>
                </Row>
                <Row className = "mt-3">
        
                    {
                        eventSelectionScreens.map((screen, key) => {
                            return (
                                screen.name != 'Event' ? 
                                <Col md="3" key={key}>
                                    <Link to={screen.path} key={key} >
                                        <Card style={cardStyle}>
                                            <CardBody>
                                                <CardTitle style={cardHeadingStyle}>
                                                    {screen.name}
                                                </CardTitle>
                                                <hr/>
                                                <CardText style={textStyle}>
                                                    {screen.description}
                                                </CardText>
                                            </CardBody>
                                        </Card>
                                    </Link>
                                </Col> : null
                            )
                        })
                    }
                </Row>
                </div>
            )
    }
    
}
const eventSelectionScreens = [{
    name: 'Standard Event',
    path: '/events/standard/',
    description: 'Single Event at same venue with single slot i.e single day conference',
    exact: false,
    component: StandardEvent
},{
    name: 'Series of Event',
    path: '/events/series',
    description: 'Multiple Events with single / Multiple Slots i.e League/Series',
    exact: false,
    component: SeriesEvent
},{
    name: 'Recurring Event',
    path: '/events/recurring',
    description: 'Single Event occuring regularly for a selective duration. i.e Movie Screening',
    exact: false,
    component: RecurringEvent
}]


class EventSelection extends Component {
    componentDidMount() {
        window.scrollTo(0, 0)
        this.props.resetEventBasicInfo()
        this.props.resetSlot()
    }
    render() {
        const routes = eventSelectionScreens.map((prop, key) => {
            return <Route exact = {prop.exact} path={prop.path} key={key} component = {prop.component}/>;
        });
        return(
            <div>
                <Switch>
                    <Route exact = {true} path =  '/events/passes' component = {EventWithPasses} ></Route>
                    <Route exact = {true} path = '/events/create' component = {EventTypeSelection}> </Route>
                    <Route exact = {true} path = '/events' component = {EventListing}> </Route>
                    <Route exact = {true} path = '/events/detail/:parentEventId/:eventType/:title' component = {EventSlotListing}> </Route>
                </Switch>
                {routes}
                {/* {this.props.location.pathname === '/events'? this.cards(): routes} */}
            </div>
            
        )
    }
}
const mapStateToProps = (state) => {
    return state
}
export default connect(mapStateToProps, {resetEventBasicInfo, resetSlot})(EventSelection)


