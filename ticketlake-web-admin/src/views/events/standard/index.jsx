import React, { Component } from 'react';
import StepZilla from 'react-stepzilla';
import {
	Card,
	CardBody,
	CardTitle
} from 'reactstrap';

import EventBasicInfo from '../../../components/basicInfo';
import EventTicketClasses from '../../../components/ticketClasses';
import {getEventDetail} from '../../../redux/actions/eventSlot'
import Details from './Details';
import {connect} from 'react-redux'
import Loader from '../../../components/loader';
import {resetEventBasicInfo} from '../../../redux/actions/events'
import {resetSlot} from '../../../redux/actions/eventSlot'
class formSteps extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loader: false,
			showNext: false,
			slotId: "",
			eventSlot: null,
			eventId: ""
		};
	}
	showNext = (flag) => {
		this.setState({
			showNext: flag
		})
	}
	componentWillMount() {
		this.props.resetEventBasicInfo()
		this.props.resetSlot()
	}
	componentDidMount() {
		window.scrollTo(0, 0)
		if(this.props.location.search !== "") {
			const slotId = this.props.location.search.split("?")[1]
			this.state.slotId = slotId
			this.props.getEventDetail(slotId)
		}
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.EventSlot !== this.props.EventSlot) {
			let _state = {...this.state}
			_state.eventSlot = nextProps.EventSlot.data
			_state.loader = false
			this.setState(_state)
		}
	}
	render() {
		const {EventSlot} = this.props
		const steps =
			[
				{ name: 'Basic Information', component: <EventBasicInfo eventType = "STANDARD" loader = {this.state.loader} 
				showNext = {this.showNext} eventSlotDetail = {this.state.eventSlot}/> },
				{ name: 'Ticket Classes', component: <EventTicketClasses eventType = "STANDARD" showNext = {this.showNext}   loader = {this.state.loader}
				 eventSlotDetail = {this.state.eventSlot}/> },
				{ name: 'Details', component: <Details showNext = {this.showNext} id = {0} eventSlotDetail = {this.state.eventSlot}/> },
			]

		return (
			<Card>
				<CardBody className="border-bottom">
					<CardTitle className="mb-0">
						<h2>{
							this.state.slotId ? "Update Event": "Create New  Event"}</h2>
					</CardTitle>
				</CardBody>
				<CardBody>
					{EventSlot.loading ? <Loader/>: null}
					<div className='example'>
						<div className='step-progress'>
							<StepZilla
								preventEnterSubmission = {true}
								stepsNavigation = {false}
								onStepChange = {this.stepChange}
								showNavigation = {true}
								steps={steps}
							/>
						</div>
					</div>
				</CardBody>
			</Card>

		)
	}
}
const mapStateTopProps = (state) => {
	return state
}
export default connect(mapStateTopProps, {getEventDetail, resetEventBasicInfo, resetSlot})(formSteps);
