import React, { Component } from 'react';
import {Row, Col, Input, Table,
	ModalBody,ModalFooter,
	Modal, FormGroup, Label, FormText, Button,
	ButtonGroup
} from 'reactstrap';
import swal from 'sweetalert';
import Seats from '../../../components/seats'
import Venue from '../../../components/venues'
import EventSpeakers from '../../../components/eventSpeakers'
import { getAllVenues } from '../../../redux/actions/venues'
import { saveEventSlot, resetEventSlotState } from '../../../redux/actions/events'
import { uploadImage,resetImageState } from '../../../redux/actions/images'
import ListTicketClasses from '../../../components/listTicketClasses'
import { connect } from 'react-redux'
import EventSchedule from '../../../components/schedule'
import DropImage from '../../../components/imageUpload'
import moment from 'moment'
import Loader from '../../../components/loader'
import {NotificationManager} from "react-notifications";
import uuid from "uuid";
import VenueComponent from '../../Venues/VenueForm'
import _ from 'lodash'
import { Popconfirm, message } from 'antd';
class Details extends Component {
	constructor(props) {
		super(props);
		this.state = {
			details: {
				eventDateTimeSlot: {
					eventStartTime: '',
					eventEndTime: ''
				},
				eventEntryTime: {
					entryStartTime: '',
					entryEndTime: ''
				},
				eventImagesKeys: [],
				bannerImageKey: '',
				isFeatured: true,
				venueId: '',
				isOnlineSaleOpen : 0,
				isPublished : 0,
				isTicketsPictureProtected : true,
				isTicketsPasswordProtected: true,
				ticketClassesConfig: this.props.ticketClassesConfig || [],
				sections: []
			},
			galleryFiles: [],
			bannerFile: null,
			uploadedImage: '',
			modal: true,
			show: false,
			showSpeakersSection: false,
			availableSeats: 1,
			totalVenueSeats: 0,
			selectedClass: {
				ticketClassName: '',
				ticketClassColor: '',
				ticketClassPrice: '',
				ticketClassId: '',
				_id: ''
			},
			slotSaved:false,
			reservedSeats: [[]],
			ticketClassesCount: [],
			errors :{},
			venues: [],
			title: "",
			eventType: "",
			agenda: "",
			uniqueSections: [],
			Venuemodal: false,
			assgiendSeats: 0,
			parentEventId: null,
			ticketClasses: [],
			eventSlotId: null,
			availableEventSeats: [[]]
		};
		this.saveEventSlot = this.saveEventSlot.bind(this)
	}
	toggle = () => {
		this.setState({
			showSpeakersSection: !this.state.showSpeakersSection
		})
	}
	showVenue = (flag) => {
		this.setState({
			Venuemodal: !this.state.Venuemodal
		}, function() {
			this.props.getAllVenues()
		})
	}
	showVenueModal = () => {
		return (<Modal isOpen={this.state.Venuemodal} toggle={this.showVenue} className={this.props.className} size="lg">
          <ModalBody>
			  	<VenueComponent redirect  = {false} closeModal = {this.showVenue}/>
          </ModalBody>
        </Modal>)

	}
	setSeats = (seats) => {
		this.setState({seats})
	}
	loadSpeakers = () => {
		this.setState({
			show: !this.state.show
		}, function() {
		});
	}
	showSpeakerSection = ()=> {
		this.setState({
			showSpeakersSection: !this.state.showSpeakersSection
		})
	}
	handleEventStartTime = ({_d}) => {
		let _state = {...this.state}
		_state.details.eventDateTimeSlot.eventStartTime = _d
		_state.errors.eventStartTime = false
		this.setState (_state)
	}
	handleEventEndTime = ({_d}) => {
		let _state = {...this.state}
		_state.details.eventDateTimeSlot.eventEndTime = _d
		_state.errors.eventEndTime = false
		this.setState (_state)

	}
	handleEventEntryStartTime = ({_d}) => {
		let _state = {...this.state}
		_state.details.eventEntryTime.entryStartTime = _d
		_state.errors.entryStartTime = false
		this.setState (_state)
	}
	handleEventEventEntryEndTime = ({_d}) => {
		let _state = {...this.state}
		_state.details.eventEntryTime.entryEndTime = _d
		_state.errors.entryEndTime = false
		this.setState (_state)
	}
	
	handleVenueChange =(event) => {
		const venue = event.target.value.split(" ")
		let _state = {...this.state}
		_state.details.venueId = venue[0] + " " + venue[1]
		_state.availableSeats = venue[1]
		_state.totalVenueSeats = venue[1]
		_state.reservedSeats = [[]]
		_state.assgiendSeats = 0
		for (let key in _state.ticketClassesCount) {
			_state.ticketClassesCount[key] = 0
		}
		this.setState(_state)

	}
	componentDidMount() {
		window.scrollTo(0, 0)
	}
	componentWillMount() {
		let _state = {...this.state}
		const { StandardEvent, eventSlotDetail, seriesDetail, EventSlot} = this.props
		this.props.getAllVenues()
		let _tickets = []
		if (seriesDetail) {
			_state.details.eventDateTimeSlot.eventStartTime = new Date(seriesDetail.eventTime.eventStartTime)
			_state.details.eventDateTimeSlot.eventEndTime = new Date(seriesDetail.eventTime.eventEndTime)
			_state.details.eventEntryTime.entryStartTime = new Date(seriesDetail.eventTime.entryStartTime)
			_state.details.eventEntryTime.entryEndTime = new Date(seriesDetail.eventTime.entryEndTime)
			this.setState(_state, function(){

			})
		}
		if(EventSlot.data) {
			const {data: event} = EventSlot
			this.state.details.isFeatured = event.isFeatured
			this.state.details.isPublished = event.isPublished
			this.state.details.isTicketsPictureProtected = event.isTicketsPictureProtected
			this.state.details.isTicketsPasswordProtected = event.isTicketsPasswordProtected
			this.state.details.eventDateTimeSlot.eventStartTime = new Date(event.eventDateTimeSlot.eventStartTime)
			this.state.details.eventDateTimeSlot.eventEndTime = new Date(event.eventDateTimeSlot.eventEndTime)
			this.state.details.eventEntryTime.entryStartTime = new Date(event.eventEntryTime.entryStartTime)
			this.state.details.eventEntryTime.entryEndTime = new Date(event.eventEntryTime.entryEndTime)
			this.state.assgiendSeats = event.totalAvailableTickets
			if (event.sections.length > 0) {
				event.sections.map(section => {
					if(!section[section.sectionLabel]) 
					this.state.uniqueSections[section.sectionLabel] = section.sectionId
				})
			}
			if(event.venue) {
				this.state.details.venueId = event.venue._id + " " + event.venue.seatingCapacity
				this.state.availableSeats = event.venue.seatingCapacity
				this.state.totalVenueSeats = event.venue.seatingCapacity
			}
			this.state.details.bannerImageKey = event.bannerImageKey
			this.state.details.eventImagesKeys = event.eventImagesKeys
			this.state.details.sections = event.sections
			this.state.eventType = event.parentEventInfo.eventType
			this.state.parentEventId = event.parentEventId
			this.state.title = event.eventTitle
			this.state.agenda = event.agenda || ""
			this.state.eventSlotId = event.eventSlotId
			this.state.ticketClasses = event.ticketClasses
			if(event.parentEventInfo.ticketClassesConfig.length > 0) {
				console.log('event.parentEventInfo.ticketClassesConfig.length')
				const {parentEventInfo: {ticketClassesConfig}} = event
				event.parentEventInfo.ticketClassesConfig.map((ticket, index) => {
					//if(ticket !== 'undefined' && typeof event.ticketClasses[index] !== 'undefined' && ticket._id === event.ticketClasses[index].ticketClassId) {
						
						this.state.ticketClassesCount[ticket._id] = parseInt(event.ticketClasses[index] ? event.ticketClasses[index].availableTickets : 0) || 0
						
						return (
							this.state.details.ticketClassesConfig.push({
								ticketClassName:  ticket.ticketClassName,
								ticketClassId:  ticket._id,
								ticketClassPrice: event.ticketClasses[index] ? event.ticketClasses[index].ticketClassPrice : 0,
								ticketClassColor: ticket.ticketClassColor,
								availableTickets: event.ticketClasses[index] ? event.ticketClasses[index].availableTickets : 0,
								_id: event.ticketClasses[index] ? event.ticketClasses[index]._id : null
							})
						)
				//	} 
					
					
				})
			}
			if (event.parentEventInfo.ticketClassesConfig.length > 0 && StandardEvent.ticketClasses.length ) {
				console.log('event.parentEventInfo.ticketClassesConfig.length  and Standard')
				StandardEvent.ticketClasses.map((ticket, index) => {
					if(typeof this.state.details.ticketClassesConfig[index] === 'undefined') {
						this.state.ticketClassesCount[ticket._id]  = 0
						return (
							this.state.details.ticketClassesConfig.push({
								ticketClassName:  ticket.ticketClassName,
								ticketClassId:  ticket._id,
								ticketClassPrice: 0,
								ticketClassColor: ticket.ticketClassColor,
								availableTickets:  0,
								_id: event.ticketClasses[index] ? event.ticketClasses[index]._id : null
							})
						)
					}
				})
			}
			let seats = [[]]
			if(typeof event.seats !== 'undefined') {
				seats = event.seats.seats[0].seats
				this.state.reservedSeats = seats
			}
			
			this.setState(this.state)
		}
		if (StandardEvent.ticketClasses.length > 0 && !EventSlot.data) {

			StandardEvent.ticketClasses.map(ticket => {
				_state.ticketClassesCount[ticket._id] = parseInt(ticket.availableTickets) || 0
				return (_tickets.push({
					ticketClassName: ticket.ticketClassName,
					ticketClassId: ticket._id,
					ticketClassPrice: 0,
					ticketClassColor: ticket.ticketClassColor,
				}))
			})
			this.state.details.ticketClassesConfig = _tickets
		}
		if (typeof this.props.ticketClassesConfig !== 'undefined' && this.props.ticketClassesConfig.length > 0 ) {
			let _tickets = []
			this.props.ticketClassesConfig.map((ticket, index) => {
				this.state.ticketClassesCount[ticket._id] =  0
				return (_tickets.push({
					ticketClassName: ticket.ticketClassName,
					ticketClassId: ticket._id,
					ticketClassPrice: 0,
					availableTickets: 0,
					ticketClassColor: ticket.ticketClassColor,
				}))
			})
			this.state.details.ticketClassesConfig = _tickets
			this.state.eventType = this.props.eventType
		}
		if (this.props.parentEventId && this.props.newSlot) this.state.parentEventId = this.props.parentEventId

		this.setState(this.state);	
			
	}
	sumSeats = (_state) => {
		let sum = 0
		Object.keys(_state.ticketClassesCount).map(ticket => {
			sum = _state.ticketClassesCount[ticket] + sum
		})
		_state.assgiendSeats = sum
		this.setState(_state)

	}
	reserveSeat = (seatNumber) => {
		const _state = {...this.state}
		const { selectedClass } = _state
		if(selectedClass.ticketClassColor === "") {
			return swal("Error", "Select the ticket class first", "error");
		}
		if (_state.reservedSeats[0][seatNumber].ticketClassId === selectedClass.ticketClassId) {
			_state.ticketClassesCount[selectedClass.ticketClassId] < 1 ? _state.ticketClassesCount[selectedClass.ticketClassId]  = 0 : 
			_state.ticketClassesCount[selectedClass.ticketClassId] = _state.ticketClassesCount[selectedClass.ticketClassId] - 1
			_state.reservedSeats[0][seatNumber] = {
				purchased: false,
				seatNumber: seatNumber,
				ticketClassColor: null,
				ticketClassId: null,
				ticketClassName: null
			}
			return this.sumSeats(_state)
			
		}
		if (_state.reservedSeats[0][seatNumber].ticketClassId !== null && _state.reservedSeats[0][seatNumber].ticketClassId !== selectedClass.ticketClassId) {
			let previousSeat = _state.reservedSeats[0][seatNumber].ticketClassId
			_state.ticketClassesCount[previousSeat]  > 0 ? _state.ticketClassesCount[previousSeat] = _state.ticketClassesCount[previousSeat] -1 :
			_state.ticketClassesCount[previousSeat] = 0
			_state.ticketClassesCount[selectedClass.ticketClassId] = _state.ticketClassesCount[selectedClass.ticketClassId] + 1
			_state.reservedSeats[0][seatNumber] = {
				purchased: false,
				seatNumber: seatNumber,
				ticketClassColor: selectedClass.ticketClassColor,
				ticketClassId: selectedClass.ticketClassId,
				ticketClassName: selectedClass.ticketClassName
			}
			return this.sumSeats(_state)
			
		} 
		if(_state.reservedSeats[0][seatNumber].ticketClassId === null) {
			_state.ticketClassesCount[selectedClass.ticketClassId] = _state.ticketClassesCount[selectedClass.ticketClassId] + 1
			_state.reservedSeats[0][seatNumber] = {
				purchased: false,
				seatNumber: seatNumber,
				ticketClassColor: selectedClass.ticketClassColor,
				ticketClassId: selectedClass.ticketClassId,
				ticketClassName: selectedClass.ticketClassName
			}
			return this.sumSeats(_state)
		}
		
	}

	setAvailableSeats = (e) => {
		if (parseInt(e.target.value) >parseInt( this.state.totalVenueSeats)){ 
			swal("Error", "You can allocate seats more then the available venue seats", "error")
			return
		}
		this.setState({availableSeats: e.target.value, reservedSeats: [[]]})
	}
	handleImageDrop  = (file) => {
		let preview = file[0].preview
		this.setState({
			bannerFile: preview
		}, function () {
		})
		let formData = new FormData();
		formData.append('image', file[0]);
		this.props.uploadImage('banner', formData, this.props.id)
	}
	handleFeatureDropDown = ({target}) => {
		const name = target.name
		const value = target.value
		let _state = {...this.state}
		_state.details[name] = JSON.parse(value)
		this.setState(_state)
	}

	handleMultiDrop = (file) => {
		let _files = this.state.galleryFiles.concat(file[0].preview);
		this.setState({galleryFiles: _files}, function(){
		})
		let formData = new FormData();
		formData.append('image', file[0]);
		this.props.uploadImage('gallery', formData, this.props.id)
	}
	setCurrentClass = (selectedClass) => {
		this.setState({selectedClass})
	}
	handleTicketChange = idx => evt => {
		const { details } = this.state
        const newTicketClasses = details.ticketClassesConfig.map((ticket, sidx) => {
            if (idx !== sidx) return ticket;
            return { ...ticket, [evt.target.name]: evt.target.value };
        });

		let _state = {...this.state}
		_state.details.ticketClassesConfig = newTicketClasses
        this.setState(_state, function () {
        });
	};
	
	saveEventSlot = () => {
		let flag = true
		const {StandardEvent, EventSlot} = this.props;
		const { details } = this.state
		let _state = {...this.state}
		if (details.eventDateTimeSlot.eventStartTime === "")	{
			flag = false
			_state.errors.eventStartTime = true
			_state.errors.eventStartTimeMsg = "Event Start time is required"
		}
		if (details.eventDateTimeSlot.eventEndTime === "") {
			flag = false
			_state.errors.eventEndTime = true
			_state.errors.eventEndTimeMsg = "Event End time is required"
		}
		if(details.eventEntryTime.entryStartTime === "") {
			flag = false
			_state.errors.entryStartTime = true
			_state.errors.entryStartTimeMsg = "Event Entry time is required"
		}
		if(details.eventEntryTime.entryEndTime === "") {
			flag = false
			_state.errors.entryEndTime = true
			_state.errors.entryEndTimeMsg = "Event Entry Close time is required"
		}
		if(details.eventDateTimeSlot.eventStartTime != "" && details.eventDateTimeSlot.eventEndTime != "" &&
				details.eventDateTimeSlot.eventEndTime < details.eventDateTimeSlot.eventStartTime ) {
			flag = false
			_state.errors.eventEndTime = true
			const now = moment(details.eventDateTimeSlot.eventEndTime);
			const duration = moment.duration(now.diff(details.eventDateTimeSlot.eventStartTime));
			_state.errors.eventEndTimeMsg = "Event end time should be greater than start time"
			if (duration.asHours() <1) _state.errors.eventEndTimeMsg = "End Date cannot be less than Start Date"
			
		}
		if(details.eventDateTimeSlot.eventEndTime > details.eventDateTimeSlot.eventStartTime) {
			var diff = Math.abs(new Date(details.eventDateTimeSlot.eventEndTime) - new Date(details.eventDateTimeSlot.eventStartTime));
			var minutes = Math.floor((diff/1000)/60);
			if (minutes < 60) {
				flag = false
				_state.errors.eventEndTime = true
				_state.errors.eventEndTimeMsg = "Even Slot can be minimum of 1 hour"
			}


		}
		if(details.eventEntryTime.entryStartTime != "" && details.eventDateTimeSlot.eventStartTime != "" &&
			details.eventEntryTime.entryStartTime >= details.eventDateTimeSlot.eventStartTime) {
			flag = false
			_state.errors.entryStartTime = true
			_state.errors.entryStartTimeMsg = 'Event Entry time cannot be greater than start time'
		}

		if(details.eventEntryTime.entryEndTime > details.eventDateTimeSlot.eventEndTime) {
			flag = false
			_state.errors.entryEndTime = true
			_state.errors.entryEndTimeMsg = 'Event Entry time cannot be greater than start time'
		}
		if(this.props.eventType !== "STANDARD" || this.state.eventType !== 'STANDARD') {
			if(this.state.title === "") {
				flag = false
				_state.errors.title = true
				_state.errors.titleMsg = "Title is required field"
			}
			if (this.state.agenda === "") {
				flag = false
				_state.errors.agenda = true
				_state.errors.agendaMsg = "Agenda is required"
			}
		}
		if (!details.bannerImageKey.imageUrl) {
			flag = false
			_state.errors.bannerImageKey = true
			_state.errors.bannerImageKeyMsg = "Banner Image is required"
		}
		if(!flag) {
			this.setState(_state)
			return swal("Error", "Required information is missing", "error")
		}
		if (StandardEvent.eventId === "" && (!this.props.eventSlotDetail) && this.state.parentEventId === "") {
			return swal('Error', 'Create Event First', 'error') 
		}
		
		if (details.ticketClassesConfig.length > 0) {
			Object.keys(_state.ticketClassesCount).map((key , index) => {
				// if (this.props.newSlot) {
				// 	if(typeof this.props.ticketClasses[index] !== 'undefined' && key === this.props.ticketClasses[index].ticketClassId) {
				// 		details.ticketClassesConfig[index].availableTickets = _state.ticketClassesCount[key]	
				// 	}
				// }
				if (StandardEvent.ticketClasses.length > 0 && key === StandardEvent.ticketClasses[index].ticketClassId && !this.props.EventSlot.data){
					details.ticketClassesConfig[index].availableTickets = _state.ticketClassesCount[key]
				}
				if (details.ticketClassesConfig.length > 0 && key === details.ticketClassesConfig[index].ticketClassId){
					details.ticketClassesConfig[index].availableTickets = _state.ticketClassesCount[key]
				}
			})
		}
		let _eventImagesKeys = [] 
		if(details.eventImagesKeys.length > 0) {
			_eventImagesKeys = details.eventImagesKeys.filter(image => {
				return image != null;
			})
		}
		let obj = {
			"eventDateTimeSlot" : details.eventDateTimeSlot,
			"eventEntryTime" : details.eventEntryTime,
			"venueId" : details.venueId.split(" ")[0],
			"eventImagesKeys" : _eventImagesKeys,
			"bannerImageKey" : details.bannerImageKey,
			"sections" : details.speakers,
			"isOnlineSaleOpen" : 1,
			"isPublished" : this.state.details.isPublished,
			"totalAvailableTickets": _state.assgiendSeats,
			"isFeatured" : this.state.details.isFeatured,
			"slotSaved":false,
			"sections": this.state.details.sections,
			"isTicketsPasswordProtected" : this.state.details.isTicketsPasswordProtected,
			"ticketClasses" : details.ticketClassesConfig,
			"slotSeats" : {
				"seats": [{
					"sectionId": "abc",
					"seats": this.state.reservedSeats
				}]
			}
		}
		if (EventSlot.data && EventSlot.data.parentEventId != null) {
			obj.parentEventId = this.props.EventSlot.data.parentEventId
			obj.eventSlotId = this.props.EventSlot.data.eventSlotId
		} 
		if(this.props.parentEventId) {
			obj.parentEventId = this.state.parentEventId
		}
		if(StandardEvent.eventId !== "") {
			obj.parentEventId = StandardEvent.eventId
		}
		if(this.props.eventType !== "STANDARD" || this.state.eventType !== 'STANDARD') {
			obj.eventTitle = this.state.title
			obj.agenda = this.state.agenda
		}
		if(flag) {
			this.props.saveEventSlot(obj, this.props.id)
		}
			
	}

	componentWillReceiveProps(nextProps) {
		const { Images, EventSlot, Venues } = nextProps;
		let _state = {...this.state}
		if (this.props.EventSlot !== nextProps.EventSlot && nextProps.EventSlot.slotId === this.props.id) {
			if(nextProps.EventSlot.error && nextProps.EventSlot.slotId === this.props.id) {
				this.props.resetEventSlotState()
				if(this.props.newSlot)this.props.toggle(true)
				if(Array.isArray(nextProps.EventSlot.errors)) {
					nextProps.EventSlot.errors.map(error => {
						NotificationManager.error(error, "Error")
					})
				} else {
					NotificationManager.error(nextProps.EventSlot.errors, "Error")
				}
					
				
			}
			
			if(nextProps.EventSlot.saved && nextProps.EventSlot.slotId === this.props.id) {
				if(this.props.newSlot)this.props.toggle(true)
				this.props.resetEventSlotState()
				this.setState({slotSaved: true, errors: {}}, function() {
					NotificationManager.success("Event Slot saved successfully", "Sucess", 3000)
					this.props.resetEventSlotState()
				})
				
			}
		}
		
		if (Venues !== this.props.Venues && Venues.data.length > 0) {
			if(_state.details.venueId === '') {
				_state.details.venueId = Venues.data[0]._id + " " + Venues.data[0].seatingCapacity
			_state.availableSeats = Venues.data[0].seatingCapacity
			_state.totalVenueSeats = Venues.data[0].seatingCapacity
			}
			
			_state.venues = Venues.data
			this.setState(_state, function(){
			})
		}
		if(Images.bannerImage != "" && Images.bannerImage !== this.props.Images.bannerImage && nextProps.Images.slotId === this.props.id) {
			_state.details.bannerImageKey = Images.bannerImage
			this.setState(_state, function(){
			})
		}
		if (Images.galleryImages != "" && Images.galleryImages !== this.props.Images.galleryImages && nextProps.Images.slotId === this.props.id) {
			let _eventImagesKeys = [];
			_eventImagesKeys = _state.details.eventImagesKeys.concat(Images.galleryImages)
			_state.details.eventImagesKeys = _eventImagesKeys
			this.setState(_state, function(){
				//this.props.resetImageState()
			})
		}
		

	}

	saveSpeakers = (speakers) => {
		let _state = {...this.state}
		if(speakers.length > 0) {
			speakers.map(speaker => {
				speaker.sectionLabel = (speaker.sectionLabel).toLowerCase().trim()
				if(!_state.uniqueSections[speaker.sectionLabel]) {
					_state.uniqueSections[speaker.sectionLabel]  = uuid();
				}
				speaker.sectionId = _state.uniqueSections[speaker.sectionLabel]
			})
			
		}
		
		_state.details.sections = _state.details.sections.concat(speakers)
		_state.showSpeakersSection = false
		this.setState(_state)
	}

	handleInputChange = (event) => {
		const {target} = event;
		this.setState({
			[target.name]: target.value,
			errors: {...this.errors, [target.name]: false}
		}, () => {
			if(target.name === 'title' && !this.props.newSlot && typeof this.props.newSlot !== 'undefined') 
				this.props.setSlotName(this.props.id, target.value)
		})

	}
	removeSpeaker = (id) => {
		let _state = {...this.state.details}
		let _speakers = _state.sections.filter((speaker, index)  => {
			return index != id
		})
		_state.sections  = _speakers
		this.setState({details: _state})
	}
	showSpeakers = () => {
		return (
			<Table>
				<thead>
					<tr>
						<th>
							Title
						</th>
						<th>Name</th>
						<th>Description</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{
						this.state.details.sections.map((section, index) => {
							return (
								<tr>
								<td>
									{section.celebrityTitle}
								</td>
								<td>
									{section.celebrityName}
								</td>
								<td>
									<div>
									{section.celebrityDescription ? _.truncate(section.celebrityDescription): null}
									</div>
								</td>
								<td>
									<img src={section.celebrityImageKey.imageUrl} style = {styles.speakerImage} />
								</td>
								<td>
									<Button onClick={() => this.removeSpeaker(index)} disabled = {this.state.slotSaved}>
									<i className="fas fa-trash-alt"></i>
									</Button>
								</td>
							</tr>
							)
							
						})
					}
				</tbody>
			</Table>
		)
	}
	showSlotTitle = () => {
		const {errors} = this.state
		const slotsTitleJSX = 
		<Row>
			<Col>
				<FormGroup style = {{maxWidth: '50%'}}>
					<Label>Event Title  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
					<Input type ="text" name = "title" onChange = {this.handleInputChange} value = {this.state.title} disabled = {this.state.slotSaved}/>
					<FormText>{errors.title  ? <p style = {styles.error}>{errors.titleMsg}</p>: null}</FormText>
				</FormGroup>
				<FormGroup style = {{maxWidth: '50%'}}>
					<Label>Event Agenda  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
					<Input type ="text" name = "agenda" onChange = {this.handleInputChange} value = {this.state.agenda} disabled = {this.state.slotSaved}/>
					<FormText>{errors.agenda  ? <p style = {styles.error}>{errors.agendaMsg}</p>: null}</FormText>
				</FormGroup>
			</Col>
		</Row>
		return slotsTitleJSX
	}

	changeImage = (imageType, index) => {
		let _state = {...this.state}
		if(imageType === "banner") {
			_state.details.bannerImageKey = {}
		} else  delete _state.details.eventImagesKeys[index]
		this.setState(_state)

	}

	isEventSaved = () => {
		if(this.props.EventSlot.saved && this.props.EventSlot.slotId === this.props.id) return true
		return false
	}

	confirm = () => {
		let _state = {...this.state}
		_state.details.isPublished = true
		this.setState(_state, () => {this.saveEventSlot()})
	}
	cancel = () => {
		let _state = {...this.state}
		_state.details.isPublished = false
		this.setState(_state, () => {this.saveEventSlot()})
	}

	render() {
		const { Images, EventSlot,Venues } = this.props
		return (
			<React.Fragment>
				{Venues.loading || Images.loading || EventSlot.loading ? <Loader/> : null}
				{this.showVenueModal()}
				<Row className = "mt-3">
					<Col md={{size:8, offset: 2}}>
					{this.props.eventType !== 'STANDARD' || this.state.eventType !== 'STANDARD' ? this.showSlotTitle(): null}
					<Row className = "mt-3">
						<Col md = "6">
						<div className="form-group content form-block-holder">
									<label className="control-label">
										<h4>Is this a Featured Event?</h4>
									</label>
									<div>
										<select
										className='form-control'
											autoComplete="off"
											required
											disabled = {this.props.StandardEvent.eventSaved}
											onChange = {this.handleFeatureDropDown}
											name = "isFeatured"
											disabled = {this.state.slotSaved}
											value = {this.state.details.isFeatured}
											defaultValue={this.state.details.isFeatured}
											onBlur={this.validationCheck}>
											<option value= {true}>Yes</option>
											<option value= {false}>No</option>
										</select>
									</div>
								</div>
						</Col>
					</Row>
					<Row>
						<Col md = "6">
						<div className="form-group content form-block-holder">
									<label className="control-label">
										<h4>Are Tickets Password Protected?</h4>
									</label>
									<div>
										<select
										className='form-control'
											autoComplete="off"
											required
											disabled = {this.props.StandardEvent.eventSaved}
											onChange = {this.handleFeatureDropDown}
											name = "isTicketsPasswordProtected"
											disabled = {this.state.slotSaved}
											value = {this.state.details.isTicketsPasswordProtected}
											defaultValue={this.state.details.isTicketsPasswordProtected}
											onBlur={this.validationCheck}>
											<option value= {true}>Yes</option>
											<option value= {false}>No</option>
										</select>
									</div>
								</div>
						</Col>
					</Row>
					<Row>
						<Col md = "6">
						<div className="form-group content form-block-holder">
									<label className="control-label">
										<h4>Are Tickets Picture Protected?</h4>
									</label>
									<div>
										<select
										className='form-control'
											autoComplete="off"
											required
											disabled = {this.props.StandardEvent.eventSaved}
											onChange = {this.handleFeatureDropDown}
											name = "isTicketsPictureProtected"
											disabled = {this.state.slotSaved}
											value = {this.state.details.isTicketsPictureProtected}
											defaultValue={this.state.details.isTicketsPictureProtected}
											onBlur={this.validationCheck}>
											<option value= {true}>Yes</option>
											<option value= {false}>No</option>
										</select>
									</div>
								</div>
						</Col>
					</Row>
					<Row>
						<Col className = "mt-2">
							<EventSchedule startTime = {this.handleEventStartTime} endTime = {this.handleEventEndTime}
								eventEntry = {this.handleEventEntryStartTime}
								startEntryTimeErr = {this.state.startEntryTimeErr}
								errors = {this.state.errors}
								isSaved = {this.state.slotSaved}
								eventEntryTime= {this.state.details.eventEntryTime}
								eventDateTime = {this.state.details.eventDateTimeSlot}
								eventEntryClose = {this.handleEventEventEntryEndTime}
							/>
						</Col>
					</Row>
					<Row>
						<Col md = "6" className = "mt-3">
							<Venue venues = {this.state.venues} handleVenueChange = {this.handleVenueChange}
								venueId = {this.state.details.venueId}
								totalSeating = {this.state.totalVenueSeats} enableAvailableTickets = {false} 
								availableSeats = {this.state.availableSeats} 
								availableSeatsCallback = {this.setAvailableSeats}
								isSaved = {this.state.slotSaved}
							/>
						</Col>
						<Col md = "2" style = {{marginTop: 62}}>
							<Button onClick = {this.showVenue} disabled = {this.state.slotSaved}>Add Venue</Button>
						</Col>
					</Row>
					<Row className = "mt-3">
						<Col md = "4" >
							<h3>Add Selected seats to class</h3>
						</Col>
						<Col md = "8">
							{
								
								this.state.details.ticketClassesConfig ? this.state.details.ticketClassesConfig.map((ticket, index) => {
									return (
										<ButtonGroup key = {index}>
											<Button
												onClick={() => this.setCurrentClass(ticket)} 
												disabled = {this.state.slotSaved}
												active={this.state.selectedClass === ticket.ticketClassName}
												style={{ backgroundColor: ticket.ticketClassColor, marginRight: 5, marginBottom:5, }}>
												{ticket.ticketClassName}
											</Button>
										</ButtonGroup>
										
										
									)
								}): <p>Create ticket classes first</p>
							}
						</Col>
					</Row>
					<Row>
						<Col>
							<h4>Assigned {this.state.assgiendSeats}/{this.state.availableSeats}</h4>
						</Col>
					</Row> 
					<Row>
						<Col md = "8" className = "mt-3" style = {styles.seatsScroll}>
						<Seats totalSeats = {this.state.availableSeats} handleClick = {this.reserveSeat} isSaved = {this.state.slotSaved}
							reservedSeats = {this.state.reservedSeats} activeClass = {this.state.selectedClass} callback = {this.callback}/>
						</Col>
					</Row>
					<Row>
						<Col md = "8" className = "mt-3">
						<Row>
                    <Col md = "3">
                     Class Name
                    </Col>
                    <Col md = "3">
											Available Tickets
                    </Col>
                    <Col md = "3">
                     Ticket Price
                    </Col>
                    </Row>
						<ListTicketClasses isSaved = {this.state.slotSaved} ticketClasses = {this.state.details.ticketClassesConfig} ticketClassesCount = {this.state.ticketClassesCount} handleTicketChange = {this.handleTicketChange}/>
						</Col>
					</Row>
					
						<Row className = "mt-3">
							<Col style = {{border: '1px solid',marginTop: 15, marginLeft: 10}} md ="8"></Col>
						</Row>

						<Row className = "mt-3">
						<Row>
								<Col>
									<Col><h3>
										Banner Image  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span>
										</h3>
									</Col>
									<Col md = "12">
									 {this.state.details.bannerImageKey.imageUrl? 
									 	<div style = {{float: 'left'}}>
											 	<img src = {this.state.details.bannerImageKey.imageUrl} 
										style = {{ float: 'left', 
										height:  "150px", width : "150px"}} />
										<Button style = {styles.editButton} disabled= {this.state.slotSaved} onClick ={() => this.changeImage("banner", 0)}><i className="fa fa-trash"></i></Button>
										 </div>
									 	: <DropImage  multi={false}  isSaved = {this.props.EventSlot.saved}
									 handleImageDrop={this.handleImageDrop} name ="banner" />}

									</Col>
									<Col md = "12" style={{textAlign:'center', width : "182px", color: "#d0cece"}}>1024 x 768</Col>
								</Col>
								
									{/* <Col>{this.state.details.eventImagesKeys.length!==0? 
											this.state.details.eventImagesKeys.map((image, index) => {
												return (
												<div style = {{float: 'left'}}>
														<img src = {image.imageUrl} 
														style = {{float: 'left', height:  "150px", width : "150px"}}
															key = {index}/>
												<Button className="delete-btn" onClick ={() => this.changeImage("gallery", index)} disabled= {this.props.EventSlot.saved} ><i className="fa fa-trash"></i></Button>
												</div>
												)
												})
												: 
												<DropImage  multi={false}  isSaved = {this.props.EventSlot.saved}
														handleImageDrop={this.handleMultiDrop} name ="gallery" />
												}
									</Col> */}
							</Row>
							<Col className = "col-md-12">
								{this.state.errors.bannerImageKey ? <p style = {styles.error}>
										 {this.state.errors.bannerImageKeyMsg}</p>: null}
								</Col>
							<Row>
								
							</Row>

						</Row>
						<Row className = "mt-3">
							<Row>
								<Col>
									<Col><h3>
										Gallery Images (optional)
										</h3>
									</Col>
									<Col md = "12">
									 {this.state.details.eventImagesKeys.length >0 ? this.state.details.eventImagesKeys.map((image, index) => {
										 return (
											<div style = {{float: 'left'}}>
												 <img src = {image ? image.imageUrl: null} 
										style = {{marginLeft: '10px', marginRight: '10px', float: 'left', 
										height:  "150px", width : "150px"}}
										  key = {index}/>
										  <Button className="delete-btn" onClick ={() => this.changeImage("gallery", index)} disabled= {this.state.slotSaved} ><i className="fa fa-trash"></i></Button>
										</div>
										 )
									 }): null}
									 <DropImage  multi={false}  isSaved = {this.state.slotSaved}
									 handleImageDrop={this.handleMultiDrop} name ="gallery" />
									</Col>

								</Col>
							</Row>

						</Row>
						<Row className = "mt-3 mx-1">
							<Col className='gradient-border-div' md ="8"></Col>
						</Row>
						<Row className = "mt-3">
							<Col md = "6">

								<Button onClick = {this.showSpeakerSection} disabled = {this.state.slotSaved}>
									Add Custom Section
								</Button>
							</Col>
						</Row>
						<Row className = "mt-3 mx-1">
							<Col className='gradient-border-div' md ="8"></Col>
						</Row>
						<Row className = "mt-3">
							<Col md = "6">
							{this.state.details.sections.length > 0 ? this.showSpeakers(): null }
							</Col>
						
							

						</Row>
						<Row className = "mt-3">
							<Col md = "6">
							{typeof this.props.newSlot !== 'undefined' ?<Button 
								disabled = {this.state.isSaved ? true: false} onClick = {this.saveEventSlot}>Save</Button> :
								<Popconfirm
								title="Do  you want to save and publish event ?"
								onConfirm={this.confirm}
								onCancel={this.cancel}
								okText="Yes Save & Publish"
								cancelText="Only Save Event"
							>
								<Button 
								disabled = {this.state.slotSaved}>Save</Button>
							</Popconfirm>}
								
							</Col>
						</Row>
						{this.state.showSpeakersSection ? 
							<EventSpeakers toggle = {this.toggle} saveSpeakers = {this.saveSpeakers}
								isSaved = {this.state.slotSaved}
								isOpen  = {this.state.showSpeakersSection}/>: null}
								</Col>
				</Row>
			</React.Fragment>
		)
	}
}

const styles = {
    margin: {
        marginTop: 10,
        marginBottom: 10
	},
	error: {
		color: 'red',
		fontWeight: 'bold',
		marginTop: 2
	},
	seatsScroll: {
		overflowY: 'scroll',
		maxHeight: '250px'
	},
	speakerImage: {
		height: 50,
		width: 50
	},
	editButton: {
		zIndex: 5,
		left: '127px',
		position: 'absolute'
	}
}
const mapStateToProps = (state) => {
	return state
}

export default connect(mapStateToProps, {resetEventSlotState, resetImageState, getAllVenues, uploadImage, saveEventSlot})(Details)
