import React from 'react';
import TicketClasses from '../../../components/ticketClasses'

const EventTicketClasses = ({showNext}) => {
	return (
		<TicketClasses showAddButton = {true} disabled = {false} showNext = {showNext}/>
	)
}

export default EventTicketClasses