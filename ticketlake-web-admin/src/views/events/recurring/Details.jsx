import React, { Component } from 'react';
import { Row, Col, Input,Modal,ModalBody,ModalFooter, Table, FormGroup,
	Label, FormText, Button,ButtonGroup } from 'reactstrap';
import swal from 'sweetalert';
import Seats from '../../../components/seats'
import Venue from '../../../components/venues'
import EventSpeakers from '../../../components/eventSpeakers'
import { getAllVenues } from '../../../redux/actions/venues'
import { saveRecurringEvent, resetSlot } from '../../../redux/actions/recurringEvent'
import { uploadImage,resetImageState } from '../../../redux/actions/images'
import ListTicketClasses from '../../../components/listTicketClasses'
import { connect } from 'react-redux'
import DropImage from '../../../components/imageUpload'
import moment from 'moment'
import Loader from '../../../components/loader'
import Datetime from "react-datetime";
import 'react-datetime/css/react-datetime.css';
import {NotificationManager} from "react-notifications";
import uuid from "uuid";
import _ from 'lodash'
import { Popconfirm, message } from 'antd';
import VenueComponent from '../../Venues/VenueForm'
const yesterday = Datetime.moment().subtract(1, 'day');
class Details extends Component {
	constructor(props) {
		super(props);
		this.state = {
			details: {
				eventImagesKeys: [],
				bannerImageKey: '',
				venueId: '',
				isOnlineSaleOpen : 0,
				isPublished : 0,
				isTicketsPasswordProtected: 0,
				ticketClassesConfig: this.props.ticketClassesConfig || [],
				sections: []
			},
			isFeatured: true,
			title: '',
			agenda: '',
			skipDay: [{"skipDay": ""}],
            startFrom: "",
            endDate: "",
            timeSlot: [{"startTime": "", "endTime": ""}],
            repeatType: 3,
            entrySlot: [{"entryStart": "", "entryEnd": ""}],
			galleryFiles: [],
			bannerFile: null,
			uploadedImage: '',
			ticketClassesCount: [],
			modal: true,
			show: false,
			showSpeakersSection: false,
			availableSeats: 1,
			totalVenueSeats: 0,
			selectedClass: {
				ticketClassName: '',
				ticketClassColor: '',
				ticketClassPrice: '',
				ticketClassId: '',
				_id: ''
			},
			slotSaved:false,
			reservedSeats: [[]],
			ticketClassesCount: [],
			errors :{
				monthDays:[{monthDays: false, monthDaysMsg: ""}],
				title: false, agenda: false,
				skipDay: [{skipDayMsg: "", skipDay: false}],
                timeSlot: [{startTimeMsg: "", startTime: false, endTime: false, endTimeMsg: ""}],
                entrySlot: [{entryStartMsg: "", entryStart: false, entryEnd: false, entryEndMsg: ""}]
			},
			venues: [],
			uniqueSections: [],
			parentEventId: null,
			eventSlotId: null,
			assgiendSeats: 0,
			availableEventSeats: [[]],
			Venuemodal: false,
			monthDays: [{monthDays: moment(new Date()).format("YYYY-MM-DD")}],
			days: [{
				value: 0,
				label: 'Sunday',
				isChecked: false
			},{
				value: 1,
				label: 'Monday',
				isChecked: false
			},{
				value: 2,
				label: 'Tuesday',
				isChecked: false
			},{
				value: 3,
				label: 'Wednesday',
				isChecked: false
			},{
				value: 4,
				label: 'Thursday',
				isChecked: false
			},{
				value: 5,
				label: 'Friday',
				isChecked: false
			},{
				value: 6,
				label: 'Saturday',
				isChecked: false
			}],
			isSaved: false
		};
		this.saveEventSlot = this.saveEventSlot.bind(this)
	}
	toggle = () => {
		this.setState({
			showSpeakersSection: !this.state.showSpeakersSection
		})
	}
	setSeats = (seats) => {
		this.setState({seats})
	}
	loadSpeakers = () => {
		this.setState({
			show: !this.state.show
		}, function() {
		});
	}
	showSpeakerSection = ()=> {
		this.setState({
			showSpeakersSection: !this.state.showSpeakersSection
		})
	}
	
	handleVenueChange =(event) => {
		const venue = event.target.value.split(" ")
		let _state = {...this.state}
		_state.details.venueId = venue[0] + " " + venue[1]
		_state.availableSeats = venue[1]
		_state.totalVenueSeats = venue[1]
		_state.reservedSeats = [[]]
		_state.assgiendSeats = 0
		for (let key in _state.ticketClassesCount) {
			_state.ticketClassesCount[key] = 0
		}
		this.setState(_state)

	}
	showVenue = (flag) => {
		this.setState({
			Venuemodal: !this.state.Venuemodal
		}, function() {
			this.props.getAllVenues()
		})
	}
	showVenueModal = () => {
		return (<Modal isOpen={this.state.Venuemodal} toggle={this.showVenue} className={this.props.className} size="lg">
          <ModalBody>
			  	<VenueComponent redirect  = {false} closeModal = {this.showVenue}/>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.showVenue}>Cancel</Button>
          </ModalFooter>
        </Modal>)

	}
	
	componentDidMount() {
		window.scrollTo(0, 0)
		let _state = {...this.state}
		const { StandardEvent } = this.props
		this.props.getAllVenues()
		let _tickets = []
		if (StandardEvent.ticketClasses.length > 0) {
			StandardEvent.ticketClasses.map(ticket => {
				_state.ticketClassesCount[ticket._id] = 0
				return (_tickets.push({
					ticketClassName: ticket.ticketClassName,
					ticketClassId: ticket._id,
					ticketClassPrice: 0,
					ticketClassColor: ticket.ticketClassColor,
					availableTickets: 5
				}))
			})
			_state.details.ticketClassesConfig = _tickets
		}

		if (typeof this.props.ticketClassesConfig !== 'undefined' && this.props.ticketClassesConfig.length > 0 ) {
			this.props.ticketClassesConfig.map((ticket, index) => {
				_state.ticketClassesCount[ticket._id] =  0
				return (_tickets.push({
					ticketClassName: ticket.ticketClassName,
					ticketClassId: ticket._id,
					ticketClassPrice: 0,
					availableTickets: 0,
					ticketClassColor: ticket.ticketClassColor,
				}))
			})
			_state.details.ticketClassesConfig = _tickets
			_state.eventType = this.props.eventType
			_state.parentEventId = this.props.parentEventId
		}
	
		this.setState(_state);	
			
	}
	sumSeats = (_state) => {
		let sum = 0;
		Object.keys(_state.ticketClassesCount).map(ticket => {
			sum = _state.ticketClassesCount[ticket] + sum
		})
		_state.assgiendSeats = sum
		this.setState(_state)

	}
	reserveSeat = (seatNumber) => {
		const _state = {...this.state}
		const { selectedClass } = _state
		if(selectedClass.ticketClassColor === "") {
			return swal("Error", "Select the ticket class first", "error");
		}
		if (_state.reservedSeats[0][seatNumber].ticketClassId === selectedClass.ticketClassId) {
			_state.ticketClassesCount[selectedClass.ticketClassId] < 0 ? _state.ticketClassesCount[selectedClass.ticketClassId]  = 0 : 
			_state.ticketClassesCount[selectedClass.ticketClassId] = _state.ticketClassesCount[selectedClass.ticketClassId] - 1
			_state.reservedSeats[0][seatNumber] = {
				purchased: false,
				seatNumber: seatNumber,
				ticketClassColor: null,
				ticketClassId: null,
				ticketClassName: null
			}
			return this.sumSeats(_state)
			
		}
		if (_state.reservedSeats[0][seatNumber].ticketClassId !== null && _state.reservedSeats[0][seatNumber].ticketClassId !== selectedClass.ticketClassId) {
			let previousSeat = _state.reservedSeats[0][seatNumber].ticketClassId
			_state.ticketClassesCount[previousSeat]  > 0 ? _state.ticketClassesCount[previousSeat] = _state.ticketClassesCount[previousSeat] -1 :
			_state.ticketClassesCount[previousSeat] = 0
			_state.ticketClassesCount[selectedClass.ticketClassId] = _state.ticketClassesCount[selectedClass.ticketClassId] + 1
			_state.reservedSeats[0][seatNumber] = {
				purchased: false,
				seatNumber: seatNumber,
				ticketClassColor: selectedClass.ticketClassColor,
				ticketClassId: selectedClass.ticketClassId,
				ticketClassName: selectedClass.ticketClassName
			}
			return this.sumSeats(_state)
			
		} 
		if(_state.reservedSeats[0][seatNumber].ticketClassId === null) {
			_state.ticketClassesCount[selectedClass.ticketClassId] = _state.ticketClassesCount[selectedClass.ticketClassId] + 1
			_state.reservedSeats[0][seatNumber] = {
				purchased: false,
				seatNumber: seatNumber,
				ticketClassColor: selectedClass.ticketClassColor,
				ticketClassId: selectedClass.ticketClassId,
				ticketClassName: selectedClass.ticketClassName
			}
			return this.sumSeats(_state)
		}
		
	}

	setAvailableSeats = (e) => {
		if (parseInt(e.target.value) >parseInt( this.state.totalVenueSeats)){ 
			swal("Error", "You can allocate seats more then the available venue seats", "error")
			return
		}
		this.setState({availableSeats: e.target.value, reservedSeats: [[]]})
	}
	handleImageDrop  = (file) => {
		let preview = file[0].preview
		this.setState({
			bannerFile: preview
		}, function () {
		})
		let formData = new FormData();
		formData.append('image', file[0]);
		this.props.uploadImage('banner', formData)
	}

	handleMultiDrop = (file) => {
		let _files = this.state.galleryFiles.concat(file[0].preview);
		this.setState({galleryFiles: _files}, function(){
		})
		let formData = new FormData();
		formData.append('image', file[0]);
		this.props.uploadImage('gallery', formData)
	}
	setCurrentClass = (selectedClass) => {
		this.setState({selectedClass})
	}
	handleTicketChange = idx => evt => {
		const { details } = this.state
        const newTicketClasses = details.ticketClassesConfig.map((ticket, sidx) => {
            if (idx !== sidx) return ticket;
            return { ...ticket, [evt.target.name]: evt.target.value };
        });

		let _state = {...this.state}
		_state.details.ticketClassesConfig = newTicketClasses
        this.setState(_state, function () {
        });
	};

	handleSpeakerChange = idx => evt => {
        const newSpeaker = this.state.details.speakers.map((speaker, sidx) => {
            if (idx !== sidx) return speaker;
            return { ...speaker, [evt.target.name]: evt.target.value };
		});
		let _state = {...this.state}
		_state.details.speakers = newSpeaker

        this.setState(_state);
	};
	handleTimeChange = (event,index,label, varName) => {
		let dateTime = event._d
		if(typeof dateTime === 'undefined' && label === 'monthDays') 
			dateTime = moment(new Date()).format("YYYY-MM-DD")
		let _state = {...this.state}
        const _mobile = this.state[varName].map((mobile, sidx) => {
            if (index !== sidx) return mobile;
            return { ...mobile, [label]: dateTime};
		});
		if(typeof _state.errors[varName][index] !== 'undefined')
			_state.errors[varName][index][label] = false
		_state[varName] = _mobile
		this.setState(_state);
    };
    handleInputChange = (event) => {
		const {target} = event;
		const _state = {...this.state}
		_state.errors[target.name] = false
		_state[target.name] = target.value
        this.setState(_state)
    }
    handleEventStartDate = ({_d}, label) => {
		const _state = {...this.state}
		_state[label] = _d
		_state.errors[label] = false
        this.setState(_state)
    }
    removeSlot = (event, index, varName) => {
        this.setState({
            [varName]: this.state[varName].filter((s, sidx) => {
                return sidx != index
            })
        });
    };

    addSlot = (varName, newValues) => {
		if(varName === 'monthDays') {
			let _state = {...this.state}
			_state.monthDays.push({monthDays: moment(new Date()).format("YYYY-MM-DD")})
			return this.setState(_state)
		}
        this.setState({
            [varName]: this.state[varName].concat([newValues])
        });
    };

    addAnotherSpeaker = () => {
        const { details } = this.state
        let flag = false
        for (let key in details.speakers) {
			if(details.speakers[key].sectionName === ""  || details.speakers[key].celebrityName === "" 
			|| details.speakers[key].description === "" ) {
                flag = true
                break;
            }
        }
        if (flag) {
            swal("Error", "Please fill data to add more", "error")
            return
		}
		let _state = {...this.state}
		_state.details.speakers = details.speakers.concat([{ sectionName: "", celebrityName: "", description: "" }])
        this.setState(_state);
	};
	setEventReccurType = ({target}) => {
		this.setState({
			repeatType: parseInt(target.value)
		})
	}
	setDay = (index) => {
		const _state = {...this.state}
		_state.days[index].isChecked = !_state.days[index].isChecked
		this.setState(_state)
	}
	valid = ( current ) => {
		return current.isAfter( yesterday );
	};
	handleMonthlyTime = ({_d}, idx, fieldName) => {
		const _monthDays = this.state.monthDays.map((day, sidx) => {
            if (idx !== sidx) return day;
            return { ...day, [fieldName]: _d };
		});
		let _state = {...this.state}
		_state.details.monthDays = _monthDays

        this.setState(_state);
	}
	renderMonthlyView = () => {
		const {errors} = {...this.state}
		return <Row>
			<Col md = {12}>
			{
                    this.state.monthDays.map((day, index) => {
                       return (
						   <Row className="mt-3">
							   <Col md="4">
								   <Label>Month Day</Label>
								   <FormGroup>
									   <Datetime
										   locale="en-gb"
										   dateFormat="YYYY-MM-DD"
										   timeFormat={false}
										   isValidDate={this.valid}
										   value={day.monthDays}
										   onChange={(event) => this.handleTimeChange(event, index, "monthDays", "monthDays")}
										   inputProps={{ placeholder: "Datetime Picker Here",disabled: this.state.isSaved }}
									   />
									   <FormText>
										   {
											   typeof errors.monthDays[index] !== 'undefined' && errors.monthDays[index].monthDays ?
												   <p style={styles.error}>{errors.monthDays[index].monthDaysMsg}</p> : null
										   }
									   </FormText>
								   </FormGroup>
							   </Col>
							   <Col md="2" style={styles.marginTop}>
								   {index > 0 ? <Button onClick={(event) => { this.removeSlot(event, index, "monthDays") }}>
									   <i className="fa fa-trash" aria-hidden="true"></i>
								   </Button> : null}
							   </Col>
						   </Row>
                       )
                    })
                }
				<Row>
				<Col md="2" style = {styles.marginTop}>
                        <Button onClick = {(event) =>{this.addSlot("monthDays", new Date())}} disabled = {this.state.isSaved}>
                            Add Another Day
                        </Button>
                        </Col>
				</Row>
			</Col>
		</Row>
	}
	renderSchedule = () => {
		const {errors} = this.state
		const {skipDay, timeSlot, entrySlot} = errors
		const jsx = <div>
                <Row className = "mt-3">
                    <Col md="4">
                        <Label>Start Date  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
                        <FormGroup>
                            <Datetime
								isValidDate = {this.valid}
								locale="en-gb"
								dateFormat = "YYYY-MM-DD"
                                timeFormat={false}
                                onChange = {(event) => this.handleEventStartDate(event, 'startFrom')}
                                inputProps={{ placeholder: "Datetime Picker Here", disabled: this.state.isSaved}}
                            />
                            <FormText>
                                {errors.startFrom ? <p style = {styles.error}>{errors.startFromMsg}</p>: null}
                            </FormText>
                        </FormGroup>
                    </Col>
                </Row>
				<Row className = "mt-3">
                    <Col md="4">
                        <Label>End Date  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
                        <FormGroup>
                            <Datetime
								locale="en-gb"
								isValidDate = {this.valid}
								dateFormat = "YYYY-MM-DD"
                                timeFormat={false}
                                onChange = {(event) => this.handleEventStartDate(event, 'endDate')}
                                inputProps={{ placeholder: "Datetime Picker Here",disabled: this.state.isSaved }}
                            />
                            <FormText>
                                {errors.endDate ? <p style = {styles.error}>{errors.endDateMsg}</p>: null}
                            </FormText>
                        </FormGroup>
                    </Col>
                </Row>
                <Row className = "mt-3">
                    <Col md="6">
                    <Label>Repeat</Label>
                        <FormGroup>
                            <Input type="select"  name = "repeatType" onChange = {this.setEventReccurType} value = {this.state.repeatType} disabled = {this.state.isSaved}>
                                <option value = {1}>
                                    Daily
                                </option>
                                <option value = {2}>
                                    Weekly
                                </option>
								<option value = {3}>
                                    Monthly
                                </option>
                            </Input>
                        </FormGroup>
                    </Col>
                </Row>
                {
                    this.state.repeatType === 2 ? 
                
                <Row className = "mt-3" style = {{paddingLeft: 20}}>
                    
                        {
                            this.state.days.map((day, index) => {
                                return (
                                    
                                    <Col md = "3" key = {day.label}>
                                        <FormGroup>
                                        <Label check>
											<Input type="checkbox" value = {day.value}  type="checkbox" checked={day.isChecked} onChange = {() => this.setDay(day.value)}/> 
											{day.label}
                                        </Label>
                                        </FormGroup>
                                    </Col>
                                    
                                )
                            })
                        }
                </Row>
                : null
				}
				{
					this.state.repeatType === 3 ? this.renderMonthlyView(): null
				}
                
                {
                    this.state.skipDay.map((day, index) => {
                       return (
                           <Row className = "mt-3">
                                <Col md="3">
                            <Label>Skip Day</Label>
                            <FormGroup>
                                <Datetime
									locale="en-gb"
									dateFormat = "YYYY-MM-DD"
									timeFormat={false}
									isValidDate = {this.valid}
                                    value = {day.skipDay}
                                    onChange = {(event) => this.handleTimeChange(event, index, 'skipDay', 'skipDay')}
                                    inputProps={{ placeholder: "Datetime Picker Here",disabled: this.state.isSaved }}
                                />
                                <FormText>
                                    {
                                        typeof skipDay[index] !== 'undefined' && skipDay[index].skipDay ? 
                                        <p style = {styles.error}>{skipDay[index].skipDayMsg}</p>: null
                                    }
                                </FormText>
                            </FormGroup>
                        </Col>
                        <Col md="2" style = {styles.marginTop}>
                        <Button onClick = {(event) =>{this.removeSlot(event,index, 'skipDay')}}>
                            <i className="fa fa-trash" aria-hidden="true"></i>
                        </Button>
                        </Col>
                           </Row>
                       )
                    })
                }
                <Row className = "mt-3">
                    
                    <Col md = "2">
                        <Button onClick = {() => {this.addSlot('skipDay', {skipDay: ""})}} disabled = {this.state.isSaved}>Skip Another Day</Button>
                    </Col>
                    
                </Row>
                {
                    this.state.timeSlot.map((time,index) => {
                        return (
                            <Row key = {index} className = "mt-3">
                                <Col md="3">
                                    <Label>Event Time Slot  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
                                    <FormGroup>
                                        <Datetime
                                            value = {time.startTime}
                                            onChange = {(event) => this.handleTimeChange(event, index, 'startTime', 'timeSlot')}
                                            locale="en-gb"
                                            dateFormat={false}
                                            inputProps={{ placeholder: "Time Picker Here", disabled: this.state.isSaved}}
                                        />
                                        <FormText>
                                            {
                                                typeof timeSlot[index] !== 'undefined' && timeSlot[index].startTime ?
                                                    <p style={styles.error}>{timeSlot[index].startTimeMsg}</p> : null
                                            }
                                        </FormText>
                                    </FormGroup>
                                </Col>
                                <Col md="3" style = {styles.marginTop}>
                                    <FormGroup>
                                        <Datetime
                                            value = {time.endTime}
                                            locale="en-gb"
                                            onChange = {(event) => this.handleTimeChange(event, index, 'endTime', 'timeSlot')}
                                            dateFormat={false}
                                            inputProps={{ placeholder: "Time Picker Here",disabled: this.state.isSaved}}
                                        />
                                        <FormText>
                                            {
                                                typeof timeSlot[index] !== 'undefined' && timeSlot[index].endTime ?
                                                    <p style={styles.error}>{timeSlot[index].endTimeMsg}</p> : null
                                            }
                                        </FormText>
                                    </FormGroup>
                                </Col>
								
                                <Col md="2" style = {styles.marginTop}>
                                    <Button onClick = {(event) =>{this.removeSlot(event,index, 'timeSlot')}}>
                                        <i className="fa fa-trash" aria-hidden="true"></i>
                                    </Button>
                                </Col>
                            </Row>
                        )
                    })
                }
                <Row className = "mt-3">
                    <Col md="2">
                        <Button onClick = {() => this.addSlot('timeSlot', {startTime: "", endTime: ""}) } disabled = {this.state.isSaved}>Add Another Slot</Button>
                    </Col>
                </Row>
                {
                    this.state.entrySlot.map((entry, index) => {
                        return (
                            <Row className = "mt-3">
                                <Col md="3">
                                    <FormGroup>
                                        <Label>Entry Start Time  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
                                        <Datetime
                                            locale="en-gb"
                                            value = {entry.entryStart}
                                            dateFormat={false}
                                            onChange = {(event) => this.handleTimeChange(event, index, 'entryStart', 'entrySlot')}
                                            inputProps={{ placeholder: "Time Picker Here" ,  disabled: this.state.isSaved}}
                                        />
										<FormText>
                                            {
                                                typeof entrySlot[index] !== 'undefined' && entrySlot[index].entryStart ?
                                                    <p style={styles.error}>{entrySlot[index].entryStartMsg}</p> : null
                                            }
                                        </FormText>
                                    </FormGroup>
                                </Col>
                                <Col md="3">
                                    <FormGroup>
                                        <Label>Entry End Time  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
                                        <Datetime
                                            locale="en-gb"
                                            dateFormat={false}
                                            value = {entry.entryEnd}
                                            onChange = {(event) => this.handleTimeChange(event, index, 'entryEnd', 'entrySlot')}
                                            inputProps={{ placeholder: "Time Picker Here",disabled: this.state.isSaved }}
                                        />
										<FormText>
                                            {
                                                typeof entrySlot[index] !== 'undefined' && entrySlot[index].entryEnd ?
                                                    <p style={styles.error}>{entrySlot[index].entryEndMsg}</p> : null
                                            }
                                        </FormText>
                                    </FormGroup>
                                </Col>
                                <Col md="2" style = {styles.marginTop}>
                                    <Button onClick={(event) => { this.removeSlot(event, index, 'entrySlot') }}>
                                        <i className="fa fa-trash" aria-hidden="true"></i>
                                    </Button>
                                </Col>
                            </Row>
                        )
                    })
                }
                <Row className = "mt-3">
                <Col md="2">
                        <Button onClick = {() =>this.addSlot('entrySlot', {entryStart: "", entryEnd: ""})} disabled = {this.state.isSaved}>Add Another Entry Time</Button>
                    </Col>
                </Row>
            </div>
		return jsx
	}
	
	saveEventSlot = () => {
		let flag = true
		const {StandardEvent} = this.props;
		const { details } = this.state
		const _state = {...this.state}
		if (_state.title === "") {
			flag = false
			_state.errors.title = true
			_state.errors.titleMsg = "Title is required"
		}
		if (_state.agenda === "") {
			flag = false
			_state.errors.agenda = true
			_state.errors.agendaMsg = "Agenda is required"
		}
		if (!details.bannerImageKey.imageUrl) {
			flag = false
			_state.errors.bannerImageKey = true
			_state.errors.bannerImageKeyMsg = "Banner Image is required"
		}
		
		if (_state.timeSlot.length < 1) return swal("Error", "Atleast one timeslot is required", "error")
        else {
            _state.timeSlot.map((time, index) => {
                if (time.endTime === "") {
                    flag = false
					_state.errors.timeSlot[index].endTime = true
					_state.errors.timeSlot[index].endTimeMsg =  "Slot End time is required"
                }
                if (time.startTime === "") {
                    flag = false
					_state.errors.timeSlot[index].startTime = true 
					_state.errors.timeSlot[index].startTimeMsg =  "Slot start time is reuqired"
                }
                if (time.startTime > time.endTime) {
                    flag = false
                    _state.errors.timeSlot[index] = {startTime: true, startTimeMsg: "Slot start time cannot be greater"} 
                }
            })
		}
		if (_state.monthDays < 1) return swal("Error", "Atleast one monthly day is required", "error")
        else {
            _state.monthDays.map((day, index) => {
                if (day.monthDays === "" || typeof day.monthDays === 'undefined') {
					flag = false
					_state.errors.monthDays[index].monthDays = true 
					_state.errors.monthDays[index].monthDaysMsg =  "Month Day is reuqired"
                }
            })
		}
        if (_state.entrySlot.length < 1) return swal("Error", "Atleast one Entry time is required", "error")
        else {
            _state.entrySlot.map((entry, index) =>{
                if(entry.entryStart === "") {
					flag = false
                    _state.errors.entrySlot[index].entryStart = true
                    _state.errors.entrySlot[index].entryStartMsg = "Entry Date cannot be empty"
                }
                if (entry.entryEnd === "") {
					flag = false
                    _state.errors.entrySlot[index].entryEnd = true
                    _state.errors.entrySlot[index].entryEndMsg = "Entry End Date cannot be empty"

                } if (entry.entryEnd < entry.entryStart) {
					flag = false
                    _state.errors.entrySlot[index].entryEnd = true
                    _state.errors.entrySlot[index].entryEndMsg = "Entry End Date should greater than start date"
                }
            })
		}
		if (_state.endDate === "") {
			flag = false
			_state.errors.endDate = true
			_state.errors.endDateMsg = "End date cannot be empty"
		}
		if (_state.startFrom === "") {
			flag = false
			_state.errors.startFrom = true
			_state.errors.startFromMsg = "Start date cannot be empty"

		}
		if (_state.endDate < _state.startFrom) {
			flag = false
			_state.errors.endDate = true
			_state.errors.endDateMsg = "End date cannot be small"
		}
		if(_state.timeSlot.length !== _state.entrySlot.length) {
			flag = false
			return swal("Error", "Entry time should be equal to Event Time Slots", "error")
		}
		if (!flag) {
			this.setState(_state)
			return swal("Error", "required information is missing", "error")
		}
		if (details.ticketClassesConfig.length > 0) {
			Object.keys(_state.ticketClassesCount).map((key , index) => {
				if (StandardEvent.ticketClasses.length > 0 && key === StandardEvent.ticketClasses[index].ticketClassId && !this.props.EventSlot.data){
					details.ticketClassesConfig[index].availableTickets = _state.ticketClassesCount[key]
				}
				if (details.ticketClassesConfig.length > 0 && key === details.ticketClassesConfig[index].ticketClassId){
					details.ticketClassesConfig[index].availableTickets = _state.ticketClassesCount[key]
				}
			})
		}
		if (StandardEvent.eventId === "" && (!this.props.eventSlotDetail) && this.state.parentEventId === "") {
			return swal('Error', 'Create Event First', 'error') 
		}
		const perDayrepetition = []
		let timeConvert = {}
		let DateFormat
		_state.timeSlot.map((time, index) => {
			timeConvert.startTimeFormat = moment(time.startTime).format("HH:mm")
			timeConvert.endTimeFormat = moment(time.endTime).format("HH:mm")
			DateFormat = moment(this.state.startFrom).format('YYYY-MM-DD')
			timeConvert.ISOStartTime = new Date(DateFormat + " " + timeConvert.startTimeFormat).toISOString()
			timeConvert.ISOEndTime = new Date(DateFormat + " " + timeConvert.endTimeFormat).toISOString()
			timeConvert.entryStartTimeFormat = moment(_state.entrySlot[index].entryStart).format("HH:mm")
			timeConvert.entryEndTimeFormat = moment(_state.entrySlot[index].entryEnd).format("HH:mm")
			timeConvert.ISOEntryStart = new Date(DateFormat + " " + timeConvert.entryStartTimeFormat).toISOString()
			timeConvert.ISOEntryEnd = new Date(DateFormat + " " + timeConvert.entryEndTimeFormat).toISOString()
			perDayrepetition.push({
				startTime: timeConvert.ISOStartTime,
				endTime: timeConvert.ISOEndTime,
				entryStart: timeConvert.ISOEntryStart,
				entryEnd: timeConvert.ISOEntryEnd,
			})
		})
		const skipDays = []
		if(_state.skipDay.length > 0) _state.skipDay.map(skip => {skipDays.push(skip.skipDay)})
		let _eventImagesKeys = [] 
		if(details.eventImagesKeys.length > 0) {
			_eventImagesKeys = details.eventImagesKeys.filter(image => {
				return image != null;
			})
		}
		
		let obj = {
			"recurringMetaData" : {
				"repeatType": this.state.repeatType,
				"occurenceDateRange": {
					"startFrom": moment(this.state.startFrom).format('YYYY-MM-DD'),
					"endDate": moment(this.state.endDate).format('YYYY-MM-DD')
				},
				"skipDate": skipDays,
				"perDayrepetition": perDayrepetition
			},
			"venueId" : details.venueId.split(" ")[0],
			"eventImagesKeys" : _eventImagesKeys,
			"bannerImageKey" : details.bannerImageKey,
			"sections" : details.speakers,
			"isOnlineSaleOpen" : 1,
			"isPublished" : this.state.details.isPublished,
			"isFeatured" : this.state.isFeatured,
			"agenda": this.state.agenda,
			"eventTitle": this.state.title,
			"slotSaved":false,
			"sections": this.state.details.sections,
			"isTicketsPasswordProtected" : 0,
			"totalAvailableTickets": _state.assgiendSeats,
			"ticketClasses" : details.ticketClassesConfig,
			"slotSeats" : {
				"seats": [{
					"sectionId": "abc",
					"seats": this.state.reservedSeats
				}]
			}
		}
		if (this.state.repeatType === 2) {
			const filterDays = []
			this.state.days.filter(day => {
				if(day.isChecked) return filterDays.push(day.value)
			})
			obj.recurringMetaData.weekDays = filterDays
		}
		if (this.state.repeatType === 3) {
			let monthlyDays = []
			this.state.monthDays.map(day => {
				day = moment(day.monthDays).format("YYYY-MM-DD")
				monthlyDays.push(parseInt(day.split('-')[2]))
			})
			obj.recurringMetaData.monthDays = monthlyDays
		}
		if (this.props.parentEventId) obj.parentEventId = this.props.parentEventId
		if (this.state.parentEventId != null) {
			obj.parentEventId = this.state.parentEventId
			obj.eventSlotId = this.state.eventSlotId
		} else obj.parentEventId = StandardEvent.eventId
		if(flag) {
			this.props.saveRecurringEvent(obj)
		}
			
	}
	confirm = () => {
		let _state = {...this.state}
		_state.details.isPublished = true
		this.setState(_state, () => {this.saveEventSlot()})
	}
	cancel = () => {
		let _state = {...this.state}
		_state.details.isPublished = false
		this.setState(_state, () => {this.saveEventSlot()})
	}

	componentWillReceiveProps(nextProps) {
		const { Images, Venues } = nextProps;
		let _state = {...this.state}
		if (Venues !== this.props.Venues && Venues.data.length > 0) {
			if(_state.details.venueId === '') {
				_state.details.venueId = Venues.data[0]._id + " " + Venues.data[0].seatingCapacity
			_state.availableSeats = Venues.data[0].seatingCapacity
			_state.totalVenueSeats = Venues.data[0].seatingCapacity
			}
			
			_state.venues = Venues.data
			this.setState(_state, function(){
			})
		}
		
		if(Images.bannerImage != "" && Images.bannerImage !== this.props.Images.bannerImage) {
			_state.details.bannerImageKey = Images.bannerImage
			this.setState(_state, function(){
			})
		}
		if (Images.galleryImages != "" && Images.galleryImages !== this.props.Images.galleryImages) {
			let _eventImagesKeys = [];
			_eventImagesKeys = _state.details.eventImagesKeys.concat(Images.galleryImages)
			_state.details.eventImagesKeys = _eventImagesKeys
			this.setState(_state, function(){
				//this.props.resetImageState()
			})
		}
		if(nextProps.RecurringEvent.error) {
			if (this.props.newSlot) this.props.toggle(true)
			if(Array.isArray(nextProps.RecurringEvent.errors)) {
				nextProps.RecurringEvent.errors.map(error => {
					NotificationManager.error(error, "Error")
				})
			} else {
				NotificationManager.error(nextProps.RecurringEvent.errors, "Error")
			}
			this.props.resetSlot()
		}
		if(nextProps.RecurringEvent.saved) {
			this.setState({
				isSaved: true
			}, () => {
				this.props.resetSlot()
			})
			if (this.props.newSlot) this.props.toggle(true)
			NotificationManager.success("Event Slot saved successfully", "Sucess", 3000)
	
		}

	}

	saveSpeakers = (speakers) => {
		let _state = {...this.state}
		if(speakers.length > 0) {
			speakers.map(speaker => {
				speaker.sectionLabel = (speaker.sectionLabel).toLowerCase().trim()
				if(!_state.uniqueSections[speaker.sectionLabel]) {
					_state.uniqueSections[speaker.sectionLabel]  = uuid();
				}
				speaker.sectionId = _state.uniqueSections[speaker.sectionLabel]
			})
			
		}
		
		_state.details.sections = _state.details.sections.concat(speakers)
		_state.uniqueSections = _state.uniqueSections
		_state.showSpeakersSection = false
		this.setState(_state)
	}

	removeSpeaker = (id) => {
		let _state = {...this.state.details}
		let _speakers = _state.sections.filter((speaker, index)  => {
			return index != id
		})
		_state.sections  = _speakers
		this.setState({details: _state})
	}
	showSpeakers = () => {
		return (
			<Table>
				<thead>
					<tr>
						<th>
							Title
						</th>
						<th>Name</th>
						<th>Description</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{
						this.state.details.sections.map((section, index) => {
							return (
								<tr>
								<td>
									{section.celebrityTitle}
								</td>
								<td>
									{section.celebrityName}
								</td>
								<td>
									<div>
									{section.celebrityDescription ? _.truncate(section.celebrityDescription): null}
									</div>
								</td>
								<td>
									<img src={section.celebrityImageKey.imageUrl} style = {styles.speakerImage} />
								</td>
								<td>
									<Button onClick={() => this.removeSpeaker(index)} disabled = {this.state.isSaved}>Delete</Button>
								</td>
							</tr>
							)
							
						})
					}
				</tbody>
			</Table>
		)
	}

	changeImage = (imageType, index) => {
		let _state = {...this.state}
		if(imageType === "banner") {
			_state.details.bannerImageKey = {}
		} else  delete _state.details.eventImagesKeys[index]
		this.setState(_state)

	}

	render() {
		const { Images, RecurringEvent,Venues } = this.props
		const {errors} = this.state
		return (
			<React.Fragment>
				{Venues.loading || Images.loading || RecurringEvent.loading ? <Loader/> : null}
				{this.showVenueModal()}
				<Row>
					<Col md={{size:8, offset: 2}}>
					<Row>
						<Col md = "8" className = "mt-3">
							<FormGroup>
								<Label>Title  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
								<Input type="text" name = "title" value = {this.state.title} onChange = {this.handleInputChange} disabled = {this.state.isSaved}/>
							</FormGroup>
							<FormText>
								{errors.title ? <p style = {styles.error}>{errors.titleMsg}</p>: null}
							</FormText>
						</Col>
					</Row>
					<Row>
						<Col md = "8" className = "mt-3">
							<FormGroup>
								<Label>Agenda  <span style = {{fontSize: 16, color: 'red', fontWeight: 'bold'}}>*</span></Label>
								<Input type="text" name = "agenda" value = {this.state.agenda} onChange = {this.handleInputChange} disabled = {this.state.isSaved}/>
							</FormGroup>
							<FormText>
								{errors.agenda ? <p style = {styles.error}>{errors.agendaMsg}</p>: null}
							</FormText>
						</Col>
					</Row>
					<Row>
						<Col md = "8" className = "mt-3">
								<FormGroup>
									<Label for="exampleSelect">Featured Event</Label>
									<Input type="select" name="isFeatured" value = {this.state.isFeatured} disabled = {this.state.isSaved} onChange = {this.handleInputChange}>
										<option value = {true}>Yes</option>
										<option value = {false}>No</option>
									</Input>
								</FormGroup>
						</Col>
					</Row>
					<Row>
						<Col md = "8" className = "mt-3">
							{this.renderSchedule()}
						</Col>
					</Row>
					
					<Row>
						<Col md = "6" className = "mt-3">
							<Venue venues = {this.state.venues} handleVenueChange = {this.handleVenueChange}
								venueId = {this.state.details.venueId}
								totalSeating = {this.state.totalVenueSeats} enableAvailableTickets = {false} 
								availableSeats = {this.state.availableSeats} 
								availableSeatsCallback = {this.setAvailableSeats}
								isSaved = {this.state.isSaved}
							/>
						</Col>
						<Col md = "2" style = {{marginTop: 62}}>
							<Button onClick = {this.showVenue}>Add Venue</Button>
						</Col>
					</Row>
					<Row className = "mt-3">
						<Col md = "4" >
							<h3>Add Selected seats to class</h3>
						</Col>
						<Col md = "8">
							{
								
								this.state.details.ticketClassesConfig ? this.state.details.ticketClassesConfig.map((ticket, index) => {
									return (
										<ButtonGroup key = {index}>
											<Button
												onClick={() => this.setCurrentClass(ticket)} 
												disabled = {this.state.isSaved}
												active={this.state.selectedClass === ticket.ticketClassName}
												style={{ backgroundColor: ticket.ticketClassColor, marginRight: 5, marginBottom:5, }}>
												{ticket.ticketClassName}
											</Button>
										</ButtonGroup>
										
										
									)
								}): <p>Create ticket classes first</p>
							}
						</Col>
					</Row>
					<Row>
						<Col>
							<h4>Assigned {this.state.assgiendSeats}/{this.state.availableSeats}</h4>
						</Col>
					</Row> 
					<Row>
						<Col md = "8" className = "mt-3" style = {styles.seatsScroll}>
						<Seats totalSeats = {this.state.availableSeats} handleClick = {this.reserveSeat} isSaved = {this.state.isSaved}
							reservedSeats = {this.state.reservedSeats} activeClass = {this.state.selectedClass} callback = {this.callback}/>
						</Col>
					</Row>
					<Row>
						<Col md = "8" className = "mt-3">
						<Row>
                    <Col md = "3">
                     Class Name
                    </Col>
                    <Col md = "3">
                     Availabe Tickets
                    </Col>
                    <Col md = "3">
                     Ticket Price
                    </Col>
                    </Row>
						<ListTicketClasses isSaved = {this.state.isSaved} ticketClasses = {this.state.details.ticketClassesConfig} ticketClassesCount = {this.state.ticketClassesCount} handleTicketChange = {this.handleTicketChange}/>
						</Col>
					</Row>
					
						<Row className = "mt-3">
							<Col style = {{border: '1px solid',marginTop: 15, marginLeft: 10}} md ="8"></Col>
						</Row>

						<Row className = "mt-3">
						<Row>
								<Col>
									<Col><h3>
										Banner Image
										</h3>
									</Col>
									<Col md = "12">
									 {this.state.details.bannerImageKey.imageUrl? 
									 	<div style = {{float: 'left'}}>
											 	<img src = {this.state.details.bannerImageKey.imageUrl} 
										style = {{marginLeft: '10px', marginRight: '10px', float: 'left', 
										height:  "150px", width : "150px"}} />
										<Button style = {styles.editButton} disabled= {this.state.isSaved} onClick ={() => this.changeImage("banner", 0)}><i className="fa fa-trash"></i></Button>
										 </div>
									 	: null}
									 {<DropImage  multi={false}  isSaved = {this.state.isSaved}
									 handleImageDrop={this.handleImageDrop} name ="banner" /> }
									</Col>
								</Col>
							</Row>
							<Col className = "col-md-12">
								{this.state.errors.bannerImageKey ? <p style = {styles.error}>
										 {this.state.errors.bannerImageKeyMsg}</p>: null}
								</Col>
							<Row>
								
							</Row>

						</Row>
						<Row className = "mt-3">
							<Row>
								<Col>
									<Col><h3>
										Gallery Images (optional)
										</h3>
									</Col>
									<Col md = "12">
									 {this.state.details.eventImagesKeys.length >0 ? this.state.details.eventImagesKeys.map((image, index) => {
										 return (
											<div style = {{float: 'left'}}>
												 <img src = {image ? image.imageUrl: null} 
										style = {{marginLeft: '10px', marginRight: '10px', float: 'left', 
										height:  "150px", width : "150px"}}
										  key = {index}/>
										  <Button onClick ={() => this.changeImage("gallery", index)} disabled= {this.state.isSaved} ><i className="fa fa-trash"></i></Button>
										</div>
										 )
									 }): null}
									 <DropImage  multi={false}  isSaved = {this.state.isSaved}
									 handleImageDrop={this.handleMultiDrop} name ="gallery" />
									</Col>

								</Col>
							</Row>

						</Row>
						<Row className = "mt-3 mx-1">
							<Col className='gradient-border-div' md ="8"></Col>
						</Row>
						<Row className = "mt-3">
							<Col md = "6">

								<Button onClick = {this.showSpeakerSection} disabled = {this.state.isSaved}>
									Add Custom Section
								</Button>
							</Col>
						</Row>
						<Row className = "mt-3 mx-1">
							<Col className='gradient-border-div' md ="8"></Col>
						</Row>
						<Row className = "mt-3">
							<Col md = "6">
							{this.state.details.sections.length > 0 ? this.showSpeakers(): null }
							</Col>
						
							

						</Row>
						<Row className = "mt-3">
						{typeof this.props.newSlot !== 'undefined' ?<Button 
								disabled = {this.state.isSaved ? true: false} onClick = {this.saveEventSlot}>Save</Button> :
								<Popconfirm
								title="Do  you want to save and publish event ?"
								onConfirm={this.confirm}
								onCancel={this.cancel}
								okText="Yes Save & Publish"
								cancelText="Only Save Event"
							>
								<Button 
								disabled = {this.state.isSaved ? true: false}>Save</Button>
							</Popconfirm>}
						</Row>
						{this.state.showSpeakersSection ? 
							<EventSpeakers toggle = {this.toggle} saveSpeakers = {this.saveSpeakers}
								isSaved = {this.state.isSaved}
								isOpen  = {this.state.showSpeakersSection}/>: null}
								</Col>
				</Row>
			</React.Fragment>
		)
	}
}

const styles = {
	marginTop: {
		marginTop: 33
	},
    margin: {
        marginTop: 10,
        marginBottom: 10
	},
	error: {
		color: 'red',
		fontWeight: 'bold',
		marginTop: 2
	},
	seatsScroll: {
		overflowY: 'scroll',
		maxHeight: '250px'
	},
	speakerImage: {
		height: 50,
		width: 50
	},
	editButton: {
		left: '133px',
		position: 'absolute'
	}
}
const mapStateToProps = (state) => {
	return state
}

export default connect(mapStateToProps, {resetImageState, resetSlot,getAllVenues, uploadImage, saveRecurringEvent})(Details)
