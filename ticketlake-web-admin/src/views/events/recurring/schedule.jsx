import React from 'react'
import {Row, Col, FormGroup,Label, Input, Button, FormText} from 'reactstrap'
import Datetime from "react-datetime";
import 'react-datetime/css/react-datetime.css';
import moment from 'moment'
import swal from 'sweetalert';
const days = [{
    value: 'Monday',
    label: 'Monday'
},{
    value: 'Tuesday',
    label: 'Tuesday'
},{
    value: 'Wednesday',
    label: 'Wednesday'
},{
    value: 'Thursday',
    label: 'Thursday'
},{
    value: 'Friday',
    label: 'Friday'
},{
    value: 'Saturday',
    label: 'Saturday'
},{
    value: 'Sunday',
    label: 'Sunday'
}]
export default class schedule extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            skipDay: [{"skipDay": ""}],
            startFrom: "",
            endDate: "",
            timeSlot: [{"startTime": "", "endTime": ""}],
            repeatType: 1,
            entrySlot: [{"entryStart": "", "entryEnd": ""}],
            errors: {
                skipDay: [{skipDayMsg: "", skipDay: false}],
                timeSlot: [{startTimeMsg: "", startTime: false, endTime: false, endTimeMsg: ""}],
                entrySlot: [{entryStartMsg: "", entryStart: false, entryEnd: false, entryEndMsg: ""}]
            }
        }
    }
    handleTimeChange = (event,index,label, varName) => {
		let _state = {...this.state}
        const _mobile = this.state[varName].map((mobile, sidx) => {
            if (index !== sidx) return mobile;
            return { ...mobile, [label]: event._d };
		});
		
		_state[varName] = _mobile
		this.setState(_state);
    };
    handleInputChange = (event) => {
        const {target} = event;
        this.setState({
            [target.name]: parseInt(target.value)
        })
    }
    handleEventStartDate = ({_d}, label) => {
        this.setState({
            [label]: _d
        })
    }
    removeSlot = (event, index, varName) => {
        this.setState({
            [varName]: this.state[varName].filter((s, sidx) => {
                return sidx != index
            })
        });
    };

    addSlot = (varName, newValues) => {
        this.setState({
            [varName]: this.state[varName].concat([newValues])
        });
    };
    saveSchedule = () => {
        const _state = {...this.state}
        let flag = false
        if (_state.timeSlot.length < 1) return swal("Error", "Atleast one timeslot is required", "error")
        else {
            _state.timeSlot.map((time, index) => {
                if (time.endTime === "") {
                    flag = true
                    _state.errors.timeSlot[index] = {endTime: true, endTimeMsg: "Slot End time is required"}
                }
                if (time.startTime === "") {
                    flag = true
                    _state.errors.timeSlot[index] = {startTime: true, startTimeMsg: "Slot start time is reuqired"}
                }
                if (time.startTime > time.endTime) {
                    flag = true
                    _state.errors.timeSlot[index] = {startTime: true, startTimeMsg: "Slot start time cannot be greater"} 
                }
            })
        }
        if (_state.entrySlot.length <1) return swal("Error", "Atleast one Entry time is required", "error")
        else {
            _state.entrySlot.map((entry, index) =>{
                if(entry.entryStart === "") {
                    _state.errors.entrySlot[index].entryStart = true
                    _state.errors.entrySlot[index].entryStartMsg = "Entry Date cannot be empty"
                }
                else if (entry.entryEnd === "") {
                    _state.errors.entrySlot[index].entryEnd = true
                    _state.errors.entrySlot[index].entryEndMsg = "Entry End Date cannot be empty"

                } else if (entry.entryEnd < entry.entryStart) {
                    _state.errors.entrySlot[index].entryEnd = true
                    _state.errors.entrySlot[index].entryEndMsg = "Entry End Date should greater than start date"
                }
            })
        }
        this.setState(_state)
        
    }
    render() {
        const {errors} = this.state
        const {timeSlot, skipDay, entrySlot}  = errors
        return (
            <div></div>   
        )
    }
}

const styles = {
    error : {
        'fontWeight': 'bold',
        'color': 'red'
    }
}
