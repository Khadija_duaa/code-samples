import React from 'react';
import ListGroupCollapse from './ListGroupCollapse';
import Schedule from '../../../components/eventSchedule'
import Detail from '../standard/Details'
// the things.... I see you used an object and thus so will I
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { array } from 'prop-types';

class ReactstrapExample extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            modal: false,
            eventSchedule : []
        }
        this.toggle = this.toggle.bind(this);
    }
    toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
    }
    addSlots = (eventTime) => {
        this.setState({ 
            eventSchedule: [...this.state.eventSchedule,{ eventTime}]
        }, function() {
            this.toggle()
        })
    }
    setSlotName = (index, slotName) => {
        let _state = {...this.state}
        _state.eventSchedule[index].slotName = slotName;
        this.setState(_state)
    }
    removeSlot = (id) => {
		let _state = {...this.state}
		let _events = _state.eventSchedule.filter((event, index)  => {
			return index != id
		})
		_state.eventSchedule  = _events
		this.setState(_state)
	}
    render() {
        return (
            <div>
                <Button color="primary" size="lg" onClick={this.toggle} style = {{marginBottom: '20px'}}>Add Slot</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Add Event Slot</ModalHeader>
                    <ModalBody>
                        <Schedule addSlots = {this.addSlots}/>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggle}>Close</Button>{' '}
                    </ModalFooter>
                </Modal>

                
                {
                    this.state.eventSchedule.length>0 ? 
                    this.state.eventSchedule.map((event, index) => {
                        return (
                            <div>
                                <ListGroupCollapse key = {index} 
                                id = {index} eventDates = {event} eventType = {this.props.eventType} removeSlot = {this.removeSlot} setSlotName = {this.setSlotName}/>
                            </div>
                        )
                    })
                    :null
                }

            </div>
        );
    }
}
const style = {
    button : {
        display: 'flex',
        justifyContent: 'flex-end',
        marginBottom: 10
    }
}
export default ReactstrapExample