import React, { Component } from 'react';
import StepZilla from 'react-stepzilla';
import {
	Card,
	CardBody,
	CardTitle
} from 'reactstrap';

import EventBasicInfo from '../../../components/basicInfo';
import EventTicketClasses from '../../../components/ticketClasses';
import Details from '../standard/Details';
import {resetEventBasicInfo} from '../../../redux/actions/events'
import {resetSlot} from '../../../redux/actions/eventSlot'
import { connect } from 'react-redux'
import EventSlot from './eventSlots'
import Passes from '../../PassesView'

class formSteps extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showNext: true
		};
	}
	showNext = (flag) => {
		this.setState({
			showNext: flag
		})
	}
	componentDidMount() {
		window.scrollTo(0, 0)
		this.props.resetEventBasicInfo()
		this.props.resetSlot()
	}
	render() {
		const steps =
			[
				{ name: 'Basic Information', component: <EventBasicInfo eventType = "SERIES" showNext = {this.showNext}/> },
				{ name: 'Ticket Classes', component: <EventTicketClasses showNext = {this.showNext}/> },
				{ name: 'Details', component: <EventSlot  showNext = {this.showNext} eventType = "SERIES"/> },
				{ name: 'Passes', component: <Passes/>}
			]

		return (
			<Card>
				<CardBody className="border-bottom">
					<CardTitle className="mb-0">
						<h2>Create New Series Event</h2>
					</CardTitle>
				</CardBody>
				<CardBody>
					<div className='example'>
						<div className='step-progress'>
							<StepZilla
								preventEnterSubmission = {true}
								stepsNavigation = {false}
								onStepChange = {this.stepChange}
								showNavigation = {true}
								steps={steps}
							/>
						</div>
					</div>
				</CardBody>
			</Card>

		)
	}
}
const mapStateTopProps = (state) => {
	return state
}
export default connect(mapStateTopProps, {resetEventBasicInfo, resetSlot})(formSteps);
