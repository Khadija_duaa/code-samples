import React from 'react';
import { ListGroupItem, Collapse, Button} from 'reactstrap';
import Details from '../standard/Details'
import moment from 'moment'
class ListGroupCollapse extends React.Component {
  constructor(props) {
    super(props);
    
    this.toggle = this.toggle.bind(this);
    this.state = {collapse: false, firstTime: true};
  }
  componentDidMount() {
    window.scrollTo(0, 0)
  }
  toggle() {
    this.setState({ collapse: !this.state.collapse, firstTime: !this.state.firstTime });
  }
  
  render() {
    const eventDetails = this.props.eventDates;
    return (
      <ListGroupItem style = {{marginTop: 10, marginBottom: 10}}>
        <div>
          <div style = {styles.collapse}>
            <div>
              <div>
              <p onClick={this.toggle}>
                <p style = {{marginRight: '1px'}}><strong>{eventDetails.slotName || null}</strong></p>
                <strong>{moment(eventDetails.eventTime.eventStartTime).format('MM/DD/YYYY')}</strong> -
                <strong>{moment(eventDetails.eventTime.eventEndTime).format('MM/DD/YYYY')}</strong>
              </p>
              </div>
            </div>
            <div>
                <Button onClick = {() => this.props.removeSlot(this.props.id)}> <i className="fa fa-trash" aria-hidden="true"></i></Button>
            </div>
          </div>
          
          <Collapse isOpen={this.props.id === 0 ? this.state.firstTime: this.state.collapse}>
            <Details  id = {this.props.id}  eventType = {this.props.eventType} seriesDetail = {eventDetails} setSlotName = {this.props.setSlotName}/>
          </Collapse>
          
        </div>
      </ListGroupItem>
    );
  }
}

const styles = {
  collapse: {
    display: 'flex',
    justifyContent: 'space-between'
  }
}
export default ListGroupCollapse