import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,  Label,
  Row,
} from 'reactstrap';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import Message from '../../utils/Message';
import {saveVenue,getAllVenues} from "../../redux/actions/eventVenues/index";
import {NotificationManager} from "react-notifications";
import {getCities, getCountries} from '../../utils/common-utils'
// import {GoogleComponent} from "react-google-location";
// const API_KEY = 'AIzaSyD3saUQV3v1ubgCWYdtoBVbVvZ-AVGlgl0';


class VenueForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      formName: 'Add Venue',
      name:'',
      city:'',
      country:'Ghana',
      postalCode: '',
      latitude:'',
      longitude:'',
      address:'',
      seatingCapacity: '',
      errors: {
        name: false,
        city: false,
        country: false,
        postalCode: false,
        latitude: false,
        longitude: false,
        address: false
      }
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0)
    if (this.props.venue) {
      let {name,city,country,latitude,longitude,address, postalCode, seatingCapacity} = this.props.venue;
      this.setState({name,city,country,latitude,longitude,address, postalCode,seatingCapacity, formName: 'Update Venue'});
    }

  }

  onSaveChanges = () => {
    let _state = {...this.state}
    let flag = false
    for (let key in this.state) {
      if (key === 'errors') continue;
      if (this.state[key] === "") {
          flag = true
        _state.errors[key] = true
      }
    }
    if (flag) {
      this.setState(_state)
      return
    }
      // alert('validated')
      this.props.saveVenue(this.state.name,this.state.city,this.state.country,
        this.state.latitude,this.state.longitude,this.state.address,this.state.seatingCapacity,this.state.postalCode, () => {
        if (typeof this.props.redirect === 'undefined') {
          this.props.history.push('/venues')
        }
        if(typeof this.props.redirect !== 'undefined' && !this.props.redirect) {
          this.props.closeModal()}
      },() => {
          this.props.getAllVenues()},() =>{  
             NotificationManager.success(this.props.venue ? 'updated Successfully' :'Created Successfully')},   () =>{NotificationManager.error( 'Error', "Venue Already Exist")})
    
  };
  onInputChange = (e) => {

    let {target} = e;
    let state = {...this.state};
    state[target.name] = target.value;
    state.errors[target.name] = false
    this.setState(state);
  };

  onKeyPress = (e) => {

    const characterCode = e.key;
    if (characterCode === 'Backspace') return;

    const characterNumber = Number(characterCode);
    if (characterNumber >= 0 && characterNumber <= 9) {
      if (e.currentTarget.value && e.currentTarget.value.length) {
        return;
      }
      else if (characterNumber === 0) {
        e.preventDefault()
      }
    } else {
      e.preventDefault()
    }

  };



  // getAddress = () => {
  //   return (
  //     <FormGroup row style={{marginTop:'60px'}}>
  //
  //       <Col md="1" >
  //         <Label style={{fontSize:'15px'}}>Address</Label>
  //       </Col>
  //
  //       <Col md="11" >
  //         <GoogleComponent
  //           apiKey={API_KEY}
  //           language={'en'}
  //           coordinates={true}
  //           onChange={(e) => {
  //             this.setState({place: e})
  //           }}/>
  //       </Col>
  //
  //     </FormGroup>
  //   )
  // };

  getAddress = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>

        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Address</Label>
        </Col>

        <Col md="11" >
          <Input type="text" id="address" name="address"  value={this.state.address} onChange={this.onInputChange} placeholder='Enter Address' />
          <p className='red'>{this.state.errors.address ? "Address is required" : null}</p>
        </Col>
      </FormGroup>
    )
  };
  getName = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>

        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Name</Label>
        </Col>

        <Col md="11" >
          <Input type="text" id="name" name="name"  value={this.state.name} onChange={this.onInputChange} placeholder='Enter Name' />
          <p className='red'>{this.state.errors.name ? "Name is required" : null}</p>
        </Col>

      </FormGroup>
    )
  };

  getLong = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>

        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Long</Label>
        </Col>

        <Col md="11" >
          <Input type="number" id="longitude" name="longitude" value={this.state.longitude} onChange={this.onInputChange}placeholder='Enter Long'/>
          <p className='red'>{this.state.errors.longitude ? "Longitude is required" : null}</p>
        </Col>

      </FormGroup>
    )
  };

  getLat = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>

        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Lat</Label>
        </Col>

        <Col md="11" >
          <Input type="number" id="latitude" name="latitude"  value={this.state.latitude} onChange={this.onInputChange} placeholder='Enter Lat' />
          <p className='red'>{this.state.errors.latitude ? "Latitude is required" : null}</p>
        </Col>

      </FormGroup>
    )
  };

  getCountry = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>

        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Country</Label>
        </Col>

        <Col md="11" >
          <Input type="select" id="country" name="country" placeholder='Enter Country' value={this.state.country} onChange={this.onInputChange}>
              {
                getCountries().map(country => {
                  return (
                    <option value = {country}>{country}</option>
                  )
                })
              }
          </Input>
          <p className='red'>{this.state.errors.country ? "Country is required" : null}</p>
        </Col>

      </FormGroup>
    )
  };

  getSeatingCapacity= () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>

        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Seating Capacity</Label>
        </Col>

        <Col md="11" >
          <Input type="number" id="seatingCapacity" name="seatingCapacity" placeholder='Enter Capacity' value={this.state.seatingCapacity} onChange={this.onInputChange} min = "1"/>
          <p className='red'>{this.state.errors.seatingCapacity ? "Seating Capacity required" : null}</p>
        </Col>

      </FormGroup>
    )
  };


  getCity = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>

        <Col md="1" >
          <Label style={{fontSize:'15px'}}>City</Label>
        </Col>

        <Col md="11" >
          <Input type="select" id="city" name="city"  placeholder='Enter City' value={this.state.city} onChange={this.onInputChange}>
              {
                getCities(this.state.country).map(city => {
                  return (
                    <option value = {city}>{city}</option>
                  )
                })
              }
          
          </Input>
          <p className='red'>{this.state.errors.city ? "City required" : null}</p>
        </Col>

      </FormGroup>
    )
  };

  getPostalCode = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>

        <Col md="1" >
          <Label style={{fontSize:'15px'}}>Postal Code</Label>
        </Col>

        <Col md="11" >
          <Input type="number" id="postalCode" name="postalCode" placeholder='Enter Postal Code' onChange={this.onInputChange} value = {this.state.postalCode}/>
          <p className='red'>{this.state.errors.postalCode ? "Postal Code is required" : null}</p>
        </Col>

      </FormGroup>
    )
  };

  getSeat = () => {
    return (
      <FormGroup row style={{marginTop:'60px'}}>

        <Col md="2" >
          <Label style={{fontSize:'15px'}}>Postal Code</Label>
        </Col>

        <Col md="10" >
          <Input type="number" id="postalCode" name="postalCode" placeholder='Enter Postal Code' onKeyPress={this.onKeyPress}/>
        </Col>

      </FormGroup>
    )
  };




  getForm = () => {
    return (
      <Form action="" method="post" className="form-horizontal">

        <FormGroup row style={{marginTop:'10px'}}>

          <Col md="12">
            <Message style={{color:'black', fontSize:'15px'}}> Please fill the following input fields to add a venue</Message>
          </Col>

        </FormGroup>
        {this.getName()}
        {/*{this.getAddress()}*/}
        {this.getLat()}
        {this.getLong()}
        {this.getCountry()}
        {this.getCity()}
        {this.getAddress()}
        {this.getPostalCode()}
        {this.getSeatingCapacity()}

      </Form>
    );
  };

  handleCancelButton = () => {
    if(typeof this.props.redirect !== 'undefined' && !this.props.redirect) {
      return this.props.closeModal()
    }
    this.props.history.push('/venues')

  }
  getCardFooter = () => {
    return (
      <Row>
        <Col>
          <Button type="submit" size="md" color="secondary" onClick={this.handleCancelButton}>
            Cancel
          </Button>
        </Col>
        <Col>
          <Button type="submit" size="md" color="primary" onClick={this.onSaveChanges}>
            Submit
          </Button>
        </Col>
        
      </Row>
    );
  };


  render() {
    return (
      <div>
        <Card>
          <CardHeader>
            <p style={{fontSize: '30px'}}>Add a Venue</p>

          </CardHeader>

          <CardBody>
            {this.getForm()}
          </CardBody>

          <CardFooter>
            {this.getCardFooter()}
          </CardFooter>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    venue: state.eventVenues.venue,



  }
};


const connectedComponent = connect(mapStateToProps, {saveVenue,getAllVenues})(VenueForm);
export default withRouter(connectedComponent);
