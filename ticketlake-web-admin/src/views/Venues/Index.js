import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container, Modal, ModalBody, ModalFooter, ModalHeader,
  Row,
  Table,
} from "reactstrap";
import connect from "react-redux/es/connect/connect";
import {getAllVenues, setVenue, deleteVenue,resetRedux} from "../../redux/actions/eventVenues/index";
import {Route, Switch, withRouter} from "react-router-dom";
import TableHeads from "../../utils/Headers";
import {NotificationManager} from "react-notifications";
import VenueForm from "./VenueForm";


const header = ["Name", "Country", "City", "Address", "Seating","Postal Code", "Actions"];


class Index extends React.Component {

  state = {
    modalOpen: false
  };

  componentDidMount() {
    window.scrollTo(0, 0)
      // this.props.resetRedux();
    this.props.getAllVenues();
  };

  openDeleteModal = (venue) => this.setState({modalOpen: !this.state.modalOpen,venueDeleteInfo: venue});

  getModalContent = () => {
    return (<Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal}
                   className={'modal-danger ' + this.props.className}>

        <ModalHeader toggle={this.openDeleteModal}>
          Confirm your action
        </ModalHeader>

        <ModalBody>
          Are you sure you want to delete this Venue ?
        </ModalBody>

        <ModalFooter>
          <Button color="danger" onClick={() => {
            let {venueDeleteInfo} = this.state;
            this.props.deleteVenue(venueDeleteInfo.venueId, venueDeleteInfo.i,  () =>{
              NotificationManager.success('Deleted Successfully')
              this.props.getAllVenues();
            },() =>{ NotificationManager.error('Error');});
            this.openDeleteModal(null);
          }}>
            Delete
          </Button>{' '}

          <Button color="primary" onClick={this.openDeleteModal}>
            Cancel
          </Button>

        </ModalFooter>
      </Modal>
    );
  };



  getVenueData = () => {
    return (
      this.props.venues.length > 0 ? this.props.venues.map((n, i) => {
        return (
          <tr key={n._id} >
            <td >
              {n.name}
            </td>

            <td >
              {n.country}
            </td>

            <td >
              {n.city}
            </td>

            <td style={{width:'30%'}}>
              {n.address}
            </td>
            <td>
              {n.seatingCapacity}
            </td>
            <td>
              {n.postalCode}
            </td>

            <td >
              {this.getVenueActions(n, i)}
            </td>
          </tr>)
      }) : null

    );
  };

  getVenueActions = (n, i) => {
    return (
      <Container>
        <Row>
          <Col xs={12} xl={'auto'} style={{marginBottom: '12px'}}>
            <Button type="submit" size="md" color="primary" onClick={() => {
              this.props.setVenue(n);
              this.props.history.push('/venues/venue-form');
            }}>
              Edit
            </Button>
          </Col>

          <Col xs={12} xl={'auto'}>
            <Button type="submit" size="md" color="danger"  onClick={() => {
              this.openDeleteModal(n,i);
            }}>
              Delete
            </Button>
          </Col>
        </Row>
      </Container>
    );
  };
  getTableHeaders = () => {
    return (
      <TableHeads>
        {header}
      </TableHeads>
    );
  };


  getTable = () => {
    return (
      <Table bordered hover striped responsive>
        <thead>
        {this.getTableHeaders()}
        </thead>

        <tbody>
        {this.getVenueData()}
        </tbody>

      </Table>
    );
  };


  getCardBody = () => {
    return (
      <div>
        <Row style={{marginTop: '10px'}}>
          <Col sm={12}>
            {this.getTable()}
          </Col>
        </Row>
      </div>
    );
  };


  render() {
    return (
      <div className="animated fadeIn">
       
        {this.getModalContent()}
        <Row>
          <Col xs="12">
              <Switch>
                  <Route path={'/venues/venue-form'} component={VenueForm}/>
            <Card>
              <CardHeader>
                <Col md={2} style={{float: 'left'}}>
                  <p style={{fontSize: '25px'}}>Venues</p>
                </Col>
                <Col md={11}  style={{textAlign: 'right', marginTop:'5px'}}>
                  <Button color="primary" size="md"
                          onClick={() => {
                            this.props.setVenue(null);
                            this.props.history.push('/venues/venue-form')}}>
                    Add Venue
                  </Button>
                </Col>
              </CardHeader>

              <CardBody>
                {this.getCardBody()}
              </CardBody>
            </Card>
              </Switch>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    venues: state.eventVenues.venues,
    message: state.eventVenues.message,
    error: state.eventVenues.error,
  }
};

const connectedComponent = connect(mapStateToProps, {getAllVenues,setVenue,deleteVenue,resetRedux})(Index);
export default withRouter(connectedComponent);
