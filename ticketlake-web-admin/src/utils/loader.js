import React from 'react';
import Loader from "react-loader-spinner";


const loader = (props) => {
  let _props = {
    type: "Puff",
    color: "red",
    height: "50px",
    width: "50px",
    ...props
  };

  return (
    <div style={{textAlign: 'center', verticalAlign: 'center'}}>
      <Loader {..._props} />
    </div>
  );
};

export default loader;
