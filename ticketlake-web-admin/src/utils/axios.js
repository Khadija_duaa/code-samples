import axios from 'axios';
import {store} from '../index';
import serverConfig from './config.json';

const getJWT = ()=>{
  let {getState} = store;
  let user = getState().userReducer.token;
  return  "";
};
const getOptions = () => {
  let {getState} = store;
  let user = getState().userReducer.token;
  return {
    headers: {
      // 'X-Auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzYyYmM3M2Q3YTVlNjFmZTU2YzViMzgiLCJyb2xlcyI6IjVjNjI1YzVhZjllNTQ3MTc0NzhkMTc5ZiIsImFjY2VzcyI6ImF1dGgiLCJleHBpcnkiOiIyMDE5LTA2LTAzVDA2OjM5OjE1KzAwOjAwIiwiaWF0IjoxNTU0MzU5OTU2fQ.s7ok-Aq9DgA4kbAX6uRpulmR9PtC26KA4CRgRU7XEGQ',
      'X-Auth': `${user}`,
    }
  };
};

const prepareUrl = (api) => (serverConfig.serverUrl + api);

const wrapper = {
  get: (api) => axios.get(prepareUrl(api), getOptions()),
  post: (api, formData = {}) => axios.post(prepareUrl(api), formData, getOptions()),
  patch: (api, formData = {}) => axios.patch(prepareUrl(api), formData, getOptions()),
  put: (api, formData = {}) => axios.put(prepareUrl(api), formData, getOptions()),
  delete: (api) => axios.delete(prepareUrl(api), getOptions()),
};

export default wrapper;
