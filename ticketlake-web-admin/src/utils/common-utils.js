import moment from 'moment';
import _ from 'lodash';

export const getObjectValue = (obj, path) => {
    let val = null;

    if (path.indexOf('.') > -1) {
        let paths = path.split('.');
        val = obj;
        paths.forEach(_path => {
            val = val[_path];
        });
    }
    else {
        val = obj[path];
    }

    return val;
};


export const getCountries = () => {
    const countries = require('./countries');
    return (_.keys(countries)).sort();
};

export const getCities = (country) => {
    const countries = require('./countries');
    return (countries[country]).sort();
};

export const formatAmount = (amount) => {
    if (isNaN(amount)) {
        return amount;
    }

    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'SEK',
        minimumFractionDigits: 2
    });

    return formatter.format(amount);
};

export const formatDate = (date, showTime = true) => {
    if (!date) {
        return "-";
    }
    return showTime ? moment(date).format("DD MMM,YY hh:mm A") : moment(date).format("DD MMM,YY");
};

export const capitalize = (str) => {
    if (str) {
        str = str.toLowerCase();
        return `${str.charAt(0).toUpperCase()}${str.length > 1 ? str.substr(1) : ''}`;
    }
    return str;
};