import React from 'react';
import 'react-notifications/lib/notifications.css';
import ReactDOM from 'react-dom';
import './assets/scss/style.css';
import { Provider } from 'react-redux';
import configureStore from './redux';
import App from './App';
import 'antd/dist/antd.css';

export const store = configureStore();


ReactDOM.render(
  <Provider store = {store}>
  <App/>
  </Provider>
,document.getElementById('root')); 
