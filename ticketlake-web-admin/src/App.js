import React, {Component} from 'react';
import 'react-notifications/lib/notifications.css';
import ReactDOM from 'react-dom';
import { Route, Switch, Router, HashRouter, BrowserRouter, Redirect} from 'react-router-dom';
import './assets/scss/style.css';
// import { Provider } from 'react-redux';
import configureStore from './redux';
// import indexRoutes from './routes/index.jsx';
import { createBrowserHistory } from 'history';
import Fulllayout from './layouts/fulllayout';
import Login from './views/authentication/login';
import Recoverpwd from './views/authentication/recover-pwd';
import {connect} from 'react-redux';
const history = createBrowserHistory();

export const store = configureStore();

class App extends Component {
  authRedirect = () => {
      return this.props.authenticated ? <Route path={'/'} component={Fulllayout} /> : <Route path={'/'} component={Login} />;
  };

  // home = () => {
  //   console.log('looog', )
  //     if (this.props.authenticated===true){
  //       console.log('his', this)
  //         return (
  //           <Redirect to={'/venues'} />
  //         )
  //     }
  //     return null;
  // };
  render() {
    // console.log('this app', this.props.activeUser.data)
    return (
        <BrowserRouter history = {history}>
          <Switch>
                       <>
                              {/* {this.home()} */}
                            
                              {this.authRedirect()}
                              
                            {/* <Route path="/" name="Login Page" component={Login}/> */}
                            {/* <Route path="/venues" name="Login Page" component={Fulllayout}/> */}
                        </>
          </Switch>
        </BrowserRouter>
    );
  }
}
const mapStateToProps = state => {
  return {
      activeUser: state.userReducer.activeUser,
      authenticated: state.userReducer.authenticated,
      token:state
  }
};

export default connect(mapStateToProps)(App);