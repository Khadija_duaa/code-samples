import React from 'react';
import {ModalBody, ModalFooter, ModalHeader, Button,Input} from 'reactstrap';
import Modals from './modal';
import {required, email} from '../CommonComponents/validations';


class ModalPopup extends React.Component{

    state = {

    };
    prepareInputFields = () => {
        const inputFields = [];
        inputFields.push({
            name: 'name',
            label: 'Name',
            validate: [required]
        });

        inputFields.push({
            name: 'phone',
            label: 'Phone',
            validate: [required]

        });

        inputFields.push({
            name: 'email',
            label: 'Email',
            validate: [required, email]
        });

        inputFields.push({
            name: 'type',
            label: 'Type',
            validate: [required]
        });

        inputFields.push({
            name: 'class',
            label: 'Class',
            validate: [required]
        });

        inputFields.push({
            name: 'date',
            label: 'Date',
            validate: [required]
        });

        return inputFields;
    };


    getDeletePopup = (deleteText, onClick, openDeleteModal, modalOpen) => {

        return (

            <Modals isOpen={modalOpen} toggle={openDeleteModal} >
                <ModalHeader >
                    Are you sure ?
                </ModalHeader>

                <ModalBody>
                    <p >
                        Are you sure you want to delete this {deleteText} ?
                    </p>
                </ModalBody>

                <ModalFooter>
                    <Button color="danger" onClick={onClick}>
                        Generate
                    </Button>

                    <Button color="primary" onClick={openDeleteModal}>
                        Cancel
                    </Button>

                </ModalFooter>
            </Modals>
        )
    };

    getFormPopup = (inputFields,openDeleteModal,modalOpen,onClick) => {

        return (

            <Modals isOpen={modalOpen} toggle={openDeleteModal} >

                <ModalBody>
                    <div key={1} className={"col-md-10"} style={{margin: '4% 0 0 6%'}}>
                        <div className={'row'}>
                            {inputFields.map(field => {
                                return (
                                    <div key={field.name} className={"col-md-12"}>
                                        <Input style={{marginBottom: '1%'} }
                                               {...field}
                                               value={this.state[field.name]}
                                               placeholder={field.label}
                                        />
                                    </div>
                                );
                            })}
                        </div>
                    </div>,
                </ModalBody>

                <ModalFooter>
                    <Button color="danger" onClick={onClick}>
                        Add
                    </Button>

                    <Button color="primary" onClick={openDeleteModal}>
                        Cancel
                    </Button>

                </ModalFooter>
            </Modals>
        )
    };


    render(){
        const inputFields = this.prepareInputFields();
        const {isFormEnabled,deleteText, onClick, openDeleteModal, modalOpen} = this.props;
        return(
            <div>
            {isFormEnabled ? this.getFormPopup(inputFields,openDeleteModal,modalOpen,onClick) : this.getDeletePopup(deleteText, onClick, openDeleteModal, modalOpen) }
            </div>
        )
    }
}

export default ModalPopup;