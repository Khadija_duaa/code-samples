import React from 'react';
import {Modal} from 'reactstrap'


class Modals extends React.Component{


    render(){
        const {isOpen,toggle,autoFocus} = this.props;
        return(
            <Modal aria-labelledby='modal-label'  isOpen={isOpen} toggle={toggle} autoFocus={autoFocus} className={'modal-danger ' }>
                <div >
                    {this.props.children && this.props.children}
                </div>
            </Modal>
        )
    }
}

export default Modals