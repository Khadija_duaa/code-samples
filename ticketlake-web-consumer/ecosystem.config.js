module.exports = {
    apps : [{
        name      : 'ticketlake-web-customer',
        script    : 'npm',
        args      : 'start',
        env: {
            NODE_ENV: 'development',
            PORT : 3008
        },
        env_production : {
            NODE_ENV: 'development',

        }
    }],

    deploy : {
        production : {
            user : 'node',
            host : '212.83.163.1',
            ref  : 'origin/master',
            repo : 'git@github.com:repo.git',
            path : '/var/www/html/ticketlake-web-customer-build',
            'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
        }
    }
};
