#!/bin/bash
BRANCH_NAME=$GIT_BRANCH

echo "=============================="

echo $BRANCH_NAME

echo "=============================="
if [ "$BRANCH_NAME" == "origin/master" ];
then
   DOCKERFILE_NAME="Dockerfile.master"
elif [ "$BRANCH_NAME"=="origin/development" ];
then
   DOCKERFILE_NAME="Dockerfile.development"
else
   DOCKERFILE_NAME='';
fi

echo "=============================="
echo $DOCKERFILE_NAME
echo "=============================="