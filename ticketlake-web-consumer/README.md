# Ticketlake Website

Ticket lake is an event management website. User can buy tickets online from the website.
Ticket lake website is developed using React.js and Redux

## Setup development Server
 

```bash
git clone http://58.27.238.210:8060/scm/tic/ticketlake-web-customer.git
cd ticketlake-web-customer
git checkout development
npm install
```

When installation are done, use the following command to start the website server

## Start the server
``` 
 npm start
 ```

