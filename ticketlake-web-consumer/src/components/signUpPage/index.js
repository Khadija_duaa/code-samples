// library
import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import validator from 'validator';
import moment from "moment";
import DatePicker from "react-datepicker";
import Select ,{ components } from 'react-select'

// Redux
import {resetRedux, saveFormData, verifyUser} from '../../redux/user/user-actions';
import connect from 'react-redux/es/connect/connect';
import {NotificationManager} from "react-notifications";
import Loader from "../../commonComponents/loader";
// Helpers
import {getCountries, getCities, getDateFromISO} from '../../utils/common-utils';

const countries = [];
let cities = [];

class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            phoneNumber: '',
            country: '',
            city: '',
            dateOfBirth: '',
            error: '',
            selectedOptionCountry: '',
            selectedOptionCity: []
        };
        this.fetchCities = this.fetchCities.bind(this);
        this.setCity = this.setCity.bind(this);
    }

    componentDidMount() {
        this.props.resetRedux();
        getCountries().forEach((data, i) => {
            countries.push({
                value: data,
                label: data
            });
        });
    };

    onInputChange = (e) => {
        let state = {...this.state};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    onSaveChanges = (e) => {

        e.preventDefault();
        const state = {...this.state};
        if (state.name === '' || state.email === '' || state.password === '' || state.phoneNumber === '' || state.country.value === '' || state.city.value === '' || state.dateOfBirth === '') {
            NotificationManager.error("Error", '', 3000);
            this.setState({error: "Kindly fill all the fields"})
        } else {
            this.props.resetRedux();

            const stateData = {
                name: this.state.name,
                email: this.state.email,
                password: this.state.password,
                phoneNumber: this.state.phoneNumber,
                country: this.state.country ? (this.state.country.value ? this.state.country.value : this.state.country) : this.state.country,
                city: this.state.city ? (this.state.city.value ? this.state.city.value : this.state.city) : this.state.city,
                dateOfBirth: getDateFromISO(this.state.dateOfBirth),
            };

            this.props.saveFormData(stateData);

            this.props.verifyUser(
                (userState) => {
                    this.props.history.push('/sign-up/verification', {...userState});
                },
                () => {
                    this.props.history.push('/')
                },
                (errorCB) => {
                    console.log(errorCB);
                }
            );
        }
    };

    handleChange = (date) => {
        this.setState({
            dateOfBirth: date
        });
    };

    _style = {
        paddingLeft : '20px',
        float: 'left',
        border: ' 1px solid #eee',
        backgroundColor: '#F7F9FB',
        width: '100%',
        padding: ' 14px 170px 14px 20px',
        borderRadius: '6px',
        color: '#666',
        fontSize: '13px',
        webkitAppearance : 'none'
    };

    fetchCities(e) {
        cities = [];
        const citiesArr = [];
        getCities(e.value).forEach((data, i) => {
            citiesArr.push({
                label: data,
                value: data,
                link: e.value
            });
        });
        this.setState({selectedOptionCountry: e});
        this.setState({selectedOptionCity: []});
        this.setState({country: e});
        cities = citiesArr;
    }

    setCity(e) {
        this.setState({selectedOptionCity: e});
        this.setState({city: e});
    }
    getForm = () => {
        const {error} = this.props;

        const required = (value) => {
            if (!value.toString().trim().length) {
                return <span className="error">Required</span>
            }
        };

        const email = (value) => {
            if (!validator.isEmail(value)) {
                return `${value} is not a valid email.`
            }
        };

        const styles = {
            valueContainer: base => ({
                ...base,
                paddingLeft: 20
            })
        };
        return (
            <div id="tab-2" className="tab-content" style={{display: 'block'}}>
                <h3>Sign Up <span>Ticket<strong>Lake</strong></span></h3>
                {
                    this.state.error !== '' ?
                        (this.state.name === '' || this.state.email === '' || this.state.password === '' ||
                            this.state.phoneNumber === '' || this.state.country.value === '' || this.state.city.value === '' ||
                            this.state.dateOfBirth === '')
                            ?
                            <span style={{color: 'red'}}>{this.state.error}</span> : null : null
                }

                {error ? <p style={{color: 'red'}}>{error}</p> : null}

                {
                    this.props.processing ?
                        <Loader/> :
                        <Form>
                            <div className="custom-form">
                                <div className="main-register-form" id="main-register-form2">

                                    <label>Email <span>*</span> </label>
                                    <Input name="email" id="email" type="email" placeholder="Email" value={this.state.email}
                                           validations={[required, email]}
                                           onChange={this.onInputChange}/>

                                    <label>Password <span>*</span></label>
                                    <Input name="password" type="password" placeholder="Password"
                                           validations={[required]}
                                           value={this.state.password}
                                           onChange={this.onInputChange}/>

                                    <label>Name <span>*</span> </label>
                                    <Input name="name" id="name" type="text" placeholder="Name" value={this.state.name} required
                                           validations={[required]}
                                           onChange={this.onInputChange}/>


                                    <label>Phone Number <span>*</span> </label>
                                    <Input name="phoneNumber" id="phoneNumber" type="text" placeholder="Phone Number"
                                           validations={[required]}
                                           value={this.state.phoneNumber}
                                           onChange={this.onInputChange}/>

                                    <label>Country<span>*</span></label>

                                    <Select
                                        validations={[required]}
                                        id="country"
                                        name="country"
                                        className='react-select-container iconSelectDropDown'
                                        classNamePrefix='react-select'
                                        value={this.state.selectedOptionCountry}
                                        onChange={this.fetchCities}
                                        options={countries}

                                        styles={styles}
                                    />

                                    {/*<Input name="country" id="country" type="text" placeholder="Country"
                                   value={this.state.country}
                                   validations={[required]}
                                   onChange={this.onInputChange}/>*/}

                                    <label>City<span>*</span></label>

                                    <Select
                                        validations={[required]}
                                        id="city"
                                        name="city"
                                        className='react-select-container iconSelectDropDown'
                                        classNamePrefix='react-select'
                                        value={this.state.selectedOptionCity}
                                        options={cities}
                                        onChange={this.setCity}
                                        styles={styles}
                                    />

                                    {/* <Input name="city" id="city" type="text" placeholder="City"
                                   value={this.state.city}
                                   validations={[required]}
                                   onChange={this.onInputChange}/>*/}

                                    <label>Date of Birth<span>*</span></label>
                                    <div className={"row dateOfBirth"}>
                                        <div className={"col-md-12"}>
                                            <DatePicker
                                                placeholderText={"Date of Birth"}
                                                selected={this.state.dateOfBirth}
                                                onChange={this.handleChange}
                                                customInput={<Input style={this._style}/>}
                                                maxDate={new Date(moment(new Date()).subtract(10, 'years').format())}
                                                minDate={new Date(moment(new Date()).subtract(100, 'years').format())}
                                                peekNextMonth
                                                showMonthDropdown
                                                scrollableYearDropdown
                                                yearDropdownItemNumber={15}
                                                showYearDropdown
                                                dropdownMode="select"
                                            />
                                        </div>
                                    </div>

                                    <button type="submit" className="log-submit-btn"
                                            onClick={this.onSaveChanges}><span>Register</span></button>
                                </div>
                            </div>
                        </Form>
                }

            </div>
        )
    };

    getLoader = () => {
        if (this.props.processing)
            return (
                <Loader/>
            );

        return null;
    };

    render() {
        return (
            <div>
                {this.getForm()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        error: state.user.error,
        processing: state.user.processing,
    }
};

const connected = connect(mapStateToProps, {resetRedux, saveFormData, verifyUser})(SignUp);
export default withRouter(connected);
