// Library
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Row, Col} from 'reactstrap';
import ReactPaginate from 'react-paginate';
import {Modal, ModalHeader, ModalBody} from 'reactstrap';
import {
    FacebookShareButton,
    TwitterShareButton,
    FacebookIcon,
    TwitterIcon
} from 'react-share';
// Component
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';
import DefaultCard from '../../commonComponents/defaultCard';
import Heading from '../../commonComponents/heading';
import Loader from "../../commonComponents/loader";
//redux
import {getAllPromotedEvents} from '../../redux/event/event-actions';
import {getWishListIdsFromApi, wishListToggle} from '../../redux/wishlist/wishlist-actions';
// Helpers
import {getCardDates, getMaxAndMinPrice} from '../../utils/common-utils';

let isWishlist = false;

class PromotedEventsPage extends Component {

    state = {
        offset: 0,
        pageSize: 12,
        totalPages: 0,
        currentPage: 1,
        activeModal: ''
    };


    getEvents(paginate, currentPage, pageSize, skip) {
        this.props.getAllPromotedEvents(paginate, currentPage, pageSize, skip);
    }

    componentWillMount() {
        this.setState({totalPages: 0});
        document.title = "Ticket Lake - Promoted Events";
        if (this.props.wishLists === null && this.props.auth) {
            this.props.getWishListIdsFromApi();
        }

        this.getEvents(true, this.state.currentPage, this.state.pageSize);

    }

    loadMoreEvents = (e) => {
        console.log("Current Page: ", e.selected + 1);
        this.setState(
            {
                offset: Math.ceil(e.selected * this.state.pageSize),
                currentPage: e.selected + 1
            },
            () => {
                this.getEvents(true, this.state.currentPage, this.state.pageSize);
            }
        );
    };


    wishListToggle = (eventSlotId) => {
        if (this.props.auth) {
            this.props.wishListToggle(eventSlotId);
        }
    };

    sharingSocial = (id) => {
        if (id) {
            this.setState({activeModal: id});
        } else {

            this.setState({activeModal: ''});
        }
    };

    render() {

        if (this.props.promotedEvents) {
            if (this.state.totalPages === 0) {
                this.setState({totalPages: this.props.promotedEvents.promotedEvents.totalPages});
            }
        }

        let shareUrl = 'http://google.com/';
        let promotedEvents = this.props.promotedEvents && this.props.promotedEvents.promotedEvents;

        const cardDisplayJSX = promotedEvents && Array.isArray(promotedEvents.data) && promotedEvents.data.map((data, i) => {
            shareUrl = window.location.protocol + '//' + window.location.host + '/event/detail/' + data.eventSlotId;
            if (this.props.auth) {
                isWishlist = this.props.wishLists && this.props.wishLists !== '' && this.props.wishLists.includes(data.eventSlotId);
            }
            return (
                <>

                    <DefaultCard key={i}
                                 grid={3}
                                 auth={this.props.auth}
                                 cardTitle={data.eventTitle}
                                 image={data.bannerImageKey.imageUrl}
                                 cardLink={'#'}
                                 dates={getCardDates(data.eventDateTimeSlot)}
                                 isWishList={isWishlist}
                                 wishlistLink={() => this.wishListToggle(data.eventSlotId)}
                                 cardAddress={data.venue ? data.venue.address : ''}
                                 country={data.venue ? data.venue.country : []}
                                 city={data.venue ? data.venue.city : []}
                                 onClick={() => this.props.history.push(`/event/detail/${data.eventSlotId}`)}
                                 buttonText={getMaxAndMinPrice(data)}
                                 buttonLink={`/buy-ticket/${data.eventSlotId}`}
                                 sharing={this.sharingSocial}
                                 id={data._id}
                    />

                    <Modal isOpen={this.state.activeModal === data._id} toggle={this.sharingSocial}
                           className={this.props.className}>
                        <ModalHeader toggle={this.sharingSocial}>{data.eventTitle}</ModalHeader>
                        <ModalBody>
                            <h3>Share The Event!!</h3>
                            <Row>
                                <Col md={{size: 2, offset: 4}}>
                                    <FacebookShareButton
                                        url={shareUrl}
                                        quote={data.eventTitle}
                                        className="Demo__some-network__share-button">
                                        <FacebookIcon
                                            size={64}
                                            round/>
                                    </FacebookShareButton>
                                </Col>
                                <Col md={{size: 2}}>
                                    <TwitterShareButton
                                        url={shareUrl}
                                        title={data.eventTitle}
                                        className="Demo__some-network__share-button">
                                        <TwitterIcon
                                            size={64}
                                            round/>
                                    </TwitterShareButton>
                                </Col>
                            </Row>
                        </ModalBody>
                    </Modal>
                </>
            )
        });
        return (
            <div>
                <div id="main" key={1}>
                    <Header/>
                    <div id="wrapper" key={2}>
                        <div className="content">

                            <section id="sec2" style={{paddingTop: '30px', paddingBottom: '10px'}} className={"light-red-bg"}>
                                <div style={{width: '88%', paddingLeft: '5%'}}>
                                    <Heading
                                        style={{marginBottom:'0px', textAlign:'left'}}
                                        heading={"Promoted Events"}
                                        text={"Explore some of the best tips from around the city from our partners and friends!"}/>
                                </div>
                            </section>

                            <section className="light-red-bg small-padding" id="sec1">
                                <div className="container">
                                    <div className="row">

                                        <div className="col-md-12">

                                            {
                                                (!this.props.processing) ?
                                                    (
                                                        <div className="col-list-wrap fw-col-list-wrap">

                                                            <div
                                                                className="list-main-wrap fl-wrap card-listing">

                                                                {promotedEvents ?
                                                                    <Row>
                                                                        {cardDisplayJSX}
                                                                    </Row>
                                                                    :
                                                                    'no data'
                                                                }


                                                            </div>

                                                        </div>
                                                    ) :
                                                    (
                                                        <Loader/>
                                                    )
                                            }


                                        </div>
                                    </div>
                                    <div className="row">

                                        <div
                                            className="col-lg-12 float-left">

                                            <div className="d-flex">


                                                <ReactPaginate
                                                    previousLabel={<i className="fa fa-angle-left"/>}
                                                    nextLabel={<i className="fa fa-angle-right"/>}
                                                    breakLabel={'...'}
                                                    breakClassName={'break-me'}
                                                    pageCount={this.state.totalPages}
                                                    marginPagesDisplayed={2}
                                                    pageRangeDisplayed={5}
                                                    onPageChange={(data) => this.loadMoreEvents(data)}
                                                    containerClassName={'list-inline mx-auto justify-content-center pagination'}
                                                    subContainerClassName={'list-inline-item pages pagination'}
                                                    activeClassName={'active'}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <Footer/>
                </div>
            </div>
        )

    };


}

const mapStateToProps = (state) => {
    return {
        promotedEvents: state.event.promotedEvents,
        processing: state.event.processing,
        auth: state.user.authenticated,
        wishLists: state.wishlist.wishListIds,
        paginationProcessing: state.event.paginationProcessing,
    }
};


const connectedComponent = connect(mapStateToProps, {
    getAllPromotedEvents,
    getWishListIdsFromApi,
    wishListToggle
})(PromotedEventsPage);
export default withRouter(connectedComponent);