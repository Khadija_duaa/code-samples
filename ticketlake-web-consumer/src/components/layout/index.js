// Library
import React from 'react';
// Components
import Header from '../../commonComponents/header';
import Main from '../homePage';
import Footer from '../../commonComponents/footer';

const layout = (props) => {
    return (
        <div id="main">
            <Header/>
            <Main/>
            <Footer/>
            <a className="to-top"><i className="fa fa-caret-up"/></a>
        </div>
    );
};

export default layout;