// Library
import React from 'react';


const buyTicketMyselfForm = (props) => {
    return (
        <div className="card ticketCard">
            <div className="card-header">
                My Self
            </div>
            <div className="card-body">
                <div className="row">
                    <div className='col-md-12'>
                        <div className="col-list-search-input-item location autocomplete-container">
                            <select data-placeholder="Select Seat"
                                    name="myselfSeat"
                                    onChange={(e) => props.changer(e.target.value)}
                                    style={{
                                        float: 'left',
                                        height: '40px',
                                        width: '100%',
                                        marginBottom: '10px',
                                        paddingLeft: '10px',
                                        borderRadius: '4px'
                                    }}
                                    className="chosen-select no-search-select">
                                <option disabled selected>Select Seat</option>
                                {props.seats ? props.seats.map(item => (
                                    <option
                                        value={item.ticketClassType + '|' +item.ticket.name + '|' + item.rowNumber + '|' + item.seatNumber}>
                                        {item.ticketClassType} | {item.ticket.name} | Row #: {item.rowName} | Seat
                                        # {item.seatName}
                                    </option>
                                )) : null}
                                {props.passes ? props.passes.map(item => (
                                    <option
                                        value={item.ticketClassType + '|' + item.passTitle + '|' + item.passId + '|' + item.ticketClassId}>
                                        {item.ticketClassType} | {item.passTitle}
                                    </option>
                                )) : null}
                                <option value={0}>I don't want any</option>
                            </select>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default buyTicketMyselfForm;