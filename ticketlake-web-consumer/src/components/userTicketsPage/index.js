// Library
import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import {
    Table
} from 'reactstrap';

// Component
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';
import {withRouter} from "react-router-dom";
import TableHead from '../../commonComponents/tableHead';
import AuthRoutes from '../../commonComponents/authRotes';
import {getDateAndTimeFromIso} from "../../utils/common-utils";
import Loader from "../../commonComponents/loader";
import UserPagesContainer from '../../commonComponents/userPagesContainer';
//redux
import {getAllTickets} from '../../redux/user/user-actions';
import {BreadcrumbsItem} from "react-breadcrumbs-dynamic";

import {getStateObjForUrl} from './ticketHelper';

const header = ["SR#", "Ticket ID", "Event", "Status", "Type | Class", "Event Date", "Action"];
const spanStyle = {
    color: "#EC1B23",
    cursor: "pointer"
};

let page = '1';
let pageSize = '10';

class Tickets extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ticket: '',
            status: '',
            activeModal: "",
        };
    }

    toggle(id) {
        if (id) {
            this.setState({activeModal: id});
        } else {

            this.setState({activeModal: ''});
        }
    }

    componentDidMount() {
        document.title = "Ticket Lake - User Tickets";
        this.props.getAllTickets('', '', page, 10, true);
    }

    onTicketChange = (e) => {
        let {target} = e;
        let state = {...this.state};
        state[target.name] = target.value;
        this.setState(state);
        this.getChangeFilters(state);
    };

    getChangeFilters = (state) => {
        if (state.ticket === "My Tickets" && state.status === '') {
            this.props.getAllTickets(true, '', 1, 10, true)
        }
        if (state.ticket === "My Tickets" && state.status === "Upcoming") {
            this.props.getAllTickets(true, true, 1, 10, true)
        }
        if (state.ticket === "My Tickets" && state.status === "Expired") {
            this.props.getAllTickets(true, false, 1, 10, true)
        }

        if (state.ticket === "Guest Tickets" && state.status === '') {
            this.props.getAllTickets(false, '', 1, 10, true)
        }

        if (state.ticket === "Guest Tickets" && state.status === "Upcoming") {
            this.props.getAllTickets(false, true, 1, 10, true)
        }

        if (state.ticket === "Guest Tickets" && state.status === "Expired") {
            this.props.getAllTickets(false, false, 1, 10, true)
        }

        if (state.ticket === "All") {
            this.props.getAllTickets('', '', 1, 10, true)
        }

        if (state.ticket === "All" && state.status === "Upcoming") {
            this.props.getAllTickets('', '', 1, 10, true)
        }

        if (state.ticket === "All" && state.status === "Expired") {
            this.props.getAllTickets('', false, 1, 10, true)
        }
    };

    onStatusChange = (e) => {
        let {target} = e;
        let state = {...this.state};
        state[target.name] = target.value;
        this.setState(state);
        this.getStatusFilters(state);
    };

    getStatusFilters = (state) => {
        this.setState({allTickets: []});
        if (state.status === "Upcoming" && state.ticket === '') {
            this.props.getAllTickets('', true, 1, 10, true)
        }
        if (state.status === "Upcoming" && state.ticket === "My Tickets") {
            this.props.getAllTickets(true, true, 1, 10, true)
        }
        if (state.status === "Upcoming" && state.ticket === "Guest Tickets") {
            this.props.getAllTickets(false, true, 1, 10, true)
        }

        if (state.status === "Expired" && state.ticket === "Guest Tickets") {
            this.props.getAllTickets(false, false, 1, 10, true)
        }

        if (state.status === "Expired") {
            this.props.getAllTickets('', false, 1, 10, true)
        }

        if (state.status === "Upcoming" && state.ticket === "All") {
            this.props.getAllTickets('', true, 1, 10, true)
        }

        if (state.status === "Expired" && state.ticket === "All") {
            this.props.getAllTickets('', false, 1, 10, true)
        }
    };

    loadMoreTickets = (e) => {
        e.preventDefault();
        let getStateObj = {};

        if (this.props.ticketPagination && this.props.ticketPagination.hasNextPage === true) {
            if (this.props.ticketPagination.page) {
                page = this.props.ticketPagination.page + 1
            }
        }

        getStateObj = getStateObjForUrl(this.state);

        this.props.getAllTickets(
            getStateObj.self, //self
            getStateObj.status, //status
            page, //page
            pageSize, //pageSize
            false // resetTicket
        )

    };

    getFilters = () => {
        return (
            <div className={'row justify-content-end'}>
                <div className="col-lg-4 col-md-12 offset-lg-3" style={{marginTop: '27px'}}>
                    <select name="ticket"
                            style={{width: '100%'}}
                            onChange={this.onTicketChange}
                            className="filterDropDowns">
                        <option disabled selected>Tickets</option>
                        <option>All</option>
                        <option>My Tickets</option>
                        <option>Guest Tickets</option>
                    </select>
                </div>
                <div className="col-lg-4 col-md-12" style={{marginTop: '27px'}}>
                    <select name="status"
                            style={{width: '100%'}}
                            onChange={this.onStatusChange}
                            className="filterDropDowns">
                        <option disabled selected>Status</option>
                        <option>Upcoming</option>
                        <option>Expired</option>
                    </select>
                </div>

            </div>
        )
    };

    download = () => {
        setTimeout(() => {
            const response = {
                file: 'https://ticketlake.herokuapp.com/api/v1/consumers/get-pdf-from-html',
            };
            // now download:
            // window.open(response.file);
            window.location.href = response.file;
        }, 100);
    };

    getData = () => {
        return (
            <>
                {
                    Array.isArray(this.props.allTickets) && this.props.allTickets.length === 0 ?
                        <tbody>
                        <tr>
                            <td colSpan={7}>No Ticket Purchase</td>
                        </tr>
                        </tbody>
                        :
                        Array.isArray(this.props.allTickets) && this.props.allTickets.map((data, i) => {
                            return (
                                <tbody key={i}>
                                <tr key={i}>
                                    <td>
                                        {++i}
                                    </td>
                                    <td>
                                        {data.srNo}
                                    </td>
                                    <td>
                                        {data.eventInfo.eventTitle}
                                    </td>
                                    <td>
                                        {
                                            data.ticketStatus === 'VALID' ?
                                                <span className={'badge badge-success'}>{data.ticketStatus}</span>
                                                : data.ticketStatus === 'REFUNDED' ?
                                                <span className={'badge badge-warning'}>{data.ticketStatus}</span> :
                                                <span className={'badge badge-danger'}>{data.ticketStatus}</span>
                                        }
                                    </td>
                                    <td>
                                        {data.type} {data.ticketClassInfo.ticketClassName ? "|" : ''} {data.ticketClassInfo.ticketClassName}
                                    </td>
                                    <td>
                                        {getDateAndTimeFromIso(data.eventInfo && data.eventInfo.eventDateTimeSlot ? data.eventInfo.eventDateTimeSlot.eventStartTime : 'Invalid Date')}
                                    </td>
                                    <td>
                                        <span style={spanStyle} onClick={() => this.toggle(data._id)}>Preview</span>
                                        <Modal isOpen={this.state.activeModal === data._id} toggle={() => this.toggle()}
                                               className={this.props.className}>
                                            <ModalHeader toggle={() => this.toggle()}>QR Code
                                                For Ticket ID: <strong>{data.srNo}</strong></ModalHeader>
                                            <ModalBody>
                                                <img src={data.qrCode} alt={"Not Found"}/>
                                            </ModalBody>
                                            {/*<ModalFooter>*/}
                                            {/*<Button color="secondary" onClick={() => this.toggle()}>Cancel</Button>*/}
                                            {/*</ModalFooter>*/}
                                        </Modal>
                                        <span style={{color: "#EC1B23", padding: "0px 4px"}}>|</span>
                                        <span style={spanStyle} onClick={() => this.download()}>Download</span>
                                    </td>

                                </tr>

                                </tbody>
                            )
                        })
                }
            </>
        )
    };

    getTicket = () => {
        return (
            <section className="middle-padding">
                <div className="container">
                    <div className="dasboard-wrap fl-wrap">
                        <div className="dashboard-content fl-wrap">
                            <div className="box-widget-item-header">
                                <h3 style={{color: 'black'}}>Tickets</h3>
                            </div>

                            <div className="list-single-facts fl-wrap">
                                <div className="inline-facts-wrap">
                                    <div className="inline-facts">
                                        <div className="milestone-counter">
                                            <div className="stats animaper">
                                                12
                                            </div>
                                        </div>
                                        <h6>My Tickets</h6>
                                    </div>
                                </div>

                                <div className="inline-facts-wrap">
                                    <div className="inline-facts">
                                        <div className="milestone-counter">
                                            <div className="stats animaper">
                                                04
                                            </div>
                                        </div>
                                        <h6>Guests Tickets</h6>
                                    </div>
                                </div>
                            </div>

                            <div className="list-single-facts fl-wrap" style={{backgroundColor: 'transparent'}}>
                                <div className="box-widget-item-header">
                                    <div className={"row"}>
                                        <div className={"col-lg-4 col-md-12"}>
                                            <h3 style={{marginTop: '14%', color: 'black'}}>Purchased Tickets</h3>
                                        </div>
                                        <div className={"col-lg-6 col-md-12 offset-lg-2"}>
                                            {this.getFilters()}

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <Table responsive>
                                <thead style={{backgroundColor: '#f2f3f8'}}>
                                <TableHead>
                                    {header}
                                </TableHead>
                                </thead>
                                {
                                    this.props.processing ?
                                        <tbody>
                                        <tr>
                                            <td colSpan={7}><Loader/></td>
                                        </tr>
                                        </tbody> :
                                        this.getData()
                                }
                            </Table>
                            {
                                this.props.ticketPagination && this.props.ticketPagination.hasNextPage === true ?
                                    <a className="load-more-button load-more-button-light" href="#"
                                       onClick={(e) => this.loadMoreTickets(e)}>Load more
                                        {
                                            this.props.paginateProcessing ?
                                                <i className="fas fa-spinner"/> : null
                                        }
                                    </a> : null
                            }
                        </div>
                    </div>
                </div>
            </section>
        )
    };

    render() {
        const breadCrumbs = [];
        breadCrumbs.push(<BreadcrumbsItem glyph='home' to='/' key={1}>Home Page</BreadcrumbsItem>);
        breadCrumbs.push(<BreadcrumbsItem to='/user/ticket' key={2}>User Ticket</BreadcrumbsItem>);
        return (
            <AuthRoutes>
                <div id="main">
                    <Header/>
                    <div id="wrapper">
                        <UserPagesContainer
                            page={'ticket'}
                            breadcrumbs={breadCrumbs}
                        >
                            {this.getTicket()}

                        </UserPagesContainer>

                    </div>
                    <Footer/>

                </div>
            </AuthRoutes>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        allTickets: state.user.allTickets,
        processing: state.user.processing,
        ticketPagination: state.user.ticketPagination,
        paginateProcessing: state.user.paginateProcessing
    }
};


const connectedComponent = connect(mapStateToProps, {getAllTickets})(Tickets);
export default withRouter(connectedComponent);
