// Library
import React, {Component} from 'react';
import {withRouter} from "react-router-dom";
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input} from 'reactstrap';
import swal from "@sweetalert/with-react";

// Component
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';
import HeadingWithButton from '../../commonComponents/headingWithButton';
import UserPagesContainer from '../../commonComponents/userPagesContainer';
import AuthRoutes from '../../commonComponents/authRotes';
import TableHead from '../../commonComponents/tableHead';
import Loader from "../../commonComponents/loader";

// Redux
import {connect} from "react-redux";
import {setTopUpAmount} from '../../redux/user/user-actions';
import {BreadcrumbsItem} from "react-breadcrumbs-dynamic";
import {getTransactionHistory} from '../../redux/wallet/wallet-actions';

const header = ["Date", "Transaction ID", "Payment Method", "Payment Type", "Amount"];

class Wallet extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
        this.props.getTransactionHistory(1, 10);
    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    proceed() {
        const val = document.getElementById('topUpAmount').value;
        if (isNaN(val) || val < 0) {
            swal({
                    title: 'Error',
                    icon: "warning",
                    text: 'Please check your amount',
                }
            )
        } else {
            this.setState({
                modal: false
            });
            this.props.setTopUpAmount(parseInt(val));
            this.props.history.push('/user/wallet/top-up');
        }
    }

    loadMoreTransaction = (e) => {
        e.preventDefault();

        if (this.props.walletPagination && this.props.walletPagination.hasNextPage === true) {
            if (this.props.walletPagination.page) {
                this.props.walletPagination.page = this.props.walletPagination.page + 1
            }
        }

        this.props.getTransactionHistory(this.props.walletPagination.page, 10);

    };

    getData = () => {
        return (
            <>
                {
                    this.props.transactionHistory && this.props.transactionHistory.length === 0 ?
                        <tbody key={2}>
                        <tr>
                            <td colSpan={5}>No Wallet Transaction</td>
                        </tr>
                        </tbody> :
                        Array.isArray(this.props.transactionHistory) && this.props.transactionHistory.map(data => {
                            return (
                                <tbody id="scrollTable" key={data._id}>
                                <tr>
                                    <td>{data.createdAt}</td>
                                    <td>{data.transactionId}</td>
                                    <td>{data.paymentMethod}</td>
                                    <td>{data.type}</td>
                                    <td>{data.transactionAmount}</td>
                                </tr>
                                </tbody>
                            )
                        })
                }
            </>
        )
    };

    getWallet = () => {
        return (
            <>
                <section className="middle-padding">
                    <div className="container">
                        <div className="dasboard-wrap fl-wrap">

                            <HeadingWithButton
                                heading={'My Wallet'}
                                buttonText={'+ Top-up balance'}
                                clicker={this.toggle}
                            />

                            <div className="list-single-facts fl-wrap">
                                <div className="inline-facts-wrap">
                                    <div className="inline-facts">
                                        <div className="milestone-counter">
                                            <div className="stats animaper">
                                                ${this.props.userWallet ? this.props.userWallet.availableBalance : ""}
                                            </div>
                                        </div>
                                        <h6>Available balance</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="middle-padding" style={{paddingTop: '15px'}}>
                    <div className="container">
                        <div className="dasboard-wrap fl-wrap">
                            <div className="dashboard-content fl-wrap">
                                <div className="box-widget-item-header">
                                    <h3> Transaction History</h3>
                                </div>
                            </div>

                            <div className={'table-responsive'}>
                                <table className={'table'}>
                                    <thead key={1} style={{backgroundColor: '#f2f3f8'}}>

                                    <TableHead>
                                        {header}
                                    </TableHead>

                                    </thead>

                                    {
                                        this.props.processing ?
                                            <Loader style={{marginLeft: '430%'}}/> :
                                        this.getData()
                                    }

                                </table>
                                {
                                    this.props.walletPagination && this.props.walletPagination.hasNextPage === true ?
                                        <a className="load-more-button load-more-button-light" href="#"
                                           onClick={(e) => this.loadMoreTransaction(e)}>Load more
                                            {
                                                this.props.walletPaginationProcessing ?
                                                    <i className="fas fa-spinner"/> : null
                                            }
                                        </a> : null
                                }
                            </div>
                        </div>
                    </div>
                </section>
            </>
        )
    };

    render() {
        const breadCrumbs = [];
        breadCrumbs.push(<BreadcrumbsItem glyph='home' to='/' key={1}>Home Page</BreadcrumbsItem>);
        breadCrumbs.push(<BreadcrumbsItem to='/user/wallet' key={2}>User Wallet</BreadcrumbsItem>);


        return (
            <AuthRoutes>
                <div id="main">
                    <Header/>
                    <div id="wrapper">
                        <UserPagesContainer
                            page={'wallet'}
                            breadcrumbs={breadCrumbs}
                        >
                            {this.getWallet()}
                        </UserPagesContainer>
                    </div>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                        <ModalHeader toggle={this.toggle}>Top up your balance</ModalHeader>
                        <ModalBody>

                            <FormGroup>
                                <Label for="amount">Top Up Amount</Label>
                                <Input type="text" name="amount" id="topUpAmount"
                                       placeholder="Top up amount"/>
                            </FormGroup>

                        </ModalBody>
                        <ModalFooter>
                            <Button color="success" className={'buttonDefault'}
                                    onClick={() => this.proceed()}>Proceed</Button>{' '}
                            <Button color="secondary" className={'buttonDefault defaultBackground'}
                                    onClick={this.toggle}>Cancel</Button>
                        </ModalFooter>
                    </Modal>

                    <Footer/>

                </div>
            </AuthRoutes>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        userWallet: state.user.userWallet,
        transactionHistory: state.wallet.transactionHistory,
        walletPagination: state.wallet.walletPagination,
        walletPaginationProcessing: state.wallet.walletPaginationProcessing,
        processing: state.wallet.processing
    }
};

const connectedComponent = connect(mapStateToProps, {setTopUpAmount, getTransactionHistory})(Wallet);
export default withRouter(connectedComponent);