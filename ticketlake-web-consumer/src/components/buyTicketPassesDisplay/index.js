// library
import React, {Component} from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";


class BuyTicketPassesDisplay extends Component {

    state = {
        activeModal: null
    };


    toggle(string) {
        if (string) {
            this.setState({activeModal: string});
        } else {
            this.setState({activeModal: null});
        }
    };

    render() {
        const required = (value) => {
            if (!value.toString().trim().length) {
                return <span className="error">Required</span>
            }
        };
        const passData = this.props.passes;
        if (passData.seats && passData.seats.length > 0) {
            return (

                <div className="booking-form-wrap">
                    <div className="list-single-main-item fl-wrap hidden-section tr-sec" style={{border: '0'}}>
                        <div className="profile-edit-container">
                            <div className="custom-form">
                                <fieldset className="fl-wrap">
                                    <div className="list-single-main-item-title">
                                        <h3 style={{fontSize: '16px'}}>
                                            {passData.passTitle}
                                            <a href="javascript:void(0)"
                                               onClick={() => this.toggle(passData.passId + '|' + passData.uniqueIndex)}
                                               style={{
                                                   color: '#000',
                                                   fontSize: '18px'
                                               }}
                                            > <i className={'fa fa-info-circle'}/></a>
                                            <div style={{
                                                height: '40px',
                                                width: '40px',
                                                float: 'right',
                                                background: passData.ticketClassColor,
                                                borderRadius: '10px'
                                            }}/>
                                        </h3>

                                    </div>
                                    <div>

                                    </div>
                                    <div className="row  float-left">
                                        <Form>
                                            <div className='col-md-12'>
                                                <div className='col-md-4 float-left'>
                                                    <Input
                                                        type="text"
                                                        name={'name'}
                                                        validations={[required]}
                                                        placeholder={'Name'}
                                                        onChange={(e) => this.props.onInputChange(
                                                            'name',
                                                            e.target.value,
                                                            passData.uniqueIndex,
                                                            passData.passId
                                                        )}/>
                                                </div>
                                                <div className='col-md-4 float-left'>
                                                    <Input
                                                        type="email"
                                                        name={'email'}
                                                        validations={[required]}
                                                        placeholder={'Email'}
                                                        onChange={(e) => this.props.onInputChange(
                                                            'email',
                                                            e.target.value,
                                                            passData.uniqueIndex,
                                                            passData.passId
                                                        )}
                                                    />
                                                </div>
                                                <div className='col-md-4 float-left'>
                                                    <Input
                                                        type="text"
                                                        name={'phoneNumber'}
                                                        validations={[required]}
                                                        placeholder={'Contact Number'}
                                                        onChange={(e) => this.props.onInputChange(
                                                            'phoneNumber',
                                                            e.target.value,
                                                            passData.uniqueIndex,
                                                            passData.passId
                                                        )}
                                                    />
                                                </div>
                                            </div>
                                        </Form>
                                    </div>
                                </fieldset>

                            </div>
                        </div>
                    </div>
                    <Modal isOpen={this.state.activeModal === passData.passId + '|' + passData.uniqueIndex}
                           toggle={() => this.toggle()} className={this.props.className}>
                        <ModalHeader toggle={() => this.toggle()}>Seats Detail</ModalHeader>
                        <ModalBody>
                            <h5><strong>{passData.passTitle}</strong></h5>
                            <br/>
                            {passData.seats.map((item, i) => (
                                <>
                                    {/*<h6>SLOT: {item.slotId}</h6>*/}
                                    {/*<h6>SEAT #: <b>{item.seat.seatNumber}</b> | ROW*/}
                                    {/*    #: <b>{item.seat.rowNumber}</b></h6>*/}
                                    <h6>SEAT NAME: <b>{item.seat.seatName}</b> | ROW
                                        Name: <b>{item.seat.rowName}</b></h6>
                                    <span>--------------------------------------------------</span>
                                </>
                            ))}
                        </ModalBody>

                    </Modal>
                </div>

            );
        } else {
            return (
                <>
                </>
            )
        }
    }
}


export default BuyTicketPassesDisplay;