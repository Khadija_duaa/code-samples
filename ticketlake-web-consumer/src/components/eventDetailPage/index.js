// Library
import React, {Component} from 'react';
import _ from 'lodash';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {NavLink} from 'react-router-dom';
import {Breadcrumbs, BreadcrumbsItem} from 'react-breadcrumbs-dynamic';
import {Modal, ModalHeader, ModalBody} from 'reactstrap';
import {Row, Col} from 'reactstrap';
import Gallery from 'react-grid-gallery';
import {
    FacebookShareButton,
    TwitterShareButton,
    FacebookIcon,
    TwitterIcon
} from 'react-share';
// Component
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';
import CardComponent from '../../commonComponents/cardComponent';
import Loader from "../../commonComponents/loader/";
import EventDetailSlider from '../eventDetailSlider';
import SuggestedEventSlider from '../suggestedEventSlider';
import {getWishListIdsFromApi, wishListToggle} from "../../redux/wishlist/wishlist-actions";
import MapContainer from '../../commonComponents/googleMapComponent';
// Error
import Error from '../../commonComponents/error';
//redux
import {getEventDetail, getAllEventsDefault} from '../../redux/event/event-actions';
//config
import {STANDARD_EVENT, SERIES, RECUR} from "../../utils/config";
// Helper
import {getDateFromISO, getTimeFromISO, dateSplitter, timeSplitter} from '../../utils/common-utils';
import {valueAlreadyExists, dateAlreadyExists, getTags} from './detailPageHelper';

let isWishlist = false;
let shareUrl = 'http://google.com/';

let dateTimeArray = [];
let eventsArray = [];

class EventDetail extends Component {

    state = {
        labelArray: [],
        click: false,
        date: '',
        time: '',
        activeModal: '',
        timeArray: []
    };

    componentDidUpdate() {
        const eventId = this.props.match.params.id;
        if (this.props.singleEventDetail) {
            let slotId = this.props.singleEventDetail.eventSlotId;
            if (slotId !== eventId) {
                if (this.state.timeArray.length > 0) {
                    this.setState({timeArray: []});
                }
                this.props.getEventDetail(eventId);
                if (this.props.auth) {
                    this.props.getWishListIdsFromApi();
                }
            }

        }
    }

    componentDidMount() {
        this.props.getAllEventsDefault(
            false, // IsFeatured
            true, // isPublished
            false, // isDraft
            [], //Categories
            null, // To
            null, // From
            null, // City
            false, // Paginate
            1, // page
            null, // PageSize
            null // search
        );
    }

    componentWillMount() {
        const eventId = this.props.match.params.id;
        this.props.getEventDetail(eventId);
        if (this.props.auth) {
            this.props.getWishListIdsFromApi();
        }
    }

    sharingSocial = (id) => {
        if (id) {
            this.setState({activeModal: id});
        } else {

            this.setState({activeModal: ''});
        }
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.singleEventDetail && nextProps.singleEventDetail.sections) {
            if (this.state.labelArray !== nextProps.singleEventDetail.sections) {
                this.setState({
                    labelArray: this.getGroupBySections(nextProps.singleEventDetail.sections, true),
                    date: getDateFromISO(nextProps.singleEventDetail.eventDateTimeSlot.eventStartTime),
                    time: getTimeFromISO(nextProps.singleEventDetail.eventDateTimeSlot.eventStartTime),
                })
            }
        }
    }

    getRelatedEvents = (relatedEvents, id) => {
        if (typeof relatedEvents !== 'undefined') {
            for (let i = 0; i < relatedEvents.length; i++) {
                if (id && id !== relatedEvents[i]._id) {
                    return (
                        relatedEvents.length > 0 ?
                            <EventDetailSlider heading={"Related Events"}
                                               text={"Explore Some More Events related to this Category"}
                                               cards={relatedEvents}/> : null
                    )
                }
            }
        }
    };

    getGroupBySections = (sections, label) => {

        if (label) {
            let uniqueIDs = [];
            if (typeof sections !== 'undefined') {
                for (let i = 0; i < sections.length; i++) {
                    if (!valueAlreadyExists(uniqueIDs, sections[i])) {
                        uniqueIDs.push(sections[i].sectionId);
                    }
                }
            }

            let list = [];
            for (let i = 0; i < uniqueIDs.length; i++) {
                let dataList = [];
                let label = '';
                if (typeof sections !== 'undefined') {
                    for (let j = 0; j < sections.length; j++) {
                        if (uniqueIDs[i] === sections[j].sectionId) {
                            if (label === '') {
                                label = sections[j].sectionLabel
                            }
                            dataList.push({
                                data: sections[j]
                            })
                        }
                    }

                }

                list.push({
                    label: label,
                    data: dataList
                });
            }
            return list;
        } else {
            let uniqueIDs = [];

            if (typeof sections !== 'undefined') {
                for (let i = 0; i < sections.length; i++) {
                    if (!valueAlreadyExists(uniqueIDs, sections[i])) {
                        uniqueIDs.push(sections[i].sectionId);
                    }
                }
            }

            let list = [];

            for (let i = 0; i < uniqueIDs.length; i++) {
                let dataList = [];
                let label = '';
                if (typeof sections !== 'undefined') {
                    for (let j = 0; j < sections.length; j++) {
                        if (uniqueIDs[i] === sections[j].sectionId) {
                            if (label === '') {
                                label = sections[j].sectionLabel
                            }
                            dataList.push({
                                data: sections[j]
                            })
                        }
                    }
                }

                list.push({
                    label: label,
                    data: dataList
                });
            }

            return (
                <div>
                    {
                        list.map(data => {
                            return (
                                <div className="list-single-main-item fl-wrap" id="sec3">

                                    <div className="list-single-main-item-title fl-wrap">
                                        <h3 style={{textTransform: 'capitalize'}}>{data.label}</h3>
                                    </div>

                                    <div className="listing-features fl-wrap">
                                        {
                                            data.data.map(card => {
                                                return (
                                                    <CardComponent {...card}/>
                                                )
                                            })
                                        }
                                    </div>

                                </div>
                            )
                        })
                    }
                </div>
            )
        }
    };

    getUniqueDates = (dataArray) => {
        let uniqueDates = [];
        if (typeof dataArray !== 'undefined') {
            for (let i = 0; i < dataArray.length; i++) {
                if (!dateAlreadyExists(uniqueDates, dataArray[i])) {
                    uniqueDates.push(dateSplitter(dataArray[i].eventDateTimeSlot.eventStartTime));
                }
            }
        }
        return uniqueDates;
    };

    onDateChange = (e) => {
        let {target} = e;
        let state = {...this.state};
        state[target.name] = target.value;
        this.setState({...state});
    };

    getSlotID = (time) => {

        let data = this.props.singleEventDetail;
        let eventSlotID = '';

        data.relatedEvents && data.relatedEvents.map(events => {

            let splitDate = dateSplitter(events.eventDateTimeSlot.eventStartTime);
            let date = getDateFromISO(splitDate);

            let splitTime = timeSplitter(events.eventDateTimeSlot.eventStartTime);
            let thisTime = getTimeFromISO("2019-07-21T" + splitTime);

            if (this.state.date === date && time === thisTime) {
                eventSlotID = events.eventSlotId
            }

        });

        return eventSlotID

    };

    onDivClick = (time) => {

        for (let i = 0; i < this.state.timeArray && this.state.timeArray.length; i++) {
            if (time && time === this.state.timeArray[i].time) {
                this.state.timeArray[i].click = true
            }
        }

        let eventSlotID = this.getSlotID(time);
        this.props.history.push(`/event/detail/${eventSlotID}`)
    };


    getDateTime = (date, time, dateList, timeList) => {

        let dateTimeArray = [];
        let timeArray = [];

        timeArray.push({
            time: time,
            click: false
        });

        _.forEach(timeList, (item) => {
            timeArray.push({
                time: item,
                click: false
            });
        });

        dateTimeArray.push({
            date: date,
            time: _.sortBy(timeArray, (o) => o.time),
        });

        for (let i = 0; i < dateList && dateList.length; i++) {
            dateTimeArray.push({
                date: getDateFromISO(dateList[i]),
                time: timeArray,
            });
        }
        return dateTimeArray;
    };

    getTimeArray = (dateTimeArray, date, time) => {
        let timeArrayForSpecificDate = [];
        if (typeof dateTimeArray !== 'undefined') {
            for (let i = 0; i < dateTimeArray.length; i++) {
                if (date && date === dateTimeArray[i].date) {
                    for (let j = 0; j < dateTimeArray[i].time.length; j++) {

                        timeArrayForSpecificDate.push(dateTimeArray[i].time[j]);

                        if (time && time === dateTimeArray[i].time[j].time) {
                            dateTimeArray[i].time[j].click = true
                        }
                    }

                }
            }
        }

        if (this.state.timeArray.length === 0) {
            this.setState({timeArray: _.uniqBy(timeArrayForSpecificDate, 'time')})
        }
    };

    getTimeForDate = (array, date) => {
        let timeForDate = [];
        if (typeof array !== 'undefined') {
            for (let i = 0; i < array.length; i++) {
                let dateInArray = dateSplitter(array[i].eventDateTimeSlot.eventStartTime);
                if (date && date === dateInArray) {
                    timeForDate.push(getTimeFromISO(array[i].eventDateTimeSlot.eventStartTime))
                }
            }
        }
        return timeForDate
    };


    getImages = () => {
        const event = this.props.singleEventDetail;
        const arrangeImages = [];
        if (event) {
            _.forEach(event.eventImagesKeys && event.eventImagesKeys, (item) => {
                if (item) {
                    if (item.hasOwnProperty("imageUrl")) {
                        arrangeImages.push({
                            src: item.imageUrl ? item.imageUrl : '',
                            thumbnail: item.imageUrl ? item.imageUrl : '',
                            caption: event.eventTitle ? event.eventTitle : '',
                            tags: getTags(event.parentEventInfo && event.parentEventInfo.tags)
                        });
                    }
                }
            });
        }

        return arrangeImages;
    };

    getForm = () => {

        const wishListToggle = (eventSlotId) => {
            if (this.props.auth) {
                this.props.wishListToggle(eventSlotId);
            }
        };

        if (!this.props.processing && this.props.singleEventDetail) {

            let data = this.props.singleEventDetail;
            let eventStartTime = data && data.eventDateTimeSlot ? data.eventDateTimeSlot.eventStartTime : ' ';

            const {venue} = this.props.singleEventDetail;
            shareUrl = window.location.protocol + '//' + window.location.host + '/event/detail/' + data.eventSlotId;

            let uniqueDatesForDropDown = [];


            const allEvents = this.props.allEvents;

            if(allEvents) {
                if (allEvents.hasOwnProperty("data") && allEvents.data.hasOwnProperty("data")) {

                    if (this.props.singleEventDetail) {
                        allEvents.data.data.forEach(event => {
                            if (event._id === this.props.singleEventDetail._id) {
                                return eventsArray;
                            } else {
                                return eventsArray.push(event)
                            }
                        });
                    }
                }
            }


            let uniqueDates = this.getUniqueDates(data.relatedEvents);
            for (let i = 0; i < uniqueDates.length; i++) {
                if (uniqueDates[i] !== dateSplitter(eventStartTime)) {
                    uniqueDatesForDropDown.push(uniqueDates[i])
                }
            }

            let timeForDate = this.getTimeForDate(data && data.relatedEvents, dateSplitter(eventStartTime));

            dateTimeArray = this.getDateTime(getDateFromISO(eventStartTime),
                getTimeFromISO(eventStartTime),
                uniqueDatesForDropDown, timeForDate);

            this.getTimeArray(dateTimeArray, getDateFromISO(eventStartTime), getTimeFromISO(eventStartTime));

            document.title = "Ticket Lake - Event - " + data.eventTitle;
            let bannerImage = data.bannerImageKey && data.bannerImageKey.imageUrl ? data.bannerImageKey.imageUrl : window.location.origin + '/images/banner_1.png';

            if (this.props.auth) {
                isWishlist = this.props.wishLists && this.props.wishLists !== '' && this.props.wishLists.includes(data.eventSlotId);
            }

            return (
                <div id="main">
                    <Header/>
                    <Modal isOpen={this.state.activeModal === data._id} toggle={this.sharingSocial}
                           className={this.props.className}>
                        <ModalHeader toggle={this.sharingSocial}>{data.eventTitle}</ModalHeader>
                        <ModalBody>
                            <h3>Share The Event!!</h3>
                            <Row>
                                <Col md={{size: 2, offset: 4}}>
                                    <FacebookShareButton
                                        url={shareUrl}
                                        quote={data.eventTitle}
                                        className="Demo__some-network__share-button">
                                        <FacebookIcon
                                            size={64}
                                            round/>
                                    </FacebookShareButton>
                                </Col>
                                <Col md={{size: 2}}>
                                    <TwitterShareButton
                                        url={shareUrl}
                                        title={data.eventTitle}
                                        className="Demo__some-network__share-button">
                                        <TwitterIcon
                                            size={64}
                                            round/>
                                    </TwitterShareButton>
                                </Col>
                            </Row>


                        </ModalBody>
                    </Modal>
                    <div id="wrapper">
                        <div className="content">
                            <section className="list-single-hero" data-scrollax-parent="true" id="sec1">
                                <div className="bg par-elem" style={{
                                    float: 'left',
                                    backgroundImage: "url('" + bannerImage + "')",
                                    translateY: '30%'
                                }}/>
                                <div className="list-single-hero-title fl-wrap">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-md-7">
                                                <div className="listing-rating-wrap">
                                                    <div className="listing-rating card-popup-rainingvis"
                                                         data-starrating2={5}/>
                                                </div>
                                                <h2>
                                                    <span>{data.eventTitle ? data.eventTitle : 'Event Title'}</span>
                                                </h2>
                                                <div className="list-single-header-contacts fl-wrap">
                                                    <ul>
                                                        <li><i className="fa fa-address-book"/><a
                                                            href="#">{data.organizationName ? data.organizationName : 'Organizer Name'}</a>
                                                        </li>
                                                        {data.parentEventInfo && data.parentEventInfo.contactPersonInfo.phoneNumber.length === 0 ? null :
                                                            <li><i className="fa fa-phone"/><a
                                                                href="#">{data.parentEventInfo ? data.parentEventInfo.contactPersonInfo.phoneNumber[0] : 'Organizer Name'}</a>
                                                            </li>
                                                        }
                                                        <li><i className="fa fa-envelope"/><a
                                                            href="#">{data.parentEventInfo ? data.parentEventInfo.contactPersonInfo.email : 'Email'}</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <BreadcrumbsItem glyph='home' to='/'>Home Page</BreadcrumbsItem>
                                        <BreadcrumbsItem to='/events/listing'>All Events</BreadcrumbsItem>
                                        <BreadcrumbsItem
                                            to={'/event/detail/' + this.props.match.params.id}>{data.eventTitle ? data.eventTitle : 'Event Title'}</BreadcrumbsItem>

                                        <div className="breadcrumbs-hero-buttom fl-wrap">
                                            <div className="breadcrumbs">
                                                <Breadcrumbs
                                                    item={NavLink}
                                                    finalItem={'span'}
                                                    finalProps={{
                                                        style: {color: 'red'}
                                                    }}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section className="grey-blue-bg small-padding scroll-nav-container" id="sec2">
                                <div className="scroll-nav-wrapper fl-wrap">
                                    <div className="hidden-map-container fl-wrap">
                                        <input id="pac-input" className="controls fl-wrap controls-mapwn" type="text"
                                               placeholder="What Nearby ?   Bar , Gym , Restaurant "/>
                                        <div className="map-container">
                                            <div id="singleMap" data-latitude="40.7427837"
                                                 data-longitude="-73.11445617675781"/>
                                        </div>
                                    </div>
                                    <div className="clearfix"/>
                                    <div className="container">
                                        <nav className="scroll-nav scroll-init">
                                            <ul>
                                                <li><a className="act-scrlink" href="#sec1">Details</a></li>
                                                {
                                                    this.state.labelArray && this.state.labelArray.length > 0 ?
                                                        this.state.labelArray.map((list, i) => {
                                                            return (
                                                                <li><a className="act-scrlink"
                                                                       style={{
                                                                           backgroundColor: "#F2F3F8",
                                                                           color: 'black',
                                                                           textTransform: 'capitalize'
                                                                       }}
                                                                       href="#sec1">{list.label}</a></li>
                                                            )
                                                        }) : null
                                                }
                                            </ul>
                                        </nav>
                                    </div>
                                </div>

                                <div className="container">
                                    <div className="row">
                                        <div className="col-lg-8 col-md-12 col-sm-12">
                                            <div className="list-single-main-container " style={{paddingLeft: "0px"}}>
                                                <Gallery
                                                    images={this.getImages()}
                                                    enableImageSelection={false}
                                                />
                                                <div className={'clearfix'} style={{marginBottom: '30px'}}/>
                                                <div className="list-single-main-item fl-wrap">
                                                    <div className="row small-display-table">
                                                        <div className={"col-md-9"}>
                                                            <div className="list-single-main-item-title fl-wrap">
                                                                <div className="row">
                                                                    <div className='col-md-12'>
                                                                        <div className="row"
                                                                             style={{marginRight: "-90px"}}>
                                                                            <div
                                                                                className={"col-sm-3 col-md-4 c-class"}>
                                                                                <h3><i
                                                                                    className="far fa-calendar-alt mr-2"
                                                                                    style={{
                                                                                        fontSize: '22px',
                                                                                        color: 'red'
                                                                                    }}/>
                                                                                    Date </h3>
                                                                            </div>
                                                                            <div className={"col-sm-9 col-md-8"}
                                                                                 style={{width: 'auto'}}>
                                                                                <p style={{fontSize: '18px'}}>{data.eventDateTimeSlot ? getDateFromISO(data.eventDateTimeSlot.eventStartTime) : 'Date'}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className='col-md-12'>
                                                                        <div className="row">
                                                                            <div
                                                                                className={"col-sm-3 col-md-4 c-class"}>
                                                                                <h3><i className="far fa-clock mr-2"
                                                                                       style={{
                                                                                           fontSize: '22px',
                                                                                           color: 'red'
                                                                                       }}/> Time </h3>
                                                                            </div>
                                                                            <div className={"col-sm-9 col-md-8"}
                                                                                 style={{width: 'auto'}}>
                                                                                <p style={{fontSize: '18px'}}>{data.eventDateTimeSlot ? getTimeFromISO(data.eventDateTimeSlot.eventStartTime) : 'Time'}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="row">
                                                                    <div className='col-md-12'>
                                                                        <div className="row">
                                                                            <div
                                                                                className={"col-sm-3 col-md-4 c-class"}>
                                                                                <h3><i
                                                                                    className="fa fa-map-marker-alt mr-2"
                                                                                    style={{
                                                                                        fontSize: '22px',
                                                                                        color: 'red'
                                                                                    }}/> Venue </h3>
                                                                            </div>
                                                                            <div className={"col-sm-9 col-md-8"}
                                                                                 style={{width: 'auto'}}>
                                                                                <p style={{fontSize: '18px'}}>{data.venue ? data.venue.address : 'Address'}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className={"col-md-3"}>
                                                            <div
                                                                style={{width: 'auto'}}>
                                                                {(this.props.auth) ? (
                                                                    (isWishlist === true) ?
                                                                        (
                                                                            <img
                                                                                src={'/images/fav-selected.svg'}
                                                                                onClick={() => wishListToggle(data.eventSlotId)}
                                                                                className={"heart-icon"}
                                                                                alt={"image"}/>
                                                                        ) :
                                                                        (
                                                                            <img src={'/images/fav.svg'}
                                                                                 onClick={() => wishListToggle(data.eventSlotId)}
                                                                                 className={"heart-icon"}
                                                                                 alt={"image"}/>
                                                                        )
                                                                ) : null}

                                                                <img src={'/images/share.svg'}
                                                                     onClick={() => this.sharingSocial(data._id)}
                                                                     alt={"image"}/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <p style={{
                                                        marginTop: "18px",
                                                        marginBottom: "initial"
                                                    }}>{data.parentEventInfo ? data.parentEventInfo.description : "Description"}</p>
                                                </div>

                                                {
                                                    data.agenda ?
                                                        <div className="list-single-main-item fl-wrap">
                                                            <div className="row">
                                                                <div className="col-md-12">
                                                                    <div className="box-widget-item-header">
                                                                        <h3> Agenda </h3>
                                                                    </div>
                                                                    <p>{data.agenda ? data.agenda : "Agenda"}</p>
                                                                </div>
                                                            </div>
                                                        </div> : null
                                                }

                                                {
                                                    this.getGroupBySections(data.sections && data.sections, false)

                                                }
                                            </div>
                                        </div>

                                        <div className="col-lg-4 col-md-12 col-sm-12">
                                            <div className="box-widget-wrap">
                                                {
                                                    data.parentEventInfo && data.parentEventInfo.eventType === `${RECUR}` ?
                                                        <div className="box-widget-item fl-wrap">
                                                            <div className="box-widget">
                                                                <div className="box-widget-content">

                                                                    <div className="box-widget-item-header">
                                                                        <h3> Purchase Tickets</h3>
                                                                    </div>

                                                                    <div className="cal-item">
                                                                        <div className="col-list-search-input-item ">
                                                                            <label className={"newLabel"}>Date</label>
                                                                            <select data-placeholder="Date"
                                                                                    name="date"
                                                                                    onChange={(e) => this.onDateChange(e)}
                                                                                    className="chosen-select no-search-select date-select-dropDown dropDown-border">
                                                                                <option selected>
                                                                                    {this.state.date ? this.state.date : 'Date'}
                                                                                </option>
                                                                                {
                                                                                    uniqueDatesForDropDown && uniqueDatesForDropDown.map(date => {
                                                                                        return (
                                                                                            <option>{getDateFromISO(date)}</option>
                                                                                        )
                                                                                    })
                                                                                }

                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div className="cal-item">
                                                                        <div className="col-list-search-input-item ">
                                                                            <div className={"row"}>
                                                                                <div className={"col-md-12"}>
                                                                                    <label className={"newLabel"}>Time
                                                                                        Slot</label>
                                                                                </div>
                                                                                <div className={"col-md-12"}
                                                                                     style={{cursor: 'pointer'}}>

                                                                                    <div className={"row"}>
                                                                                        {
                                                                                            this.state.timeArray && this.state.timeArray.map(timeArray => {
                                                                                                return (
                                                                                                    <div
                                                                                                        className={"col-md-3 col-sm-4 col-lg-6 col-xl-3"}
                                                                                                        style={{marginTop: '8px'}}>

                                                                                                        {
                                                                                                            timeArray.click === true ?
                                                                                                                <span
                                                                                                                    onClick={() => this.onDivClick(timeArray.time)}
                                                                                                                    className={"badge badge-light-click"}>
                                                                                                                    {timeArray.time}
                                                                                                                 </span> : null
                                                                                                        }

                                                                                                        {
                                                                                                            timeArray.click === false ?
                                                                                                                <span
                                                                                                                    className={"badge badge-light"}
                                                                                                                    onClick={() => this.onDivClick(timeArray.time)}
                                                                                                                >
                                                                                                                    {timeArray.time}
                                                                                                                 </span> : null
                                                                                                        }


                                                                                                    </div>
                                                                                                )
                                                                                            })
                                                                                        }

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <NavLink to={`/buy-ticket/${data.eventSlotId}`}
                                                                             className="btn flat-btn color-bg big-btn float-btn image-popup"
                                                                             style={{
                                                                                 width: '100%',
                                                                                 textAlign: 'center'
                                                                             }}>
                                                                        Buy Ticket <i className="fa fa-play"/>
                                                                    </NavLink>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        :
                                                        <div className="box-widget-item fl-wrap">
                                                            <div className="box-widget">
                                                                <div className="box-widget-content">
                                                                    <NavLink to={`/buy-ticket/${data.eventSlotId}`}
                                                                             className="btn flat-btn color-bg big-btn float-btn image-popup"
                                                                             style={{
                                                                                 width: '100%',
                                                                                 textAlign: 'center'
                                                                             }}>
                                                                        Buy Ticket <i className="fa fa-play"/>
                                                                    </NavLink>
                                                                </div>
                                                            </div>
                                                        </div>
                                                }

                                                <div className="box-widget-item fl-wrap">
                                                    <div className="box-widget" style={{minHeight: '510px'}}>
                                                        <div className="box-widget-content"
                                                             style={{minHeight: '450px'}}>

                                                            <MapContainer
                                                                latitude={venue && venue.latitude !== '' && typeof venue.latitude !== 'undefined' ? venue.latitude : -1.2342}
                                                                longitude={venue && venue.longitude !== '' && typeof venue.longitude !== 'undefined' ? venue.longitude : 38.2883}/>

                                                        </div>
                                                    </div>
                                                </div>


                                                <div className="box-widget-item fl-wrap">
                                                    <div className="box-widget">
                                                        <div className="box-widget-content">
                                                            <div className="box-widget-item-header">
                                                                <h3> Contact Information</h3>
                                                            </div>
                                                            <div className="box-widget-list">
                                                                <ul>
                                                                    <li><span><i
                                                                        className="fa fa-address-book"/> Name :</span>
                                                                        <a href="#">{data.organizationName ? data.organizationName : 'Organizer Name'}</a>
                                                                    </li>

                                                                    {data.parentEventInfo && data.parentEventInfo.contactPersonInfo.phoneNumber.length === 0 ? null :
                                                                        <li><span><i
                                                                            className="fa fa-phone"/> Phone :</span>
                                                                            <a href="#">{data.parentEventInfo ?
                                                                                data.parentEventInfo.contactPersonInfo && data.parentEventInfo.contactPersonInfo.phoneNumber[0]
                                                                                : 'Organizer Phone Number'}</a>
                                                                        </li>
                                                                    }
                                                                    <li><span><i
                                                                        className="fa fa-envelope"/> Email :</span> <a
                                                                        href="#">{data.parentEventInfo ?
                                                                        data.parentEventInfo.contactPersonInfo && data.parentEventInfo.contactPersonInfo.email : 'Organizer Email'}</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div className="list-widget-social">
                                                                <ul>
                                                                    <li><a href="#" target="_blank"><i
                                                                        className="fab fa-facebook-f"/></a></li>
                                                                    <li><a href="#" target="_blank"><i
                                                                        className="fab fa-twitter"/></a></li>
                                                                    <li><a href="#" target="_blank"><i
                                                                        className="fab fa-vk"/></a></li>
                                                                    <li><a href="#" target="_blank"><i
                                                                        className="fab fa-instagram"/></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {
                                    data.parentEventInfo && data.parentEventInfo.eventType === `${SERIES}` ?
                                        data.relatedEvents && data.relatedEvents.length > 0 ?
                                            this.getRelatedEvents(data.relatedEvents, data._id) : null
                                        :
                                        data.parentEventInfo && data.parentEventInfo.eventType === `${STANDARD_EVENT}` ?
                                            eventsArray && eventsArray.length > 0 ?
                                                (<SuggestedEventSlider heading={"Similar Events"}
                                                                       text={"Explore Some More Events related to this Category"}
                                                                       cards={eventsArray}/>) : null
                                            :
                                            data.parentEventInfo && data.parentEventInfo.eventType === `${RECUR}` ?
                                                data.relatedEvents && data.relatedEvents.length > 0 ?
                                                    this.getRelatedEvents(data.relatedEvents, data._id) : null
                                                :
                                                null

                                }
                            </section>
                        </div>
                        <div className="limit-box fl-wrap"/>
                    </div>
                    <Footer/>
                </div>
            )
        }
        return null;
    };

    getLoader = () => {
        return null;
    };


    render() {
        if (this.props.processing) {
            return (
                <div id="main">
                    <Header/>
                    <div id="wrapper">

                        <div className="content">
                            <Loader style={{marginBottom: "20%"}}/>
                        </div>
                    </div>
                    <Footer/>
                </div>
            );
        } else {

            if (this.props.error) {
                return (
                    <Error/>
                )
            } else {
                return (
                    <>
                        {this.getForm()}
                    </>
                );
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        singleEventDetail: state.event.singleEventDetail,
        wishLists: state.wishlist.wishListIds,
        allEvents: state.event.allEvents,
        processing: state.event.processing,
        auth: state.user.authenticated,
        error: state.event.error,
        errorMessage: state.event.errorMessage
    }
};
const connectedComponent = connect(mapStateToProps, {
    getEventDetail,
    wishListToggle,
    getWishListIdsFromApi,
    getAllEventsDefault
})(EventDetail);
export default withRouter(connectedComponent);