// Library
import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
// Redux
import {connect} from 'react-redux';
// Component
import {login, errorHandling} from '../../redux/user/user-actions';
import Loader from "../../commonComponents/loader";

class SignIn extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            name: ''
        };
    }

    onButtonClick = () => {
        this.props.history.push('/authentication/forgot-password');
        this.props.errorHandling(false,'');
    };

    onInputChange = (e) => {
        let state = {...this.state};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    checkAuth = () => {

        if (this.props.authenticated) {
            if (this.props.redirectTo !== null) {
                this.props.history.push(this.props.redirectTo);
            } else if(sessionStorage.getItem('redirectTo')){
                let redirectTo = sessionStorage.getItem('redirectTo');
                sessionStorage.removeItem('redirectTo');
                this.props.history.push(redirectTo);
            } else {
                this.props.history.push('/');
            }
        }
    };
    onFormSubmit = (e) => {
        e.preventDefault();
        this.props.login(this.state.email, this.state.password);
    };

    getForm = () => {

        return (
            <div id="tab-1" className="tab-content" style={{display: 'block'}}>
                <h3>Sign In <span>Ticket<strong>Lake</strong></span></h3>

                {this.props.error ? <p style={{color: 'red'}}>{this.props.errorMessage}</p> : null}

                <div className="custom-form">
                    <form method="post" name="registerform">
                        <label>Username or Email Address <span>*</span> </label>

                        <input name="email" type="email" placeholder="Email" value={this.state.email} required
                               onChange={this.onInputChange}/>

                        <label>Password <span>*</span> </label>

                        <input name="password" type="password" placeholder="Password" value={this.state.password}
                               required
                               onChange={this.onInputChange}/>

                        <button type="submit" className="log-submit-btn" onClick={this.onFormSubmit}><span>Log In</span>
                        </button>

                        <div className="clearfix"/>

                        {/*<div className="filter-tags">*/}
                            {/*<input id="check-a" type="checkbo*/}
                            {/*x" name="check"/>*/}
                            {/*<label htmlFor="check-a">Remember me</label>*/}
                        {/*</div>*/}

                    </form>
                    <div className="lost_password">
                        <a href="#" onClick={this.onButtonClick}>Lost Your
                            Password?</a>
                    </div>
                </div>
            </div>
        )
    };

    getLoader = () => {
        if (this.props.processing)
            return (
                <Loader/>
            );

        return null;
    };

    render() {
        return (
            <div>
                {this.checkAuth()}
                {this.getLoader()}
                {this.getForm()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        authenticated: state.user.authenticated,
        processing: state.user.processing,
        error: state.user.error,
        errorMessage: state.user.message,
        redirectTo: state.user.redirectTo
    }
};
const connectedComponent = connect(mapStateToProps, {login, errorHandling})(SignIn);
export default withRouter(connectedComponent);
