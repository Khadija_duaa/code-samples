// Library
import React, {Component} from 'react';
// Component
import CardSlider from '../../commonComponents/cardSlider';
import TwoShadedButton from '../../commonComponents/twoShadedButton';
import HeroBanner from '../../commonComponents/heroBanner';
import FullImageCard from '../../commonComponents/fullImageCard';
import {getAllCategories} from "../../redux/category/category-actions";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
// Settings
const sliderSettings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    initialSlide: 3,
    autoplay: false,
    autoplaySpeed: 2000,
    cssEase: "linear",
    focusOnSelect: true,
    className: "center",
    centerMode: false,
    centerPadding: "100px",
    arrows: false,
};
// Buttons
const buttonSettings = {
    display: true,
    mainPreviousClass: 'fc-cont  lc-prev',
    previousIcon: 'fa fa-arrow-left',
    mainNextClass: 'fc-cont  lc-next',
    nextIcon: 'fa fa-arrow-right'
};

class UpcomingEvents extends Component {

    componentDidMount() {
        this.props.getAllCategories();
    }

    render() {
        // console.log("Categories in componnets", this.props.categories);
        const cardDiplay = this.props.categories.map((item,i) => (
            // i && i<3 ?
            <div className={"col-md-12"}>
                <FullImageCard
                    items={item.name}
                    tags={item.tags}
                    key={i}
                    cardImage={item.imageKey && item.imageKey.imageUrl}
                    cardLink={"/events/listing?category=" + item._id}
                />
            </div> //: null
        ));

        return (
            <HeroBanner
                backgroundImage={window.location.origin + '/images/upcoming_events.png'}
                transformStyle={'translateZ(0px) translateY(-38.1485px)'}>
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="colomn-text fl-wrap pad-top-column-text_small">
                                <div className="colomn-text-title">
                                    <h3>Explore Categories</h3>
                                    <p>Explore some of the best categories from ticketlake.</p>
                                    <TwoShadedButton
                                        buttonLink={'/events/listing'}
                                        buttonText={'View All Categories'}
                                        float={true}
                                    />

                                </div>
                            </div>
                        </div>
                        <div className="col-md-8">
                            {/*<div className={"row"}>*/}
                            {/*    {cardDiplay}*/}
                            {/*</div>*/}

                            <div className="light-carousel-wrap fl-wrap">
                                <div className="light-carousel">
                                    <CardSlider settings={sliderSettings} buttons={buttonSettings}>
                                        {cardDiplay}
                                    </CardSlider>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </HeroBanner>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        categories: state.category.categories
    }
};

const connectedComponent = connect(mapStateToProps, {getAllCategories})(UpcomingEvents);
export default withRouter(connectedComponent);
