// Library
import React, {Component} from 'react';
// Components
import SearchForEvents from '../searchForEvents';
import PromotedEvents from '../promotedEvents';
import RecentlyAddedEvents from '../recentlyAddedEvents';
import UpcomingEvents from '../upcomingEvents';
import WhyChooseUs from '../whyChooseUs';
import Section from '../homePageSections';
import MobileAppBanner from '../mobileAppBanner';
import EventOrganizerBanner from '../eventOrganizerBanner';
import Loader from "../../commonComponents/loader";
// Redux
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {getWishListIdsFromApi} from '../../redux/wishlist/wishlist-actions';


class Main extends Component {
    componentDidMount() {
        document.title = "Ticket Lake - Welcome";
        if (this.props.auth) {
            this.props.getWishListIdsFromApi();
        }
    }

    renderHomePage = () => {
        return (
            <div id="wrapper">

                <div className="content">
                    <SearchForEvents/>
                    {/*<Section/>*/}
                    <PromotedEvents/>
                    <RecentlyAddedEvents heading={"Upcoming Events"}
                                         text={"Explore some of the best tips from around the city from our partners and friends."}/>
                    <UpcomingEvents/>
                    <WhyChooseUs/>
                    <MobileAppBanner/>
                </div>
            </div>
        )
    };

    render() {
        if (this.props.auth) {
            if (this.props.processing) {
                return (
                    <Loader/>
                )
            } else {
                return (
                    <>
                        {this.renderHomePage()}
                    </>
                )
            }
        } else {
            return (
                <>
                    {this.renderHomePage()}
                </>
            )
        }

    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.wishlist.processing,
        auth: state.user.authenticated
    }
};

const connectedComponent = connect(mapStateToProps, {getWishListIdsFromApi})(Main);
export default withRouter(connectedComponent);