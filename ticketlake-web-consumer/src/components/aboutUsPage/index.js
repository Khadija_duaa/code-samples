// Library
import React, {Component} from 'react';
// Component
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';

class AboutUsPage extends Component {
    render() {
        return (
            <div id="main">
                <Header/>
                <div id="wrapper">
                    {/* content*/}
                    <div className="content">
                        {/*  section  */}
                        <section className="parallax-section single-par" data-scrollax-parent="true">
                            <div className="bg par-elem " data-bg="images/bg/1.jpg"
                                 data-scrollax="properties: { translateY: '30%' }"/>
                            <div className="overlay"/>
                            <div className="container">
                                <div className="section-title center-align big-title">
                                    <div className="section-title-separator"><span/></div>
                                    <h2><span>About Our Company</span></h2>
                                    <span className="section-separator"/>
                                    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec tincidunt arcu,
                                        sit amet fermentum sem.</h4>
                                </div>
                            </div>
                            <div className="header-sec-link">
                                <div className="container"><a href="#sec1" className="custom-scroll-link color-bg"><i
                                    className="fal fa-angle-double-down"/></a></div>
                            </div>
                        </section>
                        {/*  section  end*/}
                        <div className="breadcrumbs-fs fl-wrap">
                            <div className="container">
                                <div className="breadcrumbs fl-wrap"><a href="#">Home</a><a href="#">Pages</a><span>About Us</span>
                                </div>
                            </div>
                        </div>
                        <section id="sec1" className="middle-padding">
                            <div className="container">
                                {/*about-wrap */}
                                <div className="about-wrap">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="video-box fl-wrap">
                                                <img src="images/all/1.jpg" className="respimg" alt/>
                                                <a className="video-box-btn image-popup"
                                                   href="https://vimeo.com/264074381"><i className="fa fa-play"
                                                                                         aria-hidden="true"/></a>
                                                <span className="video-box-title">Our Video Presentation</span>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="list-single-main-item-title fl-wrap">
                                                <h3>Our Awesome <span>Story</span></h3>
                                            </div>
                                            <p>Ut euismod ultricies sollicitudin. Curabitur sed dapibus nulla. Nulla
                                                eget iaculis lectus. Mauris ac maximus neque. Nam in mauris quis libero
                                                sodales eleifend. Morbi varius, nulla sit amet rutrum elementum, est
                                                elit finibus tellus, ut tristique elit risus at metus.</p>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in
                                                pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur
                                                nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi
                                                tincidunt. Curabitur convallis fringilla diam sed aliquam. Sed tempor
                                                iaculis massa faucibus feugiat. In fermentum facilisis massa, a
                                                consequat purus viverra. Aliquam erat volutpat. Curabitur convallis
                                                fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat.
                                                In fermentum facilisis massa, a consequat purus viverra.
                                            </p>
                                            <a href="#sec2" className="btn  color-bg float-btn custom-scroll-link">View
                                                Our Team <i className="fal fa-users"/></a>
                                        </div>
                                    </div>
                                </div>
                                {/* about-wrap end  */}
                                <div className=" single-facts fl-wrap">
                                    {/* inline-facts */}
                                    <div className="inline-facts-wrap">
                                        <div className="inline-facts">
                                            <i className="fal fa-users"/>
                                            <div className="milestone-counter">
                                                <div className="stats animaper">
                                                    <div className="num" data-content={0} data-num={254}>154</div>
                                                </div>
                                            </div>
                                            <h6>New Visiters Every Week</h6>
                                        </div>
                                    </div>
                                    {/* inline-facts end */}
                                    {/* inline-facts  */}
                                    <div className="inline-facts-wrap">
                                        <div className="inline-facts">
                                            <i className="fal fa-thumbs-up"/>
                                            <div className="milestone-counter">
                                                <div className="stats animaper">
                                                    <div className="num" data-content={0} data-num={12168}>12168</div>
                                                </div>
                                            </div>
                                            <h6>Happy customers every year</h6>
                                        </div>
                                    </div>
                                    {/* inline-facts end */}
                                    {/* inline-facts  */}
                                    <div className="inline-facts-wrap">
                                        <div className="inline-facts">
                                            <i className="fal fa-award"/>
                                            <div className="milestone-counter">
                                                <div className="stats animaper">
                                                    <div className="num" data-content={0} data-num={172}>172</div>
                                                </div>
                                            </div>
                                            <h6>Won Awards</h6>
                                        </div>
                                    </div>
                                    {/* inline-facts end */}
                                    {/* inline-facts  */}
                                    <div className="inline-facts-wrap">
                                        <div className="inline-facts">
                                            <i className="fal fa-hotel"/>
                                            <div className="milestone-counter">
                                                <div className="stats animaper">
                                                    <div className="num" data-content={0} data-num={732}>732</div>
                                                </div>
                                            </div>
                                            <h6>New Listing Every Week</h6>
                                        </div>
                                    </div>
                                    {/* inline-facts end */}
                                </div>
                            </div>
                        </section>
                        {/* section end */}
                        {/*section */}
                        <section className="color-bg hidden-section">
                            <div className="wave-bg wave-bg2"/>
                            <div className="container">
                                <div className="section-title">
                                    <h2>Why Choose Us</h2>
                                    <span className="section-separator"/>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar
                                        neque. Nulla finibus lobortis pulvinar.</p>
                                </div>
                                {/* */}
                                <div className="row">
                                    <div className="col-md-4">
                                        {/* process-item*/}
                                        <div className="process-item big-pad-pr-item">
                                            <span className="process-count"> </span>
                                            <div className="time-line-icon"><i className="fal fa-headset"/></div>
                                            <h4><a href="#"> 24 Hours Support</a></h4>
                                            <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus,
                                                finibus tellus ut, convallis eros sollicitudin turpis.</p>
                                        </div>
                                        {/* process-item end */}
                                    </div>
                                    <div className="col-md-4">
                                        {/* process-item*/}
                                        <div className="process-item big-pad-pr-item">
                                            <span className="process-count"> </span>
                                            <div className="time-line-icon"><i className="fal fa-cogs"/></div>
                                            <h4><a href="#">Admin Panel</a></h4>
                                            <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus,
                                                finibus tellus ut, convallis eros sollicitudin turpis.</p>
                                        </div>
                                        {/* process-item end */}
                                    </div>
                                    <div className="col-md-4">
                                        {/* process-item*/}
                                        <div className="process-item big-pad-pr-item nodecpre">
                                            <span className="process-count"> </span>
                                            <div className="time-line-icon"><i className="fab fa-android"/></div>
                                            <h4><a href="#"> Mobile Friendly</a></h4>
                                            <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus,
                                                finibus tellus ut, convallis eros sollicitudin turpis.</p>
                                        </div>
                                        {/* process-item end */}
                                    </div>
                                </div>
                                {/*process-wrap   end*/}
                            </div>
                        </section>
                        {/* section end */}
                        {/*section */}
                        <section id="sec2">
                            <div className="container">
                                <div className="section-title">
                                    <h2>Our Team</h2>
                                    <span className="section-separator"/>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar
                                        neque. Nulla finibus lobortis pulvinar.</p>
                                </div>
                                <div className="team-holder section-team fl-wrap">
                                    {/* team-item */}
                                    <div className="team-box">
                                        <div className="team-photo">
                                            <img src="images/team/1.jpg" alt className="respimg"/>
                                        </div>
                                        <div className="team-info">
                                            <div className="team-dec"><i className="fal fa-chart-line"/></div>
                                            <h3><a href="#">Alisa Gray</a></h3>
                                            <h4>Business consultant</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                                tempor incididunt ut labore et dolore magna aliqua.</p>
                                            <div className="team-social">
                                                <ul>
                                                    <li><a href="#" target="_blank"><i
                                                        className="fab fa-facebook-f"/></a></li>
                                                    <li><a href="#" target="_blank"><i className="fab fa-twitter"/></a>
                                                    </li>
                                                    <li><a href="#" target="_blank"><i
                                                        className="fab fa-instagram"/></a></li>
                                                    <li><a href="#" target="_blank"><i className="fab fa-vk"/></a></li>
                                                </ul>
                                                <a className="team-contact_link" href="#"><i
                                                    className="fal fa-envelope"/></a>
                                            </div>
                                        </div>
                                    </div>
                                    {/* team-item  end*/}
                                    {/* team-item */}
                                    <div className="team-box">
                                        <div className="team-photo">
                                            <img src="images/team/1.jpg" alt className="respimg"/>
                                        </div>
                                        <div className="team-info">
                                            <div className="team-dec"><i className="fal fa-camera-retro"/></div>
                                            <h3><a href="#">Austin Evon</a></h3>
                                            <h4>Photographer</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                                tempor incididunt ut labore et dolore magna aliqua.</p>
                                            <div className="team-social">
                                                <ul>
                                                    <li><a href="#" target="_blank"><i
                                                        className="fab fa-facebook-f"/></a></li>
                                                    <li><a href="#" target="_blank"><i className="fab fa-twitter"/></a>
                                                    </li>
                                                    <li><a href="#" target="_blank"><i
                                                        className="fab fa-instagram"/></a></li>
                                                    <li><a href="#" target="_blank"><i className="fab fa-vk"/></a></li>
                                                </ul>
                                                <a className="team-contact_link" href="#"><i
                                                    className="fal fa-envelope"/></a>
                                            </div>
                                        </div>
                                    </div>
                                    {/* team-item end  */}
                                    {/* team-item */}
                                    <div className="team-box">
                                        <div className="team-photo">
                                            <img src="images/team/1.jpg" alt className="respimg"/>
                                        </div>
                                        <div className="team-info">
                                            <div className="team-dec"><i className="fal fa-desktop"/></div>
                                            <h3><a href="#">Taylor Roberts</a></h3>
                                            <h4>Co-manager associated</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                                tempor incididunt ut labore et dolore magna aliqua.</p>
                                            <div className="team-social">
                                                <ul>
                                                    <li><a href="#" target="_blank"><i
                                                        className="fab fa-facebook-f"/></a></li>
                                                    <li><a href="#" target="_blank"><i className="fab fa-twitter"/></a>
                                                    </li>
                                                    <li><a href="#" target="_blank"><i
                                                        className="fab fa-instagram"/></a></li>
                                                    <li><a href="#" target="_blank"><i className="fab fa-vk"/></a></li>
                                                </ul>
                                                <a className="team-contact_link" href="#"><i
                                                    className="fal fa-envelope"/></a>
                                            </div>
                                        </div>
                                    </div>
                                    {/* team-item end  */}
                                </div>
                            </div>
                        </section>
                        {/* section end */}
                        <section className="parallax-section" data-scrollax-parent="true">
                            <div className="bg par-elem " data-bg="images/bg/1.jpg"
                                 data-scrollax="properties: { translateY: '30%' }"/>
                            <div className="overlay"/>
                            {/*container*/}
                            <div className="container">
                                <div className="intro-item fl-wrap">
                                    <h2>Need more information</h2>
                                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore.</h3>
                                    <a className="btn  color-bg" href="contacts.html">Get in Touch <i
                                        className="fal fa-envelope"/></a>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div className="container">
                                <div className="section-title">
                                    <h2>Testimonials</h2>
                                    <span className="section-separator"/>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar
                                        neque. Nulla finibus lobortis pulvinar.</p>
                                </div>
                            </div>
                            <div className="clearfix"/>
                            {/*slider-carousel-wrap */}
                            <div className="slider-carousel-wrap text-carousel-wrap fl-wrap">
                                <div className="swiper-button-prev sw-btn"><i className="fa fa-long-arrow-left"/></div>
                                <div className="swiper-button-next sw-btn"><i className="fa fa-long-arrow-right"/></div>
                                <div className="text-carousel single-carousel cur_carousel-slider-container fl-wrap">
                                    {/*slick-item */}
                                    <div className="slick-item">
                                        <div className="text-carousel-item">
                                            <div className="popup-avatar"><img src="images/avatar/1.jpg" alt/></div>
                                            <div className="listing-rating card-popup-rainingvis"
                                                 data-starrating2={5}></div>
                                            <div className="review-owner fl-wrap">Milka Antony
                                                - <span>Happy Client</span></div>
                                            <p> "In ut odio libero, at vulputate urna. Nulla tristique mi a massa
                                                convallis cursus. Nulla eu mi magna. Etiam suscipit commodo gravida.
                                                Lorem ipsum dolor sit amet, conse ctetuer adipiscing elit, sed diam nonu
                                                mmy nibh euismod tincidunt ut laoreet dolore magna aliquam erat."</p>
                                            <a href="#" className="testim-link">Via Facebook</a>
                                        </div>
                                    </div>
                                    {/*slick-item end */}
                                    {/*slick-item */}
                                    <div className="slick-item">
                                        <div className="text-carousel-item">
                                            <div className="popup-avatar"><img src="images/avatar/1.jpg" alt/></div>
                                            <div className="listing-rating card-popup-rainingvis"
                                                 data-starrating2={4}></div>
                                            <div className="review-owner fl-wrap">Milka Antony
                                                - <span>Happy Client</span></div>
                                            <p> "In ut odio libero, at vulputate urna. Nulla tristique mi a massa
                                                convallis cursus. Nulla eu mi magna. Etiam suscipit commodo gravida.
                                                Lorem ipsum dolor sit amet, conse ctetuer adipiscing elit, sed diam nonu
                                                mmy nibh euismod tincidunt ut laoreet dolore magna aliquam erat."</p>
                                            <a href="#" className="testim-link">Via Facebook</a>
                                        </div>
                                    </div>
                                    {/*slick-item end */}
                                    {/*slick-item */}
                                    <div className="slick-item">
                                        <div className="text-carousel-item">
                                            <div className="popup-avatar"><img src="images/avatar/1.jpg" alt/></div>
                                            <div className="listing-rating card-popup-rainingvis"
                                                 data-starrating2={5}></div>
                                            <div className="review-owner fl-wrap">Milka Antony
                                                - <span>Happy Client</span></div>
                                            <p> "In ut odio libero, at vulputate urna. Nulla tristique mi a massa
                                                convallis cursus. Nulla eu mi magna. Etiam suscipit commodo gravida.
                                                Lorem ipsum dolor sit amet, conse ctetuer adipiscing elit, sed diam nonu
                                                mmy nibh euismod tincidunt ut laoreet dolore magna aliquam erat."</p>
                                            <a href="#" className="testim-link">Via Facebook</a>
                                        </div>
                                    </div>
                                    {/*slick-item end */}
                                    {/*slick-item */}
                                    <div className="slick-item">
                                        <div className="text-carousel-item">
                                            <div className="popup-avatar"><img src="images/avatar/1.jpg" alt/></div>
                                            <div className="listing-rating card-popup-rainingvis"
                                                 data-starrating2={5}></div>
                                            <div className="review-owner fl-wrap">Milka Antony
                                                - <span>Happy Client</span></div>
                                            <p> "In ut odio libero, at vulputate urna. Nulla tristique mi a massa
                                                convallis cursus. Nulla eu mi magna. Etiam suscipit commodo gravida.
                                                Lorem ipsum dolor sit amet, conse ctetuer adipiscing elit, sed diam nonu
                                                mmy nibh euismod tincidunt ut laoreet dolore magna aliquam erat."</p>
                                            <a href="#" className="testim-link">Via Facebook</a>
                                        </div>
                                    </div>
                                    {/*slick-item end */}
                                </div>
                            </div>
                            {/*slider-carousel-wrap end*/}
                            <div className="section-decor"/>
                        </section>
                    </div>
                    {/* content end*/}
                </div>
                <Footer/>
            </div>
        );
    }
}

export default AboutUsPage;