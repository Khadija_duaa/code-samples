// Library
import React, {Component} from 'react';
import moment from 'moment';
import queryString from 'query-string';
import ReactPaginate from 'react-paginate';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Row, Col} from 'reactstrap';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import {
    FacebookShareButton,
    TwitterShareButton,
    EmailShareButton,
    FacebookIcon,
    TwitterIcon
} from 'react-share';
// Component
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';
import DefaultCard from '../../commonComponents/defaultCard';
import ResultForHeading from "../../commonComponents/resultForHeading";
import EventListingFilters from '../eventListingFilters';
import Loader from "../../commonComponents/loader";
import Heading from '../../commonComponents/heading';
//redux
import {
    getAllEventsDefault
} from '../../redux/event/event-actions';
import {getWishListIdsFromApi, wishListToggle} from '../../redux/wishlist/wishlist-actions';
// Helpers
import {dateSplitter, getCardDates, getMaxAndMinPrice, getObjectValue} from '../../utils/common-utils';
// Redux Action
import {getAllCategories} from '../../redux/category/category-actions';
// Importing Config
import {GET_CITIES_FOR_FILTERS as city} from '../../utils/config';

let isWishlist = false;

class EventListing extends Component {

    state = {
        storeCategories: null,
        country: '',
        city: '',
        keyword: null,
        allSearchedEvents: [],
        date: [new Date(), new Date()],
        from: null,
        to: null,
        doSearch: false,
        title: '',
        activeModal: '',
        currentPage: 1,
        pageSize: 12

    };



    componentWillMount() {
        const query = queryString.parse(this.props.location.search);
        if (query.category) {
            this.setState({storeCategories: query.category});
        }
        if (query.when) {
            const dates = query.when.split(" ");
            let from = `${dates[0]}T00:00:00.000Z`;
            let to = `${dates[1]}T11:59:59.000Z`;

            let date = [new Date(moment(from).format()), new Date(moment(to).format())];

            this.setState({date: date, from: from, to: to});

        }
        if (query.location) {
            this.setState({city: query.location});
        }

        if (query.keyword) {
            let keyword = query.keyword;
            this.setState({title: keyword, keyword: keyword});
        }

        this.setState({
            currentPage: 1,
            pageSize: 12,
            doSearch: false
        });


    }

    fetchEvents = (
        paginate = true,
        currentPage = this.state.currentPage,
        pageSize = this.state.pageSize,
        to = this.state.to,
        from = this.state.from,
        city = this.state.city,
        keyword = this.state.keyword,
        category = this.state.storeCategories
    ) => {

        let cat = category && (category !== 'All') ? [category] : null;
        this.props.getAllEventsDefault(
            false, // IsFeatured
            true, // isPublished
            false, // isDraft
            cat, //Categories
            to, // To
            from, // From
            (city === 'All') ? null : city, // City
            paginate, // Paginate
            currentPage, // page
            pageSize, // PageSize
            keyword // search
        );
    };

    loadMoreEvents = (e) => {
        let currentPage = e.selected + 1;
        this.setState({currentPage: e.selected + 1}, () => {
                this.fetchEvents(
                    true,
                    this.state.currentPage,
                    this.state.pageSize
                );
            }
        );
    };

    componentDidUpdate() {
        if (this.state.doSearch) {
            let category = null;
            let from = null;
            let to = null;
            let city = null;
            let keyword = null;
            let date = null;

            const query = queryString.parse(this.props.location.search);
            if (query.category) {
                category = query.category;
            }
            if (query.when) {
                const dates = query.when.split(" ");
                from = `${dates[0]}T00:00:00.000Z`;
                to = `${dates[1]}T23:59:59.000Z`;
                date = [new Date(moment(from).format()), new Date(moment(to).format())]
            }
            if (query.location) {
                city = query.location;
            }
            if (query.keyword) {
                keyword = query.keyword;
            }

            this.setState({
                storeCategories: category,
                city: city,
                keyword: keyword,
                date: date,
                from: from,
                to: to,
                doSearch: false,
                title: keyword,
                currentPage: 1
            });

            this.fetchEvents(true, 1, this.state.pageSize, to, from, city, keyword);

        }
    }

    componentDidMount() {
        debugger
        document.title = "Ticket Lake - Events";
        this.props.getAllCategories();
        const query = queryString.parse(this.props.location.search);


        if (this.props.wishLists === null && this.props.auth) {
            this.props.getWishListIdsFromApi();
        }

        this.fetchEvents(true, this.state.currentPage, this.state.pageSize);

    }

    onDateChange = (dateer) => {

        if (dateer) {
            const fromDate = dateer && dateer[0] !== null ? dateer[0] : null;
            const fromDateTime = dateSplitter(fromDate);
            const toDate = dateer && dateer[1] !== null ? dateer[1] : null;
            const toDateTime = dateSplitter(toDate);

            this.setState({dates: encodeURI(fromDateTime + ' ' + toDateTime)});
            this.setState({date: dateer});
        } else {
            this.setState({dates: null});
            this.setState({date: null});
        }

    };

    onCategoryChange = (e) => {
        if (e.target.value === 'All') {
            this.setState({storeCategories: "All"});
        } else {
            this.setState({storeCategories: e.target.value});
        }
    };

    onLocationChange = (e) => {
        if (e.target.value === 'All') {
            this.setState({city: "All"});
        } else {
            this.setState({city: e.target.value});
        }
    };


    handleChange = (event) => {
        this.setState({
            keyword: event.target.value
        })

    };

    sharingSocial = (id) => {
        if (id) {
            this.setState({activeModal: id});
        } else {

            this.setState({activeModal: ''});
        }
    };

    wishListToggleLink = (eventSlotId) => {
        if (this.props.auth) {
            this.props.wishListToggle(eventSlotId);
        }
    };


    handleSearch = () => {
        this.setState({
            pageSize: 12,
            totalPages: 0,
            currentPage: 1,
        });

        console.log("STATE: ", this.state);
        const url = [];

        if (this.state.keyword) {
            url.push("keyword=" + encodeURI(this.state.keyword));
        }

        if (this.state.storeCategories) {
            url.push("category=" + this.state.storeCategories);

        }
        if (this.state.city) {
            url.push("location=" + this.state.city);


        }

        if (this.state.dates) {
            url.push("when=" + this.state.dates);
        }

        this.setState({doSearch: true});

        console.log("URLS:  ", 'events/listing/?' + url.join("&"));
        this.props.history.push('?' + url.join("&"));

    };

    render() {
        // console.log("STATE CHANGED: ", this.state);
        let allEvents = this.props.allEvents && this.props.allEvents.data && this.props.allEvents.data.data;
        let pages = this.props.allEvents && this.props.allEvents.data;
        // console.log(this.props.allEvents);
        // console.log(allEvents);
        // console.log(this.props.allEvents);
        if (pages) {
            if (this.state.totalPages !== pages.totalPages) {
                this.setState({totalPages: pages.totalPages});
            }
        }
        let shareUrl = 'http://google.com/';
        const cardDisplayJSX = Array.isArray(allEvents) && allEvents.map((data, i) => {
            shareUrl = window.location.protocol + '//' + window.location.host + '/event/detail/' + data.eventSlotId;
            if (this.props.auth) {
                isWishlist = this.props.wishLists && this.props.wishLists !== '' && this.props.wishLists.includes(data.eventSlotId);
            }
            return (
                <>
                    <DefaultCard key={i}
                                 gridLg={3}
                                 gridMd={6}
                                 gridSm={12}
                                 auth={this.props.auth}
                                 cardTitle={data.eventTitle}
                                 image={data.bannerImageKey.imageUrl}
                                 cardLink={'#'}
                                 dates={getCardDates(data.eventDateTimeSlot)}
                                 isWishList={isWishlist}
                                 wishlistLink={() => this.wishListToggleLink(data.eventSlotId)}
                                 cardAddress={data.venue ? data.venue.address : ''}
                                 country={data.venue ? data.venue.country : []}
                                 city={data.venue ? data.venue.city : []}
                                 onClick={() => this.props.history.push(`/event/detail/${data.eventSlotId}`)}
                                 buttonText={getMaxAndMinPrice(data)}
                                 buttonLink={`/buy-ticket/${data.eventSlotId}`}
                                 sharing={this.sharingSocial}
                                 id={data._id}
                    />

                    <Modal isOpen={this.state.activeModal === data._id} toggle={this.sharingSocial}
                           className={this.props.className}>
                        <ModalHeader toggle={this.sharingSocial}>{data.eventTitle}</ModalHeader>
                        <ModalBody>
                            <h3>Share The Event!!</h3>
                            <Row>
                                <Col md={{size: 2, offset: 4}}>
                                    <FacebookShareButton
                                        url={shareUrl}
                                        quote={data.eventTitle}
                                        className="Demo__some-network__share-button">
                                        <FacebookIcon
                                            size={64}
                                            round/>
                                    </FacebookShareButton>
                                </Col>
                                <Col md={{size: 2}}>
                                    <TwitterShareButton
                                        url={shareUrl}
                                        title={data.eventTitle}
                                        className="Demo__some-network__share-button">
                                        <TwitterIcon
                                            size={64}
                                            round/>
                                    </TwitterShareButton>
                                </Col>
                            </Row>
                        </ModalBody>
                    </Modal>

                </>
            )
        });

        if (allEvents) {
            return (
                <div>
                    <div id="main" key={1}>
                        <Header/>
                        <div id="wrapper" key={2}>
                            <div className="content">

                                <section id="sec2" style={{paddingTop: '30px', paddingBottom: '10px'}}
                                         className={"light-red-bg"}>
                                    <div style={{width: '88%', paddingLeft: '5%'}}>
                                        <Heading
                                            style={{marginBottom: '0px', textAlign: 'left'}}
                                            heading={"All Events"}
                                            text={"Explore some of the best tips from around the city from our partners and friends!"}/>
                                    </div>
                                </section>

                                <section className="light-red-bg small-padding" id="sec1" style={{paddingTop: '0px'}}>
                                    <div className="container">
                                        <div className="row">

                                            <div className="col-md-12">

                                                <EventListingFilters
                                                    changeCategory={this.onCategoryChange}
                                                    changeCity={this.onLocationChange}
                                                    changeDate={this.onDateChange}
                                                    handleChange={this.handleChange}
                                                    categories={this.props.categories}
                                                    handleSearch={this.handleSearch}
                                                    city={city}
                                                    category={this.state.storeCategories}
                                                    location={this.state.city}
                                                    from={this.state.from}
                                                    to={this.state.to}
                                                    search={this.state.keyword}
                                                    date={this.state.date}
                                                />
                                                {
                                                    (!this.props.processing) ?
                                                        (
                                                            <div className="col-list-wrap fw-col-list-wrap">

                                                                <div className="list-main-wrap fl-wrap card-listing">
                                                                    <ResultForHeading
                                                                        firstText={'Results For'}
                                                                        secondText={this.state.title || 'All'}
                                                                    />

                                                                    {allEvents ?
                                                                        <Row>
                                                                            {cardDisplayJSX}
                                                                        </Row>
                                                                        :
                                                                        'no data'
                                                                    }
                                                                    {
                                                                        allEvents.length > 0 ?
                                                                            this.props.paginateEvents.hasNextPage === true ?
                                                                                <a className="load-more-button" href="#"
                                                                                   onClick={(e) => this.loadMoreEvents(e)}>Load
                                                                                    more
                                                                                    {this.props.paginationProcessing ? (
                                                                                        <i className="fas fa-spinner"/>) : null}
                                                                                </a> : null :
                                                                            <div>No Events</div>
                                                                    }

                                                                </div>

                                                            </div>
                                                        ) :
                                                        (
                                                            <Loader/>
                                                        )
                                                }
                                            </div>
                                        </div>
                                        <div className="row">

                                            <div
                                                className="col-lg-12 float-left">

                                                <div className="d-flex">

                                                    {this.state.totalPages > 1 ? (
                                                        <ReactPaginate
                                                            previousLabel={<i className="fa fa-angle-left"/>}
                                                            nextLabel={<i className="fa fa-angle-right"/>}
                                                            breakLabel={'...'}
                                                            breakClassName={'break-me'}
                                                            pageCount={this.state.totalPages}
                                                            marginPagesDisplayed={2}
                                                            pageRangeDisplayed={5}
                                                            onPageChange={(data) => this.loadMoreEvents(data)}
                                                            containerClassName={'list-inline mx-auto justify-content-center pagination'}
                                                            subContainerClassName={'list-inline-item pages pagination'}
                                                            activeClassName={'active'}
                                                        />
                                                    ) : null}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <Footer/>
                    </div>
                </div>
            )
        } else {
            return (
                <Loader/>
            );
        }

    };
}

const mapStateToProps = (state) => {
    return {
        allEvents: state.event.allEvents,
        search: state.event.search,
        allFilters: state.event.allFilters,
        dateTimeSearch: state.event.dateTimeSearch,
        citySearch: state.event.citySearch,
        city: state.event.city,
        from: state.event.from,
        to: state.event.to,
        categories: state.category.categories,
        paginateEvents: state.event.paginateEvents,
        processing: state.event.processing,
        auth: state.user.authenticated,
        wishLists: state.wishlist.wishListIds,
        paginationProcessing: state.event.paginationProcessing,
    }
};


const connectedComponent = connect(mapStateToProps, {
    getAllEventsDefault,
    getWishListIdsFromApi,
    getAllCategories,
    wishListToggle
})(EventListing);
export default withRouter(connectedComponent);
