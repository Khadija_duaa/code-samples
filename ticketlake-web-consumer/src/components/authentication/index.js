import React, {Component} from 'react';
import Transition from '../../commonComponents/transition';
import SignIn from '../signInPage';
import SignUp from '../signUpPage';

class Authentication extends Component {

    state = {
        signIn: true
    };

    showSignIn = () => {
        this.setState({signIn: true});

    };

    showRegister = () => {
        this.setState({signIn: false});
    };

    getForm = () => this.state.signIn ? <SignIn/> : <SignUp/>;

    getFormNav = () => {
        return (
            <ul className="tabs-menu">
                <li className={this.state.signIn ? 'current' : ''} onClick={this.showSignIn}><a href="#tab-1"><i
                    className="fa fa-sign-in-alt"/> Login</a></li>
                <li className={this.state.signIn ? '' : 'current'} onClick={this.showRegister}><a href="#tab-2"><i
                    className="fa fa-user-plus"/> Register</a></li>
            </ul>
        );
    };

    render() {
        return (
            <Transition>
                <div className="main-register-wrap modal" style={{display: 'block', backgroundImage: "url('/images/BG.png')"}}>
                    <div className="reg-overlay"/>
                    <div className="main-register-holder">
                        <div className="main-register fl-wrap">
                            <div className="close-reg color-bg" onClick={() => this.props.history.push('/')}><i
                                className="fa fa-times"/></div>
                            {this.getFormNav()}
                            <div id="tabs-container">
                                <div className="tab">
                                    {this.getForm()}
                                </div>

                                {/*<div className="log-separator fl-wrap"><span>or</span></div>*/}
                                {/*<div className="soc-log fl-wrap">*/}
                                    {/*<p>For faster login or register use your social account.</p>*/}
                                    {/*<a href="#" className="facebook-log"><i className="fab fa-facebook-f"/>Connect*/}
                                        {/*with Facebook</a>*/}
                                {/*</div>*/}

                            </div>
                        </div>
                    </div>
                </div>
            </Transition>
        )
    }
}

export default Authentication;