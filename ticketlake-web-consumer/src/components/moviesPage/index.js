// Library
import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
// Components
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';
import Loader from "../../commonComponents/loader/";
import Error from '../../commonComponents/error';
import BannerComponent from '../../commonComponents/bannerComponent';
import MoviePageSection from '../../components/moviePageSection';
import CardLeftAlign from '../../commonComponents/cardLeftAlign';
// Redux
import {connect} from "react-redux";

class Movies extends Component {

    getForm = () => {
        return (
            <div id="main">
                <Header/>
                <div id="wrapper">
                    <div className="content">
                        <BannerComponent heading={"Movies Tickets for You!"}
                                         para={"Over 20 Categories and 540 Movies screening right now."}
                                         image={"/images/banner/cover-movies.png"}/>
                    </div>
                    <MoviePageSection/>
                    <CardLeftAlign heading={"Promoted Movies"}
                                   text={"Explore great range of movies available on Ticketlake!"}/>
                    <CardLeftAlign heading={"Currently Trending Movies"}
                                   text={"Explore great range of movies available on Ticketlake!"}/>
                    <CardLeftAlign heading={"Coming Soon"}
                                   text={"Explore great range of movies available on Ticketlake!"}/>
                </div>
                <Footer/>
            </div>
        )
    };

    render() {
        if (this.props.processing) {
            return (
                <div id="main">
                    <Header/>
                    <div id="wrapper">
                        <div className="content">
                            <Loader style={{marginBottom: "20%"}}/>
                        </div>
                    </div>
                    <Footer/>
                </div>
            );
        } else {
            if (this.props.error) {
                return (
                    <Error/>
                )
            } else {
                return (
                    <>
                        {this.getForm()}
                    </>
                );
            }
        }
    }
}


const mapStateToProps = (state) => {
    return {
        processing: state.event.processing,
    }
};
const connectedComponent = connect(mapStateToProps)(Movies);
export default withRouter(connectedComponent);

