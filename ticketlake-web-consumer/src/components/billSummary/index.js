// Library
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Loader from "../../commonComponents/loader";


let walletInner = 0;

class BillSummary extends Component {


    componentDidMount() {
        if (this.props.wallet) {
            walletInner = this.props.wallet;
        }
    }

    getWalletDisplay = (step, wallet, sum, totalBill, currency) => {

        const jsx = [];
        if (step >= 2 && wallet) {
            if (wallet.availableBalance > 0) {
                if (sum < wallet.availableBalance || sum === wallet.availableBalance) {
                    jsx.push(<div className='ticketPriceDetail '>
                        <div className='row'>
                            <div className='col-md-6'>
                                <strong>Wallet Balance</strong>
                                <small>${Math.abs(wallet.availableBalance - sum)} Remaining</small>
                            </div>
                            <div className='col-md-6'>
                            <span>
                                -{currency}{Math.abs(Math.abs(wallet.availableBalance - sum) - wallet.availableBalance)}
                            </span>
                            </div>
                        </div>

                    </div>);
                } else {
                    jsx.push(<div className='ticketPriceDetail'>
                        <div className='row'>
                            <div className='col-md-6'>
                                <strong>Wallet Balance</strong>
                                <small>${0} Remaining</small>
                            </div>
                            <div className='col-md-6'>
                            <span>
                                -{currency}{wallet.availableBalance}
                            </span>
                            </div>
                        </div>

                    </div>);
                }
            } else if(walletInner > 0) {
                if (sum < walletInner || sum === walletInner) {
                    jsx.push(<div className='ticketPriceDetail '>
                        <div className='row'>
                            <div className='col-md-6'>
                                <strong>Wallet Balance</strong>
                                <small>${Math.abs(walletInner - sum)} Remaining</small>
                            </div>
                            <div className='col-md-6'>
                            <span>
                                -{currency}{Math.abs(Math.abs(walletInner - sum) - walletInner)}
                            </span>
                            </div>
                        </div>

                    </div>);
                } else {
                    jsx.push(<div className='ticketPriceDetail'>
                        <div className='row'>
                            <div className='col-md-6'>
                                <strong>Wallet Balance</strong>
                                <small>${0} Remaining</small>
                            </div>
                            <div className='col-md-6'>
                            <span>
                                -{currency}{walletInner}
                            </span>
                            </div>
                        </div>

                    </div>);
                }
            }
        }
        return jsx;
    };

    getDisplay = () => {
        const displayButton = [];
        const displayData = [];
        if (this.props.currentStep === 1) {
            displayButton.push(<button className='checkoutButton' key={1} onClick={this.props.forward}>
                Continue</button>);
        } else if (this.props.currentStep === 2) {

            displayButton.push(
                <>
                    <button className='checkoutButton' onClick={this.props.paymentPage}>
                        Checkout
                    </button>
                    {/*<button className='checkoutButton' onClick={this.props.backward}*/}
                    {/*        style={{marginTop: '0px'}}>Back*/}
                    {/*</button>*/}
                </>);

        } else {
            displayButton.push("");
        }

        const sum = [0];
        if (this.props.billSummary) {
            const billData = this.props.billSummary;
            return (
                <div className='billSummary whiteBackground'>
                    <div className="col-md-12">
                        <h1>Bill Summary</h1>
                        {
                            billData.map((item, i) => {
                                if (item.ticketClassType === 'REGULAR') {
                                    if (item.availableTickets > 0 && this.props.seats[item.ticketClassName].length) {
                                        sum.push(parseInt(item.ticketClassPrice) * parseInt(item.ticketClassQty));
                                        displayData.push(
                                            <div className='ticketPriceDetail' key={i}>
                                                <div className='row'>
                                                    <div className='col-md-6'>
                                                        <strong>{item.ticketClassName}</strong>
                                                        <small>Type: <b>Regular</b></small>
                                                        <small>{this.props.currency + item.ticketClassPrice + ' X ' + item.ticketClassQty}</small>
                                                    </div>
                                                    <div className='col-md-6'>
                                            <span>
                                                {this.props.currency}{parseInt(item.ticketClassPrice) * parseInt(item.ticketClassQty)}
                                            </span>
                                                    </div>
                                                </div>

                                            </div>
                                        );
                                    }
                                } else {

                                    sum.push(parseInt(item.passPrice) * parseInt(item.ticketClassQty));
                                    displayData.push(
                                        <div className='ticketPriceDetail' key={i}>
                                            <div className='row'>
                                                <div className='col-md-6'>
                                                    <strong>{item.passTitle}</strong>
                                                    <small>Type: <b>Pass</b></small>
                                                    <small>Class: <b>{item.ticketClassName}</b></small>
                                                    <small>{this.props.currency + item.passPrice + ' X ' + item.ticketClassQty}</small>
                                                </div>
                                                <div className='col-md-6'>
                                            <span>
                                                {this.props.currency}{parseInt(item.passPrice) * parseInt(item.ticketClassQty)}
                                            </span>
                                                </div>
                                            </div>

                                        </div>
                                    );
                                }


                            })

                        }


                        {displayData}
                        {this.getWalletDisplay(
                            this.props.currentStep,
                            this.props.wallet,
                            sum.reduce((partial_sum, a) => partial_sum + a),
                            this.props.totalBill,
                            this.props.currency
                        )}

                        <div className='ticketTotalPrice'>
                            <div className='row'>
                                <div className='col-md-6'>
                                    <strong>Total Cost</strong>
                                </div>
                                <div className='col-md-6'>
                                    <span>{this.props.currency}{this.props.totalBill}</span>
                                </div>
                            </div>
                        </div>

                        {(displayData.length) ? displayButton : (
                            <button className='checkoutButton' onClick={this.props.history.goBack}>
                                Back
                            </button>)}

                    </div>

                </div>

            );


        } else {
            return (
                <Loader/>
            );
        }
    };

    render() {
        return (
            <>
                {this.getDisplay()}
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        billSummary: state.ticket.billSummary,
        totalBill: state.ticket.totalBill,
        currency: state.ticket.ticketCurrency,
        seats: state.ticket.seats,
        wallet: state.user.userWallet
    }
};
const connectedComponent = connect(mapStateToProps)(BillSummary);

export default withRouter(connectedComponent);