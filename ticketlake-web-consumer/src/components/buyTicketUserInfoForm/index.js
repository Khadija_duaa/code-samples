// Library
import React from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';


const buyTicketUserInfoForm = (props) => {
    const required = (value) => {
        if (!value.toString().trim().length) {
            return <span className="error">Required</span>
        }
    };
    return (

        <div className="booking-form-wrap">
            <div className="list-single-main-item fl-wrap hidden-section tr-sec" style={{border: '0'}}>
                <div className="profile-edit-container">
                    <div className="custom-form">
                        <fieldset className="fl-wrap">
                            <div className="list-single-main-item-title">
                                <h3 style={{fontSize: '16px'}}>
                                    {props.ticketClassName} - Seat {props.seatName}, Row {props.rowName}
                                    <div style={{
                                        height: '40px',
                                        width: '40px',
                                        float: 'right',
                                        background: props.ticketClassColor,
                                        borderRadius: '10px'
                                    }}/>
                                </h3>
                            </div>
                            <div className="row">
                                <Form>
                                    <div className='col-md-12'>
                                        <div className='col-md-4 float-left'>
                                            <Input
                                                type="text"
                                                name={'name'}
                                                validations={[required]}
                                                placeholder={'Name'}
                                                onChange={(e) => props.onInputChange('name', e.target.value, props.rowNumber, props.seatNumber, props.uniqueId)}/>
                                        </div>
                                        <div className='col-md-4 float-left'>
                                            <Input
                                                type="email"
                                                name={'email'}
                                                validations={[required]}
                                                placeholder={'Email'}
                                                onChange={(e) => props.onInputChange('email', e.target.value, props.rowNumber, props.seatNumber, props.uniqueId)}/>
                                        </div>
                                        <div className='col-md-4 float-left'>
                                            <Input
                                                type="text"
                                                name={'phoneNumber'}
                                                validations={[required]}
                                                placeholder={'Contact Number'}
                                                onChange={(e) => props.onInputChange('phoneNumber', e.target.value, props.rowNumber, props.seatNumber, props.uniqueId)}/>
                                        </div>
                                    </div>
                                </Form>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default buyTicketUserInfoForm;