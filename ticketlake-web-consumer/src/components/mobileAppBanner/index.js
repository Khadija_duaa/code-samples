// Library
import React, {Component} from 'react';
// Style
const bannerStyle = {
    background: 'url( "' + window.location.origin + '/images/all/presentation.png")',
    opacity: 1,
    backgroundSize: '100%',
    backgroundRepeat: 'no-repeat'
};

class MobileAppBanner extends Component {
    render() {
        return (
            <section className="color-bg hidden-section">
                <div className="wave-bg wave-bg2" style={bannerStyle}/>
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            {/* */}
                            <div className="colomn-text  pad-top-column-text fl-wrap">
                                <div className="colomn-text-title">
                                    <h3>Get the App, Get the ticket</h3>
                                    <p>
                                        In ut odio libero, at vulputate urna. Nulla tristique mi a
                                        massa convallis cursus. Nulla eu mi magna. Etiam suscipit
                                        commodo gravida. Lorem ipsum dolor sit amet, conse ctetuer
                                        adipiscing elit, sed diam nonu mmy nibh euismod tincidunt ut
                                        laoreet dolore magna aliquam erat.
                                    </p>
                                    <a href="#" className=" down-btn color3-bg"
                                       style={{background: '#FFE0E1', color: '#EC1B23'}}>
                                        <i className="fab fa-apple"/> Download for iPhone</a>
                                    <a href="#" className=" down-btn color3-bg"
                                       style={{background: '#FFE0E1', color: '#EC1B23'}}>
                                        <i className="fab fa-android"/> Download for Android</a>
                                </div>
                            </div>
                            {/*process-wrap   end*/}
                        </div>
                        <div className="col-md-6">

                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default MobileAppBanner;