// Library
import React, {Component} from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import DatePicker from "react-datepicker";
import moment  from 'moment';
import Select ,{ components } from 'react-select'
import _ from 'lodash';
// Component
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {resetRedux, saveFormData, updateUser} from "../../redux/user/user-actions";
import AuthRoutes from '../../commonComponents/authRotes';
import UserPagesContainer from '../../commonComponents/userPagesContainer';
import {Breadcrumbs, BreadcrumbsItem} from "react-breadcrumbs-dynamic";
// Helpers
import {getCountries, getCities} from '../../utils/common-utils';
// Css
import './style.css';
const countries = [];
let cities = [];

class UserProfile extends Component {

    state = {
        name: '',
        email: '',
        phoneNumber: '',
        country: '',
        city: '',
        dateOfBirth: '',
        profileImageKey: {},
        selectedOptionCountry: '',
        selectedOptionCity: []
    };


    constructor(props) {
        super(props);

        this.fetchCities = this.fetchCities.bind(this);
        this.setCity = this.setCity.bind(this);
    }

    componentDidMount() {
        getCountries().forEach((data, i) => {
            countries.push({
                value: data,
                label: data
            });
        });
        if (this.props.user) {
            let {name, email, phoneNumber, country, city, dateOfBirth} = this.props.user;
            this.setState(
                {
                    name,
                    email,
                    phoneNumber,
                    country,
                    city,
                    dateOfBirth
                }
            );
            cities = [];
            const citiesArr = [];
            const citySelection = {};
            getCities(country).forEach((data, i) => {
                citiesArr.push({
                    label: data,
                    value: data,
                    link: country
                });
            });
            this.setState({selectedOptionCountry: {label: country, value: country}});
            this.setState({selectedOptionCity: {label: city, value: city}});
            cities = citiesArr;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.profileImage) {
            this.setState({
                profileImageKey: nextProps.profileImage
            })
        }
    }


    onSaveChanges = (e) => {

        e.preventDefault();
        const state = {...this.state};

        this.props.resetRedux();

        const preparingFormData = {
            name: this.state.name,
            email: this.state.email,
            phoneNumber: this.state.phoneNumber,
            country: this.state.country ? (this.state.country.value ? this.state.country.value : this.state.country) : this.state.country,
            city: this.state.city ? (this.state.city.value ? this.state.city.value : this.state.city) : this.state.city,
            dateOfBirth: this.state.dateOfBirth,
            profileImageKey: this.state.profileImageKey,
        };


        this.props.saveFormData(preparingFormData);

        this.props.updateUser(
            (userState) => {
                this.props.history.push('/sign-up/verification', {...userState});
            },
            () => {
                this.props.history.push('/user/profile')
            },
            (errorCB) => {
                console.log(errorCB);
            }
        );

    };

    fetchCities(e) {
        cities = [];
        const citiesArr = [];
        getCities(e.value).forEach((data, i) => {
            citiesArr.push({
                label: data,
                value: data,
                link: e.value
            });
        });
        this.setState({selectedOptionCountry: e});
        this.setState({selectedOptionCity: []});
        this.setState({country: e});
        cities = citiesArr;
    }
    handleChange = (date) => {
        this.setState({
            dateOfBirth: date
        });
    };
    setCity(e) {
        this.setState({selectedOptionCity: e});
        this.setState({city: e});
    }

    onInputChange = (e) => {
        let state = {...this.state};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };
    _style = {
        paddingLeft : '20px',
        float: 'left',
        border: ' 1px solid #eee',
        backgroundColor: '#F7F9FB',
        width: '100%',
        padding: ' 14px 170px 14px 20px',
        borderRadius: '6px',
        color: '#666',
        fontSize: '13px',
        webkitAppearance : 'none',
        textIndent: '20px'
    };

    getProfile = () => {
        const ValueContainer = ({ children, ...props }) => {
            return (
                components.ValueContainer && (
                    <components.ValueContainer {...props}>
                        {!!children && (
                            <i
                                className={props.selectProps && props.selectProps.id === "country" ? "fas fa-flag" : "fas fa-city"}
                                aria-hidden="true"
                                style={{ position: "absolute", left: 10 , color: '#c6161d'}}
                            />
                        )}
                        {children}
                    </components.ValueContainer>
                )
            );
        };
        const styles = {
            valueContainer: base => ({
                ...base,
                paddingLeft: 34
            })
        };
        const required = (value) => {
            if (!value.toString().trim().length) {
                return <span className="error">Required</span>
            }
        };

        return (
            <section className="middle-padding">
                <div className="container">
                    <div className="dasboard-wrap fl-wrap">
                        <div className="dashboard-content fl-wrap">
                            <div className="box-widget-item-header">
                                <h3> Your Profile</h3>
                            </div>
                            <div className="profile-edit-container">
                                {this.props.error ? <p style={{color: 'red'}}>{this.props.error}</p> : null}

                                <Form>

                                    <div className="custom-form">
                                        <label>Name <i className="fa fa-user"/></label>
                                        <Input type="text"
                                               id="name"
                                               name="name"
                                               validations={[required]}
                                               onChange={this.onInputChange}
                                               value={this.state.name}/>

                                        <label>Email<i className="fa fa-envelope"/></label>
                                        <Input type="text"
                                               id="email"
                                               name="email"
                                               validations={[required]}
                                               onChange={this.onInputChange}
                                               value={this.state.email}/>

                                        <label>Phone Number<i className="fa fa-phone"/></label>
                                        <Input type="text"
                                               id="phoneNumber"
                                               name="phoneNumber"
                                               validations={[required]}
                                               onChange={this.onInputChange}
                                               value={this.state.phoneNumber}/>

                                        <label> Country <i className="fas fa-city"/></label>

                                        <Select
                                            validations={[required]}
                                            id="country"
                                            name="country"
                                            className='react-select-container iconSelectDropDown'
                                            classNamePrefix='react-select'
                                            value={this.state.selectedOptionCountry}
                                            onChange={this.fetchCities}
                                            options={countries}
                                            components={{ValueContainer}}
                                            styles={styles}
                                            // defaultMenuIsOpen={true}
                                        />
                                        <label> City <i className="fa fa-map-marker"/></label>
                                        <Select
                                            validations={[required]}
                                            id="city"
                                            name="city"
                                            className='react-select-container iconSelectDropDown'
                                            classNamePrefix='react-select'
                                            value={this.state.selectedOptionCity}
                                            options={cities}
                                            onChange={this.setCity}
                                            components={{ ValueContainer }}
                                            styles={styles}
                                        />
                                        <label> Date of Birth <i className="fa fa-calendar-alt" style={{zIndex: '1'}}/></label>
                                        <DatePicker
                                            placeholderText={"Date of Birth"}
                                            // selected={this.state.dateOfBirth ? new Date(this.state.dateOfBirth) : new Date()}
                                            value={this.state.dateOfBirth}
                                            onChange={this.handleChange}
                                            customInput={<Input style={this._style}/>}
                                            maxDate={new Date(moment(new Date()).subtract(10, 'years').format())}
                                            peekNextMonth
                                            showMonthDropdown
                                            scrollableYearDropdown
                                            yearDropdownItemNumber={15}
                                            showYearDropdown
                                            dropdownMode="select"
                                        />
                                    </div>
                                </Form>
                            </div>

                            <div className="profile-edit-container">
                                <div className="custom-form">

                                    <button className="btn color2-bg  float-btn" onClick={this.onSaveChanges}>
                                        Save Changes<i className="fa fa-save"/></button>
                                    <button
                                        className={"btn btn-danger buttonDefault defaultBackground custom-cancel-button"}
                                        onClick={() => window.location.reload()}>
                                        Cancel
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    };

    render() {
        const breadCrumbs = [];
        breadCrumbs.push(<BreadcrumbsItem glyph='home' to='/'>Home Page</BreadcrumbsItem>);
        breadCrumbs.push(<BreadcrumbsItem to='/user/profile'>User Profile</BreadcrumbsItem>);
        return (
            <AuthRoutes>
                <div id="main">
                    <Header/>
                    <div id="wrapper">
                        <UserPagesContainer
                            page={'profile'}
                            showUploadButton={true}
                            breadcrumbs={breadCrumbs}

                        >
                            {this.getProfile()}
                        </UserPagesContainer>

                    </div>
                    <Footer/>

                </div>
            </AuthRoutes>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.user.authenticated,
        user: state.user.user,
        profileImage: state.user.profileImage,
        error: state.user.error,
    }
};
const connectedComponent = connect(mapStateToProps, {resetRedux, saveFormData, updateUser})(UserProfile);
export default withRouter(connectedComponent);
