// library
import React, {Component} from "react";
import DropIn from "braintree-web-drop-in-react";
import Loader from "../../commonComponents/loader";
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {NotificationManager} from 'react-notifications';
import swal from '@sweetalert/with-react';
import {
    setClientToken,
    checkout
} from "../../redux/ticket/ticket-actions";


class Checkout extends Component {
    instance;

    state = {
        clientToken: null
    };

    componentWillMount() {
        const clientToken = "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI0MzMzMmQxMDZjNTY3N2Q4ZjczYTM1YzhlYzJiNzc0N2M2MjY0NmMzYWVjMzM0NTg3Y2QzZGVlY2FlMWI5MGU5fGNyZWF0ZWRfYXQ9MjAxOS0wNS0wN1QwOToyNzozMC4wMTA4NTgxMTArMDAwMFx1MDAyNm1lcmNoYW50X2lkPXI4aHpnajV0emN3a2R2NGNcdTAwMjZwdWJsaWNfa2V5PTM1dHJ3aHc2djdneDdmbWYiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvcjhoemdqNXR6Y3drZHY0Yy9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJncmFwaFFMIjp7InVybCI6Imh0dHBzOi8vcGF5bWVudHMuc2FuZGJveC5icmFpbnRyZWUtYXBpLmNvbS9ncmFwaHFsIiwiZGF0ZSI6IjIwMTgtMDUtMDgifSwiY2hhbGxlbmdlcyI6W10sImVudmlyb25tZW50Ijoic2FuZGJveCIsImNsaWVudEFwaVVybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy9yOGh6Z2o1dHpjd2tkdjRjL2NsaWVudF9hcGkiLCJhc3NldHNVcmwiOiJodHRwczovL2Fzc2V0cy5icmFpbnRyZWVnYXRld2F5LmNvbSIsImF1dGhVcmwiOiJodHRwczovL2F1dGgudmVubW8uc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbSIsImFuYWx5dGljcyI6eyJ1cmwiOiJodHRwczovL29yaWdpbi1hbmFseXRpY3Mtc2FuZC5zYW5kYm94LmJyYWludHJlZS1hcGkuY29tL3I4aHpnajV0emN3a2R2NGMifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoic3luYXZvcyIsImNsaWVudElkIjoiQVNETUFpblFDUDJ2LWpoU19LWC1Od3BSYlp3SWZibnlDVm9NMUZaeWtncEZVR2RpdUdxQkZwd3lvdlhHTU9tbDA5aWx5QlZ5TUE3NFFDZWUiLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjpmYWxzZSwiZW52aXJvbm1lbnQiOiJvZmZsaW5lIiwidW52ZXR0ZWRNZXJjaGFudCI6ZmFsc2UsImJyYWludHJlZUNsaWVudElkIjoibWFzdGVyY2xpZW50MyIsImJpbGxpbmdBZ3JlZW1lbnRzRW5hYmxlZCI6dHJ1ZSwibWVyY2hhbnRBY2NvdW50SWQiOiJzeW5hdm9zIiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sIm1lcmNoYW50SWQiOiJyOGh6Z2o1dHpjd2tkdjRjIiwidmVubW8iOiJvZmYifQ==" // If returned as JSON string
        this.setState({
            clientToken
        });
    }

    async buy() {
        if (this.instance) {
            if (this.instance.isPaymentMethodRequestable()) {
                const {nonce} = await this.instance.requestPaymentMethod();
                console.log("Nonce: ", nonce);
                swal({
                        title: 'Checkout Summary',
                        text: 'Please review your bill',
                        content: <div>
                            <div className='billSummary'>
                                <div className="col-md-12">
                                    <div className='ticketTotalPrice'>
                                        <div className='row'>
                                            <div className='col-md-6'>
                                                <strong>Total Cost</strong>
                                            </div>
                                            <div className='col-md-6'>
                                                <span>{this.props.currency}{this.props.totalBill}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <br/>
                                    <span>Are you sure you want to continue?</span>
                                </div>
                            </div>
                        </div>,
                        buttons: true
                    }
                ).then(res => {
                    if (res) {
                        this.props.checkout(
                            this.props.assignedSeats,
                            this.props.assignedSeatsForDisplay,
                            this.props.event,
                            nonce,
                            this.props.passesAssignedSeats,
                            this.props.passesAssignedSeatsForDisplay
                        );
                    } else {
                        swal("Checkout has been canceled!");
                    }
                });

            } else {
                NotificationManager.error("Please login to your PayPal Account before checking out", '', 3000);
            }
        } else {
            NotificationManager.error("Please login to your PayPal Account before checking out", '', 3000);
        }
    }

    buyWithoutPayPal() {
        swal({
                title: 'Checkout Summary',
                text: 'Please review your bill',
                content: <div>
                    <div className='billSummary'>
                        <div className="col-md-12">
                            <div className='ticketTotalPrice'>
                                <div className='row'>
                                    <div className='col-md-12'>
                                        <strong>Total Cost is Deducting from your Wallet</strong>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <span>Are you sure you want to continue?</span>
                        </div>
                    </div>
                </div>,
                buttons: true
            }
        ).then(res => {
            if (res) {
                this.props.checkout(
                    this.props.assignedSeats,
                    this.props.assignedSeatsForDisplay,
                    this.props.event,
                    null,
                    this.props.passesAssignedSeats,
                    this.props.passesAssignedSeatsForDisplay);
            } else {
                swal("Checkout has been canceled!");
            }
        });
    }

    render() {


        if (this.props.totalBill === 0) {

            return (
                <button className='checkoutButton' onClick={this.buyWithoutPayPal.bind(this)}>
                    Checkout
                </button>
            );


        } else {
            const paypalConfig = {
                flow: 'checkout',
                amount: this.props.totalBill,
                currency: 'USD'
            };

            if (!this.state.clientToken) {
                return (
                    <Loader height={'100px'}/>
                );
            } else {
                return (
                    <div style={{clear: 'both'}}>
                        <DropIn
                            options={{
                                authorization: this.state.clientToken,
                                paypal: paypalConfig,
                                card: false,
                            }}
                            onInstance={instance => (this.instance = instance)}
                        />
                        <button className='checkoutButton' onClick={this.buy.bind(this)}>
                            Checkout
                        </button>
                    </div>
                );
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        clientToken: state.ticket.clientToken,
        totalBill: state.ticket.totalBill,
        event: state.ticket.event,
        assignedSeats: state.ticket.assignedSeats,
        assignedSeatsForDisplay: state.ticket.assignedSeatsForDisplay,
        currency: state.ticket.ticketCurrency,
        passesAssignedSeats: state.ticket.passesAssignedSeats,
        passesAssignedSeatsForDisplay: state.ticket.passesAssignedSeatsForDisplay
    };

};

const connectedComponent = connect(mapStateToProps, {setClientToken, checkout})(Checkout);
export default withRouter(connectedComponent);