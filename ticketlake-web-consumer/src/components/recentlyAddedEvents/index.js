// Library
import React, {Component} from 'react';

// Component
import SectionHeading from "../../commonComponents/sectionHeading";
import CardSlider from '../../commonComponents/cardSlider';
import Loader from '../../commonComponents/loader';
import DefaultCard from '../../commonComponents/defaultCard';
// Redux
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {getUpcomingEventsForHome} from '../../redux/event/event-actions';
import {wishListToggle, getWishListIdsFromApi} from '../../redux/wishlist/wishlist-actions';
// Helpers
import {getMaxAndMinPrice, getCardDates, isNullOrEmpty} from '../../utils/common-utils';
// Slider Settings
const sliderSettings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 1,
    cssEase: "ease-in",
    centerMode: true,
    centerPadding: "50px",
    arrows: false,
    lazyLoad: true,
};
// Buttons
const buttonSettings = {
    display: true,
    mainPreviousClass: 'swiper-button-prev sw-btn',
    previousIcon: 'fa fa-arrow-left',
    mainNextClass: 'swiper-button-next sw-btn',
    nextIcon: 'fa fa-arrow-right'
};

let wishListFlag = false;

class RecentlyAddedEvents extends Component {

    componentWillMount() {
        this.props.getUpcomingEventsForHome();
        if (this.props.auth) {
            this.props.getWishListIdsFromApi();
        }
    }

    wishListToggle = (eventSlotId) => {
        this.props.wishListToggle(eventSlotId);
    };

    render() {
        const {heading, text} = this.props;
        const event = this.props.upcomingEventsHome;

        console.log("upcomingEventsHome:", event)
        if (this.props.processing) {
            return (
                <section className="light-red-bg">
                    <SectionHeading
                        heading={heading}
                        text={text && text ? text : ''}
                    />
                    <Loader/>

                </section>
            )
        } else {
            if (isNullOrEmpty(event)) {
                return (
                    <section className="light-red-bg">
                        <SectionHeading
                            heading={heading}
                            text={text && text ? text : ''}
                        />

                        No Upcoming Events Found!
                    </section>
                );
            } else {
                return (
                    <section className="light-red-bg">
                        <SectionHeading
                            heading={heading}
                            text={text && text ? text : ''}
                        />

                        <CardSlider settings={sliderSettings} buttons={buttonSettings}>
                            {event.map((card, i) => {
                                if (this.props.auth) {
                                    wishListFlag = this.props.wishLists && this.props.wishLists !== '' && this.props.wishLists.includes(card.eventSlotId);
                                }
                                return (

                                    <DefaultCard key={i}
                                                 gridLg={12}
                                                 gridMd={12}
                                                 gridSm={12}
                                                 auth={this.props.auth}
                                                 cardTitle={card.eventTitle}
                                                 image={card.bannerImageKey.imageUrl}
                                                 cardLink={'#'}
                                                 isWishList={wishListFlag}
                                                 wishlistLink={() => {
                                                     this.wishListToggle(card.eventSlotId)
                                                 }}
                                                 onClick={() => this.props.history.push(`/event/detail/${card.eventSlotId}`)}
                                                 cardAddress={card.venue ? card.venue.address : ''}
                                                 country={card.venue ? card.venue.country : []}
                                                 dates={getCardDates(card.eventDateTimeSlot)}
                                                 city={card.venue ? card.venue.city : []}
                                                 buttonText={getMaxAndMinPrice(card)}
                                                 buttonLink={`/buy-ticket/${card.eventSlotId}`}
                                    />
                                )

                            })
                            }
                        </CardSlider>

                    </section>
                );
            }
        }


    }
}

const mapStateToProps = (state) => {
    return {
        upcomingEventsHome: state.event.upcomingEventsHome,
        auth: state.user.authenticated,
        wishLists: state.wishlist.wishListIds,
        processing: state.event.processing
    }
};

const connectedComponent = connect(mapStateToProps, {
    wishListToggle,
    getWishListIdsFromApi,
    getUpcomingEventsForHome
})(RecentlyAddedEvents);
export default withRouter(connectedComponent);