// Library
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavLink, withRouter} from 'react-router-dom';
import {NotificationManager} from 'react-notifications';
import {Breadcrumbs, BreadcrumbsItem} from "react-breadcrumbs-dynamic";
// Component
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';
import ResultForHeading from '../../commonComponents/resultForHeading';
import BillSummary from '../billSummary';
import BuyTicketStepOne from '../buyTicketStepOne';
import BuyTicketStepTwo from '../buyTicketStepTwo';
import Checkout from '../checkout';
import BuyTicketConfirmation from '../buyTicketConfirmation';
import Loader from "../../commonComponents/loader";
import AuthRoutes from '../../commonComponents/authRotes'
// Helper
import {getDateAndTimeFromIso} from '../../utils/common-utils';
// Redux
import {
    setStep,
    setBillSummary,
    getEventDetail,
    setAssignedSeats,
    resetRedux
} from '../../redux/ticket/ticket-actions';
// Error
import Error from '../../commonComponents/error';

class BuyTicketPage extends Component {
    componentWillMount() {
        this.props.resetRedux();
        let eventId = this.props.match.params.id;
        this.props.getEventDetail(eventId);


    }

    onInputChange = (name, val, event, ticketClassId, uniqueId) => {
        const billS = this.props.billSummary;

        billS.find(obj => {
            if (obj.ticketClassName === name && obj.ticketClassId === ticketClassId && obj.uniqueId === uniqueId) {
                if (
                    parseInt(val) <= parseInt(event.target.max) &&
                    parseInt(val) >= parseInt(event.target.min)
                ) {
                    obj.ticketClassQty = parseInt(val);
                }
            }
        });
        this.props.setBillSummary(billS);
    };


    getFormView = (arr) => {
        const jsx = [];
        arr.map((singleItem, i) => {
            if (singleItem.ticketClassType === 'REGULAR') {
                if (singleItem.availableTickets > 0 && this.props.seats[singleItem.ticketClassName].length) {
                    jsx.push(
                        <tr key={i}>
                            <td>{singleItem.ticketClassName}</td>
                            <td>{this.props.currency}{singleItem.ticketClassPrice}</td>
                            <td>{singleItem.availableTickets}</td>
                            <td>
                                <div className="quantity">
                                    <input
                                        type="number"
                                        id={"inputTypeNumber" + singleItem.ticketClassName}
                                        value={singleItem.ticketClassQty}
                                        name={singleItem.ticketClassName}
                                        onChange={(e) => this.onInputChange(e.target.name, e.target.value, e, singleItem.ticketClassId, singleItem.uniqueId)}
                                        min="0"
                                        max={singleItem.availableTickets}/>
                                </div>

                            </td>

                        </tr>
                    )
                }
            }

        });

        if (!jsx.length) {
            jsx.push(<tr>
                <td colSpan={4}>No Seats Available</td>
            </tr>)
        }

        return jsx;

    };

    getPassesView = (passData) => {

        const jsx = [];
        passData.map((singleItem, i) => {
            if (singleItem.availablePassCount > 0) {
                jsx.push(
                    <tr key={i}>
                        <td>{singleItem.passTitle}</td>
                        <td>{singleItem.ticketClassName}</td>
                        <td>{this.props.currency}{singleItem.passPrice}</td>
                        <td>{singleItem.availablePassCount}</td>
                        <td>
                            <div className="quantity">
                                <input
                                    type="number"
                                    id={"inputTypeNumber" + singleItem.ticketClassName + singleItem.ticketClassId}
                                    value={singleItem.ticketClassQty}
                                    name={singleItem.ticketClassName}
                                    onChange={(e) => this.onInputChange(e.target.name, e.target.value, e, singleItem.ticketClassId, singleItem.uniqueId)}
                                    min="0"
                                    max={singleItem.availablePassCount}/>
                            </div>

                        </td>

                    </tr>
                )
            }

        });

        if (!jsx.length) {
            jsx.push(<tr>
                <td colSpan={4}>No Passes Available</td>
            </tr>)
        }

        return jsx;


    };

    changeStepForward = () => {
        if (this.props.step === 1) {
            if (!(this.props.seatsAssignedFlag || this.props.passesAssignedFlag)) {
                NotificationManager.error("Seats are not assigned for this event", '', 3000);
            } else {
                if (this.props.totalBill <= 0) {
                    NotificationManager.error("Please Select at-least one seat", '', 3000);
                } else {
                    this.props.setAssignedSeats(
                        this.props.billSummary,
                        this.props.seats,
                        this.props.wallet,
                        this.props.event,
                        this.props.passData,
                        this.props.passTicketClasses
                    );
                }
            }
        }
    };

    changeStepBackward = () => {

        this.props.setStep(1);
    };

    goToPayment = () => {
        this.props.setStep(3);
    };

    getViewFromState = (data, ticketClassData) => {
        const eventTime = getDateAndTimeFromIso(data.eventDateTimeSlot.eventStartTime);

        switch (this.props.step) {
            case 1:
                return (
                    <BuyTicketStepOne
                        eventTime={eventTime}
                        ticketClasses={this.getFormView(ticketClassData)}
                        passClasses={this.getPassesView(ticketClassData)}
                        currency={this.props.currency}
                        processing={this.props.processing}
                    />
                );
            case 2:
                return (
                    <BuyTicketStepTwo
                        eventTime={eventTime}
                    />
                );
            case 3:
                return (
                    <Checkout/>
                );
            case 4:
                return (
                    <BuyTicketConfirmation
                        confirmation={this.props.successfulPayment}
                    />
                );
            default:
                return (
                    <BuyTicketStepOne
                        eventTime={eventTime}
                        ticketClasses={this.getFormView(ticketClassData)}
                        currency={this.props.currency}
                    />
                )
        }
    };

    getForm = () => {
        if (!this.props.processing && this.props.event && this.props.event.data) {
            const oldData = this.props.event && this.props.event.data;
            const data = oldData.data;
            const ticketClassesFilteredData = this.props.billSummary;
            return (
                <div id="main">
                    <Header/>
                    <div id="wrapper">

                        <div className="content">
                            <div className="breadcrumbs-fs">
                                <div className="container">
                                    <BreadcrumbsItem glyph='home' to='/'>Home Page</BreadcrumbsItem>
                                    <BreadcrumbsItem to='/events/listing'>All Events</BreadcrumbsItem>
                                    <BreadcrumbsItem
                                        to={'/event/detail/' + this.props.match.params.id}>{data.eventTitle} Event</BreadcrumbsItem>
                                    <BreadcrumbsItem
                                        to={'/event/buy/ticket/' + this.props.match.params.id}>{data.eventTitle ? data.eventTitle : 'Event Title'} Ticket</BreadcrumbsItem>


                                    <div className="breadcrumbs-hero-buttom fl-wrap">
                                        <div className="breadcrumbs">
                                            <Breadcrumbs
                                                item={NavLink}
                                                finalItem={'span'}
                                                finalProps={{
                                                    style: {color: 'red'}
                                                }}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <section className="middle-padding gre light-red-bg">
                                <div className="container">
                                    <ResultForHeading
                                        firstText={'Buy Tickets'}
                                        secondText={data.eventTitle ? data.eventTitle + ' / ' + getDateAndTimeFromIso(data.eventDateTimeSlot.eventStartTime) : 'Event Title'}
                                    />
                                    <div className={'row'}>

                                    </div>

                                    <div className="row blockOnMobile">

                                        <div className='col-lg-8 col-md-12 col-sm-12 float-left whiteBackground'>
                                            <div className="col-lg-12 whiteBackground">
                                                <ul id="progressbar">
                                                    <li
                                                        className={(this.props.step === 1) ? 'active' : null}>
                                                        <span>01.</span>
                                                        Select Tickets
                                                    </li>
                                                    <li
                                                        className={(this.props.step === 2) ? 'active' : null}>
                                                        <span>02.</span>
                                                        Add Information
                                                    </li>
                                                    <li
                                                        className={(this.props.step === 3 || this.props.step === 4) ? 'active' : null}>
                                                        <span>03.</span>
                                                        Checkout
                                                    </li>
                                                </ul>
                                            </div>
                                            {this.getViewFromState(data, ticketClassesFilteredData)}
                                        </div>
                                        <div className="col-lg-4 col-md-12 col-sm-12 float-left billSummaryContainer">
                                            <BillSummary
                                                forward={this.changeStepForward}
                                                backward={this.changeStepBackward}
                                                currentStep={this.props.step}
                                                paymentPage={this.goToPayment}
                                            />

                                        </div>
                                    </div>

                                </div>
                            </section>
                        </div>
                    </div>
                    <Footer/>
                </div>
            );
        }

        return null;
    };

    render() {
        if (this.props.processing) {
            return (
                <AuthRoutes>
                    <div id="main">
                        <Header/>
                        <div id="wrapper">

                            <div className="content">
                                <div className="breadcrumbs-fs">
                                    <div className="container">
                                        <Breadcrumbs/>
                                    </div>
                                </div>
                                <Loader height={'100px'}/>
                            </div>
                        </div>
                        <Footer/>
                    </div>
                </AuthRoutes>
            );
        } else {
            if (this.props.error) {
                return (
                    <Error message={this.props.errorMessage}/>
                )
            } else {
                return (
                    <AuthRoutes>
                        {this.getForm()}
                    </AuthRoutes>

                );
            }
        }

    }
}

const mapStateToProps = (state) => {
    return {
        event: state.ticket.event,
        processing: state.ticket.processing,
        step: state.ticket.step,
        billSummary: state.ticket.billSummary,
        seats: state.ticket.seats,
        assignedSeats: state.ticket.assignedSeats,
        successfulPayment: state.ticket.successfulPayment,
        user: state.user.user,
        userToken: state.user.token,
        totalBill: state.ticket.totalBill,
        currency: state.ticket.ticketCurrency,
        assignedSeatsForDisplay: state.ticket.assignedSeatsForDisplay,
        seatsAssignedFlag: state.ticket.seatsAssignedFlag,
        passesAssignedFlag: state.ticket.passesAssignedFlag,
        error: state.ticket.error,
        errorMessage: state.ticket.errorMessage,
        wallet: state.user.userWallet,
        passData: state.ticket.passData,
        passTicketClasses: state.ticket.passTicketClasses,
    };

};

const connectedComponent = connect(mapStateToProps, {
    getEventDetail,
    setStep,
    setBillSummary,
    setAssignedSeats,
    resetRedux
})(BuyTicketPage);

export default withRouter(connectedComponent);