import React, {Component} from 'react';
import {connect} from 'react-redux';
import {logout, setProfilePic} from '../../redux/user/user-actions';
import {withRouter} from "react-router-dom";
import Loader from '../../commonComponents/loader';

class ProfileSidebar extends Component {

    state = {
        profilePictureProcessing: false,
        imageKey: ''
    };

    setProcessing = (processing, cb) => {
        if (processing) {
            this.setState({profilePictureProcessing: processing}, cb);
        } else {
            setTimeout(() => {
                this.setState({profilePictureProcessing: processing}, cb);
            }, 500);
        }
    };

    logoutUser = () => {
        this.props.logout();
    };

    onDrop = (e) => {
        if (e.target.files.length) {
            const file = e.target.files[0];

            const MAX_FILE_SIZE = 50 * 1024 * 1024;

            if (file.size > MAX_FILE_SIZE) {
                let message = 'Error occurred! File size exceeds the limit';
                console.log("Error", message)
            } else {
                const formData = new FormData();
                formData.append('image', file);

                this.setProcessing(true);

                this.props.setProfilePic(formData, this.props.userToken);
            }
        }
    };

    getForm = () => {
        return (
            <div className="dasboard-sidebar">
                <div className="dasboard-sidebar-content fl-wrap">
                    <div className="dasboard-avatar">
                        {
                            this.props.profilePictureProcessing ?
                                <Loader style={{color: "white"}}/>
                                :
                                <img
                                    src={this.props.profileImage && this.props.profileImage.imageUrl ? this.props.profileImage.imageUrl : '/images/default-dp.png'}
                                    alt="image"/>

                        }
                    </div>
                    {this.props.showUploadButton ? (
                        <>
                            <input type={'file'}
                                   ref={uploadElement => this.uploadElement = uploadElement}
                                   accept={'.jpg,.jpeg,.png'}
                                   hidden
                                   onChange={this.onDrop}/>

                            <a href="dashboard-add-listing.html"
                               onClick={e => {
                                   e.preventDefault();
                                   this.uploadElement.click()
                               }}
                               className="ed-btn">Upload Photo</a>
                        </>
                    ) : ""}
                    <div className="dasboard-sidebar-item fl-wrap">
                        <h3>
                            <span>Welcome </span>
                            {this.props.user ? this.props.user.name : "My Name"}
                        </h3>
                    </div>
                    <div className="user-stats fl-wrap">
                        <ul>
                            <li>
                                Balance
                                <span>{(this.props.wallet === null) ? 0 : (this.props.wallet.availableBalance) ? this.props.wallet.availableBalance : 0}</span>
                            </li>
                            <li>
                                Tickets
                                <span>12</span>
                            </li>
                            <li>
                                Wishlist
                                <span>{(this.props.wishListIds === null) ? 0 : this.props.wishListIds.length}</span>
                            </li>
                        </ul>
                    </div>
                    <a href="#" className="log-out-btn color-bg" onClick={this.logoutUser}
                       style={{backgroundColor: 'black'}}>Log Out <i
                        className="fa fa-sign-out-alt"/></a>
                </div>
            </div>
        )
    };


    render() {
        return (
            <div>
                {this.getForm()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.user,
        profileImage: state.user.profileImage,
        userToken: state.user.token,
        processing: state.user.processing,
        wishListIds: state.wishlist.wishListIds,
        profilePictureProcessing: state.user.profilePictureProcessing,
        wallet: state.user.userWallet
    }
};
const connectedComponent = connect(mapStateToProps, {setProfilePic, logout})(ProfileSidebar);
export default withRouter(connectedComponent);

