// Library
import React, {Component} from 'react';
import {Row, Col, Container} from 'reactstrap';
// Component
import SectionHeading from "../../commonComponents/sectionHeading";
import CardWithIcon from '../../commonComponents/cardWithIcon';
// Data Array
const cards = [
    {
        id: 1,
        cardIcon: 'fa fa-shopping-cart',
        cardLink: '#',
        cardTitle: 'Buy Tickets Online',
        cardDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu augue a nibh interdum'
    },
    {
        id: 2,
        cardIcon: 'fa fa-share-square',
        cardLink: '#',
        cardTitle: 'Share with anyone',
        cardDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu augue a nibh interdum'
    },
    {
        id: 3,
        cardIcon: 'fa fa-ticket-alt',
        cardLink: '#',
        cardTitle: 'Discount Coupons',
        cardDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu augue a nibh interdum'
    },


];

class WhyChooseUs extends Component {
    render() {
        const cardDisplay = cards.map(card => (
            <Col md={4} sm={12} key={card.id}>
                <CardWithIcon {...card}/>
            </Col>
        ));
        return (
            <section>
                <Container>
                    <SectionHeading
                        heading='Why Choose Us'
                        text='Explore some of the best tips from around the city from our partners and friends.'
                    />
                    <Row>
                        {cardDisplay}
                    </Row>
                </Container>
            </section>
        );
    }
}

export default WhyChooseUs;