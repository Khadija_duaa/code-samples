import React, {Component} from 'react';
import Transition from '../../commonComponents/transition';
import {verifyCodes} from '../../redux/user/user-actions';
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import Loader from "../../commonComponents/loader";

class Verification extends Component {

    constructor(props) {
        super(props);
        this.state = {
            emailOtp: '',
            phoneOtp: ''
        };
    }

    onInputChange = (e) => {
        let state = {...this.state};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };

    submitValues = (callback, next) => {

        const codes = {
            emailOtp: document.getElementById('emailOtpSetter').value,
            phoneOtp: ''
        };

        const {_id} = this.props.location.state;

        this.props.verifyCodes(codes, _id, next, callback,
            (errors) => {
                console.log(errors);
            });
    };

    getEmailOtpField = () => {
        if (this.props.location && this.props.location.state && this.props.location.state.emailVerificationRequired) {
            return (
                <div>
                    <label>Email OTP </label>
                    <input name="emailOtp" type="text" id='emailOtpSetter' placeholder="Email OTP" required/>
                </div>
            );
        }

        return null;
    };

    getPhoneOtpField = () => {
        if (this.props.location && this.props.location.state && this.props.location.state.smsVerificationRequired) {
            return (
                <div>
                    <label>Phone Number OTP </label>
                    <input name="phoneOtp" type="text" placeholder="Phone Number OTP" required
                           onChange={this.onInputChange}/>
                </div>
            );
        }

        return null;
    };

    getForm = () => {
        const {callback, next, codeError} = this.props;
        return(
          <Transition>
              <div className="main-register-wrap modal" key={1} style={{display: 'block'}}>
                  <div className="reg-overlay"/>
                  <div className="main-register-holder">
                      <div className="main-register fl-wrap">
                          <div id="tabs-container">
                              <div className="tab">

                                  <div id="tab-1" className="tab-content" style={{display: 'block'}}>
                                      <h3 style={{textAlign: 'center'}}><strong>Verification</strong></h3>

                                      <div className="custom-form">

                                          {this.getLoader()}

                                          {codeError ? <p style={{color: 'red'}}>{codeError}</p> : null}

                                          {this.getEmailOtpField()}

                                          {this.getPhoneOtpField()}

                                          <div className="clearfix"/>

                                          <div className={"row"} style={{flexWrap: 'unset'}}>
                                              <div className={"col-md-2"}>
                                                  <div className="filter-tags">
                                                      <button type="button" className="log-submit-btn"
                                                              onClick={() => {this.submitValues(callback, next)}}><span>Verify</span>
                                                      </button>
                                                  </div>
                                              </div>
                                              <div className={"col-md-2 offset-md-2"}>
                                                  <div className="filter-tags">
                                                      <button type="button" className="log-submit-btn"
                                                              onClick={() => {this.props.history.push('/authentication')}}><span>Cancel</span>
                                                      </button>
                                                  </div>
                                              </div>
                                          </div>

                                      </div>

                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
          </Transition>
      )
    };

    getLoader = () => {
        if (this.props.processing)
            return (
                <Loader/>
            );

        return null;
    };

    render() {
        return (
            <div>
                {
                    (this.props.location && this.props.location.state && this.props.location.state.emailVerificationRequired)
                    &&
                    (this.props.location && this.props.location.state && this.props.location.state.emailVerificationRequired) ?
                    this.getForm() : null
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        callback: state.user.callback,
        next: state.user.next,
        codeError: state.user.codeError,
        processing: state.user.processing,
    }
};

const connected = connect(mapStateToProps, {verifyCodes})(Verification);
export default withRouter(connected);
