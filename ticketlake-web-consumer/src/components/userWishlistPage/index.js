// Library
import React, {Component} from 'react';
// Component
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';
import {withRouter} from "react-router-dom";
import DefaultCard from '../../commonComponents/defaultCard';
import UserPagesContainer from '../../commonComponents/userPagesContainer';
import Loader from "../../commonComponents/loader";
//redux
import {getWishlistEvents, getWishListIdsFromApi, wishListToggle} from '../../redux/wishlist/wishlist-actions';
import {connect} from "react-redux";
// Helpers
import {getMaxAndMinPrice, getCardDates} from '../../utils/common-utils';
import {BreadcrumbsItem} from "react-breadcrumbs-dynamic";
import {Col} from "reactstrap";


class Wishlist extends Component {

    componentDidMount() {
        this.props.getWishlistEvents();
    }


    wishListToggle = (eventSlotId) => {
        if (this.props.auth) {
            this.props.wishListToggle(eventSlotId, true);
        }
    };

    getLoader = () => {
        if (this.props.processing)
            return (
                <Loader/>
            );

        return null;
    };
    getWishList = () => {

        return (
            <section className="middle-padding">
                <div className="container">
                    <div className="dasboard-wrap fl-wrap">
                        <div className="dashboard-content fl-wrap">
                            {
                                this.props.wishList && this.props.wishList.length > 0 ?
                                    <>
                                        <div className="box-widget-item-header">
                                            <h3>My Wishlist</h3>
                                        </div>
                                        {
                                            this.props.processing ?
                                            <Loader/> :
                                                <div>
                                                    {
                                                        this.props.wishList && this.props.wishList.map(data => {
                                                            return (
                                                                <DefaultCard key={1}
                                                                             gridLg={4}
                                                                             gridMd={6}
                                                                             gridSm={12}
                                                                             auth={this.props.auth}
                                                                             cardTitle={data.eventTitle}
                                                                             image={data.bannerImageKey.imageUrl}
                                                                             isWishList={true}
                                                                             wishlistLink={() => this.wishListToggle(data.eventSlotId)}

                                                                             cardLink={'#'}
                                                                             cardAddress={data.venue ? data.venue.address : ''}
                                                                             country={data.venue ? data.venue.country : []}
                                                                             city={data.venue ? data.venue.city : []}

                                                                             dates={getCardDates(data.eventDateTimeSlot)}
                                                                             onClick={() => this.props.history.push(`/event/detail/${data.eventSlotId}`)}
                                                                             buttonVersion={2}
                                                                             buttonText={getMaxAndMinPrice(data)}
                                                                             buttonLink={`/buy-ticket/${data.eventSlotId}`}
                                                                />
                                                            )
                                                        })
                                                    }
                                                </div>
                                        }
                                    </> :
                                    <div className="box-widget-item-header" style={{paddingBottom: '20%', borderBottom: 'none'}}>
                                        <h3> No Liked Events in Your Wishlist</h3>
                                    </div>
                            }
                        </div>
                    </div>
                </div>
            </section>
        )
    };

    render() {
        const breadCrumbs = [];
        breadCrumbs.push(<BreadcrumbsItem glyph='home' to='/'>Home Page</BreadcrumbsItem>);
        breadCrumbs.push(<BreadcrumbsItem to='/user/wishlist'>User Wishlist</BreadcrumbsItem>);

        return (
            <div id="main">
                <Header/>
                <div id="wrapper">
                    <div className="content">
                        <UserPagesContainer
                            page={'wishlist'}
                            breadcrumbs={breadCrumbs}>
                            {this.getWishList()}
                        </UserPagesContainer>
                    </div>
                </div>
                <Footer/>

            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        wishList: state.wishlist.wishList,
        auth: state.user.authenticated,
        processing: state.wishlist.processing
    }
};

const connectedComponent = connect(mapStateToProps, {
    getWishlistEvents,
    getWishListIdsFromApi,
    wishListToggle
})(Wishlist);
export default withRouter(connectedComponent);

