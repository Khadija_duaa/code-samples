// Library
import React, {Component} from 'react';
import _ from 'lodash';
// Components
import SectionHeading from '../../commonComponents/sectionHeading';
import AnimatedCards from '../../commonComponents/animatedCard';
import TwoShadedButton from '../../commonComponents/twoShadedButton';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import Loader from "../../commonComponents/loader";
import {setPromotedEventsForHome} from '../../redux/event/event-actions';
// Helper
import {isUndefined, isNullOrEmpty} from '../../utils/common-utils';

const pageHeading = "Promoted Events";
const pageDescription = "Explore some of the best tips from around the city from our partners and friends.";

class PromotedEvents extends Component {

    componentWillMount() {
        this.props.setPromotedEventsForHome();
    }

    render() {
        let counter = 1;
        let cardClass = '';
        const events = this.props.promotedEventsHome;

        console.log("EVENTS: " , events);


        if (this.props.processing) {
            return (
                <section id="sec2" className={'mt-5'} style={{padding: "0px"}}>
                    <SectionHeading
                        heading={pageHeading}
                        text={pageDescription}
                    />
                    <div className="gallery-items fl-wrap mr-bot spad home-grid">
                        <Loader/>
                    </div>
                </section>

            );
        } else {
            if (isNullOrEmpty(events)) {
                return (
                    <section id="sec2" className={'mt-5'} style={{padding: "0px"}}>
                        <SectionHeading
                            heading={pageHeading}
                            text={pageDescription}
                        />
                        No Data Found!!
                    </section>

                );
            } else {
                return (
                    <section id="sec2" className={'mt-5'} style={{padding: "0px"}}>
                        <SectionHeading
                            heading={pageHeading}
                            text={pageDescription}
                        />
                        <div className="gallery-items fl-wrap mr-bot spad home-grid">

                            {events.map((neweventList, i) => {
                                    if (counter === 2) {
                                        cardClass = 'gallery-item gallery-item-second';
                                    } else {
                                        cardClass = '';
                                    }
                                    counter++;
                                    return (

                                        <AnimatedCards
                                            cardWidth={neweventList.cardWidth}
                                            cardImage={neweventList.bannerImageKey.imageUrl}
                                            heading={neweventList.heading}
                                            paragraph={neweventList.paragraph}
                                            date={neweventList.eventDateTimeSlot.eventEndTime}
                                            cardClass={cardClass}
                                            cardLink={`event/detail/${neweventList.eventSlotId}`}
                                            key={i}
                                            item={neweventList}
                                            eventTitle={neweventList.eventTitle}
                                            categories={neweventList.categories.length > 0 ? neweventList.categories[0].name : null}
                                        />
                                    );

                                }
                            )}


                        </div>

                        <div style={{marginBottom: '4%'}}>
                            <TwoShadedButton
                                buttonLink={'/events/promoted'}
                                buttonText={'Explore all Events'}
                            />
                        </div>
                    </section>

                );
            }
        }


    }
}


const mapStateToProps = (state) => {
    return {
        promotedEventsHome: state.event.promotedEventsHome,
        processing: state.event.processing,
    }
};

const connectedComponent = connect(mapStateToProps, {
    setPromotedEventsForHome
})(PromotedEvents);
export default withRouter(connectedComponent);