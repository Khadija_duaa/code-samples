// Library
import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import DateRangePicker from '@wojtekmaj/react-daterange-picker'
// Importing Config
import {GET_CITIES_FOR_FILTERS as city} from '../../utils/config';
// Components
import HeroBanner from '../../commonComponents/heroBanner';
// Icons
import {
    faMapMarker,
    faDotCircle,
    faCalendarCheck,
    faUsers,
    faMale,
    faChild,
    faAngleDoubleDown,
    faSearch, faStar
} from '@fortawesome/free-solid-svg-icons';
import SectionHeading from "../../commonComponents/sectionHeading";
import {dateSplitter} from "../../utils/common-utils";
// Redux
import {getAllCategories} from "../../redux/category/category-actions";


const backgroundStyle = {
    transform: 'translateZ(0px) translateY(-59.1398px)',
    backgroundImage: 'url( "' + window.location.origin + '/images/banner_1.png")'
};

class SearchFormEvents extends React.Component {

    state = {
        location: null,
        dates: null,
        datesInput: null,
        categories: null
    };

    componentDidMount() {
        this.props.getAllCategories();
    }

    onDateChange = (date) => {
        const fromDate = date && date[0] !== null ? date[0] : null;
        const fromDateTime = dateSplitter(fromDate);

        const toDate = date && date[1] !== null ? date[1] : null;
        const toDateTime = dateSplitter(toDate);

        this.setState({dates: encodeURI(fromDateTime + ' ' + toDateTime)});
        this.setState({datesInput: date});

    };

    onCategoryChange = (e) => {
        this.setState({categories: e.target.value});
    };
    onLocationChange = (e) => {
        this.setState({location: e.target.value});
    };

    handleSubmit = (e) => {

        const category = this.state.categories;
        const location = this.state.location;
        const dates = this.state.dates;
        const url = [];


        if (category) {
            url.push("category=" + category);
        }
        if (location) {
            url.push("location=" + location);
        }

        if (dates) {
            url.push("when=" + dates);
        }

        this.props.history.push('events/listing/?' + url.join("&"));
    };

    render() {

        const {categories} = this.props;
        return (
            <HeroBanner
                backgroundImage={window.location.origin + '/images/banner_1.png'}
                transformStyle={'translateZ(0px) translateY(-59.1398px)'}>
                <div className="hero-section-wrap fl-wrap">
                    <div className="container">
                        <SectionHeading
                            heading='Ticketlake - Book Tickets Online'
                            text='“The trusted name in all things ticket”'
                            fontSize={60}
                            mainClass={'home-intro'}
                            textColor={'#FFFFFF'}
                        />
                        <div className="main-search-input-wrap">
                            <div className="main-search-input fl-wrap">
                                <div className="main-search-input-item location"
                                     id="autocomplete-container">
                                    <span className="inpt_dec"><i className='fas fa-map-pin'/></span>
                                    <select data-placeholder="City"
                                            name="city"
                                            style={{
                                                height: "50px",
                                                width: "100%",
                                                textIndent: "35px",
                                                background: "white",
                                                border: 'none'
                                            }}
                                            onChange={this.onLocationChange}>
                                        <option disabled selected>Location</option>
                                        <option>All</option>
                                        {
                                            Array.isArray(city) && city.map((x, i) => {
                                                return (
                                                    <option key={i} value={x._id}>{x.name}</option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>
                                <div
                                    className="main-search-input-item main-date-parent main-search-input-item_small">
                                    <span className="inpt_dec">
                                        <i className='fas fa-calendar'/>
                                    </span>
                                    <DateRangePicker
                                        calendarIcon={null}
                                        clearIcon={null}
                                        onChange={this.onDateChange}
                                        value={this.state.datesInput}
                                        className={'dateRange'}
                                    />
                                </div>
                                <div className="main-search-input-item">
                                    <div className="qty-dropdown fl-wrap">
                                        <span className="inpt_dec">
                                        <i className='fas fa-th-large'/>
                                    </span>
                                        <select data-placeholder="Category"
                                                name="categories"
                                                onChange={this.onCategoryChange}
                                                style={{
                                                    height: "50px",
                                                    width: "100%",
                                                    textIndent: "35px",
                                                    background: "white",
                                                    border: 'none'
                                                }}
                                        >
                                            <option disabled selected>Category</option>
                                            <option>All</option>
                                            {
                                                Array.isArray(categories) && categories.map((x, i) => {
                                                    return (
                                                        <option key={i} value={x._id}>{x.name}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                    </div>
                                </div>
                                <button className="main-search-button color2-bg"
                                        onClick={this.handleSubmit}>
                                    Search   &emsp;
                                    <FontAwesomeIcon icon={faSearch}/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="header-sec-link">
                    <div className="container">
                        <a href="#sec2" className="custom-scroll-link color-bg">
                            <FontAwesomeIcon icon={faAngleDoubleDown}/>
                        </a>
                    </div>
                </div>
            </HeroBanner>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        categories: state.category.categories
    }
};
const connectedComponent = connect(mapStateToProps, {getAllCategories})(SearchFormEvents);
export default withRouter(connectedComponent);

