// library
import React from 'react';
import DateRangePicker from '@wojtekmaj/react-daterange-picker'


const eventListingFilters = (props) => {


    const toggleShowOnMobile = () => {
        const container = document.getElementById('filtersContainer');
        if (container.style.display === '' || container.style.display === 'none') {
            container.style.display = 'block';
        } else {
            container.style.display = 'none';
        }
    };
    const {categories} = props;
    const {city} = props;
    return (
        <>
            <div className="mobile-list-controls fl-wrap mar-bot-cont" onClick={toggleShowOnMobile}>
                <div className="mlc show-list-wrap-search fl-wrap">
                    <i className="fa fa-filter" style={{color: 'white'}}/> Filter
                </div>
            </div>
            <div className="list-wrap-search lisfw fl-wrap lws_mobile" id={'filtersContainer'}>
                <div className="container">
                    <div className="row">
                        <div className="col-md-5ths">
                            <div className="col-list-search-input-item fl-wrap location autocomplete-container">
                                <label>Keyword</label>
                                <input
                                    type="text"
                                    placeholder="I am Looking for.."
                                    className="autocomplete-input filterDropDowns"
                                    id="autocompleteid3"
                                    style={{padding: '12px 10px 11px 10px', height: '31px'}}
                                    onChange={(e) => props.handleChange(e)}
                                    value={props.search}
                                />
                            </div>
                        </div>
                        <div className="col-sm-5ths">
                            <div className="col-list-search-input-item fl-wrap location autocomplete-container">
                                <label>Event Category</label>
                                <select data-placeholder="Category"
                                        name="categories"
                                        onChange={(e) => props.changeCategory(e)}
                                        className="chosen-select no-search-select filterDropDowns">
                                    <option disabled selected>Select Category</option>
                                    <option selected={(props.category === 'All') ? "selected" : null}>All</option>
                                    {
                                        Array.isArray(categories) && categories.map((x, i) => {
                                            return (
                                                <option
                                                    key={i}
                                                    selected={(props.category === x._id) ? "selected" : null}
                                                    value={x._id}>{x.name}</option>
                                            )
                                        })
                                    }
                                </select>

                            </div>
                        </div>
                        <div className="col-md-5ths fullWidthOnSmallScreen">
                            <div className="col-list-search-input-item fl-wrap location autocomplete-container">
                                <label>City</label>

                                <select data-placeholder="City"
                                        name="city"
                                        onChange={(e) => props.changeCity(e)}
                                        className="chosen-select no-search-select filterDropDowns">
                                    <option disabled selected>Select City</option>
                                    <option selected={(props.location === 'All') ? "selected" : null}>All</option>
                                    {
                                        Array.isArray(city) && city.map((x, i) => {
                                            return (
                                                <option key={i}
                                                        selected={(props.location === x.name) ? 'selected' : null}
                                                        value={x._id}>{x.name}</option>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="col-md-5ths">
                            <div className="col-list-search-input-item fl-wrap location autocomplete-container">
                                <label>Date </label>

                                <DateRangePicker
                                    onChange={(e) => props.changeDate(e)}
                                    value={props.date}
                                    className={'filterDateRange'}
                                    calendarIcon={null}
                                />

                            </div>
                        </div>
                        <div className="col-md-5ths" style={{top: '5px'}}>
                            <div className="col-list-search-input-item fl-wrap">
                                <button className="header-search-button" id={'searchButton'}
                                        onClick={props.handleSearch} style={{height: '42px', width:'95%' , float:'right'}}>
                                    Search
                                    <i className="fas fa-search"/></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );

};

export default eventListingFilters;

