// Library
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
// Component
import BuyTicketUserInfoForm from '../buyTicketUserInfoForm';
import BuyTicketMySelfForm from '../buyTicketMyselfForm';
import BuyTicketPassesDisplay from '../buyTicketPassesDisplay';
// Redux Action
import {
    setAssignedBillFromForm,
    removeAssignedSeatsFromDisplay,
    setAssignedBillFromFormForPasses
} from '../../redux/ticket/ticket-actions';

class BuyTicketStepTwo extends Component {

    displayFormForTicket = (arr) => {
        const display = [];
        arr.map(item => {
            if (item.ticketClassType === 'REGULAR') {
                display.push(
                    <BuyTicketUserInfoForm
                        ticketClassName={item.ticket.name}
                        seatNumber={item.seatNumber}
                        seatName={item.seatName}
                        rowNumber={item.rowNumber}
                        rowName={item.rowName}
                        ticketClassColor={item.ticket.color}
                        uniqueId={item.uniqueId}
                        onInputChange={this.onInputChange}
                    />
                );
            }
        });
        return (
            <>
                {
                        <div className="card ticketCard">
                            <div className="card-header">
                                Guest Tickets
                            </div>
                            <div className="card-body">
                                {display}
                            </div>
                        </div>
                }
            </>
        );

    };
    displayMySelfForm = (arr, passesArray) => {

        return (
            <BuyTicketMySelfForm
                seats={arr}
                passes={passesArray}
                changer={this.onMyselfChange}
            />
        );
    };


    displayPasses = (passedSeats) => {
        if (passedSeats) {
            const display = [];
            passedSeats.map((item, i) => {
                display.push(
                    <BuyTicketPassesDisplay
                        passes={item}
                        onInputChange={this.onInputChangeForPasses}
                    />
                );
            });

            return (
                <>
                    {
                            <div className="card ticketCard">
                                <div className="card-header">
                                    Guest Passes
                                </div>
                                <div className="card-body">
                                    {display}
                                </div>
                            </div>
                    }
                </>

            );
        }

        return null;
    };
    onMyselfChange = (rowAndSeat) => {
        this.props.removeAssignedSeatsFromDisplay(rowAndSeat, this.props.assignedSeats, this.props.passesAssignedSeats);
    };

    onInputChangeForPasses = (index, val, uniqueIndex, passId) => {

        this.props.setAssignedBillFromFormForPasses(index,val,uniqueIndex, passId, this.props.passesAssignedSeats);
    };


    onInputChange = (index, val, rowNumber, seatNumber, uniqueId) => {

        this.props.setAssignedBillFromForm(index, val, rowNumber, seatNumber, this.props.assignedSeats, uniqueId);
    };

    render() {
        return (
            <div className={'col-lg-12'}>


                <h4 style={{textAlign: 'left', textIndent: '15px', margin: '55px 0'}}>
                    Tickets ({this.props.eventTime})
                </h4>
                {this.displayMySelfForm(this.props.assignedSeats, this.props.passesAssignedSeats)}
                {this.displayFormForTicket(this.props.assignedSeatsForDisplay)}
                {this.props.passesAssignedSeats ?
                    (
                        <h4 style={{textAlign: 'left', textIndent: '15px', margin: '55px 0'}}>
                            Passes ({this.props.eventTime})
                        </h4>
                    ) :
                    null
                }

                {this.displayPasses(this.props.passesAssignedSeatsForDisplay)}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        processing: state.ticket.processing,
        assignedSeats: state.ticket.assignedSeats,
        totalBill: state.ticket.totalBill,
        assignedSeatsForDisplay: state.ticket.assignedSeatsForDisplay,
        passesAssignedSeats: state.ticket.passesAssignedSeats,
        passesAssignedSeatsForDisplay: state.ticket.passesAssignedSeatsForDisplay
    }
};
const connectedComponent = connect(mapStateToProps, {
    setAssignedBillFromForm,
    removeAssignedSeatsFromDisplay,
    setAssignedBillFromFormForPasses
})(BuyTicketStepTwo);

export default withRouter(connectedComponent);