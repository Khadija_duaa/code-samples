// Library
import React, {Component} from 'react';
// Component
import Header from '../../commonComponents/header';
import Footer from '../../commonComponents/footer';

class ContactUsPage extends Component {
    render() {
        return (
            <div id="main">
                <Header/>
                <div id="wrapper">
                    {/* content*/}
                    <div className="content">
                        {/* map-view-wrap */}
                        <div className="map-view-wrap">
                            <div className="container">
                                <div className="map-view-wrap_item">
                                    <div className="list-single-main-item-title fl-wrap">
                                        <h3>Contacts</h3>
                                    </div>
                                    <div className="box-widget-list mar-top">
                                        <ul>
                                            <li><span><i className="fal fa-map-marker"/> Adress :</span> <a href="#">USA
                                                27TH Brooklyn NY</a></li>
                                            <li><span><i className="fal fa-phone"/> Phone :</span> <a
                                                href="#">+7(123)987654</a></li>
                                            <li><span><i className="fal fa-envelope"/> Mail :</span> <a
                                                href="#">AlisaNoory@domain.com</a></li>
                                            <li><span><i className="fal fa-browser"/> Website :</span> <a
                                                href="#">themeforest.net</a></li>
                                        </ul>
                                    </div>
                                    <div className="list-widget-social">
                                        <ul>
                                            <li><a href="#" target="_blank"><i className="fab fa-facebook-f"/></a></li>
                                            <li><a href="#" target="_blank"><i className="fab fa-twitter"/></a></li>
                                            <li><a href="#" target="_blank"><i className="fab fa-vk"/></a></li>
                                            <li><a href="#" target="_blank"><i className="fab fa-instagram"/></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*map-view-wrap end */}
                        {/* Map */}
                        <div className="map-container fw-map2">
                            <div id="singleMap" data-latitude="40.7427837" data-longitude="-73.11445617675781"/>
                        </div>
                        {/* Map end */}
                        <div className="breadcrumbs-fs fl-wrap">
                            <div className="container">
                                <div className="breadcrumbs fl-wrap"><a href="#">Home</a><a
                                    href="#">Pages</a><span>Contacts</span></div>
                            </div>
                        </div>
                        <section id="sec1" className="grey-b lue-bg middle-padding">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-4">
                                        {/*   list-single-main-item */}
                                        <div className="list-single-main-item fl-wrap">
                                            <div className="list-single-main-item-title fl-wrap">
                                                <h3>Our Office </h3>
                                            </div>
                                            <div className="list-single-main-media fl-wrap">
                                                <img src="images/all/1.jpg" alt className="respimg"/>
                                            </div>
                                            <p>Praesent eros turpis, commodo vel justo at, pulvinar mollis eros. Mauris
                                                aliquet eu quam id ornare. Morbi ac quam enim. Cras vitae nulla
                                                condimentum, semper dolor non, faucibus dolor. </p>
                                            <div className="list-single-main-item-title fl-wrap mar-top">
                                                <h3>Working Hours </h3>
                                            </div>
                                            <ul className="cat-item">
                                                <li><a href="#">Monday to Friday</a> <span>9am - 7pm</span></li>
                                                <li><a href="#">Saturday to Sunday </a> <span>Closed</span></li>
                                            </ul>
                                        </div>
                                        {/*   list-single-main-item end */}
                                    </div>
                                    <div className="col-md-8">
                                        <div className="list-single-main-item fl-wrap">
                                            <div className="list-single-main-item-title fl-wrap">
                                                <h3>Get In Touch</h3>
                                            </div>
                                            <div id="contact-form">
                                                <div id="message"/>
                                                <form className="custom-form" action="php/contact.php"
                                                      name="contactform" id="contactform">
                                                    <fieldset>
                                                        <label><i className="fal fa-user"/></label>
                                                        <input type="text" name="name" id="name"
                                                               placeholder="Your Name *" defaultValue/>
                                                        <div className="clearfix"/>
                                                        <label><i className="fal fa-envelope"/></label>
                                                        <input type="text" name="email" id="email"
                                                               placeholder="Email Address*" defaultValue/>
                                                        <textarea name="comments" id="comments" cols={40} rows={3}
                                                                  placeholder="Your Message:" defaultValue={""}/>
                                                    </fieldset>
                                                    <button className="btn float-btn color2-bg"
                                                            style={{marginTop: '15px'}} id="submit">Send Message<i
                                                        className="fal fa-angle-right"/></button>
                                                </form>
                                            </div>
                                            {/* contact form  end*/}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="section-decor"/>
                        </section>
                        {/* section end */}
                    </div>
                    {/* content end*/}
                </div>
                <Footer/>
            </div>
        );
    }
}

export default ContactUsPage;