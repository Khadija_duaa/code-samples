// Library
import React from 'react';

const buyTicketStepOne = (props) => {
    return (

        <>
            <h4 style={{textAlign: 'left', textIndent: '30px'}}>Tickets ({props.eventTime})</h4>
            <div className='table-responsive' style={{padding: '20px'}}>
                <table className='table table-borderless customTable'>
                    <thead>
                    <tr>
                        <th>Class</th>
                        <th>Price</th>
                        <th>Available</th>
                        <th>Qty.</th>
                    </tr>
                    </thead>
                    <tbody>
                    {props.ticketClasses}
                    </tbody>
                </table>
            </div>


            <h4 style={{textAlign: 'left', textIndent: '30px'}}>Pass ({props.eventTime})</h4>
            <div className='table-responsive' style={{padding: '20px'}}>
                <table className='table table-borderless customTable'>
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Class</th>
                        <th>Price</th>
                        <th>Available</th>
                        <th>Qty.</th>
                    </tr>
                    </thead>
                    <tbody>
                    {props.passClasses}
                    </tbody>
                </table>
            </div>
        </>
    );
};


export default buyTicketStepOne;