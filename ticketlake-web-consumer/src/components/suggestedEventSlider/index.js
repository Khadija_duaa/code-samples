// Library
import React, {Component} from 'react';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {Modal, ModalHeader, ModalBody} from 'reactstrap';
import {Row, Col} from 'reactstrap';
import {
    FacebookShareButton,
    TwitterShareButton,
    FacebookIcon,
    TwitterIcon
} from 'react-share';
// Component
import SectionHeading from "../../commonComponents/sectionHeading";
import CardSlider from '../../commonComponents/cardSlider';
import DefaultCard from '../../commonComponents/defaultCard';
import CardWithImageAndWishlist from '../../commonComponents/cardWithImageAndWishlist';
import {getWishListIdsFromApi, wishListToggle} from "../../redux/wishlist/wishlist-actions";
import {getCardDates, getMaxAndMinPrice} from "../../utils/common-utils";

let isWishlist = false;

// Slider Settings
const sliderSettings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    cssEase: "linear",
    focusOnSelect: true,
    className: "center",
    centerMode: true,
    centerPadding: "50px",
    arrows: false,
    max: 10,
};

// Buttons
const buttonSettings = {
    display: true,
    mainPreviousClass: 'swiper-button-prev sw-btn',
    previousIcon: 'fa fa-arrow-left',
    mainNextClass: 'swiper-button-next sw-btn',
    nextIcon: 'fa fa-arrow-right'
};

class SuggestedEventSlider extends Component {

    state = {
        activeModal: ''
    };

    componentDidMount() {
        document.title = "Ticket Lake - Events";
        if (this.props.wishLists === null) {
            this.props.getWishListIdsFromApi();
        }
    }

    wishListToggle = (eventSlotId) => {
        if (this.props.auth) {
            this.props.wishListToggle(eventSlotId);
        }
    };
    sharingSocial = (id) => {
        if (id) {
            this.setState({activeModal: id});
        } else {

            this.setState({activeModal: ''});
        }
    };
    render() {
        const {heading, text, cards} = this.props;
        const onChange = (e, eventSlotId) => {
            e.preventDefault();
            // window.location.reload();
            this.props.history.push(`/event/detail/${eventSlotId}`)
        };
        const cardDisplay = [];
        let shareUrl = 'http://google.com/';

        if (cards.length <= 4) {
            let lgGrid = null;
            switch (cards.length) {
                case 1:
                    lgGrid = 3;
                    break;
                case 2:
                    lgGrid = 6;
                    break;
                case 3:
                    lgGrid = 4;
                    break;
                case 4:
                    lgGrid = 3;
                    break;
                default:
                    lgGrid = 12
            }
            Array.isArray(cards) && cards.map((card, i) => {
                shareUrl = window.location.protocol + '//' + window.location.host + '/event/detail/' + card.eventSlotId;
                if (this.props.auth) {
                    isWishlist = this.props.wishLists && this.props.wishLists !== '' && this.props.wishLists.includes(card.eventSlotId);
                }
                cardDisplay.push(
                    <>
                        <DefaultCard key={i}
                                     gridLg={lgGrid}
                                     gridMd={12}
                                     gridSm={12}
                                     auth={this.props.auth}
                                     cardTitle={card.eventTitle}
                                     image={card.bannerImageKey.imageUrl}
                                     cardLink={'#'}
                                     dates={getCardDates(card.eventDateTimeSlot)}
                                     isWishList={isWishlist}
                                     wishlistLink={() => this.wishListToggle(card.eventSlotId)}
                                     cardAddress={card.venue ? card.venue.address : ''}
                                     country={card.venue ? card.venue.country : []}
                                     city={card.venue ? card.venue.city : []}
                                     onClick={() => this.props.history.push(`/event/detail/${card.eventSlotId}`)}
                                     buttonText={getMaxAndMinPrice(card)}
                                     buttonLink={`/buy-ticket/${card.eventSlotId}`}
                                     sharing={this.sharingSocial}
                                     id={card._id}
                        />
                        <Modal isOpen={this.state.activeModal === card._id} toggle={this.sharingSocial}
                               className={this.props.className}>
                            <ModalHeader toggle={this.sharingSocial}>{card.eventTitle}</ModalHeader>
                            <ModalBody>
                                <h3>Share The Event!!</h3>
                                <Row>
                                    <Col md={{size: 2, offset: 4}}>
                                        <FacebookShareButton
                                            url={shareUrl}
                                            quote={card.eventTitle}
                                            className="Demo__some-network__share-button">
                                            <FacebookIcon
                                                size={64}
                                                round/>
                                        </FacebookShareButton>
                                    </Col>
                                    <Col md={{size: 2}}>
                                        <TwitterShareButton
                                            url={shareUrl}
                                            title={card.eventTitle}
                                            className="Demo__some-network__share-button">
                                            <TwitterIcon
                                                size={64}
                                                round/>
                                        </TwitterShareButton>
                                    </Col>
                                </Row>
                            </ModalBody>
                        </Modal>
                    </>
                )
            });
        } else {
            Array.isArray(cards) && cards.map((card, i) => {
                shareUrl = window.location.protocol + '//' + window.location.host + '/event/detail/' + card.eventSlotId;

                if (this.props.auth) {
                    isWishlist = this.props.wishLists && this.props.wishLists !== '' && this.props.wishLists.includes(card.eventSlotId);
                }
                cardDisplay.push (

                    <div className="slick-slide-item" key={card._id}>
                        <>
                            <DefaultCard key={i}
                                         gridLg={12}
                                         gridMd={12}
                                         gridSm={12}
                                         auth={this.props.auth}
                                         cardTitle={card.eventTitle}
                                         image={card.bannerImageKey.imageUrl}
                                         cardLink={'#'}
                                         dates={getCardDates(card.eventDateTimeSlot)}
                                         isWishList={isWishlist}
                                         wishlistLink={() => this.wishListToggle(card.eventSlotId)}
                                         cardAddress={card.venue ? card.venue.address : ''}
                                         country={card.venue ? card.venue.country : []}
                                         city={card.venue ? card.venue.city : []}
                                         onClick={() => this.props.history.push(`/event/detail/${card.eventSlotId}`)}
                                         buttonText={getMaxAndMinPrice(card)}
                                         buttonLink={`/buy-ticket/${card.eventSlotId}`}
                                         sharing={this.sharingSocial}
                                         id={card._id}
                            />
                            <Modal isOpen={this.state.activeModal === card._id} toggle={this.sharingSocial}
                                   className={this.props.className}>
                                <ModalHeader toggle={this.sharingSocial}>{card.eventTitle}</ModalHeader>
                                <ModalBody>
                                    <h3>Share The Event!!</h3>
                                    <Row>
                                        <Col md={{size: 2, offset: 4}}>
                                            <FacebookShareButton
                                                url={shareUrl}
                                                quote={card.eventTitle}
                                                className="Demo__some-network__share-button">
                                                <FacebookIcon
                                                    size={64}
                                                    round/>
                                            </FacebookShareButton>
                                        </Col>
                                        <Col md={{size: 2}}>
                                            <TwitterShareButton
                                                url={shareUrl}
                                                title={card.eventTitle}
                                                className="Demo__some-network__share-button">
                                                <TwitterIcon
                                                    size={64}
                                                    round/>
                                            </TwitterShareButton>
                                        </Col>
                                    </Row>
                                </ModalBody>
                            </Modal>
                        </>
                    </div>
                )
            })
        }

        return (
            <section className="light-red-bg">
                <SectionHeading
                    heading={heading}
                    text={text && text ? text : ''}
                />

                {
                    cards && cards.length < 4 ?
                        <div className={"container"}>
                            <div className={'row'}>
                                {cardDisplay}
                            </div>

                        </div>
                        :
                        <div className="list-carousel fl-wrap card-listing ">
                            <div className="listing-carousel  fl-wrap ">
                                <CardSlider settings={sliderSettings} buttons={buttonSettings}>
                                    {cardDisplay}
                                </CardSlider>
                            </div>
                        </div>
                }

            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        wishLists: state.wishlist.wishListIds,
        auth: state.user.authenticated
    }
};

const connectedComponent = connect(mapStateToProps, {
    getWishListIdsFromApi,
    wishListToggle
})(SuggestedEventSlider);
export default withRouter(connectedComponent);
