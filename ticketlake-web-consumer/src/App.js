// Library
import React, {Component} from 'react';
import {Redirect, BrowserRouter, Route, Switch} from 'react-router-dom';
import {NotificationContainer} from 'react-notifications';
import PathRegExp from 'path-to-regexp';
// Components
import Layout from './components/layout';
// Auth Routes
import AuthRoutes from './components/authRoutes';
// Pages
import AboutUsPage from './components/aboutUsPage';
import ContactUsPage from './components/contactUsPage';
import EventListing from './components/eventListingPage';
import EventDetail from './components/eventDetailPage';
import Authentication from './components/authentication';
import Verification from './components/verificationPage';
import BuyTicketPage from './components/buyTicketPage';
import ChangePassword from './components/userChangePassword';
import Wishlist from './components/userWishlistPage';
import Tickets from './components/userTicketsPage';
import Wallet from './components/userWalletPage';
import WalletTopUp from './components/userWalletTopUp';
import UserProfile from './components/userProfilePage';
import ForgotPassword from './components/forgotPasswordPage';
import Movies from './components/moviesPage';
import Sports from './components/sportsPage';
import PromotedEvents from './components/promotedEventsPage';

// CSS
import './App.css';
import 'react-notifications/lib/notifications.css';

class App extends Component {
    render() {
        return (
            <div>
                <BrowserRouter>
                    <Switch>
                        <Route path="/events/listing" name={"Events"} component={EventListing}/>
                        <Route path="/buy-ticket/:id" name={"BuyTicket"} component={BuyTicketPage}/>
                        <Route path="/contact-us" name={"ContactUs"} component={ContactUsPage}/>
                        <Route path="/about-us" name={"AboutUs"} component={AboutUsPage}/>
                        <Route path="/user/change-password" name={"ChangePassword"} component={ChangePassword}/>
                        <Route path="/user/wishlist" name={"UserWishList"} component={Wishlist}/>
                        <Route path="/user/ticket" name={"UserTickets"} component={Tickets}/>
                        <Route path="/user/wallet/top-up" name={"UserWalletTopUp"} component={WalletTopUp}/>
                        <Route path="/user/wallet" name={"UserWallet"} component={Wallet}/>
                        <Route path="/user/profile" name={"UserProfile"} component={UserProfile}/>
                        <Route path="/event/detail/:id" name={"EventDetail"} component={EventDetail}/>
                        <Route path="/movies" name={"Movies"} component={Movies}/>
                        <Route path="/sports" name={"Sports"} component={Sports}/>
                        <Route path="/events/promoted" name={"PromotedEvents"} component={PromotedEvents}/>
                        <Route path="/sign-up/verification" name={"Register"} component={Verification}/>
                        <Route path="/authentication/forgot-password" name={"ForgotPassword"} component={ForgotPassword}/>
                        <Route path="/authentication" name={"SignIn"} component={Authentication}/>
                        <Route path="/" exact name={"Home"} component={Layout}/>
                        <Redirect to="/"/>
                    </Switch>
                    <NotificationContainer/>
                </BrowserRouter>



            </div>
        );
    }
}


export default App;
