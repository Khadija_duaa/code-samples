import axios from 'axios';
import {SERVER_URL} from './config';
import store from '../redux/store';

const getOptions = () => {
    return {
        headers: {
            'X-Auth': store.getState().user.token
        }
    };
};


const prepareUrl = (api) => (SERVER_URL + api);

const wrapper = {
    get: (api) => axios.get(prepareUrl(api), getOptions()),
    post: (api, formData = {}) => axios.post(prepareUrl(api), formData, getOptions()),
    put: (api, formData = {}) => axios.put(prepareUrl(api), formData, getOptions()),
    delete: (api) => axios.delete(prepareUrl(api), getOptions()),
};

export default wrapper;