// library
import moment from 'moment';
import _ from 'lodash';

export const getObjectValue = (obj, path) => {
    let val = null;

    if (path.indexOf('.') > -1) {
        let paths = path.split('.');
        val = obj;
        paths.forEach(_path => {
            val = val[_path];
        });
    } else {
        val = obj[path];
    }

    return val;
};


export const nameSplitter = (name) => {
    let nameSplitter = name.split(' ');
    return nameSplitter[0];
};

export const dateSplitter = (date) => {
    let newDate = moment(date).format();
    let dateSplitter = newDate.split('T');
    return dateSplitter[0]
};

export const timeSplitter = (date) => {
    let newDate = moment(date).format();
    let dateSplitter = newDate.split('T');
    return dateSplitter[1]
};

export const getTimeFromISO = (iso) => {
    return moment(iso).format('LT');
};


export const getDateFromISO = (iso) => {
    return moment(iso).format('LL');
};

export const getDateAndTimeFromIso = (iso) => {
    return moment(iso).format('LLL');
};

export const getMaxAndMinPrice = (arr) => {
    if (!arr.parentEventInfo) {
        return "Buy Ticket";
    } else {
        const currency = arr.parentEventInfo && arr.parentEventInfo.currency;
        const prices = [];
        _.forEach(arr.ticketClasses, (item) => {
            prices.push(item.ticketClassPrice);
        });
        const max = _.max(prices);
        const min = _.min(prices);
        if (max === min) {
            return currency + max;
        } else {
            return currency + min + ' - ' + currency + max;
        }
    }
};

export const getCardDates = (dateSlots) => {
    return moment(dateSlots.eventStartTime).format('LL') + ' - ' + moment(dateSlots.eventEndTime).format('LL');
};
export const isUndefined = (obj) => {
    return typeof obj === 'undefined';
};

export const isNullOrEmpty = (val) => {
    return (
        val === undefined
        ||
        val === null
        ||
        val.length <= 0
    );
};

export const getCountries = () => {
    const countries = require('./countries');
    return (_.keys(countries)).sort();
};

export const getCities = (country) => {
    const countries = require('./countries');
    return (countries[country]).sort();
};

export const getRandom = (arr, n) => {
    if (n > arr.length) {
        return arr;
    }
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len)
        throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
};