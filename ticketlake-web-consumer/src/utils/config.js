// const BASE_SERVER_URL = 'https://24c67c24.ngrok.io/';
const BASE_SERVER_URL = 'http://192.168.1.161:5000/';
const SERVER_API_PREFIX = 'api/';
const SERVER_API_VERSION = 'v1';

export const SERVER_URL = BASE_SERVER_URL + SERVER_API_PREFIX + SERVER_API_VERSION;
export const STANDARD_EVENT = 'STANDARD';
export const SERIES = 'SERIES';
export const RECUR = 'RECUR';

// API URLs

// WishLists
export const WISHLIST_API_GET_IDS = '/consumers/get-wishlist-eventIds';
export const WISHLIST_API_TOGGLE = '/consumers/add-event-wishlist';
export const GET_WISHLIST_EVENTS = '/consumers/get-wishlist-events';

// Events
export const EVENTS_GET_EVENT_DETAIL = '/events/event-detail/';
export const EVENTS_GET_ALL_PUBLIC = '/events/get-all-events-public';

// User
export const USER_UPLOAD_PROFILE_PICTURE = '/consumers/image-upload';
export const USER_LOGIN = '/consumers/login';
export const USER_UPDATE_PROFILE = '/consumers/update-profile';
export const USER_VERIFY = '/consumers/verify';
export const UPDATE_USER = '/consumers/verification';
export const USER_REGISTER = '/consumers/register/';
export const USER_CHECK_OTP = '/consumers/check-otp/';
export const USER_FORGOT_PASSWORD = '/consumers/forgot-password/';
export const USER_GET_ALL_TICKETS = '/tickets/get-my-tickets';
export const USER_TOP_UP_WALLET = '/consumers/topup-balance';
export const USER_VIEW_PROFILE = '/consumers/view-profile';
export const USER_CHANGE_PASSWORD = '/consumers/change-password';

// Tickets
export const TICKET_PURCHASE = '/tickets/purchase-ticket-consumer';

// Passes
export const PASSES_GET_SEATS = '/events/get-seats-passSlots';

// Tickets Purchase
export const CLIENT_ID_GET = '';

// Wallet
export const GET_WALLET_TRANSACTION_HISTORY = '/consumers/get-transaction-history';

// Useful Constants
export const TITLE_SIZE = 35;
export const TICKET_CLASS_REGULAR = 'REGULAR';
export const TICKET_CLASS_PASS = 'PASS';


// Useful JSON

export const GET_CITIES_FOR_FILTERS = [
    {name: 'Islamabad'},
    {name: 'Lahore'},
    {name: 'Faisalabad'},
    {name: 'Ghana'},
    {name: 'Multan'}
];