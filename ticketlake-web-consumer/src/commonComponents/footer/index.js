// Library
import React, {Component} from 'react';
import Alert from 'react-bootstrap/Alert'
import {
    faMapMarker,
    faDotCircle,
    faCalendarCheck,
    faUsers,
    faMale,
    faChild,
    faAngleDoubleDown,
    faFacebook,
    faSearch, faStar
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import {NotificationManager} from 'react-notifications';


// Components
import SubFooter from '../../commonComponents/subFooter';
import Axios from 'axios';
// Style
const footerLinkStyle = {
    color: '#ACAEB2',
    textAlign: 'left',
    width: '100%',
    float: 'left',
    margin: '2px 0',
    fontWeight: 400,
    fontFamily: ['Montserrat', 'sans-serif'].join(',')
};

const socialMediaStyle = {
    color: 'white',
    textAlign: 'left',
    width: '100%',
    float: 'left',
    margin: '2px 0',
    fontWeight: 400,
    fontFamily: ['Montserrat', 'sans-serif'].join(',')
};


class Footer extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            subscribeValue:'',
        }
    }
    
    subscribeNowValue=(e)=>{
        // console.log('sss', e.target.value)
        this.setState({
            subscribeValue:e.target.value
        })
    };

    subscribeNow=(value)=>{
        var subscribe_email = this.state.subscribeValue
        console.log('subscribe_email', subscribe_email)
        if(!value){
        }else{
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(subscribe_email)){
                    Axios.get(`https://ticketlake.herokuapp.com/api/v1/subscriptions/subscribe-me/${subscribe_email}`)
                    .then((response)=>{
                        console.log(response.data);
                        NotificationManager.success("You are successfully subscribed!", '', 3000);
                    })
                    .catch((error)=>{
                        console.log(error.response.data._error);
                        NotificationManager.error(error.response.data._error)
                    });
                }else{
                    // alert('Invalid email!')
                    NotificationManager.error("Invalid email!", '', 3000);
                }
        }
        
    }
    render() {
        return (
            <footer className="main-footer">
                <div className="footer-inner">
                    <div className="container">
                        <div className="row">

                            <div className="col-md-4 col-lg-4 col-xl-3" span={8}>
                                {/*<img src={window.location.origin + '/images/white_logo.png'} alt={'logo'}*/}
                                     {/*style={{width: '100%'}}/>*/}

                                <div className="footer-widget fl-wrap">
                                    <h3>Ticketlake</h3>
                                    <div className='pull-left'>
                                        <p style={{textAlign: 'left', color:"#ACAEB2"}}>Ticketlake is an amazing platform to help users purchase tickets globally with great range of coupons and promotions</p>
                                    </div>
                                </div>
                                <ul className="footer-contacts fl-wrap red-text">
                                    <li>
                                        <span>
                                            {/*<i className="far fa-envelope"/>*/}
                                        </span>
                                        <a href="#" target="_blank">ask@ticketlake.com</a>
                                    </li>
                                    <li>
                                        <span>
                                            {/*<i className="fas fa-map-marker-alt"/>*/}
                                        </span>
                                        <a href="#" target="_blank">
                                            54- A Turebergs alle 4, Sollentuna, Sweden
                                        </a>
                                    </li>
                                    <li>
                                        <span>
                                            {/*<i className="fas fa-phone"/>*/}
                                        </span>
                                        <a href="#">
                                            +21 (324) 6545343
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div className="col-md-4 col-lg-4 col-xl-2 offset-xl-1" span={5} style={{marginBottom:'17px'}}>
                                <div className="footer-widget fl-wrap">
                                    <h3>Explore</h3>
                                    <div className='pull-left'>
                                        <ul className="footer-contacts-custom">
                                            <li>    
                                                <a href="#" style={footerLinkStyle}>About Ticketlake</a>
                                            </li>
                                            <li>
                                                <a href="#" style={footerLinkStyle}>Services</a>
                                            </li>
                                            <li>
                                                <a href="#" style={footerLinkStyle}>Clients & Partners</a>
                                            </li>
                                            <li>
                                                <a href="#" style={footerLinkStyle}>Events Organisers</a>
                                            </li>
                                            <li>
                                                <a href="#" style={footerLinkStyle}>Gigs</a>
                                            </li>
                                            <li>
                                                <a href="#" style={footerLinkStyle}>News & Articles</a>
                                            </li>
                                            <li>
                                                <a href="#" style={footerLinkStyle}>International</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-lg-4 col-xl-2 offset-xl-1" span={5} style={{marginBottom:'17px'}}>
                                <div className="footer-widget fl-wrap">
                                    <h3>Events</h3>
                                </div>
                                <div className='pull-left'>
                                    <ul className="footer-contacts-custom">
                                        <li>    
                                            <a href="#" style={footerLinkStyle}>Arts</a>
                                        </li>
                                        <li>
                                            <a href="#" style={footerLinkStyle}>Food</a>
                                        </li>
                                        <li>
                                            <a href="#" style={footerLinkStyle}>Games</a>
                                        </li>
                                        <li>
                                            <a href="#" style={footerLinkStyle}>Exhibition</a>
                                        </li>
                                        <li>
                                            <a href="#" style={footerLinkStyle}>Music</a>
                                        </li>
                                        <li>
                                            <a href="#" style={footerLinkStyle}>Movies</a>
                                        </li>
                                        <li>
                                            <a href="#" style={footerLinkStyle}>Sports</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-md-5 col-lg-5 col-xl-3" span={6}>
                                <div className="footer-widget fl-wrap">
                                    <h3>Social Media</h3>
                                </div>

                                <div className="footer-social-btns">
                                    <ul>
                                        <li>
                                            <a href="#" style={socialMediaStyle}><i className="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" style={socialMediaStyle}><i className="fab fa-instagram"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" style={socialMediaStyle}><i className="fab fa-google"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" style={socialMediaStyle}><i className="fab fa-twitter"></i></a>
                                        </li>
                                    </ul>
                                </div>

                                    <div className="footer-widget fl-wrap">
                                        <h3 style={{marginTop:'70px'}}>Subscribe</h3>
                                    </div>
                                    <div className="subscribe-form">
                                        <Input type="email" name="email" id="examplePassword" placeholder="Enter your email"
                                               onChange={(value)=>this.subscribeNowValue(value)}
                                               style={{fontFamily:'Roboto', fontStyle:'italic'}}
                                               className="autocomplete-input"/>
                                        <button onClick={this.subscribeNow} className="color2-bg subscribe-btn" style={{background: '#494949'}}>Subscribe</button>
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="footer-bg"/>
                <SubFooter/>
            </footer>

        );
    }
}

export default Footer;