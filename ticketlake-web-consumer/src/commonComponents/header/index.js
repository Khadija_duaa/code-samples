// Library
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavLink} from "react-router-dom";
// Components
import HeaderNavMenu from '../../commonComponents/headerNavMenu';
// Helper
import {nameSplitter} from '../../utils/common-utils';
// Logo
import Logo from '../../commonComponents/logo';
// Redux
import {logout} from '../../redux/user/user-actions';
import {setLanguage} from '../../redux/multilingual/multilingual-actions';
import {HEADING_FONT} from "../../utils/css-utils";

class Header extends Component {

    logoutUser = () => {
        this.props.logout();
    };

    showDropDownAccount = (event) => {
        let parent = document.getElementById('headerNavUserMenu').parentNode;
        if (parent.children[1].className === 'hu-menu-vis') {
            parent.children[1].className = '';
            parent.children[0].className = 'header-user-name';
        } else {
            parent.children[1].className = 'hu-menu-vis';
            parent.children[0].className = 'header-user-name hu-menu-visdec';
        }

    };

    render() {
        let {activeUser} = this.props;
        return (

            <>
                <header className="main-header">
                    <div className="header-top fl-wrap">
                        <div className="container">
                            <Logo
                                logoLink={'/'}
                                logoImage={window.location.origin + '/images/logo.jpg'}
                            />
                            {
                                activeUser ?
                                    <div className="header-user-menu">

                                        <div className="header-user-name" id='headerNavUserMenu' onClick={(event) => {
                                            this.showDropDownAccount(event)
                                        }}>
                                            <span><img
                                                src={this.props.profileImage && this.props.profileImage.imageUrl ? this.props.profileImage.imageUrl : '/images/default-dp.png'}
                                                alt={''}/></span>
                                            {nameSplitter(activeUser.name)}
                                        </div>

                                        <ul>
                                            <li><NavLink to="/user/profile"> Profile</NavLink></li>
                                            <li className={"hide-on-web"}><NavLink to="user/wishlist"> Wishlist</NavLink></li>
                                            <li><NavLink to="/" onClick={this.logoutUser}>Sign Out</NavLink></li>
                                        </ul>
                                    </div>
                                    :
                                    <div className="show-reg-form modal-open">

                                        <div key={2} onClick={() => {
                                            window.location.href = '/authentication'
                                        }}>
                                            <i className="fas fa-sign-in-alt"/> Sign In
                                        </div>

                                    </div>
                            }

                            {/*<div className="lang-wrap">*/}
                                {/*<div className="show-lang"><img src={window.location.origin + "/images/lan/1.png"}*/}
                                                                {/*alt='ENG'/>*/}
                                    {/*<span>Eng</span>*/}
                                    {/*<i className='fas fa-caret-down'/>*/}
                                {/*</div>*/}
                                {/*<ul className="lang-tooltip green-bg">*/}
                                    {/*<li><a href="#"><img src={window.location.origin + "/images/lan/4.png"}*/}
                                                         {/*alt='LANG'/> De</a>*/}
                                    {/*</li>*/}
                                    {/*<li><a href="#"><img src={window.location.origin + "/images/lan/5.png"}*/}
                                                         {/*alt='LANG'/> It</a>*/}
                                    {/*</li>*/}
                                    {/*<li><a href="#"><img src={window.location.origin + "/images/lan/2.png"}*/}
                                                         {/*alt='LANG'/> Fr</a>*/}
                                    {/*</li>*/}
                                    {/*<li><a href="#"><img src={window.location.origin + "/images/lan/3.png"}*/}
                                                         {/*alt='LANG'/> Es</a>*/}
                                    {/*</li>*/}
                                {/*</ul>*/}
                            {/*</div>*/}


                            {/*<div className="currency-wrap">*/}
                                {/*<div className="show-currency-tooltip">*/}
                                    {/*<i className='fas fa-dollar-sign'/>*/}
                                    {/*<span>USD <i className='fas fa-caret-down'/></span></div>*/}
                                {/*<ul className="currency-tooltip">*/}
                                    {/*<li><a href="#"><i className='fas fa-euro-sign'/>EUR</a></li>*/}
                                    {/*<li><a href="#"><i className='fas fa-pound-sign'/> GBP</a></li>*/}
                                    {/*<li><a href="#"><i className='fas fa-ruble-sign'/> RUR</a></li>*/}
                                {/*</ul>*/}
                            {/*</div>*/}

                            {
                                activeUser ?
                                    <div className="show-reg-form modal-open hide-on-mobile" style={{height: '42px', width: '103px', marginRight:'10px'}}>
                                        <NavLink to="/user/wishlist">
                                            <span >
                                                <img
                                                    src={'/images/heart.svg'}
                                                    style={{height: '50%', width: '50%'}}
                                                    alt={''}/>
                                            </span>
                                            <span style={{color: 'black', fontFamily: HEADING_FONT}}>Wishlist</span>
                                        </NavLink>
                                    </div>
                                    :
                                    null
                            }

                        </div>
                    </div>

                    <div className="header-inner fl-wrap">
                        <div className="container">
                            <HeaderNavMenu/>
                        </div>
                    </div>

                </header>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        activeUser: state.user.user,
        authenticated: state.user.authenticated,
        profileImage: state.user.profileImage,
        activeLanguage: state.multilingual.activeLanguage
    }
};

const connected = connect(mapStateToProps, {logout, setLanguage})(Header);
export default connected;