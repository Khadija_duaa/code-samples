// Library
import React from 'react';
import {NavLink} from "react-router-dom";
import {DropdownItem, DropdownMenu, DropdownToggle, NavItem, UncontrolledDropdown} from "reactstrap";

const menuNavItem = (props) => {

    let mapSubmenu = [];
    let displaySubmenu = [];

    if (props.subMenus) {
        props.subMenus.map((item, i) => (
            mapSubmenu.push(<DropdownItem><NavLink to={item.menuLink}>{item.menuName}</NavLink></DropdownItem>)
        ));

        displaySubmenu.push(<UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                {props.menuName}
            </DropdownToggle>
            <DropdownMenu right>
                {mapSubmenu}
            </DropdownMenu>
        </UncontrolledDropdown>);
    } else {
        displaySubmenu.push(<NavItem key={1}>
            <NavLink to={props.menuLink}>{props.menuName}</NavLink>
        </NavItem>);
    }
    return (

        <>
            {displaySubmenu}
        </>

    );
};

export default menuNavItem;