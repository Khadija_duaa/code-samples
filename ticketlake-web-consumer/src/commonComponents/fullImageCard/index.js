// Library
import React from 'react';
import {NavLink} from "react-router-dom";
import { Badge } from 'reactstrap';

const fullImageCard = (props) => {
    let ratingDisplay = [];
    for (var i = 0; i < props.rating; i++) {
        ratingDisplay.push(<i className='fa fa-star'/>);
    }
    console.log("Props Tags:", props.tags);
    return (
        <div className="slick-slide-item">
            <div className="hotel-card fl-wrap title-sin_item">
                <div className="geodir-category-img card-post">
                    <NavLink to={props.cardLink ? props.cardLink: '/'}>
                        <img src={props.cardImage? props.cardImage : '/images/card_3.png'} alt={'image'}/>
                    </NavLink>
                    {/* <div className="listing-counter">
                        {props.price}
                    </div> */}
                    <div className="geodir-category-opt">
                        {/* <div className="listing-rating card-popup-rainingvis">
                            {ratingDisplay}
                        </div> */}
                        <h4 className="title-sin_map">
                            <NavLink to={props.cardLink ? props.cardLink: '/'}>
                                {props.items}
                            </NavLink>
                        </h4>
                        <div className="geodir-category-location">
                            {props.tags.map((tag, i)=>{
                                return (
                                    <a href="#" className="single-map-item space" key={i}>
                                        {tag.name} {i === props.tags.length-1 ? " " : "-"}
                                    </a>
                                )
                            })}
                        </div>
                        {/* <div className="rate-class-name">
                            <div className="score">
                                <strong> Good</strong>8 Reviews
                            </div>
                            <span>{props.rating}</span>
                        </div> */}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default fullImageCard;