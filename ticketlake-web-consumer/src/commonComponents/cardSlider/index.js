// Library
import React, {Component} from "react";
import Slider from "react-slick";

export default class CardSlider extends Component {
    constructor(props) {
        super(props);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
    }

    next() {
        this.slider.slickNext();
    }

    previous() {
        this.slider.slickPrev();
    }

    render() {
        const propsSettings = {
            dots: this.props.settings.dots,
            infinite: this.props.settings.infinite,
            speed: this.props.settings.speed,
            slidesToShow: this.props.settings.slidesToShow,
            slidesToScroll: this.props.settings.slidesToScroll,
            initialSlide: this.props.settings.initialSlide,
            autoplay: this.props.settings.autoplay,
            autoplaySpeed: this.props.settings.autoplaySpeed,
            cssEase: this.props.settings.cssEase,
            focusOnSelect: this.props.settings.focusOnSelect,
            className: this.props.settings.className,
            centerMode: this.props.settings.centerMode,
            centerPadding: this.props.settings.centerPadding,
            arrows: this.props.settings.arrows,

            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        initialSlide: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };

        return (
            <div>
                <Slider ref={c => (this.slider = c)}  {...propsSettings}>
                    {this.props.children}
                </Slider>
                {this.props.buttons.display ? (
                    <div>
                        <div className={this.props.buttons.mainPreviousClass} onClick={this.previous}>
                            <i className={this.props.buttons.previousIcon}/>
                        </div>
                        <div className={this.props.buttons.mainNextClass} onClick={this.next}>
                            <i className={this.props.buttons.nextIcon}/>
                        </div>
                    </div>
                ) : ''}
            </div>
        );
    }
}