// Library
import React from 'react';
// Components
import ThreeStarIcons from '../../commonComponents/threeStarIcons';

const sectionHeading = (props) => {
    return (

        <div className={props.mainClass ? props.mainClass : "section-title"} style={props.style?props.style:null}>
            {/*<ThreeStarIcons/>*/}
            <h2 style={{fontSize: props.fontSize + 'px', color: props.headingColor}}>{props.heading}</h2>
            <span className="section-separator"/>
            <p style={{color: props.textColor}}>{props.text}</p>
        </div>
    );
};

export default sectionHeading;