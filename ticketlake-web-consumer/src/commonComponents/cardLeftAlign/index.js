import React from 'react';
import Heading from '../../commonComponents/heading';

const array = [
    {id: '1'},
    {id: '2'},
    {id: '3'},
    {id: '4'},
    {id: '5'},
    {id: '6'},
];


const cardLeftAlign = (props) => {
    return (
        <section id="sec2" style={{paddingTop: '30px'}}>
            <div style={{width: '88%', paddingLeft: '5%'}}>
                <Heading
                    style={{marginBottom:'0px', textAlign:'left'}}
                    heading={props.heading}
                    text={props.text}
                />
                <div className={"row"}>
                    {
                        array.map( data => {
                            return(
                                <div className={"col-lg-2 col-md-4 col-sm-12"}>
                                    <div className="listing-item custom-listing" style={{width: "100%"}}>
                                        <article className="geodir-category-listing fl-wrap">
                                            <div className="geodir-category-img">
                                                <a href={"#"}>
                                                    <img src={'/images/Image 97.png'} alt={"image"}/>
                                                </a>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            )
                        })
                    }


                </div>
            </div>
        </section>
    )
};

export default cardLeftAlign