// Library
import React from 'react';
import {NavLink} from "react-router-dom";
// Component
import Header from "../header";
import Footer from "../footer";

const error = (props) => {
    console.log("PROPS: " , props);
    return (
        <div id="main">
            <Header/>
            <div id="wrapper">
                <div className="content">
                    <section className="color-bg parallax-section">
                        <div className="city-bg"/>
                        <div className="cloud-anim cloud-anim-bottom x1"><i className="fal fa-cloud"/></div>
                        <div className="cloud-anim cloud-anim-top x2"><i className="fal fa-cloud"/></div>
                        <div className="overlay op1 color3-bg"/>
                        <div className="container">
                            <div className="error-wrap">
                                <h2>404</h2>
                                <p>We're sorry, but the Page you were looking for, couldn't be found.</p>
                                <div className="clearfix"/>
                                <div className="clearfix"/>
                                <p>Or</p>
                                <NavLink to="/" className="btn color2-bg flat-btn">
                                    Back to Home Page
                                    <i className="fas fa-home"/>
                                </NavLink>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <Footer/>
        </div>

    );
};

export default error;