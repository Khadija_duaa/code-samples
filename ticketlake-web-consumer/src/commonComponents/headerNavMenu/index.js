// Library
import React, {Component} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';

// Component
import MenuNavItem from '../../commonComponents/menuNavItem';
// Data Array
const navMenus = [
    {
        menuName: 'Event Listing',
        menuLink: "/events/listing",
    },
/*    {
        menuName: 'Entertainment',
        menuLink: "/",
        subMenus: [
            {
                menuName: 'Theatre & Arts',
                menuLink: '/'
            },
            {
                menuName: 'Music',
                menuLink: '/'
            },
            {
                menuName: 'Dance',
                menuLink: '/'
            },
            {
                menuName: 'Gigs',
                menuLink: '/'
            },
            {
                menuName: 'Concerts',
                menuLink: '/'
            },
            {
                menuName: 'Party',
                menuLink: '/'
            },
            {
                menuName: 'Shows',
                menuLink: '/'
            },
            {
                menuName: 'Culture',
                menuLink: '/'
            },
            {
                menuName: 'Fun & Games',
                menuLink: '/'
            },
            {
                menuName: 'Festival',
                menuLink: '/'
            },
            {
                menuName: 'Exhibition',
                menuLink: '/'
            },
            {
                menuName: 'Seasonal',
                menuLink: '/'
            },
            {
                menuName: 'Premiere',
                menuLink: '/'
            },
            {
                menuName: 'Magic',
                menuLink: '/'
            },
            {
                menuName: 'Award Ceremony',
                menuLink: '/'
            }


        ]
    },
    {
        menuName: 'Movies',
        menuLink: "/movies"
    },
    {
        menuName: 'Business',
        menuLink: "/",
        subMenus: [
            {
                menuName: 'Corporate',
                menuLink: '/'
            },
            {
                menuName: 'Trade',
                menuLink: '/'
            },
            {
                menuName: 'Launch',
                menuLink: '/'
            },
            {
                menuName: 'Press',
                menuLink: '/'
            },
            {
                menuName: 'Networking',
                menuLink: '/'
            },
            {
                menuName: 'Promotional',
                menuLink: '/'
            },
            {
                menuName: 'Packages',
                menuLink: '/'
            }
        ]
    },
    {
        menuName: 'Tech & Education',
        menuLink: "/",
        subMenus: [
            {
                menuName: 'Seminar',
                menuLink: '/'
            },
            {
                menuName: 'Campus Events',
                menuLink: '/'
            },
            {
                menuName: 'Science',
                menuLink: '/'
            },
            {
                menuName: 'Religion',
                menuLink: '/'
            },
            {
                menuName: 'Training',
                menuLink: '/'
            },
            {
                menuName: 'Conference',
                menuLink: '/'
            },
            {
                menuName: 'Networking',
                menuLink: '/'
            }

        ]
    },
    {
        menuName: 'Sports',
        menuLink: "/sports"
    },
    {
        menuName: 'Tour',
        menuLink: "/",
        subMenus: [
            {
                menuName: 'Packages',
                menuLink: '/'
            },
            {
                menuName: 'Adventure & Outdoors',
                menuLink: '/'
            }
        ]
    },
    {
        menuName: 'More',
        menuLink: "/",
        subMenus: [
            {
                menuName: 'Health & Wellness',
                menuLink: '/'
            },
            {
                menuName: 'Charity',
                menuLink: '/'
            },
            {
                menuName: 'Politics',
                menuLink: '/'
            },
            {
                menuName: 'Food',
                menuLink: '/'
            },
            {
                menuName: 'Special Events',
                menuLink: '/'
            },
            {
                menuName: 'Fund Raising',
                menuLink: '/'
            },
            {
                menuName: 'Social',
                menuLink: '/'
            },

        ]
    },*/
    {
        menuName: 'About',
        menuLink: "/about-us",
    },
    {
        menuName: 'Contact Us',
        menuLink: "/contact-us",
    }
    // {
    //     menuName: 'International',
    //     menuLink: "/",
    // },
];

class HeaderNavMenu extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }


    render() {

        const displayMenu = navMenus.map((item, i) => (
            <MenuNavItem key={i} {...item}/>
        ));
        return (

            <div className='navbarHolder'>
                <Navbar expand="md">
                    <div className="nav-button-wrap color-bg">

                        <div onClick={this.toggle} className={'nav-button'}>
                            <span/>
                            <span/>
                            <span/>
                        </div>

                    </div>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav navbar>
                            {displayMenu}
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>


        )
    }
}

export default HeaderNavMenu;