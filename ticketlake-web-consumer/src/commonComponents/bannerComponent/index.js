import React from 'react';
import {HEADING_FONT} from "../../utils/css-utils";

const bannerComponent = (props) => {
    return(
        <section className="list-single-hero" data-scrollax-parent="true" id="sec1">
            <div className="bg par-elem" style={{
                float: 'left',
                backgroundImage: "url('" + window.location.origin + props.image + "')",
                translateY: '30%'
            }}/>
            <div className="list-single-hero-title fl-wrap">
                <div className="container">
                    <div className="row">
                        <div className="col-md-7">
                            <div className="listing-rating-wrap">
                                <div className="listing-rating card-popup-rainingvis"
                                     data-starrating2={5}/>
                            </div>
                            <h2>
                                <span style={{fontFamily: HEADING_FONT}}>{props.heading}</span>
                                <p style={{fontFamily: HEADING_FONT, fontSize: '19px', color: '#eae9e8', paddingBottom: 'unset'}}>{props.para}</p>
                            </h2>
                        </div>
                    </div>

                    <div className="breadcrumbs-hero-buttom fl-wrap">
                        <div className="breadcrumbs">
                            <i className='fas fa-chevron-right'></i>
                            <a href="#">Home</a>
                            <a href="#">Movies</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
};

export default bannerComponent