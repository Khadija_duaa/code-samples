// Library
import React from 'react';
// Data Array
const footerMenu = [
    {
        menuName: 'Terms of use',
        menuLink: '#'
    },
    {
        menuName: 'Privacy Policy',
        menuLink: '#'
    },
];
const subFooter = () => {
    return (
        <div className="sub-footer">
            <div className="container">
                <div className="copyright"> © Ticketlake 2019 - All Rights Reserved</div>
                {/*<div className="subfooter-lang">
                    <div className="subfooter-show-lang"><span>Eng</span><i
                        className="fa fa-caret-up"/></div>
                    <ul className="subfooter-lang-tooltip">
                        <li><a href="#">Dutch</a></li>
                        <li><a href="#">Italian</a></li>
                        <li><a href="#">French</a></li>
                        <li><a href="#">Spanish</a></li>
                    </ul>
                </div>*/}
                <div className="subfooter-nav" style={{marginRight: '0px'}}>
                    <ul>
                        {footerMenu.map((menu, i) => (
                            <li key={i}><a href={menu.menuLink}>{menu.menuName}</a></li>
                        ))}
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default subFooter;