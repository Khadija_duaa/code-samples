import React from 'react';
import LinesEllipsis from 'react-lines-ellipsis'

class CardComponent extends React.Component{

    state = {
        arrow : false
    };

    render(){

        const showDetail = () => {
            this.setState({arrow:true})
        };

        const closeDetail = () => {
            this.setState({arrow:false})
        };

        const {onClick} = this.props;
        return (
            <div className="listing-item" style={{padding: '7px 12px'}}>
                <article className="geodir-category-listing fl-wrap">
                    <div className="geodir-category-img" style={{maxHeight: "199px"}}>
                        <a href={"#"} onClick={onClick}>
                            <img
                                src={this.props.data.celebrityImageKey.imageUrl ? this.props.data.celebrityImageKey.imageUrl : window.location.origin + '/images/city/1.jpg'}
                                alt={"image"}/>
                        </a>
                    </div>
                    <div className="geodir-category-content fl-wrap title-sin_item">
                        <div className="geodir-category-content-title fl-wrap">

                            <div className="geodir-category-content-title-item">

                                <h3 className="title-sin_map">
                                    <a href={"#"} onClick={onClick}>
                                        {this.props.data.celebrityName ? this.props.data.celebrityName : "Celebrity Name"}
                                    </a>
                                </h3>
                                {
                                    this.state.arrow === false ?
                                        <i className="fa fa-angle-down fa-arrow"
                                           onClick={showDetail}/> : null
                                }

                                {
                                    this.state.arrow === true ?
                                        <i className="fa fa-angle-up fa-arrow"
                                           onClick={closeDetail}/> : null
                                }
                                <div className="geodir-category-location fl-wrap">
                                    <a href={"#"} className="map-item" style={{color: 'black'}}>
                                        {this.props.data.celebrityTitle ? this.props.data.celebrityTitle : "Celebrity Title"}
                                    </a>
                                </div>
                            </div>
                        </div>

                        <p>
                            <LinesEllipsis
                                text={this.props.data.celebrityDescription? this.props.data.celebrityDescription : "Celebrity Description"}
                                maxLine={this.state.arrow === true ? '40' : '3'}
                                ellipsis=' . . .'
                                trimRight
                                basedOn='letters'
                            />
                        </p>

                    </div>
                </article>
            </div>
        )
    }
}


export default CardComponent;