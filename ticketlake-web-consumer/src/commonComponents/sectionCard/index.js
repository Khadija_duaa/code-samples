// Library
import React from 'react';

const sectionCard = (props) => {

    return (
        <div className="slick-slide-item" style={{width: "100%"}}>
                <div className="geodir-category-img card-post" style={{borderRadius: "50%", height: '135px', width: '150px'}}>
                    <a href={'#'}>
                        <img src={props.img ? props.img : 'http://localhost:3000/images/card_3.png'}
                             style={{backgroundColor: "rgba(0, 0, 0, 0.5)"}}
                             alt={'image'}/>
                    </a>
                    <div className="geodir-category-opt">
                        <div className={"col-md-12"} style={{padding: '0px'}}>
                            <h3 style={{marginBottom: '35px', color: "white", fontSize: "16px",fontFamily:"Montserrat", fontWeight: "600"}}>
                                {props.title ? props.title : "Title"}
                            </h3>
                        </div>
                    </div>
            </div>
        </div>
    );
};

export default sectionCard;