// Library
import React from 'react';
import {NavLink} from "react-router-dom";
import Moment from 'react-moment';

const animatedCard = (props) => {
    return (
            <div key={props.item._id} className={props.cardClass ? props.cardClass : "gallery-item"}>
                <div className="grid-item-holder">
                    <div className="listing-item-grid">

                        <img src={'/images/badge.svg'} className={'cardBadge'} alt={'image'}/>
                        <img src={props.cardImage? props.cardImage : 'http://newmoonriverinn.com/wp-content/uploads/2018/10/club-one-casino-1.jpg'} alt={'image'}/>


                        <div className="listing-item-cat">

                            <h3><NavLink to={props.cardLink ? props.cardLink : '/'}>{props.eventTitle}</NavLink></h3>
                            <div className="clearfix"/>
                            <p>{props.categories}<br/><Moment format="MM/DD/YYYY">{props.date}</Moment></p>
                        </div>
                    </div>
                </div>
            </div>
    );
};

export default animatedCard;