// Library
import React from 'react';

const resultForHeading = (props) => {
    return (
        <div className="list-main-wrap-title single-main-wrap-title">
            <h2>{props.firstText}: <span>{props.secondText}</span></h2>
        </div>
    );
};

export default resultForHeading;