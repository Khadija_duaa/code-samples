// Library
import React from 'react';
import {Col} from 'reactstrap';
import {NavLink} from "react-router-dom";
// Components
import TwoShadedButtonBuy from "../twoShadedButtonBuy";
import TwoShadedButton from "../twoShadedButton";
import EllipsisText from "react-ellipsis-text";
// Constant
import {TITLE_SIZE} from '../../utils/config';
const defaultCard = (props) => {
    return (
        <Col
            lg={props.gridLg ? props.gridLg : 3}
            md={props.gridMd ? props.gridMd : 6}
            sm={props.gridSm ? props.gridSm : 12}
            className={'defaultCard'}>
            <article className="geodir-category-listing fl-wrap" style={{
                minHeight: "430px",
                maxHeight: "430px"
            }}>
                <div className="geodir-category-img">
                    <a href={props.cardLink} onClick={props.onClick}>
                        <img src={props.image ? props.image : window.location.origin + '/images/city/1.jpg'}
                             alt={"image"}/>
                    </a>
                    <div className="listing-avatar">
                        {(props.auth) ? (
                            <a href="javascript:void(0)" onClick={props.wishlistLink}>
                                {(props.isWishList === true) ?
                                    (
                                        <i className='fas fa-heart' style={{color: 'red'}}/>
                                    ) :
                                    (
                                        <i className='far fa-heart'/>
                                    )}

                            </a>
                        ) : null}
                    </div>
                    {(props.sharing) ? (
                        <div className="sale-window" onClick={() => props.sharing(props.id)}>
                            <i className='far fa-share-square'/>
                        </div>
                    ) : null}

                </div>
                <div className="geodir-category-content fl-wrap title-sin_item">
                    <div className="geodir-category-content-title fl-wrap">
                        <div className="geodir-category-content-title-item">
                            <h3 className="title-sin_map">
                                <a href={props.cardLink} onClick={props.onClick}>
                                    <EllipsisText
                                        text={props.cardTitle ? props.cardTitle : "Title"}
                                        length={TITLE_SIZE}
                                    />

                                </a>
                            </h3>
                            <div className="geodir-category-location fl-wrap">
                                <NavLink to={props.cardLink} className="map-item">
                                    <i className="fa fa-map-marker-alt"/>
                                    {props.country || props.city ?
                                        props.country + props.city
                                        :
                                        "N/A"
                                    }
                                </NavLink>
                            </div>
                            <div className="geodir-category-location fl-wrap">
                                <NavLink to={props.cardLink} className="map-item">
                                    <i className="fa fa-clock"/>
                                    {props.dates || props.dates ?
                                        props.dates
                                        :
                                        "N/A"
                                    }
                                </NavLink>
                            </div>
                        </div>
                    </div>

                    <div className="geodir-category-footer fl-wrap">
                        {
                            props.buttonVersion && props.buttonVersion === 2 ?
                                (
                                    <TwoShadedButton
                                        buttonText={props.buttonText ? props.buttonText : ''}
                                        buttonLink={props.buttonLink ? props.buttonLink : ''}
                                    />
                                ) :
                                (
                                    <TwoShadedButtonBuy
                                        buttonText={props.buttonText ? props.buttonText : ''}
                                        buttonLink={props.buttonLink ? props.buttonLink : ''}
                                    />
                                )
                        }

                    </div>
                </div>
            </article>
        </Col>
    )


};

export default defaultCard;