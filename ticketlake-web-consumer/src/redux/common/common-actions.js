// Action Constants
export const PROCESSING = 'ACTION_COMMON_PROCESSING';

export const setProcessing = (processing) => {
    return {
        type: PROCESSING,
        payload: processing
    }
};