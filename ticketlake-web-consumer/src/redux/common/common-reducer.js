// Actions
import {PROCESSING} from './common-actions';

const _initialState = {
    processing: false
};

const reducer = (state = _initialState, action) => {
    let newState = {...state};

    switch (action.type) {
        case PROCESSING:
            setProcessing(newState, action.payload);
            break;
        default:
            return newState
    }
};

const setProcessing = (state, processing) => {
    state.processing = processing;
};

export default reducer;