import axios from '../../utils/axios';

export const ALL_CATEGORIES = 'ACTION_ALL_CATEGORIES';

const setCategories = (payload) => {
    return {
        type: ALL_CATEGORIES,
        payload
    }
};

export const getAllCategories = (cb) => {

    return (dispatch) => {

        axios.get('/categories/get-all-categories')

            .then(response => {
                dispatch(setCategories(response.data.data));
                cb && cb();
            })
            .catch((err) => {
                console.log(err);
                dispatch(setCategories([]));
            });
    };
};