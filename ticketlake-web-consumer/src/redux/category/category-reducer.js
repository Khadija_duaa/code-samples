import {ALL_CATEGORIES} from './category-actions';

const initState = {
    categories: [],
};

const reducer = (state = initState, action) => {
    let newState = {...state};

    switch (action.type) {
        case ALL_CATEGORIES:
            setCategories(newState, action.payload);
            break;

        default : {
            // do nothing
        }
    }
    return newState;
};

const setCategories = (state, data) => {
    state.categories = data;
};

export default reducer;

