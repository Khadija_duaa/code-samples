// Axios
import axios from '../../utils/axios';
// URLs
import {
    WISHLIST_API_GET_IDS,
    WISHLIST_API_TOGGLE,
    GET_WISHLIST_EVENTS
} from '../../utils/config';
import {PROCESSING} from "../user/user-actions";
// Actions
export const ADD_WISHLIST_IDS = 'ACTION_WISHLIST_ADD_IDS';
export const WISHLIST_PROCESSING = 'ACTION_WISHLIST_PROCESSING';
export const RESET_WISHLIST_IDS = 'ACTION_WISHLIST_IDS_RESET';
export const GET_ALL_WISHLIST_EVENTS = 'ACTION_GET_ALL_WISHLIST_EVENTS';

export const resetWishListIds = () => {
    return {
        type: RESET_WISHLIST_IDS,
        payload: null
    }
};
export const addWishListIds = (wishList) => {
    return {
        type: ADD_WISHLIST_IDS,
        payload: wishList
    };
};

const setProcessing = (processing) => {
    return {
        type: WISHLIST_PROCESSING,
        payload: processing
    };
};
export const getWishListIdsFromApi = () => {
    return ((dispatch) => {
        axios.get(WISHLIST_API_GET_IDS)
            .then(respose => {
                dispatch(addWishListIds(respose.data.data));
            })
            .catch(err => {
                console.log("ERR", err);
            })
    });
};

export const wishListToggle = (eventSlotId, dispatchSecond) => {

    return ((dispatch) => {
        axios.post(WISHLIST_API_TOGGLE, {
            eventSlotId: eventSlotId
        })
            .then(respose => {
                dispatch(addWishListIds(respose.data.data));
                if (dispatchSecond) {
                    dispatch(getWishlistEvents());
                }
            })
            .catch(err => {
                console.log("ERR", err);
            })
    });

};

export const getWishlistEvents = () => {
    return ((dispatch) => {
        const body = {
            paginate: true,
            page: 0,
            skip: 0,
            pageSize: 10,
            all: true
        };
        dispatch(setProcessing(true));
        axios.post(GET_WISHLIST_EVENTS, body)
            .then(respose => {
                dispatch(setProcessing(false));
                dispatch({
                    type: GET_ALL_WISHLIST_EVENTS,
                    payload: respose.data.data
                });
            })
            .catch(err => {
                dispatch({
                    type: GET_ALL_WISHLIST_EVENTS,
                    payload: null
                });
                dispatch(setProcessing(false));
            })
    });
};
