// Axios
import axios from '../../utils/axios';
import _ from "lodash";
// Helpers
import {
    getTicketClassConfigData,
    searchInArr,
    formatObject,
    getSeatsFromResponse,
    seatsQtySearch,
    setCheckoutData,
    checkTicketsForMySelf,
    checkSeatsAssigned,
    getPassesConfigData,
    formatObjectForPasses,
    arrangePassesSeatsWithEventSlots,
    arrangeAllPassesSeatsData,
    setAssignedSeatsForPasses,
    checkPassesForMySelf,
    getPassesTicketClassesData,
    checkEvent
} from './ticket-helper';
// URLs
import {
    EVENTS_GET_EVENT_DETAIL,
    TICKET_PURCHASE,
    CLIENT_ID_GET,
    PASSES_GET_SEATS
} from '../../utils/config';
// Import Redux Action
import {userViewProfile} from '../user/user-actions';
// Actions
export const MOVE_STEP = 'ACTION_TICKET_MOVE_STEP';
export const PROCESSING = 'ACTION_TICKET_PROCESSING';
export const SETTING_BILL_SUMMARY = 'ACTION_TICKET_BILL_SUMMARY';
export const SET_EVENT = 'ACTION_GET_EVENT_DETAIL';
export const SET_SEATS = 'ACTION_SEATS_SETS';
export const SET_ASSIGNED_SEATS = 'ACTION_SET_ASSIGNED_SEATS';
export const SET_ASSIGNED_PASS = 'ACTION_SET_ASSIGNED_PASSES';
export const SET_TOTAL_BILL = 'ACTION_SET_TOTAL_BILL';
export const SET_TICKET_CURRENCY = 'ACTION_SET_TICKET_CURRENCY';
export const SET_PAYMENT_SUCCESS = 'ACTION_SET_PAYMENT_SUCCESS';
export const RESET_REDUX = 'ACTION_RESET_REDUX';
export const ASSIGNED_SEATS_FOR_DISPLAY = 'ACTION_DISPLAY_ASSIGNED_SEATS';
export const ASSINGED_SEATS_FLAG = 'ACTION_ASSIGNED_SEATS_FLAG';
export const SET_CLIENT_TOKEN = 'ACTION_SET_CLIENT_TOKEN';
export const SET_ERROR = 'ACTION_SET_ERROR';
export const SET_ERROR_MESSAGE = 'ACTION_SET_ERROR_MESSAGE';
export const SET_PASSES_DATA = 'ACTION_SET_PASSES_DATA';
export const SET_PASSES_SEATS = 'ACTION_SET_PASSES_SEATS';
export const SET_PASSES_ASSIGNED_SEATS = 'ACTION_SET_PASSES_ASSIGNED_SEATS';
export const SET_PASSES_ASSIGNED_SEATS_FOR_DISPLAY = 'ACTION_SET_PASSES_ASSIGNED_SEATS_FOR_DISPLAY';
export const SET_PASSES_TICKET_CLASSES = 'ACTION_SET_PASSES_TICKET_CLASSES';


export const setClientToken = () => {

    return (dispatch => {
        dispatch(setProcessing(true));
        axios.get(CLIENT_ID_GET)
            .then(responce => {
                dispatch(setProcessing(false));
            })
            .catch(err => {
                console.log(err);
                dispatch(setProcessing(false));
            })
    });
};

export const getEventDetail = (id) => {
    return (dispatch, getState) => {
        dispatch(setProcessing(true));
        axios.get(EVENTS_GET_EVENT_DETAIL + id)
            .then(responce => {
                dispatch(setError(false));
                console.log("EVENT: ", responce.data.data);
                const checkEventForDate = checkEvent(responce.data.data);

                dispatch(setTicketCurrency(responce.data.data.parentEventInfo.currency));
                const ticketData = getTicketClassConfigData(responce);
                console.log("TICKET DATA: " , ticketData);
                dispatch(setSeats(responce, ticketData));
                dispatch(setEvent(responce));
                const passesData = getPassesConfigData(responce);
                dispatch(setPassesTicketClasses(getPassesTicketClassesData(passesData)));
                // console.log("PASSES DATA: " , passesData);
                dispatch(setAssignedPassesFlag(passesData.length > 0));
                dispatch(setPassesData(passesData));

                dispatch(setBillSummary(_.concat(passesData, ticketData)));

                if (checkEventForDate) {
                    dispatch(setError(true));
                    dispatch(setErrorMessage("The event starting date has passed"));
                }
                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setError(true));
                dispatch(setErrorMessage(err));
                dispatch(setProcessing(false));
            })
    };
};


export const setBillSummary = (arr, wallet = 0) => {
    const billSumamry = [];
    const sum = [0];
    arr.forEach(item => {

        if (item.ticketClassType === 'PASS') {
            sum.push(parseInt(item.passPrice) * parseInt(item.ticketClassQty));
            billSumamry.push(formatObjectForPasses(item))
        } else {
            sum.push(parseInt(item.ticketClassPrice) * parseInt(item.ticketClassQty));
            billSumamry.push(formatObject(item))
        }
    });
    return (dispatch) => {
        const totalBill = sum.reduce((partial_sum, a) => partial_sum + a);
        if (wallet) {
            if (parseInt(wallet) === totalBill || parseInt(wallet.availableBalance) > totalBill) {
                dispatch(setTotalBill(0));
            } else {
                dispatch(setTotalBill(totalBill - parseInt(wallet.availableBalance)));
            }
        } else {

            dispatch(setTotalBill(totalBill));
        }
        dispatch({
            type: SETTING_BILL_SUMMARY,
            payload: billSumamry
        });
    }
};

const setSeats = (res, ticketData) => {
    const seatDataFromResponse = res.data.data.seats.seats[0].seats;
    const seats = getSeatsFromResponse(seatDataFromResponse, ticketData);
    // console.log("SEATS: " ,seats);
    const assignedSeatFlag = checkSeatsAssigned(seats) > 0;
    return (dispatch => {
        dispatch(setAssignedSeatsFlag(assignedSeatFlag));
        dispatch(setSeating(seats));
    });
};


const setPassesSeatsData = (billSummary, event, passData, passTicketClasses) => {

    return (dispatch => {
        dispatch(setProcessing(true));
        const passesRequest = [];
        _.forEach(billSummary, (item) => {
            if (item.ticketClassType === "PASS") {
                if (item.ticketClassQty > 0) {
                    passesRequest.push({
                        id: item.uniqueId
                    })
                }
            }
        });
        const passesSeats = [];
        if (passesRequest.length) {
            dispatch(setProcessing(true));
            const eventParentId = event.data.data.parentEventInfo._id;

            let promise = new Promise((resolve, reject) => {
                const length = passesRequest.length;
                let counter = 0;
                _.forEach(passesRequest, (item) => {
                    axios.post(PASSES_GET_SEATS, {
                        parentEventId: eventParentId,
                        passId: item.id
                    })
                        .then((res) => {
                            counter++;
                            passesSeats.push(arrangePassesSeatsWithEventSlots(res.data.data, passData));
                            if (counter === length) {
                                resolve();
                            }


                        }).catch((err) => {
                        console.log("ERR: ", err);
                    });


                });


            });


            promise.then(() => {
                const passesUniqueSeats = arrangeAllPassesSeatsData(passesSeats, passData);
                dispatch(setPassesSeats(passesUniqueSeats));
                const assignedSeats = setAssignedSeatsForPasses(billSummary, passesUniqueSeats, _.keys(passesUniqueSeats), passData, passTicketClasses);
                dispatch(setPassesAssignedSeats(assignedSeats));
                dispatch(setPassesAssignedSeatsForDisplay(assignedSeats));

                setProcessing(false);
            });

            promise.catch(() => {
                console.log("ERROR");
                setProcessing(false);
            });


        }


    })


};

export const setAssignedSeats = (billSummary, seats, wallet = 0, event, passData, passTicketClasses) => {

    return (dispatch) => {


        dispatch(setProcessing(true));
        dispatch(setPassesSeatsData(billSummary, event, passData, passTicketClasses));
        const assignedSeats = seatsQtySearch(billSummary, seats);
        dispatch(setBillSummary(billSummary, wallet));
        dispatch({
            type: SET_ASSIGNED_SEATS,
            payload: assignedSeats
        });
        dispatch(setAssignedSeatsForDisplay(assignedSeats));
        dispatch(setProcessing(false));
        dispatch(setStep(2));

    };
};


export const setAssignedBillFromForm = (index, val, rowNumber, seatNumber, assignedSeats, uniqueId) => {
    return (dispatch) => {
        assignedSeats.forEach(item => {
            if (item.seatNumber === seatNumber && item.rowNumber === rowNumber) {
                item.userInfo[index] = val;
            }
        });
        dispatch(setAssignedSeatsInner(assignedSeats));
    };

};
export const setAssignedBillFromFormForPasses = (index, val, uniqueId, passId, assignedSeats) => {
    return (dispatch) => {
        _.forEach(assignedSeats, (item) => {
            if (item.passId === passId && item.uniqueIndex === uniqueId) {
                item.userInfo[index] = val;
            }
        });
        dispatch(setPassesAssignedSeats(assignedSeats));

    };

};


export const checkout = (tickets, guestsTickets, event, paymentNonce, allPasses, guestPasses) => {
    const {data} = event.data;
    return (dispatch) => {
        dispatch(setProcessing(true));
        const checkoutData = setCheckoutData(
            data.parentEventId,
            data._id,
            checkTicketsForMySelf(tickets, guestsTickets),
            paymentNonce,
            checkPassesForMySelf(allPasses, guestPasses)
        );
        axios.post(TICKET_PURCHASE, checkoutData)
            .then(response => {
                dispatch(setPaymentSuccess(true));
                dispatch({
                    type: MOVE_STEP,
                    payload: 4
                });
                dispatch(userViewProfile());
                dispatch(setProcessing(false));
            })
            .catch(err => {
                dispatch(setProcessing(false));
                dispatch(setPaymentSuccess(false));
                dispatch({
                    type: MOVE_STEP,
                    payload: 4

                });

            });

    };
};

export const removeAssignedSeatsFromDisplay = (string, assignedSeatsFromDisplay, passesSeats) => {
    if (string === 0 || string === "0") {
        return (dispatch) => {
            dispatch(setAssignedSeatsForDisplay(assignedSeatsFromDisplay));
            dispatch(setPassesAssignedSeatsForDisplay(passesSeats));
        }

    } else {
        let splittedString = string.split("|");
        if (splittedString[0] === 'PASS') {
            const newArray = [];
            let counter = 0;
            passesSeats.forEach(item => {
                if (item.passId === splittedString[2]) {

                    if (counter > 0) {
                        newArray.push(item);
                    } else {
                        console.log("REMOVED: ", item);
                        counter++;
                    }

                } else {
                    newArray.push(item);
                }
            });
            return (dispatch => {
                dispatch(setPassesAssignedSeatsForDisplay(newArray));
                dispatch(setAssignedSeatsForDisplay(assignedSeatsFromDisplay));
            });
        } else {
            const newArray = [];
            assignedSeatsFromDisplay.forEach(item => {
                if (item.ticket.name === splittedString[1]) {
                    if (
                        parseInt(item.rowNumber) === parseInt(splittedString[2])
                        &&
                        parseInt(item.seatNumber) === parseInt(splittedString[3])
                    ) {
                        console.log("Item Found: ", item);
                    } else {
                        newArray.push(item);
                    }
                } else {
                    newArray.push(item);
                }
            });

            return (dispatch => {
                dispatch(setAssignedSeatsForDisplay(newArray));
                dispatch(setPassesAssignedSeatsForDisplay(passesSeats));
            });
        }
    }
};
export const setError = (error) => {
    return {
        type: SET_ERROR,
        payload: error
    }
};
export const setErrorMessage = (message) => {
    return {
        type: SET_ERROR_MESSAGE,
        payload: message
    }
};
const setAssignedSeatsInner = (seats) => {
    return {
        type: SET_ASSIGNED_SEATS,
        payload: seats
    }
};
const setTotalBill = (bill) => {
    return {
        type: SET_TOTAL_BILL,
        payload: bill
    }
};


const setPaymentSuccess = (success) => {
    return {
        type: SET_PAYMENT_SUCCESS,
        payload: success
    }
};
const setAssignedSeatsFlag = (flag) => {
    return {
        type: ASSINGED_SEATS_FLAG,
        payload: flag
    }
};
const setAssignedPassesFlag = (flag) => {
    return {
        type: SET_ASSIGNED_PASS,
        payload: flag
    }
};

const setSeating = (seats) => {
    return {
        type: SET_SEATS,
        payload: seats
    };
};
export const setAssignedSeatsForDisplay = (seats) => {
    return {
        type: ASSIGNED_SEATS_FOR_DISPLAY,
        payload: seats
    }
};

const setTicketCurrency = (currency) => {
    return {
        type: SET_TICKET_CURRENCY,
        payload: currency
    }
};

export const resetRedux = () => {
    return {
        type: RESET_REDUX,
        payload: null
    }
};

const setProcessing = (processing) => {
    return {
        type: PROCESSING,
        payload: processing
    }
};

export const setEvent = (event) => {
    return {
        type: SET_EVENT,
        payload: event
    }
};
export const setStep = (step) => {
    return {
        type: MOVE_STEP,
        payload: step
    }
};

export const setPassesData = (passesData) => {
    return {
        type: SET_PASSES_DATA,
        payload: passesData
    }
};

export const setPassesSeats = (passesData) => {
    return {
        type: SET_PASSES_SEATS,
        payload: passesData
    }
};
export const setPassesAssignedSeats = (passesData) => {
    return {
        type: SET_PASSES_ASSIGNED_SEATS,
        payload: passesData
    }
};


export const setPassesAssignedSeatsForDisplay = (passesData) => {
    return {
        type: SET_PASSES_ASSIGNED_SEATS_FOR_DISPLAY,
        payload: passesData
    }
};


export const setPassesTicketClasses = (passesTicketClasses) => {
    return {
        type: SET_PASSES_TICKET_CLASSES,
        payload: passesTicketClasses
    }
};

