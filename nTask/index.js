import React from "react";
import { render } from "react-dom";
import "./index.css";


import App from "./App";
import { Provider } from "react-redux";
import configureStore from "./redux/store/index";
export const store = configureStore();


render(
  <React.Fragment>
    <Provider store={store}>
    
    <App />
    
 </Provider>

  </React.Fragment>,
  document.getElementById("rootCnt")
);
