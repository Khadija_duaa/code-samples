import Avatar from '@material-ui/core/Avatar';
import React from 'react';
import GroupIcon from '@material-ui/icons/Group';
import CombinedAvatarStyles from './CombinedAvatar.style';
import CustomAvatar from "../../components/Avatar/Avatar";
import { compose } from 'redux';
import { withStyles } from '@material-ui/styles';


const CombinedAvatar = (props) => {
    const { classes, theme, profileData } = props;


    const getRandomColor = str => {
        let hash = 0;
        if (str) {
            for (let i = 0; i < str.length; i++) {
                hash = str.charCodeAt(i) + ((hash << 5) - hash);
            }
        }
        const h = hash % 360;
        return `hsl(${h}, ${60}%, ${50}%)`;
    };

    return (
        <Avatar className={classes.groupAvatar}>

                {profileData && profileData.map((data, index) => (
                    data.profilePicUrl !== '' ?
                        <Avatar
                            key={index}
                            src={data.profilePicUrl}
                            className={classes.icon}
                        />
                        :
                        <Avatar
                            className={classes.icon}
                            style={{
                                background: getRandomColor(data.initial),
                            }}
                        >
                            {data.initial.slice(0,1)}
                        </Avatar>

                ))
            }
        </Avatar>
    );
}

export default compose(withStyles(CombinedAvatarStyles, { withTheme: true }))(CombinedAvatar);