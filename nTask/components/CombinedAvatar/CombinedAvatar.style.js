const CombinedAvatarStyles = (theme) => ({
    groupAvatar: {
        // display: 'flex',
        // alignItems: 'center',
        // justifyContent: 'center',
        // width: theme.spacing(7),
        // height: theme.spacing(7),
        // color: theme.palette.primary.contrastText,
        backgroundColor: theme.palette.primary.main,
        width: 40,
        height: 40,
        borderRadius: '50%',
        fontFamily: theme.typography.fontFamilyLato,
        fontSize: '1.0714285714285714rem',
        fontWeight: 500,
        fontSize: "20px !important",
    },
    icon: {
        borderRadius: '0% !important',
        height: '100%',
        fontSize: '13px !important',
        width: '50%',
        objectFit: 'none',
        objectPosition: '10% center'
    },
    
});

export default CombinedAvatarStyles;
