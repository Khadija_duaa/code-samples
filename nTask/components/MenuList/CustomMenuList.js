import React from 'react'
import List from "@material-ui/core/List";
import withStyles from "@material-ui/core/styles/withStyles";
import customMenuListStyles from "./customMenuList.style";

function CustomMenuList(props){
  const {children, classes, ...rest} = props;
  return (
    <List
      className={classes.list}
      disablePadding={true}
      {...rest}
    >
     {children}
    </List>
  )
}

CustomMenuList.defaultProps = {
  classes: {}
}
export default withStyles(customMenuListStyles, {withTheme: true})(CustomMenuList)