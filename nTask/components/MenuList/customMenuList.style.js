const customMenuListStyles = (theme) => ({
  listItem: {
    padding: '8px 10px'
  },
  listItemText: {
    color: theme.palette.text.lightGray,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    fontWeight: 600,
  },
  list:{
    // paddingRight: 10
  }
})

export default customMenuListStyles;