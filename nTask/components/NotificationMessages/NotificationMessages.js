import React, { Component } from "react";
import CalendarIcon from "@material-ui/icons/CalendarToday";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import notificationStyle from "./style";
import InfoIcon from "@material-ui/icons/Info";
import FailureIcon from "@material-ui/icons/Error";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import IconCheckCircle from "../Icons/IconCheckCircle";

function NotificationMessage(props) {
  const { theme, classes, type, iconType, children, style } = props;

  const Icon = () => {
    return iconType == "calendar" ? (
      <CalendarIcon className={classes[`icon${type}`]} />
    ) : iconType == "info" ? (
      <InfoIcon className={classes[`icon${type}`]} />
    ) : iconType == "sucess" ? (
      // <CheckCircleOutlineIcon className={classes[`icon${iconType}`]} />
      <IconCheckCircle svgProps={{ className: classes[`icon${iconType}`] }} />
    ) : iconType == "failure" ? (
      <FailureIcon className={classes[`icon${type}`]} />
    ) : null;
  };
  // const cntClass = classes[`NotifMessageCnt${type}`]

  return (
    <div className={`flex_start_start_row ${classes[`NotifMessageCnt${type}`]}`} style={style}>
      <Icon />
      <Typography variant="body2" className={classes[`text${type}`]}>
        {children}
      </Typography>
    </div>
  );
}

export default withStyles(notificationStyle, { withTheme: true })(NotificationMessage);
