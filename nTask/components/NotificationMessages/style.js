const notificationStyle = theme => ({
    NotifMessageCntsucess: {
        background: theme.palette.primary.lightGreen,
        padding: "8px 10px",
        borderRadius: 4
    },
    NotifMessageCntpassword: {
        background: theme.palette.background.success,
        padding: "10px",
        borderRadius: 4
    },
    NotifMessageCntinfo: {
        background: theme.palette.background.items,
        padding: "8px 10px",
        borderRadius: 4
    },
    NotifMessageCntfailure: {
        background: theme.palette.error.xlight,
        padding: "8px 10px",
        borderRadius: 4
    },
    textsucess: {
        color: theme.palette.primary.light,
        marginLeft: 5
    },
    textpassword: {
        color: theme.palette.common.white,
        marginLeft: 5,
        fontSize: "13px !important",
        fontWeight: 400
    },
    textfailure: {
        color: theme.palette.error.dark,
        marginLeft: 5
    },
    iconsucess: {
        color: theme.palette.common.white,
        fontSize: "18px !important",
        marginRight: 8
    },
    iconfailure: {
        color: theme.palette.error.dark,
        fontSize: "16px !important"
    },
    iconinfo: {
        color: theme.palette.secondary.medDark,
        fontSize: "14px !important",
        marginTop: 2
    },
    iconNewInfo: {
        color: theme.palette.text.light,
        fontSize: "22px !important",
        marginRight: 8,
    },
    textNewInfo: {
        fontSize: "13px !important",
        fontFamily: theme.typography.fontFamilyLato,
    },
    textinfo: {
        color: theme.palette.secondary.medDark,
        marginLeft: 5
    },

})

export default notificationStyle;