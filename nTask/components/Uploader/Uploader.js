import React, { Component } from "react";
const Uppy = require("@uppy/core");
const Tus = require("@uppy/tus");
const XHRUpload = require("@uppy/xhr-upload");
const ProgressBar = require("@uppy/progress-bar");
const FileInput = require("@uppy/file-input");
const { DragDrop } = require("@uppy/react");
import "uppy/dist/uppy.min.css";
import helper from "../../helper/index";

class Uploader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.uppy = Uppy({ debug: true, autoProceed: true });
  }
  render() { 
    this.uppy.use(FileInput, {
      
      replaceTargetContent: true
    });
  
    this.uppy.use(XHRUpload, {
      endpoint:
        "https://dev.naxxa.io/api/docsfileuploader/uploaddocsfileamazons3",
        headers: {
          'authorization': helper.getToken()
        },
      formData: true,
      fieldName: "files[]"
    });
    // And display uploaded files
    this.uppy.on("upload-success", (file, response) => {
      alert("hello");
    });

    const AvatarPicker = () => {
      return (
        <div>
          <form className="UppyForm">
          <input type="file" name="files[]" multiple="" />
          </form>
        </div>
      );
    };
    return <AvatarPicker uppy={this.uppy} />;
  }
}

export default Uploader;
