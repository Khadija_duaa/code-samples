import React, { useState, useReducer } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import Grid from "@material-ui/core/Grid";

import sectionStyles from "./section.style";
import EditableTextField from "../../components/Form/EditableTextField";
import IconEditSmall from "../../components/Icons/IconEditSmall";
import IconColor from "../../components/Icons/IconColor";
import ColorPickerDropdown from "../../components/Dropdown/ColorPicker/Dropdown";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import {
  editCustomField,
  updateCustomFieldData,
  dispatchDeleteField,
} from "../../redux/actions/customFields";
import { getCustomFields } from "../../helper/customFieldsData";

import CustomFieldsChecklistView from "../../components/CustomFieldsViews/CustomFieldsChecklist.view";
import CustomFieldsComponent from "../../components/CustomFieldComponent";
import {
  updateCustomFieldDialogState,
  updateDeleteDialogState,
} from "../../redux/actions/allDialogs";
import { getContrastYIQ } from "../../helper/index";
import differenceBy from "lodash/differenceBy";

function Sections({
  classes,
  type,
  customFields,
  profileState,
  editCustomField,
  updateCustomFieldData,
  disabled,
  customFieldData,
  permission,
  customFieldChange,
  dispatchDeleteField,
  handleEditField,
  handleCopyField,
  groupId,
  handleClickHideOption,
  handleSelectHideOption,
  updateCustomFieldDialogState,
  updateDeleteDialogState,
  workspaceId,
}) {
  const [error, setError] = useState([]);
  const [editedSectionArr, setEditedSectionArr] = useState([]);
  const [openedSections, setOpenedSections] = useState([]);

  const handleShowSystemField = section => {
    const isExist = openedSections.indexOf(section.fieldId);
    if (isExist >= 0) setOpenedSections(openedSections.filter(id => id !== section.fieldId));
    else setOpenedSections([...openedSections, section.fieldId]);
  };

  const validateCustomFieldName = param => {
    /** validate custom field name */
    return customFields.data.filter(ele => ele.fieldName == param).length < 1;
  };

  const updateSectionName = (name, section) => {
    let object = { ...section };
    if (section.fieldName !== name && !validateCustomFieldName(name)) {
      setError([...error, section.fieldId]);
      return;
    } /** validate custom field name */ else setError(error.filter(e => e !== section.fieldId));

    object = { ...object, fieldName: name };
    delete object.team;
    delete object.workspaces;
    editCustomField(
      object,
      object.fieldId,
      res => {
        updateCustomFieldData(res);
      },
      failure => {}
    );
    setEditedSectionArr(editedSectionArr.filter(item => item !== section.fieldId));
  };
  const handleEdit = section => {
    setEditedSectionArr([...editedSectionArr, section.fieldId]);
  };
  const colorChange = (selectedColor, section) => {
    let object = { ...section, settings: { ...section.settings, color: selectedColor } };
    delete object.team;
    delete object.workspaces;
    editCustomField(
      object,
      object.fieldId,
      res => {
        updateCustomFieldData(res);
      },
      failure => {}
    );
  };
  let customSections = getCustomFields(customFields, profileState, type, workspaceId).filter(
    f => f.fieldType == "section"
  );
  let nonSectionFields = getCustomFields(customFields, profileState, type, workspaceId).filter(
    f => f.fieldType !== "section" && f.isSystem == false && f.sectionId !== null
  );
  const getChildComponents = id => {
    let childCmp = getCustomFields(customFields, profileState, type, workspaceId).reduce(
      (res, cv) => {
        let isExist = cv.sectionId ? cv.sectionId.find(ele => ele === id) : false;
        if (isExist) res.push(cv);
        return res;
      },
      []
    );
    return childCmp;
  };
  const handleDeleteField = field => {
    let obj = [field.fieldId];
    dispatchDeleteField(
      obj,
      succ => {
        updateDeleteDialogState({ open: false, btnQuery: "" });
      },
      fail => {}
    );
  };
  let fieldsWithoutSections = getCustomFields(customFields, profileState, type, workspaceId).filter(
    f => f.isSystem == false && f.sectionId == null && f.fieldType !== "section"
  );
  const filteredFieldsWithoutSec = () => {
    let childFilds = [];
    customSections.map(cf => {
      childFilds = [...childFilds, ...getChildComponents(cf.fieldId)];
    });
    let Bfiltered = differenceBy(nonSectionFields, childFilds, "fieldId");
    return [...Bfiltered, ...fieldsWithoutSections];
  };

  let data = filteredFieldsWithoutSec();
  return (
    <>
      <div>
        {customSections.map((section, secIndex) => {
          const customFieldComponents = getChildComponents(section.fieldId);
          return (
            <div key={section.fieldId}>
              <div
                className={classes.systemFieldsHeading}
                style={{ background: section.settings.color }}
                onClick={() => {
                  handleShowSystemField(section);
                }}>
                <EditableTextField
                  title={section.fieldName}
                  module={"Section"}
                  handleEditTitle={name => updateSectionName(name, section)}
                  titleProps={{
                    className: classes.sectionTitle,
                    onClick: () => {},
                    style: {
                      color: getContrastYIQ(section.settings.color),
                    },
                  }}
                  textFieldProps={{
                    customInputClass: {
                      input: classes.outlinedInputsmall,
                      root: classes.outlinedInputCnt,
                    },
                  }}
                  editTitle={editedSectionArr.indexOf(section.fieldId) >= 0}
                  formStyles={{
                    backgroundColor: "rgb(255 255 255 / 10%)",
                    width: "85%",
                    height: "24px",
                    borderRadius: 4,
                  }}
                />
                <div>
                  <>
                    {permission.isAllowEdit && !disabled && (
                      <>
                        <SvgIcon
                          viewBox="0 0 12 11.957"
                          className={classes.editIcon}
                          style={{ color: getContrastYIQ(section.settings.color) }}
                          onClick={e => {
                            e.stopPropagation();
                            // handleEdit(section);
                            updateCustomFieldDialogState({
                              moduleViewType: "editCopyModal",
                              data: section,
                              mode: "edit",
                            });
                          }}>
                          <IconEditSmall />
                        </SvgIcon>
                        <ColorPickerDropdown
                          selectedColor={"white"}
                          onSelect={color => colorChange(color, section)}
                          style={{ marginRight: 4, marginTop: -10 }}
                          placement="bottom-end"
                          disabled={disabled}
                          iconRenderer={
                            <SvgIcon
                              viewBox="0 5 25 12"
                              className={classes.documentIcon}
                              style={{ color: getContrastYIQ(section.settings.color) }}>
                              <IconColor />
                            </SvgIcon>
                          }
                          colors={[
                            "#89A3B2",
                            "#6A7AA1",
                            "#6886C5",
                            "#5EAAA8",
                            "#9E7777",
                            "#E59881",
                            "#DB996C",
                            "#AC8DAF",
                            "#949CDF",
                            "#F38BA0",
                            "#986D8E",
                            "#E36387",
                            "#ABB8C3",
                            "#0693E3",
                            "#FFF",
                          ]}
                        />
                      </>
                    )}

                    {openedSections.indexOf(section.fieldId) >= 0 ? (
                      <RemoveIcon
                        className={classes.systemFieldRemoveIcon}
                        style={{ color: getContrastYIQ(section.settings.color) }}
                        onClick={() => {
                          handleShowSystemField(section);
                        }}
                      />
                    ) : (
                      <AddIcon
                        className={classes.systemFieldAddIcon}
                        style={{ color: getContrastYIQ(section.settings.color) }}
                        onClick={() => {
                          handleShowSystemField(section);
                        }}
                      />
                    )}
                  </>
                </div>
              </div>
              {error.indexOf(section.fieldId) >= 0 && (
                <span className={classes.error}>Section name already exists!</span>
              )}
              {openedSections.indexOf(section.fieldId) >= 0 && (
                <Grid container spacing={1} classes={{ container: classes.sectionCnt }}>
                  {customFieldComponents.length > 0 ? (
                    <>
                      {customFieldComponents.map(cmp => {
                        return (
                          <CustomFieldsComponent
                            groupType={type}
                            disabled={disabled}
                            permission={permission.isAllowUpdate}
                            customFieldChange={customFieldChange}
                            customFieldData={customFieldData}
                            field={cmp}
                            handleDeleteField={handleDeleteField}
                            handleEdit={handleEditField}
                            handleCopyField={handleCopyField}
                            handleSelectHideOption={handleSelectHideOption}
                            customProps={{
                              customfieldlabelProps: {
                                editOperation: permission.isAllowEdit,
                                copyOperation: permission.isAllowAdd,
                                deleteOperation: permission.isAllowDelete,
                                hideOperation: permission.isAllowHide,
                              },
                              workspaceId : workspaceId
                            }}
                            groupId={groupId}
                          />
                        );
                      })}
                      <Grid item sm={12} md={12}>
                        <CustomFieldsChecklistView
                          groupType={type}
                          groupId={groupId}
                          handleClickHideOption={handleClickHideOption}
                          permission={permission}
                          isSystem={false}
                          isArchivedSelected={false}
                          renderFieldsWithoutSec={false}
                          handleEdit={handleEditField}
                          handleCopyField={handleCopyField}
                          handleDeleteField={handleDeleteField}
                          customfieldlabelProps={{
                            editOperation: permission.isAllowEdit,
                            copyOperation: permission.isAllowAdd,
                            deleteOperation: permission.isAllowDelete,
                            hideOperation: permission.isAllowHide,
                          }}
                          handleSelectHideOption={handleSelectHideOption}
                          sectionId={section.fieldId}
                          workspaceId={workspaceId}
                        />
                      </Grid>
                    </>
                  ) : (
                    <div className={classes.emptyContainer}>
                      <span className={classes.emptyTxt}>
                        You dont have any fields in this section yet!
                      </span>
                    </div>
                  )}
                </Grid>
              )}
            </div>
          );
        })}
        <Grid container spacing={1} classes={{ container: classes.sectionCnt }}>
          {data.map(field => {
            return (
              <CustomFieldsComponent
                groupType={type}
                disabled={disabled}
                permission={permission.isAllowUpdate}
                customFieldChange={customFieldChange}
                customFieldData={customFieldData}
                field={field}
                handleDeleteField={handleDeleteField}
                handleEdit={handleEditField}
                handleCopyField={handleCopyField}
                handleSelectHideOption={handleSelectHideOption}
                customProps={{
                  customfieldlabelProps: {
                    editOperation: permission.isAllowEdit,
                    copyOperation: permission.isAllowAdd,
                    deleteOperation: permission.isAllowDelete,
                    hideOperation: permission.isAllowHide,
                  },
                  workspaceId : workspaceId
                }}
                groupId={groupId}
              />
            );
          })}
          <Grid item sm={12} md={12}>
            <CustomFieldsChecklistView
              groupType={type}
              groupId={groupId}
              handleClickHideOption={handleClickHideOption}
              permission={permission}
              isSystem={false}
              isArchivedSelected={false}
              renderFieldsWithoutSec={true}
              handleEdit={handleEditField}
              handleCopyField={handleCopyField}
              handleDeleteField={handleDeleteField}
              customfieldlabelProps={{
                editOperation: permission.isAllowEdit,
                copyOperation: permission.isAllowAdd,
                deleteOperation: permission.isAllowDelete,
                hideOperation: permission.isAllowHide,
              }}
              handleSelectHideOption={handleSelectHideOption}
              data={data}
              workspaceId={workspaceId}
            />
          </Grid>
        </Grid>
      </div>
    </>
  );
}
Sections.defaultProps = {
  classes: {},
  theme: {},
  type: "",
  groupId: "",
  profileState: {},
  customFields: [],
  editCustomField: () => {},
  handleEditField: () => {},
  handleCopyField: () => {},
  handleSelectHideOption: () => {},
  updateCustomFieldData: () => {},
  customFieldChange: () => {},
  dispatchDeleteField: () => {},
  handleClickHideOption: () => {},
  disabled: false,
  customFieldData: {},
  permission: {},
};
const mapStateToProps = state => {
  return {
    profileState: state.profile.data || {},
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(sectionStyles, { withTheme: true }),
  connect(mapStateToProps, {
    editCustomField,
    updateCustomFieldData,
    dispatchDeleteField,
    updateCustomFieldDialogState,
    updateDeleteDialogState,
  })
)(Sections);
