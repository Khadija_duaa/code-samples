const sectionStyles = theme => ({
  systemFieldsHeading: {
    width: "100%",
    height: "32px",
    background: "#7F8F9A",
    borderRadius: "4px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "10px",
    marginTop: "8px",
    cursor: "pointer",
    "& h3": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      color: theme.palette.common.white,
      marginBottom: 3,
    },
    "&:hover $editIcon": {
      visibility: "visible",
    },
    "&:hover $documentIcon": {
      visibility: "visible",
    },
  },
  sectionTitle: {
    // paddingTop: "9px",
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    padding: "9px 0px 10px 5px",
    height: "fit-content",
    marginRight: 10,
    fontWeight: theme.typography.fontWeightExtraLight,
    color: "white",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
  outlinedInputCnt: {
    height: 39,
  },
  outlinedInputsmall: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
    padding: "0px 0px 1px 5px",
    color: "white",
  },
  editIcon: {
    top: "-2px",
    color: "#fff",
    position: "relative",
    visibility: "hidden",
    marginRight: "10px",
    fontSize: "13px !important",
    cursor: "pointer",
  },
  documentIcon: {
    color: "#fff",
    visibility: "hidden",
    fontSize: "21px !important",
    cursor: "pointer",

    // marginRight: "7px",
  },
  systemFieldRemoveIcon: {
    color: theme.palette.common.white,
    fontSize: "20px !important",
    cursor: "pointer",
  },
  systemFieldAddIcon: {
    color: theme.palette.common.white,
    fontSize: "20px !important",
    cursor: "pointer",
  },
  error: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
    color: "red",
  },
  sectionCnt: {
    margin: "10px 4px",
    width: 'auto'
  },
  emptyContainer: {
    height: 50,
    backgroundColor: "#f4f4f4",
    flex: 1,
    borderRadius: 4,
    margin: "0px 5px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  emptyTxt: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
    color:"#505050"
  },
});

export default sectionStyles;
