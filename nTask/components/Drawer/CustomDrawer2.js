import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";
import { Scrollbars } from "react-custom-scrollbars";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../Buttons/IconButton";
import ReactResizeDetector from "react-resize-detector";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import {
  calculateFilterContentHeight,
  calculateHeaderHeight,
} from "../../utils/common";
import sidebarExpandArrow from "../../assets/images/sidebarExpandArrow.svg"


const styles = (theme) => ({
  drawer: {
    flexShrink: 0,
  },
  drawerPaper: {
    background: theme.palette.common.white,
    position: "fixed",
    top: 108,
    display: "flex",
    zIndex: 100,
    flexDirection: "row",
    justifyContent: "stretch",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    overflow: "visible",
    borderTop: 0,
    borderBottom: 0,
  },
  drawerHeader: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  headerCnt: {
    background: "#404040",
    padding: "12px 15px",
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerTextCnt: {
    display: "flex",
    alignItems: "center",
  },
  filtersHeaderText: {
    marginLeft: 10,
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "16px",
  },
  headerCloseIcon: {
    fontSize: "11px",
    color: "#404040",
  },
  drawerExpandColapseArrow:{
    position: "absolute",
    left: -24,
    cursor: "pointer"
  }
});
class CustomDrawer2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterCntHeight: calculateFilterContentHeight(),
    };
  }
  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {}

  onResize = () => {
    this.setState({ filterCntHeight: calculateFilterContentHeight() });
  };
  handleClickAway = () => {
    this.props.closeDrawer();
  };
  render() {
    const { classes, theme, open, closeDrawer, headerTitle, drawerWidth, drawerProps, hideHeader } = this.props;
    const { filterCntHeight } = this.state;
    return (
      <ClickAwayListener onClickAway={()=>{}}>
      {/* <ClickAwayListener onClickAway={closeDrawer}> */}
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="right"
          open={open}
          PaperProps={{ style: { height: 'calc(100% - 40px)', top: calculateHeaderHeight(), display: "flex", flexDirection: "column", minWidth: drawerWidth, padding: !header && "43px 0 0 0", } }}
          
          classes={{
            paper: classes.drawerPaper,
          }}
          style={{minWidth: drawerWidth}}
          {...drawerProps}
        >
                      <img src={sidebarExpandArrow} onClick={closeDrawer} className={classes.drawerExpandColapseArrow}/>

          <ReactResizeDetector
            handleWidth
            handleHeight
            onResize={this.onResize}
          />
  {!hideHeader &&
          <div className={classes.drawerHeader}>
            <div className={classes.headerCnt}>
              <div className={classes.headerTextCnt}>
                <IconButton
                  btnType="grayBg"
                  onClick={closeDrawer}
                  style={{ padding: 4 }}
                >
                  <CloseIcon
                    classes={{ root: classes.headerCloseIcon }}
                    htmlColor={theme.palette.secondary.medDark}
                  />
                </IconButton>
                <Typography
                  variant="h3"
                  classes={{ h3: classes.filtersHeaderText }}
                >
                  {headerTitle}
                </Typography>
              </div>
            </div>
          </div>}
          {/* <Scrollbars
            autoHide
            style={{ minWidth: drawerWidth, height: filterCntHeight + 122 + "px", display: "flex", flexDirection: "column" }}
          > */}
            {this.props.children}
          {/* </Scrollbars> */}
        </Drawer>
      </ClickAwayListener>
    );
  }
}

CustomDrawer2.defaultProps = {
  drawerWidth: "auto",
  hideHeader: false
}
export default compose(
  withRouter,
  withStyles(styles, {
    withTheme: true,
  }),
  connect(null, {})
)(CustomDrawer2);
