import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";
import { Scrollbars } from "react-custom-scrollbars";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../Buttons/IconButton";
import ReactResizeDetector from "react-resize-detector";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import {
  calculateFilterContentHeight,
  calculateHeaderHeight,
} from "../../utils/common";

const drawerWidth = 330;

const styles = (theme) => ({
  drawer: {
    maxWidth: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    maxWidth: drawerWidth,
    background: theme.palette.common.white,
    position: "fixed",
    padding: "0 0 0 0",
    top: 108,
    display: "flex",
    height: "100%",
    zIndex: 100,
    flexDirection: "column",
    justifyContent: "stretch",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    overflowX: "hidden",
    borderBottom: 0,
  },
  drawerHeader: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  headerCnt: {
    background: "#404040",
    padding: "12px 15px",
    position: "relative",
    width: '100%',
    top: 0,
    left: 0,
    right: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerTextCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    width: '100%',
  },
  filtersHeaderText: {
    marginLeft: 10,
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "16px !important",
  },
  headerCloseIcon: {
    fontSize: "11px",
    color: "#404040",
  },
});
class CustomDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterCntHeight: calculateFilterContentHeight(),
    };
  }
  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {}

  onResize = () => {
    this.setState({ filterCntHeight: calculateFilterContentHeight() });
  };
  handleClickAway = () => {
    this.props.closeDrawer();
  };
  render() {
    const { classes, theme, open, closeDrawer, headerTitle } = this.props;
    const { filterCntHeight } = this.state;
    return (
      <ClickAwayListener onClickAway={this.handleClickAway}>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="right"
          open={open}
          PaperProps={{ style: { top: calculateHeaderHeight() } }}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <ReactResizeDetector
            handleWidth
            handleHeight
            onResize={this.onResize}
          />

          <div className={classes.drawerHeader}>
            <div className={classes.headerCnt}>
              <div className={classes.headerTextCnt}>
                <Typography
                  variant="h3"
                  classes={{ h3: classes.filtersHeaderText }}
                >
                  {headerTitle}
                </Typography>
                <IconButton
                  btnType="grayBg"
                  onClick={closeDrawer}
                  style={{ padding: 4 }}
                >
                  <CloseIcon
                    classes={{ root: classes.headerCloseIcon }}
                    htmlColor={theme.palette.secondary.medDark}
                  />
                </IconButton>
              </div>
            </div>
          </div>
          <Scrollbars
            autoHide
            // style={{ width: drawerWidth, height: filterCntHeight + 60 + "px" }}
            style={{ width: drawerWidth, height:"100%" }}
          >
            {this.props.children}
          </Scrollbars>
        </Drawer>
      </ClickAwayListener>
    );
  }
}

export default compose(
  withRouter,
  withStyles(styles, {
    withTheme: true,
  }),
  connect(null, {})
)(CustomDrawer);
