import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";
import { Scrollbars } from "react-custom-scrollbars";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../Buttons/IconButton";
import ReactResizeDetector from "react-resize-detector";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import {
  calculateFilterContentHeight,
  calculateHeaderHeight,
} from "../../utils/common";

const drawerWidth = 600;

const styles = (theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    background: theme.palette.common.white,
    position: "fixed",
    padding: "43px 0 0 0",
    top: 108,
    display: "flex",
    height: "100%",
    zIndex: 100,
    flexDirection: "row",
    justifyContent: "stretch",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    overflowX: "hidden",
    borderTop: 0,
    borderBottom: 0,
  },
  drawerHeader: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  headerCnt: {
    background: "#404040",
    padding: "12px 15px",
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerTextCnt: {
    display: "flex",
    alignItems: "center",
  },
  filtersHeaderText: {
    marginLeft: 10,
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "16px",
  },
  headerCloseIcon: {
    fontSize: "11px",
    color: "#404040",
  },
});
class GanttToDoDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterCntHeight: calculateFilterContentHeight(),
    };
  }
  componentDidMount() { }

  componentDidUpdate(prevProps, prevState) { }

  onResize = () => {
    this.setState({ filterCntHeight: calculateFilterContentHeight() });
  };
  handleClickAway = () => {
    this.props.closeDrawer();
  };
  render() {
    const { classes, theme, open, closeDrawer, headerTitle } = this.props;
    const { filterCntHeight } = this.state;
    return (
      <ClickAwayListener onClickAway={() => { }}>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="right"
          open={open}
          PaperProps={{ style: { top: calculateHeaderHeight() } }}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <ReactResizeDetector handleWidth handleHeight onResize={this.onResize} />

          <div className={classes.drawerHeader}>
            <div className={classes.headerCnt}>
              <div className={classes.headerTextCnt}>
                <IconButton
                  btnType="grayBg"
                  onClick={closeDrawer}
                  style={{ padding: 4 }}
                >
                  <CloseIcon
                    classes={{ root: classes.headerCloseIcon }}
                    htmlColor={theme.palette.secondary.medDark}
                  />
                </IconButton>
                <Typography
                  variant="h3"
                  classes={{ h3: classes.filtersHeaderText }}
                >
                  {headerTitle}
                </Typography>
              </div>
            </div>
          </div>
          <Scrollbars
            autoHide={false}
            style={{ width: drawerWidth, height: filterCntHeight + 60 + "px" }}
          >
            {this.props.children}
          </Scrollbars>
        </Drawer>
      </ClickAwayListener>
    );
  }
}

export default compose(
  withRouter,
  withStyles(styles, {
    withTheme: true,
  }),
  connect(null, {})
)(GanttToDoDrawer);
