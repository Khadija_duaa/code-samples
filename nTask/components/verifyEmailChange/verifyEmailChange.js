import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { confirmChangeEmail } from "../../redux/actions/profileSettings";

function VerifyEmailChange(props) {
  confirmChangeEmail(
    (data) => {
      props.history.push("/tasks");
    },
    () => {}
  );
  return <></>;
}
export default withRouter(VerifyEmailChange);
