import React from "react";
import loaderStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";

function MasterLoader(props){
    const {classes, sidebar} = props;

    return (
        <div className={classes.mainContainer}>

          {sidebar ?
           <div className={classes.sideBar}>

                <div className={classes.sideBarInner}>
                    <div className={classes.top}>
                        <div className={classes.item}></div>
                    </div>
                    <div className={classes.bottom}>
                        <div className={classes.item}></div>
                        <div className={classes.item}></div>
                        <div className={classes.item}></div>
                        <div className={classes.item}></div>
                        <div className={classes.item}></div>
                        <div className={classes.item}></div>
                    </div>
                </div>
           </div> : <div className={classes.sidebarPlaceHolder}></div>}
           <div className={classes.header} style={{marginLeft: sidebar && 180}}>
                <div className={classes.headerLeft}>
                    <div className={classes.circle}></div>
                    <div className={classes.line}></div>
                </div>
                <div className={classes.headerRight}>
                    <div className={classes.circle}></div>
                    <div className={classes.circle}></div>
                    <div className={classes.line}></div>
                    <div className={classes.circle}></div>
                    <div className={classes.circle}></div>
                </div>
           </div>
           <div className={classes.footer} style={{marginLeft: sidebar && 180}}>
                <div className={classes.footerLeft}>
                    <div className={classes.line}></div>
                </div>
                <div className={classes.footerRight}>
                    <div className={classes.footerInnerRight}>
                        <div className={classes.line}></div>
                        <div className={classes.line2}></div>
                    </div>
                    <div className={classes.footerInnerLeft}>
                        <div className={classes.circle}></div>
                    </div>
                </div>
           </div>
        </div>
    )
}

MasterLoader.defaultProps = {
  sidebar: true
}

export default withStyles(loaderStyles, {withTheme: true})(MasterLoader);