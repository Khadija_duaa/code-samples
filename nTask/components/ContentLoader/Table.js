import React from "react";
import loaderStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";


function TableLoader(props){
    const {classes} = props;
    return (
        <div className={classes.table}>
           <div className={classes.topSection}>
               <div className={classes.item2}></div>
               <div className={classes.item2}></div>
           </div>
           <div className={classes.bottomSection}>
                <div className={classes.item}></div>
                <div className={classes.item1}></div>
                <div className={classes.item2}></div>
                <div className={classes.item1}></div>
                <div className={classes.item2}></div>
                <div className={classes.item1}></div>
                <div className={classes.item2}></div>
                <div className={classes.item1}></div>
                <div className={classes.item2}></div>
                <div className={classes.item1}></div>
                <div className={classes.item2}></div>
           </div>
        </div>
    )
}

export default withStyles(loaderStyles, {withTheme: true})(TableLoader);