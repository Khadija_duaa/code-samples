const loaderStyles = theme => ({
  contentLoaderCnt: {
    padding: "20px 30px 0 20px"
  },
  topSection: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 40,

    "& $topLeft": {
      borderRadius: 5,
      backgroundColor: theme.palette.background.items,
      maxWidth: 400,
      width: "100%",
      height: 37
    },
    "& $topRight": {
      width: 561,
      display: "flex",
      justifyContent: "flex-end",

      "& $item1": {
        borderRadius: 5,
        flex: "0 245px",
        height: 43,
        backgroundColor: theme.palette.background.items
      },
      "& $item2": {
        borderRadius: 5,
        flex: "0 229px",
        height: 43,
        backgroundColor: theme.palette.background.items,
        marginRight: 15
      },
      "& $item3": {
        borderRadius: 5,
        flex: "0 42px",
        height: 43,
        backgroundColor: theme.palette.background.items
      }
    }
  },
  headerItems: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 27,
    "& $headerItemsLeft": {
      "& $item1": {
        borderRadius: 5,
        flex: "0 136px",
        height: 20,
        backgroundColor: theme.palette.background.items,
        minWidth: 100
      }
    },
    "& $headerItemsRight": {
      display: "flex",
      justifyContent: "flex-end",
      "& $item2": {
        borderRadius: 5,
        flex: "0 60px",
        height: 20,
        backgroundColor: theme.palette.background.items,
        minWidth: 100,
        marginRight: "20%"
      },
      "& $item3": {
        borderRadius: 5,
        flex: "0 65px",
        height: 20,
        backgroundColor: theme.palette.background.items,
        minWidth: 100,
        marginRight: "20%"
      },
      "& $item4": {
        borderRadius: 5,
        flex: "0 58px",
        height: 20,
        backgroundColor: theme.palette.background.items,
        minWidth: 100,
        marginRight: "20%"
      },
      "& $item5": {
        borderRadius: 5,
        flex: "0 72px",
        height: 20,
        backgroundColor: theme.palette.background.items,
        minWidth: 100
      }
    }
  },
  list: {
    display: "flex",
    flexDirection: "column",
    "& $item": {
      flex: 1,
      marginBottom: 7,
      backgroundColor: theme.palette.background.items,
      padding: 20,
      borderRadius: 5,
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      "& $itemLeft": {
        width: "10%",
        height: 15,
        backgroundColor: "#e3e3e3",
        borderRadius: 2
      },
      "& $itemRight": {
        display: "flex",
        justifyContent: "flex-end",
        width: "42%",
        "& $itemRightInner1": {
          height: 15,
          backgroundColor: "#e3e3e3",
          borderRadius: 2,
          marginRight: 15,
          width: "80%"
        },
        "& $itemRightInner2": {
          height: 15,
          backgroundColor: "#e3e3e3",
          borderRadius: 2,
          minWidth: 20,
          width: "3%"
        }
      }
    }
  },
  calendarFilter: {
    width: 280,
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 25,
    "& $item1": {
      width: 60,
      height: 37,
      backgroundColor: theme.palette.background.items,
      borderRadius: 5
    },
    "& $item2": {
      width: 184,
      height: 37,
      backgroundColor: theme.palette.background.items,
      borderRadius: 5
    }
  },
  calendar: {
    width: "100%",
    backgroundColor: theme.palette.background.items,
    height: window.innerWidth - 1300,
    borderRadius: 5
  },
  table: {
    width: "100%",
    height: window.innerWidth - 1300,
    borderRadius: 5,
    "& $topSection": {
      display: "flex",
      backgroundColor: theme.palette.background.default,
      borderRadius: 5,
      justifyContent: "space-between",
      padding: "40px 20px",
      marginBottom: 40,
      "& $item1": {
        padding: "15px 40px",
        backgroundColor: theme.palette.background.light,
        borderRadius: 5
      },
      "& $item2": {
        padding: "15px 40px",
        backgroundColor: theme.palette.background.light,
        borderRadius: 5
      }
    },
    "& $bottomSection": {
      borderRadius: 5,
      "& $item": {
        height: 37,
        backgroundColor: theme.palette.background.dark
      },
      "& $item1": {
        height: 37,
        backgroundColor: theme.palette.background.default
      },
      "& $item2": {
        height: 37,
        backgroundColor: theme.palette.background.light
      }
    }
  },
  bottomSection: {},
  grids: {
    "& $gridContainer": {
      "& $gridItem": {
        backgroundColor: theme.palette.background.items,
        height: 165,
        borderRadius: 5,
        padding: 15,
        "& $gridItemInner": {
          position: "relative",
          minHeight: "100%",
          "& $gridTop": {
            display: "flex",
            justifyContent: "flex-start",
            alignItems: "center",
            "& $box": {
              width: 30,
              height: 30,
              backgroundColor: "#e3e3e3",
              borderRadius: 5
            },
            "& $line": {
              maxWidth: 200,
              width: "100%",
              height: 18,
              backgroundColor: "#e3e3e3",
              borderRadius: 3,
              marginLeft: 15
            }
          },
          "& $gridMiddle": {
            display: "flex",
            justifyContent: "flex-start",
            marginTop: 14,
            "& $circle": {
              width: 30,
              height: 30,
              backgroundColor: "#e3e3e3",
              borderRadius: "100%",
              marginRight: 10
            }
          },
          "& $gridBottom": {
            marginTop: 27,
            "& $thinLine": {
              minWidth: "100%",
              backgroundColor: "#e3e3e3",
              height: 4,
              borderRadius: 1.5
            }
          },
          "& $lineBottom": {
            position: "absolute",
            bottom: 0,
            left: 0,
            minWidth: "100%",
            height: 18,
            backgroundColor: "#e3e3e3",
            borderRadius: 3
          }
        }
      }
    }
  },
  timesheetTopSection: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 32,
    "& $topLeft": {
      width: 480,
      display: "flex",
      justifyContent: "space-between",
      "& $left": {
        borderRadius: 5,
        flex: "0 138px",
        backgroundColor: theme.palette.background.items,
        height: 37
      },
      "& $right": {
        borderRadius: 5,
        flex: "0 290px",
        backgroundColor: theme.palette.background.items,
        height: 37
      }
    },
    "& $topRight": {
      borderRadius: 5,
      width: 42,
      backgroundColor: theme.palette.background.items,
      height: 37,
      borderRadius: 5
    }
  },
  filter: {
    width: 340,
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 32,
    "& $left": {
      flex: "0 206px",
      backgroundColor: theme.palette.background.items,
      height: 34,
      borderRadius: 5
    },
    "& $right": {
      flex: "0 100px",
      backgroundColor: theme.palette.background.items,
      height: 34,
      borderRadius: 5
    }
  },
  sidebarPlaceHolder: {
    maxWidth: 180,
    width: 180,
    position: "fixed",
    top: 0,
    bottom: 0,
    left: 0,
    zIndex: "1200",
  },
  mainContainer: {
    display: "flex",
    justifyContent: "space-between",
    "& $sideBar": {
      maxWidth: 180,
      width: 180,
      // position: "fixed",
      // top: 0,
      // bottom: 0,
      // left: 0,
      zIndex: "1200",
      backgroundColor: "#333333",
      "& $sideBarInner": {
        "& $top": {
          padding: 22,
          backgroundColor: "#404040",
          "& $item": {
            width: "100%",
            height: 27,
            backgroundColor: "#4a4a4a",
            borderRadius: 3,
            margin: "0 auto"
          }
        },
        "& $bottom": {
          padding: "20px 16px 16px 16px",
          "& $item": {
            width: "100%",
            height: 27,
            backgroundColor: "#4a4a4a",
            borderRadius: 3,
            margin: "0 auto",
            marginBottom: 12
          }
        }
      }
    },
    "& $header": {
      height: 70,
      position: "fixed",
      top: 0,
      right: 0,
      left: 0,
      marginLeft: 240,
      zIndex: "1200",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      "& $headerLeft": {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        maxWidth: 400,
        width: "100%",
        "& $circle": {
          backgroundColor: "#e3e3e3",
          width: 40,
          height: 40,
          borderRadius: "100%",
          marginRight: 10,
          marginLeft: 22
        },
        "& $line": {
          backgroundColor: "#e3e3e3",
          maxWidth: 180,
          width: "100%",
          height: 20,
          borderRadius: 3
        }
      },
      "& $headerRight": {
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        maxWidth: 600,
        width: "100%",
        "& $circle": {
          backgroundColor: "#e3e3e3",
          width: 40,
          height: 40,
          borderRadius: "100%",
          margin: "0 7px"
        },
        "& $line": {
          backgroundColor: "#e3e3e3",
          maxWidth: 180,
          width: "100%",
          height: 30,
          borderRadius: 3,
          margin: "0 7px"
        }
      }
    },
    "& $footer": {
      height: 70,
      position: "fixed",
      bottom: 0,
      right: 0,
      left: 0,
      marginLeft: 240,
      zIndex: "1200",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      "& $footerLeft": {
        maxWidth: 300,
        width: "100%",
        "& $line": {
          backgroundColor: "#e3e3e3",
          maxWidth: 250,
          width: "100%",
          height: 20,
          borderRadius: 3,
          marginLeft: 22
        }
      },
      "& $footerRight": {
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        maxWidth: 500,
        width: "100%",
        "& $footerInnerRight": {
          maxWidth: 500,
          width: "100%",
          display: "flex",
          justifyContent: "flex-end",
          flexDirection: "column",
          alignItems: "flex-end",
          "& $line": {
            backgroundColor: "#e3e3e3",
            maxWidth: 350,
            width: "100%",
            height: 15,
            marginBottom: 7,
            borderRadius: 3
          },
          "& $line2": {
            backgroundColor: "#e3e3e3",
            maxWidth: 250,
            width: "100%",
            height: 13,
            borderRadius: 3
          }
        },
        "& $footerInnerLeft": {
          maxWidth: 50,
          width: "100%",
          marginRight: 20,
          "& $circle": {
            backgroundColor: "#e3e3e3",
            width: 40,
            height: 40,
            borderRadius: "100%",
            margin: "0 7px"
          }
        }
      }
    }
  },
  topLeft: {},
  topRight: {},
  item: {},
  item1: {},
  item2: {},
  item3: {},
  item4: {},
  item5: {},
  gridItem: {},
  gridContainer: {},
  left: {},
  right: {},
  gridTop: {},
  box: {},
  line: {},
  gridMiddle: {},
  circle: {},
  gridBottom: {},
  thinLine: {},
  lineBottom: {},
  gridItemInner: {},
  headerItemsLeft: {},
  headerItemsRight: {},
  itemLeft: {},
  itemRight: {},
  itemLeftInner: {},
  itemRightInner1: {},
  itemRightInner2: {},
  top: {},
  bottom: {},
  sideBar: {},
  sideBarInner: {},
  header: {},
  headerLeft: {},
  headerRight: {},
  footer: {},
  footerLeft: {},
  footerRight: {},
  footerInnerRight: {},
  footerInnerLeft: {},
  line2: {}
});

export default loaderStyles;
