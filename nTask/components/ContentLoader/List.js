import React from "react";
import loaderStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import { calculateContentHeight } from "../../utils/common";


function ListLoader(props){
    const {classes, style} = props;
    return (
        <div className={classes.contentLoaderCnt} style={style}>
           <div className={classes.topSection}>
                <div className={classes.topLeft}>
                </div>
                <div className={classes.topRight}>
                    <div className={classes.item2}></div>
                    <div className={classes.item3}></div>
                </div>
           </div>
           <div className={classes.headerItems}>
                <div className={classes.headerItemsLeft}>
                    <div className={classes.item1}></div>
                </div>
                <div className={classes.headerItemsRight}>
                    <div className={classes.item2}></div>
                    <div className={classes.item3}></div>
                    <div className={classes.item4}></div>
                    <div className={classes.item5}></div>
                </div>
           </div>
           <div className={classes.list}>
                <div className={classes.item}>
                    <div className={classes.itemLeft}>
                        <div className={classes.itemLeftInner}></div>
                    </div>
                    <div className={classes.itemRight}>
                        <div className={classes.itemRightInner1}></div>
                        <div className={classes.itemRightInner2}></div>
                    </div>
                </div>
           </div>
           <div className={classes.list}>
                <div className={classes.item}>
                    <div className={classes.itemLeft}>
                        <div className={classes.itemLeftInner}></div>
                    </div>
                    <div className={classes.itemRight}>
                        <div className={classes.itemRightInner1}></div>
                        <div className={classes.itemRightInner2}></div>
                    </div>
                </div>
           </div>
           <div className={classes.list}>
                <div className={classes.item}>
                    <div className={classes.itemLeft}>
                        <div className={classes.itemLeftInner}></div>
                    </div>
                    <div className={classes.itemRight}>
                        <div className={classes.itemRightInner1}></div>
                        <div className={classes.itemRightInner2}></div>
                    </div>
                </div>
           </div>
           <div className={classes.list}>
                <div className={classes.item}>
                    <div className={classes.itemLeft}>
                        <div className={classes.itemLeftInner}></div>
                    </div>
                    <div className={classes.itemRight}>
                        <div className={classes.itemRightInner1}></div>
                        <div className={classes.itemRightInner2}></div>
                    </div>
                </div>
           </div>
        </div>
    )
}

export default withStyles(loaderStyles, {withTheme: true})(ListLoader);