import React from "react";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import loaderStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";


function GridLoader(props){
    const {classes} = props;
    return (
        <div>
            <div className={classes.contentLoaderCnt}>
                <div className={classes.topSection}>
                    <div className={classes.topLeft}>
                    </div>
                    <div className={classes.topRight}>
                        <div className={classes.item2}></div>
                        <div className={classes.item3}></div>
                    </div>
            </div>
            <div className={classes.grids}>
                    <Grid className={classes.gridContainer} container spacing={4}>
                        <Grid  item xs={12}
                                    sm={6}
                                    md={6}
                                    lg={4}
                                    xl={3}
                        >
                        <div className={classes.gridItem}>
                            <div className={classes.gridItemInner}>
                                <div className={classes.gridTop}>
                                    <div className={classes.box}></div>
                                    <div className={classes.line}></div>
                                </div>
                                <div className={classes.gridMiddle}>
                                    <div className={classes.circle}></div>
                                    <div className={classes.circle}></div>
                                    <div className={classes.circle}></div>
                                </div>
                                <div className={classes.gridBottom}>
                                    <div className={classes.thinLine}></div>
                                </div>
                                <div className={classes.lineBottom}></div>
                            </div>
                        </div>
                        </Grid>
                    </Grid>
                </div>
            </div>   
        </div>
    )
}

export default withStyles(loaderStyles, {withTheme: true})(GridLoader);