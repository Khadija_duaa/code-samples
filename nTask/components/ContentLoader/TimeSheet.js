import React from "react";
import loaderStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";


function TimeSheetLoader(props){
    const {classes} = props;
    return (
        <div>
           <div className={classes.timesheetTopSection}>
                <div className={classes.topLeft}>
                    <div className={classes.left}></div>
                    <div className={classes.right}></div>
                </div>
                <div className={classes.topRight}></div>
           </div>
           <div className={classes.filter}>
                <div className={classes.left}></div>
                <div className={classes.right}></div>
           </div>
           <div className={classes.calendar}></div>
        </div>
    )
}

export default withStyles(loaderStyles, {withTheme: true})(TimeSheetLoader);