import React from "react";
import loaderStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";


function CalendarLoader(props){
    const {classes} = props;
    return (
        <div>
           <div className={classes.topSection}>
                <div className={classes.topLeft}>
                </div>
                <div className={classes.topRight}>
                    <div className={classes.item1}></div>
                    <div className={classes.item2}></div>
                    <div className={classes.item3}></div>
                </div>
           </div>
           <div className={classes.calendarFilter}>
                <div className={classes.item1}></div>
                <div className={classes.item2}></div>
           </div>
           <div className={classes.calendar}></div>
        </div>
    )
}

export default withStyles(loaderStyles, {withTheme: true})(CalendarLoader);