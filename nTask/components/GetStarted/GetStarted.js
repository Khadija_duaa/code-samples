import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import withStyles from "@material-ui/core/styles/withStyles";
import {
  getYoutubeVideos,
  UpdateUserState
} from "../../redux/actions/onboarding";
import videoList from "../Onboarding/videoList";
import getStartedStyles from "./styles";
import { Scrollbars } from "react-custom-scrollbars";
import Typography from "@material-ui/core/Typography";
import DefaultButton from "../Buttons/DefaultButton";
import { DeleteTempData } from "../../redux/actions/tempdata";
import ntaskLogo from "../../assets/images/nTask_logo_getStarted.png";
const circleIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHkAAABmCAYAAADvej4HAAAGSklEQVR4nO2da4hVVRTH/5Mmakn2piihhzWalFBgUqNM0IP6kJkYmD21MgwkCPtSUfQpiMroPRk9pNAy6ktpyOg4JTOl0GTUTGiGmJHN0DhNOjM43VjDOnE769y599xz19577l0/ONxhrfPYZ/3nnLvvfqxdN9R8HWK8AeC+uNFQoQnA/UknPr5xs7CVy3Gx404DsNT0dMZSjrkqcZHvBDCpOuMZJBTru7QLFhfZXtPuWa59xXyRLwdQL/YwtKnn2DsRebHwGq5QjX2+yAuF13DFLS5EvgDAhcJruGI6a6Aq8g3CY7hGTYNI5KuFx3CNmgaRyFcJj1FVIp8O4FzhMVxzDmuhIvKlwmr4YraWyJcIq+GLGVoiny+shi9UtCCRzxNWo+pEPltYDV+cpSWyen+mUTJqteupwmr44iQtkccJq+GL8VoiTxFWwxcnaolsVDkk8l8mcjD0axSERB4WVsMXx7RE7hVWwxeHtUTuFlbDF39oiXxQWA1f/KYl8j5hNXyhogWJ/LOwGr7YqyXyD8Jq+OJHLZE7hNXwxXdaIlON7oDwGK4hDX7XEpn4UngM16hpYCKHw1daJYlE3iQ8hms+1xZ5r1b13SgJ1fjndzV+LLyGKzZqXidf5PXCa7jiQ1ci7wLQKfYwtOkCsNOVyMSbYg9DmybtC8RFfgfAUbGXocVRjrkqcZGpb3mdSeqMdS7685MG8j0H4B9hNSoNxfh5F1FNErnTatpOWK/V6xQnSWTiSa1BZcYIFNunXIWikMg/cSJVQ4cm/unkhEIiE08A+FNYjazQ6NjHXUZxNJF7ADwqrEZWVnNsnTGayODGkW3CapTLNh8NTsVEzgG4B0Cf8Bhp6eNY5lxHrpQJb78AWCmsRloe4lg6p9RZjdQys1ZYjVKh2L3nK1pppq7S0/y1sBrF2On7TZhG5EEAt9rIzlQc4DTHgz4LkXYSOhX6Rq3Zd1XGYY6V94einEwDuzltr8qE6Sqhn2O0O4TbKTedRBuAm0zoRPo5Nm1JTh9kyRmyHcC1Non9f/RyTLYLj0eyJoah/9Z5APYLT+2xn2MRzBMcUYnsP/S9cyWAb4SndqB7nxvKd3CcpLUay2UigJcALPN5Qx5Yy61ZA2kuPX7XTGHTopJ5vAZ4tbI7aqStu4+XPVyeVuAUXMS9Vls47ccgbwfZtpr3GZVKPsn5UErftwDMF57qoAXAvVmyNBR5kiluTwNoEJ5kWrmPuiXJq5WRj26+kZeNrabady/fU6NSGg76ynsNwNYUAoP33crHTow7NdMu5niYCy1s9coYHzN2jO9hOt+TRnfhKQCaATxAb1jhLU4dH9vM5/oPF7k1u7mBfhaA98fYcF8q6wdc9pWKY6QnA/iMa+hZmcvnmhydx2UCVRq4djuAmTxIUKuyUgkGuIy0CMsSB4Pu1gCYI6zlM4fPOYKPLLld/FqZBuCRwCbZdXKZpnEZXZRtvtLPzmVRxVerdp2WKwDcBmCBh4VB9wD4hAe7q84uzCevdt2quLobpQlpCEXkfEjk63lpwQZe+ayS/Mpty5SjYzOL7BwW+TIA3ypfe7ZKOvyM7OHtZT7NGbwK3Sxe3oi2M3mBlJMBTABwAu/7N4AhHi/ezSmT9vH2PefJOhTQvbpYs3phiCLHOcStO1uEZ+yT5rdwucyz5Qn8Uu/g6hfX5XLOhwEb1Jry7MPgdugJyvEYsie5BjCR/eJiTlSPiewXF40tXSayX1odXL3VRPaLiyyIG01kv3QoZyimc3eYyP55TKl/OhdlNDCR/dOilJWvKUogYCKHAbWMtFewJO18zhFM5DA4wlNrKiF0O5/rSGQwkcOhhwcIvl7md3SOj70m3shiIocFJVRdwWLvSFGyHSzuivwnOGIsdDXWIi08aIJ6qW7mQRQzAJzKsejhlI006OHTYi1nJnLYdPL2TJZSWldjDWDfyTWAiVwDmMg1QBAVr7o1wlSQ3KpCHqMQoT7JUwGs4k8jI6GKvBjAC/xpZCRUkcfFPo0MhNQYQq/mRfyPF80Nos9hnkL6kaWTKo+QRL47YQmdJbwRUxL8RgmEJPLb/MRO4pRRC3i2YRs33L8rjjBKIiSR6VX8Iv/9IIv8BYBXxZ5GKkKteA3HPo0MhNoLtYFzXmwQHiMdAP4FDf4h+JfUbHQAAAAASUVORK5CYII="
const smallCircles = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAAA9CAYAAAAQyx+GAAAFA0lEQVR4nOWbaWgdVRiGn9xEC2oqiBS1irUYTN2QusYt1QhqoraCChUEQYpFVFBKiwstbkVboYKKRERBUdC6tKi1uCQaUUTUilTaYqp1q8u/WqlYrZFT35Gb+ebOOXMzuZm5eeD8+c467z1z5pzvO7dlZGSEMjHy4C1po90XmKfUBRwG7AZ+ADYArwBrZTO0LFr1v6nN5JaXy4EVwNEJYnUqzQeGgSXAy2lPWjGW8tEKPKAHjYuShCvzkuq0JuQ3zYxZDiw2Vj9RnSVJJcs+Y66oU5SIxWrDUGZh3Nqxyliz85DaGkWZhbkSONxYszMduCpeq8zCzDOWHNsqszCnGkv9nBKvWWZhDjGW+jk0XrPMwiTuXvOizML8ZCw5tlVmYb4wlvr5JF6zzMKsMZYc2yqzMKt1ah4rP6qtUZR98b3VWLPj/Bh/xmuV/azkfumVxhrOyqTZQpO4HW4DHjFWP4+qbiJtczfdnmSfENbOWl5Pt3uAm4B3gfsDfDLDEuRFk1NFM3nwnPPpVR0unTfvZB0Q0QL7qVybq0M2h1mFmQb0At3A8cAMYKryfgO2ARuB94D1wM+mhfHFPfCzSmMiVJhz5NS5KKXOwUruQHatpvgb8sO+b0rXSbXDejzxLb4zgdeAIeCSFFGSaFWdIbUxM6FMYUkTZr5CDn0mJzt9auvqsguzFHiuav3Ig6l695c25tHGRpIw7vt9l7Hmh2v7jsIqIuLCuNfnPlMqf+5VX4WlWpijgP4GDrRffRaSamEeBtoDBzkAXA90AFOUOmQbMKWTaVefhRbm7MCvz2bgPKAHeFzb691Kw7L1qMxmU9vSpz1S4YiECYnmuf3I6TqT+HBlTtMO2MciT/6EUNE2/2JP51uAy7TtD2UnMDdg5vRqDIUTptezo3UXaK4DdpgcPztUN+0STpvGUCgqOhCm8Q7wQUq+jw/VRhpzUvImhIpOyWm8kJIXiq+N44ylAMLMMNbRDBpLdnyf8MLtZyoBe5fvjCU733tqhO6fGoYTpsXT2RRjmQRU9FlNY3pKXihHeMr5xtBwnDDfeDrNY2d6vrGMZpuxTDBOmC89QzC3jerA18ZGYymAML4tvjv7nGms4XSpjTRCjg4NpSKH9d8pnbrF+QngQJPjx9V50rPAu77XGWsBhPlFoY40ZumqeZbParvqdJqc0azXGApFdLpeETAod3T4OHD7PkdlfceN0L4bTiSMi/u8HtB5p3bCbwELFBLZB9hfeQuUNxgwU9ArlFvMKU+qT9U3A+cGvi4XKI2FnYo5F5Jq1+bXwMIGDnKh+iy8MCiW1Ii4TxS3KixJcaV7gGXGmh/L1EehqeW5u1vObRfiOMDk1sfviiKM+0z5a/BCY8tK0oyJcA9wUuDXysc6tdXo16dDX8oTTI6HNGEcW3VjoVs3FvaYErX5R3W6FSbZWrPk+OA2pZ8rpOPuBL+tcQRdr6v1KsUZUpqmxqOLQ0dWHRWc4/tbHQiHdNQwN64biHuf9qvqrkdpi/6j9DSwq9Zwst6o+hV4SqnofKRZG58hxwCPKX7er0uK2+PPUrq/F4dQtfg6P9CdiozWwkVRn9e/5TZEZZrhOmsaAxJnNvBMjUuJ7m9/1wCfqfxeEZt9xsRxf1C/UduGg0zuf7gPzFnNPmPibNfFKOeDvkELcRx3d7BrsgkTsUsL8LHApbG41x/Am5PtVUrjROCMvS4T+OpfLNLpfCoRIPgAAAAASUVORK5CYII=";
class GetStarted extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videosData: [],
      selectedVideo: ""
    };
    this.playVideo = this.playVideo.bind(this);
  }
  playVideo(videoId) {
    this.setState({ selectedVideo: videoId });
  }
  redirectToDashboard = () => {
    this.props.UpdateUserState(data => {
      if (data && data.status === 200) {
        //Redirect Here
        localStorage.removeItem("CompanyInfo");
        localStorage.removeItem("CreateProfile");
        localStorage.removeItem("CreateWorkspace");
        localStorage.removeItem("InviteMembers");
        localStorage.removeItem("Onboarding");
        localStorage.removeItem("teamId");
        //this.props.authAction(true);
        this.props.DeleteTempData();
        this.props.history.replace(`/tasks`);
      } else {
      }
    });
  };
  render() {
    const { classes, profile, companyInfo } = this.props;
    const firstName = this.props.location.state && firstName
    const companyName = companyInfo ? companyInfo.companyName : "nTask";
    const companyLogo = companyInfo ? companyInfo.headerImageUrl : ntaskLogo;
    return (
      <div className={classes.getStartedCnt}>
        <img src={circleIcon} className={classes.circleBg}/>
        <img src={smallCircles} className={classes.smallCircleBg}/>
        <div>
         <div style={{textAlign: "center"}}>
          <img src={companyLogo} />
        </div>
        <div className={classes.getStartedHeadingCnt}>
        <Typography variant="h1" align="center" classes={{h1: classes.mainHeading}}>Hi, Welcome to the {companyName} 2.0</Typography>
        <Typography variant="body1" align="center" className={classes.mainHeadingTagline}>
          Smart project management platform that's built around your needs.
        </Typography>
        </div>
       
        {/* <div className={classes.VideosCnt}>
          <iframe
            id="ytplayer"
            type="text/html"
            width="720"
            height="405"
            src={`https://www.youtube.com/embed/${selectedVideo}?controls=0&disablekb=1&fs=0&iv_load_policy=3`}
            frameBorder="0"
            allowFullScreen
          />

          
          <div className={classes.videoListCnt}>
            <Scrollbars autoHide style={{ height: 405, width: 350 }}>
              <ul className={classes.videoList}>
                {videosData.map((video, i) => {
                  return (
                    <li
                      key={i}
                      onClick={event => {
                        this.playVideo(video.snippet.resourceId.videoId);
                      }}
                    >
                      <div className={classes.videoThumbnailCnt}>
                        <img
                          src={video.snippet.thumbnails.medium.url}
                          width={110}
                        />
                      </div>
                      <div className={classes.videoThumbnailDetailsCnt}>
                        <Typography variant="body2">Tutorial #{i+1} </Typography>
                        <Typography variant="h6">
                          {video.snippet.title}
                        </Typography>
                      </div>
                    </li>
                  );
                })}
              </ul>
            </Scrollbars>
          </div>
        </div> */}
        <div className={classes.getStartedBtnCnt}>
        <DefaultButton
              text="Get Started"
              buttonType="Plain"
              style={{padding: "11px 31px", fontSize: "18px"}}
             onClick={this.redirectToDashboard}
            />
            </div>
            </div>
     </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.profile.data,
    companyInfo: state.whiteLabelInfo.data
  }
}
export default compose(
  withRouter,
  withStyles(getStartedStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    {
      getYoutubeVideos,
      UpdateUserState,
      DeleteTempData
    }
  )
)(GetStarted);
