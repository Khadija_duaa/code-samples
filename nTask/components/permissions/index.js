export const GetPermission = {
    canView: (isOwner, permissions, key, check) => {
        // return isOwner ? true : permissions[key][check]
        return true
    },

    canDo: (isOwner, permissions, key) => {
        // return isOwner ? true : permissions[key]
        return true
    },

    canAdd: (isOwner, permissions, key) => {
        // return isOwner ? true : permissions[key].add
        return true

    },

    canEdit: (isOwner, permissions, key) => {
        // return isOwner ? true : permissions[key].edit
        return true

    },

    canDelete: (isOwner, permissions, key) => {
        // return isOwner ? true : permissions[key].delete
        return true

    }
}