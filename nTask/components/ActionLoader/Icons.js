import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import IconProjectOutlined from '../../components/Icons/IconProjectOutlined'
import IconIssueOutlined from '../../components/Icons/IconIssueOutlined'
import IconTaskOutlined from '../../components/Icons/IconTaskOutlined'
import IconRiskOutlined from '../../components/Icons/IconRiskOutlined'
import IconMeetingOutlined from '../../components/Icons/IconMeetingOutlined'
import IconCheck from '../../components/Icons/IconCheck'
import IconFailed from '../../components/Icons/IconFailed'
import clsx from "clsx";

export function getIcon(classes, value, theme) {
  /** get icons on the basis of field type */
  switch (value) {
    case "projects":
      return (
        <div className={clsx({ [classes.icon]:true,[classes.iconProjects]:true})}>
          <SvgIcon viewBox="0 0 16 16" fontSize="inherit">
            <IconProjectOutlined />
          </SvgIcon>
        </div>
      );
      break
    case "issues":
      return (
        <div className={clsx({ [classes.icon]:true,[classes.iconIssues]:true})}>
          <SvgIcon viewBox="0 0 15.488 18" fontSize="inherit">
            <IconIssueOutlined />
          </SvgIcon>
        </div>
      );
      break
    case "tasks":
      return (
        <div className={clsx({ [classes.icon]:true,[classes.iconTasks]:true})}>
          <SvgIcon viewBox="0 0 16 17.641" fontSize="inherit">
            <IconTaskOutlined />
          </SvgIcon>
        </div>
      );
      break
    case "risks":
      return (
        <div className={clsx({ [classes.icon]:true,[classes.iconRisks]:true})}>
          <SvgIcon viewBox="0 0 16 15.111" fontSize="inherit">
            <IconRiskOutlined />
          </SvgIcon>
        </div>
      );
      break
    case "meetings":
      return (
        <div className={clsx({ [classes.icon]:true,[classes.iconMeetings]:true})}>
          <SvgIcon viewBox="0 0 16 17.641" fontSize="inherit">
            <IconMeetingOutlined />
          </SvgIcon>
        </div>
      );
      break
    case "completed":
      return (
        <div className={clsx({ [classes.icon]:true,[classes.iconCompleted]:true})}>
          <SvgIcon viewBox="0 0 16 16.002" fontSize="inherit">
            <IconCheck />
          </SvgIcon>
        </div>
      );
      break
    case "failed":
      return (
        <div className={clsx({ [classes.icon]:true,[classes.iconFailed]:true})}>
          <SvgIcon viewBox="0 0 16 15.996" fontSize="inherit">
            <IconFailed />
          </SvgIcon>
        </div>
      );
      break

    default:
      return <> </>;
      break;
  }
}
