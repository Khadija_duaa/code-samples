import React, { useState } from "react";
import styles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import ExportLoader from "./ExportLoader";
import DeleteLoader from "./DeleteLoader";
import ImportLoader from "./ImportLoader";
import { useSelector } from "react-redux";
import { compose } from "redux";
import { withSnackbar } from "notistack";


function ActionLoader(props) {
  const { classes, theme, enqueueSnackbar } = props;
  const state = useSelector(state => {
    return {
      allProcesses: state.backProcesses
    }
  });
  const { allProcesses } = state;
  const [open, setOpen] = useState('export');
  const handleOpen = (state) => {
    // showSnackBar(` risks owner updated successfully`, "success");
    if (state != open) {
      setOpen(state);
    } else {
      setOpen('')
    }
  }
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };

  return (
    <div className={classes.LoaderWrapper}>
      {allProcesses.export.length ?
        <ExportLoader
          open={open}
          handleOpen={handleOpen}
          data={allProcesses.export}
          showSnackBar={showSnackBar}
        />
        : null}
      {/* <DeleteLoader open={open} handleOpen={handleOpen} />
            <ImportLoader open={open} handleOpen={handleOpen} /> */}
    </div>
  )
}

// ActionLoader.defaultProps = {
//   sidebar: true
// }
// export default withStyles(styles, { withTheme: true })(ActionLoader);
export default compose(
  withSnackbar,
  withStyles(styles, { withTheme: true })
)(ActionLoader);