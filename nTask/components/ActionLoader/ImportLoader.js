import React, { useState, useEffect } from "react";
import styles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import { SvgIcon, Typography } from "@material-ui/core";
import DownloadIcon from "../Icons/DownloadIcon";
import { getMenuIcons } from "../../Views/Sidebar/constants";

const componentVariable = 'import';
function ImportLoader(props) {
    const { classes, theme, handleOpen, open } = props;
    const [openState, setOpenState] = useState(open);
    useEffect(() => {
        setOpenState(open)
    }, [open])

    const handleOpenClick = () => {
        handleOpen(componentVariable);
    }
    return (
        <div className={classes.actionContentWrapper}
            style={{ transform: openState == componentVariable ? 'translateX(-200px)' : 'translateX(0)' }}>
            <div className={classes.actionButton} onClick={handleOpenClick}>
                {/* <div className={classes.spinner}> 
                    <div className={classes.spinnerArea}></div>
                    <div className={classes.spinnerArea}></div>
                    <div className={classes.spinnerArea}></div>
                </div> */}
                <SvgIcon
                    viewBox="0 0 16 14"
                    htmlColor={theme.palette.secondary.medDark}
                    className={classes.actionIcon}>
                    <DownloadIcon />
                </SvgIcon>
            </div>
            <div className={classes.pendingActionList}>
                <div className={classes.pendingActionHeader}>
                    <Typography variant="h1" className={classes.headerTitle}>
                        <SvgIcon
                            viewBox="0 0 16 14"
                            htmlColor={theme.palette.secondary.medDark}
                            className={classes.actionIcon}>
                            <DownloadIcon />
                        </SvgIcon>
                        Importing File
                    </Typography>
                </div>

                <div className={classes.pendingActionBody}>
                    <div className={classes.actionCard}>
                        <Typography variant="h1" className={classes.actionCardTitle}>
                            {getMenuIcons('projects', classes)}
                            projects.xlsx
                        </Typography>
                        <div className={classes.linearLoader}></div>
                    </div>
                    <div className={classes.actionCard}>
                        <Typography variant="h1" className={classes.actionCardTitle}>
                            {getMenuIcons('tasks', classes)}
                            tasks.xlsx
                        </Typography>
                        <div className={classes.linearLoader}></div>
                    </div>
                    <div className={classes.actionCard}>
                        <Typography variant="h1" className={classes.actionCardTitle}>
                            {getMenuIcons('issues', classes)}
                            issues.xlsx
                        </Typography>
                        <div className={classes.linearLoader}></div>
                    </div>
                    <div className={classes.actionCard}>
                        <Typography variant="h1" className={classes.actionCardTitle}>
                            {getMenuIcons('meetings', classes)}
                            meetings.xlsx
                        </Typography>
                        <div className={classes.linearLoader}></div>
                    </div>
                    <div className={classes.actionCard}>
                        <Typography variant="h1" className={classes.actionCardTitle}>
                            {getMenuIcons('risks', classes)}
                            risks.xlsx
                        </Typography>
                        <div className={classes.linearLoader}></div>
                    </div>
                </div>
            </div>
        </div>
    )
}

// ImportLoader.defaultProps = {
//   sidebar: true
// }

export default withStyles(styles, { withTheme: true })(ImportLoader);