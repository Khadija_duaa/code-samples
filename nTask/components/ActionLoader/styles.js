const styles = theme => ({
    LoaderWrapper: {
        position: 'fixed',
        bottom: 90,
        right: 30,
        marginBottom: 5,
    },
    linearLoader: {
        width: "100%",
        height: 2,
        background: "#e8e8e8",
        position: "relative",
        color: '#00cc90',
        overflow: "hidden",
        marginTop: 15,
        "&:before": {
            content: "''",
            position: "absolute",
            height: "100%",
            background: "currentColor",
            animation: "LinearLoading 2s linear infinite alternate forwards"
        },
    },
    "@keyframes LinearLoading": {
        "0%": {
            left: "-100%",
            right: "100%"
        },
        "50%": {
            left: "50%",
            right: "40%"
        },
        "100%": {
            left: "100%",
            right: "-100%"

        }
    },

    // inner content stats here
    actionContentWrapper: {
        position: 'relative',
        // '&:hover': {
        //     transform: 'translateX(-200px)',
        // }
    },
    actionButton: {
        cursor: 'pointer',
        position: 'relative',
        width: 46,
        height: 46,
        background: '#00cc90',
        borderRadius: '50%',
        padding: 15,
        marginBottom: 10,
        color: '#fff',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    },
    actionIcon: {
        fontSize: "24px !important",
        color: '#fff',
    },
    downloadIcon:{
        fontSize: "24px !important",
        color: '#fff',
    },
    downloadIconAnimation: {
        // fontSize: "24px !important",
        // color: '#fff',
        overflow: 'visible',
        "& .arrowIconAnimated": {
            animation: "arrowAnimation 0.5s cubic-bezier(0.68, 0.09, 0.94, 0.76) infinite alternate forwards"
        },
    },
    "@keyframes arrowAnimation": {
        "0%": {
            transform: "translateY(-3px)",
        },
        "100%": {
            transform: "translateY(3px)",

        }
    },
    // list starts here 
    pendingActionList: {
        position: 'absolute',
        bottom: '-20px',
        right: 0,
        width: 300,
        height: 'auto',
        border: '1px solid #dfdfdf',
        background: '#fff',
        zIndex: 20,
        overflow: 'hidden',
        borderRadius: 6,
        padding: '10px',
        boxShadow: '0px 2px 8px #00000029',
        opacity: 0,
        pointerEvents: 'none',
        transition: '0.4s ease all',
    },
    showPendingActionList: {
        opacity: 1,
        bottom: 'calc(100% + 10px)',
        pointerEvents: 'all',
    },
    pendingActionHeader: {
        padding: 10,
        // background: '#00cc90',
    },
    headerTitle: {
        fontSize: "14px !important",
        display: 'flex',
        alignItems: 'center',
        '& svg': {
            fontSize: 'inherit',
            marginRight: 5,
            color: 'inherit',
        }
    },
    actionCard: {
        padding: '10px 15px 10px 0px',
        // borderBottom: '1px solid #e1e1e1',
        display: 'flex',
        alignItems: 'center',
    },
    actionCardTitle: {
        fontSize: "13px !important",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 0,
        '& span': {
            fontSize: "11px !important",
            fontWeight: 300,
            // marginRight: 5,
            color: '#969597',
            fontStyle: 'italic',
        }
    },
    isCompleted: {
        fontSize: "12px !important",
        fontWeight: 300,
        marginTop: 5,
        color: '#969597',
    },
    isFailed: {
        fontSize: "12px !important",
        fontWeight: 300,
        marginTop: 5,
        color: '#969597',
    },
    actionCardContent: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        paddingLeft: 10,
    },
    actionCardIcon: {
    },
    reloadIcon:{
        color:'#0090FF',
        marginTop:6,
    },
    icon: {
        width: 36,
        height: 36,
        display: 'flex',
        fontSize: "16px !important",
        alignItems: 'center',
        borderRadius: '50%',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    iconProjects: {
        backgroundColor: '#00ABED2B',
        color: '#00ABED',
    },
    iconIssues: {
        backgroundColor: '#FE62592B',
        color: '#FE6259',
    },
    iconTasks: {
        backgroundColor: '#00CC902B',
        color: '#00CC90',
    },
    iconMeetings: {
        backgroundColor: '#8255EB2B',
        color: '#8255EB',
    },
    iconRisks: {
        backgroundColor: '#FE85002B',
        color: '#FE8500',
    },
    iconCompleted: {
        backgroundColor: '#00CC902B',
        color: '#00CC90',
    },
    iconFailed: {
        backgroundColor: '#E945452B',
        color: '#E94545',
    },
    clearBtn: {
        background: 'transparent',
        border: 'none',
        borderTop: `1px solid ${theme.palette.border.lightBorder}`,
        width: '100%',
        margin: '10px -10px -10px',
        display: 'block',
        boxSizing: 'content-box',
        padding: '15px 10px',
        cursor: 'pointer',
        color: theme.palette.text.brightBlue,
        fontSize: "13px !important",
    },
    reloadBtn: {
        cursor: 'pointer',
        background: 'transparent',
        border: 'none',
    },
});

export default styles;
