import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import styles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import { SvgIcon, Typography } from "@material-ui/core";
import IconDownload from "../Icons/IconDownload";
import IconReload from "../Icons/IconReload";
import { exportPostApi, removePendingProcess, updatePendingProcess, removeProcessById, addPendingHandler } from "../../redux/actions/backProcesses";
import fileDownload from "js-file-download";
import { getIcon } from "./Icons";
import clsx from "clsx";
import ClearIcon from "@material-ui/icons/Clear";
import { Scrollbars } from "react-custom-scrollbars";

const componentVariable = 'export';
function ExportLoader(props) {
    const { classes, theme, handleOpen, open, data, showSnackBar } = props;
    const [openState, setOpenState] = useState(open);
    const [completedProcesses, setCompletedProcesses] = useState([]);
    const [flag, setFlag] = useState(true);

    const dispatch = useDispatch();
    useEffect(() => {
        setOpenState(open);
    }, [open]);
    // main logic start here
    useEffect(() => {
        const filteredExports = data.filter(d => d.isNew);
        if (filteredExports.length && flag) {
            executeAPi(filteredExports[0])
        };
        // manage state for done processes
        const completedArray = data.reduce((r, cv) => {
            if (cv.isDone || cv.isFail) {
                r.push(cv.processId);
            }
            return r;
        }, []);
        setCompletedProcesses(completedArray);
    }, [data]);

    const handleOpenClick = () => {
        handleOpen(componentVariable);
    }
    const executeAPi = (process) => {
        const { type, apiType, data, fileName, isNew, processId, apiEndpoint } = process;
        if (apiType == 'post' && isNew) {
            setFlag(false);
            dispatch(updatePendingProcess(processId, 'export', { isNew: false }));
            exportPostApi(data, apiEndpoint, processId, dispatch,
                success => {
                    setTimeout(() => {
                        setFlag(true);
                        fileDownload(success.data, fileName);
                        dispatch(updatePendingProcess(processId, 'export', { isDone: true }));
                    }, 100);
                },
                async failure => {
                    setFlag(true);
                    dispatch(updatePendingProcess(processId, 'export', { isFail: true }));
                    const message = await failure.response.data.text();
                    showSnackBar(JSON.parse(message).message, 'error')
                }
            );
        }
    }

    const clearAllProcess = () => {
        dispatch(removePendingProcess('export'));
    };

    const reloadProcess = (item) => {
        dispatch(removeProcessById(item.processId, 'export'));
        const obj = {
            type: item.type,
            apiType: item.apiType,
            data: item.data,
            fileName: item.fileName,
            apiEndpoint: item.apiEndpoint,
        }
        addPendingHandler(obj, dispatch)
    };

    return (
        <div className={classes.actionContentWrapper}>
            <div className={classes.actionButton} onClick={handleOpenClick}>
                {openState
                    ?
                    <ClearIcon
                        className={classes.actionIcon}
                        htmlColor={theme.palette.secondary.medDark}
                    />
                    :
                    <SvgIcon
                        viewBox="0 0 22 19.781"
                        htmlColor={theme.palette.secondary.medDark}
                        className={clsx({
                            [classes.downloadIcon]: true,
                            [classes.downloadIconAnimation]: (data.length - completedProcesses.length)
                        })}
                    >
                        <IconDownload />
                    </SvgIcon>
                }
            </div>

            <div className={clsx({ [classes.pendingActionList]: true, [classes.showPendingActionList]: openState == componentVariable })}>
                <div className={classes.pendingActionHeader}>
                    <Typography variant="h1" className={classes.headerTitle}>
                        {data.length - completedProcesses.length} files downloading...
                    </Typography>
                </div>
                <div className={classes.pendingActionBody}>
                    <Scrollbars
                        autoHide={false}
                        autoHeight
                        autoHeightMin={0}
                        autoHeightMax={280}
                    >
                        <div >
                            {data && data.map(item => {
                                return (
                                    <div className={classes.actionCard}>
                                        <div className={classes.actionCardIcon}>
                                            {getIcon(classes, item.isDone ? 'completed' : item.isFail ? 'failed' : item.type, theme)}
                                        </div>
                                        <div className={classes.actionCardContent}>
                                            <Typography variant="h1" className={classes.actionCardTitle}>
                                                {item.fileName}
                                                {item.isNew ? <span>waiting...</span> :
                                                    !item.isNew && !item.isDone && !item.isFail ? <span>downloading...</span> :
                                                        null}
                                            </Typography>
                                            {!item.isNew && !item.isDone && !item.isFail ?
                                                <div className={classes.linearLoader}></div>
                                                : item.isDone ?
                                                    <div className={classes.isCompleted}>Download Completed</div> :
                                                    item.isFail ?
                                                        <div className={classes.isFailed}>Download failed</div>
                                                        : null}
                                        </div>
                                        {item.isFail ?
                                            <button
                                                onClick={() => reloadProcess(item)}
                                                className={classes.reloadBtn}
                                            >
                                                <SvgIcon
                                                    viewBox="0 0 20 20"
                                                    htmlColor={theme.palette.secondary.medDark}
                                                    className={classes.reloadIcon}>
                                                    <IconReload />
                                                </SvgIcon>
                                            </button>
                                            : null
                                        }
                                    </div>
                                );
                            })}
                        </div>
                    </Scrollbars>
                    {completedProcesses.length || true ?
                        <button
                            onClick={clearAllProcess}
                            className={classes.clearBtn}
                        >
                            Clear Completed
                        </button> : null}
                </div>
            </div>
        </div >
    )
}

// ImportLoader.defaultProps = {
//   sidebar: true
// }

export default withStyles(styles, { withTheme: true })(ExportLoader);