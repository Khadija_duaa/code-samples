import React, { Component } from "react";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../Buttons/CustomButton";
import {
  generateTimeHoursData,
  generateTimeMinsData
} from "../../helper/generateSelectData";
import timeSelectStyles from "./styles";
import InputLabel from "@material-ui/core/InputLabel";
import isEqual from "lodash/isEqual";
class TimeSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mins: "",
      hours: "",
      timeFormat: "AM"
    };
  }
  handleTimeFormatChange = () => {
    // Function to toggle AM or PM time format
    const { updateTime } = this.props;
    const { hours, mins, timeFormat } = this.state;
    const newTimeFormat = timeFormat == "AM" ? "PM" : "AM"; // Getting the toggled time format
    this.setState({ timeFormat: newTimeFormat }); // Updating state with the new selected time format
    updateTime(hours.value, mins.value, newTimeFormat); // calling update time parent function to update values in parent state
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  handleOptionsSelect = (type, option) => {
    const { updateTime } = this.props;
    const { hours, mins, timeFormat } = this.state;
    this.setState({ [type]: option });
    if (type == "hours") {
      updateTime(option.value, mins.value, timeFormat); // calling update time parent function to update values in parent state
    } else {
      updateTime(hours.value, option.value, timeFormat); // calling update time parent function to update values in parent state
    }
  };

  handleClearSelect = (key, value) => {
    this.setState({ [key]: value });
    this.props.clearSelect(key, value);
  };
  componentDidMount() {
    const { hours, mins, timeFormat } = this.props;
    const selectedHours = generateTimeHoursData().find(h => h.value == hours);
    const selectedMins = generateTimeMinsData().find(m => m.value == mins);
    this.setState({ hours: selectedHours, mins: selectedMins, timeFormat });
  }
  componentDidUpdate(prevProps, prevState) {
    const { hours, mins, timeFormat } = this.props;
    const prevTime = {
      hours: prevProps.hours,
      mins: prevProps.mins,
      timeFormat: prevProps.timeFormat
    };
    const currentTime = { hours: hours, mins: mins, timeFormat: timeFormat };
    if (!isEqual(prevTime, currentTime)) {
      // if prev time and new time is not equal than update the state
      const selectedHours = generateTimeHoursData().find(h => h.value == hours);
      const selectedMins = generateTimeMinsData().find(m => m.value == mins);
      this.setState({ hours: selectedHours, mins: selectedMins, timeFormat });
    }
  }
  render() {
    const { mins, hours, timeFormat } = this.state;
    const {
      classes,
      theme,
      label,
      style,
      labelAlign = "left",
      isDisabled
    } = this.props;
    return (
      <div
        className={
          labelAlign == "left"
            ? classes.timeInputsCntLabelLeft
            : classes.timeInputsCntLabelTop
        }
        style={style}
      >
        <InputLabel
          classes={{
            root: classes.selectLabel
          }}
          shrink={false}
        >
          {label}
        </InputLabel>
        <div className={classes.timeSelectCnt}>
          <SelectSearchDropdown
            data={() => {
              return generateTimeHoursData();
            }}
            label=""
            selectChange={this.handleOptionsSelect}
            styles={{ flex: 1, marginTop: 0, marginBottom: 0 }}
            type="hours"
            selectedValue={hours}
            placeholder={"HH"}
            isMulti={false}
            customStyles={{ control: { borderRadius: "4px 0 0 4px" } }}
            isDisabled={isDisabled}
          />
          <SelectSearchDropdown
            data={() => {
              return generateTimeMinsData();
            }}
            styles={{ flex: 1, marginTop: 0, marginBottom: 0 }}
            label=""
            selectChange={this.handleOptionsSelect}
            type="mins"
            selectedValue={mins}
            placeholder={"MM"}
            isMulti={false}
            customStyles={{
              control: { borderRadius: 0 }
            }}
            isDisabled={isDisabled}
          />
          <CustomButton
            onClick={this.handleTimeFormatChange}
            btnType="gray"
            variant="contained"
            style={{
              borderRadius: "0 4px 4px 0",
              padding: "7.5px 14px",
              minWidth: 42
            }}
            // query={btnQuery}
            disabled={isDisabled}
          >
            {timeFormat}
          </CustomButton>
        </div>
      </div>
    );
  }
}

export default withStyles(timeSelectStyles, {
  withTheme: true
})(TimeSelect);
