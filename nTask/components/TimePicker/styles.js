const timeSelectStyles = (theme) => ({
    timeInputsCntLabelTop: {
        width: 190,
        marginTop: 1
    },
    timeInputsCntLabelLeft: {
        display: "flex",
        alignItems: "center",
        width: 270
    },
    selectLabel: {
        color: theme.palette.text.primary,
        fontSize: "12px !important",
        marginRight: 15
    },
    timeSelectCnt:{
        display: "flex",
        flex: 1
    }
})

export default timeSelectStyles