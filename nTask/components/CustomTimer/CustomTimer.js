import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Chip from "@material-ui/core/Chip";
import PlayIcon from "@material-ui/icons/PlayArrow";
import CustomTimerStyles from "./CustomTimerStyles";
import moment from "moment";
const CustomTimer = props => {
  const { classes } = props;
  let createTimer = 0;
  let diff = 0;
  const [timer, setTimer] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [isPaused, setIsPaused] = useState(false);
  const increment = useRef(null);

  const handleStart = () => {
    setIsActive(true);
    setIsPaused(true);
    localStorage.setItem("leftState", isPaused);
    createTimer = new Date().getTime();
    localStorage.setItem("createTimer", createTimer);
   if (localStorage.getItem("createTimer") > 0 ){
    let sec = moment(new Date()).diff(+localStorage.getItem("createTimer"), "miliseconds")
      localStorage.setItem("createTimer", +localStorage.getItem("createTimer")+ sec);
      // setTimer()
   }
    increment.current = setInterval(() => {
      setTimer(timer => timer + 1);
    }, 1000);
 
    
  };

  const handlePause = () => {
    clearInterval(increment.current);
    setIsPaused(false);
    localStorage.setItem("leftState", isPaused);
    let pauseTimer = new Date().getTime();
    localStorage.setItem("pauseTimer", pauseTimer);

    localStorage.setItem("timer", timer);
  };

  const handleReset = () => {
    clearInterval(increment.current);
    setIsActive(false);
    setIsPaused(false);
    setTimer(0);
    localStorage.removeItem("createTimer");
    localStorage.removeItem("timer");
    localStorage.removeItem("pauseTimer");
  };
  const formatTime = () => {
    const getSeconds = `0${timer % 60}`.slice(-2);
    const minutes = `${Math.floor(timer / 60)}`;
    const getMinutes = `0${minutes % 60}`.slice(-2);
    const getHours = `0${Math.floor(timer / 3600)}`.slice(-2);
    return `${getHours} : ${getMinutes} : ${getSeconds}`;
  };

  useEffect(() => {
    createTimer = +localStorage.getItem("createTimer");
    if (createTimer > 0 && localStorage.getItem("leftState") == "false") {
      diff = moment(new Date()).diff(createTimer, "seconds");
      let timeLeft =(+localStorage.getItem("timer"))
      setTimer(diff + timeLeft);
    } else {
      setTimer(+localStorage.getItem("timer"));
    }
    if (localStorage.getItem("leftState") == "false") {
      handleStart();
    }
  }, []);
  return (
    <div className={classes.timer}>
      <div className={classes.chip}>
        <Chip
          avatar={
            isPaused ? (
              <p
                className={classes.timerBtnIcon}
                style={{ marginLeft: 10, fontWeight: "bold" }}
                onClick={handlePause}>
                ||
              </p>
            ) : (
              <PlayIcon className={classes.timerBtnIcon} onClick={handleStart} />
            )
          }
          label={<>{formatTime()}</>}
          variant="outlined"
        />

        <button
          style={{
            backgroundColor: "transparent",
            color: "#00cc90",
            padding: 4,
            borderRadius: 20,
            border: "1px solid lightgray",
          }}
          onClick={handleReset}
          disabled={!isActive}>
          Reset
        </button>
      </div>
    </div>
  );
};
CustomTimer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(CustomTimerStyles, { withTheme: true })(CustomTimer);
