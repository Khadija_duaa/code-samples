import React, { Component, Fragment, useState, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./styles";
import { compose } from "redux";
import menuStyles from "../../assets/jss/components/menu";
import CustomIconButton from "../Buttons/IconButton";
import SelectionMenu from "../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../utils/mergeStyles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { connect } from "react-redux";
import MeetNowIcon from "../Icons/MeetNowIcon";
import SvgIcon from "@material-ui/core/SvgIcon";

function VideoMoreAction(props) {

  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState("");
  const anchorEl = useRef();


  const handleClick = (event, newPlacement) => {
    const { currentTarget } = event;
    let newOPen = placement !== newPlacement || !open;
    setOpen(newOPen);
    setPlacement(newPlacement);
  }
  const handleClose = (event) => {
    setOpen(false);
  }
  const onZoomHandler = (Item) => {
    props.onZoom(Item, success => {
    },
      fail => {
      });
    setOpen(false);
  }
  const onMicrosoftTeamHandler = (Item) => {
    props.onTeam(Item, success => {
    },
      fail => {
      });
    setOpen(false);
  }
  const onGoogleMeetHandler = (Item) => {
    props.onGoogle(Item, success => {
    },
      fail => {
      });
    setOpen(false);
  }

  const { classes, theme, item, isZoomLinked, isTeamLinked, isGoogleLinked } = props;
  return (

    <div>
      <ClickAwayListener onClickAway={handleClose}>
        <div className={`${classes.dropDown} dropDown`}>
          <CustomIconButton
            btnType="filledWhite"
            onClick={event => {
              handleClick(event, "bottom-end");
            }}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: "10px",
            }}
            buttonRef={node => {
              anchorEl.current = node;
            }}
          >
            <SvgIcon
              viewBox="0 0 20.87 14.602"
              htmlColor={theme.palette.secondary.medDark}
              className={classes.meetNowIcon}
            >
              <MeetNowIcon />
            </SvgIcon>
          </CustomIconButton>
          <SelectionMenu
            open={open}
            closeAction={handleClose}
            placement={placement}
            anchorRef={anchorEl.current}
            list={
              <List>
                {isZoomLinked && !item.zoomMeetingId && <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  onClick={onZoomHandler.bind(this, item)}
                >
                  <ListItemText
                    primary="Zoom"
                    classes={{
                      primary: classes.statusItemText
                    }}
                  />
                </ListItem>}
                {isTeamLinked && !item.teamMeetingId && <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  onClick={onMicrosoftTeamHandler.bind(this, item)}
                >
                  <ListItemText
                    primary="Microsoft Team"
                    classes={{
                      primary: classes.statusItemText
                    }}
                  />
                </ListItem>}
                {isGoogleLinked && !item.googleMeetingId && <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  onClick={onGoogleMeetHandler.bind(this, item)}
                >
                  <ListItemText
                    primary="Google Meet"
                    classes={{
                      primary: classes.statusItemText
                    }}
                  />
                </ListItem>}
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    </div>
  );
}
const mapStateToProps = state => {
  return {
    isZoomLinked: state.profile.data.isZoomLinked,
    isTeamLinked: state.profile.data.isTeamLinked,
    isGoogleLinked: state.profile.data.isGoogleLinked,
  };
};
export default compose(
  connect(mapStateToProps, {
  }),
  withStyles(combineStyles(Styles, menuStyles), {
    withTheme: true
  })
)(VideoMoreAction);
