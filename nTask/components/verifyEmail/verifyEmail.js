import React, { Component, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { confirmUserEmail } from "../../redux/actions/profileSettings";
import queryString from "query-string";

function VerifyEmailFromEmail(props) {
  useEffect(() => {
    const path = queryString.parse(props.location.search);

    confirmUserEmail(path.token,
      (data) => {
        const promoCode = data.data.promoCode ? "&promoCode=" + data.data.promoCode : "";
        props.history.push(
          `/welcome/createprofile?email=${data.data.email}&plan=${data.data.plan ? data.data.plan : ''}${promoCode}`
        );

      },
      () => { }
    );
  }, [])

  return <></>;
}
export default withRouter(VerifyEmailFromEmail);
