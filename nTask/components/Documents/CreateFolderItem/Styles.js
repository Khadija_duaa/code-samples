const CreateFolderItemStyles = (theme) => ({
  docItemCnt:{
    display: 'flex',
    padding: '8px 10px',
    borderRadius: 6,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    background: theme.palette.common.white,
    // width: '90%', 
    alignItems: 'center',
    // marginBottom: 6,
    padding: '8px 8px 8px 8px',
  },

  fileDetailsCnt:{
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: 8
  },
  fileNameCnt:{
    fontSize: "13px !important",    
  },
  fileSizeCreaterCnt:{
    display: 'flex'
  },
  fileSize: {
    color: theme.palette.text.light,
  },
  fileCreater:{
    color: theme.palette.text.light,
  },
});

export default CreateFolderItemStyles;