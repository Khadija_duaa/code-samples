const chatDocumentsStyles = (theme) => ({
    noConversation: {
        display: 'flex',
        height: '100%',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    docsCnt: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
    headerMainCnt:{
        flexDirection: 'column',
        padding: "10px 15px 0px",
    },
    headerSearchCnt:{
        display: 'flex',
        alignItems: 'center',
    },
    outlinedInput:{
        padding: '9px 14px'
    },
    addDocCnt:{
        padding: '2px 4px',
        marginLeft: 8,
        borderRadius: 4,
        border: `1px solid ${theme.palette.border.grayLighter}`,
            background: theme.palette.common.white,
            color: theme.palette.text.grayDarker,
        '&:hover':{
            background: theme.palette.text.brightBlue,
            color: theme.palette.common.white
        }
    },
    docsCountCnt: {
        display: 'flex',
        fontSize: "12px !important",
        alignItems: 'center',
        padding: '10px 12px 0px 16px',
    },
    sepratorHr: {
        marginLeft: 5,
        marginRight: 5,
        flex: 1
    },
    tooltip: {
        fontSize: "12px !important",
        backgroundColor: theme.palette.common.black,
    },
    docFooter: {
        display: 'flex',
        padding: '9px 0px 8px 12px',
        backgroundColor: theme.palette.common.white,
    },
    countStyle: {
        color: theme.palette.text.medGray
    },
    searchIcon: {
        fontSize: "14px !important",
    },
    linkStyle : {
        color: theme.palette.background.blue,
        fontSize: "12px !important",
        margin: '10px 20px',
        textDecoration: 'underline',
        display: 'flex',
    },
    loaderCnt: {
        width: '100%',
        padding: '0px 20px 20px 20px',
    },
    clearSearchStyle : {
        color: theme.palette.text.grayDarker,
        fontSize: "12px !important",
        textDecoration: 'none',
        display: 'flex',
    },
    nodataCnt:{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 30
    },
    nofoundTitle:{
        fontSize: "16px !important",
        fontWeight: theme.typography.fontWeightMedium,
    },
    nofoundText:{
        color: theme.palette.text.grayDarker,
        fontSize: "13px !important"
    },
    clearFilter:{
        color: theme.palette.background.blue,
        fontSize: "13px !important",
        marginTop: 16,
        textDecoration: 'underline',
        display: 'flex',
    }
    
});

export default chatDocumentsStyles;