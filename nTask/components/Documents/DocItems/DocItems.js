import React, { useState, useRef, Fragment, useEffect } from 'react';
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
import { Scrollbars } from "react-custom-scrollbars";
import FileItem from '../FileItem/FileItem';
import FolderItem from '../FolderItem/FolderItem';
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";



function DocItems (props) {
    const scrollComponent = useRef();


      useEffect(() => {
        if (scrollComponent.current) {
            scrollComponent.current.scrollToBottom();
          }
          return () => {
              
          }
      }, [props.allChatList])

    const { classes, allDocList, intl , docPermission} = props;
    
    return(
        <Fragment>
            
            <Scrollbars
                style={{ flex:1 }}
                ref={node => {
                    scrollComponent.current  = node;
                }}
            >
                <div className={classes.itemsCnt}>
                    <ul>
                        {allDocList.map((docItem, i) => 
                            docItem.currentItem == 'fileItem' ? <li>
                                <FileItem
                                docItem= {docItem}
                                onRemoveFile={props.onRemoveFile}
                                showSnackBar={props.showSnackBar}
                                intl = {intl}
                                docPermission={docPermission}

                                >
                                    
                                </FileItem>
                            </li>:
                            <li>
                                <FolderItem
                                    docItem= {docItem}
                                    onOpenFolder={props.onOpenFolder}
                                    onDownloadFolder={props.onDownloadFolder}
                                    onRemoveFolder={props.onRemoveFolder}
                                    renameFoldeHandler={props.renameFoldeHandler}
                                    getCurrNodeFolders={props.getCurrNodeFolders}
                                    showSnackBar={props.showSnackBar}
                                    intl = {intl}
                                    docPermission={docPermission}

                                >

                                </FolderItem>
                            </li>
                        )}
                    </ul>
                </div>
            </Scrollbars>
            </Fragment>

    )
}
const mapStateToProps = (state) => {
    return {
    }
  }
export default compose(
    connect(mapStateToProps),
    withStyles(Styles, { withTheme: true })
  )(DocItems);
