const DocItemsStyles = (theme) => ({
    itemsCnt: {
        padding: "16px 20px 8px 14px",
        '& ul,li':{
            margin: 0,
            padding: 0,
            listStyleType: 'none',
        }
    },
    
});

export default DocItemsStyles;