import React, { useState, Fragment } from 'react';
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
import PreviewImageFileIcon from "../../Icons/PreviewImageFileIcon";
import Typography from "@material-ui/core/Typography";
import SvgIcon from "@material-ui/core/SvgIcon";
import FileItemMoreAction from "./FileItemMoreAction";
import { getIconByAttachmentType } from "../../../helper/getIconByAttachmentType";
import moment from "moment";



function FileItem (props) {

    const {theme, classes, docItem, intl, docPermission } = props;

    return(
        <div className={classes.docItemCnt}>
            {getIconByAttachmentType(
                theme,
                classes,
                docItem.type
            )}
            <div className={classes.fileDetailsCnt}>
                <div className={classes.headerCnt}>
                    <Typography variant="h6" title={docItem.name} classes={{h6: classes.fileNameCnt}}>{docItem.name}</Typography>
                    <FileItemMoreAction
                        onRemoveFile={props.onRemoveFile}
                        fileItem={docItem}
                        showSnackBar={props.showSnackBar}
                        intl={intl}
                        docPermission={docPermission}
                        />
                </div>
                <span className={classes.fileSizeCreaterCnt}>
                    <Typography variant="h6" classes={{h6: classes.fileSize}}>{docItem.size}</Typography>
                    <span className={classes.dot}>•</span>
            <Typography variant="h6" classes={{h6: classes.fileCreater}}>{intl.formatMessage({id:"project.dev.documents.file.uploaded-by",defaultMessage:'Uploaded by'})}{" "}{docItem.fullName}</Typography>
             <span className={classes.dot}>•</span>
              <Typography variant="h6" classes={{h6: classes.fileSize}}>{moment(docItem.createdDate).format('llll')}</Typography>
                </span>
            </div>
        </div>
    )
}
const mapStateToProps = (state) => {
    return {
        profileState: state.profile
      }
  }
export default compose(
    connect(mapStateToProps),
    withStyles(Styles, { withTheme: true })
  )(FileItem);