const FileItemStyles = (theme) => ({
  docItemCnt:{
    display: 'flex',
    padding: '8px 10px',
    borderRadius: 6,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    background: theme.palette.common.white,
    // width: '90%', 
    alignItems: 'center',
    marginBottom: 6,
    "&:hover .dropDown": {
      visibility: "visible",
    },
  },
  headerCnt:{
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'relative',
  },
  dropDown:{
    visibility: "hidden",
    position: 'absolute',
    right: '-5px',
    top: '-24px',
    // background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.background.contrast}`,
    // boxShadow: '0px 3px 6px #00000029',
    // borderRadius: 4,
  },
  actionIcon:{
    fontSize: "16px !important",
  },
  tooltip: {
    fontSize: "12px !important",
    backgroundColor: theme.palette.common.black,
  },
  iconImage:{
    color: theme.palette.text.azure,
  },
  iconVideo:{
    color: theme.palette.text.azure,
  },
  iconFile:{
    color: theme.palette.text.azure,
  },
  iconZip:{
    color: theme.palette.text.azure,
  },
  fileDetailsCnt:{
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: 14,
    flex: 1,
  },
  fileNameCnt:{
    fontSize: "13px !important",
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    width: 250,
    textOverflow: 'ellipsis'
  },
  fileSizeCreaterCnt:{
    display: 'flex'
  },
  fileSize: {
    color: theme.palette.text.light,
  },
  fileCreater:{
    color: theme.palette.text.light,
  },
  dot: {
    color: theme.palette.secondary.medDark,
    fontSize: "24px !important",
    margin: "0px 5px",
    lineHeight: '0.6',
  },
});

export default FileItemStyles;