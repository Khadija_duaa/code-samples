import React, { Component, Fragment, useState, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
import { compose } from "redux";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../Buttons/IconButton";
import SelectionMenu from "../../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { connect } from "react-redux";
import MoreOptionsIconH from "../../Icons/MoreOptionsIconH";
import SvgIcon from "@material-ui/core/SvgIcon";
import helper from "../../../helper/index";
import DownloadIcon from "../../Icons/DownloadIcon";
import DeleteActionIcon from "../../Icons/DeleteActionIcon";
import Tooltip from "@material-ui/core/Tooltip";
import DeleteConfirmDialog from "../../Dialog/ConfirmationDialogs/DeleteConfirmation";
import { FormattedMessage } from "react-intl";

function FileItemtMorAction(props) {
  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState("");
  const anchorEl = useRef();
  const [deleteDialogueStatus, setDeleteDialogueStatus] = useState(false);
  const [selectedDeleteFile, setSelectedDeleteFile] = useState(null);
  const [btnQuery, setBtnQuery] = useState("");

  const handleClick = (event, newPlacement) => {
    const { currentTarget } = event;
    let newOPen = placement !== newPlacement || !open;
    setOpen(newOPen);
    setPlacement(newPlacement);
  };
  const handleClose = event => {
    setOpen(false);
  };
  const onDownloadHandler = fileItem => {
    helper.DOWNLOAD_TEMPLATE(fileItem.fileUrlPath, fileItem.name, null, "attachment");
    setOpen(false);
  };
  // const onRemoveHandler = fileItem => {
  //   props.onRemoveFile(fileItem, success => {
  //     setOpen(false);
  //   },
  //   fail => {

  //   });
  // }
  const handleDeleteAttachment = () => {
    setBtnQuery("progress");
    props.onRemoveFile(
      selectedDeleteFile,
      success => {
        setBtnQuery("");
        setDeleteDialogueStatus(false);
        setSelectedDeleteFile(null);
        props.showSnackBar("File has been deleted.");
      },
      fail => {
        setDeleteDialogueStatus(false);
        setSelectedDeleteFile(null);
        setBtnQuery("");
        props.showSnackBar("File has not been deleted.");
      }
    );
  };
  const openDeleteDialogue = fileItem => {
    setDeleteDialogueStatus(true);
    setSelectedDeleteFile(fileItem);
  };
  const closeDeleteDialog = () => {
    setBtnQuery("");
    setDeleteDialogueStatus(false);
    setSelectedDeleteFile(null);
  };

  const { classes, theme, fileItem, intl, iconColor = theme.palette.background.black, docPermission } = props;
  return (
    <div className={`${classes.dropDown} dropDown`}>
      {/* <CustomIconButton
            btnType="condensed"
            onClick={event => {
              handleClick(event, "bottom-end");
            }}
            style={{ padding: "3px 5px"}}
            buttonRef={node => {
              anchorEl.current  = node;
            }}
          >
            <SvgIcon viewBox="0 0 14 3" style={{fontSize: "16px !important", color:iconColor }} >
                <MoreOptionsIconH />
            </SvgIcon>
          </CustomIconButton>

        <SelectionMenu
          open={open}
          closeAction={handleClose}
          placement={placement}
          anchorRef={anchorEl.current}
          list={
            <List>
              <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onDownloadHandler.bind(this,fileItem)}
              >
                <ListItemText
                  primary="Download"
                  classes={{
                    primary: classes.statusItemText
                  }}
                />
              </ListItem>
              {fileItem.isOwner && <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onRemoveHandler.bind(this, fileItem)}
              >
                <ListItemText
                  primary="Delete"
                  classes={{
                    primary: classes.statusItemTextDanger
                  }}
                />
              </ListItem>}
            </List>
          }
        /> */}
      {docPermission.downloadDocument && (
        <CustomIconButton
          btnType="filledWhiteRound"
          onClick={onDownloadHandler.bind(this, fileItem)}>
          <Tooltip
            classes={{
              tooltip: classes.tooltip,
            }}
            title={intl.formatMessage({
              id: "project.dev.documents.tooltips.download",
              defaultMessage: "Download",
            })}
            placement="bottom">
            <SvgIcon
              viewBox="0 0 16 14"
              htmlColor={theme.palette.secondary.medDark}
              className={classes.actionIcon}>
              <DownloadIcon />
            </SvgIcon>
          </Tooltip>
        </CustomIconButton>
      )}
      {(docPermission.deleteDocument || fileItem.isOwner) && (
        <CustomIconButton
          btnType="filledWhiteRound"
          onClick={openDeleteDialogue.bind(this, fileItem)}
          style={{
            marginLeft: 6,
          }}>
          <Tooltip
            classes={{
              tooltip: classes.tooltip,
            }}
            title={<FormattedMessage id="common.action.delete.label" defaultMessage="Delete" />}
            placement="bottom">
            <SvgIcon
              viewBox="0 0 16 16"
              htmlColor={theme.palette.secondary.medDark}
              style={{ fontSize: "16px" }}
              className={classes.actionIcon}>
              <DeleteActionIcon />
            </SvgIcon>
          </Tooltip>
        </CustomIconButton>
      )}
      <DeleteConfirmDialog
        open={deleteDialogueStatus}
        closeAction={closeDeleteDialog}
        cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
        successBtnText={
          <FormattedMessage id="common.action.delete.label" defaultMessage="Delete" />
        }
        alignment="center"
        headingText={<FormattedMessage id="common.action.delete.label" defaultMessage="Delete" />}
        successAction={handleDeleteAttachment}
        msgText={
          <FormattedMessage
            id="project.dev.documents.file.delete-confirmation.label"
            defaultMessage="Are you sure you want to delete this file?"
          />
        }
        btnQuery={btnQuery}
      />
    </div>
  );
}
const mapStateToProps = state => {
  return {};
};
export default compose(
  connect(mapStateToProps, {}),
  withStyles(combineStyles(Styles, menuStyles), {
    withTheme: true,
  })
)(FileItemtMorAction);
