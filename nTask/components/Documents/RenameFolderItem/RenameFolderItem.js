import React, { useState, Fragment, useRef, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
import FolderIcon from "../../Icons/FolderIcon";
import Typography from "@material-ui/core/Typography";
import SvgIcon from "@material-ui/core/SvgIcon";
import DefaultTextField from "../../Form/TextField";

function RenameFolderItem(props) {
  const inputEl = useRef();
  useEffect(() => {
    inputEl.current.select();
    return () => {};
  }, []);

  const [folderName, setFolderName] = useState(props.folderItem.name);
  const [errorState, setErrorState] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleFolderNameChange = e => {
    setFolderName(e.target.value);
    setErrorState(false);
    setErrorMessage("");
  };
  const renameFolderHandler = event => {
    if (event.key === "Enter") {
      renameFolderName();
    }
  };
  const renameFolderName = event => {
    let rx = /[<>:"\/\\|?*\x00-\x1F]|^(?:aux|con|clock\$|nul|prn|com[1-9]|lpt[1-9])$/i;
    if (rx.test(folderName)) {
      setErrorMessage("Invalid Character");
      setErrorState(true);
    } else {
      if (props.folderItem.name == folderName) {
        props.noRenameFolder();
      } else if (props.folderNames.indexOf(folderName) == -1) {
        let folderItem = props.folderItem;
        folderItem.name = folderName;
        props.renameFolder(folderItem);
      } else {
        setErrorMessage("Name already exist");
        setErrorState(true);
      }
    }
  };

  const { theme, classes, docItem } = props;

  return (
    <div className={classes.fileDetailsCnt}>
      <DefaultTextField
        label={false}
        fullWidth={true}
        errorMessage={errorMessage}
        errorState={errorState}
        error={errorState}
        autoFocus={true}
        formControlStyles={{
          marginBottom: 0,
          width: 220,
          borderRadius: "0",
        }}
        defaultProps={{
          id: "renamefolder",
          type: "text",
          style: { borderRadius: "0", border: "none" },
          onChange: handleFolderNameChange,
          onKeyUp: renameFolderHandler,
          onBlur: renameFolderName,
          value: folderName,
          inputProps: {
            maxLength: 45,
            style: {
              borderRadius: "0",
              fontSize: "12px !important",
              border: "none",
              padding: "7px 14px",
            },
          },
          inputRef: node => {
            inputEl.current = node;
          },
        }}
      />
    </div>
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile,
  };
};
export default compose(
  connect(mapStateToProps),
  withStyles(Styles, { withTheme: true })
)(RenameFolderItem);
