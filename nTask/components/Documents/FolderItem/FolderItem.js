import React, { useState, Fragment } from 'react';
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
// import FolderIcon from "../../Icons/FolderIcon";
import Typography from "@material-ui/core/Typography";
import SvgIcon from "@material-ui/core/SvgIcon";
import FolderItemMoreAction from "./FolderItemMoreAction";
import { getIconByAttachmentType } from "../../../helper/getIconByAttachmentType";
import RenameFolderItem from '../RenameFolderItem/RenameFolderItem';
import moment from "moment";

function FolderItem (props) {
  const [renameFolder, setRenameFolder] = useState(null);

  const onRenameFolder = (data,success,fail) => {
    let folderItem = props.getCurrNodeFolders().find(item => item.id == data.id )
    setRenameFolder({...folderItem});
    success();
  }
  const noRenameHandler = () => {
    setRenameFolder(null);
  }
  const renameFoldeHandler = (folderItem) => {
    props.renameFoldeHandler(folderItem,succes => {
        setRenameFolder(null);
    },fail => {

    })
  }

    const handleDoubleClick = (docItem) => {
        props.onOpenFolder(docItem,succes => {

        },fail => {

        });
    }

    const {theme, classes, docItem, intl, docPermission } = props;

    return(
        <Fragment>

            <div className={classes.docItemCnt}  onDoubleClick={() => handleDoubleClick(docItem)}>
                {getIconByAttachmentType(
                    theme,
                    classes,
                    docItem.type
                )}
                    {/* <SvgIcon viewBox="0 0 28 28" style={{fontSize: "28px !important", color: theme.palette.text.azure}} >
                        <FolderIcon/>
                    </SvgIcon> */}
                {renameFolder ? 
                    <RenameFolderItem
                        folderItem = {renameFolder}
                        noRenameFolder={noRenameHandler}
                        renameFolder={renameFoldeHandler}
                        folderNames={props.getCurrNodeFolders().map(item => item.name)}>
        
                    </RenameFolderItem>
                :<div className={classes.fileDetailsCnt}>
                    <div className={classes.headerCnt}>
        
                        <Typography variant="h6" title={docItem.name} classes={{h6: classes.fileNameCnt}}>{docItem.name}</Typography>
                        <FolderItemMoreAction
                                docItem= {docItem}
                                onOpenFolder={props.onOpenFolder}
                                onRenameFolder={onRenameFolder}
                                onDownloadFolder={props.onDownloadFolder}
                                onRemoveFolder={props.onRemoveFolder}
                                showSnackBar={props.showSnackBar}
                                docPermission={docPermission}
                        />
                    </div>
                    <span className={classes.fileSizeCreaterCnt}>
                        <Typography variant="h6" classes={{h6: classes.fileSize}}>{docItem.count}</Typography>
                        <span className={classes.dot}>•</span>
                        <Typography variant="h6" classes={{h6: classes.fileCreater}}>{`${intl.formatMessage({id:"common.created-by.action" ,defaultMessage:"Created by"})} ${docItem.fullName}`}</Typography>
                        <span className={classes.dot}>•</span>
                        <Typography variant="h6" classes={{h6: classes.fileSize}}>{moment(docItem.createdDate).format('llll')}</Typography>
                    </span>
                </div>}
            </div>
            
        </Fragment>
    )
}
const mapStateToProps = (state) => {
    return {
        profileState: state.profile
      }
  }
export default compose(
    connect(mapStateToProps),
    withStyles(Styles, { withTheme: true })
  )(FolderItem);
