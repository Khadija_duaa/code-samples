import React, { Component, Fragment, useState, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
import { compose } from "redux";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../Buttons/IconButton";
import SelectionMenu from "../../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { connect } from "react-redux";
import MoreOptionsIconH from "../../Icons/MoreOptionsIconH";
import SvgIcon from "@material-ui/core/SvgIcon";
import helper from "../../../helper/index";
import DeleteActionIcon from "../../Icons/DeleteActionIcon";
import DownloadIcon from "../../Icons/DownloadIcon";
import EditReplyActionIcon from "../../Icons/EditReplyActionIcon";
import OpenIcon from "../../Icons/OpenIcon";
import Tooltip from "@material-ui/core/Tooltip";
import DeleteConfirmDialog from "../../Dialog/ConfirmationDialogs/DeleteConfirmation";
import { FormattedMessage } from "react-intl";
function FolderItemtMorAction(props) {
  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState("");
  const anchorEl = useRef();
  const [deleteDialogueStatus, setDeleteDialogueStatus] = useState(false);
  const [selectedDeleteFolder, setSelectedDeleteFolder] = useState(null);
  const [btnQuery, setBtnQuery] = useState("");

  const handleClick = (event, newPlacement) => {
    const { currentTarget } = event;
    let newOPen = placement !== newPlacement || !open;
    setOpen(newOPen);
    setPlacement(newPlacement);
  };
  const handleClose = event => {
    setOpen(false);
  };
  const onOpenHandler = item => {
    props.onOpenFolder(
      item,
      success => {
        setOpen(false);
      },
      fail => {}
    );
  };
  const onRenameHandler = item => {
    props.onRenameFolder(
      item,
      success => {
        // setOpen(false);
        // props.showSnackBar('Folder rename successfully');
      },
      fail => {}
    );
  };
  const onDownloadHandler = item => {
    props.onDownloadFolder(
      item,
      success => {
        setOpen(false);
      },
      fail => {}
    );
  };
  // const onRemoveHandler = item => {
  //   props.onRemoveFolder(item, success => {
  //     setOpen(false);
  //   },
  //   fail => {

  //   });
  // }
  const handleDeleteAttachment = () => {
    setBtnQuery("progress");
    props.onRemoveFolder(
      selectedDeleteFolder,
      success => {
        setBtnQuery("");
        setDeleteDialogueStatus(false);
        setSelectedDeleteFolder(null);
        props.showSnackBar("Folder has been deleted.");
      },
      fail => {
        setDeleteDialogueStatus(false);
        setSelectedDeleteFile(null);
        setBtnQuery("");
        props.showSnackBar("Folder has not been deleted.");
      }
    );
  };
  const openDeleteDialogue = docItem => {
    setDeleteDialogueStatus(true);
    setSelectedDeleteFolder(docItem);
  };
  const closeDeleteDialog = () => {
    setBtnQuery("");
    setDeleteDialogueStatus(false);
    setSelectedDeleteFolder(null);
  };

  const {
    classes,
    theme,
    docItem,
    iconColor = theme.palette.background.black,
    docPermission,
  } = props;
  return (
    <div className={`${classes.dropDown} dropDown`}>
      {/* <CustomIconButton
            btnType="condensed"
            onClick={event => {
              handleClick(event, "bottom-end");
            }}
            style={{ padding: "3px 5px"}}
            buttonRef={node => {
              anchorEl.current  = node;
            }}
          >
            <SvgIcon viewBox="0 0 14 3" style={{fontSize: "16px !important", color:iconColor }} >
                <MoreOptionsIconH />
            </SvgIcon>
          </CustomIconButton>

        <SelectionMenu
          open={open}
          closeAction={handleClose}
          placement={placement}
          anchorRef={anchorEl.current}
          list={
            <List>
              <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onOpenHandler.bind(this,docItem)}
              >
                <ListItemText
                  primary="Open"
                  classes={{
                    primary: classes.statusItemText
                  }}
                />
              </ListItem>
              <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onRenameHandler.bind(this,docItem)}
              >
                <ListItemText
                  primary="Rename"
                  classes={{
                    primary: classes.statusItemText
                  }}
                />
              </ListItem>
              <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onDownloadHandler.bind(this,docItem)}
              >
                <ListItemText
                  primary="Download"
                  classes={{
                    primary: classes.statusItemText
                  }}
                />
              </ListItem>
              {docItem.isOwner && <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onRemoveHandler.bind(this, docItem)}
              >
                <ListItemText
                  primary="Delete"
                  classes={{
                    primary: classes.statusItemTextDanger
                  }}
                />
              </ListItem>}
            </List>
          }
        /> */}
      {docPermission.openFolder && (
        <CustomIconButton btnType="filledWhiteRound" onClick={onOpenHandler.bind(this, docItem)}>
          <Tooltip
            classes={{
              tooltip: classes.tooltip,
            }}
            title={
              <FormattedMessage id="project.dev.documents.tooltips.open" defaultMessage="Open" />
            }
            placement="bottom">
            <SvgIcon
              viewBox="0 0 18.01 12.606"
              htmlColor={theme.palette.secondary.medDark}
              className={classes.actionIcon}>
              <OpenIcon />
            </SvgIcon>
          </Tooltip>
        </CustomIconButton>
      )}
      {docPermission.renameFolder && (
        <CustomIconButton
          btnType="filledWhiteRound"
          onClick={onRenameHandler.bind(this, docItem)}
          style={{
            marginLeft: 6,
          }}
          disabled={!docItem.canDownload && !docItem.canUpload}>
          <Tooltip
            classes={{
              tooltip: classes.tooltip,
            }}
            title={
              <FormattedMessage
                id="project.dev.documents.tooltips.rename"
                defaultMessage="Rename"
              />
            }
            placement="bottom">
            <SvgIcon
              viewBox="0 0 16 16.07"
              htmlColor={theme.palette.secondary.medDark}
              style={{ fontSize: "16px" }}
              className={classes.actionIcon}>
              <EditReplyActionIcon />
            </SvgIcon>
          </Tooltip>
        </CustomIconButton>
      )}
      {/* {docPermission.downloadDocument && (
        <CustomIconButton
          btnType="filledWhiteRound"
          onClick={onDownloadHandler.bind(this, docItem)}
          style={{
            marginLeft: 6,
          }}
          disabled={(!docItem.canDownload && !docItem.canUpload) || !docItem.canDownload}>
          <Tooltip
            classes={{
              tooltip: classes.tooltip,
            }}
            title={
              <FormattedMessage
                id="project.dev.documents.tooltips.download"
                defaultMessage="Download"
              />
            }
            placement="bottom">
            <SvgIcon
              viewBox="0 0 16 14"
              htmlColor={theme.palette.secondary.medDark}
              className={classes.actionIcon}>
              <DownloadIcon />
            </SvgIcon>
          </Tooltip>
        </CustomIconButton>
      )} */}
      {(docPermission.deleteDocument || docItem.isOwner) && (
        <CustomIconButton
          btnType="filledWhiteRound"
          onClick={openDeleteDialogue.bind(this, docItem)}
          style={{
            marginLeft: 6,
          }}
          disabled={!docItem.canDownload && !docItem.canUpload}>
          <Tooltip
            classes={{
              tooltip: classes.tooltip,
            }}
            title={
              <FormattedMessage
                id="project.dev.documents.tooltips.delete"
                defaultMessage="Delete"
              />
            }
            placement="bottom">
            <SvgIcon
              viewBox="0 0 16 16"
              htmlColor={theme.palette.secondary.medDark}
              style={{ fontSize: "16px" }}
              className={classes.actionIcon}>
              <DeleteActionIcon />
            </SvgIcon>
          </Tooltip>
        </CustomIconButton>
      )}
      <DeleteConfirmDialog
        open={deleteDialogueStatus}
        closeAction={closeDeleteDialog}
        cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
        successBtnText={
          <FormattedMessage id="common.action.delete.label" defaultMessage="Delete" />
        }
        alignment="center"
        headingText={
          <FormattedMessage
            id="project.dev.documents.folder.delete-folder-confirmation.title"
            defaultMessage="Delete"
          />
        }
        successAction={handleDeleteAttachment}
        msgText={
          <FormattedMessage
            id="project.dev.documents.folder.delete-folder-confirmation.label"
            defaultMessage="Are you sure you want to delete this Folder?"
          />
        }
        btnQuery={btnQuery}
      />
    </div>
  );
}
const mapStateToProps = state => {
  return {};
};
export default compose(
  connect(mapStateToProps, {}),
  withStyles(combineStyles(Styles, menuStyles), {
    withTheme: true,
  })
)(FolderItemtMorAction);
