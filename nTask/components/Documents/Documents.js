import React, { useEffect, useState, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import DefaultTextField from "../Form/TextField";
import Styles from "./Styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import isEmpty from "lodash/isEmpty";
import Typography from "@material-ui/core/Typography";
import CreateFolderIcon from "../Icons/CreateFolderIcon";
import UploadFileIcon from "../Icons/UploadFileIcon";
import { withSnackbar } from "notistack";
import { nChatSignalConnectionOpen } from "../../utils/nchatSignalR";
import Divider from "@material-ui/core/Divider";
import Dropzone from "react-dropzone";
import DocItems from "./DocItems/DocItems";
import Tooltip from "@material-ui/core/Tooltip";
import CustomButton from "../Buttons/CustomButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchTextIcon from "../Icons/SearchTextIcon";
import { Scrollbars } from "react-custom-scrollbars";

import {
  GetAllFolders,
  uploadFileToAmazon,
  saveDocToContextObject,
  // saveUserComment,
  createFolder,
  OpenFolder,
  RenameFolder,
  DownloadFolder,
  DeleteFolder,
  RemoveFilesFromFolder,
} from "../../redux/actions/documents";
import CreateFolderItem from "./CreateFolderItem/CreateFolderItem";
import moment from "moment";
import helper from "../../helper/";
import BreadCrumbs from "../Breadcrumbs/BreadCrumbs";
import FileUploader from "../FileUploader/FileUploader";
import { FormattedMessage, injectIntl } from "react-intl";
import EmptyState from "../EmptyStates/EmptyState";
import Loader from "../Loader/Loader";
function Documents(props) {
  const [selectedNode, setSelectedNode] = useState({
    id: "root",
    name: "Documents",
  });
  const [allDocsData, setAllDocsData] = useState({
    attachments: [],
    folders: [],
  });
  const [searchText, setSearchText] = useState("");
  const [openCreateFolder, setOpenCreateFolder] = useState(false);
  const [breadCrumbArr, setBreadCrumbArr] = useState([{ key: "root", value: "Documents" }]);
  const [openFileLoader, setOpenFileLoader] = useState(false);
  const [userCanUploadFile, setUserCanUploadFile] = useState(true);
  const [userCanDownloadFile, setUserCanDownloadFile] = useState(true);
  const [folderCreated, setFolderCreated] = useState(false);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    nChatSignalConnectionOpen(props.docConfig.contextId);
    props.GetAllFolders(
      props.docConfig.contextUrl,
      props.docConfig.contextId,
      props.docConfig.contextView,
      response => {
        if (response && response.status === 200) {
          setLoading(false);
          // let comments = response.data.taskComments && response.data.taskComments.length>0?response.data.taskComments:[];
          // setActuallAllItems(comments);
          // setFormattedAllItems(formatedAllItems(comments));
        }
      },
      error => {
        if (error) {
          showSnackBar("Server throws error", "error");
        }
      }
    );
    return () => {
      setSelectedNode({});
    };
  }, [0]);

  useEffect(() => {
    setAllDocsData(props.documentsItems);
  }, [props.documentsItems]);

  const handleInput = () => {
    setSearchText(event.target.value);
  };
  const clearSearch = () => {
    setSearchText("");
  };

  const fileLoaderHandler = value => {
    if (userCanUploadFile) setOpenFileLoader(true);
  };
  const onFileUpload = result => {
    if (result.successful.length > 0) {
      let items = [];
      items = result.successful[0].response.body.map(item => {
        return {
          type: item.fileType,
          description: "",
          name: item.fileName,
          fileSize: item.fileSize,
          path: item.fileId,
          fileID: item.fileId,
          fileName: item.fileName,
          groupId: props.docConfig.contextId,
          groupType: props.docConfig.contextView,
          fileExtension: item.fileExtension,
          folderId: selectedNode.id == "root" ? "" : selectedNode.id,
        };
      });

      // let type = item.type;//.match(/[0-9a-zA-Z]{1,5}$/gm)[0];
      //         let attachment = {
      //           type: type,
      //           description: '',
      //           name: item.name,
      //           fileSize: item.size,
      //           path: item.response.body.fileID,
      //           fileID: item.response.body.fileID,
      //           fileName: item.response.body.fileName,
      //           groupId: props.docConfig.contextId,
      //           folderId: selectedNode.id == 'root' ? '' : selectedNode.id
      //         };
      props.saveDocToContextObject(
        props.docConfig.contextUrl,
        props.docConfig.contextId,
        props.docConfig.contextView,
        items,
        result => {},
        fail => {}
      );
    }
    setOpenFileLoader(false);
  };

  const getAllDocItems = () => {
    let files = [];
    let folders = [];
    if (selectedNode.id == "root") {
      files = allDocsData.attachments
        .filter(item => item.folderId == "")
        .map(item => formattedFile(item));
      folders = allDocsData.folders
        .filter(item => item.parentFolderId == "")
        .map(item => formattedFolder(item));
    } else {
      files = allDocsData.attachments
        .filter(item => item.folderId == selectedNode.id)
        .map(item => formattedFile(item));
      folders = allDocsData.folders
        .filter(item => item.parentFolderId == selectedNode.id)
        .map(item => formattedFolder(item));
    }
    let sortFiles = files.sort(sortItemsByDate);
    let sortFolders = folders.sort(sortItemsByDate);
    let allItems = [...sortFolders, ...sortFiles];
    if (searchText.trim() != "")
      return allItems.filter(item => item.name.toLowerCase().includes(searchText.toLowerCase()));
    return allItems;
  };
  const sortItemsByDate = (a, b) => {
    const dateA = new Date(a.createdDate).getTime();
    const dateB = new Date(b.createdDate).getTime();

    let comparison = 0;
    if (dateA > dateB) {
      comparison = -1;
    } else if (dateA < dateB) {
      comparison = 1;
    }
    return comparison;
  };
  const getCurrNodeFiles = () => {
    let files = [];
    if (selectedNode.id == "root") {
      files = allDocsData.attachments
        .filter(item => item.folderId == "")
        .map(item => formattedFile(item));
    } else {
      files = allDocsData.attachments
        .filter(item => item.folderId == selectedNode.id)
        .map(item => formattedFile(item));
    }
    return [...files];
  };
  const getCurrNodeFolders = () => {
    let folders = [];
    if (selectedNode.id == "root") {
      folders = allDocsData.folders.filter(item => item.parentFolderId == "");
    } else {
      folders = allDocsData.folders.filter(item => item.parentFolderId == selectedNode.id);
    }
    return [...folders];
  };

  const formattedFile = fileItem => {
    let updatedItem = {};
    let loginUser = props.profileState.data;
    updatedItem.currentItem = "fileItem";
    if (fileItem.createdBy === loginUser.userId) {
      updatedItem.user = "self";
      updatedItem.isOwner = true;
      updatedItem.fullName = isEmpty(loginUser.fullName.trim())
        ? loginUser.userName
        : loginUser.fullName;
    } else {
      let selectedMember = props.profileState.data.member.allMembers.find(member => {
        return member.userId == fileItem.createdBy;
      });
      updatedItem.user = "other";
      updatedItem.isOwner = false;
      updatedItem.fullName = isEmpty(selectedMember.fullName.trim())
        ? selectedMember.userName
        : selectedMember.fullName;
    }
    updatedItem.createdDate = fileItem.createdDate;
    updatedItem.description = fileItem.description;
    updatedItem.id = fileItem.id;
    updatedItem.name = fileItem.name || "";
    updatedItem.fileUrlPath = fileItem.path || "";
    updatedItem.parentId = fileItem.folderId;
    updatedItem.updatedBy = fileItem.updatedBy;
    updatedItem.fileExtension = fileItem.fileExtension;
    updatedItem.size = helper.convertBytes(fileItem.fileSize);
    updatedItem.type = fileItem.type;
    updatedItem.updatedDate = helper.RETURN_CHATDATEITEM_FORMAT(fileItem.updatedDate);
    updatedItem.version = fileItem.version;
    return updatedItem;
  };

  const formattedFolder = folderItem => {
    let updatedItem = {};
    let loginUser = props.profileState.data;
    updatedItem.currentItem = "folderItem";
    updatedItem.attachments = folderItem.attachments;
    updatedItem.canDownload = folderItem.canDownload;
    updatedItem.canUpload = folderItem.canUpload;
    if (folderItem.createdBy === loginUser.userId) {
      updatedItem.user = "self";
      updatedItem.isOwner = true;
      updatedItem.fullName = isEmpty(loginUser.fullName.trim())
        ? loginUser.userName
        : loginUser.fullName;
    } else {
      let selectedMember = props.profileState.data.member.allMembers.find(member => {
        return member.userId == folderItem.createdBy;
      });
      updatedItem.user = "other";
      updatedItem.isOwner = false;
      updatedItem.fullName = selectedMember
        ? isEmpty(selectedMember.fullName.trim())
          ? selectedMember.userName
          : selectedMember.fullName
        : "";
    }
    // updatedItem.createdBy= "5e8363407fded42110a965e9"
    updatedItem.createdDate = folderItem.createdDate;
    updatedItem.description = folderItem.description;
    updatedItem.id = folderItem.id;
    updatedItem.name = folderItem.name || "";
    updatedItem.parentId = folderItem.parentFolderId;
    updatedItem.type = "folder";
    updatedItem.updatedBy = folderItem.updatedBy;
    updatedItem.count =
      allDocsData.attachments.filter(item => item.folderId == folderItem.id).length +
      ` ${intl.formatMessage({
        id: "project.dev.documents.file.label2",
        defaultMessage: "files",
      })}`;
    updatedItem.updatedDate = helper.RETURN_CHATDATEITEM_FORMAT(folderItem.updatedDate);
    return updatedItem;
  };
  const onDrop = acceptedFiles => {
    /** function call when user select file from computer and add */
    if (!isEmpty(acceptedFiles)) {
      var data = new FormData();
      data.append("File", acceptedFiles[0]);
      data.append([props.docConfig.contextKeyId], props.docConfig.contextId);
      data.append("userid", props.profileState.userId);
      props.uploadFileToAmazon(
        /** uploading file to s3 bucket */
        props.docConfig.contextUrl,
        data,
        response => {
          if (response) {
            let type = acceptedFiles[0].type; //.match(/[0-9a-zA-Z]{1,5}$/gm)[0];
            let attachment = {
              type: type,
              description: "",
              name: acceptedFiles[0].name,
              fileSize: acceptedFiles[0].size,
              path: response.data,
              groupId: props.docConfig.contextId,
              groupType: props.docConfig.contextView,
              folderId: selectedNode.id == "root" ? "" : selectedNode.id,
            };
            props.saveDocToContextObject(
              props.docConfig.contextUrl,
              props.docConfig.contextId,
              props.docConfig.contextView,
              attachment,
              result => {
                // var fileData = {
                //   commentText: result.data.name,
                //   groupId: props.docConfig.contextId,
                //   commentType: 1,
                //   fileCommentId: result.data.path
                // };
                // props.saveUserComment(props.docConfig.contextUrl,fileData,
                //   (res) => {
               
                //   },
                //   (error) => {
              
                //   });
              },
              fail => {}
            ); /** on succ saving file information to db */
          }
        },
        err => {
          if (err.data) showSnackBar(err.data.message, "error");
        }
      );
    } else {
      showSnackBar("Oops! File not supported.", "error");
    }
  };

  const acceptedFormats = [
    " application/msword",
    "application/x-zip-compressed",
    "application/vnd.ms-powerpoint",
    "application/vnd.ms-excel",
    "application/vnd.ms-excel",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "text/csv",
    "text/plain",
    "application/pdf",
    "application/zip",
    "image/bmp",
    "image/gif",
    "image/jpeg",
    "image/png",
    "image/webp",
    "video/webm",
    "video/ogg",
    "video/mp4",
  ];

  const createFolderHandler = folderName => {
    if (!folderCreated) {
      setFolderCreated(true);
      let data = {
        name: folderName,
        groupId: props.docConfig.contextId,
        parentFolderId: selectedNode.id == "root" ? "" : selectedNode.id,
      };
      props.createFolder(
        props.docConfig.contextUrl,
        data,
        res => {
          setFolderCreated(false);
          setOpenCreateFolder(false);
        },
        error => {
          setFolderCreated(false);
        }
      );
    }
  };
  const onOpenFolder = (data, success, fail) => {
    //check if user can download
    //disable Upload file button user cant uplaod this

    setUserCanUploadFile(data.canUpload);
    setUserCanDownloadFile(data.canDownload);
    setSelectedNode({ id: data.id, name: data.name });
    setBreadCrumbArr([...breadCrumbArr, { key: data.id, value: data.name }]);
    success();

    // props.OpenFolder(
    //   props.docConfig.contextUrl,
    //   data,
    //   (res) => {
    //     success();
    //   },
    //   (error) => {
    //     fail(error);
    //   }
    // );
  };

  const renameFoldeHandler = (folderItem, success, fail) => {
    // let folderItem = renameFolder;
    // folderItem.name = newName;
    props.RenameFolder(
      props.docConfig.contextUrl,
      folderItem,
      res => {
        success();
      },
      error => {}
    );
  };
  const onDownloadFolder = (docItem, success, fail) => {
    // let files = allDocsData.attachments
    //   .filter((item) => item.folderId == docItem.id)
    //   .map((item) => {
    //     return {
    //       fileId: item.id,
    //       fileExt: item.fileExtension,
    //     };
    //   });
    // let data = {
    //   folderName: docItem.name,
    //   files: files,
    //   email: props.profileState.data.email,
    // };
    // setOpen(false);

    helper.DOWNLOAD_FOLDER(docItem.id, docItem.name + ".zip", showSnackBar);

    // props.DownloadFolder(
    //   props.docConfig.contextUrl,
    //   docItem.id,
    //   (res) => {
    //     success();
    //     showSnackBar("Folder send to email.", "info");
    //   },
    //   (error) => {
    //     fail(error);
    //   }
    // );
  };
  const onRemoveFolder = (data, success, fail) => {
    props.DeleteFolder(
      props.docConfig.contextUrl,
      data,
      res => {
        success();
      },
      error => {
        fail(error);
      }
    );
  };
  const onRemoveFile = (data, success, fail) => {
    props.RemoveFilesFromFolder(
      props.docConfig.contextUrl,
      props.docConfig.contextId,
      props.docConfig.contextView,
      data,
      res => {
        success();
      },
      error => {
        fail(error);
      }
    );
  };
  const handlebreadCrumClick = item => {
    if (item.key == "root") {
      setUserCanUploadFile(true);
      setUserCanDownloadFile(true);
    }
    setSelectedNode({ id: item.key, name: item.value });
    let index = breadCrumbArr.findIndex(elm => elm.key == item.key);
    setBreadCrumbArr(breadCrumbArr.slice(0, index + 1));
  };
  const showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  const { classes, theme, docConfig, chatPermission, intl, docPermission } = props;
  return (
    <div className={classes.docsCnt}>
      {openFileLoader ? (
        <div style={{ flex: 1 }}>
          <a href="#" onClick={e => setOpenFileLoader(false)} className={classes.linkStyle}>
            <FormattedMessage
              id="project.dev.documents.back-button.label"
              defaultMessage="Back To Document"
            />
          </a>
          {/* <CustomButton
              style={{ margin: 10 }}
              onClick={e => setOpenFileLoader(false)}
              btnType="success"
              variant="contained"
            >
              {`Back To Document`}
            </CustomButton> */}
          <Scrollbars style={{ flex: 1 }}>
            <div id="document_loader" className={classes.loaderCnt}>
              <FileUploader onComplete={onFileUpload} maxNumberOfFiles={10} customPlugin={[]} />
            </div>
          </Scrollbars>
        </div>
      ) : (
        <Fragment>
          <div className={classes.headerMainCnt}>
            <div className={classes.headerSearchCnt}>
              <DefaultTextField
                fullWidth={true}
                labelProps={{
                  shrink: true,
                }}
                error={false}
                defaultProps={{
                  id: "searchDoc",
                  onChange: handleInput,
                  value: searchText,
                  placeholder: props.intl.formatMessage({
                    id: "common.search.label",
                    defaultMessage: "Search",
                  }),
                  inputProps: {
                    maxLength: 15,
                    style: {
                      padding: "8px 8px 8px 0px",
                    },
                  },
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon
                        viewBox="0 0 14 14"
                        classes={{ root: classes.searchIcon }}
                        htmlColor={theme.palette.text.grayDarker}>
                        <SearchTextIcon />
                      </SvgIcon>
                    </InputAdornment>
                  ),
                  endAdornment: (
                    <InputAdornment>
                      {searchText.trim().length > 0 ? (
                        <a href="#" onClick={clearSearch} className={classes.clearSearchStyle}>
                          <FormattedMessage
                            id="project.dev.clear-button.label"
                            defaultMessage="Clear"
                          />
                        </a>
                      ) : null}
                    </InputAdornment>
                  ),
                }}
                formControlStyles={{
                  width: "100%",
                  marginBottom: 0,
                }}
                classes={{
                  input: classes.outlinedInput,
                }}
              />
              {/* <Dropzone onDrop={onDrop} accept={acceptedFormats}>
            {({ getRootProps, getInputProps }) => (
              <div {...getRootProps()}>
                <input {...getInputProps()} />
                <Tooltip classes={{
                        tooltip: classes.tooltip
                    }} title={'Add New File'}
                  placement="bottom">
                    <div className={classes.addDocCnt}>
                          <SvgIcon
                            style={{ fontSize: "22px !important" }}
                            viewBox="0 0 22 22"
                          >
                            <UploadFileIcon />
                          </SvgIcon>

                        </div>
                </Tooltip>
              </div>
            )}
          </Dropzone> */}
              {docPermission.uploadDocument && userCanUploadFile && (
                <Tooltip
                  classes={{
                    tooltip: classes.tooltip,
                  }}
                  title={
                    <FormattedMessage
                      id="project.dev.documents.tooltips.addnewfile"
                      defaultMessage="Add New File"
                    />
                  }
                  placement="bottom"
                  disabled={selectedNode.key != "root" && !userCanUploadFile}>
                  <div
                    className={classes.addDocCnt}
                    onClick={e => fileLoaderHandler(true)}
                    disabled={selectedNode.key != "root" && !userCanUploadFile}>
                    <SvgIcon style={{ fontSize: "22px" }} viewBox="0 0 22 22">
                      <UploadFileIcon />
                    </SvgIcon>
                  </div>
                </Tooltip>
              )}

              {docPermission.addFolder && userCanUploadFile && (
                <Tooltip
                  classes={{
                    tooltip: classes.tooltip,
                  }}
                  title={
                    <FormattedMessage
                      id="project.dev.documents.tooltips.createnewfolder"
                      defaultMessage="Create New Folder"
                    />
                  }
                  placement="bottom"
                  disabled={
                    selectedNode.key != "root" && !userCanUploadFile && !userCanDownloadFile
                  }>
                  <div
                    className={classes.addDocCnt}
                    onClick={e =>
                      setOpenCreateFolder(
                        prevState => userCanUploadFile && userCanDownloadFile && !prevState
                      )
                    }
                    disabled={
                      selectedNode.key != "root" && !userCanUploadFile && !userCanDownloadFile
                    }>
                    <SvgIcon style={{ fontSize: "22px" }} viewBox="0 0 22 22">
                      <CreateFolderIcon />
                    </SvgIcon>
                  </div>
                </Tooltip>
              )}
            </div>
            <div style={{ paddingTop: 6 }}>
              <BreadCrumbs
                breadCrumArr={breadCrumbArr}
                handlebreadCrumClick={handlebreadCrumClick}
                intl={intl}></BreadCrumbs>
            </div>
            {openCreateFolder && (
              <div style={{ padding: "6px 4px 0px 0px" }}>
                <CreateFolderItem
                  createFolder={createFolderHandler}
                  folderNames={getCurrNodeFolders().map(item => item.name)}></CreateFolderItem>
              </div>
            )}
          </div>
          {searchText.trim().length > 0 && getAllDocItems().length == 0 ? (
            <div className={classes.nodataCnt}>
              <Typography variant="h6" classes={{ h6: classes.nofoundTitle }}>
                <FormattedMessage
                  id="project.dev.documents.not-found.label1"
                  defaultMessage="Oops! No Result Found"
                />
              </Typography>
              <Typography variant="h6" classes={{ h6: classes.nofoundText }}>
                <FormattedMessage
                  id="project.dev.documents.not-found.label2"
                  defaultMessage="We couldn't find any result for"
                />{" "}
                <span style={{ color: theme.palette.text.azure }}>{searchText}</span>.
              </Typography>
              <Typography variant="h6" classes={{ h6: classes.nofoundText }}>
                <FormattedMessage
                  id="project.dev.documents.not-found.label3"
                  defaultMessage="Please try another search."
                />
              </Typography>
              <a href="#" onClick={clearSearch} className={classes.clearFilter}>
                {" "}
                <FormattedMessage
                  id="project.dev.documents.not-found.label4"
                  defaultMessage="Clear Filter"
                />{" "}
              </a>
            </div>
          ) : null}
          {loading ? 
          
          <Loader />
      : !loading && isEmpty(getAllDocItems()) && searchText.trim().length == 0 ? (
        <div style={{ display: "flex", height: "100%" }}>
          <EmptyState
          screenType="documents"
          heading={
            // <FormattedMessage
            //   id="common.search-list.label"
            //   defaultMessage="No Results Found"
            // />
            "No Documents Yet"
          }
          message={
            // <FormattedMessage
            //   id="common.search-list.message"
            //   defaultMessage="No matching results found against your filter criteria."
            // />
            "Documents let you keep your docs files organized in one place."
          }
          button={false}
        />
        </div>)
        :(<DocItems
            allDocList={getAllDocItems()}
            onOpenFolder={onOpenFolder}
            renameFoldeHandler={renameFoldeHandler}
            onDownloadFolder={onDownloadFolder}
            onRemoveFolder={onRemoveFolder}
            onRemoveFile={onRemoveFile}
            getCurrNodeFolders={getCurrNodeFolders}
            showSnackBar={showSnackBar}
            docPermission={docPermission}
            intl={intl}>
          </DocItems>)}
        </Fragment>
      )}
      <div className={classes.docFooter}>
        <Typography variant="h6" classes={{ h6: classes.countStyle }}>
          {<FormattedMessage id="project.dev.documents.content.label" defaultMessage="Content:" />}
        </Typography>
        <Typography variant="h6" classes={{ h6: classes.countStyle }}>
          &nbsp;
          {`${getCurrNodeFolders().length} ${
            getCurrNodeFolders().length == 1 || getCurrNodeFolders().length == 0
              ? intl.formatMessage({
                  id: "project.dev.documents.folder.label",
                  defaultMessage: "folder",
                })
              : ""
          }${
            getCurrNodeFolders().length > 1
              ? `${intl.formatMessage({
                  id: "project.dev.documents.folder.label2",
                  defaultMessage: "folders",
                })},`
              : ","
          }`}
        </Typography>
        <Typography variant="h6" classes={{ h6: classes.countStyle }}>
          &nbsp;
          {`${getCurrNodeFiles().length} ${
            getCurrNodeFiles().length == 1 || getCurrNodeFiles().length == 0
              ? intl.formatMessage({
                  id: "project.dev.documents.file.label",
                  defaultMessage: "file",
                })
              : ""
          }${
            getCurrNodeFiles().length > 1
              ? `${intl.formatMessage({
                  id: "project.dev.documents.file.label2",
                  defaultMessage: "files",
                })},`
              : ""
          }`}
        </Typography>
      </div>
    </div>
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile,
    documentsItems: state.documents.data,
  };
};
export default compose(
  withSnackbar,
  injectIntl,
  connect(mapStateToProps, {
    GetAllFolders,
    uploadFileToAmazon,
    saveDocToContextObject,
    // saveUserComment,
    createFolder,
    OpenFolder,
    RenameFolder,
    DownloadFolder,
    DeleteFolder,
    RemoveFilesFromFolder,
  }),
  withStyles(Styles, { withTheme: true })
)(Documents);
