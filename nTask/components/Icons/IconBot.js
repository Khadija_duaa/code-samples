

import React, { Fragment } from "react";

const IconBot = () => {
  return (
    <svg id="iconBot" xmlns="http://www.w3.org/2000/svg" width="40.475" height="38.997" viewBox="0 0 40.475 38.997">
      <defs>
        <linearGradient id="linear-gradient" x1="767.856" y1="-8.458" x2="768.899" y2="1.711" gradientUnits="objectBoundingBox">
          <stop offset="0.41" stop-color="#002649" />
          <stop offset="1" stop-color="#0096ff" />
        </linearGradient>
      </defs>
      <path id="Path_41552" data-name="Path 41552" d="M116.02,98.181a19.01,19.01,0,0,0-3.736-6.116,17.7,17.7,0,0,0-5.583-4.146,16.045,16.045,0,0,0-13.629,0,18.168,18.168,0,0,0-5.583,4.146,19.01,19.01,0,0,0-3.736,6.116,20.943,20.943,0,0,0-1.355,7.471A17.528,17.528,0,0,0,99.887,123.3a.369.369,0,1,0,0-.739A16.889,16.889,0,0,1,83.1,105.652c0-10.221,7.512-18.513,16.789-18.513s16.789,8.292,16.789,18.513a.369.369,0,1,0,.739,0A20.98,20.98,0,0,0,116.02,98.181Z" transform="translate(-79.65 -86.4)" fill="#0f9dff" />
      <g id="Group_65" data-name="Group 65" transform="translate(0 12.315)">
        <g id="Group_64" data-name="Group 64">
          <ellipse id="Ellipse_138" data-name="Ellipse 138" cx="5.747" cy="6.199" rx="5.747" ry="6.199" transform="translate(28.981)" fill="#0f9dff" />
          <ellipse id="Ellipse_139" data-name="Ellipse 139" cx="5.747" cy="6.199" rx="5.747" ry="6.199" transform="translate(0 0.082)" fill="#0f9dff" />
        </g>
      </g>
      <ellipse id="Ellipse_140" data-name="Ellipse 140" cx="15.106" cy="13.998" rx="15.106" ry="13.998" transform="translate(5.131 4.515)" fill="#dff7ff" />
      <path id="Path_41553" data-name="Path 41553" d="M99.6,119.6h15.435a4.774,4.774,0,0,1,4.6,4.926h0a4.8,4.8,0,0,1-4.6,4.926H99.6a4.774,4.774,0,0,1-4.6-4.926h0A4.722,4.722,0,0,1,99.6,119.6Z" transform="translate(-87.077 -105.971)" fill="#0f9dff" />
      <path id="Path_41554" data-name="Path 41554" d="M121.144,150.593a2.89,2.89,0,0,0,1.56-.452,2.994,2.994,0,0,0,1.067-1.19.395.395,0,1,0-.7-.369,2.159,2.159,0,0,1-3.859,0,.395.395,0,1,0-.7.369,2.994,2.994,0,0,0,1.067,1.19A2.89,2.89,0,0,0,121.144,150.593Z" transform="translate(-100.907 -122.925)" fill="url(#linear-gradient)" />
      <g id="Group_67" data-name="Group 67" transform="translate(10.673 16.338)">
        <g id="Group_66" data-name="Group 66">
          <ellipse id="Ellipse_141" data-name="Ellipse 141" cx="2.052" cy="2.176" rx="2.052" ry="2.176" transform="translate(15.024)" fill="#e6f7fd" />
          <ellipse id="Ellipse_142" data-name="Ellipse 142" cx="2.052" cy="2.176" rx="2.052" ry="2.176" fill="#e6f7fd" />
        </g>
      </g>
      <ellipse id="Ellipse_143" data-name="Ellipse 143" cx="3.366" cy="2.381" rx="3.366" ry="2.381" transform="translate(16.871 34.235)" fill="#0f9dff" />
    </svg>
  );
};

export default IconBot;
