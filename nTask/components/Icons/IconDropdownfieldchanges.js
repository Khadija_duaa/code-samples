
import React from "react";

const IconDropdownfieldchanges = () => {
  return (
  <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
  <g id="iconDropdown" transform="translate(-164 -198)">
    <path id="Ellipse_45" data-name="Ellipse 45" d="M9,1.5A7.5,7.5,0,1,0,16.5,9,7.508,7.508,0,0,0,9,1.5M9,0A9,9,0,1,1,0,9,9,9,0,0,1,9,0Z" transform="translate(164 198)" fill="#85898b"/>
    <path id="Path_176" data-name="Path 176" d="M9.48,12a1.115,1.115,0,0,1-.455-.1A.88.88,0,0,1,8.5,11.1V6.894A.88.88,0,0,1,9.025,6.1a1.05,1.05,0,0,1,1.1.13l2.55,2.1a.85.85,0,0,1,0,1.33l-2.55,2.1A1.03,1.03,0,0,1,9.48,12Z" transform="translate(181.999 196.5) rotate(90)" fill="#85898b"/>
  </g>
</svg>
  );
};

export default IconDropdownfieldchanges;