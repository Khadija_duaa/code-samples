import React, { Component, Fragment } from 'react';

const AdvFilterIcon = () => {
    return (
        <Fragment>
            <g transform="translate(82 -51.5)">
                <rect style={{opacity:0}} width="24" height="24" transform="translate(-82 51.5)"/>
                <path d="M5.444,85.833H8.556V84.278H5.444ZM0,76.5v1.556H14V76.5Zm2.333,5.444h9.333V80.389H2.333Z" transform="translate(-77 -18)"/>
            </g>
        </Fragment>
    )
}

export default AdvFilterIcon