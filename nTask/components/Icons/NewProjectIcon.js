import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
function NewProjectIcon({className}) {
    return (
        <SvgIcon id="ProjectManagement" viewBox="0 0 18.914 20.079" className={className}>
  <path id="Shape_1" data-name="Shape 1" d="M308.026,393.5h4.492v2.828h-4.492Zm-11.584,0h7.329v2.828h-7.329Zm11.584,5.668h7.33v2.828h-7.33Zm-11.584,0h7.329v2.828h-7.329Z" transform="translate(-296.442 -385.934)" fill="#922bd6"/>
  <path id="Path_8005" data-name="Path 8005" d="M307.872,389.154v-3.3h-3.789v3.3l1.187,1.177v14.033a.969.969,0,0,0-.237.621.947.947,0,0,0,1.895,0,.967.967,0,0,0-.236-.62V390.331Z" transform="translate(-296.523 -385.853)" fill="#cf98f9"/>
</SvgIcon>

    )
}

export default NewProjectIcon
