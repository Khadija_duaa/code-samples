import React, { Component, Fragment } from "react";
// viewBox="0 0 70.67 22"
const GoogleCalendarMeetIcon = () => {
    return (
        <Fragment>
            <g transform="translate(-11 -11)">
                <g transform="translate(11 11)">
                    <rect width="11.58" height="11.58" transform="translate(5.212 5.212)" fill="#fff" />
                    <path d="M381.543,386.755l5.212-5.212h-5.212Z" transform="translate(-364.755 -364.755)" fill="#ea4335" />
                    <rect width="5.212" height="11.58" transform="translate(16.788 5.212)" fill="#fbbc04" />
                    <rect width="11.58" height="5.212" transform="translate(5.212 16.788)" fill="#34a853" />
                    <path d="M0,381.543v3.472a1.738,1.738,0,0,0,1.736,1.74H5.208v-5.212Z" transform="translate(0 -364.755)" fill="#188038" />
                    <path d="M386.849,5.212V1.736A1.737,1.737,0,0,0,385.113,0h-3.472V5.212Z" transform="translate(-364.849 0)" fill="#1967d2" />
                    <path d="M16.788,0H1.736A1.737,1.737,0,0,0,0,1.736V16.788H5.212V5.212h11.58V0Z" transform="translate(0)" fill="#4285f4" />
                    <path d="M152.851,178.879a2.247,2.247,0,0,1-.9-1.285l1.005-.412a1.522,1.522,0,0,0,.477.808,1.246,1.246,0,0,0,.821.284,1.261,1.261,0,0,0,.846-.3.963.963,0,0,0,.357-.765.952.952,0,0,0-.374-.773,1.466,1.466,0,0,0-.937-.3h-.58v-.993h.52a1.253,1.253,0,0,0,.812-.262.861.861,0,0,0,.331-.713.784.784,0,0,0-.3-.645,1.156,1.156,0,0,0-.748-.241,1.006,1.006,0,0,0-.7.236,1.376,1.376,0,0,0-.378.58l-.993-.412a2.221,2.221,0,0,1,.726-.988,2.113,2.113,0,0,1,1.358-.43,2.443,2.443,0,0,1,1.1.236,1.932,1.932,0,0,1,.765.653,1.666,1.666,0,0,1,.275.941,1.541,1.541,0,0,1-.258.9,1.807,1.807,0,0,1-.632.567v.06a1.914,1.914,0,0,1,.808.632,1.646,1.646,0,0,1,.314,1.014,1.875,1.875,0,0,1-.3,1.053,2.057,2.057,0,0,1-.825.726,2.616,2.616,0,0,1-1.186.266A2.465,2.465,0,0,1,152.851,178.879Zm6.17-4.984-1.1.8-.55-.838,1.977-1.427h.761v6.733h-1.083Z" transform="translate(-145.267 -164.686)" fill="#4285f4" />
                </g>
                <g transform="translate(1258 143)">
                    <path d="M69,33.673V26.3l.764-1.59,3.821-3.027a.773.773,0,0,1,1.274.6V36.752a.785.785,0,0,1-1.291.6Z" transform="translate(-1251.189 -150.496)" fill="#00ac47" />
                    <path d="M14.168,15,8,21.167h6.168Z" transform="translate(-1209 -146)" fill="#ea4335" />
                    <path d="M14.168,35H8v8.018h6.168Z" transform="translate(-1209 -159.833)" fill="#2684fc" />
                    <path d="M8,61v4.112a2.062,2.062,0,0,0,2.056,2.056h4.112V61Z" transform="translate(-1209 -177.815)" fill="#0066da" />
                    <path d="M41.414,17.032A2.028,2.028,0,0,0,39.4,15H28v6.167h7.555v4.009l5.859-.185Z" transform="translate(-1222.832 -146)" fill="#ffba00" />
                    <path d="M35.555,52.009H28v6.167H39.4a2.027,2.027,0,0,0,2.012-2.029V48H35.555Z" transform="translate(-1222.832 -168.824)" fill="#00ac47" />
                    <path d="M58.359,32v9.56L52.5,36.934Z" transform="translate(-1239.777 -157.758)" fill="#00832d" />
                </g>
                <g transform="translate(35 12)">
                    <path d="M12,14.75a.75.75,0,0,1-.75-.75V6a.75.75,0,0,1,1.5,0v8A.75.75,0,0,1,12,14.75Z" transform="translate(-2 0)" fill="#5d5d5d" />
                    <path d="M14,12.75H6a.75.75,0,0,1,0-1.5h8a.75.75,0,0,1,0,1.5Z" transform="translate(0 -2)" fill="#5d5d5d" />
                </g>
            </g>
        </Fragment>
    );
};

export default GoogleCalendarMeetIcon;
