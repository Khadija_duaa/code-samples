import React from 'react';
const IconSection = ({color="#20cc90"}) => {
    return (
        <>
            <path style={{fill:color}} d="M16.687,0H1.313A1.314,1.314,0,0,0,0,1.313V3.188A1.314,1.314,0,0,0,1.313,4.5H16.687A1.314,1.314,0,0,0,18,3.188V1.313A1.314,1.314,0,0,0,16.687,0Z" transform="translate(0 0)" /><path style={{fill:color}} d="M16.687,0H1.313A1.314,1.314,0,0,0,0,1.313V3.188A1.314,1.314,0,0,0,1.313,4.5H16.687A1.314,1.314,0,0,0,18,3.188V1.313A1.314,1.314,0,0,0,16.687,0Z" transform="translate(0 13)" /><rect style={{fill:color}} width="18" height="2" rx="1" transform="translate(0 6)" /><rect style={{fill:color}} width="11" height="2" rx="1" transform="translate(0 9)" />
        </>
    )
}
export default IconSection;