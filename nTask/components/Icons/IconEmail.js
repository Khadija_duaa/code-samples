import React from "react";
const IconEmail = ({ color = "#00cc90", style }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="16.326"
      viewBox="0 0 18 16.326"
      style={style}>
      <path
        style={{ fill: color }}
        d="M17.3,4H4.7A2.7,2.7,0,0,0,2,6.7v9a2.7,2.7,0,0,0,2.7,2.7H17.3A2.7,2.7,0,0,0,20,15.7v-9A2.7,2.7,0,0,0,17.3,4Zm-.6,1.8L11,10.075,5.3,5.8Zm.6,10.8H4.7a.9.9,0,0,1-.9-.9V6.925l6.66,4.995a.9.9,0,0,0,1.08,0L18.2,6.925V15.7A.9.9,0,0,1,17.3,16.6Z"
        transform="translate(-2 -4)"
      />
    </svg>
  );
};
export default IconEmail;
