import React, { Component, Fragment } from "react";

const ArrowIncreasedRed = () => {
  return (
    <Fragment>
      <g transform="translate(-164.002 0)">
        <path
          style={{ fill: "#e52c2c" }}
          d="M72.95,5.6,68.7.1a.25.25,0,0,0-.4,0l-4.25,5.5a.25.25,0,0,0,.2.4H66.5v5.75a.25.25,0,0,0,.25.25h3.5a.25.25,0,0,0,.25-.25V6h2.25a.25.25,0,0,0,.2-.4Z"
          transform="translate(100)"
        />
      </g>
    </Fragment>
  );
};

export default ArrowIncreasedRed;
