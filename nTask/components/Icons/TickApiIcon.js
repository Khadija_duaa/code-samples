import React from 'react'

function TickApiIcon() {
    return (
        <>
        <svg width="90" height="90" viewBox="0 0 90 90">
  <g id="iconTick" transform="translate(-140 -81)">
    <circle id="Ellipse_132" data-name="Ellipse 132" cx="35" cy="35" r="35" transform="translate(150 91)" fill="#20cc96" opacity="0.2"/>
    <circle id="Ellipse_134" data-name="Ellipse 134" cx="45" cy="45" r="45" transform="translate(140 81)" fill="#20cc96" opacity="0.1"/>
    <circle id="Ellipse_133" data-name="Ellipse 133" cx="20" cy="20" r="20" transform="translate(165 106)" fill="#fff"/>
    <path id="Icon_awesome-check-circle" data-name="Icon awesome-check-circle" d="M40.563,20.563a20,20,0,1,1-20-20A20,20,0,0,1,40.563,20.563ZM18.249,31.152,33.088,16.314a1.29,1.29,0,0,0,0-1.825l-1.825-1.825a1.29,1.29,0,0,0-1.825,0l-12.1,12.1-5.65-5.65a1.29,1.29,0,0,0-1.825,0L8.037,20.94a1.29,1.29,0,0,0,0,1.825l8.387,8.387A1.29,1.29,0,0,0,18.249,31.152Z" transform="translate(164.925 105.5)" fill="#00cc96"/>
  </g>
</svg>

        </>
    )
}

export default TickApiIcon
