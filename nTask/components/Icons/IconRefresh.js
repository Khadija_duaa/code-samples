// viewBox="0 0 12 12.015"

import React from "react";
const IconRefresh = () => {
  return (
    <path
      id="iconRefresh"
      d="M6.008,0a6.008,6.008,0,1,0,4.265,10.273L9.191,9.191a4.509,4.509,0,1,1-3.2-7.69A4.369,4.369,0,0,1,9.131,2.869L7.494,4.506H12V0L10.213,1.787A5.974,5.974,0,0,0,5.992,0Z"
      style={{ fill: "#969696" }}
    />
  );
};
export default IconRefresh;
