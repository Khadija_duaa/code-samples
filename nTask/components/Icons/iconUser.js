import SvgIcon from "@material-ui/core/SvgIcon";
import React from "react";

export const IconUser = props => {
  return (
    <SvgIcon
      width="14"
      height="16.114"
      viewBox="0 0 14 16.114"
      {...props}
      fill={props.fill ?? "#6f7374"}>
      <g transform="translate(-2.66 -1.25)">
        <path
          d="M10.559,9.869a4.309,4.309,0,1,1,4.309-4.309A4.315,4.315,0,0,1,10.559,9.869Zm0-7.495a3.185,3.185,0,1,0,3.185,3.185A3.192,3.192,0,0,0,10.559,2.374Z"
          transform="translate(-0.899)"
          fill="currentColor"
        />
        <path
          d="M16.1,20.62a.566.566,0,0,1-.562-.562c0-2.586-2.638-4.684-5.876-4.684s-5.876,2.1-5.876,4.684a.562.562,0,0,1-1.124,0c0-3.2,3.14-5.808,7-5.808s7,2.608,7,5.808A.566.566,0,0,1,16.1,20.62Z"
          transform="translate(0 -3.257)"
          fill="currentColor"
        />
      </g>
    </SvgIcon>
  );
};
