import React, { Component, Fragment } from "react";

const ResourceProjectIcon = ({ color = "#ffa148" }) => {
  return (
    <Fragment>
      <path
        id="Path_41556"
        data-name="Path 41556"
        d="M15.312,6.194a.574.574,0,0,1-.538.881H2.7a.7.7,0,0,1-.7-.7V5.094A3.1,3.1,0,0,1,5.094,2H6.718a2.006,2.006,0,0,1,1.953.98l.98,1.3c.217.287.245.322.651.322h1.953A3.729,3.729,0,0,1,15.312,6.194Z"
        transform="translate(-2 -2)"
        fill="#bfbfbf"
      />
      <path
        id="Path_41557"
        data-name="Path 41557"
        d="M15.288,10.75a.7.7,0,0,1,.7.7L16,14.88a3.749,3.749,0,0,1-3.745,3.745H5.745A3.749,3.749,0,0,1,2,14.88V11.45a.7.7,0,0,1,.7-.7Z"
        transform="translate(-2 -4.625)"
        fill="#969696"
      />
    </Fragment>
  );
};

export default ResourceProjectIcon;
