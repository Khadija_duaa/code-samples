import React, { Component, Fragment } from "react";

const DockIcon = (color) => {
  return (
    <Fragment>
      <rect style={{ fill: "#969696", opacity: 0 }} width="24" height="24" />
      <path
        style={{ fill: "#969696", fillRule: "evenodd" }}
        d="M13.2,5.2h3.2V3.6H13.2Zm0,2.4h3.2V6H13.2Zm0,2.4h3.2V8.4H13.2Zm-1.6,6.4H4.4a.8.8,0,0,1-.8-.8V4.4a.8.8,0,0,1,.8-.8h7.2ZM15.6,2H4.4A2.4,2.4,0,0,0,2,4.4V15.6A2.4,2.4,0,0,0,4.4,18H15.6A2.4,2.4,0,0,0,18,15.6V4.4A2.4,2.4,0,0,0,15.6,2Z"
        transform="translate(2 2)"
      />
    </Fragment>
  );
};

export default DockIcon;
