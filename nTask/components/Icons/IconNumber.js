import React from 'react';
const IconNumber= ({color="#00cc90"})=>{
    return(
        <path style={{fill:color}} d="M7.375-.43,8.06-4.6H4.411L3.726-.43H1.9L2.586-4.6H.328l.3-1.825H2.883l.593-3.588H1.233l.3-1.825H3.772L4.456-16H6.281L5.6-11.834H9.246L9.93-16h1.825l-.684,4.166h2.258l-.3,1.825H10.774l-.593,3.588h2.243l-.3,1.825H9.884L9.2-.43ZM4.707-6.421H8.356l.593-3.588H5.3Z" transform="translate(-0.328 16)"/>
    )
}
export default IconNumber;