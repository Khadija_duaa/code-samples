import SvgIcon from "@material-ui/core/SvgIcon";
import React from "react";

export const IconSubtask = (props) => {
  return (
    <SvgIcon
      width="13"
      height="17.079"
      viewBox="0 0 13 17.079"
      {...props}
      fill={props.fill ?? "#6f7374"}
    >
      <path
        d="M-22665.412-2264.933a1.953,1.953,0,0,1-1.947-1.95v-.32h-3.707a3.259,3.259,0,0,1-3.252-3.254v-11.555h1.514v5.288h5.445v-.343a1.953,1.953,0,0,1,1.947-1.95h2.145a1.95,1.95,0,0,1,1.949,1.95v2.144a1.95,1.95,0,0,1-1.949,1.95h-2.145a1.953,1.953,0,0,1-1.947-1.95v-.3h-5.445v4.763a1.747,1.747,0,0,0,1.738,1.75h3.707v-.32a1.953,1.953,0,0,1,1.947-1.95h2.145a1.95,1.95,0,0,1,1.949,1.95v2.144a1.95,1.95,0,0,1-1.949,1.95Zm-.623-4.094v2.144a.619.619,0,0,0,.623.617h2.145a.618.618,0,0,0,.621-.617v-2.144a.623.623,0,0,0-.621-.623h-2.145A.624.624,0,0,0-22666.035-2269.027Zm0-8.039v2.144a.62.62,0,0,0,.623.618h2.145a.618.618,0,0,0,.621-.618v-2.144a.617.617,0,0,0-.621-.617h-2.145A.619.619,0,0,0-22666.035-2277.066Z"
        transform="translate(22674.318 2282.011)"
        fill="currentColor"
      />
    </SvgIcon>
  );
};
