import React, { Component, Fragment } from 'react';

const CrossIcon = () => {
    return (
        <Fragment>
            <g id="icon_cancel_changes" transform="translate(0 0)">
                <path id="Path_1" data-name="Path 1" d="M18.782,3.218a11.005,11.005,0,1,0,0,15.564A11.018,11.018,0,0,0,18.782,3.218ZM15.34,15.34a.846.846,0,0,1-1.2,0L11,12.2,7.708,15.489a.846.846,0,0,1-1.2-1.2L9.8,11,6.66,7.857a.846.846,0,0,1,1.2-1.2L11,9.8,13.993,6.81a.846.846,0,0,1,1.2,1.2L12.2,11l3.143,3.143A.846.846,0,0,1,15.34,15.34Z" transform="translate(0)" fill="#ff5b5b"/>
            </g>
        </Fragment>
    )
}

export default CrossIcon