import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
function GoogleIcon({googleIconStyle}) {
    return (
        <>
            <SvgIcon style={googleIconStyle} viewBox="0 0 20 20.408" >  <path id="Path_12" data-name="Path 12" d="M140.346,108.847a8.744,8.744,0,0,0-.215-2.086h-9.58v3.787h5.624a4.987,4.987,0,0,1-2.086,3.311l-.019.127,3.029,2.347.21.021a9.977,9.977,0,0,0,3.039-7.506" transform="translate(-120.346 -98.416)" fill="#4285f4" />
                <path id="Path_13" data-name="Path 13" d="M23.041,164.556a9.726,9.726,0,0,0,6.757-2.472l-3.22-2.494a6.039,6.039,0,0,1-3.537,1.02,6.143,6.143,0,0,1-5.805-4.24l-.12.01-3.15,2.438-.041.115a10.2,10.2,0,0,0,9.116,5.624" transform="translate(-12.837 -144.148)" fill="#34a853" />
                <path id="Path_14" data-name="Path 14" d="M4.4,77.96a6.282,6.282,0,0,1-.34-2.018,6.6,6.6,0,0,1,.329-2.018l-.006-.135L1.193,71.312l-.1.05a10.183,10.183,0,0,0,0,9.161L4.4,77.96" transform="translate(0 -65.738)" fill="#fbbc05" />
                <path id="Path_15" data-name="Path 15" d="M23.041,3.946a5.655,5.655,0,0,1,3.946,1.519l2.88-2.812A9.8,9.8,0,0,0,23.041,0a10.2,10.2,0,0,0-9.116,5.624l3.3,2.562a6.168,6.168,0,0,1,5.816-4.24" transform="translate(-12.837)" fill="#eb4335" />
            </SvgIcon>

        </>
    )
}

export default GoogleIcon
