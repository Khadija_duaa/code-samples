import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
function NewGanttIocn({className}) {
    return (
        <SvgIcon id="iconGantt"  viewBox="0 0 21.657 16.993" className={className}>
  <path id="Path_7997" data-name="Path 7997" d="M297.27,387.45h13.352a1.636,1.636,0,0,1,0,3.272H297.27a1.636,1.636,0,1,1,0-3.272Z" transform="translate(-295.634 -387.45)" fill="#f4871f"/>
  <path id="Path_7998" data-name="Path 7998" d="M299.889,400.915h9.765a1.635,1.635,0,1,1,0,3.27h-9.775a1.635,1.635,0,1,1,0-3.27h.01Z" transform="translate(-295.584 -387.191)" fill="#f4871f"/>
  <path id="Path_7999" data-name="Path 7999" d="M301.918,394.237h13.648a1.636,1.636,0,0,1,0,3.272H301.92a1.636,1.636,0,0,1,0-3.272Z" transform="translate(-295.545 -387.32)" fill="#f9bd8c"/>
</SvgIcon>

    )
}

export default NewGanttIocn
