{
  /* <svg xmlns="http://www.w3.org/2000/svg" width="14" height="13.393" viewBox="0 0 14 13.393"><defs><style>.a{fill:#7e7e7e;}</style></defs></svg> */
}

import React, { Fragment } from "react";

const IconDivide = () => {
  return (
    <Fragment>
      <path
        // style={{ color: "#7e7e7e" }}
        d="M.84,58.725H13.16a.84.84,0,0,0,0-1.68H.84a.84.84,0,1,0,0,1.68Z"
        transform="translate(0 -50.792)"
      />
      <path
        // style={{ color: "#7e7e7e" }}
        d="M44.71,6.793a2.077,2.077,0,1,0-2.077-2.077A2.079,2.079,0,0,0,44.71,6.793Z"
        transform="translate(-37.733 -2.64)"
      />
      <path
        // style={{ color: "#7e7e7e" }}
        d="M46.785,85.108a2.077,2.077,0,1,0-2.077,2.077A2.079,2.079,0,0,0,46.785,85.108Z"
        transform="translate(-37.732 -73.791)"
      />
    </Fragment>
  );
};

export default IconDivide;
