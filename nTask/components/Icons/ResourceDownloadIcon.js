import React, { Component, Fragment } from "react";

const ResourceDownloadIcon = ({ color = "#ffa148" }) => {
  return (
    <Fragment>
      <g id="Icon-cloud" transform="translate(0.015 -2.981)">
        <path
          id="Path_8244"
          data-name="Path 8244"
          d="M13.831,28a.664.664,0,0,1-.471-.2L10.7,25.137a.666.666,0,0,1,.942-.942l2.194,2.194L16.025,24.2a.666.666,0,1,1,.942.942L14.3,27.8A.664.664,0,0,1,13.831,28Z"
          transform="translate(-5.844 -11.683)"
          fill="#595c5d"
        />
        <path
          id="Path_8245"
          data-name="Path 8245"
          d="M17.166,23.829a.666.666,0,0,1-.666-.666v-6a.666.666,0,0,1,1.332,0v6A.666.666,0,0,1,17.166,23.829Z"
          transform="translate(-9.18 -7.514)"
          fill="#595c5d"
        />
        <path
          id="Path_8246"
          data-name="Path 8246"
          d="M13.9,14.376a.666.666,0,0,1-.384-1.211A2.665,2.665,0,0,0,11.985,8.32h-.84a.666.666,0,0,1-.645-.5,4.664,4.664,0,1,0-8.01,4.25.666.666,0,1,1-1,.883A6,6,0,1,1,11.638,6.987h.346a4,4,0,0,1,2.3,7.267A.663.663,0,0,1,13.9,14.376Z"
          transform="translate(0 0)"
          fill="#595c5d"
        />
      </g>
    </Fragment>
  );
};

export default ResourceDownloadIcon;
