import React, { Fragment } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconMessageText = ({ color = "#332900", style }) => {
  return (
    <Fragment>
      <SvgIcon
        xmlns="http://www.w3.org/2000/svg"
        width="18"
        height="18.05"
        viewBox="0 0 18 18.05"
        style={style}>
        <g id="message-text" transform="translate(-1.25 -1.25)">
          <path
            id="Path_8220"
            data-name="Path 8220"
            d="M10.25,19.3a1.9,1.9,0,0,1-1.507-.8L7.487,16.822a.4.4,0,0,0-.167-.084H6.9c-3.491,0-5.651-.946-5.651-5.651V6.9C1.25,3.2,3.2,1.25,6.9,1.25h6.7C17.3,1.25,19.25,3.2,19.25,6.9v4.186c0,3.7-1.951,5.651-5.651,5.651H13.18a.219.219,0,0,0-.167.084L11.757,18.5A1.9,1.9,0,0,1,10.25,19.3ZM6.9,2.506c-3,0-4.4,1.4-4.4,4.4v4.186c0,3.784,1.3,4.4,4.4,4.4H7.32a1.57,1.57,0,0,1,1.172.586l1.256,1.674a.581.581,0,0,0,1,0l1.256-1.674a1.467,1.467,0,0,1,1.172-.586H13.6c3,0,4.4-1.4,4.4-4.4V6.9c0-3-1.4-4.4-4.4-4.4Z"
            transform="translate(0 0)"
            style={{ fill: color }}
          />
          <path
            id="Path_8221"
            data-name="Path 8221"
            d="M15.25,8.506H6.878a.628.628,0,0,1,0-1.256H15.25a.628.628,0,0,1,0,1.256Z"
            transform="translate(-0.814 -0.977)"
            style={{ fill: color }}
          />
          <path
            id="Path_8222"
            data-name="Path 8222"
            d="M11.9,13.506H6.878a.628.628,0,1,1,0-1.256H11.9a.628.628,0,0,1,0,1.256Z"
            transform="translate(-0.814 -1.791)"
            style={{ fill: color }}
          />
        </g>
      </SvgIcon>
    </Fragment>
  );
};

export default IconMessageText;
