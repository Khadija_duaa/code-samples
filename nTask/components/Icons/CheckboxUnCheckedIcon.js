import React, { Component, Fragment } from "react";

const CheckBoxUncheckIcon = () => {
  return (
    <Fragment>
       <rect id="Rounded_Rectangle_1595" style={{
        fill: "#fff",
        stroke: "rgb(191, 191, 191)",
        strokeLinejoin: "round",
        strokeWidth: 1,
      }} data-name="Rounded Rectangle 1595" x="2.5" y="2.5" width="16" height="16" rx="2" ry="2"/>

    </Fragment>
  );
};

export default CheckBoxUncheckIcon;
