import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconProfileSettings = ({ color = "#a5b4be", style }) => {
  return (
    <SvgIcon style={{ ...style }} viewBox="0 0 12 15.429">
      <g id="iconProfileSettings" transform="translate(-64 -501)">
        <path
          id="Path_466"
          data-name="Path 466"
          d="M11.429,9.857A3.429,3.429,0,1,0,8,6.429,3.429,3.429,0,0,0,11.429,9.857Z"
          transform="translate(58.571 498)"
        />
        <path
          id="Path_467"
          data-name="Path 467"
          d="M16.143,19.857A.857.857,0,0,0,17,19,6,6,0,0,0,5,19a.857.857,0,0,0,.857.857Z"
          transform="translate(59 496.571)"
        />
      </g>
    </SvgIcon>
  );
};

export default IconProfileSettings;
