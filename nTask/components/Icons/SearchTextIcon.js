import React, { Component, Fragment } from 'react';

const SearchTextIcon = () => {
    return (
        <Fragment>
            <path d="M10.274,9.039a5.708,5.708,0,1,0-1.237,1.237L12.763,14,14,12.765,10.274,9.039Zm-4.586.588A3.937,3.937,0,1,1,9.625,5.689,3.942,3.942,0,0,1,5.687,9.627Z" transform="translate(0 -0.002)"/>
        </Fragment>
    )
}

export default SearchTextIcon