import React, { Component, Fragment } from 'react';

const DeliveredIcon = () => {
    return (
        <Fragment>
            <path style={{fill: '#bfbfbf'}} d="M.655,7.822a.78.78,0,0,0,.085,1.1l3.851,3.3a.775.775,0,0,0,.507.188c.023,0,.046,0,.07,0a.777.777,0,0,0,.535-.282l6.587-8.017a.781.781,0,0,0-.108-1.1l-1.206-.991a.782.782,0,0,0-1.1.108L4.807,8.3,2.77,6.552a.782.782,0,0,0-1.1.085Z" transform="translate(-0.466 -1.842)"/>
         </Fragment>
    )
}

export default DeliveredIcon