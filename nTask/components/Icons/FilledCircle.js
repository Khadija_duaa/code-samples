import React, { Component, Fragment } from "react";

const FilledCircle = (color) => {
  return (
    <Fragment>
      <g>
        <g id="check-circle-blank">
          <path
            // style={{ fill: color }}
            d="M255,0C114.75,0,0,114.75,0,255s114.75,255,255,255s255-114.75,255-255S395.25,0,255,0z"
          />
        </g>
      </g>
    </Fragment>
  );
};

export default FilledCircle;
