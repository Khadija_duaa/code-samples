{/* <svg xmlns="http://www.w3.org/2000/svg" width="15" height="13.5" viewBox="0 0 15 13.5"></svg>; */}

import React from "react";
const IconFilter = () => {
  return (
    <path
      id="Iconfilter"
      d="M2.182,3H15.818a.682.682,0,0,1,.618.391.67.67,0,0,1-.1.72l-5.293,6.2v5.517a.674.674,0,0,1-.323.574.688.688,0,0,1-.663.03l-2.727-1.35a.674.674,0,0,1-.377-.6V10.308l-5.293-6.2a.67.67,0,0,1-.1-.72A.682.682,0,0,1,2.182,3ZM14.349,4.35H3.651L8.157,9.625a.671.671,0,0,1,.161.436v4l1.364.675V10.06a.671.671,0,0,1,.161-.436Z"
      transform="translate(-1.5 -3)"
      style={{ fill: "#434f5b" }}
    />
  );
};
export default IconFilter;
