import React, { Component, Fragment } from "react";

const CheckkAllItemsSelectedIcon = () => {
  return (
    <Fragment>
        <g transform="translate(-91.78 -375.78)">
          <circle style={{fill: '#dcffe0', stroke: '#6cd576'}} cx="11.72" cy="11.72" r="11.72" transform="translate(92.28 376.28)"/>
          <path style={{fill: '#38cc46', stroke: '#38cc46', strokeLinejoin: 'round'}} d="M108.864,385.2a.525.525,0,0,0-.707,0l-5.994,5.612-2.3-2.157a.525.525,0,0,0-.708,0,.447.447,0,0,0,0,.662l2.658,2.488a.525.525,0,0,0,.708,0l6.348-5.943A.448.448,0,0,0,108.864,385.2Z"/>
        </g>      
    </Fragment>
  );
};

export default CheckkAllItemsSelectedIcon;
