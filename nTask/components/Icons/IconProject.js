
import React from "react";

const IconProject = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
    <g id="iconProject" transform="translate(-8 -8)">
      <path id="iconProject-2" data-name="iconProject" d="M12.971,17.25H5.529c-3.282,0-4.279-1-4.279-4.279V5.529c0-3.282,1-4.279,4.279-4.279H6.645A2.3,2.3,0,0,1,8.878,2.366L9.994,3.855c.246.327.283.372.744.372h2.233c3.282,0,4.279,1,4.279,4.279v4.465C17.25,16.253,16.253,17.25,12.971,17.25ZM5.529,2.366c-2.657,0-3.163.506-3.163,3.163v7.442c0,2.657.506,3.163,3.163,3.163h7.442c2.657,0,3.163-.506,3.163-3.163V8.506c0-2.657-.506-3.163-3.163-3.163H10.738A1.709,1.709,0,0,1,9.1,4.524L7.985,3.036a1.246,1.246,0,0,0-1.34-.67Z" transform="translate(6.75 6.75)" fill="#00abed"/>
      <path id="Path_41581" data-name="Path 41581" d="M14.576,7.506h-6.7a.628.628,0,1,1,0-1.256h6.7a.628.628,0,0,1,0,1.256Z" transform="translate(4.517 8.937)" fill="#00abed"/>
      <path id="Path_41582" data-name="Path 41582" d="M13.9,11.506H8.878a.628.628,0,1,1,0-1.256H13.9a.628.628,0,1,1,0,1.256Z" transform="translate(4.355 8.285)" fill="#00abed"/>
    </g>
  </svg>
  );
};

export default IconProject;
