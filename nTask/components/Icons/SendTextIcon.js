import React, { Component, Fragment } from 'react';

const SendTextIcon = () => {
    return (
        <Fragment>
            <g transform="translate(-1154.99 -681.99)">
                <circle cx="14" cy="14" r="14" transform="translate(1154.99 681.99)"/>
                <path style={{fill: '#fff'}} d="M13.167,39.9l-11.3-4.687A1.352,1.352,0,0,0,.043,36.8L1.049,40.74H5.971a.41.41,0,1,1,0,.82H1.049L.043,45.5a1.352,1.352,0,0,0,1.828,1.583l11.3-4.687a1.352,1.352,0,0,0,0-2.5Z" transform="translate(1162.99 654.88)"/>
            </g>
        </Fragment> 
    )
}

export default SendTextIcon