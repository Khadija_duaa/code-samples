import React, { Component, Fragment } from "react";

const MagnifyGlassResources = ({ color = "#ffa148" }) => {
    return (
        <Fragment>
            <path xmlns="http://www.w3.org/2000/svg" id="iconZoom" d="M8,3.5A4.5,4.5,0,1,0,12.5,8,4.5,4.5,0,0,0,8,3.5ZM2,8a6,6,0,1,1,10.74,3.68l4.04,4.04a.75.75,0,1,1-1.061,1.061l-4.04-4.04A6,6,0,0,1,2,8ZM8,5.75a.75.75,0,0,1,.75.75v.75H9.5a.75.75,0,0,1,0,1.5H8.75V9.5a.75.75,0,1,1-1.5,0V8.75H6.5a.75.75,0,1,1,0-1.5h.75V6.5A.75.75,0,0,1,8,5.75Z" transform="translate(-2 -2.001)" fill="#7f8f9a" />
        </Fragment>
    );
};

export default MagnifyGlassResources;
