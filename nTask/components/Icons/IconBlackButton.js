import React from 'react'

function IconBlackButton() {
    return (
        <>
        <svg  width="33" height="33" viewBox="0 0 33 33">
  <g id="backButton" transform="translate(-52 -80)">
    <g id="Ellipse_127" data-name="Ellipse 127" transform="translate(52 80)" fill="none" stroke="silver" stroke-width="1">
      <circle cx="16.5" cy="16.5" r="16.5" stroke="none"/>
      <circle cx="16.5" cy="16.5" r="16" fill="none"/>
    </g>
    <path id="Icon_ionic-ios-arrow-back" data-name="Icon ionic-ios-arrow-back" d="M13.662,13.188,18.957,7.9a1,1,0,0,0-1.416-1.412l-6,5.995a1,1,0,0,0-.029,1.379L17.536,19.9a1,1,0,0,0,1.416-1.412Z" transform="translate(52.749 83.806)"/>
  </g>
</svg>

        </>
    )
}

export default IconBlackButton
