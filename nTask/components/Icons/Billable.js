import React, { Component, Fragment } from 'react';
// viewBox="0 0 17.99 17.99"
const BillableIcon = () => {
    return (
        <Fragment>
            <g>
                <path fill="currentColor" d="M9,0C4.03,0,0,4.03,0,9s4.03,9,9,9,9-4.03,9-9S13.96,0,9,0Zm.74,14.05v1.68h-1.62v-1.57c-1.11-.05-2.18-.35-2.81-.71l.5-1.93c.69,.38,1.66,.73,2.74,.73,.94,0,1.58-.36,1.58-1.03,0-.63-.53-1.02-1.75-1.44-1.76-.59-2.97-1.42-2.97-3.02,0-1.45,1.03-2.59,2.79-2.94v-1.57h1.62v1.45c1.1,.05,1.85,.28,2.39,.54l-.47,1.87c-.44-.18-1.19-.56-2.38-.56-1.07,0-1.42,.46-1.42,.93,0,.54,.58,.89,1.98,1.42,1.96,.69,2.76,1.6,2.76,3.09s-1.04,2.72-2.94,3.05h0Z" />
            </g>
        </Fragment >
    )
}

export default BillableIcon