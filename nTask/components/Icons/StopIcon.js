import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
function StopIcon({className}) {
    return (
        <SvgIcon className={className} viewBox="0 0 17 17">
          <path id="Stop" d="M8,16a8,8,0,1,1,8-8A8.007,8.007,0,0,1,8,16ZM6.333,5.333a1,1,0,0,0-1,1V9.667a1,1,0,0,0,1,1H9.667a1,1,0,0,0,1-1V6.333a1,1,0,0,0-1-1Z" transform="translate(0.5 0.5)" fill="#fb6e6e" stroke="rgba(0,0,0,0)" stroke-width="1"/>
</SvgIcon>

    )
}

export default StopIcon
