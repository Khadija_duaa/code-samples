import React, { Component, Fragment } from 'react';

const MarkAllIcon = () => {
    return (
        <Fragment>
            <g transform="translate(-92.28 -376.28)">
                <g style={{fill: '#f6fff7', stroke: '#6cd576', strokeWidth: 2}} transform="translate(92.28 376.28)">
                    <circle style={{stroke: 'none'}} cx="40.36" cy="40.36" r="40.36"/>
                    <circle style={{fill: 'none'}} cx="40.36" cy="40.36" r="39.36"/>
                </g>
                <path style={{fill: '#38cc46', stroke: '#38cc46', strokeLinejoin: 'round'}} class="b" d="M132.958,385.537a1.807,1.807,0,0,0-2.436,0L109.88,404.861l-7.935-7.428a1.807,1.807,0,0,0-2.436,0,1.541,1.541,0,0,0,0,2.281l9.153,8.569a1.808,1.808,0,0,0,2.437,0l21.861-20.465A1.541,1.541,0,0,0,132.958,385.537Z" transform="translate(16.431 21.466)"/>
            </g>
        </Fragment>
    )
}

export default MarkAllIcon