
import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconPremiumYellow = ({ style, className }) => {
  return (
    <SvgIcon style={style} viewBox="0 0 18 12.112" className={className}>
      <path
        id="icon_Premium_yellow"
        d="M600.291,403.368a.51.51,0,0,1-.013.15l-1.11,4.441a.5.5,0,0,1-.487.382l-6.662.034h-6.664a.5.5,0,0,1-.49-.383l-1.11-4.458a.5.5,0,0,1-.013-.154,1.06,1.06,0,1,1,.991-.193l1.39,1.4a1.887,1.887,0,0,0,2.844-.193l2.285-3.027a1.06,1.06,0,1,1,1.515-.018l0,0,2.268,3.035a1.892,1.892,0,0,0,1.51.757,1.872,1.872,0,0,0,1.333-.552l1.4-1.4a1.06,1.06,0,1,1,1.015.174Zm-1.232,6.589a.5.5,0,0,0-.5-.5H585.5a.5.5,0,0,0-.5.5v1.211a.5.5,0,0,0,.5.5h13.054a.5.5,0,0,0,.5-.5Z"
        transform="translate(-583 -399.56)"
        style={{ fill: "#fa0" }}
      />
    </SvgIcon>
  );
};

export default IconPremiumYellow;
