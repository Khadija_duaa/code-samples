import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconBasicGreen = ({ style, className }) => {
  return (
    <SvgIcon style={style} viewBox="0 0 15 14.43" className={className}>
      <path
        d="M335.684,403.9a1.082,1.082,0,0,0-.6-1.842l-3.632-.53a.474.474,0,0,1-.357-.261l-1.624-3.3a1.074,1.074,0,0,0-1.929,0l-1.624,3.3a.476.476,0,0,1-.358.261l-3.632.53a1.082,1.082,0,0,0-.6,1.842l2.628,2.572a.479.479,0,0,1,.137.422l-.62,3.632a1.06,1.06,0,0,0,.234.876,1.084,1.084,0,0,0,1.326.263l3.248-1.715a.483.483,0,0,1,.442,0l3.248,1.715a1.061,1.061,0,0,0,.5.126,1.077,1.077,0,0,0,.825-.389,1.06,1.06,0,0,0,.235-.876l-.62-3.632a.479.479,0,0,1,.137-.422Z"
        transform="translate(-321.01 -397.365)"
      // style={{ fill: "#26bb88" }}
      />
    </SvgIcon>
  );
};

export default IconBasicGreen;
