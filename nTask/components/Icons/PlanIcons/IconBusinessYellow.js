import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconBusinessYellow = ({ style, className }) => {
  return (
    <SvgIcon style={style} viewBox="0 0 16 13.344" className={className}>
      <path
        id="icon_Business_yellow"
        d="M1.334,13.344A1.336,1.336,0,0,1,0,12.009V7.331a.335.335,0,0,1,.185-.3.331.331,0,0,1,.35.033,1.316,1.316,0,0,0,.8.274H6.667v1A.333.333,0,0,0,7,8.673H9a.333.333,0,0,0,.333-.333v-1h5.333a1.317,1.317,0,0,0,.8-.274A.333.333,0,0,1,16,7.331v4.678a1.335,1.335,0,0,1-1.333,1.335Zm8-6.672V6.338A.333.333,0,0,0,9,6H7a.333.333,0,0,0-.333.334v.334H1.334A1.336,1.336,0,0,1,0,5.337v-2A1.336,1.336,0,0,1,1.334,2H4.667V1.335A1.335,1.335,0,0,1,6,0h4a1.335,1.335,0,0,1,1.333,1.335V2h3.333A1.335,1.335,0,0,1,16,3.336v2a1.335,1.335,0,0,1-1.333,1.335ZM6,2h4V1.335H6Z"
        style={{ fill: "#fa0" }}
      />
    </SvgIcon>
  );
};

export default IconBusinessYellow;
