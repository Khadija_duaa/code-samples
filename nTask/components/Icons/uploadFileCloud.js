import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
function UploadFileCloudIcon({color, ...rest}) {
  return (
    <SvgIcon viewBox="0 0 26.748 24.05" style={{fill: color || '#00b781'}} {...rest}>
      <g id="iconUpload" transform="translate(-2.658 -3.95)">
        <path id="Path_8217" data-name="Path 8217" d="M14.3,11.385a1.333,1.333,0,0,0-1.867,0l-4,3.867a1.333,1.333,0,1,0,1.84,1.92l1.747-1.693V23a1.333,1.333,0,0,0,2.667,0V15.545l1.72,1.733A1.339,1.339,0,1,0,18.3,15.385Z" transform="translate(2.649 3.668)" style={{fill: 'inherit'}}/>
        <path id="Path_8218" data-name="Path 8218" d="M22.9,8.346a8,8,0,0,0-15.12,0A6.667,6.667,0,0,0,3.669,19.373a1.333,1.333,0,1,0,2-1.693,4,4,0,0,1,3-6.667H8.8a1.333,1.333,0,0,0,1.333-1.067,5.333,5.333,0,0,1,10.453,0,1.333,1.333,0,0,0,1.333,1.067H22a4,4,0,0,1,3,6.667,1.336,1.336,0,1,0,2,1.773A6.667,6.667,0,0,0,22.9,8.346Z" transform="translate(0.665 0.987)" style={{fill: 'inherit'}}/>
      </g>
    </SvgIcon>
  )
}

export default UploadFileCloudIcon
