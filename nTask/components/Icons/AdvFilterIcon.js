import React, { Component, Fragment } from 'react';

const QuickFilterIcon = () => {
    return (
        <Fragment>
            <g transform="translate(82 -51.5)">
                <rect style={{opacity:0}} width="24" height="24" transform="translate(-82 51.5)"/>
                <path d="M1280.448,91.831a.509.509,0,0,0-.5-.332h-10.893a.509.509,0,0,0-.5.332.492.492,0,0,0,.119.6l4.2,4.2v4.143a.525.525,0,0,0,.162.384l2.179,2.182a.506.506,0,0,0,.383.162.591.591,0,0,0,.213-.043.51.51,0,0,0,.332-.5V96.63l4.2-4.2A.492.492,0,0,0,1280.448,91.831Z" transform="translate(-1344.499 -33.998)"/>
            </g>
        </Fragment> 
    )
}

export default QuickFilterIcon