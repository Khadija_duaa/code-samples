// viewBox="0 0 16 16"
import React, { Fragment } from "react";

const IconChart = () => {
  return (
    <Fragment>
      <g id="iconChart" transform="translate(0 0)">
        <path
          id="Path_492"
          data-name="Path 492"
          d="M.469,0A8.7,8.7,0,0,1,9.177,8.708a.469.469,0,0,1-.469.469H.469A.469.469,0,0,1,0,8.708V.469A.469.469,0,0,1,.469,0Z"
          transform="translate(6.823 0)"
          // fill="#00cc90"
        />
        <path
          id="Path_493"
          data-name="Path 493"
          d="M57.741,329.53a.469.469,0,0,1,.411-.244h2.775a.469.469,0,0,1,.448.61,7.311,7.311,0,0,1-6.5,5.1v-3.517A3.806,3.806,0,0,0,57.741,329.53Z"
          transform="translate(-47.111 -318.996)"
          // fill="#00cc90"
        />
        <path
          id="Path_494"
          data-name="Path 494"
          d="M334.386,54.9a.469.469,0,0,1,.61.448v2.775a.469.469,0,0,1-.244.411A3.806,3.806,0,0,0,332.8,61.4h-3.517A7.311,7.311,0,0,1,334.386,54.9Z"
          transform="translate(-329.286 -53.159)"
          // fill="#00cc90"
        />
        <path
          id="Path_495"
          data-name="Path 495"
          d="M297.195,293.678a3.812,3.812,0,0,0,3.306,3.306V300.5a7.3,7.3,0,0,1-6.822-6.822Z"
          transform="translate(-293.679 -284.501)"
          // fill="#00cc90"
        />
      </g>
    </Fragment>
  );
};

export default IconChart;
