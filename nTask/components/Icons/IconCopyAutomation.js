import React from "react";

export const IconCopy = (props) => {
  const { color = "#969696", style } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="16"
      viewBox="0 0 16 16"
      style={style}>
      <g transform="translate(-2 -2)">
        <path
          d="M13.2,11.92v3.36c0,2.8-1.12,3.92-3.92,3.92H5.92C3.12,19.2,2,18.08,2,15.28V11.92C2,9.12,3.12,8,5.92,8H9.28C12.08,8,13.2,9.12,13.2,11.92Z"
          transform="translate(0 -1.2)"
          fill={color}
        />
        <path
          d="M15.293,2h-3.36c-2.467,0-3.623.875-3.864,2.991A.744.744,0,0,0,8.83,5.8h1.663c3.36,0,4.92,1.56,4.92,4.92v1.662a.744.744,0,0,0,.809.762c2.116-.241,2.991-1.4,2.991-3.864V5.92C19.213,3.12,18.093,2,15.293,2Z"
          transform="translate(-1.213)"
          fill={color}
        />
      </g>
    </svg>
  );
};

export default IconCopy;
