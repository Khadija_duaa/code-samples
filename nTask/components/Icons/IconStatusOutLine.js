import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';
const IconStatusOutLine = ({ color = "#fff" }) => {
  return (
    <>
      <SvgIcon  xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
        <circle id="status" cx="7" cy="7" r="7" fill={color} />
      </SvgIcon>
    </>

  )
}
export default IconStatusOutLine;
