import React, { Component, Fragment } from 'react';

const StorageIcon = () => {
    return (
        <Fragment>
                <g transform="translate(-431 -395)">
                    <rect style={{fill: '#fa0'}}  width="20" height="20" rx="4" transform="translate(431 395)"/>
                    <path style={{fill: '#fff'}} d="M12.727,82.978A3.275,3.275,0,0,1,9.456,86.25H2.6a2.6,2.6,0,0,1-.072-5.208A3.268,3.268,0,0,1,8.9,79.754,3.275,3.275,0,0,1,12.727,82.978Z" transform="translate(434.636 322.955)"/>
                </g>
        </Fragment>
    )
}

export default StorageIcon