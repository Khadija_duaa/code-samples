
import React from "react";

const IconRule = props => {

  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
  <g id="iconRule" transform="translate(2 2)">
    <g id="Circle" fill="#e6f7fd" stroke="#fff" stroke-width="2">
      <circle cx="18" cy="18" r="18" stroke="none"/>
      <circle cx="18" cy="18" r="19" fill="none"/>
    </g>
    <path id="flash" d="M15.6,9.192H13.05V3.254c0-1.385-.75-1.666-1.666-.627l-.66.75L5.142,9.728c-.767.866-.445,1.575.709,1.575H8.4V17.24c0,1.385.75,1.666,1.666.627l.66-.75,5.583-6.35C17.074,9.9,16.753,9.192,15.6,9.192Z" transform="translate(7.275 8)" fill="#00abed"/>
  </g>
</svg>
  );
};

export default IconRule;
