import React from "react";
const IconCopyContent = () => {
  return (
    <>
      <path
        // style={{ fill: "#969696" }}
        d="M13.316,1.5H4.474A1.478,1.478,0,0,0,3,2.974V13.289H4.474V2.974h8.842Zm2.211,2.947H7.421A1.478,1.478,0,0,0,5.947,5.921V16.237a1.478,1.478,0,0,0,1.474,1.474h8.105A1.478,1.478,0,0,0,17,16.237V5.921A1.478,1.478,0,0,0,15.526,4.447Zm0,11.789H7.421V5.921h8.105Z"
        transform="translate(-3 -1.5)"
      />
    </>
  );
};

export default IconCopyContent;
