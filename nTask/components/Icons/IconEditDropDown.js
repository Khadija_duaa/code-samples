import React from "react";
const IconEditDropDown = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="13.95" viewBox="0 0 14 13.95">
      <defs></defs>
      <path
        style={{ fill: "#0090ff " }}
        d="M17.475,6.93l-2.4-2.4a1.75,1.75,0,0,0-2.328-.061L4.874,12.346a1.75,1.75,0,0,0-.5,1.059L4,17.054a.873.873,0,0,0,.875.954h.079L8.6,17.675a1.75,1.75,0,0,0,1.059-.5L17.536,9.3a1.68,1.68,0,0,0-.061-2.371Zm-9.03,9-2.625.245.236-2.625L11,8.662l2.363,2.363ZM14.5,9.852,12.155,7.507l1.706-1.75L16.25,8.146Z"
        transform="translate(-3.996 -4.058)"
      />
    </svg>
  );
};
export default IconEditDropDown;
