import React from "react";

const IconTimesheet = props => {
  const { color = "#7e7e7e" } = props;
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19.129" viewBox="0 0 20 19.129">
      <path
        d="M17,2.43H7a4.724,4.724,0,0,0-5,5v6a4.724,4.724,0,0,0,5,5h4l4.45,2.96A1,1,0,0,0,17,20.56V18.43a4.724,4.724,0,0,0,5-5v-6A4.724,4.724,0,0,0,17,2.43Zm-1.5,8.82h-7a.75.75,0,0,1,0-1.5h7a.75.75,0,0,1,0,1.5Z"
        transform="translate(-2 -2.43)"
        fill={color}
      />
    </svg>
  );
};

export default IconTimesheet;
