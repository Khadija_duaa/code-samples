import React from "react";
// viewBox="0 0 16 15.111"
const IconRiskOutlined = () => {
  return (
    <g transform="translate(-37.61 -49.75)">
      <path d="M51.853,64.861H39.367a1.758,1.758,0,0,1-1.548-2.592l6.243-11.594a1.759,1.759,0,0,1,3.1,0L53.4,62.269a1.758,1.758,0,0,1-1.548,2.592ZM45.61,50.922a.574.574,0,0,0-.516.308L38.851,62.824a.586.586,0,0,0,.516.864H51.853a.586.586,0,0,0,.516-.864L46.126,51.231A.574.574,0,0,0,45.61,50.922Z" transform="translate(0 0)" fill="currentColor"/>
      <path d="M235.059,173.44a.8.8,0,0,1,.8.832l-.21,4.468a.586.586,0,0,1-1.171,0l-.21-4.469a.8.8,0,0,1,.788-.832Z" transform="translate(-189.449 -119.159)" fill="currentColor"/>
      <path d="M236.733,358.715a.733.733,0,1,1,.733-.733A.733.733,0,0,1,236.733,358.715Z" transform="translate(-191.123 -296.236)" fill="currentColor"/>
    </g>
  );
};

export default IconRiskOutlined;
