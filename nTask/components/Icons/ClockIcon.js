
import React, { Component, Fragment } from "react";

const ClockIcon = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
      <g id="Icon_Clock" data-name="Icon Clock" transform="translate(-1.25 -1.25)">
        <path id="Path_41593" data-name="Path 41593" d="M9.25,17.25a8,8,0,1,1,8-8A8.007,8.007,0,0,1,9.25,17.25Zm0-14.884A6.884,6.884,0,1,0,16.134,9.25,6.892,6.892,0,0,0,9.25,2.366Z" fill="#7e7e7e" />
        <path id="Path_41594" data-name="Path 41594" d="M14.477,13.584a.5.5,0,0,1-.283-.082l-2.307-1.377a2.195,2.195,0,0,1-1-1.756V7.318a.558.558,0,1,1,1.116,0v3.051a1.1,1.1,0,0,0,.454.8l2.307,1.377a.557.557,0,0,1,.193.767A.573.573,0,0,1,14.477,13.584Z" transform="translate(-2.466 -1.41)" fill="#7e7e7e" />
      </g>
    </svg>
  );
};

export default ClockIcon;