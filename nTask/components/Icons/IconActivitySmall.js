import React, { Component, Fragment } from "react";

const IconActivitySmall = () => {
  return (
    <Fragment>
      <path
        style={{ fill: "#969696" }}
        d="M10.631,15.2h-.147A1.4,1.4,0,0,1,9.252,14.1L7.376,5.4,5.444,9.882a.7.7,0,0,1-.644.42H2.7a.7.7,0,0,1,0-1.4H4.338L6.1,4.849a1.4,1.4,0,0,1,2.653.266L10.624,13.8l1.932-4.466A.7.7,0,0,1,13.2,8.9h2.1a.7.7,0,1,1,0,1.4H13.662L11.9,14.355A1.4,1.4,0,0,1,10.631,15.2Z"
        transform="translate(-2 -4.008)"
      />
    </Fragment>
  );
};

export default IconActivitySmall;
