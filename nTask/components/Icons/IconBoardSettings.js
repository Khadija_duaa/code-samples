import SvgIcon from "@material-ui/core/SvgIcon";
import React from "react";

export const IconBoardSettings = ({ color = "#85898b", style }) => {
  return (
    <SvgIcon width="24" height="24" viewBox="0 0 14 12.444" style={style}>
      <path
        d="M11.593,7.714a2.366,2.366,0,1,1-2.407,2.366A2.386,2.386,0,0,1,11.593,7.714ZM5.658,9.071a1.037,1.037,0,1,1,0,2.074h-4.6a1.037,1.037,0,1,1,0-2.074h4.6ZM2.407,0A2.386,2.386,0,0,1,4.814,2.365,2.386,2.386,0,0,1,2.407,4.731,2.387,2.387,0,0,1,0,2.365,2.387,2.387,0,0,1,2.407,0ZM12.945,1.329a1.037,1.037,0,1,1,0,2.073h-4.6a1.037,1.037,0,1,1,0-2.073h4.6Z"
        style={{
          fill: color,
        }}
      />
    </SvgIcon>
  );
};
