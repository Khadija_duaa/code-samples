
import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconFeedback = ({ color = "#a5b4be", style }) => {
  return (
    <SvgIcon style={{  ...style }} viewBox="0 0 16 14.772">
      <g id="iconLogout" transform="translate(0 -0.004)">
        <path
          id="iconFeedback"
          d="M.2,13.708a.614.614,0,0,0,.417,1.064H8.02c4.4,0,7.98-3.3,7.98-7.366S12.42,0,8.02,0,0,3.345,0,7.407a6.981,6.981,0,0,0,1.876,4.744ZM4.338,3.724H11.7a.614.614,0,1,1,0,1.228H4.338a.614.614,0,1,1,0-1.228Zm0,2.455H11.7a.614.614,0,1,1,0,1.228H4.338a.614.614,0,0,1,0-1.228Zm0,2.455H11.7a.614.614,0,1,1,0,1.228H4.338a.614.614,0,0,1,0-1.228Z"
          
        />
      </g>
    </SvgIcon>
  );
};

export default IconFeedback;
