import React, { Component, Fragment } from "react";

const WorkspaceIcon = () => {
  return (
    <Fragment>
      <path
        style={{ fill: "fff", fillRule: "evenodd" }}
        d="M169.75,219.75h2.625a1.75,1.75,0,0,1,1.75,1.75v2.625a1.75,1.75,0,0,1-1.75,1.75H169.75a1.75,1.75,0,0,1-1.75-1.75V221.5A1.75,1.75,0,0,1,169.75,219.75Zm7.875,0h2.625A1.75,1.75,0,0,1,182,221.5v2.625a1.75,1.75,0,0,1-1.75,1.75h-2.625a1.75,1.75,0,0,1-1.75-1.75V221.5A1.75,1.75,0,0,1,177.625,219.75Zm-7.875,7.438h2.625a1.75,1.75,0,0,1,1.75,1.75v2.625a1.75,1.75,0,0,1-1.75,1.75H169.75a1.75,1.75,0,0,1-1.75-1.75v-2.625A1.75,1.75,0,0,1,169.75,227.188Zm7.875,0h2.625a1.75,1.75,0,0,1,1.75,1.75v2.625a1.75,1.75,0,0,1-1.75,1.75h-2.625a1.75,1.75,0,0,1-1.75-1.75v-2.625A1.75,1.75,0,0,1,177.625,227.188Z"
        transform="translate(-168 -219.75)"
      />
    </Fragment>
  );
};

export default WorkspaceIcon;
