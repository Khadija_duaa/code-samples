import React, { Component, Fragment } from "react";

const ReplyLaterActionIcon = () => {
  return (
    <Fragment>
      <path d="M8,0a8,8,0,1,0,8,8A8.009,8.009,0,0,0,8,0ZM8,14.251A6.251,6.251,0,1,1,14.251,8,6.258,6.258,0,0,1,8,14.251Z"/>
      <path d="M258.144,143.286v-3.353a.678.678,0,1,0-1.355,0v3.57c0,.011,0,.021,0,.031a.674.674,0,0,0,.2.512l2.524,2.524a.678.678,0,0,0,.958-.958Z" transform="translate(-249.452 -135.276)"/>
    </Fragment>
  )
}

export default ReplyLaterActionIcon;
