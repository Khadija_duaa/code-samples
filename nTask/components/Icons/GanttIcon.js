import React, { Component, Fragment } from 'react';

const GanttIcon = () => {
    return (
        <Fragment>
            <g transform="translate(-431 -425)">
                <rect style={{fill: '#fa0'}} width="20" height="20" rx="4" transform="translate(431 425)"/>
                <path style={{fill: '#fff', fillRule: 'evenodd'}} d="M167.882,220.437h7.092a.869.869,0,0,1,0,1.738h-7.092a.869.869,0,0,1,0-1.738Zm2.518,3.676h7.252a.869.869,0,0,1,0,1.738H170.4a.869.869,0,1,1,0-1.738Zm-1.1,3.617h5.192a.869.869,0,1,1,0,1.738H169.3a.869.869,0,0,1,0-1.738Z" transform="translate(267.964 209.563)"/>
            </g>
        </Fragment>
    )
}

export default GanttIcon