import React, { Component, Fragment } from 'react';

const ImprovementIcon = () => {
    return (
        <Fragment>
          <path style={{fill: "#72c70e", fillRule: "evenodd"}} data-name="icon improvement" d="M489.643,335.607h4.714v-4.714H497.5l-5.5-5.5-5.5,5.5h3.143v4.714Z" transform="translate(-486.5 -325.406)"/>
        </Fragment>
    )
}

export default ImprovementIcon