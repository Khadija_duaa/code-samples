{
  /* <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22"><defs><style>.a{fill:;}.b{fill:#fff;fill-rule:;}</style></defs></svg> */
}


import React, { Component, Fragment } from "react";

const IconKanban = () => {
  return (
    <Fragment>
      <g transform="translate(-200 -18)">
        <rect
          style={{ fill: "#fa0" }}
          width="22"
          height="22"
          rx="4"
          transform="translate(200 18)"
        />
        <path
          style={{ fill: "#fff", fillRule: "evenodd" }}
          d="M208.219,4201.5h0a1.722,1.722,0,0,1,1.718,1.724v8.552a1.721,1.721,0,0,1-1.718,1.723h0a1.722,1.722,0,0,1-1.719-1.723v-8.552A1.722,1.722,0,0,1,208.219,4201.5Zm4.263,0h.035a1.683,1.683,0,0,1,1.686,1.689v3.743a1.684,1.684,0,0,1-1.686,1.691h-.035a1.686,1.686,0,0,1-1.687-1.691v-3.743a1.684,1.684,0,0,1,1.687-1.689Zm3.212,0h2.2a.6.6,0,0,1,.61.6v8.379a.608.608,0,0,1-.61.6h-2.2a.608.608,0,0,1-.61-.6V4202.1a.6.6,0,0,1,.61-.6Z"
          transform="translate(-1.5 -4178.5)"
        />
      </g>
    </Fragment>
  );
};

export default IconKanban;
