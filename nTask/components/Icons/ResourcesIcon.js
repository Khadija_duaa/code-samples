import React, { Component, Fragment } from "react";

const ResourcesIcon = () => {
  return (
    <Fragment>
      <path
        class="a"
        d="M13.5,10A2.5,2.5,0,1,0,11,7.5,2.49,2.49,0,0,0,13.5,10ZM6.833,10a2.5,2.5,0,1,0-2.5-2.5A2.49,2.49,0,0,0,6.833,10Zm0,1.667C4.892,11.667,1,12.642,1,14.583v2.083H12.667V14.583C12.667,12.642,8.775,11.667,6.833,11.667Zm6.667,0c-.242,0-.517.017-.808.042a3.517,3.517,0,0,1,1.642,2.875v2.083h5V14.583C19.333,12.642,15.442,11.667,13.5,11.667Z"
        transform="translate(-0.167 -0.833)"
      />
    </Fragment>
  );
};

export default ResourcesIcon;
