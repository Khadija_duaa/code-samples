import React, { Component, Fragment } from "react";

const ConvertToTaskIcon = () => {
  return (
    <Fragment>
      <path style={{fillRule:'evenodd'}} d="M368.528,100.417h-13a.509.509,0,0,0-.5.5v2a.507.507,0,0,0,.5.5h13a.507.507,0,0,0,.5-.5v-2A.509.509,0,0,0,368.528,100.417Zm-.5,2h-5v-1h5v1Zm.5-6h-13a.507.507,0,0,0-.5.5v2a.507.507,0,0,0,.5.5h13a.507.507,0,0,0,.5-.5v-2a.507.507,0,0,0-.5-.5Zm-.5,2h-8v-1h8Zm.852-5.855a.481.481,0,0,0-.351-.148h-13a.507.507,0,0,0-.5.5v2a.507.507,0,0,0,.5.5h13a.507.507,0,0,0,.5-.5v-2a.481.481,0,0,0-.149-.35Zm-.852,1.851h-3v-1h3v1Z" transform="translate(-355.028 -92.414)"/>
    </Fragment>
  )
}

export default ConvertToTaskIcon;
