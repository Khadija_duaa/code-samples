import React, { Component, Fragment } from "react";

const ContrastIcon = () => {
  return (
    <Fragment>
      <path
        style={{ fill: "#0090ff" }}
        d="M256,0C114.507,0,0,114.497,0,256c0,140.84,113.914,256,256,256c141.493,0,256-114.497,256-256
	C512,114.507,397.503,0,256,0z"
      />
      <path
        style={{ fill: "#f24c4c" }}
        d="M256,0v512c141.493,0,256-114.497,256-256C512,114.507,397.503,0,256,0z"
      />
    </Fragment>
  );
};

export default ContrastIcon;
