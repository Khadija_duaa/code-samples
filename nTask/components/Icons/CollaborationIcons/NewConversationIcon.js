import React from "react";

const NewConversationIcon = color => {
  return (
    <>
      <g transform="translate(-15.1 -14.5)">
        <path
          style={{fill: color}}
          d="M39.517,35.5H38.14v2.64H35.5v1.377h2.64v2.64h1.377v-2.64h2.64V38.14h-2.64Z"
          transform="translate(-15.717 -16.179)"
        />
        <path
          style={{fill: color}}
          d="M23.111,14.5A8.019,8.019,0,0,0,15.1,22.511a7.834,7.834,0,0,0,.551,2.915,8.058,8.058,0,0,0,1.1,1.928L15.7,29.834a.676.676,0,0,0,.092.689.686.686,0,0,0,.528.253h.115l4.063-.712a7.792,7.792,0,0,0,2.594.436,8,8,0,1,0,.023-16Zm0,14.646a6.546,6.546,0,0,1-2.3-.413.656.656,0,0,0-.367-.023l-3.007.528.712-1.7a.673.673,0,0,0-.115-.712,6.31,6.31,0,0,1-1.125-1.882,6.636,6.636,0,1,1,6.2,4.2Z"
          transform="translate(0 0)"
        />
      </g>
    </>
  );
};

NewConversationIcon.defaultProps = {
  color: "#ffffff",
};
export default NewConversationIcon;
