import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const Love = ({ color = "#a5b4be", style }) => {
  return (
    <SvgIcon style={style} viewBox="0 0 42 42">
      <defs>
        <filter id="Ellipse_54" x="0" y="0" width="42" height="42" filterUnits="userSpaceOnUse">
          <feOffset dy="2" input="SourceAlpha" />
          <feGaussianBlur stdDeviation="1" result="blur" />
          <feFlood flood-opacity="0.161" />
          <feComposite operator="in" in2="blur" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g id="Love" transform="translate(-49 -660)">
        <g transform="matrix(1, 0, 0, 1, 49, 660)" filter="url(#Ellipse_54)">
          <circle
            id="Ellipse_54-2"
            data-name="Ellipse 54"
            cx="18"
            cy="18"
            r="18"
            transform="translate(3 1)"
            style={{ fill: "#ffffff" }}
          />
        </g>
        <path
          id="Path_472"
          data-name="Path 472"
          d="M19.8,40.853a5.189,5.189,0,0,0-9.572-1.827c-.086.143-.284.482-.288.477s-.2-.334-.288-.477A5.189,5.189,0,0,0,.085,40.853,6.422,6.422,0,0,0,1.2,45.533,22.782,22.782,0,0,0,3.658,48.7a55.514,55.514,0,0,0,6.286,5.332A55.475,55.475,0,0,0,16.231,48.7a22.794,22.794,0,0,0,2.462-3.162A6.421,6.421,0,0,0,19.8,40.853ZM4.4,39.124a1.825,1.825,0,0,0-1.312.613,1.905,1.905,0,0,0-.5,1.451.514.514,0,0,1-.47.555l-.043,0a.514.514,0,0,1-.512-.472,2.964,2.964,0,0,1,.764-2.227,2.856,2.856,0,0,1,2.06-.95A.514.514,0,0,1,4.4,39.124Z"
          transform="translate(60.1 633.49)"
          style={{ fill: "#ee4173" }}
        />
      </g>
    </SvgIcon>
  );
};

export default Love;
