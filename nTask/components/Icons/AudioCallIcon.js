import React from "react";

const AudioCallIcon = color => {
  return (
    <>
      <path
        style={{ fill: color }}
        d="M15.146,11.032l-2.1-2.1a1.4,1.4,0,0,0-2.322.524,1.428,1.428,0,0,1-1.648.9,6.469,6.469,0,0,1-3.9-3.9,1.358,1.358,0,0,1,.9-1.648,1.4,1.4,0,0,0,.524-2.322l-2.1-2.1a1.5,1.5,0,0,0-2.023,0L1.061,1.817c-1.423,1.5.15,5.469,3.671,8.99s7.492,5.169,8.99,3.671l1.423-1.423A1.5,1.5,0,0,0,15.146,11.032Z"
        transform="translate(-0.539 0)"
      />
    </>
  );
};

AudioCallIcon.defaultProps = {
  color: "#ffffff",
};
export default AudioCallIcon;
