import React from "react";
// viewBox="0 0 20 20"
const IconReload = () => {
  return (
      <path d="M58,48A10,10,0,1,0,68,58,10.011,10.011,0,0,0,58,48Zm5.769,9.172H59.49L61.263,55.4l-.269-.313a4.2,4.2,0,1,0,.937,4.315l.258-.727,1.45.513-.255.727a5.742,5.742,0,1,1-1.263-5.876l.027.03.207.24,1.413-1.412Z" transform="translate(-48 -48)" fill="currentColor"/>
  );
};

export default IconReload;
