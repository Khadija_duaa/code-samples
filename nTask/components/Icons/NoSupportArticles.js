{
  /* <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15"><defs><style>.a{fill:;}</style></defs></svg> */
}
// viewBox="0 0 180 117.752"
import React, { Component, Fragment } from "react";

const NoSupportArticles = () => {
  return (
    <Fragment>
      <g transform="translate(-714.02 -76.247)">
        <g transform="translate(714.02 192.851)">
          <g transform="translate(0)">
            <rect width="180" height="0.985" fill="#28728e" />
          </g>
        </g>
        <path d="M946.382,76.248h-97.5c-4.423,0-8.022,3.05-8.022,6.8V194H954.4V83.047C954.4,79.3,950.805,76.248,946.382,76.248Z" transform="translate(-85.215 -0.001)" fill="#00abed" opacity="0.25" />
        <g transform="translate(748.674 76.247)">
          <path d="M20.945,0H90.3a20.945,20.945,0,0,1,20.945,20.945V115.8a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V20.945A20.945,20.945,0,0,1,20.945,0Z" transform="translate(1.149 0.974)" fill="#fff" />
          <path d="M933.161,194H819.618V83.046c0-3.749,3.6-6.8,8.022-6.8h97.5c4.423,0,8.022,3.05,8.022,6.8Zm-111.245-1.947H930.864V83.046c0-2.676-2.568-4.852-5.725-4.852h-97.5c-3.157,0-5.725,2.177-5.725,4.852Z" transform="translate(-819.618 -76.247)" fill="#28728e" />
        </g>
        <g transform="translate(760.941 102.526)">
          <rect width="87.294" height="31.439" transform="translate(0.492 0.281)" fill="#00abed" />
          <path d="M945.274,188.324H856.995v-32h88.279Zm-87.294-.562h86.31V156.886H857.98Z" transform="translate(-856.995 -156.324)" fill="#28728e" />
        </g>
        <g transform="translate(760.941 143.247)">
          <rect width="34.136" height="17.393" transform="translate(0.192 0.155)" fill="#00abed" />
          <path d="M891.516,174.028H856.995v-17.7h34.521Zm-34.136-.311h33.751V156.635H857.38Z" transform="translate(-856.995 -156.324)" fill="#28728e" />
        </g>
        <g transform="translate(760.941 167.128)">
          <rect width="34.136" height="17.393" transform="translate(0.192 0.155)" fill="#00abed" />
          <path d="M891.516,174.028H856.995v-17.7h34.521Zm-34.136-.311h33.751V156.635H857.38Z" transform="translate(-856.995 -156.324)" fill="#28728e" />
        </g>
        <g transform="translate(0 -27.908)">
          <g transform="translate(801.428 172.155)">
            <rect width="47.299" height="1.784" transform="translate(0 0)" fill="#9ed6ec" />
          </g>
          <g transform="translate(801.428 176.614)">
            <rect width="47.299" height="1.784" transform="translate(0 0)" fill="#9ed6ec" />
          </g>
          <g transform="translate(801.428 181.073)">
            <rect width="47.299" height="1.784" transform="translate(0 0)" fill="#9ed6ec" />
          </g>
          <g transform="translate(801.428 185.532)">
            <rect width="24.718" height="1.784" transform="translate(0 0)" fill="#9ed6ec" />
          </g>
        </g>
        <g transform="translate(0 -4.027)">
          <g transform="translate(801.428 172.155)">
            <rect width="47.299" height="1.784" transform="translate(0 0)" fill="#9ed6ec" />
          </g>
          <g transform="translate(801.428 176.614)">
            <rect width="47.299" height="1.784" transform="translate(0 0)" fill="#9ed6ec" />
          </g>
          <g transform="translate(801.428 181.073)">
            <rect width="47.299" height="1.784" transform="translate(0 0)" fill="#9ed6ec" />
          </g>
          <g transform="translate(801.428 185.532)">
            <rect width="24.718" height="1.784" transform="translate(0 0)" fill="#9ed6ec" />
          </g>
        </g>
        <g transform="translate(756.018 84.205)">
          <circle cx="2.789" cy="2.789" r="2.789" transform="translate(0.492 0.492)" fill="#00abed" />
          <path d="M845.277,107.058a3.282,3.282,0,1,1,3.282-3.282A3.285,3.285,0,0,1,845.277,107.058Zm0-5.579a2.3,2.3,0,1,0,2.3,2.3A2.3,2.3,0,0,0,845.277,101.48Z" transform="translate(-841.995 -100.495)" fill="#28728e" />
        </g>
        <g transform="translate(765.973 84.205)">
          <circle cx="2.789" cy="2.789" r="2.789" transform="translate(0.492 0.492)" fill="#00abed" />
          <path d="M875.61,107.058a3.282,3.282,0,1,1,3.282-3.282A3.285,3.285,0,0,1,875.61,107.058Zm0-5.579a2.3,2.3,0,1,0,2.3,2.3A2.3,2.3,0,0,0,875.61,101.48Z" transform="translate(-872.328 -100.495)" fill="#28728e" />
        </g>
        <g transform="translate(775.927 84.205)">
          <circle cx="2.789" cy="2.789" r="2.789" transform="translate(0.492 0.492)" fill="#00abed" />
          <path d="M905.943,107.058a3.282,3.282,0,1,1,3.282-3.282A3.285,3.285,0,0,1,905.943,107.058Zm0-5.579a2.3,2.3,0,1,0,2.3,2.3A2.3,2.3,0,0,0,905.943,101.48Z" transform="translate(-902.661 -100.495)" fill="#28728e" />
        </g>
      </g>
    </Fragment>
  );
};

export default NoSupportArticles;
