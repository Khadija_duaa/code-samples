

import React, { Component, Fragment } from "react";

const RepeatIcon = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="13.65" height="16" viewBox="0 0 13.65 16">
      <g id="Icon_Repeat" data-name="Icon Repeat" transform="translate(-2.83 -1.252)">
        <path id="Path_41597" data-name="Path 41597" d="M15.922,10.22a.562.562,0,0,1-.558-.558V7.191a1.677,1.677,0,0,0-1.675-1.675H3.388a.558.558,0,0,1,0-1.116h10.3A2.792,2.792,0,0,1,16.48,7.191V9.662A.557.557,0,0,1,15.922,10.22Z" transform="translate(0 -0.805)" fill="#7e7e7e" />
        <path id="Path_41598" data-name="Path 41598" d="M5.74,7.071a.552.552,0,0,1-.394-.164L2.994,4.555a.557.557,0,0,1,0-.789L5.346,1.414a.558.558,0,1,1,.789.789L4.177,4.161,6.135,6.118a.562.562,0,0,1,0,.789A.585.585,0,0,1,5.74,7.071Z" fill="#7e7e7e" />
        <path id="Path_41599" data-name="Path 41599" d="M15.922,17.59H5.621A2.792,2.792,0,0,1,2.83,14.8V12.328a.558.558,0,0,1,1.116,0V14.8a1.677,1.677,0,0,0,1.675,1.675h10.3a.558.558,0,1,1,0,1.116Z" transform="translate(0 -2.69)" fill="#7e7e7e" />
        <path id="Path_41600" data-name="Path 41600" d="M17.068,20.751a.552.552,0,0,1-.394-.164.562.562,0,0,1,0-.789l1.957-1.957-1.957-1.957a.558.558,0,0,1,.789-.789l2.352,2.352a.557.557,0,0,1,0,.789l-2.352,2.352A.528.528,0,0,1,17.068,20.751Z" transform="translate(-3.499 -3.498)" fill="#7e7e7e" />
      </g>
    </svg>
  );
};

export default RepeatIcon;