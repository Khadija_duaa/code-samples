{
  /* <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="50" viewBox="0 0 50 50"><defs><style>.a{fill:#fff;}.b{fill:#646464;}.c{filter:url(#a);}</style><filter id="a" x="0" y="0" width="50" height="50" filterUnits="userSpaceOnUse"><feOffset dy="3" input="SourceAlpha"/><feGaussianBlur stdDeviation="3" result="b"/><feFlood flood-opacity="0.102"/><feComposite operator="in" in2="b"/><feComposite in="SourceGraphic"/></filter></defs><g transform="translate(-516 -154)"><g class="c" transform="matrix(1, 0, 0, 1, 516, 154)"><circle class="a" cx="16" cy="16" r="16" transform="translate(9 6)"/></g></g></svg> */
}

import React, { Component, Fragment } from "react";

const IconSaveAsTemplate = () => {
  return (
    <Fragment>
      <path
        // style={{ fill: "#646464" }}
        d="M820.129-233.524,820-245.964a1.92,1.92,0,0,1,.51-1.382,1.918,1.918,0,0,1,1.34-.612l8.138-.043a1.927,1.927,0,0,1,1.892,1.96L832-233.6a.849.849,0,0,1-.415.738.85.85,0,0,1-.849,0l-4.837-2.681-4.488,2.74a1.006,1.006,0,0,1-.433.128A.848.848,0,0,1,820.129-233.524Zm9.877-12.813-8.146.076c-.067,0-.161.111-.161.28l.1,10.946,3.631-2.232a.908.908,0,0,1,.433-.119.941.941,0,0,1,.424.1l4,2.215-.1-10.989c0-.164-.1-.281-.171-.281Zm-6.081,7.28a.339.339,0,0,1-.134-.335l.34-1.915-1.4-1.361a.34.34,0,0,1-.085-.34.341.341,0,0,1,.276-.231l1.938-.283.854-1.745a.342.342,0,0,1,.306-.192.34.34,0,0,1,.306.192l.864,1.742,1.939.283a.34.34,0,0,1,.276.231.342.342,0,0,1-.085.341l-1.4,1.36.34,1.915a.341.341,0,0,1-.136.34.342.342,0,0,1-.211.062.345.345,0,0,1-.157-.037l-1.735-.909-1.734.909a.34.34,0,0,1-.158.039A.341.341,0,0,1,823.924-239.058Z"
        transform="translate(-820 248)"
      />
    </Fragment>
  );
};

export default IconSaveAsTemplate;
