import React, { Component, Fragment } from "react";

const TickIcon = () => {
  return (
    <Fragment>
        <path d="M700.7,233.913a1.777,1.777,0,0,0-2.513,0L676.89,255.206l-8.184-8.185a1.777,1.777,0,1,0-2.513,2.514l9.441,9.44a1.776,1.776,0,0,0,2.513,0L700.7,236.427A1.778,1.778,0,0,0,700.7,233.913Z" transform="translate(-665.688 -233.406)"/>

    </Fragment>
  );
};

export default TickIcon;
