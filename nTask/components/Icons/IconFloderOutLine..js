import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';
const IconFloderOutLine = ({ color = "#fff" }) => {
  return (
    <>
      <SvgIcon  xmlns="http://www.w3.org/2000/svg" width="16" height="14.4" viewBox="0 0 16 14.4">
        <path id="Icon_feather-folder" data-name="Icon feather-folder" d="M16.269,18.25H4.431A2.075,2.075,0,0,1,2.35,16.186V5.914A2.075,2.075,0,0,1,4.431,3.85h3.7a.6.6,0,0,1,.5.265l1.3,1.936h6.337A2.075,2.075,0,0,1,18.35,8.115v8.071A2.075,2.075,0,0,1,16.269,18.25ZM4.431,5.042a.876.876,0,0,0-.879.871V16.186a.876.876,0,0,0,.879.871H16.269a.876.876,0,0,0,.879-.871V8.115a.876.876,0,0,0-.879-.871H9.61a.6.6,0,0,1-.5-.265l-1.3-1.936Z" transform="translate(-2.35 -3.85)" fill={color} />
      </SvgIcon>

    </>


  )
}
export default IconFloderOutLine;
