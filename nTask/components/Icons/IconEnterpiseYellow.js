import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconEnterpiseYellow = ({ color = "#fa0", style }) => {
  return (
    <SvgIcon style={{ ...style }} viewBox="0 0 16 14.219" width="16" height="14.219">
      <path
        id="icon_Enterpise_Yellow"
        d="M1093.08,695.448l7.035,8.859-3.19-8.859Zm4.469,0,3.4,9.44,3.466-9.44Zm4.155,9.088,7.21-9.088h-3.872Zm4.464-13.867h-4.549l3.12,3.893Zm-1.963,4.181-3.205-4-3.205,4.006h6.411Zm-3.824-4.181h-4.55l1.408,3.922Zm-5.04.378-2.341,3.8h3.707Zm13.659,3.8-2.341-3.807-1.4,3.807Z"
        transform="translate(-1093 -690.669)"
        style={{ fill: color }}
      />
    </SvgIcon>
  );
};

export default IconEnterpiseYellow;
