import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconCheckCircle = props => {
  const { svgProps } = props;
  return (
    <SvgIcon
      xmlns="http://www.w3.org/2000/svg"
      width="17"
      height="17.006"
      viewBox="0 0 17 17.006"
      {...svgProps}>
      <g id="Icon-check-circle" transform="translate(-2 -1.988)">
        <path
          id="Path_570"
          data-name="Path 570"
          d="M10.5,18.994A8.5,8.5,0,1,1,13.959,2.727a1,1,0,0,1-.814,1.827A6.5,6.5,0,1,0,17,10.495V9.805a1,1,0,0,1,2,0v.69a8.513,8.513,0,0,1-8.5,8.5Z"
          transform="translate(0)"
          style={{ fill: "#fff" }}
        />
        <path
          id="Path_571"
          data-name="Path 571"
          d="M15.75,14.507a1,1,0,0,1-.707-.293l-2.25-2.25a1,1,0,0,1,1.414-1.414l1.543,1.543,6.793-6.8a1,1,0,0,1,1.415,1.414l-7.5,7.507A1,1,0,0,1,15.75,14.507Z"
          transform="translate(-5.25 -1.505)"
          style={{ fill: "#fff" }}
        />
      </g>
    </SvgIcon>
  );
};

export default IconCheckCircle;
