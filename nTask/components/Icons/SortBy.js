import React, { Component, Fragment } from "react";

const SortBy = () => {
  return (
    <Fragment>
        <g transform="translate(-1035 -84)">
          <rect style={{opacity:0}} width="24" height="24" transform="translate(1035 84)"/>
          <path style={{}} d="M-979.611-2292.8v-1.171H-972v1.171Zm-6.39-2.635.828-.828,1.171,1.17v-8.3l-1.171,1.171-.828-.828,2.585-2.584,2.584,2.584-.828.828-1.17-1.171v8.3l1.17-1.17.828.828-2.584,2.584Zm6.39-.292v-1.171h5.269v1.171Zm0-2.927v-1.171H-972v1.171Zm0-2.927v-1.171h5.269v1.171Zm0-2.927v-1.171H-972v1.171Z" transform="translate(2026 2395.684)"/>
        </g>      
        {/* <g transform="translate(-91.78 -375.78)">
          <circle style={{fill: '#dcffe0', stroke: '#6cd576'}} cx="11.72" cy="11.72" r="11.72" transform="translate(92.28 376.28)"/>
          <path style={{fill: '#38cc46', stroke: '#38cc46', strokeLinejoin: 'round'}} d="M108.864,385.2a.525.525,0,0,0-.707,0l-5.994,5.612-2.3-2.157a.525.525,0,0,0-.708,0,.447.447,0,0,0,0,.662l2.658,2.488a.525.525,0,0,0,.708,0l6.348-5.943A.448.448,0,0,0,108.864,385.2Z"/>
        </g> */}
    </Fragment>
  );
};

export default SortBy;