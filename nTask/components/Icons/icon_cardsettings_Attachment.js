import SvgIcon from "@material-ui/core/SvgIcon";
import React from "react";

export const IconAttachment = (props) => {
  return (
    <SvgIcon
      width="13"
      height="18.778"
      viewBox="0 0 13 18.778"
      {...props}
      fill={props.fill ?? "#6f7374"}
    >
      <path
        d="M13.676,21.028A4.579,4.579,0,0,1,9.1,16.454V8.75a2.648,2.648,0,0,1,5.3,0v7.7a.722.722,0,1,1-1.444,0V8.75a1.2,1.2,0,0,0-2.407,0v7.7a3.13,3.13,0,1,0,6.259,0V8.75a5.056,5.056,0,1,0-10.111,0v5.778a.722.722,0,1,1-1.444,0V8.75a6.5,6.5,0,1,1,13,0v7.7A4.579,4.579,0,0,1,13.676,21.028Z"
        transform="translate(-5.25 -2.25)"
        fill="currentColor"
      />
    </SvgIcon>
  );
};
