import React, { Component, Fragment } from 'react';

const ReportingIcon = () => {
    return (
        <Fragment>
            <g transform="translate(-431 -455)">
                <rect style={{fill: '#fa0'}} width="20" height="20" rx="4" transform="translate(431 455)"/>
                <g transform="translate(434 458)">
                    <path style={{fill: '#fff'}} d="M6.93,53.561V48.128H6.182a6.182,6.182,0,1,0,6.182,6.182v-.749Z" transform="translate(0 -46.491)"/>
                    <path style={{fill: '#fff'}} d="M235.453,5.346A6.187,6.187,0,0,0,230.229.122L229.376,0V6.2h6.2Z" transform="translate(-221.575)"/>
                </g>
            </g>
        </Fragment>
    )
}

export default ReportingIcon