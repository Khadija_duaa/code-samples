import React, { Component, Fragment } from 'react';

const BulkActionsIcon = () => {
    return (
        <Fragment>
            <g transform="translate(-431 -505)">
                <rect style={{fill: '#fa0'}} width="20" height="20" rx="4" transform="translate(431 505)"/>
                <g transform="translate(434.636 507.727)">
                    <g transform="translate(0 0)">
                        <path style={{fill: '#fff'}} d="M34.253,3.144l-4.5-2.8a2.723,2.723,0,0,0-2.634,0l-4.5,2.8a.89.89,0,0,0,0,1.641l4.5,2.8a2.723,2.723,0,0,0,2.634,0l4.5-2.8A.89.89,0,0,0,34.253,3.144ZM29.114,6.268a1.368,1.368,0,0,1-1.343,0l-3.626-2.3,3.626-2.294a1.372,1.372,0,0,1,1.343,0l3.626,2.294Z" transform="translate(-22.072 0)"/>
                    </g>
                    <g transform="translate(0 6.342)">
                        <g transform="translate(0)">
                            <path style={{fill: '#fff'}} d="M34.252,170.736l-.094-.059-4.73,2.984h0l-.314.2a1.368,1.368,0,0,1-1.343,0l-3.626-2.3h0l-1.417-.893-.112.07a.89.89,0,0,0,0,1.641l4.5,2.8a2.723,2.723,0,0,0,2.634,0l4.5-2.8A.89.89,0,0,0,34.252,170.736Z" transform="translate(-22.071 -170.666)"/>
                        </g>
                    </g>
                    <g transform="translate(0 9.514)">
                        <path style={{fill: '#fff'}} d="M34.253,256.07l-.094-.059L29.43,259h0l-.314.2a1.368,1.368,0,0,1-1.343,0l-3.626-2.3h0L22.729,256l-.112.07a.89.89,0,0,0,0,1.641l4.5,2.8a2.723,2.723,0,0,0,2.634,0l4.5-2.8A.89.89,0,0,0,34.253,256.07Z" transform="translate(-22.072 -256)"/>
                    </g>
                </g>
            </g>
        </Fragment>
    )
}

export default BulkActionsIcon