import React, { Fragment } from "react";
// viewBox="0 0 80 69.8"
const MSProjectIcon = props => {
  return (
    <Fragment>
      <path fill="#185C37" d="M76.6,69.8H31.1c-1.9,0-3.4-1.5-3.4-3.4V46.5l17.5-8.7l21.8,8.7h9.7c1.9,0,3.4,1.5,3.4,3.4v16.4   C80,68.3,78.5,69.8,76.6,69.8z" />
      <path fill="#33C481" d="M53.8,23.3l-20.4,7.3l-23.2-7.3V3.4c0-1.9,1.5-3.4,3.4-3.4h36.8c1.9,0,3.4,1.5,3.4,3.4L53.8,23.3L53.8,23.3z" />
      <path fill="#21A366" d="M10.2,23.3h53.2c1.9,0,3.4,1.5,3.4,3.4v19.9H13.6c-1.9,0-3.4-1.5-3.4-3.4V23.3z" />
      <path style={{ opacity: "0.1", enableBackground: 'new' }} d="M27.6,59.6h11.1c1.7,0,3.1-1.1,3.3-2.8V18c0-1.9-1.5-3.4-3.3-3.4H10.2v28.5c0,1.9,1.5,3.4,3.4,3.4h14V59.6z" />
      <path style={{ opacity: "0.2", enableBackground: 'new' }} d="M38.7,58.2c2.7,0,4.8-2.2,4.8-4.8V18c0-2.7-2.2-4.8-4.8-4.8H10.2v30.1c0,1.9,1.5,3.4,3.4,3.4h14v11.6H38.7z" />
      <path style={{ opacity: "0.2", enableBackground: 'new' }} d="M38.7,56.7c1.9,0,3.4-1.5,3.4-3.3V18c0-1.9-1.5-3.4-3.3-3.4H10.2v28.5c0,1.9,1.5,3.4,3.4,3.4h14v10.2H38.7z" />
      <path style={{ opacity: "0.1", enableBackground: 'new' }} d="M37.3,56.7c1.9,0,3.4-1.5,3.4-3.3V18c0-1.9-1.5-3.4-3.3-3.4H10.2v28.5c0,1.9,1.5,3.4,3.4,3.4h14v10.2H37.3z" />

      <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-53.8394" y1="-1989.2258" x2="-48.2824" y2="-1998.8513" gradientTransform="matrix(4.7771 0 0 -4.7771 264.2847 -9490.8535)">
        <stop offset="0" style={{ stopColor: "#18884F" }} />
        <stop offset="0.5" style={{ stopColor: "#117E43" }} />
        <stop offset="1" style={{ stopColor: "#0B6631" }} />
      </linearGradient>
      <path fill="url(#SVGID_1_)" d="M3.4,14.5h33.9c1.9,0,3.4,1.5,3.4,3.4v33.9c0,1.9-1.5,3.4-3.4,3.4H3.4c-1.9,0-3.4-1.5-3.4-3.4V18   C0,16.1,1.5,14.5,3.4,14.5z" />
      <path fill="#FFFFFF" d="M21,23.6c2.2-0.1,4.3,0.5,6,1.8c1.4,1.4,2.2,3.3,2,5.2c0,1.4-0.4,2.7-1,3.9c-0.8,1.1-1.8,2-2.9,2.7   c-1.4,0.6-2.9,1-4.5,0.9h-4.2v8h-4.3V23.6H21z M16.6,34.6h3.7c1.1,0.1,2.3-0.3,3.3-1c0.8-0.8,1.1-1.8,1.1-2.9   c0-2.5-1.4-3.7-4.2-3.7h-3.8L16.6,34.6z" />

    </Fragment>
  );
};

export default MSProjectIcon;
