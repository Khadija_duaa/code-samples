import React, { Fragment } from 'react';

const ReplyThinIcon = () => {
    return (
        <Fragment>
            <g id="iconReplyThin" transform="translate(1.105 0.908)">
                <path id="Path_177" data-name="Path 177" d="M225.427,377.2s-1.351,5.483-9.7,4.009" transform="translate(-215.427 -377.196)" stroke-linecap="round" stroke-miterlimit="10" stroke-width="1.5"/>
                <path id="Path_178" data-name="Path 178" d="M215.589,389.449l-2.916,3.017,3.13,2.563" transform="translate(-212.674 -388.232)" stroke-linecap="round" stroke-miterlimit="10" stroke-width="1.5"/>
            </g>
	    </Fragment>
    )
}

export default ReplyThinIcon