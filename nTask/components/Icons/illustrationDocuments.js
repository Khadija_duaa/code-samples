// viewBox="0 0 112 84.802"
import React, { Component, Fragment } from "react";

const illustrationDocuments = () => {
  return (
    <Fragment>
      <g id="illustration_Documents" transform="translate(-71.484 -168.15)">
        <path
          id="Path_179"
          data-name="Path 179"
          d="M123.66,217.932h0A3.782,3.782,0,0,0,119.974,215h-27.5a3.782,3.782,0,0,0-3.782,3.782v61.132a3.782,3.782,0,0,0,3.782,3.782h87.321a3.782,3.782,0,0,0,3.782-3.782V228.562a3.782,3.782,0,0,0-3.782-3.782H132.265a8.829,8.829,0,0,1-8.605-6.85Z"
          transform="translate(-11.291 -30.744)"
          style={{ fill: "#8d8d8d" }}
        />
        <path
          id="Path_180"
          data-name="Path 180"
          d="M123.66,217.932h0A3.782,3.782,0,0,0,119.974,215h-27.5a3.782,3.782,0,0,0-3.782,3.782v61.132a3.782,3.782,0,0,0,3.782,3.782h87.321a3.782,3.782,0,0,0,3.782-3.782V228.562a3.782,3.782,0,0,0-3.782-3.782H132.265a8.829,8.829,0,0,1-8.605-6.85Z"
          transform="translate(-11.291 -30.744)"
          style={{ fill: "#7b7b7b", opacity: "0.6" }}
        />
        <path
          id="Path_181"
          data-name="Path 181"
          d="M162.17,168.15l2.451,7.938a3.991,3.991,0,0,0,4.988,2.633l7.935-2.451Z"
          transform="translate(-59.51)"
          style={{ fill: "#407bff", opacity: "0.22" }}
        />
        <path
          id="Path_182"
          data-name="Path 182"
          d="M102.66,168.15,74.3,176.913a3.985,3.985,0,0,0-2.637,4.985l16.192,52.42a3.988,3.988,0,0,0,4.988,2.633l36.3-11.214a3.985,3.985,0,0,0,2.633-4.985L118.024,176.27Z"
          style={{ fill: "#d1d1d1" }}
        />
        <path
          id="Path_183"
          data-name="Path 183"
          d="M102.66,168.15,74.3,176.913a3.985,3.985,0,0,0-2.637,4.985l16.192,52.42a3.988,3.988,0,0,0,4.988,2.633l36.3-11.214a3.985,3.985,0,0,0,2.633-4.985L118.024,176.27Z"
          style={{ fill: "#d6d6d6" }}
        />
        <g id="Group_13" data-name="Group 13" transform="translate(92.068 220.043)">
          <path
            id="Path_184"
            data-name="Path 184"
            d="M157.212,322.5l-23.6,7.288a1.746,1.746,0,0,1-2.18-1.148h0a1.746,1.746,0,0,1,1.152-2.176l23.6-7.292a1.746,1.746,0,0,1,2.18,1.152h0A1.746,1.746,0,0,1,157.212,322.5Z"
            transform="translate(-131.358 -319.096)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_14" data-name="Group 14" transform="translate(77.79 177.268)">
          <path
            id="Path_185"
            data-name="Path 185"
            d="M104.563,198.078l-12.479,3.854a1.75,1.75,0,0,1-2.18-1.148h0a1.75,1.75,0,0,1,1.152-2.18l12.479-3.854a1.743,1.743,0,0,1,2.18,1.148h0a1.75,1.75,0,0,1-1.152,2.18Z"
            transform="translate(-89.828 -194.672)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_15" data-name="Group 15" transform="translate(79.835 181.356)">
          <path
            id="Path_186"
            data-name="Path 186"
            d="M118.649,209.97l-20.627,6.37a1.746,1.746,0,0,1-2.169-1.152h0A1.743,1.743,0,0,1,97,213.013l20.627-6.37a1.746,1.746,0,0,1,2.18,1.148h0a1.753,1.753,0,0,1-1.159,2.18Z"
            transform="translate(-95.775 -206.565)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_16" data-name="Group 16" transform="translate(81.871 185.032)">
          <path
            id="Path_187"
            data-name="Path 187"
            d="M134.052,220.662l-30.1,9.282a1.743,1.743,0,0,1-2.176-1.148h0a1.75,1.75,0,0,1,1.148-2.18l30.1-9.282a1.746,1.746,0,0,1,2.176,1.148h0A1.746,1.746,0,0,1,134.052,220.662Z"
            transform="translate(-101.697 -217.258)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_17" data-name="Group 17" transform="translate(96.513 191.634)">
          <path
            id="Path_188"
            data-name="Path 188"
            d="M164.039,239.864l-17.5,5.408a1.746,1.746,0,0,1-2.176-1.152h0a1.746,1.746,0,0,1,1.148-2.176l17.5-5.408a1.753,1.753,0,0,1,2.18,1.152h0a1.746,1.746,0,0,1-1.152,2.176Z"
            transform="translate(-144.288 -236.461)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_18" data-name="Group 18" transform="translate(83.909 199.519)">
          <path
            id="Path_189"
            data-name="Path 189"
            d="M114.458,262.8l-4.576,1.413a1.746,1.746,0,0,1-2.18-1.148h0a1.746,1.746,0,0,1,1.152-2.18l4.576-1.413a1.75,1.75,0,0,1,2.18,1.148h0A1.753,1.753,0,0,1,114.458,262.8Z"
            transform="translate(-107.626 -259.397)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_19" data-name="Group 19" transform="translate(85.951 198.237)">
          <path
            id="Path_190"
            data-name="Path 190"
            d="M145.92,259.072l-30.1,9.3a1.746,1.746,0,0,1-2.176-1.152h0a1.743,1.743,0,0,1,1.148-2.176l30.1-9.3a1.746,1.746,0,0,1,2.176,1.148h0A1.746,1.746,0,0,1,145.92,259.072Z"
            transform="translate(-113.565 -255.668)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_20" data-name="Group 20" transform="translate(87.99 208.427)">
          <path
            id="Path_191"
            data-name="Path 191"
            d="M140.235,288.712l-18.485,5.71a1.746,1.746,0,0,1-2.176-1.148h0a1.75,1.75,0,0,1,1.148-2.18l18.485-5.71a1.75,1.75,0,0,1,2.18,1.152h0A1.743,1.743,0,0,1,140.235,288.712Z"
            transform="translate(-119.497 -285.308)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_21" data-name="Group 21" transform="translate(113.155 204.841)">
          <path
            id="Path_192"
            data-name="Path 192"
            d="M199.888,278.28l-4.933,1.523a1.75,1.75,0,0,1-2.18-1.152h0a1.746,1.746,0,0,1,1.152-2.176l4.933-1.523a1.743,1.743,0,0,1,2.176,1.148h0A1.746,1.746,0,0,1,199.888,278.28Z"
            transform="translate(-192.698 -274.875)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_22" data-name="Group 22" transform="translate(90.032 211.433)">
          <path
            id="Path_193"
            data-name="Path 193"
            d="M157.788,297.471l-30.095,9.282a1.746,1.746,0,0,1-2.18-1.152h0a1.743,1.743,0,0,1,1.152-2.176l30.1-9.3a1.753,1.753,0,0,1,2.18,1.152h0a1.747,1.747,0,0,1-1.155,2.193Z"
            transform="translate(-125.436 -294.051)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <path
          id="Path_197"
          data-name="Path 197"
          d="M193.357,179.23l-29.627,1.829a3.988,3.988,0,0,0-3.744,4.225l3.379,54.758a3.988,3.988,0,0,0,4.232,3.74l37.92-2.341a3.981,3.981,0,0,0,3.733-4.225L206.4,190.747Z"
          transform="translate(-58.071 -7.271)"
          style={{ fill: "#bfbfbf" }}
        />
        <path
          id="Path_198"
          data-name="Path 198"
          d="M193.357,179.23l-29.627,1.829a3.988,3.988,0,0,0-3.744,4.225l3.379,54.758a3.988,3.988,0,0,0,4.232,3.74l37.92-2.341a3.981,3.981,0,0,0,3.733-4.225L206.4,190.747Z"
          transform="translate(-58.071 -7.271)"
          style={{ fill: "#d6d6d6", opacity: "0.8" }}
        />
        <g id="Group_23" data-name="Group 23" transform="translate(110.578 225.813)">
          <path
            id="Path_199"
            data-name="Path 199"
            d="M211.7,339.36l-24.653,1.523a1.746,1.746,0,0,1-1.843-1.633h0a1.75,1.75,0,0,1,1.63-1.846l24.653-1.52a1.746,1.746,0,0,1,1.846,1.63h0A1.746,1.746,0,0,1,211.7,339.36Z"
            transform="translate(-185.202 -335.881)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_24" data-name="Group 24" transform="translate(107.598 178.254)">
          <path
            id="Path_200"
            data-name="Path 200"
            d="M191.42,201.027l-13.04.8a1.75,1.75,0,0,1-1.846-1.63h0a1.75,1.75,0,0,1,1.633-1.846l13.04-.8a1.743,1.743,0,0,1,1.843,1.63h0a1.746,1.746,0,0,1-1.63,1.853Z"
            transform="translate(-176.532 -197.541)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_25" data-name="Group 25" transform="translate(108.024 184.635)">
          <path
            id="Path_201"
            data-name="Path 201"
            d="M201.165,219.579l-21.545,1.327a1.743,1.743,0,0,1-1.846-1.63h0a1.746,1.746,0,0,1,1.633-1.846l21.542-1.327a1.743,1.743,0,0,1,1.846,1.63h0a1.746,1.746,0,0,1-1.63,1.846Z"
            transform="translate(-177.771 -216.1)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_26" data-name="Group 26" transform="translate(108.443 190.919)">
          <path
            id="Path_202"
            data-name="Path 202"
            d="M212.29,237.864,180.847,239.8a1.75,1.75,0,0,1-1.853-1.633h0a1.743,1.743,0,0,1,1.63-1.843l31.446-1.942a1.746,1.746,0,0,1,1.843,1.633h0A1.746,1.746,0,0,1,212.29,237.864Z"
            transform="translate(-178.991 -234.382)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_27" data-name="Group 27" transform="translate(122.04 197.819)">
          <path
            id="Path_203"
            data-name="Path 203"
            d="M238.669,257.929l-18.279,1.128a1.746,1.746,0,0,1-1.846-1.63h0a1.743,1.743,0,0,1,1.63-1.846l18.279-1.128a1.743,1.743,0,0,1,1.846,1.63h0A1.746,1.746,0,0,1,238.669,257.929Z"
            transform="translate(-218.54 -254.45)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_28" data-name="Group 28" transform="translate(108.876 199.463)">
          <path
            id="Path_204"
            data-name="Path 204"
            d="M186.879,262.71l-4.782.3a1.743,1.743,0,0,1-1.843-1.63h0a1.746,1.746,0,0,1,1.63-1.846l4.782-.3a1.75,1.75,0,0,1,1.846,1.633h0a1.746,1.746,0,0,1-1.633,1.843Z"
            transform="translate(-180.251 -259.232)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_29" data-name="Group 29" transform="translate(109.299 204.715)">
          <path
            id="Path_205"
            data-name="Path 205"
            d="M214.777,277.99l-31.446,1.939a1.746,1.746,0,0,1-1.846-1.63h0a1.75,1.75,0,0,1,1.633-1.846l31.443-1.939a1.746,1.746,0,0,1,1.846,1.633h0A1.743,1.743,0,0,1,214.777,277.99Z"
            transform="translate(-181.482 -274.511)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_30" data-name="Group 30" transform="translate(109.726 212.361)">
          <path
            id="Path_206"
            data-name="Path 206"
            d="M203.881,300.23l-19.31,1.193a1.75,1.75,0,0,1-1.846-1.633h0a1.746,1.746,0,0,1,1.633-1.843l19.31-1.193a1.746,1.746,0,0,1,1.843,1.633h0A1.74,1.74,0,0,1,203.881,300.23Z"
            transform="translate(-182.722 -296.752)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_31" data-name="Group 31" transform="translate(136.014 211.615)">
          <path
            id="Path_207"
            data-name="Path 207"
            d="M266.193,298.059l-5.157.316a1.743,1.743,0,0,1-1.843-1.63h0a1.743,1.743,0,0,1,1.63-1.846l5.157-.316a1.743,1.743,0,0,1,1.846,1.63h0A1.75,1.75,0,0,1,266.193,298.059Z"
            transform="translate(-259.19 -294.58)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <g id="Group_32" data-name="Group 32" transform="translate(110.152 218.511)">
          <path
            id="Path_208"
            data-name="Path 208"
            d="M217.253,318.12l-31.443,1.939a1.746,1.746,0,0,1-1.846-1.629h0a1.746,1.746,0,0,1,1.63-1.846l31.446-1.939a1.743,1.743,0,0,1,1.843,1.629h0A1.743,1.743,0,0,1,217.253,318.12Z"
            transform="translate(-183.961 -314.641)"
            style={{ fill: "#bfbfbf" }}
          />
        </g>
        <path
          id="Path_209"
          data-name="Path 209"
          d="M257.07,179.23l.512,8.292a3.988,3.988,0,0,0,4.225,3.737l8.3-.512Z"
          transform="translate(-121.785 -7.271)"
          style={{ fill: "#bfbfbf" }}
        />
        <path
          id="Path_210"
          data-name="Path 210"
          d="M140.879,233.8h0a2.974,2.974,0,0,0-3.094-2.932H110.248a4.808,4.808,0,0,0-4.517,3.782L94.864,290.323a3.025,3.025,0,0,0,3.042,3.782h87.322a4.82,4.82,0,0,0,4.517-3.782l8.962-45.9a3.024,3.024,0,0,0-3.042-3.782H148.15A6.945,6.945,0,0,1,140.879,233.8Z"
          transform="translate(-15.295 -41.153)"
          style={{ fill: "#bfbfbf", opacity: "0.6" }}
        />
      </g>
    </Fragment>
  );
};

export default illustrationDocuments;
