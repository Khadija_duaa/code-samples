import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconLogout = ({ color = "#a5b4be", style }) => {
  return (
    <SvgIcon style={{ ...style }} viewBox="0 0 16 16">
      <g id="iconLogout" transform="translate(0 -0.004)">
        <path
          id="Path_470"
          data-name="Path 470"
          d="M10,8.67a.666.666,0,0,0-.667.667V12a.667.667,0,0,1-.667.667h-2v-10A1.344,1.344,0,0,0,5.759,1.4l-.2-.066H8.666A.667.667,0,0,1,9.333,2V4a.667.667,0,0,0,1.333,0V2a2,2,0,0,0-2-2H1.5a.524.524,0,0,0-.071.015C1.4.016,1.366,0,1.333,0A1.335,1.335,0,0,0,0,1.337v12A1.344,1.344,0,0,0,.908,14.6L4.92,15.942A1.38,1.38,0,0,0,5.333,16,1.335,1.335,0,0,0,6.667,14.67V14h2a2,2,0,0,0,2-2V9.337A.666.666,0,0,0,10,8.67Zm0,0"
          transform="translate(0 0)"
        />
        <path
          id="Path_471"
          data-name="Path 471"
          d="M284.47,109.528l-2.667-2.667a.666.666,0,0,0-1.138.471v2H278a.667.667,0,1,0,0,1.333h2.667v2a.666.666,0,0,0,1.138.471l2.667-2.667a.666.666,0,0,0,0-.943Zm0,0"
          transform="translate(-268.666 -103.329)"
        />
      </g>
    </SvgIcon>
  );
};

export default IconLogout;
