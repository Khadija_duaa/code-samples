
import React from "react";

const IconAddWorkspace = () => {

  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="19.5" height="19.5" viewBox="0 0 19.5 19.5">
    <g id="iconAddTrigger" transform="translate(-2.25 -2.249)">
      <path id="Ellipse_153" data-name="Ellipse 153" d="M9-.75A9.75,9.75,0,1,1-.75,9,9.761,9.761,0,0,1,9-.75Zm0,18A8.25,8.25,0,1,0,.75,9,8.259,8.259,0,0,0,9,17.25Z" transform="translate(3 2.999)" fill="#00abed"/>
      <path id="Union_21" data-name="Union 21" d="M-6748.2-11255.45v-2.85h-2.85a.951.951,0,0,1-.949-.949.951.951,0,0,1,.949-.952h2.85v-2.848a.952.952,0,0,1,.949-.953.953.953,0,0,1,.952.953v2.848h2.851a.951.951,0,0,1,.949.952.951.951,0,0,1-.949.949h-2.851v2.85a.951.951,0,0,1-.952.949A.951.951,0,0,1-6748.2-11255.45Z" transform="translate(6759.25 11271.251)" fill="#00abed"/>
    </g>
  </svg>
  );
};

export default IconAddWorkspace;
