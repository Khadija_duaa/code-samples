import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const GroupByIcon = ({color = '#969696', style}) => {
  return (
   <SvgIcon style={style} viewBox="0 0 16 11.96">
     <g transform="translate(-12 -17.413)">
       <rect style={{fill: color}} width="1.783" height="1.673" transform="translate(12 17.413)"/>
       <rect style={{fill: color}} width="12.455" height="1.673" transform="translate(15.544 17.413)"/>
       <rect style={{fill: color}} width="1.783" height="1.673" transform="translate(16.016 22.642)"/>
       <rect style={{fill: color}} width="8.44" height="1.673" transform="translate(19.56 22.642)"/>
       <rect style={{fill: color}} width="1.783" height="1.673" transform="translate(16.016 27.7)"/>
       <rect style={{fill: color}} width="8.44" height="1.673" transform="translate(19.56 27.7)"/>
     </g>
   </SvgIcon>
  )
}

export default GroupByIcon;
