import React, { Component, Fragment } from 'react';

const CustomStatusesIcon = () => {
    return (
        <Fragment>
            <g transform="translate(-200 -18)">
                <rect class="a" width="22" height="22" rx="4" transform="translate(200 18)"/>
                <path class="b" d="M0,6.847A7.158,7.158,0,0,1,4.963.045a.943.943,0,0,1,1.231.9.986.986,0,0,1-.677.935,5.233,5.233,0,1,0,6.613,6.58.94.94,0,0,1,.895-.652.965.965,0,0,1,.917,1.265A7.148,7.148,0,0,1,0,6.847Zm13.041-.957a.961.961,0,0,1-.913-.662A5.254,5.254,0,0,0,8.761,1.868.941.941,0,0,1,8.111.974V.952a.95.95,0,0,1,1.243-.9,7.168,7.168,0,0,1,4.6,4.612.939.939,0,0,1-.895,1.228Z" transform="translate(0.001 -0.001)"/>
            </g>
        </Fragment>
    )
}

export default CustomStatusesIcon