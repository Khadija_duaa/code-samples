// viewBox="0 0 15 15"
import React from "react";
const IconSurvey = ({ color = "#20cc90" }) => {
  return (
    <>
      <path
        id="iconSurveys"
        d="M13.333,0H1.667A1.666,1.666,0,0,0,0,1.667V13.333A1.666,1.666,0,0,0,1.667,15H13.333A1.666,1.666,0,0,0,15,13.333V1.667A1.666,1.666,0,0,0,13.333,0ZM5,11.667H3.333V5.833H5Zm3.333,0H6.667V3.333H8.333v8.333Zm3.333,0H10V8.333h1.667Z"
        // fill="#a5b4be"
      />
    </>
  );
};
export default IconSurvey;
