import React, { Component, Fragment } from "react";

const CreateFolderIcon = () => {
  return (
    <Fragment>
    <path d="M18.042,6.754H11.625L9.214,3.839A.917.917,0,0,0,8.508,3.5H4.292A2.264,2.264,0,0,0,2,5.727V16.856a2.264,2.264,0,0,0,2.292,2.227h13.75a2.264,2.264,0,0,0,2.292-2.227V8.982a2.264,2.264,0,0,0-2.292-2.227ZM13,14.042h-.917v.917a.917.917,0,1,1-1.833,0v-.917H9.333a.917.917,0,0,1,0-1.833h.917v-.917a.917.917,0,1,1,1.833,0v.917H13a.917.917,0,1,1,0,1.833Z" transform="translate(-0.167 -0.292)"/>
    </Fragment>
  );
};

export default CreateFolderIcon;
