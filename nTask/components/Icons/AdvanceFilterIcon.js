import React, { Component, Fragment } from 'react';

const AdvanceFilterIcon = () => {
    return (
        <Fragment>
            <g transform="translate(-431 -505)">
                <rect style={{fill: '#fa0'}} width="20" height="20" rx="4" transform="translate(431 505)"/>
                <path style={{fill: '#fff'}} d="M1278.457,91.775a.424.424,0,0,0-.418-.277h-9.078a.424.424,0,0,0-.418.277.411.411,0,0,0,.1.5l3.5,3.5v3.452a.437.437,0,0,0,.135.32l1.816,1.818a.421.421,0,0,0,.319.135.493.493,0,0,0,.177-.036.425.425,0,0,0,.277-.419V95.775l3.5-3.5A.41.41,0,0,0,1278.457,91.775Z" transform="translate(-832.499 418.502)"/>
            </g>
        </Fragment>
    )
}

export default AdvanceFilterIcon