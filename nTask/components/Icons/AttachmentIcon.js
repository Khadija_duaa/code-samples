import React, { Component, Fragment } from 'react';

const AttachmentIcon = () => {
    return (
        <Fragment>
                <g transform="translate(-431 -395)">
                    <rect style={{fill: '#fa0'}} width="20" height="20" rx="4" transform="translate(431 395)"/>
                    <path style={{fill: '#fff'}} d="M4.545,14.545A4.55,4.55,0,0,1,0,10V3.636a.606.606,0,0,1,1.212,0V10a3.333,3.333,0,0,0,6.667,0V3.333a2.121,2.121,0,0,0-4.242,0V9.394a.909.909,0,0,0,1.818,0V3.636a.606.606,0,0,1,1.212,0V9.394a2.121,2.121,0,0,1-4.243,0V3.333a3.333,3.333,0,0,1,6.667,0V10a4.55,4.55,0,0,1-4.545,4.545Zm0,0" transform="translate(436.85 397.934)"/>
                </g>
        </Fragment>
    )
}

export default AttachmentIcon