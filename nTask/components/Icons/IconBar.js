{
  /* <svg xmlns="http://www.w3.org/2000/svg" width="140" height="140.117" viewBox="0 0 140 140.117"><</svg> */
}

import React, { Component, Fragment } from "react";

const IconBar = () => {
  return (
    <Fragment>
      <g transform="translate(-0.213)">
        <path
          style={{ fill: "#bfbfbf" }}
          d="M32.323,234.667H3.132a2.92,2.92,0,0,0-2.919,2.919v70.058a2.92,2.92,0,0,0,2.919,2.919H32.323a2.92,2.92,0,0,0,2.919-2.919V237.586A2.917,2.917,0,0,0,32.323,234.667Z"
          transform="translate(0 -170.447)"
        />
        <path
          style={{ fill: "#bfbfbf" }}
          d="M223.9,0H194.705a2.92,2.92,0,0,0-2.919,2.919V137.2a2.92,2.92,0,0,0,2.919,2.919H223.9a2.92,2.92,0,0,0,2.919-2.919V2.919A2.92,2.92,0,0,0,223.9,0Z"
          transform="translate(-139.146)"
        />
        <path
          style={{ fill: "#bfbfbf" }}
          d="M415.9,106.667H386.705a2.92,2.92,0,0,0-2.919,2.919V214.674a2.92,2.92,0,0,0,2.919,2.919H415.9a2.92,2.92,0,0,0,2.919-2.919V109.586A2.92,2.92,0,0,0,415.9,106.667Z"
          transform="translate(-278.603 -77.476)"
        />
      </g>
    </Fragment>
  );
};

export default IconBar;
