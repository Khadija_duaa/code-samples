import React, { Component, Fragment } from 'react';

const SaveIcon = () => {
    return (
        <Fragment>
            <path id="Path_2" data-name="Path 2" d="M11,0A11,11,0,1,0,22,11,11.012,11.012,0,0,0,11,0Zm6.133,7.331-6.769,7.615a.847.847,0,0,1-1.161.1L4.971,11.661a.846.846,0,0,1,1.057-1.322l3.6,2.882,6.236-7.015a.846.846,0,0,1,1.265,1.125Z" fill="#7cb147"/>
        </Fragment>
    )
}

export default SaveIcon