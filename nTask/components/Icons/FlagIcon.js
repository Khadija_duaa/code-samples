import React, { Component, Fragment } from "react";

const FlagIcon = () => {
  return (
    <Fragment>
       <path data-name="Icon Flag" d="M625.679,200.959l-22.589-10.49V189.18a0.546,0.546,0,0,0-1.091,0v31.639a0.546,0.546,0,1,0,1.091,0v-9.993l22.559-8.865A0.545,0.545,0,0,0,625.679,200.959Z" transform="translate(-602 -188.625)"/>

    </Fragment>
  );
};

export default FlagIcon;
