import React, { Component, Fragment } from "react";

const IconCaret = () => {
  return (
    <Fragment>
      <path
        class="a"
        d="M910.29,354.874a.6.6,0,0,0-.44-.186h-8.755a.621.621,0,0,0-.44,1.064l4.378,4.374a.614.614,0,0,0,.88,0l4.378-4.374a.612.612,0,0,0,0-.879Z"
        transform="translate(-900.469 -354.688)"
      />
    </Fragment>
  );
};

export default IconCaret;
