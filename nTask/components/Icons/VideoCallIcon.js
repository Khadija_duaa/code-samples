import React from "react";

const VideoCallIcon = color => {
  return (
    <>
      <path
        className="a"
        d="M17.2,6.72a1.36,1.36,0,0,0-1.48.24L14,8.56V7.4A2.4,2.4,0,0,0,11.6,5H4.4A2.4,2.4,0,0,0,2,7.4v6.4a2.4,2.4,0,0,0,2.4,2.4h7.2A2.4,2.4,0,0,0,14,13.8v-1.16l1.728,1.6a1.392,1.392,0,0,0,.928.36,1.344,1.344,0,0,0,.552-.12,1.28,1.28,0,0,0,.8-1.184V7.9A1.28,1.28,0,0,0,17.2,6.72Z"
        transform="translate(-2 -5)"
      />
    </>
  );
};

VideoCallIcon.defaultProps = {
  color: "#ffffff",
};
export default VideoCallIcon;
