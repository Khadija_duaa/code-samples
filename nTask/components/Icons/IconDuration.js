// 0 0 16 18
import React from "react";
const IconDuration = ({ color = "#20cc90" }) => {
  return (
    <>
      <g transform="translate(-48 -48)">
        <path d="M57,48a9,9,0,1,1-9,9A9.01,9.01,0,0,1,57,48Zm0,16.615A7.615,7.615,0,1,0,49.385,57,7.624,7.624,0,0,0,57,64.615Z" fill={color} />
        <path d="M244.846,119.615h-4.154a.692.692,0,0,1-.692-.692v-6.231a.692.692,0,0,1,1.385,0v5.538h3.462a.692.692,0,0,1,0,1.385Z" transform="translate(-183.692 -61.231)" fill={color} />
      </g>
    </>
  );
};
export default IconDuration;
