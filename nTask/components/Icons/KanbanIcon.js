import React, { Component, Fragment } from 'react';

const KanbanIcon = () => {
    return (
        <Fragment>
                <g transform="translate(-431 -455)">
                    <rect style={{fill: '#fa0'}} width="20" height="20" rx="4" transform="translate(431 455)"/>
                    <path style={{fill: '#fff', fillRule: 'evenodd'}} d="M207.933,4201.5h0a1.436,1.436,0,0,1,1.432,1.437v7.126a1.435,1.435,0,0,1-1.432,1.436h0a1.435,1.435,0,0,1-1.433-1.436v-7.126A1.436,1.436,0,0,1,207.933,4201.5Zm3.552,0h.029a1.4,1.4,0,0,1,1.405,1.408v3.118a1.4,1.4,0,0,1-1.405,1.409h-.029a1.4,1.4,0,0,1-1.406-1.409v-3.118a1.4,1.4,0,0,1,1.406-1.408Zm2.677,0h1.83a.5.5,0,0,1,.508.5v6.981a.507.507,0,0,1-.508.5h-1.83a.507.507,0,0,1-.508-.5V4202a.5.5,0,0,1,.508-.5Z" transform="translate(229.5 -3741.5)"/>
                </g>
        </Fragment>
    )
}

export default KanbanIcon