import SvgIcon from "@material-ui/core/SvgIcon";
import React from "react";

export const IconRejected = props => {
  const { color = "#c33d3d" } = props;
  return (
    <svg width="18" height="17.995" viewBox="0 0 18 17.995" fill={color}>
      <path
        d="M10.994,19.99a8.994,8.994,0,1,1,6.363-2.632A9,9,0,0,1,10.994,19.99ZM3.8,11.15a7.2,7.2,0,1,0,0-.155Zm4.866,3.443L7.4,13.325l2.33-2.33L7.4,8.665,8.665,7.4l2.33,2.33,2.33-2.33,1.268,1.268-2.33,2.33,2.33,2.33-1.267,1.268-2.331-2.33-2.33,2.33Z"
        transform="translate(-1.994 -1.996)"
      />
    </svg>
  );
};
