import React, { Component, Fragment } from "react";

const ShowColumnsIcon = () => {
  return (
    <Fragment>
      <path
        id="Rounded_Rectangle_1203"
        data-name="Rounded Rectangle 1203"
        style={{
          fill: "#f6f6f6",
          stroke: "#eaeaea",
          strokeLinejoin: "round",
          strokeWidth: 1
        }}
        d="M0.5,0.5h12a4,4,0,0,1,4,4v43a4,4,0,0,1-4,4H0.5a0,0,0,0,1,0,0V0.5A0,0,0,0,1,.5.5Z"
      />
      <text
        id="Hide"
        style={{
          fontSize: "10.377px",
          fill: "#969696"
        }}
        transform="translate(12.511 31.5) rotate(-90) scale(1.001 0.964)"
      >
        Show
      </text>
      <path
        id="icon_arrow"
        data-name="icon arrow"
        style={{
          fill: "#b4b4b4",
          fillRule: "evenodd"
        }}
        d="M989.161,486.857a0.459,0.459,0,0,1-.166-0.352v-7a0.459,0.459,0,0,1,.166-0.351,0.588,0.588,0,0,1,.782,0l3.9,3.5a0.456,0.456,0,0,1,0,.7l-3.9,3.5A0.588,0.588,0,0,1,989.161,486.857Z"
        transform="translate(-982.5 -440.5)"
      />
    </Fragment>
  );
};

export default ShowColumnsIcon;
