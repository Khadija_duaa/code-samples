//     viewBox="0 0 21.333 26.667"

import React, { Component, Fragment } from "react";

const IconFile = () => {
  return (
    <Fragment>
      <g transform="translate(-5.333 -2.666)">
        <path
          d="M24.987,9.107,19.067,2.44A1.333,1.333,0,0,0,18.08,2H7.413A3.373,3.373,0,0,0,4,5.333v20a3.373,3.373,0,0,0,3.413,3.333H21.92a3.373,3.373,0,0,0,3.413-3.333V10a1.333,1.333,0,0,0-.347-.893Zm-14.32,6.227h4a1.333,1.333,0,0,1,0,2.667h-4a1.333,1.333,0,1,1,0-2.667Zm8,8h-8a1.333,1.333,0,1,1,0-2.667h8a1.333,1.333,0,1,1,0,2.667ZM18.28,10a1.053,1.053,0,0,1-.947-1.133v-4.2L22.32,10Z"
          transform="translate(1.333 0.667)"
        />
      </g>
    </Fragment>
  );
};

export default IconFile;
