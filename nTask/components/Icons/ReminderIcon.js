import React, { Component, Fragment } from "react";

const ReminderIcon = () => {
  return (
    <Fragment>
      <path
        style={{fill: "#444", fillRule: "evenodd"}}
        d="M1195.56,278.971l-3.02-2.534-0.85.975,3.02,2.534Zm-9.25-1.494-0.85-.975-3.02,2.469,0.85,0.975Zm3.02,2.989h-0.99v3.9l3.09,1.885,0.52-.78-2.62-1.559v-3.444Zm-0.33-2.6a5.848,5.848,0,1,0,5.91,5.848A5.857,5.857,0,0,0,1189,277.867Zm0,10.4a4.549,4.549,0,1,1,4.59-4.548A4.539,4.539,0,0,1,1189,288.263Z"
        transform="translate(-1182.44 -276.438)"
      />
    </Fragment>
  )
}

export default ReminderIcon;
