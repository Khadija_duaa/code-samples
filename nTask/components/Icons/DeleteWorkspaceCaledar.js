


import React from "react";

export const DeleteWorkspaceCaledar = () => {

  return (
<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18.001" viewBox="0 0 18 18.001">
  <g id="iconDelete" transform="translate(-2 -1.999)">
    <path id="Path_41610" data-name="Path 41610" d="M19.1,5.6H14.6V4.1A2.178,2.178,0,0,0,12.35,2H9.65A2.178,2.178,0,0,0,7.4,4.1V5.6H2.9a.9.9,0,0,0,0,1.8h.9v9.9A2.7,2.7,0,0,0,6.5,20h9a2.7,2.7,0,0,0,2.7-2.7V7.4h.9a.9.9,0,1,0,0-1.8ZM9.2,4.1c0-.144.189-.3.45-.3h2.7c.261,0,.45.153.45.3V5.6H9.2Zm7.2,13.2a.9.9,0,0,1-.9.9h-9a.9.9,0,0,1-.9-.9V7.4H16.4Z" fill="#ef5350"/>
    <path id="Path_41611" data-name="Path 41611" d="M8.9,16.4a.9.9,0,0,0,.9-.9V11.9a.9.9,0,0,0-1.8,0v3.6A.9.9,0,0,0,8.9,16.4Z" transform="translate(-0.6 -0.9)" fill="#ef5350"/>
    <path id="Path_41612" data-name="Path 41612" d="M14.9,16.4a.9.9,0,0,0,.9-.9V11.9a.9.9,0,0,0-1.8,0v3.6A.9.9,0,0,0,14.9,16.4Z" transform="translate(-1.2 -0.9)" fill="#ef5350"/>
  </g>
</svg>
  );
};

export default DeleteWorkspaceCaledar;