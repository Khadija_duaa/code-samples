// viewBox="0 0 18 15.246"
import React from "react";

const IconFolder = () => {
  return (
    <>
      <g id="iconFolderYellow" transform="translate(-16.027 -64)">
        <path
          id="Path_3163"
          data-name="Path 3163"
          d="M61.235,65.27H56.1a.948.948,0,0,1-.528-.159l-1.1-.738A2.214,2.214,0,0,0,53.237,64H50.223A2.226,2.226,0,0,0,48,66.223v.953H63.458A1.976,1.976,0,0,0,61.235,65.27Z"
          transform="translate(-30.704 0)"
          style={{ fill: "#e5bb00" }}
        />
        <path
          id="Path_3164"
          data-name="Path 3164"
          d="M31.156,186.8H18.894a2.223,2.223,0,0,1-2.221-2.19l-.641-6.541v-.011a1.906,1.906,0,0,1,1.9-2.058H32.123a1.906,1.906,0,0,1,1.9,2.058v.011l-.644,6.541A2.223,2.223,0,0,1,31.156,186.8Zm2.229-8.792Z"
          transform="translate(0 -107.553)"
          style={{ fill: "#ffd000" }}
        />
      </g>
    </>
  );
};

export default IconFolder;
