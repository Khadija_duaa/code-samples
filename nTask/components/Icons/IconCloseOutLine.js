import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';
const IconCloseOutLine = ({ color = "#fff",style }) => {
    return (
        <>
            <SvgIcon  style={style} xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12">
                <path id="iconClose" d="M19.523,8.723l-1.2-1.2-4.8,4.8-4.8-4.8-1.2,1.2,4.8,4.8-4.8,4.8,1.2,1.2,4.8-4.8,4.8,4.8,1.2-1.2-4.8-4.8Z" transform="translate(-7.523 -7.523)" fill={color} />
            </SvgIcon>
        </>

    )
}
export default IconCloseOutLine;
