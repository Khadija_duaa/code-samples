
import React from "react";


const IconArrowForward = (props) => {
  const { style } = props

  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="14.286" viewBox="0 0 10 14.286" style={style}>
      <g id="iconArrowForward" transform="translate(0 14.286) rotate(-90)">
        <path id="Path_8304" data-name="Path 8304" d="M14.033,0H.253C.113,0,0,.339,0,.758s.113.758.253.758H14.033c.139,0,.253-.339.253-.758S14.173,0,14.033,0Z" transform="translate(0 4.242)" fill="#646464" />
        <path id="Path_8305" data-name="Path 8305" d="M.758,0A.758.758,0,0,0,.222,1.293L3.929,5,.222,8.707A.758.758,0,0,0,1.293,9.778L5.536,5.536a.758.758,0,0,0,0-1.071L1.293.222A.755.755,0,0,0,.758,0Z" transform="translate(8.528)" fill="#646464" />
      </g>
    </svg>
  );
};

export default IconArrowForward;