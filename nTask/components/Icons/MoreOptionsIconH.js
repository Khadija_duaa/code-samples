import React, { Component, Fragment } from 'react';

const MoreOptionsIconH = () => {
    return (
        <Fragment>
            <g transform="translate(-3 -10)">
                <path d="M10.889,1.555a1.555,1.555,0,1,1,1.555,1.557A1.555,1.555,0,0,1,10.889,1.555Zm-5.444,0A1.555,1.555,0,1,1,7,3.112,1.555,1.555,0,0,1,5.445,1.555ZM0,1.555A1.556,1.556,0,1,1,1.555,3.112,1.555,1.555,0,0,1,0,1.555Z" transform="translate(3 10)"/>
            </g>        
        </Fragment>
    )
}

export default MoreOptionsIconH