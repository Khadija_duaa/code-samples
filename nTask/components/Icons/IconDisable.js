import React from 'react'

function IconDisable() {
    return (
        <>
            <svg width="14" height="14" viewBox="0 0 14 14">
                <path id="iconDisable" d="M9,2a7,7,0,1,0,7,7A7,7,0,0,0,9,2Zm5.6,7a5.544,5.544,0,0,1-1.183,3.43L5.57,4.583A5.544,5.544,0,0,1,9,3.4,5.6,5.6,0,0,1,14.6,9ZM3.4,9A5.544,5.544,0,0,1,4.583,5.57l7.847,7.847A5.544,5.544,0,0,1,9,14.6,5.6,5.6,0,0,1,3.4,9Z" transform="translate(-2 -2)" fill="#969696" />
            </svg>

        </>
    )
}

export default IconDisable
