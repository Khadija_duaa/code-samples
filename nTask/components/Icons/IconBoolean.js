import React from 'react';
const IconBoolean = ({ color = "#fea148" }) => {
    return (
        <g transform="translate(-1.25 -3.25)">
            {/* <path d="M13.11,19.529H9.39a8.14,8.14,0,1,1,0-16.279H13.11a8.14,8.14,0,1,1,0,16.279ZM9.39,4.645a6.744,6.744,0,1,0,0,13.488H13.11a6.744,6.744,0,1,0,0-13.488Z" transform="translate(0)" style={{ fill: color }} />
            <path d="M9.669,16.087a4.419,4.419,0,1,1,4.419-4.419A4.423,4.423,0,0,1,9.669,16.087Zm0-7.442a3.023,3.023,0,1,0,3.023,3.023A3.029,3.029,0,0,0,9.669,8.645Z" transform="translate(-0.279 -0.279)" style={{ fill: color }} /> */}
            <path d="M13.11,19.529H9.39a8.14,8.14,0,1,1,0-16.279H13.11a8.14,8.14,0,1,1,0,16.279ZM9.39,4.645a6.744,6.744,0,1,0,0,13.488H13.11a6.744,6.744,0,1,0,0-13.488Z" transform="translate(0)" style={{ fill: color, stroke: color }} stroke-width="0.2" />
            <path d="M9.669,16.087a4.419,4.419,0,1,1,4.419-4.419A4.423,4.423,0,0,1,9.669,16.087Zm0-7.442a3.023,3.023,0,1,0,3.023,3.023A3.029,3.029,0,0,0,9.669,8.645Z" transform="translate(-0.279 -0.279)" style={{ fill: color, stroke: color }} stroke-width="0.2" />
        </g>
    )
}
export default IconBoolean;
