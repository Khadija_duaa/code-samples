import SvgIcon from "@material-ui/core/SvgIcon";
import React from "react";

export const IconToDoList = (props) => {
  return (
    <SvgIcon width="18" height="18" viewBox="0 0 18 18" {...props} fill={props.fill ?? "#6f7374"}>
      <g transform="translate(-2.25 -2.25)">
        <path
          d="M8.25-.75a9,9,0,1,1-9,9A9.01,9.01,0,0,1,8.25-.75Zm0,16.615A7.615,7.615,0,1,0,.635,8.25,7.624,7.624,0,0,0,8.25,15.865Z"
          transform="translate(3 3)"
          fill="currentColor"
        />
        <path
          d="M10.788,14.327a.69.69,0,0,1-.49-.2L8.453,12.278a.692.692,0,1,1,.979-.979l1.357,1.357,3.2-3.2a.692.692,0,1,1,.979.979l-3.692,3.692A.69.69,0,0,1,10.788,14.327Z"
          transform="translate(-0.462 -0.538)"
          fill="currentColor"
        />
      </g>
    </SvgIcon>
  );
};
