import React, { Component, Fragment } from "react";

const HideColumnsIcon = () => {
  return (
    <Fragment>
      <path
        id="Rounded_Rectangle_1203"
        data-name="Rounded Rectangle 1203"
        style={{
          fill: "#f6f6f6",
          stroke: "#eaeaea",
          strokeLinejoin: "round",
          strokeWidth: 1
        }}
        d="M0.5,0.5h12a4,4,0,0,1,4,4v40a4,4,0,0,1-4,4H0.5a0,0,0,0,1,0,0V0.5A0,0,0,0,1,.5.5Z"
      />
      <text
        id="Hide"
        style={{
			fontSize: "10.377px",
        fill: "#969696"
		}}
        transform="translate(12.511 31.5) rotate(-90) scale(1.001 0.964)"
      >
        Hide
      </text>
      <path
        id="icon_arrow"
        data-name="icon arrow"
        style={{
			fill: "#b4b4b4",
        fillRule: "evenodd"
		}}
        d="M993.135,486.357a0.478,0.478,0,0,0,.15-0.352v-7a0.478,0.478,0,0,0-.15-0.351,0.5,0.5,0,0,0-.715,0l-3.555,3.5a0.488,0.488,0,0,0,0,.7l3.555,3.5A0.5,0.5,0,0,0,993.135,486.357Z"
        transform="translate(-982.5 -443.5)"
      />
    </Fragment>
  );
};

export default HideColumnsIcon;
