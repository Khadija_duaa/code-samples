import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';
const IconPoriorityOutLine = ({ color = "#fff" }) => {
  return (
    <>
      <SvgIcon xmlns="http://www.w3.org/2000/svg" width="12" height="14.485" viewBox="0 0 12 14.485">
        <path id="iconPriorityWhite" d="M20.891,6.711v6.5l8-3.248-8-3.248M20.044,5c.014,0,.026,0,.039,0l.035,0a.453.453,0,0,1,.084.022l.012,0,.005,0h0l11.087,4.5a.453.453,0,0,1,0,.84L20.5,14.77v4.262a.453.453,0,1,1-.907,0V5.453a.451.451,0,0,1,.013-.1c0-.01.006-.019.009-.028a.453.453,0,0,1,.026-.068c0-.01.01-.02.016-.03a.444.444,0,0,1,.04-.057c.007-.008.013-.017.021-.025a.457.457,0,0,1,.076-.066h0a.446.446,0,0,1,.089-.045l.03-.009A.542.542,0,0,1,20.044,5Z" transform="translate(-19.591 -5)" fill={color} />
      </SvgIcon>

    </>
  )
}
export default IconPoriorityOutLine;
