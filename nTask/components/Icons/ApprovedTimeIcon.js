import React, { Component, Fragment } from "react";

const ApprovedTimeIcon = () => {
  return (
    <Fragment>
      <path
        class="a"
        d="M7,0a7,7,0,1,0,7,7A7.008,7.008,0,0,0,7,0Zm3.912,5.158L6.439,9.6a.688.688,0,0,1-.965.018L3.105,7.456a.712.712,0,0,1-.053-.982.694.694,0,0,1,.982-.035L5.912,8.158l4-4a.707.707,0,0,1,1,1Z"
      />
    </Fragment>
  );
};

export default ApprovedTimeIcon;