import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';
const IconExportOutLine = ({ color = "#fff"}) => {
  return (
    <>
      <SvgIcon  xmlns="http://www.w3.org/2000/svg" width="16" height="16.2" viewBox="0 0 16 16.2">
        <g id="iconExport" transform="translate(-2 -3)">
          <path id="Union_19" data-name="Union 19" d="M-822-3764.8a2,2,0,0,1-2-2V-3779a2,2,0,0,1,2-2h12a2,2,0,0,1,2,2v12.2a2,2,0,0,1-2,2Zm-1.2-14.2v12.2a1.2,1.2,0,0,0,1.2,1.2h12a1.2,1.2,0,0,0,1.2-1.2V-3779a1.2,1.2,0,0,0-1.2-1.2h-12A1.2,1.2,0,0,0-823.2-3779Zm5.2,11.8h-3.2a.4.4,0,0,1-.4-.4v-9.6a.4.4,0,0,1,.4-.4h10.4a.4.4,0,0,1,.4.4v9.6a.4.4,0,0,1-.4.4Zm6.8-.8v-1.6h-6.4v1.6Zm-9.6,0h2.4v-1.6h-2.4Zm9.6-2.4v-1.6h-6.4v1.6Zm-7.2,0v-1.6h-2.4v1.6Zm7.2-2.4v-1.6h-6.4v1.6Zm-7.2,0v-1.6h-2.4v1.6Zm7.2-2.4v-1.6h-6.4v1.6Zm-7.2,0v-1.6h-2.4v1.6Z" transform="translate(826 3784)" fill={color} />
        </g>
      </SvgIcon>

    </>




  )
}
export default IconExportOutLine;
