import React, { Component, Fragment } from "react";

const AssigneeIcon = ({ color }) => {
  return (
    <Fragment>
      <rect style={{ fill: color, opacity: 0 }} width="26.805" height="24" />
      <path
        style={{ fill: color }}
        d="M11.994,15.208a4.759,4.759,0,0,0-5-4.475A4.757,4.757,0,0,0,2,15.208a.951.951,0,0,1-1,.9.953.953,0,0,1-1-.9c0-3.46,3.136-6.262,7-6.262s7,2.8,7,6.262a1.006,1.006,0,0,1-2,0ZM3,3.574A3.8,3.8,0,0,1,7,0a3.8,3.8,0,0,1,4,3.574A3.8,3.8,0,0,1,7,7.154,3.8,3.8,0,0,1,3,3.574Zm2,0A1.9,1.9,0,0,0,7,5.366,1.9,1.9,0,0,0,9,3.574,1.9,1.9,0,0,0,7,1.787,1.9,1.9,0,0,0,5,3.574Zm9.993,1.792v-.9h-1a.951.951,0,0,1-1-.9.951.951,0,0,1,1-.891h1v-.9a1.006,1.006,0,0,1,2,0v.9h1a.949.949,0,0,1,1,.891.949.949,0,0,1-1,.9h-1v.9a1.006,1.006,0,0,1-2,0Z"
        transform="translate(4.465 4.003)"
      />
    </Fragment>
  );
};

AssigneeIcon.defaultProps = {
  color: "",
};

export default AssigneeIcon;
