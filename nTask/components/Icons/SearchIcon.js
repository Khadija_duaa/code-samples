import React, { Component, Fragment } from 'react';

const SearchIcon = () => {
    return (
        <Fragment>
  <path d="M663.884,393.223l-18-18.812a30.485,30.485,0,1,0-23.366,10.958,30.1,30.1,0,0,0,17.49-5.552l18.133,18.955a3.969,3.969,0,0,0,5.629.111A4.022,4.022,0,0,0,663.884,393.223Zm-41.362-61.218a22.68,22.68,0,1,1-22.565,22.68A22.648,22.648,0,0,1,622.522,332.005Z" transform="translate(-592 -324)"/>
        </Fragment>
    )
}

export default SearchIcon