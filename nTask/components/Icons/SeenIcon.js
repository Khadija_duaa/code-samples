import React, { Component, Fragment } from 'react';

const SeenIcon = () => {
    return (
        <Fragment>
			<path style={{fill: '#bfbfbf'}} d="M866.482,245.627a.3.3,0,0,1,0-.255,7.7,7.7,0,0,1,13.962,0,.3.3,0,0,1,0,.255,7.695,7.695,0,0,1-13.962,0Zm3.821-.126a3.159,3.159,0,1,0,3.159-3.163A3.16,3.16,0,0,0,870.3,245.5Zm1.137,0a2.02,2.02,0,1,1,2.02,2.02A2.02,2.02,0,0,1,871.44,245.5Z" transform="translate(-866.454 -240.916)"/>
         </Fragment>
    )
}

export default SeenIcon