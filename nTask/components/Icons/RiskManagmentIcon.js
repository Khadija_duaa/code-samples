import React, { Component, Fragment } from 'react';

const RiskManagmentIcon = () => {
    return (
        <Fragment>
            <g transform="translate(-431 -398)">
                <rect style={{fill:'#fa0'}} width="20" height="20" rx="4" transform="translate(431 398)"/>
                <path style={{fill:'#fff', fillRule: 'evenodd'}} d="M1018.785,100.13l-4.641-7.844a1.375,1.375,0,0,0-2.354,0l-4.641,7.844a1.312,1.312,0,0,0,0,1.327,1.356,1.356,0,0,0,1.18.667h9.275a1.356,1.356,0,0,0,1.18-.667,1.283,1.283,0,0,0,0-1.327Zm-6.654-5.4a.84.84,0,0,1,1.68,0v2.661a.84.84,0,0,1-1.68,0Zm.84,5.9a.841.841,0,1,1,.86-.84.853.853,0,0,1-.86.84Z" transform="translate(-571.97 310.378)"/>
            </g>
        </Fragment>
    )
}

export default RiskManagmentIcon