import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';
const IconPeopleOutLine = ({ color = "#fff"}) => {
  return (
    <>
      <SvgIcon xmlns="http://www.w3.org/2000/svg" width="14" height="12.6" viewBox="0 0 14 12.6">
        <path id="iconPeople" d="M8.4,11.9a3.5,3.5,0,1,0-7,0,.7.7,0,0,1-1.4,0A4.9,4.9,0,0,1,8.356,8.435,3.5,3.5,0,0,1,14,11.2a.7.7,0,0,1-1.4,0A2.1,2.1,0,0,0,9.2,9.555,4.844,4.844,0,0,1,9.8,11.9a.7.7,0,1,1-1.4,0Zm0-7A2.1,2.1,0,1,1,10.5,7,2.1,2.1,0,0,1,8.4,4.9Zm1.4,0a.7.7,0,1,0,.7-.7A.7.7,0,0,0,9.8,4.9ZM2.1,2.8A2.8,2.8,0,1,1,4.9,5.6,2.8,2.8,0,0,1,2.1,2.8Zm1.4,0A1.4,1.4,0,1,0,4.9,1.4,1.4,1.4,0,0,0,3.5,2.8Z" fill={color} />
      </SvgIcon>

    </>
  )
}
export default IconPeopleOutLine;
