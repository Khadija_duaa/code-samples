//  viewBox="0 0 18 15.428"

import React, { Fragment } from "react";

const IconActivity = () => {
  return (
    <Fragment>
      <g id="iconActivity" transform="translate(-1 -2)">
        <path
          id="Path_8041"
          data-name="Path 8041"
          d="M4.214,9.5a.641.641,0,0,1-.5-.242L1.14,6.044a.643.643,0,1,1,1-.8L4.39,8.05l3.393-1.7A.643.643,0,1,1,8.358,7.5L4.5,9.432A.648.648,0,0,1,4.214,9.5Z"
          transform="translate(0 0.857)"
          style={{ fill: "#969696" }}
        />
        <path
          id="Path_8042"
          data-name="Path 8042"
          d="M10.714,17.428a7.666,7.666,0,0,1-6.111-3,.643.643,0,0,1,1.018-.786A6.429,6.429,0,1,0,4.286,9.714.643.643,0,0,1,3,9.714a7.714,7.714,0,1,1,7.714,7.714Z"
          transform="translate(0.571)"
          style={{ fill: "#969696" }}
        />
        <path
          id="Path_8043"
          data-name="Path 8043"
          d="M11.214,12.714a.646.646,0,0,1-.455-.188L8.188,9.955A.647.647,0,0,1,8,9.5V5.643a.643.643,0,1,1,1.286,0V9.234l2.384,2.384a.642.642,0,0,1-.455,1.1Z"
          transform="translate(2 0.857)"
          style={{ fill: "#969696" }}
        />
      </g>
    </Fragment>
  );
};

export default IconActivity;
