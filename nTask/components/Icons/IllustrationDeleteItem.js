
import React from "react";

const IllustrationDeleteItem = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="102" height="102" viewBox="0 0 102 102">
      <g id="illustrationDeleteItem" transform="translate(-618.026 -180)">
        <g id="Ellipse_150" data-name="Ellipse 150" transform="translate(619.026 181)" fill="#ffeceb" stroke="#fe6259" stroke-width="1">
          <circle cx="50" cy="50" r="50" stroke="none" />
          <circle cx="50" cy="50" r="50.5" fill="none" />
        </g>
        <g id="trash" transform="translate(650.78 211.75)">
          <path id="Path_41563" data-name="Path 41563" d="M33.026,8.023h-.131A129.293,129.293,0,0,0,6.962,7.694l-3.348.328A1.239,1.239,0,0,1,2.252,6.906a1.223,1.223,0,0,1,1.1-1.346L6.7,5.232a133.179,133.179,0,0,1,26.441.328,1.232,1.232,0,0,1,1.1,1.346A1.216,1.216,0,0,1,33.026,8.023Z" transform="translate(0 2.222)" fill="#e3352a" />
          <path id="Path_41564" data-name="Path 41564" d="M8.982,8.586a1.01,1.01,0,0,1-.213-.016,1.236,1.236,0,0,1-1-1.411l.361-2.15c.263-1.576.624-3.758,4.448-3.758h4.3c3.841,0,4.2,2.265,4.448,3.775l.361,2.134a1.224,1.224,0,1,1-2.413.41l-.361-2.134c-.23-1.428-.279-1.707-2.019-1.707h-4.3c-1.74,0-1.772.23-2.019,1.691L10.2,7.552A1.232,1.232,0,0,1,8.982,8.586Z" transform="translate(3.529)" fill="#e3352a" />
          <path id="Path_41565" data-name="Path 41565" d="M22.144,31.959H11.607c-5.728,0-5.958-3.168-6.138-5.728L4.4,9.7A1.234,1.234,0,0,1,6.864,9.54L7.93,26.067c.181,2.495.246,3.43,3.676,3.43H22.144c3.447,0,3.512-.936,3.676-3.43L26.887,9.54A1.254,1.254,0,0,1,28.2,8.391,1.232,1.232,0,0,1,29.349,9.7L28.282,26.231C28.1,28.792,27.872,31.959,22.144,31.959Z" transform="translate(1.38 4.578)" fill="#e3352a" />
          <path id="Path_41566" data-name="Path 41566" d="M16.276,18.212H10.811a1.231,1.231,0,1,1,0-2.462h5.465a1.231,1.231,0,0,1,0,2.462Z" transform="translate(4.703 9.298)" fill="#e3352a" />
          <path id="Path_41567" data-name="Path 41567" d="M18.187,14.212H9.981a1.231,1.231,0,1,1,0-2.462h8.206a1.231,1.231,0,0,1,0,2.462Z" transform="translate(4.171 6.733)" fill="#e3352a" />
        </g>
      </g>
    </svg>
  );
};

export default IllustrationDeleteItem;
