import React from "react";

const AuomationRuleicon = () => {
  return (
    <svg
      id="iconRule"
      xmlns="http://www.w3.org/2000/svg"
      width="34"
      height="34"
      viewBox="0 0 34 34">
      <circle id="Circle" cx="17" cy="17" r="17" fill="#00abed" />
      <path
        id="flash"
        d="M14.888,8.722H12.506V3.172c0-1.295-.7-1.557-1.557-.586l-.617.7L5.115,9.223c-.717.809-.416,1.472.663,1.472H8.159v5.55c0,1.295.7,1.557,1.557.586l.617-.7,5.218-5.935C16.268,9.385,15.967,8.722,14.888,8.722Z"
        transform="translate(6.275 7)"
        fill="#fff"
      />
    </svg>
  );
};
export default AuomationRuleicon;
