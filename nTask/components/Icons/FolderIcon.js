import React, { Component, Fragment } from "react";

const FolderIcon = () => {
  return (
    <Fragment>
      <path d="M22.417,23.333H4.917A2.882,2.882,0,0,1,2,20.5V6.335A2.882,2.882,0,0,1,4.917,3.5h5.367a1.167,1.167,0,0,1,.9.432l3.033,3.71h8.167a2.882,2.882,0,0,1,2.952,2.835V20.5A2.882,2.882,0,0,1,22.417,23.333Z" transform="translate(0.333 0.583)"/>
    </Fragment>
  );
};

export default FolderIcon;
