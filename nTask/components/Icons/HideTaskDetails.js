import React, { Component, Fragment } from "react";

const HideTaskDetails = (color) => {
  return (
    <Fragment>
      <g transform="translate(-3.001 -5)">
        <path
          style={{ color: color }}
          d="M-21216.521-1922.739v-1.592h11.52v1.592Zm0-4.77v-1.587h11.52v1.587Zm-6.48-5.561,3.932-3.931,1.121,1.122-2.01,2.013h14.508v1.592h-14.508l2.01,2.013-1.121,1.122Z"
          transform="translate(21226.004 1942)"
        />
      </g>
    </Fragment>
  );
};

export default HideTaskDetails;
