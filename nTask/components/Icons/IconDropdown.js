import React, { Fragment } from "react";

const IconDropdown = ({color = "#ffa148" }) => {
  return (
    <Fragment>
      <g transform="translate(-164 -198)">
        <g
          style={{ fill: "none", strokeWidth: "1.5px", stroke: color }}
          transform="translate(164 198)">
          <circle style={{ stroke: "none" }} cx="9" cy="9" r="9" />
          <circle style={{ fill: "none" }} cx="9" cy="9" r="8.25" />
        </g>
        <path
          style={{ fill: color  }}
          d="M9.48,12a1.115,1.115,0,0,1-.455-.1A.88.88,0,0,1,8.5,11.1V6.894A.88.88,0,0,1,9.025,6.1a1.05,1.05,0,0,1,1.1.13l2.55,2.1a.85.85,0,0,1,0,1.33l-2.55,2.1A1.03,1.03,0,0,1,9.48,12Z"
          transform="translate(181.999 196.5) rotate(90)"
        />
      </g>
    </Fragment>
  );
};

export default IconDropdown;
