import React, { Fragment } from "react";

const ViewIcon = props => {
  return (
    <Fragment>
      <path
        style={{ fill: props.htmlColor, fillRule: "evenodd" }}
        d="M1325,128h-14a2,2,0,0,0-2,2v4h18v-4A2,2,0,0,0,1325,128Zm-16,16a2,2,0,0,0,2,2h8V136h-10v8Zm12,2h4a2,2,0,0,0,2-2v-8h-6v10Z"
        transform="translate(-1309 -128)"
      />
    </Fragment>
  );
};

export default ViewIcon;
