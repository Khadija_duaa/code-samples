//    viewBox="0 0 12 11.957"

import React, { Component, Fragment } from "react";

const IconEditSmall = (color = "#7e7e7e") => {
  return (
    <Fragment>

  <path d="M15.549,6.519,13.494,4.464a1.5,1.5,0,0,0-2-.053l-6.75,6.75a1.5,1.5,0,0,0-.428.908L4,15.2a.748.748,0,0,0,.75.818h.067l3.128-.285a1.5,1.5,0,0,0,.908-.428l6.75-6.75a1.44,1.44,0,0,0-.052-2.033ZM13,9.024l-2.01-2.01,1.463-1.5L14.5,7.562Z" transform="translate(-3.996 -4.058)"></path>
    </Fragment>
  );
};

export default IconEditSmall;
