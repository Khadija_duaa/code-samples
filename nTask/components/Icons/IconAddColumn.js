// viewBox="0 0 16 16"
import React, { Fragment } from "react";

const IconAddColumn = () => {
  return (
    <Fragment>
      <g id="iconAddColumn" transform="translate(-3.375 -3.375)">
        <path
          id="Path_8051"
          data-name="Path 8051"
          d="M18.089,13.992H15.223V11.127a.615.615,0,1,0-1.231,0v2.865H11.127a.589.589,0,0,0-.615.615.6.6,0,0,0,.615.615h2.865v2.865a.6.6,0,0,0,.615.615.612.612,0,0,0,.615-.615V15.223h2.865a.615.615,0,1,0,0-1.231Z"
          transform="translate(-3.233 -3.233)"
          style={{ fill: "#7e7e7e" }}
        />
        <path
          id="Path_8052"
          data-name="Path 8052"
          d="M11.375,4.452a6.92,6.92,0,1,1-4.9,2.027,6.877,6.877,0,0,1,4.9-2.027m0-1.077a8,8,0,1,0,8,8,8,8,0,0,0-8-8Z"
          transform="translate(0 0)"
          style={{ fill: "#7e7e7e" }}
        />
      </g>
    </Fragment>
  );
};

export default IconAddColumn;
