import React, { Component, Fragment } from 'react';
import SvgIcon from "@material-ui/core/SvgIcon";

const SearchIcon = ({ fillColor = "#7f8f9a", className }) => {
    return (
      <SvgIcon id="SearchIconV2" viewBox="0 0 16 16" className={className}>
        <path id="iconSearch" d="M18.737,17.475l-3.021-3.012a7.038,7.038,0,0,0,1.5-4.354,7.109,7.109,0,1,0-7.109,7.109,7.038,7.038,0,0,0,4.354-1.5l3.012,3.021a.892.892,0,1,0,1.262-1.262ZM4.777,10.109a5.332,5.332,0,1,1,5.332,5.332A5.332,5.332,0,0,1,4.777,10.109Z" transform="translate(-3 -3)" fill={fillColor}/>
      </SvgIcon>
    )
}

export default SearchIcon