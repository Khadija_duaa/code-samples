import React from 'react';
const IconTextarea = ({color="#f3669a"}) => {
    return (
        <>
            <g transform="translate(-135 -378)"><g style={{fill: 'none', stroke:color, strokeWidth:1.5}} transform="translate(135 378)">
                <rect style={{stroke:'none'}} width="18" height="18" rx="4" /><rect class="d" x="0.75" y="0.75" width="16.5" height="16.5" rx="3.25" />
            </g>
                <rect style={{fill:color}} width="10" height="2" rx="1" transform="translate(139 382)" />
                <rect style={{fill:color}} width="10" height="2" rx="1" transform="translate(139 386)" />
                <rect style={{fill:color}} width="5" height="2" rx="1" transform="translate(139 390)" />
            </g>
        </>
    )
}
export default IconTextarea;