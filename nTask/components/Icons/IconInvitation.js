import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconInvitation = ({ color = "#a5b4be", style }) => {
  return (
    <SvgIcon style={{ ...style }} viewBox="0 0 16 12.8">
      <path
        id="iconInvitation"
        d="M15.6,4H4.4A2.4,2.4,0,0,0,2,6.4v8a2.4,2.4,0,0,0,2.4,2.4H15.6A2.4,2.4,0,0,0,18,14.4v-8A2.4,2.4,0,0,0,15.6,4Zm0,1.6L10.4,9.176a.8.8,0,0,1-.8,0L4.4,5.6Z"
        transform="translate(-2 -4)"
      />
    </SvgIcon>
  );
};

export default IconInvitation;
