import React from "react";
// viewBox="0 0 16 15.996"
const IconFailed = () => {
  return (
      <path d="M10,17.995a7.99,7.99,0,0,1-8-8v-.16a8,8,0,1,1,8,8.157Zm0-6.87L12.069,13.2,13.2,12.069,11.125,10,13.2,7.926,12.069,6.8,10,8.87,7.926,6.8,6.8,7.926,8.87,10,6.8,12.069,7.926,13.2,10,11.126Z" transform="translate(-2 -1.999)" fill="currentColor"/>
  );
};

export default IconFailed;
