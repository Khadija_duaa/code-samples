import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
function NewTaskIcon({className}) {
    return (
        <SvgIcon  viewBox="0 0 18 20" className={className}>
  <path id="iconTasks" d="M37.333,2H33.148a2.981,2.981,0,0,0-5.63,0H23.333a2,2,0,0,0-2,2V18a2,2,0,0,0,2,2h14a2,2,0,0,0,2-2V4A2,2,0,0,0,37.333,2Zm-7,0a1,1,0,1,1-1,1A1,1,0,0,1,30.333,2Zm2,14h-7V14h7Zm3-4h-10V10h10Zm0-4h-10V6h10Z" transform="translate(-21.333)" fill="#00cc90"/>
</SvgIcon>

    )
}

export default NewTaskIcon
