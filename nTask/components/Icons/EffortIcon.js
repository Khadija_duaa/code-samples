import React, { Component, Fragment } from "react";

const EffortIcon = ({ color = "#ffa148" }) => {
  return (
    <Fragment>
      <g id="iconEffort" transform="translate(-2.5 -1.25)">
        <path
          id="Path_41570"
          data-name="Path 41570"
          d="M9.5,17.75a7,7,0,1,1,7-7A7.006,7.006,0,0,1,9.5,17.75Zm0-12.895a5.895,5.895,0,1,0,5.895,5.895A5.9,5.9,0,0,0,9.5,4.855Z"
          transform="translate(0 -0.658)"
          fill="#7f8f9a"
        />
        <path
          id="Path_41571"
          data-name="Path 41571"
          d="M11.8,12.039a.557.557,0,0,1-.553-.553V7.8a.553.553,0,1,1,1.105,0v3.684A.557.557,0,0,1,11.8,12.039Z"
          transform="translate(-2.303 -1.579)"
          fill="#7f8f9a"
        />
        <path
          id="Path_41572"
          data-name="Path 41572"
          d="M13.224,2.355H8.8a.553.553,0,0,1,0-1.105h4.421a.553.553,0,0,1,0,1.105Z"
          transform="translate(-1.513)"
          fill="#7f8f9a"
        />
      </g>
    </Fragment>
  );
};

export default EffortIcon;
