import React, { Component, Fragment } from 'react';

const ProjectMoveIcon = () => {
    return (
        <Fragment>
         <circle style={{fill: "#f9f9f9",
        stroke: "#f1f1f1",
        strokeLinejoin: "round",
        strokeWidth: 1}} cx="58.5" cy="58.5" r="58"/>
  <path style={{fill: "#c9c9c9", fillRule: "evenodd"}} d="M708,290.188A6.156,6.156,0,0,0,701.875,284H676.261a6.156,6.156,0,0,0-6.125,6.188v30.375a6.156,6.156,0,0,0,6.125,6.187h25.614A6.156,6.156,0,0,0,708,320.563V290.188Zm-10.579,26.437H680.716a1.688,1.688,0,0,1,0-3.375h16.705A1.688,1.688,0,0,1,697.421,316.625Zm0-6.75H680.716a1.688,1.688,0,0,1,0-3.375h16.705A1.688,1.688,0,0,1,697.421,309.875Zm0-6.75H680.716a1.688,1.688,0,0,1,0-3.375h16.705A1.688,1.688,0,0,1,697.421,303.125Z" transform="translate(-624.5 -252.5)"/>
  <path id="Shape_4762_copy" data-name="Shape 4762 copy" style={{fillRule: "evenodd", fill: "#ddd"}} d="M676.261,330.125a9.525,9.525,0,0,1-9.465-9.562V290.188c0-.194.044-0.374,0.055-0.563h-1.726A6.161,6.161,0,0,0,659,295.812v36A6.161,6.161,0,0,0,665.125,338h26.727a6.161,6.161,0,0,0,6.125-6.188v-1.687H676.261Z" transform="translate(-624.5 -252.5)"/>
        </Fragment>
    )
}

export default ProjectMoveIcon