import React, { Component, Fragment } from "react";

const GoogleCalendarIcon = () => {
    return (
        <Fragment>
            <g id="Group_3" data-name="Group 3">
                <g id="Group_2" data-name="Group 2" clip-path="url(#clip-path)">
                    <path id="Path_11" data-name="Path 11" d="M53.77,0H37.411a1.692,1.692,0,0,0-1.692,1.692l.032,10.718H55.463V1.692A1.692,1.692,0,0,0,53.77,0Z" transform="translate(-34.591 0)" fill="#c2c2c2" />
                </g>
            </g>
            <g id="Group_6" data-name="Group 6">
                <g id="Group_5" data-name="Group 5" clip-path="url(#clip-path)">
                    <g id="Group_4" data-name="Group 4" transform="translate(-1.622 2.256)">
                        <path id="Path_13" data-name="Path 13" d="M20.308,107.158H1.692A1.692,1.692,0,0,0,0,108.85a1.772,1.772,0,0,0,.016.231l1.113,7.1H20.872l1.113-7.1A1.8,1.8,0,0,0,22,108.85,1.692,1.692,0,0,0,20.308,107.158Z" transform="translate(1.622 -106.03)" fill="#3367d6" />
                    </g>
                    <path id="Path_14" data-name="Path 14" d="M20.308,107.158H1.692A1.692,1.692,0,0,0,0,108.85a1.772,1.772,0,0,0,.016.231l1.113,7.1H20.872l1.113-7.1A1.8,1.8,0,0,0,22,108.85,1.692,1.692,0,0,0,20.308,107.158Z" transform="translate(0 -103.773)" fill="#2a56c6" opacity="0.5" />
                </g>
            </g>
            <g id="Group_8" data-name="Group 8">
                <g id="Group_7" data-name="Group 7" clip-path="url(#clip-path)">
                    <path id="Path_16" data-name="Path 16" d="M38.728,400.13l-.56-7.216H18.424l-.56,7.216c0,.039,0,.078,0,.117a1.692,1.692,0,0,0,1.692,1.692H37.039a1.692,1.692,0,0,0,1.692-1.692C38.732,400.208,38.73,400.17,38.728,400.13Z" transform="translate(-17.296 -380.504)" fill="#4285f4" />
                </g>
            </g>
            <g id="Group_12" data-name="Group 12">
                <g id="Group_11" data-name="Group 11" clip-path="url(#clip-path)">
                    <path id="Path_20" data-name="Path 20" d="M1.692,107.3H20.308A1.691,1.691,0,0,1,22,108.935c0-.028,0-.056,0-.085a1.692,1.692,0,0,0-1.692-1.692H1.692A1.692,1.692,0,0,0,0,108.85c0,.028,0,.056,0,.085A1.691,1.691,0,0,1,1.692,107.3Z" transform="translate(0 -103.773)" fill="rgba(255,255,255,0.2)" />
                </g>
            </g>
            <g id="Group_14" data-name="Group 14">
                <g id="Group_13" data-name="Group 13" clip-path="url(#clip-path)">
                    <path id="Path_22" data-name="Path 22" d="M37.039,624.048H19.552a1.691,1.691,0,0,1-1.689-1.636c0,.028,0,.056,0,.085a1.692,1.692,0,0,0,1.692,1.692H37.039a1.692,1.692,0,0,0,1.692-1.692c0-.028,0-.056,0-.085A1.691,1.691,0,0,1,37.039,624.048Z" transform="translate(-17.296 -602.753)" fill="rgba(26,35,126,0.2)" />
                </g>
            </g>
            <g id="Group_16" data-name="Group 16">
                <g id="Group_15" data-name="Group 15" clip-path="url(#clip-path)">
                    <path id="Path_24" data-name="Path 24" d="M53.77,0H37.411a1.692,1.692,0,0,0-1.692,1.692v.134A1.693,1.693,0,0,1,37.411.141H53.77a1.692,1.692,0,0,1,1.692,1.692V1.692A1.692,1.692,0,0,0,53.77,0Z" transform="translate(-34.591 0)" fill="rgba(255,255,255,0.2)" />
                </g>
            </g>
            <g id="Group_21" data-name="Group 21">
                <g id="Group_20" data-name="Group 20" clip-path="url(#clip-path)">
                    <g id="Group_19" data-name="Group 19" transform="translate(0 3.385)">
                        <g id="Group_18" data-name="Group 18" clip-path="url(#clip-path-8)">
                            <path id="Path_26" data-name="Path 26" d="M163.752,245.781a2.3,2.3,0,0,0,1.468-2.032,2.52,2.52,0,0,0-2.821-2.641,2.671,2.671,0,0,0-2.87,2.538h1.145a1.586,1.586,0,0,1,1.723-1.6,1.5,1.5,0,0,1,1.674,1.681c0,1.124-.86,1.643-1.909,1.643h-.8v.9h.8c1.543,0,2.05.8,2.05,1.73a1.814,1.814,0,0,1-3.616.166h-1.154a2.979,2.979,0,0,0,5.915-.192A2.18,2.18,0,0,0,163.752,245.781Zm6.546-4.674-3.339,1.2v1.077l2.4-.8V250.7h1.128v-9.59Z" transform="translate(-154.408 -236.876)" fill="#e1e1e1" />
                            <g id="Group_17" data-name="Group 17" transform="translate(5.036 4.231)" opacity="0.5">
                                <path id="Path_27" data-name="Path 27" d="M163.752,245.781a2.3,2.3,0,0,0,1.468-2.032,2.52,2.52,0,0,0-2.821-2.641,2.671,2.671,0,0,0-2.87,2.538h1.145a1.586,1.586,0,0,1,1.723-1.6,1.5,1.5,0,0,1,1.674,1.681c0,1.124-.86,1.643-1.909,1.643h-.8v.9h.8c1.543,0,2.05.8,2.05,1.73a1.814,1.814,0,0,1-3.616.166h-1.154a2.979,2.979,0,0,0,5.915-.192A2.18,2.18,0,0,0,163.752,245.781Zm6.546-4.674-3.339,1.2v1.077l2.4-.8V250.7h1.128v-9.59Z" transform="translate(-159.444 -241.107)" fill="#c2c2c2" />
                            </g>
                        </g>
                    </g>
                </g>
            </g>
            <g id="Group_25" data-name="Group 25">
                <g id="Group_24" data-name="Group 24" clip-path="url(#clip-path)">
                    <g id="Group_23" data-name="Group 23" transform="translate(0.564 12.41)">
                        <g id="Group_22" data-name="Group 22" clip-path="url(#clip-path-10)">
                            <path id="Path_30" data-name="Path 30" d="M163.752,245.781a2.3,2.3,0,0,0,1.468-2.032,2.52,2.52,0,0,0-2.821-2.641,2.671,2.671,0,0,0-2.87,2.538h1.145a1.586,1.586,0,0,1,1.723-1.6,1.5,1.5,0,0,1,1.674,1.681c0,1.124-.86,1.643-1.909,1.643h-.8v.9h.8c1.543,0,2.05.8,2.05,1.73a1.814,1.814,0,0,1-3.616.166h-1.154a2.979,2.979,0,0,0,5.915-.192A2.18,2.18,0,0,0,163.752,245.781Zm6.546-4.674-3.339,1.2v1.077l2.4-.8V250.7h1.128v-9.59Z" transform="translate(-154.972 -245.902)" fill="#eee" />
                        </g>
                    </g>
                </g>
            </g>
            <g id="Group_27" data-name="Group 27">
                <g id="Group_26" data-name="Group 26" clip-path="url(#clip-path)">
                    <path id="Path_33" data-name="Path 33" d="M160.759,36.938l-1.093-1.094,1.2-1.2,2.292,2.29Z" transform="translate(-154.623 -33.554)" fill="url(#linear-gradient)" />
                    <circle id="Ellipse_1" data-name="Ellipse 1" cx="0.846" cy="0.846" r="0.846" transform="translate(4.795 0.846)" fill="#eee" />
                    <path id="Path_34" data-name="Path 34" d="M152.653,26.931a.844.844,0,0,1,.842.776.591.591,0,0,0,0-.07.846.846,0,1,0-1.692,0,.591.591,0,0,0,0,.07A.844.844,0,0,1,152.653,26.931Z" transform="translate(-147.012 -25.944)" fill="rgba(255,255,255,0.4)" />
                    <path id="Path_35" data-name="Path 35" d="M153.5,55.812a.845.845,0,0,1-1.684,0,.591.591,0,0,0,0,.07.846.846,0,1,0,1.692,0A.594.594,0,0,0,153.5,55.812Z" transform="translate(-147.012 -54.049)" fill="rgba(33,33,33,0.1)" />
                </g>
            </g>
            <g id="Group_29" data-name="Group 29">
                <g id="Group_28" data-name="Group 28" clip-path="url(#clip-path)">
                    <rect id="Rectangle_2" data-name="Rectangle 2" width="20.026" height="0.141" transform="translate(0.987 12.41)" fill="rgba(255,255,255,0.05)" />
                    <rect id="Rectangle_3" data-name="Rectangle 3" width="20.026" height="0.141" transform="translate(0.987 12.269)" fill="rgba(26,35,126,0.05)" />
                </g>
            </g>
            <g id="Group_31" data-name="Group 31">
                <g id="Group_30" data-name="Group 30" clip-path="url(#clip-path)">
                    <path id="Path_38" data-name="Path 38" d="M500.094,36.938,499,35.844l1.2-1.2,1.728,2.29Z" transform="translate(-483.24 -33.554)" fill="url(#linear-gradient-2)" />
                    <circle id="Ellipse_2" data-name="Ellipse 2" cx="0.846" cy="0.846" r="0.846" transform="translate(15.513 0.846)" fill="#eee" />
                    <path id="Path_39" data-name="Path 39" d="M491.989,26.931a.844.844,0,0,1,.842.776c0-.024,0-.047,0-.07a.846.846,0,0,0-1.692,0,.594.594,0,0,0,0,.07A.844.844,0,0,1,491.989,26.931Z" transform="translate(-475.63 -25.944)" fill="rgba(255,255,255,0.4)" />
                    <path id="Path_40" data-name="Path 40" d="M492.83,55.812a.845.845,0,0,1-1.684,0c0,.024,0,.047,0,.07a.846.846,0,1,0,1.692,0A.581.581,0,0,0,492.83,55.812Z" transform="translate(-475.629 -54.049)" fill="rgba(33,33,33,0.1)" />
                </g>
            </g>

        </Fragment>
    );
};

export default GoogleCalendarIcon;
