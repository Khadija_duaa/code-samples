import React from "react";
// viewBox="0 0 16 16.002"
const IconCheck = () => {
  return (
    <path d="M10,18a8.01,8.01,0,0,1-8-8v-.16A8,8,0,1,1,10,18ZM6.328,9.673,5.2,10.8,8.4,14l6.4-6.4L13.673,6.464,8.4,11.737Z" transform="translate(-2 -2)" fill="currentColor"/>
  );
};

export default IconCheck;
