import React, { Component, Fragment } from "react";

const IconPermissionAlert = () => {
  return (
    <Fragment>
      <g transform="translate(-643 -322)">
        <circle style={{ fill: "rgba(252,153,23,0.16)" }} cx="40" cy="40" r="40" transform="translate(643 322)" />
        <path
          style={{ fill: "#fc9917" }}
          d="M35.252,40.718,22.535,17.34a5.274,5.274,0,0,0-9.07,0L.749,40.718a5.271,5.271,0,0,0,4.534,7.966H30.717A5.272,5.272,0,0,0,35.252,40.718ZM18,44.465a2.109,2.109,0,1,1,2.109-2.109A2.112,2.112,0,0,1,18,44.465Zm2.109-8.438a2.109,2.109,0,1,1-4.219,0V25.48a2.109,2.109,0,1,1,4.219,0Z"
          transform="translate(665 328.241)"
        />
      </g>
    </Fragment>
  );
};

export default IconPermissionAlert;
