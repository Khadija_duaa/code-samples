import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const AutoSizeIcon = ({ color = "#969696", style }) => {
  return (
    <SvgIcon style={style} viewBox="0 0 18 9.062">
      <path
        style={{ fill: color }}
        d="M16.594,111v4.491L13,112.3,12,113.18l1.907,1.7H4.074l1.907-1.7-.994-.884-3.581,3.183V111H0v9.062H1.406v-4.541L4.987,118.7l.994-.884-1.907-1.7h9.837L12,117.82,13,118.7l3.595-3.2v4.554H18V111Z"
        transform="translate(0 -111)"
      />
    </SvgIcon>
  );
};

export default AutoSizeIcon;
