import SvgIcon from "@material-ui/core/SvgIcon";
import React from "react";

export const IconApproved = props => {
  const { color = "#19ac75" } = props;
  return (
    <svg width="18" height="18.046" viewBox="0 0 18 18.046" color={color}>
      <path
        d="M2.66,15.421A9.024,9.024,0,0,1,8.977,0a9.494,9.494,0,0,1,2.139.253A.9.9,0,1,1,10.691,2a7.959,7.959,0,0,0-1.714-.2A7.219,7.219,0,1,0,16.2,9.023a.9.9,0,0,1,1.8,0,8.99,8.99,0,0,1-15.34,6.4Zm5.676-3.05L5.629,9.664A.906.906,0,0,1,6.911,8.382l2.066,2.03,5.612-6.5a.9.9,0,0,1,1.354,1.191L9.626,12.326a.9.9,0,0,1-.65.306H8.97A.9.9,0,0,1,8.336,12.371Z"
        fill="currentColor"
      />
    </svg>
  );
};
