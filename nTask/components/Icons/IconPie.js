{
  /* <svg xmlns="http://www.w3.org/2000/svg" width="140" height="140.009" viewBox="0 0 140 140.009"><defs><style>.a{fill:#bfbfbf;}</style></defs></svg> */
}

import React, { Component, Fragment } from "react";

const IconPie = () => {
  return (
    <Fragment>
      <g transform="translate(-0.016)">
        <path
          style={{ fill: "#bfbfbf" }}
          d="M199.185,87.106h-4.1v56.783l-33.669,45.64,3.3,2.435A58.1,58.1,0,1,0,199.185,87.106Z"
          transform="translate(-117.263 -63.286)"
        />
        <path
          style={{ fill: "#bfbfbf" }}
          d="M16.769,118.691a77.8,77.8,0,0,0-9.333,12.6,58.09,58.09,0,0,0,5.629,65.117c4,4.909,8.8,8.777,13.866,12.514l36.728-49.787Z"
          transform="translate(0 -86.234)"
        />
        <path
          style={{ fill: "#bfbfbf" }}
          d="M124,0c-8.442,0-16.475.829-24.258,4.4s-13.66,9.115-19.175,15.51L124,57.366Z"
          transform="translate(-58.521)"
        />
      </g>
    </Fragment>
  );
};

export default IconPie;
