import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
function NewCustomStatusIcon({className}) {
    return (
        <SvgIcon className={className} viewBox="0 0 20 19.819">
  <g id="iconStatus" transform="translate(-231 -248)">
    <path id="Ellipse_85" data-name="Ellipse 85" d="M9-1a9.9,9.9,0,0,1,6.184,2.141L13.946,2.712A7.917,7.917,0,0,0,9,1Zm-.616.019.121,2a8.017,8.017,0,0,0-4.83,2.014L2.344,1.537A10.026,10.026,0,0,1,8.384-.981Zm9.579,5.542A9.9,9.9,0,0,1,19,9a10.066,10.066,0,0,1-.211,2.053l-1.958-.409A8.062,8.062,0,0,0,17,9a7.918,7.918,0,0,0-.829-3.55ZM-.22,5.122,1.623,5.9a8.044,8.044,0,0,0-.339,5.225l-1.929.529A10.043,10.043,0,0,1-.22,5.122Zm15.584,8.727,1.59,1.213a10.045,10.045,0,0,1-5.45,3.622L11,16.747A8.036,8.036,0,0,0,15.364,13.849Zm-12.416.383a8.026,8.026,0,0,0,4.529,2.624L7.1,18.819A10.035,10.035,0,0,1,1.435,15.54Z" transform="translate(232 249)" fill="#e256a1"/>
    <circle id="Ellipse_86" data-name="Ellipse 86" cx="5.4" cy="5.4" r="5.4" transform="translate(235.6 252.6)" fill="#e256a1"/>
  </g>
</SvgIcon>

    )
}

export default NewCustomStatusIcon
