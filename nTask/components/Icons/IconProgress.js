import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconProgress = ({ color = "#a5b4be", style }) => {
  return (
    <SvgIcon style={{ ...style }} viewBox="0 0 22 10.522">
      <g transform="translate(-161.5 -261.5)">
        <path d="M3.761-.065a3.826,3.826,0,0,0,0,7.652H15.239a3.826,3.826,0,0,0,0-7.652H3.761m0-1.435H15.239a5.261,5.261,0,0,1,0,10.522H3.761a5.261,5.261,0,0,1,0-10.522Z" transform="translate(163 263)" fill={color} />
        <rect width="9.565" height="3.826" rx="1.913" transform="translate(164.848 264.848)" fill={color} />
      </g>
    </SvgIcon>
  );
};

export default IconProgress;
