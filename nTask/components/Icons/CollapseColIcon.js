import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const CollapseColIcon = ({color = '#969696', style}) => {
  return (
   <SvgIcon style={style} viewBox="0 0 22 10.001">
     <path style={{fill: color}}
           d="M-534.6-2267.466l5-5,.881.882-3.487,3.494h7.606v1.249H-532.2l3.487,3.494-.881.882Zm-7.881,4.118,3.488-3.494H-546.6v-1.249h7.606l-3.488-3.494.881-.882,5,5-5,5Z"
           transform="translate(546.597 2272.467)"/>
   </SvgIcon>
  )
}

export default CollapseColIcon;
