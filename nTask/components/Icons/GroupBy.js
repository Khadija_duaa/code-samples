import React, { Component, Fragment } from "react";

const GroupBy = () => {
  return (
    <Fragment>
        <g transform="translate(-1027 -84)">
          <rect style={{opacity:0}} width="24" height="24" transform="translate(1027 84)"/>
          <path class="b" d="M256,23v-.875l7,3.5V26.5L256,23m7,3.5v-.875l7-3.5V23l-7,3.5m-7-.875V24.75l7,3.5v.875l-7-3.5m7,3.5V28.25l7-3.5v.875l-7,3.5M263,16l-7,3.938,7,3.938,7-3.938Z" transform="translate(776 74)"/>
        </g>
    </Fragment>
  );
};

export default GroupBy;