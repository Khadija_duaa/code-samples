// viewBox="0 0 14 11"

import React, { Component, Fragment } from "react";

const IconGantt = () => {
  return (
    <Fragment>
      <path
        id="icon_gantt"
        data-name="icon gantt"
        style={{ fill: "#c2c2c2", fillRule: "evenodd" }}
        d="M242.063,234H250.7a1.064,1.064,0,0,1,0,2.127h-8.638A1.064,1.064,0,0,1,242.063,234Zm3.076,4.453h8.8a1.064,1.064,0,1,1,0,2.127h-8.8A1.064,1.064,0,1,1,245.139,238.453Zm-1.346,4.42h6.3a1.064,1.064,0,1,1,0,2.127h-6.3A1.064,1.064,0,0,1,243.793,242.873Z"
        transform="translate(-241 -234)"
      />
    </Fragment>
  );
};

export default IconGantt;
