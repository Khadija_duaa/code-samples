{
  /* <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs><style>.a,.b{fill:#969696;}.a{opacity:0;}.b{fill-rule:evenodd;}</style></defs></svg> */
}

//    viewBox="0 0 24 24"

import React, { Component, Fragment } from "react";

const IconDock = (color = "#969696") => {
  return (
    <Fragment>
      <path
        style={{ fill: color }}
        d="M13.2,11.2h3.2V9.6H13.2Zm0,2.4h3.2V12H13.2Zm0,2.4h3.2V14.4H13.2Zm-1.6.4H4.4a.8.8,0,0,1-.8-.8V4.4a.8.8,0,0,1,.8-.8h7.2ZM15.6,2H4.4A2.4,2.4,0,0,0,2,4.4V15.6A2.4,2.4,0,0,0,4.4,18H15.6A2.4,2.4,0,0,0,18,15.6V4.4A2.4,2.4,0,0,0,15.6,2Z"
        transform="translate(2 22) rotate(-90)"
      />
    </Fragment>
  );
};

export default IconDock;
