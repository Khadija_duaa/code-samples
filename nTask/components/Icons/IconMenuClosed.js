import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IconMenuClosed = ({ color = "#a5b4be", style }) => {
  return (
    <SvgIcon style={{ color, ...style }} viewBox="0 0 25.719 13.5">
      <g id="iconMenuClosed" transform="translate(0)">
        <rect
          id="Rectangle_6"
          data-name="Rectangle 6"
          width="16.33"
          height="1.5"
          rx="0.75"
          transform="translate(0)"
          style={{ fill: "#1d3544" }}
        />
        <rect
          id="Rectangle_7"
          data-name="Rectangle 7"
          width="16.33"
          height="1.5"
          rx="0.75"
          transform="translate(0 12)"
          style={{ fill: "#1d3544" }}
        />
        <rect
          id="Rectangle_8"
          data-name="Rectangle 8"
          width="16.33"
          height="1.5"
          rx="0.75"
          transform="translate(0 6)"
          style={{ fill: "#1d3544" }}
        />
        <path
          id="Path_465"
          data-name="Path 465"
          d="M10.3,15a.8.8,0,0,1-.569-1.369L12.381,11,9.835,8.35a.8.8,0,1,1,1.137-1.129l3.091,3.2a.8.8,0,0,1,0,1.121l-3.2,3.2A.8.8,0,0,1,10.3,15Z"
          transform="translate(11.427 -3.984)"
          style={{ fill: "#1d3544" }}
        />
      </g>
    </SvgIcon>
  );
};

export default IconMenuClosed;
