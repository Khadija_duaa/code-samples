{/* <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22"></svg>; */}

import React, { Component, Fragment } from "react";

const CustomfieldIcon = () => {
  return (
    <Fragment>
      <g id="iconCustomFields" transform="translate(-200 -18)">
        <rect
          id="Rectangle_24"
          data-name="Rectangle 24"
          width="22"
          height="22"
          rx="4"
          transform="translate(200 18)"
          style={{fill: '#fa0'}}
        />
        <g
          id="iconCustomFields-2"
          data-name="iconCustomFields"
          transform="translate(465.41 -268.405)">
          <path
            id="Path_11715"
            data-name="Path 11715"
            d="M-252.18,371.48a1.748,1.748,0,0,0,1.5-1.568,1.745,1.745,0,0,0-1.287-1.852,1.728,1.728,0,0,0-2.006.926.277.277,0,0,1-.3.183q-2.92-.007-5.839,0c-.079,0-.157.008-.238.013v1.153h.25q2.929,0,5.858,0a.237.237,0,0,1,.253.16A1.734,1.734,0,0,0-252.18,371.48Z"
            transform="translate(-0.056 -72.729)"
            style={{fill: '#fff'}}
          />
          <path
            id="Path_11716"
            data-name="Path 11716"
            d="M-258.2,293.726a.272.272,0,0,1,.294.185,1.733,1.733,0,0,0,1.914.948,1.766,1.766,0,0,0,1.39-1.59,1.748,1.748,0,0,0-1.3-1.807,1.725,1.725,0,0,0-2.021.959.209.209,0,0,1-.225.143c-.687-.006-1.373,0-2.06,0-.066,0-.133.006-.2.009v1.141a.321.321,0,0,0,.056.015C-259.636,293.728-258.918,293.733-258.2,293.726Z"
            transform="translate(0 0)"
            style={{fill: '#fff'}}
          />
          <path
            id="Path_11717"
            data-name="Path 11717"
            d="M-255.934,444.688a1.732,1.732,0,0,0-1.962.986.208.208,0,0,1-.227.141c-.687-.005-1.373,0-2.06,0-.066,0-.132.005-.2.008v1.142a.381.381,0,0,0,.059.014c.705,0,1.411.007,2.116,0a.312.312,0,0,1,.341.2,1.718,1.718,0,0,0,1.926.917,1.747,1.747,0,0,0,1.366-1.665A1.755,1.755,0,0,0-255.934,444.688Z"
            transform="translate(-0.031 -145.502)"
            style={{fill: '#fff'}}
          />
          <path
            id="Path_11718"
            data-name="Path 11718"
            d="M-130.018,314.558V315.7h5.415v-1.144Z"
            transform="translate(-123.807 -21.984)"
            style={{fill: '#fff'}}
          />
          <path
            id="Path_11719"
            data-name="Path 11719"
            d="M-130.017,469.089h5.409v-1.146h-5.409Z"
            transform="translate(-123.808 -167.622)"
            style={{fill: '#fff'}}
          />
          <path
            id="Path_11720"
            data-name="Path 11720"
            d="M-53.223,392.456h1.536v-1.134h-1.536Z"
            transform="translate(-196.723 -94.871)"
            style={{fill: '#fff'}}
          />
        </g>
      </g>
    </Fragment>
  );
};

export default CustomfieldIcon;
