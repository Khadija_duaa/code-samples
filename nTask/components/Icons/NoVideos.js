{
  /* <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15"><defs><style>.a{fill:;}</style></defs></svg> */
}
// viewBox="0 0 180 117.752"
import React, { Component, Fragment } from "react";

const NoVideos = () => {
  return (
    <Fragment><g transform="translate(-714.02 -76.247)">
      <g transform="translate(714.02 192.851)">
        <g transform="translate(0)">
          <rect width="180" height="0.985" fill="#28728e" />
        </g>
      </g>
      <path d="M946.382,76.248h-97.5c-4.423,0-8.022,3.05-8.022,6.8V194H954.4V83.047C954.4,79.3,950.805,76.248,946.382,76.248Z" transform="translate(-85.215 -0.001)" fill="#00abed" opacity="0.25" />
      <g transform="translate(748.674 76.247)">
        <path d="M20.945,0H90.3a20.945,20.945,0,0,1,20.945,20.945V115.8a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V20.945A20.945,20.945,0,0,1,20.945,0Z" transform="translate(1.149 0.974)" fill="#fff" />
        <path d="M933.161,194H819.618V83.046c0-3.749,3.6-6.8,8.022-6.8h97.5c4.423,0,8.022,3.05,8.022,6.8Zm-111.245-1.947H930.864V83.046c0-2.676-2.568-4.852-5.725-4.852h-97.5c-3.157,0-5.725,2.177-5.725,4.852Z" transform="translate(-819.618 -76.247)" fill="#28728e" />
      </g>
      <g transform="translate(760.941 102.526)">
        <rect width="87.294" height="55.116" transform="translate(0.492 0.492)" fill="#00abed" />
        <path d="M945.274,212.424H856.995v-56.1h88.279Zm-87.294-.985h86.31V157.309H857.98Z" transform="translate(-856.995 -156.324)" fill="#28728e" />
      </g>
      <g transform="translate(794.843 114.065)">
        <g transform="translate(0)">
          <path d="M971.447,191.484a11.146,11.146,0,1,0,11.146,11.146A11.145,11.145,0,0,0,971.447,191.484Zm0,20.94a9.795,9.795,0,1,1,9.794-9.794A9.793,9.793,0,0,1,971.447,212.424Z" transform="translate(-960.302 -191.484)" fill="#fff" />
        </g>
        <g transform="translate(6.931 4.852)">
          <path d="M982.273,206.353a.559.559,0,0,0-.85.477v11.554a.559.559,0,0,0,.85.477l9.438-5.777a.559.559,0,0,0,0-.953Z" transform="translate(-981.423 -206.27)" fill="#fff" />
        </g>
      </g>
      <g transform="translate(768.498 148.426)">
        <path d="M880.4,296.221a.22.22,0,0,0-.336.184l-.02,2.269-.02,2.269a.22.22,0,0,0,.332.19l3.727-2.236a.22.22,0,0,0,0-.374Z" transform="translate(-880.024 -296.187)" fill="#28728e" />
      </g>
      <g transform="translate(774.724 150.208)">
        <rect width="65.252" height="1.414" transform="translate(0 0)" fill="#fff" />
      </g>
      <g transform="translate(774.724 150.208)">
        <rect width="28.059" height="1.414" transform="translate(0 0)" fill="#28728e" />
      </g>
      <g transform="translate(801.388 149.356)">
        <circle data-name="Ellipse 120" cx="1.559" cy="1.559" r="1.559" transform="translate(0 0)" fill="#28728e" />
      </g>
      <g transform="translate(761.433 172.155)">
        <rect width="87.294" height="1.313" transform="translate(0)" fill="#9ed6ec" />
      </g>
      <g transform="translate(761.433 175.437)">
        <rect width="87.294" height="1.313" transform="translate(0)" fill="#9ed6ec" />
      </g>
      <g transform="translate(761.433 178.719)">
        <rect width="87.294" height="1.313" transform="translate(0)" fill="#9ed6ec" />
      </g>
      <g transform="translate(761.433 182)">
        <rect width="45.62" height="1.313" transform="translate(0)" fill="#9ed6ec" />
      </g>
      <g transform="translate(756.018 84.205)">
        <circle data-name="Ellipse 121" cx="2.789" cy="2.789" r="2.789" transform="translate(0.492 0.492)" fill="#00abed" />
        <path d="M845.277,107.058a3.282,3.282,0,1,1,3.282-3.282A3.285,3.285,0,0,1,845.277,107.058Zm0-5.579a2.3,2.3,0,1,0,2.3,2.3A2.3,2.3,0,0,0,845.277,101.48Z" transform="translate(-841.995 -100.495)" fill="#28728e" />
      </g>
      <g transform="translate(765.973 84.205)">
        <circle data-name="Ellipse 122" cx="2.789" cy="2.789" r="2.789" transform="translate(0.492 0.492)" fill="#00abed" />
        <path d="M875.61,107.058a3.282,3.282,0,1,1,3.282-3.282A3.285,3.285,0,0,1,875.61,107.058Zm0-5.579a2.3,2.3,0,1,0,2.3,2.3A2.3,2.3,0,0,0,875.61,101.48Z" transform="translate(-872.328 -100.495)" fill="#28728e" />
      </g>
      <g transform="translate(775.927 84.205)">
        <circle data-name="Ellipse 123" cx="2.789" cy="2.789" r="2.789" transform="translate(0.492 0.492)" fill="#00abed" />
        <path d="M905.943,107.058a3.282,3.282,0,1,1,3.282-3.282A3.285,3.285,0,0,1,905.943,107.058Zm0-5.579a2.3,2.3,0,1,0,2.3,2.3A2.3,2.3,0,0,0,905.943,101.48Z" transform="translate(-902.661 -100.495)" fill="#28728e" />
      </g>
    </g>
    </Fragment>
  );
};

export default NoVideos;
