import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const ResizeIcon = ({color = '#969696', style}) => {
  return (
   <SvgIcon style={style} viewBox="0 0 16.501 14.927">
     <path style={{fill: color, stroke: '#fff', strokeWidth:0.5}}
           d="M15.992,26.867,15.131,21.2a.511.511,0,0,0-.495-.472.6.6,0,0,0-.338.114L9.592,24.114a.477.477,0,0,0,.088.875l1.826.786a5.2,5.2,0,1,1-4.293-8.131,1.007,1.007,0,1,0,0-2.013,7.213,7.213,0,1,0,6.167,10.951l1.915.824a.628.628,0,0,0,.248.055.443.443,0,0,0,.342-.152A.54.54,0,0,0,15.992,26.867Z"
           transform="translate(0.25 -15.38)"/>
   </SvgIcon>
  )
}

export default ResizeIcon;
