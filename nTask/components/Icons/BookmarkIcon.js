import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const BookmarkIcon = ({color = '#969696', style}) => {
  return (
   <SvgIcon style={style} viewBox="0 0 16.722 16.722">
     <path style={{color}}
           d="M261.921,254.8l4.171-2.085a.6.6,0,0,0,.157-.96l-5.94-5.94a.6.6,0,0,0-.961.156l-2.086,4.171a5.98,5.98,0,0,0-5.438,1.613.6.6,0,0,0,0,.848L255.218,256l-5.091,5.091-.425,1.273,1.273-.424,5.091-5.091,3.394,3.394a.6.6,0,0,0,.849,0,5.985,5.985,0,0,0,1.612-5.44Zm-8.8-2.594a4.806,4.806,0,0,1,4.29-.79.6.6,0,0,0,.706-.307l1.929-3.857,4.762,4.762-3.856,1.928a.6.6,0,0,0-.308.7,4.806,4.806,0,0,1-.79,4.293Z"
           transform="translate(-249.702 -245.642)"/>
   </SvgIcon>
  )
}

export default BookmarkIcon;
