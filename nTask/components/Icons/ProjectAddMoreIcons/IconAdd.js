//    viewBox="0 0 24 24"

import React, { Component, Fragment } from "react";

const IconAdd = (color = "#bfbfbf") => {
  return (
    <Fragment>
      <rect
        style={{ fill: color, opacity: 0 }}
        width="24"
        height="24"
        transform="translate(24 24) rotate(180)"
      />
      <path
        style={{ fill: color }}
        d="M17.125,10.125h-5.25V4.875a.875.875,0,1,0-1.75,0v5.25H4.875a.875.875,0,1,0,0,1.75h5.25v5.25a.875.875,0,0,0,1.75,0v-5.25h5.25a.875.875,0,0,0,0-1.75Z"
        transform="translate(1 1)"
      />
    </Fragment>
  );
};

export default IconAdd;
