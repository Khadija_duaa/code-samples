import React, { Component, Fragment } from "react";

const IconReminder = (color = "#bfbfbf") => {
  return (
    <Fragment>
      <rect
        style={{ fill: color, opacity: 0 }}
        width="24"
        height="24"
      />
      <path
        style={{ fill: color }}
        d="M20.55,3.51,16.41,0,15.24,1.35l4.14,3.51ZM7.86,1.44,6.69.09,2.55,3.51,3.72,4.86ZM12,5.58H10.65v5.4l4.23,2.61.72-1.08L12,10.35Zm-.45-3.6a8.1,8.1,0,1,0,8.1,8.1A8.066,8.066,0,0,0,11.55,1.98Zm0,14.4a6.3,6.3,0,1,1,6.3-6.3A6.261,6.261,0,0,1,11.55,16.38Z"
        transform="translate(0.45 3)"
      />
    </Fragment>
  );
};

export default IconReminder;
