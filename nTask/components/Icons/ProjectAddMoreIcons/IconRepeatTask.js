import React, { Component, Fragment } from "react";

const IconRepeatTask = (color = "#bfbfbf") => {
  return (
    <Fragment>
      <rect style={{ fill: color, opacity: 0 }} width="24" height="24" />
      <path
        style={{ fill: color, fillRule: "evenodd" }}
        d="M1065.991,88H1063.9l1.3-1.292a1,1,0,0,0-1.419-1.413l-3.7,3.7,3.7,3.7a1,1,0,0,0,1.419-1.413L1063.9,90h2.089a2.785,2.785,0,0,1,2.5,3,3,3,0,0,1-3,3h0a1,1,0,1,0,0,2h0a5,5,0,0,0,5-5,4.773,4.773,0,0,0-4.5-5Zm-8.2,5.29a1,1,0,0,0,0,1.413l1.3,1.292H1057a2.785,2.785,0,0,1-2.5-3,3,3,0,0,1,3-3h0a1,1,0,1,0,0-2h0a5,5,0,0,0-5,5,4.773,4.773,0,0,0,4.5,5h2.089l-1.3,1.292a1,1,0,0,0,1.419,1.413l3.7-3.7-3.7-3.7a1.006,1.006,0,0,0-1.419,0Z"
        transform="translate(-1049.494 -80.998)"
      />
    </Fragment>
  );
};

export default IconRepeatTask;
