//    viewBox="0 0 24 24"

import React, { Component, Fragment } from "react";

const IconImage = (color = "#969696") => {
  return (
    <Fragment>
      <g transform="translate(-4 -4)">
        <path
          style={{ fill: color }}
          d="M-3768-4605.97a4,4,0,0,1-4-4v-16a4,4,0,0,1,4-4h16a4,4,0,0,1,4,4v16a4,4,0,0,1-4,4Zm-1.334-20v15.6l9.706-8.094a3.7,3.7,0,0,1,4.694,0l4.265,3.642v-11.146A1.334,1.334,0,0,0-3752-4627.3h-16A1.334,1.334,0,0,0-3769.333-4625.969Zm2,3.332a2,2,0,0,1,2-2,2,2,0,0,1,2,2,2,2,0,0,1-2,2A2,2,0,0,1-3767.333-4622.638Z"
          transform="translate(3776 4633.97)"
        />
      </g>
    </Fragment>
  );
};

export default IconImage;
