import React, { Component, Fragment } from "react";

const CheckBoxCheckIcon = (color="#7cb147") => {
  return (
    <Fragment>
      <rect
        id="Rounded_Rectangle_1595"
        style={{
          fill: color,
          stroke: color,
          strokeLinejoin: "round",
          strokeWidth: 1
        }}
        data-name="Rounded Rectangle 1595"
        x="2.5"
        y="2.5"
        width="16"
        height="16"
        rx="2"
        ry="2"
      />
      <path
        id="Tick"
        style={{ fill: "#fff", stroke: "#fff", fillRule: "evenodd", strokeLinejoin: "round", strokeWidth: 1 }}
        d="M5806.01,1752.04a0.539,0.539,0,0,0-.73,0l-6.17,5.77-2.37-2.22a0.539,0.539,0,0,0-.73,0,0.46,0.46,0,0,0,0,.68l2.74,2.56a0.539,0.539,0,0,0,.73,0l6.53-6.11A0.46,0.46,0,0,0,5806.01,1752.04Z"
        transform="translate(-5790.5 -1744.5)"
      />
    </Fragment>
  );
};

export default CheckBoxCheckIcon;
