import React, { Component, Fragment } from "react";

const IconAttachment = ({color = "#5fb7ec"}) => {
  return (
    <Fragment>
      <svg xmlns="http://www.w3.org/2000/svg" width="18" height="17.036" viewBox="0 0 18 17.036"><defs></defs><path style={{ fill: color }} d="M16.315,15.389a5.761,5.761,0,0,0-8.138,0L1.2,22.364a4.11,4.11,0,0,0,5.813,5.813l6.394-6.394A2.466,2.466,0,1,0,9.921,18.3L5.74,22.477A.822.822,0,1,0,6.9,23.639l4.181-4.181a.822.822,0,1,1,1.163,1.162L5.853,27.014a2.466,2.466,0,0,1-3.488-3.488L9.34,16.552a4.11,4.11,0,1,1,5.813,5.813L8.178,29.34A.822.822,0,0,0,9.34,30.5l6.975-6.975a5.754,5.754,0,0,0,0-8.138Z" transform="translate(-0.001 -13.707)"/></svg>
    </Fragment>
  );
};

export default IconAttachment;
