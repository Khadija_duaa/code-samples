// @flow
import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import Typography from "@material-ui/core/Typography";
import ArrowRight from "@material-ui/icons/KeyboardArrowRight";
import isEmpty from "lodash/isEmpty";

type BreadCrumbProps = {
  classes: Object,
  theme: Object,
  handlebreadCrumClick: Function,
  breadCrumArr: Array,
  endBreadCrumbName: String,
};

function BreadCrumb(props: BreadCrumbProps) {
  const {
    classes,
    theme,
    handlebreadCrumClick,
    breadCrumArr,
    endBreadCrumbName,
  } = props;

  const handleItemClick = (b) => {
    handlebreadCrumClick(b);
  };

    return (
      <div className={classes.breadCrumbCnt}>
        {!isEmpty(breadCrumArr) && (
          <div style={{ marginBottom: 5 }}>
            {breadCrumArr.map((b) => {
              return (
                <>
                  {" "}
                  <span
                    className={classes.breadCrumbTitle}
                    onClick={() => handleItemClick(b)}
                    title={b.taskTitle}
                  >
                    {b.taskTitle}
                  </span>
                  <ArrowRight
                    className={classes.arrowRight}
                    htmlColor={theme.palette.secondary.medDark}
                  />
                </>
              );
            })}
          </div>
        )}

        <span title={endBreadCrumbName} className={classes.breadCrumbEndTitle}>{endBreadCrumbName}</span>
      </div>
    );
}

BreadCrumb.defaultProps = {
  classes: {},
  theme: {},
  handlebreadCrumClick: () => {},
  breadCrumArr: [
    { key: "Bread Crumb 01", value: 1 },
    { key: "Bread Crumb 02", value: 2 },
    { key: "Bread Crumb 03", value: 3 },
  ],
  endBreadCrumbName: "Bread Crumb Ends",
};
const mapStateToProps = (state) => {
  return {};
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(BreadCrumb);
