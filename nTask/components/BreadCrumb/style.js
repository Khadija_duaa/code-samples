const BreadCrumbStyles = (theme) => ({
  breadCrumbTitle: {
    color: "#0090ff",
    fontSize: "13px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
    textDecoration: "underline",
    cursor: "pointer",
    
    display: "inline-block",
    // overflow: "hidden",
    // whiteSpace: "nowrap",
    lineHeight: "normal",
    // textOverflow: "ellipsis",
    // maxWidth: 290,
    wordBreak: "break-word",
  },
  arrowRight: {
    fontSize: "18px !important",
    marginBottom: -4,
  },
  breadCrumbCnt: {
    margin: "5px 0px",
  },
  breadCrumbEndTitle: {
    color: "#202020",
    fontSize: "18px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,

    display: "inline-block",
    // overflow: "hidden",
    // whiteSpace: "nowrap",
    lineHeight: "normal",
    // textOverflow: "ellipsis",
    // width: 340,
    wordBreak: "break-word",
  },
});
export default BreadCrumbStyles;
