import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import DefaultDialog from "../Dialog/Dialog";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Divider from "@material-ui/core/Divider";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import CustomButton from "../Buttons/CustomButton";
import { generateSelectData } from "../../helper/generateSelectData";
import RoundIcon from "@material-ui/icons/Brightness1";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../Icons/DeleteIcon";
import Typography from "@material-ui/core/Typography";
import NotificationMessage from "../NotificationMessages/NotificationMessages";
import templatesConstants from "../../utils/constants/templatesConstants";

type DeleteTemplateDialogProps = {
  classes: Object,
  theme: Object,
  open: Boolean,
  handleClose: Function,
  WSAllStatus: Object,
  oldStatus: Object,
  // newStatus: Object,
  handleSaveAsTemplate: Function,
};

function DeleteTemplateDialog(props: DeleteTemplateDialogProps) {
  const { classes, theme, handleClose, open, tasks, projects, oldStatus, newStatus, WSAllStatus, templateConfig } = props;
  const [currentView, setView] = useState("mappManually");
  const [selectedStatus, setSelectedStatus] = useState({});
  const [statusArr, setStatusArr] = useState([]);
  const [template, setTemplate] = useState([]);
  const handleChangeTab = (event, nextView) => {
    setView(nextView);
  };

  const generateTemplate = () => {
    /* function for generating the array of status and returing array to the data prop for multi drop down */
    let templates = [];
    WSAllStatus.forEach(groupItem => {
      groupItem.templates.forEach(templateItem => {
        if (templateItem.templateId != oldStatus.templateId)
          templates.push({
            label: templateItem.name,
            value: templateItem.name,
            uniqueId: null,
            id: null,
            obj: templateItem,
          });
      });
    });
    return templates;
  };

  const generateStatuses = () => {
    /* function for generating the array of status and returing array to the data prop for multi drop down */
    let statuses = [];
    statuses = template.obj
      ? template.obj.statusList.map((s, i) => {
          //   return generateSelectData(s.statusTitle, s.statusId, s.statusId, s.statusId, s);
          return {
            label: s.statusTitle,
            value: s.statusId,
            icon: <RoundIcon htmlColor={s.statusColor} className={classes.statusesIcon} />,
          };
        })
      : [];
    return statuses;
  };

  const handleSelectTemplate = (type, item) => {
    setTemplate(item);
    handleClearSelectedStatuses();
  };

  const handleClearSelectedStatuses = () => {
    let arr = statusArr.map(ele => {
      ele.linkStatus = null;
      return ele;
    });
    setStatusArr(arr);
  };

  const handleSelectStatus = (key, opt, s) => {
    // setSelectedStatus(status);
    let option = {
      status: s.status,
      linkStatus: opt,
    };
    let arr = statusArr.map(ele => {
      if (ele.status.statusCode == option.status.statusCode) return option;
      else return ele;
    });
    setStatusArr(arr);
  };

  const setStatusArray = () => {
    let arr = oldStatus.statusList.map(s => {
      return {
        status: s,
        linkStatus: null,
      };
    });
    setStatusArr(arr);
  };

  useEffect(() => {
    setStatusArray();
  }, []);

  const handleSuccess = () => {
    let arr = [];
    if (currentView == "mappManually") {
      if (newStatus.isDefault) {
        statusArr.map(s => {
          arr.push({
            statusIDFrom: s.status.statusId,
            statusIDTo: s.linkStatus.value,
          });
        });
      }
    }

    let obj = {
      // fromTemplateID: newStatus.templateId,
      // toTemplateID: newStatus.isDefault ? template.obj.templateId : null,
      // issetDefaultStatus: newStatus.isDefault ? true : false,
      // mappingStatus: arr,
      templateId: newStatus.templateId
    };
    props.deleteTemplate(obj);
  };
  const filterTask = () => {
    let total = []
    if(templateConfig.contextView == templatesConstants.WORKSPACE_VIEW){
      tasks.map(el => {
          if(el.projectId == null){
            total.push(el);
          } else{
            let project = projects.find(item => item.projectId == el.projectId);
            if(project && project.projectTemplateId == oldStatus.templateId){
              total.push(el)
            }
          }
      })
    }else{
      tasks.map(el => {
        if(el.projectId != null && templateConfig.project.projectId == el.projectId){
            total.push(el)
        }
      })
    }
    return total;
  }
  const getDisabled = () => {
    if(newStatus.isDefault){
    if(currentView == "mappManually")
      return statusArr.some(item => item.linkStatus==null)
    return false;
    }else return false
  }
  const associatedTasks = filterTask();//contextView == templatesConstants.WORKSPACE_VIEW ? tasks.filter(el => el.projectId == null) :tasks.filter(el => el.projectId != null)

  return (
    <>
      <DefaultDialog
        title={newStatus.isDefault ? "Status Mapping" : "Delete Template"}
        sucessBtnText="Done"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={handleClose}>
        <div className={classes.statusMapCnt}>
          {newStatus.isDefault ? (
            <>
              <div className={classes.heading}>
                <span className={classes.title}>
                  <span
                    className={classes.title}
                    style={{ color: "#202020", fontWeight: 800 }}>{`${associatedTasks.length} tasks`}</span>
                  {` will be affected by mapping the default statuses.`}
                </span>
              </div>
              <div className={classes.heading}>
                <span className={classes.title}>How should we handle these tasks?</span>
              </div>

              <div className={classes.statusesCnt}>
                <SelectSearchDropdown /* Issue Status select drop down */
                  data={generateTemplate}
                  label={"Select Template"}
                  selectChange={handleSelectTemplate}
                  type="status"
                  selectedValue={template}
                  placeholder={"Select status"}
                  icon={true}
                  isMulti={false}
                  isDisabled={false}
                  styles={{ width: 230, marginBottom: 0 }}
                />
              </div>

              <Divider style={{ margin: "20px 0" }} />

              {/* <div className={classes.toggleContainer}>
                <ToggleButtonGroup
                  value={currentView}
                  exclusive
                  onChange={handleChangeTab}
                  classes={{ root: classes.toggleBtnGroup }}>
                  <ToggleButton
                    value="mappManually"
                    classes={{
                      root: classes.toggleButton,
                      selected: classes.toggleButtonSelected,
                    }}>
                    Map Status Manually
                  </ToggleButton>
                  <ToggleButton
                    value="allOpen"
                    classes={{
                      root: classes.toggleButton,
                      selected: classes.toggleButtonSelected,
                    }}>
                    Change all to <Brightness1Icon className={classes.openIcon} /> Open
                  </ToggleButton>
                </ToggleButtonGroup>
              </div> */}

              {currentView == "mappManually" && (
                <>
                  <div className={classes.labelCnt}>
                    {" "}
                    <span className={classes.labelTitleLeft}>Old Status</span>
                    <span className={classes.labelTitleRight}>New Status</span>
                  </div>
                  {statusArr.map((s, index) => {
                    return (
                      <div className={classes.statusesCnt} key={index}>
                        <CustomButton
                          btnType="white"
                          variant="contained"
                          style={{
                            fontSize: "14px",
                            color: "#202020",
                            border: "none",
                            background: "#F6F6F6",
                            marginLeft: 0,
                            fontFamily: theme.typography.fontFamilyLato,
                            fontWeight: theme.palette.fontWeightMedium,
                            padding: "7px 50px 7px 9px",
                            width: 170,
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "end",
                          }}>
                          <Brightness1Icon
                            className={classes.statusesIcon}
                            style={{ color: s.status.statusColor }}
                          />
                          {s.status.statusTitle}
                        </CustomButton>
                        <span className={classes.taskCountTitle}>
                          {" "}
                          {s.status.taskCount} tasks{" "}
                          <ArrowForwardIcon className={classes.arrowRight} />{" "}
                        </span>

                        <SelectSearchDropdown /* Issue Status select drop down */
                          data={generateStatuses}
                          label={null}
                          selectChange={(key, opt) => {
                            handleSelectStatus(key, opt, s);
                          }}
                          type="status"
                          selectedValue={s.linkStatus}
                          placeholder={"Select status"}
                          icon={true}
                          isMulti={false}
                          isDisabled={false}
                          styles={{ width: 170, margin: 0 }}
                        />
                      </div>
                    );
                  })}
                </>
              )}
            </>
          ) : (
            <>
              {" "}
              <div className={classes[`centerAlignContent`]}>
                <div className={classes.deleteIconCnt}>
                  <SvgIcon
                    viewBox="0 0 512 512"
                    className={classes.deleteConfirmationIcon}
                    htmlColor={theme.palette.error.main}>
                    <DeleteIcon />
                  </SvgIcon>
                </div>

                <span
                  className={classes.title}
                  style={{ textAlign: "center", padding: "0px 15px", lineHeight: 1.6 }}>
                  This template will be permanently deleted and this action cannot be undone.
                </span>
                <span className={classes.title} style={{ textAlign: "center", lineHeight: 1.6 }}>
                  {`Are you sure you want to delete the`}{" "}
                  <span
                    className={classes.title}
                    style={{
                      color: "#202020",
                      fontWeight: 800,
                    }}>{` ${newStatus.name}`}</span>
                  {` template.`}
                </span>
              </div>{" "}
            </>
          )}

          {newStatus.isDefault && (
            <>
              <Divider style={{ margin: "20px 0" }} />

              <NotificationMessage
                type="info"
                iconType="info"
                style={{
                  // width: "calc(100% - 50px)",
                  // padding: "10px 15px 10px 15px",
                  backgroundColor: "rgba(243, 243, 243, 1)",
                  borderRadius: 4,
                  // margin: "10px 20px",
                }}>
                {`New selected template will also be marked as default`}
              </NotificationMessage>
            </>
          )}
        </div>
        <ButtonActionsCnt
          cancelAction={handleClose}
          successAction={handleSuccess}
          successBtnText={newStatus.isDefault ? "Done" : "Delete"}
          cancelBtnText={"Cancel"}
          btnType={newStatus.isDefault ? "blue" : "danger"}
          btnQuery={""}
          disabled={getDisabled()}
        />
      </DefaultDialog>
    </>
  );
}

DeleteTemplateDialog.defaultProps = {
  classes: {},
  theme: {},
  open: false,
  handleClose: () => {},
  tasks: [],
  projects: [],
  oldStatus: {},
  // newStatus: {},
};
const mapStateToProps = state => {
  return {
    tasks: state.tasks.data || [],
    projects: state.projects.data || [],
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(DeleteTemplateDialog);
