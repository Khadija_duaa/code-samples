import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import DefaultDialog from "../Dialog/Dialog";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Divider from "@material-ui/core/Divider";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import CustomButton from "../Buttons/CustomButton";
import { generateSelectData } from "../../helper/generateSelectData";
import RoundIcon from "@material-ui/icons/Brightness1";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../Icons/DeleteIcon";
import Typography from "@material-ui/core/Typography";
import IconButton from "../Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DefaultTextField from "../Form/TextField";
import { validateTitleField } from "../../utils/formValidations";
import debounce from "lodash/debounce";
import constants from "../../redux/constants/types";
import templatesConstants from "../../utils/constants/templatesConstants";

type SaveNewTemplateModalProps = {
  classes: Object,
  theme: Object,
  open: Boolean,
  handleClose: Function,
  updatedItem: Object,
};

function SaveNewTemplateModal(props: SaveNewTemplateModalProps) {
  const { classes, theme, handleClose, open, updatedItem} = props;
  const [newNameInputError, setNewNameInputError] = useState("");
  const [templateInputErrorMessage, setTemplateInputErrorMessage] = useState("");
  const [templateNewname, setTemplateNewname] = useState(updatedItem.customstatus.name + ' Copy');
  const [saveBtnQuery, setSaveBtnQuery] = useState("");

  useEffect(() => {}, []);
  // handle Tempalte name input
  const handleTemplateName = e => {
    setTemplateNewname(e.target.value);
  };
  const handleShowHideInput = value => {
    // Show/Hide Input on click
    // setIsRenameTemplate(false);
  };
  const templateExist = newName => {
    const { workSpaceStatus } = props;
    let nameExist = false;
    workSpaceStatus.map(group => {
      if (group.groupName == templatesConstants.SAVED_TEMPLATES) {
        group.templates.map(temp => {
          if (temp.name == newName)
            nameExist = true;
        });
      }
    });
    return nameExist;
  };
  const templateNewnameHandler = debounce(event => {
    setSaveBtnQuery("progress");
    const validationObj = validateTitleField("template title", templateNewname, true, 250);
    const alreadyExist = templateExist(templateNewname);
    if (validationObj.isError) {
      setNewNameInputError(validationObj.isError);
      setTemplateInputErrorMessage(validationObj.message);
      setSaveBtnQuery("");
      return;
    }
    if (alreadyExist) {
      setNewNameInputError(true);
      setTemplateInputErrorMessage("Already Exist");
      setSaveBtnQuery("");
      return;
    }
      updatedItem.customstatus.name = templateNewname.trim();
      updatedItem.customstatus.isWorkspaceDefault = false;
      props.handleSuccess(
        updatedItem,
        response => {
          setTemplateNewname("");
          setNewNameInputError(false);
          setTemplateInputErrorMessage("");
          setSaveBtnQuery("");
        },
        fail => {
          setTemplateNewname("");
          setNewNameInputError(false);
          setTemplateInputErrorMessage("");
          setSaveBtnQuery("");
        }
      );
  }, 300);

  return (
    <>
      {/* <DefaultDialog
        title="Save Template"
        sucessBtnText="Done"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={handleClose}> */}
          <div className={classes.newTemplateSaveCnt}>
                    <div className={classes.newTemplateSaveTitleCnt}>
                      <Typography variant="h2" style={{fontWeight: theme.palette.fontWeightMedium}}>
                          Save as Template
                      </Typography>
                      <IconButton btnType="transparent" onClick={handleClose}>
                        <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                      </IconButton>
                    </div>
                    <div className={classes.txtCnt}>
                      <DefaultTextField
                        label='Template Title'
                        fullWidth={false}
                        error={newNameInputError ? true : false}
                        errorState={newNameInputError}
                        errorMessage={templateInputErrorMessage}
                        formControlStyles={{ marginBottom: 0, width: 300,}}
                        defaultProps={{
                          id: "templateNewname",
                          type: "text",
                          onChange: handleTemplateName,
                          onBlur: event => handleShowHideInput(false),
                          value: templateNewname,
                          placeholder: "Enter template name",
                          inputProps: {
                            maxLength: 80,
                          },
                        }}
                      />
                    </div>
                      <div className={classes.newTemplateSaveFooterCnt} >
                        <CustomButton
                          btnType="white"
                          variant="contained"
                          style={{ marginBottom: 0, minWidth: 120 }}
                          onClick={() => handleClose()}
                          query={""}
                          disabled={false}>
                          Cancel
                        </CustomButton>
                        <CustomButton
                          btnType="success"
                          variant="contained"
                          style={{ marginLeft: 10, marginBottom: 0, minWidth: 120 }}
                          onClick={() => templateNewnameHandler(updatedItem)}
                          query={""}
                          disabled={saveBtnQuery == "progress"}>
                          Save Template
                        </CustomButton>
                      </div>
                    </div>
        
        {/* <ButtonActionsCnt
          cancelAction={handleClose}
          successAction={templateNewnameHandler}
          successBtnText={"Save"}
          cancelBtnText={"Cancel"}
          btnType={"success"}
          btnQuery={""}
          disabled={false}
        /> */}
      {/* </DefaultDialog> */}
    </>
  );
}

SaveNewTemplateModal.defaultProps = {
  classes: {},
  theme: {},
  open: false,
  handleClose: () => {},
};
const mapStateToProps = state => {
  return {
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(SaveNewTemplateModal);
