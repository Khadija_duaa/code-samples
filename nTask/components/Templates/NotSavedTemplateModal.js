import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import DefaultDialog from "../Dialog/Dialog";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Divider from "@material-ui/core/Divider";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import CustomButton from "../Buttons/CustomButton";
import { generateSelectData } from "../../helper/generateSelectData";
import RoundIcon from "@material-ui/icons/Brightness1";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../Icons/DeleteIcon";
import Typography from "@material-ui/core/Typography";
import IconButton from "../Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DefaultTextField from "../Form/TextField";
import { validateTitleField } from "../../utils/formValidations";
import debounce from "lodash/debounce";
import constants from "../../redux/constants/types";

type NotSavedTemplateModalProps = {
  classes: Object,
  theme: Object,
  open: Boolean,
  handleClose: Function,
  handleSuccess: Function
};

function NotSavedTemplateModal(props: NotSavedTemplateModalProps) {
  const { classes, theme, handleClose, open, handleSuccess } = props;

  useEffect(() => {}, []);

  return (
    <>
      <DefaultDialog
        title="Changes not Saved"
        sucessBtnText="Done"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={handleClose}>
          <div className={classes.newTemplateSaveCnt}>
                    {/* <div className={classes.newTemplateSaveTitleCnt}>
                      <Typography variant="h2" style={{fontWeight: theme.typography.fontWeightMedium}}>
                        Changes not Saved
                      </Typography>
                      <IconButton btnType="transparent" onClick={handleClose}>
                        <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                      </IconButton>
                    </div> */}
                    <div className={classes.txtCnt} >
                      <Typography variant="h6" style={{fontSize: "15px", textAlign: 'center', fontWeight: 400}}>
                        Looks like there are some unsaved changes on this page. <br/>
                        If you leave this page, change you made will not be saved.<br/>
                         What would you like to do?
                      </Typography>
                      
                    </div>
                      <div className={classes.newTemplateSaveFooterCnt} >
                        <CustomButton
                          btnType="danger"
                          variant="contained"
                          style={{ marginBottom: 0, minWidth: 120 }}
                          onClick={() => handleClose()}
                          query={""}
                          disabled={false}>
                          Leave Page and Discard Changes
                        </CustomButton>
                        <CustomButton
                          btnType="blue"
                          variant="contained"
                          style={{ marginLeft: 10, marginBottom: 0, minWidth: 120 }}
                          onClick={() => handleSuccess()}
                          query={""}
                          disabled={false}>
                          Stay on the Page
                        </CustomButton>
                      </div>
                    </div>
        
        {/* <ButtonActionsCnt
          cancelAction={handleClose}
          successAction={handleSuccess}
          successBtnText={"Stay on the Page"}
          cancelBtnText={"Leave Page and Discard Changes"}
          btnType={"success"}
          btnQuery={""}
          disabled={false}
        /> */}
      </DefaultDialog>
    </>
  );
}

NotSavedTemplateModal.defaultProps = {
  classes: {},
  theme: {},
  open: false,
  handleClose: () => {},
  handleSuccess: () => {},
};
const mapStateToProps = state => {
  return {
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(NotSavedTemplateModal);
