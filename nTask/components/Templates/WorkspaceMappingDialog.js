import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import DefaultDialog from "../Dialog/Dialog";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Divider from "@material-ui/core/Divider";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import CustomButton from "../Buttons/CustomButton";
import { generateSelectData } from "../../helper/generateSelectData";
import RoundIcon from "@material-ui/icons/Brightness1";
import templatesConstants from "../../utils/constants/templatesConstants";

type WorkspaceMappingDialogProps = {
  classes: Object,
  theme: Object,
  open: Boolean,
  handleClose: Function,
  oldStatus: Object,
  newStatus: Object,
};

function WorkspaceMappingDialog(props: WorkspaceMappingDialogProps) {
  const { classes, theme, handleClose, open, tasks, projects, oldStatus, newStatus, templateConfig } = props;
  const [currentView, setView] = useState("mappManually");
  const [selectedStatus, setSelectedStatus] = useState({});
  const [statusArr, setStatusArr] = useState([]);
  const [btnQuery, setBtnQuery] = useState("");
  const handleChangeTab = (event, nextView) => {
    setView(nextView);
  };

  const generateStatuses = () => {
    /* function for generating the array of status and returing array to the data prop for multi drop down */
    let statuses = newStatus.statusList.map((s, i) => {
      //   return generateSelectData(s.statusTitle, s.statusId, s.statusId, s.statusId, s);
      return {
        label: s.statusTitle,
        value: s.statusId,
        icon: <RoundIcon htmlColor={s.statusColor} className={classes.statusesIcon} />,
      };
    });
    return statuses;
  };

  const handleSelectStatus = (key, opt, s) => {
    // setSelectedStatus(status);
    let option = {
      status: s.status,
      linkStatus: opt,
    };
    let arr = statusArr.map(ele => {
      if (ele.status.statusCode == option.status.statusCode) return option;
      else return ele;
    });
    setStatusArr(arr);
  };

  const setStatusArray = () => {
    let arr = []
    oldStatus.statusList.map(s => {
      if(associatedTasks.filter(el => el.status == s.statusId).length > 0){
        arr.push({
        status: s,
        linkStatus: null,
      });
      }
    });
    setStatusArr(arr);
  };

  useEffect(() => {
    setStatusArray();
  }, []);

  const handleSuccess = () => {
    let arr = [];
    if (currentView == "mappManually") {
      statusArr.map(s => {
        arr.push({
          statusIDFrom: s.status.statusId,
          statusIDTo: s.linkStatus.value,
        });
      });
    }
    if (currentView == "allOpen") {
      let defaultStatus = newStatus.statusList.find(o => o.isDefault) || {};
      statusArr.map(s => {
        arr.push({
          statusIDFrom: s.status.statusId,
          statusIDTo: defaultStatus.statusId,
        });
      });
    }

    let obj = {
      fromTemplateID: oldStatus.templateId,
      toTemplateID: newStatus.templateId,
      issetDefaultStatus: true,
      mappingStatus: arr,
    };
    setBtnQuery("progress");
    props.handleSaveAsTemplate(obj);
  };
  const getTaskCountByStatus = (status) => {
    return associatedTasks.filter(el => el.status == status.statusId).length
  }
  const filterTask = () => {
    let total = []
    if(templateConfig.contextView == templatesConstants.WORKSPACE_VIEW){
      tasks.map(el => {
          if(el.projectId == null){
            total.push(el);
          } else{
            let project = projects.find(item => item.projectId == el.projectId);
            if(project && project.projectTemplateId == oldStatus.templateId){
              total.push(el)
            }
          }
      })
    }else{
      tasks.map(el => {
        if(el.projectId != null && templateConfig.project.projectId == el.projectId){
            total.push(el)
        }
      })
    }
    return total;
  }
  const getDisabled = () => {
    if(currentView == "mappManually")
      return statusArr.some(item => item.linkStatus==null)
    return false;
  }

  const defaultStatus = newStatus.statusList.find(s => s.isDefault) || {};
  const associatedTasks = filterTask();//contextView == templatesConstants.WORKSPACE_VIEW ? tasks.filter(el => el.projectId == null) :tasks.filter(el => el.projectId != null)

  return (
    <>
      <DefaultDialog
        title={"Status Mapping"}
        sucessBtnText="Done"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={handleClose}>
        <div className={classes.statusMapCnt}>
          <div className={classes.heading}>
            <span className={classes.title}>
              <span
                className={classes.title}
                style={{ color: "#202020", fontWeight: 800 }}>{`${associatedTasks.length} tasks`}</span>
              {` will be affected by mapping the default statuses.`}
            </span>
          </div>
          <div className={classes.heading}>
            <span className={classes.title}>How should we handle these tasks?</span>
          </div>

          <Divider style={{ margin: "20px 0" }} />

          <div className={classes.toggleContainer}>
            <ToggleButtonGroup
              value={currentView}
              exclusive
              onChange={handleChangeTab}
              classes={{ root: classes.toggleBtnGroup }}>
              <ToggleButton
                value="mappManually"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                Map Status Manually
              </ToggleButton>
              <ToggleButton
                value="allOpen"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                Change all to{" "}
                <Brightness1Icon
                  style={{ color: defaultStatus.statusColor }}
                  className={classes.openIcon}
                />{" "}
                {`${defaultStatus.statusTitle}`}
              </ToggleButton>
            </ToggleButtonGroup>
          </div>
          {currentView == "mappManually" && (
            <>
              <div className={classes.labelCnt}>
                {" "}
                <span className={classes.labelTitleLeft}>Old Status</span>
                <span className={classes.labelTitleRight}>New Status</span>
              </div>
              {statusArr.map((s, index) => {
                return (
                  <div className={classes.statusesCnt} key={index}>
                    <CustomButton
                      btnType="white"
                      variant="contained"
                      style={{
                        fontSize: "14px",
                        color: "#202020",
                        border: "none",
                        background: "#F6F6F6",
                        marginLeft: 0,
                        fontFamily: theme.typography.fontFamilyLato,
                        fontWeight: theme.palette.fontWeightMedium,
                        padding: "7px 50px 7px 9px",
                        width: 170,
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "end",
                      }}>
                      <Brightness1Icon
                        className={classes.statusesIcon}
                        style={{ color: s.status.statusColor }}
                      />
                     <span className={classes.statusTitle} title={s.status.statusTitle}>{s.status.statusTitle}</span>
                    </CustomButton>
                    <span className={classes.taskCountTitle}>
                      {" "}
                      {getTaskCountByStatus(s.status)} tasks <ArrowForwardIcon className={classes.arrowRight} />{" "}
                    </span>

                    <SelectSearchDropdown /* Issue Status select drop down */
                      data={generateStatuses}
                      label={null}
                      selectChange={(key, opt) => {
                        handleSelectStatus(key, opt, s);
                      }}
                      type="status"
                      selectedValue={s.linkStatus}
                      placeholder={"Select status"}
                      icon={true}
                      isMulti={false}
                      isDisabled={false}
                      styles={{ width: 170, margin: 0 }}
                    />
                  </div>
                );
              })}
            </>
          )}
        </div>
        <ButtonActionsCnt
          cancelAction={handleClose}
          successAction={handleSuccess}
          successBtnText={"Done"}
          cancelBtnText={"Cancel"}
          btnType="blue"
          btnQuery={btnQuery}
          disabled={getDisabled()}
        />
      </DefaultDialog>
    </>
  );
}

WorkspaceMappingDialog.defaultProps = {
  classes: {},
  theme: {},
  open: false,
  handleClose: () => {},
  tasks: [],
  projects: [],
  oldStatus: {},
  newStatus: {},
};
const mapStateToProps = state => {
  return {
    tasks: state.tasks.data || [],
    projects: state.projects.data || [],
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(WorkspaceMappingDialog);
