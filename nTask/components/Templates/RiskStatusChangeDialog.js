import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import DefaultDialog from "../Dialog/Dialog";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Divider from "@material-ui/core/Divider";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import CustomButton from "../Buttons/CustomButton";
import RoundIcon from "@material-ui/icons/Brightness1";
import isEmpty from "lodash/isEmpty";

type RiskStatusChangeDialogProps = {
  classes: Object,
  theme: Object,
  open: Boolean,
  handleClose: Function,
  oldStatus: Object,
  newTemplateItem: Object,
  handleSaveAsTemplate: Function
};

function RiskStatusChangeDialog(props: RiskStatusChangeDialogProps) {
  const { classes, theme, handleClose, open, tasks, oldStatus, newTemplateItem } = props;
  const [currentView, setView] = useState("mappManually");
  const [selectedStatus, setSelectedStatus] = useState({});
  const [btnQuery, setBtnQuery] = useState("");
  const handleChangeTab = (event, nextView) => {
    setView(nextView);
  };

  const generateStatuses = () => {
    /* function for generating the array of status and returing array to the data prop for multi drop down */
    let statuses = newTemplateItem.statusList.map((s, i) => {
      //   return generateSelectData(s.statusTitle, s.statusId, s.statusId, s.statusId, s);
      return {
        label: s.statusTitle,
        value: s.statusId,
        icon: <RoundIcon htmlColor={s.statusColor} className={classes.statusesIcon} />,
      };
    });
    return statuses;
  };

  const handleSelectStatus = (key, opt, s) => {
    setSelectedStatus(opt);
  };

  useEffect(() => {
  }, []);

  const handleSuccess = () => {
    let newStatus = "";
    if (currentView == "mappManually") {
      newStatus = newTemplateItem.statusList.find(o => o.statusId == selectedStatus.value) || {};
    }
    if (currentView == "allOpen") {
      newStatus = newTemplateItem.statusList.find(o => o.isDefault) || {};
    }
    setBtnQuery("progress");
    props.handleSaveAsTemplate(newStatus);
  };

  const defaultStatus = newTemplateItem.statusList.find(s => s.isDefault) || {};
  let buttonDisable = currentView == "allOpen" || !isEmpty(selectedStatus) ? false : true;
  return (
    <>
      <DefaultDialog
        title={"Status Mapping"}
        sucessBtnText="Done"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={handleClose}>
        <div className={classes.statusMapCnt}>
          <div className={classes.heading}>
            <span className={classes.title}>
              {/* <span
                className={classes.title}
                style={{ color: "#202020", fontWeight: 800 }}>{`${tasks.length} tasks`}</span> */}
              {`Looks like you are trying to change the project.`}
            </span>
          </div>
          <div className={classes.heading}>
            <span className={classes.title}>How would we handle this task?</span>
          </div>

          <Divider style={{ margin: "20px 0" }} />

          <div className={classes.toggleContainer}>
            <ToggleButtonGroup
              value={currentView}
              exclusive
              onChange={handleChangeTab}
              classes={{ root: classes.toggleBtnGroup }}>
              <ToggleButton
                value="mappManually"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                Map Status Manually
              </ToggleButton>
              <ToggleButton
                value="allOpen"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                Change all to{" "}
                <Brightness1Icon
                  style={{ color: defaultStatus.statusColor }}
                  className={classes.openIcon}
                />{" "}
                {`${defaultStatus.statusTitle}`}
              </ToggleButton>
            </ToggleButtonGroup>
          </div>
          {currentView == "mappManually" && (
            <>
              <div className={classes.labelCnt}>
                {" "}
                <span className={classes.labelTitleLeft}>Old Status</span>
                <span className={classes.labelTitleRight}>New Status</span>
              </div>
              {/* {statusArr.map((s, index) => {
                return ( */}
                  <div className={classes.statusesCnt} style={{justifyContent: "space-between", padding:"0px 20px"}} key={0}>
                    <CustomButton
                      btnType="white"
                      variant="contained"
                      style={{
                        fontSize: "14px",
                        color: "#202020",
                        border: "none",
                        background: "#F6F6F6",
                        marginLeft: 0,
                        fontFamily: theme.typography.fontFamilyLato,
                        fontWeight: theme.palette.fontWeightMedium,
                        // padding: "7px 50px 7px 9px",
                        width: 170,
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "start",
                      }}>
                      <Brightness1Icon
                        className={classes.statusesIcon}
                        style={{ color: oldStatus.statusColor }}
                      />
                      {oldStatus.statusTitle}
                    </CustomButton>

                    <SelectSearchDropdown /* Issue Status select drop down */
                      data={generateStatuses}
                      label={null}
                      selectChange={(key, opt) => {
                        handleSelectStatus(key, opt);
                      }}
                      type="status"
                      selectedValue={selectedStatus}
                      placeholder={"Select status"}
                      icon={true}
                      isMulti={false}
                      isDisabled={false}
                      styles={{ width: 170, margin: 0 }}
                    />
                  </div>
                {/* );
              })} */}
              </>
          )}
        </div>
        <ButtonActionsCnt
          cancelAction={handleClose}
          successAction={handleSuccess}
          successBtnText={"Done"}
          cancelBtnText={"Cancel"}
          btnType="blue"
          btnQuery={btnQuery}
          disabled={buttonDisable}
        />
      </DefaultDialog>
    </>
  );
}

RiskStatusChangeDialog.defaultProps = {
  classes: {},
  theme: {},
  open: false,
  handleClose: () => {},
  tasks: [],
  oldStatus: {},
  newTemplateItem: {},
  handleSaveAsTemplate: () => {},

};
const mapStateToProps = state => {
  return {
    tasks: state.tasks.data || [],
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(RiskStatusChangeDialog);
