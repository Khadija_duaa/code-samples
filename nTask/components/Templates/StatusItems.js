/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/aria-role */
import React, { useEffect, useState, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import DragIndicator from "@material-ui/icons/DragIndicator";
import SvgIcon from "@material-ui/core/SvgIcon";
import PropTypes from "prop-types";
import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";
import StatusIcon from '@material-ui/icons/Brightness1';
import classes from "./style";
import DefaultTextField from "../Form/TextField";
import { validateTitleField } from "../../utils/formValidations";
import CrossIcon from "../Icons/CrossIcon";
import SaveIcon from "../Icons/SaveIcon";
import DefaultButton from "../Buttons/DefaultButton";
import CustomIconButton from "../Buttons/CustomIconButton";
import CustomTooltip from "../Tooltip/Tooltip";
import RemoveIcon2 from "../Icons/RemoveIcon2";
import ColorPickerDropdown from '../Dropdown/ColorPicker/Dropdown';
import DeleteModal from "./deleteModal";
import DeleteDefaultModal from "./DeleteDefaultModal";
import templatesConstants from "../../utils/constants/templatesConstants";
import {FormattedMessage, injectIntl} from "react-intl";

function StatusItems(props) {
  const { classes, templateItem, theme, isArchivedSelected, permission, taskPer, draggable, templatePermission, templateConfig, tasks, projects, intl } = props;
  const deletePermission = true;

  const [statusInputError, setStatusInputError] = useState(false);
  const [statusInputErrorMessage, setStatusInputErrorMessage] = useState(false);
  const [edit, setEdit] = useState({});
  const [statusTitle, setStatusTitle] = useState("");
  const [selectedColor, setSelectedColor] = useState('#bcbcbc');
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [openDeleteDefault, setOpenDeleteDefault] = useState(false);
  const [deleteItem, setDeleteItem] = useState({});
  const [deletedStatus, setDeletedStatus] = useState({});

  const handleItemNameInput = event => {
    setStatusTitle(event.target.value);
    setStatusInputError(false);
    setStatusInputErrorMessage("");
  };
  const filterTask = () => {
    let total = []
    if(templateConfig.contextView == templatesConstants.WORKSPACE_VIEW){
      tasks.map(el => {
          if(el.projectId == null){
            if(templateConfig.workspaceDefaultTemplate.templateId == templateItem.customstatus.templateId) total.push(el);
          } else{
            let project = projects.find(item => item.projectId == el.projectId);
            if(project && project.projectTemplateId == templateItem.customstatus.templateId){
              total.push(el)
            }
          }
      })
    }else{
      tasks.map(el => {
        if(el.projectId != null && templateConfig.project.projectId == el.projectId){
            total.push(el)
        }
      })
    }
    return total;
  }
  const associatedTasks = filterTask();//templateConfig.contextView == templatesConstants.WORKSPACE_VIEW ? tasks.filter(el => el.projectId == null) :tasks.filter(el => el.projectId != null)

  const enterKeyHandler = (event, list, index) => {
    if (event.key === "Enter") {
      saveHandler(list, index);
    }
  };

  const saveItem = (list, index) => {
    saveHandler(list, index);
  };

  const saveHandler = (item, index) => {
    // if (event.key === "Enter") {
    const validationObj = validateTitleField("Checklist", statusTitle, true, 80);
    if (validationObj.isError) {
      setStatusInputError(validationObj.isError);
      setStatusInputErrorMessage(validationObj.message);
      return;
    }

    const newItem = cloneDeep(templateItem);
    newItem.customstatus.statusList = newItem.customstatus.statusList.map(el => {
      if (el.statusCode == item.statusCode) el.statusTitle = statusTitle;
      return el;
    });
    setEdit({});
    props.updateItem(newItem, "editItem", response => {
      setStatusTitle("");
      setStatusInputError(false);
      setStatusInputErrorMessage("");
      setEdit({});
    });
  };

  const cancelItem = () => {
    event.stopPropagation();
    setEdit({});
    setStatusTitle("");
  };

  const onDragEnd = result => {
    const { source, destination, draggableId, type } = result;

    const items = reorder(templateItem.customstatus.statusList, source.index, destination.index);

    const status = cloneDeep(templateItem);
    status.customstatus.statusList = items;

    props.updateItem(status, "reorderItems", response => {});
  };
  const reorder = (list, startIndex, endIndex) => {
    const result = [...list];
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  };
  const DefaultItem = item => {
    const newItem = cloneDeep(templateItem);
    newItem.customstatus.statusList.map(el => {
      if (el.statusCode == item.statusCode) el.isDefault = true;
      else el.isDefault = false;
      return el;
    });
    props.updateItem(newItem, "updateItem", response => {});
  };
  const DeleteItem = item => {
    const status = cloneDeep(templateItem);
    // updateItem.statusList = updateItem.statusList.filter(el => el.statusId != item.statusId );
    // props.updateItem(updateItem, "deleteItem", response => {});
  //   openDeleteDialog(item, newItem);
  // };
  // const openDeleteDialog = (item, status) => {
    if (!item.isDefault && !item.isDoneState) {
      setOpenDeleteModal(true);
    }
    if (item.isDefault || item.isDoneState) {
      setOpenDeleteDefault(true);
    }
    setDeletedStatus(cloneDeep(status));
    setDeleteItem(cloneDeep(item));
  };
  const DoneItem = item => {
    const newItem = cloneDeep(templateItem);
    newItem.customstatus.statusList.map(el => {
      if (el.statusCode == item.statusCode) el.isDoneState = true;
      else el.isDoneState = false;
      return el;
    });
    props.updateItem(newItem, "updateItem", response => {});
  };

  const handleEditName = (e, item) => {
    e.stopPropagation();
    setEdit(item);
    setStatusTitle(item.statusTitle);
    // setEditStatusTitle(item.statusTitle);
  };
  //Handle color change function
  const onColorChange = (color,item) => {
    // setSelectedColor(color);
    const newItem = cloneDeep(templateItem);
    newItem.customstatus.statusList.map(el => {
      if (el.statusId == item.statusId) el.statusColor = color;
      return el;
    });
    props.updateItem(newItem, "updateItem", response => {});
  };
  const handleCloseDeleteDialog = () => {
    setOpenDeleteModal(false);
    };
  const deleteStatusHandler = data => {
    let newStatus = data.templateItem;
    newStatus.mappingStatus = [...newStatus.mappingStatus, ...data.mappingStatus];
    props.updateItem(newStatus, "updateItem", response => {});
    handleCloseDeleteDialog();
    // props.DeleteAndMapStatus(
    //   data,
    //   succ => {
    //     handleCloseDeleteDialog();
    //   },
    //   fail => {}
    // );
  };
  const deleteDoneDefaultStatus = data => {
    let newStatus = data.templateItem;
    newStatus.mappingStatus = [...newStatus.mappingStatus, ...data.mappingStatus];
    props.updateItem(newStatus, "updateItem", response => {});
    handleCloseDeleteDefaultDialog();
    // props.DeleteAndMapStatus(
    //   data,
    //   succ => {
    //     handleCloseDeleteDefaultDialog();
    //   },
    //   fail => {}
    // );
  };
  const handleCloseDeleteDefaultDialog = () => {
    setOpenDeleteDefault(false);
  };

  const getTaskCountByStatus = (statusId) => {
    return associatedTasks.filter(el => el.status == statusId).length
  }

  return (
    <>
      <Grid xs={12} item classes={{ item: classes.statuslistCnt }}>
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="droppable">
            {(provided, snapshot) => (
              <div>
                <ul className={classes.statusItemList} ref={provided.innerRef}>
                  {templateItem.customstatus.statusList &&
                    templateItem.customstatus.statusList.map((list, index) => (
                      <Draggable key={list.statusCode} draggableId={list.statusCode} index={index}>
                        {(provided, snapshot) => (
                          <li
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            className={classes.statusItem} 
                            key={index}>
                            {!isEmpty(edit) && edit.statusCode === list.statusCode ? (
                              <div className={classes.editCnt}>
                                <DefaultTextField
                                  fullWidth
                                  errorState={statusInputError}
                                  errorMessage={statusInputErrorMessage}
                                  transparentInput
                                  formControlStyles={{height: 35}}
                                  defaultProps={{
                                    id: "statusEdit",
                                    onChange: handleItemNameInput,
                                    onKeyUp: e => enterKeyHandler(e, list, index),
                                    // onBlur: e => setEdit({}),
                                    autoFocus: true,
                                    value: statusTitle,
                                    placeholder: intl.formatMessage({id:"workspace-settings.task-status.placeholder",defaultMessage: "Enter status name"}),
                                    // placeholder: "Enter status name",
                                    inputProps: { maxLength: 30 },
                                    disabled: false,
                                  }}
                                />
                                <div
                                  style={{
                                    marginLeft: 10,
                                    marginTop: 7,
                                    display: "flex",
                                  }}>
                                  <DefaultButton
                                    buttonType="smallIconBtn"
                                    disableRipple
                                    onClick={cancelItem}>
                                    <SvgIcon viewBox="0 0 22 22">
                                      <CrossIcon />
                                    </SvgIcon>
                                  </DefaultButton>
                                  <DefaultButton
                                    buttonType="smallIconBtn"
                                    disableRipple
                                    style={{ marginLeft: 10 }}
                                    onClick={e => saveItem(list, index)}>
                                    <SvgIcon viewBox="0 0 22 22">
                                      <SaveIcon />
                                    </SvgIcon>
                                  </DefaultButton>
                                </div>
                              </div>
                            ) : (
                              <div className={classes.statusBody}>
                                <div className={classes.statusTitleCnt}>
                                  <span
                                    {...provided.dragHandleProps}
                                    className={classes.dragHandleCnt}>
                                    <DragIndicator
                                      htmlColor={theme.palette.secondary.light}
                                      className={classes.dragHandle}
                                    />
                                  </span>
                                  <div className={classes.statusNameCnt}>
                                    <ColorPickerDropdown
                                      theme={theme}
                                      classes={classes}
                                      selectedColor={selectedColor}
                                      onSelect={(color)=>onColorChange(color,list)}
                                      iconRenderer={
                                        <StatusIcon
                                          className={classes.statusIcon}
                                          style={{ color: list.statusColor || "gray" }}
                                        />
                                      }
                                      style={{ marginRight: 10 }}
                                      disabled={!permission.templateStatus.isAllowEdit}
                                    />
                                    {/* <StatusIcon className={classes.statusIcon} style={{color: list.statusColor || 'gray'}} /> */}
                                    <Typography
                                      variant="body1"
                                      className={classes.statusItemTxt}
                                      title={list.statusTitle}
                                      onClick={e => {
                                        permission.templateStatus.isAllowEdit
                                          ? handleEditName(e, list)
                                          : null;
                                      }}>
                                      {list.statusTitle}
                                    </Typography>
                                  </div>
                                </div>
                                <div style={{ display: "flex" }}>
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection:
                                        list.isDefault && !list.isDoneState ? "row-reverse" : "row",
                                    }}>
                                    <span
                                      className={
                                        list.isDefault
                                          ? classes.defaultChipsTag
                                          : classes.grayChipsTag
                                      }
                                      style={{ marginLeft: 5 }}
                                      onClick={() => {
                                       permission.changeDefaultStatus.cando ?  DefaultItem(list, index) : null;
                                      }}>
                                      Initial
                                    </span>
                                    <span
                                      className={
                                        list.isDoneState
                                          ? classes.doneChipsTag
                                          : classes.grayChipsTag
                                      }
                                      style={{ marginLeft: 5 }}
                                      onClick={() => {
                                        permission.changeDoneStatus.cando ? DoneItem(list, index) : null;
                                      }}>
                                      Final
                                    </span>
                                  </div>
                                  { getTaskCountByStatus(list.statusId) > 0 ? (
                                    <span className={classes.taskCountChipsTag}>
                                      {`${getTaskCountByStatus(list.statusId)} tasks`}
                                    </span>
                                  ) : null}
                                </div>
                                {templateItem.customstatus.statusList.length > 1 &&
                                  permission.templateStatus.isAllowDelete && (
                                    <div>
                                      <CustomTooltip
                                        helptext={<FormattedMessage id="team-settings.billing.bill-dialog.remove-button.label" defaultMessage="Remove"/>}
                                        iconType="help"
                                        placement="top"
                                        style={{
                                          color: theme.palette.common.white,
                                        }}>
                                        <CustomIconButton
                                          onClick={() => {
                                            DeleteItem(list, index);
                                          }}
                                          btnType="transparent"
                                          variant="contained"
                                          style={{
                                            width: 20,
                                            height: 20,
                                            borderRadius: 4,
                                            color: "#7E7E7E",
                                            marginLeft: 7,
                                          }}>
                                          <SvgIcon viewBox="0 0 24 24">
                                            <RemoveIcon2 />
                                          </SvgIcon>
                                        </CustomIconButton>
                                      </CustomTooltip>
                                    </div>
                                  )}
                              </div>
                            )}
                          </li>
                        )}
                      </Draggable>
                    ))}
                </ul>
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </Grid>
      {/* It is called when user delete status Item */}
      {openDeleteModal && (
        <DeleteModal
          open={openDeleteModal}
          handleClose={handleCloseDeleteDialog}
          oldTemplate={deletedStatus}
          item={deleteItem}
          deleteStatus={deleteStatusHandler}
          templateConfig = {templateConfig}
        />
      )}
      {/* It is called when user delete status Item */}
      {openDeleteDefault && (
        <DeleteDefaultModal
          open={openDeleteDefault}
          handleClose={handleCloseDeleteDefaultDialog}
          oldTemplate={deletedStatus}
          item={deleteItem}
          deleteStatus={deleteDoneDefaultStatus}
          templateConfig = {templateConfig}
        />
      )}
    </>
  );
}
StatusItems.defaultProps = {
  templateItem: {},
  updateItem(e) {
    return e;
  },
  isArchivedSelected: false,
  closeTaskDetailsPopUp(e) {
    return e;
  },
  permission: {},
  tasks: [],
  projects: [],
};
StatusItems.propTypes = {
  templateItem: PropTypes.object.isRequired,
  updateItem: PropTypes.func.isRequired,
  isArchivedSelected: PropTypes.bool.isRequired,
  closeTaskDetailsPopUp: PropTypes.func.isRequired,
  permission: PropTypes.object.isRequired,
  templatePermission: PropTypes.object.isRequired,
  templateConfig: PropTypes.object.isRequired,
};
const mapStateToProps = state => {
  return {
    members: state.profile.data.member.allMembers || [],
    user: state.profile.data,
    tasks: state.tasks.data || [],
    projects: state.projects.data || [],
  };
};
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {}),
  withStyles(classes, { withTheme: true })
)(StatusItems);
