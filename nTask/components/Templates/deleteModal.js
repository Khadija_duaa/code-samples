import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import DefaultDialog from "../Dialog/Dialog";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Divider from "@material-ui/core/Divider";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import CustomButton from "../Buttons/CustomButton";
import { generateSelectData } from "../../helper/generateSelectData";
import RoundIcon from "@material-ui/icons/Brightness1";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../Icons/DeleteIcon";
import Typography from "@material-ui/core/Typography";
import templatesConstants from "../../utils/constants/templatesConstants";

type DeleteModalProps = {
  classes: Object,
  theme: Object,
  open: Boolean,
  handleClose: Function,
  oldTemplate: Object,
  item: Object,
};

function DeleteModal(props: DeleteModalProps) {
  const { classes, theme, handleClose, open, tasks, projects, oldTemplate, item, templateConfig } = props;
  const [currentView, setView] = useState("mappManually");
  const [selectedStatus, setSelectedStatus] = useState(null);
  const [statusArr, setStatusArr] = useState([]);

  const handleChangeTab = (event, nextView) => {
    setView(nextView);
  };
  const filterTask = () => {
    let total = []
    if(templateConfig.contextView == templatesConstants.WORKSPACE_VIEW){
      tasks.map(el => {
          if(el.projectId == null){
           if(el.status == item.statusId)
            total.push(el);
          } else{
            let project = projects.find(item => item.projectId == el.projectId);
            if(project && project.projectTemplateId == oldTemplate.customstatus.templateId && el.status == item.statusId){
              total.push(el)
            }
          }
      })
    }else{
      tasks.map(el => {
        if(el.projectId != null && templateConfig.project.projectId == el.projectId && el.status == item.statusId){
            total.push(el)
        }
      })
    }
    return total;
  }
  const associatedTasks = filterTask();//contextView == templatesConstants.WORKSPACE_VIEW ? tasks.filter(el => el.projectId == null && el.statusTitle == item.statusTitle) :tasks.filter(el => el.projectId != null && el.statusTitle == item.statusTitle)
  const generateStatuses = () => {
    /* function for generating the array of status and returing array to the data prop for multi drop down */
    let filterStatus = oldTemplate.customstatus.statusList.filter(s => s.statusCode !== item.statusCode);
    let statuses = filterStatus.map((s, i) => {
      //   return generateSelectData(s.statusTitle, s.statusId, s.statusId, s.statusId, s);
      return {
        label: s.statusTitle,
        value: s.statusId,
        icon: <RoundIcon htmlColor={s.statusColor} className={classes.statusesIcon} />,
      };
    });
    return statuses;
  };

  const handleSelectStatus = (key, opt, s) => {
    setSelectedStatus(opt);
  };

  useEffect(() => {}, []);

  const handleSuccess = () => {
    let arr = [];
    if (currentView == "mappManually") {
      if (associatedTasks.length == 0) {
        arr = [];
      } else {
        arr.push({
          statusIDFrom: item.statusId,
          statusIDTo: selectedStatus.value,
        });
      }
    }
    let filterStatus = oldTemplate;
    filterStatus.customstatus.statusList = filterStatus.customstatus.statusList.filter(
      s => s.statusCode !== item.statusCode
    );

    let obj = {
      templateItem: filterStatus,
      mappingStatus: arr,
    };
    props.deleteStatus(obj);
  };

  const defaultStatus = oldTemplate.customstatus.statusList.find(s => s.isDefault) || {};

  return (
    <>
      <DefaultDialog
        title={associatedTasks.length == 0 ? "Delete Status" : "Status Mapping"}
        sucessBtnText="Done"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={handleClose}>
        {associatedTasks.length !== 0 && (
          <div className={classes.statusMapCnt}>
            <div className={classes.heading}>
              <span className={classes.title}>
                <span
                  className={classes.title}
                  style={{ color: "#202020", fontWeight: 800 }}>{`${associatedTasks.length} tasks`}</span>
                {` will be affected by deleting`}{" "}
                <span className={classes.title} style={{ color: "#202020", fontWeight: 800 }}>
                  {item.statusTitle}
                </span>{" "}
                {` status.`}
              </span>
            </div>
            <div className={classes.heading}>
              <span className={classes.title}>How should we handle these tasks?</span>
            </div>

            <Divider style={{ margin: "20px 0" }} />

            <div className={classes.toggleContainer}>
              <ToggleButtonGroup
                value={currentView}
                exclusive
                onChange={handleChangeTab}
                classes={{ root: classes.toggleBtnGroup }}>
                <ToggleButton
                  value="mappManually"
                  classes={{
                    root: classes.toggleButton,
                    selected: classes.toggleButtonSelected,
                  }}>
                  Map Status Manually
                </ToggleButton>
                <ToggleButton
                  value="allOpen"
                  classes={{
                    root: classes.toggleButton,
                    selected: classes.toggleButtonSelected,
                  }}>
                  Change all to{" "}
                  <Brightness1Icon
                    className={classes.openIcon}
                    style={{ color: defaultStatus.statusColor }}
                  />{" "}
                  {`${defaultStatus.statusTitle}`}
                </ToggleButton>
              </ToggleButtonGroup>
            </div>
            {currentView == "mappManually" && (
              <>
                <div className={classes.labelCnt}>
                  {" "}
                  <span className={classes.labelTitleLeft}>Old Status</span>
                  <span className={classes.labelTitleRight}>New Status</span>
                </div>
                <div className={classes.statusesCnt}>
                  <CustomButton
                    btnType="white"
                    variant="contained"
                    style={{
                      fontSize: "14px !important",
                      color: "#202020",
                      border: "none",
                      background: "#F6F6F6",
                      marginLeft: 0,
                      fontFamily: theme.typography.fontFamilyLato,
                      fontWeight: theme.palette.fontWeightMedium,
                      padding: "7px 50px 7px 9px",
                      width: 170,
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "end",
                    }}>
                    <Brightness1Icon
                      className={classes.statusesIcon}
                      style={{ color: item.statusColor }}
                    />
                    {item.statusTitle}
                  </CustomButton>
                  <span className={classes.taskCountTitle}>
                    {" "}
                    {associatedTasks.length} tasks <ArrowForwardIcon className={classes.arrowRight} />{" "}
                  </span>

                  <SelectSearchDropdown /* Issue Status select drop down */
                    data={generateStatuses}
                    label={null}
                    selectChange={(key, opt) => {
                      handleSelectStatus(key, opt);
                    }}
                    type="status"
                    selectedValue={selectedStatus}
                    placeholder={"Select status"}
                    icon={true}
                    isMulti={false}
                    isDisabled={false}
                    styles={{ width: 170, margin: 0 }}
                  />
                </div>
              </>
            )}
          </div>
        )}
        {associatedTasks.length == 0 && (
          <>
            {" "}
            <div className={classes[`centerAlignContent`]}>
              <div className={classes.deleteIconCnt}>
                <SvgIcon
                  viewBox="0 0 512 512"
                  className={classes.deleteConfirmationIcon}
                  htmlColor={theme.palette.error.main}>
                  <DeleteIcon />
                </SvgIcon>
              </div>

              <span className={classes.title}>
                {`Are you sure you want to delete the`}{" "}
                <span
                  className={classes.title}
                  style={{ color: "#202020", fontWeight: 800 }}>{` ${item.statusTitle}`}</span>
                {` status.`}
              </span>
            </div>{" "}
          </>
        )}
        <ButtonActionsCnt
          cancelAction={handleClose}
          successAction={handleSuccess}
          successBtnText={associatedTasks.length == 0 ? "Delete" : "Done"}
          cancelBtnText={"Cancel"}
          btnType={associatedTasks.length == 0 ? "danger" : "blue"}
          btnQuery={""}
          disabled={false}
        />
      </DefaultDialog>
    </>
  );
}

DeleteModal.defaultProps = {
  classes: {},
  theme: {},
  open: false,
  handleClose: () => {},
  tasks: [],
  projects: [],
  oldTemplate: {},
  item: {},
};
const mapStateToProps = state => {
  return {
    tasks: state.tasks.data || [],
    projects: state.projects.data || [],
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(DeleteModal);
