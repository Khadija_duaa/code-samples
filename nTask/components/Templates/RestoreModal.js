import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import DefaultDialog from "../Dialog/Dialog";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Divider from "@material-ui/core/Divider";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import CustomButton from "../Buttons/CustomButton";
import { generateSelectData } from "../../helper/generateSelectData";
import RoundIcon from "@material-ui/icons/Brightness1";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../Icons/DeleteIcon";
import Typography from "@material-ui/core/Typography";

type RestoreModalProps = {
  classes: Object,
  theme: Object,
  open: Boolean,
  handleClose: Function,
  item: Object,
};

function RestoreModal(props: RestoreModalProps) {
  const { classes, theme, handleClose, open, handleRestoreStatus, item } = props;

  const handleSuccess = () => {
    handleRestoreStatus(item);
  };

  return (
    <>
      <DefaultDialog
        title={"Restore Statuses"}
        sucessBtnText="Done"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={handleClose}>
        <div className={classes.statusMapCnt} style={{ minHeight: 0 }}>
          <div className={classes.heading}>
            <span className={classes.title} style={{ textAlign: "center", padding: "0px 30px" }}>
              All changes made to this template will be discarded and this action cannot be undone.
            </span>
          </div>
          <div className={classes.heading}>
            <span className={classes.title}>
              {`Are you sure you want to restore statuses of `}{" "}
              <span className={classes.title} style={{ color: "#202020", fontWeight: 800 }}>
                {item.statusTitle}
              </span>
              {`?`}
            </span>
          </div>
        </div>

        <ButtonActionsCnt
          cancelAction={handleClose}
          successAction={handleSuccess}
          successBtnText={"Done"}
          cancelBtnText={"Cancel"}
          btnType={"blue"}
          btnQuery={""}
          disabled={false}
        />
      </DefaultDialog>
    </>
  );
}

RestoreModal.defaultProps = {
  classes: {},
  theme: {},
  open: false,
  handleClose: () => {},
  item: {},
};
const mapStateToProps = state => {
  return {};
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(RestoreModal);
