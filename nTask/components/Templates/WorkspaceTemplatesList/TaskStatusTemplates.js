import React, { useState, useEffect } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ArrowRight from "@material-ui/icons/ArrowRight";
import ArrowDown from "@material-ui/icons/ArrowDropDown";
import ListSubheader from "@material-ui/core/ListSubheader";
import { withStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import debounce from "lodash/debounce";
import clsx from "clsx";
import taskStatusTemplatesStyles from "./taskStatusTemplates.style";
import CustomButton from "../../Buttons/CustomButton";
import DefaultTextField from "../../Form/TextField";
import { validateTitleField } from "../../../utils/formValidations";
// import constants from "../../../redux/constants/types";
import templatesConstants from "../../../utils/constants/templatesConstants";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import UnPlanned from "../../../Views/billing/UnPlanned/UnPlanned";
import {FormattedMessage} from "react-intl";
import Modal from "@material-ui/core/Modal";
import {TRIALPERIOD} from '../../constants/planConstant';
/*  {props}
classes: style classes
onClick: contains function to be called when user click on main task status tab
data: all status templates data
onStatusSelect: function to be called when user select a status template
addTemplate: function called when user adds a template
selectedTemplate: contains object of currently selected status template
*/

function TaskStatus({
  classes,
  onClick,
  data,
  onStatusSelect,
  addTemplate,
  selectedTemplate,
  templateConfig,
  selectedIndex,
  intl,
  permission
}) {
  const [open, setOpen] = useState(false);
  const [templateName, setTemplateName] = useState("");
  const [addNewTemplate, setAddNewTemplate] = useState(false);
  const [selectedGroup, setSelectedGroup] = useState({});
  const [templateInputError, setTemplateInputError] = useState("");
  const [templateInputErrorMessage, setTemplateInputErrorMessage] = useState("");
  const [templates, setTemplates] = useState({
    ntaskDefault: [],
    otherTemplates: [],
    mySavedTemplates: [],
    totalTemplates: 0,
  });
  const [unplanned, setUnplanned] = useState(false);

  // In this useEffect data is generated according to template types
  useEffect(() => {
    if (data.length) {
      const ntaskDefault = data.filter(t => t.groupName === "nTask Standard");
      const otherTemplates = data.filter(
        t => t.groupName !== "nTask Standard" && t.groupName !== templatesConstants.SAVED_TEMPLATES
      );
      const mySavedTemplates = data.filter(t => t.groupName === templatesConstants.SAVED_TEMPLATES);
      let totalTemplates = 0;
      [otherTemplates, mySavedTemplates].forEach(g => {
        g.forEach(f => {
          totalTemplates += f.templates.length;
        });
      });
      setTemplates({
        ntaskDefault,
        otherTemplates,
        mySavedTemplates,
        totalTemplates,
      });
    }
    // if (templateConfig.contextView == templatesConstants.PROJECT_VIEW) {
    //   setOpen(true);
    // }
  }, [data]);

  const handleClick = e => {
    if(!teamCanView("customStatusAccess"))
    setUnplanned(true);
 else  {
    setOpen(true);
    onClick(e);
 }
  };
  // Handle click on add template button click to enable template input
  const handleAddTemplateClick = () => {
    setAddNewTemplate(true);
  };
  // handle Tempalte name input
  const handleTemplateName = e => {
    setTemplateName(e.target.value);
    setTemplateInputError("");
    setTemplateInputErrorMessage("")
  };
  // hide the input on blur if the value of input is null
  const handleInputBlur = () => {
    if (!templateName) {
      setAddNewTemplate(false);
    }
  };
  // handle Template group click to expand
  const handleTemplateGroupClick = (e, template) => {
    // Checking if user click on the template which is already expanded so it should be collapsed
    if (selectedGroup.groupName === template.groupName) {
      setSelectedGroup({});
    } else {
      setSelectedGroup(template);
    }
  };
  const templateExist = newName => {
    let nameExist = false;
    data.forEach(group => {
      if (group.groupName === templatesConstants.SAVED_TEMPLATES) {
        group.templates.forEach(temp => {
          if (temp.name.toLowerCase() === newName.toLowerCase()) nameExist = true;
        });
      }
    });
    return nameExist;
  };
  const addNewItem = debounce(() => {
    const validationObj = validateTitleField("a template name.", templateName, true, 250);
    const alreadyExist = templateExist(templateName);
    if (validationObj.isError) {
      setTemplateInputError(validationObj.isError);
      setTemplateInputErrorMessage(validationObj.message);
      return;
    }
    if (alreadyExist) {
      setTemplateInputError(true);
      setTemplateInputErrorMessage("Template name already exists.");
      return;
    }
    const newItem = {
      name: templateName.trim(),
    };
    if (addTemplate)
      addTemplate(
        newItem,
        response => {
          setTemplateName("");
          setTemplateInputError(false);
          setTemplateInputErrorMessage("");
        },
        fail => {
          setTemplateName("");
          setTemplateInputError(false);
          setTemplateInputErrorMessage("");
        }
      );
  }, 300);
  const enterKeyHandler = event => {
    if (event.key === "Enter") {
      addNewItem();
    }
  };
  useEffect(() => {
    if (selectedIndex !== 3) {
      setOpen(false);
    }
  }, [selectedIndex]);
  const handleClose = e => {
    setUnplanned(false);
  };
  return (
    <>
      {/* Main Task Status Tab Start */}
      {/* <ListItem button className={classes.defaultTemplateNameListItem}>
        <ListItemText
          primary="test"
          classes={{
            root: classes.templateNameListItemTextRoot,
            primary: clsx({
              [classes.templateNameListItemText]: true,
              [classes.templateNameListItemTextSelected]: false,
            }),
          }}
        /> */}
        {/* {s.templates.some(t => t.isWorkspaceDefault) &&
                      s.groupName !== selectedGroup.groupName && (
                        <span className={classes.defaultChipsTag}>
                          {templateConfig.contextView == templatesConstants.WORKSPACE_VIEW
                            ? "Default"
                            : "Active"} */}
        {/* </span> */}
        {/* )} */}
      {/* </ListItem> */}
      <List
        component="div"
        disablePadding
        // subheader={
        //   <ListSubheader className={classes.listSubHeader} component="div">
        //     TEMPLATES ({templates.totalTemplates})
        //   </ListSubheader>
        // }
        >
        {/* <ListItem button className={classes.defaultTemplateNameListItem}>
          <ListItemText
            primary="test"
            classes={{
              root: classes.templateNameListItemTextRoot,
              primary: clsx({
                [classes.templateNameListItemText]: true,
                [classes.templateNameListItemTextSelected]: false,
              }),
            }}
          /> */}
          {/* {s.templates.some(t => t.isWorkspaceDefault) &&
                      s.groupName !== selectedGroup.groupName && (
                        <span className={classes.defaultChipsTag}>
                          {templateConfig.contextView == templatesConstants.WORKSPACE_VIEW
                            ? "Default"
                            : "Active"} */}
          {/* </span> */}
          {/* )} */}
        {/* </ListItem> */}
        {/* {templateConfig.contextView == templatesConstants.WORKSPACE_VIEW && ( */}
          <ListItem
            button
            onClick={handleClick}
            className={open ? classes.mainListItemSelected : classes.mainListItem}>
            <ListItemText
              primary={intl.formatMessage({
                id: "workspace-settings.task-status.label",
                defaultMessage: "Task Status",
              })}
              classes={{ primary: classes.mainListItemText }}
            />
          </ListItem>
        {/* )} */}
        {/* Main Task Status Tab End */}

        {/* List of all status starts */}
        <Collapse in={open} timeout="auto" unmountOnExit>
          {/* Standarad Ntask List Item Start */}
          {templates.ntaskDefault.map(s => {
            return (
              s.groupName === "nTask Standard" && (
          <ListItem  style={{height: 10}}>
            <ListItemText priamry={`TEMPLATES (${templates.totalTemplates})`}  classes={{ primary: classes.listTemplateHeader}}>
            {`TEMPLATES (${templates.totalTemplates})`}
            </ListItemText>
          </ListItem>
              ));})}
          {templates.ntaskDefault.map(t => {
            return (
              t.groupName === "nTask Standard" && (
                <ListItem
                  button
                  onClick={() => onStatusSelect(t.templates[0])}
                  className={classes.defaultTemplateNameListItem}>
                  <ListItemText
                    primary="Standard"
                    classes={{ primary: classes.defaultTemplateNameListItemText }}
                  />
                  {t.templates[0].isWorkspaceDefault && (
                    <span className={classes.defaultChipsTag}>
                      Default
                      {/* {templateConfig.contextView == templatesConstants.WORKSPACE_VIEW
                        ? "Default"
                        : "Active"} */}
                    </span>
                  )}
                </ListItem>
              )
            );
          })}
          {/* Standarad Ntask List Item End */}

          {/* Pre build templates starts */}
          <List component="div" disablePadding>
            {/* Other Templates List Item Start */}
            {templates.otherTemplates.map(t => {
              const isGroupSelected =
                selectedTemplate.customstatus &&
                selectedTemplate.customstatus.groupName === t.groupName;
              return (
                <>
                  <ListItem
                    button
                    onClick={e => {
                      handleTemplateGroupClick(e, t);
                    }}
                    className={classes.templateNameListItem}>
                    <ListItemIcon className={classes.templateNameListItemIcon}>
                      {t.groupName === selectedGroup.groupName ? <ArrowDown /> : <ArrowRight />}
                    </ListItemIcon>
                    <ListItemText
                      primary={t.groupName}
                      classes={{
                        root: classes.templateNameListItemTextRoot,
                        primary: clsx({
                          [classes.templateNameListItemText]: true,
                          [classes.templateNameListItemTextSelected]: isGroupSelected,
                        }),
                      }}
                    />
                    {t.templates.some(t => t.isWorkspaceDefault) &&
                      t.groupName !== selectedGroup.groupName && (
                        <span className={classes.defaultChipsTag}>
                          Default
                          {/* {templateConfig.contextView == templatesConstants.WORKSPACE_VIEW
                            ? "Default"
                            : "Active"} */}
                        </span>
                      )}
                  </ListItem>

                  <Collapse
                    in={t.groupName === selectedGroup.groupName}
                    timeout="auto"
                    unmountOnExit>
                    {t.templates.map(s => {
                      const isStatusSelected =
                        selectedTemplate.customstatus && selectedTemplate.customstatus.id === s.id;
                      return (
                        <ListItem
                          button
                          disableRipple
                          className={clsx({
                            [classes.nestedTemplateNameListItem]: true,
                            [classes.nestedTemplateNameListItemSelected]: isStatusSelected,
                          })}
                          onClick={() => onStatusSelect(s)}>
                          <ListItemText
                            primary={s.name}
                            classes={{ primary: classes.nestedTemplateNameListItemText }}
                          />
                          {s.isWorkspaceDefault && (
                            <span className={classes.defaultChipsTag}>
                              Default
                              {/* {templateConfig.contextView == templatesConstants.WORKSPACE_VIEW
                                ? "Default"
                                : "Active"} */}
                            </span>
                          )}
                        </ListItem>
                      );
                    })}
                  </Collapse>
                </>
              );
            })}
            {/* Other Templates List Item End */}
            {/* my saved Templates List Item Start */}
            {templates.mySavedTemplates.map(t => {
              const isGroupSelected =
                selectedTemplate.customstatus &&
                selectedTemplate.customstatus.groupName === t.groupName;
              return (
                <>
                  <ListItem
                    button
                    onClick={e => {
                      handleTemplateGroupClick(e, t);
                    }}
                    className={classes.templateNameListItem}>
                    <ListItemIcon className={classes.templateNameListItemIcon}>
                      {t.groupName === selectedGroup.groupName ? <ArrowDown /> : <ArrowRight />}
                    </ListItemIcon>
                    <ListItemText
                      primary={t.groupName}
                      classes={{
                        root: classes.templateNameListItemTextRoot,
                        primary: clsx({
                          [classes.templateNameListItemText]: true,
                          [classes.templateNameListItemTextSelected]: isGroupSelected,
                        }),
                      }}
                    />
                    {t.templates.some(t => t.isWorkspaceDefault) &&
                      t.groupName !== selectedGroup.groupName && (
                        <span className={classes.defaultChipsTag}>
                          Default
                          {/* {templateConfig.contextView == templatesConstants.WORKSPACE_VIEW
                            ? "Default"
                            : "Active"} */}
                        </span>
                      )}
                  </ListItem>

                  <Collapse
                    in={t.groupName === selectedGroup.groupName}
                    timeout="auto"
                    unmountOnExit>
                    {t.templates.map(s => {
                      const isStatusSelected =
                        selectedTemplate.customstatus && selectedTemplate.customstatus.id === s.id;
                      return (
                        <ListItem
                          button
                          disableRipple
                          className={clsx({
                            [classes.nestedTemplateNameListItem]: true,
                            [classes.nestedTemplateNameListItemSelected]: isStatusSelected,
                          })}
                          onClick={() => onStatusSelect(s)}>
                          <ListItemText
                            primary={s.name}
                            classes={{ primary: classes.nestedTemplateNameListItemText }}
                          />
                          {s.isWorkspaceDefault && (
                            <span className={classes.defaultChipsTag}>
                              Default
                              {/* {templateConfig.contextView == templatesConstants.WORKSPACE_VIEW
                                ? "Default"
                                : "Active"} */}
                            </span>
                          )}
                        </ListItem>
                      );
                    })}
                    <ListItem className={classes.addNewTemplateListItem}>
                      {addNewTemplate ? (
                        <DefaultTextField
                          label={false}
                          fullWidth
                          error={!!templateInputError}
                          errorState={templateInputError}
                          errorMessage={templateInputErrorMessage}
                          formControlStyles={{ marginBottom: 0 }}
                          transparentInput
                          defaultProps={{
                            id: "templateName",
                            type: "text",
                            onBlur: handleInputBlur,
                            onChange: handleTemplateName,
                            autoFocus: true,
                            onKeyUp: enterKeyHandler,
                            value: templateName,
                            placeholder: "Enter template name",
                            inputProps: {
                              maxLength: 80,
                              style: { padding: "3px 2px" },
                            },
                          }}
                        />
                      ) : templateConfig.contextView == templatesConstants.WORKSPACE_VIEW ? (
                        permission.createRenameTemplate.cando && <CustomButton
                          variant="text"
                          btnType="plain"
                          onClick={handleAddTemplateClick}>
                          <AddIcon className={classes.addIconTemplate} />
                          <span className={classes.addTemplateTitle}>Create Template</span>
                        </CustomButton>
                      ) : null}
                    </ListItem>
                  </Collapse>
                </>
              );
            })}
            {/* my saved Templates List Item End */}
          </List>
          {/* Pre built templates ends */}
        </Collapse>
        {/* List of all status ends */}
      </List>
      {unplanned  && (
          <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={unplanned}
          onClose={handleClose}
          onBackdropClick={handleClose}
          BackdropProps={{
            classes: {
              root: classes.unplannedRoot,
            },
          }}
        >
          <div
            className={classes.unplannedMain}
            style={{ top: 270 }}
          >
              <div className={classes.unplannedCnt}>
                <UnPlanned
                  feature="business"
                  titleTxt={
                    <FormattedMessage
                      id="common.discovered-dialog.business-title"
                      defaultMessage="Wow! You've discovered a Business feature!"
                    />
                  }
                  boldText={"Custom Task Status"}
                  descriptionTxt={
                    <FormattedMessage
                      id="common.discovered-dialog.list.custom-status.label"
                      values={{n: <span style={{color: '#0090ff'}}>nTask Business</span>}}
                      defaultMessage={"is available on our Business Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Business features."}
                      values={{TRIALPERIOD: TRIALPERIOD}}
                    />
                  }
                  showBodyImg={false}
                  showDescription={true}
                  selectUpgrade={handleClose}
                  // imgUrl={template}
                />
              </div>
            </div>
            </Modal>
        )}
    </>
  );
}
TaskStatus.defaultProps = {
  templateConfig: {},
  selectedIndex: 0,
  intl: {},
  permission: null
};

export default withStyles(taskStatusTemplatesStyles, { withTheme: true })(TaskStatus);
