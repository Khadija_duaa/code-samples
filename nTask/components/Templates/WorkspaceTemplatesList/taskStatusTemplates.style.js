const taskStatusTemplatesStyles = theme => ({
  nested: {
    paddingLeft: 10,
  },
  mainListItem: {
    padding: "6px 10px",
    marginBottom: 4,
    "&:hover": {
      background: "transparent",
    },
  },
  mainListItemSelected: {
    padding: "6px 10px",
    background: theme.palette.background.azureLight,
    borderRadius: 4,
    "&:focus": {
      background: theme.palette.background.azureLight,
    },
    "& $mainListItemText": {
      color: "#0090ff",
      fontWeight: 600,
    },
  },
  mainListItemText: {
    fontSize: "14px !important",
    color: theme.palette.text.grayDarker,
    fontFamily: theme.typography.fontFamilyLato,
  },
  defaultTemplateNameListItem: {
    padding: "1px 21px",
    marginBottom: 4,
    color: theme.palette.text.darkGray,
    "&:hover": {
      background: "transparent",
    },
  },
  defaultTemplateNameListItemText: {
    fontSize: "14px !important",
    color: theme.palette.text.darkGray,
    fontFamily: theme.typography.fontFamilyLato,
  },
  templateNameListItem: {
    padding: "1px 21px 1px 10px",
    marginBottom: 4,
    color: theme.palette.text.darkGray,
    "&:hover": {
      background: "transparent",
    },
  },
  templateNameListItemTextRoot: {
    padding: 0,
    margin: 0,
  },
  templateNameListItemIcon: {
    marginRight: 0,
    minWidth: 0,
  },
  templateNameListItemText: {
    fontSize: "14px !important",
    color: theme.palette.text.darkGray,
    padding: 0,
    fontFamily: theme.typography.fontFamilyLato,
  },
  templateNameListItemTextSelected: {
    color: theme.palette.text.azure,
  },
  nestedTemplateNameListItem: {
    padding: "2px 21px 2px 40px",
    marginBottom: 4,
    color: theme.palette.text.darkGray,
    background: "transparent !important",
    "&:hover": {
      background: "transparent",
    },
  },
  addNewTemplateListItem: {
    padding: "1px 40px 1px 28px",
    marginBottom: 4,
    color: theme.palette.text.darkGray,
    background: "transparent !important",
    "&:hover": {
      background: "transparent",
    },
  },
  nestedTemplateNameListItemText: {
    fontSize: "14px !important",
    color: theme.palette.text.darkGray,
    fontFamily: theme.typography.fontFamilyLato,
  },
  nestedTemplateNameListItemSelected: {
    "& $nestedTemplateNameListItemText": {
      color: theme.palette.text.azure,
    },
  },
  listSubHeader: {
    color: theme.palette.text.disabled,
    fontSize: "11px !important",
    padding: "0 21px",
    margin: "8px 0 6px 0",
    lineHeight: "normal",
  },
  listTemplateHeader: {
    color: theme.palette.text.disabled,
    fontSize: "11px !important",
    padding: "0 5px",
    margin: "8px 0 6px 0",
    lineHeight: "normal",
  },
  defaultChipsTag: {
    background: theme.palette.background.btnBlue,
    padding: "3px 7px",
    color: theme.palette.common.white,
    fontSize: "11px !important",
    borderRadius: 4,
  },
  addTemplateTitle: {
    fontSize: "14px !important",
    color: "#646464",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    // marginRight: 12,
    cursor: "pointer",
  },
  addIconTemplate: {
    fontSize: "18px !important",
    color: "#646464",
    marginLeft: -8,
    marginRight: 2,
  },
  unplannedRoot: {
    backgroundColor: "rgba(0, 0, 0, 0)",
  },
  unplannedMain: {
    fontFamily: theme.typography.fontFamilyLato,
    background: theme.palette.common.white,
    width: 356,
    height: "auto",
    top: 250,
    left: 200,
    padding: 20,
    position: "absolute",
    borderRadius: 4,
    boxShadow: `-1px 1px 5px -2px rgba(0,0,0,0.32)`,
  },
});

export default taskStatusTemplatesStyles;
