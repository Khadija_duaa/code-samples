import React, { useState, useEffect } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ArrowRight from "@material-ui/icons/ArrowRight";
import ArrowDown from "@material-ui/icons/ArrowDropDown";
import ListSubheader from "@material-ui/core/ListSubheader";
import { withStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import debounce from "lodash/debounce";
import clsx from "clsx";
import taskStatusTemplatesStyles from "./taskStatusTemplates.style";
import CustomButton from "../../Buttons/CustomButton";
import DefaultTextField from "../../Form/TextField";
import { validateTitleField } from "../../../utils/formValidations";
import constants from "../../../redux/constants/types";
import templatesConstants from "../../../utils/constants/templatesConstants";
import templateTypes from "../../../utils/constants/templatesTypes";

/*  {props}
classes: style classes
onClick: contains function to be called when user click on main task status tab
data: all status templates data
onTemplateSelect: function to be called when user select a status template
addTemplate: function called when user adds a template
selectedTemplate: contains object of currently selected status template
*/

function TaskStatus({
  classes,
  onClick,
  data,
  onTemplateSelect,
  addTemplate,
  selectedTemplate,
  templateConfig,
  selectedIndex,
  intl,
  permission,
  workspaceDefaultTemplate
}) {
  const [open, setOpen] = useState(false);
  const [templateName, setTemplateName] = useState("");
  const [addNewTemplate, setAddNewTemplate] = useState(false);
  const [selectedGroup, setSelectedGroup] = useState({});
  const [templateInputError, setTemplateInputError] = useState("");
  const [templateInputErrorMessage, setTemplateInputErrorMessage] = useState("");
  const [templates, setTemplates] = useState({
    ntaskDefault: [],
    otherTemplates: [],
    mySavedTemplates: [],
    totalTemplates: 0,
  });

  // In this useEffect data is generated according to template types
  useEffect(() => {
    if (data.length) {
      // const ntaskDefault = data.filter(t => t.groupName === "nTask Standard");
      const ntaskDefault = data
        .filter(t => t.groupName === "nTask Standard")
        .map(el => {
          el.templates = el.templates.filter(item => item.isWorkspaceDefault == false);
          return el;
        });
      // const otherTemplates = data.filter(
      //   t => t.groupName !== "nTask Standard" && t.groupName !== templatesConstants.SAVED_TEMPLATES
      // );
      const otherTemplates = data
        .filter(
          t =>
            t.groupName !== "nTask Standard" && t.groupName !== templatesConstants.SAVED_TEMPLATES
        )
        .map(el => {
          el.templates = el.templates.filter(item => item.isWorkspaceDefault == false);
          return el;
        });
      // const mySavedTemplates = data.filter(t => t.groupName === templatesConstants.SAVED_TEMPLATES);
      const mySavedTemplates = data
        .filter(t => t.groupName === templatesConstants.SAVED_TEMPLATES)
        .map(el => {
          el.templates = el.templates.filter(item => item.isWorkspaceDefault == false);
          return el;
        });
      let totalTemplates = 0;
      [otherTemplates, mySavedTemplates].forEach(g => {
        g.forEach(f => {
          totalTemplates += f.templates.length;
        });
      });
      setTemplates({
        ntaskDefault,
        otherTemplates,
        mySavedTemplates,
        totalTemplates,
      });
    }
    // if (templateConfig.contextView == templatesConstants.PROJECT_VIEW) {
    setOpen(true);
    // }
  }, [data]);

  const handleClick = e => {
    setOpen(true);
    onClick(e);
  };
  // Handle click on add template button click to enable template input
  const handleAddTemplateClick = () => {
    setAddNewTemplate(true);
  };
  // handle Tempalte name input
  const handleTemplateName = e => {
    setTemplateName(e.target.value);
  };
  // hide the input on blur if the value of input is null
  const handleInputBlur = () => {
    if (!templateName) {
      setAddNewTemplate(false);
    }
  };
  // handle Template group click to expand
  const handleTemplateGroupClick = (e, template) => {
    // Checking if user click on the template which is already expanded so it should be collapsed
    if (selectedGroup.groupName === template.groupName) {
      setSelectedGroup({});
    } else {
      setSelectedGroup(template);
    }
  };
  const templateExist = newName => {
    let nameExist = false;
    data.forEach(group => {
      if (group.groupName === templatesConstants.SAVED_TEMPLATES) {
        group.templates.forEach(temp => {
          if (temp.templates === newName) nameExist = true;
        });
      }
    });
    return nameExist;
  };
  const addNewItem = debounce(() => {
    const validationObj = validateTitleField("to-do list item.", templateName, true, 250);
    const alreadyExist = templateExist(templateName);
    if (validationObj.isError) {
      setTemplateInputError(validationObj.isError);
      setTemplateInputErrorMessage(validationObj.message);
      return;
    }
    if (alreadyExist) {
      setTemplateInputError(true);
      setTemplateInputErrorMessage("Template name already exists.");
      return;
    }
    const newItem = {
      name: templateName.trim(),
    };
    if (addTemplate)
      addTemplate(
        newItem,
        response => {
          setTemplateName("");
          setTemplateInputError(false);
          setTemplateInputErrorMessage("");
        },
        fail => {
          setTemplateName("");
          setTemplateInputError(false);
          setTemplateInputErrorMessage("");
        }
      );
  }, 300);
  const enterKeyHandler = event => {
    if (event.key === "Enter") {
      addNewItem();
    }
  };
  useEffect(() => {
    if (selectedIndex !== 3) {
      setOpen(false);
    }
  }, [selectedIndex]);
  return (
    <>
      {/* Main Task Status Tab Start */}
      {templateConfig.projectTemplate && templateConfig.projectTemplate.category == 3 && (
        <ListItem
          button
          onClick={() => onTemplateSelect(templateConfig.projectTemplate)}
          className={classes.defaultTemplateNameListItem}>
          <ListItemText
            primary={`Custom (${templateConfig.projectTemplate.name == "nTask Standard" ?'Standard' : templateConfig.projectTemplate.name})`}
            classes={{
              root: classes.templateNameListItemTextRoot,
              primary: clsx({
                [classes.templateNameListItemText]: true,
                [classes.templateNameListItemTextSelected]: true,
              }),
            }}
          />
          {/* {s.templates.some(t => t.isWorkspaceDefault) &&
                      s.groupName !== selectedGroup.groupName && (
                        <span className={classes.defaultChipsTag}>
                          {templateConfig.contextView == templatesConstants.WORKSPACE_VIEW
                            ? "Default"
                            : "Active"} */}
          {/* </span> */}
          {/* )} */}
        </ListItem>
      )}
      <List
        component="div"
        disablePadding
        subheader={
          <ListSubheader className={classes.listSubHeader} component="div">
            TEMPLATES ({templates.totalTemplates})
          </ListSubheader>
        }>
        {templateConfig.workspaceDefaultTemplate ? (
          <ListItem
            button
            onClick={() => onTemplateSelect(templateConfig.workspaceDefaultTemplate)}
            className={classes.defaultTemplateNameListItem}>
            <ListItemText
              // primary={`Workspace Default (${templateConfig.workspaceDefaultTemplate.name})`}
              primary={`Workspace Default (Standard)`}
              classes={{
                root: classes.templateNameListItemTextRoot,
                primary: clsx({
                  [classes.templateNameListItemText]: true,
                  [classes.templateNameListItemTextSelected]: templateConfig.projectTemplate.templateId == templateConfig.workspaceDefaultTemplate.templateId,
                }),
              }}
            />

            
              {templateConfig.projectTemplate &&
              templateConfig.projectTemplate.templateId ==
                templateConfig.workspaceDefaultTemplate.templateId
                ? <span className={classes.defaultChipsTag}>Active</span>
                : null}
           
          </ListItem>
        ) : null}
        {/* {templateConfig.contextView == templatesConstants.WORKSPACE_VIEW && (
          <ListItem
            button
            onClick={handleClick}
            className={open ? classes.mainListItemSelected : classes.mainListItem}>
            <ListItemText
              primary={intl.formatMessage({
                id: "workspace-settings.task-status.label",
                defaultMessage: "Task Statuses",
              })}
              classes={{ primary: classes.mainListItemText }}
            />
          </ListItem>
        )} */}
        {/* Main Task Status Tab End */}

        {/* List of all status starts */}
        <Collapse in={open} timeout="auto" unmountOnExit>
          {/* Standarad Ntask List Item Start */}
          {templates.ntaskDefault.length > 0 &&
            templates.ntaskDefault[0].templates.length > 0 &&
            templates.ntaskDefault.map(s => {
              return (
                s.groupName === "nTask Standard" && (
                  <ListItem
                    button
                    onClick={() => onTemplateSelect(s.templates[0])}
                    className={classes.defaultTemplateNameListItem}>
                    <ListItemText
                      primary="Standard"
                      classes={{ primary: classes.defaultTemplateNameListItemText }}
                    />
                    {templateConfig.projectTemplate &&
                    templateConfig.projectTemplate.templateId == s.templates[0].templateId ? (
                      <span className={classes.defaultChipsTag}>Active</span>
                    ) : !templateConfig.projectTemplate && s.templates[0].isWorkspaceDefault ? (
                      <span className={classes.defaultChipsTag}>Active</span>
                    ) : null}
                  </ListItem>
                )
              );
            })}
          {/* Standarad Ntask List Item End */}

          {/* Pre build templates starts */}
          <List component="div" disablePadding>
            {/* Other Templates List Item Start */}
            {templates.otherTemplates.map(s => {
              const isGroupSelected =
                ((selectedTemplate.customstatus &&
                  selectedTemplate.customstatus.groupName === s.groupName &&
                  selectedTemplate.customstatus.category !== 3 &&
                  selectedTemplate.customstatus.isWorkspaceDefault == false) ||
                  s.templates.some(t => t.templateId == templateConfig.projectTemplate.templateId))
              return (
                <>
                  <ListItem
                    button
                    onClick={e => {
                      handleTemplateGroupClick(e, s);
                    }}
                    className={classes.templateNameListItem}>
                    <ListItemIcon className={classes.templateNameListItemIcon}>
                      {s.groupName === selectedGroup.groupName ? <ArrowDown /> : <ArrowRight />}
                    </ListItemIcon>
                    <ListItemText
                      primary={s.groupName}
                      classes={{
                        root: classes.templateNameListItemTextRoot,
                        primary: clsx({
                          [classes.templateNameListItemText]: true,
                          [classes.templateNameListItemTextSelected]: isGroupSelected,
                        }),
                      }}
                    />
                      {s.templates.some(t => templateConfig.projectTemplate.templateId == t.templateId) && s.groupName !== selectedGroup.groupName && <span className={classes.defaultChipsTag}>Active</span> }
                  </ListItem>

                  <Collapse
                    in={s.groupName === selectedGroup.groupName}
                    timeout="auto"
                    unmountOnExit>
                    {s.templates.map(l => {
                      const isStatusSelected =
                        selectedTemplate.customstatus && selectedTemplate.customstatus.templateId === l.templateId;
                      return (
                        <ListItem
                          button
                          disableRipple
                          className={clsx({
                            [classes.nestedTemplateNameListItem]: true,
                            [classes.nestedTemplateNameListItemSelected]: isStatusSelected,
                          })}
                          onClick={() => onTemplateSelect(l)}>
                          <ListItemText
                            primary={l.name}
                            classes={{ primary: classes.nestedTemplateNameListItemText }}
                          />
                          {templateConfig.projectTemplate &&
                          templateConfig.projectTemplate.templateId == l.templateId ? (
                            <span className={classes.defaultChipsTag}>Active</span>
                          ) : !templateConfig.projectTemplate && l.isWorkspaceDefault ? (
                            <span className={classes.defaultChipsTag}>Active</span>
                          ) : null}
                          {/* {l.isWorkspaceDefault && (
                            <span className={classes.defaultChipsTag}>
                              Active
                            </span>
                          )} */}
                        </ListItem>
                      );
                    })}
                  </Collapse>
                </>
              );
            })}
            {/* Other Templates List Item End */}
            {/* my saved Templates List Item Start */}
            {templates.mySavedTemplates.map(s => {
              const isGroupSelected =
                ((selectedTemplate.customstatus &&
                selectedTemplate.customstatus.groupName === s.groupName &&
                selectedTemplate.customstatus.category !== 3 &&
                selectedTemplate.customstatus.isWorkspaceDefault == false) ||
                s.templates.some(t => t.templateId == templateConfig.projectTemplate.templateId))
              return (
                <>
                  <ListItem
                    button
                    onClick={e => {
                      handleTemplateGroupClick(e, s);
                    }}
                    className={classes.templateNameListItem}>
                    <ListItemIcon className={classes.templateNameListItemIcon}>
                      {s.groupName === selectedGroup.groupName ? <ArrowDown /> : <ArrowRight />}
                    </ListItemIcon>
                    <ListItemText
                      primary={s.groupName}
                      classes={{
                        root: classes.templateNameListItemTextRoot,
                        primary: clsx({
                          [classes.templateNameListItemText]: true,
                          [classes.templateNameListItemTextSelected]: isGroupSelected,
                        }),
                      }}
                    />
                    {s.templates.some(t => t.isWorkspaceDefault) &&
                      s.groupName !== selectedGroup.groupName && (
                        <span className={classes.defaultChipsTag}>Active</span>
                      )}
                  </ListItem>

                  <Collapse
                    in={s.groupName === selectedGroup.groupName}
                    timeout="auto"
                    unmountOnExit>
                    {s.templates.map(l => {
                      const isTemplateSelected =
                        selectedTemplate.customstatus && selectedTemplate.customstatus.templateId === l.templateId;
                      return (
                        <ListItem
                          button
                          disableRipple
                          className={clsx({
                            [classes.nestedTemplateNameListItem]: true,
                            [classes.nestedTemplateNameListItemSelected]: isTemplateSelected,
                          })}
                          onClick={() => onTemplateSelect(l)}>
                          <ListItemText
                            primary={l.name}
                            classes={{ primary: classes.nestedTemplateNameListItemText }}
                          />
                          {templateConfig.projectTemplate &&
                          templateConfig.projectTemplate.templateId == l.templateId ? (
                            <span className={classes.defaultChipsTag}>Active</span>
                          ) : !templateConfig.projectTemplate && l.isWorkspaceDefault ? (
                            <span className={classes.defaultChipsTag}>Active</span>
                          ) : null}

                          {/* {l.isWorkspaceDefault && (
                            <span className={classes.defaultChipsTag}>
                              Active
                            </span>
                          )} */}
                        </ListItem>
                      );
                    })}
                    {/* <ListItem className={classes.addNewTemplateListItem}>
                      {addNewTemplate ? (
                        <DefaultTextField
                          label={false}
                          fullWidth
                          error={!!templateInputError}
                          errorState={templateInputError}
                          errorMessage={templateInputErrorMessage}
                          formControlStyles={{ marginBottom: 0 }}
                          transparentInput
                          defaultProps={{
                            id: "templateName",
                            type: "text",
                            onBlur: handleInputBlur,
                            onChange: handleTemplateName,
                            autoFocus: true,
                            onKeyUp: enterKeyHandler,
                            value: templateName,
                            placeholder: "Enter template name",
                            inputProps: {
                              maxLength: 80,
                              style: { padding: "3px 2px" },
                            },
                          }}
                        />
                      ) : templateConfig.contextView == templatesConstants.WORKSPACE_VIEW ? (
                        <CustomButton
                          variant="text"
                          btnType="plain"
                          onClick={handleAddTemplateClick}>
                          <AddIcon className={classes.addIconTemplate} />
                          <span className={classes.addTemplateTitle}>Create Template</span>
                        </CustomButton>
                      ) : null}
                    </ListItem> */}
                  </Collapse>
                </>
              );
            })}
            {/* my saved Templates List Item End */}
          </List>
          {/* Pre built templates ends */}
        </Collapse>
        {/* List of all status ends */}
      </List>
    </>
  );
}
TaskStatus.defaultProps = {
  templateConfig: {},
  selectedIndex: 0,
  intl: {},
  permission: null
};

export default withStyles(taskStatusTemplatesStyles, { withTheme: true })(TaskStatus);
