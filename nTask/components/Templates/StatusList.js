import React, { useEffect, useState } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import classes from "./style";
import StatusCreate from "./StatusCreate";
import StatusItems from "./StatusItems";
import CustomButton from "../Buttons/CustomButton";
import WorkspacMappingDialog from "./WorkspaceMappingDialog";
import RestoreModal from "./RestoreModal";
import debounce from "lodash/debounce";
import { validateTitleField } from "../../utils/formValidations";
import constants from "../../redux/constants/types";
import DefaultTextField from "../Form/TextField";
import DeleteTemplateDialog from "./DeleteTemplateDialog";
import SaveNewTemplateModal from "./SaveNewTemplateModal";
import NotSavedTemplateModal from "./NotSavedTemplateModal";
import cloneDeep from "lodash/cloneDeep";
import DropdownMenu from "../Dropdown/DropdownMenu";
import InfoIcon from "@material-ui/icons/Info";
import Divider from "@material-ui/core/Divider";
import templateTypes from "../../utils/constants/templatesTypes";
import templatesConstants from "../../utils/constants/templatesConstants";
import moment from "moment";
import UseTemplateMappingDialog from "./UseTemplateMappingDialog";
import CustomTooltip from "../../components/Tooltip/Tooltip";

function StatusList(props) {
  const {
    isArchivedSelected,
    permission,
    templateItem,
    classes,
    draggable,
    style = {},
    intl,
    workSpaceStatus,
    templatePermission,
    templateConfig,
    theme,
    allMembers,
    tasks,
    projects
  } = props;

  const [workspaceDefault, setWorkspaceDefault] = useState("");
  const [workspaceDefaultDialog, setWorkspaceDefaultDialog] = useState(false);
  const [openUseTemplateDialog, setOpenUseTemplateDialog] = useState(false);
  const [openUseCustomTemplateDialog, setOpenUseCustomTemplateDialog] = useState(false);
  const [wsDeleteTemplate, setWSDeleteTemplate] = useState(false);
  const [oldStatus, setOldStatus] = useState({});
  const [restoredItem, setRestoredItem] = useState({});
  const [restoreModal, setRestoreModal] = useState(false);
  const [isRenameTemplate, setIsRenameTemplate] = useState(false);
  const [templateInputError, setTemplateInputError] = useState("");
  const [templateInputErrorMessage, setTemplateInputErrorMessage] = useState("");
  // const [templateRename, setTemplateRename] = useState("");
  const [isTemplateChange, setIsTemplateChange] = useState(false);
  const [updatedItem, setUpdatedItem] = useState(cloneDeep(templateItem));
  const [openCreateTemplate, setOpenCreateTemplate] = useState(false);
  const [openNotSaved, setOpenNotSaved] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [deleteTemplateBtn, setDeleteTemplateBtn] = useState("");

  useEffect(() => {
    setUpdatedItem(cloneDeep(templateItem));
    setIsTemplateChange(false);
    return () => {
      // if (isTemplateChange) {
      //   setOpenNotSaved(true);
      // }
    };
  }, [props.templateItem]);

  const updateItem = (item, mode, callback) => {
    if (templateConfig.contextView == templatesConstants.WORKSPACE_VIEW) {
      setUpdatedItem(cloneDeep(item));
      setIsTemplateChange(true);
    } else if (templateConfig.contextView == templatesConstants.PROJECT_VIEW) {
      setUpdatedItem(cloneDeep(item));
      setIsTemplateChange(true);
    }
  };
  const WorkspaceDefaultHandler = () => { };
  const DeleteTemplateHandler = () => { };
  const openSetWorkspaceDefaultDialog = () => {
    if (filterTask().length !== 0) {
      setWorkspaceDefaultDialog(true);
    } else {
      let obj = {
        fromTemplateID: oldStatus.templateId,
        toTemplateID: updatedItem.customstatus.templateId,
        issetDefaultStatus: true,
        mappingStatus: [],
      };
      handleSaveTemplate(obj);
    }
  };
  const filterTask = () => {
    let total = []
    if (templateConfig.contextView == templatesConstants.WORKSPACE_VIEW) {
      tasks.map(el => {
        if (el.projectId == null) {
          total.push(el);
        } else {
          let project = projects.find(item => item.projectId == el.projectId);
          if (project && project.projectTemplateId == oldStatus.templateId) {
            total.push(el)
          }
        }
      })
    } else {
      tasks.map(el => {
        if (el.projectId != null && templateConfig.project.projectId == el.projectId) {
          total.push(el)
        }
      })
    }
    return total;
  }
  const handleCloseDialog = () => {
    setWorkspaceDefaultDialog(false);
  };
  const openWSTemplateDeleteDialog = () => {
    setWSDeleteTemplate(true);
    setDeleteTemplateBtn("progress");
  };
  const handleDeleteCloseDialog = () => {
    setWSDeleteTemplate(false);
    setDeleteTemplateBtn("");
  };

  const setDefaultStatus = () => {
    let defaultStatus = {};
    if (templateConfig.contextView == templatesConstants.PROJECT_VIEW && templateConfig.projectTemplate) {
      defaultStatus = templateConfig.projectTemplate;
    } else {
      defaultStatus = templateConfig.workspaceDefaultTemplate;
      // props.workSpaceStatus.map(w =>
      //   w.statusList.map(t => {
      //     if (t.isDefault) defaultStatus = t;
      //   })
      // );
    }
    setOldStatus(cloneDeep(defaultStatus));
  };

  useEffect(() => {
    setDefaultStatus();
    setIsTemplateChange(false);
  }, [props.workSpaceStatus, props.workspaceDefault, props.templateConfig.projectTemplate]);
  /**
   * it is called when user change workspace default template and select new template as workspace default
   * @param {} data 
   */
  const handleSaveTemplate = data => {
    props.MapTaskWithSetDefaultTemplate(
      data,
      succ => {
        handleCloseDialog();
        // setDefaultStatus();
      },
      fail => { }
    );
  };

  // const handleClickRestoreTemplate = item => {
  //   setRestoredItem(item);
  //   setRestoreModal(true);
  // };
  const handleDiscardTemplate = () => {
    setUpdatedItem(cloneDeep(templateItem));
    setIsTemplateChange(false);
    setIsRenameTemplate(false);
  };
  const handleOpenSaveDialog = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
    // setTemplateNewname(updatedItem.customstatus.statusTitle + ' Copy');
    setOpenCreateTemplate(true);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
    setOpenCreateTemplate(false);
  };
  const handleCloseRestoreDialog = () => {
    setRestoredItem({});
    setRestoreModal(false);
  };
  // const handleRestoreStatus = () => {
  //   let obj = {
  //     statusId: restoredItem.id,
  //   };
  //   props.restoreStatus(
  //     obj,
  //     succ => {
  //       handleCloseRestoreDialog();
  //     },
  //     fail => {}
  //   );
  // };
  const templateNewnameHandler = newTemplate => {
    props.CreateTemplate(
      newTemplate,
      response => {
        setIsTemplateChange(false);
        setOpenCreateTemplate(false);
        setAnchorEl(null);
      },
      fail => {
        setIsTemplateChange(false);
        setOpenCreateTemplate(false);
        setAnchorEl(null);
      }
    );
  };
  const openUseCustomTemplateHandler = () => {
    if (templateConfig.project && templateConfig.project.tasks.length > 0 && templateConfig.projectTemplate.templateId != updatedItem.customstatus.templateId) {
      setOpenUseCustomTemplateDialog(true);
    }
    else {
      let obj = {
        customStatus: updatedItem.customstatus,
        templateId: updatedItem.customstatus.templateId,
        mappingStatus: updatedItem.mappingStatus ? updatedItem.mappingStatus : []
      }
      useCustomTemplateHandler(obj);
    }
  }
  const closeUseCustomTemplateHandler = () => {
    setOpenUseCustomTemplateDialog(false)
  }
  const useCustomTemplateHandler = (data) => {
    // data.customstatus.isWorkspaceDefault = true;
    props.UseCustomTemplate(
      data,
      response => {
        setIsTemplateChange(false);
        closeUseCustomTemplateHandler();
      },
      fail => {
        setIsTemplateChange(false);
        closeUseCustomTemplateHandler();
        setUpdatedItem(cloneDeep(templateItem));
      }
    );
  };
  const openUseTemplateHandler = () => {
    if (templateConfig.project && templateConfig.project.tasks.length > 0) {
      setOpenUseTemplateDialog(true);
    }
    else {
      let obj = {
        mappingStatus: [],
        templateId: updatedItem.customstatus.templateId
      }
      useTemplateHandler(obj);
    }
  }
  const closeUseTemplateHandler = () => {
    setOpenUseTemplateDialog(false)
  }
  const useTemplateHandler = (data) => {
    // data.customstatus.isWorkspaceDefault = true;
    props.UseStandardTemplate(
      data,
      response => {
        closeUseTemplateHandler();
      },
      fail => {
        closeUseTemplateHandler();
      }
    );
  };
  const updateSavedTemplateHandler = () => {
    if (isRenameTemplate) {
      templateRenameHandler();
    } else {
      props.UpdateSaveTemplate(
        updatedItem,
        response => {
          setIsTemplateChange(false);
          setIsRenameTemplate(false);
        },
        fail => {
          setIsTemplateChange(false);
          setIsRenameTemplate(false);
        }
      );
    }
  };

  const handleShowHideInput = value => {
    // Show/Hide Input on click
    setIsRenameTemplate(false);
    // setUpdatedItem(cloneDeep(templateItem));
    setIsTemplateChange(false);

  };
  const handleRenameTemplateClick = value => {
    setIsTemplateChange(true);
    setIsRenameTemplate(true);
    // setTemplateRename(value);
  };
  // handle Tempalte name input
  const handleTemplateNameChange = e => {
    // setTemplateRename(e.target.value);
    let newItem = cloneDeep(updatedItem);
    newItem.customstatus.name = e.target.value;
    setUpdatedItem(newItem);
  };
  const enterKeyHandler = event => {
    if (event.key === "Enter") {
      templateRenameHandler();
    }
  };
  const templateExist = newName => {
    const { workSpaceStatus } = props;
    let nameExist = false;
    const { templateItem } = props;
    workSpaceStatus.map(group => {
      if (group.groupName == templatesConstants.SAVED_TEMPLATES) {
        group.templates.map(temp => {
          if (
            temp.name == newName &&
            temp.templateId != templateItem.customstatus.templateId
          )
            nameExist = true;
        });
      }
    });
    return nameExist;
  };
  const templateRenameHandler = debounce(event => {
    const { workSpaceStatus } = props;
    const validationObj = validateTitleField("to-do list item.", updatedItem.customstatus.name, true, 250);
    const alreadyExist = templateExist(updatedItem.customstatus.name);
    if (validationObj.isError) {
      setTemplateInputError(validationObj.isError);
      setTemplateInputErrorMessage(validationObj.message);
      return;
    }
    if (alreadyExist) {
      setTemplateInputError(true);
      setTemplateInputErrorMessage("Already Exist");
      return;
    }
    // let updatedGroup = {};
    // workSpaceStatus.map(group => {
    //   if (group.groupName == templatesConstants.SAVED_TEMPLATES) {
    //     group.templates.map(temp => {
    //       if (temp.templateId == templateItem.customstatus.templateId) {
    //         temp.name = updatedItem.customstatus.name;
    //         updatedGroup = temp;
    //       }
    //     });
    //   }
    // });
    props.UpdateSaveTemplate(
      updatedItem,
      response => {
        setIsRenameTemplate(false);
        setIsTemplateChange(false);
        setTemplateInputError(false);
        setTemplateInputErrorMessage("");
      },
      fail => {
        setIsRenameTemplate(false);
        setIsTemplateChange(false);
        setTemplateInputError(false);
        setTemplateInputErrorMessage("");
      }
    );
    // if (props.SaveTemplateStatusItem)
    // props.SaveTemplateStatusItem(
    //   updatedGroup,
    //   response => {
    //     setIsRenameTemplate(true);
    //     setTemplateRename("");
    //     setTemplateInputError(false);
    //     setTemplateInputErrorMessage("");
    //     setOpenNotSaved(false);
    //   },
    //   fail => {
    //     setIsRenameTemplate(false);
    //     setTemplateRename("");
    //     setTemplateInputError(false);
    //     setTemplateInputErrorMessage("");
    //     setOpenNotSaved(false);
    //   }
    // );
  }, 300);

  const deleteSavedTemplateHandler = obj => {
    if (props.DeleteSavedTemplate) {
      props.DeleteSavedTemplate(
        obj,
        succ => {
          handleDeleteCloseDialog();
          setDeleteTemplateBtn("");
        },
        fail => { }
      );
    }
  };
  const handleNotSaveClose = () => {
    setIsTemplateChange(false);
    setOpenNotSaved(false);
  };
  const handleNotSaveSuccess = () => {
    setOpenNotSaved(false);
  };
  const getControls = () => {
    if (templateConfig.contextView == templatesConstants.WORKSPACE_VIEW) {
      let templateLinkWithProject = projects.some(ele => ele.projectTemplateId === updatedItem.customstatus.templateId); /** if template is linked with the project than do not show delete template button */
      return <div style={{ display: 'flex', marginLeft: 5 }}>
        {/* this condition is true when user delete saved template and there is no change happened and it is not user workspace default template */}
        {updatedItem.customstatus.groupName == templatesConstants.SAVED_TEMPLATES &&
          !isTemplateChange && updatedItem.customstatus.isWorkspaceDefault == false &&
          permission.deleteSavedTempplate.cando && !templateLinkWithProject &&
          <CustomButton
            btnType="white"
            variant="contained"
            style={{ marginBottom: 0, minWidth: 120, whiteSpace: 'nowrap' }}
            onClick={openWSTemplateDeleteDialog}
            query={deleteTemplateBtn}
            disabled={deleteTemplateBtn === "progress"}>
            Delete Template
          </CustomButton>}
        {/* this flow will called when user change any template in workspace context */}
        {isTemplateChange ? (
          <div>
            {/* it is enable when user change any template and want to discard that changes */}
            <CustomButton
              btnType="white"
              variant="contained"
              style={{ marginBottom: 0, minWidth: 120, whiteSpace: 'nowrap' }}
              onClick={() => handleDiscardTemplate()}
              query={""}
              disabled={false}>
              Discard Changes
            </CustomButton>
            {/* this will enable when user change system templates and want to save as new template in Saved Template group */}
            {templateConfig.templateType == templateTypes.NTASKDEFAULT ? (
              permission.saveNewTemplate.cando && <CustomButton
                ref={anchorEl}
                btnType="blue"
                variant="contained"
                style={{ marginLeft: 10, marginBottom: 0, minWidth: 120 }}
                onClick={(e) => handleOpenSaveDialog(e, updatedItem)}
                query={""}
                disabled={false}>
                Save as New Template
              </CustomButton>
            ) : null}
            {/* this will enable when user change saved templates and want to update template in Saved Template group */}
            {templateConfig.templateType == templateTypes.WORKSPACE_SAVED_TEMPLATE &&
              <CustomButton
                btnType="blue"
                variant="contained"
                style={{ marginLeft: 10, marginBottom: 0, minWidth: 120, whiteSpace: 'nowrap' }}
                onClick={() => updateSavedTemplateHandler()}
                query={""}
                disabled={false}>
                {"Save Changes"}
              </CustomButton>
            }
            <DropdownMenu
              open={openCreateTemplate}
              closeAction={handleClose}
              anchorEl={anchorEl}
              style={{
                width: 342,
                position: "absolute",
                right: templateConfig.contextView == "WORKSPACEVIEW" ? "auto" : 40
              }}
              id="meetingRepeatDropdown"
              placement="bottom-start">
              <SaveNewTemplateModal
                open={openCreateTemplate}
                handleClose={handleClose}
                handleSuccess={templateNewnameHandler}
                workSpaceStatus={workSpaceStatus}
                updatedItem={updatedItem}
              />
            </DropdownMenu>
          </div>
        ) : null}
        {/* it is enable, if template is not default and user didn't any change in it, then he can adopt that template as workspace default  */}
        {!updatedItem.customstatus.isWorkspaceDefault &&
          !isTemplateChange ? (
          permission.setWorkspaceDefault.cando && <CustomButton
            btnType="blue"
            variant="contained"
            style={{ marginBottom: 0, marginLeft: 15, minWidth: 120, whiteSpace: 'nowrap' }}
            // onClick={WorkspaceDefaultHandler}
            query={workspaceDefault}
            disabled={workspaceDefault === "progress"}
            onClick={openSetWorkspaceDefaultDialog}>
            Set as Workspace Default
          </CustomButton>
        ) : null}
      </div>
    } else if (templateConfig.contextView == templatesConstants.PROJECT_VIEW) {
      return <div style={{ display: 'flex', marginLeft: 5 }}>
        {/* this flow will called when user change any template in project context  */}
        {isTemplateChange ? (
          <div>
            <CustomButton
              btnType="white"
              variant="contained"
              style={{ marginBottom: 0, minWidth: 120, whiteSpace: 'nowrap' }}
              onClick={() => handleDiscardTemplate()}
              query={""}
              disabled={false}>
              Discard Changes
            </CustomButton>
            {/* this will enable when user change nTask system template or saved template and want to use in project */}
            {(templateConfig.templateType == templateTypes.NTASKDEFAULT ||
              templateConfig.templateType == templateTypes.WORKSPACE_SAVED_TEMPLATE) ?
              <CustomButton
                btnType="blue"
                variant="contained"
                style={{ marginLeft: 10, marginBottom: 0, minWidth: 120, whiteSpace: 'nowrap' }}
                onClick={openUseCustomTemplateHandler}
                query={""}
                disabled={false}>
                Use this Custom Template
              </CustomButton> : null
            }
            {/* this will enable when user change custom template and want to update that in project */}
            {templateConfig.templateType == templateTypes.PROJECT_CUSTOM_TEMPLATE ?
              // templateConfig.projectTemplate && templateConfig.projectTemplate.templateId == updatedItem.customstatus.templateId
              // ) &&
              <CustomButton
                btnType="blue"
                variant="contained"
                style={{ marginLeft: 10, marginBottom: 0, minWidth: 120, whiteSpace: 'nowrap' }}
                onClick={() => updateSavedTemplateHandler()}
                query={""}
                disabled={false}>
                {"Save Changes"}
              </CustomButton> : null
            }
          </div>
        ) : null}
        {!isTemplateChange ? (
          <div>
            {((templateConfig.templateType == templateTypes.NTASKDEFAULT ||
              templateConfig.templateType == templateTypes.WORKSPACE_SAVED_TEMPLATE) &&
              templateConfig.projectTemplate.templateId != updatedItem.customstatus.templateId) ?
              // it is enable when template is system or saved template and not project selected
              <CustomButton
                btnType="blue"
                variant="contained"
                style={{ marginBottom: 0, marginLeft: 15, minWidth: 120, whiteSpace: 'nowrap' }}
                onClick={openUseTemplateHandler}
                query={workspaceDefault}
                disabled={workspaceDefault === "progress"}
              // onClick={openSetWorkspaceDefaultDialog}
              >
                Use this Template
              </CustomButton>
              : null}
            {templateConfig.templateType == templateTypes.PROJECT_CUSTOM_TEMPLATE ?
              <>
                {/* it is enable when template is custom template and user want to save in SAVED Group */}
                <CustomButton
                  ref={anchorEl}
                  btnType="blue"
                  variant="contained"
                  style={{ marginLeft: 10, marginBottom: 0, minWidth: 120, whiteSpace: 'nowrap' }}
                  onClick={(e) => handleOpenSaveDialog(e, updatedItem)}
                  query={""}
                  disabled={false}>
                  Save as New Template
                </CustomButton>
                <DropdownMenu
                  open={openCreateTemplate}
                  closeAction={handleClose}
                  anchorEl={anchorEl}
                  style={{
                    width: 342,
                    position: "absolute",
                    right: templateConfig.contextView == "WORKSPACEVIEW" ? "auto" : 40
                  }}
                  id="meetingRepeatDropdown"
                  placement="bottom-start">
                  <SaveNewTemplateModal
                    open={openCreateTemplate}
                    handleClose={handleClose}
                    handleSuccess={templateNewnameHandler}
                    workSpaceStatus={workSpaceStatus}
                    updatedItem={updatedItem}
                  />
                </DropdownMenu>
              </> : null
            }
          </div>

        ) : null}
        {/* {!isTemplateChange && templateConfig.templateType !== templateTypes.PROJECT_CUSTOM_TEMPLATE &&
          // (templateConfig.projectTemplate && templateConfig.projectTemplate.templateId != updatedItem.customstatus.templateId) &&
          //   !isTemplateChange ? (
              <CustomButton
                btnType="blue"
                variant="contained"
                style={{ marginBottom: 0, marginLeft: 15, minWidth: 120 }}
                onClick={openUseTemplateHandler}
                query={workspaceDefault}
                disabled={workspaceDefault === "progress"}
                // onClick={openSetWorkspaceDefaultDialog}
              >
                Use this Template
              </CustomButton>
            // ) : null
            } */}
        {/* {!isTemplateChange && templateConfig.projectTemplate && 
            templateConfig.projectTemplate.isCustomTemplate && 
            templateConfig.projectTemplate.templateId == updatedItem.customstatus.templateId ? (
              <>
                <CustomButton
                  ref={anchorEl}
                  btnType="blue"
                  variant="contained"
                  style={{ marginLeft: 10, marginBottom: 0, minWidth: 120 }}
                  onClick={() => handleOpenSaveDialog(updatedItem)}
                  query={""}
                  disabled={false}>
                  Save as New Template
                </CustomButton>
            <DropdownMenu
              open={openCreateTemplate}
              closeAction={handleClose}
              anchorEl={anchorEl}
              style={{
                width: 342,
                position: "absolute",
              }}
              id="meetingRepeatDropdown"
              placement="bottom-start">
              <SaveNewTemplateModal
                open={openCreateTemplate}
                handleClose={handleClose}
                handleSuccess={templateNewnameHandler}
                workSpaceStatus={workSpaceStatus}
                updatedItem={updatedItem}
              />
            </DropdownMenu>
            </>
              ) : null} */}


      </div>
    }
    else {
      return null;
    }
  }

  {/* {!isTemplateChange && templateConfig.contextView == templatesConstants.PROJECT_VIEW &&
    templateConfig.templateType == templateTypes.NTASKDEFAULT &&(
        <CustomButton
          ref={anchorEl}
          btnType="blue"
          variant="contained"
          style={{ marginLeft: 10, marginBottom: 0, minWidth: 120 }}
          onClick={() => useTemplateHandler()}
          query={""}
          disabled={false}>
          Use this Template
        </CustomButton>
      )} */}

  // let saveTemplateBtnTxt =
  //   templateConfig.contextView == templatesConstants.WORKSPACE_VIEW
  //     ? "Save as New Template"
  //     : "Save as New Template";
  // let setTemplateBtnTxt =
  //   templateConfig.contextView == templatesConstants.WORKSPACE_VIEW
  //     ? "Set as Workspace Default"
  //     : "Use this Template";
  let createdby = allMembers.find(ele => ele.userId == updatedItem.customstatus.createdBy) || null;
  let updatedBy = allMembers.find(ele => ele.userId == updatedItem.customstatus.updatedBy) || null;
  return (
    <div className={classes.statusListCntActive}>
      {/* it is mapping dialog open when user select other template as default worksapce template in workspace context */}
      {workspaceDefaultDialog && (
        <WorkspacMappingDialog
          open={workspaceDefaultDialog}
          handleClose={handleCloseDialog}
          oldStatus={oldStatus}
          newStatus={updatedItem.customstatus}
          handleSaveAsTemplate={handleSaveTemplate}
          templateConfig={templateConfig}
        />
      )}
      {/* it is mapping dialog open when user select use template option for project */}
      {openUseTemplateDialog && (
        <UseTemplateMappingDialog
          open={openUseTemplateDialog}
          handleClose={closeUseTemplateHandler}
          oldStatus={oldStatus}
          newStatus={updatedItem}
          handleUseTemplate={useTemplateHandler}
          projectView={"Use Template"}
          templateConfig={templateConfig}
        />
      )}
      {/* it is mapping dialog open when user select custom template option for project */}
      {openUseCustomTemplateDialog && (
        <UseTemplateMappingDialog
          open={openUseCustomTemplateDialog}
          handleClose={closeUseCustomTemplateHandler}
          oldStatus={oldStatus}
          newStatus={updatedItem}
          handleUseTemplate={useCustomTemplateHandler}
          projectView={"Custom Template"}
          templateConfig={templateConfig}
        />
      )}
      <div className={classes.statusListCntTopActive}>
        <div className={classes.statusItemsHeader}>
          {isRenameTemplate ? (
            <DefaultTextField
              label={false}
              fullWidth={false}
              error={templateInputError ? true : false}
              errorState={templateInputError}
              errorMessage={templateInputErrorMessage}
              formControlStyles={{ marginBottom: 0, width: "50%" }}
              defaultProps={{
                id: "templateRename",
                type: "text",
                onChange: handleTemplateNameChange,
                onKeyUp: enterKeyHandler,
                // onBlur: event => handleShowHideInput(false),
                value: updatedItem.customstatus.name,
                placeholder: "Enter template name",
                inputProps: {
                  maxLength: 80,
                },
              }}
            />
          ) : (
            <div
              style={{ display: 'flex', alignItems: 'center' }}
              onClick={() => {
                if (templateConfig.templateType == templateTypes.WORKSPACE_SAVED_TEMPLATE)
                  handleRenameTemplateClick(updatedItem.customstatus.name);
              }}>
              <Typography
                variant="caption"
                className={classes.statusNameTxt}
                title={updatedItem.customstatus.name == updatedItem.customstatus.name ? 'Standard' : updatedItem.customstatus.name}>
                {updatedItem.customstatus.category == 3 ? `Custom (${updatedItem.customstatus.name == updatedItem.customstatus.name ? 'Standard' : updatedItem.customstatus.name})` :
                  `${updatedItem.customstatus.name == updatedItem.customstatus.name ? 'Standard' : updatedItem.customstatus.name}`}
              </Typography>
              <div>
                {(
                  templateConfig.templateType == templateTypes.PROJECT_CUSTOM_TEMPLATE) &&
                  <CustomTooltip
                    helptext={"This is a custom template. Changes made in this template are only applicable for this project. However, if you want to use this as a template for other projects, you can save this as a template and it will be saved in the Saved Template group."}
                    iconType="info"
                    placement="top"
                    position="static"
                  />}
              </div>
            </div>
          )}
          {getControls()}
        </div>
        <StatusItems
          templateItem={updatedItem}
          isArchivedSelected={isArchivedSelected}
          updateItem={updateItem}
          permission={permission}
          draggable={draggable}
          templatePermission={templatePermission}
          templateConfig={templateConfig}
        // intl={intl}
        // openDialog={openDeleteDialog}
        />
        {permission.templateStatus.isAllowAdd &&
          <StatusCreate
            isArchivedSelected={isArchivedSelected}
            permission={permission}
            statusItemsArr={updatedItem.customstatus.name}
            updateItem={updateItem}
            style={style}
            intl={intl}
            statusItem={updatedItem}
            templatePermission={templatePermission}
            templateConfig={templateConfig}
          />
        }
      </div>
      {/* {createdby || updatedBy ? (
        <div className={classes.templateInfo}>
          <div className={classes.templateInfoLeft}>
            <InfoIcon className={classes.iconinfo} />
            Created by: {createdby.fullName} •{" "}
            {moment(updatedItem.customstatus.createdDate).format("ll")}
          </div>
          <Divider className={classes.sepratorVr} />
          <div className={classes.templateInfoRight}>
            Last modified by: {updatedBy.fullName} •{" "}
            {moment(updatedItem.customstatus.updatedDate).format("ll")}
          </div>
        </div>
      ) : null} */}
      {createdby || updatedBy ? (<div className={classes.templateInfo}>
        {createdby ? (
          <div className={classes.templateInfo}>
            <div className={classes.templateInfoLeft}>
              <InfoIcon className={classes.iconinfo} />
              Created by: {createdby.fullName} •{" "}
              {moment(updatedItem.customstatus.createdDate).format("ll")}
            </div>
          </div>
        ) : null}
        {
          createdby && updatedBy ? (
            <Divider className={classes.sepratorVr} />
          ) : null
        }
        {updatedBy ? (
          <div className={classes.templateInfoRight}>
            Last modified by: {updatedBy.fullName} •{" "}
            {moment(updatedItem.customstatus.updatedDate).format("ll")}
          </div>) : null
        }
      </div>) : null}
      {/* {restoreModal && (
        <RestoreModal
          open={restoreModal}
          handleClose={handleCloseRestoreDialog}
          item={restoredItem}
          handleRestoreStatus={handleRestoreStatus}
        />
      )} */}
      {/* it is called when user delete template */}
      {wsDeleteTemplate && (
        <DeleteTemplateDialog
          open={wsDeleteTemplate}
          handleClose={handleDeleteCloseDialog}
          WSAllStatus={workSpaceStatus}
          oldStatus={oldStatus}
          newStatus={updatedItem.customstatus}
          deleteTemplate={deleteSavedTemplateHandler}
          templateConfig={templateConfig}
        />
      )}
      {/* it is called when user change any template and without save move to any other view */}
      {openNotSaved && (
        <NotSavedTemplateModal
          open={openNotSaved}
          handleClose={handleNotSaveClose}
          handleSuccess={handleNotSaveSuccess}
        />
      )}
    </div>
  );
}
StatusList.defaultProps = {
  templateItem: {},
  permission: {},
  isArchivedSelected: false,
  templatePermission: {},
  templateConfig: {},
  allMembers: [],
  projects: [],
  // SaveTemplateStatusItem(e) {
  //   return e;
  // },
  CreateTemplate(e) {
    return e;
  },
  UpdateSaveTemplate(e) {
    return e;
  },
  // CreateCustomTemplate(e) {
  //   return e;
  // },
  // DeleteWSTemplateStatusItem(e) {
  //   return e;
  // },
  MapTaskWithSetDefaultTemplate() {
    return e;
  },
  // DeleteAndMapStatus() {
  //   return e;
  // },
  // restoreStatus() {
  //   return e;
  // },
};
StatusList.propTypes = {
  templateItem: PropTypes.object.isRequired,
  isArchivedSelected: PropTypes.bool.isRequired,
  // SaveTemplateStatusItem: PropTypes.func.isRequired,
  CreateTemplate: PropTypes.func.isRequired,
  UpdateSaveTemplate: PropTypes.func.isRequired,
  // CreateCustomTemplate: PropTypes.func.isRequired,
  // DeleteWSTemplateStatusItem: PropTypes.func.isRequired,
  MapTaskWithSetDefaultTemplate: PropTypes.func.isRequired,
  // DeleteAndMapStatus: PropTypes.func.isRequired,
  // restoreStatus: PropTypes.func.isRequired,
  templatePermission: PropTypes.object.isRequired,
  templateConfig: PropTypes.object.isRequired,
};
const mapStateToProps = state => {
  return {
    user: state.profile.data,
    allMembers: state.profile.data.member.allMembers || [],
    tasks: state.tasks.data || [],
    workspaceDefault: state.workspaceTemplates.data.defaultWSTemplate,
    projects: state.projects.data || [],
  };
};
export default compose(
  withRouter,
  connect(mapStateToProps, {}),
  withStyles(classes, { withTheme: true })
)(StatusList);
