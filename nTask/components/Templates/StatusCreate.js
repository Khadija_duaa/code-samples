import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import AddIcon from "@material-ui/icons/Add";
import debounce from "lodash/debounce";
import InputAdornment from "@material-ui/core/InputAdornment";
import StatusIcon from "@material-ui/icons/Brightness1";
import classes from "./style";
import { validateTitleField } from "../../utils/formValidations";
import DefaultTextField from "../Form/TextField";
import CustomButton from "../Buttons/CustomButton";
import ColorPickerDropdown from "../Dropdown/ColorPicker/Dropdown";

function StatusCreate(props) {
  const [statusListInputError, setStatusListInputError] = useState(false);
  const [statusListInputErrorMessage, setStatusListInputErrorMessage] = useState(false);
  const [statusListInput, setStatusListInput] = useState("");
  const [btnAddQuery, setBtnAddQuery] = useState("");
  const [selectedColor, setSelectedColor] = useState("#bcbcbc");
  const [addNew, setAddNew] = useState(false);

  const { intl, statusItem } = props;
  const handleItemNameInput = event => {
    setStatusListInput(event.target.value);
    setStatusListInputError(false);
    setStatusListInputErrorMessage("");
  };
  const enterKeyHandler = event => {
    if (event.key === "Enter") {
      setBtnAddQuery("progress");
      addNewItem();
    } else if (event.key === "Escape") {
      handleClearValues();
    }
  };
  const statusExist = newName => {
    let nameExist = false;
    statusItem.customstatus.statusList.forEach(temp => {
          if (temp.statusTitle.toLowerCase() === newName.toLowerCase()) nameExist = true;
        });
    return nameExist;
  };
  const getStatusCode = (title) => {
    // return new Date().getYear() + new Date().getMonth() + new Date().getDate() + title.toUpperCase().trim() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds();
    return `${title.toUpperCase().trim()}-1`
  }
  const getStatusId = (list) => {
    let counter=0;
    list.forEach(item => {
      if(item.statusId>counter)
        counter = item.statusId
    })
    return ++counter;
    }

  const addNewItem = () => {
    const validationObj = validateTitleField("status", statusListInput, true, 80);
    const alreadyExist = statusExist(statusListInput);
    if (validationObj.isError) {
      setStatusListInputError(validationObj.isError);
      setStatusListInputErrorMessage(validationObj.message);
      setBtnAddQuery("");
      return;
    }
    if (alreadyExist) {
      setStatusListInputError(true);
      setStatusListInputErrorMessage("Status name already exists.");
      setBtnAddQuery("");
      return;
    }
    const newItem = {
      position : statusItem.customstatus.statusList.length,
      statusCode : getStatusCode(statusListInput),
      statusTitle : statusListInput.trim(),
      statusColor : selectedColor,
      statusId : getStatusId(statusItem.customstatus.statusList),

      // templateId: statusItem.customstatus.id,
      // statusTitle: statusListInput.trim(),
      // workspaceId: statusItem.customstatus.workSpaceId,
      // colorCode: selectedColor,
    };

    if (props.updateItem){
      statusItem.customstatus.statusList.push(newItem);
      props.updateItem(statusItem, "addItem");
      handleClearValues();
    }
  };
  const addBtnHandler = () => {
    setBtnAddQuery("progress");
    addNewItem();
  };
  const handleShowHideInput = value => {
    // Show/Hide Input on click
    if (value == false && statusListInput) return; // This will run incase user entered any value in input and try to focus out to hide the input
  };
  // Handle color change function
  const onColorChange = color => {
    setSelectedColor(color);
  };
  const handleClearValues = () => {
    setStatusListInput("");
    setStatusListInputError(false);
    setStatusListInputErrorMessage("");
    setBtnAddQuery("");
    setAddNew(false);
  };

  useEffect(() => {
    return () => {
      handleClearValues();
    };
  },[props.statusItemsArr]);

  const random_hex_color_code = () => {
    let n = (Math.random() * 0xfffff * 1000000).toString(16);
    return '#' + n.slice(0, 6);
  };
  const addNewStatus = () => {
    setAddNew(true);
    setSelectedColor(random_hex_color_code());
  };

  const { classes, statusItemsArr } = props;

  return (
    <div className={classes.statusHeader}>
      <div className={classes.statusHeaderTxtCnt}>
        {addNew ? (
          <DefaultTextField
            label=""
            fullWidth
            borderBottom={!!statusItemsArr.length}
            formControlStyles={{ marginBottom: 0 }}
            error={!!statusListInputError}
            errorState={statusListInputError}
            errorMessage={statusListInputErrorMessage}
            transparentInput
            defaultProps={{
              id: "statusNameInput",
              onChange: handleItemNameInput,
              onKeyUp: enterKeyHandler,
              onBlur: event => {
                event.stopPropagation();
                handleShowHideInput(false);
                // setAddNew(false);
              },
              value: statusListInput,
              style: { paddingRight: 0 },
              autoFocus: false,
              placeholder: intl.formatMessage({id:"workspace-settings.task-status.create",defaultMessage: "Add New Status"}),
              inputProps: { maxLength: 30 },
              startAdornment: (
                <InputAdornment position="start" style={{ paddingLeft: 11, marginRight: 0 }}>
                  <ColorPickerDropdown
                    classes={classes}
                    selectedColor={selectedColor}
                    onSelect={onColorChange}
                    iconRenderer={
                      <StatusIcon className={classes.statusIcon} style={{ color: selectedColor }} />
                    }
                  />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <span className={classes.cancelTitle} onClick={handleClearValues}>
                    {" "}
                    Cancel{" "}
                  </span>
                  <CustomButton
                    onClick={addBtnHandler}
                    variant="contained"
                    query={btnAddQuery}
                    disabled={btnAddQuery === "progress"}
                    btnType="blue"
                    style={{ padding: "3px 11px", minWidth: 48, marginRight: 10 }}>
                    Add
                  </CustomButton>
                </InputAdornment>
              ),
              autoFocus: true,
              // disabled: {isArchivedSelected}
            }}
          />
        ) : (
          <div className={classes.statusNewCnt} onClick={() =>addNewStatus() }>
            <AddIcon className={classes.addIconStatus} />
            <span className={classes.newStatusTitle}> {intl.formatMessage({id:"workspace-settings.task-status.create",defaultMessage: "Add New Status"})} </span>
          </div>
        )}
      </div>
    </div>
  );
}
StatusCreate.defaultProps = {
  isArchivedSelected: false,
  statusItem: {},
};
StatusCreate.propTypes = {};
const mapStateToProps = state => {
  return {};
};
export default compose(
  withRouter,
  connect(mapStateToProps, {}),
  withStyles(classes, { withTheme: true })
)(StatusCreate);
