import React, { useState, useEffect, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import DefaultDialog from "../Dialog/Dialog";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Divider from "@material-ui/core/Divider";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import CustomButton from "../Buttons/CustomButton";
import { generateSelectData } from "../../helper/generateSelectData";
import RoundIcon from "@material-ui/icons/Brightness1";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../Icons/DeleteIcon";
import Typography from "@material-ui/core/Typography";
import NotificationMessage from "../NotificationMessages/NotificationMessages";
import templatesConstants from "../../utils/constants/templatesConstants";

type DeleteDefaultModallProps = {
  classes: Object,
  theme: Object,
  open: Boolean,
  handleClose: Function,
  oldTemplate: Object,
  item: Object,
};

function DeleteDefaultModal(props: DeleteDefaultModalProps) {
  const { classes, theme, handleClose, open, tasks, projects, oldTemplate, item, templateConfig } = props;
  const [currentView, setView] = useState("mappManually");
  const [selectedStatus, setSelectedStatus] = useState(null);
  const [statusArr, setStatusArr] = useState([]);

  const handleChangeTab = (event, nextView) => {
    setView(nextView);
  };
  const filterTask = () => {
    let total = []
    if(templateConfig.contextView == templatesConstants.WORKSPACE_VIEW){
      tasks.map(el => {
        if(el.projectId == null){
          if(el.status == item.statusId)
           total.push(el);
         } else{
            let project = projects.find(item => item.projectId == el.projectId);
            if(project && project.projectTemplateId == oldTemplate.customstatus.templateId && el.status == item.statusId){
              total.push(el)
            }
          }
      })
    }else{
        tasks.map(el => {
          if(el.projectId != null && templateConfig.project.projectId == el.projectId && el.status == item.statusId){
              total.push(el)
          }
        })
    }
    return total;
  }
  const associatedTasks = filterTask();//contextView == templatesConstants.WORKSPACE_VIEW ? tasks.filter(el => el.projectId == null && el.statusTitle == item.statusTitle) :tasks.filter(el => el.projectId != null && el.statusTitle == item.statusTitle)

  const generateStatuses = () => {
    /* function for generating the array of status and returing array to the data prop for multi drop down */
    let filterStatus = oldTemplate.customstatus.statusList.filter(s => s.statusCode !== item.statusCode);
    let statuses = filterStatus.map((s, i) => {
      //   return generateSelectData(s.statusTitle, s.statusId, s.statusId, s.statusId, s);
      return {
        label: s.statusTitle,
        value: s.statusId,
        icon: <RoundIcon htmlColor={s.statusColor} className={classes.statusesIcon} />,
      };
    });
    return statuses;
  };

  const handleSelectStatus = (key, opt, s) => {
    setSelectedStatus(opt);
  };

  useEffect(() => {}, []);

  const handleSuccess = () => {
    let arr = [];
    let filterStatus;
    if (currentView == "mappManually") {
      filterStatus = oldTemplate;
      arr.push({
        statusIDFrom: item.statusId,
        statusIDTo: selectedStatus.value,
      });
      //update status of new selected statusItem
      filterStatus.customstatus.statusList.map(el => {
        if(el.statusId == selectedStatus.value){
          el.isDefault = item.isDefault ? item.isDefault : el.isDefault;
          el.isDoneState = item.isDoneState? item.isDoneState: el.isDoneState;
        } 
      });
      //remove deleted statusItem from template
      filterStatus.customstatus.statusList = filterStatus.customstatus.statusList.filter(
        s => s.statusCode !== item.statusCode
      );

    }
    if (currentView == "allOpen") {
      filterStatus = oldTemplate;
      //select statusItem which has default status in statuses array
      let defaultStatus = oldTemplate.customstatus.statusList.find(o => o.isDefault) || {};
      arr.push({
        statusIDFrom: item.statusId,
        statusIDTo: defaultStatus.statusId,
      });
      //update status of new selected statusItem
      filterStatus.customstatus.statusList.map(el => {
        if(el.statusId == defaultStatus.statusId){
          el.isDefault = item.isDefault ? item.isDefault : el.isDefault;
          el.isDoneState = item.isDoneState? item.isDoneState: el.isDoneState;
        }
        return el;
      });
      //remove deleted statusItem from template
      filterStatus.customstatus.statusList = filterStatus.customstatus.statusList.filter(
        s => s.statusCode !== item.statusCode
      );
    }
    

    let obj = {
      templateItem: filterStatus,
      mappingStatus: arr,
      // issetDefaultStatus: item.isDefault,
      // issetDoneStatus: item.isDoneState,
    };
    props.deleteStatus(obj);
  };
  const getDefaultDone = (item) => {
    if(item.isDefault && item.isDoneState){
      return <Fragment>
        <span style={{marginRight: 5}}
          className={
            classes.defaultSpan
          }>
          {"Initial"}
        </span>and
        <span
          className={classes.doneSpan}>
          {"Final"}
        </span>
        </Fragment>
    }else if(item.isDefault && !item.isDoneState){
        return <span
          className={
            item.isDefault
              ? classes.defaultSpan
              : item.isDoneState
              ? classes.doneSpan
              : null
          }>
          {"Initial"}
        </span>
    }else if(!item.isDefault && item.isDoneState){
        return <span
          className={
            item.isDefault
              ? classes.defaultSpan
              : item.isDoneState
              ? classes.doneSpan
              : null
          }>
          {"Final"}
        </span>
    }
  }

  const defaultStatus = oldTemplate.customstatus.statusList.find(s => s.isDefault) || {};

  return (
    <>
      <DefaultDialog
        title={associatedTasks.length == 0 ? "Delete Status" : "Status Mapping"}
        sucessBtnText="Done"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={handleClose}>
        <div className={classes.statusMapCnt}>
          {associatedTasks.length !== 0 && (
            <>
              <div className={classes.heading}>
                <span
                  className={classes.title}
                  style={{ textAlign: "center", padding: "0px 43px", lineHeight: 1.6 }}>
                  <span
                    className={classes.title}
                    style={{ color: "#202020", fontWeight: 800 }}>{`${associatedTasks.length} tasks`}</span>
                  {` will be affected by deleting`}{" "}
                  <span className={classes.title} style={{ color: "#202020", fontWeight: 800 }}>
                    {item.statusTitle}
                  </span>{" "}
                  {` status which is marked as`}
                  {getDefaultDone(item)}
                  
                </span>
              </div>
              <div className={classes.heading}>
                <span className={classes.title}>How should we handle these tasks?</span>
              </div>

              <Divider style={{ margin: "20px 0" }} />

              {item.isDoneState && (
                <div className={classes.toggleContainer}>
                  <ToggleButtonGroup
                    value={currentView}
                    exclusive
                    onChange={handleChangeTab}
                    classes={{ root: classes.toggleBtnGroup }}>
                    <ToggleButton
                      value="mappManually"
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      Map Status Manually
                    </ToggleButton>
                    <ToggleButton
                      value="allOpen"
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      Change all to{" "}
                      <Brightness1Icon
                        className={classes.openIcon}
                        style={{ color: defaultStatus.statusColor }}
                      />{" "}
                      {`${defaultStatus.statusTitle}`}
                    </ToggleButton>
                  </ToggleButtonGroup>
                </div>
              )}
              {currentView == "mappManually" && (
                <>
                  <div className={classes.labelCnt}>
                    {" "}
                    <span className={classes.labelTitleLeft}>Old Status</span>
                    <span className={classes.labelTitleRight}>New Status</span>
                  </div>
                  <div className={classes.statusesCnt}>
                    <CustomButton
                      btnType="white"
                      variant="contained"
                      style={{
                        fontSize: "14px !important",
                        color: "#202020",
                        border: "none",
                        background: "#F6F6F6",
                        marginLeft: 0,
                        fontFamily: theme.typography.fontFamilyLato,
                        fontWeight: theme.palette.fontWeightMedium,
                        padding: "7px 50px 7px 9px",
                        width: 170,
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "end",
                      }}>
                      <Brightness1Icon
                        className={classes.statusesIcon}
                        style={{ color: item.statusColor }}
                      />
                      {item.statusTitle}
                    </CustomButton>
                    <span className={classes.taskCountTitle}>
                      {" "}
                      {associatedTasks.length} tasks <ArrowForwardIcon className={classes.arrowRight} />{" "}
                    </span>

                    <SelectSearchDropdown /* Issue Status select drop down */
                      data={generateStatuses}
                      label={null}
                      selectChange={(key, opt) => {
                        handleSelectStatus(key, opt);
                      }}
                      type="status"
                      selectedValue={selectedStatus}
                      placeholder={"Select status"}
                      icon={true}
                      isMulti={false}
                      isDisabled={false}
                      styles={{ width: 170, margin: 0 }}
                    />
                  </div>
                </>
              )}
            </>
          )}

          {associatedTasks.length == 0 && (
            <>
              <div className={classes.heading}>
                <span
                  className={classes.title}
                  style={{ textAlign: "center", padding: "0px 43px", lineHeight: 1.6 }}>
                  {`Looks like you are about to delete `}
                  <span className={classes.title} style={{ color: "#202020", fontWeight: 800 }}>
                    {item.statusTitle}
                  </span>{" "}
                  {` status which is marked as`}
                  {getDefaultDone(item)}
                  {/* <span
                    className={
                      item.isDefault
                        ? classes.defaultSpan
                        : item.isDoneState
                        ? classes.doneSpan
                        : null
                    }>
                    {item.isDefault ? "Default" : item.isDoneState ? "Done" : ""}
                  </span> */}
                </span>
              </div>
              <div className={classes.heading}>
                <span className={classes.title}>
                  Please select the new status to mark as{" "}
                  {item.isDoneState && item.isDefault ? "initial and final" : item.isDefault ? "initial" : item.isDoneState ? "final" : ""}.
                </span>
              </div>

              <Divider style={{ margin: "20px 0" }} />
              <div className={classes.statusesCnt}>
                <SelectSearchDropdown /* Issue Status select drop down */
                  data={generateStatuses}
                  label={
                    item.isDefault && item.isDoneState
                      ? "New Initial and Final Status"
                      : item.isDoneState && !item.isDefault
                      ? "New Final Status"
                      : !item.isDoneState && item.isDefault
                      ? "New Initial Status"
                      : null
                  }
                  selectChange={(key, opt) => {
                    handleSelectStatus(key, opt);
                  }}
                  type="status"
                  selectedValue={selectedStatus}
                  placeholder={"Select status"}
                  icon={true}
                  isMulti={false}
                  isDisabled={false}
                  styles={{ width: 270, marginBottom: 0 }}
                />
              </div>
            </>
          )}

          <Divider style={{ margin: "20px 0" }} />

          <NotificationMessage
            type="info"
            iconType="info"
            style={{
              // width: "calc(100% - 50px)",
              // padding: "10px 15px 10px 15px",
              backgroundColor: "rgba(243, 243, 243, 1)",
              borderRadius: 4,
              // margin: "10px 20px",
            }}>
            {`New selected status will also be marked as 
              ${ item.isDoneState && item.isDefault ? "initial and final" : item.isDefault ? "initial" : item.isDoneState ? "final" : ""
            }.`}
          </NotificationMessage>
        </div>
        <ButtonActionsCnt
          cancelAction={handleClose}
          successAction={handleSuccess}
          successBtnText={"Done"}
          cancelBtnText={"Cancel"}
          btnType={"blue"}
          btnQuery={""}
          disabled={false}
        />
      </DefaultDialog>
    </>
  );
}

DeleteDefaultModal.defaultProps = {
  classes: {},
  theme: {},
  open: false,
  handleClose: () => {},
  tasks: [],
  projects:[],
  oldTemplate: {},
  item: {},
};
const mapStateToProps = state => {
  return {
    tasks: state.tasks.data || [],
    projects: state.projects.data || [],
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(DeleteDefaultModal);
