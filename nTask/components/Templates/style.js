const WorkspaceStatusStyles = theme => ({
  statusListCntActive: {
    padding: "20px 40px",
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
  statusListCntTopActive: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
  statuslistCnt: {
    flexBasis: 'unset',
  },
  fontSize: {
    fontSize: "15px !important"
  },
  statusNameTxt: {
    fontSize: "16px !important",
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.common.black,
  },
  statusHeader: {
    // display: "flex",
  },
  statusHeaderTxtCnt: {
    display: "flex",
    alignItems: "center",
    position: "relative",
    // '&:hover $statusHeaderAction': {
    //   display: 'flex',
    // },
  },
  addIconCnt: {
    display: "flex",
  },
  statusHeaderAction: {
    position: "absolute",
    right: 10,
    top: 5,
    display: "flex",
    alignItems: "center",
    display: "flex",
  },
  progressCnt: {
    width: "100%",
    marginLeft: 10,
  },
  editCnt: {
    display: "flex",
    alignItems: "flex-start",
    width: "100%",
    justifyContent: "inherit",
    marginLeft: 10,
  },
  statusBody: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    // padding: "0 10px",
    // minHeight: 36,
  },
  checkAllCnt: {
    display: "flex",
    margin: "15px 0",
  },
  statusItemList: {
    listStyleType: "none",
    // margin: '0 0 0 -20px',
    padding: 0,
    "& li:first-child": {
      borderTop: `1px solid ${theme.palette.border.grayLighter}`,
    },
  },
  statusCntActive: {
    background: theme.palette.common.white,
    marginBottom: 8,
    padding: "0 10px",
    fontFamily: theme.typography.fontFamilyLato,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    borderRadius: 6,
  },
  statusItemCheckedDetails: {
    display: "flex",
  },
  statusItem: {
    fontSize: "17px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    fontFamily: theme.typography.fontFamilyLato,
    // paddingRight: 23,
    padding: "8px 0 8px 0",
    borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    // minHeight: 50,
    "&:hover": {
      // transition: "ease all 0.001s",
      //   background: theme.palette.background.light,
    },
    "&:hover $dragHandleCnt": {
      visibility: "visible",
    },
    "&:hover $emptyCheckbox": {
      display: "none",
    },
    "&:hover $unCheckedIcon": {
      display: "block",
    },
    "&:hover $editIconCnt": {
      display: "inline !important",
    },
    "&:hover $grayChipsTag": {
      display: "inline-block",
    },
  },
  statusItemDisabled: {
    pointerEvents: "none",
    fontSize: "17px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    fontFamily: theme.typography.fontFamilyLato,
    // paddingRight: 23,
    padding: "5px 0 5px 0",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    // minHeight: 50,
    "&:hover $dragHandleCnt": {
      visibility: "visible",
    },
    "&:hover $emptyCheckbox": {
      display: "none",
    },
    "&:hover $unCheckedIcon": {
      display: "block",
    },
  },
  dragHandleCnt: {
    visibility: "hidden",
  },
  dragHandle: {
    fontSize: "24px !important",
  },
  btnSelected: {},
  btnNotSelected: {
    visibility: "hidden",
  },
  checkListItemSpacer: {
    marginLeft: 28,
  },
  statusItemInner: {
    display: "flex",
    alignItems: "flex-start",
    fontFamily: theme.typography.fontFamilyLato,
    "& span": {
      fontSize: "12px !important",
      color: theme.palette.text.primary,
      lineHeight: 0,
      // display: 'block',
      wordWrap: "break-word",
    },
  },
  markCompleteDetails: {
    display: "flex",
    alignItems: "center",
  },
  checklistCheckboxCnt: {
    margin: "2px 0 0 0",
  },
  checkAllCheckboxCnt: {
    margin: 0,
    paddingLeft: 12,
  },
  checklistItemDetails: {
    marginLeft: 10,
    display: "inline",
    fontFamily: theme.typography.fontFamilyLato,
  },
  checklistItemDetailsName: {
    maxWidth: 100,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    fontFamily: theme.typography.fontFamilyLato,
    marginLeft: 3,
  },

  progressBar: {
    backgroundColor: theme.palette.background.contrast,
    borderRadius: 2,
  },
  greenBar: {
    backgroundColor: theme.palette.background.barGreen,
  },
  allmarkCheckedIcon: {
    fontSize: "24px !important",
  },
  checkedIcon: {
    fontSize: "18px !important",
  },
  emptyCheckbox: {
    width: 18,
    height: 18,
    background: theme.palette.background.paper,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  unCheckedIcon: {
    fontSize: "18px !important",
    display: "none",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  emptyDate: {
    display: "flex",
    // border: `2px dashed ${theme.palette.border.lightBorder}`,
    // borderRadius: 18,
    // padding: 5,
    textDecoration: "underline",
  },
  addSelectionDate: {
    display: "flex",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 18,
    padding: 5,
  },
  selectionDate: {
    display: "flex",
    // textDecoration: 'underline',
  },
  actionCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  actionCntCmplete: {
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  // dateCnt: {
  // display: 'none',
  // },
  // dateCntSelected: {
  // pointerEvents: 'none',
  // },
  dateExpire: {
    color: theme.palette.text.danger,
    minWidth: 48,
    textAlign: "center",
  },
  addAssigneeList: {
    marginLeft: 10,
  },
  assigneeListCnt: {
    // display: 'none',
    marginLeft: 9,
  },
  outlinedInputCnt: {
    marginRight: 15,
  },
  outlinedTaskInput: {
    padding: "10px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
    fontSize: "22px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  notchedOutlineCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  outlineInputFocus: {
    "& $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: 0,
    },
    "& $notchedOutlineAMCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  StaticDatePickerPopper: {
    zIndex: 6,
    left: "0px !important",
  },
  statusItemTxt: {
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal !important",
    textOverflow: "ellipsis",
    maxWidth: 270,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.black,
  },
  statusNameCnt: {
    display: "flex",
    alignItems: "center",
  },
  editIcon: {
    fontSize: "11px !important",
    marginTop: -11,
    marginLeft: 5,
    color: "#7e7e7e",
  },
  editIconCnt: {
    display: "none",
  },
  addIcon: {
    marginBottom: -8,
  },
  taskCount: {
    fontSize: "12px !important",
    borderRadius: 4,
    marginLeft: 7,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    color: theme.palette.common.black,
    backgroundColor: theme.palette.common.white,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "0 4px",
  },
  statusItemsHeader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "10px 0",
  },
  statusIcon: {
    fontSize: "17px !important",
  },
  grayChipsTag: {
    background: theme.palette.background.contrast,
    padding: "5.5px 11px 3px 9px",
    color: theme.palette.text.darkGray,
    fontSize: "12px !important",
    borderRadius: 4,
    cursor: "pointer",
    display: "none",
    lineHeight: "normal",
    marginLeft: 5,
  },
  statusTitleCnt: {
    display: "flex",
    alignItems: "center",
    flex: 1,
  },
  defaultChipsTag: {
    background: theme.palette.background.btnBlue,
    padding: "5.5px 11px 3px 9px",
    color: theme.palette.common.white,
    fontSize: "12px !important",
    lineHeight: "normal",
    borderRadius: 4,
    cursor: "pointer",
  },
  doneChipsTag: {
    background: theme.palette.background.brightGreen,
    padding: "5.5px 11px 3px 9px",
    color: theme.palette.common.white,
    fontSize: "12px !important",
    borderRadius: 4,
    lineHeight: "normal",
    cursor: "pointer",
    marginLeft: 5,
  },
  taskCountChipsTag: {
    background: theme.palette.common.white,
    padding: "3.5px 10px",
    color: theme.palette.text.darkGray,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    fontSize: "12px !important",
    lineHeight: "normal",
    borderRadius: 4,
    marginLeft: 5,
  },
  statusMapCnt: {
    minHeight: 200,
    padding: 30,
  },
  heading: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    margin: "10px 0px",
  },
  title: {
    color: "#202020",
    fontSize: "15px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  toggleBtnGroup: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    flexWrap: "nowrap",
    width: "100%",
    borderRadius: 4,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    height: 40,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.secondary,
      backgroundColor: "white",
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.common.white,
      },
    },
  },
  toggleButton: {
    height: "auto",
    padding: "4px 20px 5px",
    fontSize: "13px !important",
    fontWeight: theme.palette.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
    textTransform: "capitalize",
    width: "100%",
    background: "#F6F6F6",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&[value = 'mappManually']": {
      //  borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  toggleButtonSelected: {},
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    marginBottom: 25,
  },
  openIcon: {
    fontSize: "11px !important",
    color: "#ABB4BB",
    margin: "2px 3px 0px 3px",
  },
  statusesCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  arrowRight: {
    marginBottom: "-4px",
    fontSize: "18px",
    marginLeft: 10,
  },
  taskCountTitle: {
    fontSize: "14px !important",
    color: "#202020",
    fontWeight: theme.palette.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
  },
  statusesIcon: {
    marginRight: 5,
    fontSize: "12px !important",
  },
  labelCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    fontSize: "13px !important",
    color: "#202020",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    marginBottom: 5,
  },
  // labelTitleLeft: {
  //   marginLeft: 0,
  // },
  labelTitleRight: {
    marginRight: 104,
  },
  deleteConfirmationIcon: {
    fontSize: "50px !important",
  },
  deleteIconCnt: {
    border: `1px solid ${theme.palette.border.redBorder}`,
    borderRadius: "50%",
    width: 90,
    height: 90,
    paddingLeft: 26,
    paddingTop: 23,
    marginBottom: 20,
    background: theme.palette.background.paper,
  },
  centerAlignContent: {
    padding: "20px 60px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  defaultSpan: {
    background: "#0090ff",
    color: "white",
    borderRadius: 4,
    padding: "1px 5px 3px 4px",
    marginLeft: 4,
  },
  doneSpan: {
    background: "#1DC571",
    color: "white",
    borderRadius: 4,
    padding: "1px 5px 3px 4px",
    marginLeft: 4,
  },
  statusNewCnt: {
    display: "flex",
    flexDirection: "row",
    height: 40,
    /* justify-content: center; */
    alignItems: "center",
    padding: "11px 29px",
    width: "100%",
    borderBottom: "1px solid #eaeaea",
    cursor: "pointer",
  },
  newStatusTitle: {
    fontSize: "14px !important",
    color: "#111111",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  addIconStatus: {
    color: "#0090ff",
    marginRight: "15px",
    fontSize: "20px !important",
  },
  cancelTitle: {
    fontSize: "14px !important",
    color: "#646464",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    marginRight: 12,
    cursor: "pointer",
  },
  newTemplateSaveCnt: {

  },
  newTemplateSaveTitleCnt: {
    padding: '14px 22px',
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    justifyContent: 'space-between',
    alignItems: 'center',
    display: 'flex',
  },
  txtCnt:{
    padding: '22px 22px 28px 22px' 
  },
  newTemplateSaveFooterCnt: {
    display: 'flex', 
    justifyContent: 'flex-end', 
    padding: '15px 20px', 
    borderTop: `1px solid ${theme.palette.border.lightBorder}`
  },
  templateInfo:{
    borderRadius: 4,
    backgroundColor: theme.palette.border.extraLightBorder,
    padding: '9px 8px',
    fontSize: "12px !important",
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    color: "#646464"
  },
  templateInfoLeft:{
    display: 'flex',
    alignItems: 'center'
  },
  templateInfoRight:{

  },
  iconinfo: {
    color: theme.palette.secondary.lightGrey,
    fontSize: "14px !important",
    marginRight: 5
  },
  sepratorVr: {
    height: '100%',
    width: 1
  },
  statusTitle: {
    // maxWidth : 50,
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
});
export default WorkspaceStatusStyles;
