const TableStyles = theme => ({ 
    root: {
        width: '100%',
        background: theme.palette.background.default,
        boxShadow: 'none',
    },
    tableWrapper: {
        overflow: 'auto',
        minHeight: 517,
    },
    tableHeader: {
        background: theme.palette.background.light,
    },
    tableHeaderRow: {
        height: 36,
    },
    tableRow:{
        verticalAlign: 'middle',
    },
    linkStyle: {
        fontWeight: theme.typography.fontWeightMedium,
    },
    success:{
        color: theme.palette.text.green,
    },
    danger:{
        color: theme.palette.text.danger,
    }
});
export default TableStyles;