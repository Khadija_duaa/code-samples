import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import CustomAvatar from "../Avatar/Avatar";
import tableStyle from "./style";
import Typography from "@material-ui/core/Typography";
import DefaultTextField from "../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import DefaultCheckbox from "../Form/Checkbox";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import CrossIcon from "../Icons/CrossIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import DefaultButton from "../Buttons/DefaultButton";
import ProjectSelectIconMenu from "../Menu/TaskMenus/ProjectSelectIconMenu";
import CustomTooltip from "../Tooltip/Tooltip";
import { createNewProjectHelpText } from "../Tooltip/helptext";
import DeleteIcon from '../Icons/IconDelete';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';

class ProjectResourcesTable extends Component {
  constructor(props) {
    super(props);
    this.state = { resourcesListValues: [] };
  }

  componentDidMount() {}

  handleCreateOptionsSelect = (type, option) => {
    this.setState({
      resourcesListValues: option,
    });
  };

  render() {
    const {
      classes,
      theme,
      rows,
      rateType = "",
      currencySymbol = "",
    } = this.props;
    const { resourcesListValues } = this.state;
    return (
      <Fragment>
        <div className={classes.resourceCnt}>
          <Paper className={classes.root}>
            <div className={classes.resourceTableWrapper}>
              <Table stickyHeader>
                <TableHead className={classes.tableHeader}>
                  <TableRow className={classes.tableHeaderRow}>
                    <TableCell className={classes.tableHeaderCell}>
                      <Typography
                        variant="h6"
                        className={classes.tableHeaderTxt}
                      >
                        Team Member
                      </Typography>
                    </TableCell>
                    <TableCell
                      className={classes.tableHeaderCell}
                      align="right"
                    >
                      <Typography
                        variant="h6"
                        className={classes.tableHeaderTxt}
                      >
                        Weekly Capacity
                      </Typography>
                    </TableCell>
                    {/* rateType == "differentHourlyPerResource" */}
                    {rateType == "differentHourlyPerResource" && (
                      <TableCell
                        className={classes.tableHeaderCell}
                        align="left"
                      >
                        <Typography
                          variant="h6"
                          className={classes.tableHeaderTxt}
                        >
                          Hourly Rate{" "}
                        </Typography>
                      </TableCell>
                    )}
                    <TableCell className={classes.tableHeaderCell} align="left">
                      <div style={{ display: "flex" }}>
                        <Typography
                          variant="h6"
                          className={classes.tableHeaderTxt}
                        >
                          Permission{" "}
                        </Typography>
                        <CustomTooltip
                          helptext={
                            createNewProjectHelpText.permissionViewhelpText
                          }
                          iconType="help"
                          className={classes.permissionTooltip}
                        />
                      </div>{" "}
                    </TableCell>
                    <TableCell
                      className={classes.tableHeaderCell}
                      align="right"
                    ></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map((row) => (
                    <TableRow
                      key={row.user.userId}
                      className={classes.tableRow}
                    >
                      <TableCell
                        component="th"
                        scope="row"
                        className={classes.tableHeaderCell}
                      >
                        <div className="flex_center_start_row">
                          <CustomAvatar
                            styles={{ marginRight: 10 }}
                            otherMember={{
                              imageUrl: row.user.imageUrl,
                              firstName: row.user.fullName,
                              lastName: "",
                              email: row.user.email,
                              isOnline: row.user.isOnline,
                              isOwner: row.user.teamOwner,
                            }}
                            size="small"
                          />
                          <div>
                            <Typography variant="h6">
                              <span
                                style={{
                                  fontSize: "13px !important",
                                  fontWeight: theme.typography.fontWeightMedium,
                                }}
                              >
                                {row.user.fullName}{" "}
                              </span>
                              {row.user.teamOwner && (
                                <span className={classes.disabledUserText}>
                                  (You)
                                </span>
                              )}
                            </Typography>
                            <Typography variant="caption">
                              {row.user.roleId == "000"
                                ? "Team Owner"
                                : row.user.roleName}
                            </Typography>
                          </div>
                        </div>
                      </TableCell>
                      <TableCell
                        align="right"
                        className={classes.tableBodyCell}
                      >
                        <DefaultTextField
                          label=""
                          // fullWidth={true}
                          errorState={false}
                          formControlStyles={{ marginBottom: 0, maxWidth: 86 }}
                          errorMessage=""
                          error={false}
                          defaultProps={{
                            id: `weeklyCapacity-${row.user.userId}`,
                            type: "text",
                            onChange: (event) => {
                              this.props.handleInputTable(
                                event,
                                row,
                                "weeklyCapacity"
                              );
                            },
                            value: row.weeklyCapacity,
                            autoFocus: false,
                            placeholder: "",
                            inputProps: { maxLength: 3 },
                            endAdornment: (
                              <InputAdornment
                                position="end"
                                classes={{
                                  positionEnd: classes.detailsEndIA,
                                }}
                              >
                                <Typography
                                  variant="h6"
                                  className={classes.currencySymbol}
                                >
                                  {"hr(s)"}
                                </Typography>
                              </InputAdornment>
                            ),
                          }}
                        />
                      </TableCell>
                      {/* rateType == "differentHourlyPerResource"  */}
                      {rateType == "differentHourlyPerResource" && (
                        <TableCell
                          align="right"
                          className={classes.tableBodyCell}
                          style={{ width: 97 }}
                        >
                          <DefaultTextField
                            label=""
                            // fullWidth={true}
                            errorState={false}
                            formControlStyles={{
                              marginBottom: 0,
                              maxWidth: 82,
                            }}
                            errorMessage=""
                            error={false}
                            defaultProps={{
                              id: `hourlyRateResource-${row.user.userId}`,
                              type: "text",
                              onChange: (event) => {
                                this.props.handleInputTable(
                                  event,
                                  row,
                                  "hourlyRate"
                                );
                              },
                              value: row.hourlyRate,
                              autoFocus: false,
                              placeholder: "",
                              inputProps: { maxLength: 85 },
                              startAdornment: (
                                <InputAdornment
                                  position="start"
                                  classes={{
                                    positionStart: classes.detailsStartIA,
                                  }}
                                >
                                  <Typography
                                    variant="h6"
                                    className={classes.currencySymbol}
                                  >
                                    {currencySymbol}
                                  </Typography>
                                </InputAdornment>
                              ),
                            }}
                          />
                        </TableCell>
                      )}
                      <TableCell
                        align="left"
                        className={classes.tableBodyCell}
                        // style={{ padding: "4px 9px 4px 0" }}
                      >
                        <ProjectSelectIconMenu
                          iconType="status"
                          view="permission"
                          isSingle={true}
                          isSimpleList={true}
                          isUpdated={(obj) =>
                            this.props.handleChangePermission(row, obj)
                          }
                          style={{ marginBottom: 0, maxWidth: 120 }}
                          transform="translateY(48px)"
                          label=""
                          selectedValue={row.permission}
                        />
                      </TableCell>
                      <TableCell align="right" style={{ padding: "4px 0" }}>
                        {!row.user.teamOwner && (
                          <DefaultButton
                            buttonType="smallIconBtn"
                            disableRipple
                            onClick={() => {
                              this.props.removeResource(row);
                            }}
                          >
                            <SvgIcon
                              viewBox="0 0 24 24"
                              className={classes.iconDelete}
                            >
                              <DeleteIcon />
                            </SvgIcon>
                          </DefaultButton>
                        )}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          </Paper>
        </div>
      </Fragment>
    );
  }
}

export default compose(withStyles(tableStyle, { withTheme: true }))(
  ProjectResourcesTable
);
