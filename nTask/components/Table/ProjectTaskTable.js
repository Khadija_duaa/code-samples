import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import CustomAvatar from "../Avatar/Avatar";
import tableStyle from "./style";
import Typography from "@material-ui/core/Typography";
import DefaultTextField from "../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import DefaultCheckbox from "../Form/Checkbox";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import CrossIcon from "../Icons/CrossIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import DefaultButton from "../Buttons/DefaultButton";
import DurationInput from "../DatePicker/DurationInput";

class ProjectTaskTable extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const {
      classes,
      theme,
      rows,
      rateType = "",
      currencySymbol = "",
    } = this.props;
    const { resourcesListValues } = this.state;
    return (
      <Fragment>
        <div className={classes.resourceTableWrapper}>
          <Table stickyHeader>
            <TableHead className={classes.tableHeader}>
              <TableRow className={classes.tableHeaderRow}>
                <TableCell className={classes.tableHeaderCell}>
                  <Typography variant="h6" className={classes.tableHeaderTxt}>
                    Title
                  </Typography>
                </TableCell>
                <TableCell
                  className={classes.tableHeaderCell}
                  align="right"
                  style={{ width: "35%" }}
                >
                  <Typography variant="h6" className={classes.tableHeaderTxt}>
                    Estimated Time (hh:mm)
                  </Typography>
                </TableCell>
                {/* rateType == "differentPerTask" || rateType == "differentHourlyPerTask"  */}
                {(rateType == "differentPerTask" ||
                  rateType == "differentHourlyPerTask") && (
                  <TableCell className={classes.tableHeaderCell} align="left">
                    <Typography variant="h6" className={classes.tableHeaderTxt}>
                      {rateType == "differentPerTask"
                        ? "Fixed Rate"
                        : "Hourly Rate"}
                    </Typography>
                  </TableCell>
                )}
                <TableCell
                  className={classes.tableHeaderCell2}
                  align="right"
                ></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.taskId} className={classes.tableRow}>
                  <TableCell
                    component="th"
                    scope="row"
                    className={classes.tableHeaderCell}
                  >
                    <Typography variant="h6">{row.taskTitle}</Typography>
                  </TableCell>
                  <TableCell align="right" className={classes.tableBodyCell}>
                    <DurationInput
                      clearSelect={() => {}}
                      disabledInput={false}
                      updateTime={(hrs, mins) =>
                        this.props.updateEstimatedTime(row, hrs, mins)
                      }
                      hours={row.hours}
                      mins={row.mins}
                      styles={{ margin: 0 }}
                      disableError={true}
                    />
                  </TableCell>
                  {/* rateType == "differentPerTask" || rateType == "differentHourlyPerTask" */}
                  {(rateType == "differentPerTask" ||
                    rateType == "differentHourlyPerTask") && (
                    <TableCell
                      align="right"
                      className={classes.tableBodyCell}
                      style={{ width: 97 }}
                    >
                      <DefaultTextField
                        label=""
                        // fullWidth={true}
                        errorState={false}
                        formControlStyles={{
                          marginBottom: 0,
                          maxWidth: 82,
                        }}
                        errorMessage=""
                        error={false}
                        defaultProps={{
                          id: `hourlyRate-${row.taskId}`,
                          type: "text",
                          onChange: (event) => {
                            this.props.handleInputTaskTable(
                              event,
                              row,
                              rateType
                            );
                          },
                          value:
                            rateType == "differentPerTask"
                              ? row.fixedRate
                              : row.hourlyRate,
                          autoFocus: false,
                          placeholder: "",
                          inputProps: { maxLength: 85 },
                          startAdornment: (
                            <InputAdornment
                              position="start"
                              classes={{
                                positionStart: classes.detailsStartIA,
                              }}
                            >
                              <Typography
                                variant="h6"
                                className={classes.currencySymbol}
                              >
                                {currencySymbol}
                              </Typography>
                              {/* <AttachMoneyIcon
                                style={{ color: "#969696", fontSize: "19px !important" }}
                              /> */}
                            </InputAdornment>
                          ),
                        }}
                      />
                    </TableCell>
                  )}
                  <TableCell align="right" style={{ padding: "4px 0" }}>
                    <DefaultButton
                      buttonType="smallIconBtn"
                      disableRipple
                      onClick={() => {
                        this.props.removeTask(row);
                      }}
                    >
                      <SvgIcon viewBox="0 0 22 22" style={{ fontSize: "19px !important" }}>
                        <CrossIcon />
                      </SvgIcon>
                    </DefaultButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      </Fragment>
    );
  }
}

export default compose(withStyles(tableStyle, { withTheme: true }))(
  ProjectTaskTable
);
