import React from "react";
import Grid from "@material-ui/core/Grid";

import CustomFieldsText from "../CustomFieldsViews/CustomFieldsText.view";
import CustomFieldsTextArea from "../CustomFieldsViews/CustomFieldsTextArea.view";
import CustomFieldsLocation from "../CustomFieldsViews/CustomFieldsLocation.view";
import CustomFieldsEmail from "../CustomFieldsViews/CustomFieldsEmail.view";
import CustomFieldsUrl from "../CustomFieldsViews/CustomFieldsWebSite.view";
import CustomFieldsDate from "../CustomFieldsViews/CustomFieldsDate.view";
import CustomFieldsMoney from "../CustomFieldsViews/CustomFieldsMoney.view";
import CustomFieldsNumber from "../CustomFieldsViews/CustomFieldsNumber.view";
import CustomFieldsCountry from "../CustomFieldsViews/CustomFieldsCountry.view";
import CustomFieldsPhone from "../CustomFieldsViews/CustomFieldsPhone.view";
import CustomFieldsDropDown from "../CustomFieldsViews/CustomFieldsDropDown.view";
import CustomFieldsPeople from "../CustomFieldsViews/CustomFieldsPeople.view";
import CustomFieldsRating from "../CustomFieldsViews/CustomFieldsRating.view";
import CustomFieldsFormula from "../CustomFieldsViews/CustomFieldsFormula.view";
import CustomFieldsFileAndMedia from "../CustomFieldsViews/CustomFieldsFileAndMedia.view";

function CustomFieldComponents(props) {
  const {
    groupType,
    disabled,
    permission,
    customFieldChange,
    customFieldData,
    field,
    handleDeleteField,
    handleEdit,
    handleCopyField,
    customProps,
    handleSelectHideOption,
    groupId,
  } = props;

  const Components = {
    textfield: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsText
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    textarea: (
      <Grid item xs={12} sm={12} md={12} key={field.fieldId}>
        <CustomFieldsTextArea
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    location: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsLocation
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    email: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsEmail
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    websiteurl: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsUrl
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    date: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsDate
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    money: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsMoney
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    number: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsNumber
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    country: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsCountry
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    dropdown: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsDropDown
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    phone: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsPhone
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    people: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsPeople
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    rating: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsRating
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    formula: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsFormula
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
        />
      </Grid>
    ),
    filesAndMedia: (
      <Grid item xs={12} sm={12} md={field.settings.layoutWidth} key={field.fieldId}>
        <CustomFieldsFileAndMedia
          groupType={groupType}
          disabled={disabled}
          permission={permission}
          customFieldChange={customFieldChange}
          customFieldData={customFieldData}
          field={field}
          handleDeleteField={handleDeleteField}
          handleEdit={handleEdit}
          handleCopyField={handleCopyField}
          handleSelectHideOption={handleSelectHideOption}
          {...customProps}
          groupId={groupId}
        />
      </Grid>
    ),
  };
  const ExportCmp = () => Components[field.fieldType];
  return <>{ExportCmp()}</>;
}

export default CustomFieldComponents;
