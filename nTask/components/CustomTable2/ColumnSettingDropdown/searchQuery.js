let searchQuery = {
  taskFilter: {},
  quickFilters: {},
  issueFilter: {},
  projectFilter: {},
  meetingFilter: {},
  riskFilter: {},
};

export default searchQuery;
