const headerComponentsStyles = theme => ({
  tableHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    color: theme.palette.text.lightGray,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
    width: '100%',
  },
  menuIcon:{
    fontSize: "16px !important",
    color: theme.palette.icon.gray600
  },
  ascSortIcon: {
    fontSize:14,
    color: theme.palette.icon.gray600
  },
  descSortIcon: {
    fontSize:13,
    color: theme.palette.icon.gray600
  },
  active: {
    display: 'inline-block'
  },
  inactive: {
    display: 'none'
  },
  headerName: {
    left: '50%',
    transform: 'translateX(-50%)',
    position: 'absolute',
    display: 'flex',
    alignItems: 'center'
  },
  headerNameLeft: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: 30
  },
  headerInnerCnt: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    '&:hover $headerName': {
transform: 'none',
        position: 'absolute',
        left: 28,
      // transition: 'ease all 0.2s'
    },
    '&:hover $columnSettingBtnCnt': {
display: 'inline-block',
      visibility: 'visible'
    }
  },
  columnSettingBtnCnt:{
    visibility: 'hidden'
  },

  sortCnt: {
    borderRadius: 36,
    marginLeft: 10,
    display: 'flex',
    alignItems: 'center'
  },
  filterIconCnt: {
    marginLeft: 5,
    display: 'flex',
    alignItems: 'center',
    '& svg': {
      fontSize: "20px !important"
    }
  },
sortIndexText: {
  fontSize: "12px !important",
  marginRight: 3
}
});

export default headerComponentsStyles;
