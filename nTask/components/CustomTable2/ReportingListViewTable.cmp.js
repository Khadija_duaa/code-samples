import React, { useState, useEffect, useRef, useMemo } from "react";
import "@ag-grid-community/core/dist/styles/ag-grid.css";
import "@ag-grid-community/core/dist/styles/ag-theme-alpine.css";
import { ClipboardModule } from "@ag-grid-enterprise/clipboard";
import { ExcelExportModule } from "@ag-grid-enterprise/excel-export";
import { MenuModule } from "@ag-grid-enterprise/menu";
import { RowGroupingModule } from "@ag-grid-enterprise/row-grouping";
import { SetFilterModule } from "@ag-grid-enterprise/set-filter";
import { MultiFilterModule } from "@ag-grid-enterprise/multi-filter";
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model'
import { AgGridReact } from "@ag-grid-community/react";
import isEqual from "lodash/isEqual";
import tableStyles from "./table.style";
import withStyles from "@material-ui/core/styles/withStyles";
import "./table.style.css";
import AddNewInput from "./addNewInput/addNewInput.cmp";
import { useDispatch, useSelector } from "react-redux";
import { saveRowOrder, updateTaskCountAction } from "../../redux/actions/tasks";
import { calculateContentHeight } from "../../utils/common";
import AggregationComponents from "./AggregationComponents/AggregationComponents";
import HeaderCmp from "./ReportHeaderComponent/headerComponent";
import { saveReportingColumnPostion } from "../../redux/actions/grid";
import { updateColumn, updateTeamColumn, updateColumnAction } from "../../redux/actions/columns";

import { grid } from "./gridInstance";
import { getContrastYIQ } from "../../helper/index";
import { getRowStyles } from "../../helper/listView/listView.helper";

let sortActive = false;
let filterActive = false;

const CustomTable = props => {
  const {
    children,
    defaultColDef,
    headerHeight,
    classes,
    theme,
    createableProps,
    createable,
    frameworkComponents,
    tableHeight,
    isExternalFilterPresent,
    doesExternalFilterPass,
    data,
    type,
    groupType,
    gridProps,
    headerProps,
    onSelectionChanged,
    selectedTasks,
    onRowGroupChange,
    columns,
  } = props;
  const taskStatusGroup = useRef(null);

  useEffect(()=>{
    taskStatusGroup.current = headerProps.taskStatusGroup
  },[headerProps.taskStatusGroup])

  const [rowOrder, setRowOrder] = useState([]);

  const dispatch = useDispatch();
  let gridApi = useRef(null);
  let gridColumnApi = useRef(null);
  const state = useSelector(state => {
    return {
      columns: state.columns,
      reportingFilters: state.reportingFilters?.[type],
    };
  });
  const { reportingFilters } = state;
  const onGridReady = params => {
    gridApi.current = params.api;
    gridColumnApi = params.columnApi;

    grid.grid = params.api;
    // params.api.showLoadingOverlay()
    // params.api.setRowsData(dataWithId);
    // updateColumnDef();

    // dispatch(saveTaskGridInstance(params.api))
    getGridColumnState();

    // fetch("https://test.ntaskmanager.com/api/tasks?records=0", { headers: { Authorization: "Bearer CAvfHmFA33WWHbqbfSr79g1r9umb8m29ieIqMYy0QWz1dOUyFfDxFMbaZlHTP8JFE4UNXPkPe_Fs2RwUh9RU0RZAdIaSq1Yl4eQgzQEi46jW60lRJpugPd-BXuvPM3oWg463zp60hPR6lqXINoE6R-KYOBG60-yx8GenR3UCbfwo5AZV4QhEKuSrCrhQPeb8fQLgEqV7Vm4UfHJQjD-63ojWVaf2m1lNqPic5aBHRfyuFjEdTcRAXoOKZ1H_o7pcOmxp-3km6l5aAwUE0-zoOjfgIJWCPmklbEODxp4MQjYMONBuvzkFYMIyOMleEmPsdRy02WWBM2VdNvw22DDNLzoCpR4DFaqt9xF2Wpac3lN1vjrx8dBQDrQgvp4DTByAEU6g-PNgQR6FKJ5jlObwwdqVPFIU9-1Uu74QWLxOm44oqRhQcVfxxEzIoM-ALizYrjvRmW1xgOJOfMBFHDOwCDdmtwqbN9ZDgaVebdyWouyX8ZtQ87hwY67Jfk0-16gtpN7_GZSkuo5Iqn-6Sl6Nel1MtseDb0pf_EmUFufbDCHNyT8iEvXQa7zxEBZmlNtAD1TZ3Nc4CPq4QDVCxndm_gBrKmDOiePgyq03-qmZnhlA5tnajaalJ3f0d7KhCpeozo0Im1K2d4VAp3oCSUFtlkjNJ4C8-k425JvjApMzNZ_kPx_u" } })
    //   .then(result => result.json())
    //   .then(data => {
    //     const updatedData = [...rowData, ...data.entity];
    //
    //     const dataWithId = updatedData.map(r => {
    //       return { ...r, id: r.taskId };
    //     });
    //     dataWithId.forEach(function(data, index) {
    //       data.id = index;
    //     });
    //
    //     setRowData(dataWithId);
    //     params.api.setRowData(dataWithId);
    //   });
  };
  // useEffect(() => {
  //   if (data.length && gridApi.current) {
  //     gridApi.current.refreshCells({ force: true })
  //   }
  // }, [data])

  // const updateColumnDef = () => {
  //   setColumnDefs(gridApi.current && gridApi.current.getColumnDefs());
  // };

  const columnRowGroupChanged = () => {
    handleUpdateColumn();
  };
  const ColumnHeaderRenderer = column => {
    return <HeaderCmp
      column={column}
      allColumn={columns}
      gridApi={gridApi.current}
      feature={type}
      groupType={groupType}
      headerProps={{ ...headerProps, taskStatusGroup : taskStatusGroup.current || [] }}
    />;
  };
  const CustomHeaderGroup = column => {
    return (
      <div
        className={classes.columnGorupHeaderCnt}
        style={{
          background: column.columnGroup.providedColumnGroup.colGroupDef.color,
          color: getContrastYIQ(column.columnGroup.providedColumnGroup.colGroupDef.color),
        }}>
        {column.displayName}
      </div>
    );
  };
  //on grid sort changed
  const onSortChanged = data => {
    //Save sort columns to backend
    let suppressRowDrag = sortActive;
    // || filterActive;
    gridApi.current.setSuppressRowDrag(suppressRowDrag);
    handleUpdateColumn();
  };

  const onFilterChanged = () => {
    filterActive = gridApi.current.isAnyFilterPresent();
    // let suppressRowDrag = sortActive
    // || filterActive;
    // gridApi.current.setSuppressRowDrag(suppressRowDrag);
    dispatch(updateTaskCountAction(gridApi.current.rowModel.rowsToDisplay.filter(r => !r.group).length));
  };
  // useEffect(() => {
  //   gridApi.current && gridApi.current.onFilterChanged();
  // }, [reportingFilters]);

  const onRowDragMove = event => {
    const isDataGrouped = event.columnApi.getRowGroupColumns().length;
    let rowsData = data;
    let movingNode = event.node;
    let overNode = event.overNode;
    let overNodeField = overNode.parent.field;
    let movingData = movingNode.data;
    let overData = overNode.data;
    let rowNeedsToMove = !isEqual(movingNode, overNode);
    let groupCountry;

    function moveInArray(arr, fromIndex, toIndex) {
      let element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    if (rowNeedsToMove && !isDataGrouped) {
      let fromIndex = data.indexOf(movingData);
      let toIndex = data.indexOf(overData);
      let newStore = data.slice();
      moveInArray(newStore, fromIndex, toIndex);
      rowsData = newStore;
      gridApi.current.setRowData(newStore);
      gridApi.current.clearFocusedCell();
    }
  };

  // useEffect(() => {
  //    if(!selectedTasks || !selectedTasks.length ){
  //      gridApi.current && gridApi.current.deselectAll()
  //    }
  // }, [selectedTasks])
  const getRowNodeId = data => {
    return data.id;
  };
  // const columnRowGroupChanged = (data) => {
  //
  //   saveGridSettings(data.columnApi.getColumnState())
  // }
  //Row Drag End
  const onRowDragEnd = event => {
    if (event.overNode.parent.field == "statusTitle") {
      return false;
    }
    const isDataGrouped = event.columnApi.getRowGroupColumns().length;
    let rowsData = data;
    let movingNode = event.node;
    let overNode = event.overNode;
    let overNodeField = overNode.parent.field == "project" ? "projectId" : overNode.parent.field;
    let movingData = movingNode.data;
    let overData = overNode.data;

    let needToChangeParent = movingData[overNodeField] !== overData[overNodeField];
    if (needToChangeParent && isDataGrouped) {
      movingData[overNodeField] = overData[overNodeField];
      if (overNodeField == "projectId") {
        movingData.projectInfor = overData.projectInfor;
      }
      gridApi.current.applyTransaction({ update: [movingData] });
      gridApi.current.clearFocusedCell();
      onRowGroupChange(movingData, overNodeField);
    } else {
      let arr = [];
      gridApi.current.forEachNode((rowNode, index) => {
        rowNode.data && arr.push(rowNode.data);
      });
      const newRowOrder = arr.map((ele, i) => {
        return { ItemId: ele.taskId, ItemOrder: i };
      });
      saveRowOrder(type, newRowOrder, dispatch);
      setRowOrder(newRowOrder);
    }
  };

  //Pinned Row Renderer
  const CustomPinnedRowRenderer = data => {
    return <AggregationComponents data={data} />;
  };
  //Pinned Row Renderer
  const onGridResize = row => {
  };
  //on any column drag/resize event called
  const onColumnDragEnd = data => {
    const isResized = data.target.classList.contains("ag-header-cell-resize");
    const isGroupOrderChanged = data.target.classList.contains(
      "ag-column-drop-horizontal-cell-drag-handle",
    );
    if (isResized || isGroupOrderChanged) {
      // In case column is resize
      handleUpdateColumn();
      return;
    }

    //In case column position is changed
    const updatedColumnOrders = data.columnApi.getColumnState().reduce(
      (r, cv, i) => {
        const selectedColumn = columns.find(col => col.columnKey == cv.colId);
        if (!headerProps.columnDragDisabled.includes(cv.colId)) {
          r.gridColumns.push({ columnId: selectedColumn.id, position: i });
          r.featuredColumns.push({ ...selectedColumn, position: i });
        }
        return r;
      },
      { gridColumns: [], featuredColumns: [] },
    );
    headerProps.columnChangeCallback(updatedColumnOrders.featuredColumns)
    dispatch(updateColumnAction(updatedColumnOrders.featuredColumns));
    saveReportingColumnPostion(updatedColumnOrders.gridColumns, type, groupType, dispatch);
  };

  const handleUpdateColumn = () => {
    const columnState = gridApi.current.columnModel.getColumnState();
    const featureColumns = columnState.reduce((r, cv) => {
      const actualColumnObj = window[`${type}Columns`].find(c => c.columnKey == cv.colId);
      if (actualColumnObj) {
        const { aggFunc, colId, pivot, pivotIndex, ...rest } = actualColumnObj;
        r.push({ ...actualColumnObj, ...cv, columnId: actualColumnObj.id });
      }
      return r;
    }, []);

    // updateColumn(featureColumns, type, dispatch);
    headerProps.columnChangeCallback(featureColumns)
    updateTeamColumn(type, groupType, featureColumns, res => { }, fail => { console.log('api fail') })
  };

  const handleRowSelection = e => {
    const selectedNodes = gridApi.current.getSelectedNodes();
    onSelectionChanged(selectedNodes);
  };
  const getGridColumnState = () => {
    // getGridSettings(
    //   //Success
    //   (res) => {
    //     //Removing static column
    //     const actualColumns = res.filter(c => c.colId !== 'columnDropdown')
    //     gridColumnApi.columnController.applyColumnState({ state: actualColumns })
    //   })
  };
  const isRowGrouped = useMemo(() => {
    return columns.some(c => c.rowGroup);
    return false;
  });
  const extraContentHeight = localStorage.getItem("oldSidebarView") == "true" ? 58 : 55;

  return (
    <div
      className="ag-theme-alpine"
      style={{ position: "relative", height: calculateContentHeight() + extraContentHeight, flex: 1, overflow: 'hidden' }}>
      <span className={classes.columnSelectionDDCnt} style={{ top: isRowGrouped ? 74 : "" }}></span>
      <AgGridReact
        defaultColDef={defaultColDef}
        rowHeight={40}
        headerHeight={headerHeight}
        modules={[ClientSideRowModelModule, MultiFilterModule, SetFilterModule, RowGroupingModule, MenuModule, ExcelExportModule, ClipboardModule]}
        rowBuffer={25}
        rowGroupPanelShow={"onlyWhenGrouping"}
        rowData={data}
        frameworkComponents={{
          agColumnHeader: ColumnHeaderRenderer,
          customHeaderGroupComponent: CustomHeaderGroup,
          ...frameworkComponents,
        }}
        //Row Drag
        // rowDragManaged={true}
        // suppressMoveWhenRowDragging={true}
        suppressContextMenu={false}
        suppressCellSelection={true}
        animateRows={false}
        doesExternalFilterPass={doesExternalFilterPass}
        isExternalFilterPresent={isExternalFilterPresent}
        suppressDragLeaveHidesColumns={true}
        debounceVerticalScrollbar={false}
        enableMultiRowDragging={true}
        immutableData={true}
        getRowNodeId={getRowNodeId}
        onGridReady={onGridReady}
        onSortChanged={onSortChanged}
        onFilterChanged={onFilterChanged}
        onRowDragEnd={onRowDragEnd}
        onRowDragMove={onRowDragMove}
        onDragStopped={onColumnDragEnd}
        onSelectionChanged={handleRowSelection}
        onColumnRowGroupChanged={columnRowGroupChanged}
        suppressRowClickSelection={true}
        getRowStyle={function (params) {

          if (params.node.group) {
            return getRowStyles(params, theme, groupType);
          }
          if (params.node.rowPinned) {
            return { "font-weight": "bold" };
          }
        }}
        getRowClass={function (params) {
          if (params.node.group) {
            return "statusGroupRow";
          }
        }}
        // pinnedBottomRowData={pinnedRowData}
        // pinnedTopRowData={pinnedRowData}
        groupDefaultExpanded={0}
        groupRowRendererParams={{
          innerRenderer: "groupRowInnerRenderer",
          checkbox: false,
        }}
        rowSelection="multiple"
        groupSelectsChildren={true}
        groupSelectsFiltered={true}
        // fullWidthCellRenderer={"fullWidthCellRenderer"}
        // isFullWidthCell={function (rowNode) {
        //   return rowNode.rowPinned == "top";
        // }}
        {...gridProps}>
        {children}
      </AgGridReact>
    </div>
  );
};

CustomTable.defaultProps = {
  headerHeight: 30,
  classes: {},
  theme: {},
  createableProps: { placeholder: "Enter text here", createBtnText: "Add new" },
  createable: true,
  data: [],
  columns: []
};
export default withStyles(tableStyles, { withTheme: true })(CustomTable);
