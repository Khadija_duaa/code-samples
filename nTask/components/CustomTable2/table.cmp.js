import React, { useState, useEffect, useRef } from "react";
import { AgGridColumn, AgGridReact } from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";
import "ag-grid-enterprise";
import MenuIcon from "@material-ui/icons/Menu";
import isEqual from "lodash/isEqual";
import tableStyles from "./table.style";
import withStyles from "@material-ui/core/styles/withStyles";
import "./table.style.css";
import CustomIconButton from "../Buttons/CustomIconButton";
import AddNewInput from "./addNewInput/addNewInput.cmp";
import ColumnSelectionDropdown from "../Dropdown/SelectedItemsDropDown";
import ColumnSettingDropdown from "./ColumnSettingDropdown/columnSettingDropdown";

let sortActive = false;
let filterActive = false;

const CustomTable = ({
                       children,
                       defaultColDef,
                       headerHeight,
                       classes,
                       createableProps,
                       createable,
                       frameworkComponents,
                       tableHeight,
                     }) => {

  const [rowData, setRowData] = useState([]);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [gridApi, setGridApi] = useState(null);
  const [gridColumnApi, setGridColumnApi] = useState(null);
  // const [columnDefs, setColumnDefs] = useState(null);
  const [rowOrder, setRowOrder] = useState([]);

  const onGridReady = (params) => {
    setGridApi(params.api);
    setGridColumnApi(params.columnApi);
    params.api.sizeColumnsToFit();
    const dataWithId = rowData.map(r => {
      return { ...r, id: r.taskId };
    });
    dataWithId.forEach(function(data, index) {
      data.id = index;
    });

    params.api.setRowData(dataWithId);
    // updateColumnDef();
    fetch("https://test.ntaskmanager.com/api/tasks?records=0", { headers: { Authorization: "Bearer CAvfHmFA33WWHbqbfSr79g1r9umb8m29ieIqMYy0QWz1dOUyFfDxFMbaZlHTP8JFE4UNXPkPe_Fs2RwUh9RU0RZAdIaSq1Yl4eQgzQEi46jW60lRJpugPd-BXuvPM3oWg463zp60hPR6lqXINoE6R-KYOBG60-yx8GenR3UCbfwo5AZV4QhEKuSrCrhQPeb8fQLgEqV7Vm4UfHJQjD-63ojWVaf2m1lNqPic5aBHRfyuFjEdTcRAXoOKZ1H_o7pcOmxp-3km6l5aAwUE0-zoOjfgIJWCPmklbEODxp4MQjYMONBuvzkFYMIyOMleEmPsdRy02WWBM2VdNvw22DDNLzoCpR4DFaqt9xF2Wpac3lN1vjrx8dBQDrQgvp4DTByAEU6g-PNgQR6FKJ5jlObwwdqVPFIU9-1Uu74QWLxOm44oqRhQcVfxxEzIoM-ALizYrjvRmW1xgOJOfMBFHDOwCDdmtwqbN9ZDgaVebdyWouyX8ZtQ87hwY67Jfk0-16gtpN7_GZSkuo5Iqn-6Sl6Nel1MtseDb0pf_EmUFufbDCHNyT8iEvXQa7zxEBZmlNtAD1TZ3Nc4CPq4QDVCxndm_gBrKmDOiePgyq03-qmZnhlA5tnajaalJ3f0d7KhCpeozo0Im1K2d4VAp3oCSUFtlkjNJ4C8-k425JvjApMzNZ_kPx_u" } })
      .then(result => result.json())
      .then(data => {
        const updatedData = [...rowData, ...data.entity];

        const dataWithId = updatedData.map(r => {
          return { ...r, id: r.taskId };
        });
        dataWithId.forEach(function(data, index) {
          data.id = index;
        });

        setRowData(dataWithId);
        params.api.setRowData(dataWithId);
      });
  };

  const handleClick = (event, column) => {
    setAnchorEl({ anchor: event.currentTarget, column });
  };
  const isDescendant = (parent, child) => {
    let node = child.parentNode;
    while (node != null) {
      if (node == parent) {
        return true;
      }
      node = node.parentNode;
    }
    return false;
  }
  const handleClose = (e) => {
    // setAnchorEl(null);
    let parent = document.querySelector('#columnSettingMenu');
    let children = e.target;
    const checkIsDescendant = isDescendant(parent, children)
    if(checkIsDescendant){
      return false
    } else {
      setAnchorEl(null);
    }
  };

  useEffect(() => {
    fetch("https://test.ntaskmanager.com/api/tasks?records=1", { headers: { Authorization: "Bearer frnYV2T7G8c4KKSJET_gZXNbp_sy5nfY25cY3k1rLs9s8H-rpv0zcc1aclD6BT2Sbqa9XA0n22IChFVcDCKpRUHNgPz_tSPjwjrmjSAo7Pn1x6CcXkj5RssuC7P9YhzixKsDRsJO0j53Qjl-3h13qOdAqcBTW02N9fiy2bqtkQGMAmURzLUwtNa9a4-O3M01MREG4uoy3CgpxcXyR8OVoUjZL8L3uHfYU-go1XL833Kc5G-zMu4fdSjpsB0F9ILow5wbgsXvA52kHRAaZ4U-HMHj7GzdDB1aw9YUUDtV9zpQx8EdBIX-wjzU_vA7TDLg4PclkACiDo7VOolqvf7fISFKqFiEOVAcVrlCk0kK_ZdkZEmeUu0ukEUi8rLNre5GyoTi9KiZB185QpAP-yWHuOpMiuaI8dbMPYw4qhc2uqK0N0VK4aIVgDRzJTK5IEgFhZ5DiQzy0b9AQc-R5G1p8AwuTcr5wLaBHa_nUOuCcc0xVMhnpd-yY1H8oiKqbAxnE4sAlU_cSnlVA6KQxco6pPaX_-TARJI7vdui4cgEa96sHxg2MQ2HlDdWAWknmYG8NTkOg5Jnq-5hYl9lP95L_1msFWvdsowS7oLvi8rLfLFhYyhmjah_QOcx0Wes-pGjScIVWNbeLzoT81dc7jrMR10zXSyL1aWDHUGITU6lzfmZi1CQDd332NfCM-OUdVgZT1068w" } })
      .then(result => result.json())
      .then(data => {

        setRowData(data.entity);

      });
  }, []);
//Column Pinning
  const handlePin = (ele, direction, e) => {
    e.stopPropagation()
    switch (direction) {
      case "left":
        gridApi.columnController.applyColumnState({
          state: [
            {
              colId: ele.column.colId,
              pinned: "left",
            },
          ],
        });
        break;
      case "right":
        gridApi.columnController.applyColumnState({
          state: [
            {
              colId: ele.column.colId,
              pinned: "right",
            },
          ],
        });
        break;
      case "none":
        gridApi.columnController.applyColumnState({
          state: [
            {
              colId: ele.column.colId,
              pinned: null,
            },
          ],
        });
        break;
      case "clear":
        gridApi.columnController.applyColumnState({
          defaultState: {
            pinned: null,
          },
        });
        break;
    }
    // updateColumnDef();
    handleClose();
  };

  //Group By
  const handleGroupBy = (ele, type) => {
    switch (type) {
      case "groupBy":

        gridApi.columnController.applyColumnState({
          state: [
            {
              colId: ele.column.colId,
              rowGroup: true,
            },
          ],
        });
        break;
    }
    handleClose();
  };
  const HeaderCmp = (x, y, z) => {

    return x.column.colId == "columnDropdown" ? <ColumnSelectionDropdown
        feature={"task"}
        // columnChangeCallback={this.columnChangeCallback}
        hideColumns={["matrix"]}
      /> :

      <div className={classes.tableHeader}><span>{x.displayName}</span>
        {x.column.colDef.showRowGroup ? null :
          <CustomIconButton btnType="transparent" style={{ padding: 0 }} id={x.displayName}
                            onClick={(e) => handleClick(e, x)}>
            <MenuIcon className={classes.menuIcon}/>
          </CustomIconButton>}

      </div>;

  };

  const onSortChanged = () => {
    let sortModel = gridApi.getSortModel();
    sortActive = sortModel && sortModel.length > 0;
    let suppressRowDrag = sortActive || filterActive;
    console.log(
      "sortActive = " +
      sortActive +
      ", filterActive = " +
      filterActive +
      ", allowRowDrag = " +
      suppressRowDrag,
    );
    gridApi.setSuppressRowDrag(suppressRowDrag);
    handleClose();
  };

  const onFilterChanged = () => {

    filterActive = gridApi.isAnyFilterPresent();
    let suppressRowDrag = sortActive || filterActive;
    console.log(
      "sortActive = " +
      sortActive +
      ", filterActive = " +
      filterActive +
      ", allowRowDrag = " +
      suppressRowDrag,
    );
    gridApi.setSuppressRowDrag(suppressRowDrag);

  };
  const onRowDragMove = (event) => {
    const isDataGrouped = event.columnApi.getRowGroupColumns().length;
    let data = rowData;
    let movingNode = event.node;
    let overNode = event.overNode;
    let movingData = movingNode.data;
    let overData = overNode.data;
    let rowNeedsToMove = !isEqual(movingNode, overNode);
    let groupCountry;

    function moveInArray(arr, fromIndex, toIndex) {
      let element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    if (rowNeedsToMove && !isDataGrouped) {
      let fromIndex = data.indexOf(movingData);
      let toIndex = data.indexOf(overData);
      let newStore = data.slice();
      moveInArray(newStore, fromIndex, toIndex);
      data = newStore;
      gridApi.setRowData(newStore);
      gridApi.clearFocusedCell();
    } else {
      if (overNode.group) {
        groupCountry = overNode.key;
      } else {
        groupCountry = overNode.data.priority;
      }
      let needToChangeParent = movingNode.priority !== groupCountry;
      if (needToChangeParent) {
        movingData.priority = groupCountry;
        gridApi.applyTransaction({ update: [movingData] });
        gridApi.clearFocusedCell();
      }

    }
  };
  // const onRowDragMove = (event) => {

  // };
  // const updateColumnDef = () => {
  //
  //   setColumnDefs(gridApi && gridApi.getColumnDefs());
  // };
  const getRowNodeId = (data) => {
    return data.id;
  };

  //Row Drag End
  const onRowDragEnd = () => {
    let arr = [];
    gridApi.forEachNode((rowNode, index) => {
      rowNode.data && arr.push(rowNode.data);
    });
    const newRowOrder = arr.map((ele, i) => {

        return { id: ele.taskId, index: i };
      },
    );
    setRowOrder(newRowOrder);
  };

  //Pinned Row Renderer
  const CustomPinnedRowRenderer = (e) => {

    return <span>test</span>;

  }; //Pinned Row Renderer
  const createItemRenderer = () => {
    return createable ?
      <AddNewInput placeholder={createableProps.placeholder} id={createableProps.id}
                   btnText={createableProps.btnText}/> : null;
  };

  return (
    <div className="ag-theme-alpine" style={{ height: tableHeight, width: "100%" }}>
      {rowData.length ?
        <AgGridReact
          defaultColDef={defaultColDef}
          rowHeight={40}
          headerHeight={headerHeight}
          rowSelection="multiple"
          rowGroupPanelShow={"onlyWhenGrouping"}
          // rowData={rowData}
          frameworkComponents={{
            agColumnHeader: HeaderCmp,
            customPinnedRowRenderer: CustomPinnedRowRenderer,
            fullWidthCellRenderer: createItemRenderer, ...frameworkComponents,
          }}
          //Row Drag
          // rowDragManaged={true}
          // suppressMoveWhenRowDragging={true}
          suppressCellSelection={true}
          animateRows={true}
          enableMultiRowDragging={true}
          immutableData={true}
          getRowNodeId={getRowNodeId}
          onGridReady={onGridReady}
          onSortChanged={onSortChanged}
          onFilterChanged={onFilterChanged}
          onRowDragEnd={onRowDragEnd}
          onRowDragMove={onRowDragMove}
          getRowStyle={function(params) {
            if (params.node.rowPinned) {
              return { "font-weight": "bold" };
            }
          }}
          pinnedBottomRowData={pinnedRowData}
          pinnedTopRowData={pinnedRowData}
          groupDefaultExpanded={1}
          fullWidthCellRenderer={"fullWidthCellRenderer"}
          isFullWidthCell={function(rowNode) {
            return rowNode.rowPinned == "top";
          }}
        >
          {children}

        </AgGridReact> : null}
      <ColumnSettingDropdown gridApi={gridApi} anchorEl={anchorEl} closeAction={handleClose}/>

    </div>
  );
};

CustomTable.defaultProps = {
  headerHeight: 24,
  classes: {},
  theme: {},
  createableProps: { placeholder: "Enter text here", createBtnText: "Add new" },
  createable: false,
};
export default withStyles(tableStyles, { withTheme: true })(CustomTable);