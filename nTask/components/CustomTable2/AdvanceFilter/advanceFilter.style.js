const advanceFilterStyles = theme => ({
  advanceFilterCnt: {
    display: 'flex',
    position: 'relative'
  },
  advanceFilterTitleBar: {
    width: 30,
    background: theme.palette.background.medium,
    height: '100%',
    '& :hover': {
      cursor: 'pointer',
    },
    '& label': {
      // width: 110,
      display: 'block',
      transform: 'rotateZ(90deg) translateY(-26px) translateX(12px)',
      transformOrigin: '0 0',
      fontSize: "14px !important",
      fontWeight: 400,
      marginTop :-7,
      marginLeft:3
    }
  },
  advanceFilterTitleBarOpen: {
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
  },
  filterContentCntOpen: {
    width: 390,
    height: '100%',
    transition: 'ease width 0.4s',
    position: 'relative'
  },
  filterContentCntClose: {
    width: 0,
    height: '100%',
    overflow: 'hidden',
    transition: 'ease width 0.4s',
    position: 'relative'
  },
  advanceFilterSelected: {
    background: theme.palette.background.lightBlue
  },
  filterIcon:{
    fontSize: "15px !important",
    marginTop: 15,
    // marginLeft: 9
  },
  iconContainer:{
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  }
});

export default advanceFilterStyles;
