import React, { useEffect, useRef, useState } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import advanceFilterStyles from "./advanceFilter.style";
import clsx from "clsx";
import IconFilter from "../../Icons/IconFilter";
import SvgIcon from "@material-ui/core/SvgIcon";

function AdvanceFilter(props) {
  const { children, classes, isFilterApplied } = props;
  const [open, setOpen] = useState(false);
  const handleFilterOpen = () => {
    setOpen(!open);
  };
  return (
    <div className={classes.advanceFilterCnt}>
      <div
        className={clsx({
          [classes.advanceFilterTitleBar]: true,
          [classes.advanceFilterSelected]: isFilterApplied,
          [classes.advanceFilterTitleBarOpen]: open,
        })}
        onClick={handleFilterOpen}>
        <div className={classes.iconContainer}>
          <SvgIcon viewBox="0 0 15 13.5" className={classes.filterIcon}>
            <IconFilter />
          </SvgIcon>
          <label>Filters</label>
        </div>
      </div>

      <div className={open ? classes.filterContentCntOpen : classes.filterContentCntClose}>
        {children}
      </div>
    </div>
  );
}

AdvanceFilter.defaultProps = {};
export default withStyles(advanceFilterStyles, { withTheme: true })(AdvanceFilter);
