import React, { useEffect } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Slider, { Range } from "rc-slider";
import "rc-slider/assets/index.css";
import { useState } from "react";
import { useSelector } from "react-redux";
import filterComponentsStyle from "./filterComponents.style";
import CustomListItem from "../../ListItem/CustomListItem";
import CustomMenuList from "../../MenuList/CustomMenuList";
import CustomDatePicker from "../../../components/DatePicker/DatePicker/DatePicker";
import CustomMultiSelectDropdown from "../../Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import CustomMultiSelectDropdown2 from "../../Dropdown/multiSelectWithSearchAndGroup/Dropdown";
import { generateProjectData, generateAssigneeData } from "../../../helper/generateSelectData";
import { generateWorkspaceData } from "../../../helper/generateTasksOverviewDropdownData";
import { compose } from "redux";
import { connect } from "react-redux";
import "./rc-slider.less";
import moment from "moment";
import {
  severity,
  typeData,
  statusData,
  generateTaskDropdownData,
  priorityData,
} from "../../../helper/issueDropdownData";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import clsx from "clsx";
import { priorityData as taskpriorityData } from "../../../helper/taskDropdownData";
import debounce from "lodash/debounce";
import CircularIcon from "@material-ui/icons/Brightness1";

const dateFilterTypes = [
  {
    label: "today",
    value: "Today",
  },
  {
    label: "yesterday",
    value: "Yesterday",
  },
  {
    label: "currentWeek",
    value: "This Week",
  },
  {
    label: "nextWeek",
    value: "Next Week",
  },
  {
    label: "currentMonth",
    value: "This Month",
  },
  {
    label: "nextMonth",
    value: "Next Month",
  },
  {
    label: "custom",
    value: "Custom",
  },
];
const createdDateFilterTypes = [
  {
    label: "today",
    value: "Today",
  },
  {
    label: "yesterday",
    value: "Yesterday",
  },
  {
    label: "currentWeek",
    value: "This Week",
  },
  {
    label: "currentMonth",
    value: "This Month",
  },
  {
    label: "custom",
    value: "Custom",
  },
];

function ReportFilterComponents({
  data,
  classes,
  theme,
  members,
  handleFilterChange,
  workspaceStatus,
  feature,
  projects,
  intl,
  profileState: { workspace },
  customStatusList,
  taskStatusGroup,
}) {
  const { column } = data;
  const { reportingFilters } = useSelector(state => ({
    reportingFilters: state.reportingFilter?.[feature],
  }));
  const { createSliderWithTooltip } = Slider;
  const Range = createSliderWithTooltip(Slider.Range);
  const rowData = data ? data : { data: {} };
  const { dueDateString, dueTime } = rowData;
  const [filterOption, setFilterOption] = useState(null);
  const [fromDate, setFromDate] = useState(null);
  const [toDate, setTodate] = useState(null);

  // useEffect(() => {
  //   console.log('this is header props', customStatusList)
  // }, []);
  useEffect(() => {
    const selectedValue =
      reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues;
    const type = reportingFilters?.[column.colId] && reportingFilters?.[column.colId].type;
    if (type == "custom" && (selectedValue[0] || selectedValue[1])) {
      setFilterOption("custom");
      return;
    }
    setFilterOption(type);
  }, [reportingFilters]);
  const handleDateSave = (type, date) => {
    if (type === "from") {
      /** custom filter start date */
      setFromDate(moment(date).format("l"));
      handleFilterChange(
        [date ? moment(date).format("l") : "", toDate ? toDate : ""],
        "custom"
      ); /** callback calls  */
    }
    if (type === "to") {
      /** custom filter to date */
      setTodate(moment(date).format("l"));
      handleFilterChange(
        [fromDate ? fromDate : "", date ? moment(date).format("l") : ""],
        "custom"
      ); /** callback calls  */
    }
  };
  const onSelectDateItems = option => {
    setFilterOption(option);
    if (option !== "custom") handleFilterChange([], option);
  };
  // on select from dropdown
  // status
  const onStatusFilterSelect = options => {
    if (feature == "taskoverview") {
      handleFilterChange(options);
      return;
    }
    handleFilterChange(options.map(p => p.value));
  };
  // Severity
  const onSeverityFilterSelect = options => {
    handleFilterChange(options.map(s => s.value));
  };
  // type
  const onTypeFilterSelect = options => {
    handleFilterChange(options.map(s => s.value));
  };
  // proprity
  const onPriorityFilterSelect = options => {
    handleFilterChange(options.map(p => p.value));
  };
  // project
  const onreportingFiltersSelect = options => {
    handleFilterChange(options.map(p => p.obj.taskId));
  };
  const onProjectFilterSelect = options => {
    handleFilterChange(options.map(p => p.id));
  };
  // assignee
  const onAssigneeFilterSelect = options => {
    handleFilterChange(options.map(p => p.id));
  };
  const onCreatedByFilterSelect = options => {
    handleFilterChange(options.map(p => p.id));
  };
  const throttleHandleProgress = debounce(range => {
    handleFilterChange(range);
  }, 1000);
  const onProgressSliderFilterSelect = range => {
    throttleHandleProgress(range);
  };

  // generate dropdown data here
  const issueOverviewDdData = statusData(theme, classes, intl);
  const severityData = severity(theme, classes, intl);
  const issueTypeData = typeData(theme, classes, intl);
  const userData = generateAssigneeData(members);
  const projectsOptions = generateProjectData(projects);
  const tasksData = generateTaskDropdownData();
  const workspacesData = generateWorkspaceData(workspace);
  const Components = {
    status: () => {
      const selectedValues =
        (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || [];
      const selectedOptions =
        feature == "issueoverview"
          ? issueOverviewDdData.filter(s => selectedValues.includes(s.value))
          : selectedValues;

      return feature == "issueoverview" ? (
        <>
          <CustomMultiSelectDropdown
            label="status"
            option={selectedOptions}
            options={() => issueOverviewDdData}
            optionIcon={true}
            onSelect={onStatusFilterSelect}
            placeholder="Status"
            valueSelector={"value"}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      ) : (
        <>
          <CustomMultiSelectDropdown2
            label="status"
            option={selectedOptions}
            options={() => taskStatusGroup}
            optionIcon={true}
            onSelect={onStatusFilterSelect}
            placeholder=""
            height="140px"
            width="140px"
            valueSelector={"value"}
            scrollHeight={400}
          />
        </>
      );
    },
    severity: () => {
      const selectedValues =
        (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || [];
      const selectedOptions = severityData.filter(s => selectedValues.includes(s.value));
      return (
        <CustomMultiSelectDropdown
          label="severity"
          option={selectedOptions}
          options={() => severityData}
          optionIcon={true}
          onSelect={onSeverityFilterSelect}
          placeholder="Severity"
          valueSelector={"value"}
          // heading={`Customize Column (${selectedColumns.length}/10)`}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    type: () => {
      const selectedValues =
        (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || [];
      const selectedOptions = issueTypeData.filter(s => selectedValues.includes(s.value));
      return (
        <CustomMultiSelectDropdown
          label="type"
          option={selectedOptions}
          options={() => issueTypeData}
          optionIcon={true}
          onSelect={onTypeFilterSelect}
          placeholder="Type"
          valueSelector={"value"}
          // heading={`Customize Column (${selectedColumns.length}/10)`}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    startDate: () => {
      const selectedType =
        reportingFilters?.[column.colId] && reportingFilters?.[column.colId].type;
      const selectedValue =
        selectedType == "custom" ? reportingFilters?.[column.colId].selectedValues : "";
      return (
        <>
          <CustomMenuList>
            {dateFilterTypes.map(t => {
              const isSelected = filterOption == t.label;
              return (
                <CustomListItem
                  isSelected={isSelected}
                  rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
            {filterOption && filterOption === "custom" ? (
              <>
                <div
                  style={{
                    margin: "0px 6px",
                    border: "1px solid #DDDDDD",
                    borderRadius: "4px",
                    padding: 6,
                  }}>
                  <CustomDatePicker
                    date={selectedValue && selectedValue.length ? selectedValue[0] : ""}
                    label={"From:"}
                    PopperProps={{ disablePortal: true, size: null }}
                    icon={false}
                    dateFormat="MMM DD, YYYY"
                    timeInput={false}
                    onSelect={date => {
                      handleDateSave("from", date);
                    }}
                    disabled={false}
                    deleteIcon={false}
                    placeholder={"Select Date"}
                    containerProps={{ style: { alignItems: "center" } }}
                    btnProps={{
                      style: {
                        background: "transparent",
                        border: "none",
                        padding: 0,
                        textAlign: "left",
                      },
                    }}
                    labelProps={{
                      style: {
                        width: "auto",
                        marginTop: 0,
                        marginRight: 5,
                        fontSize: "14px !important",
                      },
                    }}
                    closeOnDateSelect={true}
                  />
                </div>
                <div
                  style={{
                    margin: "4px 6px",
                    border: "1px solid #DDDDDD",
                    borderRadius: "4px",
                    padding: 6,
                  }}>
                  <CustomDatePicker
                    date={selectedValue && selectedValue.length ? selectedValue[1] : ""}
                    label={"To:"}
                    PopperProps={{ disablePortal: true, size: null }}
                    icon={false}
                    dateFormat="MMM DD, YYYY"
                    timeInput={false}
                    onSelect={date => {
                      handleDateSave("to", date);
                    }}
                    disabled={false}
                    deleteIcon={false}
                    placeholder={"Select Date"}
                    containerProps={{ style: { alignItems: "center" } }}
                    btnProps={{
                      style: {
                        background: "transparent",
                        border: "none",
                        padding: 0,
                        textAlign: "left",
                      },
                    }}
                    labelProps={{
                      style: {
                        width: "auto",
                        marginTop: 0,
                        marginRight: 5,
                        fontSize: "14px !important",
                      },
                    }}
                    closeOnDateSelect={true}
                  />
                </div>
              </>
            ) : (
              <></>
            )}
          </CustomMenuList>
        </>
      );
    },
    dueDate: () => {
      const selectedType =
        reportingFilters?.[column.colId] && reportingFilters?.[column.colId].type;
      const selectedValue =
        selectedType == "custom" ? reportingFilters?.[column.colId].selectedValues : "";
      return (
        <>
          <CustomMenuList>
            {dateFilterTypes.map(t => {
              const isSelected = filterOption == t.label;
              return (
                <>
                  <CustomListItem
                    isSelected={isSelected}
                    rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                    <span>{t.value}</span>
                  </CustomListItem>
                </>
              );
            })}
            {filterOption && filterOption === "custom" ? (
              <>
                <div
                  style={{
                    margin: "0px 6px",
                    border: "1px solid #DDDDDD",
                    borderRadius: "4px",
                    padding: 6,
                  }}>
                  <CustomDatePicker
                    date={selectedValue && selectedValue.length ? selectedValue[0] : ""}
                    label={"From:"}
                    PopperProps={{ disablePortal: true, size: null }}
                    icon={false}
                    dateFormat="MMM DD, YYYY"
                    timeInput={false}
                    onSelect={date => {
                      handleDateSave("from", date);
                    }}
                    disabled={false}
                    deleteIcon={false}
                    placeholder={"Select Date"}
                    containerProps={{ style: { alignItems: "center" } }}
                    btnProps={{
                      style: {
                        background: "transparent",
                        border: "none",
                        padding: 0,
                        textAlign: "left",
                      },
                    }}
                    labelProps={{
                      style: {
                        width: "auto",
                        marginTop: 0,
                        marginRight: 5,
                        fontSize: "14px !important",
                      },
                    }}
                    closeOnDateSelect={true}
                  />
                </div>
                <div
                  style={{
                    margin: "4px 6px",
                    border: "1px solid #DDDDDD",
                    borderRadius: "4px",
                    padding: 6,
                  }}>
                  <CustomDatePicker
                    date={selectedValue && selectedValue.length ? selectedValue[1] : ""}
                    label={"To:"}
                    PopperProps={{ disablePortal: true, size: null }}
                    icon={false}
                    dateFormat="MMM DD, YYYY"
                    timeInput={false}
                    onSelect={date => {
                      handleDateSave("to", date);
                    }}
                    disabled={false}
                    deleteIcon={false}
                    placeholder={"Select Date"}
                    containerProps={{ style: { alignItems: "center" } }}
                    btnProps={{
                      style: {
                        background: "transparent",
                        border: "none",
                        padding: 0,
                        textAlign: "left",
                      },
                    }}
                    labelProps={{
                      style: {
                        width: "auto",
                        marginTop: 0,
                        marginRight: 5,
                        fontSize: "14px !important",
                      },
                    }}
                    closeOnDateSelect={true}
                  />
                </div>
              </>
            ) : (
              <></>
            )}
          </CustomMenuList>
        </>
      );
    },
    actualStartDate: () => {
      const selectedType =
        reportingFilters?.[column.colId] && reportingFilters?.[column.colId].type;
      const selectedValue =
        selectedType == "custom" ? reportingFilters?.[column.colId].selectedValues : "";
      return (
        <>
          <CustomMenuList>
            {dateFilterTypes.map(t => {
              const isSelected = filterOption == t.label;
              return (
                <CustomListItem
                  rootProps={{ onClick: () => onSelectDateItems(t.label) }}
                  isSelected={isSelected}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
          </CustomMenuList>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ""}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[1] : ""}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      );
    },
    actualDueDate: () => {
      const selectedType =
        reportingFilters?.[column.colId] && reportingFilters?.[column.colId].type;
      const selectedValue =
        selectedType == "custom" ? reportingFilters?.[column.colId].selectedValues : "";
      return (
        <>
          <>
            {dateFilterTypes.map(t => {
              const isSelected = filterOption == t.label;
              return (
                <CustomListItem
                  isSelected={isSelected}
                  rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
          </>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ""}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue[1] ? selectedValue[1] : ""}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      );
    },
    createdDate: () => {
      const selectedType =
        reportingFilters?.[column.colId] && reportingFilters?.[column.colId].type;
      const selectedValue =
        selectedType == "custom" ? reportingFilters?.[column.colId].selectedValues : "";
      return (
        <>
          <>
            {createdDateFilterTypes.map(t => {
              const isSelected = filterOption == t.label;
              return (
                <CustomListItem
                  isSelected={isSelected}
                  rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
          </>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ""}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue[1] ? selectedValue[1] : ""}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      );
    },
    updateDate: () => {
      const selectedType =
        reportingFilters?.[column.colId] && reportingFilters?.[column.colId].type;
      const selectedValue =
        selectedType == "custom" ? reportingFilters?.[column.colId].selectedValues : "";
      return (
        <>
          <>
            {createdDateFilterTypes.map(t => {
              const isSelected = filterOption == t.label;
              return (
                <CustomListItem
                  isSelected={isSelected}
                  rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
          </>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ""}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue[1] ? selectedValue[1] : ""}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      );
    },
    updatedDate: () => {
      const selectedType = reportingFilters?.[column.colId] && reportingFilters?.[column.colId].type
      const selectedValue = selectedType == "custom" ? reportingFilters?.[column.colId].selectedValues : "";
      return (
        <>
          <>
            {createdDateFilterTypes.map(t => {
              const isSelected = filterOption == t.label;
              return (
                <CustomListItem
                  isSelected={isSelected}
                  rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
          </>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ""}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue[1] ? selectedValue[1] : ""}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      );
    },
    priority: () => {
      const priorityOptions =
        feature == "taskoverview" ? taskpriorityData(theme, classes) : priorityData(theme, classes);
      const selectedValues =
        (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || [];
      const selectedOptions = priorityOptions.filter(p => selectedValues.includes(p.value));
      return (
        <CustomMultiSelectDropdown
          label=""
          option={selectedOptions}
          options={() => priorityOptions}
          optionIcon={true}
          placeholder="Priority"
          onSelect={onPriorityFilterSelect}
          height="140px"
          valueSelector="value"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    project: () => {
      const selectedValues =
        (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || [];
      const selectedOptions = projectsOptions.filter(p => selectedValues.includes(p.id));
      return (
        <CustomMultiSelectDropdown
          label=""
          option={selectedOptions}
          options={() => projectsOptions}
          optionIcon={false}
          valueSelector="id"
          placeholder="Project"
          onSelect={onProjectFilterSelect}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    tasks: () => {
      const selectedValues =
        (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || [];
      const selectedOptions = tasksData.filter(p => selectedValues.includes(p.obj.taskId));
      return (
        <CustomMultiSelectDropdown
          label=""
          option={selectedOptions}
          options={() => tasksData}
          optionIcon={false}
          valueSelector="id"
          placeholder="Tasks"
          onSelect={onreportingFiltersSelect}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    progress: () => {
      const selectedValue = (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || [40, 80];
      return (
        <>
          <div className={classes.progressSlider}>
            <Range
              allowCross={false}
              defaultValue={selectedValue}
              tipFormatter={value => `${value}%`}
              min={0}
              max={100}
              onChange={onProgressSliderFilterSelect}
            />
          </div>
        </>
      )
    },
    assignee: () => {
      const selectedValues =
        (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.id));
      return (
        <>
          <CustomMultiSelectDropdown
            label="assignee"
            option={selectedOptions}
            options={() => userData}
            optionIcon={true}
            onSelect={onAssigneeFilterSelect}
            placeholder="Assignee"
            valueSelector={"id"}
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      );
    },
    assigneeList: () => {
      const selectedValues =
        (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.id));
      return (
        <>
          <CustomMultiSelectDropdown
            label="assignee"
            option={selectedOptions}
            options={() => userData}
            optionIcon={true}
            onSelect={onAssigneeFilterSelect}
            placeholder="Assignee"
            valueSelector={"id"}
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      );
    },
    createdBy: () => {
      const selectedValues =
        (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.obj.userId));
      return (
        <>
          <CustomMultiSelectDropdown
            label=""
            options={() => userData}
            option={selectedOptions}
            optionIcon={true}
            placeholder="Created By"
            onSelect={onCreatedByFilterSelect}
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      );
    },
    updatedBy: () => {
      const selectedValues =
        (reportingFilters?.[column.colId] && reportingFilters?.[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.obj.userId));
      return (
        <>
          <CustomMultiSelectDropdown
            label=""
            options={() => userData}
            option={selectedOptions}
            optionIcon={true}
            placeholder="Updated By"
            onSelect={onCreatedByFilterSelect}
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      )
    },
    workspaceName: () => {
      return (
        <>
          <CustomMultiSelectDropdown
            label=""
            options={() => workspacesData}
            // option={selectedOptions}
            optionIcon={true}
            placeholder="Workspace Name"
            onSelect={onCreatedByFilterSelect}
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      );
    },
    totalEffort: (
      <div className={classes.totalEffort}>
        <label>80 hrs</label>
        <label>15 mins</label>
        <div className={classes.textLogged}>
          <label>Sum</label>
        </div>
      </div>
    ),
    attachments: (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Sum</label>
      </div>
    ),
    totalComment: (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Avg.</label>
      </div>
    ),
    meetings: (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>min.</label>
      </div>
    ),
    issues: (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Avg.</label>
      </div>
    ),
    risks: (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Avg.</label>
      </div>
    ),
  };
  const SelectedCmp = () => (Components[column.colId] ? Components[column.colId]() : null);
  return <>{SelectedCmp()}</>;
}
const mapStateToProps = state => {
  return {
    members: state.profile.data.teamMember,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    projects: state.projects.data,
    profileState: state.profile.data,
  };
};
ReportFilterComponents.defaultProps = {
  customStatusList: [],
  taskStatusGroup: [],
};
export default compose(
  connect(mapStateToProps),
  withStyles(filterComponentsStyle, { withTheme: true })
)(ReportFilterComponents);
