import React, { useEffect } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Slider, { SliderTooltip } from "rc-slider";
import "rc-slider/assets/index.css";
import { useState } from "react";
import { useSelector } from "react-redux";
import filterComponentsStyle from "./filterComponents.style";
import CustomListItem from "../../ListItem/CustomListItem";
import CustomMenuList from "../../MenuList/CustomMenuList";
import CustomDatePicker from "../../../components/DatePicker/DatePicker/DatePicker";
import CustomMultiSelectDropdown from "../../Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import { priorityData } from "../../../helper/taskDropdownData";
import { generateProjectData } from "../../../helper/generateSelectData";
import { compose } from "redux";
import { connect } from "react-redux";
import CustomAvatar from "../../Avatar/Avatar";
import "./rc-slider.less";
import CircularIcon from "@material-ui/icons/Brightness1";
import moment from "moment";
import isDateEqual from "../../../../src/helper/dates/dates";
import { generateActiveMembers } from "../../../helper/getActiveMembers";
import { statusData as statusdData } from "../../../helper/projectDropdownData";
import debounce from "lodash/debounce";

const generateTaskColumnsData = [
  {
    label: "Workspace",
    value: "workspaceName",
  },
  {
    label: "Project",
    value: "projectName",
  },
  {
    label: "Task Id",
    value: "uniqueId",
  },
  {
    label: "Status",
    value: "statusTitle",
  },
  {
    label: "Priority",
    value: "priority",
  },
  {
    label: "Planned Start Date",
    value: "startDate",
  },
  {
    label: "Planned End Date",
    value: "dueDate",
  },
  {
    label: "Planned Effort",
    value: "plannedEffort",
  },
  {
    label: "Effort To Date",
    value: "effortToDate",
  },
  {
    label: "Actual Start Date",
    value: "actualStartDate",
  },
  {
    label: "Actual End Date",
    value: "actualDueDate",
  },
  {
    label: "Progress",
    value: "progress",
  },
  {
    label: "Assignee",
    value: "assignee",
  },
  {
    label: "Comments",
    value: "comments",
  },
  {
    label: "Time Logged",
    value: "timeLogged",
  },
  {
    label: "Attachments",
    value: "attachments",
  },
  {
    label: "Meetings",
    value: "meetings",
  },
  {
    label: "Issues",
    value: "issues",
  },
  {
    label: "Risks",
    value: "risks",
  },
  {
    label: "Creation Date",
    value: "creationDate",
  },
  {
    label: "Created By",
    value: "createdBy",
  },
  {
    label: "Created Date",
    value: "createdDate",
  },
];
const dateFilterTypes = [
  {
    label: "today",
    value: "Today",
  },
  {
    label: "yesterday",
    value: "Yesterday",
  },
  {
    label: "currentWeek",
    value: "This Week",
  },
  {
    label: "nextWeek",
    value: "Next Week",
  },
  {
    label: "currentMonth",
    value: "This Month",
  },
  {
    label: "nextMonth",
    value: "Next Month",
  },
  {
    label: "custom",
    value: "Custom",
  },
];
const createdDateFilterTypes = [
  {
    label: "today",
    value: "Today",
  },
  {
    label: "yesterday",
    value: "Yesterday",
  },
  {
    label: "currentWeek",
    value: "This Week",
  },
  {
    label: "currentMonth",
    value: "This Month",
  },
  {
    label: "custom",
    value: "Custom",
  },
];

function ProjectFilterComponents({
  data,
  classes,
  theme,
  intl,
  members,
  handleFilterChange,
  workspaceStatus,
  projects,
}) {
  const { column } = data;
  const { projectFilter } = useSelector(state => ({
    projectFilter: state.projects.projectFilter,
  }));
  const { createSliderWithTooltip } = Slider;
  const Range = createSliderWithTooltip(Slider.Range);

  const rowData = data ? data : { data: {} };
  const { dueDateString, dueTime } = rowData;
  const [filterOption, setFilterOption] = useState(null);
  const [fromDate, setFromDate] = useState(null);
  const [toDate, setTodate] = useState(null);

  useEffect(() => {
    const selectedValue = projectFilter[column.colId] && projectFilter[column.colId].selectedValues;
    const type = projectFilter[column.colId] && projectFilter[column.colId].type;
    if (type == 'custom' && (selectedValue[0] || selectedValue[1])) {
      setFilterOption('custom');
      return;
    }
    setFilterOption(type);
  }, [projectFilter]);

  const handleDateSave = (type, date) => {
    if (type === "from") {
      /** custom filter start date */
      setFromDate(moment(date).format("l"));
      handleFilterChange([
        date ? moment(date).format("l") : '', toDate ? toDate : "",
      ], 'custom'); /** callback calls  */
    }
    if (type === "to") {
      /** custom filter to date */
      setTodate(moment(date).format("l"));
      handleFilterChange([
        fromDate ? fromDate : "",
        date ? moment(date).format("l") : '',
      ], 'custom'); /** callback calls  */
    }
  };
  const onSelectDateItems = option => {
    setFilterOption(option);
    if (option !== "custom") handleFilterChange([], option);
  };
  const onPriorityFilterSelect = options => {
    handleFilterChange(options.map(p => p.value));
  };
  const onProjectFilterSelect = options => {
    handleFilterChange(options.map(p => p.id));
  };
  const onAssigneeFilterSelect = options => {
    handleFilterChange(options.map(p => p.obj.userId));
  };
  const onCreatedByFilterSelect = options => {
    handleFilterChange(options.map(p => p.obj.userId));
  };
  const onStatusFilterSelect = options => {
    handleFilterChange(options.map(p => p.value));
  };
  const throttleHandleProgress = debounce(range => {
    handleFilterChange(range);
  }, 1000);
  const onProgressSliderFilterSelect = range => {
    throttleHandleProgress(range);
  };

  const userData = generateActiveMembers().map(x => {
    return {
      label: x.fullName,
      value: x.fullName,
      obj: x,
      icon: (
        <span style={{ marginRight: 5 }}>
          <CustomAvatar
            otherMember={{
              imageUrl: x.imageUrl,
              fullName: x.fullName,
              lastName: "",
              email: x.email,
              isOnline: false,
              isOwner: x.isOwner,
            }}
            size="small18"
          />
        </span>
      ),
    };
  });
  const Components = {
    status: () => {
      // const statusData = generateStatusData();
      const statusData = statusdData(theme, classes, intl);
      const selectedStatus =
        (projectFilter[column.colId] && projectFilter[column.colId].selectedValues) || [];
      const selectedOptions = statusData.filter(s => selectedStatus.includes(s.value));
      return (
        <CustomMultiSelectDropdown
          label=""
          option={selectedOptions}
          options={() => statusData}
          optionIcon={true}
          onSelect={onStatusFilterSelect}
          placeholder="Status"
          valueSelector={'value'}
          // heading={`Customize Column (${selectedColumns.length}/10)`}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    startDate: () => {
      const selectedType = projectFilter[column.colId] && projectFilter[column.colId].type
      const selectedValue = selectedType == 'custom' ? projectFilter[column.colId].selectedValues : '';
      return (
        <>
          <CustomMenuList>
            {dateFilterTypes.map(t => {
              const isSelected = filterOption == t.label
              return (
                <CustomListItem
                  isSelected={isSelected}
                  rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
            {filterOption && filterOption === "custom" ? (
              <>
                <div
                  style={{
                    margin: "0px 6px",
                    border: "1px solid #DDDDDD",
                    borderRadius: "4px",
                    padding: 6,
                  }}>
                  <CustomDatePicker
                    date={selectedValue && selectedValue.length ? selectedValue[0] : ''}
                    label={"From:"}
                    PopperProps={{ disablePortal: true, size: null }}
                    icon={false}
                    dateFormat="MMM DD, YYYY"
                    timeInput={false}
                    onSelect={date => {
                      handleDateSave("from", date);
                    }}
                    disabled={false}
                    deleteIcon={false}
                    placeholder={"Select Date"}
                    containerProps={{ style: { alignItems: "center" } }}
                    btnProps={{
                      style: {
                        background: "transparent",
                        border: "none",
                        padding: 0,
                        textAlign: "left",
                      },
                    }}
                    labelProps={{
                      style: {
                        width: "auto",
                        marginTop: 0,
                        marginRight: 5,
                        fontSize: "14px !important",
                      },
                    }}
                    closeOnDateSelect={true}
                  />
                </div>
                <div
                  style={{
                    margin: "4px 6px",
                    border: "1px solid #DDDDDD",
                    borderRadius: "4px",
                    padding: 6,
                  }}>
                  <CustomDatePicker
                    date={selectedValue && selectedValue.length ? selectedValue[1] : ''}
                    label={"To:"}
                    PopperProps={{ disablePortal: true, size: null }}
                    icon={false}
                    dateFormat="MMM DD, YYYY"
                    timeInput={false}
                    onSelect={date => {
                      handleDateSave("to", date);
                    }}
                    disabled={false}
                    deleteIcon={false}
                    placeholder={"Select Date"}
                    containerProps={{ style: { alignItems: "center" } }}
                    btnProps={{
                      style: {
                        background: "transparent",
                        border: "none",
                        padding: 0,
                        textAlign: "left",
                      },
                    }}
                    labelProps={{
                      style: {
                        width: "auto",
                        marginTop: 0,
                        marginRight: 5,
                        fontSize: "14px !important",
                      },
                    }}
                    closeOnDateSelect={true}
                  />
                </div>
              </>
            ) : (
              <></>
            )}
          </CustomMenuList>
        </>
      );
    },
    dueDate: () => {
      const selectedType = projectFilter[column.colId] && projectFilter[column.colId].type
      const selectedValue = selectedType == 'custom' ? projectFilter[column.colId].selectedValues : '';
      return (
        <>
          <CustomMenuList>
            {dateFilterTypes.map(t => {
              const isSelected = filterOption == t.label
              return (
                <>
                  <CustomListItem isSelected={isSelected} rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                    <span>{t.value}</span>
                  </CustomListItem>
                </>
              );
            })}
            {filterOption && filterOption === "custom" ? (
              <>
                <div
                  style={{
                    margin: "0px 6px",
                    border: "1px solid #DDDDDD",
                    borderRadius: "4px",
                    padding: 6,
                  }}>
                  <CustomDatePicker
                    date={selectedValue && selectedValue.length ? selectedValue[0] : ''}
                    label={"From:"}
                    PopperProps={{ disablePortal: true, size: null }}
                    icon={false}
                    dateFormat="MMM DD, YYYY"
                    timeInput={false}
                    onSelect={date => {
                      handleDateSave("from", date);
                    }}
                    disabled={false}
                    deleteIcon={false}
                    placeholder={"Select Date"}
                    containerProps={{ style: { alignItems: "center" } }}
                    btnProps={{
                      style: {
                        background: "transparent",
                        border: "none",
                        padding: 0,
                        textAlign: "left",
                      },
                    }}
                    labelProps={{
                      style: {
                        width: "auto",
                        marginTop: 0,
                        marginRight: 5,
                        fontSize: "14px !important",
                      },
                    }}
                    closeOnDateSelect={true}
                  />
                </div>
                <div
                  style={{
                    margin: "4px 6px",
                    border: "1px solid #DDDDDD",
                    borderRadius: "4px",
                    padding: 6,
                  }}>
                  <CustomDatePicker
                    date={selectedValue && selectedValue.length ? selectedValue[1] : ''}
                    label={"To:"}
                    PopperProps={{ disablePortal: true, size: null }}
                    icon={false}
                    dateFormat="MMM DD, YYYY"
                    timeInput={false}
                    onSelect={date => {
                      handleDateSave("to", date);
                    }}
                    disabled={false}
                    deleteIcon={false}
                    placeholder={"Select Date"}
                    containerProps={{ style: { alignItems: "center" } }}
                    btnProps={{
                      style: {
                        background: "transparent",
                        border: "none",
                        padding: 0,
                        textAlign: "left",
                      },
                    }}
                    labelProps={{
                      style: {
                        width: "auto",
                        marginTop: 0,
                        marginRight: 5,
                        fontSize: "14px !important",
                      },
                    }}
                    closeOnDateSelect={true}
                  />
                </div>
              </>
            ) : (
              <></>
            )}
          </CustomMenuList>
        </>
      );
    },
    actualStartDate: () => {
      const selectedType = projectFilter[column.colId] && projectFilter[column.colId].type
      const selectedValue = selectedType == 'custom' ? projectFilter[column.colId].selectedValues : '';
      return (
        <>
          <CustomMenuList>
            {dateFilterTypes.map(t => {
              const isSelected = filterOption == t.label
              return (
                <CustomListItem
                  rootProps={{ onClick: () => onSelectDateItems(t.label) }}
                  isSelected={isSelected}
                >
                  <span>{t.value}</span>
                </CustomListItem>
              )
            })}
          </CustomMenuList>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ''}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[1] : ''}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      )
    },
    actualDueDate: () => {
      const selectedType = projectFilter[column.colId] && projectFilter[column.colId].type
      const selectedValue = selectedType == 'custom' ? projectFilter[column.colId].selectedValues : '';
      return (
        <>
          <>
            {dateFilterTypes.map(t => {
              const isSelected = filterOption == t.label
              return (
                <CustomListItem isSelected={isSelected} rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
          </>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ''}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue[1] ? selectedValue[1] : ''}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      )
    },
    createdDate: () => {
      const selectedType = projectFilter[column.colId] && projectFilter[column.colId].type
      const selectedValue = selectedType == 'custom' ? projectFilter[column.colId].selectedValues : '';
      return (
        <>
          <>
            {createdDateFilterTypes.map(t => {
              const isSelected = filterOption == t.label
              return (
                <CustomListItem isSelected={isSelected} rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
          </>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ''}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue[1] ? selectedValue[1] : ''}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      )
    },
    updatedDate: () => {
      const selectedType = projectFilter[column.colId] && projectFilter[column.colId].type
      const selectedValue = selectedType == 'custom' ? projectFilter[column.colId].selectedValues : '';
      return (
        <>
          <>
            {createdDateFilterTypes.map(t => {
              const isSelected = filterOption == t.label
              return (
                <CustomListItem isSelected={isSelected} rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
          </>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ''}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue[1] ? selectedValue[1] : ''}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      )
    },
    priority: () => {
      const priorityOptions = priorityData(theme, classes);
      const selectedValues = (projectFilter[column.colId] && projectFilter[column.colId].selectedValues) || [];
      const selectedOptions = priorityOptions.filter(p => selectedValues.includes(p.value));
      return (
        <CustomMultiSelectDropdown
          label=""
          option={selectedOptions}
          options={() => priorityOptions}
          optionIcon={true}
          placeholder="Priority"
          onSelect={onPriorityFilterSelect}
          height="140px"
          valueSelector='value'
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    project: () => {
      const projectsOptions = generateProjectData(projects);
      const selectedValues =
        (projectFilter[column.colId] && projectFilter[column.colId].selectedValues) || [];
      const selectedOptions = projectsOptions.filter(p => selectedValues.includes(p.id));
      return (
        <CustomMultiSelectDropdown
          label=""
          option={selectedOptions}
          options={() => projectsOptions}
          optionIcon={false}
          valueSelector='id'
          placeholder="Project"
          onSelect={onProjectFilterSelect}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    progress: () => {
      const selectedValue = (projectFilter[column.colId] && projectFilter[column.colId].selectedValues) || [40, 80];
      return (
        <>
          <div className={classes.progressSlider}>
            <Range
              allowCross={false}
              defaultValue={selectedValue}
              tipFormatter={value => `${value}%`}
              min={0}
              max={100}
              onChange={onProgressSliderFilterSelect}
            />
          </div>
        </>
      )
    },
    projectManager: () => {
      const selectedValues =
        (projectFilter[column.colId] && projectFilter[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.obj.userId));
      return (
        <>
          <CustomMultiSelectDropdown
            label=""
            option={selectedOptions}
            options={() => userData}
            optionIcon={true}
            onSelect={onAssigneeFilterSelect}
            placeholder="Assignee"
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      );
    },
    resources: () => {
      const selectedValues =
        (projectFilter[column.colId] && projectFilter[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.obj.userId));
      return (
        <>
          <CustomMultiSelectDropdown
            label=""
            option={selectedOptions}
            options={() => userData}
            optionIcon={true}
            onSelect={onAssigneeFilterSelect}
            placeholder="Assignee"
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      );
    },
    createdBy: () => {
      const selectedValues =
        (projectFilter[column.colId] && projectFilter[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.obj.userId));
      return (
        <>
          <CustomMultiSelectDropdown
            label=""
            options={() => userData}
            option={selectedOptions}
            optionIcon={true}
            placeholder="Created By"
            onSelect={onCreatedByFilterSelect}
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      )
    },
    updatedBy: () => {
      const selectedValues =
        (projectFilter[column.colId] && projectFilter[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.obj.userId));
      return (
        <>
          <CustomMultiSelectDropdown
            label=""
            options={() => userData}
            option={selectedOptions}
            optionIcon={true}
            placeholder="Updated By"
            onSelect={onCreatedByFilterSelect}
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      )
    },
    totalEffort: (
      <div className={classes.totalEffort}>
        <label>80 hrs</label>
        <label>15 mins</label>
        <div className={classes.textLogged}>
          <label>Sum</label>
        </div>
      </div>
    ),
    attachments: (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Sum</label>
      </div>
    ),
    totalComment: (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Avg.</label>
      </div>
    ),
    meetings: (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>min.</label>
      </div>
    ),
    issues: (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Avg.</label>
      </div>
    ),
    risks: (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Avg.</label>
      </div>
    ),
  };
  const SelectedCmp = () => (Components[column.colId] ? Components[column.colId]() : null);
  return <>{SelectedCmp()}</>;
}
const mapStateToProps = state => {
  return {
    members: state.profile.data.member.allMembers,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    projects: state.projects.data,
  };
};
export default compose(
  connect(mapStateToProps),
  withStyles(filterComponentsStyle, { withTheme: true })
)(ProjectFilterComponents);
