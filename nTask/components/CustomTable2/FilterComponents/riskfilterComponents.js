import React, { useEffect, useState } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import withStyles from "@material-ui/core/styles/withStyles";
import { useSelector } from "react-redux";
import { injectIntl } from "react-intl";
import filterComponentsStyle from "./filterComponents.style";
import CustomListItem from "../../ListItem/CustomListItem";
import CustomDatePicker from "../../DatePicker/DatePicker/DatePicker";
import CustomMultiSelectDropdown from "../../Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import { generateProjectData, generateAssigneeData } from "../../../helper/generateSelectData";
import {
  impactData,
  likelihoodData,
  statusData,
  generateTaskDropdownData,
} from "../../../helper/riskDropdownData";
import moment from "moment";

const createdDateFilterTypes = [
  {
    label: "today",
    value: "Today",
  },
  {
    label: "yesterday",
    value: "Yesterday",
  },
  {
    label: "currentWeek",
    value: "This Week",
  },
  {
    label: "currentMonth",
    value: "This Month",
  },
  {
    label: "custom",
    value: "Custom",
  },
];

function RiskFilterComponents({
  data,
  classes,
  theme,
  members,
  handleFilterChange,
  projects,
  intl,
}) {
  const { column } = data;
  const { riskFilter } = useSelector(state => ({
    riskFilter: state.risks.riskFilter,
  }));
  const [filterOption, setFilterOption] = useState(null);
  const [fromDate, setFromDate] = useState(null);
  const [toDate, setTodate] = useState(null);

  useEffect(() => {
    const selectedValue = riskFilter[column.colId] && riskFilter[column.colId].selectedValues;
    const type = riskFilter[column.colId] && riskFilter[column.colId].type;
    if (type == "custom" && (selectedValue[0] || selectedValue[1])) {
      setFilterOption("custom");
      return;
    }
    setFilterOption(type);
  }, [riskFilter]);

  const handleDateSave = (type, date) => {
    if (type === "from") {
      /** custom filter start date */
      setFromDate(moment(date).format("l"));
      handleFilterChange(
        [date ? moment(date).format("l") : "", toDate ? toDate : ""],
        "custom"
      ); /** callback calls  */
    }
    if (type === "to") {
      /** custom filter to date */
      setTodate(moment(date).format("l"));
      handleFilterChange(
        [fromDate ? fromDate : "", date ? moment(date).format("l") : ""],
        "custom"
      ); /** callback calls  */
    }
  };
  const onSelectDateItems = option => {
    setFilterOption(option);
    if (option !== "custom") handleFilterChange([], option);
  };
  // on select from dropdown
  // status
  const onStatusFilterSelect = options => {
    handleFilterChange(options.map(p => p.value));
  };
  const onImpactFilterSelect = options => {
    handleFilterChange(options.map(p => p.value));
  };
  const onLikelihoodFilterSelect = options => {
    handleFilterChange(options.map(p => p.value));
  };
  // project
  const onTaskFilterSelect = options => {
    handleFilterChange(options.map(p => p.obj.taskId));
  };
  const onProjectFilterSelect = options => {
    handleFilterChange(options.map(p => p.id));
  };
  const riskOwnerFilterSelect = options => {
    handleFilterChange(options.map(p => p.id));
  };
  const onCreatedByFilterSelect = options => {
    handleFilterChange(options.map(p => p.id));
  };

  // generate dropdown data here
  const statusDropDownData = statusData(theme, classes, intl);
  const impactDData = impactData(theme, classes, intl);
  const userData = generateAssigneeData(members);
  const projectsOptions = generateProjectData(projects);
  const tasksData = generateTaskDropdownData();

  const Components = {
    status: () => {
      const selectedValues =
        (riskFilter[column.colId] && riskFilter[column.colId].selectedValues) || [];
      const selectedOptions = statusDropDownData.filter(s => selectedValues.includes(s.value));
      return (
        <CustomMultiSelectDropdown
          label="status"
          option={selectedOptions}
          options={() => statusDropDownData}
          optionIcon={true}
          onSelect={onStatusFilterSelect}
          placeholder="Status"
          valueSelector={"value"}
          // heading={`Customize Column (${selectedColumns.length}/10)`}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    impact: () => {
      const selectedValues =
        (riskFilter[column.colId] && riskFilter[column.colId].selectedValues) || [];
      const selectedOptions = impactDData.filter(s => selectedValues.includes(s.value));
      return (
        <CustomMultiSelectDropdown
          label="impact"
          option={selectedOptions}
          options={() => impactDData}
          optionIcon={true}
          onSelect={onImpactFilterSelect}
          placeholder="Impact"
          valueSelector={"value"}
          // heading={`Customize Column (${selectedColumns.length}/10)`}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    likelihood: () => {
      const selectedValues =
        (riskFilter[column.colId] && riskFilter[column.colId].selectedValues) || [];
      const selectedOptions = likelihoodData(intl).filter(s => selectedValues.includes(s.value));
      return (
        <CustomMultiSelectDropdown
          label="likelihood"
          option={selectedOptions}
          options={() => likelihoodData(intl)}
          optionIcon={true}
          onSelect={onLikelihoodFilterSelect}
          placeholder="Likelihood"
          valueSelector={"value"}
          // heading={`Customize Column (${selectedColumns.length}/10)`}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    createdDate: () => {
      const selectedType = riskFilter[column.colId] && riskFilter[column.colId].type;
      const selectedValue = selectedType == "custom" ? riskFilter[column.colId].selectedValues : "";
      return (
        <>
          <>
            {createdDateFilterTypes.map(t => {
              const isSelected = filterOption == t.label;
              return (
                <CustomListItem
                  isSelected={isSelected}
                  rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
          </>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ""}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue[1] ? selectedValue[1] : ""}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      );
    },
    updatedDate: () => {
      const selectedType = riskFilter[column.colId] && riskFilter[column.colId].type
      const selectedValue = selectedType == 'custom' ? riskFilter[column.colId].selectedValues : '';
      return (
        <>
          <>
            {createdDateFilterTypes.map(t => {
              const isSelected = filterOption == t.label
              return (
                <CustomListItem isSelected={isSelected} rootProps={{ onClick: () => onSelectDateItems(t.label) }}>
                  <span>{t.value}</span>
                </CustomListItem>
              );
            })}
          </>
          {filterOption && filterOption === "custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue.length ? selectedValue[0] : ''}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("from", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  date={selectedValue && selectedValue[1] ? selectedValue[1] : ''}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  timeInput={false}
                  onSelect={date => {
                    handleDateSave("to", date);
                  }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px !important",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </>
      )
    },
    project: () => {
      const selectedValues =
        (riskFilter[column.colId] && riskFilter[column.colId].selectedValues) || [];
      const selectedOptions = projectsOptions.filter(p => selectedValues.includes(p.id));
      return (
        <CustomMultiSelectDropdown
          label=""
          option={selectedOptions}
          options={() => projectsOptions}
          optionIcon={false}
          valueSelector="id"
          placeholder="Project"
          onSelect={onProjectFilterSelect}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    tasks: () => {
      const selectedValues =
        (riskFilter[column.colId] && riskFilter[column.colId].selectedValues) || [];
      const selectedOptions = tasksData.filter(p => selectedValues.includes(p.obj.taskId));
      return (
        <CustomMultiSelectDropdown
          label=""
          option={selectedOptions}
          options={() => tasksData}
          optionIcon={false}
          valueSelector="id"
          placeholder="Tasks"
          onSelect={onTaskFilterSelect}
          height="140px"
          width="140px"
          scrollHeight={180}
          inline={true}
        />
      );
    },
    riskOwner: () => {
      const selectedValues =
        (riskFilter[column.colId] && riskFilter[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.id));

      return (
        <>
          <CustomMultiSelectDropdown
            label="riskOwner"
            option={selectedOptions}
            options={() => userData}
            optionIcon={true}
            onSelect={riskOwnerFilterSelect}
            placeholder="Risk Owner"
            valueSelector={"id"}
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      );
    },
    createdBy: () => {
      const selectedValues =
        (riskFilter[column.colId] && riskFilter[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.obj.userId));
      return (
        <>
          <CustomMultiSelectDropdown
            label=""
            options={() => userData}
            option={selectedOptions}
            optionIcon={true}
            placeholder="Created By"
            onSelect={onCreatedByFilterSelect}
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      );
    },
    updatedBy: () => {
      const selectedValues =
        (riskFilter[column.colId] && riskFilter[column.colId].selectedValues) || "";
      const selectedOptions = userData.filter(u => selectedValues.includes(u.obj.userId));
      return (
        <>
          <CustomMultiSelectDropdown
            label=""
            options={() => userData}
            option={selectedOptions}
            optionIcon={true}
            placeholder="Updated By"
            onSelect={onCreatedByFilterSelect}
            // heading={`Customize Column (${selectedColumns.length}/10)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            inline={true}
          />
        </>
      )
    },
  };
  const SelectedCmp = () => (Components[column.colId] ? Components[column.colId]() : null);
  return <>{SelectedCmp()}</>;
}
const mapStateToProps = state => {
  return {
    members: state.profile.data.member.allMembers,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    projects: state.projects.data,
  };
};
export default compose(
  injectIntl,
  connect(mapStateToProps),
  withStyles(filterComponentsStyle, { withTheme: true })
)(RiskFilterComponents);
