const filterComponentsStyles = theme => ({
  progressSlider: {
    padding: "15px 8px",
  },
  priorityIcon: {
    paddingRight: 3,
  },
  horizontalSlider: {
    width: "100%",
    maxWidth: "500px",
    height: "50px",
    border: "1px solid grey",
  },
  exampleThumb: {
    fontSize: "0.9em !important",
    textAlign: "center",
    backgroundColor: "black",
    color: "white",
    cursor: "pointer",
    border: "5px solid gray",
    boxSizing: "border-box",
  },
  exampleTrack: {
    top: "20px",
    height: 10,
    background: "rgb(221, 221, 221)",
  },
  exampleTrack1: {
    background: "rgb(255, 0, 0)",
  },
  exampleTrack: {
    background: " rgb(0, 255, 0)",
  },
  statusIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  impactIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  statusCnt: {
    display: "flex",
    flexDirection: "column",
  },
  title: {
    fontSize: "13px !important",
    fontFamily: "'lato', sans-serif",
    fontWeight: 600,
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  titleLabel: {
    fontSize: "13px !important",
    fontFamily: "'lato', sans-serif",
    fontWeight: 600,
    display: "flex",
    alignItems: "center",
    marginLeft: 15,
    cursor: "pointer",
  },
  show: {
    display: "block",
  },
  hide: {
    display: "none",
  },
});

export default filterComponentsStyles;
