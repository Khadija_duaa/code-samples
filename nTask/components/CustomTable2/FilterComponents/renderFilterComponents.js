import React from "react";
import FilterComponents from "./filterComponents";
import IssuefilterComponents from "./issuefilterComponents";
import ProjectFilterComponents from "./projectComponents";
import MeetingfilterComponents from "./meetingfilterComponents";
import RiskfilterComponents from "./riskfilterComponents";

function RenderFilterComponent(props) {
    const { type, data, handleFilterChange } = props;
    const Components = {
        task: (<FilterComponents data={data} handleFilterChange={handleFilterChange} />),
        project: (<ProjectFilterComponents data={data} handleFilterChange={handleFilterChange} />),
        issue: (<IssuefilterComponents data={data} handleFilterChange={handleFilterChange} />),
        meeting: (<MeetingfilterComponents data={data} handleFilterChange={handleFilterChange} />),
        risk: (<RiskfilterComponents data={data} handleFilterChange={handleFilterChange} />),
    }
    const ExportCmp = () => Components[type];
    return <>{ExportCmp()}</>;
}
export default RenderFilterComponent;
