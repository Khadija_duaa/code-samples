import DefaultTextField from "../../Form/TextField";
import placeholder from "lodash/fp/placeholder";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import React, { useState } from "react";
import addNewInputStyles from "./addNewInput.style";
import withStyles from "@material-ui/core/styles/withStyles";
import KeyboardReturnIcon from "@material-ui/icons/KeyboardReturn";
import { v4 as uuidv4 } from "uuid";
import { useSelector } from "react-redux";

function AddNewInput(props) {
  const [errorState, setErrorState] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [value, setValue] = useState("");
  const [edit, setEdit] = useState(false);
  const { placeholder, type, id, classes, btnText, theme, addAction, gridApi, disabled, addHelpTask, emptyErrorMessage } = props;
  const { projects } = useSelector(state => {
    return {
      projects: state.projects.data,
    }
  })
  const handleInput = e => {
    setValue(e.target.value);
    if (e.target.value.length > 250) {
      setErrorState(true);
      setErrorMessage("Please enter less than 250 characters");
    } else {
      setErrorState(false);
      setErrorMessage("");
    }
  };
  const handleEdit = () => {
    setEdit(true);
  };
  //disable input on clickaway
  const handleClickAway = () => {
    setEdit(false);
  };
  //When user press enter add a task and if user press escape disable add task input
  const handleKeyDown = e => {
    if (e.key === "Escape") {
      setEdit(false);
      setValue("");
      setErrorState(false);
      setErrorMessage("");
      return;
    }
    if (e.keyCode == 13 && (value === "" || !value.trim().length)) {
      setErrorState(true);
      setErrorMessage(emptyErrorMessage);
      return;
    }
    if (e.keyCode == 13 && type == 'project') {
      const newProject = projects.some(function (element) {
        return element.projectName == value;
      })
      if (newProject) {
        setErrorState(true);
        setErrorMessage('Project Name already exists please try with other name');
        return;
      }
    }
    if (e.keyCode == 13 && !errorState) {
      const data = { value: value, clientId: uuidv4() };
      setValue("");
      setErrorState(false);
      setErrorMessage("");
      addAction(
        data,
        //Add item before the item is added to database
        (data, type) => { }
      );
    }
  };
  return (
    <>
      {edit ? (
        <div className={classes.addInputCnt}>
          <DefaultTextField
            noBorderRadius={true}
            transparentInput={true}
            error={false}
            formControlStyles={{ marginBottom: 0 }}
            defaultProps={{
              type: "text",
              id: id,
              placeholder: placeholder,
              value: value,
              autoFocus: true,
              inputProps: { style: { padding: "12px 225px 12px 21px" } },
              onChange: e => handleInput(e),
              onKeyDown: e => handleKeyDown(e),
              onBlur: handleClickAway,
              style: {
                background: errorState
                  ? theme.palette.background.lightRed
                  : theme.palette.background.azureLight,
              },
            }}
          />
          <label className={classes.addNewTaskLabel}>
            {errorState ? (
              <span style={{ color: theme.palette.text.danger }}> {errorMessage} </span>
            ) : (
              <>
                <KeyboardReturnIcon className={classes.returnIcon} />
                {addHelpTask}
              </>
            )}
          </label>
        </div>
      ) : (
        <div onClick={disabled ? null : handleEdit} className={classes.addBtnCnt} style={disabled ? { opacity: 0.4, pointerEvents: 'none' } : null}>
          <AddCircleIcon className={props.classes.addTaskIcon} />
          <span className={props.classes.addText}>{btnText}</span>
        </div>
      )}
    </>
  );
}

export default withStyles(addNewInputStyles, { withTheme: true })(AddNewInput);
