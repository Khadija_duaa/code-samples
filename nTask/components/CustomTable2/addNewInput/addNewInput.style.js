const addNewInputStyles = (theme) => ({
  addTaskIcon: {
    fontSize: "20px !important",
    color: theme.palette.icon.azure,
    marginRight: 10
  },
  addText: {
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "14px !important",
    color: theme.palette.text.primary,
    fontWeight: 500
  },
  addBtnCnt: {
    display: 'flex',
    alignItems: 'center',
    flex: 1,
    height: 40,
    padding: '0 5px',
    cursor: 'pointer',
    '&:hover': {
      background: theme.palette.background.azureLight
    },
    '&:hover $addText': {
      color: theme.palette.text.azure,
    }
  },
  addInputCnt: {
    position: 'relative',
    width: '100%'
  },
  addNewTaskLabel: {
    display: 'flex',
    alignItems: 'center',
    position: 'absolute',
    right: 15,
    top: '50%',
    transform: 'translateY(-50%)',
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.text.grayDarker,
    fontSize: "12px !important",
    fontWeight: 500
  },
  returnIcon: {
    color: theme.palette.icon.gray400,
    fontSize: "20px !important",
    marginRight: 6
  },
  addProjectBtn: {
    marginTop: 0,
    background: "transparent",
    border: `1px dashed ${theme.palette.icons.darkGray}`,
    color: theme.palette.icons.darkGray,
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    width: 30,
    height: 30,
    '&:hover': {
      borderColor: theme.palette.customFieldIcons.blue,
      background: "transparent",
      '& $addProjectIcon': {
        color: theme.palette.customFieldIcons.blue,
      }
    }
  },
  addProjectIcon: {
    fontSize: "16px !important",
  },
  selectedProjectBtn: {
    width: 'auto',
    color: theme.palette.background.darkBlack,
    borderRadius: 4,
    border: `1px solid ${theme.palette.customFieldIcons.blue}`,
    background: `${theme.palette.customFieldIcons.blue}2b`,
    '& $addProjectIcon': {
      color: theme.palette.customFieldIcons.blue,
    }
  },
  projectName: {
    maxWidth: 100,
    display: 'inline-block',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    margin: '0 5px',
    transform: 'translateY(3px)',
  },
  addProjectBtnError: {
    borderColor: theme.palette.background.redSoft,
    background: `${theme.palette.background.redSoft}2b`,
    '&:hover': {
      borderColor: theme.palette.background.redSoft,
      background: `${theme.palette.background.redSoft}2b`,
      '& $addProjectIcon': {
        color: theme.palette.background.redSoft,
      }
    },
    '& $addProjectIcon': {
      color: theme.palette.background.redSoft,
    }
  },
  clearProject: {
    background: 'transparent !important',
    padding: 0,
    width: 12,
    '&:hover': {
      color: theme.palette.customFieldIcons.blue,
    }
  }
})

export default addNewInputStyles;