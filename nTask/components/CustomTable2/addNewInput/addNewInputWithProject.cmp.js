import DefaultTextField from "../../Form/TextField";
import { useSelector } from "react-redux";
import placeholder from "lodash/fp/placeholder";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import React, { useEffect, useState } from "react";
import addNewInputStyles from "./addNewInput.style";
import withStyles from "@material-ui/core/styles/withStyles";
import KeyboardReturnIcon from "@material-ui/icons/KeyboardReturn";
import { v4 as uuidv4 } from "uuid";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import SvgIcon from "@material-ui/core/SvgIcon";
import IconProjects from "../../Icons/IconProjects";
import Tooltip from "../../Tooltip/Tooltip";
import SearchDropdown from "../../../components/Menu/SearchMenu";
import CustomButton from "../../../components/Buttons/CustomButton";
import { ClickAwayListener } from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import clsx from "clsx";
import CustomTooltip from "../../Tooltip/Tooltip";

function AddNewInputWithProject(props) {
  const [errorState, setErrorState] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [projectErrorState, setProjectErrorState] = useState(false);
  const [projectErrorMessage, setProjectErrorMessage] = useState("");
  const [value, setValue] = useState("");
  const [edit, setEdit] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [projectData, setProjectData] = useState([]);
  const [selectedProject, setSelectedProject] = useState(null);
  const { projects } = useSelector(state => {
    return {
      projects: state.projects.data,
    };
  });
  const { placeholder, type, id, classes, btnText, theme, addAction, gridApi, disabled, addHelpTask } = props;
  const handleInput = e => {
    setValue(e.target.value);
    if (e.target.value.length > 250) {
      setErrorState(true);
      setErrorMessage("Please enter less than 250 characters");
    } else {
      setErrorState(false);
      setErrorMessage("");
      setProjectErrorState(false)
      setProjectErrorMessage("");
    }
  };
  const handleEdit = () => {
    setEdit(true);
  };
  //disable input on clickaway
  const handleClickAway = () => {
    if (!anchorEl) {
      setEdit(false);
    }
  };
  //When user press enter add a task and if user press escape disable add task input
  const handleKeyDown = e => {
    if (e.key === "Escape") {
      setEdit(false);
      setValue("");
      setErrorState(false);
      setErrorMessage("");
      setProjectErrorState(false)
      setProjectErrorMessage("");
      handleClose();
      setSelectedProject(null);
      return;
    }
    if (e.keyCode == 13 && value === "") {
      setErrorState(true);
      setErrorMessage("Please Enter task title");
      return;
    }
    if (e.keyCode == 13 && type == 'project') {
      const newProject = projects.some(function (element) {
        return element.projectName == value;
      })
      if (newProject) {
        setErrorState(true);
        setErrorMessage('Project Name already exists please try with other name');
        return;
      }
    }
    if (e.keyCode == 13 && !selectedProject) {
      // project error here
      // setErrorState(true);
      // setErrorMessage("Project selection is required");
      setProjectErrorState(true)
      setProjectErrorMessage("Project selection is required");
      return;
    }
    if (e.keyCode == 13 && !errorState) {
      const data = { value: value, clientId: uuidv4(), projectId: selectedProject.id };
      setValue("");
      setErrorState(false);
      setErrorMessage("");
      setProjectErrorState(false)
      setProjectErrorMessage("");
      setSelectedProject(null);
      addAction(
        data,
        //Add item before the item is added to database
        (data, type) => { }
      );
    }
  };
  const clearError = (e) => {
    e.stopPropagation();
    setErrorState(false);
    setErrorMessage("");
    setProjectErrorState(false)
    setProjectErrorMessage("");
  }

  useEffect(() => {
    let projectArr = projects.length
      ? projects.map((x) => {
        return { id: x.projectId, value: { name: x.projectName } };
      })
      : [];
    setProjectData(projectArr);

  }, []);

  const handleClick = event => {
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  }
  const handleClose = () => {
    setAnchorEl(null);
  }
  const handleSelectProject = (event, pName, pId) => {
    setSelectedProject({
      name: pName,
      id: pId,
    })
    handleClose();
  }
  const handleClearProject = (e) => {
    e.stopPropagation();
    setSelectedProject(null);
    handleClose();
  }
  const open = Boolean(anchorEl);
  return (
    <>
      {edit ? (
        <ClickAwayListener
          onClickAway={handleClickAway}
        >
          <div className={classes.addInputCnt}>
            <DefaultTextField
              noBorderRadius={true}
              transparentInput={true}
              error={false}
              formControlStyles={{ marginBottom: 0 }}
              defaultProps={{
                type: "text",
                id: id,
                placeholder: placeholder,
                value: value,
                autoFocus: true,
                inputProps: { style: { padding: "12px 225px 12px 21px" } },
                onChange: e => handleInput(e),
                onKeyDown: e => handleKeyDown(e),
                style: {
                  background: errorState
                    ? theme.palette.background.lightRed
                    : theme.palette.background.azureLight,
                },
              }}
            />
            <label className={classes.addNewTaskLabel}>
              {errorState ? (
                <span style={{ color: theme.palette.text.danger, cursor: 'pointer' }} onClick={clearError}> {errorMessage} </span>
              ) : (
                <>

                  <CustomTooltip helptext={projectErrorMessage ? projectErrorMessage : 'Select Project'} placement="top">
                    <CustomIconButton
                      className={clsx({ [classes.addProjectBtn]: true, [classes.selectedProjectBtn]: selectedProject, [classes.addProjectBtnError]: projectErrorState })}
                      ref={anchorEl}
                      onClick={e => handleClick(e)}
                    >
                      <SvgIcon
                        viewBox="0 0 16 13.516"
                        style={{
                          fontSize: "16px"
                        }}
                        className={classes.addProjectIcon}
                      >
                        <IconProjects />
                      </SvgIcon>
                      {selectedProject ?
                        <span className={classes.selectProject}>
                          <span className={classes.projectName}>
                            {selectedProject.name}
                          </span>
                          <CustomIconButton
                            className={classes.clearProject}
                            ref={anchorEl}
                            onClick={e => handleClearProject(e)}
                          >
                            <ClearIcon style={{ fontSize: "15px" }} />
                          </CustomIconButton>
                        </span>
                        : ''}
                    </CustomIconButton>
                  </CustomTooltip>
                  <span
                    style={{
                      margin: '0 15px',
                      borderLeft: `1px solid ${theme.palette.icons.darkGray}`,
                      height: 30
                    }}
                  ></span>
                  <CustomButton
                    btnType="success"
                    variant="contained"
                    style={{
                      height: 30,
                      padding: 15,
                      fontWeight: 400,
                      fontFamily: theme.typography.fontFamilyLato,
                      fontSize: "13px !important",
                    }}
                    onClick={() => handleKeyDown({ keyCode: 13 })}
                    disabled={false}>
                    Create
                  </CustomButton>
                </>
              )}
            </label>
            {open && <SearchDropdown
              placement={"bottom-end"}
              open={open}
              title={"Projects"}
              closeAction={handleClose}
              selectAction={handleSelectProject}
              searchQuery={["id", "value.name"]}
              data={projectData}
              isProjectSelect={true}
              anchorRef={anchorEl}
              searchInputPlaceholder={'Search projects'}
              pooperProps={{ disablePortal: false }}
              scrollHeight={300}
            />}
          </div>
        </ClickAwayListener>
      ) : (
        <div onClick={disabled ? null : handleEdit} className={classes.addBtnCnt} style={disabled ? { opacity: 0.4, pointerEvents: 'none' } : null}>
          <AddCircleIcon className={props.classes.addTaskIcon} />
          <span className={props.classes.addText}>{btnText}</span>
        </div>
      )
      }


    </>
  );
}

export default withStyles(addNewInputStyles, { withTheme: true })(AddNewInputWithProject);
