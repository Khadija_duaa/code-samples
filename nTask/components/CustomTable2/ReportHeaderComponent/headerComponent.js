import ColumnSelectionDropdown from "../../Dropdown/SelectedItemsDropDown";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import CustomIconButton from "../../Buttons/CustomIconButton";
import MenuIcon from "@material-ui/icons/Menu";
import React, { useEffect, useRef, useState } from "react";
import ColumnSettingDropdown from "../ReportColumnSettingDropdown/ColumnSettingDropdown";
import withStyles from "@material-ui/core/styles/withStyles";
import headerComponentsStyles from "./headerComponent.style";
import searchQuery from "../ReportColumnSettingDropdown/searchQuery";
import AdvFilterIcon from "../../Icons/AdvFilterIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import { useSelector } from "react-redux";

function HeaderCmp({
  column,
  allColumn,
  gridApi,
  theme,
  classes,
  feature,
  groupType,
  headerProps,
}) {
  const {
    columnSettingDisabled,
    columnSortingDisabled,
    columnGroupingDisabled,
    columnFilterDisabled,
    columnPinDisabled,
    columnWordWrapDisabled,
    customStatusList,
    columnChangeCallback,
    taskStatusGroup,
  } = headerProps;
  const [ascSort, setAscSort] = useState("inactive");
  const [descSort, setDescSort] = useState("inactive");
  const [noSort, setNoSort] = useState("inactive");
  const { reportingFilter } = useSelector(state => {
    return {
      reportingFilter: state.reportingFilter?.[feature],
    };
  });
  const refButton = useRef(null);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const anchorElRef = useRef(null);
  const onSortChanged = () => {
    setAscSort(column.column.isSortAscending() ? "active" : "inactive");
    setDescSort(column.column.isSortDescending() ? "active" : "inactive");
    setNoSort(
      !column.column.isSortAscending() && !column.column.isSortDescending() ? "active" : "inactive"
    );
  };
  //Function that sorts the data in asc, desc and remove sorting
  const onSortRequested = (order, event) => {
    if (ascSort == "active") {
      column.setSort("desc", true);
    } else if (descSort == "active") {
      column.setSort("", true);
    } else {
      column.setSort("asc", true);
    }
  };
  //Handle Dialog Open
  const handleClick = (event, column) => {
    event.stopPropagation();
    setAnchorEl({ column });
    anchorElRef.current = event.currentTarget;
  };
  const isDescendant = (parent, child) => {
    let node = child.parentNode;
    while (node != null) {
      if (node.id == parent.id) {
        return true;
      }
      node = node.parentNode;
    }
    return false;
  };
  useEffect(() => {
    column.column.addEventListener("sortChanged", onSortChanged);
    onSortChanged();
    return () => {
      column.column.removeEventListener("sortChanged", onSortChanged);
    };
  }, []);
  //callback when columns change
  const onColumnHide = (colId, value) => {
    gridApi.columnModel.applyColumnState({
      state: [
        {
          colId: colId,
          hide: value,
        },
      ],
    });
  };
  const handleClose = e => {
    if (!e) {
      setAnchorEl(null);
      anchorElRef.current = null;
      return;
    }
    let parent = document.querySelector("#columnSettingMenu");
    let children = e.target;
    const checkIsDescendant = isDescendant(parent, children);
    if (checkIsDescendant) {
      return;
    } else {
      setAnchorEl(null);
      anchorElRef.current = null;
    }
  };
  const { sortIndex, colId } = column.column;
  const isFilterApplied =
    reportingFilter &&
    reportingFilter?.[colId] &&
    (reportingFilter?.[colId].type || reportingFilter?.[colId].selectedValues.length);
  return (
    <>
      {/*{colId == "columnDropdown" && <ColumnSelectionDropdown*/}
      {/*  feature={"task"}*/}
      {/*  onColumnHide={onColumnHide}*/}
      {/*  hideColumns={["matrix"]}*/}
      {/*/>}*/}
      {columnSettingDisabled.includes(colId) ? null : (
        <div className={classes.headerInnerCnt}>
          <div
            className={classes.tableHeader}
            onClick={!columnSortingDisabled.includes(colId) ? e => onSortRequested(null, e) : null}>
            <span
              className={
                column.column.colDef.align == "left" ? classes.headerNameLeft : classes.headerName
              }>
              {column.displayName}

              {sortIndex !== null && sortIndex > -1 && (
                <span className={classes.sortCnt}>
                  {sortIndex !== null && sortIndex > -1 && (
                    <span className={classes.sortIndexText}>{sortIndex + 1}</span>
                  )}
                  <ArrowUpwardIcon className={`${classes.descSortIcon} ${classes[ascSort]}`} />
                  <ArrowDownwardIcon className={`${classes.ascSortIcon} ${classes[descSort]}`} />
                </span>
              )}
              {isFilterApplied ? (
                <span className={classes.filterIconCnt}>
                  <SvgIcon viewBox="0 0 24 24" htmlColor={theme.palette.icon.brightBlue}>
                    <AdvFilterIcon />
                  </SvgIcon>{" "}
                </span>
              ) : null}
            </span>
          </div>
          <div
            className={classes.columnSettingBtnCnt}
            style={{ visibility: anchorEl && anchorEl.anchor ? "visible" : "" }}>
            {column.column.colDef.showRowGroup ? null : (
              <span className="columnSettingBtn">
                <CustomIconButton
                  buttonRef={node => (anchorElRef.current = node)}
                  btnType="transparent"
                  style={{ padding: 0 }}
                  id={column.displayName}
                  onClick={e => handleClick(e, column)}>
                  <MenuIcon className={classes.menuIcon} />
                </CustomIconButton>
              </span>
            )}
            {anchorEl ? (
              <ColumnSettingDropdown
                gridApi={gridApi}
                anchorEl={anchorEl}
                anchorRef={anchorElRef.current}
                closeAction={handleClose}
                columnGroupingDisabled={columnGroupingDisabled}
                columnFilterDisabled={columnFilterDisabled}
                columnPinDisabled={columnPinDisabled}
                columnWordWrapDisabled={columnWordWrapDisabled}
                customStatusList={customStatusList}
                taskStatusGroup={taskStatusGroup}
                feature={feature}
                groupType={groupType}
                columns={allColumn}
                columnChangeCallback={columnChangeCallback}
              />
            ) : null}
          </div>
        </div>
      )}
    </>
  );
}

HeaderCmp.defaultProps = {
  columnSettingDisabled: [],
  customStatusList: [],
};
export default withStyles(headerComponentsStyles, { withTheme: true })(HeaderCmp);
