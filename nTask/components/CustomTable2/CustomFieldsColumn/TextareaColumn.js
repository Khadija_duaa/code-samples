import React from "react";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import ReactHtmlParser from 'react-html-parser';

function TextareaColumn({ classes, theme, field }) {
  return <><span>{ReactHtmlParser(field.fieldData.data) || "-"}</span></>;
}
TextareaColumn.defaultProps = {};
export default withStyles(styles, { withTheme: true })(TextareaColumn);

