import React from "react";
import TextfieldColumn from "./TextfieldColumn";
import TextareaColumn from "./TextareaColumn";
import LocationColumn from "./LocationColumn";
import CountryColumn from "./CountryColumn";
import NumberColumn from "./NumberColumn";
import MoneyColumn from "./MoneyColumn";
import EmailColumn from "./EmailColumn";
import UrlColumn from "./UrlColumn";
import DateColumn from "./DateColumn";
import PhoneColumn from "./PhoneColumn";
import RatingColumn from "./RatingColumn";
import FormulaColumn from "./FormulaColumn";
import PeopleColumn from "./PeopleColumn";
import DropdownColumn from "./DropdownColumn";
import FilesAndMediaColumn from "./FilesAndMediaColumn";
import MatrixColumn from "./MatrixColumn";

function CustomFieldRenderer(props) {
  const { field = {}, fieldType, rowData, handleUpdateCustomField, permission, groupType, placeholder, workspaceId="" } = props;

  const Components = {
    textfield: () => <TextfieldColumn placeholder={placeholder} groupType={groupType} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>, /** render component for custom text field */
    matrix: () => <MatrixColumn placeholder={placeholder} groupType={groupType} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>, /** render component for custom text field */
    textarea: () => <TextareaColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>, /** render component for custom text area (frola editor) */
    location: () => <LocationColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>, /** render component for custom location field */
    email: () => <EmailColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>,  /** render component for custom email field */
    websiteurl: () => <UrlColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>,  /** render component for custom url field */
    date: () => <DateColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>,  /** render component for custom date field */
    money: () => <MoneyColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>,  /** render component for custom money field */
    number: () => <NumberColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>, /** render component for custom number field */
    country: () => <CountryColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>, /** render component for custom country field */
    dropdown: () => <DropdownColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>, /** render component for custom drop down field single and multi select */
    phone: () => <PhoneColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>,  /** render component for custom phone number field */
    people: () =>  <PeopleColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>, /** render component for custom people field */
    rating: () => <RatingColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>,  /** render component for custom rating field */
    formula: () => <FormulaColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>,  /** render component for custom formula field */
    filesAndMedia: () => <FilesAndMediaColumn groupType={groupType} placeholder={placeholder} field={field} permission={permission} rowData={rowData} handleUpdateCustomField={handleUpdateCustomField} workspaceId={workspaceId}/>, /** render component for custom files and media field */
  };
  const ExportComponent = () => fieldType && Components[fieldType]();
  return <>{ExportComponent()}</>;
}

export default CustomFieldRenderer;
