import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import IconPhone from "../../Icons/IconPhone";
import clsx from "clsx";

import CopyIcon from "../../Icons/IconCopyContent";
import CustomTooltip from "../../Tooltip/Tooltip";
import SvgIcon from "@material-ui/core/SvgIcon";

function PhoneColumn({ classes, theme, field, handleUpdateCustomField, customfields, rowData, permission }) {
  const phoneField = customfields[field.fieldId];
  const phone = field.fieldData.data ? field.fieldData.data : "";
  const [value, setValue] = useState(phone);
  const [active, setActive] = useState(false);

  const handleCall = e => {
    e.stopPropagation();
    window.open(`tel:${value}`, "_blank");
    setActive(false);
  };
  const onKeyDown = e => {
    e.stopPropagation();
    if (e.keyCode === 13) {
      /** on Enter click */
      if (phone !== value) handleUpdateCustomField(rowData, value, phoneField, phoneField.settings);
      setActive(false);
    } else if (e.keyCode === 27) {
      /** on ESC click */
      setValue(phone);
      setActive(false);
    }
  };
  const onBlur = e => {
    e.stopPropagation();
    if (phone !== value) handleUpdateCustomField(rowData, value, phoneField, phoneField.settings);
    setActive(false);
  };
  const handleChange = num => {
    setValue(`+${num}`);
  };
  const handleCopyClick = e => {
    e.stopPropagation();
    let textArea = document.createElement("textarea");
    textArea.value = value;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
    setActive(false);
  };
  const handleClick = () => {
    setActive(true);
  };
  useEffect(() => {
    setValue(phone);
  }, [field.fieldData]);
  return (
    <div
      data-rowClick="cell"
      className={clsx({
        [classes.activePhoneField]: active,
        [classes.phone]: !active,
      })}>
      <PhoneInput
        country={phoneField.settings.country}
        value={value}
        onChange={handleChange}
        onBlur={onBlur}
        onKeyDown={onKeyDown}
        disableDropdown={true}
        disableCountryCode={false}
        autoFormat={false}
        countryCodeEditable={false}
        containerClass={classes.phoneCnt}
        inputClass={classes.phoneInputCnt}
        buttonClass={classes.selectedFlag}
        disabled={!permission}
        onClick={handleClick}
      />
      {value && (
        <div className={classes.icons}>
          <CustomTooltip
            helptext={"Call"}
            iconType="help"
            placement="top"
            style={{ color: theme.palette.common.white }}>
            <SvgIcon
              className={classes.callIcon}
              fontSize="inherit"
              viewBox="0 0 16 17.778"
              onClick={handleCall}>
              <IconPhone color={"#999"} />
            </SvgIcon>
          </CustomTooltip>
          <CustomTooltip
            helptext={"Copy"}
            iconType="help"
            placement="top"
            style={{ color: theme.palette.common.white }}>
            <SvgIcon className={classes.copyIcon} viewBox="0 0 14 16.211" onClick={handleCopyClick}>
              <CopyIcon />
            </SvgIcon>
          </CustomTooltip>
        </div>
      )}
    </div>
  );
}
PhoneColumn.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  rowData: {},
  handleUpdateCustomField: () => {},
};
const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(PhoneColumn);
