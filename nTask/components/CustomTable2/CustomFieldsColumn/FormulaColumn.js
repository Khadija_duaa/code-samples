import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import CustomTooltip from "../../Tooltip/Tooltip";
import IconEdit from "../../Icons/IconEdit";
import SvgIcon from "@material-ui/core/SvgIcon";
import DropdownMenu from "../../Dropdown/DropdownMenu";
import SelectSearchDropdown from "../../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { getCustomFields } from "../../../helper/customFieldsData";
import { generateSelectData, generateDMAS } from "../../../helper/generateSelectData";
import CustomSelectIconDropdown from "../../Dropdown/CustomSelectIconDropdown/Dropdown.js";
import ButtonActionsCnt from "../../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import isEmpty from "lodash/isEmpty";
import clsx from "clsx";

function FormulaColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  customFieldsArr,
  profile,
  permission,
  groupType,
  placeholder = false,
  workspaceId=""
}) {
  const formulaField = customfields[field.fieldId];
  const fieldData = field.fieldData.data ? field.fieldData.data : {};
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedFieldfrom, setSelectedFieldfrom] = useState([]);
  const [selectedFieldTo, setSelectedFieldTo] = useState([]);
  const [selectedOperation, setSelectedOperation] = useState(generateDMAS(classes)[0]);

  const generateResult = (a, b, operator) => {
    switch (operator) {
      case 0:
        if (a !== "" && b !== "") return parseFloat(a) + parseFloat(b);
        return "";
        break;
      case 1:
        if (a !== "" && b !== "") return parseFloat(a) - parseFloat(b);
        return "";
        break;
      case 2:
        if (a !== "" && b !== "") return parseFloat(a) * parseFloat(b);
        return "";
        break;
      case 3:
        if (a !== "" && b !== "") return parseFloat(a) / parseFloat(b);
        return "";
        break;

      default:
        return "";
        break;
    }
  };
  const handleCalRes = () => {
    let fromField =
      rowData.customFieldData &&
      rowData.customFieldData.find(fromItem => fromItem.fieldId == selectedFieldfrom.id);
    let toField =
      rowData.customFieldData &&
      rowData.customFieldData.find(toItem => toItem.fieldId == selectedFieldTo.id);
    let result = "";
    if (fromField && toField) {
      result = generateResult(
        fromField.fieldData.data,
        toField.fieldData.data,
        selectedOperation.value
      );
    }
    return result;
  };
  const handleClick = event => {
    // Function Opens the dropdown
    setAnchorEl(event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const handleCloseAction = event => {
    // Function closes dropdown
    setAnchorEl(null);
    settingValuesInFields();
  };
  const handleCalculate = () => {
    handleUpdateCustomField(
      rowData,
      {
        fieldIdTo: selectedFieldTo.id,
        fieldIdFrom: selectedFieldfrom.id,
        operation: selectedOperation.value,
        result: handleCalRes(),
      },
      formulaField,
      formulaField.settings
    );

    setAnchorEl(null);
  };
  const handleSelectFrom = (type, option) => {
    setSelectedFieldfrom(option);
  };
  const handleSelectTo = (type, option) => {
    setSelectedFieldTo(option);
  };
  const handleSelectOperation = option => {
    setSelectedOperation(option);
  };

  const open = Boolean(anchorEl);

  let filteredFields = getCustomFields(customFieldsArr, profile, groupType, workspaceId).filter(
    f => f.fieldType == "number" || f.fieldType == "money"
  );
  const ddData = () => {
    return filteredFields.map(f => generateSelectData(f.fieldName, f.fieldId, f.fieldId, "", f));
  };
  const settingValuesInFields = () => {
    if (!isEmpty(fieldData)) {
      setSelectedFieldfrom(ddData().find(item => item.id === fieldData.fieldIdFrom) || {});
      setSelectedFieldTo(ddData().find(item => item.id === fieldData.fieldIdTo) || {});
      setSelectedOperation(generateDMAS(classes).find(item => item.value === fieldData.operation));
    } else {
      setSelectedFieldfrom([]);
      setSelectedFieldTo([]);
      setSelectedOperation(generateDMAS(classes)[0]);
    }
  };

  useEffect(() => {
    settingValuesInFields();
  }, [field.fieldData.data]);

  return (
    <>
      <span
        title={handleCalRes()}
        className={clsx({
          [classes.formula]: true,
          wrapText: true,
          [classes.disablePointerEvents]: !permission,
          [classes.centerAlign]: !handleCalRes() && placeholder,
        })}
        data-rowClick="cell">
        {handleCalRes() || placeholder}
        {permission && (
          <div className={classes.icons}>
            <CustomTooltip
              helptext={"Edit"}
              iconType="help"
              placement="top"
              ref={anchorEl}
              style={{ color: theme.palette.common.white }}>
              <SvgIcon className={classes.editIcon} viewBox="0 0 14 13.95" onClick={handleClick}>
                <IconEdit />
              </SvgIcon>
            </CustomTooltip>
          </div>
        )}
      </span>
      {open && (
        <DropdownMenu
          open={open}
          closeAction={handleClose}
          anchorEl={anchorEl}
          size="xlarge"
          placement="bottom"
          disablePortal={false}>
          <div className={classes.formulaCnt}>
            <div className={classes.fieldsCnt}>
              <SelectSearchDropdown
                data={ddData}
                placeholder={"Select"}
                isMulti={false}
                selectChange={(type, option) => {
                  handleSelectFrom(type, option);
                }}
                selectedValue={selectedFieldfrom}
                styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
              />
              <CustomSelectIconDropdown
                options={() => generateDMAS(classes)}
                option={selectedOperation}
                onSelect={handleSelectOperation}
                size="small"
                scrollHeight={320}
                disabled={false}
              />
              <SelectSearchDropdown
                data={ddData}
                placeholder={"Select"}
                isMulti={false}
                selectChange={(type, option) => {
                  handleSelectTo(type, option);
                }}
                selectedValue={selectedFieldTo}
                styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
              />
            </div>
            <ButtonActionsCnt
              cancelAction={handleCloseAction}
              successAction={handleCalculate}
              successBtnText={"Save"}
              cancelBtnText={"Cancel"}
              btnType="blue"
              disabled={false}
              btnQuery={false}
              customGridProps={{
                classes: {
                  container: classes.btnContainer,
                },
              }}
            />
          </div>
        </DropdownMenu>
      )}
    </>
  );
}

const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
    customFieldsArr: state.customFields,
    profile: state.profile.data,
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(FormulaColumn);
