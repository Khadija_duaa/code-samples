import React, { useEffect, useState } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import { getContrastYIQ } from "../../../helper/index";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import { customFieldDropdownData } from "../../../helper/customFieldsData";
import CustomButton from "../../Buttons/CustomIconButton";
import AddIcon from "@material-ui/icons/Add";

function DropdownColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  permission,
  placeholder = false,
}) {
  const ddField = customfields[field.fieldId];

  const [anchorEl, setAnchorEl] = useState(null);

  const ddData = ddField ? customFieldDropdownData(ddField.values.data) : [];

  const getSelectedValues = () => {
    if (ddField?.settings?.multiSelect) {
      const selectedIds = field.fieldData.data ? field.fieldData.data.map(i => i.id) : [];
      return ddData.filter(v => selectedIds.includes(v.id));
    } else {
      return field.fieldData.data ? ddData.filter(v => v.id === field.fieldData.data.id) : [];
    }
  };
  const [selectedOptions, setSelectedOptions] = useState([]);

  const handleClick = e => {
    e.stopPropagation();
    permission && setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleUpdateOption = (option, obj) => {
    if (ddField.settings.multiSelect) {
      handleUpdateCustomField(rowData, option, ddField, ddField.settings, succ => {});
    } else {
      handleUpdateCustomField(rowData, option.length ? option[0] : {}, ddField, ddField.settings);
    }
    handleClose();
  };

  useEffect(() => {
    const selectedValues = getSelectedValues();
    setSelectedOptions(selectedValues);
  }, [field.fieldData.data]);

  return ddField ?
    <div style={{ height: "100%", width: "100%" }}>
      {placeholder && selectedOptions.length == 0 ? (
        <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          width: "100%",
        }}>
        {placeholder}
      </div>
      ) : (
        <SearchDropdown
          initSelectedOption={selectedOptions}
          obj={{}}
          disabled={!permission}
          tooltip={false}
          TaskView={false}
          iconOption={false}
          popperProps={{ disablePortal: false }}
          optionsList={ddData}
          isMulti={ddField.settings.multiSelect}
          updateAction={(option, obj) => {
            handleUpdateOption(option, obj);
          }}
          customSearchRender={
            ddField.settings.multiSelect ? (
              <>
                <div className={`${classes.multiSelect} multiSelectDD`} 
                data-rowClick={permission ? "cell" : ''}
                >
                  <span>
                  {permission && (
                    <CustomButton
                      onClick={handleClick}
                      btnType="blueOutlined"
                      variant="contained"
                      buttonRef={anchorEl}
                      style={{
                        padding: 0,
                        marginRight: 5,
                      }}
                      disabled={!permission}>
                      <AddIcon
                        htmlColor={theme.palette.background.blue}
                        style={{ fontSize: "18px" }}
                      />
                    </CustomButton>
                  )}
                  </span>
                  {selectedOptions && selectedOptions.length
                    ? selectedOptions.map(value => {
                        return (
                          <div
                            className={classes.ddChip}
                            style={{
                              background: value.color,
                              color: getContrastYIQ(value.color && value.color),
                            }}
                            title={value.value}>
                            <span className={classes.ddChipOption}>{value.value}</span>
                          </div>
                        );
                      })
                    : null}
                </div>
              </>
            ) : (
              <>
                <div
                  className={classes.singleSelectCnt}
                  style={{
                    backgroundColor: selectedOptions.length ? selectedOptions[0].color : "white",
                    color: selectedOptions.length
                      ? getContrastYIQ(selectedOptions[0].color)
                      : "black",
                  }}
                  ref={anchorEl}
                  data-rowClick={permission ? "cell" : ''}
                  onClick={handleClick}
                  title={selectedOptions.length ? selectedOptions[0].label : ""}>
                  <span className={`${classes.singleSelect} wrapText`}>
                    {selectedOptions.length ? selectedOptions[0].label : "Select"}
                  </span>
                </div>
              </>
            )
          }
          selectedOptionHead={"Selected"}
          allOptionsHead={"All options"}
          buttonPlaceholder={"Select"}
          anchorEl={anchorEl}
          handleCloseProject={handleClose}
        />
      )}
    </div>
  : <></>
}

const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(DropdownColumn);
