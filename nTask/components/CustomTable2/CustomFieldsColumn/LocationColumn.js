import React, { useMemo, useRef, useState, useEffect } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import clsx from "clsx";

import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import IconEdit from "../../Icons/IconEdit";
import SvgIcon from "@material-ui/core/SvgIcon";
import DropdownMenu from "../../Dropdown/DropdownMenu";
import Grid from "@material-ui/core/Grid";
import CopyIcon from "../../Icons/IconCopyContent";
import CustomTooltip from "../../Tooltip/Tooltip";
import { flags } from "../../../helper/flags";
import SelectSearchDropdown from "../../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { CopyToClipboard } from "react-copy-to-clipboard";
import EditableTextField from "../../Form/EditableTextField";

function LocationColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  permission,placeholder
}) {
  const locationField = customfields[field.fieldId];
  const fieldData = field.fieldData.data ? field.fieldData.data : {};

  const handleCountryCode = flags.map(f => {
    return {
      label: f.name,
      value: f.dial_code,
      code: f.code,
      obj: f,
      icon: (
        <img
          style={{ marginRight: "5px" }}
          src={`https://flagcdn.com/${f.code}.svg`}
          width="20"
          height="15"
        />
      ),
    };
  });

  const selectedCountry = handleCountryCode.filter(
    country => country.label === fieldData.countryCode
  );

  const [anchorEl, setAnchorEl] = useState(null);
  const [locationObj, setLocationObj] = useState({
    lineOne: fieldData.lineOne || "",
    lineTwo: fieldData.lineTwo || "",
    city: fieldData.city || "",
    province: fieldData.province || "",
    zipCode: fieldData.zipCode || "",
    countryCode: selectedCountry || [],
  });
  const handleClick = event => {
    // Function Opens the dropdown
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const handleChange = (e, type) => {
    e.stopPropagation();
    setLocationObj({ ...locationObj, [type]: e.target.value });
  };
  const handleSelect = (type, option, key) => {
    setLocationObj({ ...locationObj, [key]: option });
    handleUpdateCustomField(
      rowData,
      { ...locationObj, countryCode: option.label },
      locationField,
      locationField.settings
    );
  };
  const handleUpdateData = () => {
    handleUpdateCustomField(
      rowData,
      {
        ...locationObj,
        countryCode: locationObj.countryCode.length ? locationObj.countryCode[0].label : "",
      },
      locationField,
      locationField.settings
    );
  };
  const updateData = (type, clearVal) => {
    if (clearVal) {
      setLocationObj({ ...locationObj, [type]: fieldData[type] || "" });
    } else {
      handleUpdateData();
    }
  };
  const handleKeyDown = (e, type) => {
    e.stopPropagation();
    //   function calls onKeyDown event and checks the validation
    if (e.keyCode === 13) {
      updateData(type, false);
    } else if (e.keyCode === 27) {
      /** on ESC click */
      updateData(type, true);
    }
  };

  useEffect(() => {
    setLocationObj({
      lineOne: fieldData.lineOne || "",
      lineTwo: fieldData.lineTwo || "",
      city: fieldData.city || "",
      province: fieldData.province || "",
      zipCode: fieldData.zipCode || "",
      countryCode: selectedCountry || {},
    });
  }, [field.fieldData]);

  const fullAddress = `${fieldData.lineOne || ""} ${fieldData.lineTwo || ""} ${fieldData.city ||
    ""} ${fieldData.province || ""} ${fieldData.zipCode || ""} ${fieldData.countryCode ||
    ""}`;

  const open = Boolean(anchorEl);
  return (
    <>
      <span
        title={fullAddress}
        className={clsx({
          [classes.address]: true,
          wrapText: true,
          [classes.disablePointerEvents]: !permission,
          [classes.centerAlign] : !fullAddress.trim() && placeholder
        })}
        data-rowClick="cell">
        {fullAddress.trim() || placeholder}
        {permission && (
          <div className={classes.icons}>
            <CustomTooltip
              helptext={"Edit"}
              iconType="help"
              placement="top"
              ref={anchorEl}
              style={{ color: theme.palette.common.white }}>
              <SvgIcon className={classes.editIcon} viewBox="0 0 14 13.95" onClick={handleClick}>
                <IconEdit />
              </SvgIcon>
            </CustomTooltip>
          </div>
        )}
      </span>
      {open && (
        <DropdownMenu
          open={open}
          closeAction={handleClose}
          anchorEl={anchorEl}
          size="xxlarge"
          placement="bottom"
          disablePortal={false}>
          <div className={classes.dropDownCnt}>
            <Grid container spacing={1} style={{ flex: 1 }}>
              <Grid item xs={12} sm={12} md={12}>
                <EditableTextField
                  title={locationObj.lineOne}
                  module={""}
                  placeholder={"Line 1"}
                  titleProps={{
                    className: classes.fieldTitle,
                  }}
                  textFieldProps={{
                    customInputClass: {
                      input: classes.outlinedInputsmall2,
                      root: classes.outlinedInputCnt,
                    },
                  }}
                  formStyles={{
                    height: "32px",
                    borderRadius: 4,
                  }}
                  defaultProps={{
                    value: locationObj.lineOne,
                    onChange: e => handleChange(e, "lineOne"),
                    onKeyDown: e => handleKeyDown(e, "lineOne"),
                    onBlur: event => {
                      event.stopPropagation();
                      handleUpdateData();
                    },
                    inputProps: { maxLength: 125 },
                  }}
                  inputEl={{}}
                  handleEditTitle={() => {
                    // updateData("lineOne", true);
                  }}
                  textFieldIcons={
                    <CopyToClipboard text={fullAddress}>
                      <CustomTooltip
                        helptext={"Copy"}
                        iconType="help"
                        placement="top"
                        style={{ color: theme.palette.common.white }}>
                        <SvgIcon
                          className={classes.copyIcon}
                          viewBox="0 0 14 16.211"
                          // onClick={handleCopyClick}
                        >
                          <CopyIcon />
                        </SvgIcon>
                      </CustomTooltip>
                    </CopyToClipboard>
                  }
                  textFieldIconsProps={{
                    className: classes.iconCnt,
                  }}
                  permission={true}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <EditableTextField
                  title={locationObj.lineTwo}
                  module={""}
                  placeholder={"Line 2"}
                  titleProps={{
                    className: classes.fieldTitle,
                  }}
                  textFieldProps={{
                    customInputClass: {
                      input: classes.outlinedInputsmall2,
                      root: classes.outlinedInputCnt,
                    },
                  }}
                  formStyles={{
                    height: "32px",
                    borderRadius: 4,
                  }}
                  inputEl={{}}
                  handleEditTitle={() => {
                    // updateData("lineTwo", true);
                  }}
                  textFieldIcons={null}
                  textFieldIconsProps={{
                    className: classes.iconCnt,
                  }}
                  permission={true}
                  defaultProps={{
                    value: locationObj.lineTwo,
                    onChange: e => handleChange(e, "lineTwo"),
                    onKeyDown: e => handleKeyDown(e, "lineTwo"),
                    onBlur: event => {
                      event.stopPropagation();
                      handleUpdateData();
                    },
                    inputProps: { maxLength: 125 },
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <EditableTextField
                  title={locationObj.city}
                  module={""}
                  placeholder={"City"}
                  titleProps={{
                    className: classes.fieldTitle,
                  }}
                  textFieldProps={{
                    customInputClass: {
                      input: classes.outlinedInputsmall2,
                      root: classes.outlinedInputCnt,
                    },
                  }}
                  formStyles={{
                    height: "32px",
                    borderRadius: 4,
                  }}
                  inputEl={{}}
                  handleEditTitle={() => {
                    // updateData("city", true);
                  }}
                  textFieldIcons={null}
                  textFieldIconsProps={{
                    className: classes.iconCnt,
                  }}
                  permission={true}
                  defaultProps={{
                    value: locationObj.city,
                    onChange: e => handleChange(e, "city"),
                    onKeyDown: e => handleKeyDown(e, "city"),
                    onBlur: event => {
                      event.stopPropagation();
                      handleUpdateData();
                    },
                    inputProps: { maxLength: 38 },
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <EditableTextField
                  title={locationObj.province}
                  module={""}
                  placeholder={"St/Pro/Reg"}
                  titleProps={{
                    className: classes.fieldTitle,
                  }}
                  textFieldProps={{
                    customInputClass: {
                      input: classes.outlinedInputsmall2,
                      root: classes.outlinedInputCnt,
                    },
                  }}
                  formStyles={{
                    height: "32px",
                    borderRadius: 4,
                  }}
                  inputEl={{}}
                  handleEditTitle={() => {
                    // updateData("province", true);
                  }}
                  textFieldIcons={null}
                  textFieldIconsProps={{
                    className: classes.iconCnt,
                  }}
                  permission={true}
                  defaultProps={{
                    value: locationObj.province,
                    onChange: e => handleChange(e, "province"),
                    onKeyDown: e => handleKeyDown(e, "province"),
                    onBlur: event => {
                      event.stopPropagation();
                      handleUpdateData();
                    },
                    inputProps: { maxLength: 38 },
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <EditableTextField
                  title={locationObj.zipCode}
                  module={""}
                  placeholder={"Zip code"}
                  titleProps={{
                    className: classes.fieldTitle,
                  }}
                  textFieldProps={{
                    customInputClass: {
                      input: classes.outlinedInputsmall2,
                      root: classes.outlinedInputCnt,
                    },
                  }}
                  formStyles={{
                    height: "32px",
                    borderRadius: 4,
                  }}
                  inputEl={{}}
                  handleEditTitle={() => {
                    // updateData("zipCode", true);
                  }}
                  textFieldIcons={null}
                  textFieldIconsProps={{
                    className: classes.iconCnt,
                  }}
                  permission={true}
                  defaultProps={{
                    value: locationObj.zipCode,
                    onChange: e => handleChange(e, "zipCode"),
                    onKeyDown: e => handleKeyDown(e, "zipCode"),
                    onBlur: event => {
                      event.stopPropagation();
                      handleUpdateData();
                    },
                    inputProps: { maxLength: 10 },
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <SelectSearchDropdown
                  data={() => handleCountryCode}
                  placeholder={<span style={{ color: "#7e7e7e" }}>Country </span>}
                  icon={true}
                  label={""}
                  isMulti={false}
                  selectChange={(type, option) => {
                    handleSelect(type, option, "countryCode");
                  }}
                  selectedValue={locationObj.countryCode || []}
                  styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                  customStyles={{
                    control: {
                      border: "none",
                      // padding: "0px 5px",
                      fontFamily: theme.typography.fontFamilyLato,
                      fontWeight: theme.typography.fontWeightExtraLight,
                      background: `#F6F6F6 !important`,
                      "&:hover": {
                        // background: `${theme.palette.background.items} !important`,
                      },
                      minHeight: 32,
                    },
                  }}
                  isDisabled={false}
                  writeFirst={true}
                />
              </Grid>
            </Grid>
          </div>
        </DropdownMenu>
      )}
    </>
  );
}
LocationColumn.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  rowData: {},
  handleUpdateCustomField: () => {},
  placeholder:false
};
const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(LocationColumn);
