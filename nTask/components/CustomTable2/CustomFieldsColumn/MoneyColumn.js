import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import IconEdit from "../../Icons/IconEdit";
import CustomTooltip from "../../Tooltip/Tooltip";
import DefaultTextField from "../../Form/TextField";
import SvgIcon from "@material-ui/core/SvgIcon";
import clsx from "clsx";

function MoneyColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  userPreferenceState,
  permission,
}) {
  const moneyField = customfields[field.fieldId];

  const [value, setValue] = useState(field.fieldData.data || "");
  const [edit, setEdit] = useState(false);

  const handleEdit = e => {
    e && e.stopPropagation();
    setEdit(!edit);
  };

  const handleChange = e => {
    e && e.stopPropagation();
    setValue(e.target.value);
  };

  const handleUpdateData = () => {
    handleEdit();
    if (value !== field.fieldData.data)
      handleUpdateCustomField(rowData, value.replace(/,/g, ""), moneyField, moneyField.settings);
  };

  useEffect(() => {
    setValue(field.fieldData.data || "");
  }, [field.fieldData]);

  const selectedCurrency =
    userPreferenceState.constant.currencies.find(
      c => c.isoCurrencySymbol === moneyField.settings.currency
    ) || "";
  const moneyValue = `${selectedCurrency.currencySymbol} ${value}`;

  return (
    <>
      {!edit ? (
        <span
          title={moneyValue}
          className={clsx({
            [classes.money]: true,
            wrapText: true,
            [classes.disablePointerEvents]: !permission,
          })}
          data-rowClick="cell">
          {moneyValue}
          {permission && (
            <div className={classes.icons} onClick={handleEdit}>
              <CustomTooltip
                helptext={"Edit"}
                iconType="help"
                placement="top"
                style={{ color: theme.palette.common.white }}>
                <SvgIcon className={classes.editIcon} viewBox="0 0 14 13.95">
                  <IconEdit />
                </SvgIcon>
              </CustomTooltip>
            </div>
          )}
        </span>
      ) : (
        <div data-rowClick="cell" className={classes.fieldCnt}>
          <DefaultTextField
            label=""
            fullWidth
            borderBottom={false}
            noRightBorder={false}
            noLeftBorder={false}
            outlineCnt={false}
            formControlStyles={{ marginBottom: 0, height: "100%" }}
            error={false}
            errorState={false}
            errorMessage={""}
            defaultProps={{
              id: "customMoneyField",
              type: "number",
              onChange: handleChange,
              onKeyDown: e => {
                if (e.keyCode === 13) {
                  /** Enter */
                  handleUpdateData();
                }
                if (e.keyCode === 27) {
                  /** Esc */
                  setValue(field.fieldData.data || "");
                  handleEdit();
                }
              },
              onBlur: event => {
                event.stopPropagation();
                handleUpdateData();
              },
              value: value,
              placeholder: "",
              autoFocus: true,
              inputProps: { maxLength: 250 },
              style: {},
              disabled: !permission,
              autoComplete: "off",
              inputRef: () => {},
            }}
            transparentInput={true}
            customInputClass={{
              input: classes.outlinedInputsmall,
              root: classes.outlinedInputCnt,
            }}
          />
        </div>
      )}
    </>
  );
}

MoneyColumn.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  rowData: {},
  handleUpdateCustomField: () => {},
};
const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
    userPreferenceState: state.userPreference.data || [],
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(MoneyColumn);
