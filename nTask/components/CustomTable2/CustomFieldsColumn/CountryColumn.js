import React, { useState, useMemo } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import { flags } from "../../../helper/flags";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";

function CountryColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  permission,
  placeholder,
}) {
  const countryField = customfields[field.fieldId];

  const countryData = flags.map(f => {
    return {
      label: f.name,
      value: f.dial_code,
      code: f.code,
      id: f.dial_code,
      obj: f,
      icon: (
        <img
          src={`https://flagcdn.com/${f.code}.svg`}
          width="20"
          height="15"
          style={{ marginRight: "5px", marginBottom: -2 }}
        />
      ),
    };
  });

  const selectedValue = countryData.filter(f => f.label === field.fieldData.data) || {};

  const handleSelect = (newValue, obj) => {
    const label = newValue.length ? newValue[0].label : "";
    handleUpdateCustomField(rowData, label, countryField, countryField.settings);
  };
  return (
    <>
      {placeholder && selectedValue.length == 0 ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
          }}>
          {placeholder}
        </div>
      ) : (
        <SearchDropdown
          initSelectedOption={selectedValue}
          obj={{}}
          disabled={!permission}
          labelAlign={"left"}
          tooltip={false}
          TaskView={false}
          iconOption={true}
          popperProps={{ disablePortal: false }}
          optionsList={countryData}
          singleSelect
          updateAction={(option, obj) => {
            handleSelect(option, obj);
          }}
          selectedOptionHead={"Selected country"}
          allOptionsHead={"All countries list"}
          buttonPlaceholder={"Select Country"}
          buttonProps={{
            style: {
              width: "100%",
              height: "100%",
              textAlign: "left",
            },
          }}
          btnTextProps={{
            style: {
              width: "90%",
              overflow: "hidden",
              whiteSpace: "nowrap",
              textOverflow: "ellipsis",
              fontSize: "13px",
            },
          }}
        />
      )}
    </>
  );
}

CountryColumn.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  rowData: {},
  handleUpdateCustomField: () => {},
  placeholder: false,
};

const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(CountryColumn);
