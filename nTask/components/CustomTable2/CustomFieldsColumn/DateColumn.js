import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import styles from "./style";
import moment from "moment";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import CustomDatePicker from "../../DatePicker/DatePicker/DatePicker";

function DateColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  permission,
  placeholder,
}) {
  const dateField = customfields[field.fieldId];
  const date =
    field.fieldData.data && field.fieldData.data.date
      ? moment(field.fieldData.data.date).format("lll")
      : "";
  const time = field.fieldData.data && field.fieldData.data.time ? field.fieldData.data.time : "";

  const [selectedDate, setSelectedDate] = useState(date);
  const [selectedTime, setSelectedtime] = useState(time);
  const handleUpdate = (date, time) => {
    handleUpdateCustomField(
      rowData,
      { date: date || "", time: time || "" },
      dateField,
      dateField.settings
    );
  };
  useEffect(() => {
    setSelectedDate(date);
    setSelectedtime(time);
  }, [field.fieldData]);

  return (
    <div data-rowClick="cell">
      {placeholder && !selectedDate ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
          }}>
          {placeholder}
        </div>
      ) : (
        <CustomDatePicker
          date={selectedDate && moment(selectedDate)}
          dateFormat="MMM DD, YYYY"
          timeInput={true}
          onSelect={handleUpdate}
          selectedTime={selectedTime}
          style={{ position: "relative", height: "100%", width: "100%" }}
          label={null}
          icon={false}
          PopperProps={{ size: null, disablePortal: false }}
          filterDate={moment()}
          timeInputLabel={"Start Time"}
          btnProps={{ className: classes.datePickerCustomStyle }}
          deleteIcon={true}
          placeholder={"Select Date"}
          containerProps={{
            className: classes.datePickerContainer,
          }}
          disabled={!permission}
          deleteIconProps={{
            classes: { root: classes.deleteIcon },
          }}
        />
      )}
    </div>
  );
}
DateColumn.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  rowData: {},
  handleUpdateCustomField: () => {},
  placeholder: false,
};
const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(DateColumn);
