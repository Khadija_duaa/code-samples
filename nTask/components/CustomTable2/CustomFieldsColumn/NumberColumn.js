import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import { generateNumberValue } from "../../../helper/generateNumberFieldData";
import IconEdit from "../../Icons/IconEdit";
import CustomTooltip from "../../Tooltip/Tooltip";
import DefaultTextField from "../../Form/TextField";
import SvgIcon from "@material-ui/core/SvgIcon";
import clsx from "clsx";

function NumberColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  permission,
}) {
  const numberField = customfields[field.fieldId];
  const [value, setValue] = useState(
    generateNumberValue(field.fieldData.data || "", numberField) || ""
  );
  const [edit, setEdit] = useState(false);

  const handleEdit = e => {
    e && e.stopPropagation();
    setEdit(!edit);
  };

  const handleChange = e => {
    e && e.stopPropagation();
    setValue(generateNumberValue(e.target.value, numberField));
  };

  const handleUpdateData = () => {
    handleEdit();
    if (value !== field.fieldData.data)
      handleUpdateCustomField(rowData, value.replace(/,/g, ""), numberField, numberField.settings);
  };
  useEffect(() => {
    setValue(generateNumberValue(field.fieldData.data || "", numberField) || "");
  }, [field.fieldData]);

  const numberValue = `${
    numberField.settings.direction === "left" && numberField.settings.unit !== "none"
      ? numberField.settings.unit
      : ""
  } ${generateNumberValue(value || "", numberField)} ${
    numberField.settings.direction === "right" && numberField.settings.unit !== "none"
      ? numberField.settings.unit
      : ""
  }`;

  return (
    <>
      {!edit ? (
        <span
          title={numberValue}
          className={clsx({
            [classes.number]: true,
            wrapText: true,
            [classes.disablePointerEvents]: !permission,
          })}
          data-rowClick="cell">
          {numberValue}
          {permission && (
            <div className={classes.icons} onClick={handleEdit}>
              <CustomTooltip
                helptext={"Edit"}
                iconType="help"
                placement="top"
                style={{ color: theme.palette.common.white }}>
                <SvgIcon className={classes.editIcon} viewBox="0 0 14 13.95">
                  <IconEdit />
                </SvgIcon>
              </CustomTooltip>
            </div>
          )}
        </span>
      ) : (
        <div data-rowClick="cell" className={classes.fieldCnt}>
          <DefaultTextField
            label=""
            fullWidth
            borderBottom={false}
            noRightBorder={false}
            noLeftBorder={false}
            outlineCnt={false}
            formControlStyles={{ marginBottom: 0, height: "100%" }}
            error={false}
            errorState={false}
            errorMessage={""}
            defaultProps={{
              id: "customNumberField",
              onChange: handleChange,
              onKeyDown: e => {
                if (e.keyCode === 13) {
                  /** Enter */
                  handleUpdateData();
                }
                if (e.keyCode === 27) {
                  /** Esc */
                  setValue(field.fieldData.data || "");
                  handleEdit();
                }
              },
              onBlur: event => {
                event.stopPropagation();
                handleUpdateData();
              },
              value: value,
              placeholder: "",
              autoFocus: true,
              inputProps: { maxLength: 250 },
              style: {},
              disabled: !permission,
              autoComplete: "off",
              inputRef: () => {},
            }}
            transparentInput={true}
            customInputClass={{
              input: classes.outlinedInputsmall,
              root: classes.outlinedInputCnt,
            }}
          />
        </div>
      )}
    </>
  );
}

NumberColumn.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  rowData: {},
  handleUpdateCustomField: () => {},
};

const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(NumberColumn);
