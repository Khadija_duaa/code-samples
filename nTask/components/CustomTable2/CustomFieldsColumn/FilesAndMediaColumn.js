import React, { useState } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import CancelIcon from "@material-ui/icons/Cancel";
import CustomButton from "../../Buttons/CustomIconButton";
import AddIcon from "@material-ui/icons/Add";
import Dropzone from "react-dropzone";
import { withSnackbar } from "notistack";
import { attachMultipleFile } from "../../../redux/actions/projects";

const acceptedFormats = [
  "image/bmp",
  "image/gif",
  "image/jpeg",
  "image/png",
  "image/webp",
  "video/webm",
  "video/ogg",
  "video/mp4",
];
const maxSize = 20971520; //bytes = 20 MB

function FilesAndMediaColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  profile,
  groupType = "task",
  attachMultipleFile,
  permission,
  placeholder = false,
}) {
  const mediaField = customfields[field.fieldId];
  const [percentage, setPercentage] = useState(0);
  const [filename, setFilename] = useState("");

  const handleDeleteAttachment = (e, file) => {
    e.stopPropagation();
    let updatedAttachment = field.fieldData.data.filter(f => f.fileId !== file.fileId);
    handleUpdateCustomField(rowData, updatedAttachment, mediaField, mediaField.settings);
  };
  const handleDownloadFile = (e, file) => {
    e.stopPropagation();
    let a = document.createElement("a");
    a.href = file.publicURL;
    a.setAttribute("download", file.fileName);
    a.click();
  };

  const getId = () => {
    if (groupType === "risk") return "riskid";
    if (groupType === "task") return "taskid";
    if (groupType === "issue") return "issueid";
    if (groupType === "project") return "projectid";
    if (groupType === "meeting") return "meetingid";
  };

  const groupId = () => {
    if (groupType === "risk") return rowData.id;
    if (groupType === "task") return rowData.taskId;
    if (groupType === "issue") return rowData.id;
    if (groupType === "project") return rowData.projectId;
  };

  const verifyFile = files => {
    if (files && files.length > 0) {
      const currentFile = files[0];
      let error = "";
      if (currentFile.hasOwnProperty("errors")) {
        currentFile.errors.forEach(err => {
          if (err.code === "file-too-large") {
            showSnackbar("!Oops Uploading failed, maximum upload size is 20MB", "error");
          }
          if (err.code === "file-invalid-type") {
            showSnackbar("File type not supported", "error");
          }
        });
      }
      if (error != "") {
        alert(error);
        return false;
      }
      return true;
    }
  };
  const onhandleDrop = (files, rejectedFiles) => {
    if (rejectedFiles && rejectedFiles.length > 0) {
      verifyFile(rejectedFiles);
    }

    if (files && files.length > 0) {
      const isVerified = verifyFile(files);
      setFilename(files[0].name);
      const options = {
        onUploadProgress: progressEvent => {
          const { loaded, total } = progressEvent;
          let percent = Math.floor((loaded * 100) / total);
          if (percent < 100) {
            setPercentage(percent);
          }
        },
      };

      if (isVerified) {
        var newData = new FormData();
        newData.append("File", files[0]);
        newData.append(getId(), groupId());
        newData.append("userid", profile.userId);
        attachMultipleFile(
          /** uploading file to s3 bucket */
          newData,
          data => {
            const fileData =
              field && field.fieldData.data ? [...data, ...field.fieldData.data] : data;
            handleUpdateCustomField(
              rowData,
              fileData,
              mediaField,
              mediaField.settings,
              succ => {
                setPercentage(0);
              },
              fail => {
                setPercentage(0);
                showSnackbar("!Oops Server throws error", "error");
              }
            );
          },
          err => {},
          options
        );
      }
    }
  };
  const showSnackbar = (snackBarMessage, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  const renderValues = () => {
    if (field.fieldData.data && field.fieldData.data.length) {
      return field.fieldData.data.map(value => {
        return (
          <div className={classes.attachmentCapsule} onClick={e => handleDownloadFile(e, value)}>
            <span>{`${value.fileName}`}</span>
            {permission && (
              <span onClick={e => handleDeleteAttachment(e, value)}>
                <CancelIcon className={classes.deleteAttachmentIcon} />
              </span>
            )}
          </div>
        );
      });
    } else if (placeholder) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
          }}>
          {placeholder}
        </div>
      );
    }
  };

  return (
    <>
      <div className={`${classes.multiSelect} multiSelectDD`} data-rowClick="cell">
        <Dropzone
          onDrop={(files, rejectedFile) => {
            permission && onhandleDrop(files, rejectedFile);
          }}
          onDragOver={() => {}}
          onDropRejected={() => {}}
          accept={acceptedFormats}
          activeStyle={{}}
          style={{}}
          maxSize={maxSize}
          // disableClick
          onClick={evt => evt.preventDefault()}>
          {({ getRootProps, getInputProps, open }) => {
            return (
              <>
                <div {...getRootProps()} onClick={e => e.stopPropagation()} style={{}}>
                  <input {...getInputProps()} />
                  {permission && (
                    <CustomButton
                      onClick={() => open()}
                      btnType="blueOutlined"
                      variant="contained"
                      style={{
                        padding: 0,
                        marginRight: 5,
                      }}
                      disabled={!permission}>
                      <AddIcon
                        htmlColor={theme.palette.background.blue}
                        style={{ fontSize: "18px" }}
                      />
                    </CustomButton>
                  )}
                </div>
              </>
            );
          }}
        </Dropzone>
        {percentage > 0 && (
          <span
            className={classes.attachmentCapsule}
            style={{
              background: `linear-gradient(to right, #0090ff ${percentage}%, white ${percentage -
                100}%) `,
            }}>{`${filename}`}</span>
        )}
        {renderValues()}
      </div>
    </>
  );
}

const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
    profile: state.profile.data,
  };
};
export default compose(
  withSnackbar,
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, { attachMultipleFile })
)(FilesAndMediaColumn);
