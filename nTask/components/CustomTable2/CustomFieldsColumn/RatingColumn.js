import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import RatingComponent from "../../Rating";

function RatingColumn({ classes, theme, field, handleUpdateCustomField, customfields, rowData, permission }) {
  const ratingField = customfields[field.fieldId];
  const handleSelectEmoji = value => {
    handleUpdateCustomField(rowData, value, ratingField, ratingField.settings);
  };

  return (
    <>
      <RatingComponent
        item={ratingField.settings.emoji}
        iteration={ratingField.settings.scale}
        handleSelect={handleSelectEmoji}
        selectionNum={field.fieldData.data}
        permission={permission}
      />
    </>
  );
}

RatingColumn.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  rowData: {},
  handleUpdateCustomField: () => {},
};

const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(RatingColumn);
