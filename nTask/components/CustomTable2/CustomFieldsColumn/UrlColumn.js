import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import IconEdit from "../../Icons/IconEdit";
import CopyIcon from "../../Icons/IconCopyContent";
import clsx from "clsx";

import CustomTooltip from "../../Tooltip/Tooltip";
import DefaultTextField from "../../Form/TextField";
import SvgIcon from "@material-ui/core/SvgIcon";

function UrlColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  permission,
  placeholder
}) {
  const urlField = customfields[field.fieldId];

  const [value, setValue] = useState(field.fieldData.data || "");
  const [edit, setEdit] = useState(false);
  const [errorState, setErrorState] = useState(false);

  const handleEdit = e => {
    e && e.stopPropagation();
    setEdit(!edit);
  };

  const handleChange = e => {
    e && e.stopPropagation();
    setValue(e.target.value);
  };

  const handleUpdateData = () => {
    if (value == "") {
      handleEdit();
      setErrorState(false);
      handleUpdateCustomField(rowData, value, urlField, urlField.settings);
      return;
    }

    let expressionA = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi; /** do not require HTTP protocol */
    let expressionB = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi; /** ensure URL starts with HTTP/HTTPS */
    let regexA = new RegExp(expressionA);
    let regexB = new RegExp(expressionB);
    if (value.match(regexA) || value.match(regexB)) {
      handleEdit();
      setErrorState(false);
      if (value !== field.fieldData.data)
        handleUpdateCustomField(rowData, value, urlField, urlField.settings);
    } else {
      setErrorState(true);
    }
  };

  const handleClick = e => {
    e.stopPropagation();
    if (field.fieldData.data !== "") {
      let url = field.fieldData.data;
      if (!url.match(/^https?:\/\//i)) {
        url = "http://" + url;
      }
      window.open(url, "_blank");
    }
  };

  const handleCopyClick = e => {
    e.stopPropagation();
    let copyText = document.getElementById(`url:${field.customFieldDataId}`);
    let textArea = document.createElement("textarea");
    textArea.value = copyText.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
  };
  useEffect(() => {
    setValue(field.fieldData.data || "");
  }, [field.fieldData]);

  return (
    <>
      {!edit ? (
        <span
          title={value}
          className={clsx({
            [classes.url]: true,
            wrapText: true,
            [classes.disablePointerEvents]: !permission,
            [classes.centerAlign] : !value && placeholder
          })}
          data-rowClick="cell"
          onClick={handleClick}
          id={`url:${field.customFieldDataId}`}>
          {value || placeholder}
          {permission && (
            <div className={classes.icons}>
              <CustomTooltip
                helptext={"Edit"}
                iconType="help"
                placement="top"
                style={{ color: theme.palette.common.white }}>
                <SvgIcon className={classes.editIcon} viewBox="0 0 14 13.95" onClick={handleEdit}>
                  <IconEdit />
                </SvgIcon>
              </CustomTooltip>
              {value && (
                <CustomTooltip
                  helptext={"Copy"}
                  iconType="help"
                  placement="top"
                  style={{ color: theme.palette.common.white }}>
                  <SvgIcon
                    className={classes.copyIcon}
                    viewBox="0 0 14 16.211"
                    onClick={handleCopyClick}>
                    <CopyIcon />
                  </SvgIcon>
                </CustomTooltip>
              )}
            </div>
          )}
        </span>
      ) : (
        <div
          data-rowClick="cell"
          className={classes.fieldCnt}
          style={{
            border: errorState ? "1px solid #de133e" : "1px dashed #0090ff",
          }}>
          <DefaultTextField
            label=""
            fullWidth
            borderBottom={false}
            noRightBorder={false}
            noLeftBorder={false}
            outlineCnt={false}
            formControlStyles={{ marginBottom: 0, height: "100%" }}
            error={false}
            errorState={false}
            errorMessage={""}
            defaultProps={{
              id: `customUrlField:${field.customFieldDataId}`,
              onChange: handleChange,
              onKeyDown: e => {
                if (e.keyCode === 13) {
                  /** Enter */
                  handleUpdateData();
                }
                if (e.keyCode === 27) {
                  /** Esc */
                  setValue(field.fieldData.data || "");
                  setErrorState(false);
                  handleEdit();
                }
              },
              onBlur: event => {
                event.stopPropagation();
                handleUpdateData();
              },
              value: value,
              placeholder: "",
              autoFocus: true,
              inputProps: { maxLength: 250 },
              style: {},
              disabled: false,
              autoComplete: "off",
              inputRef: () => {},
            }}
            transparentInput={true}
            customInputClass={{
              input: classes.outlinedInputsmall,
              root: classes.outlinedInputCnt,
            }}
          />
        </div>
      )}
    </>
  );
}

UrlColumn.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  rowData: {},
  handleUpdateCustomField: () => {},
  placeholder:false
};
const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(UrlColumn);
