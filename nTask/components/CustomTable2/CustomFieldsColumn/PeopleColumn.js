//flow
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import { generateActiveMembers } from "../../../helper/getActiveMembers";
import AddIcon from "@material-ui/icons/Add";

function PeopleColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  permission,
  placeholder,
}) {
  const peopleField = customfields[field.fieldId];

  const updateAssignee = (assignee, obj) => {
    const idArr = assignee.map(assig => assig.userId);
    handleUpdateCustomField(rowData, idArr, peopleField, peopleField.settings);
  };
  const compareMembersList = () => {
    /** filtering the list of members on basic od user Id */
    const members = generateActiveMembers();
    let memberList = field.fieldData.data
      ? members.filter(member => field.fieldData.data.includes(member.userId))
      : [];
    return memberList;
  };
  const membersList = compareMembersList();
  return (
    <div data-rowClick="cell">
      {placeholder && membersList.length == 0 ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
          }}>
          {placeholder}
        </div>
      ) : (
        <AssigneeDropdown
          popperProps={{ disablePortal: false }}
          assignedTo={membersList}
          updateAction={updateAssignee}
          isArchivedSelected={false}
          obj={{}}
          avatarSize={"xsmall"}
          buttonVariant={"small"}
          customIconRenderer={<AddIcon htmlColor={theme.palette.text.dark} />}
          disabled={!permission}
          customIconButtonProps={{
            classes: {
              root: classes.iconBtnStyles,
            },
          }}
          totalAssigneeBtnProps={{
            style: {
              height: 29,
              width: 29,
              fontSize: "14px",
              fontWeight: theme.typography.fontWeightLarge,
            },
          }}
          addPermission={permission}
          deletePermission={permission}
          singleSelect={!peopleField?.settings?.multiplePeople}
        />
      )}
    </div>
  );
}

PeopleColumn.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  rowData: {},
  handleUpdateCustomField: () => {},
  placeholder: false,
};
const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(PeopleColumn);
