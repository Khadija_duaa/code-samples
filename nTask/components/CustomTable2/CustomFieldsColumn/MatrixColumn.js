import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomfieldSelector from "../../../redux/selectors/customfieldSelector";
import IconEdit from "../../Icons/IconEdit";
import CustomTooltip from "../../Tooltip/Tooltip";
import DefaultTextField from "../../Form/TextField";
import clsx from "clsx";
import { getContrastYIQ } from "../../../helper/index";
import UpdateMatrixDialog from "../../../components/Matrix/updateMatrixDialog.cmp";

function MatrixColumn({
  classes,
  theme,
  field,
  handleUpdateCustomField,
  customfields,
  rowData,
  permission,
  placeholder,
}) {
  const [matrixState, setMatrixState] = useState(null);

  const data = customfields[field.fieldId];
  let val = "Select";
  const selectedField = field;
  let columnObject = {};
  if (selectedField && selectedField.fieldData.data) {
    let indexing = "ABCDEFGH";
    let rowValueIndex = indexing.indexOf(
      selectedField.fieldData.data[selectedField.fieldData.data.length - 1].cellName.slice(0, 1)
    );
    let columnValueIndex = selectedField.fieldData.data[
      selectedField.fieldData.data.length - 1
    ].cellName.slice(1, 2);
    columnObject = data.settings.matrix[rowValueIndex][columnValueIndex - 1];
    val = Array.isArray(selectedField.fieldData.data)
      ? selectedField.fieldData.data[selectedField.fieldData.data.length - 1].cellName
      : "Select";
  }

  const handleAddAssessmentDialog = () => {
    const assessmentMatrixValues = field;
    setMatrixState({
      assessmentDialogOpen: true,
      selectedRisk: rowData,
      selectedMatrix: data.fieldId,
      assessmentMatrixValues: assessmentMatrixValues,
    });
  };

  return (
    <>
      <div
        data-rowClick="cell"
        className={classes.captionCustom}
        style={
          selectedField?.fieldData?.data?.length > 0
            ? {
                backgroundColor: columnObject.color,
                color: getContrastYIQ(columnObject.color),
              }
            : {
                color: `#202020`,
              }
        }
        onClick={event => permission && handleAddAssessmentDialog(true, event, rowData, data)}>
        <CustomTooltip helptext={val} placement="top">
          {val === "Select" /** no value select respect to each risk */ ? (
            <span
              key={rowData.id}
              style={{
                color: `#202020`,
                // textDecoration: "underline",
              }}>
              {val}
            </span>
          ) : (
            <span key={rowData.id}>{val}</span>
          )}
        </CustomTooltip>
      </div>
      {matrixState?.assessmentDialogOpen && (
        <UpdateMatrixDialog
          saveOption={(param1, param2) => {
            handleUpdateCustomField(matrixState.selectedRisk, param1, param2);
            setTimeout(() => {
              setMatrixState(null);
            }, 500);
          }}
          feature={"risk"}
          selectedMatrix={matrixState.selectedMatrix}
          open={matrixState.assessmentDialogOpen}
          closeAction={e => setMatrixState(null)}
          selectedValues={matrixState.assessmentMatrixValues?.fieldData?.data || []}
          assessMatrix={false}
        />
      )}
    </>
  );
}

MatrixColumn.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  rowData: {},
  handleUpdateCustomField: () => {},
  placeholder: false,
};
const mapStateToProps = state => {
  return {
    customfields: CustomfieldSelector(state),
  };
};
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, null)
)(MatrixColumn);
