import DropdownMenu from "../../Dropdown/DropdownMenu";
import CustomMenuList from "../../MenuList/CustomMenuList";
import CustomListItem from "../../ListItem/CustomListItem";
import BookmarkIcon from "../../Icons/BookmarkIcon";
import GroupByIcon from "../../Icons/GroupByIcon";
import AutoSizeIcon from "../../Icons/AutoSizeIcon";
import CollapseColIcon from "../../Icons/CollapseColIcon";
import CopyIcon from "../../Icons/CopyIcon";
import ResetIcon from "../../Icons/ResetIcon";
import React, { useEffect, useState } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import MenuIcon from "@material-ui/icons/Menu";
import AdvFilterIcon from "../../Icons/AdvFilterIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import columnSettingDropdownStyles from "./columnSettingDropdown.style";
import withStyles from "@material-ui/core/styles/withStyles";
import DefaultTextField from "../../Form/TextField";
import {
  deleteReportingFilter,
  updateReportingFilter,
} from "../../../redux/actions/reportingFilters";
import { connect, useDispatch, useSelector } from "react-redux";
import debounce from "lodash/debounce";
import CustomButton from "../../Buttons/CustomButton";
import { updateColumn } from "../../../redux/actions/columns";
import { updateTeamColumn } from "../../../redux/actions/columns";
import { compose } from "redux";
import searchQuery from "./searchQuery";
import WordWrapIcon from "../../Icons/WordWrapIcon";
import UnPlanned from "../../../Views/billing/UnPlanned/UnPlanned";
import { FormattedMessage } from "react-intl";
import { teamCanView } from "../../PlanPermission/PlanPermission";
import ReportFilterComponents from "../FilterComponents/reportFilterComponents";
import { grid } from "../gridInstance";
import {TRIALPERIOD} from '../../constants/planConstant';
function ColumnSettingDropdown({
  gridApi,
  anchorEl,
  anchorRef,
  closeAction,
  classes,
  theme,
  feature,
  groupType,
  columnPinDisabled,
  columnGroupingDisabled,
  columnFilterDisabled,
  columnWordWrapDisabled,
  intl,
  columns,
  customStatusList,
  columnChangeCallback,
  taskStatusGroup,
}) {
  const [value, setValue] = useState(0);
  const [searchValue, setSearchValue] = useState("");
  const state = useSelector(state => {
    return {
      reportingFilters: state.reportingFilters?.[feature],
      columns: state.columns,
    };
  });
  const dispatch = useDispatch();
  const { taskFilters, issueFilter, reportingFilters = {} } = state;

  const resetColumns = (obj = {}) => {
    const columnState = grid.grid.columnModel.getColumnState();
    const featureColumns = columnState.reduce((r, cv) => {
      const actualColumnObj = columns.find(c => c.columnKey == cv.colId);
      if (actualColumnObj) {
        const { aggFunc, colId, pivot, pivotIndex, ...rest } = actualColumnObj;
        const additionalProperties =
          anchorEl.column.column.colId == actualColumnObj.columnKey ? obj : {};
        r.push({
          ...actualColumnObj,
          ...cv,
          sort: null,
          sortIndex: null,
          rowGroup: false,
          pinned: null,
          columnId: actualColumnObj.id,
          ...additionalProperties,
        });
      }
      return r;
    }, []);
    // props.updateColumn(featureColumns, feature);
    updateTeamColumn(
      feature,
      groupType,
      featureColumns,
      res => {},
      fail => {
        console.log("api fail");
      }
    );
  };
  const handleUpdateColumn = (obj = {}) => {
    const columnState = grid.grid.columnModel.getColumnState();
    const featureColumns = columnState.reduce((r, cv) => {
      const actualColumnObj = window[`${feature}Columns`].find(c => c.columnKey == cv.colId);
      if (actualColumnObj) {
        const { aggFunc, colId, pivot, pivotIndex, ...rest } = actualColumnObj;
        const additionalProperties =
          anchorEl.column.column.colId == actualColumnObj.columnKey ? obj : {};
        r.push({
          ...actualColumnObj,
          ...cv,
          columnId: actualColumnObj.id,
          ...additionalProperties,
        });
      }
      return r;
    }, []);

    // props.updateColumn(featureColumns, feature);
    columnChangeCallback(featureColumns);
    updateTeamColumn(
      feature,
      groupType,
      featureColumns,
      res => {},
      fail => {
        console.log("api fail");
      }
    );
  };
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const applyFilter = obj => {
    setTimeout(() => {
      updateReportingFilter(obj, feature, dispatch);
      grid.grid.onFilterChanged();
    }, 0);
  };

  const externalFilterChanged = (options, type = "") => {
    const obj = { [anchorEl.column.column.colId]: { type: type, selectedValues: options } };
    // setSearchValue(e.target.value);
    applyFilter(obj);
  };

  useEffect(() => {
    const currentFilter = anchorEl && reportingFilters[anchorEl.column.column.colId];
    setSearchValue(currentFilter && currentFilter.query);
    return () => {
      setSearchValue("");
    };
  }, [anchorEl]);
  // handle column pinning
  const handlePin = (ele, direction) => {
    switch (direction) {
      case "left":
        grid.grid.columnModel.applyColumnState({
          state: [
            {
              colId: ele.column.colId,
              pinned: "left",
            },
          ],
        });
        break;
      case "right":
        grid.grid.columnModel.applyColumnState({
          state: [
            {
              colId: ele.column.colId,
              pinned: "right",
            },
          ],
        });
        break;
      case "clear":
        grid.grid.columnModel.applyColumnState({
          state: [
            {
              colId: ele.column.colId,
              pinned: null,
            },
          ],
        });
        break;
    }
    closeAction();
    handleUpdateColumn();
  };
  const handleWordWrap = (ele, wordWrapped) => {
    closeAction();
    const wordWrapObj = {
      wrapText: !wordWrapped,
      autoHeight: !wordWrapped,
    };
    setTimeout(() => {
      grid.grid.redrawRows();
    }, 0);

    handleUpdateColumn(wordWrapObj, anchorEl.column.column.colId);
  };
  //Auto size all columns
  const handleAutoSizeAllCol = skipHeader => {
    let allColumnIds = [];
    grid.grid.columnModel.columnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });
    grid.grid.columnModel.columnApi.autoSizeColumns(allColumnIds, skipHeader);
    closeAction();
    handleUpdateColumn();
    // const updatedObject = { ...obj, hide: obj.hide ? false : true, columnId: obj.id };
    // props.updateColumn([updatedObject]);
  };
  //Handle Reset cols
  const handleResetAllCol = () => {
    // grid.grid.columnModel.columnApi.resetColumnState();
    // grid.grid.columnModel.columnApi.resetColumnGroupState();
    let allColumnIds = [];
    grid.grid.columnModel.columnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });
    grid.grid.columnModel.columnApi.autoSizeColumns(allColumnIds, false);
    closeAction();
    resetColumns();
  };
  //Handle Auto
  const handleAutosizeSingleCol = col => {
    grid.grid.columnModel.columnApi.autoSizeColumns([col.column.colId], false);
    closeAction();
    handleUpdateColumn();
  };
  //Group By
  const handleGroupBy = (ele, isColGrouped) => {
    grid.grid.columnModel.applyColumnState({
      state: [
        {
          colId: ele.column.colId,
          rowGroup: !isColGrouped,
        },
      ],
    });
    closeAction();
    // handleUpdateColumn()
  };
  const clearFilter = () => {
    // switch (feature) {
    //   case 'task':
    //     deleteTaskFilter(anchorEl.column.column.colId, dispatch);
    //     return
    //     break
    //   case 'issue':
    //     deleteIssueFilter(anchorEl.column.column.colId, dispatch)
    //     return
    //     break
    // }
    deleteReportingFilter(anchorEl.column.column.colId, feature, dispatch);
    grid.grid.onFilterChanged();
    closeAction();
  };
  const isWordWrapped = anchorEl && anchorEl.column && anchorEl.column.column.colDef.wrapText;
  const isColumnGrouped = anchorEl && anchorEl.column && anchorEl.column.column.colDef.rowGroup;
  const pinnedType = anchorEl && anchorEl.column && anchorEl.column.column.colDef.pinned;
  const customFieldColumnWordWrapDisabled = [
    "phone",
    "people",
    "rating",
    "date",
    "dropdown",
    "filesAndMedia",
    "country",
  ];
  return (
    <DropdownMenu
      open={anchorEl !== null}
      closeAction={closeAction}
      anchorEl={anchorRef}
      size={"medium"}
      placement="bottom-start"
      disablePortal={false}
      id="columnSettingMenu"
      paperProps={
        value == 1 && anchorEl.column.column.colId == "status" && feature == "taskoverview" && {
          classes: {
            root: classes.paperRoot,
          },
        }
      }>
      {(!anchorEl.column.column.colDef.fieldType &&
        !columnFilterDisabled.includes(anchorEl && anchorEl.column.column.colId)) ||
      (anchorEl.column.column.colDef.fieldType &&
        !columnFilterDisabled.includes(anchorEl && anchorEl.column.column.colDef.fieldType)) ? (
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          fullWidth={true}
          classes={{ indicator: classes.tabIndicator }}
          style={{ minHeight: 43 }}>
          <Tab
            classes={{root: classes.tab, selected: classes.tabSelected}}
            className={classes.filterListTab}
            label={
              <MenuIcon
                className={classes.filterIcon}
                htmlColor={value == 0 ? theme.palette.icon.azure : theme.palette.icon.gray300}
              />
            }
          />

          <Tab
            classes={{root: classes.tab, selected: classes.tabSelected}}
            className={classes.optionListTab}
            label={
              <SvgIcon
                className={classes.optionsIcon}
                htmlColor={value == 1 ? theme.palette.icon.azure : theme.palette.icon.gray300}
                viewBox="0 0 24 24">
                <AdvFilterIcon />
              </SvgIcon>
            }
          />
        </Tabs>
      ) : null}
      {value == 0 ? (
        <CustomMenuList>
          {!columnPinDisabled.includes(anchorEl && anchorEl.column.column.colId) && (
            <CustomListItem
              subNav={true}
              popperProps={{ id: "childMenu" }}
              icon={<BookmarkIcon />}
              subNavRenderer={
                <>
                  <CustomListItem
                    isSelected={!pinnedType}
                    rootProps={{ onClick: e => handlePin(anchorEl.column, "clear", e) }}>
                    No Pin
                  </CustomListItem>
                  <CustomListItem
                    isSelected={pinnedType == "left"}
                    rootProps={{ onClick: e => handlePin(anchorEl.column, "left", e) }}>
                    Pin To Left
                  </CustomListItem>
                  {/* <CustomListItem
                    isSelected={pinnedType == "right"}
                    rootProps={{ onClick: e => handlePin(anchorEl.column, "right", e) }}>
                    Pin To Right
                  </CustomListItem> */}
                </>
              }>
              {" "}
              Pin Column
            </CustomListItem>
          )}
          {!columnGroupingDisabled.includes(anchorEl && anchorEl.column.column.colId) &&
            teamCanView("advanceSortAccess") && (
              <CustomListItem
                rootProps={{ onClick: () => handleGroupBy(anchorEl.column, isColumnGrouped) }}
                lineBreak={true}
                icon={<GroupByIcon />}>
                {isColumnGrouped ? "Un-Group by" : "Group By"}{" "}
                {anchorEl && anchorEl.column.displayName}
              </CustomListItem>
            )}
          {!columnWordWrapDisabled.includes(anchorEl && anchorEl.column.column.colId) &&
            !customFieldColumnWordWrapDisabled.includes(
              anchorEl && anchorEl.column.column.colDef.fieldType
            ) && (
              <CustomListItem
                rootProps={{ onClick: () => handleWordWrap(anchorEl.column, isWordWrapped) }}
                icon={<WordWrapIcon />}
                lineBreak={true}>
                {isWordWrapped ? "Disable Word Wrap" : "Word Wrap"}
              </CustomListItem>
            )}
          <CustomListItem
            rootProps={{ onClick: () => handleAutosizeSingleCol(anchorEl.column, false) }}
            icon={<AutoSizeIcon />}>
            Autosize This Column
          </CustomListItem>

          <CustomListItem
            // lineBreak={true}
            rootProps={{ onClick: () => handleAutoSizeAllCol(anchorEl.column, false) }}
            icon={<AutoSizeIcon />}
            lineBreak={true}>
            Autosize All Column
          </CustomListItem>
          <CustomListItem rootProps={{ onClick: () => handleResetAllCol() }} icon={<ResetIcon />}>
            Reset Columns
          </CustomListItem>
        </CustomMenuList>
      ) : null}
      {value == 1 &&
      ((!anchorEl.column.column.colDef.fieldType &&
        !columnFilterDisabled.includes(anchorEl && anchorEl.column.column.colId)) ||
        (anchorEl.column.column.colDef.fieldType &&
          !columnFilterDisabled.includes(anchorEl && anchorEl.column.column.colDef.fieldType))) ? (
        !teamCanView("advanceFilterAccess") ? (
          <div className={classes.unplannedMain}>
            <UnPlanned
              feature="premium"
              titleTxt={
                <FormattedMessage
                  id="common.discovered-dialog.premium-title"
                  defaultMessage="Wow! You've discovered a Premium feature!"
                />
              }
              boldText={intl.formatMessage({
                id: "common.discovered-dialog.list.custom-filter.title",
                defaultMessage: "Custom Filters",
              })}
              descriptionTxt={
                <FormattedMessage
                  id="common.discovered-dialog.list.custom-filter.label"
                  defaultMessage={"is available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."}
                  values={{TRIALPERIOD: TRIALPERIOD}}
                />
              }
              showBodyImg={false}
              showDescription={true}
            />
          </div>
        ) : (
          <div>
            {anchorEl && (
              <ReportFilterComponents
                feature={feature}
                data={anchorEl.column}
                customStatusList={customStatusList}
                taskStatusGroup={taskStatusGroup}
                handleFilterChange={externalFilterChanged}
              />

              // <FilterComponents data={anchorEl.column} handleFilterChange={externalFilterChanged} />
              // <IssueFilterComponents data={anchorEl.column} handleFilterChange={externalFilterChanged} />
            )}
            {/* <DefaultTextField
            // label="Search"
            fullWidth={true}
            defaultProps={{
              onChange: e => externalFilterChanged(e),
              id: "Search",
              placeholder: "Search",
              value: searchValue,
            }}
          /> */}
            <div className={classes.resetBtnContainer}>
              <CustomButton
                btnType="blue"
                variant="outlined"
                style={{
                  height: 35,
                  fontWeight: 400,
                  fontFamily: theme.typography.fontFamilyLato,
                  fontSize: "12px !important",
                }}
                onClick={clearFilter}>
                Clear
              </CustomButton>
            </div>
          </div>
        )
      ) : null}
    </DropdownMenu>
  );
}

const mapStateToProps = () => {
  return {};
};
ColumnSettingDropdown.defaultProps = {
  customStatusList: [],
  taskStatusGroup: [],
};
export default compose(
  // injectIntl,
  connect(mapStateToProps, { updateColumn, updateTeamColumn }),
  withStyles(columnSettingDropdownStyles, {
    withTheme: true,
  })
)(ColumnSettingDropdown);
