const columnSettingDropdownStyles = theme => ({
  tab: {
    minWidth: "unset",
    minHeight: "40px !important",
    flexGrow: 1,
  },
  tabSelected: {
    background: "unset !important",
    boxShadow: "unset !important",
  },
  optionListTab: {
    minWidth: 70,
    minHeight: 34,
  },
  filterListTab: {
    minWidth: 70,
    minHeight: 34,
  },
  filterIcon: {
    fontSize: "18px !important",
  },
  optionsIcon: {
    fontSize: "24px !important",
  },
  tabIndicator: {
    background: theme.palette.background.btnBlue,
  },
  filterTextField: {
    padding: "8px 10px",
  },
  resetBtn: {
    background: "#FFFFFF 0% 0% no-repeat padding-box",
    border: theme.palette.background.btnBlue,
    borderRadius: "4px",
    opacity: 1,
    height: "27px",
    width: "62px",
    fontSize: "12px",
    color: theme.palette.background.btnBlue,
    font: theme.palette.fontFamilyLato,
  },
  resetBtnContainer: {
    textAlign: "right",
    padding: "10px 5px",
    borderTop: "1px solid #e4e4e4",
  },
  unplannedMain: {
    padding: 15,
    display: "flex",
    alignItems: "center",
  },
  paperRoot: {
    background: theme.palette.background.default,
    width: 240,
  },
});

export default columnSettingDropdownStyles;
