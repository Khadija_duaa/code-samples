const tableStyles = (theme) => ({

  addTaskIcon: {
    fontSize: "18px !important",
    color: theme.palette.icon.azure,
    marginRight: 10
  },
  addText: {
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    color: theme.palette.text.lightGray,
  },
  addBtnCnt: {
    display: 'flex',
    alignItems: 'center',
    height: 40,
    padding: '0 5px',
    cursor: 'pointer'
  },
  columnGorupHeaderCnt: {
    height: "100%",
    width: "100%",
    padding: '0 16px',
    display: 'flex',
    alignItems: 'center'
  },
  columnSelectionDDCnt: {
    position: 'absolute',
    right: 22,
    zIndex: 11,
    top: 26
  }

})

export default tableStyles;