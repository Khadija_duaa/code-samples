import React, { useState, useEffect, useRef, useMemo, useCallback } from "react";
import "@ag-grid-community/core/dist/styles/ag-grid.css";
import "@ag-grid-community/core/dist/styles/ag-theme-alpine.css";
import { ClipboardModule } from "@ag-grid-enterprise/clipboard";
import { ExcelExportModule } from "@ag-grid-enterprise/excel-export";
import { MenuModule } from "@ag-grid-enterprise/menu";
import { RowGroupingModule } from "@ag-grid-enterprise/row-grouping";
import { SetFilterModule } from "@ag-grid-enterprise/set-filter";
import { MultiFilterModule } from "@ag-grid-enterprise/multi-filter";
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model'
import { AgGridReact } from "@ag-grid-community/react";
import isEqual from "lodash/isEqual";
import tableStyles from "./table.style";
import withStyles from "@material-ui/core/styles/withStyles";
import "./table.style.css";
import AddNewInput from "./addNewInput/addNewInput.cmp";
import AddNewInputWithProject from "./addNewInput/addNewInputWithProject.cmp";
import { useDispatch, useSelector } from "react-redux";
import { saveRowOrder, updateTaskCountAction } from "../../redux/actions/tasks";
import { calculateContentHeight } from "../../utils/common";
import AggregationComponents from "./AggregationComponents/AggregationComponents";
import HeaderCmp from "./HeaderComponent/headerComponent";
import { saveColumnPostion } from "../../redux/actions/grid";
import { updateColumn, updateColumnAction } from "../../redux/actions/columns";
import { grid } from "./gridInstance";
import { getContrastYIQ } from "../../helper/index";
import ColumnSelectionDropdown from "../Dropdown/SelectedItemsDropDown";
import searchQuery from "./ColumnSettingDropdown/searchQuery";
import isEmpty from "lodash/isEmpty";
import { getRowStyles } from "../../helper/listView/listView.helper";
import { ProjectMandatory } from "../../helper/config.helper";

let sortActive = false;
let filterActive = false;

const pinnedRowData = [
  {
    id: "60ae45f87fdee811389552e3",
    taskId: "",
    uniqueId: "-",
    taskTitle: "james task",
    description: "",
    isOwner: true,
    startDate: null,
    actualStartDate: null,
    dueDate: null,
    actualDueDate: null,
    createdDate: "2021-05-26T17:58:32",
    createdBy: "5e70707b7fdee13560330167",
    updatedDate: "2021-05-26T17:58:32",
    updatedBy: "5e70707b7fdee13560330167",
    priority: 3,
    assigneeList: [],
    usersInfo: [],
    unreadComments: [],
    ownerId: "5e70707b7fdee13560330167",
    status: 1626601625,
    statusTitle: "Pending Allocation",
    statusColor: "#0090ff",
    progress: 0,
    projectId: "f853b0e5a1e148d38c8787edf03b41ce",
    projectInfor: {
      projectId: "f853b0e5a1e148d38c8787edf03b41ce",
      projectName: "one project",
    },
    staredUserIds: [],
    userColors: [],
    slackChannelId: null,
    repeatInformation: "",
    repeatTask: null,
    parentId: null,
    taskOrder: 0,
    repeatDates: null,
    colorCode: "",
    dueDateString: null,
    startDateString: null,
    actualDueDateString: null,
    actualStartDateString: null,
    dueTime: null,
    startTime: null,
    actualDueTime: null,
    actualStartTime: null,
    totalAttachment: 0,
    totalComment: 0,
    isStared: false,
    totalUnreadComment: 0,
    estimatedEffortAddedBy: "",
    userTaskEfforts: [],
    effortMins: 0,
    effortHrs: 0,
    totalEffortsInMins: 0,
    totalEffort: "00:00:00",
    timeLogged: "00:00",
    customFieldData: [],
  },
];

const CustomTable = props => {
  const {
    children,
    defaultColDef,
    headerHeight,
    classes,
    createableProps,
    createable,
    frameworkComponents,
    tableHeight,
    isExternalFilterPresent,
    doesExternalFilterPass,
    data,
    type,
    gridProps,
    headerProps,
    onSelectionChanged,
    onRowGroupChange,
    theme,
    idKey,
    exportCallback,
    handleReadyFunction,
  } = props;

  const [rowOrder, setRowOrder] = useState([]);

  const dispatch = useDispatch();
  let gridApi = useRef(null);
  let gridColumnApi = useRef(null);
  const state = useSelector(state => {
    return {
      columns: state.columns,
      sidebarPannel: state.sidebarPannel
    };
  });
  const { columns } = state;

  const onGridReady = params => {
    gridApi.current = params.api;
    gridColumnApi = params.columnApi;

    grid.grid = params.api;
    handleReadyFunction()

    // setTimeout(() => {
    //
    // }, [])
    // params.api.showLoadingOverlay()
    // params.api.setRowsData(dataWithId);
    // updateColumnDef();

    // dispatch(saveTaskGridInstance(params.api))


    // fetch("https://test.ntaskmanager.com/api/tasks?records=0", { headers: { Authorization: "Bearer CAvfHmFA33WWHbqbfSr79g1r9umb8m29ieIqMYy0QWz1dOUyFfDxFMbaZlHTP8JFE4UNXPkPe_Fs2RwUh9RU0RZAdIaSq1Yl4eQgzQEi46jW60lRJpugPd-BXuvPM3oWg463zp60hPR6lqXINoE6R-KYOBG60-yx8GenR3UCbfwo5AZV4QhEKuSrCrhQPeb8fQLgEqV7Vm4UfHJQjD-63ojWVaf2m1lNqPic5aBHRfyuFjEdTcRAXoOKZ1H_o7pcOmxp-3km6l5aAwUE0-zoOjfgIJWCPmklbEODxp4MQjYMONBuvzkFYMIyOMleEmPsdRy02WWBM2VdNvw22DDNLzoCpR4DFaqt9xF2Wpac3lN1vjrx8dBQDrQgvp4DTByAEU6g-PNgQR6FKJ5jlObwwdqVPFIU9-1Uu74QWLxOm44oqRhQcVfxxEzIoM-ALizYrjvRmW1xgOJOfMBFHDOwCDdmtwqbN9ZDgaVebdyWouyX8ZtQ87hwY67Jfk0-16gtpN7_GZSkuo5Iqn-6Sl6Nel1MtseDb0pf_EmUFufbDCHNyT8iEvXQa7zxEBZmlNtAD1TZ3Nc4CPq4QDVCxndm_gBrKmDOiePgyq03-qmZnhlA5tnajaalJ3f0d7KhCpeozo0Im1K2d4VAp3oCSUFtlkjNJ4C8-k425JvjApMzNZ_kPx_u" } })
    //   .then(result => result.json())
    //   .then(data => {
    //     const updatedData = [...rowData, ...data.entity];
    //
    //     const dataWithId = updatedData.map(r => {
    //       return { ...r, id: r.taskId };
    //     });
    //     dataWithId.forEach(function(data, index) {
    //       data.id = index;
    //     });
    //
    //     setRowData(dataWithId);
    //     params.api.setRowData(dataWithId);
    //   });
  };
  // useEffect(() => {
  //   if(data.length && gridApi.current) {
  //     gridApi.current.refreshCells({ force: true })
  //   }
  // }, [data])

  // const updateColumnDef = () => {
  //   setColumnDefs(gridApi.current && gridApi.current.getColumnDefs());
  // };

  const columnRowGroupChanged = () => {
    handleUpdateColumn();
  };
  const ColumnHeaderRenderer = column => {
    return <HeaderCmp column={column} gridApi={gridApi.current} feature={type} {...headerProps} />;
  };
  const CustomHeaderGroup = column => {
    return (
      <div
        className={classes.columnGorupHeaderCnt}
        style={{
          background: column.columnGroup?.providedColumnGroup?.colGroupDef?.color || "#000000",
          color: getContrastYIQ(column.columnGroup?.providedColumnGroup?.colGroupDef?.color || '#000000'),
        }}>
        {column.displayName}
      </div>
    );
  };
  //on grid sort changed
  const onSortChanged = data => {
    //Save sort columns to backend
    let suppressRowDrag = sortActive;
    // || filterActive;
    gridApi.current.setSuppressRowDrag(suppressRowDrag);
    handleUpdateColumn();
  };

  const onFilterChanged = () => {
    filterActive = gridApi.current.isAnyFilterPresent();
    // let suppressRowDrag = sortActive
    // || filterActive;
    // gridApi.current.setSuppressRowDrag(suppressRowDrag);
    dispatch(updateTaskCountAction(gridApi.current.rowModel.rowsToDisplay.filter(r => !r.group).length));
  };
 

  const onRowDragMove = event => {
    if(!event.overNode) return
    const isDataGrouped = event.columnApi.getRowGroupColumns().length;
    let rowsData = data;
    let movingNode = event.node;
    let overNode = event.overNode;
    let overNodeField = overNode.parent.field;
    let movingData = movingNode.data;
    let overData = overNode.data;
    let rowNeedsToMove = !isEqual(movingNode, overNode);
    let groupCountry;

    function moveInArray(arr, fromIndex, toIndex) {
      let element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    if (rowNeedsToMove && !isDataGrouped) {
      let fromIndex = data.indexOf(movingData);
      let toIndex = data.indexOf(overData);
      let newStore = data.slice();
      moveInArray(newStore, fromIndex, toIndex);
      rowsData = newStore;
      gridApi.current.setRowData(newStore);
      gridApi.current.clearFocusedCell();
    }
  };

  const getRowNodeId = data => {
    return data.id;
  };
  // const columnRowGroupChanged = (data) => {
  //
  //   saveGridSettings(data.columnApi.getColumnState())
  // }
  //Row Drag End
  const onRowDragEnd = event => {
    if(!event.overNode){
      return
    }
    if (event.overNode.parent.field == "statusTitle") {
      return false;
    }
    const isDataGrouped = event.columnApi.getRowGroupColumns().length;
    let rowsData = data;
    let movingNode = event.node;
    let overNode = event.overNode;
    let overNodeField = overNode.parent.field == "project" ? "projectId" : overNode.parent.field;
    let movingData = movingNode.data;
    let overData = overNode.data;

    let needToChangeParent = movingData[overNodeField] !== overData[overNodeField];
    if (needToChangeParent && isDataGrouped) {
      movingData[overNodeField] = overData[overNodeField];
      if (overNodeField == "projectId") {
        movingData.projectInfor = overData.projectInfor;
      }
      gridApi.current.applyTransaction({ update: [movingData] });
      gridApi.current.clearFocusedCell();
      onRowGroupChange(movingData, overNodeField);
    } else {
      let arr = [];
      gridApi.current.forEachNode((rowNode, index) => {
        rowNode.data && arr.push(rowNode.data);
      });
      const newRowOrder = arr.map((ele, i) => {
        return { ItemId: ele[idKey], ItemOrder: i };
      });
      saveRowOrder(type, newRowOrder, dispatch);
      setRowOrder(newRowOrder);
    }
  };

  //Pinned Row Renderer
  const CustomPinnedRowRenderer = data => {
    return <AggregationComponents data={data}/>;
  };
  //Pinned Row Renderer
  const onGridResize = row => {
  };
  const createItemRenderer = row => {
    const { placeholder, id, btnText, addAction, disabled, ...rest } = createableProps;
    const isArchived = !isEmpty(searchQuery.quickFilters["Archived"]);
    return createable && ProjectMandatory() && id === "quickAddTask" ? /** only show new quick add input in tasklist view */
      (<AddNewInputWithProject
        disabled={isArchived ? isArchived : disabled}
        placeholder={placeholder}
        id={id}
        btnText={btnText}
        type={type}
        addAction={addAction}
        gridApi={gridApi.current}
        {...rest}
      />     
    ) : createable ? ( <AddNewInput
      disabled={isArchived ? isArchived : disabled}
      placeholder={placeholder}
      id={id}
      btnText={btnText}
      type={type}
      addAction={addAction}
      gridApi={gridApi.current}
      {...rest}
    />) : null;
  };
  //on any column drag/resize event called
  const onColumnDragEnd = data => {
    const isResized = data.target.classList.contains("ag-header-cell-resize");
    const isGroupOrderChanged = data.target.classList.contains(
      "ag-column-drop-horizontal-cell-drag-handle",
    );
    if (isResized || isGroupOrderChanged) {
      // In case column is resize
      handleUpdateColumn();
      return;
    }

    //In case column position is changed
    const updatedColumnOrders = data.columnApi.getColumnState().reduce(
      (r, cv, i) => {
        const selectedColumn = columns[type].find(col => col.columnKey == cv.colId);
        if (!headerProps.columnDragDisabled.includes(cv.colId)) {
          r.gridColumns.push({ columnId: selectedColumn.id, position: i });
          r.featuredColumns.push({ ...selectedColumn, position: i });
        }
        return r;
      },
      { gridColumns: [], featuredColumns: [] },
    );

    dispatch(updateColumnAction(updatedColumnOrders.featuredColumns));
    saveColumnPostion(updatedColumnOrders.gridColumns, type, dispatch);
  };

  const handleUpdateColumn = () => {
    const columnState = gridApi.current.columnModel.getColumnState();
    const featureColumns = columnState.reduce((r, cv) => {
      const actualColumnObj = columns[type].find(c => c.columnKey == cv.colId);
      if (actualColumnObj) {
        const { aggFunc, colId, pivot, pivotIndex, ...rest } = actualColumnObj;
        r.push({ ...actualColumnObj, ...cv, columnId: actualColumnObj.id });
      }
      return r;
    }, []);

    updateColumn(featureColumns, type, dispatch);
  };

  const handleRowSelection = e => {
    const selectedNodes = gridApi.current.getSelectedNodes();
    onSelectionChanged(selectedNodes);
  };

  const isRowGrouped = useMemo(() => {
    return columns[type].some(c => c.rowGroup);
  });
const extraContentHeight = localStorage.getItem("oldSidebarView") == "true" ? 58 : 55;
  return (
    <div
      className="ag-theme-alpine"
      style={{ position: "relative", height: calculateContentHeight() + extraContentHeight, flex: 1, overflow: 'hidden' }}>
      <span className={classes.columnSelectionDDCnt} style={{ top: isRowGrouped ? 74 : "" }}></span>
      <AgGridReact
        defaultColDef={defaultColDef}
        rowHeight={40}
        headerHeight={headerHeight}
        rowSelection="multiple"
        modules={[ClientSideRowModelModule, MultiFilterModule, SetFilterModule, RowGroupingModule, MenuModule, ExcelExportModule, ClipboardModule]}
        rowBuffer={25}
        rowGroupPanelShow={"onlyWhenGrouping"}
        rowData={data}
        frameworkComponents={{
          agColumnHeader: ColumnHeaderRenderer,
          customHeaderGroupComponent: CustomHeaderGroup,
          // customPinnedRowRenderer: CustomPinnedRowRenderer,
          fullWidthCellRenderer: createItemRenderer,
          ...frameworkComponents,
        }}
        //Row Drag
        // rowDragManaged={true}
        // suppressMoveWhenRowDragging={true}
        // suppressContextMenu={true}
        suppressCellSelection={true}
        animateRows={false}
        doesExternalFilterPass={doesExternalFilterPass}
        isExternalFilterPresent={isExternalFilterPresent}
        suppressDragLeaveHidesColumns={true}
        debounceVerticalScrollbar={false}
        enableMultiRowDragging={true}
        immutableData={true}
        getRowNodeId={getRowNodeId}
        onGridReady={onGridReady}

        onSortChanged={onSortChanged}
        onFilterChanged={onFilterChanged}
        onRowDragEnd={onRowDragEnd}
        onRowDragMove={onRowDragMove}
        onDragStopped={onColumnDragEnd}
        onSelectionChanged={handleRowSelection}
        onColumnRowGroupChanged={columnRowGroupChanged}
        suppressRowClickSelection={true}
        getRowStyle={function(params) {

          if (params.node.group) {
            return getRowStyles(params, theme, type);
          }
          if (params.node.rowPinned) {
            return { "font-weight": "bold" };
          }
        }}
        getRowClass={function(params) {
          if (params.node.group) {
            return "statusGroupRow";
          }
        }}
        // pinnedBottomRowData={pinnedRowData}
        pinnedTopRowData={pinnedRowData}
        groupDefaultExpanded={false}
        groupRowRendererParams={{
          innerRenderer: "groupRowInnerRenderer",
          checkbox: true,
        }}
        groupSelectsChildren={true}
        groupSelectsFiltered={true}
        fullWidthCellRenderer={"fullWidthCellRenderer"}
        isFullWidthCell={function(rowNode) {
          return rowNode.rowPinned == "top";
        }}
        {...gridProps}>
        {children}
      </AgGridReact>
    </div>
  );
};

CustomTable.defaultProps = {
  headerHeight: 30,
  classes: {},
  theme: {},
  createableProps: { placeholder: "Enter text here", createBtnText: "Add new" },
  createable: true,
  data: [],
  handleReadyFunction : () => {}
};
export default withStyles(tableStyles, { withTheme: true })(CustomTable);
