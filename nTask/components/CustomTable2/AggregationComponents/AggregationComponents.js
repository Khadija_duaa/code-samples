import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { useState } from "react";
import AggregationComponentsStyle from "../AggregationComponents/aggregationComponents.style";
import CustomTooltip from "../../Tooltip/Tooltip";
function AggregationComponents({ data, classes, theme }) {
  const { column } = data;

  const Components = {
    status: (
      <ul className={classes.statusList}>
        <CustomTooltip helptext={"Not Started - 6"} placement="top">
          <li
            className={`${classes.status} ${classes.list1}`}
            style={{ background: "#5185FC", flex: "1" }}></li>
        </CustomTooltip>
        <CustomTooltip helptext={"  In Progress - 3"} placement="top">
          <li
            className={`${classes.status} ${classes.list2}`}
            style={{ background: "#FCAE51", flex: "2" }}></li>
        </CustomTooltip>
        <CustomTooltip helptext={" In Review-3"} placement="top">
          <li
            className={`${classes.status} ${classes.list3}`}
            style={{ background: "#00D495", flex: "3" }}></li>
        </CustomTooltip>
        <CustomTooltip helptext={" Completed-1"} placement="top">
          <li
            className={`${classes.status} ${classes.list4}`}
            style={{ background: "#7CB147", flex: "4" }}></li>
        </CustomTooltip>
      </ul>
    ),
    startDate: (
      <CustomTooltip
        helptext={"90 Day left "}
        placement="top"
        style={{ color: theme.palette.common.white }}>
        <div className={classes.plannedFooterDown}>
          <span>Jun 22 - Sep 29</span>
          <div className={classes.innerBox}>
            <span className={classes.date}>98 d</span>
          </div>
          <div className={classes.leftSideColor}></div>
        </div>
      </CustomTooltip>
    ),
    actualStartDate: (
      <CustomTooltip
        helptext={"153 Day left "}
        placement="top"
        style={{ color: theme.palette.common.white }}>
        <div className={classes.actualFooterDown}>
          <span>Jul 01 - Nov 30</span>
          <div className={classes.innerBox}>
            <span className={classes.date}>153 d</span>
          </div>
        </div>
      </CustomTooltip>
    ),
    priority: (
      <ul className={classes.prioirtyList}>
        <CustomTooltip helptext={" Medium-6"} placement="top">
          <li
            className={`${classes.prioirty} ${classes.mediumPriority}`}
            style={{ flex: "1" }}></li>
        </CustomTooltip>
        <CustomTooltip helptext={" High-3"} placement="top">
          <li className={`${classes.prioirty} ${classes.highPriority}`} style={{ flex: "2" }}></li>
        </CustomTooltip>
        <CustomTooltip helptext={" Low-1"} placement="top">
          <li className={`${classes.prioirty} ${classes.lowPriority}`} style={{ flex: "3" }}></li>
        </CustomTooltip>
      </ul>
    ),
    project: (
      <div className={classes.mainProject}>
        <div className={classes.productStatus}>
          <label>Product Sa...</label>
        </div>
        <CustomTooltip
          helptext={
            <span className={classes.styleToolTip}>
              Product Marketing & Launch Campaign Product Development
            </span>
          }
          placement="top">
          <div className={classes.projectNumber}>
            <label>+2</label>
          </div>
        </CustomTooltip>
      </div>
    ),
    progress: (
      <div className={classes.mainProgress}>
        <div className={classes.innerProgress}>
          <div className={classes.progressGrayColor}>
          <div className={classes.progressGreenColor}></div>
          </div>
<label>40</label>
        </div>
        <div className={classes.Average}>
<label>Avg.</label>
</div>
      </div>
    ),
    totalEffort:(
      <div className={classes.totalEffort} >
<label>80 hrs</label>
<label>15 mins</label>
<div className={classes.textLogged}>
  <label>Sum</label>
  </div>
  </div>
    ),
    attachments:(
      <div className={classes.reuseFile}>
<label className={classes.editNum}>0</label>
<label className={classes.innerText}>Sum</label>
      </div>
    ),
   totalComment:(
      <div className={classes.reuseFile}>
      <label className={classes.editNum}>0</label>
      <label className={classes.innerText}>Avg.</label>
            </div>
    ),
    meetings:(
      <div className={classes.reuseFile}>
      <label className={classes.editNum}>0</label>
      <label className={classes.innerText}>min.</label>
            </div>
    ),
    issues:(
      <div className={classes.reuseFile}>
      <label className={classes.editNum}>0</label>
      <label className={classes.innerText}>Avg.</label>
            </div>
    ),
    risks:(
      <div className={classes.reuseFile}>
      <label className={classes.editNum}>0</label>
      <label className={classes.innerText}>Avg.</label>
            </div>
    )
  };
  const SelectedCmp = () => Components[column.colId];
  return <>{SelectedCmp()}

  </>;
}

export default withStyles(AggregationComponentsStyle, { withTheme: true })(AggregationComponents);
