const AggregationComponentsStyle = theme => ({
  statusList: {
    display: "flex",
    margin: 0,
    padding: 0,
  },
  status: {
    listStyleType: "none",
    height: "40px",
  },
  prioirtyList: {
    display: "flex",
    margin: 0,
    padding: 0,
  },
  prioirty: {
    listStyleType: "none",
    height: "40px",
  },
  highPriority: {
    backgroundColor: theme.palette.taskPriority.High,
  },
  mediumPriority: {
    backgroundColor: theme.palette.taskPriority.Medium,
  },
  lowPriority: {
    backgroundColor: theme.palette.taskPriority.Low,
  },
  plannedFooterDown: {
    height: "40px",
    background: "#7E7E7E",

    position: "relative",

    "&:hover $innerBox": {
      display: "block",
      width: "100%",
    },
    "& span": {

      paddingLeft: "16px",
      fontSize: "13px !important",
      color: theme.palette.common.white,

      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightExtraLight,
    },
  },
  actualFooterDown: {
    height: "40px",
    background: "#7E7E7E",

    position: "relative",


    "&:hover $innerBox": {
      width: "100%",
      display: "block",

    },
    "& span": {
      fontSize: "13px !important",
      color: theme.palette.common.white,

      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightExtraLight,
    },
  },
  innerBox: {
    height: " 40px",
    background: "#7E7E7E",
    display: "none",
    position: "absolute",
    top: "0px",


    "& span": {
      fontSize: "13px !important",
      color: theme.palette.common.white,

      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightExtraLight,
    },
  },
  leftSideColor: {
    width: "18px",
    height: "40px",
    background: "#49AEFC",

    position: "absolute",
    top: "0px",
    left: "0px",
  },
  mainProject: {
    display: "flex",
    alignItems: "center",
    marginTop: "9px",
  },
  productStatus: {
    height: "24px",
    display: "flex",
    alignItems: "center",
    background: "#FFFFFF",
    border: "1px solid #DDDDDD",
    borderRadius: "4px",
    padding: " 4px 6px",

    "& label": {
      fontSize: "13px !important",
      color: theme.palette.common.black,

      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightExtraLight,
    },
  },
  projectNumber: {
    height: "24px",
    background: "#E2F2FF",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "4px 6px",

    marginLeft: "4px",
    borderRadius: "4px",

    "& label": {
      fontSize: "13px !important",
      color: " #0090ff",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightExtraLight,
    },
  },
  styleToolTip: {
    height: "60px",
    fontSize: "13px !important",
    color: theme.palette.common.white,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,

  },
  mainProgress: {
    lineHeight: "0px",

    textAlign: "center",
    marginTop: "5px",
  },
  innerProgress: {
    display: "flex",
    alignItems: "center",

    "& label": {
      fontSize: "11px",
      marginLeft: "3px",
      color: theme.palette.common.black,

      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightExtraLight,


    },
  },
  progressGrayColor: {

    width: "100%",
    height: "6px",
    background: "#DDDDDD",
    borderRadius: "4px",
  },
  progressGreenColor: {
    width: "50%",
    height: "6px",
    background: "#28D168",
    borderRadius: " 4px 0px 0px 4px",


  },
  Average: {
    marginTop: " 10px",
    "& label": {
      fontSize: "12px",
      color: "#7E7E7E",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightExtraLight,

    },
  },
  totalEffort: {
    lineHeight: "20px",
    textAlign:"center",
    "& label": {
      marginRight: " 4px",
      fontSize: "13px !important",
      color: theme.palette.common.black,

      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightExtraLight,
    },
  },
  textLogged: {
lineHeight:"11px",
    "& label": {
      fontSize: "12px",
      color: "#7E7E7E",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightExtraLight,

    },
  },
  reuseFile:{
    display: "flex",
    flexDirection: "column",
    lineHeight:" 20px",
    textAlign:"center",
  
  },
  editNum:{
    fontSize: "13px !important",
    color: theme.palette.common.black,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
  },
  innerText:{
    fontSize: "12px",
    color: "#7E7E7E",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
  },
});

export default AggregationComponentsStyle;
