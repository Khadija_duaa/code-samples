import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import cloneDeep from "lodash/cloneDeep";
// import workspaceSetting from "../styles";
import menuStyles from "../../assets/jss/components/menu";
import CustomIconButton from "../Buttons/IconButton";
import SelectionMenu from "../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { FormattedMessage } from "react-intl";

class FiltersActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
    };
    this.handleDialogOpen = this.handleDialogOpen.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }

  handleDialogClose(event) {
    if (event) event.stopPropagation();
    this.setState({ open: false });
  }

  handleDialogOpen(event, placement) {
    event.stopPropagation();
    this.setState({ open: true });
  }

  handleDeleteFilter = (event) => {
    event.stopPropagation();
    const { filter, type, deleteUserFilter } = this.props;
    deleteUserFilter(
      filter,
      type,
      (resp) => { },
      (error) => { }
    );
  };

  makeThisFilterDefault = (event) => {
    event.stopPropagation();
    const { itemOrder, filter, type, SaveItemOrder } = this.props;
    let view = type.toLowerCase();
    let Item = cloneDeep(itemOrder);
    Item[`${view}Filter`] = { ...filter, defaultFilter: true };
    SaveItemOrder(
      Item,
      (resp) => {
        this.handleDialogClose();
      },
      (error) => {
        this.handleDialogClose();
      }
    );
  };

  render() {
    const { classes, theme, filter } = this.props;
    const { open } = this.state;
    return (
      <ClickAwayListener onClickAway={handleDialogClose}>
        <div>
          <CustomIconButton
            btnType="condensed"
            onClick={(event) => {
              this.handleDialogOpen(event);
            }}
            buttonRef={(node) => {
              this.anchorEl = node;
            }}
          >
            <MoreVerticalIcon
              htmlColor={theme.palette.secondary.medDark}
              style={{ fontSize: "24px" }}
            />
          </CustomIconButton>
          <SelectionMenu
            open={open}
            closeAction={this.handleDialogClose}
            placement="bottom-start"
            disablePortal={false}
            style={{ width: 200, borderRadius: 4 }}
            anchorRef={this.anchorEl}
            list={
              <List disablePadding={true}>
                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  onClick={(e) => this.handleDeleteFilter(e)}
                >
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="sidebar.filter.delete"
                        defaultMessage="Delete Filter"
                      />
                    }
                    classes={{
                      primary: classes.statusItemText,
                    }}
                  />
                </ListItem>
                {filter.defaultFilter ? null : (
                  <ListItem
                    button
                    disableRipple
                    classes={{ selected: classes.statusMenuItemSelected }}
                    // onClick={(e) => this.makeThisFilterDefault(e)}
                    onClick={(e) => {
                      this.handleDialogClose();
                      this.props.setDefaultfilter(this.props.filter, this.props.type)
                    }}
                  >
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="sidebar.filter.default"
                          defaultMessage="Make as default"
                        />
                      }
                      classes={{
                        primary: classes.statusItemText,
                      }}
                    />
                  </ListItem>
                )}
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

export default withStyles(menuStyles, {
  withTheme: true,
})(FiltersActionDropdown);
