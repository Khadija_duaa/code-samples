import React, { Component } from "react";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import sidebarStyles from "../../assets/jss/components/sidebar";
import Logo from "../../assets/images/nTask-Logo-Black-Copy.svg";
import CustomIconButton from "../Buttons/IconButton";
import { compose } from "redux";
import { connect } from "react-redux";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import RoundIcon from "@material-ui/icons/Brightness1";
import Typography from "@material-ui/core/Typography";
import { withRouter } from "react-router-dom";
import SvgIcon from "@material-ui/core/SvgIcon";
import PushPin from "../Icons/PushPin";
import cloneDeep from "lodash/cloneDeep";
import { withSnackbar } from "notistack";
import FiltersActionDropdown from "./FiltersActionDropdown";
import CustomAvatar from "../Avatar/Avatar";
import { Scrollbars } from "react-custom-scrollbars";
import { calculateSidebarHeight, generateUsername } from "../../utils/common";
import AddIcon from "@material-ui/icons/Add";
import CustomButton from "../Buttons/CustomButton";
import { SaveItemOrder } from "../../redux/actions/itemOrder";
import { setSideBarTheme } from "../../redux/actions/sidebar";
import {
  setAppliedFilters,
  deleteUserFilter,
  clearAllAppliedFilters,
  deleteUserCustomFilter,
  setDefaultfilter,
} from "../../redux/actions/appliedFilters";
import { UpdateProject } from "../../redux/actions/projects";
import StarredCheckBox from "../Form/Starred";
import GetUserPendingTask from "../../helper/getUserPendingTask";
import GetUserPendingIssues from "../../helper/getUserPendingIssues";
import GetProjectResources from "../../helper/getProjectResources";
import { generateActiveMembers } from "../../helper/getActiveMembers";
import InviteUserDialog from "../Dialog/InviteUserDialog";
import { canDo } from "../workspacePermissions/permissions";
import ReactResizeDetector from "react-resize-detector";
import { teamCanView } from "../PlanPermission/PlanPermission";
import IconButton from "../Buttons/IconButton";
import DownArrowIcon from "@material-ui/icons/ArrowDropDown";
import UpArrowIcon from "@material-ui/icons/ArrowDropUp";
import Basic from "../../assets/images/plans/Basic.svg";
import Business from "../../assets/images/plans/Business.svg";
import Enterprise from "../../assets/images/plans/Enterprise.svg";
import Premium from "../../assets/images/plans/Premium.svg";
import PlanConstant from "../constants/planConstant";
import UpgradePlan from "../../Views/billing/UpgradePlan/UpgradePlan";
import moment from "moment";
import Icon_Recommend from "../../assets/images/icon_recommend.svg";
import RecommendDialog from "../Dialog/RecommendDialog";
import { showUpgradePlan } from "../../redux/actions/userBillPlan";
import { FormattedMessage, injectIntl } from "react-intl";
import { updateTaskFilter, deleteTaskFilter, clearTaskFilter } from "../../redux/actions/tasks";
import { updateIssueFilter, deleteIssueFilter, clearIssueFilter } from "../../redux/actions/issues";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import nTaskLogo from "../../assets/images/nTask-logo32x32.png";
import clsx from "clsx";
import DefaultSwitch from "../Form/Switch";
import { showHideSideBar } from "../../redux/actions/sidebarPannel";

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      memberCntHeight: `${window.outerHeight - window.outerHeight / 2 - 40 - 70}px`,
      allMembers: [],
      inviteDialog: false,
      activeMemberFilter: "",
      planExpanded: true,
      openRecommend: false,
    };
  }
  componentDidMount() {
    if (!localStorage.getItem("sidebarMode")) {
      localStorage.setItem("sidebarMode", "dark");
    }
    const sidebarMode = localStorage.getItem("sidebarMode");
    this.props.setSideBarTheme(sidebarMode);

    this.setAllMembers();

    this.setDefaultFilter();
  }

  componentDidUpdate(prevProps, prevState) {
    const { allMembers } = this.props.profile.data.member;
    if (prevProps.match.params.view !== this.props.match.params.view) {
      this.setDefaultFilter();
    } else if (
      JSON.stringify(allMembers) !== JSON.stringify(prevProps.profile.data.member.allMembers)
    ) {
      this.setAllMembers();
    }
  }

  setAllMembers = () => {
    const allMembers = generateActiveMembers();
    let members = allMembers
      ? allMembers
          .filter(m => !m.isDeleted)
          .map(x => {
            return {
              id: x.userId,
              value: {
                name: x.fullName,
                avatar: x.imageUrl,
                email: x.email,
                fullName: x.fullName,
                isOnline: x.isOnline,
                isOwner: x.isOwner,
                isActive: x.isActive,
              },
            };
          })
      : [];
    this.setState({ allMembers: members });
  };

  setDefaultFilter = () => {
    let view = this.getViewType();
    const defaultFilter = view.viewFilter.filter(filter => {
      return filter.defaultFilter;
    })[0];
    if (defaultFilter) {
      this.props.setAppliedFilters(defaultFilter, view.viewType);
    }
  };

  renderThumb = ({ style, ...props }) => {
    const thumbStyle = {
      backgroundColor: this.props.theme.palette.background.dark,
    };
    return <div style={{ ...style, ...thumbStyle }} {...props} />;
  };
  getViewType = () => {
    const { itemOrder, match } = this.props;
    let viewType = match.params.view;
    let viewFilter = [];
    switch (viewType) {
      case "projects":
        viewType = `Project`;
        viewFilter =
          itemOrder.userProjectFilter &&
          itemOrder.userProjectFilter.length &&
          itemOrder.userProjectFilter[0].projectFilter
            ? itemOrder.userProjectFilter[0].projectFilter
            : [];
        break;
      case "meetings":
        viewType = `Meeting`;
        viewFilter =
          itemOrder.userMeetingFilter &&
          itemOrder.userMeetingFilter.length &&
          itemOrder.userMeetingFilter[0].meetingFilter
            ? itemOrder.userMeetingFilter[0].meetingFilter
            : [];
        break;
      case "issues":
        viewType = `issue`;
        viewFilter = [];
        break;
      case "risks":
        viewType = `Risk`;
        viewFilter =
          itemOrder.userRiskFilter &&
          itemOrder.userRiskFilter.length &&
          itemOrder.userRiskFilter[0].riskFilter
            ? itemOrder.userRiskFilter[0].riskFilter
            : [];
        break;
      case "tasks":
        viewType = `task`;
        viewFilter = [];
        break;
      default:
        viewType = ``;
        viewFilter = [];
    }
    return { viewType, viewFilter };
  };
  handleInviteDialogClose = () => {
    this.setState({ inviteDialog: false });
  };
  handleInviteDialogOpen = () => {
    this.setState({ inviteDialog: true });
  };
  handleRecommendDialogClose = () => {
    this.setState({ openRecommend: false });
  };
  handleRecommendDialogOpen = () => {
    this.setState({ openRecommend: true });
  };
  handleChange = (event, name, project) => {
    // need to change here
    event.stopPropagation();
    let isMark = event.target.checked;
    let obj = {
      Id: project.id,
      MarkStar: isMark,
    };
    this.props.UpdateProject(obj);
  };
  handleFavProjectClick = (e, projectId) => {
    this.props.history.push({
      pathname: "/projects",
      search: `?projectId=${projectId}`,
    });
  };
  handleInviteMembers = param => {
    if (param == "team") {
      this.props.history.push({
        pathname: "/team-settings",
        state: "inviteMember",
      });
    } else {
      this.props.history.push({
        pathname: "/workspace-settings",
        state: "inviteMember",
      });
    }
  };
  handleFilterClick = filter => {
    let view = this.getViewType();
    if (view.viewType) this.props.setAppliedFilters(filter, view.viewType);
  };
  onResize = () => {
    this.setState({
      memberCntHeight: `${window.outerHeight - window.outerHeight / 2 - 40 - 70}px`,
    });
  };
  handleViewAllTasksClick = (e, userId) => {
    const { match, taskFilter, issueFilter } = this.props;
    const { activeMemberFilter } = this.state;
    let viewType = match.params.view;
    if (activeMemberFilter == userId) {
      this.setState({ activeMemberFilter: "" }, () => {
        this.props.clearAllAppliedFilters(null);
      });
      switch (viewType) {
        case "issues":
          this.props.deleteIssueFilter("assignee");
          break;
        case "tasks":
          this.props.deleteTaskFilter("assigneeList");
          break;
      
        default:
          break;
      }
    } else {
      switch (viewType) {
        case "issues": {
          if (issueFilter.assignee && issueFilter.assignee.selectedValues.includes(userId)) {
            this.props.deleteIssueFilter("assignee");
            return;
          }
          const filterObj = { assignee: { type: "", selectedValues: [userId] } };
          this.props.updateIssueFilter(filterObj);
          break;
        }
        case "tasks": {
          if (taskFilter.assigneeList && taskFilter.assigneeList.selectedValues.includes(userId)) {
            this.props.deleteTaskFilter("assigneeList");
            return;
          }
          const filterObj = { assigneeList: { type: "", selectedValues: [userId] } };
          this.props.updateTaskFilter(filterObj);
          break;
        }
        case "boards": {
          this.props.setAppliedFilters({ assignees: [userId], project: [] }, "Board");
          break;
        }
        case "risks": {
          this.props.setAppliedFilters({ owner: [userId], task: [] }, "Risk");
          break;
        }
        case "meetings": {
          this.props.setAppliedFilters(
            {
              participants: [userId],
              createdBy: [],
              task: [],
              status: ["Upcoming", "In Review", "Overdue"],
              date: { startDate: "", endDate: "" },
            },
            "Meeting"
          );
          break;
        }
        default:
          break;
      }
      this.setState({ activeMemberFilter: userId });
    }
  };
  handlePlanExpandClick = () => {
    this.setState((prevState, props) => {
      return {
        planExpanded: !prevState.planExpanded,
      };
    });
  };
  showUpgradeDialog = () => {
    let data = {
      show: true,
      mode: PlanConstant.UPGRADEPLANMODE,
    };
    if (window.dataLayer) {
      window.dataLayer.push({ event: "upgrade-premium-button-sidebar-click" });
    }
    this.props.showUpgradePlan(data);
  };
  getPlanImage = title => {
    switch (title) {
      case PlanConstant.FREEPLAN:
        return <img src={Basic} alt="Basic Plan" />;
        break;
      case PlanConstant.TRIALPLAN:
      case PlanConstant.PREMIUMTRIALPLAN:
      case PlanConstant.PREMIUMPLAN:
        return <img src={Premium} alt="Premium Plan" />;
        break;

      case PlanConstant.BUSINESSTRIALPLAN:
      case PlanConstant.BUSINESSPLAN:
        return <img src={Business} alt="Business Plan" />;
        break;
      case PlanConstant.ENTERPRISEPLAN:
        return <img src={Enterprise} alt="Enterprise Plan" />;
        break;
    }
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  getSubscriptionDetails = () => {
    const { teams, activeTeam } = this.props;
    if (activeTeam) {
      const subscriptionDetails = teams.find(t => t.companyId == activeTeam).subscriptionDetails;
      return subscriptionDetails;
    } else {
      return {};
    }
  };
  getPlanTitle = paymentPlanTitle => {
    if (paymentPlanTitle) {
      return PlanConstant.DisplayPlanName(paymentPlanTitle);
    } else {
      return "";
    }
  };
  //Return  Active filter according to the view
  getActiveFilter = () => {
    const { match } = this.props;
    switch (match.params.view) {
      case "tasks":
        return this.props.savedTaskFilters;
        break;
      case "issues":
        return this.props.savedIssueFilters;
        break;
    }
  };
  //Handle Apply Task Filter
  applyTaskFilter = filter => {
    const { taskFilter } = this.props;
    if (taskFilter.filterName === filter.filterName) this.props.clearTaskFilter();
    else this.props.updateTaskFilter(filter);
  };
  //Handle Apply Issues Filter
  applyIssueFilter = filter => {
    const { issueFilter } = this.props;
    if (issueFilter.filterName === filter.filterName) this.props.clearIssueFilter();
    else this.props.updateIssueFilter(filter);
  };

  applyFilter = filter => {
    const { match } = this.props;
    switch (match.params.view) {
      case "tasks":
        this.applyTaskFilter(filter);
        return;
        break;
      case "issues":
        this.applyIssueFilter(filter);
        return;
        break;
    }
  };
  render() {
    const {
      classes,
      theme,
      sidebar,
      projects,
      itemOrder,
      permissions,
      tasks,
      issues,
      profile,
      location,
      risks,
      appliedFilters,
      match,
      promoBar,
      notificationBar,
      companyInfo,
      loading,
      taskFilter,
      issueFilter,
    } = this.props;
    let cloneProject = cloneDeep(projects || []);
    const {
      inviteDialog,
      memberCntHeight,
      activeMemberFilter,
      planExpanded,
      openRecommend,
    } = this.state;
    let view = this.getViewType();

    const memberList = this.state.allMembers;
    const teamMemberList = profile.data.teamMember.filter(m => {
      return m.isActive && !m.isDeleted;
    });
    const starProjects = projects.filter(starProj => {
      return starProj.isStared == true;
    });
    const currentViewFilter = this.props.appliedFilters[this.getViewType().viewType] || {};

    const workspaceCheck = () => {
      if (location.pathname.indexOf("/teams/") == 0) {
        return false;
      } else if (location.pathname.indexOf("/team-settings") == 0) {
        return false;
      } else if (location.pathname.indexOf("/notifications") == 0) {
        return false;
      } else if (location.pathname.indexOf("/reports/") == 0) {
        return false;
      } else {
        return true;
      }
    };
    let currentUser = profile.data.teamMember.find(m => {
      return profile.data.userId == m.userId;
    });
    const backgroundColor = companyInfo ? { backgroundColor: companyInfo.backgroudColor } : null;
    const subscriptionDetails = this.getSubscriptionDetails();
    let { paymentPlanTitle = "", currentPeriondEndDate } = subscriptionDetails;
    let expMomentDate = moment(currentPeriondEndDate).format("MMM DD, YYYY");
    let panelTitle = this.getPlanTitle(paymentPlanTitle);
    const name = companyInfo ? companyInfo.brandName : "nTask";
    const assigneeList =
      view.viewType == "task"
        ? taskFilter.assigneeList
        : view.viewType == "issue"
        ? issueFilter.assignee
        : null;
    const taskLabelMulti = companyInfo?.task?.pName;
    const isOldSidebarVisible = localStorage.getItem('oldSidebarView') == 'true' ? true : false
    return (
      <React.Fragment>
        <RecommendDialog
          open={openRecommend}
          closeDialog={this.handleRecommendDialogClose}
          showSnackBar={this.showSnackBar}
        />
        <InviteUserDialog open={inviteDialog} closeDialog={this.handleInviteDialogClose} />
        <Drawer
          variant="permanent"
          style={{
            marginTop: promoBar || notificationBar ? 50 : "",
            height: "100vh",
          }}
          id="sidebar"
          PaperProps={{
            style: {
              background: sidebar.sidebarMode == "dark" ? "#111111" : null,
            },
          }}
          classes={{
            paper: classNames(classes.drawerPaper, !this.props.open && classes.drawerPaperClose),
          }}
          open={this.props.open}>
          <div
            className={companyInfo ? classes.toolbarOuterCntWhite : classes.toolbarOuterCnt}
            style={backgroundColor}>
            {loading ? (
              <div style={{ padding: 15 }}>
                <div
                  style={{
                    height: "100%",
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <div className={`loader ${classes.sideBarLoad}`}></div>
                </div>
              </div>
            ) : (
              <>
                {!this.props.open ? (
                  <img
                    src={nTaskLogo}
                    alt="Ntask_Logo"
                    style={{ position: "relative", left: "8px" }}
                  />
                ) : (
                  ""
                )}
                {companyInfo ? (
                  <img
                    style={{ width: 170, height: 45 }}
                    src={companyInfo.headerImageUrl}
                    alt="Ntask_Logo"
                  />
                ) : (
                  <img
                    src={Logo}
                    // style={{ width: "100%", height: "100%" }}
                    style={{ height: 40, top: 5 }}
                    alt="Ntask_Logo"
                    className={`${classes.sideBarLogoCntOpen} ${
                      this.props.open ? classes.sideBarLogoCntOpen : classes.sideBarLogoCntClose
                    }`}
                  />
                )}
              </>
            )}
          </div>
          {profile.data.loggedInTeam && workspaceCheck() ? (
            <>
              <div className={classes.allMembersCnt}>
                <div className="flex_center_space_between_row">
                  <Typography variant="caption" className={classes.allMembersHeading}>
                    <FormattedMessage id="sidebar.members.label" defaultMessage="MEMBERS" />
                  </Typography>
                  {canDo(permissions || {}, "accessManageMembers") ? (
                    <Typography
                      variant="caption"
                      className={
                        sidebar.sidebarMode == "light"
                          ? classes.viewAllHeadingDark
                          : classes.viewAllHeading
                      }
                      onClick={this.handleInviteMembers}>
                      <FormattedMessage id="common.action.view.label" defaultMessage="View all" />
                    </Typography>
                  ) : null}
                </div>
                <ReactResizeDetector handleHeight onResize={this.onResize} />
                <Scrollbars
                  renderThumbHorizontal={this.renderThumb}
                  renderThumbVertical={this.renderThumb}
                  autoHide
                  autoHeight
                  autoHeightMin={0}
                  autoHeightMax={memberCntHeight}>
                  <List>
                    {memberList.map(data => {
                      return (
                        <ListItem
                          key={data.id}
                          button
                          disableRipple={true}
                          style={
                            !this.props.open
                              ? { marginBottom: 13, justifyContent: "center", paddingTop: 3 }
                              : { marginBottom: 0, justifyContent: "start", paddingTop: 0 }
                          }
                          className={`${classes.memberListItem} ${
                            assigneeList && assigneeList.selectedValues.includes(data.id)
                              ? classes.selectedMember
                              : ""
                          }`}
                          classes={{ selected: classes.menuItemSelected }}
                          onClick={event => {
                            match.params.view !== "projects" &&
                              this.handleViewAllTasksClick(event, data.id);
                          }}>
                          {data.value && !this.props.open ? (
                            <CustomAvatar
                              otherMember={{
                                imageUrl: data.value.avatar,
                                firstName: data.value.fullName,
                                lastName: "",
                                email: data.value.email,
                                isOnline: data.value.isOnline,
                                isOwner: data.value.isOwner,
                              }}
                              size="xsmall"
                              darkBg={true}
                            />
                          ) : null}
                          <div
                            className={
                              this.props.open
                                ? `${classes.memberListItemInner}`
                                : `${classes.displayNone}`
                            }>
                            <p
                              className={
                                sidebar.sidebarMode == "light"
                                  ? classes.membersNameDark
                                  : classes.membersName
                              }
                              title={data.value.name}>
                              {data.value.name}
                            </p>

                            {!data.value.isActive ? (
                              <Typography variant="caption">
                                <FormattedMessage
                                  id="common.action.invite.label"
                                  defaultMessage="Invite sent"
                                />
                              </Typography>
                            ) : (
                              <Typography variant="caption" className={classes.userDocsDetails}>
                                {`${
                                  GetProjectResources(cloneProject, data.id).length
                                } ${this.props.intl.formatMessage({
                                  id: "project.label",
                                  defaultMessage: "Projects",
                                })}, `}
                                {`${
                                  GetUserPendingTask(tasks ? tasks : [], data.id).length
                                } ${taskLabelMulti || 'Tasks'}`}

                                {/*{`${GetUserPendingIssues(issues, data.id).length}*/}
                                {/* ${this.props.intl.formatMessage({*/}
                                {/*   id: "issue.label",*/}
                                {/*   defaultMessage: "Issues",*/}
                                {/* })} `}*/}
                                &nbsp;<FormattedMessage
                                  id="common.action.active.label"
                                  defaultMessage="active"
                                />
                              </Typography>
                            )}
                          </div>
                        </ListItem>
                      );
                    })}
                  </List>
                </Scrollbars>
                {canDo(permissions || {}, "inviteMembers") ? (
                  <div className={classes.inviteMemberBtnCnt}>
                    <CustomButton
                      btnType="dashedIcon"
                      style={{
                        background: "transparent",
                        width: "100%",
                        color: theme.palette.common.white,
                        borderRadius: 30,
                      }}
                      variant="contained"
                      onClick={this.handleInviteDialogOpen}>
                      <AddIcon htmlColor={theme.palette.secondary.light} fontSize="small" />
                      <FormattedMessage
                        id="common.action.invite.action"
                        defaultMessage="Invite Members"
                      />
                    </CustomButton>
                  </div>
                ) : null}
              </div>
              {starProjects.length > 0 ? (
                <div
                  className={
                    this.props.open ? `${classes.favProjectListCnt}` : `${classes.displayNone}`
                  }>
                  <Typography variant="caption" className={classes.favProjectsHeading}>
                    <FormattedMessage
                      id="project.favourite.label"
                      defaultMessage="FAVORITE PROJECTS"
                    />
                  </Typography>
                  <Scrollbars autoHide={true} autoHeight autoHeightMin={100} autoHeightMax={300}>
                    <List className={classes.sidebarList} disablePadding>
                      {starProjects.map(project => {
                        return (
                          <ListItem
                            button
                            disableRipple={true}
                            className={classes.sidebarListItem}
                            onClick={e => this.handleFavProjectClick(e, project.projectId)}>
                            <div className="flex_center_start_row">
                              <RoundIcon
                                className={
                                  sidebar.sidebarMode == "light"
                                    ? classes.projectColorIconDark
                                    : classes.projectColorIcon
                                }
                                htmlColor={project.colorCode == "" ? "#fff" : project.colorCode}
                              />
                              <Typography
                                variant="body2"
                                className={
                                  sidebar.sidebarMode == "light"
                                    ? classes.favProjectListItemTextDark
                                    : classes.favProjectListItemText
                                }>
                                {project.projectName}
                              </Typography>
                            </div>
                            <StarredCheckBox
                              checkboxStyles={{ padding: 0 }}
                              checked={true}
                              onChange={e => this.handleChange(e, project.projectId, project)}
                            />
                          </ListItem>
                        );
                      })}
                    </List>
                  </Scrollbars>
                </div>
              ) : null}
              {view.viewFilter.length && teamCanView("advanceFilterAccess") ? (
                <div className={this.props.open ? `${classes.savedFilterListCnt}` : `${classes.displayNone}`} >
                  <Typography variant="caption" className={classes.favProjectsHeading}>
                    <FormattedMessage id="filters.saved.label" defaultMessage="SAVED FILTERS" />
                  </Typography>
                  <List className={classes.sidebarList} disablePadding>
                    {view.viewFilter.map(filter => {
                      if(view.viewType == "Issue" || view.viewType == "task") return;
                      return (
                        <ListItem
                          key={filter.filterId}
                          button
                          disableRipple={true}
                          onClick={() => this.handleFilterClick(filter)}
                          className={`${classes.sidebarFilterListItem} ${currentViewFilter.filterId === filter.filterId
                            ? classes.selectedFilter
                            : ""
                            }`}>
                          {currentViewFilter.filterId === filter.filterId ? (
                            <RoundIcon
                              htmlColor={theme.palette.secondary.main}
                              className={classes.selectedValueIcon}
                            />
                          ) : null}
                          <Typography variant="body2" className={classes.savedFilterListItemText}>
                            {filter.filterName}
                          </Typography>
                          <div className="flex_center_start_row">
                            {filter.defaultFilter ? (
                              <SvgIcon
                                viewBox="0 0 8 11.562"
                                className={classes.pushPinIcon}
                                htmlColor={theme.palette.common.white}>
                                <PushPin />
                              </SvgIcon>
                            ) : null}
                            <FiltersActionDropdown
                              itemOrder={itemOrder}
                              filter={filter}
                              type={view.viewType}
                              deleteUserFilter={this.props.deleteUserFilter}
                              SaveItemOrder={this.props.SaveItemOrder}
                              setDefaultfilter={this.props.setDefaultfilter}
                            />
                          </div>
                        </ListItem>
                      );
                    })}
                  </List>
                </div>
              ) : null}
              {this.getActiveFilter() &&
              this.getActiveFilter().length &&
              teamCanView("advanceFilterAccess") ? (
                <div
                  className={
                    this.props.open ? `${classes.savedFilterListCnt}` : `${classes.displayNone}`
                  }>
                  <Typography variant="caption" className={classes.favProjectsHeading}>
                    <FormattedMessage id="filters.saved.label" defaultMessage="SAVED FILTERS" />
                  </Typography>
                  <List className={classes.sidebarList} disablePadding>
                    {this.getActiveFilter().map(filter => {
                      return (
                        <ListItem
                          key={filter.filterId}
                          button
                          disableRipple={true}
                          // onClick={() => this.handleFilterClick(filter)}
                          onClick={() => this.applyFilter(filter)}
                          className={clsx({
                            [classes.sidebarFilterListItem]: true,
                            [classes.selectedFilter]:
                              this.props[`${view.viewType.toLowerCase()}Filter`].filterId ===
                              filter.filterId,
                          })}>
                          {this.props[`${view.viewType.toLowerCase()}Filter`].filterId ===
                          filter.filterId ? (
                            <RoundIcon
                              htmlColor={theme.palette.secondary.main}
                              className={classes.selectedValueIcon}
                            />
                          ) : null}
                          <Typography variant="body2" className={classes.savedFilterListItemText}>
                            {filter.filterName}
                          </Typography>
                          <div className="flex_center_start_row">
                            {filter.defaultFilter ? (
                              <SvgIcon
                                viewBox="0 0 8 11.562"
                                className={classes.pushPinIcon}
                                htmlColor={theme.palette.common.white}>
                                <PushPin />
                              </SvgIcon>
                            ) : null}
                            <FiltersActionDropdown
                              itemOrder={itemOrder}
                              filter={filter}
                              type={view.viewType}
                              // deleteUserFilter={this.props.deleteUserFilter}
                              deleteUserFilter={this.props.deleteUserCustomFilter}
                              SaveItemOrder={this.props.SaveItemOrder}
                              setDefaultfilter={this.props.setDefaultfilter}
                            />
                          </div>
                        </ListItem>
                      );
                    })}
                  </List>
                </div>
              ) : null}
            </>
          ) : (
            <div className={classes.allMembersCnt}>
              <div className="flex_center_space_between_row">
                <div className={this.props.open ? `${classes.open}` : `${classes.displayNone}`}>
                  <Typography variant="caption" className={classes.allMembersHeading}>
                    <FormattedMessage id="sidebar.members.label" defaultMessage="MEMBERS" />
                  </Typography>
                  {currentUser.isOwner ? (
                    <Typography
                      variant="caption"
                      className={
                        sidebar.sidebarMode == "light"
                          ? classes.viewAllHeadingDark
                          : classes.viewAllHeading
                      }
                      onClick={() => this.handleInviteMembers("team")}>
                      <FormattedMessage id="common.action.view.label" defaultMessage="View all" />
                    </Typography>
                  ) : null}
                </div>
              </div>
              <ReactResizeDetector handleHeight onResize={this.onResize} />
              <Scrollbars
                renderThumbHorizontal={this.renderThumb}
                renderThumbVertical={this.renderThumb}
                autoHide
                autoHeight
                autoHeightMin={0}
                autoHeightMax={memberCntHeight}>
                <List>
                  {teamMemberList.map(data => {
                    return (
                      <ListItem key={data.userId} button disableRipple={true}>
                        <CustomAvatar
                          otherMember={{
                            imageUrl: data.imageUrl,
                            fullName: data.fullName,
                            lastName: "",
                            email: data.email,
                            isOnline: data.isOnline,
                            isOwner: data.isOwner,
                          }}
                          size="xsmall"
                          darkBg={true}
                          disableCard
                        />

                        <ListItemText
                          primary={data.fullName}
                          classes={{
                            primary:
                              sidebar.sidebarMode == "light"
                                ? classes.teamMembersNameDark
                                : classes.teamMembersName,
                          }}
                          style={this.props.open ? { display: "block" } : { display: "none" }}
                          title={!data.isActive ? `${data.fullName}(Invite sent)` : data.fullName}
                        />
                      </ListItem>
                    );
                  })}
                </List>
              </Scrollbars>
              {canDo(permissions || {}, "inviteMembers") ? (
                <div className={classes.inviteMemberBtnCnt}>
                  <CustomButton
                    btnType="dashedIcon"
                    style={{
                      background: "transparent",
                      width: "100%",
                      color: theme.palette.common.white,
                      borderRadius: 30,
                    }}
                    variant="contained"
                    onClick={this.handleInviteDialogOpen}>
                    <AddIcon htmlColor={theme.palette.secondary.light} fontSize="small" />
                    <FormattedMessage
                      id="common.action.invite.action"
                      defaultMessage="Invite Members"
                    />
                  </CustomButton>
                </div>
              ) : null}
            </div>
          )}

          <div className={classes.panel} style={{ height: "auto", bottom: 0 }}>
            {this.props.profile.data.activeTeam !== '4c027962d01e4bf1bcdd5e0486556edc' &&
            <div className={classes.newSidebarSwitchCnt}>
              <p>New Menu <span className={classes.betaTag}>beta</span></p>
              <div style={{ marginTop: -3 }}>
                <DefaultSwitch
                  checked={!isOldSidebarVisible}
                  onChange={event => {
                    this.props.showHideSideBar(!isOldSidebarVisible);
                    localStorage.setItem("oldSidebarView", !isOldSidebarVisible);
                    window.location.reload();
                  }}
                  color= 'dark'
                  size='small'

                  // value="oldSidebarView"
                />
              </div>
            </div>}
            <div className={classes.panelHeader} onClick={this.handlePlanExpandClick}>
              {planExpanded ? (
                <DownArrowIcon
                  className={classes.arrowIcon}
                  htmlColor={theme.palette.secondary.light}
                />
              ) : (
                <UpArrowIcon
                  className={classes.arrowIcon}
                  htmlColor={theme.palette.secondary.light}
                />
              )}
            </div>
            {planExpanded && (
              <div className={classes.panelBody}>
                <span
                  className={this.props.open ? `${classes.panelTitle}` : `${classes.displayNone}`}>
                  {" "}
                  <FormattedMessage id="sidebar.plan.label" defaultMessage="Team Plan" />
                </span>
                <div
                  className={classes.planCnt}
                  style={
                    !this.props.open
                      ? { justifyContent: "center" }
                      : { justifyContent: "space-between" }
                  }>
                  <span style={this.props.open ? { fontSize: "12px" } : { display: "none" }}>
                    {name} {panelTitle}
                  </span>

                  {this.getPlanImage(paymentPlanTitle)}
                </div>
                {PlanConstant.isPlanTrial(paymentPlanTitle) && (
                  <span
                    className={
                      this.props.open ? `${classes.planDate}` : `${classes.displayNone}`
                    }>{`${this.props.intl.formatMessage({
                    id: "common.ends-on.label",
                    defaultMessage: "Ends on",
                  })} ${expMomentDate}`}</span>
                )}
                {PlanConstant.isPlanNonPaid(paymentPlanTitle) && currentUser.isOwner && (
                  <CustomButton
                    btnType="blue"
                    variant="contained"
                    style={
                      this.props.open ? { marginBottom: 0, marginTop: 10 } : { display: "none" }
                    }
                    onClick={this.showUpgradeDialog}>
                    <FormattedMessage
                      id="common.action.upgrade.label"
                      defaultMessage="Upgrade Now"
                    />
                  </CustomButton>
                )}
              </div>
            )}
            {companyInfo ? null : (
              <div
                className={classes.recommend}
                onClick={this.props.toggleAction}
              >
                <CustomIconButton
                  // onClick={this.props.toggleAction}
                  btnType="transparent"
                  style={{ color: "white" }}>
                  {this.props.open ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                </CustomIconButton>
                {/* <img src={Icon_Recommend} alt="Recommend" style={{ marginRight: 5 }} />
                <span style={{ whiteSpace: "break-spaces" }}>
                  <FormattedMessage
                    id="sidebar.fotter.label"
                    defaultMessage="Get 2 GB Free Storage"
                  />
                </span> */}
              </div>
            )}
          </div>
        </Drawer>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    sidebar: state.sidebar,
    projects: state.projects.data,
    profile: state.profile,
    promoBar: state.promoBar.promoBar,
    itemOrder: state.itemOrder.data,
    tasks: state.tasks.data,
    risks: state.risks.data,
    issues: state.issues.data,
    appliedFilters: state.appliedFilters,
    teams: state.profile.data.teams,
    activeTeam: state.profile.data.activeTeam,
    permissions: state.workspacePermissions.data.workSpace,
    notificationBar: state.notificationBar.notificationBar,
    companyInfo: state.whiteLabelInfo.data,
    savedIssueFilters: state.issues.savedFilters || [],
    savedTaskFilters: state.tasks.savedFilters || [],
    taskFilter: state.tasks.taskFilter || {},
    issueFilter: state.issues.issueFilter || {},
    sidebarPannel: state.sidebarPannel,
  };
}
export default compose(
  injectIntl,
  connect(mapStateToProps, {
    setSideBarTheme,
    clearTaskFilter,
    setAppliedFilters,
    deleteUserFilter,
    deleteUserCustomFilter,
    SaveItemOrder,
    clearAllAppliedFilters,
    UpdateProject,
    showUpgradePlan,
    updateTaskFilter,
    deleteTaskFilter,
    setDefaultfilter,
    updateIssueFilter,
    deleteIssueFilter,
    clearIssueFilter,
      showHideSideBar
  }),
  withSnackbar,
  withRouter,
  withStyles(sidebarStyles, { withTheme: true })
)(Sidebar);
