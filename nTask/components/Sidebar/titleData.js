import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import TaskIcon from "../Icons/TaskIcon";
import MeetingsIcon from "../Icons/MeetingIcon";
import ProjectsIcon from "../Icons/ProjectIcon";
import TimesheetIcon from "../Icons/TimesheetIcon";
import IssuesIcon from "../Icons/IssueIcon";
import RiskIcon from "../Icons/RiskIcon";
import map from "lodash/map";
import { withStyles } from "@material-ui/core/styles";
import sidebarStyles from "../../assets/jss/components/sidebar";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import { connect } from "react-redux";
import SvgIcon from "@material-ui/core/SvgIcon";

class SideBarList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 1
    };
    this.GenerateListItem = this.GenerateListItem.bind(this);
    this.handleListItemClick = this.handleListItemClick.bind(this);
  }
  componentDidMount(prevProps, prevState) {
    
    const match = this.props.match.params.view;
    const viewsArray = [
      "projects",
      "tasks",
      "meetings",
      "timesheet",
      "issues",
      "risks"
    ];
    this.setState({ selectedIndex: viewsArray.indexOf(match) });
  }
  handleListItemClick(event, index) {
    const push = match => {
      this.props.history.push(match);
    };
    this.setState({ selectedIndex: index }, () => {
      if (index === 0) {
        push("/projects");
      } else if (index === 1) {
        push("/tasks");
      } else if (index === 2) {
        push("/meetings");
      } else if (index === 3) {
        push("/timesheet");
      } else if (index === 4) {
        push("/issues");
      } else {
        push("/risks");
      }
    });
  }
  GenerateListItem(classes) {
    const sidebarList = {
      Projects: (
        <SvgIcon viewBox="0 0 18 15.188" className={classes.sideBarIcon}>
          <ProjectsIcon />
        </SvgIcon>
      ),
      Tasks: (
        <SvgIcon viewBox="0 0 18 15.188" className={classes.sideBarIcon}>
          <TaskIcon />
        </SvgIcon>
      ),
      Meetings: (
        <SvgIcon viewBox="0 0 17.031 17" className={classes.sideBarIcon}>
          <MeetingsIcon />
        </SvgIcon>
      ),
      TimeSheet: (
        <SvgIcon viewBox="0 0 17 19.625" className={classes.sideBarIcon}>
          <TimesheetIcon />
        </SvgIcon>
      ),
      Issues: (
        <SvgIcon viewBox="0 0 16 17.375" className={classes.sideBarIcon}>
          <IssuesIcon />
        </SvgIcon>
      ),
      Risk: (
        <SvgIcon viewBox="0 0 18 15.75" className={classes.sideBarIcon}>
          <RiskIcon />
        </SvgIcon>
      )
    };
    return map(sidebarList, (Ele, key, arr) => {
      const itemIndex = Object.keys(arr).indexOf(key);
      const { sidebarMode } = this.props;
      return (
        <ListItem
          button
          selected={this.state.selectedIndex === itemIndex}
          onClick={event => this.handleListItemClick(event, itemIndex)}
          classes={{ selected: classes.itemSelected, root: classes.itemRoot }}
          disableRipple={true}
          key={itemIndex}
        >
          <ListItemIcon
            className={
              sidebarMode == "dark"
                ? classes.listItemIconDark
                : classes.listItemIcon
            }
          >
            {Ele}
          </ListItemIcon>
          <ListItemText
            primary={key}
            classes={{ primary: classes.listItemText }}
          />
        </ListItem>
      );
    });
  }

  render() {
    const { classes, theme } = this.props;
    return <React.Fragment>{this.GenerateListItem(classes)}</React.Fragment>;
  }
}
export default compose(
  withRouter,
  withStyles(sidebarStyles, { withTheme: true }),
  connect(
    null,
    { showLoading }
  )
)(SideBarList);
