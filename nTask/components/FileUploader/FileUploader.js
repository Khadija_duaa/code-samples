import React, { Component, useMemo, useState, useRef, useEffect } from 'react';
import '@uppy/core/dist/style.css'
import '@uppy/dashboard/dist/style.css'
import Dashboard from '@uppy/react/lib/Dashboard'
import Uppy from '@uppy/core'
import '@uppy/core/dist/style.css'
import '@uppy/webcam/dist/style.css'
const GoogleDrive = require('@uppy/google-drive')
const OneDrive = require('@uppy/onedrive')
const Dropbox = require('@uppy/dropbox')
const Url = require('@uppy/url')
const Webcam = require('@uppy/webcam')
const ImageEditor = require('@uppy/image-editor')
const Instagram = require('@uppy/instagram')
const Facebook = require('@uppy/facebook')
const Zoom = require('@uppy/zoom')
const XHRUpload = require('@uppy/xhr-upload')
const AwsS3 = require('@uppy/aws-s3')
const ms = require('ms')
import "./fileUploader.css";
import helper from "../../helper/index";

function FileUploader(props) {

    const uppy = useMemo(() => {
        return Uppy({
          allowMultipleUploads: true,
          locale: {
            strings: {
              dropPasteFiles: `Drop file here, paste or %{browseFiles}`
            }
          },
          restrictions: {
            maxFileSize: props.id === 'collaborationEditorToolbar' ? 500000 : null,
            maxNumberOfFiles: props.maxNumberOfFiles ? props.maxNumberOfFiles: 1,
            minNumberOfFiles: 1
          }
        })
          // .use(Webcam) // `id` defaults to "Webcam"
          // or
          // .use(Webcam, { id: 'MyWebcam' }) // `id` is… "MyWebcam"
          // .use(Url, {
          //   companionUrl:  `https://doc.ntaskmanager.com/`
          // })
          .use(XHRUpload, {
            endpoint: `${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL }/api/docsfileuploader/UploadMultipleDocsFileAmazonS3`,
            // endpoint: 'https://fm.naxxa.io/api/file/uploads',
            headers: {
              'Authorization': helper.getToken() || null
            },
            method: 'post',
            formData: true,
            bundle: true,
            fieldName: 'file',
          })
          // .use(AwsS3, {
          //   limit: 2,
          //   timeout: ms('1 minute'),
          //   companionUrl: `https://dev.naxxa.io/api/docsfileuploader/uploaddocsfileamazons3`
          // })
          .use(GoogleDrive, {
            companionUrl: 'https://doc.ntaskmanager.com/',
            headers: {
              'uppy-auth-token': helper.getToken() || null
            },
          })
          .use(OneDrive, {
            companionUrl: 'https://doc.ntaskmanager.com/',
          })
          .use(Dropbox, {
            companionUrl: 'https://doc.ntaskmanager.com/',
          })
          .use(Instagram, {
            companionUrl: 'https://doc.ntaskmanager.com/',
          })
          .use(Facebook, {
            companionUrl: 'https://doc.ntaskmanager.com/',
          })
          .use(Zoom, {
            companionUrl: 'https://doc.ntaskmanager.com/',
          })
          .use(ImageEditor, {
            id: 'ImageEditor',
            quality: 0.8,
            cropperOptions: { 
              viewMode: 1,
              background: false,
              autoCropArea: 1,
              responsive: true
            }
          })
      }, [])

    uppy.on('complete', (result) => {
        props.onComplete(result);
        uppy.reset()
    })
  
    useEffect(() => {
        return () => uppy.close()
      }, [])
      let defaultPlugin = props.customPlugin ? props.customPlugin : [ 'ImageEditor', 'GoogleDrive', 'OneDrive', 'Dropbox', 'Url', 'Webcam']

      return (
          <Dashboard
                uppy={uppy}
                plugins={defaultPlugin}
                {...props}
            />
      );
    };
  export default FileUploader;