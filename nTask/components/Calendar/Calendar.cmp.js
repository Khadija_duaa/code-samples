import React, { useEffect, useState, useRef } from "react";
import { Calendar, Views, momentLocalizer } from "react-big-calendar";
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import DefaultButton from "../Buttons/DefaultButton";
import Grid from "@material-ui/core/Grid";
import { FormattedMessage } from "react-intl";
import CustomIconButton from "../Buttons/IconButton";
import LeftArrow from "@material-ui/icons/ArrowLeft";
import RightArrow from "@material-ui/icons/ArrowRight";
import { withStyles } from "@material-ui/core/styles";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";
import "./calendar.css";
import CustomButton from "../Buttons/CustomButton";
import calendarStyles from "./Calendar.style";
import clsx from "clsx";
import { excludeOffdays, getCalendar } from "../../helper/dates/dates";

const localizer = momentLocalizer(moment);
const DragAndDropCalendar = withDragAndDrop(Calendar);
function CustomCalendar({
  data,
  theme,
  classes,
  EventRenderer,
  updateDates,
  updateView,
  monthView,
  weekView,
  dayView,

  ...props
}) {
  const [draggedEvent, setDraggedEvent] = useState(null);
  const [calendarData, setCalendarData] = useState([]);
  const [currentDate, setCurrentDate] = useState(new Date());
  const [activeView, setActiveView] = useState(Views.MONTH);
  const selectedView = useRef(Views.MONTH);

  //set Active View on calendar
  const handleSetActiveView = view => {
    setActiveView(Views[view]);
    selectedView.current = Views[view];
    if (updateView) updateView(Views[view]);
  };
  useEffect(() => {
    setCalendarData(data);
  }, [data]);
  useEffect(() => {
    return () => {
      if (updateView) updateView('month');
    }
  }, []);

  const handleDragStart = event => {
    setDraggedEvent(event);
  };
  const onDropFromOutside = ({ start, end, allDay }) => {
    const event = {
      id: draggedEvent.id,
      title: draggedEvent.title,
      start,
      end,
      allDay: allDay,
    };

    setDraggedEvent(null);
    moveEvent({ event, start, end });
  };

  const moveEvent = ({ event, start, end, isAllDay: droppedOnAllDaySlot }) => {
    let allDay = event.allDay;

    if (!event.allDay && droppedOnAllDaySlot) {
      allDay = true;
    } else if (event.allDay && !droppedOnAllDaySlot) {
      allDay = false;
    }

    const nextEvents = calendarData.map(existingEvent => {
      return existingEvent.id == event.id
        ? { ...existingEvent, start, end, allDay }
        : existingEvent;
    });
    const updatedEvent = { ...event, start, end, allDay };
    setCalendarData(nextEvents);
    updateDates(updatedEvent);
  };

  const resizeEvent = ({ event, start, end }) => {
    const nextEvents = calendarData.map(existingEvent => {
      return existingEvent.id == event.id ? { ...existingEvent, start, end } : existingEvent;
    });
    const updatedEvent = { ...event, start, end };
    setCalendarData(nextEvents);
    updateDates(updatedEvent);
  };
  const newEvent = _event => { };


  const Toolbar = toolbar => {
    const goToBack = () => {
      if (selectedView.current === Views.MONTH) {
        toolbar.date.setMonth(toolbar.date.getMonth() - 1);
        toolbar.onNavigate("prev");
        setCurrentDate(toolbar.date);
      } else if (selectedView.current === Views.WEEK) {
        toolbar.date.setDate(toolbar.date.getDate() - 7);
        toolbar.onNavigate("prev");
        setCurrentDate(toolbar.date);
      } else if (selectedView.current === Views.DAY) {
        toolbar.date.setDate(toolbar.date.getDate() - 1);
        toolbar.onNavigate("prev");
        setCurrentDate(toolbar.date);
      }
    };

    const goToNext = () => {
      if (selectedView.current === Views.MONTH) {
        toolbar.date.setMonth(toolbar.date.getMonth() + 1);
        toolbar.onNavigate("next");
        setCurrentDate(toolbar.date);
      } else if (selectedView.current === Views.WEEK) {
        toolbar.date.setDate(toolbar.date.getDate() + 7);
        toolbar.onNavigate("next");
        setCurrentDate(toolbar.date);
      } else if (selectedView.current === Views.DAY) {
        toolbar.date.setDate(toolbar.date.getDate() + 1);
        toolbar.onNavigate("next");
        setCurrentDate(toolbar.date);
      }
    };

    const goToCurrent = () => {
      const now = new Date();
      toolbar.date.setDate(now.getDate());
      toolbar.date.setMonth(now.getMonth());
      toolbar.date.setYear(now.getFullYear());
      // DefaultButton;
      toolbar.onNavigate("current");
      setCurrentDate(toolbar.date);
    };

    return (
      <div className={classes.calendarToolbar}>
        <Grid container>
          <Grid item>
            <DefaultButton
              text={<FormattedMessage id="common.action.today.label" defaultMessage="Today" />}
              buttonType="Plain"
              onClick={goToCurrent}
            />
          </Grid>
          <Grid item classes={{ item: classes.NextBackBtnCnt }}>
            <CustomIconButton btnType="condensed" onClick={goToBack}>
              <LeftArrow
                nativeColor={theme.palette.secondary.dark}
                classes={{ root: classes.navigationArrow }}
              />
            </CustomIconButton>
            <span className={classes.toolbarCurrentDate}>{toolbar.label}</span>
            <CustomIconButton btnType="condensed" onClick={goToNext}>
              <RightArrow
                nativeColor={theme.palette.secondary.dark}
                classes={{ root: classes.navigationArrow }}
              />
            </CustomIconButton>
          </Grid>
        </Grid>
        <div className={classes.calendarViewBtnCnt}>
          {monthView && (
            <CustomButton
              variant="outlined"
              btnType={"gray"}
              className={clsx({ [classes.selectedView]: selectedView.current === Views.MONTH })}
              onClick={() => handleSetActiveView("MONTH")}>
              Month
            </CustomButton>
          )}
          {weekView && (
            <CustomButton
              variant="outlined"
              btnType={"gray"}
              className={clsx({ [classes.selectedView]: selectedView.current === Views.WEEK })}
              onClick={() => handleSetActiveView("WEEK")}>
              Week
            </CustomButton>
          )}
          {dayView && (
            <CustomButton
              variant="outlined"
              btnType={"gray"}
              className={clsx({ [classes.selectedView]: selectedView.current === Views.DAY })}
              onClick={() => handleSetActiveView("DAY")}>
              Day
            </CustomButton>
          )}
        </div>
      </div>
    );
  };
  const OffDayRenderer = (props, selected) => {
    console.log(props, selected)
    return (
      <div style={{ width: '100%' }}>
        test
      </div>
    );
  };
  let components = {
    toolbar: Toolbar,
    event: EventRenderer,
    month: {
      dateHeader: ({ date, label }) => {
        return (
          <button type="button" className={`rbc-button-link ${!excludeOffdays(date, calendarWorkingdays) ? 'highlightWeekend' : ''}`} role="cell">{label}</button>
        );
      }
    }
    // dateCellWrapper: OffDayRenderer

  };
  // highligh off days
  const calendarWorkingdays = getCalendar({}) || null;
  const customDayPropGetter = (date) => {
    if (!excludeOffdays(date, calendarWorkingdays))
      return { className: 'off-day' };
    else return {};
  };

  return (
    <div className={"calendarComponent"}>
      <DragAndDropCalendar
        selectable
        localizer={localizer}
        components={components}
        dayPropGetter={customDayPropGetter}
        step={15}
        events={calendarData}
        onEventDrop={moveEvent}
        resizable
        onEventResize={resizeEvent}
        showMultiDayTimes={true}
        onSelectSlot={newEvent}
        onDragStart={handleDragStart}
        defaultView={Views.MONTH}
        view={activeView}
        defaultDate={currentDate}
        popup={true}
        onDropFromOutside={onDropFromOutside}
        {...props}
      />
    </div>
  );
}
CustomCalendar.defaultProps = {
  updateView: () => { },
  monthView: false,
  dayView: false,
  weekView: false,

};

export default withStyles(calendarStyles, { withTheme: true })(CustomCalendar);
