const calendarStyles = theme => ({
  calendarToolbar: {
    marginBottom: 20,
    display: "flex",
    justifyContent: "space-between",
  },
  calendarViewBtnCnt: {
    display: "flex",
    "& button": {
      marginLeft: 10,
    },
  },
  navigationArrow: {
    fontSize: "28px !important",
  },
  toolbarCurrentDate: {
    fontSize: "18px !important",
    fontWeight: 500,
    verticalAlign: "middle",
    display: "inline-block",
    width: 160,
    textAlign: "center",
  },
  NextBackBtnCnt: {
    margin: "4px 0 0 20px",
  },
  recurrenceIcon: {
    fontSize: "23px !important",
    position: "absolute",
    right: 5,
    top: 1,
    background: "white",
    padding: 3,
    border: "1px solid #cecece",
    borderRadius: 15,
  },
  repeatOccuranceStyle: {
    padding: "6px !important",
    "&:hover": {
      color: "white !important",
      background: "#00CC90 !important",
    },
  },
  selectedView: {
    background: `${theme.palette.background.green} !important`,
    color: "white",
    cursor: "pointer",
    border: `1px solid ${theme.palette.background.green}`
  },
});

export default calendarStyles;
