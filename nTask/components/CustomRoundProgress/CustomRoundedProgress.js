import * as React from 'react';
import { CircularProgress, Typography } from '@material-ui/core';
import { withStyles } from "@material-ui/core/styles";

function CustomRoundedProgress(props) {
  return (
    <div sx={{ position: 'relative', display: 'inline-flex' }}>
    <CircularProgress  variant="determinate"  {...props} />
    <div
      style={{
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Typography variant="caption" component="div" color="text.secondary">
        {`${Math.round(props.value)}%`}
      </Typography>
    </div>
  </div>

  );
}

export default withStyles({ withTheme: true })(CustomRoundedProgress);
