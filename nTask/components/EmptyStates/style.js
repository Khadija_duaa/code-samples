const emptyStateStyles = theme => ({
    emptyStateIcon: {
        fontSize: "75px !important",
        marginBottom: 20
    },
    emptyStateIconDocument: {
        fontSize: "100px !important",
        // marginBottom: 20
    },
    innerCnt:{
        width: 480,
        textAlign: "center"
    },
    emptyStateHeading:{
        marginBottom: 15,
        fontWeight: 700,
        fontFamily: theme.typography.fontFamilyLato,
        fontSize: "20px !important"
    },
    workspaceIcon:{
        fontSize: "64px !important",
        marginBottom: 20
      },
      content: {
        fontWeight: 500,
        fontFamily: theme.typography.fontFamilyLato,
        fontSize: "14px !important"
      }
})

export default emptyStateStyles;