import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import SvgIcon from "@material-ui/core/SvgIcon";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import TimerIcon from "@material-ui/icons/Timer";
import MessageIcon from "@material-ui/icons/Message";
import emptyStateStyles from "./style";
import CustomIconButton from "../Buttons/CustomIconButton";
// import TaskIcon from "../Icons/TaskIcon";
import TaskIcon from "../../assets/images/taskEmptyState.svg";
import MilestoneIcon from "../../assets/images/milestone_emptyState_icon.svg";
import MeetingsIcon from "../Icons/MeetingIcon";
import ProjectsIcon from "../Icons/ProjectIcon";
import IssuesIcon from "../Icons/IssueIcon";
import RiskIcon from "../Icons/RiskIcon";
import DefaultDialog from "../Dialog/Dialog";
import AddNewTaskForm from "../../Views/AddNewForms/addNewTaskFrom";
import AddNewProject from "../../Views/AddNewForms/AddNewProjectForm";
import AddIssueForm from "../../Views/AddNewForms/AddNewIssueForm";
import AddRiskForm from "../../Views/AddNewForms/AddNewRiskForm";
import AddMeetingForm from "../../Views/AddNewForms/AddMeetingForm";
import SearchIcon from "../Icons/SearchIcon";
import ArchieveIcon from "../Icons/ArchiveIcon";
import AddWorkspaceForm from "../Header/addWorkSpaceForm";
import WorkspaceIcon from "../Icons/WorkspaceIcon";
import IllustrationDocuments from "../Icons/illustrationDocuments";
import NoConversationImg from "../../assets/images/conversation-empty-state-icon.svg";
import Hotkeys from "react-hot-keys";

class EmptyState extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectDialog: false,
      taskDialog: false,
      issueDialog: false,
      riskDialog: false,
      meetingDialog: false,
      newWorkspaceDialog: false,
    };
  }

  handleDialogClose = (event, name) => {
    this.setState({ [name]: false });
  };

  handleNewWorkspaceDialogClose = (event, name) => {
    this.setState({ newWorkspaceDialog: false });
  };

  handleDialog = (event, name) => {
    this.setState({ [name]: true });
  };

  render() {
    const { classes, theme, clickAction, screenType, message, button, heading, style } = this.props;
    const {
      projectDialog,
      taskDialog,
      issueDialog,
      meetingDialog,
      riskDialog,
      newWorkspaceDialog,
    } = this.state;

    return (
      <Grid container direction="column" justify="center" alignItems="center">
        <Grid item style={style} classes={{ item: classes.innerCnt }}>
          {screenType == "milestone" ? (
            <img src={MilestoneIcon} />
          ) : screenType == "task" ? (
            <img src={TaskIcon} />
          ) : // <SvgIcon
          //   viewBox="0 0 18 15.188"
          //   classes={{ root: classes.emptyStateIcon }}
          //   htmlColor={theme.palette.secondary.light}
          // >
          //   <TaskIcon />
          // </SvgIcon>
          screenType == "newWorkspace" ? (
            <SvgIcon
              viewBox="0 0 32 31"
              htmlColor={theme.palette.secondary.light}
              className={classes.workspaceIcon}>
              <WorkspaceIcon />
            </SvgIcon>
          ) : screenType == "project" ? (
            <SvgIcon
              viewBox="0 0 18 15.188"
              classes={{ root: classes.emptyStateIcon }}
              htmlColor={theme.palette.secondary.light}>
              <ProjectsIcon />
            </SvgIcon>
          ) : screenType == "risk" ? (
            <SvgIcon
              viewBox="0 0 18 15.75"
              classes={{ root: classes.emptyStateIcon }}
              htmlColor={theme.palette.secondary.light}>
              <RiskIcon />
            </SvgIcon>
          ) : screenType == "issue" ? (
            <SvgIcon
              viewBox="0 0 16 17.375"
              classes={{ root: classes.emptyStateIcon }}
              htmlColor={theme.palette.secondary.light}>
              <IssuesIcon />
            </SvgIcon>
          ) : screenType == "meeting" ? (
            <SvgIcon
              viewBox="0 0 17.031 17"
              classes={{ root: classes.emptyStateIcon }}
              htmlColor={theme.palette.secondary.light}>
              <MeetingsIcon />
            </SvgIcon>
          ) : screenType == "timesheet" ? (
            <TimerIcon
              classes={{ root: classes.emptyStateIcon }}
              htmlColor={theme.palette.secondary.light}
            />
          ) : screenType == "comments" ? (
            <MessageIcon
              classes={{ root: classes.emptyStateIcon }}
              htmlColor={theme.palette.secondary.light}
            />
          ) : screenType == "search" ? (
            <SvgIcon
              viewBox="0 0 73 76"
              classes={{ root: classes.emptyStateIcon }}
              htmlColor={theme.palette.secondary.light}>
              <SearchIcon />
            </SvgIcon>
          ) : screenType == "documents" ? (
            <>
              <SvgIcon
                viewBox="0 0 112 84.802"
                classes={{ root: classes.emptyStateIconDocument }}
                htmlColor={theme.palette.secondary.light}
                >
                <IllustrationDocuments />
              </SvgIcon>
            </>
          ) : screenType == "Archived" ? (
            <SvgIcon
              viewBox="0 0 512 512"
              classes={{ root: classes.emptyStateIcon }}
              htmlColor={theme.palette.secondary.light}>
              <ArchieveIcon />
            </SvgIcon>
          ) : null}
          {screenType === "conversation" && <img src={NoConversationImg} width={176} />}
          <Typography variant="h4" classes={{ h4: classes.emptyStateHeading }}>
            {heading}
          </Typography>
          <Typography variant="subtitle2" className={classes.content}>
            {message}
          </Typography>
          {button ? (
            <Hotkeys keyName="alt+t" onKeyDown={event => this.handleDialog(event, `${screenType}Dialog`)}>
            <CustomIconButton
              onClick={event => {
                clickAction ? clickAction() : this.handleDialog(event, `${screenType}Dialog`);
              }}
              btnType="success"
              style={{ marginTop: 15 }}>
              <AddIcon htmlColor={theme.palette.common.white} />
            </CustomIconButton>
          </Hotkeys>
          ) : null}

          <DefaultDialog
            title="Add Task"
            open={taskDialog}
            onClose={event => this.handleDialogClose(event, "taskDialog")}>
            <AddNewTaskForm closeAction={event => this.handleDialogClose(event, "taskDialog")} />
          </DefaultDialog>

          <DefaultDialog
            title="Create Project"
            open={projectDialog}
            onClose={event => this.handleDialogClose(event, "projectDialog")}>
            <AddNewProject closeAction={event => this.handleDialogClose(event, "projectDialog")} />
          </DefaultDialog>
          <DefaultDialog
            title="Add Issue"
            open={issueDialog}
            onClose={event => this.handleDialogClose(event, "issueDialog")}>
            <AddIssueForm
              view="Header-Menu"
              closeAction={event => this.handleDialogClose(event, "issueDialog")}
            />
          </DefaultDialog>

          <DefaultDialog
            title="Add Risk"
            open={riskDialog}
            onClose={event => this.handleDialogClose(event, "riskDialog")}>
            <AddRiskForm
              view="Header-Menu"
              closeAction={event => this.handleDialogClose(event, "riskDialog")}
            />
          </DefaultDialog>

          <DefaultDialog
            title="Add Meeting"
            open={meetingDialog}
            onClose={event => this.handleDialogClose(event, "meetingDialog")}>
            <AddMeetingForm
              handleCloseMeeting={event => this.handleDialogClose(event, "meetingDialog")}
              isNew
              isDialog
              maxLength={250}
            />
          </DefaultDialog>

          <DefaultDialog
            title="Add Workspace"
            sucessBtnText="Create Workspace"
            open={newWorkspaceDialog}
            onClose={this.handleNewWorkspaceDialogClose}>
            <AddWorkspaceForm closeDialog={this.handleNewWorkspaceDialogClose} />
          </DefaultDialog>
        </Grid>
      </Grid>
    );
  }
}
export default withStyles(emptyStateStyles, { withTheme: true })(EmptyState);
