const videoList = [
    {
        "contentDetails": {
            "endAt": null,
            "note": null,
            "startAt": null,
            "videoId": "rR0dxlg-hL4",
            "videoPublishedAt": "2018-07-09T12:43:15.000Z",
            "eTag": null
        },
        "etag": "\"XI7nbFXulYBIpL0ayR_gDh3eu1k/PmX4VGyy6BDiOkUVDTnn2cAAGlI\"",
        "id": "UExmOU1pS0YxeFZXVTMyWTBJZjNuQW5NU1BsUnN0T1p1by41NkI0NEY2RDEwNTU3Q0M2",
        "kind": "youtube#playlistItem",
        "snippet": {
            "channelId": "UCGjlcTGCxXyRCb380Tq8_Rg",
            "channelTitle": "nTask",
            "description": "Explore how to create and manage your first meeting in nTask with Josh from nTask.\n\nExplore more about Meeting Management in nTask: https://www.ntaskmanager.com/product/meeting-management-software/\n\nHave some feedback to share? We're all ears. Share it here: https://ntaskmanager.typeform.com/to/oDTsrO\n\nFollow us on the social media:\n\n1. Facebook: https://www.facebook.com/ntaskmanager\n2. Twitter: https://www.twitter.com/ntaskmanager\n3. Instagram: https://www.instagram.com/ntaskmanager\n4. Google+: https://www.google.com/+ntaskmanager\n5. Pinterest: https://www.pinterest.com/ntaskmanager",
            "playlistId": "PLf9MiKF1xVWU32Y0If3nAnMSPlRstOZuo",
            "position": 0,
            "publishedAt": "2018-07-13T06:45:44.000Z",
            "resourceId": {
                "channelId": null,
                "kind": "youtube#video",
                "playlistId": null,
                "videoId": "rR0dxlg-hL4",
                "eTag": null
            },
            "thumbnails": {
                "default": {
                    "height": 90,
                    "url": "https://i.ytimg.com/vi/rR0dxlg-hL4/default.jpg",
                    "width": 120,
                    "eTag": null
                },
                "high": {
                    "height": 360,
                    "url": "https://i.ytimg.com/vi/rR0dxlg-hL4/hqdefault.jpg",
                    "width": 480,
                    "eTag": null
                },
                "maxres": {
                    "height": 720,
                    "url": "https://i.ytimg.com/vi/rR0dxlg-hL4/maxresdefault.jpg",
                    "width": 1280,
                    "eTag": null
                },
                "medium": {
                    "height": 180,
                    "url": "https://i.ytimg.com/vi/rR0dxlg-hL4/mqdefault.jpg",
                    "width": 320,
                    "eTag": null
                },
                "standard": {
                    "height": 480,
                    "url": "https://i.ytimg.com/vi/rR0dxlg-hL4/sddefault.jpg",
                    "width": 640,
                    "eTag": null
                },
                "eTag": null
            },
            "title": "nTask Essentials - Managing Meetings in nTask",
            "eTag": null
        },
        "status": null
    },
    {
        "contentDetails": {
            "endAt": null,
            "note": null,
            "startAt": null,
            "videoId": "yBvjXIMcAec",
            "videoPublishedAt": "2018-07-09T12:39:32.000Z",
            "eTag": null
        },
        "etag": "\"XI7nbFXulYBIpL0ayR_gDh3eu1k/Uw-KKCKB0vkyKTsH5q8ViWo9w0U\"",
        "id": "UExmOU1pS0YxeFZXVTMyWTBJZjNuQW5NU1BsUnN0T1p1by4yODlGNEE0NkRGMEEzMEQy",
        "kind": "youtube#playlistItem",
        "snippet": {
            "channelId": "UCGjlcTGCxXyRCb380Tq8_Rg",
            "channelTitle": "nTask",
            "description": "Explore how to create and manage your first project in nTask with Josh from nTask.\n\nExplore more about Project Management in nTask: https://www.ntaskmanager.com/product/project-management-software/\n\nHave some feedback to share? We're all ears. Share it here: https://ntaskmanager.typeform.com/to/oDTsrO\n\nFollow us on the social media:\n\n1. Facebook: https://www.facebook.com/ntaskmanager\n2. Twitter: https://www.twitter.com/ntaskmanager\n3. Instagram: https://www.instagram.com/ntaskmanager\n4. Google+: https://www.google.com/+ntaskmanager\n5. Pinterest: https://www.pinterest.com/ntaskmanager",
            "playlistId": "PLf9MiKF1xVWU32Y0If3nAnMSPlRstOZuo",
            "position": 1,
            "publishedAt": "2018-07-13T06:45:48.000Z",
            "resourceId": {
                "channelId": null,
                "kind": "youtube#video",
                "playlistId": null,
                "videoId": "yBvjXIMcAec",
                "eTag": null
            },
            "thumbnails": {
                "default": {
                    "height": 90,
                    "url": "https://i.ytimg.com/vi/yBvjXIMcAec/default.jpg",
                    "width": 120,
                    "eTag": null
                },
                "high": {
                    "height": 360,
                    "url": "https://i.ytimg.com/vi/yBvjXIMcAec/hqdefault.jpg",
                    "width": 480,
                    "eTag": null
                },
                "maxres": {
                    "height": 720,
                    "url": "https://i.ytimg.com/vi/yBvjXIMcAec/maxresdefault.jpg",
                    "width": 1280,
                    "eTag": null
                },
                "medium": {
                    "height": 180,
                    "url": "https://i.ytimg.com/vi/yBvjXIMcAec/mqdefault.jpg",
                    "width": 320,
                    "eTag": null
                },
                "standard": {
                    "height": 480,
                    "url": "https://i.ytimg.com/vi/yBvjXIMcAec/sddefault.jpg",
                    "width": 640,
                    "eTag": null
                },
                "eTag": null
            },
            "title": "nTask Essentials - Managing Projects in nTask",
            "eTag": null
        },
        "status": null
    },
    {
        "contentDetails": {
            "endAt": null,
            "note": null,
            "startAt": null,
            "videoId": "xDpl26EUBwQ",
            "videoPublishedAt": "2018-06-21T13:18:10.000Z",
            "eTag": null
        },
        "etag": "\"XI7nbFXulYBIpL0ayR_gDh3eu1k/yopJZ-Qno7mGh1XG3nN9418TQ9Y\"",
        "id": "UExmOU1pS0YxeFZXVTMyWTBJZjNuQW5NU1BsUnN0T1p1by4wMTcyMDhGQUE4NTIzM0Y5",
        "kind": "youtube#playlistItem",
        "snippet": {
            "channelId": "UCGjlcTGCxXyRCb380Tq8_Rg",
            "channelTitle": "nTask",
            "description": "Explore how to manage task details in this overview video by Josh from nTask.\n\nExplore more about Task Management in nTask: https://www.ntaskmanager.com/product/task-management-software/\n\nHave some feedback to share? We're all ears. Share it here: https://ntaskmanager.typeform.com/to/oDTsrO\n\nFollow us on the social media:\n\n1. Facebook: https://www.facebook.com/ntaskmanager\n2. Twitter: https://www.twitter.com/ntaskmanager\n3. Instagram: https://www.instagram.com/ntaskmanager\n4. Google+: https://www.google.com/+ntaskmanager\n5. Pinterest: https://www.pinterest.com/ntaskmanager",
            "playlistId": "PLf9MiKF1xVWU32Y0If3nAnMSPlRstOZuo",
            "position": 2,
            "publishedAt": "2018-07-13T06:46:02.000Z",
            "resourceId": {
                "channelId": null,
                "kind": "youtube#video",
                "playlistId": null,
                "videoId": "xDpl26EUBwQ",
                "eTag": null
            },
            "thumbnails": {
                "default": {
                    "height": 90,
                    "url": "https://i.ytimg.com/vi/xDpl26EUBwQ/default.jpg",
                    "width": 120,
                    "eTag": null
                },
                "high": {
                    "height": 360,
                    "url": "https://i.ytimg.com/vi/xDpl26EUBwQ/hqdefault.jpg",
                    "width": 480,
                    "eTag": null
                },
                "maxres": {
                    "height": 720,
                    "url": "https://i.ytimg.com/vi/xDpl26EUBwQ/maxresdefault.jpg",
                    "width": 1280,
                    "eTag": null
                },
                "medium": {
                    "height": 180,
                    "url": "https://i.ytimg.com/vi/xDpl26EUBwQ/mqdefault.jpg",
                    "width": 320,
                    "eTag": null
                },
                "standard": {
                    "height": 480,
                    "url": "https://i.ytimg.com/vi/xDpl26EUBwQ/sddefault.jpg",
                    "width": 640,
                    "eTag": null
                },
                "eTag": null
            },
            "title": "nTask Essentials - Managing Task Details in nTask",
            "eTag": null
        },
        "status": null
    },
    {
        "contentDetails": {
            "endAt": null,
            "note": null,
            "startAt": null,
            "videoId": "4_bwV5wAhXI",
            "videoPublishedAt": "2018-06-28T15:23:18.000Z",
            "eTag": null
        },
        "etag": "\"XI7nbFXulYBIpL0ayR_gDh3eu1k/NPdQAcqnYyHx0aubQWWDSLnsdrM\"",
        "id": "UExmOU1pS0YxeFZXVTMyWTBJZjNuQW5NU1BsUnN0T1p1by4wOTA3OTZBNzVEMTUzOTMy",
        "kind": "youtube#playlistItem",
        "snippet": {
            "channelId": "UCGjlcTGCxXyRCb380Tq8_Rg",
            "channelTitle": "nTask",
            "description": "Josh from nTask shows how to get started with tasks in nTask.\n\nExplore more about nTask: https://www.ntaskmanager.com/product/task-management-software/\n\nHave some feedback to share? We're all ears. Share it here: https://ntaskmanager.typeform.com/to/oDTsrO\n\nFollow us on the social media:\n\n1. Facebook: https://www.facebook.com/ntaskmanager\n2. Twitter: https://www.twitter.com/ntaskmanager\n3. Instagram: https://www.instagram.com/ntaskmanager\n4. Google+: https://www.google.com/+ntaskmanager\n5. Pinterest: https://www.pinterest.com/ntaskmanager",
            "playlistId": "PLf9MiKF1xVWU32Y0If3nAnMSPlRstOZuo",
            "position": 3,
            "publishedAt": "2018-07-13T06:46:16.000Z",
            "resourceId": {
                "channelId": null,
                "kind": "youtube#video",
                "playlistId": null,
                "videoId": "4_bwV5wAhXI",
                "eTag": null
            },
            "thumbnails": {
                "default": {
                    "height": 90,
                    "url": "https://i.ytimg.com/vi/4_bwV5wAhXI/default.jpg",
                    "width": 120,
                    "eTag": null
                },
                "high": {
                    "height": 360,
                    "url": "https://i.ytimg.com/vi/4_bwV5wAhXI/hqdefault.jpg",
                    "width": 480,
                    "eTag": null
                },
                "maxres": {
                    "height": 720,
                    "url": "https://i.ytimg.com/vi/4_bwV5wAhXI/maxresdefault.jpg",
                    "width": 1280,
                    "eTag": null
                },
                "medium": {
                    "height": 180,
                    "url": "https://i.ytimg.com/vi/4_bwV5wAhXI/mqdefault.jpg",
                    "width": 320,
                    "eTag": null
                },
                "standard": {
                    "height": 480,
                    "url": "https://i.ytimg.com/vi/4_bwV5wAhXI/sddefault.jpg",
                    "width": 640,
                    "eTag": null
                },
                "eTag": null
            },
            "title": "nTask Essentials - Creating Your First Task",
            "eTag": null
        },
        "status": null
    },
    {
        "contentDetails": {
            "endAt": null,
            "note": null,
            "startAt": null,
            "videoId": "QF7PWaAazbc",
            "videoPublishedAt": "2018-06-08T13:07:34.000Z",
            "eTag": null
        },
        "etag": "\"XI7nbFXulYBIpL0ayR_gDh3eu1k/23A3-Lo83OvQmXpB4ZFSdTrdKUQ\"",
        "id": "UExmOU1pS0YxeFZXVTMyWTBJZjNuQW5NU1BsUnN0T1p1by41MjE1MkI0OTQ2QzJGNzNG",
        "kind": "youtube#playlistItem",
        "snippet": {
            "channelId": "UCGjlcTGCxXyRCb380Tq8_Rg",
            "channelTitle": "nTask",
            "description": "Take a quick tour guide with Josh from nTask to explore and see how nTask works.\n\nIn this video, Josh gives you a brief overview of how to navigate your way around nTask.\n\n\nExplore more about nTask: https://www.ntaskmanager.com/product/\n\nHave some feedback to share? We're all ears. Share it here: https://ntaskmanager.typeform.com/to/oDTsrO\n\nFollow us on the social media:\n\n1. Facebook: https://www.facebook.com/ntaskmanager\n2. Twitter: https://www.twitter.com/ntaskmanager\n3. Instagram: https://www.instagram.com/ntaskmanager\n4. Google+: https://www.google.com/+ntaskmanager\n5. Pinterest: https://www.pinterest.com/ntaskmanager",
            "playlistId": "PLf9MiKF1xVWU32Y0If3nAnMSPlRstOZuo",
            "position": 4,
            "publishedAt": "2018-07-13T06:46:14.000Z",
            "resourceId": {
                "channelId": null,
                "kind": "youtube#video",
                "playlistId": null,
                "videoId": "QF7PWaAazbc",
                "eTag": null
            },
            "thumbnails": {
                "default": {
                    "height": 90,
                    "url": "https://i.ytimg.com/vi/QF7PWaAazbc/default.jpg",
                    "width": 120,
                    "eTag": null
                },
                "high": {
                    "height": 360,
                    "url": "https://i.ytimg.com/vi/QF7PWaAazbc/hqdefault.jpg",
                    "width": 480,
                    "eTag": null
                },
                "maxres": {
                    "height": 720,
                    "url": "https://i.ytimg.com/vi/QF7PWaAazbc/maxresdefault.jpg",
                    "width": 1280,
                    "eTag": null
                },
                "medium": {
                    "height": 180,
                    "url": "https://i.ytimg.com/vi/QF7PWaAazbc/mqdefault.jpg",
                    "width": 320,
                    "eTag": null
                },
                "standard": {
                    "height": 480,
                    "url": "https://i.ytimg.com/vi/QF7PWaAazbc/sddefault.jpg",
                    "width": 640,
                    "eTag": null
                },
                "eTag": null
            },
            "title": "nTask Essentials - Basic Overview",
            "eTag": null
        },
        "status": null
    }
]

export default videoList;