import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import DefaultTextField from "../Form/TextField";

import onBoardStyles from "./styles";
import Select from "react-select";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/Button";
import CustomButton from "../Buttons/CustomButton";
import constants from "../constants/radioOptions";
import Paper from "@material-ui/core/Paper";
import GlobeIcon from "@material-ui/icons/Language";

import {
  validateUrlField,
  validateTitleField
} from "../../utils/formValidations";
import {
  IsWorkspaceUrlValid,
  addCompanyInformation
} from "./../../redux/actions/onboarding";
import {
  GLOBE_ICON,
  WORKSPACE_ICON,
  WORKSPACE_USE,
  HANDS_ICON
} from "../../utils/constants/icons";
import { onboardingHelpText } from "../Tooltip/helptext";
import CustomTooltip from "../Tooltip/Tooltip";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../mixpanel';

class CreateWorkspace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      workspaceName: "",
      workspaceUrl: "",
      workspaceUseOther: "",

      workspaceError: false,
      workspaceUrlError: false,
      workspaceUseOtherError: false,

      workspaceErrorMessage: "",
      workspaceUrlErrorMessage: "",
      workspaceUseOtherErrorMessage: "",

      workspaceUse: "",
      otherWorkspaceOption: false,
      wsNameBind: true,
      btnQuery: ""
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleWorkSpaceInfoClick = this.handleWorkSpaceInfoClick.bind(this);
  }
  componentDidMount() {
    const localStore = localStorage.getItem("CreateWorkspace");
    mixpanel.time_event(MixPanelEvents.WorkspaceCreationEvent);
    if (localStore) {
      this.setState({
        ...this.state,
        ...JSON.parse(localStore),
        workspaceError: false,
        workspaceUrlError: false,
        workspaceUseOtherError: false,
        workspaceErrorMessage: "",
        workspaceUrlErrorMessage: "",
        workspaceUseOtherErrorMessage: ""
      });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state) {
      localStorage.setItem("CreateWorkspace", JSON.stringify(this.state));
    }
  }
  handleInput = fieldName => event => {
    let inputValue = event.target.value;
    switch (fieldName) {
      //////////////////////////////////// WORKSPACE NAME ///////////////////////////////////
      case "workspaceName":
        //Replacing all spaces with Dash from workspace url
        let workspaceUrl = inputValue.replace(/[\s\+]+/g, "-").toLowerCase();
        this.setState({
          [fieldName]: inputValue,
          workspaceError: false,
          workspaceErrorMessage: "",
          wsNameBind: true,
          workspaceUrl: workspaceUrl
        });
        break;
      //////////////////////////////////// WORKSPACE URL ////////////////////////////////////
      case "workspaceUrl": {
        //Disallowing user to enter space in url
        let newWorkspaceUrl = inputValue.replace(/ /g, "");
        this.setState({
          [fieldName]: newWorkspaceUrl,
          workspaceUrlError: false,
          workspaceUrlErrorMessage: ""
        });
        if (this.state.wsNameBind) {
          this.setState({ wsNameBind: false });
        }
      }
      //////////////////////////////////// WORKSPACE OTHER USAGE ////////////////////////////////////
      case "workspaceUseOther":
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
              [fieldName]: inputValue,
              workspaceUseOtherError: false,
              workspaceUseOtherErrorMessage: ""
            });
        break;
    }
  };
  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };
  handleSelectChange(value) {
    if (value.label == "other" || value.label == "Other") {
      this.setState({ workspaceUse: value, otherWorkspaceOption: true });
    } else {
      this.setState({
        workspaceUse: value,
        otherWorkspaceOption: false,
        workspaceUseOther: ""
      });
    }
  }
  handleCompanySizeChange = event => {
    this.setState({ companySize: event.target.value });
  };
  onClickValidationsChecker() {
    let errorCheck_workspaceName = false;
    let errorCheck_workspaceUrl = false;
    let errorCheck_workspaceOtherUsage = true;
    //////////////////////////////////// WORKSPACE NAME ////////////////////////////////////
    let workspaceNameValue = this.state.workspaceName.trim();
    let validationObj = validateTitleField(
      "workspace name",
      workspaceNameValue
    );
    if (validationObj["isError"]) {
      this.setState({
        workspaceError: validationObj["isError"],
        workspaceErrorMessage: validationObj["message"]
      });
    } else {
      errorCheck_workspaceName = true;
      this.setState({
        workspaceError: false,
        workspaceErrorMessage: ""
      });
    }
    //////////////////////////////////// WORKSPACE URL ////////////////////////////////////
    let workspaceUrlValue = this.state.workspaceUrl.trim();
    validationObj = validateUrlField("workspace URL", workspaceUrlValue);
    if (validationObj["isError"]) {
      this.setState({
        workspaceUrlError: validationObj["isError"],
        workspaceUrlErrorMessage: validationObj["message"]
      });
    } else {
      errorCheck_workspaceUrl = true;
      this.setState({
        workspaceUrlError: false,
        workspaceUrlErrorMessage: ""
      });
    }
    //////////////////////////////////// WORKSPACE OTHER USAGE ////////////////////////////////////
    if (this.state.otherWorkspaceOption) {
      let workspaceOtherUsageValue = this.state.workspaceUseOther.trim();
      validationObj = validateTitleField(
        "workspace usage",
        workspaceOtherUsageValue,
        true
      );
      if (validationObj["isError"]) {
        errorCheck_workspaceOtherUsage = false;
        this.setState({
          workspaceUseOtherError: validationObj["isError"],
          workspaceUseOtherErrorMessage: validationObj["message"]
        });
      } else {
        this.setState({
          workspaceUseOtherError: false,
          workspaceUseOtherErrorMessage: ""
        });
      }
    }
    //////////////////////////////////////////////////////////////////////////////////
    return {
      errorCheck_workspaceName,
      errorCheck_workspaceUrl,
      errorCheck_workspaceOtherUsage
    };
  }
  APIhandlerCompanyInformation() {
    let workspaceUseage = this.state.workspaceUse.value;
    if (
      this.state.workspaceUse &&
      workspaceUseage &&
      workspaceUseage.toLowerCase() === "other"
    ) {
      workspaceUseage = this.state.workspaceUseOther.trim();
    }
    let object = {
      companyName: this.state.workspaceName.trim(),
      companyUrl: this.state.workspaceUrl.trim(),
      WrokspaceUse: workspaceUseage
    };

    if (!this.props.workSpaceData) {
      object = Object.assign({}, this.props.workSpaceData);
      object.companyName = this.state.workspaceName;
      object.companyUrl = this.state.workspaceUrl;
      object.WrokspaceUse = this.state.workspaceUse.value;
    }
    this.setState({ btnQuery: "progress" });
    this.props.addCompanyInformation(
      object,
      response => {
        this.setState({ btnQuery: "" });
        if (response.data) {
          mixpanel.track(MixPanelEvents.WorkspaceCreationEvent, response.data);
          localStorage.setItem("teamId", String(response.data.teamId));
          this.props.SetWorkSpaceData(response.data);
          this.props.history.push("/welcome/invite-members");
          this.props.setActiveStep(3);
        }
      },
      error => {
        this.setState({ btnQuery: "" });

        this.setState({
          workspaceUrlError: true,
          workspaceUrlErrorMessage: error.data ? error.data.message : null
        });
      }
    );
  }
  handleBack = () => {
    this.props.handleBack(1);
    this.props.history.push("/welcome/company");
  };

  handleWorkSpaceInfoClick() {
    let responce = this.onClickValidationsChecker();
    if (
      responce.errorCheck_workspaceName &&
      responce.errorCheck_workspaceUrl &&
      responce.errorCheck_workspaceOtherUsage
    ) {
      this.APIhandlerCompanyInformation();
    }
  }
  render() {
    const { activeStep } = this.props;
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: "4px 0 4px 20px",
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: "none",
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        }
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "5px 0 5px 38px"
      })
    };
    const {
      workspaceUrl,
      workspaceName,
      workspaceUse,
      workspaceUseOtherError,
      workspaceUseOtherErrorMessage,
      workspaceError,
      workspaceErrorMessage,
      workspaceUrlError,
      workspaceUrlErrorMessage,
      workspaceUseOther,
      otherWorkspaceOption,
      btnQuery
    } = this.state;
    const { classes, theme } = this.props;
    const disabledButtonAction = workspaceName == '' && workspaceUrl == '' ? true : false;
    return (
      <Fragment>
        <div className={classes.innerPaddedCnt}>
          <div className={classes.profileHeadingCnt}>
            <Typography
              variant="h1"
              align="center"
              classes={{ root: classes.mainHeading }}
            >
              Let's setup your workspace&nbsp;
              <img src={HANDS_ICON} className={classes.clapIcon} />
            </Typography>
            <Typography variant="body1" align="center">
              Create as many workspaces as you want.
            </Typography>
          </div>

          <DefaultTextField
            label="Workspace Name"
            helptext={onboardingHelpText.workspaceNameHelpText}
            fullWidth={true}
            errorState={workspaceError}
            errorMessage={workspaceErrorMessage}
            defaultProps={{
              id: "workspaceName",
              onChange: this.handleInput("workspaceName"),
              placeholder: "Workspace Name",
              value: workspaceName,
              autoFocus: true,
              inputProps: { maxLength: 50, tabIndex: 1 },
              startAdornment: (
                <InputAdornment position="start">
                  <img src={WORKSPACE_ICON} width={18} height={18} />
                </InputAdornment>
              )
            }}
          />

          {/* <FormControl
            fullWidth={true}
            classes={{ root: classes.selectFormControl }}
          >
            <img
              src={WORKSPACE_USE}
              width={20}
              height={20}
              className={classes.selectAdornment}
            />
            <InputLabel
              htmlFor="workspaceUse"
              classes={{
                root: classes.selectLabel
              }}
              shrink={false}
            >
              How will you use this workspace
              <CustomTooltip
                helptext={onboardingHelpText.howWillYouUseThisWorkspaceHelpText}
                iconType="help"
              />
            </InputLabel>
            <Select
              styles={customStyles}
              onChange={this.handleSelectChange}
              inputId="workspaceUse"
              options={constants.WORKSPACEUSAGE}
              value={workspaceUse}
              tabIndex={2}
            />
          </FormControl> */}

          {otherWorkspaceOption ? (
            <DefaultTextField
              label="Please specify your usage here"
              fullWidth={true}
              errorState={workspaceUseOtherError}
              errorMessage={workspaceUseOtherErrorMessage}
              defaultProps={{
                id: "workspaceUseOther",
                onChange: this.handleInput("workspaceUseOther"),
                value: workspaceUseOther,
                inputProps: { maxLength: 50, tabIndex: 3 }
              }}
            />
          ) : null}

          <DefaultTextField
            label="Workspace URL"
            helptext={onboardingHelpText.workspaceUrlHelpText}
            fullWidth={true}
            errorState={workspaceUrlError}
            errorMessage={workspaceUrlErrorMessage}
            defaultProps={{
              id: "workspaceUrl",
              onChange: this.handleInput("workspaceUrl"),
              placeholder: "workspace URL",
              value: workspaceUrl,
              inputProps: {
                style: { paddingLeft: 0 },
                maxLength: 80,
                tabIndex: 4
              },
              startAdornment: (
                <InputAdornment
                  position="start"
                  classes={{ root: classes.urlAdornment }}
                >
                  <GlobeIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.globeIcon}
                  />
                  <p className={classes.workspaceUrlAdornment}>
                  {window.location.origin+"/"}
                  </p>
                </InputAdornment>
              )
            }}
          />
        </div>
        <Paper
          classes={{
            root: classes.buttonPaperCntStatic
          }}
        >
          <div></div>
          <div>
            <Button
              disabled={activeStep === 0}
              //onClick={this.handleBack}
              className={classes.button}
              onClick={this.handleBack}
            >
              Back
            </Button>
            <CustomButton
              onClick={this.handleWorkSpaceInfoClick}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress" || disabledButtonAction}
            >
              Create Workspace
            </CustomButton>
          </div>
        </Paper>
      </Fragment>
    );
  }
}

export default compose(
  withRouter,
  withStyles(onBoardStyles, { withTheme: true }),
  connect(
    null,
    {
      IsWorkspaceUrlValid,
      addCompanyInformation
    }
  )
)(CreateWorkspace);
