import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import DefaultTextField from "../Form/TextField";
import IconButton from "@material-ui/core/IconButton";
import onBoardStyles from "./styles";
import Select from "react-select";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import constants from "../constants/radioOptions";
import CustomButton from "../Buttons/CustomButton";
import { validateTitleField } from '../../utils/formValidations';
import Paper from "@material-ui/core/Paper";
import { createusercompany } from "./../../redux/actions/onboarding";
import { onboardingHelpText } from "../Tooltip/helptext";
const JobTitle =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAQAAABKfvVzAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfiCw4IIBX2Hcf6AAABe0lEQVQ4y72TPyxDURTGf/d52oqBoSRCqLVdcLsxlKSN6Ch5sZnFRMJG/InFv8QgZomhUydESMRATFVLN0kbIUI7dKhoSnsN76Fton0WZ7rfvd855/vOyYU/hqjGMqhGhWYCVRInsTNUjQS5wAqPfFhQp5PF2GqNhjLpj5Rjf0QmqyT4l0qBMjjEg0qVYQ9d6rIMnwupyHBl0/Egbh3Q80aiUJ/tc7ieQQNaXUE75V1BWs0EMGwJMsy5KzI05tvrifI5XC+849ZARGlxhuqVd4ZoEVHQQcW5w+DQfOjzNGzQaLHei3O3X0M2uFNx0AHUgZiX1zwBiAkV5tQihfWiNBfZwbhaF5aHKcd+4ZjAd/dUrNc8yBQ937cXjrHCJHs6wPUbw/1tqhmgYZoZ+SWji83iLoB4jadBYkkCiKdJA/iWm7I/Ht52ErlK8zpVkcixVmtamq2V/XNCFq9ttpeskFvMcl/5b38JQTfbAm0grI0oYYOvSuc3R3+1wCeLLWt3aH+P/wAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOC0xMS0xNFQwODozMjoyMSswMTowMLnSS+gAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTgtMTEtMTRUMDg6MzI6MjErMDE6MDDIj/NUAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg==";
const OfficeImage =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH4gsOCDEaNXv5ewAAAH5QTFRFAAAAAAAAJCQkLi4uOzs7LS0tOTk5Nzc3MzMzNTU1MzMzNjY2NDQ0MzMzNDQ0NDQ0MjIyMTExNTU1MjIyMzMzNDQ0MjIyMjIyNDQ0MzMzMzMzMzMzMjIyMzMzMzMzMzMzMjIyMzMzMjIyMzMzMzMzMzMzMzMzMzMzMzMzMzMz/4lMlQAAACl0Uk5TAAIHCw0REhweIiMmJygxO0JDREhaYmtwdoKLjY6QqrO2uMrM4PL2+/5nJ49MAAAAAWJLR0QAiAUdSAAAAK1JREFUKM+N0ucOgyAUhuHP4ta6Fzjr5v5vsNq0iZCa8P7kSQ4kHADRyqXWCGcln15CEy+/kEIovQDNkDBdZwkyKsDWg3HX5Qz9JoBlgDiAQ2BYanA76vby2+d2FbLeNPsMVSfA0oFutr1RdIvaqNCHFWtabMEPBdgHNNzzeINhFyCPEBSPRxEgytXuGOvfaT3KQJ5HRIYjdn4qk0cdtSe0ivBZhvmE+boM/9fnDY8GJEN3laI3AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE4LTExLTE0VDA4OjQ5OjI2KzAxOjAwra6bEgAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOC0xMS0xNFQwODo0OToyNiswMTowMNzzI64AAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC";
const ClapIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAbCAYAAACJISRoAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjMwNjk1Nzk1RTdFODExRTg4RjRFRjBFNjk2RTY4QkZEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjMwNjk1Nzk2RTdFODExRTg4RjRFRjBFNjk2RTY4QkZEIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzA2OTU3OTNFN0U4MTFFODhGNEVGMEU2OTZFNjhCRkQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzA2OTU3OTRFN0U4MTFFODhGNEVGMEU2OTZFNjhCRkQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4iKJ65AAAESUlEQVR42rxWTYgcRRR+Nd3T0/+72d3Z7J9JNpt1VVTiIZd4UPSgGBFDxLigooeQhByiV6PoxYOimCAGRS+iIAajAZEIGhFFhSAiLHGN7o8xIdn87XbPdM90z/R0+1VP9zD7x84IWvBR1dVd76v36nuvmkVRRP91Y/924T3H/96Lrh+4BEx9s2vDqZW+405k2jD6DPB401QeeBF4G7iv6Tt56dpMC8YV4AMM3wDub3r1bdK7wKvJt5z4a/RqWyRo6xLwdnfT/GnAB15HqK4lc/uBO4H3WiaJpvfqp7a+rMDIg3h8FBCwy5v4O8xV0H2ZeMi9yKE7kCwdx/PBNUlK3z0xhG6SHyrI3gfZSYxvBipNn+0BmZWMdwG/AS8k3ry1orpgjMf0KA9D6XzpL6lLOiRqYvp6GniMjbzz85I1GrrXgA/x7oeV1CU2T3hz5cNyn/IIH6s3qPPurOMFmijLvbFgRoCfYPTQ9Pf2R7VqxMNxwr/q7czl5X0Y3wLctWqenH1391PoduTXs405M7tNHVIbPlatyrx31evShw1iYn2ybAXupTMlLfDDhb7BzDlzzNya2HsA3pxc6gkDgYnxPD9UpVOk7gHRD6yKpA/rLJOrH1lYDS1nxulQBhSWNbLxHDyhuckSZWtB0eyXDESAT88B20E0uzQZPcDOSkSDt6mk9sk5fYvB3Atu5F/36+rIZjqNUZNV7YBwVliJHWUZDd6ukZSXDd+qhonNPuBHhHTbIgmP7fmYq+WoIDAqX6gbyCAsxojBeFlzZooU1eAytqMOKSR2SFQ4a8dzvHUO5UjtlzOhn/LERF+BaEODBOHiWfy8V454rMk+Y1G6QO7JkdyvkTNVpMAN4jnJFEnbbFLNqzV2mgVxGtqkdSRSbuQJPxMS4b66SSdlk0HOrENpqEQlQ/qoSZWFCpUvluM5QWLUJO3V2m54I6Ykn/GsNUwWKR0CSbpAxhh2Cm+c6eZQqSToWaoWqq3WVAO4NSXBkdMdAaJR/L1AUQCjUKo6oJDUqyD+hUWhEhN1tdjWpyT38sJXtENWQjSsiQUKnMSoIZJxo0neZY+8K149sdq4gSpWhaUkE0CBPxTsiOySQIU/i4h/qX5oUJq+WSdBFdu61GrlGt+clUqYJw7P2C9idj+iaxYj90qVCpN2HL5YQXp7JCA4X7nun15WICHnh9EdAWKNazoUJ4WkD2uxTFttPAWQS5+v23nsoWXXL7w6kRS6V7hTrhPRgs2oOOPGso7CFn468Enxj8JlnOvBNX8k4NUousO86MXXY3eGxDAgbSNKSVduVQKocR4yf6ln/JM3GwVyrY2BbEdCtoXXN1PnaolIzucWZXqA/HHOuaUoCJ/rGT9+ZFEVbiXGIOJbf5aXH35UAk9OXFWigFCEIQwhZwP6RQhrT/c++enEslLfjmJA1plcs9uB7uQq/hU4hvOcWu2/i/0ff5D/CDAAuxPK7CWPbF4AAAAASUVORK5CYII=";

class CompanyInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      passwordError: "",
      usernameError: "",
      lastNameError: "",
      firstNameError: "",
      otherCompanyIndustry: "",
      otherIndustry: false,

      passwordErrorMessage: "",
      usernameErrorMessage: "",
      lastNameErrorMessage: "",
      firstNameErrorMessage: "",
      showPassword: false,
      companyName: "",
      jobTitle: null,
      companyIndustry: null,
      companySize: "1-5",

      otherCompanyIndustryError: false,
      otherCompanyIndustryErrorMessage: "",

      otherJobTitle: "",
      otherJobTitleErrorMessage: "",
      otherJobTitleError: false,
      OtherJob: false,
      btnQuery: ''
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleClickShowPassword = this.handleClickShowPassword.bind(this);
    this.handleIndustrySelect = this.handleIndustrySelect.bind(this);
    this.handleCompanySizeChange = this.handleCompanySizeChange.bind(this);
    this.handleCompanyInfoClick = this.handleCompanyInfoClick.bind(this);
  }
  componentDidMount() {
    const localStore = localStorage.getItem("CompanyInfo");
    if (localStore) {
      this.setState({
        ...this.state, ...JSON.parse(localStore),
        otherCompanyIndustryError: false,
        otherCompanyIndustryErrorMessage: "",
        otherJobTitleErrorMessage: "",
        otherJobTitleError: false
      });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state) {
      localStorage.setItem("CompanyInfo", JSON.stringify(this.state));
    }
  }
  handleInput = fieldName => event => {
    let inputValue = event.currentTarget.value;
    switch (fieldName) {
      //////////////////////////////////// COMPANY INFORMATION ///////////////////////////////////
      case 'companyName':
        (inputValue === '') ? this.setState({ [fieldName]: inputValue }) :
          this.setState({ [fieldName]: inputValue, companyError: false, companyErrorMessage: '' });
        break;
      //////////////////////////////////// OTHER JOB ////////////////////////////////////
      case 'otherJobTitle':
        (inputValue === '') ? this.setState({ [fieldName]: inputValue }) :
          this.setState({ [fieldName]: inputValue, otherJobTitleError: false, otherJobTitleErrorMessage: '' });
        break;
      //////////////////////////////////// OTHER INDUSTRY ////////////////////////////////////
      case 'otherCompanyIndustry':
        (inputValue === '') ? this.setState({ [fieldName]: inputValue }) :
          this.setState({ [fieldName]: inputValue, otherCompanyIndustryError: false, otherCompanyIndustryErrorMessage: '' });
        break;
    };
  };
  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };
  handleSelectChange(value) {
    if (value.label.toLowerCase() == "other") {
      this.setState({ jobTitle: value, OtherJob: true });
    } else {
      this.setState({
        jobTitle: value,
        OtherJob: false,
        otherJobTitle: ""
      });
    }
  }
  handleIndustrySelect(value) {
    if (value.label.toLowerCase() == "other") {
      this.setState({ companyIndustry: value, otherIndustry: true });
    } else {
      this.setState({
        companyIndustry: value,
        otherIndustry: false,
        otherCompanyIndustry: ""
      });
    }
  }
  handleCompanySizeChange = event => {
    this.setState({ companySize: event.target.value });
  };
  onClickValidationsChecker() {
    let errorCheck_companyName = true;
    let errorCheck_CompanyIndustry = true;
    let errorCheck_JobTitle = true;
    //////////////////////////////////// COMPANY INFORMATION ////////////////////////////////////
    let companyNameValue = this.state.companyName;
    let validationObj = validateTitleField('company name', companyNameValue, false);
    if (validationObj['isError']) {
      errorCheck_companyName = false;
      this.setState({
        companyError: validationObj['isError'],
        companyErrorMessage: validationObj['message']
      });
    }
    else {
      this.setState({
        companyError: false,
        companyErrorMessage: ''
      });
    }
    //////////////////////////////////// OTHER INDUSTRY ////////////////////////////////////
    if (this.state.otherIndustry) {
      let otherCompanyValue = this.state.otherCompanyIndustry;
      validationObj = validateTitleField('other industry', otherCompanyValue, true);
      if (validationObj['isError']) {
        errorCheck_CompanyIndustry = false;
        this.setState({
          otherCompanyIndustryError: validationObj['isError'],
          otherCompanyIndustryErrorMessage: validationObj['message']
        });
      }
      else {
        this.setState({
          otherCompanyIndustryError: false,
          otherCompanyIndustryErrorMessage: ""
        });
      }
    }

    //////////////////////////////////// OTHER JOB ////////////////////////////////////
    if (this.state.OtherJob) {
      let otherJobValue = this.state.otherJobTitle;
      validationObj = validateTitleField('other job', otherJobValue, true);
      if (validationObj['isError']) {
        errorCheck_JobTitle = false;
        this.setState({
          otherJobTitleError: validationObj['isError'],
          otherJobTitleErrorMessage: validationObj['message']
        });
      }
      else {
        this.setState({
          otherJobTitleErrorMessage: "",
          otherJobTitleError: false,
        });
      }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    return { errorCheck_companyName, errorCheck_CompanyIndustry, errorCheck_JobTitle };
  }
  apiURLCall() {
    let object = {
      companyName: this.state.companyName.trim(),
      industry: (this.state.companyIndustry && this.state.companyIndustry.value) ? this.state.companyIndustry.value.trim() : null,
      industryDetails: this.state.otherCompanyIndustry.trim(),
      jobTitle: (this.state.jobTitle && this.state.jobTitle.value) ? this.state.jobTitle.value.trim() : null,
      jobTitleDetails: this.state.otherJobTitle.trim(),
      numberOfMembers: this.state.companySize
    };
    this.setState({ btnQuery: "progress" });
    this.props.createusercompany(object, data => {
      this.setState({ btnQuery: "" });
      if (data.status) {
        this.setState({
          companyError: false,
          companyErrorMessage: "",
          otherCompanyIndustryError: false,
          otherCompanyIndustryErrorMessage: "",
          otherJobTitleErrorMessage: "",
          otherJobTitleError: false,
        });
        this.props.history.push("/welcome/create-team");
        //this.props.handleNext();
      }
    }, error => {
      this.props.showSnackBar(error.message, 'error');
    });
  }
  handleCompanyInfoClick() {
    let response = this.onClickValidationsChecker();
    if (response.errorCheck_companyName && response.errorCheck_CompanyIndustry && response.errorCheck_JobTitle) {
      this.apiURLCall();
    }
  }
  render() {
    const { activeStep } = this.props;
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: "4px 0 4px 20px",
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: "none",
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        }
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "5px 0 5px 38px"
      })
    };
    const {
      companyName,
      jobTitle,
      companyError,
      companyErrorMessage,
      companyIndustry,
      companySize,
      otherCompanyIndustry,
      otherIndustry,
      otherCompanyIndustryError,
      otherCompanyIndustryErrorMessage,

      otherJobTitle,
      otherJobTitleErrorMessage,
      otherJobTitleError,
      OtherJob,
      btnQuery
    } = this.state;
    const { classes, theme, skipAction } = this.props;
    return (
      <Fragment>
        <div className={classes.innerPaddedCnt}>
          <div className={classes.profileHeadingCnt}>
            <Typography
              variant="h1"
              align="center"
              classes={{ root: classes.mainHeading }}
            >
              Let's get to know each other&nbsp;{" "}
              <img src={ClapIcon} className={classes.clapIcon} />
            </Typography>
            <Typography variant="body1" align="center">
              Tell us a little about yourself.
          </Typography>
          </div>

          <DefaultTextField
            label="Company Name"
            helptext={onboardingHelpText.companyNameHelpText}
            fullWidth={true}
            errorState={companyError}
            errorMessage={companyErrorMessage}
            defaultProps={{
              id: "companyName",
              onChange: this.handleInput("companyName"),
              placeholder: 'Your Company Name',
              value: companyName,
              autoFocus: true,
              inputProps: { maxLength: 50, tabIndex: 1 },
              startAdornment: (
                <InputAdornment position="start">
                  <img src={OfficeImage} width={20} height={20} />
                </InputAdornment>
              )
            }}
          />

          <FormControl
            fullWidth={true}
            classes={{ root: classes.selectFormControl }}
          >
            <InputLabel
              htmlFor="jobTitle"
              classes={{
                root: classes.selectLabel
              }}
              shrink={false}
            >
              Job Function
          </InputLabel>
            <img
              src={JobTitle}
              className={classes.selectAdornment}
              width={20}
              height={20}
            />
            <Select
              styles={customStyles}
              onChange={this.handleSelectChange}
              inputId="jobTitle"
              options={constants.JOBTITLELIST}
              value={jobTitle}
              tabIndex={2}
            />
          </FormControl>
          {OtherJob ? (
            <DefaultTextField
              label="Please specify your job title."
              fullWidth={true}
              errorState={otherJobTitleError}
              errorMessage={otherJobTitleErrorMessage}
              defaultProps={{
                id: "otherJobTitle",
                onChange: this.handleInput("otherJobTitle"),
                value: otherJobTitle,
                inputProps: { maxLength: 50, tabIndex: 3 }
              }}
            />
          ) : null}

          <FormControl
            fullWidth={true}
            classes={{ root: classes.selectFormControl }}
          >
            <img
              src={OfficeImage}
              width={20}
              height={20}
              className={classes.selectAdornment}
            />

            <InputLabel
              htmlFor="jobTitle"
              classes={{
                root: classes.selectLabel
              }}
              shrink={false}
            >
              Company's Industry
          </InputLabel>
            <Select
              styles={customStyles}
              onChange={this.handleIndustrySelect}
              inputId="companyIndustry"
              options={constants.INDUSTRYLIST}
              value={companyIndustry}
              tabIndex={4}
            />
          </FormControl>
          {otherIndustry ? (
            <DefaultTextField
              label="Please specify your industry"
              fullWidth={true}
              errorState={otherCompanyIndustryError}
              errorMessage={otherCompanyIndustryErrorMessage}
              defaultProps={{
                id: "otherCompanyIndustry",
                onChange: this.handleInput("otherCompanyIndustry"),
                value: otherCompanyIndustry,
                inputProps: { maxLength: 50, tabIndex: 5 }
              }}
            />
          ) : null}

          <FormControl
            component="fieldset"
            className={classes.companySizeFormControl}
          >
            <FormLabel classes={{ root: classes.formLabel }} component="legend">
              What's your team size?
          </FormLabel>
            <RadioGroup
              name="CompanySize"
              value={companySize}
              onChange={this.handleCompanySizeChange}
              row
              style={{ justifyContent: "space-between" }}
            >
              <FormControlLabel value="1-5" control={<Radio />} label="1-5" />
              <FormControlLabel value="6-10" control={<Radio />} label="6-10" />
              <FormControlLabel value="11-25" control={<Radio />} label="11-25" />
              <FormControlLabel value="26-50" control={<Radio />} label="26-50" />
              <FormControlLabel value="50+" control={<Radio />} label="50+" />
            </RadioGroup>
          </FormControl>
        </div>
        <Paper
          classes={{
            root: classes.buttonPaperCnt
          }}
        >
          <span className={classes.skipText} onClick={skipAction}>
            Skip, I'll do it later
          </span>
          <div>
            <CustomButton
              onClick={this.handleCompanyInfoClick}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress"}
            >
              {
                activeStep === 0
                  ? "Create Profile"
                  : activeStep === 1
                    ? "Continue"
                    : activeStep === 2
                      ? "Create Workspace"
                      : "Continue"
              }
            </CustomButton>
          </div>
        </Paper>
      </Fragment>
    );
  }
}

export default compose(
  withRouter,
  withStyles(onBoardStyles, { withTheme: true }),
  connect(
    null,
    {
      createusercompany
    }
  )
)(CompanyInfo);
