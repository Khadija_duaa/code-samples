import React, { useEffect } from "react";
import queryString from "query-string";
import { withSnackbar } from "notistack";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter, Route } from "react-router-dom";
import { connect } from "react-redux";
import combinedStyles from "../../utils/mergeStyles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import CreateProfile from "./CreateProfileForm";
import onBoardStyles from "./styles";
import Paper from "@material-ui/core/Paper";
import CompanyInfo from "./CompanyInfoForm";
import CreateWorkspace from "./CreateWrokspaceForm";
import InviteMembers from "./InviteMembersForm";
import { authenticateUser } from "../../redux/actions/authentication";
import NoActiveWorkspace from "./NoActiveWorkspace";
import CreateTeam from "./CreateTeam";
import {
  ValidateToken,
  isUserValid,
  IsValidInvitedUser,
} from "./../../redux/actions/onboarding";

function OnBoard(props) {
  const parsedUrl = queryString.parse(location.search);
  useEffect(() => {
    if (parsedUrl.token && parsedUrl.code) {
      props.history.push("/invalid-invite");
    } else {
      props.history.push(
        "/welcome/createprofile?email=" +
          parsedUrl.email +
          "&code=" +
          parsedUrl.code
      );
    }
  }, []);
  return <></>;
}
const mapStateToProps = (state) => {
  return {
    tempDataState: state.tempData,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(combinedStyles(onBoardStyles)),
  connect(mapStateToProps, {
    ValidateToken,
    isUserValid,
    authenticateUser,
  })
)(OnBoard);
