import React, { Component } from "react";
import queryString from 'query-string';
import Dropzone from "react-dropzone";
import DeleteIcon from "@material-ui/icons/Cancel";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import CameraIcon from "@material-ui/icons/CameraAlt";
import { uploadprofileimage, getProfileImage } from "../../redux/actions/onboarding";
import { RemoveProfileImage } from "../../redux/actions/profileSettings";
import getErrorMessages from "../../utils/constants/errorMessages";
import { getImageExtensions } from '../../helper/getImageExtensions';

class DnDImageUpload extends Component {
  constructor() {
    super();
    this.state = {
      files: []
    };
  }

  componentDidMount() {
    // const parsed = queryString.parse(location.search);
    // this.props.getProfileImage(
    //   parsed.email,
    //   (response) => {
    //     const { data } = response;
    //     if (data && data !== 'default-male-image.png') {
    //       let request = new XMLHttpRequest();
    //       request.open('GET', `https://ntaskattachments.s3.amazonaws.com/ProfileImages/${data}`, true);
    //       request.responseType = 'blob';
    //       request.onload = () => {
    //         const file = new File([request.response], "profile.png", { type: "image/png" })
    //         this.setImagePreview(file);
    //       };
    //       request.send();
    //     }
    //   },
    //   (error) => {
    //     if (error.status === 500) {
    //       // this.props.showSnackBar('Server throws error', 'error');
    //     }
    //     else {
    //       // this.props.showSnackBar(error.data.message, 'error');
    //     }
    //   }
    // );
  }

  componentWillUnmount() {
    // Make sure to revoke the data uris to avoid memory leaks
    // this.state.files.forEach(file => URL.revokeObjectURL(file.preview));
  }

  setImagePreview = (file) => {
    let Picture = [];
    Picture.push(Object.assign(file, {
      preview: URL.createObjectURL(file)
    }));
    this.setState({
      files: Picture
    });
  }

  onDrop(files) {
    var file = files[0];
    var data = new FormData();
    const reader = new FileReader();
    if (files.length > 0) {
      let type = files[0].type.split("/")[1];
      if (
        type != null && getImageExtensions(type) ) {
        reader.onload = event => {
          let Picture = [];
          Picture.push(Object.assign(file, {
            preview: URL.createObjectURL(files[0])
          }));
          this.setState({
            files: Picture
          });
        };
        reader.readAsDataURL(files[0]);
        data.append("PrevImageName", null);
        data.append("UploadedImage", files[0]);
        const parsed = queryString.parse(location.search);
        data.append("email", parsed.email);
        let self = this;
        self.props.uploadprofileimage(data,
          (response) => {
            self.props.setPictureURL(response.data);
            self.setImagePreview(file);
          },
          error => {
            self.setState({ file: "", imagePreviewUrl: "" });
            this.props.showSnackBar(error.message, 'error');
          },false);
          
      }
      else {
        this.props.showSnackBar(getErrorMessages().INVALID_IMAGE_EXTENSION, 'error');
      }
    }
  }

  removeProfilePicture = () => {
    const parsed = queryString.parse(location.search);
    this.props.RemoveProfileImage(`?email=${parsed.email}`,
      data => {
        this.setState({ files: [] });
      });
  }

  render() {
    const { theme, classes } = this.props;
    const { files } = this.state;

    const thumbsContainer = {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap"
    };

    const thumb = {
      display: "inline-flex",
      border: 'none',
      width: "100%",
      height: "100%",
      boxSizing: "border-box"
    };

    const thumbInner = {
      display: "flex",
      minWidth: 0,
      overflow: "hidden"
    };

    const img = {
      display: "block",
      width: "auto",
      height: "100%",
      width: "100%"
    };

    const baseStyle = {
      width: "100%",
      height: "100%",
    };
    const activeStyle = {
      outline: '1px solid #6c6',
      backgroundColor: "#eee"
    };
    const rejectStyle = {
      outline: '1px solid #c66',
      backgroundColor: "#eee"
    };

    const thumbs =
      files.length > 0 ? (
        files.map(file => (
          <div style={thumb} key={file.name}>
            <div style={thumbInner}>
              <img src={file.preview} style={img} />
            </div>
          </div>
        ))
      ) : (
          <div className={classes.cameraIconCnt}>
            <CameraIcon
              classes={{ root: classes.cameraIcon }}
              htmlColor={this.props.theme.palette.secondary.light}
            />
            <p>Add Image</p>
          </div>
        );
    return (
      <section>
        <div style={thumbsContainer}>
          <Dropzone onDrop={this.onDrop.bind(this)}>
            {({
              getRootProps,
              getInputProps,
              isDragActive,
              isDragReject,
            }) => {
              let styles = { ...baseStyle };
              styles = isDragActive ? { ...styles, ...activeStyle } : styles;
              styles = isDragReject ? { ...styles, ...rejectStyle } : styles;

              return (
                <div {...getRootProps()} style={styles}>
                  <input {...getInputProps()} />
                  {thumbs}
                </div>
              );
            }}
          </Dropzone>
          {
            (files.length)
              ?
              <DeleteIcon
                htmlColor={theme.palette.error.main}
                classes={{ root: classes.deleteProfileImageIcon }}
                onClick={this.removeProfilePicture}
              />
              : null
          }
        </div>
      </section>
    );
  }
}

export default compose(
  withRouter,
  connect(
    null,
    {
      uploadprofileimage,
      getProfileImage,
      RemoveProfileImage
    }
  )
)(DnDImageUpload);
