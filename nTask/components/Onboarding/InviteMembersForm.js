import React, { Component, Fragment } from "react";
import orderBy from 'lodash/orderBy';
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import DefaultTextField from "../Form/TextField";
import IconButton from "@material-ui/core/IconButton";
import onBoardStyles from "./styles";
import Hotkeys from "react-hot-keys";
import Paper from "@material-ui/core/Paper";
import AddIcon from "@material-ui/icons/AddCircle";
import DeleteIcon from "@material-ui/icons/Cancel";
import Button from "@material-ui/core/Button";
import CustomButton from "../Buttons/CustomButton";
import { InviteMembers as sendinvitationemail, validateEmailAddress } from "./../../redux/actions/teamMembers";
import { validateEmailField } from '../../utils/formValidations';
import Tooltip from "@material-ui/core/Tooltip";
import {injectIntl} from "react-intl";
const EnvelopIcon =
  "data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ4My4zIDQ4My4zIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0ODMuMyA0ODMuMzsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSIxNnB4IiBoZWlnaHQ9IjE2cHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik00MjQuMyw1Ny43NUg1OS4xYy0zMi42LDAtNTkuMSwyNi41LTU5LjEsNTkuMXYyNDkuNmMwLDMyLjYsMjYuNSw1OS4xLDU5LjEsNTkuMWgzNjUuMWMzMi42LDAsNTkuMS0yNi41LDU5LjEtNTkuMSAgICB2LTI0OS41QzQ4My40LDg0LjM1LDQ1Ni45LDU3Ljc1LDQyNC4zLDU3Ljc1eiBNNDU2LjQsMzY2LjQ1YzAsMTcuNy0xNC40LDMyLjEtMzIuMSwzMi4xSDU5LjFjLTE3LjcsMC0zMi4xLTE0LjQtMzIuMS0zMi4xdi0yNDkuNSAgICBjMC0xNy43LDE0LjQtMzIuMSwzMi4xLTMyLjFoMzY1LjFjMTcuNywwLDMyLjEsMTQuNCwzMi4xLDMyLjF2MjQ5LjVINDU2LjR6IiBmaWxsPSIjMDAwMDAwIi8+CgkJPHBhdGggZD0iTTMwNC44LDIzOC41NWwxMTguMi0xMDZjNS41LTUsNi0xMy41LDEtMTkuMWMtNS01LjUtMTMuNS02LTE5LjEtMWwtMTYzLDE0Ni4zbC0zMS44LTI4LjRjLTAuMS0wLjEtMC4yLTAuMi0wLjItMC4zICAgIGMtMC43LTAuNy0xLjQtMS4zLTIuMi0xLjlMNzguMywxMTIuMzVjLTUuNi01LTE0LjEtNC41LTE5LjEsMS4xYy01LDUuNi00LjUsMTQuMSwxLjEsMTkuMWwxMTkuNiwxMDYuOUw2MC44LDM1MC45NSAgICBjLTUuNCw1LjEtNS43LDEzLjYtMC42LDE5LjFjMi43LDIuOCw2LjMsNC4zLDkuOSw0LjNjMy4zLDAsNi42LTEuMiw5LjItMy42bDEyMC45LTExMy4xbDMyLjgsMjkuM2MyLjYsMi4zLDUuOCwzLjQsOSwzLjQgICAgYzMuMiwwLDYuNS0xLjIsOS0zLjVsMzMuNy0zMC4ybDEyMC4yLDExNC4yYzIuNiwyLjUsNiwzLjcsOS4zLDMuN2MzLjYsMCw3LjEtMS40LDkuOC00LjJjNS4xLTUuNCw0LjktMTQtMC41LTE5LjFMMzA0LjgsMjM4LjU1eiIgZmlsbD0iIzAwMDAwMCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=";
const FoldedHand =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAXCAYAAAD6FjQuAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjlGNDVDMDU5RTdFRjExRThCNzBFQTk3OEZENkUwNDlEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjlGNDVDMDVBRTdFRjExRThCNzBFQTk3OEZENkUwNDlEIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OUY0NUMwNTdFN0VGMTFFOEI3MEVBOTc4RkQ2RTA0OUQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OUY0NUMwNThFN0VGMTFFOEI3MEVBOTc4RkQ2RTA0OUQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4pTeIIAAACpklEQVR42ryWyWsUQRTGa5luZ8vihBBG0EMEBRFBvIjiAvEgIiJBD0Y0F9E/wavg0ZMXQbwlEoWANxE9qBBcLoJiAqLmoChBxyQz07P1Wn7VNONMd3VPlJiCLzWp7Vf13qtXTYUQZKNKSv6hlP7VJLF4Rc47De2DdOgTNEO3364mzaPyZEkwLLwT1cFg0QXoBfQYOhYa+g66DN2C9kKr0D3oKjbRiIUFOx+FTkE3Qou+hvbH7K0G5UNtU4BNKmEAnUd1ExpaJ1e5UA5Ak4VONIHq7jqCZOFQUf5owxpzF0eJ8O39P0q+C4ZyjVAy0DMSXUHclqvs80yXeLan6sp1wYQnxlSTIzBPEHvVUsIstMv+RJjxZKJAGS2GR7RKphLm1B11JDTdOIMMt2EwzW6ms8hlsyu2elHTi29XJ6Q/AYJBjFC1D5R+c9QweWrp02RYw3GEHTV2jLPji4Spfba104zf7KpdW+ualCXkUvX+dnRG41fAuGd1j2QpFgUh21CuhlGMp6wHbPjCA49n+IfKfJmYP1t+YDSXmsR/fkT0VFRn8adOKfv6kJ2G2j08zccRJIbx2SCVhTKxVszvmLhc/xKyLpUBImIuvCeMj1XiGN1RbP4ya07NqXYl4tL0OM0UM0epxkq5I9Pz1Ufn7mDgJa1fI+mRNJyPu7fUtJyGo6eLGZLblmubVG5g5c1yA/7PBpsnPMv9u5fKa1P9x2cmE98zwJm+WX8Osx6SUQZTN7HoM0TpCf+QAGl9mu8MjHEB4pEsnOavCmdnD4RzY/Taw5cDJ+8fHtwzuCmzJTtSODObBWiuM09aZQsmt4gK5F+rlnt9zS+14rS7UL2FtI7mOvQ++Dz4Efwvc9pLbPjpP8MC4FjwCbAIPZSvNxZ1e97Pjfy6+i3AAIWURzOnpbLsAAAAAElFTkSuQmCC";
class InviteMembers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inviteEmailAddressErrorMessage: "",
      inviteEmailAddressError: false,
      inviteEmailAddress: "",
      inviteMemberList: [],
      teamId: null,
      btnQuery: ''
    };
    this.handleAddInviteMembers = this.handleAddInviteMembers.bind(this);
    this.handleRemoveMembers = this.handleRemoveMembers.bind(this);
    this.APIhandlerSendInvitation = this.APIhandlerSendInvitation.bind(this);
    this.navigateToGetstarted = this.navigateToGetstarted.bind(this);
  }
  componentDidMount() {
    const localStore = localStorage.getItem("InviteMembers");
    this.setState({ teamId: localStorage.getItem("teamId") });
    if (localStore) {
      this.setState({
        ...this.state, ...JSON.parse(localStore),
        inviteEmailAddressErrorMessage: "",
        inviteEmailAddressError: false
      });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state) {
      localStorage.setItem("InviteMembers", JSON.stringify(this.state));
    }
  }
  handleInput = fieldName => event => {
    let inputValue = event.currentTarget.value.trim();

    (inputValue === '') ? this.setState({ [fieldName]: inputValue, inviteEmailAddressError: false }) :
      this.setState({ [fieldName]: inputValue, [`${fieldName}Error`]: false, [`${fieldName}ErrorMessage`]: '', inviteEmailAddressError: false });
  };
  handleRemoveMembers(index, event) {
    let membersList = this.state.inviteMemberList.slice() || [];
    membersList = membersList.filter((obj, iterator) => iterator !== index);
    this.setState(prevState => ({
      inviteMemberList: membersList
    }));
  }
  addInMemberList = (mailValue) => {
    if (mailValue && mailValue !== '' && this.checkEmailValidation()) {
      const emailList = JSON.stringify(this.state.inviteMemberList);
      if (!emailList.includes(mailValue)) {
        let membersList = this.state.inviteMemberList.slice() || [];
        membersList.push({
          id: membersList.length,
          value: mailValue
        });
        // SORTING MEMBERS LIST
        membersList = orderBy(membersList, ['value'], ['asc']);
        this.setState(prevState => ({
          inviteMemberList: membersList,
          inviteEmailAddress: ''
        }));
      }
      else {
        this.setState(prevState => ({
          inviteEmailAddress: ''
        }));
      }
    }
  }
  emailAlreadyexist=(email)=>{
    let emailExist = this.state.inviteMemberList.find(e=>{
    return e.value==email
    })
    return emailExist ? true : false;
  }

  handleAddInviteMembers(event) {
    const { inviteEmailAddress } = this.state;
    let mailValue = inviteEmailAddress.trim();
    if (mailValue !== '') {
      if (!(this.emailAlreadyexist(mailValue))) {
        this.props.validateEmailAddress(mailValue,
          success => {
            this.addInMemberList(mailValue);
          }, error => {
            this.setState({ inviteEmailAddressError: true })
            this.props.showSnackBar(error.data, 'error');
          })
      } else { this.setState({ inviteEmailAddressError: true, inviteEmailAddressErrorMessage: 'Oops! Member already added in the invitation list.' }) }

    } else {
      this.setState({ inviteEmailAddressError: true, inviteEmailAddressErrorMessage: this.props.intl.formatMessage({id:"team-settings.user-management.invite-members.form.validation.invite.empty",defaultMessage:'Oops! Please enter email.'}) })
    }

  }
  handleBack = () => {
    this.props.history.push("/welcome/create-workspace");
  }
  checkEmailValidation() {
    let errorCheck_email = false;

    ////////////////////////////////////// EMAIL /////////////////////////////////////
    let mailValue = this.state.inviteEmailAddress.trim();
    let validationObj = validateEmailField(mailValue, false);
    if (validationObj['isError']) {
      this.setState({
        inviteEmailAddressError: validationObj['isError'],
        inviteEmailAddressErrorMessage: validationObj['message']
      });
    }
    else {
      errorCheck_email = true;
      this.setState({
        inviteEmailAddressError: false,
        inviteEmailAddressErrorMessage: ''
      });
    }
    return errorCheck_email;
  }
  APIhandlerSendInvitation() {
    let inviteEmails = [];
    this.state.inviteMemberList.map(obj => {
      inviteEmails.push(obj.value);
    });
    if (inviteEmails.length !== 0) {
      let object = {
        userEmails: inviteEmails,
      };
      this.setState({ btnQuery: "progress" });
      this.props.sendinvitationemail(
        object,
        (response) => {
          this.setState({ btnQuery: "" });
          if (response)
            this.props.history.replace(`/welcome/getstarted`);
          else if (response.status === 500) {
            this.props.showSnackBar('Server throws error', 'error');
          }
        },
        error => {
          this.setState({ btnQuery: "" })
          this.props.showSnackBar(error.data.message, 'error');
        }
      );
    } else if (this.checkEmailValidation()) {
      this.navigateToGetstarted();
    }
  }
  navigateToGetstarted = () => {
    localStorage.removeItem("CompanyInfo");
    localStorage.removeItem("CreateProfile");
    localStorage.removeItem("CreateWorkspace");
    localStorage.removeItem("InviteMembers");
    localStorage.removeItem("Onboarding");
    localStorage.removeItem("teamId");
    this.props.authAction(true);
    this.props.history.replace(`/welcome/getstarted`);
  }
  render() {
    const {
      inviteEmailAddress,
      inviteEmailAddressError,
      inviteEmailAddressErrorMessage,
      inviteMemberList,
      btnQuery
    } = this.state;
    const { classes, theme, activeStep } = this.props;
    let addBtnToolTip = inviteMemberList.length >= 4 ? 'Your team has reached maximum limit (4 users), upgrade to add more users.' : '';

    return (
      <Fragment>
        <div className={classes.innerPaddedCnt}>
          <div className={classes.profileHeadingCnt}>
            <Typography variant="h1" align="center">
              Who's on your team?&nbsp;
            <img src={FoldedHand} className={classes.clapIcon} />
            </Typography>
            <Typography variant="body1" align="center">
              Invite a few team members to your workspace.
          </Typography>
          </div>
          <Hotkeys keyName="enter" onKeyDown={this.handleAddInviteMembers}>
            <DefaultTextField
              label="Enter Email Address"
              fullWidth={true}
              errorState={inviteEmailAddressError}
              errorMessage={inviteEmailAddressErrorMessage}
              defaultProps={{
                id: "inviteEmailAddress",
                onChange: this.handleInput("inviteEmailAddress"),
                value: inviteEmailAddress,
                autoFocus: true,
                inputProps: { maxLength: 50 },
                startAdornment: (
                  <InputAdornment position="start">
                    <img src={EnvelopIcon} alt="EnvelopIcon" />
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position="end">
                    <Tooltip title={addBtnToolTip}>
                      <IconButton
                        disableRipple={true}
                        classes={{ 
                          root: classes.passwordVisibilityBtn,
                          disabled: classes.disableInvite
                        }}
                        onClick={this.handleAddInviteMembers}
                        disabled={inviteMemberList.length >= 4}
                      >
                        <AddIcon htmlColor={inviteMemberList.length >= 4 ? theme.palette.background.items : theme.palette.primary.light} />
                      </IconButton>
                    </Tooltip>
                  </InputAdornment>
                )
              }}
            />
          </Hotkeys>
          {inviteMemberList.map((list, index) => (
            <ul className={classes.inviteEmailList}>
              <li className={classes.inviteEmailListItem}>
                <div className={classes.inviteEmailListItemContent}>
                  <span className={classes.inviteUserIndex}>{index + 1}</span>
                  {list.value}
                </div>
                <IconButton
                  disableRipple={true}
                  classes={{ root: classes.passwordVisibilityBtn }}
                  onClick={event => this.handleRemoveMembers(index, event)}
                >
                  <DeleteIcon htmlColor={theme.palette.error.main} />
                </IconButton>
              </li>
            </ul>
          ))}
        </div>
        <Paper
          classes={{
            root: classes.buttonPaperCnt
          }}
        >
          <span className={classes.skipText} onClick={this.navigateToGetstarted}>
            Skip, I'll do it later
          </span>
          <div>
            <Button
              disabled={activeStep === 0}
              className={classes.button}
              onClick={this.handleBack}
            >
              Back
          </Button>

            <CustomButton
              onClick={this.APIhandlerSendInvitation}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress"}
            >
              {
                activeStep === 0
                  ? "Create Profile"
                  : activeStep === 1
                    ? "Continue"
                    : activeStep === 2
                      ? "Create Workspace"
                      : "Continue"
              }
            </CustomButton>
          </div>
        </Paper>
      </Fragment>
    );
  }
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(onBoardStyles, { withTheme: true }),
  connect(
    null,
    {
      sendinvitationemail,
      validateEmailAddress
    }
  )
)(InviteMembers);
