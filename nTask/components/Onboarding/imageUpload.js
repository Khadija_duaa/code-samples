import React from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import CameraIcon from "@material-ui/icons/CameraAlt";
import { uploadprofileimage } from "../../redux/actions/onboarding";
import getErrorMessages from '../../utils/constants/errorMessages';
class ImageUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = { file: "", imagePreviewUrl: "" };
  }

  _handleImageChange = (e) =>{
    e.preventDefault();
    var data = new FormData();
    let reader = new FileReader();
    let file = e.target;
    if (file.files.length > 0) {
      let type = file.files[0].type.split("/")[1];
      if (
        (type != null) &&
        (type.toLowerCase() == "jpg" ||
        type.toLowerCase() == "jpeg" ||
        type.toLowerCase() == "png" ||
        type.toLowerCase() == "gif")
      ) {
        reader.onloadend = () => {
          this.setState({
            file: file.files[0],
            imagePreviewUrl: reader.result
          });
        };
        reader.readAsDataURL(file.files[0]);
        data.append("PrevImageName", null);
        data.append("UploadedImage", file.files[0]);
        let self = this;
        self.props
          .uploadprofileimage(data,
            (response) => {
              self.props.setPictureURL(response.data);
            }, 
            (error) => {
              self.setState({ file: "", imagePreviewUrl: "" });
              this.props.showSnackBar(error.message, 'error');
            })
      }
      else{
        this.props.showSnackBar(getErrorMessages().INVALID_IMAGE_EXTENSION, 'error');
      }
    }
  }
  clickFile() {
    let fileinputbtn = document.getElementById("UploadedImage");
    fileinputbtn.click();
  }

  render() {
    const hideBtn = {
      display: "none"
    };
    const imgClass = {
      width: "100%",
      height: "100%"
    };
    const parentDiv = {
      height: "100%",
      width: "100%"
    };
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = <img src={imagePreviewUrl} style={imgClass} />;
    } else {
      $imagePreview = (
        <div className={this.props.classes.cameraIconCnt}>
          <CameraIcon
            classes={{ root: this.props.classes.cameraIcon }}
            htmlColor={this.props.theme.palette.secondary.light}
          />
          <p>Add Image</p>
        </div>
      );
    }

    return (
      <div onClick={e => this.clickFile(e)} style={parentDiv}>
        <div>
          <input
            style={hideBtn}
            className="fileInput"
            type="file"
            accept="image/*"
            id="UploadedImage"
            onChange={e => this._handleImageChange(e)}
          />
        </div>
        {$imagePreview}
      </div>
    );
  }
}
export default compose(
  withRouter,
  connect(
    null,
    {
      uploadprofileimage
    }
  )
)(ImageUpload);

// <div className="previewComponent">
//                 <form onSubmit={(e) => this._handleSubmit(e) }>
//                     <input className="fileInput"
//                         type="file"
//                         onChange={(e) => this._handleImageChange(e) } />
//                     <button className="submitButton"
//                         type="submit"
//                         onClick={(e) => this._handleSubmit(e) }>Upload Image</button>
//                 </form>
//                 <div className="imgPreview">
//                     {$imagePreview}
//                 </div>
//             </div>
