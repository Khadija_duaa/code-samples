const onBoardStyles = (theme) => ({
  stepperOuterCnt: {
    backgroundRepeat: "no-repeat",
    backgroundImage:
      "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHwAAACECAMAAABlAb/4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjg1MURGMTM2RTc0RjExRThCNjVDQzgyRDZFMThFMTk0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjg1MURGMTM3RTc0RjExRThCNjVDQzgyRDZFMThFMTk0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6ODUxREYxMzRFNzRGMTFFOEI2NUNDODJENkUxOEUxOTQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6ODUxREYxMzVFNzRGMTFFOEI2NUNDODJENkUxOEUxOTQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7crVGWAAABoVBMVEUAkP/6vkb+i3D////6vkb6vkb6vkb6vkb+i3D+i3D+i3AAkP/6vkb+i3D6vkb+i3D6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb+i3D6vkb6vkb+i3D6vkb6vkb6vkb+i3D6vkb6vkb6vkb6vkb6vkb+i3D+i3D6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkYAkP8AkP8AkP/6vkb6vkb6vkYAkP8AkP/6vkYAkP/6vkb6vkb+i3AAkP/+i3D+i3D+i3D6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb+i3D6vkb+i3D6vkb6vkb+i3D6vkb6vkb6vkb6vkb6vkYAkP/6vkb6vkb6vkb+i3D6vkb+i3D6vkb+i3D6vkb+i3D+i3D+i3D+i3D6vkb+i3D6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb+i3D+i3D6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb6vkb+i3D+i3D6vkb6vkb+i3AAkP/6vkb+i3DrEz/zAAAAiHRSTlMAAAAAAQYHCAgJChASEhMTFBUWFxgZGhwgIiMkJiYnKCgpLC0vMzQ3OD4/QENERVxdY2RlZmdpa2xub3BwcXJzdnd3gYKEiYqLkJGUlZaYmZmam5ucpKWmp6+wsbK8vr6/v8DCw8XGyMjJysvX2Nvc3d/g4eLj5Obn7e7v8PHy8/T29/n6+/3+uD852AAAA+ZJREFUeNrt2vk7G0EYB3C74qg7WjehzrhKpW6tqxRVdwWl1BVX0WpKEwQh8uav7qrnQdnZnd2ddyc9vr9vPs9uZmdn3nkjBNpYq5r7Z1w7Hh+Az7PjmulvrrJSXxwpmwiqa3ManXsgkz1nYw4unt+zDQrZ7snHwhOaXKAaV1MCAm4bPgOqnA3bGOO57wNAncBkLkM8ZfACNOViMIURbnnuBc3x1llY4JkLoCsLmcbxmiPQmaMag3j0GBjIWLQR/MkGGMpGmn68cA8MZq9QL273gOF47Prwch8wiK9cD17GxAY4LtOOFx0CoxwWacXT3cAs7gxteJQLGMYVpQkfA6aZ0ILXAOPU0OOZR6zxo0xa3PIRmGfBQonXAUIcdHjSDwzck0SFvwWUDNLguRc4+EUuBT4JSHGq47YAFh7IV8VHAS2janjKKR5+mqKCvwLEtKvgW5j4ljJeAqgpUcR7cfFeRXwbF99WwrMBOdkKeCM23qiAT2HjUwr4N2zcTcatgB4rEa/Cx58R8WZ8vJmI9+Pj/UT8Az4+S8Q38fFNIv4FH/9KxL34uJeIH+PjPiIOJiQ8ca6PneuA4/qqcZ1kuE6vJnxYBsLzk1rNczFhwjLqMXkB6ca23eG6dEbfNDSF63ZJ2MG1dxR3qW94bpGRiwOlHMsin1VqMu08C0JcS2Fci4BCPl7586l64deJhTt5lrxtNMX+QRz8Hd0xhwfD9tIdcwgODNxBe7S1wN6mPtrieqiHcJxZq+Ugd4KtrekgV4heZ7pHitZ2eJ/B8fCeb9uCINgZNWz47H9aq4p07wwqFV673vakIuPtSUX6G7PSjDZmpRtpSYsxNNtMxNz9LVEh7Jvxan//pRspr2Nu3+/fn+vIU8OFrEV99mKWIItXLIVuslShggsWh54GTMeDBsxfSuzQZehOLodilXFWradXSOJq6F5WE1VwQbBNamm6dcr2/EpG3FroQdbi1HBN7caEbmvJGA/JZFwdFwRrO8Uucqud2OkujbVLOfyyggKXUtqn3GLeV6pwsSguh2SzTIdfN9fLfuq/qzbXi8UhQopp8aukVrcNzK7vek4ATjy767MDbdWp6leJ3SS8WwuuL+I8Cf8k4ZG4EfdJ+L4JuJ+E+/92nOtj5zrgiK/aaxNwxUkGG1ecXtFxwoel0hRcHJHDR0RzcIXFBD4uJj/Q15JFs3Dx0f0FZJxoHi6NupVbe6XyZulsDi6KBV3zB+fnB/NdBXc2DWbhstulSI75j4crHnEbPnh8Szw//EWwnh/eEGzghMe3dE4HpzulJ88Bbw1e5+U/d+dc/3Mp9TxHe3wrx/ccZ4b7CZrl3a1QmNOSAAAAAElFTkSuQmCC')",
    backgroundPosition: "24px -13px",
    height: "100%",
    overflow: "auto",
    paddingBottom: 60,
  },
  noWorkspaceCnt: {
    height: "100%",
    overflow: "auto",
    paddingBottom: 60,
  },
  workspaceImg: {
    margin: "0 auto 40px auto",
    display: "block",
    width: 500,
  },
  stepperCnt: {
    width: "50%",
    maxWidth: 700,
    margin: "0 auto",
    backgroundImage:
      "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA+CAYAAABgFuiwAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkJBMTI0NkZCRTc1MDExRThCNTc2QTYzOTlFMzZCRkI5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkJBMTI0NkZDRTc1MDExRThCNTc2QTYzOTlFMzZCRkI5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QkExMjQ2RjlFNzUwMTFFOEI1NzZBNjM5OUUzNkJGQjkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QkExMjQ2RkFFNzUwMTFFOEI1NzZBNjM5OUUzNkJGQjkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5TxuCBAAADdklEQVR42tyaXUhUQRTHZ3MxKKqXiOzjwTDSogj73AcpKxCs0MCEgiKIMCIfCkmSKPrASAODiqiHHoqCkj6kyCjSXCFCKkGEipYw+rBegjKM1mr7H+5ZWLfdndndu3tn7oHfg945x/t3Zs49c+71hEIhkU0LndwT71IuqGR8YBoIgg+gF9wCbfy7/8xT1zLqZ6/QwzaAJlAQQ2whswkEQD24KQs4xmFBOeAE32iBwngac4N9chINdHrGGsG+FPzCPvU6zlhViqIixVXpJoz2TosNcU5xLG2EbQQzbIgzHVTrJKwy07GcErbExliLdRI21cZYeToJC2b6DzglbDDTsZwS1mdjrGc6Cbud6VhOCWvlqj1d+8ixtEoee22IQ2egX7rVivSfbk7DvznebOlwbNkPzqTgd5Z9tT2P/QG1XKUHFMYHuM7czb7xz2MVLxtsucO2osZ03OnweIdvmk7Ti7jADSeI59waaFV9uHt5830HA6AfdIH74LMDCeUKk7Z5+TwzmaGCchtPczv3IbrtvPvopku2SyrqJ6wDfnAXzBKGmUryWCus9tdmtwkjm8hr/6DbhIXtMDjgRmFkR4XVvHSdMLLzIF93YWPBbFADOhT9JoDTugsLcqlyAawGpeCVYrYsMWkpPgZLuQKRWZ1pe2wIVCjMXDmYYlry+Aa2g5CkJCs3MSs+AY8kY1aamu6vS67PM1WY7BGQb6qw9wrPNNdUHq6oFWdKrg+ZKmyV5PqAqcKqJdf7TRTm4/oxkXWZJmwSuAg8Ccb8BvdMEkYpnD7vKZT4U5vuiynCqETqASsU/Jt0TfdUxI7nlF7CiWKNoi8twW6dhf1IwY+eXbVu7HnsBG/dJuwQuOqGkipa1BETakXVz/poH9ZkY6ZGOsuyNmOU/RY6sPyoJbgDzLdzxv6yIHrP63dgJRUJ6/uNcfwztSfo/VM735uSsBFhNW7ecUHr5wCDDm6RsghRgutV4rWwvlG8BIZlwnI13PtPeWait8occA4cE1abnV6yf4oVwJPtz9OTSB50DqQ3O6UJhlMX+xov015TWgMdLK4YXBaxX6rTatsCXkSMN6bnQTOxVVjdsOPga5xxNLMPwTJThIWN9lIDF+y7OJHEenz5TBMWtmFOIHPBejG67/kTPNA9eSRjC8By0AneJFNS6W59Iurjzn8CDADfjbB7DVUo6wAAAABJRU5ErkJggg==')",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "94.5% 84%",
  },
  "@media (max-width: 1024px)": {
    stepperCnt: {
      width: "60%",
      maxWidth: "unset",
    },
  },
  "@media (max-width: 768px)": {
    stepperCnt: {
      width: "90%",
      maxWidth: "unset",
    },
  },
  btnsCnt: {
    justifyContent: "flex-end",
    display: "flex",
    padding: "0 30px",
  },
  paperCnt: {
    background: theme.palette.background.default,
    borderRadius: "4px",
    boxShadow: "none",
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  noTeamPaperCnt: {
    background: theme.palette.background.default,
    boxShadow: "none",
    border: "none",
  },
  innerPaddedCnt: {
    padding: "20px 30px 20px 30px",
  },
  stepperRoot: {
    background: "transparent",
    padding: "30px 5px",
  },
  stepperLabel: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
  },
  buttonPaperCnt: {
    background: theme.palette.background.default,
    padding: "20px 20px 20px 30px",
    borderRadius: "0 0 4px 4px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    boxShadow: "none",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
  },

  buttonPaperCntStatic: {
    background: theme.palette.background.default,
    padding: "20px 20px 20px 30px",
    borderRadius: "0 0 4px 4px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    boxShadow: "none",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
  },
  button: {
    marginRight: theme.spacing.unit,
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },

  passwordVisibilityBtn: {
    padding: 0,
    "&:hover": {
      background: "transparent",
    },
  },
  headerCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 20,
    maxWidth: 900,
    margin: "0 auto",
  },
  plainMenuItemIcon: {
    marginRight: 5,
    fontSize: "18px !important",
  },
  ntaskLogo: {
    width: 'auto',
    display: "block",
    maxWidth: 150
  },
  cameraIcon: {
    fontSize: "42px !important",
  },
  headingCnt: {
    marginBottom: 30,
  },
  profileHeadingCnt: {
    marginBottom: 50,
  },
  noWorkspaceHeading: {
    marginBottom: 40,
  },
  createWorkspaceBtnCnt: {
    textAlign: "center",
    marginTop: 20,
  },
  addImageBox: {
    background: theme.palette.background.paper,
    height: 110,
    width: 110,
    boxSizing: "border-box",
    margin: "0 auto 70px auto",
    textAlign: "center",
    borderRadius: 2,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",
    position: "relative",
    "& p": {
      fontSize: "12px !important",
      color: theme.palette.text.light,
      margin: "5px 0",
    },
    "&:hover $deleteProfileImageIcon": {
      display: "block",
    },
  },
  deleteProfileImageIcon: {
    position: "absolute",
    right: -8,
    top: -8,
    background: theme.palette.background.default,
    borderRadius: "100%",
    display: "none",
    cursor: "pointer",
  },
  cameraIconCnt: {
    padding: 20,
  },
  updatesCheckboxCnt: {
    margin: 0,
    width: 250,
    alignItems: "start",
  },
  updatesCheckbox: {
    padding: 0,
    marginRight: 10,
    "&$checked": {
      color: theme.palette.primary.light,
    },
  },
  checked: {},
  selectLabel: {
    transform: "translate(6px, -23px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  selectFormControl: {
    marginBottom: 15,
    marginTop: 30,
  },
  companySizeFormControl: {
    width: "100%",
  },
  formLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  workspaceUrlAdornment: {
    color: theme.palette.text.primary,
    fontSize: "14px !important",
    margin: 0,
  },
  globeIcon: {
    marginRight: 5,
  },
  urlAdornment: {
    marginRight: 6,
    // width: 315,
  },
  inviteEmailList: {
    listStyleType: "none",
    margin: 0,
    padding: 0,
  },
  inviteEmailListItem: {
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "10px 12px 10px 10px",
    marginBottom: 10,
  },
  inviteEmailListItemContent: {
    display: "flex",
    fontSize: "12px !important",
    alignItems: "center",
    justifyContent: "center",
  },
  inviteUserIndex: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
    width: 24,
    height: 24,
    display: "flex",
    fontSize: "13px !important",
    alignItems: "center",
    justifyContent: "center",
    color: theme.palette.secondary.dark,
    marginRight: 10,
  },
  selectAdornment: {
    position: "absolute",
    left: 14,
    top: 12,
  },
  skipText: {
    color: theme.palette.text.light,
    textDecoration: "underline",
    cursor: "pointer",
  },
  mainHeading: {
    marginBottom: 5,
    position: "relative",
  },
  clapIcon: {
    position: "absolute",
  },
  optionChecked: {},
  tooltip: {
    fontSize: "1rem",
    backgroundColor: "#f8f8f8",
    "& .panel-body": {
      padding: 10,
      color: "#000",
      "& ul": {
        padding: "6px 0 0 24px",
        "& $optionChecked": {
          color: "#01a52f",
          listStyle: "none",
          "&:before": {
            position: "absolute",
            left: 22,
            content: '"✓"',
          },
        },
        "& li": {
          color: "#656565",
        },
      },
    },
  },
  progressBar: {
    height: 12,
    marginBottom: 10,
    backgroundColor: "#dbdbdb",
  },
  redBar: {
    backgroundColor: "#d9534f",
  },
  orangeBar: {
    backgroundColor: "#f0ad4e",
  },
  greenBar: {
    backgroundColor: "#00CC90",
  },
  teamIcon: {
    fontSize: "30px !important",
  },
  disableInvite: {
    color: theme.palette.background.items,
  },
  invitationList: {
    listStyleType: "none",
    padding: 0,
    margin: "0 0 16px 0",
    display: "flex",
    flexDirection: "column",
    "& li": {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      padding: "12px 8px",
      borderBottom: `1px solid ${theme.palette.border.extraLightBorder}`,
    },
  },
  invitationDetailsCnt: {
    display: "flex",
    alignItems: "center",
  },
  invitationTeamDetails: {
    display: "flex",
    flexDirection: "column",
    marginLeft: 8,
  },
  inviteTeamText: {
    color: theme.palette.text.primary,
    display: "flex",
  },
  invitedByUser: {},
  inviteTeamText: {
    color: theme.palette.text.primary,
    display: "flex",
  },
  teamName: {
    color: theme.palette.text.azure,
    overflow: "hidden",
    textOverflow: "ellipsis",
    width: 130,
    whiteSpace: "nowrap",
    display: "inline-block",
  },
  headingBar: {
    background: theme.palette.background.items,
    padding: 12,
    borderRadius: 4,
  },
  invitationsCount: {
    color: theme.palette.text.light,
  },
  invitationBtnCnt: {
    display: "flex",
  },
});

export default onBoardStyles;
