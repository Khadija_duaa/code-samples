import React, { Component, Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Select, { components } from "react-select";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import DefaultTextField from "../Form/TextField";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import onBoardStyles from "./styles";
import CustomButton from "../Buttons/CustomButton";
import ImageUpload from "./imageUpload";
import Paper from "@material-ui/core/Paper";
import helper from "../../helper/index";
import DnDImageUpload from "./DnDImageUpload";
let moment = require("moment-timezone");
import queryString from "query-string";

import { AcceptTeamInvitation } from "./../../redux/actions/workspace";
import { countriesList } from "../../utils/constants/countriesList";
import {
  isUserNameUnique,
  updateExternalProfile
} from "./../../redux/actions/onboarding";
import { SetUserProfile } from "../../redux/actions/profile";
import {
  validateUserNameField,
  filterNameInputValue,
  filterUserNameInputValue,
  validateMultiWordNameField,
  validatePasswordField,
  validateUserNameAndEmailField
} from "../../utils/formValidations";
import { validEmail } from "../../utils/validator/common/email";
import { authenticatePassword } from "./../../redux/actions/authentication";
import { LOCK_ICON, PERSON_ICON } from "../../utils/constants/icons";
import { onboardingHelpText } from "../Tooltip/helptext";
import Tooltip from "@material-ui/core/Tooltip";
import LinearProgress from "@material-ui/core/LinearProgress";
import { validName } from "../../utils/validator/common/name";

const passwordStrenghts = [
  { item: "with lowercase alphabets", rgx: new RegExp("[a-z]") },
  { item: "with uppercase alphabets", rgx: new RegExp("[A-Z]") },
  { item: "with numbers", rgx: new RegExp("[0-9]") },
  { item: "with special characters", rgx: new RegExp("[$@$!%*#?&]") },
  { item: "8-40 characters", rgx: new RegExp("^[\\S|\\s]{8,40}$") }
];

class CreateProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstNameInput: "",
      lastNameInput: "",
      passwordInput: "",
      usernameInput: "",

      passwordError: false,
      usernameError: false,
      lastNameError: false,
      firstNameError: false,
      emailError: false,

      passwordErrorMessage: "",
      usernameErrorMessage: "",
      lastNameErrorMessage: "",
      firstNameErrorMessage: "",
      emailErrorMessage: "",

      pictureUrl: "",

      showTooltip: false,
      passwordStrength: { error: true, progress: 0 },

      showPassword: false,
      code: this.props.code,

      email: helper
        .HELPER_GETPARAMS(window.location.href)
        .email.includes("facebook")
        ? ""
        : helper.HELPER_GETPARAMS(window.location.href).email,
      firstLastName: this.props.firstName + " " + this.props.lastName,
      marketingCheckbox: true,
      btnQuery: "",

      country: {
        label: "United States",
        value: "US"
      }
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleClickShowPassword = this.handleClickShowPassword.bind(this);
    this.handleCreateProfileClick = this.handleCreateProfileClick.bind(this);
    this.handleUpdatesCheckbox = this.handleUpdatesCheckbox.bind(this);
  }
  componentDidMount = () => {
    this.props.firstName
      ? this.setState({ firstNameInput: this.props.firstName })
      : null;
    this.props.lastName
      ? this.setState({ lastNameInput: this.props.lastName })
      : null;
  };
  handleUpdatesCheckbox = event => {
    this.setState({ marketingCheckbox: event.target.checked });
  };

  validatePassword = password => {
    let passStrengthObj = { error: true, progress: 0 };

    // Check the conditions
    var ctr = 0;
    // Do not show anything when the length of password is zero.
    if (password.length > 0) {
      for (var i = 0; i < passwordStrenghts.length; i++) {
        if (passwordStrenghts[i]["rgx"].test(password)) {
          ctr++;
          passStrengthObj[passwordStrenghts[i]["item"]] = true;
        }
      }

      passStrengthObj.progress = (100 / passwordStrenghts.length) * ctr;
    }

    return passStrengthObj;
  };

  handleInput(event) {
    let fieldName = event.currentTarget.id,
      inputValue = event.currentTarget.value;
    switch (fieldName) {
      //////////////////////////////////// FIRSTNAME ///////////////////////////////////
      case "email":
        // inputValue = filterNameInputValue(inputValue);
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
            [fieldName]: inputValue,
            firstNameError: false,
            firstNameErrorMessage: ""
          });
        break;
      //////////////////////////////////// FIRSTNAME ///////////////////////////////////
      case "firstNameInput":
        // inputValue = filterNameInputValue(inputValue);
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
            [fieldName]: inputValue,
            firstNameError: false,
            firstNameErrorMessage: ""
          });
        break;
      //////////////////////////////////// LASTNAME ///////////////////////////////////
      case "lastNameInput":
        // inputValue = filterNameInputValue(inputValue);
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
            [fieldName]: inputValue,
            lastNameError: false,
            lastNameErrorMessage: ""
          });
        break;
      //////////////////////////////////// USERNAME ///////////////////////////////////
      case "usernameInput":
        inputValue = filterUserNameInputValue(inputValue);
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
            [fieldName]: inputValue,
            usernameError: false,
            usernameErrorMessage: ""
          });
        break;
      //////////////////////////////////// PASSWORD ////////////////////////////////////
      case "passwordInput":
        const passwordValidationObj = this.validatePassword(inputValue);
        inputValue === ""
          ? this.setState({
            [fieldName]: inputValue,
            passwordStrength: passwordValidationObj
          })
          : this.setState({
            [fieldName]: inputValue,
            passwordError: false,
            passwordErrorMessage: "",
            passwordStrength: passwordValidationObj
          });
        break;
    }
  }

  validNameInput = (key, value) => {
    /**Function for checking if the first name and last name is valid or not */
    let validationObj = validName(value, key);
    if (!validationObj.validated) {
      switch (key) {
        case "firstName":
          this.setState({
            firstNameError: true,
            firstNameErrorMessage: validationObj.errorMessage
          });
          break;
        case "lastName":
          this.setState({
            lastNameError: true,
            lastNameErrorMessage: validationObj.errorMessage
          });
          break;

        default:
          break;
      }
    }
    return validationObj.validated;
  };

  onClickValidationsChecker() {
    const { firstName, lastName, isExternalUser } = this.props;
    let noUserNameError = false;
    let noFirstNameError = false;
    let noLastNameError = false;
    let noPasswordError = false;

    let emailValue = this.state.email;

    let userNameValue = this.state.usernameInput.trim(); /** Username validation */
    let validationObj = validateUserNameField("Username", userNameValue);
    let validationEmailObj = validEmail(emailValue);
    /*********************Valied Email or Not************************ */
    // if (!validationEmailObj.validated) {
    //       this.setState({
    //         emailError: false,
    //         emailErrorMessage: ''
    //       });
    //     }
    //     else {
    //       this.setState({
    //         emailError: true,
    //         emailErrorMessage: validationEmailObj.errorMessage
    //       })
    //     }
    if (validationEmailObj.validated) {
      this.setState({
        emailError: false,
        emailErrorMessage: validationEmailObj.errorMessage
      });
    } else {
      this.setState({
        emailError: true,
        emailErrorMessage: validationEmailObj.errorMessage
      });
    }

    /****************************************************** */
    if (validationObj["isError"]) {
      this.setState({
        usernameError: validationObj["isError"],
        usernameErrorMessage: validationObj["message"]
      });
    } else {
      noUserNameError = true;
      this.setState({
        usernameError: false,
        usernameErrorMessage: ""
      });
    }

    let passwordValue = this.state.passwordInput; /** password validation */
    validationObj = validatePasswordField(passwordValue);
    if (validationObj["isError"] && isExternalUser) {
      this.setState({
        passwordError: validationObj["isError"],
        passwordErrorMessage: validationObj["message"]
      });
    } else {
      noPasswordError = true;
      this.setState({ passwordError: false, passwordErrorMessage: "" });
    }

    if ((firstName || lastName) && !isExternalUser) {
      (noFirstNameError = true), (noLastNameError = true);
    } else {
      noFirstNameError = this.validNameInput(
        "firstName",
        this.state.firstNameInput
      ); /** First Name validation */
      noLastNameError = this.validNameInput(
        "lastName",
        this.state.lastNameInput
      ); /** last Name validation */
    }

    if (
      noUserNameError &&
      noFirstNameError &&
      noLastNameError &&
      noPasswordError
    )
      return true;
    else return false;
  }
  addUserToWorkSpace = teamID => {
    if (isExternalUser) {
      if (noUserNameError && noFirstNameError && noLastNameError) {
        return true;
      } else {
        return false;
      }
    } else {
      if (
        noUserNameError &&
        noFirstNameError &&
        noLastNameError &&
        noPasswordError
      ) {
        return true;
      } else {
        return false;
      }
    }
  };
  addUserToWorkSpace = teamID => {
    this.props.AcceptTeamInvitation(
      teamID,
      data => {
        if (data && data.status === 200) {
          this.props.history.replace(`/welcome/getstarted`);
        } else {
          this.props.history.replace("/welcome/company");
        }
      },
      () => { }
    );
  };
  redirectToRegister() {
    let self = this;
    let teamID = helper.HELPER_GETPARAMS(window.location.href)
      ? helper.HELPER_GETPARAMS(window.location.href).token
      : undefined;
    let postData = {
      grant_type: "password",
      username: this.state.email,
      password: this.state.passwordInput
    };
    this.props.authenticatePassword(
      postData,
      data => {
        if (data && data.access_token) {
          self.setState({ isDerived: true });
          localStorage.setItem("token", `Bearer ${data.access_token}`);
          sessionStorage.setItem("token", `Bearer ${data.access_token}`);
          localStorage.setItem("zapkey", data.zapKey);
          localStorage.setItem("userName", data.userName);
          self.props.setActiveStep(1);
          if (!teamID) this.props.history.replace("/welcome/company");
          else this.addUserToWorkSpace(teamID);
        }
        this.handleGoogleAnalytics();
      },
      err => {
        this.setState({ btnQuery: "" });
        let error = err.response.data.error_description
          ? err.response.data.error_description
          : null;
        self.props.showSnackBar(error, "error");
      }
    );
  }
  setPictureURL = URL => {
    this.setState({
      pictureUrl: URL ? URL : "",
      apiPictureURL: URL ? URL : null
    });
  };
  //Function that handles where user has signed up from
  handleGoogleAnalytics = () => {
    if (window.ga) { // Check is manadatory, incase user is unable to connect to google analytics, ga won't be available and it will through error
      let parseProvider = queryString.parse(this.props.location.search).provider;
      let provider = parseProvider ? parseProvider : "Web";
      ga("create", "UA-96014584-2", "auto");
      switch (provider) {
        case "Facebook":
          ga("send", "event", {
            eventCategory: "Registration",
            eventAction: "Sign Up",
            eventLabel: "Facebook Sign Up"
          });
          break;
        case "Google":
          ga("send", "event", {
            eventCategory: "Registration",
            eventAction: "Sign Up",
            eventLabel: "Google Sign Up"
          });
          break;
        case "Twitter":
          ga("send", "event", {
            eventCategory: "Registration",
            eventAction: "Sign Up",
            eventLabel: "Twitter Sign Up"
          });
          break;
        case "Web":
          ga("send", "event", {
            eventCategory: "Registration",
            eventAction: "Sign Up",
            eventLabel: "Web Sign Up"
          });
          break;

        default:
          break;
      }
    }
  };
  handleSetPasswordAPI(object) {
    this.props.SetUserProfile(
      object,
      data => {
        this.redirectToRegister();
      },
      err => {
        this.setState({ btnQuery: "" });
        this.props.showSnackBar(err.data.message, "error");
      }
    );
  }
  handleAPI() {
    let self = this;
    let q = queryString;
    this.setState({ btnQuery: "progress" });
    self.props.isUserNameUnique(
      self.state.usernameInput,
      responce => {
        if (responce.status === 200 && responce.data) {
          self.setState({ usernameError: false, usernameErrorMessage: null });
          let object = {
            firstName: self.state.firstNameInput.trim(),
            lastName: self.state.lastNameInput.trim(),
            email: self.state.email.trim(),
            OldEmail: helper.HELPER_GETPARAMS(window.location.href).email,
            password: self.state.passwordInput,
            confirmPassword: self.state.passwordInput,
            code: self.state.code,
            timeZone: moment.tz.guess(),
            startDayOfWeek: 1,
            pictureUrl: self.state.pictureUrl,
            country: self.state.country.value,
            username: self.state.usernameInput.trim(),
            IsSubscriptionEnabled: self.state.marketingCheckbox
          };
          // EXTERNAL USER CREATE PROFILE (SOCIAL LINK USER)
          if (self.props.isExternalUser) {
            object.password = "xxxxxxxx";
            object.confirmPassword = "xxxxxxxx";
            self.props.updateExternalProfile(object, data => {
              this.setState({ btnQuery: "" });
              //const access_token = data.data.access_token;
              if (data) {
                const access_token = data.data.access_token;
                if (data.status === 200 && access_token) {
                  localStorage.setItem("token", `Bearer ${access_token}`);
                  sessionStorage.setItem("token", `Bearer ${access_token}`);
                  self.props.history.push("/welcome/company");
                  self.props.setActiveStep(1);
                } else {
                  self.props.showSnackBar("Unable to move forward");
                }
              }
              this.handleGoogleAnalytics();
            });
          }
          // nTask USER CREATE PROFILE
          else {
            self.handleSetPasswordAPI(object);
          }
        }
      },
      error => {
        self.setState({
          btnQuery: "",
          usernameError: true,
          usernameErrorMessage: error.message
        });
      }
    );
  }
  handleCreateProfileClick() {
    let response = this.onClickValidationsChecker();
    // if (!this.props.isExternalUser && response) {
    //   if (this.state.passwordStrength.progress < 100) {
    //     this.setState({
    //       passwordError: true,
    //       passwordErrorMessage: "Oops! Password should be more complex"
    //     });
    //   } else {
    //     this.handleAPI();
    //   }
    // } else {
    //   this.handleAPI();
    // }
    this.handleAPI();
  }

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };
  componentDidMount() {
    const localStore = localStorage.getItem("CreateProfile");
    if (localStore) {
      this.setState({ ...this.state, ...JSON.parse(localStore) });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state) {
      localStorage.setItem("CreateProfile", JSON.stringify(this.state));
    }
  }

  handleCountryChange = value => {
    this.setState({ country: value });
  };

  showTooltip = () => {
    this.setState({ showTooltip: true });
  };

  hideTooltip = () => {
    this.setState({ showTooltip: false });
  };

  render() {
    const {
      emailError,
      emailErrorMessage,
      firstNameInput,
      lastNameInput,
      usernameInput,
      passwordInput,
      passwordError,
      usernameError,
      lastNameError,
      firstNameError,
      passwordErrorMessage,
      usernameErrorMessage,
      lastNameErrorMessage,
      firstNameErrorMessage,
      showPassword,
      marketingCheckbox,
      btnQuery,
      country,
      passwordStrength,
      showTooltip
    } = this.state;
    const {
      classes,
      theme,
      activeStep,
      isExternalUser,
      isUserNotSubscribed,
      firstName
    } = this.props;

    const DropdownIndicator = props => {
      return (
        components.DropdownIndicator && (
          <components.DropdownIndicator {...props}>
            <DropdownArrow htmlColor={theme.palette.secondary.light} />
          </components.DropdownIndicator>
        )
      );
    };

    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: 0,
        fontSize: "12px",
        minHeight: 41,
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: "none",
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        }
      }),
      input: (base, state) => ({
        color: theme.palette.text.secondary
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "0 0 0 14px"
      }),
      multiValue: (base, state) => ({
        ...base,
        background: theme.palette.background.light,
        padding: "5px 5px",
        borderRadius: 20
      }),
      multiValueLabel: (base, state) => ({
        ...base,
        marginRight: 10
        //background: "red"
      }),
      multiValueRemove: (base, state) => ({
        ...base,
        background: theme.palette.background.contrast,
        borderRadius: "50%",
        color: theme.palette.common.white,
        ":hover": {
          background: theme.palette.error.light,
          color: theme.palette.common.white
        }
      })
    };

    return (
      <Fragment>
        <div className={classes.innerPaddedCnt}>
          <div className={classes.headingCnt}>
            <Typography
              variant="h1"
              align="center"
              classes={{ root: classes.mainHeading }}
            >
              {isExternalUser
                ? `Welcome ${firstName},`
                : firstName
                  ? `Welcome ${firstName},`
                  : `Welcome`}
            </Typography>
            <Typography variant="body1" align="center">
              Let's set up your profile!
            </Typography>
          </div>
          <div className={classes.addImageBox}>
            <DnDImageUpload
              theme={theme}
              classes={classes}
              setPictureURL={this.setPictureURL}
              showSnackBar={this.props.showSnackBar}
            />
          </div>

          {(this.props.firstName || this.props.lastName) &&
            !isExternalUser ? null : (
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <DefaultTextField
                    label="First Name"
                    fullWidth={true}
                    errorState={firstNameError}
                    errorMessage={firstNameErrorMessage}
                    formControlStyles={{ marginBottom: 34 }}
                    defaultProps={{
                      id: "firstNameInput",
                      placeholder: "First Name",
                      onChange: this.handleInput,
                      value: firstNameInput,
                      inputProps: { maxLength: 40, tabIndex: 1 },
                      startAdornment: (
                        <InputAdornment position="start">
                          <img src={PERSON_ICON} width={18} height={18} />
                        </InputAdornment>
                      )
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <DefaultTextField
                    label="Last Name"
                    fullWidth={true}
                    errorState={lastNameError}
                    errorMessage={lastNameErrorMessage}
                    formControlStyles={{ marginBottom: 34 }}
                    defaultProps={{
                      id: "lastNameInput",
                      placeholder: "Last Name",
                      value: lastNameInput,
                      onChange: this.handleInput,
                      inputProps: { maxLength: 40, tabIndex: 2 },
                      startAdornment: (
                        <InputAdornment position="start">
                          <img src={PERSON_ICON} width={18} height={18} />
                        </InputAdornment>
                      )
                    }}
                  />
                </Grid>
              </Grid>
            )}

          {isExternalUser ? (
            <DefaultTextField
              label="Email"
              fullWidth={true}
              errorState={emailError}
              errorMessage={emailErrorMessage}
              defaultProps={{
                id: "email",
                placeholder: "Email Address",
                //value: this.props.email,
                value: this.state.email,
                // value:this.state.email.includes("facebook")?:""

                disabled: this.props.isEnabled ? true : false,
                onChange: this.handleInput
              }}
            />
          ) : null}

          <DefaultTextField
            label="Username"
            helptext={onboardingHelpText.usernameHelpText}
            fullWidth={true}
            errorState={usernameError}
            errorMessage={usernameErrorMessage}
            formControlStyles={{ marginBottom: 34 }}
            defaultProps={{
              id: "usernameInput",
              placeholder: "Username",
              onChange: this.handleInput,
              value: usernameInput,
              inputProps: { maxLength: 50, tabIndex: 3 },
              endAdornment: (
                <InputAdornment>
                  <IconButton
                    disableRipple={true}
                    classes={{ root: classes.passwordVisibilityBtn }}
                  ></IconButton>
                </InputAdornment>
              ),
              startAdornment: (
                <InputAdornment position="start">
                  <img src={PERSON_ICON} width={18} height={18} />
                </InputAdornment>
              )
            }}
          />

          {isExternalUser ? null : (
            <Tooltip
              interactive
              open={showTooltip}
              disableFocusListener
              disableHoverListener
              disableTouchListener
              classes={{
                tooltip: classes.tooltip
              }}
              title={
                <div className="panel-body">
                  <LinearProgress
                    classes={{
                      root: classes.progressBar,
                      bar: `${
                        passwordStrength.progress <= 40
                          ? classes.redBar
                          : passwordStrength.progress <= 80
                            ? classes.orangeBar
                            : classes.greenBar
                        }`
                    }}
                    variant="determinate"
                    value={passwordStrength.progress}
                  />
                  <div>A good password is :</div>
                  <ul>
                    {passwordStrenghts.map((pStrength, i) => {
                      return (
                        <li
                          key={i}
                          className={`${
                            passwordStrength[pStrength.item]
                              ? classes.optionChecked
                              : ""
                            }`}
                        >
                          <small>{pStrength.item}</small>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              }
              placement="right"
            >
              <DefaultTextField
                label="Password"
                helptext={onboardingHelpText.passwordHelpText}
                fullWidth={true}
                errorState={passwordError}
                errorMessage={passwordErrorMessage}
                formControlStyles={{ marginBottom: 34 }}
                defaultProps={{
                  id: "passwordInput",
                  placeholder: "Password",
                  type: this.state.showPassword ? "text" : "password",
                  onChange: this.handleInput,
                  onFocus: this.showTooltip,
                  onBlur: this.hideTooltip,
                  value: passwordInput,
                  inputProps: { maxLength: 40 },
                  startAdornment: (
                    <InputAdornment position="start">
                      <img
                        src={LOCK_ICON}
                        alt="LockIcon"
                        width={18}
                        height={18}
                      />
                    </InputAdornment>
                  ),
                  endAdornment: (
                    <Fragment>
                      <InputAdornment>
                        <IconButton
                          disableRipple={true}
                          classes={{ root: classes.passwordVisibilityBtn }}
                          onClick={this.handleClickShowPassword}
                          tabIndex={5}
                        >
                          {showPassword ? (
                            <Visibility
                              htmlColor={theme.palette.secondary.light}
                            />
                          ) : (
                              <VisibilityOff
                                htmlColor={theme.palette.secondary.light}
                              />
                            )}
                        </IconButton>
                      </InputAdornment>
                      <InputAdornment>
                        <IconButton
                          disableRipple={true}
                          classes={{ root: classes.passwordVisibilityBtn }}
                        ></IconButton>
                      </InputAdornment>
                    </Fragment>
                  )
                }}
              />
            </Tooltip>
          )}

          <FormControl
            fullWidth={true}
            classes={{ root: classes.reactSelectFormControl }}
          >
            <InputLabel
              htmlFor="country"
              classes={{
                root: classes.selectLabel
              }}
              shrink={false}
            >
              Country
            </InputLabel>
            <Select
              styles={customStyles}
              onChange={this.handleCountryChange}
              inputId="country"
              options={countriesList}
              components={{ DropdownIndicator, IndicatorSeparator: false }}
              value={country}
            />
          </FormControl>
        </div>
        <Paper
          classes={{
            root: classes.buttonPaperCntStatic
          }}
        >
          {isExternalUser || isUserNotSubscribed ? (
            <FormControlLabel
              classes={{ root: classes.updatesCheckboxCnt }}
              control={
                <Checkbox
                  checked={marketingCheckbox}
                  onChange={this.handleUpdatesCheckbox}
                  tabIndex={6}
                  classes={{
                    root: classes.updatesCheckbox,
                    checked: classes.checked
                  }}
                />
              }
              label="I agree to receive product updates, offers and promotions"
            />
          ) : (
              <div></div>
            )}

          <div>
            <CustomButton
              onClick={this.handleCreateProfileClick}
              tabIndex={7}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress"}
            >
              {activeStep === 0
                ? "Create Profile"
                : activeStep === 1
                  ? "Continue"
                  : activeStep === 2
                    ? "Create Workspace"
                    : "Continue"}
            </CustomButton>
          </div>
        </Paper>
      </Fragment>
    );
  }
}

//export default withStyles(onBoardStyles, { withTheme: true })(CreateProfile);
export default compose(
  withRouter,
  withStyles(onBoardStyles, { withTheme: true }),
  connect(null, {
    isUserNameUnique,
    authenticatePassword,
    updateExternalProfile,
    AcceptTeamInvitation,
    SetUserProfile
  })
)(CreateProfile);
