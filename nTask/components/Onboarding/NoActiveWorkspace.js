import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import onBoardStyles from "./styles";
import CustomButton from "../Buttons/CustomButton";
import SignOutIcon from "@material-ui/icons/PowerSettingsNew";

import {
  IsWorkspaceUrlValid,
  addCompanyInformation,
} from "../../redux/actions/onboarding";
import { WorkspaceExists } from "../../redux/actions/workspace";
import NoWorkspaceImage from "../../assets/images/workspace_illustration.svg";
import ntaskWorkLogo from "../../assets/images/nTask-Logo.svg";
import Paper from "@material-ui/core/Paper";
import { PopulateTempData } from "../../redux/actions/tempdata";
import { Logout } from "../../redux/actions/logout";
import CreateTeamForm from "../../Views/AddNewForms/addNewTeamFrom";
import {
  getTeamInvitations,
  acceptTeamInvitation,
  rejectTeamInvitation,
} from "../../redux/actions/teamInvitations";
import CustomAvatar from "../../components/Avatar/Avatar";
import axios from "axios";

import isEmpty from "lodash/isEmpty";
import constants from "../../redux/constants/types";
import {
  FetchUserInfo,
} from "../../redux/actions/profile";
import { PopulateWhiteLabelInfo } from "../../redux/actions/teamMembers";
import { store } from "../..";

class NoActiveWorkspace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      createTeam: false,
      noTeamFound: true,
      pendingInvitations: [],
      companyData: {},
      showLogo: false
    };
  }

  handleCreateWorkspace = () => {
    this.setState({ createTeam: true, noTeamFound: false });
    // this.props.PopulateTempData("Enter-WorkSpace");
    // this.props.history.push("/welcome/create-workspace");
  };

  onBackClick = () => {
    this.setState({ createTeam: false, noTeamFound: true });
  };
  getCompanyData = (companyName, isAfterSuccess = false) => {
    this.setState({ showLogo: false });
    axios.get(`${constants.WHITELABELURL}?brandUrl=${companyName}`)
      .then(response => {
        if (response.data.success) {
          this.setState({ companyData: response.data.data });
          // store.dispatch(PopulateWhiteLabelInfo(response.data.data));
        } else {
          this.setState({ companyData: {} });
        }
        this.setState({ showLogo: true });
        localStorage.removeItem("companyName");
      })
      .catch(err => {
        this.setState({ companyData: {} });
        this.setState({ showLogo: true });
        localStorage.removeItem("companyName");
      });
  };
  getCompanyDataWithUrl = () => {
    const data = { "domain": window.location.origin }
    axios.post(constants.WHITELABELURL, data)
      .then(response => {
        if (response.data.success) {
          this.setState({ companyData: response.data.data });

        } else {
          this.setState({ companyData: {} });

        }
        this.setState({ showLogo: true });
        localStorage.removeItem("companyName");
      }).catch(err => {
        this.setState({ companyData: {} });
        this.setState({ showLogo: true });
        localStorage.removeItem("companyName");
      });
  }
  getUserInfor = () => {
    this.props.FetchUserInfo(data => {
      if (data.data.teams.length) {
        this.props.history.push("/task");
      }
    });
  }
  componentDidMount() {
    // if (window.location.host == 'app.totalenvironment.com' || window.location.host == 'uat.naxxa.io' || window.location.host == 'sqa.naxxa.io' || window.location.host == 'dev.naxxa.io' || window.location.host == 'localhost:3001') {
    //   this.getCompanyData('totalenvironment');
    // } else {
    //   this.setState({ showLogo: true });
    // }
    this.getUserInfor();
    this.getCompanyDataWithUrl();
    getTeamInvitations(
      //Success
      (response) => {
        const pendingInvitationsList = response.data.filter((i) => {
          // Extracting pending invitations list
          return i.status == "Pending";
        });
        this.setState({ pendingInvitations: pendingInvitationsList });
      },
      //Failure
      () => { }
    );
  }
  logoutUser = () => {
    this.props.Logout(null, this.props.history);
  };
  handleAcceptTeamInvitation = (invitation) => {
    // Function that handles accept invitation of team
    this.props.acceptTeamInvitation(
      invitation.team.teamId,
      (response) => {
        this.props.history.push("/teams/");
      },
      () => { },
      true
    );
    this.setState({
      pendingInvites: {
        ...invitation,
        status: "Accepted",
        actionDate: new Date(),
      },
    }); // Updating local state after invitation is accepted
  };
  handleRejectTeamInvitation = (invitation) => {
    // Function that handles accept invitation of team
    this.props.rejectTeamInvitation(
      invitation.team.teamId,
      (response) => { },
      () => { },
      true
    );
    this.setState({
      pendingInvites: {
        ...invitation,
        status: "Rejected",
        actionDate: new Date(),
      },
      pendingInvitations: this.state.pendingInvitations.filter((i) => {
        return i.team.teamId !== invitation.team.teamId;
      })
    });
    // Updating local state after invitation is accepted
  };
  renderNoTeamFound = () => {
    const { classes, theme } = this.props;
    const { pendingInvitations, companyData } = this.state;
    return (
      <>
        <div className={classes.innerPaddedCnt}>
          <div className={classes.noWorkspaceHeading}>
            <Typography
              variant="h1"
              align="center"
              classes={{ root: classes.mainHeading }}
            >
              No Active Team Found
            </Typography>
            <Typography variant="body1" align="center">
              Oops! you no longer have an active Team
            </Typography>
          </div>
          {pendingInvitations.length ? (
            <div className={classes.headingBar}>
              <Typography variant="h5">
                Pending Invites&nbsp;
                <span className={classes.invitationsCount}>
                  <>({pendingInvitations.length})</>
                </span>
              </Typography>
            </div>
          ) : null}
          {pendingInvitations.length ? (
            <ul className={classes.invitationList}>
              {pendingInvitations.map((m) => {
                return (
                  <li>
                    <div className={classes.invitationDetailsCnt}>
                      <CustomAvatar
                        otherMember={{
                          imageUrl: m.invitedBy.imageUrl,
                          fullName: m.invitedBy.fullName,
                          lastName: "",
                          email: m.invitedBy.fullName,
                        }}
                        size="xmedium"
                        disableCard={true}
                      />
                      <div className={classes.invitationTeamDetails}>
                        <Typography
                          variant="h5"
                          className={classes.invitedByUser}
                        >
                          {m.invitedBy.fullName}
                        </Typography>
                        <Typography
                          variant="body2"
                          className={classes.inviteTeamText}
                        >
                          <span>invited you to join team&nbsp;</span>
                          <span className={classes.teamName}>
                            {m.team.teamName}
                          </span>
                        </Typography>
                      </div>
                    </div>
                    <div className={classes.invitationBtnCnt}>
                      <CustomButton
                        onClick={() => this.handleRejectTeamInvitation(m)}
                        style={{
                          marginRight: 16,
                        }}
                        btnType="gray"
                        variant="outlined"
                      >
                        Reject
                      </CustomButton>
                      <CustomButton
                        onClick={() => this.handleAcceptTeamInvitation(m)}
                        btnType="success"
                        variant="contained"
                        style={{
                          background: !isEmpty(companyData) && companyData.customPrimaryColor ? companyData.customPrimaryColor : null
                        }}
                      >
                        Accept
                      </CustomButton>
                    </div>
                  </li>
                );
              })}
            </ul>
          ) : null}
          {pendingInvitations.length ? (
            <div className={classes.headingBar}>
              <Typography variant="h5">Create Team</Typography>
            </div>
          ) : null}
          <div className={classes.createWorkspaceBtnCnt}>
            <CustomButton
              onClick={this.handleCreateWorkspace}
              btnType="success"
              variant="contained"
              style={{
                background: !isEmpty(companyData) && companyData.customPrimaryColor ? companyData.customPrimaryColor : null
              }}
            >
              Create New Team
            </CustomButton>
          </div>
        </div>
      </>
    );
  };

  createTeam = () => {
    const { createTeam } = this.state;
    const { classes } = this.props;
    if (createTeam) {
      return (
        <>
          <div className={classes.noWorkspaceHeading}>
            <Typography
              variant="h1"
              align="center"
              classes={{ root: classes.mainHeading }}
            >
              No Active Team Found
            </Typography>
            <Typography variant="body1" align="center">
              Oops! you no longer have an active Team
            </Typography>
          </div>
          <CreateTeamForm
            onBackClick={this.onBackClick}
            noTeamFound={createTeam}
            disableActionBtnBg={true}
          />
        </>
      );
    }
  };

  render() {
    const { classes, theme } = this.props;
    const { noTeamFound, companyData, showLogo } = this.state;

    return (
      <>
        <div className={classes.noWorkspaceCnt}>
          <div className={classes.headerCnt}>
            {" "}

            {showLogo && <img
              src={!isEmpty(companyData) ? companyData.signinLogoUrl : ntaskWorkLogo}
              alt="Logo Image"
              className={classes.ntaskLogo}
            />}
            <CustomButton
              style={{ marginRight: "20px" }}
              onClick={this.logoutUser}
              btnType="gray"
              variant="contained"
            >
              <SignOutIcon
                htmlColor={theme.palette.secondary.light}
                classes={{ root: classes.plainMenuItemIcon }}
              />
              Sign Out
            </CustomButton>
          </div>
          <div className={classes.stepperCnt}>
            <Paper classes={{ root: classes.noTeamPaperCnt }}>
              <img
                src={NoWorkspaceImage}
                alt="Workspace Image"
                className={classes.workspaceImg}
              />
              {noTeamFound
                ? this.renderNoTeamFound() /** function calling for no team found */
                : this.createTeam()}
            </Paper>
          </div>
        </div>
      </>
    );
  }
}

export default compose(
  withRouter,
  withStyles(onBoardStyles, { withTheme: true }),
  connect(null, {
    IsWorkspaceUrlValid,
    acceptTeamInvitation,
    rejectTeamInvitation,
    addCompanyInformation,
    PopulateTempData,
    WorkspaceExists,
    Logout,
    FetchUserInfo,
  })
)(NoActiveWorkspace);
