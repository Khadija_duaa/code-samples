import React, { Component, Fragment } from "react";
import Button from "@material-ui/core/Button";
import CustomButton from "../Buttons/CustomButton";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Paper from "@material-ui/core/Paper";
import onBoardStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import DefaultTextField from "../Form/TextField";
import { WORKSPACE_ICON } from "../../utils/constants/icons";
import TeamIcon from "@material-ui/icons/SupervisorAccount";
import GlobeIcon from "@material-ui/icons/Language";
import { addTeam } from "./../../redux/actions/onboarding";
import { FormattedMessage, injectIntl } from "react-intl";

class CreateTeam extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teamNameBind: true,
    };
  }
  handleBack = () => {
    this.props.handleBack(1);
    this.props.history.push("/welcome/company");
  };
  handleInput = (fieldName) => (event) => {
    let inputValue = event.target.value;
    const { teamNameBind } = this.state;
    //* Replacing all spaces with Dash from team url
    let teamUrl = inputValue.replace(/[\s\+]+/g, "-").toLowerCase();
    switch (fieldName) {
      case "teamName": {
        if (teamNameBind) {
          this.setState({
            [fieldName]: inputValue,
            teamError: false,
            teamErrorMessage: "",
            teamNameBind: true,
            teamUrl: teamUrl,
          });
        } else {
          /** */
          this.setState({
            [fieldName]: inputValue,
            teamError: false,
            teamErrorMessage: "",
          });
        }
        break;
      }
      case "teamUrl": {
        //Disallowing user to enter space in url
        let newTeamUrl = inputValue.replace(/ /g, "");
        this.setState({
          [fieldName]: newTeamUrl,
          teamUrlError: false,
          teamUrlErrorMessage: "",
        });
        if (this.state.teamNameBind) {
          this.setState({ teamNameBind: false });
        }
      }
    }
  };
  handleCreateTeam = () => {
    const { teamName, teamUrl } = this.state;
    const teamObj = {
      CompanyName: teamName,
      CompanyUrl: teamUrl,
    };
    addTeam(
      teamObj,
      //Function to be called in case of success
      () => {
        this.props.history.push("/welcome/create-workspace");
      },
      (err) => {
        if (err.data.message == "Team name already exists.") {
          this.setState({
            btnQuery: "",
            teamError: true,
            teamErrorMessage: this.props.intl.formatMessage({
              id: "common.team.validations.nameexists",
              defaultMessage: err.data.message,
            }),
          });
        } else {
          this.setState({
            btnQuery: "",
            teamUrlError: true,
            teamUrlErrorMessage: err.data.message,
          });
        }
      }
    );
  };
  render() {
    const { activeStep, theme, classes } = this.props;
    const {
      btnQuery = "",
      teamError = false,
      teamErrorMessage = "",
      teamName = "",
      teamUrl = "",
      teamUrlError,
      teamUrlErrorMessage,
    } = this.state;
    const disabledButtonAction = teamName == "" && teamUrl == "" ? true : false;
    return (
      <Fragment>
        <div className={classes.innerPaddedCnt}>
          <div className={classes.profileHeadingCnt}>
            <Typography
              variant="h1"
              align="center"
              classes={{ root: classes.mainHeading }}
            >
              <FormattedMessage
                id="common.team.new-team.label2"
                defaultMessage="Create your first team"
              />
              {/* <img src={HANDS_ICON} className={classes.clapIcon} /> */}
            </Typography>
            <Typography variant="body1" align="center">
              <FormattedMessage
                id="common.team.new-team.text"
                defaultMessage=" Give your team a name and unique URL"
              />
            </Typography>
          </div>
          <DefaultTextField
            label="Team Name"
            // helptext={onboardingHelpText.workspaceNameHelpText}
            fullWidth={true}
            errorState={teamError}
            errorMessage={teamErrorMessage}
            defaultProps={{
              id: "teamName",
              onChange: this.handleInput("teamName"),
              placeholder: "Team Name",
              value: teamName,
              autoFocus: true,
              inputProps: {
                maxLength: 50,
                tabIndex: 1,
                style: { paddingLeft: 0 },
              },
              startAdornment: (
                <InputAdornment position="start">
                  <TeamIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.teamIcon}
                  />
                </InputAdornment>
              ),
            }}
          />
          <DefaultTextField
            label="Team URL"
            // helptext={onboardingHelpText.workspaceUrlHelpText}
            fullWidth={true}
            errorState={teamUrlError}
            errorMessage={teamUrlErrorMessage}
            defaultProps={{
              id: "teamUrl",
              onChange: this.handleInput("teamUrl"),
              placeholder: "team URL",
              value: teamUrl,
              inputProps: {
                style: { paddingLeft: 0 },
                maxLength: 80,
                tabIndex: 4,
              },
              startAdornment: (
                <InputAdornment
                  position="start"
                  classes={{ root: classes.urlAdornment }}
                >
                  <GlobeIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.globeIcon}
                  />
                  <p className={classes.workspaceUrlAdornment}>
                  {window.location.origin+"/"}
                  </p>
                </InputAdornment>
              ),
            }}
          />
        </div>

        <Paper
          classes={{
            root: classes.buttonPaperCntStatic,
          }}
        >
          <div></div>
          <div>
            <Button
              disabled={activeStep === 0}
              //onClick={this.handleBack}
              className={classes.button}
              onClick={this.handleBack}
            >
              Back
            </Button>
            <CustomButton
              onClick={this.handleCreateTeam}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress" || disabledButtonAction}
            >
              Create Team
            </CustomButton>
          </div>
        </Paper>
      </Fragment>
    );
  }
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(onBoardStyles, { withTheme: true })
)(CreateTeam);
