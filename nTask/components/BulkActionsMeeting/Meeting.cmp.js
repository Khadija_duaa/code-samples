import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import BulkActionsStyle from "./BulkActions.style.js";
import IconPeopleOutLine from "../Icons/IconPeopleOutLine.js";
import IconPoriorityOutLine from "../Icons/IconPoriorityOutLine.js";
import IconColorOutLine from "../Icons/IconColorOutLine.js";
import IconExportOutLine from "../Icons/IconExportOutLine.js";
import IconArchiveOutLine from "../Icons/IconArchiveOutLine.js";
import IconDeleteOutLine from "../Icons/IconDeleteOutLine.js";
import IconCloseOutLine from "../Icons/IconCloseOutLine.js";
import AssigneeDropdown from "../Dropdown/AssigneeDropdown2";
// import FlagDropDown from "../../Views/Task/Grid/FlagDropdown";
import StatusDropdown from "../Dropdown/StatusDropdown/Dropdown";
import ColorPickerDropdown from "../Dropdown/ColorPicker/Dropdown";
import { useDispatch, useSelector } from "react-redux";
import { statusData, severity as severityData, typeData } from "../../helper/issueDropdownData";
import RoundIcon from "@material-ui/icons/Brightness1";
import RoundIconOutline from "@material-ui/icons/Brightness1Outlined";
import TypeIcon from "@material-ui/icons/Star";
import {
  exportBulkMeeting,
  archiveBulkMeeting,
  unArchiveBulkMeeting,
  updateBulkMeeting,
  deleteBulkMeeting,
} from "../../redux/actions/meetings";
import CancelIcon from "@material-ui/icons/Cancel";

import { FormattedMessage, injectIntl } from "react-intl";
import { compose } from "redux";
import { priorityData } from "../../helper/taskDropdownData";
import fileDownload from "js-file-download";
import { setDeleteDialogueState } from "../../redux/actions/allDialogs";
import { withSnackbar } from "notistack";
import ActionConfirmation from "../Dialog/ConfirmationDialogs/ActionConfirmation.js";
import { addPendingHandler } from "../../redux/actions/backProcesses.js";

const Meetingcmp = ({ classes, clearSelection, selectedMeetings, theme, intl, enqueueSnackbar }) => {
  const [assigneeEl, setAssigneeEl] = useState(null);
  const [colorEl, setColorEl] = useState(null);


  const [inputType, setInputType] = useState('')
  const [archiveBtnQuery, setArchiveBtnQuery] = useState('')
  const [unarchiveBtnQuery, setUnArchiveBtnQuery] = useState('')
  const [cancelBtnQuery, setCancelBtnQuery] = useState('')
  const [showConfirmation, setShowConfirmation] = useState(false)


  const { quickFilters } = useSelector(state => {
    return {
      quickFilters: state.meetings.quickFilters || {},
    };
  });
  const dispatch = useDispatch();
  // handle bulk update assignee
  const updateAssignee = props => {
    const postObj = { attendeeIds: [props.userId], meetingIds: getSelectedMeetings() };
    setAssigneeEl(null);
    updateBulkMeeting(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} Meetings attendee updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectAssignee = e => {
    e.stopPropagation();
    setAssigneeEl(e.currentTarget);
  };
  const handleCloseAssignee = () => {
    setAssigneeEl(null);
  };
  // Color
  const updateColor = color => {
    const postObj = { colorCode: color, meetingIds: getSelectedMeetings() };
    setColorEl(null);
    updateBulkMeeting(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} Meetings color updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an meeting, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectColor = e => {
    e.stopPropagation();
    setColorEl(e.currentTarget);
  };
  const handleCloseColor = () => {
    setColorEl(null);
  };
  // get projectids use map
  const getSelectedMeetings = () => {
    return selectedMeetings.map(item => {
      return item.data.meetingId;
    });
  };
  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };
  // export api function
  const handleExportMeeting = props => {
    const postObj = { meetingIds: getSelectedMeetings() };
    const obj = {
      type: 'meetings',
      apiType: 'post',
      data: postObj,
      fileName: 'meetings.xlsx',
      apiEndpoint: 'api/export/bulkmeeting',
    }
    addPendingHandler(obj, dispatch);
    
    // exportBulkMeeting(postObj, dispatch,
    //   res => {
    //     fileDownload(res.data, "meetings.xlsx");
    //     showSnackBar(`${selectedMeetings.length} meetings exported successfully`, "success");
    //   },
    //   (err) => {
    //     if (err.response.status == 405) {
    //       showSnackBar("You don't have sufficient rights to export meetings", "error");
    //     } else {
    //       showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //     }
    //   });
  };
  //Confirmation Dialog close
  const closeDeleteConfirmation = () => {
    dispatch(setDeleteDialogueState({ open: false, btnQuery: "" }));
  };
  //Open delete confirmation dialog
  const handleDeleteConfirmation = success => {
    const dialogObj = {
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: `Are you sure you want to delete these ${selectedMeetings.length} meetings?`,
      successAction: handleDeleteMeeting,
      closeAction: closeDeleteConfirmation,
      btnQuery: "",
    };
    dispatch(setDeleteDialogueState(dialogObj));
  };
  // delete api function
  const handleDeleteMeeting = props => {
    dispatch(setDeleteDialogueState({ btnQuery: "progress" }));
    const postObj = { meetingIds: getSelectedMeetings() };
    deleteBulkMeeting(
      postObj,
      dispatch,
      res => {
        // Success
        closeDeleteConfirmation();
        showSnackBar(`${res.length} meetings deleted successfully`, "success");
        clearSelection();
      },
      (err) => {
        // failure
        closeDeleteConfirmation();
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  // archive meeting api
  const handleArchiveMeeting = props => {
    const postObj = { meetingIds: getSelectedMeetings() };
    setArchiveBtnQuery('progress')
    archiveBulkMeeting(
      postObj,
      dispatch,
      res => {
        //Success
        setArchiveBtnQuery('')
        showSnackBar(`${res.length} meetings archived successfully`, "success");
        clearSelection();
      },
      (err) => {
        // Failure
        setArchiveBtnQuery('')
        showSnackBar(
          `${err.message}`,
          "error"
        );
      }
    );
  };
  // unarchive meeting api
  const handleUnArchiveMeeting = props => {
    const postObj = { meetingIds: getSelectedMeetings() };
    setUnArchiveBtnQuery('progress')
    unArchiveBulkMeeting(
      postObj,
      dispatch,
      res => {
        //Success
        setUnArchiveBtnQuery('')
        showSnackBar(`${res.length} Meeting unarchived successfully`, "success");
        clearSelection();
      },
      (err) => {
        // Failure
        setUnArchiveBtnQuery('')
        showSnackBar(
          `${err.message}`,
          "error"
        );
      }
    );
  };
  // cancel meeting api
  const handleCancelMeeting = props => {
    const postObj = { status: 'Cancelled', meetingIds: getSelectedMeetings() };
    setCancelBtnQuery('progress')
    updateBulkMeeting(
      postObj,
      dispatch,
      res => {
        // Success
        setCancelBtnQuery('')
        showSnackBar(`${res.length} Meetings Cancelled successfully`, "success");
        clearSelection();
      },
      (err) => {
        // failure
        setCancelBtnQuery('')
        showSnackBar(
          `${err.message}`,
          "error"
        );
      }
    );
  };
  // conformation popup 
  const handleArchiveConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Archive");
  };
  const handleCancelConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Cancel");
  };
  const handleUnArchiveConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Unarchive");
  };
  const closeConfirmationPopup = () => {
    setShowConfirmation(false)
  }
  const isArchived = quickFilters["Archived"] ? true : false;
  return (
    <>
      <div className={classes.mainTaskCmp}>
        <ul className={classes.bulkActionBtnsList}>
          <li className={classes.selectedTasks}>
            <span>{selectedMeetings && selectedMeetings.length}</span>Meetings selected
          </li>
          {!isArchived && (
            <>
              <li className={classes.meetingListItems}>
                <AssigneeDropdown
                  updateAction={updateAssignee}
                  obj={{}}
                  showSelected={false}
                  singleSelect={true}
                  handleCloseAssignee={handleCloseAssignee}
                  customBtnRender={
                    <div className={classes.renderTask} onClick={handleSelectAssignee}>
                      <span className={classes.issueCmpIcon}>
                        <IconPeopleOutLine />
                      </span>
                      Attendee
                    </div>
                  }
                  anchorEl={assigneeEl}
                />
              </li>
              <li className={classes.meetingListItems}>
                <ColorPickerDropdown
                  onSelect={updateColor}
                  obj={{}}
                  singleSelect={true}
                  handleCloseColor={handleCloseColor}
                  customColorBtnRender={
                    <div onClick={handleSelectColor} className={classes.renderTask}>
                      <span className={classes.issueCmpIcon}>
                        <IconColorOutLine />
                      </span>
                      Color
                    </div>
                  }
                  anchorEl={colorEl}
                />
              </li>
              <li className={classes.meetingListItems} onClick={handleExportMeeting}>
                <span className={classes.issueCmpIcon}>
                  <IconExportOutLine />
                </span>
                Export
              </li>
              <li className={classes.meetingListItems} onClick={handleArchiveConfirmation}>
                <span className={classes.issueCmpIcon}>
                  <IconArchiveOutLine />
                </span>
                Archive
              </li>
              <li className={classes.meetingListItems} onClick={handleCancelConfirmation}>
                <span className={classes.issueCmpIcon}>
                  <CancelIcon />
                </span>
                Cancel
              </li>
            </>
          )}
          {isArchived && (
            <li className={classes.meetingListItems} onClick={handleUnArchiveConfirmation}>
              <span className={classes.issueCmpIcon}>
                <IconArchiveOutLine />
              </span>
              UnArchive
            </li>
          )}
          <li className={classes.issueDeleteIcon} onClick={handleDeleteConfirmation}>
            <span className={classes.issueCmpIcon}>
              <IconDeleteOutLine />
            </span>
            Delete
          </li>
          <li className={classes.issueCrossIcon} onClick={clearSelection}>
            <span className={classes.iconClose}>
              <IconCloseOutLine />
            </span>
          </li>
        </ul>
      </div>
      {inputType === "Archive" /* Confirmation modal for Archive the bulk projects  */ ? (
        <>
          <ActionConfirmation
            open={showConfirmation}
            closeAction={closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.meeting.label2"
                defaultMessage="Archive Meetings"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.meetings.message"
                defaultMessage={`Are you sure you want to archive these ${selectedMeetings.length} meetings?`}
                values={{ l: selectedMeetings.length }}
              />
            }
            successAction={handleArchiveMeeting}
            btnQuery={archiveBtnQuery}
          />
        </>
      ) : inputType === "Cancel" ? (
        <ActionConfirmation
          open={showConfirmation}
          closeAction={closeConfirmationPopup}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="Cancel Meetings"
              defaultMessage="Cancel Meetings"
            />
          }
          alignment="center"
          iconType="unarchive"
          headingText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          msgText={
            <FormattedMessage
              id="common.action.cancel.meeting.message"
              defaultMessage={`Are you sure you want to cancel these ${selectedMeetings.length} meetings?`}
              values={{ m: selectedMeetings.length }}
            />
          }
          successAction={handleCancelMeeting}
          btnQuery={cancelBtnQuery}
        />) : inputType === "Unarchive" /* Confirmation modal for Unarchive the bulk meetings  */ ? (
          <>
            <ActionConfirmation
              open={showConfirmation}
              closeAction={closeConfirmationPopup}
              cancelBtnText={
                <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
              }
              successBtnText={
                <FormattedMessage
                  id="common.un-archived.meetings.label2"
                  defaultMessage="Unarchive meetings"
                />
              }
              alignment="center"
              iconType="unarchive"
              headingText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.title"
                  defaultMessage="Unarchive"
                />
              }
              // msgText={`Are you sure you want to unarchive these ${selectedMeetings.length} meetings?`}
              msgText={
                <FormattedMessage
                  id="common.un-archived.meetings.message"
                  defaultMessage={`Are you sure you want to unarchive these ${selectedMeetings.length} meetings?`}
                  values={{ l: selectedMeetings.length }}
                />
              }
              successAction={handleUnArchiveMeeting}
              btnQuery={unarchiveBtnQuery}
            />
          </>
        ) : null}
    </>
  );
};
export default compose(
  withSnackbar,
  injectIntl,
  withStyles(BulkActionsStyle, { withTheme: true })
)(Meetingcmp);
