const loaderStyles = theme => ({
  loaderContainer: {
    height: "100%",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  loader: {
    border: "5px solid rgba(243, 243, 243, 1)",
    borderRadius: "50%",
    borderTop: `5px solid ${theme.palette.background.green}`,
    width: "80px",
    height: "80px",
    WebkitAnimation: "spin 2s linear infinite",
    animation: "spin 2s linear infinite"
  },
  loaderBoard: {
    border: "5px solid rgba(243, 243, 243, 1)",
    borderRadius: "50%",
    borderTop: `5px solid ${theme.palette.background.green}`,
    width: "80px",
    height: "80px",
    WebkitAnimation: "spin 2s linear infinite",
    animation: "spin 2s linear infinite",
    marginTop: "300px",
    marginLeft: "800px"
  }
});

export default loaderStyles;
