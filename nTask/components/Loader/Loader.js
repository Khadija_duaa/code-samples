import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import loaderStyles from "./loader.style";

function Loader(props) {
    const { classes, styles } = props;
    return (
        <div className={classes.loaderContainer} style={{ height: "calc(100% - 60px)", ...styles }}>
            <div className={classes.loader}></div>
        </div>
    );
}

export default withStyles(loaderStyles, { withTheme: true })(Loader);
