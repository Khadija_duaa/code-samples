import React from 'react';
import AndroidImage from "../../assets/images/androidAppScreen.png";
import { withStyles } from "@material-ui/core/styles";
import mobileAppNotifStyles from "./styles"

function MobileAppNotification(props){
    const {classes} = props
    return (
        <div className={classes.mobileAppNotifCnt}>
            <img src={AndroidImage} className={classes.androidMobileImg}/>
        </div>
    );
};

export default withStyles(mobileAppNotifStyles, {withTheme: true})(MobileAppNotification);