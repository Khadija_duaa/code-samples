import AndroidAppScreenBgLeft from "../../assets/images/androidAppScreenBgLeft.png";
import AndroidAppScreenBgRight from "../../assets/images/androidAppScreenBgRight.png";

const mobileAppNotifStyles = () => ({
  mobileAppNotifCnt: {
    backgroundImage: `url('${AndroidAppScreenBgLeft}'), url('${AndroidAppScreenBgRight}')`,
    backgroundPosition: "bottom left, top right",
    backgroundRepeat: "no-repeat",
    height: "100%",
    textAlign: "center"
  },
  androidMobileImg: {
    width: "100%",
    maxWidth: 638,
    margin: "0 auto",
    marginTop: 50,
    marginLeft: 30
  }
});

export default mobileAppNotifStyles;
