import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { useState } from "react";
import CustomTooltip from "../../Tooltip/Tooltip";
import Typography from "@material-ui/core/Typography";
import groupByComponentStyles from "./groupByComponents.style";
import { priorityData } from "../../../helper/taskDropdownData";
import { priorityData as issuePrioirtyData } from "../../../helper/issueDropdownData";
import { getTemplate } from "../../../utils/getTemplate";
import { useSelector } from "react-redux";

function GroupByComponents({ data, classes, theme, feature }) {
  const [groupSelected, setGroupSelected] = useState(false); const state = useSelector(state => {
    return {
      projects: state.projects.data,
    };
  });
  const { projects } = state;
  const getSelectedProject = (id) => {
    return projects.find(p => p.projectId == id);
  }
  const handleSelectGroup = () => {
    setGroupSelected(!groupSelected)
    data.node.childrenAfterGroup.forEach(x => {
      x.setSelected(!groupSelected)
    })
  }
  const Components = {
    statusTitle: () => {
      let rowData = data.node.allLeafChildren.length ? data.node.allLeafChildren[0].data : {};
      let template = getTemplate(rowData);
      const templateCategory = template.category == 3 ? `Custom (${template.name})` : template.name
      return (
        <>
          <span className={classes.statusColorBar} style={{ background: rowData.statusColor }}></span>
          <Typography variant={"body2"} className={classes.groupText}>
            {data.node.key ? `${rowData.statusTitle} - ${templateCategory}` : "Other"}
          </Typography>
        </>
      );
    },
    status: () => {
      // const WSName = data.node.allLeafChildren.length ? data.node.allLeafChildren[0].data.workspaceName : ""
      return feature == "task" ? (
        <>
          <span
            className={classes.statusColorBar}
            style={{ background: data.node.allLeafChildren[0].data.statusColor }}></span>
          <Typography variant={"body2"} className={classes.groupText}>
            {/* {WSName ? `${data.node.key} (${WSName})` : data.node.key} */}
            {data.node.key}
          </Typography>
        </>
      ) : (
        <>
          <span
            className={classes.statusColorBar}
            style={{ background: theme.palette.issueStatus[data.node.key] }}></span>
          <Typography variant={"body2"} className={classes.groupText}>
            {data.node.key}
          </Typography>
        </>
      );
    },
    severity: () => {
      return (
        <>
          <span className={classes.statusColorBar} style={{ background: theme.palette.severity[data.node.key] }}></span>

          <Typography variant={"body2"} className={classes.groupText}>
            {data.node.key}
          </Typography>
        </>
      );
    },
    type: () => {
      return (
        <>
          <span className={classes.statusColorBar} style={{ background: theme.palette.issueType[data.node.key] }}></span>

          <Typography variant={"body2"} className={classes.groupText}>
            {data.node.key}
          </Typography>
        </>
      );
    },
    startDate: () => {
      return (
        <Typography variant={"body2"} className={classes.groupText}>
          {data.node.key ? data.node.key : "No Planned Start Date"}
        </Typography>
      );
    },
    dueDate: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "No Planned Due Date"}
      </Typography>
    ),
    actualStartDate: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "No Actual Start Date"}
      </Typography>
    ),
    actualDueDate: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "No Actual Due Date"}
      </Typography>
    ),
    priority: () => {
      const selectedIssuePriority = issuePrioirtyData(theme).find(x => data.node.key == x.value)
      return feature == 'task' ?
        <>
          <span className={classes.statusColorBar} style={{ background: priorityData(theme)[data.node.key - 1].color }}></span>
          <Typography variant={"body2"} className={classes.groupText}>
            {data.node.key ? priorityData()[data.node.key - 1].label : null}
          </Typography>
        </> : <>
          <span className={classes.statusColorBar} style={{ background: selectedIssuePriority.color }}></span>
          <Typography variant={"body2"} className={classes.groupText}>
            {data.node.key ? selectedIssuePriority.label : null}
          </Typography>
        </>

    },
    project: () => {
      const projectId = data.node.allLeafChildren[0].data.projectId
      return (
        <>
          <span className={classes.statusColorBar} style={{ background: getSelectedProject(projectId) && getSelectedProject(projectId).colorCode }}></span>
          <Typography variant={"body2"} className={classes.groupText}>
            {data.node.key ? data.node.key : "No Project Assigned"}
          </Typography>
        </>
      )
    },
    tasks: () => {
      return (
        <>
          <Typography variant={"body2"} className={classes.groupText}>
            {data.node.key ? data.node.key : "No Task Assigned"}
          </Typography>
        </>
      )
    },
    progress: () => <div>{data.node.key}%</div>,
    totalEffort: () => (
      <div className={classes.totalEffort}>
        <label>80 hrs</label>
        <label>15 mins</label>
        <div className={classes.textLogged}>
          <label>Sum</label>
        </div>
      </div>
    ),
    attachments: () => (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Sum</label>
      </div>
    ),
    totalComment: () => (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Avg.</label>
      </div>
    ),
    meetings: () => (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>min.</label>
      </div>
    ),
    issues: () => (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Avg.</label>
      </div>
    ),
    risks: () => (
      <div className={classes.reuseFile}>
        <label className={classes.editNum}>0</label>
        <label className={classes.innerText}>Avg.</label>
      </div>
    ),
    createdDate: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key}
      </Typography>
    ), 
    updatedDate: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key}
      </Typography>
    ), 
    workspaceName: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key}
      </Typography>
    ),
    createdBy: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key}
      </Typography>
    ),
    updatedBy: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key}
      </Typography>
    ),
    number: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "Other"}
      </Typography>
    ),
    money: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "Other"}
      </Typography>
    ),
    country: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "Other"}
      </Typography>
    ),
    rating: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "Other"}
      </Typography>
    ),
    websiteurl: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "Other"}
      </Typography>
    ),
    email: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "Other"}
      </Typography>
    ),
    phone: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "Other"}
      </Typography>
    ),
    date: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "No Date"}
      </Typography>
    ),
    textfield: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "Other"}
      </Typography>
    ),
    dropdown: () => (
      <Typography variant={"body2"} className={classes.groupText}>
        {data.node.key ? data.node.key : "Other"}
      </Typography>
    ),
  };
  const isCustomField = data.node.rowGroupColumn.colDef.fieldType;

  const SelectedCmp = () => Components[isCustomField ? isCustomField : data.node.field] && Components[isCustomField ? isCustomField : data.node.field]();
  return <>{SelectedCmp()}</>;
}

export default withStyles(groupByComponentStyles, { withTheme: true })(GroupByComponents);
