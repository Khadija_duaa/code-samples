import React, { useEffect, useCallback, useState } from "react";
import { withSnackbar } from "notistack";
import loadable from '@loadable/component'
const CustomTable = loadable(() => import("../CustomTable2/ReportingListViewTable.cmp"));
import { useDispatch, useSelector } from "react-redux";
import isEqual from "lodash/isEqual";
import { AgGridColumn } from "@ag-grid-community/react";
import withStyles from "@material-ui/core/styles/withStyles";
import repostingListStyles from "./reportingList.style";
import reportingColumnDefs, { getValueGetter, getComparator } from "./reportingColumns";
import { injectIntl } from "react-intl";
import { compose } from "redux";

import CustomCellRenderers from "./CustomCellRenderers";
import EmptyState from "../../components/EmptyStates/EmptyState";
import { grid } from "../../components/CustomTable2/gridInstance";
import TemplatesSelector from "../../redux/selectors/templatesSelector";
import { headerProps as constProps } from "./constants";
import CustomFieldRenderer from "../CustomTable2/CustomFieldsColumn/CustomFieldRenderer";
import { quickFilterText } from "./reportFilter.utils";
import { doesFilterPass } from "./reportFilter.utils";
import GroupByComponents from "./GroupByComponents/GroupByComponents";
import searchQuery from "../CustomTable2/ReportColumnSettingDropdown/searchQuery";
import { deleteAllReportingFilter, exportBulkItems } from "../../redux/actions/reportingFilters";
import fileDownload from "js-file-download";
import uniq from "lodash/uniq";
import ReportFilter from "./ReportingFilter/reportFilter.view";
import { addPendingHandler } from "../../redux/actions/backProcesses";

const defaultColSetting = { minWidth: 150, headerClass: "customHeader" };

const RepostingList = React.memo(
  ({
    classes,
    theme,
    intl,
    enqueueSnackbar,
    style,
    feature,
    groupType,
    columns = [],
    customColumns = [],
    data,
    emptyHeading,
    emptyText,
    handleRowClick = () => {
    },
    headerProps = {},
    columnChangeCallback,
    exportConfig,
  }) => {

    const state = useSelector(state => {
      return {
        quickFilters: state.tasks.quickFilters,
        templates: TemplatesSelector(state),
        customFields: state.customFields.data,
        members: state.profile.data.teamMember,
      };
    });
    const dispatch = useDispatch();
    const { quickFilters, customFields, members } = state;
    const [selectedIssues, setSelectedIssues] = useState([]);
    const { accessorKey, accessorId, apiKey, documentName } = exportConfig;

    const isExternalFilterPresent = () => {
      return true;
    };

    //Clear selection if archived view is selected
    useEffect(() => {
      if (quickFilters && quickFilters.Archived) {
        grid.grid && grid.grid.deselectAll();
      }
      return (() => {
        deleteAllReportingFilter(feature, dispatch);
      });
    }, [quickFilters]);

    const handleTaskSelection = issues => {
      setSelectedIssues(issues);
    };

    const CustomfieldRendere = row => {
      /** custom fields columns rendere */
      const obj = {
        fieldId: row.colDef.fieldId,
        fieldType: row.colDef.fieldType,
        fieldData: { data: null },
      };
      const rowData = row.data || {};
      const data = rowData.customFieldData && rowData.customFieldData.length
        ? rowData.customFieldData.find(cf => cf.fieldId === row.colDef.fieldId) || obj
        : obj;

      return (
        <div style={{ pointerEvents: "none", width: "100%", height: "100%" }}>
          <CustomFieldRenderer
            field={data}
            fieldType={row.colDef.fieldType}
            rowData={rowData}
            handleUpdateCustomField={() => {
            }}
            permission={false}
            groupType={groupType}
            placeholder={"-"}
            workspaceId={rowData.workspaceId ? rowData.workspaceId : rowData.workSpaceId}
          />
        </div>
      );
    };
    // generate custom renderer
    const frameworkRenderers = columns.reduce((r, cv) => {
      if (!cv.isSystem) {
        return r;
      }
      r[cv.columnKey] = CustomCellRenderers(cv.displayType, theme, classes, state);
      return r;
    }, {});
    let headConstProps = constProps;
    const nonGroupableColKeys = columns
      .filter(item => {
        const currentCustomField = customFields.find(c => c.fieldId == item.fieldId);
        if (currentCustomField) {
          return currentCustomField.fieldType == "textarea" ||
            currentCustomField.fieldType == "formula" ||
            currentCustomField.fieldType == "people" ||
            currentCustomField.fieldType == "filesAndMedia" ||
            currentCustomField.fieldType == "location" ||
            (currentCustomField.fieldType == "dropdown" && currentCustomField.settings.multiSelect);
        }
      },
      ).map(nsf => nsf.columnKey);
    // Disable Sort for below columns
    const nonSortableColKeys = columns
      .filter(item => {
        const currentCustomField = customFields.find(c => c.fieldId == item.fieldId);
        if (currentCustomField) {
          return currentCustomField.fieldType == "phone" ||
            currentCustomField.fieldType == "people" ||
            currentCustomField.fieldType == "formula" ||
            currentCustomField.fieldType == "filesAndMedia" ||
            currentCustomField.fieldType == "location" ||
            (currentCustomField.fieldType == "dropdown" && currentCustomField.settings.multiSelect);
        }
      },
      ).map(nsf => nsf.columnKey);

    //Disable grouping for custom columns temporary

    const disabledGroupingKeys = [...nonGroupableColKeys, ...headConstProps.columnGroupingDisabled];
    const disabledSortingKeys = [...nonSortableColKeys, ...headConstProps.columnSortingDisabled]
    headConstProps.columnGroupingDisabled = uniq(disabledGroupingKeys);
    headConstProps.columnSortingDisabled = uniq(disabledSortingKeys);

    const systemColumns = columns.filter(c => c.isSystem);
    const allColumns = [...systemColumns, ...customColumns];
    const sortedColumns = allColumns.sort((pv, cv) => {
      return pv.position - cv.position;
    });
    const handleExportItems = props => {
      const selectedNodes = grid.grid.getSelectedNodes();
      const selectedNodesLength = selectedNodes?.length || 1
      const postObj = selectedNodes.length ? { [accessorKey]: selectedNodes.map(i => i.data[accessorId]) } : { [accessorKey]: [props.node.data[accessorId]] };
      const obj = {
        type: `${groupType}s`,
        apiType: 'post',
        data: postObj,
        fileName: documentName,
        apiEndpoint: `api/${apiKey}`,
      }
      addPendingHandler(obj, dispatch);

      // exportBulkItems(postObj, apiKey, res => {
      //   fileDownload(res.data, documentName);
      //   showSnackBar(`${selectedNodesLength} ${groupType}s exported successfully`, "success");
      // }, fail => { }, dispatch);
    };
    // export all items api function
    const handleExportAllItems = props => {
      let allNodes = [];
      grid.grid.forEachNodeAfterFilter(node => !node.group && allNodes.push(node.data));
      const postObj = allNodes.length && { [accessorKey]: allNodes.map(i => i[accessorId]) }

      const obj = {
        type: `${groupType}s`,
        apiType: 'post',
        data: postObj,
        fileName: documentName,
        apiEndpoint: `api/${apiKey}`,
      }
      addPendingHandler(obj, dispatch);
      // exportBulkItems(postObj, apiKey, res => {
      //   fileDownload(res.data, documentName);
      //   showSnackBar(`${allNodes.length} ${groupType}s exported successfully`, "success");
      // }, fail => { }, dispatch);
    };

    const getContextMenuItems = useCallback((params) => {
      const selectedNodes = grid.grid.getSelectedNodes();
      let result = [
        {
          // custom item
          name: selectedNodes.length ? `Export Selected ${groupType}(s)` : `Export ${groupType}`,
          action: () => handleExportItems(params),
        },
        {
          // custom item
          name: `Export all ${groupType}s`,
          action: () => handleExportAllItems(params),
        },
      ];
      return result;
    }, []);

    //Snackbar function
    const showSnackBar = (snackBarMessage = "", type = null) => {
      enqueueSnackbar(
        <div className={classes.snackBarHeadingCnt}>
          <p className={classes.snackBarContent}>{snackBarMessage}</p>
        </div>,
        {
          anchorOrigin: {
            vertical: "top",
            horizontal: "centerdp",
          },
          variant: type ? type : "info",
        }
      );
    };

    const sectionGroup = localStorage.getItem("sectiongrouping");
    const [sectionGrouping, setSectionGrouping] = useState(sectionGroup === "true" ? true : false);
    // handle section grouping
    const handleChangeGrouping = value => {
      let g = grid.grid;
      let sectionGroup = localStorage.getItem("sectiongrouping");
      if (sectionGroup == "true") localStorage.setItem("sectiongrouping", value);
      else {
        localStorage.setItem("sectiongrouping", value);
      }
      setSectionGrouping(value);
    };
    return (
      <>
        {data.length == 0 ? (
          // 'no data found'
          <div className={classes.emptyContainer} style={style}>
            <EmptyState screenType={groupType} heading={emptyHeading} message={emptyText} />
          </div>
        ) : (
          <div className={classes.taskListViewCnt} style={style}>
            <CustomTable
              columns={columns}
              defaultColDef={{ lockPinned: true }}
              headerProps={{ columnChangeCallback, ...headerProps, ...headConstProps }}
              isExternalFilterPresent={isExternalFilterPresent}
              doesExternalFilterPass={node => doesFilterPass(node, feature)}
              onSelectionChanged={handleTaskSelection}
              frameworkComponents={{
                groupRowInnerRenderer: props => GroupRowInnerRenderer(props, groupType),
                textfield: CustomfieldRendere,
                location: CustomfieldRendere,
                country: CustomfieldRendere,
                number: CustomfieldRendere,
                money: CustomfieldRendere,
                email: CustomfieldRendere,
                websiteurl: CustomfieldRendere,
                date: CustomfieldRendere,
                phone: CustomfieldRendere,
                rating: CustomfieldRendere,
                formula: CustomfieldRendere,
                people: CustomfieldRendere,
                dropdown: CustomfieldRendere,
                filesAndMedia: CustomfieldRendere,
                ...frameworkRenderers,
              }}
              data={data}
              selectedTasks={[]}
              type={feature}
              groupType={groupType}
              // onRowGroupChange={(obj, key) => updateTask(obj, { [key]: obj[key] })}
              gridProps={{
                getContextMenuItems: getContextMenuItems,
                groupDisplayType: "groupRows",
                onRowClicked: handleRowClick,
                reactUi: true,
                // groupIncludeFooter: true,
                // groupIncludeTotalFooter: true,
                maintainColumnOrder: true,
                // autoGroupColumnDef: { minWidth: 300 },
              }}>
              {sortedColumns.map((c, i) => {
                const defaultColDef = c.fieldType && reportingColumnDefs(classes)[c.fieldType];
                const {
                  hide,
                  pinned,
                  rowGroup,
                  rowGroupIndex,
                  sort,
                  sortIndex,
                  width,
                  wrapText,
                  autoHeight,
                  columnKey,
                  displayType,
                } = c;
                return c.isSystem ? (
                  <AgGridColumn
                    key={c.id}
                    headerName={c.columnName}
                    field={columnKey}
                    hide={hide}
                    pinned={pinned}
                    align="left"
                    rowGroup={rowGroup}
                    sort={sort}
                    checkboxSelection={c.displayType == 'title'}
                    resizable={true}
                    sortIndex={sortIndex == -1 ? null : sortIndex}
                    wrapText={wrapText}
                    rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
                    autoHeight={autoHeight}
                    width={width || (defaultColSetting && defaultColSetting.minWidth)}
                    cellRenderer={c.columnKey}
                    groupType={c.groupType}
                    valueGetter={getValueGetter(displayType, groupType, columnKey)}
                    comparator={getComparator(displayType, groupType, columnKey)}
                    getQuickFilterText={quickFilterText(
                      members,
                      columnKey,
                      groupType,
                      theme,
                      classes,
                    )}
                    cellStyle={{
                      justifyContent: displayType == "title" ? "left" : "center",
                      cursor: "pointer",
                    }}
                    cellClass={`${displayType == "fill"
                      ? classes.fullSpannedCell
                      : displayType == "title"
                        ? `${classes.overviewTitleCell} ag-textAlignLeft`
                        : ""
                      }`}
                    {...defaultColSetting}
                  />
                ) : (
                  <AgGridColumn
                    key={c.id}
                    headerName={c.columnName}
                    field={c.columnKey}
                    fieldType={c.fieldType}
                    hide={hide}
                    suppressKeyboardEvent={true}
                    pinned={pinned}
                    align="left"
                    rowGroup={rowGroup}
                    rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
                    sort={sort}
                    sortIndex={sortIndex == -1 ? null : sortIndex}
                    wrapText={wrapText}
                    autoHeight={autoHeight}
                    fieldId={c.fieldId}
                    width={width || (defaultColDef && defaultColDef.minWidth)}
                    cellStyle={{
                      justifyContent:
                        c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                          ? "flex-start"
                          : "center",
                      cursor: "pointer",
                    }}
                    {...defaultColDef}
                  />
                );
              })}
            </CustomTable>
            {/* filters to be continued */}
            <ReportFilter
              columns={columns}
              groupType={groupType}
              feature={feature}
              taskStatusGroup={headerProps.taskStatusGroup}
              // sectionGrouping={sectionGrouping}
              // handleChangeGrouping={handleChangeGrouping}
            />
          </div>
        )}
      </>
    );
  },
  areEqual,
);

const GroupRowInnerRenderer = (props, groupType) => {
  return <GroupByComponents data={props} feature={groupType} />;
};

function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
    return true if passing nextProps to render would return
    the same result as passing prevProps to render,
    otherwise return false
    */
}

export default compose(
  injectIntl,
  withSnackbar,
  withStyles(repostingListStyles, { withTheme: true }),
)(RepostingList);
