import React from "react";
import moment from "moment";
import { store } from "../../index";
import { generateNumberValue } from "../../helper/generateNumberFieldData";

let createdDateValueGetter = function(params) {
  return params.data && params.data.createdDate
    ? moment(params.data.createdDate).format("MMM DD, YYYY")
    : "";
};
let startDateValueGetter = function(params) {
  return params.data && params.data.startDate
    ? moment(params.data.startDate).format("MMM DD, YYYY")
    : "";
};
let dueDateValueGetter = function(params) {
  return params.data && params.data.dueDate
    ? moment(params.data.dueDate).format("MMM DD, YYYY")
    : "";
};
let actualStartDateValueGetter = function(params) {
  return params.data && params.data.actualStartDate
    ? moment(params.data.actualStartDate).format("MMM DD, YYYY")
    : "";
};
let actualDueDateValueGetter = function(params) {
  return params.data && params.data.actualDueDate
    ? moment(params.data.actualDueDate).format("MMM DD, YYYY")
    : "";
};
let projectValueGetter = function(params) {
  return params.data && params.data.projectId && params.data.projectInfor
    ? params.data.projectInfor.projectName
    : "";
};

let numberFieldGetter = function(params) {
  const customFields = store.getState().customFields.data;
  if (!params.data) return "";
  let numberField = customFields.find(item => item.fieldId === params.colDef.fieldId);
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (numberField && rowData) {
    value = {
      unit: numberField.settings.unit,
      direction: numberField.settings.direction,
      number: generateNumberValue(rowData.fieldData.data, numberField),
    };
  }
  return value;
};
let numberFieldKeyCreator = function(params) {
  const customFields = store.getState().customFields.data;
  if (!params.data) return "";
  let numberField = customFields.find(item => item.fieldId === params.colDef.fieldId);
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (numberField && rowData) {
    value = `${
      numberField.settings.direction === "left" && numberField.settings.unit !== "none"
        ? numberField.settings.unit
        : ""
    } ${generateNumberValue(rowData.fieldData.data, numberField)} ${
      numberField.settings.direction === "right" && numberField.settings.unit !== "none"
        ? numberField.settings.unit
        : ""
    }`;
  }
  return value;
};

let moneyFieldGetter = function(params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = `${rowData.fieldData.data} `;
  }
  return value;
};

let countryFieldGetter = function(params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let ratingFieldGetter = function(params) {
  if (!params.data) return "";
  const customFields = store.getState().customFields.data;
  let ratingField = customFields.find(item => item.fieldId === params.colDef.fieldId);
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    for (let i = 0; i < rowData.fieldData.data; i++) {
      value = `${value} ${ratingField.settings.emoji}`;
    }
  }
  return value;
};

let websiteurlFieldGetter = function(params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let emailFieldGetter = function(params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let phoneFieldGetter = function(params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let dateValueGetter = function(params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = moment(rowData.fieldData.data.date).format("lll");
  }
  return value;
};

let textFieldGetter = function(params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let dropdownFieldGetter = function(params) {
  if (!params.data) return "";
  let value = "";
  if (params.data) {
    const customFields = store.getState().customFields.data;
    let dropdownField = customFields.find(item => item.fieldId === params.colDef.fieldId);
    let rowData = params.data.customFieldData
      ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
      : null;
    if (rowData && !dropdownField?.settings?.multiSelect) {
      let dataValue =
        dropdownField.values.data.find(opt => opt.id === rowData.fieldData.data.id) || {};
      value = dataValue.value || "";
    }
    if (rowData && dropdownField?.settings?.multiSelect) {
      value = rowData.fieldData.data.map(item => item.value).join(",");
    }
  }
  return value;
};

const reportingColumnDefs = classes => ({
  uniqueId: {
    sortable: true,
    filter: true,
    minWidth: 100,
    resizable: true,
  },
  taskTitle: {
    sortable: true,
    filter: true,
    resizable: true,
    width: 150,
    checkboxSelection: true,
    cellClass: classes.taskTitleCell,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    rowDrag: true,
  },
  statusTitle: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    cellClass: classes.fullSpannedCell,
    valueGetter: "data.status",
    comparator: sortStatus,
  },
  startDate: {
    sortable: true,
    filter: true,
    minWidth: 180,
    resizable: true,
    comparator: dateComparator,
    valueGetter: startDateValueGetter,
  },
  dueDate: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    comparator: dateComparator,
    valueGetter: dueDateValueGetter,
  },
  actualStartDate: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    comparator: dateComparator,
    valueGetter: actualStartDateValueGetter,
  },
  actualDueDate: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    comparator: dateComparator,
    valueGetter: actualDueDateValueGetter,
  },
  priority: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellClass: classes.fullSpannedCell,
  },
  project: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    valueGetter: projectValueGetter,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    comparator: sortProject,
  },
  progress: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 130,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  assigneeList: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 200,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  timeLogged: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  workspaceName: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 200,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  totalAttachment: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  comments: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  meetings: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  issues: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer"
  },
  risks: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  createdDate: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    comparator: dateComparator,
    valueGetter: createdDateValueGetter,
  },
  createdBy: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  textfield: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    valueGetter: textFieldGetter,
    comparator: sortAlphabetically,
    cellClass: "ag-textAlignLeft",
    cellRenderer: "textfield",
    align: "left",
  },
  textarea: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "textarea",
    cellClass: "ag-textAlignLeft",
    valueGetter: "data.customFieldData",
  },
  location: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "location",
    valueGetter: "data.customFieldData",
    cellClass: "ag-textAlignLeft",
  },
  country: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "country",
    valueGetter: countryFieldGetter,
    cellClass: "ag-textAlignLeft",
  },
  number: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "number",
    comparator: sortNumberCustomField,
    valueGetter: numberFieldGetter,
    keyCreator: numberFieldKeyCreator,
    cellClass: "ag-textAlignLeft",
  },
  money: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "money",
    valueGetter: moneyFieldGetter,
    comparator: sortMoney,
    cellClass: "ag-textAlignLeft",
  },
  email: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 220,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "email",
    valueGetter: emailFieldGetter,
    cellClass: "ag-textAlignLeft",
    comparator: sortAlphabetically,
  },
  websiteurl: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "websiteurl",
    valueGetter: websiteurlFieldGetter,
    cellClass: "ag-textAlignLeft",
    comparator: sortAlphabetically,
  },
  date: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "date",
    valueGetter: dateValueGetter,
    comparator: dateComparator,
    cellClass: "ag-textAlignLeft",
  },
  phone: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "phone",
    valueGetter: phoneFieldGetter,
    cellClass: "ag-textAlignLeft",
  },
  rating: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 200,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "rating",
    valueGetter: ratingFieldGetter,
  },
  formula: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "formula",
    valueGetter: "data.customFieldData",
    cellClass: "ag-textAlignLeft",
  },
  people: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "people",
    valueGetter: "data.customFieldData",
  },
  dropdown: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "dropdown",
    valueGetter: dropdownFieldGetter,
    comparator: sortAlphabetically,
    cellClass: `${classes.fullSpannedCell} ag-textAlignLeft`,
  },
  filesAndMedia: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "filesAndMedia",
    valueGetter: "data.customFieldData",
    cellClass: "ag-textAlignLeft",
  },
});

function dateComparator(date1, date2) {
  const newDate = date1 ? new Date(date1) : "";
  const prevDate = date2 ? new Date(date2) : "";
  if (newDate == prevDate) return 0;

  return newDate - prevDate;
}

function sortProject(prevProject, nextProject, prevRow, nextRow) {
  const prevProjectName =
    prevRow.data && prevRow.data.projectId ? prevRow.data.projectInfor.projectName.toLowerCase() : "";
  const nextProjectName =
    nextRow.data && nextRow.data.projectId ? nextRow.data.projectInfor.projectName.toLowerCase() : "";
  if (prevProjectName == nextProjectName) return 0;
  if (prevProjectName < nextProjectName) return -1;
  if (prevProjectName > nextProjectName) return 1;
}

function sortStatus(prevStatus, nextStatus, prevRow, nextRow) {
  const prevStatusName = prevRow.data ? prevRow.data.statusTitle : "";
  const nextStatusName = nextRow.data ? nextRow.data.statusTitle : "";
  if (prevStatusName == nextStatusName) return 0;
  if (prevStatusName < nextStatusName) return -1;
  if (prevStatusName > nextStatusName) return 1;
}

function sortAlphabetically(nextObj="", prevObj="", nextRow={}, prevRow={}) {
  const prevtext = prevObj ? prevObj.trim().toLowerCase() : "";
  const nextText = nextObj ? nextObj.trim().toLowerCase() : "";
  if (nextText == prevtext) return 0;
  if (nextText < prevtext) return -1;
  if (nextText > prevtext) return 1;
}

function sortNumberCustomField(nextObj, prevObj, nextRow, prevRow) {
  const prevNumberVal = prevObj.number ? parseFloat(prevObj.number) : "";
  const nextNumberVal = nextObj.number ? parseFloat(nextObj.number) : "";
  if (nextNumberVal == prevNumberVal) return 0;
  if (nextNumberVal < prevNumberVal) return -1;
  if (nextNumberVal > prevNumberVal) return 1;
}
function sortNumber(nextObj, prevObj, nextRow, prevRow) {
  const prevNumberVal = prevObj ? parseFloat(prevObj) : "";
  const nextNumberVal = nextObj ? parseFloat(nextObj) : "";
  if (nextNumberVal == prevNumberVal) return 0;
  if (nextNumberVal < prevNumberVal) return -1;
  if (nextNumberVal > prevNumberVal) return 1;
}

function sortMoney(nextObj, prevObj, nextRow, prevRow) {
  const prevNumberVal = prevObj ? parseFloat(prevObj) : "";
  const nextNumberVal = nextObj ? parseFloat(nextObj) : "";
  if (nextNumberVal == prevNumberVal) return 0;
  if (nextNumberVal < prevNumberVal) return -1;
  if (nextNumberVal > prevNumberVal) return 1;
}

export default reportingColumnDefs;

export const getValueGetter = (displayType, groupType, columnKey) => {
  const Components = {
    tasks: params => {
      return params.data && params.data.tasks && params.data.tasks.length
        ? params.data.tasks[0].taskTitle
        : "";
    },
    status: params => {
      if (groupType == "task" && columnKey == "status") {
        return params.data && params.data.statusTitle;
      } else {
        return params.data && params.data.status;
      }
    },
    createdDate: params => {
      return params.data && params.data.createdDate
        ? moment(params.data.createdDate).format("MMM DD, YYYY")
        : "";
    },
    updatedDate: params => {
      return params.data && params.data.updatedDate
        ? moment(params.data.updatedDate).format("MMM DD, YYYY")
        : "";
    },
    startDate: params => {
      return params.data && params.data.startDate
        ? moment(params.data.startDate).format("MMM DD, YYYY")
        : "";
    },
    dueDate: params => {
      return params.data && params.data.dueDate
        ? moment(params.data.dueDate).format("MMM DD, YYYY")
        : "";
    },
    actualStartDate: params => {
      return params.data && params.data.actualStartDate
        ? moment(params.data.actualStartDate).format("MMM DD, YYYY")
        : "";
    },
    actualDueDate: params => {
      return params.data && params.data.actualDueDate
        ? moment(params.data.actualDueDate).format("MMM DD, YYYY")
        : "";
    },
    project: params => {
      return params.data && params.data.project ? params.data.project.projectName : "";
    },
  };
  return Components[columnKey] ? Components[columnKey] : null;
};

export const getComparator = (displayType, groupType, columnKey) => {
  const ComponentsArr = {
    text: (nextObj, prevObj, nextRow, prevRow) => {
      if(typeof nextObj == 'number'){
        return sortNumber(nextObj, prevObj, nextRow, prevRow);
      } else {
        return sortAlphabetically(nextObj, prevObj, nextRow, prevRow);
      }     
    },
    textobject: (nextObj, prevObj, nextRow, prevRow) => {
      return sortAlphabetically(nextObj, prevObj, nextRow, prevRow);
    },
    multivalue : (nextObj, prevObj, nextRow, prevRow) => {
      return sortAlphabetically(nextObj, prevObj, nextRow, prevRow);
    },
    title: (nextObj, prevObj, nextRow, prevRow) => {
      return sortAlphabetically(nextObj, prevObj, nextRow, prevRow);
    },
    fill: (nextObj, prevObj, nextRow, prevRow) => {
      let prevtext = prevObj ? prevObj : "";
      let nextText = nextObj ? nextObj : "";
      if (groupType == "task" && columnKey == "status") {
        prevtext = prevRow.data?.statusTitle.toLowerCase();
        nextText = nextRow.data?.statusTitle.toLowerCase();
      } else {
        prevtext = prevObj;
        nextText = nextObj;
      }

      if (nextText == prevtext) return 0;
      if (nextText < prevtext) return -1;
      if (nextText > prevtext) return 1;
    },
    date: (date1, date2, nextRow, prevRow) => {
      const newDate = date1 ? new Date(date1) : "";
      const prevDate = date2 ? new Date(date2) : "";
      if (newDate == prevDate) return 0;

      return newDate - prevDate;
    },
  };

  return ComponentsArr[displayType];
};
