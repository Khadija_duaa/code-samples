import React, { useRef } from "react";
import CustomButton from "../Buttons/CustomButton";
import { useSelector } from "react-redux";
import Typography from "@material-ui/core/Typography";
import helper from "../../helper";
import isUndefined from "lodash/isUndefined";
import AssigneeDropdown from "../../components/Dropdown/AssigneeDropdown";
import { Circle } from "rc-progress";
import { priorityData } from "../../helper/taskDropdownData";
function CustomCellRenderers(displayType, theme, classes, { members }) {
  const getSelectedPriority = value => {
    return priorityData(theme, classes).find(p => p.value == value)?.label;
  };

  // get fill color of fill displayType from theme
  const getFillData = row => {
    const palette = theme.palette;
    const colorsObj = {
      issue: {
        Priority: { color: palette.issuePriority[row.value], value: row.value },
        Status: { color: palette.issueStatus[row.value], value: row.value },
        Severity: { color: palette.severity[row.value], value: row.value },
        Type: { color: palette.issueType[row.value], value: row.value },
      },
      task: {
        Priority: {
          color: palette.issuePriority[getSelectedPriority(row.value)],
          value: getSelectedPriority(row.value),
        },
        Status: { color: row.data.statusColor, value: row.data.statusTitle },
      },
    };
    return colorsObj[row.colDef.groupType][row.colDef.headerName];
  };
  const Components = {
    text: row => {
      return (
        <div className={classes.titleCnt}>
          <div
            style={{
              fontSize: "13px",
              fontFamily: theme.typography.fontFamilyLato,
              letterSpacing: "normal",
              textAlign: "center",
            }}
            className={`${classes.titleTextCnt} wrapText`}>
            <span className={classes.overviewTitle}>{row.value ? row.value : '-'}</span>
          </div>
        </div>
      );
    },
    title: row => {
      return (
        <div className={classes.titleCnt}>
          <div className={`${classes.titleTextCnt} wrapText`} title={row.value}>
            <span className={classes.overviewTitle}>{row.value ? row.value : '-'} </span>
          </div>
        </div>
      );
    },
    fill: row => {
      let data = getFillData(row);
      return (
        <CustomButton
          variant="contained"
          btnType="status"
          style={{
            background: data.color ? data.color : "#fff",
            color: data.color ? "#fff" : theme.palette.text.primary,
            padding: "7px 10px 7px 6px",
            borderRadius: 0,
            height: "100%",
            width: "100%",
            lineHeight: "25px",
            fontSize: "13px"
          }}>
          {data.value ? data.value : "-"}
        </CustomButton>
      );
    },
    textobject: row => {
      return (
        <div className={`${classes.titleTextCnt} ${classes.textCenter} wrapText`} title={row.value}>
          <span className={classes.overviewTitle}>
            {row.value ? row.value : "-"}{" "}
          </span>
        </div>
      );
    },
    multivalue: row => {
      return (
          <div className={classes.titleCnt}>
            <div
              style={{
                fontSize: "13px",
                fontFamily: theme.typography.fontFamilyLato,
                letterSpacing: "normal",
                textAlign: "center",
              }}
              className={`${classes.titleTextCnt} ${classes.textCenter} wrapText`}>
                <span className={classes.overviewTitle}>{row.value ? row.value : '-'}</span>
            </div>
        </div>
      );
    },
    date: row => {
      return (
        <Typography variant="h6" className={classes.dateText}>
          {row.value ? helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(row.value) : "-"}
        </Typography>
      );
    },
    assignee: row => {
      const assigneeList = members.filter(m => {
        if (row.data[row.column.colId] && row.data[row.column.colId].length) {
          return row.data[row.column.colId].includes(m.userId);
        }
        return;
      });
      return row.data[row.column.colId] && row.data[row.column.colId].length ? (
        <AssigneeDropdown
          assignedTo={row.data[row.column.colId] ? assigneeList : []}
          obj={row.data[row.column.colId]}
          addPermission={false}
          deletePermission={false}
          disabled={true}
        />
      ) : (
        "-"
      );
    },
    progress: row => {
      return (
        <div style={{ display: "flex", alignItems: "center" }}>
          <div style={{ width: 30, height: 30, position: "relative" }}>
            <Circle
              percent={row.value}
              strokeWidth="10"
              trailWidth=""
              trailColor="#dedede"
              strokeColor={"#30d56e"}
            />
            <Typography
              variant="h6"
              align="center"
              style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                fontSize: "10px",
                transform: "translate(-50%, -50%)",
              }}>
              {row.value}
            </Typography>
          </div>
        </div>
      );
    },
  };
  return Components[displayType];
}

export default CustomCellRenderers;
