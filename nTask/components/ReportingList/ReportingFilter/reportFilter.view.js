import React, { useEffect, useState, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { compose } from "redux";

import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import CustomMultiSelectGropedDropdown from "../../../components/Dropdown/multiSelectWithSearchAndGroup/DropdownGrouping";
import { FormattedMessage, injectIntl } from "react-intl";
import { generateAssigneeData } from "../../../helper/generateSelectData";
import CreateableSelectDropdown from "../../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import CircularIcon from "@material-ui/icons/Brightness1";
import {
  dateFilterOptions,
  initFilters,
  taskDateTypes,
  textFilterOptions,
  numberFilterOptions,
} from "./constants";
import CustomDatePicker from "../../../components/DatePicker/DatePicker/DatePicker";
import reportingFilterStyles from "./reportFilter.style";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import { clearTaskFilter, updateTaskFilter } from "../../../redux/actions/tasks";
import moment from "moment";
import SaveCustomFilter from "./saveCustomFilter.view";
import AdvanceFilter from "../../../components/CustomTable2/AdvanceFilter/advanceFilter";
import isEmpty from "lodash/isEmpty";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import UnPlanned from "../../../Views/billing/UnPlanned/UnPlanned";
import DefaultSwitch from "../../../components/Form/Switch";
import { getCustomFields } from "../../../helper/customFieldsData";
import { flags } from "../../../helper/flags";
import DefaultTextField from "../../../components/Form/TextField";
import { calculateAdvancedFilterHeight } from "../../../utils/common";
import { CanAccess, CanAccessFeature } from "../../../components/AccessFeature/AccessFeature.cmp";
import { TRIALPERIOD } from '../../../components/constants/planConstant';
import { clearReportingFilter, updateReportingFilter } from "../../../redux/actions/reportingFilters";
import { priorityData as taskpriorityData } from "../../../helper/taskDropdownData";
import { priorityData, statusData as statusDropDownData } from "../../../helper/issueDropdownData";
import { severity, typeData, generateTaskDropdownData } from "../../../helper/issueDropdownData";


function ReportFilter(props) {
  const { classes, theme, intl, groupType, columns, feature, taskStatusGroup } = props;
  const state = useSelector(state => {
    return {
      projects: state.projects.data,
      members: state.profile.data ? state.profile.data.member.allMembers : [],
      workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate || {},
      overViewFilters: state.reportingFilter?.[feature],
      profileState: state.profile.data || {},
      customFields: state.customFields,
      companyInfo: state.whiteLabelInfo.data,
    };
  });
  const dispatch = useDispatch();
  const { projects, members, workspaceStatus, overViewFilters, profileState, customFields, companyInfo } = state;
  const [showSaveFilter, setShowSaveFilter] = useState(false);
  const [filters, setFilters] = useState({});

  const customFieldsFilters = useMemo(() => {
    const activeFields = getCustomFields(customFields, profileState, groupType);
    let customFieldDate = activeFields.filter(f => f.fieldType == "date");
    let customFieldPeople = activeFields.filter(f => f.fieldType == "people");
    let customFieldCountry = activeFields.filter(f => f.fieldType == "country");
    let customFieldDropdown = activeFields.filter(f => f.fieldType == "dropdown");
    let customFieldRating = activeFields.filter(f => f.fieldType == "rating");
    let customFieldsContainsTextSearch = activeFields
      .filter(
        f =>
          f.fieldType == "textfield" ||
          f.fieldType == "location" ||
          f.fieldType == "websiteurl" ||
          f.fieldType == "filesAndMedia" ||
          f.fieldType == "email"
      )
      .map(item => {
        return {
          key: item.fieldId,
          value: item.fieldName,
          data: textFilterOptions,
          obj: item,
        };
      }); /** those customs fields which contains the text search option */
    let customFieldsContainsNumberSearch = activeFields
      .filter(
        f =>
          f.fieldType == "phone" ||
          f.fieldType == "number" ||
          f.fieldType == "money" ||
          f.fieldType == "formula"
      )
      .map(item => {
        return {
          key: item.fieldId,
          value: item.fieldName,
          data: numberFilterOptions,
          obj: item,
        };
      }); /** those customs fields which contains the text search option */
    return {
      date: customFieldDate,
      people: customFieldPeople,
      country: customFieldCountry,
      dropdown: customFieldDropdown,
      freeSearchCustomFields: customFieldsContainsTextSearch,
      freeNumberSearchCustomFields: customFieldsContainsNumberSearch,
      rating: customFieldRating,
    };
  }, [customFields]);

  const countryData = useMemo(() => {
    return flags.map(f => {
      return {
        label: f.name,
        value: f.dial_code,
        code: f.code,
        obj: f,
        icon: (
          <img
            style={{ width: "20px", height: "15px", marginRight: "5px" }}
            src={`https://flagpedia.net/data/flags/normal/${f.code}.png`}
          />
        ),
      };
    });
  }, [flags]);

  useEffect(() => {
    if (overViewFilters) {
      setFilters(overViewFilters);
    }
  }, [overViewFilters]);
  // Generate list of all projects for dropdown understandable form
  const generateProjectDropdownData = task => {
    const projectsArr = projects.map(project => {
      return {
        label: project.projectName,
        value: project.projectName,
        id: project.projectId,
        obj: project,
      };
    });
    return projectsArr;
  };
  const handleSearch = () => {
    updateReportingFilter(filters, feature, dispatch);
  };
  const handleClearFilter = () => {
    clearReportingFilter(feature, dispatch);
    setFilters({});
  };
  //Function called on select project filter
  const handleProjectChange = (key, options) => {
    const projectsIdsArr = options.map(p => p.id);
    const obj = { ...initFilters.project, type: "", selectedValues: projectsIdsArr };
    setFilters({ ...filters, project: obj });
  };
  //Function called on select issue status filter
  const handleStatusSelect = (key, options) => {
    if (feature == "taskoverview") {
      const obj = { ...initFilters.status, type: "", selectedValues: key };
      setFilters({ ...filters, status: obj });
      return;
    }
    const statusIdsArr = options.map(p => p.value);
    const obj = { ...initFilters.status, type: "", selectedValues: statusIdsArr };
    setFilters({ ...filters, status: obj });
  };
  //Function called on select task status filter
  // const onStatusFilterSelect = options => {
  //   console.log(options);
  // };
  //Function called on select task priority filter
  const handlePrioritySelect = (key, options) => {
    const priorityIdsArr = options.map(p => p.value);
    const obj = { ...initFilters.priority, type: "", selectedValues: priorityIdsArr };
    setFilters({ ...filters, priority: obj });
  };
  //Function called on select assignee
  const handleSelectAssignee = (key, options) => {
    const assigneeIdsArr = options.map(p => p.id);
    const obj = { ...initFilters[key], type: "", selectedValues: assigneeIdsArr };
    setFilters({ ...filters, [key]: obj });
  };
  //Function called on select assignee
  const handleSelectCreatedBy = (key, options) => {
    const assigneeIdsArr = options.map(p => p.id);
    const obj = {
      ...initFilters[key],
      type: "",
      selectedValues: assigneeIdsArr,
      customField: false,
    };
    setFilters({ ...filters, [key]: obj });
  };
  // date filter starts here
  const handleDateFilterSelect = (type, option) => {
    const obj = { ...initFilters[type], type: !option ? "" : option.value, customField: false };
    setFilters({ ...filters, [type]: obj });
  };
  // select custom date filters
  const handleSelectDate = (type, dateType, date = "") => {
    const [fromDate, toDate] = filters[type].selectedValues;
    const formatedDate = date ? moment(date).format("l") : "";
    const dateRange = dateType == "fromDate" ? [formatedDate, toDate] : [fromDate, formatedDate];
    const obj = {
      ...filters[type],
      type: filters[type].type,
      selectedValues: dateRange,
      customField: false,
    };
    setFilters({ ...filters, [type]: obj });
  };
  const handleSelectPeople = (key, options, id) => {
    const assigneeIdsArr = options.map(p => p.id);
    const obj = { type: "", selectedValues: assigneeIdsArr, customField: true };
    setFilters({ ...filters, [id]: obj });
  };
  const handleSelectCountry = (key, options, id) => {
    const countryNameArr = options.map(p => p.label);
    const obj = { type: "", selectedValues: countryNameArr, customField: true };
    setFilters({ ...filters, [id]: obj });
  };
  const handleSelectRating = (key, options, id) => {
    const idsArr = options.map(p => p.value);
    const obj = { type: "", selectedValues: idsArr, customField: true };
    setFilters({ ...filters, [id]: obj });
  };
  const handleSelectDropdown = (key, options, id, isMulti) => {
    const idsArr = options.map(p => p.id);
    const obj = { type: isMulti ? "multi" : "single", selectedValues: idsArr, customField: true };
    setFilters({ ...filters, [id]: obj });
  };
  const handleSelectTextSearchOption = (type, option) => {
    const obj = {
      type: option ? option.value : "",
      customField: true,
      selectedValues: "",
    };
    setFilters({ ...filters, [type]: obj });
  };
  const handleSelectNumberSearchOption = (type, option) => {
    const obj = {
      type: option ? option.value : "",
      customField: true,
      selectedValues: "",
    };
    setFilters({ ...filters, [type]: obj });
  };
  const handleChangeTextSearch = (e, id) => {
    setFilters({ ...filters, [id]: { ...filters[id], selectedValues: e.target.value } });
  };
  const handleChangeNumberSearch = (e, id) => {
    setFilters({ ...filters, [id]: { ...filters[id], selectedValues: e.target.value } });
  };
  const handleCustomDateFilterSelect = (type, option) => {
    const obj = { type: !option ? "" : option.value, customField: true, selectedValues: [] };
    setFilters({ ...filters, [type]: obj });
  };
  const handleChangeNumberSearchRange = (id, rangeType, value = "") => {
    let [from, to] = (filters[id].selectedValues);
    // from = Number(from)
    // to = Number(to)
    // value = Number(value)
    // const range = rangeType == "from" ? [String(value) , to && String(to > value ? to : value + 1)] : [String(from) , String(value > from ? value : from + 1)];
    const range = rangeType == "from" ? [value , to && String(Number(to) > Number(value) ? Number(to) : Number(value) + 1)] : [from , String(Number(value) > Number(from) ? Number(value) : Number(from) + 1)];
    setFilters({ ...filters, [id]: { ...filters[id], selectedValues: range } });
  };
  const handleSelectCustomDate = (type, dateType, date = "") => {
    const [fromDate, toDate] = filters[type].selectedValues;
    const formatedDate = date ? moment(date).format("l") : "";
    const dateRange = dateType == "fromDate" ? [formatedDate, toDate] : [fromDate, formatedDate];
    const obj = { type: "custom", selectedValues: dateRange, customField: true };
    setFilters({ ...filters, [type]: obj });
  };

  const handleSeveritySelect = (key, options) => {
    const severityArr = options.map(p => p.value);
    const obj = { ...initFilters.severity, type: "", selectedValues: severityArr };
    setFilters({ ...filters, severity: obj });
  };

  const handleTypeSelect = (key, options) => {
    const typeArr = options.map(p => p.value);
    const obj = { ...initFilters.type, type: "", selectedValues: typeArr };
    setFilters({ ...filters, type: obj });
  };
  const handleTaskChange = (key, options) => {
    const tasksIdsArr = options.map(p => p.id);
    const obj = { ...initFilters.tasks, type: "", selectedValues: tasksIdsArr };
    setFilters({ ...filters, tasks: obj });
  };
  //Show Add Custom filter View
  const handleShowSaveFilter = () => {
    setShowSaveFilter(true);
  };
  const createNewFilterCallback = () => {
    setShowSaveFilter(false);
  };
  // generate data for dropdowns
  const projectsData = generateProjectDropdownData();
  const assigneeData = generateAssigneeData(members);
  // const statusData = generateStatusData();
  const issueOverviewDdData = statusDropDownData(theme, classes, intl);
  const tasksData = generateTaskDropdownData()
  // const priorityDData = priorityData(theme, classes);
  const severityData = severity(theme, classes, intl);
  const issueTypeData = typeData(theme, classes, intl);
  const priorityDData = feature == "taskoverview" ? taskpriorityData(theme, classes) : priorityData(theme, classes);

  // set slected values from redux
  const selectedProject =
    !isEmpty(filters.project) &&
    projectsData.filter(p => filters.project.selectedValues.includes(p.id));
  const selectedAssigneeList =
    !isEmpty(filters.assigneeList) &&
    assigneeData.filter(a => filters.assigneeList.selectedValues.includes(a.id));
  const selectedAssignee =
    !isEmpty(filters.assignee) &&
    assigneeData.filter(a => filters.assignee.selectedValues.includes(a.id));
  const selectedCreatedBy =
    !isEmpty(filters.createdBy) &&
    assigneeData.filter(a => filters.createdBy.selectedValues.includes(a.id));
  const selectedUpdatedBy =
    !isEmpty(filters.updatedBy) &&
    assigneeData.filter(a => filters.updatedBy.selectedValues.includes(a.id));
  // selected status filters
  const selectedStatus =
    (!isEmpty(filters.status) && feature == "issueoverview") ?
      issueOverviewDdData.filter(s => filters.status.selectedValues.includes(s.value)) :
      filters.status?.selectedValues || [];
  const selectedPriority =
    !isEmpty(filters.priority) &&
    priorityDData.filter(s => filters.priority.selectedValues.includes(s.value));
  const selectedSeverity =
    !isEmpty(filters.severity) &&
    severityData.filter(s => filters.severity.selectedValues.includes(s.value));
  const selectedType =
    !isEmpty(filters.type) &&
    issueTypeData.filter(s => filters.type.selectedValues.includes(s.value));
  const selectedTask =
    !isEmpty(filters.tasks) &&
    tasksData.filter(p => filters.tasks.selectedValues.includes(p.id));
  const isFilterApplied = !isEmpty(overViewFilters);
  let customFieldDateArr = customFieldsFilters.date.map(item => {
    return {
      key: item.fieldId,
      value: item.fieldName,
      data: dateFilterOptions,
    };
  });

  const checkFliterInclude = (fieldName) => {
    if (columns.some(c => c.columnKey == fieldName)) {
      return true
    }
  }


  return (
    <>
      <AdvanceFilter isFilterApplied={isFilterApplied}>
        <div className={classes.headingCnt}>
          <Typography variant="h3">{groupType == 'task' ? 'Task' : 'Issue'} Filters</Typography>
          {teamCanView("advanceFilterAccess") && (
            <CustomButton variant="text" btnType="plain" onClick={handleClearFilter}>
              <span className={classes.clearFilterText}>Clear Filter</span>
            </CustomButton>
          )}
        </div>
        {!teamCanView("advanceFilterAccess") ? (
          <div className={classes.unplannedMain}>
            <UnPlanned
              feature="premium"
              titleTxt={
                <FormattedMessage
                  id="common.discovered-dialog.premium-title"
                  defaultMessage="Wow! You've discovered a Premium feature!"
                />
              }
              boldText={intl.formatMessage({
                id: "common.discovered-dialog.list.custom-filter.title",
                defaultMessage: "Custom Filters",
              })}
              descriptionTxt={
                <FormattedMessage
                  id="common.discovered-dialog.list.custom-filter.label"
                  defaultMessage={"is available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."}
                  values={{ TRIALPERIOD: TRIALPERIOD }}
                />
              }
              showBodyImg={false}
              showDescription={true}
            />
          </div>
        ) : showSaveFilter ? (
          <SaveCustomFilter
            feature={"task"}
            currentFilterValues={filters}
            createNewFilterCallback={createNewFilterCallback}
          />
        ) : (
          <>
            <div
              className={classes.filterContentCnt}
              style={{ height: calculateAdvancedFilterHeight() - 210 }}>
              {checkFliterInclude('project') ?
                <SelectSearchDropdown
                  data={() => projectsData}
                  label={<FormattedMessage id="project.project" defaultMessage="Project" />}
                  selectChange={handleProjectChange}
                  type="project"
                  styles={{ marginBottom: 10 }}
                  isMulti={true}
                  selectedValue={selectedProject}
                  placeholder={
                    <FormattedMessage
                      id="task.creation-dialog.form.project.placeholder"
                      defaultMessage="Select Project"
                    />
                  }
                /> : null}
              {checkFliterInclude('severity') ?
                <SelectSearchDropdown
                  data={() => severityData}
                  label={
                    <FormattedMessage id="common.bulk-action.severity" defaultMessage="Select Severity" />
                  }
                  styles={{ marginBottom: 10 }}
                  icon={true}
                  selectChange={handleSeveritySelect}
                  type="severity"
                  isMulti={true}
                  selectedValue={selectedSeverity}
                  placeholder={
                    <FormattedMessage
                      id="common.bulk-action.severity"
                      defaultMessage="Select Severity"
                    />
                  }
                /> : null}
              {checkFliterInclude('type') ?
                <SelectSearchDropdown
                  data={() => issueTypeData}
                  label={
                    <FormattedMessage id="common.bulk-action.type" defaultMessage="Select Type" />
                  }
                  styles={{ marginBottom: 10 }}
                  icon={true}
                  selectChange={handleTypeSelect}
                  type="type"
                  isMulti={true}
                  selectedValue={selectedType}
                  placeholder={
                    <FormattedMessage id="common.bulk-action.type" defaultMessage="Select Type" />
                  }
                /> : null}
              {checkFliterInclude('tasks') ?
                <SelectSearchDropdown
                  data={() => tasksData}
                  label={<FormattedMessage id="common.action.risk.label1" defaultMessage="Select Task" />}
                  selectChange={handleTaskChange}
                  type="tasks"
                  styles={{ marginBottom: 10 }}
                  isMulti={true}
                  selectedValue={selectedTask}
                  placeholder={
                    <FormattedMessage
                      id="common.action.risk.label1"
                      defaultMessage="Select Task"
                    />
                  }
                /> : null}
              {checkFliterInclude('assigneeList') ?
                <SelectSearchDropdown
                  data={() => assigneeData}
                  label={<FormattedMessage id="common.assigned.label" defaultMessage="Assigned To" />}
                  selectChange={handleSelectAssignee}
                  type="assigneeList"
                  styles={{ marginBottom: 10 }}
                  isMulti={true}
                  selectedValue={selectedAssigneeList}
                  placeholder={
                    <FormattedMessage
                      id="risk.creation-dialog.form.assign-to.placeholder"
                      defaultMessage="Select Assignee"
                    />
                  }
                  avatar={true}
                /> : null}
              {checkFliterInclude('assignee') ?
                <SelectSearchDropdown
                  data={() => assigneeData}
                  label={<FormattedMessage id="common.assigned.label" defaultMessage="Assigned" />}
                  selectChange={handleSelectAssignee}
                  type="assignee"
                  styles={{ marginBottom: 10 }}
                  isMulti={true}
                  selectedValue={selectedAssignee}
                  placeholder={
                    <FormattedMessage
                      id="risk.creation-dialog.form.assign-to.placeholder"
                      defaultMessage="Select Assignee"
                    />
                  }
                  avatar={true}
                /> : null}
              {checkFliterInclude('status') ? (
                feature == "issueoverview" ?
                  <SelectSearchDropdown
                    data={() => issueOverviewDdData}
                    label={
                      <FormattedMessage id="common.bulk-action.status" defaultMessage="Select Status" />
                    }
                    styles={{ marginBottom: 10 }}
                    icon={true}
                    selectChange={handleStatusSelect}
                    type="status"
                    isMulti={true}
                    selectedValue={selectedStatus}
                    placeholder={
                      <FormattedMessage id="common.bulk-action.status" defaultMessage="Select Status" />
                    }
                  /> :
                  // customSearchDropdown
                  <CustomMultiSelectGropedDropdown
                    label="Status"
                    option={selectedStatus}
                    options={() => taskStatusGroup}
                    optionIcon={true}
                    onSelect={handleStatusSelect}
                    placeholder=""
                    height="140px"
                    width="140px"
                    valueSelector={"value"}
                    scrollHeight={400}
                    styles={{ marginBottom: 10 }}
                  />
              )
                : null}
              {checkFliterInclude('priority') ?
                <SelectSearchDropdown
                  data={() => priorityDData}
                  label={
                    <FormattedMessage
                      id="task.detail-dialog.priority.label"
                      defaultMessage="Priority"
                    />
                  }
                  selectChange={handlePrioritySelect}
                  type="priority"
                  styles={{ marginBottom: 10 }}
                  selectedValue={selectedPriority}
                  placeholder={
                    <FormattedMessage
                      id="issue.common.priority.placeholder"
                      defaultMessage="Select Priority"
                    />
                  }
                  icon={true}
                /> : null}
              {taskDateTypes.map(d => {
                const dateFrom =
                  filters[d.key] &&
                    filters[d.key].selectedValues.length &&
                    filters[d.key].selectedValues[0]
                    ? filters[d.key].selectedValues[0]
                    : "";
                const dateTo =
                  filters[d.key] &&
                    filters[d.key].selectedValues &&
                    filters[d.key].selectedValues[1]
                    ? filters[d.key].selectedValues[1]
                    : "";
                const selectedValue =
                  (filters[d.key] && d.data.find(o => o.value == filters[d.key].type)) || "";
                const isCustomSelectedValue = selectedValue && selectedValue.value == "custom";
                return (
                  checkFliterInclude(d.key) ?
                    <>
                      <SelectSearchDropdown
                        data={() => d.data}
                        label={d.value}
                        styles={{ marginBottom: isCustomSelectedValue ? 5 : 10 }}
                        isMulti={false}
                        placeholder={`Select ${d.value}`}
                        selectChange={handleDateFilterSelect}
                        type={d.key}
                        selectedValue={selectedValue}
                        isClearable={true}
                        selectClear={handleDateFilterSelect}
                      />
                      {isCustomSelectedValue && (
                        <div className={classes.datePickerRangeCnt}>
                          <div
                            style={{
                              margin: "0px 6px 0 0",
                              border: "1px solid #DDDDDD",
                              borderRadius: "4px",
                              padding: 6,
                              flex: 1,
                            }}>
                            <CustomDatePicker
                              date={dateFrom || ""}
                              label={"From:"}
                              PopperProps={{ disablePortal: true, size: null }}
                              icon={false}
                              dateFormat="MMM DD, YYYY"
                              timeInput={false}
                              onSelect={date => {
                                handleSelectDate(d.key, "fromDate", date);
                              }}
                              disabled={false}
                              deleteIcon={true}
                              placeholder={"Select Date"}
                              containerProps={{ style: { alignItems: "center" } }}
                              btnProps={{
                                style: {
                                  background: "transparent",
                                  border: "none",
                                  padding: 0,
                                  textAlign: "left",
                                },
                              }}
                              labelProps={{
                                style: {
                                  width: "auto",
                                  marginTop: 0,
                                  marginRight: 5,
                                  fontSize: "14px !important",
                                },
                              }}
                              closeOnDateSelect={true}
                              datePickerProps={{
                                filterDate: date => {
                                  return dateTo
                                    ? moment(date).isBefore(dateTo, "day") ||
                                    moment(date).isSame(dateTo, "day")
                                    : true;
                                },
                              }}
                            />
                          </div>
                          <div
                            style={{
                              margin: 0,
                              border: "1px solid #DDDDDD",
                              borderRadius: "4px",
                              padding: 6,
                              flex: 1,
                            }}>
                            <CustomDatePicker
                              date={dateTo}
                              label={"To:"}
                              PopperProps={{ disablePortal: true, size: null }}
                              icon={false}
                              dateFormat="MMM DD, YYYY"
                              timeInput={false}
                              onSelect={date => {
                                handleSelectDate(d.key, "toDate", date);
                              }}
                              disabled={false}
                              deleteIcon={true}
                              placeholder={"Select Date"}
                              containerProps={{ style: { alignItems: "center" } }}
                              btnProps={{
                                style: {
                                  background: "transparent",
                                  border: "none",
                                  padding: 0,
                                  textAlign: "left",
                                },
                              }}
                              labelProps={{
                                style: {
                                  width: "auto",
                                  marginTop: 0,
                                  marginRight: 5,
                                  fontSize: "14px !important",
                                },
                              }}
                              closeOnDateSelect={true}
                              datePickerProps={{
                                filterDate: date => {
                                  return dateFrom
                                    ? moment(date).isAfter(dateFrom, "day") ||
                                    moment(date).isSame(dateFrom, "day")
                                    : true;
                                },
                              }}
                            />
                          </div>
                        </div>
                      )}
                    </>
                    : null
                )
              })}
              {checkFliterInclude('createdBy') ?
                <SelectSearchDropdown
                  data={() => assigneeData}
                  label="Created By"
                  selectChange={handleSelectCreatedBy}
                  type="createdBy"
                  styles={{ marginBottom: 10 }}
                  isMulti={true}
                  selectedValue={selectedCreatedBy}
                  placeholder="Select Created By"
                  avatar={true}
                /> : null}
              {checkFliterInclude('updatedBy') ?
                <SelectSearchDropdown
                  data={() => assigneeData}
                  label="Updated By"
                  selectChange={handleSelectCreatedBy}
                  type="updatedBy"
                  styles={{ marginBottom: 10 }}
                  isMulti={true}
                  selectedValue={selectedUpdatedBy}
                  placeholder="Select Updated By"
                  avatar={true}
                /> : null}

              {customFieldDateArr.map(d => {
                const dateFrom =
                  filters[d.key] &&
                    filters[d.key].selectedValues.length &&
                    filters[d.key].selectedValues[0]
                    ? filters[d.key].selectedValues[0]
                    : "";
                const dateTo =
                  filters[d.key] &&
                    filters[d.key].selectedValues &&
                    filters[d.key].selectedValues[1]
                    ? filters[d.key].selectedValues[1]
                    : "";
                const selectedValue =
                  (filters[d.key] && d.data.find(o => o.value == filters[d.key].type)) || "";
                const isCustomSelected = selectedValue && selectedValue.value == "custom";
                return (
                  <>
                    <SelectSearchDropdown
                      data={() => d.data}
                      label={d.value}
                      styles={{ marginBottom: isCustomSelected ? 5 : 10 }}
                      isMulti={false}
                      placeholder={`Select ${d.value}`}
                      selectChange={handleCustomDateFilterSelect}
                      type={d.key}
                      selectedValue={selectedValue}
                      isClearable={true}
                      selectClear={handleCustomDateFilterSelect}
                    />
                    {isCustomSelected && (
                      <div className={classes.datePickerRangeCnt}>
                        <div
                          style={{
                            margin: "0px 6px 0 0",
                            border: "1px solid #DDDDDD",
                            borderRadius: "4px",
                            padding: 6,
                            flex: 1,
                          }}>
                          <CustomDatePicker
                            date={dateFrom || ""}
                            label={"From:"}
                            PopperProps={{ disablePortal: true, size: null }}
                            icon={false}
                            dateFormat="MMM DD, YYYY"
                            timeInput={false}
                            onSelect={date => {
                              handleSelectCustomDate(d.key, "fromDate", date);
                            }}
                            disabled={false}
                            deleteIcon={true}
                            placeholder={"Select Date"}
                            containerProps={{ style: { alignItems: "center" } }}
                            btnProps={{
                              style: {
                                background: "transparent",
                                border: "none",
                                padding: 0,
                                textAlign: "left",
                              },
                            }}
                            labelProps={{
                              style: {
                                width: "auto",
                                marginTop: 0,
                                marginRight: 5,
                                fontSize: "14px !important",
                              },
                            }}
                            closeOnDateSelect={true}
                            datePickerProps={{
                              filterDate: date => {
                                return dateTo
                                  ? moment(date).isBefore(dateTo, "day") ||
                                  moment(date).isSame(dateTo, "day")
                                  : true;
                              },
                            }}
                          />
                        </div>
                        <div
                          style={{
                            margin: 0,
                            border: "1px solid #DDDDDD",
                            borderRadius: "4px",
                            padding: 6,
                            flex: 1,
                          }}>
                          <CustomDatePicker
                            date={dateTo}
                            label={"To:"}
                            PopperProps={{ disablePortal: true, size: null }}
                            icon={false}
                            dateFormat="MMM DD, YYYY"
                            timeInput={false}
                            onSelect={date => {
                              handleSelectCustomDate(d.key, "toDate", date);
                            }}
                            disabled={false}
                            deleteIcon={true}
                            placeholder={"Select Date"}
                            containerProps={{ style: { alignItems: "center" } }}
                            btnProps={{
                              style: {
                                background: "transparent",
                                border: "none",
                                padding: 0,
                                textAlign: "left",
                              },
                            }}
                            labelProps={{
                              style: {
                                width: "auto",
                                marginTop: 0,
                                marginRight: 5,
                                fontSize: "14px !important",
                              },
                            }}
                            closeOnDateSelect={true}
                            datePickerProps={{
                              filterDate: date => {
                                return dateFrom
                                  ? moment(date).isAfter(dateFrom, "day") ||
                                  moment(date).isSame(dateFrom, "day")
                                  : true;
                              },
                            }}
                          />
                        </div>
                      </div>
                    )}
                  </>
                );
              })}
              {customFieldsFilters.people.map(cf => {
                const selectedValue =
                  (filters[cf.fieldId] &&
                    filters[cf.fieldId].selectedValues &&
                    assigneeData.filter(a => filters[cf.fieldId].selectedValues.includes(a.id))) ||
                  [];
                return (
                  <SelectSearchDropdown
                    data={() => assigneeData}
                    label={cf.fieldName}
                    selectChange={(type, option) => handleSelectPeople(type, option, cf.fieldId)}
                    type={cf.fieldName}
                    styles={{ marginBottom: 10 }}
                    isMulti={true}
                    selectedValue={selectedValue}
                    placeholder="Select"
                    avatar={true}
                  />
                );
              })}
              {customFieldsFilters.country.map(cf => {
                const selectedValue =
                  (filters[cf.fieldId] &&
                    filters[cf.fieldId].selectedValues &&
                    countryData.filter(a =>
                      filters[cf.fieldId].selectedValues.includes(a.label)
                    )) ||
                  [];
                return (
                  <SelectSearchDropdown
                    data={() => countryData}
                    label={cf.fieldName}
                    selectChange={(type, option) => handleSelectCountry(type, option, cf.fieldId)}
                    type={cf.fieldName}
                    styles={{ marginBottom: 10 }}
                    isMulti={true}
                    selectedValue={selectedValue}
                    placeholder="Select"
                    avatar={false}
                    icon={true}
                  />
                );
              })}
              {customFieldsFilters.dropdown.map(cf => {
                const ddata = cf.values.data.map(item => {
                  return {
                    label: item.value,
                    value: item.id,
                    color: item.color,
                    obj: item,
                    id: item.id,
                  };
                });
                const selectedValue =
                  (filters[cf.fieldId] &&
                    filters[cf.fieldId].selectedValues &&
                    ddata.filter(a => filters[cf.fieldId].selectedValues.includes(a.id))) ||
                  [];
                return (
                  <SelectSearchDropdown
                    data={() => ddata}
                    label={cf.fieldName}
                    selectChange={(type, option) =>
                      handleSelectDropdown(type, option, cf.fieldId, cf.settings.multiSelect)
                    }
                    type={cf.fieldName}
                    styles={{ marginBottom: 10 }}
                    isMulti={true}
                    selectedValue={selectedValue}
                    placeholder="Select"
                    avatar={false}
                    icon={true}
                    optionBackground={true}
                  />
                );
              })}
              {customFieldsFilters.freeSearchCustomFields.map(d => {
                const selectedOption =
                  (filters[d.key] &&
                    filters[d.key].type &&
                    d.data.filter(a => a.value === filters[d.key].type)) ||
                  [];
                const freeTextSearchField = selectedOption.length > 0;
                return (
                  <>
                    <SelectSearchDropdown
                      data={() => d.data}
                      label={d.value}
                      styles={{ marginBottom: freeTextSearchField ? 5 : 10 }}
                      isMulti={false}
                      placeholder={`Select`}
                      selectChange={(key, option) => handleSelectTextSearchOption(key, option)}
                      type={d.key}
                      selectedValue={selectedOption}
                      isClearable={true}
                      selectClear={(key, option) => handleSelectTextSearchOption(key, option)}
                    />
                    {freeTextSearchField && (
                      <div>
                        <DefaultTextField
                          fullWidth={true}
                          errorState={false}
                          errorMessage={""}
                          defaultProps={{
                            id: d.key,
                            onChange: e => handleChangeTextSearch(e, d.key),
                            value: filters[d.key].selectedValues,
                            placeholder: "Filter..",
                          }}
                        />
                      </div>
                    )}
                  </>
                );
              })}
              {customFieldsFilters.freeNumberSearchCustomFields.map(d => {
                const From =
                  filters[d.key] &&
                    filters[d.key].selectedValues.length &&
                    filters[d.key].selectedValues[0]
                    ? filters[d.key].selectedValues[0]
                    : "";
                const To =
                  filters[d.key] &&
                    filters[d.key].selectedValues &&
                    filters[d.key].selectedValues[1]
                    ? filters[d.key].selectedValues[1]
                    : "";
                const selectedOption =
                  (filters[d.key] &&
                    filters[d.key].type &&
                    d.data.filter(a => a.value === filters[d.key].type)) ||
                  [];
                const freeNumberSearchField = selectedOption.length > 0;
                return (
                  <>
                    <SelectSearchDropdown
                      data={() => d.data}
                      label={d.value}
                      styles={{ marginBottom: freeNumberSearchField ? 5 : 10 }}
                      isMulti={false}
                      placeholder={`Select`}
                      selectChange={(key, option) => handleSelectNumberSearchOption(key, option)}
                      type={d.key}
                      selectedValue={selectedOption}
                      isClearable={true}
                      selectClear={(key, option) => handleSelectNumberSearchOption(key, option)}
                    />
                    {freeNumberSearchField && selectedOption[0].value !== "inRange" && (
                      <div>
                        <DefaultTextField
                          fullWidth={true}
                          errorState={false}
                          errorMessage={""}
                          defaultProps={{
                            id: d.key,
                            type: "number",
                            onChange: e => handleChangeNumberSearch(e, d.key),
                            value: filters[d.key].selectedValues,
                            placeholder: "Filter..",
                          }}
                        />
                      </div>
                    )}
                    {selectedOption.length > 0 && selectedOption[0].value == "inRange" && (
                      <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <DefaultTextField
                          fullWidth={true}
                          errorState={false}
                          errorMessage={""}
                          formControlStyles={{ marginRight: 10 }}
                          defaultProps={{
                            id: d.key,
                            type: "number",
                            onChange: e =>
                              handleChangeNumberSearchRange(d.key, "from", e.target.value),
                            value: From,
                            placeholder: "Range from..",
                          }}
                        />
                        <DefaultTextField
                          fullWidth={true}
                          errorState={false}
                          errorMessage={""}
                          defaultProps={{
                            id: d.key,
                            type: "number",
                            onChange: e =>
                              handleChangeNumberSearchRange(d.key, "to", e.target.value),
                            value: To,
                            placeholder: "Range to..",
                          }}
                        />
                      </div>
                    )}
                  </>
                );
              })}
              {customFieldsFilters.rating.map(cf => {
                let array = new Array(cf.settings.scale);
                array.fill("");
                const ddata = array.map((item, index) => {
                  let emojiArr = new Array(index + 1);
                  emojiArr.fill("");
                  let emoji = emojiArr.map(item => cf.settings.emoji);
                  return {
                    label: emoji,
                    value: index + 1,
                  };
                });
                const selectedValue =
                  (filters[cf.fieldId] &&
                    filters[cf.fieldId].selectedValues &&
                    ddata.filter(a => filters[cf.fieldId].selectedValues.includes(a.value))) ||
                  [];
                return (
                  <SelectSearchDropdown
                    data={() => ddata}
                    label={cf.fieldName}
                    selectChange={(type, option) => handleSelectRating(type, option, cf.fieldId)}
                    type={cf.fieldName}
                    styles={{ marginBottom: 10 }}
                    isMulti={true}
                    selectedValue={selectedValue}
                    placeholder="Select"
                    avatar={false}
                    icon={true}
                    optionBackground={false}
                  />
                );
              })}
            </div>
            <div className={classes.searchOuterCnt}>
              <div className={classes.searchBtnCnt}>
                {/* <CustomButton
                  style={{ flex: 1, marginRight: 20 }}
                  btnType="success"
                  variant="contained"
                  onClick={handleShowSaveFilter}
                // disabled={saveBtnQuery == "progress"}
                // query={saveFilter ? saveBtnQuery : null}
                >
                  <FormattedMessage id="filters.save.label" defaultMessage="Save Filter" />
                </CustomButton> */}
                <CustomButton
                  style={{ flex: 1 }}
                  btnType="success"
                  variant="contained"
                  onClick={handleSearch}
                // disabled={saveBtnQuery == "progress"}
                // query={saveFilter ? saveBtnQuery : null}
                >
                  <FormattedMessage id="common.search.label" defaultMessage="Search" />
                </CustomButton>
              </div>
            </div>
          </>
        )}
      </AdvanceFilter>
    </>
  );
}

export default compose(injectIntl, withStyles(reportingFilterStyles, { withTheme: true }))(ReportFilter);
