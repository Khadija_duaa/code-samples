const timerStyles = theme => ({
  header: {
    padding: "8px 10px",
    background: theme.palette.background.paper,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  timerButton: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "0 4px 4px 0",
    padding: "6px 2px 6px 10px",
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    borderLeft: 0,
    "&:hover": {
      background: theme.palette.common.white
    }
  },
  headerTimerBtnCnt: {
    marginRight: 10,
    paddingRight: 20,
    borderRight: `1px solid ${theme.palette.border.lightBorder}`
  },
  timerHeaderButton: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    padding: "7px 10px",
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white
    }
  },
  timerBtnIcon: {
    marginRight: 10,
    fontSize: "24px !important"
  },
  timerBtnArrow: {
    marginLeft: 5
  },
  mainButton: {
    background: theme.palette.common.items,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "4px 0 0 4px",
    minWidth: 30,
    width: 30,
    padding: "6px 18px",
    "&:hover": {
      background: theme.palette.common.items
    }
  },
  dropdownButtons: {
    background: theme.palette.background.white,
    borderRadius: 0,
    minWidth: 30,
    width: 30,
    "&:hover": {
      background: theme.palette.background.white
    }
  },
  stopIcon: {
    color: theme.palette.statusColor.red
  },
  playIcon: {
    color: theme.palette.statusColor.green
  },
  pauseIcon: {
    color: theme.palette.statusColor.green
  },
  resetIcon: {
    color: theme.palette.statusColor.orange,
    fontSize: "20px !important"
  },
  profilePic: {
    width: 34,
    height: 34,
    marginRight: 10
  },
  // userTimeEntryList:{
  //   padding: "0 15px"
  // },
  userTimeEntryListItem: {
    padding: "0 12px 0 12px",
    "&:not(:last-child)": {
      "& $userTimeEntryItemInner": {
        borderBottom: `1px solid ${theme.palette.border.extraLightBorder}`
      }
    }
  },
  userEstiTimeEntryListItem: {
    padding: 0
  },
  userTimeEntryItemInner: {
    flex: 1,
    padding: "7px 0"
  },
  singleTimeEntrySubMenu: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between"
  },
  subMenuList: {
    background: theme.palette.background.paper,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`
  },
  singleUserTotalTimeCnt: {
    marginLeft: 10
  },
  singleUserTotalTime: {
    fontSize: "12px !important",
    color: theme.palette.text.secondary,
    margin: 0,
    lineHeight: "normal"
  },
  userFullName: {
    color: theme.palette.text.secondary
  },
  closeIcon: {
    fontSize: "12px !important"
  },
  editIcon: {
    fontSize: "16px !important"
  },
  singleTimeEntryDate: {
    marginRight: 5
  },
  singleEntryTime: {
    color: theme.palette.text.primary
  },
  manualDurationInput: {
    display: "flex",
    flexDirection: "column",
    padding: "10px 10px 0 10px"
  },
  manualTimeEntryCnt: {
    display: "flex",
    alignItems: "center"
  },
  estimatedTimeEntryCnt: {
    display: "flex",
    alignItems: "center"
  },
  estimatedDurationInput: {
    display: "flex",
    flexDirection: "column",
    padding: 10
  },
  estimatedTimeText: {
    fontSize: "12px !important",
    color: theme.palette.text.secondary,
    textDecoration: "underline",
    cursor: "pointer"
  },
  checkIcon: {
    color: theme.palette.common.white
  },
  timerHeading: {
    color: theme.palette.text.primary
  },
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    margin: "10px 0 5px 0"
  },
  toggleBtnGroup: {
    borderRadius: 4,
    background: theme.palette.common.white,
    border: "none",
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.azure,
      backgroundColor: theme.palette.background.azureLight,
      "&:after": {
        background: theme.palette.common.white
      },
      "&:hover": {
        background: theme.palette.background.azureLight,
        color: theme.palette.text.azure
      }
    }
  },
  toggleButton: {
    height: "auto",
    padding: "5px 28px",
    fontSize: "12px",
    textTransform: "capitalize",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    background: theme.palette.background.items,
    border: 'none ',
    "&:hover": {
      background: theme.palette.background.medium
    },
    "&[value = 'left']": {
      borderRight: `2px solid ${theme.palette.common.white}`
    }
  },
  toggleButtonSelected: {},
  noEffortCnt: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "20px 0"
  },
  noEffortText: {
    margin: "10px 0 0 0"
  },
  timeLogIcon: {
    fontSize: 64
  },
  noEffortText: {
    fontSize: "16px",
    margin: 0,
    color: theme.palette.text.medGray
  }
});

export default timerStyles;
