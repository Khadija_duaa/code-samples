import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import customMenuStyles from "../../assets/jss/components/customMenu";
import classNames from "classnames"
import Fade from '@material-ui/core/Fade';
class DropdownMenu extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { classes, theme, children, closeAction, size, timer, ...rest } = this.props;
    const popperClasses = classNames([
      classes[`popper${size}`], classes.popper
    ])

    return (
      <React.Fragment>
        <Popper
          {...rest}
          className={popperClasses}
          disablePortal
          modifiers={{
            flip: {
              enabled: true,
            },
            preventOverflow: {
              enabled: true,
              boundariesElement: 'scrollParent',
            },
          }}
          transition
          onClick={e => e.stopPropagation()}
        >
          {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
              <Paper
                classes={{
                  root: classes.paperRoot
                }}
              >
                <ClickAwayListener mouseEvent="onMouseUp"
                  touchEvent="onTouchEnd" onClickAway={closeAction}>
                  {children}
                </ClickAwayListener>
              </Paper>
            </Fade>
          )}
        </Popper>
      </React.Fragment>
    );
  }
}

export default withStyles(customMenuStyles, {
  withTheme: true
})(DropdownMenu);
