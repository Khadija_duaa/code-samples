import React, { Component } from "react";
import cloneDeep from "lodash/cloneDeep";
import { compose } from "redux";
import { connect } from "react-redux";
import find from "lodash/find";
import filter from "lodash/filter";
import uniqBy from "lodash/unionBy";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import DropdownMenu from "./DropdownMenu";
import timerStyles from "./style";
import StopIcon from "@material-ui/icons/Stop";
import PlayIcon from "@material-ui/icons/PlayArrow";
import ResetIcon from "@material-ui/icons/Replay";
import PauseIcon from "@material-ui/icons/Pause";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Collapse from "@material-ui/core/Collapse";
import UpArrow from "@material-ui/icons/ArrowDropUp";
import DownArrow from "@material-ui/icons/ArrowDropDown";
import TimerIcon from "@material-ui/icons/AvTimer";
import CustomAvatar from "../Avatar/Avatar";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../Buttons/IconButton";
import DurationInput from "../DatePicker/DurationInput";
import CheckIcon from "@material-ui/icons/Check";
import AddIcon from "@material-ui/icons/Add";
import CustomIconButton from "../Buttons/CustomIconButton";
import moment from "moment";
import { getCompletePermissionsWithArchieve } from "../../Views/Task/permissions";
import { calTotalTime } from "../../Views/Timesheet/timingFunctions";
import { generateUsername } from "../../utils/common";
import { getDurationParts } from "../../utils/moment";
import { getTaskById, getTaskFromData } from "../../helper/getTaskFromTasksData";
import { taskDetailsHelpText } from "../Tooltip/helptext";
import CustomTooltip from "../Tooltip/Tooltip";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import { getTimerValues } from "../../redux/actions/tasks";
import { Scrollbars } from "react-custom-scrollbars";
import Divider from "@material-ui/core/Divider";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import CustomButton from "../Buttons/CustomButton";
import EditIcon from "@material-ui/icons/EditOutlined";
import TimeLog from "@material-ui/icons/Timer";
import { message } from "../../utils/constants/errorMessages/timer";
import isEmpty from "lodash/isEmpty";
import { GetPermission } from "../permissions";
import { FormattedMessage, injectIntl } from "react-intl";
class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      btnDropdownOpen: false,
      subMenuOpen: false,
      addEstimatedTime: true,
      estimatedTimeDisabledStatus: false,
      time: 0,
      isOn: false,
      start: 0,
      sHours: "",
      sMins: "",
      estHours: "",
      estMins: "",
      currentTask: {},
      alignment: "left",
      manualTime: false,
      permissionAccess: null,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
    this.pauseTimer = this.pauseTimer.bind(this);
    this.handleBtnDropdownClose = this.handleBtnDropdownClose.bind(this);
    this.handleBtnDropdownOpen = this.handleBtnDropdownOpen.bind(this);
    this.estimatedTimeClick = this.estimatedTimeClick.bind(this);
    this.handleAddEstimatedTime = this.handleAddEstimatedTime.bind(this);
  }
  componentDidMount() {
    const currentTask = this.props.currentTask;
    const { taskPer, taskId } = this.props;
    if (currentTask) {
      const estimatedTimeDisabledStatus =
        currentTask["estimatedHrs"] > 0 || currentTask["estimatedMins"] > 0 ? true : false;
      this.setState({
        currentTask,
        estimatedTimeDisabledStatus,
        permissionAccess: taskPer.taskDetail.taskEstimation.isAllowAdd,
      });
    }

    let createdDate = this.props.createdDate;
    let currentDate = this.props.currentDate;
    if (createdDate) this.setTimerValues(createdDate, currentDate);
    else
      this.props.getTimerValues(
        taskId,
        response => {
          createdDate = response.data.createdDate;
          currentDate = response.data.currentDate;
          this.setTimerValues(createdDate, currentDate);
        },
        error => { }
      );
    this.setState({
      estHours: currentTask.estimatedHrs,
      estMins: currentTask.estimatedMins,
    });
  }

  setTimerValues = (createdDate, currentDate) => {
    // createdDate: "2019-07-26T14:16:39.864Z"
    // currentDate: "2019-07-29T11:36:56.5695377Z"
    const { miliSecToMinus = 0 } = this.props;
    if (createdDate) {
      const timeStamp = createdDate;
      let time = moment(timeStamp, "YYYY-MM-DDThh:mm:ss");
      const date2 = moment(currentDate, "YYYY-MM-DDThh:mm:ss");
      const diff = date2.diff(time);
      time = time.format("x");
      // diff = 0, means timer is started now
      if (diff == 0) {
        this.setState({
          isOn: true,
          time: this.state.time,
          start: Date.now() - this.state.time,
        });
        this.timer = setInterval(
          () =>
            this.setState({
              time: Date.now() - this.state.start,
            }),
          1000
        );
      } else {
        this.setState({
          isOn: true,
          time: diff,
          start: diff === 0 ? Date.now() : time,
          currentTime: date2,
        });
        this.timer = setInterval(() => {
          let mtime = this.state.currentTime;
          this.setState({
            currentTime: this.state.currentTime.clone().add(1, "seconds"),
            time: this.state.currentTime.format("x") - this.state.start,
          });
        }, 1000);
      }
    }
  };
  componentWillUnmount() {
    clearInterval(this.timer)
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(this.props.tasksState.data) !== JSON.stringify(prevProps.tasksState.data)) {
      const currentTask = getTaskById(this.props.taskId);
      const prevPropsCurrentTask = getTaskFromData(prevProps.tasksState.data, this.props.taskId);
      if (currentTask) {
        const data1 = {
          timesheetStatus: currentTask["timesheetStatus"],
          estimatedHrs: currentTask["estimatedHrs"],
          estimatedMins: currentTask["estimatedMins"],
          userTaskEffort: currentTask.userTaskEffort || [],
        };
        const data2 = {
          timesheetStatus: prevPropsCurrentTask ? prevPropsCurrentTask["timesheetStatus"] : null,
          estimatedHrs: prevPropsCurrentTask ? prevPropsCurrentTask["estimatedHrs"] : 0,
          estimatedMins: prevPropsCurrentTask ? prevPropsCurrentTask["estimatedMins"] : 0,
          userTaskEffort: prevPropsCurrentTask ? prevPropsCurrentTask.userTaskEffort || [] : [],
        };
        if (data1.userTaskEffort.length > data2.userTaskEffort.length) {
          this.setState({ currentTask, sHours: "", sMins: "" });
        } else if (
          JSON.stringify(currentTask) !== JSON.stringify(prevPropsCurrentTask) ||
          data1.timesheetStatus !== data2.timesheetStatus ||
          data1.userTaskEffort.length !== data2.userTaskEffort.length
        ) {
          this.setState({ currentTask });
        }
        if (
          data1.estimatedHrs !== data2.estimatedHrs ||
          data1.estimatedMins !== data2.estimatedMins
        ) {
          if (data1.estimatedHrs === 0 && data1.estimatedMins === 0)
            this.setState({
              currentTask,
              addEstimatedTime: true,
              estimatedTimeDisabledStatus: false,
              estHours: "",
              estMins: "",
            });
          else
            this.setState({
              currentTask,
              addEstimatedTime: false,
              estimatedTimeDisabledStatus: true,
            });
        }
      }
    }
  }

  openTaskDetails = () => { };
  enableEstimatedTimeEditableState = () => {
    this.setState({
      estimatedTimeDisabledStatus: false,
      addEstimatedTime: true,
    });
  };
  //Function that deletes estimated time entry
  deleteEstimatedTime = () => {
    this.setState(
      {
        estHours: 0,
        estMins: 0,
        estimatedTimeDisabledStatus: false,
        addEstimatedTime: true,
      },
      () => {
        this.handleUpdateTask(true); // sending true in params means it's estimated time to be updated
      }
    );
  };
  editEstimatedTime = () => {
    const { taskPer } = this.props;
    this.setState({
      estimatedTimeDisabledStatus: false,
      addEstimatedTime: true,
      permissionAccess: taskPer.taskDetail.taskEstimation.isAllowEdit,
    });
  };
  handleBtnDropdownOpen(event) {
    this.setState({ btnDropdownOpen: true });
  }
  handleBtnDropdownClose(event) {
    this.setState({ btnDropdownOpen: false });
  }
  handleClick(event) {
    event.stopPropagation();
    if (this.props.isGlobal) this.props.openTimerTaskDetailsPopUp();
    else this.setState({ open: true });
  }
  handleClose(event) {
    this.setState({ open: false });
  }
  handleSubMenuClick = (event, name) => {
    event.stopPropagation();
    this.setState(state => ({ [name]: !state[name] }));
  };
  startTimer = () => {
    this.props.startTime(`${0}:${0}:${0}`, () => {
      this.setState({
        isOn: true,
        time: this.state.time,
        start: Date.now() - this.state.time,
        btnDropdownOpen: false,
      });
      this.timer = setInterval(
        () =>
          this.setState({
            time: Date.now() - this.state.start,
          }),
        1
      );
    });
  };
  stopTimer = timer => {
    const { timeFallShort, successMessage } = message;
    const { time } = this.state;
    const { showSnackBar, stopTime } = this.props;
    stopTime(timer);
    this.setState({ isOn: false, time: 0 }, () => {
      clearInterval(this.timer);
    });
    // time > 30000
    //   ? showSnackBar(successMessage, "success")
    // : showSnackBar(timeFallShort, "error"); /** 30000 = 30 seconds */
  };
  pauseTimer() {
    this.setState({ isOn: false, btnDropdownOpen: true });
    clearInterval(this.timer);
  }
  resetTimer() {
    this.setState({ time: 0, isOn: false });
  }
  estimatedTimeClick() {
    this.setState({ addEstimatedTime: true });
  }
  handleAddEstimatedTime() {
    this.setState({ addEstimatedTime: false });
  }
  handleUpdateTask = isEstimatedTime => {
    const { updateTask } = this.props;
    const { sHours, sMins, estHours, estMins } = this.state;
    const updatedTask = cloneDeep(getTaskById(this.props.taskId));
    if (isEstimatedTime) {
      updatedTask.estimatedHrs = estHours * 1;
      updatedTask.estimatedMins = estMins * 1;
      this.setState({
        addEstimatedTime: false,
        estimatedTimeDisabledStatus: true,
      });
      // updateTask(updatedTask);
      updateTask({ estimatedHrs: estHours * 1, estimatedMins: estMins * 1 })
    }
    if (!isEstimatedTime) {
      updatedTask.effortHrs = sHours * 1;
      updatedTask.effortMins = sMins * 1;
      this.setState({ manualTime: false });
      // updateTask(updatedTask);
      updateTask({ effortHrs: sHours * 1, effortMins: sMins * 1 })
    }
  };
  clearEffort = (key, value) => {
    this.setState({ [key]: value });
  };

  updateEffortTime = (hrs, mins) => {
    let task = cloneDeep(this.state.currentTask);
    task.effortHrs = hrs;
    task.effortMins = mins;
    this.setState({ currentTask: task, sHours: hrs, sMins: mins });
  };
  updateEstimatedTime = (hrs, mins) => {
    let task = cloneDeep(this.state.currentTask);
    task.estimatedHrs = hrs;
    task.estimatedMins = mins;
    this.setState({ currentTask: task, estHours: hrs, estMins: mins });
  };
  getTotalTime = tasks => {
    let durations = [];
    let intl = this.props.intl;
    tasks.map(task => {
      durations.push(`${task["hours"]}:${task["minutes"]}`);
    });
    const total = calTotalTime(durations).split(":");
    const hr =
      total[0] > 0
        ? `${total[0]} ${intl.formatMessage({
          id: "project.dev.fianancial-summary.hrs-min.hrs",
          defaultMessage: "hrs",
        })} `
        : "";
    const min =
      total[1] > 0
        ? `${total[1]} ${intl.formatMessage({
          id: "project.dev.fianancial-summary.hrs-min.min",
          defaultMessage: "mins",
        })}`
        : "";
    return `${hr} ${min}`;
  };
  getUsers = tasks => {
    const userTasks = uniqBy(tasks, "userId");
    let users = [];
    userTasks.map(userTask => {
      users.push({
        userId: userTask["userId"],
        taskEfforts: filter(tasks, { userId: userTask["userId"] }),
      });
    });
    return users;
  };
  handleTabChange = (event, newAlignment) => {
    if (newAlignment) {
      this.setState({ alignment: newAlignment });
    }
  };
  //Function triggers when user click on add manual time entry
  handleAddManualTime = () => {
    this.setState(prevState => ({ manualTime: !prevState.manualTime }));
  };
  render() {
    const {
      classes,
      theme,
      style,
      allMembers,
      handleRemoveUserTaskEffort,
      isArchivedSelected,
      isGlobal,
      taskPer,
      customBtnProps = {},
      timeBtnProps = {},
      loggedInUserId,
      globalTimeIncrement = {},
    } = this.props;
    const {
      open,
      time,
      btnDropdownOpen,
      isOn,
      subMenuOpen,
      addEstimatedTime,
      estimatedTimeDisabledStatus,
      sHours,
      sMins,
      estHours,
      estMins,
      alignment,
      manualTime,
      permissionAccess,
    } = this.state;
    const { currentTask = {} } = this.state;

    let userTaskEfforts = currentTask["userTaskEffort"] || [];
    const projectStatus = currentTask["timesheetStatus"];
    const allTotalTime = this.getTotalTime(userTaskEfforts);
    const users = this.getUsers(userTaskEfforts);
    const estiEffortUser = allMembers.find(m => {
      return m.userId == currentTask.estimatedEffortAddedBy;
    });

    const { days, totalHours, hours, mins, seconds } = getDurationParts(time);
    // format = hh:mm:ss 68:22:15
    let timer = `${totalHours}:${mins}:${seconds}`;

    let ddButton = isOn ? (
      <Button
        // buttonRef={node => {
        //   this.buttonsEl = node;
        // }}
        onClick={() => this.stopTimer(`${days}:${hours}:${mins}:${seconds}`)}
        className={classes.mainButton}
        {...customBtnProps}>
        <StopIcon className={classes.stopIcon} />
        {/* <StopIcon className={classes.stopIcon} /> */}
      </Button>
    ) : !isOn && time !== 0 ? null : (
      //Code for later use
      // <Button
      //   className={classes.mainButton}
      //   onClick={this.handleBtnDropdownOpen}
      //   buttonRef={node => {
      //     this.buttonsEl = node;
      //   }}
      // >
      //   <PauseIcon className={classes.pauseIcon} />
      // </Button>
      <Button
        onClick={this.startTimer}
        className={classes.mainButton}
        disabled={
          projectStatus === "Approved" || isArchivedSelected
            ? true
            : false || !taskPer.taskDetail.taskEfforts.isAllowAdd
        }
        {...customBtnProps}>
        <PlayIcon className={classes.playIcon} />
      </Button>
    );

    let disabledMannualTime =
      isEmpty(sHours) &&
      isEmpty(sMins); /** if hours and mins are empty then save button should also be disabled */
    let disabledEstimatedTime =
      isEmpty(estMins) &&
      isEmpty(estHours); /** if hours and mins are empty then save button should also be disabled */

    return (
      <div style={style}>
        {isGlobal ? (
          <div className={classes.headerTimerBtnCnt}>
            <Button
              disabled={isArchivedSelected}
              onClick={event => {
                this.handleClick(event);
              }}
              className={classes.timerHeaderButton}
              style={{ borderRadius: 30 }}
              {...timeBtnProps}>
              <TimerIcon
                htmlColor={theme.palette.primary.light}
                className={classes.timerBtnIcon}
              />
              <span style={{ width: 60, textAlign: "left" }}>
                {isOn || time !== 0 ? timer : "00:00"}
              </span>
            </Button>
          </div>
        ) : (
          <>
            {ddButton}
            <Button
              disabled={isArchivedSelected}
              onClick={event => {
                this.handleClick(event);
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}
              className={classes.timerButton}
              {...timeBtnProps}>
              <span style={{ width: 60, textAlign: "left" }}>
                {isOn || time !== 0 ? timer : "00:00"}
              </span>
              <DropdownArrow
                htmlColor={theme.palette.secondary.light}
                className={classes.timerBtnArrow}
              />
            </Button>
          </>
        )}

        {/* Do not remove code below it's for later use */}
        {/* <DropdownMenu />
          open={btnDropdownOpen}
          closeAction={this.handleBtnDropdownClose}
          anchorEl={this.buttonsEl}
          size="small"
          style={{ width: 30, zIndex: 11111 }}
        >
          <Button
            onClick={() => this.startTimer(duration)}
            className={classes.dropdownButtons}
          >
            <PlayIcon className={classes.playIcon} />
          </Button>

          <Button
            onClick={() => this.stopTimer(timer)}
            className={classes.dropdownButtons}
          >
            <StopIcon className={classes.stopIcon} />
          </Button> */}

        {/* <Button onClick={this.resetTimer} className={classes.dropdownButtons}>
            <ResetIcon className={classes.resetIcon} />
          </Button> */}
        {/* </DropdownMenu> */}

        <DropdownMenu
          open={open}
          closeAction={this.handleClose}
          placement="bottom-end"
          anchorEl={this.anchorEl}
          size="large"
          style={{ zIndex: 11111 }}>
          <div>
            <div className={classes.header}>
              <div>
                <Typography variant="caption" className={classes.timerHeading}>
                  <FormattedMessage
                    id="task.detail-dialog.time-logged.title"
                    defaultMessage="TOTAL TIME"
                  />
                </Typography>
                <div className="flex_center_space_between_row">
                  <Typography variant="h1">{allTotalTime}</Typography>
                </div>
              </div>

              {taskPer.taskDetail.taskEfforts.isAllowAdd && (
                <CustomIconButton
                  btnType="sucessSubmit"
                  onClick={this.handleAddManualTime}
                  style={{
                    transition: "0.2s ease transform",
                    background: manualTime ? theme.palette.error.main : "",
                    transform: manualTime ? "rotate(45deg)" : null,
                  }}>
                  <AddIcon className={classes.checkIcon} />
                </CustomIconButton>
              )}
            </div>
            <Divider />
            {manualTime ? (
              <div className={classes.manualDurationInput}>
                <Typography variant="caption">
                  <FormattedMessage
                    id="task.detail-dialog.time-logged.manual-time.label"
                    defaultMessage="Manual Time Entry"
                  />
                  {
                    <CustomTooltip
                      helptext={
                        <FormattedMessage
                          id="task.detail-dialog.time-logged.manual-time.tooltip"
                          defaultMessage={taskDetailsHelpText.manualTimeEntryHelpText}
                        />
                      }
                      iconType="help"
                      position="static"
                    />
                  }
                </Typography>

                <div className={classes.manualTimeEntryCnt}>
                  <DurationInput
                    clearSelect={this.clearEffort}
                    disabledInput={projectStatus === "Approved" ? true : false}
                    updateTime={this.updateEffortTime}
                    hours={sHours}
                    mins={sMins}
                    styles={{ flex: 1, margin: 0 }}
                    disableError={true}
                    customMins={globalTimeIncrement?.isCustomTime ? [0, 30] : []}
                  />

                  <CustomButton
                    btnType="success"
                    variant="contained"
                    style={{ marginLeft: 10 }}
                    onClick={() => this.handleUpdateTask(false)}
                    disabled={projectStatus === "Approved" ? true : false}>
                    <FormattedMessage
                      id="profile-settings-dialog.apps-integrations.common.save-button.label"
                      defaultMessage="Save"
                    />
                  </CustomButton>
                </div>
              </div>
            ) : null}
            <div className={classes.toggleContainer}>
              <ToggleButtonGroup
                size="small"
                exclusive
                onChange={this.handleTabChange}
                value={alignment}
                classes={{ root: classes.toggleBtnGroup }}>
                <ToggleButton
                  key={1}
                  value="left"
                  classes={{
                    root: classes.toggleButton,
                    selected: classes.toggleButtonSelected,
                  }}>
                  <FormattedMessage
                    id="task.detail-dialog.time-logged.all-entries.label"
                    defaultMessage="All Entries"
                  />
                </ToggleButton>

                <ToggleButton
                  key={2}
                  value="right"
                  classes={{
                    root: classes.toggleButton,
                    selected: classes.toggleButtonSelected,
                  }}>
                  <FormattedMessage
                    id="project.dev.tasks.headers.estimated-time"
                    defaultMessage="Estimated Time"
                  />
                </ToggleButton>
              </ToggleButtonGroup>
            </div>
            {alignment == "left" ? (
              <>
                <Scrollbars autoHide autoHeight autoHeightMin={0} autoHeightMax={150}>
                  {userTaskEfforts.length ? (
                    <List component="nav" disablePadding className={classes.userTimeEntryList}>
                      {users.map((user, index) => {
                        const { taskEfforts } = user;
                        const totalTime = this.getTotalTime(taskEfforts);
                        let localUser = find(allMembers, {
                          userId: user.userId,
                        });
                        let name = "";
                        if (localUser)
                          name = generateUsername(
                            localUser.email,
                            localUser.userName,
                            localUser.fullName
                          );
                        else {
                          localUser = { imageUrl: `` };
                        }
                        return (
                          <React.Fragment>
                            <ListItem
                              onClick={event => this.handleSubMenuClick(event, index)}
                              className={classes.userTimeEntryListItem}
                              key={localUser.email}>
                              <div
                                className={`${classes.userTimeEntryItemInner} flex_center_space_between_row`}
                                style={this.state[index] ? { border: "none" } : null}>
                                <div className="flex_center_start_row">
                                  <CustomAvatar
                                    otherMember={{
                                      imageUrl: localUser.imageUrl,
                                      fullName: localUser.fullName,
                                      lastName: "",
                                      email: localUser.email,
                                      isOnline: localUser.isOnline,
                                      isOwner: localUser.isOwner,
                                    }}
                                    size="xsmall"
                                  />
                                  <div
                                    className={`flex_center_start_col ${classes.singleUserTotalTimeCnt}`}>
                                    <p className={classes.singleUserTotalTime}>{totalTime}</p>
                                    <Typography variant="caption" className={classes.userFullName}>
                                      {name}
                                    </Typography>
                                  </div>
                                </div>
                                {!isGlobal ? (
                                  this.state[index] || false ? (
                                    <UpArrow htmlColor={theme.palette.secondary.light} />
                                  ) : (
                                    <DownArrow htmlColor={theme.palette.secondary.light} />
                                  )
                                ) : null}
                              </div>
                            </ListItem>
                            <Collapse in={this.state[index] || false} timeout="auto" unmountOnExit>
                              <List component="div" disablePadding className={classes.subMenuList}>
                                {taskEfforts.map((task, i) => {
                                  const date = moment(task["createdDate"]).format("MMM D, YYYY");
                                  return (
                                    <ListItem className={classes.singleTimeEntrySubMenu} key={i}>
                                      <Typography
                                        variant="body2"
                                        className={classes.singleEntryTime}>
                                        {`${task["hours"] > 0
                                          ? task["hours"] +
                                          ` ${this.props.intl.formatMessage({
                                            id: "task.detail-dialog.time-logged.hr-min.hr",
                                            defaultMessage: "hr",
                                          })} `
                                          : ""
                                          }
                                  ${task["minutes"] > 0
                                            ? task["minutes"] +
                                            ` ${this.props.intl.formatMessage({
                                              id: "task.detail-dialog.time-logged.hr-min.min",
                                              defaultMessage: "min",
                                            })}`
                                            : ""
                                          }`}
                                      </Typography>
                                      <div className="flex_center_start_row">
                                        <Typography
                                          variant="caption"
                                          className={classes.singleTimeEntryDate}>
                                          {date}
                                        </Typography>
                                        {projectStatus !== "Approved" &&
                                          taskPer.taskDetail.taskEfforts.isAllowDelete && localUser.userId === loggedInUserId ? (
                                          <IconButton
                                            btnType="smallFilledGray"
                                            style={{
                                              padding: 2,
                                              background: theme.palette.error.pinkRed,
                                            }}
                                            onClick={() => handleRemoveUserTaskEffort(task["id"])}>
                                            <CloseIcon
                                              className={classes.closeIcon}
                                              htmlColor={theme.palette.common.white}
                                            />
                                          </IconButton>
                                        ) : null}
                                      </div>
                                    </ListItem>
                                  );
                                })}
                              </List>
                            </Collapse>
                          </React.Fragment>
                        );
                      })}
                    </List>
                  ) : (
                    <div className={classes.noEffortCnt}>
                      <TimeLog
                        htmlColor={theme.palette.background.contrast}
                        className={classes.timeLogIcon}
                      />
                      <p className={classes.noEffortText}>
                        <FormattedMessage
                          id="timesheet.no-time.label"
                          defaultMessage="No Time Logged Yet"
                        />
                      </p>
                    </div>
                  )}
                </Scrollbars>
              </>
            ) : alignment == "right" ? (
              <div className={classes.estimatedDurationInput}>
                <Typography variant="caption">
                  <FormattedMessage
                    id="project.dev.tasks.headers.estimated-time"
                    defaultMessage="Estimated Time"
                  />
                </Typography>
                {addEstimatedTime && !estimatedTimeDisabledStatus ? (
                  permissionAccess ? (
                    <div className={classes.manualTimeEntryCnt}>
                      <DurationInput
                        clearSelect={this.clearEffort}
                        disabledInput={estimatedTimeDisabledStatus}
                        updateTime={this.updateEstimatedTime}
                        hours={estHours}
                        mins={estMins}
                        styles={{ flex: 1, margin: 0 }}
                        customMins={globalTimeIncrement?.isCustomTime ? [0, 30] : []}
                      />
                      <CustomButton
                        btnType="success"
                        variant="contained"
                        style={{ marginLeft: 10 }}
                        onClick={() => this.handleUpdateTask(true)}
                        disabled={estimatedTimeDisabledStatus || disabledEstimatedTime}>
                        <FormattedMessage
                          id="profile-settings-dialog.apps-integrations.common.save-button.label"
                          defaultMessage="Save"
                        />
                      </CustomButton>
                    </div>
                  ) : null
                ) : estimatedTimeDisabledStatus && estiEffortUser ? (
                  <div className={classes.singleTimeEntrySubMenu}>
                    <ListItem className={classes.userEstiTimeEntryListItem}>
                      <div
                        className={`${classes.userTimeEntryItemInner} flex_center_space_between_row`}>
                        <div className="flex_center_start_row">
                          <CustomAvatar
                            otherMember={{
                              imageUrl: estiEffortUser.imageUrl,
                              fullName: estiEffortUser.fullName,
                              lastName: "",
                              email: estiEffortUser.email,
                              isOnline: estiEffortUser.isOnline,
                              isOwner: estiEffortUser.isOwner,
                            }}
                            size="xsmall"
                          />
                          <div
                            className={`flex_center_start_col ${classes.singleUserTotalTimeCnt}`}>
                            <p className={classes.singleUserTotalTime}>
                              <Typography variant="body2" className={classes.singleEntryTime}>
                                {`${currentTask["estimatedHrs"] > 0
                                  ? currentTask["estimatedHrs"] + " hr "
                                  : ""
                                  }
                        ${currentTask["estimatedMins"] > 0
                                    ? currentTask["estimatedMins"] + " min"
                                    : ""
                                  }`}
                              </Typography>
                            </p>
                            <Typography variant="caption" className={classes.userFullName}>
                              {estiEffortUser.fullName}
                            </Typography>
                          </div>
                        </div>
                      </div>
                    </ListItem>

                    <div className="flex_center_start_row">
                      <Typography variant="caption" className={classes.singleTimeEntryDate} />
                      {taskPer.taskDetail.taskEstimation.isAllowEdit && (
                        <IconButton
                          btnType="smallFilledGray"
                          style={{
                            padding: 2,
                            background: "transparent",
                            marginRight: 5,
                          }}
                          onClick={this.editEstimatedTime}>
                          <EditIcon
                            className={classes.editIcon}
                            htmlColor={theme.palette.secondary.main}
                          />
                        </IconButton>
                      )}
                      {taskPer.taskDetail.taskEstimation.isAllowDelete && (
                        <IconButton
                          btnType="smallFilledGray"
                          style={{
                            padding: 2,
                            background: theme.palette.error.pinkRed,
                          }}
                          onClick={this.deleteEstimatedTime}>
                          <CloseIcon
                            className={classes.closeIcon}
                            htmlColor={theme.palette.common.white}
                          />
                        </IconButton>
                      )}
                    </div>
                  </div>
                ) : null}
              </div>
            ) : null}
          </div>
        </DropdownMenu>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tasksState: state.tasks,
    permissions: state.workspacePermissions.data.task,
    loggedInUserId: state.profile.data.userId || "",

  };
};

export default compose(
  injectIntl,
  withStyles(timerStyles, { withTheme: true }),
  connect(mapStateToProps, {
    getTimerValues,
  })
)(Timer);
