const customTabStyles = theme => ({
  root: {
    minHeight: "unset",
  },
  labelContainer: {
    fontSize: "13px !important",
    position: "relative",
    zIndex: 1,
  },
});

export default customTabStyles;
