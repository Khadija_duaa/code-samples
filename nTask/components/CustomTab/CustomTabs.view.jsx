import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import styles from "./CustomTabs.style";

function CustomTabs(props) {
  const { children, classes, ...rest } = props;

  return (
    <Tabs {...rest} classes={{ indicator: classes.indicator, root: classes.root }}>
      {children}
    </Tabs>
  );
}
export default withStyles(styles, { withTheme: true })(CustomTabs);
