const customTabsStyles = theme => ({
  root: {
    background: theme.palette.background.grayLighter,
    padding: "0 4px",
    borderRadius: 6,
    minHeight: "unset",
  },

  indicator: {
    background: theme.palette.background.paper,
    height: "unset",
    bottom: 4,
    top: 4,
    borderRadius: 4,
    boxShadow: "0px 1px 2px #00000029",
  },
});

export default customTabsStyles;
