import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Tab from "@material-ui/core/Tab";
import styles from "./CustomTab.style";

function CustomTab(props) {
  const { children, classes, ...rest } = props;

  return (
    <Tab classes={{ root: classes.root, wrapper: classes.labelContainer }} {...rest}>
      {children}
    </Tab>
  );
}

export default withStyles(styles, { withTheme: true })(CustomTab);
