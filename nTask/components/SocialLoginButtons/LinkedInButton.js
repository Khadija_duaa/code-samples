import React from 'react';

import { LINKEDIN_ICON } from '../../utils/constants/icons';

class LinkedInButton extends React.PureComponent {

    onLinkedInLoginClick = (config, events) => {
        
        const { onButtonClick, onSuccess, onFailure, getSocialLoginInfo } = events;
        const { provider } = config;
        onButtonClick();
        getSocialLoginInfo(provider).then(function (result) {
            
            onSuccess('LinkedIn', result.credential.accessToken);
        }).catch(function (error) {
            onFailure('LinkedIn', error.message);
        });
    }

    render() {
        const { classes, events, config } = this.props;
        return (
            <li className={classes.signInOption} style={{marginRight: 0}} onClick={() => this.onLinkedInLoginClick(config, events)}>
                <img className={classes.linkedIn} src={LINKEDIN_ICON} />
            </li >
        );
    }
}

export default LinkedInButton;
