import React from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import FacebookLoginButton from "./FacebookButton";
import GoogleLoginButton from "./GoogleButton";
import LinkedInButton from "./LinkedInButton";
import TwitterLoginButton from "./TwitterButton";
import {
  facebookProvider,
  googleProvider,
  twitterProvider,
  getSocialLoginInfo,
} from "../../utils/socialLogin";
import { handleSocialLogin } from "../../redux/actions/socialAuthentication";
import { withStyles } from "@material-ui/core/styles";
import socialLoginButtonStyles from "./styles";
import find from "lodash/find";
import queryString from "query-string";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../mixpanel';

class SocialLoginButtons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSocialBtnClicked: false,
    };
  }
  handleLogin = (provider, token, tokenSecret) => {
    const { history, closeAction, location } = this.props;
    this.props.handleSocialLogin(
      provider,
      token,
      tokenSecret,
      (response) => {
        if (response.status === 200) {
          if (response.data.isRegistered === true) {
            const parsedMicrosftTeamUrl = queryString.parse(history.location.search);
            const {microsoftteams,redirect_uri} = parsedMicrosftTeamUrl;
            const access_token = response.data.data.access_token;
            if(microsoftteams){
              window.location = decodeURIComponent(redirect_uri)+"?isAuthSuccess=true&access_token="+access_token+"&userName="+response.data.data.email;
              return;
            }
            localStorage.setItem("token", `Bearer ${access_token}`);
            localStorage.setItem("userName", response.data.data.email);
            localStorage.setItem("zapkey", response.data.data.zapKey);
            const {email, roles, userName} = response.data.data;
            mixpanel.track(MixPanelEvents.LoginEvent, { email, roles, userName });
            const parsedUrl = queryString.parseUrl(location.search).query;
            if (parsedUrl.redirect_uri) {
              history.push(`/zapierAuth?${queryString.stringify(parsedUrl)}`);
            } else if (parsedUrl.redirectUrl) {
              history.push(`/${parsedUrl.redirectUrl}`);
            } else {
              this.props.history.push("/tasks");
            }

            if (closeAction) closeAction();
          } else if (response.data.isRegistered === false) {
            // const url = response.data.url.replace(/^.*\/account\//g, '');
            const url = `/welcome/${response.data.url}`;
            localStorage.setItem("tempToken", response.data.data.access_token);
            history.push(url);
          }
        }
      },
      (error) => {
        if (error) {
          if(!error.data){
            this.showSnackBar("Oops! There seems to be an issue, please contact support@ntaskmanager.com", "info"
            );
            this.props.history.replace('/account/login')
          }
          if (error.status === 500) {
            this.showSnackBar("Server throws error", "error");
          }
          // ACCOUNT ALREADY EXISTS
          else if (error.status === 302) {
            this.props.duplicateAccount();
          } else {
            this.showSnackBar(
              error.data ? error.data.message : error.response.data,
              "info"
            );
          }
        }
      }
    );
  };
  onLoginSuccess = (provider, token, tokenSecret) => {
    //

    this.setState({ isSocialBtnClicked: false });
    this.handleLogin(provider, token, tokenSecret);
  };
  onLoginFailure = (provider, error) => {
    this.setState({ isSocialBtnClicked: false });
  };
  onClick = () => {
    this.setState({ isSocialBtnClicked: true });
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  getUserAssosiatedLoginInstances = () => {
    const { closeAction } = this.props;
    const performConditionalRendering = closeAction;

    const profile = this.props.profileState.data;
    const isGoogleAssosiated = find(profile.externalLogins || [], {
      login: { loginProvider: "Google" },
    });
    const isFacebookAssosiated = find(profile.externalLogins || [], {
      login: { loginProvider: "Facebook" },
    });
    const isTwitterAssosiated = find(profile.externalLogins || [], {
      login: { loginProvider: "Twitter" },
    });
    const isLinkedInAssosiated = find(profile.externalLogins || [], {
      login: { loginProvider: "LinkedIn" },
    });
    let obj = {
      isGoogleAssosiated: !performConditionalRendering || isGoogleAssosiated,
      isFacebookAssosiated:
        !performConditionalRendering || isFacebookAssosiated,
      isTwitterAssosiated: !performConditionalRendering || isTwitterAssosiated,
      isLinkedInAssosiated:
        !performConditionalRendering || isLinkedInAssosiated,
    };
    return obj;
  };
  render() {
    const { classes } = this.props;
    const buttons = this.getUserAssosiatedLoginInstances();

    return (
      <ul className={classes.signInOptionCnt}>
        {buttons.isGoogleAssosiated ? (
          <GoogleLoginButton
            classes={classes}
            events={{
              onButtonClick: this.onClick,
              onSuccess: this.onLoginSuccess,
              onFailure: this.onLoginFailure,
              getSocialLoginInfo,
            }}
            config={{ provider: googleProvider }}
          />
        ) : null}
        {buttons.isFacebookAssosiated ? (
          <FacebookLoginButton
            classes={classes}
            events={{
              onButtonClick: this.onClick,
              onSuccess: this.onLoginSuccess,
              onFailure: this.onLoginFailure,
              getSocialLoginInfo,
            }}
            config={{ provider: facebookProvider }}
          />
        ) : null}
        {/*{buttons.isTwitterAssosiated ? (*/}
        {/*  <TwitterLoginButton*/}
        {/*    classes={classes}*/}
        {/*    events={{*/}
        {/*      onButtonClick: this.onClick,*/}
        {/*      onSuccess: this.onLoginSuccess,*/}
        {/*      onFailure: this.onLoginFailure,*/}
        {/*      getSocialLoginInfo,*/}
        {/*    }}*/}
        {/*    config={{ provider: twitterProvider }}*/}
        {/*  />*/}
        {/*) : null}*/}
        {/* {
                    buttons.isLinkedInAssosiated ?
                        <LinkedInButton
                            classes={classes}
                            events={{ onButtonClick: this.onClick, onSuccess: this.onLoginSuccess, onFailure: this.onLoginFailure, getSocialLoginInfo }}
                            config={{ provider: 'LinkedIn' }}
                        />
                        : null
                } */}
      </ul>
    );
  }
}

SocialLoginButtons.defaultProps = {
  classes: {
    signInOption: "",
    facebookIcon: "",
    googleIcon: "",
    linkedIn: "",
  },
};

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(socialLoginButtonStyles, { withTheme: true }),
  connect(mapStateToProps, {
    handleSocialLogin,
  })
)(SocialLoginButtons);
