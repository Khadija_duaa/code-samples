import React from "react";

import { GOOGLE_ICON } from "../../utils/constants/icons";
import CustomButton from "../Buttons/CustomButton";
import GoogleIcon from "../Icons/GoogleIcon";

const onGoogleLoginClick = (config, events) => {
  const { onButtonClick, onSuccess, onFailure, getSocialLoginInfo } = events;
  const { provider } = config;
  onButtonClick();
  getSocialLoginInfo(provider)
    .then(function(result) {
      onSuccess("Google", result.credential.accessToken);
    })
    .catch(function(error) {
      onFailure("Google", error.message);
    });
};

const GoogleLoginButton = ({ classes, events, config }) => {
  return (
    <CustomButton  onClick={() => onGoogleLoginClick(config, events)} className={classes.signUpInnerBtn1}>  <GoogleIcon googleIconStyle={{ fontSize: "20px !important", marginRight: "12px" }} /> Google</CustomButton>
  );
};

export default GoogleLoginButton;
