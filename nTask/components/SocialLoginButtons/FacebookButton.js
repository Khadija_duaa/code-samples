import React from "react";

import { FACEBOOK_ICON } from "../../utils/constants/icons";
import CustomButton from "../Buttons/CustomButton";
import NewFacebookIcon from "../Icons/NewFacebookIcon";

const onFacebookLoginClick = (config, events) => {
  const { onButtonClick, onSuccess, onFailure, getSocialLoginInfo } = events;
  const { provider } = config;
  onButtonClick();
  getSocialLoginInfo(provider)
    .then(function(result) {
      onSuccess("Facebook", result.credential.accessToken);
    })
    .catch(function(error) {
      onFailure("Facebook", error.message);
    });
};

const FacebookLoginButton = ({ classes, events, config }) => {
  return (
    <CustomButton className={classes.signUpInnerBtn2} onClick={() => onFacebookLoginClick(config, events)}>
    <NewFacebookIcon facebookIconStyle={{ fontSize: "20px", marginRight: "12px" }} />
  Facebook
  </CustomButton>
  );
};

export default FacebookLoginButton;
