import React from "react";

import { TWITTER_ICON } from "../../utils/constants/icons";

const onTwitterLoginClick = (config, events) => {
  const { onButtonClick, onSuccess, onFailure, getSocialLoginInfo } = events;
  const { provider } = config;
  onButtonClick();

  getSocialLoginInfo(provider)
    .then(function(result) {
      const { accessToken, secret } = result.credential;
      onSuccess("Twitter", accessToken, secret);
    })
    .catch(function(error) {
      onFailure("Twitter", error.message);
    });
};

const TwitterLoginButton = ({ classes, events, config }) => {
  return (
    <li
      onClick={() => onTwitterLoginClick(config, events)}
      className={classes.signInOption}
    >
      <div className={classes.socialIconcontainer}>
        <img
          className={classes.facebookIcon}
          style={{ margin: "2px" }}
          src={TWITTER_ICON}
        />
        <span className={classes.socialTitle}> Twitter </span>
      </div>
    </li>
  );
};

export default TwitterLoginButton;
