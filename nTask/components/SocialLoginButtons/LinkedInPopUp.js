import React from 'react';
import QueryString from 'query-string';

import apiInstance from '../../redux/instance';
import { LINKEDIN_CLIENT_ID, LINKEDIN_CLIENT_SECRET } from '../../utils/constants/externalConfig';

class LinkedInPopUp extends React.Component {
  componentDidMount() {
    let closedIntentionally = false;
    window.onbeforeunload = function () {
      if (!closedIntentionally)
        window.opener && window.opener.postMessage({ error: 'popup_closed_by_user', errorMessage: 'popup_closed_by_user', from: 'Linked In' }, window.location.origin);
    }
    const params = QueryString.parse(window.location.search);
    if (params.error) {
      const errorMessage = params.error_description || 'Login failed. Please try again.';
      window.opener && window.opener.postMessage({ error: params.error, errorMessage, from: 'Linked In' }, window.location.origin);
      // Close tab if user cancelled login
      if (params.error === 'user_cancelled_login') {
        closedIntentionally = true;
        window.close();
      }
    }
    if (params.code) {

      apiInstance()
        .post(`api/account/GetLinkedinAccessToken`,
          {
            code: params.code,
            RedirectUrl: `${window.location.origin}/linkedin`,
            GrantType: `authorization_code`,
          })
        .then(response => {
          response = JSON.parse(response.data)
          if (response.error)
            throw error;
          window.opener && window.opener.postMessage({ access_token: response.access_token, from: 'Linked In' }, window.location.origin);
          closedIntentionally = true;
          window.close();
        })
        .catch(error => {
          window.opener && window.opener.postMessage({ error: 'error', errorMessage: JSON.stringify(error), from: 'Linked In' }, window.location.origin);
          closedIntentionally = true;
          window.close();
        });
    }
  }
  render() {
    return null;
  }
}

export default LinkedInPopUp;
