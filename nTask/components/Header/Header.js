import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import isEqual from "lodash/isEqual";
import { withSnackbar } from "notistack";
import React, { Component } from "react";
import { connect } from "react-redux";
import LoadingBar from "react-redux-loading-bar";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import HelpCenter from "../../components/HelpCenter/HelpCenter.view";
import helper from "../../helper";
import { taskDetailDialogState } from "../../redux/actions/allDialogs";
import { showHideNotificationBar } from "../../redux/actions/notificationBar";
import {
  FetchUserNotification,
  MarkAllNotificationsAsRead,
} from "../../redux/actions/notifications";
import { resendEmailVerification, UpdateLocalization } from "../../redux/actions/profile";
import { SaveLocalization, SaveProfile } from "../../redux/actions/profileSettings";
import { showHideSideBar, updateSidebarState } from "../../redux/actions/sidebarPannel";
import { showUpgradePlan, UpdatePromoCodeValue } from "../../redux/actions/userBillPlan";
import UpgradePlan from "../../Views/billing/UpgradePlan/UpgradePlan";
import ProfileSetting from "../../Views/ProfileSetting/ProfileSetting";
import ConstantsPlan from "../constants/planConstant";
import IconMenuClosed from "../Icons/IconMenuClosed";
import Search from "../Search/Search";
import Timer from "../Timer/Timer";
import CustomTooltip from "../Tooltip/Tooltip.js";
import NotificationBar from "../TopBar/NotificationBar";
import PromoBar from "../TopBar/PromoBar";
import AddNewMenu from "./addNewMenu";
import NavBar from "./NavBar";
import NotificationMenu from "./NotificationsMenu";
import HeaderProfileMenu from "./ProfileMenu";
import headerStyles from "./styles";
import TeamsDropdown from "./TeamsMenu";
import WorkspaceDropdown from "./WorkspaceMenu";

const moment = require("moment-timezone");

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      expanded: false,
      currentTimerTask: null,
      openTimerTaskDetails: false,
      readAll: true,
      openDialog: false,
      notificationBarBtnClicked: false,
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleAllRead = this.handleAllRead.bind(this);
  }
  getTaskPermission = (task) => {
    /** pushing the task permission object in the task , if task has linked with any project i-e projectId !== null then find the project and fetch the task permission in project object */
    const {
      projects,
      taskPer,
      workspaceStatus,
      workspaceLevelMeetingPer,
      workspaceLevelRiskPer,
      workspaceLevelIssuePer,
    } = this.props;
    if (task.projectId) {
      let attachedProject = projects.find((p) => p.projectId == task.projectId);
      if (attachedProject) {
        task.taskPermission = attachedProject.projectPermission.permission.task;
        task.meetingPermission = attachedProject.projectPermission.permission.meeting;
        task.riskPermission = attachedProject.projectPermission.permission.risk;
        task.issuePermission = attachedProject.projectPermission.permission.issue;
        // return task;
      } else {
        task.taskPermission = taskPer;
        task.meetingPermission = workspaceLevelMeetingPer;
        task.riskPermission = workspaceLevelRiskPer;
        task.issuePermission = workspaceLevelIssuePer;
        // return task;
      }
      if (attachedProject && attachedProject.projectTemplate) {
        task.taskStatus = attachedProject.projectTemplate;
      } else {
        task.taskStatus = workspaceStatus;
      }
      return task;
    } else {
      task.taskPermission = taskPer;
      task.taskStatus = workspaceStatus;
      task.meetingPermission = workspaceLevelMeetingPer;
      task.riskPermission = workspaceLevelRiskPer;
      task.issuePermission = workspaceLevelIssuePer;
      return task;
    }
  };

  componentDidUpdate(prevProps) {
    const { timerTask, teams, activeTeam, tasks } = this.props;
    if (
      (timerTask && timerTask.task && timerTask && !isEqual(timerTask, prevProps.timerTask)) ||
      (timerTask && timerTask.task && !isEqual(timerTask.task, this.state.currentTimerTask))
    ) {
      const currentTimerTaskObj = tasks.find((t) => {
        const timerTaskId = timerTask && timerTask.task && timerTask.task.taskId;
        return t.taskId == timerTaskId;
      });
      this.setState({
        currentTimerTask: timerTask && currentTimerTaskObj ? currentTimerTaskObj : null,
      });
    }
    if (activeTeam !== prevProps.activeTeam) {
      this.showBoardViewNotification();
    }
  }

  componentDidMount = () => {
    const { isOwner, teams, activeTeam } = this.props;
    const { subscriptionDetails } = teams.find((t) => t.companyId == activeTeam);
    const { paymentPlanTitle } = subscriptionDetails;
    // this.notifyUserTimeZone();
    this.showBoardViewNotification();
    this.showEmailVerificationBar();
    this.showDayLightSaving();
    this.props.FetchUserNotification(
      (success) => { },
      (fail) => { }
    );
  };

  showBoardViewNotification = () => {
    const { profileState, history, isOwner, teams, activeTeam } = this.props;
    const { subscriptionDetails } = teams.find((t) => t.companyId === activeTeam);
    const { paymentPlanTitle } = subscriptionDetails;
    const boardNotification = localStorage.getItem("cf2021");
    // const notificationObj = {
    //   // Notification object to be dispatched
    //   show: true,
    //   closeAction:() => {
    //     localStorage.setItem("boardView", 'seen');
    //   },
    //   buttonText: "Upgrade Plan",
    //   primaryAction: () => {
    //     const win = window.open(
    //       "https://www.ntaskmanager.com/blog/introducing-ntask-kanban-boards/",
    //       "_blank"
    //     );
    //     win.focus();
    //   },
    //   message: "Say Hello to nTask Boards: Better Organization and Smoother Collaboration",
    // };

    const notificationObj = {
      // Notification object to be dispatched
      show: true,
      // closeAction: () => {
      //   localStorage.setItem("sidebar2022", "seen");
      // },
      buttonText: localStorage.getItem("oldSidebarView") == "true" ? "Try Now" : "Old Navigation",
      primaryAction: () => {
        const oldSidebarVisible = localStorage.getItem("oldSidebarView") == "true" ? true : false;
        if (oldSidebarVisible) {
          this.props.showHideSideBar(false);
        } else {
          this.props.showHideSideBar(true);
        }
      },
      message: "Simplify your work management and get more done with our all new Navigation.",
    };
    // this.props.showHideNotificationBar(notificationObj);
    // if (
    //   !boardNotification &&
    //   isOwner &&
    //   (ConstantsPlan.isPlanFree(paymentPlanTitle) ||
    //     ConstantsPlan.isBusinessTrial(paymentPlanTitle))
    // ) {
    //   notificationObj.primaryAction = () => {
    //     const data = {
    //       show: true,
    //       mode: ConstantsPlan.BUSINESSPAYMENT,
    //     };
    //     this.props.showUpgradePlan(data);
    //   };
    //   this.props.showHideNotificationBar(notificationObj);
    // } else if (!boardNotification && ConstantsPlan.isPremiumTrial(paymentPlanTitle) && isOwner) {
    //   notificationObj.primaryAction = () => {
    //     const data = {
    //       show: true,
    //       mode: ConstantsPlan.PREMIUMPAYMENT,
    //     };
    //     this.props.showUpgradePlan(data);
    //   };
    //   this.props.showHideNotificationBar(notificationObj);
    // } else if (!boardNotification && ConstantsPlan.isPremiumPlan(paymentPlanTitle) && isOwner) {
    //   notificationObj.buttonText = "Book a Demo";
    //   notificationObj.primaryAction = () => {
    //     const win = window.open("https://calendly.com/ntask-support/democall", "_blank");
    //     win.focus();
    //   };
    //   this.props.showHideNotificationBar(notificationObj);
    // } else if (!boardNotification && ConstantsPlan.isBusinessPlan(paymentPlanTitle) && isOwner) {
    //   notificationObj.buttonText = "Book a Demo";
    //   notificationObj.primaryAction = () => {
    //     const win = window.open("https://calendly.com/ntask-support/democall", "_blank");
    //     win.focus();
    //   };
    //   notificationObj.secondaryButtonText = "Try Now";
    //   notificationObj.secondaryAction = () => {
    //     const win = window.open("https://app.ntaskmanager.com/risks", "_blank");
    //     win.focus();
    //   };
    //   this.props.showHideNotificationBar(notificationObj);
    // } else {
    //   this.props.showHideNotificationBar({ show: false });
    // }
  };

  showEmailVerificationBar = () => {
    const { profileState } = this.props;
    const notificationObj = {
      // Notification object to be dispatched
      show: true,
      buttonText: "Resend Email",
      primaryAction: () => {
        resendEmailVerification(() => {
          this.showSnackBar("Email sent successfully", "success");
        });
      },
      message: `Please confirm your email address: ${profileState.data.email}`,
    };
    if (!profileState.data.isEmailConfirmed) {
      this.props.showHideNotificationBar(notificationObj);
    }
  };

  showDayLightSaving = () => {
    const { profileState } = this.props;
    const notificationObj = {
      // Notification object to be dispatched
      show: true,
      buttonText: "Click Here",
      primaryAction: () => {
        this.props.SaveProfile(
          { ...profileState.data.profile, isDayLightSavingViewed: true },
          // Success
          () => {
            this.handleProfileSetting();
            this.props.showHideNotificationBar({ show: false });
          }
        );
      },
      closeAction: () => {
        this.props.SaveProfile(
          { ...profileState.data.profile, isDayLightSavingViewed: true },
          () => { }
        );
      },
      message:
        "Your Daylight Saving Time Adjustment setting is currently set to Automatic, to turn it off",
    };
    if (!profileState.data.profile.isDayLightSavingViewed) {
      this.props.showHideNotificationBar(notificationObj);
    }
  };

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };

  notifyUserTimeZone = () => {
    const { profileState, constants, SaveLocalization } = this.props;
    const { language, ButtonActionsCntDayOfWeek, timeZone } = profileState.data.profile;
    const userTimeZone = timeZone; // User set timezone in profile
    const userTimeZoneObj =
      constants.timeZones.find(
        // Finding object of user selected timezone to use it's values
        (t) => t.value == userTimeZone
      ) || {};
    const userSystemTimezone =
      moment.tz.guess() == "Europe/Riga" ? "Europe/Kiev" : moment.tz.guess(); // User system time zone in IANA format
    const isTimeZoneEqual = userSystemTimezone == userTimeZoneObj.olsonTimeZone; // Checking if the user set time zone and his system timezone is equal or not
    const userSystemTimeObj = constants.timeZones.find(
      // Finding object of user system timezone to use it's values
      (t) => t.olsonTimeZone == userSystemTimezone
    );
    const localObj = {
      language,
      startDayOfWeek,
      timeZone: userSystemTimeObj.value,
    };
    const notificationObj = {
      // Notification object to be dispatched
      show: true,
      buttonText: "Update",
      primaryAction: () => {
        SaveLocalization(
          localObj,
          // Success
          (response) => {
            // Hide notification Bar on timezone diff notification bar set success
            this.props.showHideNotificationBar({ show: false });
            this.props.UpdateLocalization(localObj);
          }
        );
      },
      message: `Timezone mismatch, different timezones in your profile and browser might lead to incorrect information being displayed. Change to ${userSystemTimeObj.value}`,
    };
    if (!isTimeZoneEqual) {
      // Code to be executed in case user selected time zone in profile does not match with user system timezone
      this.props.showHideNotificationBar(notificationObj);
    }
  };

  handleAllRead() {
    const currentUser = this.props.profileState.data ? this.props.profileState.data.userId : [];
    if (this.state.readAll) {
      this.setState({ readAll: false }, () => {
        this.props.MarkAllNotificationsAsRead(currentUser, (err, data) => {
          this.setState({ readAll: true });
        });
      });
    }
  }

  handleDialogCloseAction = () => {
    // open profile setting dialog
    this.setState({ openDialog: false });
  };

  handleProfileSetting = () => {
    // close profile setting dialog
    this.setState({ openDialog: true });
  };

  showHelpButton = () => {
    const { activeTeam, teams } = this.props;
    const activeTeamData = teams.find((t) => t.companyId === activeTeam);
    return [
      "/projects",
      "/boards",
      "/tasks",
      "/meetings",
      "/timesheet",
      "/issues",
      "/risks",
    ].includes(location.pathname) && !activeTeamData.isEnableWhiteLabeling;
  };

  handleClose(event) {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  }

  openTimerTaskDetailsPopUp = () => {
    this.setState({ openTimerTaskDetails: true }, () => {
      this.closeTimerTaskDetailsPopUp();
    });
  };

  closeTimerTaskDetailsPopUp = () => {
    this.setState({ openTimerTaskDetails: false });
  };

  hideUpgradeDialog = () => {
    const data = {
      show: false,
      mode: ConstantsPlan.UPGRADEPLANMODE,
    };
    this.props.showUpgradePlan(data);
  };

  render() {
    const { currentTimerTask, openTimerTaskDetails, openDialog, notificationBarBtnClicked } =
      this.state;
    const {
      classes,
      theme,
      hideLoadingState,
      showLoadingState,
      timerTask,
      profileState,
      location,
      tasks,
      planExpire,
      isOwner,
      promoBar,
      notification,
      notificationBar,
      visibleUpgradePlan,
      constants,
      taskPer,
      upgradePlanShow,
      upgradePlanMode,
      sidebarPannel,
      updateSidebarState,
      companyInfo,
    } = this.props;

    const type = "comment";
    const members = profileState.data ? profileState.data.member.allMembers : [];
    const currentUser = profileState.data.userId ? profileState.data.userId : "";
    let notifications = this.props.notificationsState || [];
    const notificationsData =
      notifications && notifications.data
        ? notifications.data.filter((x) => {
          return x.users.filter((y) => y.userId === currentUser && y.isViewed === false).length;
        })
        : [];
    notifications = notificationsData.map((x) => {
      x.createdName = members.find((m) => m.userId === x.createdBy) || { "fullName": x.userName, "email": x.userName };
      x.users = x.users.map((u) => ({
        userId: u.userId,
        isViewed: u.isViewed,
        //  userDetail: members.find(m => m.userId === u.userId)
      }));
      return x;
    });
    notifications = notifications.map((x) => ({
      id: x.notificationId,
      // title: x.createdName
      //   ? (x.createdName.fullName ||
      //       x.createdName.userName ||
      //       x.createdName.email) +
      //     " " +
      //     x.description
      //   : " ",
      title: x.description,
      date: helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(x.updatedDate || x.createdDate),
      time: helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate || x.createdDate),
      profilePic: x.createdName ? x.createdName.imageUrl : "",
      createdName: x.createdName,
      notificationUrl: x.notificationUrl,
      teamName: x.teamName || "N/A",
      workspaceName: x.workSpaceName || "N/A",
      teamId: x.teamId,
      workspaceId: x.workSpaceId,
    }));
    const workspaceCheck = () => {
      if(profileState.data.loggedInTeam){
        if (location.pathname.indexOf("/teams/") == 0) {
          return false;
        }
        if (location.pathname.indexOf("/team-settings") == 0) {
          return false;
        }
        if (location.pathname.indexOf("/notifications") == 0) {
          return false;
        }
        if (location.pathname.indexOf("/reports/") == 0) {
          return false;
        }
      } else {
        return false;
      }
      return true;
    };
    const currentTimerTaskObj = tasks.find((t) => {
      const timerTaskId = timerTask && timerTask.task && timerTask.task.taskId;
      return t.taskId == timerTaskId;
    });
    const newSidebarView = localStorage.getItem("oldSidebarView") == "true" ? false : true;
    const userCreatedDate = moment(profileState?.data?.createdDate);
    const isOldUser = userCreatedDate.isBefore("2022-03-03T11:26:29.219Z");
    const widthSidebar = sidebarPannel.width;
    const meetingAccess = companyInfo?.meeting?.isHidden ? false : true;
    const issueAccess = companyInfo?.issue?.isHidden ? false : true;
    const riskAccess = companyInfo?.risk?.isHidden ? false : true;
    const projectAccess = companyInfo?.project?.isHidden ? false : true;
    const taskAccess = companyInfo?.task?.isHidden ? false : true;

    const hideAddNewBtn =
      !meetingAccess && !issueAccess && !riskAccess && !projectAccess && !taskAccess;
    return (
      <>
        {visibleUpgradePlan /** modal opens from the side bar btn clicked */ && (
          <UpgradePlan
            mode={ConstantsPlan.UPGRADEPLANMODE}
            openPackages
            exitUpgrade={this.hideUpgradeDialog}
          />
        )}
        <PromoBar />
        {notification.show && <NotificationBar />}
        <LoadingBar
          updateTime={100}
          maxProgress={100}
          showFastActions
          style={{
            zIndex: 99999,
            position: "fixed",
            backgroundColor: "#00CC90",
          }}
        />
        <AppBar
          position="absolute"
          id="header"
          className={clsx({
            [classes.appBar]: true,
            [classes.appBarShift]: this.props.open,
            [classes.topBarAdjustment]: newSidebarView,
            [classes.sidebarClose]: !sidebarPannel.open,
          })}
          style={
            newSidebarView
              ? {
                marginLeft: `${widthSidebar}px `,
                width: `calc(100% - ${widthSidebar - 1}px) `,
                top: promoBar || notificationBar ? 50 : "",
                paddingLeft: hideAddNewBtn ? 20 : "",
              }
              : {
                top: promoBar || notificationBar ? 50 : "",
              }
          }>
          <Grid container direction="row" justify="space-between" alignItems="center">
            <Grid item classes={{ item: classes.headerLeftBtnsCnt }}>
              {!sidebarPannel.open && (
                <CustomTooltip
                  helptext={"Open Sidebar"}
                  iconType="help"
                  placement="top"
                  style={{ color: theme.palette.common.white }}>
                  <div
                    onClick={() => {
                      updateSidebarState(null, { open: true });
                      let sideBarState = {
                        ...JSON.parse(localStorage.getItem("sidebar")),
                        collape: true,
                      };
                      localStorage.setItem("sidebar", JSON.stringify(sideBarState));
                    }}
                    style={{
                      paddingLeft: 15,
                      cursor: "pointer",
                    }}>
                    <IconMenuClosed />
                  </div>
                </CustomTooltip>
              )}
              {workspaceCheck() && newSidebarView && (
                <>
                  <div style={{ marginLeft: "20px" }}>
                    <Search expanded={true} />
                  </div>
                </>
              )}
              {/* Hide workspace dropdown in header if user is on teams dashboard view */}
              {!newSidebarView && isOldUser && (
                <TeamsDropdown
                  showLoadingState={showLoadingState}
                  hideLoadingState={hideLoadingState}
                  planExpire={planExpire}
                />
              )}

              {profileState.data.loggedInTeam &&
                workspaceCheck() &&
                !newSidebarView &&
                isOldUser ? (
                <WorkspaceDropdown
                  showLoadingState={showLoadingState}
                  hideLoadingState={hideLoadingState}
                />
              ) : null}
            </Grid>

            <Grid item>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                classes={{ container: classes.headerRightButtons }}>
                <div style={{ marginRight: 10 }} className="flex_center_start_row">
                  {/* <ColorImageModal /> */}
                  {workspaceCheck() && (
                    <>
                      {!newSidebarView && isOldUser && <Search />}
                      {timerTask && currentTimerTaskObj ? (
                        <div style={{ marginLeft: 20 }}>
                          <Timer
                            allMembers={[]}
                            taskId={timerTask && timerTask.task && timerTask.task.taskId}
                            createdDate={timerTask && timerTask.task && timerTask.createdDate}
                            currentDate={timerTask && timerTask.task && timerTask.currentDate}
                            currentTask={currentTimerTaskObj}
                            openTimerTaskDetailsPopUp={this.openTimerTaskDetailsPopUp}
                            isGlobal
                            permission=""
                            taskPer={taskPer}
                          />
                        </div>
                      ) : null}
                    </>
                  )}
                  {openTimerTaskDetails && currentTimerTask
                    ? this.props.taskDetailDialogState(null, {
                      id: currentTimerTask.taskId,
                      afterCloseCallBack: () => {
                        this.closeTimerTaskDetailsPopUp();
                      },
                      type: type,
                    })
                    : null}
                </div>
                {workspaceCheck() && !newSidebarView && isOldUser && <AddNewMenu />}

                {!newSidebarView && isOldUser && (
                  <HeaderProfileMenu workspaceCheck={workspaceCheck} />
                )}
                <div className={classes.addNew}>
                  {workspaceCheck() && (
                    <AddNewMenu
                      newBtnProps={{
                        style: {
                          borderRadius: 8,
                          padding: "5px 10px",
                          marginLeft: 15,
                          // marginRight: 15,
                        },
                      }}
                      placement={"bottom-start"}
                    />
                  )}
                </div>

                <div style={{ display: "flex" }}>
                  {this.showHelpButton() && <HelpCenter />}
                  <NotificationMenu
                    notificationsData={notifications}
                    MarkAllNotificationsAsRead={this.handleAllRead}
                    history={this.props.history}
                    customButtonProps={{
                      style: {
                        marginLeft: 10,
                        marginRight: 10,
                        minWidth: 0,
                      },
                    }}
                  />
                </div>

                {openDialog && (
                  <ProfileSetting
                    open={openDialog}
                    selectedIndex={1}
                    closeAction={this.handleDialogCloseAction}
                    showSnackBar={this.showSnackBar}
                  />
                )}
              </Grid>
            </Grid>
          </Grid>
          {!newSidebarView && isOldUser && <NavBar />}
        </AppBar>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    promoBar: state.promoBar.promoBar,
    notificationBar: state.notificationBar.notificationBar,
    notification: state.notification,
    notificationsState: state.notifications,
    tasks: state.tasks.data,
    teams: state.profile.data.teams,
    activeTeam: state.profile.data.activeTeam,
    isOwner: state.profile.data.teamMember.find((item) => item.userId == state.profile.data.userId)
      .isOwner,
    visibleUpgradePlan: state.userBillPlan.visibleUpgradePlan,
    constants: state.constants.data,
    taskPer: state.workspacePermissions.data.task,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    workspaceLevelMeetingPer: state.workspacePermissions.data.meeting,
    workspaceLevelRiskPer: state.workspacePermissions.data.risk,
    workspaceLevelIssuePer: state.workspacePermissions.data.issue,
    projects: state.projects.data || [],
    sidebarPannel: state.sidebarPannel,
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(headerStyles, { withTheme: true }),
  connect(mapStateToProps, {
    MarkAllNotificationsAsRead,
    FetchUserNotification,
    UpdatePromoCodeValue,
    showUpgradePlan,
    SaveLocalization,
    showHideNotificationBar,
    UpdateLocalization,
    SaveProfile,
    taskDetailDialogState,
    updateSidebarState,
    showHideSideBar,
  })
)(Header);
