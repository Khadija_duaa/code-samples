import React, { Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import { components } from "react-select";
import Avatar from "@material-ui/core/Avatar";
import CustomAvatar from "../Avatar/Avatar";
import autoCompleteStyles from "../../assets/jss/components/autoComplete";
import combineStyles from "../../utils/mergeStyles";
import { validateEmailField } from "../../utils/formValidations";
import DefaultButton from "../Buttons/DefaultButton";
import CustomButton from "../Buttons/CustomButton";
import dialogFormStyles from "../../assets/jss/components/dialogForm";
import { IsTeamUrlValid } from "../../redux/actions/teamMembers";
import { CheckAndAddUser } from "../../redux/actions/authentication";
import DefaultTextField from "../Form/TextField";
import GlobeIcon from "@material-ui/icons/Language";
import { injectIntl, FormattedMessage } from "react-intl";

import {
  AddWorkspace,
  FetchWorkspaceInfo,
} from "./../../redux/actions/workspace";
import {
  validateUrlField,
  validateTitleField,
} from "../../utils/formValidations";
import { FetchUserInfo } from "./../../redux/actions/profile";
import uniqBy from "lodash/uniqBy";
import { createNewWorkspaceHelpText } from "../Tooltip/helptext";
import { getCustomField } from "../../redux/actions/customFields";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../mixpanel';

class AddWorkspaceForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wsName: "",
      wsUrl: "",
      value: [],
      options: [],
      invalidEmail: false,
      invalidEmailMessage: false,
      AddWorkspaceMember: false,
      invalidWorkspaceName: false,
      invalidWorkspaceUrl: false,
      wsUrlErrorMessage: "",
      wsNameErrorMessage: "",
      wsNameBind: true,
      isFailed: false,
      message: "",
      isButtonDisabled: false,
      disableButton: true,
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSelectFocus = this.onSelectFocus.bind(this);
    this.onSelectBlur = this.onSelectBlur.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.isValidNewOption = this.isValidNewOption.bind(this);
  }
  componentDidMount() {
    mixpanel.time_event(MixPanelEvents.WorkspaceCreationEvent);
    let options = this.props.profileState.data.member.allMembers || [];
    let self = this;
    if (options.length)
      options.forEach(function (obj) {
        if (self.props.profileState.data.email !== obj.email) {
          obj.Active = "false";
          obj.value = obj.userName || obj.email;
          obj.label = obj.userName || obj.email;
        }
      });
    this.setState({ options });
  }
  isValidNewOption(inputValue) {
    // this.props.profile.data.member.allMembers
    // if(inputValue){
    // }

    inputValue = inputValue.trim();
    if (inputValue) {
      const validationObj = validateEmailField(inputValue, false);
      if (!validationObj["isError"]) {
        return true;
      }
    }
    return false;
  }
  handleSubmit() {
    this.setState({ isButtonDisabled: true }, () => {
      let isNameError = false,
        isUrlError = false;
      //////////////////////////////////// WORKSPACE NAME ////////////////////////////////////
      let workspaceNameValue = this.state.wsName.trim();
      let validationObj = validateTitleField(
        "workspace name",
        workspaceNameValue
      );
      if (validationObj["isError"]) {
        isNameError = true;
        this.setState({
          invalidWorkspaceName: validationObj["isError"],
          wsNameErrorMessage: this.props.intl.formatMessage({
            id:
              "workspace-settings.general-information.edit-modal.form.validation.worksapce-name.empty",
            defaultMessage: validationObj["message"],
          }),
          isButtonDisabled: false,
        });
      } else {
        this.setState({
          invalidWorkspaceName: false,
          wsNameErrorMessage: "",
        });
      }

      //////////////////////////////////// WORKSPACE URL ////////////////////////////////////
      let workspaceUrlValue = this.state.wsUrl.trim();
      validationObj = validateUrlField("workspace URL", workspaceUrlValue);
      if (validationObj["isError"]) {
        isUrlError = true;
        this.setState({
          invalidWorkspaceUrl: validationObj["isError"],
          wsUrlErrorMessage: this.props.intl.formatMessage({
            id:
              "workspace-settings.general-information.edit-modal.form.validation.workspace-url.empty",
            defaultMessage: validationObj["message"],
          }),
          isButtonDisabled: false,
        });
      } else {
        this.setState({
          invalidWorkspaceUrl: false,
          wsUrlErrorMessage: "",
        });
      }

      if (!isNameError && !isUrlError) {
        this.setState({ btnQuery: "progress" });
        let selectedOption = this.state.value;
        let ids = selectedOption.map((x) => x.userId);
        let object = {
          teamName: workspaceNameValue.replace(/\s\s+/g, " "),
          teamUrl: workspaceUrlValue,
          members: ids,
          isMembersInvitationAllowed: true,
        };
        this.props.AddWorkspace(
          object,
          (data) => {
            mixpanel.track(MixPanelEvents.WorkspaceCreationEvent, data);
            let loggedInTeamId = data.data.teamId;
            this.props.FetchWorkspaceInfo(loggedInTeamId, () => {
              /** fetching all user created custom fields */
              this.props.getCustomField(succ => { }, fail => { });
              this.props.FetchUserInfo(
                //Success
                () => {
                  this.setState({ btnQuery: "", wsUrl: "", wsName: "" });
                  this.props.closeDialog();
                }
              );
            });
          },
          (err) => {
            if (
              err.data.message ==
              "This URL is already taken. Please enter another URL."
            ) {
              this.setState({
                btnQuery: "",
                isFailed: true,
                invalidWorkspaceUrl: true,
                wsUrlErrorMessage: err.data.message,
                isButtonDisabled: false,
              });
            } else {
              this.setState({
                btnQuery: "",
                isFailed: true,
                invalidWorkspaceName: true,
                wsNameErrorMessage: err.data.message,
                isButtonDisabled: false,
              });
            }
          }
        );
      }
    });
  }
  handleChange(event, name) {
    let inputValue = event.currentTarget.value;
    switch (name) {
      //////////////////////////////////// WORKSPACE URL ////////////////////////////////////
      case "wsUrl": {
        let wsUrl = inputValue.replace(/ /g, "");
        this.setState({
          invalidWorkspaceUrl: false,
          wsUrlErrorMessage: "",
          [name]: wsUrl, // Mapping value of textfield to local state
          wsNameBind: false,
        });
        break;
      }

      //////////////////////////////////// OTHER FIELDS ////////////////////////////////////
      default: {
        this.setState({
          invalidEmail: false,
          invalidWorkspaceName: false,
          isFailed: false,
          wsNameErrorMessage: "",
          isFailed: false,
          message: "",
          [name]: inputValue, // Mapping value of textfield to local state,
          invalidWorkspaceUrl: false,
          wsUrlErrorMessage: "",
        });
        if (this.state.wsNameBind) {
          //Replacing all spaces with Dash from workspace url
          let wsUrl = inputValue.replace(/[\s\+]+/g, "-").toLowerCase();
          this.setState({ wsUrl });
        }
      }
    }
  }
  onSelectFocus(event) {
    this.setState({
      [event.target.id]: true,
    });
  }
  onSelectBlur(event) {
    if (this.state.value.length < 1) {
      this.setState({
        [event.target.id]: false,
      });
    }
  }

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  render() {
    const { classes, theme } = this.props;
    const {
      btnQuery,
      invalidWorkspaceName,
      invalidWorkspaceUrl,
      wsNameErrorMessage,
      wsUrlErrorMessage,
      disableButton,
      wsName,
      wsUrl,
    } = this.state;
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: "4px 0 4px 0",
        minHeight: 41,
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: "none",
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`,
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`,
        },
      }),
      input: (base, state) => ({
        color: theme.palette.text.secondary,
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "0 0 0 14px",
      }),
      multiValue: (base, state) => ({
        ...base,
        background: theme.palette.background.light,
        padding: "5px 5px",
        borderRadius: 20,
      }),
      multiValueLabel: (base, state) => ({
        ...base,
        marginRight: 10,
        //background: "red"
      }),
      multiValueRemove: (base, state) => ({
        ...base,
        background: theme.palette.background.contrast,
        borderRadius: "50%",
        color: theme.palette.common.white,
        ":hover": {
          background: theme.palette.error.light,
          color: theme.palette.common.white,
        },
      }),
    };
    const disableButtonAction = wsName == "" && wsUrl == "" ? true : false;

    const addEmailLabel = (inputValue) => {
      return `Invite "${inputValue}"`;
    };
    const Option = (props) => {
      return props.data.email !== this.props.profileState.data.email ? (
        <components.Option {...props}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="center"
          >
            {props.data.__isNew__ ? (
              <Avatar
                classes={{
                  root: classes.dropDownAddAvatar,
                }}
              >
                {" "}
                +{" "}
              </Avatar>
            ) : (
              <CustomAvatar
                otherMember={{
                  imageUrl: props.data.imageUrl,
                  fullName: props.data.fullName,
                  lastName: "",
                  email: props.data.email,
                  isOnline: props.data.isOnline,
                  isOwner: props.data.isOwner,
                }}
                size="small"
              />
            )}{" "}
            <span> {props.children} </span>{" "}
          </Grid>{" "}
        </components.Option>
      ) : null;
    };

    return (
      <Fragment>
        <div className={classes.dialogFormCnt} noValidate autoComplete="off">
          <DefaultTextField
            label={
              <FormattedMessage
                id="workspace-settings.general-information.edit-modal.form.workspace-name.label"
                defaultMessage="Workspace Name"
              />
            }
            helptext={
              <FormattedMessage
                id="workspace-settings.general-information.edit-modal.form.workspace-name.hint"
                defaultMessage={
                  createNewWorkspaceHelpText.workspaceNameHelpText
                }
              />
            }
            errorState={invalidWorkspaceName}
            errorMessage={wsNameErrorMessage}
            defaultProps={{
              type: "text",
              id: "WorkspaceName",
              placeholder: this.props.intl.formatMessage({
                id:
                  "workspace-settings.general-information.edit-modal.form.workspace-name.placeholder",
                defaultMessage: "Enter your workspace name",
              }),
              value: wsName,
              autoFocus: true,
              inputProps: { maxLength: 80 },
              onChange: (event) => {
                this.handleChange(event, "wsName");
              },
            }}
          />

          <DefaultTextField
            label={
              <FormattedMessage
                id="workspace-settings.general-information.edit-modal.form.workspace-url.label"
                defaultMessage="Workspace URL"
              />
            }
            helptext={
              <FormattedMessage
                id="workspace-settings.general-information.edit-modal.form.workspace-url.hint"
                defaultMessage={createNewWorkspaceHelpText.workspaceUrlHelpText}
              />
            }
            errorState={invalidWorkspaceUrl}
            errorMessage={wsUrlErrorMessage}
            defaultProps={{
              id: "WorkspaceUrl",
              onChange: (event) => {
                this.handleChange(event, "wsUrl");
              },
              placeholder: this.props.intl.formatMessage({
                id:
                  "workspace-settings.general-information.edit-modal.form.workspace-url.label",
                defaultMessage: "Workspace URL",
              }),
              type: "text",
              value: wsUrl,
              inputProps: {
                style: { paddingLeft: 0 },
                maxLength: 80,
              },
              startAdornment: (
                <InputAdornment
                  position="start"
                  classes={{ root: classes.urlAdornment }}
                >
                  <GlobeIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.globeIcon}
                  />
                  <p className={classes.workspaceUrlAdornment}>
                    {window.location.origin + "/"}
                  </p>
                </InputAdornment>
              ),
            }}
          />
        </div>
        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="center"
          classes={{ container: classes.dialogFormActionsCnt }}
        >
          <DefaultButton
            onClick={this.props.closeDialog}
            text={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            buttonType="Transparent"
            style={{ marginRight: 20 }}
          />
          <CustomButton
            onClick={this.handleSubmit}
            btnType="success"
            variant="contained"
            query={btnQuery}
            disabled={
              btnQuery == "progress" ||
              invalidWorkspaceUrl ||
              invalidWorkspaceName ||
              disableButtonAction
            }
          >
            <FormattedMessage
              id="workspace-settings.create-workspace.label"
              defaultMessage="Create Workspace"
            />
          </CustomButton>
        </Grid>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(autoCompleteStyles, dialogFormStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    AddWorkspace,
    IsTeamUrlValid,
    CheckAndAddUser,
    // SwitchToTeam,
    FetchUserInfo,
    FetchWorkspaceInfo,
    getCustomField
  })
)(AddWorkspaceForm);
