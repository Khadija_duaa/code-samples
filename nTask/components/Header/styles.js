const newSideBarWidth = 221; //Width of sidebar when open
const sideBarWidth = 180; //Width of sidebar when open
const sideBarCloseWidth = 72; // width of sidebar when closed
import TeamArrowDD from "../../assets/images/team_arrow_dd.png";
import TeamArrowDDHover from "../../assets/images/team_arrow_dd_hover.png";

const headerStyles = theme => ({
  rootSVG: {
    fontSize: "24px !important"
  },
  root: {
    minHeight: "unset",
  },
  appBar: {
    marginLeft: sideBarCloseWidth,
    boxShadow: "0px 1px 0px #E7E9EB",
    position: "fixed",
    border: "none",
    width: `calc(100% - ${sideBarCloseWidth}px)`,
    background: "white",
  },
  topBarAdjustment: {
    padding: "3px 0px",
  },
  sidebarClose: {
    marginLeft: `0px !important`,
    width: `calc(100% - 1px) !important`,
  },
  appBarShift: {
    marginLeft: sideBarWidth,
    width: `calc(100% - ${sideBarWidth - 1}px)`,
  },
  headerRightButtons: {
    padding: "0px 20px 0px 0",
  },
  userProfileMenuAvatar: {
    marginLeft: 10,
  },
  profileInfoDropdownCnt: {
    display: "flex",
    height: "auto",
    flexDirection: "column",
    alignItems: "center",
    padding: "15px 20px 15px 20px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    cursor: "unset",
    "&:hover": {
      background: theme.palette.common.white,
    },
  },
  profileInfoList: {
    margin: "10px 0 0 0",
    padding: 0,
    listStyleType: "none",

    "& li": {
      marginBottom: 5,
      lineHeight: "normal",
      color: theme.palette.text.secondary,
      fontSize: "12px !important",
      textAlign: "center",
    },
    "& $firstName": {
      fontSize: "16px !important",
      fontWeight: theme.typography.fontWeightRegular,
      color: theme.palette.text.primary,
    },
  },
  firstName: {},
  recommendMenuItem: {
    padding: "15px 10px",
    marginTop: 15,
    borderRadius: "0 0 7px 7px",
    fontSize: "1em !important",
    lineHeight: "normal",
    background: theme.palette.background.paper,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    cursor: "initial",
    "&:hover": {
      background: theme.palette.background.paper,
      color: theme.palette.primary.light,
    },
  },
  addNew: {
    // borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    marginRight: 7,
  },
  recommendMenuIcon: {
    fontSize: "36px !important",
    marginRight: 10,
  },
  loveText: {
    display: "block",
  },
  recommendText: {
    display: "block",
  },
  headerLeftBtnsCnt: {
    // background: theme.palette.background.items,
    display: "flex",
    alignItems: "center",
  },
  workspaceSearchCnt: {
    padding: "0 10px",
  },
  headerLeftBtns: {
    padding: "0",
    background: theme.palette.background.light,
    borderRadius: 0,
    "&:hover": {
      background: theme.palette.background.grayLighter,
    },
  },
  headerLeftBtnsText: {
    textTransform: "unset",
    fontSize: "1.1em !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  // Notification Menu Styles
  NotifAvatar: {
    borderRadius: "100%",
    width: 56,
    height: 56,
  },
  readNotifMenuItem: {
    height: "auto",
    padding: "15px 15px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    alignItems: "flex-start",
    background: "transparent",
    "&:hover": {
      background: theme.palette.background.contrast,
    },
  },
  unreadNotifMenuItem: {
    height: "auto",
    padding: "15px 15px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    alignItems: "flex-start",
    background: theme.palette.background.paper,
    "&:hover": {
      background: theme.palette.background.contrast,
    },
  },
  notifMenuHeadingItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    cursor: "unset",
    "&:hover": {
      background: "transparent",
    },
  },
  menuHeaderReadAllText: {
    color: theme.palette.text.primary,
    cursor: "pointer",
  },
  menuHeadingText: {
    textTransform: "uppercase",
  },
  notifMenuItemText: {
    padding: 0,
    "& p": {
      fontSize: "12px !important",
      fontWeight: theme.typography.fontWeightLight,
      color: theme.palette.text.secondary,
      whiteSpace: "normal",
      margin: 0,
      "& b": {
        fontWeight: theme.typography.fontWeightMedium,
        color: theme.palette.text.primary,
      },
    },
    "& $notifMenuDate": {
      fontSize: "10px !important",
    },
    "& $notifMenuData": {
      fontSize: "10px !important",
      display: "flex",
    },
  },
  notifMenuDate: {},
  notifMenuData: {},
  dot: {
    color: theme.palette.secondary.medDark,
    margin: "0px 5px",
    fontSize: "24px !important",
    lineHeight: "0.7",
  },
  nameCnt: {
    maxWidth: 130,
    overflow: "hidden",
    fontSize: "12px !important",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
  badgeIcon: {
    width: 12,
    height: 12,
  },
  notificationBadge: {
    fontSize: "0.6428571428571428rem",
    background: theme.palette.common.white,
    color: theme.palette.background.danger,
    border: `1px solid ${theme.palette.border.redBorder}`,
    height: 16,
    minWidth: 16,
    borderRadius: "50%",
    top: 4,
    right: 4,
  },
  headerDetailsBottomMsg: {
    padding: "10px 20px",
    cursor: "pointer",
    background: theme.palette.background.paper,
    borderRadius: "0 0 4px 4px",
  },
  navBar: {
    background: theme.palette.common.white,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    "& ul": {
      display: "flex",
      listStyleType: "none",
      padding: 0,
      "& li": {
        padding: "10px 13px",
        margin: "0 7px",
        cursor: "pointer",
        position: "relative",
        "&:before": {
          content: '""',
          position: "absolute",
          width: "100%",
          height: 2,
          bottom: 0,
          left: 0,
          backgroundColor: theme.palette.text.green,
          visibility: "hidden",
          transform: "scaleX(0)",
          transition: "all 0.3s ease-in-out 0s",
        },
        "&:hover:before": {
          visibility: "visible",
          transform: "scaleX(1)",
        },
        "&:hover p": {
          color: theme.palette.text.green,
        },
        "& p": {
          textTransform: "capitalize",
        },
      },
      "& li:first-child": {
        marginLeft: 0,
      },
      "& $selectedNav": {
        "& p": {
          color: theme.palette.text.green,
          "&:before": {
            content: '""',
            position: "absolute",
            width: "100%",
            height: 2,
            bottom: 0,
            left: 0,
            backgroundColor: theme.palette.text.green,
          },
        },
        "& h6": {
          color: theme.palette.text.green,
          "&:before": {
            content: '""',
            position: "absolute",
            width: "100%",
            height: 2,
            bottom: 0,
            left: 0,
            backgroundColor: theme.palette.text.green,
          },
        },
      },
    },
  },
  selectedNav: {},

  //Notification Styles
  emptyNotifCnt: {
    minHeight: 200,
    background: theme.palette.common.white,
    textAlign: "center",
    padding: "36px 0",
  },
  emptyNotifImg: {
    marginBottom: 20,
  },
  notifMenuHeading: {
    padding: 10,
  },
  teamDDArrow: {
    height: 50,
    width: 34,
    background: `url(${TeamArrowDD})`,
    backgroundRepeat: "no-repeat",
    // minWidth: '37px!important',
    // backgroundColor: theme.palette.common.white,
    zIndex: 1,
    "&:hover": {
      background: `url(${TeamArrowDDHover})`,
    },
  },
  dropdownArrow: {
    position: "absolute",
    right: -6,
    top: 23,
    color: theme.palette.secondary.medDark,
  },
  teamDropdownHeadingCnt: {
    display: "flex",
    flexDirection: "column",
    alignItems: "start",
    justifyContent: "center",
  },
  teamDropdownHeading: {
    lineHeight: "normal",
    fontSize: "11px !important",
    textAlign: "left",
  },
  planLbl: {
    padding: "2px 8px",
    fontSize: "10px !important",
    backgroundColor: theme.palette.background.darkblue,
    borderRadius: 4,
    marginLeft: 5,
    color: theme.palette.common.white,
    letterSpacing: 1,
    fontWeight: 600,
    display: "flex",
    alignItems: "center",
  },
  blockWorkspaceLbl: {
    padding: "2px 10px",
    fontSize: "10px !important",
    backgroundColor: theme.palette.background.danger,
    borderRadius: 4,
    marginLeft: 5,
    color: theme.palette.common.white,
    letterSpacing: 1,
    fontWeight: 600,
    display: "flex",
    alignItems: "center",
  },
  topNotificationBar: {
    background: "#2c2053",
    padding: "0 20px",
    position: "fixed",
    minHeight: 50,
    top: 0,
    left: 0,
    right: 0,
    zIndex: 11111,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "& > p": {
      color: theme.palette.common.white,
      fontSize: "12px !important",
      marginRight: 10,
    },
  },
  leftArrowImage: {
    position: "absolute",
    left: 0,
    bottom: 0,
  },
  newYearLogo: {
    position: "absolute",
    left: 40,
    bottom: 0,
  },
  rightArrowImage: {
    position: "absolute",
    right: 40,
    bottom: 0,
  },

  menuItemName: {
    maxWidth: 150,
    minWidth: 150,
    textOverflow: "ellipsis",
    overflow: "hidden",
  },
  snackBarHeadingCnt: {
    marginLeft: 10,
  },
  snackBarContent: {
    margin: 0,
    fontSize: "12px !important",
  },
  notificationCount: {
    fontSize: "10px !important",
    color: theme.palette.common.white,
    background: theme.palette.background.danger,
    height: 16,
    width: 16,
    lineHeight: "normal",
    borderRadius: "50%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  profileAvatarBadge: {
    background: theme.palette.background.danger,
    color: theme.palette.common.white,
    height: 16,
    transform: "scale(1) translate(33%, -35%)",
    minWidth: 16,
  },
  teamNameCnt: {
    display: "flex",
    flexDirection: "column",
  },
  teamOwnerName: {
    fontSize: "11px !important",
    color: theme.palette.text.medGray,
    overflow: "hidden",
    textOverflow: "ellipsis",
    maxWidth: 150,
    lineHeight: "normal",
  },

  paymentPending: {
    position: "fixed",
    zIndex: 1300,
    backgroundColor: theme.palette.common.white,
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    opacity: 0.9,
  },
  notificationIcon: {
    fontSize: "20px !important",
  },
});

export default headerStyles;
