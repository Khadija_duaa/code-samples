import React, { Component, Fragment } from "react";
import Badge from "@material-ui/core/Badge";
import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";
import { connect } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import NotificationIcon from "@material-ui/icons/Notifications";
import CustomMenu from "../Menu/CustomMenu";
import IconButton from "../Buttons/IconButton";
import CustomAvatar from "../Avatar/Avatar";
import Typography from "@material-ui/core/Typography";
import headerStyles from "./styles";
import { Scrollbars } from "react-custom-scrollbars";
import EmptyNotifIcon from "../Icons/EmptyNotifIcon";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import AuomationRuleicon from "../Icons/AuomationRuleicon";
import SvgIcon from "@material-ui/core/SvgIcon";
import ReactHtmlParser from 'react-html-parser';
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router-dom";
import isEmpty from "lodash/isEmpty";

class NotificationMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      unread: true,
      anchorEl: null
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleAllRead = this.handleAllRead.bind(this);
  }
  handleClick(event, placement) {
    event.stopPropagation();
    this.setState({ open: true, anchorEl: event.currentTarget });
  }
  handleAllRead() {
    this.props.MarkAllNotificationsAsRead();
    this.setState({ unread: false });
  }
  handleClose(event) {
    this.setState({ open: false });
  }
  openNotification = () => {
    this.props.MarkAllNotificationsAsRead();
    this.props.history.push("/notifications");
  };
  isActiveWS = item => {
    if (
      item.workspaceId &&
      item.workspaceId == this.props.workspaceId &&
      this.props.workspaceLoaded
    )
      return true;

    return false;
  };
  isValidURL = item => {
    if (item.notificationUrl && item.notificationUrl != "#") return true;
    return false;
  };
  goToNotification = item => {
    if (this.isValidURL(item)) {
      let nUrl = new URL(item.notificationUrl);
      if (this.isActiveWS(item)) {
        let n = item.notificationUrl.replace(nUrl.origin, "");
        this.props.history.push(n);
      } else {
        window.location.href = item.notificationUrl;
      }
    }
  };


  render() {
    const { classes, theme, customButtonProps = {} } = this.props;
    const { open, unread, anchorEl } = this.state;

    const notifications = this.props.notificationsData || [];
    return (
      <>
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <IconButton
              btnType="transparent"
              onClick={this.handleClick}
              // buttonRef={node => {
              //   this.anchorEl = node;
              // }}
              {...customButtonProps}>
              <Badge badgeContent={notifications.length} classes={{ badge: classes.notificationBadge }}>
                <NotificationIcon classes={{ root: classes.rootSVG }} htmlColor={theme.palette.secondary.medDark}/>
              </Badge>
            </IconButton>
            <CustomMenu
              style={{ width: 400 }}
              open={open}
              closeAction={this.handleClose}
              anchorEl={anchorEl}
              placement="bottom-end">
              <div className={`flex_center_space_between_row ${classes.notifMenuHeading}`}>
                <Typography variant="caption">
                  <FormattedMessage
                    id="common.notifications.title"
                    defaultMessage="NOTIFICATIONS"></FormattedMessage>
                </Typography>
                {notifications.length ? (
                  <Typography
                    variant="body2"
                    classes={{ body2: classes.menuHeaderReadAllText }}
                    onClick={this.handleAllRead}>
                    <FormattedMessage
                      id="common.notifications.markAll"
                      defaultMessage="Mark all as read"></FormattedMessage>
                  </Typography>
                ) : null}
              </div>
              {notifications.length ? (
                <Scrollbars autoHide style={{ height: 400 }}>
                  <MenuList disableRipple={true} disablePadding>
                    {notifications.map((item, x, y) => {
                      let selectedMember = this.props.profile.member.allMembers.filter(member => {
                        return item.userId == member.userId;
                      });

                      const title = item.title.indexOf("Notification Alert!") === 0 ? item.title.replace("Notification Alert!", "<b>Notification Alert!</b><br/>")
                        .replace("Message:", "<br/><b>Message: </b>")
                        .replace("Task:", "<br/><b>Task: </b>")
                        .replace(`${item.title.substring(20, item.title.indexOf('Message'))}`, `<b>${item.title.substring(20, item.title.indexOf('Message'))}</b>`)
                        : item.title

                      return (
                        <MenuItem
                          key={item.id}
                          classes={{
                            root: unread ? classes.unreadNotifMenuItem : classes.readNotifMenuItem,
                          }}
                          onClick={() => this.goToNotification(item)}>
                          <ListItemIcon>
                            {item.title.indexOf("Notification Alert!") === 0 ?
                              <AuomationRuleicon /> :
                              <CustomAvatar
                                otherMember={{
                                  imageUrl: item.profilePic,
                                  fullName: item.createdName ? item.createdName.fullName : "",
                                  lastName: "",
                                  email: item.createdName ? item.createdName.email : "",
                                  isOnline: !selectedMember.length ? false : selectedMember[0].isOnline,
                                  isOwner: item.isOwner,
                                }}
                                size="small"
                              />
                            }
                          </ListItemIcon>

                          <ListItemText classes={{ root: classes.notifMenuItemText }}>
                            <p>{ReactHtmlParser(title)}</p>
                            <p className={classes.notifMenuData}>
                              <Typography variant="h6" classes={{ h6: classes.nameCnt }}>
                                {item.teamName}
                              </Typography>

                              <span className={classes.dot}>•</span>
                              <Typography variant="h6" classes={{ h6: classes.nameCnt }}>
                                {item.workspaceName}
                              </Typography>
                            </p>
                            <p className={classes.notifMenuDate}>
                              {item.date} - {item.time}
                            </p>
                          </ListItemText>
                        </MenuItem>
                      );
                    })}
                  </MenuList>
                </Scrollbars>
              ) : (
                <div className={classes.emptyNotifCnt}>
                  <img
                    src={EmptyNotifIcon}
                    className={classes.emptyNotifImg}
                    height={67}
                    width={75}
                    alt={
                      <FormattedMessage
                        id="common.notifications.no-notification.label"
                        defaultMessage="No Notification"
                      />
                    }
                  />

                  <Typography variant="h4" align="center">
                    {
                      <FormattedMessage
                        id="common.notifications.label"
                        defaultMessage="You're all caught up!"
                      />
                    }
                  </Typography>
                  <Typography variant="body2" align="center">
                    {
                      <FormattedMessage
                        id="common.notifications.no-notification.label"
                        defaultMessage="No pending notifications"
                      />
                    }
                  </Typography>
                </div>
              )}

              <Typography
                variant="body2"
                align="center"
                className={classes.headerDetailsBottomMsg}
                onClick={this.openNotification}>
                {
                  <FormattedMessage
                    id="common.notifications.view-all-notification.label"
                    defaultMessage="View All Notifications"
                  />
                }
              </Typography>
            </CustomMenu>
          </div>
        </ClickAwayListener>
      </>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.profile.data,
    workspaceId: state.profile.data.loggedInTeam,
    workspaceLoaded: !isEmpty(state.workspacePermissions.data.workSpace),
  };
};
export default compose(
  withRouter,
  connect(mapStateToProps),
  withStyles(headerStyles, { withTheme: true })
)(NotificationMenu);
