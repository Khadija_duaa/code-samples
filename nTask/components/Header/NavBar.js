import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { FormattedMessage } from "react-intl";
import headerStyles from "./styles";
import { teamCanView } from "../PlanPermission/PlanPermission";
import { GetPermission } from "../permissions";
import { clearKanbanStoreData } from "../../redux/actions/boards";
import { projectDetails } from "../../redux/actions/projects";
import { isProjectManager } from "../../redux/actions/team";

import isEmpty from "lodash/isEmpty";


class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: "tasks",
      isAnalyticsEnabled: true,
      isManager: false,
    };
    this.handleListItemClick = this.handleListItemClick.bind(this);
  }

  componentDidMount() {
    const match = this.props.match.params.view;
    this.setState({ selectedIndex: match });
    isProjectManager(response => {
      this.setState({ isProjectManager: response, });
    });
  }

  componentDidUpdate(prevProps, prevState) {
    let match = this.props.match.params.view; // Getting Url match
    if (match !== prevProps.match.params.view) {
      // Code to be executed if prev url match is not equal to new url match and updates the state for current url match
      match = match == "workspace-settings" ? "settings" : match;
      this.setState({ selectedIndex: match });
    }
  }

  // When the respective item is click in navbar
  handleListItemClick(event, navItem) {
    const { board } = this.props;
    const push = match => {
      this.props.history.push(match);
    };
    this.setState({ selectedIndex: navItem }, () => {
      if (navItem === "projects") {
        push("/projects");
      } else if (navItem === "tasks") {
        this.closeProjectDetailDockedView();
        push("/tasks");
      } else if (navItem === "meetings") {
        this.closeProjectDetailDockedView();
        push("/meetings");
      } else if (navItem === "timesheet") {
        this.closeProjectDetailDockedView();
        push("/timesheet");
      } else if (navItem === "teamtimesheet") {
        this.closeProjectDetailDockedView();
        push("/reports/timesheet");
      } else if (navItem === "issues") {
        this.closeProjectDetailDockedView();
        push("/issues");
      } else if (navItem === "risks") {
        this.closeProjectDetailDockedView();
        push("/risks");
      } else if (navItem === "team-settings") {
        push("/team-settings");
      } else if (navItem === "overview") {
        push("/reports/overview");
      } else if (navItem === "issueOverview") {
        push("/reports/IssueOverview");
      } else if (navItem === "members-overview") {
        push("/reports/members-overview");
      } else if (navItem === "analytics") {
        push("/reports/analytics");
      } else if (navItem === "iwms") {
        this.closeProjectDetailDockedView();
        push("/teams/IwmsAnalytics");
      } else if (navItem === "StatusReports") {
        this.closeProjectDetailDockedView();
        push("/teams/StatusReports");
      } else if (navItem === "resources") {
        push("/teams/resources");
      } else if (navItem === "settings") {
        this.closeProjectDetailDockedView();
        push("/workspace-settings");
      } else if (navItem === "boards") {
        this.closeProjectDetailDockedView();
        push("/boards");
        if (!isEmpty(board.data.kanban)) {
          this.props.clearKanbanStoreData();
        }
      } else {
        // this.props.PopulateTempData("Enter-WorkSpace");
        const activeTeam = this.props.profileState.data.teams.find(t => {
          return t.companyId == this.props.profileState.data.activeTeam;
        });
        // checks if there is no active workspace and redirects to active team url
        push(`/teams/${activeTeam.companyUrl}`);
      }
    });
  }

  closeProjectDetailDockedView = () => {
    this.props.projectDetails({
      projectDetails: {},
      projectSettings: {},
      fullView: true,
      docked: false,
      dialog: false,
      loadingState: false,
    });
  };

  getPermission = key => {
    /** function calling for getting permission */
    const { workspacePermissionsState, profileState } = this.props;
    const currentUser = profileState.data.teamMember.find(m => {
      /** finding current user from team members array */
      return m.userId == profileState.data.userId;
    });
    // return GetPermission.canDo(currentUser.isOwner, workspacePermissionsState.data.workSpace||{}, key) /** passing current user owner , workspace permission and a key to check */
    return true;
  };

  getTranslatedId(value) {
    switch (value) {
      case "projects":
        value = "project.label";
        break;
      case "boards":
        value = "board.label";
        break;
      case "tasks":
        value = "task.label";
        break;
      case "meetings":
        value = "meeting.label";
        break;
      case "timesheet":
        value = "timesheet.label";
        break;
      case "issues":
        value = "issue.label";
        break;
      case "risks":
        value = "risk.label";
        break;
      case "settings":
        value = "workspace-settings.label2";
        break;
    }
    return value;
  }

  render() {
    const { classes, profileState, location, workspacePermissionsState, workSpacePer, companyInfo } = this.props;
    const { isProjectManager } = this.state;
    const { activeTeam } = this.props.profileState.data;
    const taskLabel = companyInfo?.task?.pName || 'tasks';
    const viewsArray = () => {
      return [
        "projects",
        "boards",
        taskLabel,
        "meetings",
        "timesheet",
        "issues",
        "risks",
        "settings",
      ].filter(v => {
        // Filtering the optiosn list based on team permission
        if (v !== "projects" && v !== "risks" && v !== "boards") {
          return v;
        }
        if (v == "risks") {
          return v;
        }
        if (v == "projects") {
          return v;
        }
        if (v == "boards") {
          return v;
        }
        return false;
      });
    }
    const currentUser = profileState.data.teamMember.find(m => {
      return profileState.data.userId == m.userId;
    });
    const permissions = workspacePermissionsState.data
      ? workspacePermissionsState.data.workSpace
      : "";

    // Commented for later use
    // const reportsPermission = profileState.data.workspace.filter(ws => {
    //       return ws.isRWMsmin || ws.isTeamOwner
    // })
    const workspaceCheck = () => {
      if (location.pathname.indexOf("/teams/") == 0) {
        return true;
      }
      if (location.pathname.indexOf("/team-settings") == 0) {
        return true;
      }
      if (location.pathname.indexOf("/notifications") == 0) {
        return true;
      }
      if (location.pathname.indexOf("/reports/") == 0) {
        return true;
      }
      return false;
    };
    const isOverViewEnabled = () => {
      const { activeTeam } = this.props.profileState.data;
      if (activeTeam)
        return this.props.profileState.data.teams.some(
          t => t.companyId == activeTeam && t.isAllowedDashboard
        );
      return true;
    };
    const isTimeSheetEnabled = () => {
      const { activeTeam } = this.props.profileState.data;
      if (activeTeam)
        return this.props.profileState.data.teams.some(
          t => t.companyId == activeTeam && t.isAllowedTeamTimeSheet
        );
      return false;
    };
    const isAnalyticsEnabled = () => {
      const { activeTeam } = this.props.profileState.data;
      if (activeTeam)
        return this.props.profileState.data.teams.some(
          t => t.companyId == activeTeam && t.isAllowedAnalytics
        );
      return false;
    };
    return (
      <div className={classes.navBar}>
        <ul>
          {workspaceCheck() ? (
            <>
              <li
                onClick={event => this.handleListItemClick(event, 6)}
                className={
                  location.pathname.indexOf("/teams/") == 0 &&
                  location.pathname.indexOf("/teams/overview") != 0 &&
                  location.pathname.indexOf("/teams/IssueOverview") != 0 &&
                  location.pathname.indexOf("/teams/analytics") != 0 &&
                  location.pathname.indexOf("/teams/members-overview") != 0 &&
                  location.pathname.indexOf("/teams/resources") != 0 &&
                  location.pathname.indexOf("/teams/timesheet") != 0 &&
                  location.pathname.indexOf("/teams/IwmsAnalytics") != 0 &&
                  location.pathname.indexOf("/teams/StatusReports") != 0
                    ? classes.selectedNav
                    : ""
                }>
                <Typography variant="h6">
                  <FormattedMessage id="common.dashboard.label" defaultMessage="Dashboard" />
                </Typography>
              </li>

              {isOverViewEnabled() && (
                <li
                  onClick={event => this.handleListItemClick(event, "overview")}
                  className={
                    location.pathname.indexOf("/reports/overview") == 0 ? classes.selectedNav : ""
                  }>
                  <Typography variant="h6">Task Overview</Typography>
                </li>
              )}
              {isTimeSheetEnabled() && (
                <li
                  onClick={event => this.handleListItemClick(event, "issueOverview")}
                  className={
                    location.pathname.indexOf("/reports/IssueOverview") == 0
                      ? classes.selectedNav
                      : ""
                  }>
                  <Typography variant="h6">Issue Overview</Typography>
                </li>
              )}
              {(currentUser.role == "Owner" || currentUser.role == "Admin" || isProjectManager) && isOverViewEnabled() && (
                <li
                  onClick={event => this.handleListItemClick(event, "analytics")}
                  className={
                    location.pathname.indexOf("/reports/analytics") == 0 ? classes.selectedNav : ""
                  }>
                  <Typography variant="h6">Analytics</Typography>
                </li>
              )}

              {true && (
                <li
                  onClick={event => this.handleListItemClick(event, "StatusReports")}
                  className={
                    location.pathname.indexOf("/teams/StatusReports") == 0
                      ? classes.selectedNav
                      : ""
                  }>
                  <Typography variant="h6">Status Reports</Typography>
                </li>
              )}
{/* 
              {(currentUser.role == "Owner" || currentUser.role == "Admin") && isOverViewEnabled() && (
                <li
                  onClick={event => this.handleListItemClick(event, "members-overview")}
                  className={
                    location.pathname.indexOf("/reports/members-overview") == 0
                      ? classes.selectedNav
                      : ""
                  }>
                  <Typography variant="h6">
                    {activeTeam === "013f092268fb464489129c5ea19b89d3"
                      ? "Members Overview"
                      : "Status Report"}
                  </Typography>
                </li>
              )} */}

              {isTimeSheetEnabled() && (
                <li
                  onClick={event => this.handleListItemClick(event, "teamtimesheet")}
                  className={
                    location.pathname.indexOf("/reports/timesheet") == 0 ? classes.selectedNav : ""
                  }>
                  <Typography variant="h6">Timesheet</Typography>
                </li>
              )}
              {/*{true && (*/}
              {/*  <li*/}
              {/*    onClick={event => this.handleListItemClick(event, "iwms")}*/}
              {/*    className={*/}
              {/*      location.pathname.indexOf("/teams/IwmsAnalytics") == 0 ? classes.selectedNav : ""*/}
              {/*    }>*/}
              {/*    <Typography variant="h6">Iwms Analytics</Typography>*/}
              {/*  </li>*/}
              {/*  */}
              {/*)}*/}
              {/* {(ENV == "dev" || ENV == "beta") && (
                <li
                  onClick={event => this.handleListItemClick(event, "resources")}
                  className={
                    location.pathname.indexOf("/teams/resources") == 0 ? classes.selectedNav : ""
                  }>
                  <Typography variant="h6">Resources</Typography>
                </li>
              )} */}

              {(currentUser.role == "Owner" || currentUser.role == "Admin") && (
                <li
                  onClick={event => this.handleListItemClick(event, "team-settings")}
                  className={
                    location.pathname.indexOf("/team-settings") == 0 ? classes.selectedNav : ""
                  }>
                  <Typography variant="h6">
                    <FormattedMessage id="workspace-settings.label2" defaultMessage="Settings" />
                  </Typography>
                </li>
              )}
            </>
          ) : (
            viewsArray().map((Ele, key, arr) => {
              const itemIndex = arr.indexOf(Ele);
              return (
                // commented for later user
                // Ele == "reports" && reportsPermission.length == 0 ? null :
                Ele != "settings" ? (
                  <li
                    className={this.state.selectedIndex === Ele ? classes.selectedNav : ""}
                    onClick={event => this.handleListItemClick(event, Ele)}
                    key={itemIndex}>
                    <Typography variant="body2">
                      <FormattedMessage id={this.getTranslatedId(Ele)} defaultMessage={Ele} />
                    </Typography>
                  </li>
                ) : Ele == "settings" &&
                  workSpacePer.workspaceSetting &&
                  workSpacePer.workspaceSetting.cando ? (
                  <li
                    className={this.state.selectedIndex === Ele ? classes.selectedNav : ""}
                    onClick={event => this.handleListItemClick(event, Ele)}
                    key={itemIndex}>
                    <Typography variant="body2">
                      <FormattedMessage id={this.getTranslatedId(Ele)} defaultMessage={Ele} />
                    </Typography>
                  </li>
                ) : null
              );
            })
          )}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    workspacePermissionsState: state.workspacePermissions,
    workSpacePer: state.workspacePermissions.data.workSpace,
    board: state.boards,
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  withRouter,
  withStyles(headerStyles, { withTheme: true }),
  connect(mapStateToProps, {
    clearKanbanStoreData,
    projectDetails,
  })
)(NavBar);
