import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import PlainMenu from "../Menu/menu";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import menuStyles from "../../assets/jss/components/menu";
import combinedStyles from "../../utils/mergeStyles";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import DefaultDialog from "../Dialog/Dialog";
import CreateTeamForm from "../../Views/AddNewForms/addNewTeamFrom";
import {
  switchTeam,
  whiteLabelInfo,
  DefaultWhiteLabelInfo,
} from "./../../redux/actions/teamMembers";
import { FetchTasksInfo } from "./../../redux/actions/tasks";
import { FetchUserInfo } from "./../../redux/actions/profile";
import { projectDetails } from "./../../redux/actions/projects";

import { Scrollbars } from "react-custom-scrollbars";
import headerStyles from "./styles";
// import WorkspaceSetting from "../WorkspaceSetting/WorkspaceSetting";
import SettingIcon from "@material-ui/icons/Settings";
import GroupIcon from "@material-ui/icons/Group";
import CustomAvatar from "../Avatar/Avatar";
import MoveWorkspaceData from "../Dialog/NewWorkspaceDialog/MoveWorkspaceData";
import { FetchWorkspaceInfo } from "./../../redux/actions/workspace";
import { isEqual } from "lodash/isEqual";
import BreadCrumbArrow from "../../assets/images/arrow_breadCrum.png";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import Typography from "@material-ui/core/Typography";
import ConstantsPlan from "../constants/planConstant";
import moment from "moment";
import TeamArrowDD from "../../assets/images/team_arrow_dd.png";
import Basic from "../../assets/images/plans/Basic.svg";
import Business from "../../assets/images/plans/Business.svg";
import Enterprise from "../../assets/images/plans/Enterprise.svg";
import Premium from "../../assets/images/plans/Premium.svg";
import Basic_Badge from "../../assets/images/plans/Basic_Badge.svg";
import Business_Badge from "../../assets/images/plans/Business_Badge.svg";
import Enterprise_Badge from "../../assets/images/plans/Enterprise_Badge.svg";
import Premium_Badge from "../../assets/images/plans/Premium_Badge.svg";
import { FormattedMessage } from "react-intl";
import { clearBoardsData } from "./../../redux/actions/boards";
import { getCustomField } from "../../redux/actions/customFields";

class TeamsDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: null,
      openDialog: false,
      allTeams: [],
      loggedInTeam: {},
      workSettingDialog: false,
      selectedIndex: 0,
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    this.handleCreateTeam = this.handleCreateTeam.bind(this);
    this.selectedTeam = this.selectedTeam.bind(this);
  }
  componentDidMount() {
    const { profileState } = this.props;
    const loggedInTeam =
      profileState.data && profileState.data.teams
        ? profileState.data.teams.find(x => x.companyId === profileState.data.activeTeam)
        : {};
    this.setState({ loggedInTeam });
  }
  componentDidUpdate(prevProps, prevState) {
    const { loggedInTeam } = this.state;
    let prevTeamObj = prevProps.profileState.data.teams.find(t => {
      return t.companyId == this.props.profileState.data.activeTeam;
    });
    let newTeamObj = this.props.profileState.data.teams.find(t => {
      return t.companyId == this.props.profileState.data.activeTeam;
    });

    if (JSON.stringify(prevTeamObj) !== JSON.stringify(newTeamObj)) {
      const updatedTeam =
        this.props.profileState.data && this.props.profileState.data.teams
          ? this.props.profileState.data.teams.find(
              x => x.companyId === this.props.profileState.data.activeTeam
            )
          : {};
      this.setState({ loggedInTeam: updatedTeam });
    }
  }
  selectedTeam(teamItem, params) {
    this.setState({ open: false });
    this.props.showLoadingState();
    this.handleClose(params);
    // $.connection.hub.stop();
    switchTeam(teamItem.companyId, response => {
      if (teamItem.isEnableWhiteLabeling && teamItem.isEnableWhiteLabeling == true)
        this.props.whiteLabelInfo(teamItem.companyId);
      else this.props.DefaultWhiteLabelInfo();
      this.props.history.push(`/teams/${response.data.team.companyUrl}`);
      this.props.clearBoardsData(false);
      this.props.FetchUserInfo(() => {
        const { gracePeriod } = this.props;
        const { paymentPlanTitle, currentPeriondEndDate } = response.data.team.subscriptionDetails;
        if (
          response.data.team &&
          response.data.team.isStatusMappingRequired &&
          ConstantsPlan.DisplayPlanName(paymentPlanTitle) !== "Business"
        ) {
          this.props.showLoadingState();
          this.props.planExpire("StatusMapping");
        }
        if (!ConstantsPlan.isPlanFree(paymentPlanTitle)) {
          if (
            ConstantsPlan.isPlanTrial(paymentPlanTitle) &&
            ConstantsPlan.isPlanExpire(currentPeriondEndDate)
          ) {
            this.props.showLoadingState();
            this.props.planExpire("Trial");
            return;
          } else if (
            ConstantsPlan.isPlanPaid(paymentPlanTitle) &&
            ConstantsPlan.isPlanExpire(currentPeriondEndDate, gracePeriod)
          ) {
            this.props.showLoadingState();
            this.props.planExpire("Paid");
            return;
          }
        }
        /** fetching all user created custom fields */
        this.props.getCustomField(
          succ => {},
          fail => {}
        );
        this.props.hideLoadingState();
      });
    });
  }
  handleCreateTeam(event) {
    this.setState({ openDialog: true, open: false });
  }
  handleDialogClose() {
    this.setState({ openDialog: false });
  }
  goToTeamSetting = () => {
    this.setState({ open: false });
    this.props.history.push("/team-settings");
  };
  goToUserManagement = () => {
    this.setState({ open: false });
    this.props.history.push({
      pathname: "/team-settings",
      state: "inviteMember",
    });
  };
  handleClose(event) {
    if (!event || !event.target.closest("#menu-list-grow")) {
      this.setState({ open: false });
    }
  }
  openTeamDashBoard = () => {
    this.props.projectDetails({
      projectDetails: {},
      projectSettings: {},
      fullView: true,
      docked: false,
      dialog: false,
      loadingState: false,
    });
    let activeTeam = this.props.profileState.data.teams.find(t => {
      return t.companyId == this.props.profileState.data.activeTeam;
    });
    // checks if there is no active workspace and redirects to active team url
    this.props.history.push(`/teams/${activeTeam.companyUrl}`);
  };
  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(
      state => ({
        open: state.placement !== placement || !state.open,
        placement,
      }),
      () => {
        this.props.projectDetails({
          projectDetails: {},
          projectSettings: {},
          fullView: true,
          docked: false,
          dialog: false,
          loadingState: false,
        });
      }
    );
  }
  handleListItemClick = (event, index) => {
    this.setState({ selectedIndex: index });
  };

  getPlanImage = plan => {
    if (ConstantsPlan.isPlanFree(plan)) {
      return Basic;
    } else if (ConstantsPlan.isPlanTrial(plan)) {
      return Basic;
    } else if (ConstantsPlan.isPremiumPlan(plan)) {
      return Premium;
    } else if (ConstantsPlan.isBusinessPlan(plan)) {
      return Business;
    }
  };
  getPlanLabel = plan => {
    if (ConstantsPlan.isPlanFree(plan)) {
      return Basic_Badge;
    } else if (ConstantsPlan.isPlanTrial(plan)) {
      return Business_Badge;
    }
    if (ConstantsPlan.isPremiumPlan(plan)) {
      return Premium_Badge;
    } else if (ConstantsPlan.isBusinessPlan(plan)) {
      return Business_Badge;
    }
  };
  getSubscriptionDetails = () => {
    const { allteams, activeTeam } = this.props;
    if (activeTeam) {
      const subscriptionDetails = allteams.find(t => t.companyId == activeTeam).subscriptionDetails;
      return subscriptionDetails;
    } else {
      return {};
    }
  };

  render() {
    const { classes, theme, profileState } = this.props;
    const subscriptionDetails = this.getSubscriptionDetails();
    const { paymentPlanTitle } = subscriptionDetails;
    const { workSettingDialog, selectedIndex } = this.state;
    let teams = profileState.data.teams || [];
    // let showPlanLbl = ConstantsPlan.isPlanPaid(paymentPlanTitle);
    let selectedTeam = {};
    let allTeams = teams.filter(x => {
      if (x.companyId === profileState.data.activeTeam) {
        selectedTeam = x;
      }
      return x.companyId !== this.props.profileState.data.activeTeam;
    });
    // const nonPersonalWorkspace = profileState.data.teams.find(t => {
    //   return t.companyId !== "000000";
    // });
    let currentUser = profileState.data.teamMember.find(m => {
      return profileState.data.userId == m.userId;
    });
    return (
      <Fragment>
        {/* {profileState.data.loggedInTeam === "000000" ? (
          <MoveWorkspaceData
            switchToWorkspaceId={nonPersonalWorkspace.companyId}
            switchWorkspace={this.selectedTeam}
          />
        ) : null} */}
        <DefaultDialog
          title={
            <FormattedMessage
              id="team-settings.team-creation-dialog.title"
              defaultMessage="Create Team"
            />
          }
          sucessBtnText="Create Team"
          open={this.state.openDialog}
          onClose={this.handleDialogClose}>
          <CreateTeamForm handleDialogClose={this.handleDialogClose} />
          {/* <AddWorkspaceForm closeDialog={this.handleDialogClose} /> */}
        </DefaultDialog>
        <Button
          onClick={event => {
            this.openTeamDashBoard(event);
          }}
          classes={{
            root: classes.headerLeftBtns,
            text: classes.headerLeftBtnsText,
          }}
          disableRipple={true}
          style={{ padding: "2px 0px 2px 16px" }}>
          <div
            style={{ display: "flex", padding: "7px 5px 7px 0" }}
            ref={node => {
              this.anchorEl = node;
            }}>
            <CustomAvatar userActiveTeam size="small" styles={{ marginRight: 7 }} />
            <div className={classes.teamDropdownHeadingCnt}>
              <Typography variant="body2" className={classes.teamDropdownHeading}>
                <FormattedMessage id="common.team.label" defaultMessage="Team" />
              </Typography>
              <div className={classes.menuButtonText} style={{ display: "flex" }}>
                <div
                  title={selectedTeam.companyName}
                  style={{
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    fontWeight: theme.typography.fontWeightMedium,
                  }}>
                  {selectedTeam.companyName || ""}
                </div>
                {/* {showPlanLbl && (
                  <span title={paymentPlanTitle} className={classes.planLbl}>
                    {paymentPlanTitle.charAt(0)}
                  </span>
                )} */}
              </div>
            </div>
          </div>
        </Button>

        <Button
          // buttonRef={node => {
          //   this.anchorEl = node;
          // }}
          style={{ padding: 0, minWidth: 34, borderRadius: 0 }}
          onClick={event => {
            this.handleClick(event, "bottom-start");
          }}
          className={classes.teamDDArrow}
          disableRipple={true}>
          {/* <img
            src={BreadCrumbArrow}
            style={{
              position: "absolute",
              right: -26,
              top: 0,
              zIndex: 5,
              height: 68
            }}
          />
          <DropdownArrow className={classes.dropdownArrow} /> */}

          {/* <div className={classes.teamDDArrow}></div> */}
        </Button>

        <PlainMenu
          open={this.state.open}
          style={{ minWidth: 250, maxWidth: 480, width: "auto" }}
          closeAction={this.handleClose}
          placement={this.state.placement}
          anchorRef={this.anchorEl}
          list={
            <>
              <MenuList disablePadding={true} classes={{ root: classes.workspaceMenuList }}>
                {/* <MenuItem
                  classes={{ root: classes.menuHeadingWorkName }}
                  disableRipple={true}
                  disabled={true}
                >
                  <span className={classes.wsMenuHeadingText}>
                    {selectedTeam.companyName}
                  </span>
                </MenuItem> */}
                {/* //TODO will comment later for teams permission */}
                {/* {permissions && permissions.accessSetting ? ( */}
                {(currentUser.isOwner || currentUser.role == "Admin") && (
                  <Fragment>
                    <MenuItem
                      onClick={this.goToUserManagement}
                      className={classes.highlightItemTeamWork}
                      disableRipple={true}>
                      <GroupIcon
                        htmlColor={theme.palette.secondary.light}
                        classes={{ root: classes.menuItemIcon }}
                        style={{ fontSize: "24px" }}
                      />
                      <FormattedMessage
                        id="team-settings.user-management.label"
                        defaultMessage="User Management"
                      />
                    </MenuItem>
                    <MenuItem
                      onClick={this.goToTeamSetting}
                      className={classes.highlightItemTeamWork}
                      disableRipple={true}>
                      <SettingIcon
                        htmlColor={theme.palette.secondary.light}
                        classes={{ root: classes.menuItemIcon }}
                        style={{ fontSize: "24px" }}
                      />
                      <FormattedMessage id="team-settings.title" defaultMessage="Team Settings" />
                    </MenuItem>
                  </Fragment>
                )}
                {/* ) : null} */}
                <MenuItem
                  onClick={this.handleCreateTeam}
                  className={classes.highlightItem}
                  disableRipple={true}
                  style={{ background: theme.palette.background.airy }}>
                  <AddCircleIcon
                    htmlColor={theme.palette.background.green}
                    classes={{ root: classes.menuItemIcon }}
                    style={{ fontSize: "24px" }}
                  />
                  <FormattedMessage
                    id="common.team.new-team.label"
                    defaultMessage="Create New Team"
                  />
                </MenuItem>
                {allTeams.length > 0 && (
                  <MenuItem classes={{ root: classes.menuHeading }} disableRipple={true}>
                    <FormattedMessage id="common.team.all-teams.label" defaultMessage="All Teams" />
                  </MenuItem>
                )}
              </MenuList>
              {allTeams.length > 0 && (
                <Scrollbars autoHide autoHeight autoHeightMax={200}>
                  <MenuList>
                    {allTeams.map(x => {
                      return (
                        <MenuItem
                          onClick={event => this.selectedTeam(x)}
                          classes={{ root: classes.menuWorkspaceItem }}
                          disableRipple={true}
                          key={x.companyId}>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                              width: "100%",
                            }}>
                            <div
                              className={classes.menuItemInner}
                              style={{ justifyContent: "flex-start" }}>
                              <CustomAvatar
                                styles={{ marginRight: 10 }}
                                otherTeam={x}
                                size="xsmall"
                              />
                              {/* <div className={classes.teamNameCnt}> */}
                              <span
                                title={x.companyName || "Un Named"}
                                className={classes.menuItemName}>
                                {x.companyName || "Un Named"}
                              </span>
                              {/* <span className={classes.teamOwnerName}>
                                  {this.getUser(x.createdBy) &&
                                    this.getUser(x.createdBy).fullName}
                                </span>
                              </div> */}
                            </div>
                            {!ConstantsPlan.isPlanTrial(x.subscriptionDetails.paymentPlanTitle) && (
                              <div
                                style={{
                                  minWidth: 60,
                                  display: "flex",
                                  justifyContent: "flex-end",
                                  alignItems: "center",
                                }}>
                                <img
                                  src={this.getPlanImage(x.subscriptionDetails.paymentPlanTitle)}
                                  onMouseEnter={e => {
                                    e.currentTarget.src = this.getPlanLabel(
                                      x.subscriptionDetails.paymentPlanTitle
                                    );
                                  }}
                                  onMouseOut={e => {
                                    e.currentTarget.src = this.getPlanImage(
                                      x.subscriptionDetails.paymentPlanTitle
                                    );
                                  }}
                                />
                              </div>
                              // <span
                              //   title={x.subscriptionDetails.paymentPlanTitle}
                              //   className={classes.planLbl}
                              // >
                              //   {x.subscriptionDetails.paymentPlanTitle}
                              // </span>
                            )}
                          </div>
                        </MenuItem>
                      );
                    })}
                  </MenuList>
                </Scrollbars>
              )}
            </>
          }
          menuType="workspace"
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    allteams: state.profile.data.teams,
    activeTeam: state.profile.data.activeTeam,
    gracePeriod: state.profile.data.gracePeriod,
  };
};

export default compose(
  withRouter,
  withStyles(combinedStyles(menuStyles, headerStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    FetchTasksInfo,
    FetchWorkspaceInfo,
    FetchUserInfo,
    whiteLabelInfo,
    DefaultWhiteLabelInfo,
    clearBoardsData,
    projectDetails,
    getCustomField
  })
)(TeamsDropdown);
