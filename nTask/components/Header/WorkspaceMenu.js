import React, { Component, Fragment } from "react";
import DefaultTextField from "../../components/Form/TextField";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import PlainMenu from "../Menu/menu";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import menuStyles from "../../assets/jss/components/menu";
import combinedStyles from "../../utils/mergeStyles";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import DefaultDialog from "../Dialog/Dialog";
import AddWorkspaceForm from "./addWorkSpaceForm";
import { FetchTasksInfo } from "./../../redux/actions/tasks";
import { FetchWorkspaceInfo } from "./../../redux/actions/workspace";
import { clearAllAppliedFilters } from "./../../redux/actions/appliedFilters";
import { nTaskSignalConnectionOpen } from "../../utils/ntaskSignalR";

import { FetchUserInfo } from "./../../redux/actions/profile";
import { Scrollbars } from "react-custom-scrollbars";
import headerStyles from "./styles";
// import WorkspaceSetting from "../WorkspaceSetting/WorkspaceSetting";
import SettingIcon from "@material-ui/icons/Settings";
import GroupIcon from "@material-ui/icons/Group";
import CustomAvatar from "../Avatar/Avatar";
import MoveWorkspaceData from "../Dialog/NewWorkspaceDialog/MoveWorkspaceData";
import BreadCrumbArrow from "../../assets/images/arrow_breadCrum.png";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import Typography from "@material-ui/core/Typography";
import { GetPermission } from "../permissions";
import { FormattedMessage } from "react-intl";
import { clearBoardsData } from "./../../redux/actions/boards";
import { projectDetails } from "./../../redux/actions/projects";
import { getCustomField } from "../../redux/actions/customFields";
import { getTasks } from "../../redux/actions/tasks";

class WorkspaceDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: null,
      openDialog: false,
      allWorkspaces: [],
      loggedInTeam: {},
      workSettingDialog: false,
      selectedIndex: 0,
      workspaceSearchMenuValue: "",
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    this.handleCreateWorkspace = this.handleCreateWorkspace.bind(this);
    this.selectedWorkspace = this.selectedWorkspace.bind(this);
  }
  componentDidMount() {
    const { profileState } = this.props;
    const loggedInTeam =
      profileState.data && profileState.data.workspace
        ? profileState.data.workspace.find(x => x.teamId === profileState.data.loggedInTeam)
        : {};
    this.setState({ loggedInTeam });
  }

  selectedWorkspace(selectedLoggedInTeam, params) {
    this.setState({ open: false });
    this.props.showLoadingState();
    let loggedInTeamId = selectedLoggedInTeam || this.props.profileState.data.loggedInTeam;
    this.handleClose(params);
    $.connection.hub.stop();
    this.props.FetchWorkspaceInfo(loggedInTeamId, () => {
      /** fetching all user created custom fields */
      this.props.getTasks(null, null, ()=>{},()=>{});
      this.props.getCustomField(
        succ => {},
        fail => {}
      );
      this.props.history.push("/tasks");
      this.props.FetchUserInfo(() => {
        nTaskSignalConnectionOpen(this.props.profileState.data.userId);
        this.props.clearAllAppliedFilters(null);
        this.props.hideLoadingState();
        this.props.clearBoardsData(false);
      });
    });
  }
  handleCreateWorkspace(event) {
    this.setState({ openDialog: true, open: false });
  }
  handleDialogClose() {
    this.setState({ openDialog: false });
  }
  goToWorkspaceSetting = () => {
    this.setState({ open: false });
    this.props.history.push("/workspace-settings");
  };
  goToUserManagement = () => {
    this.setState({ open: false });
    this.props.history.push({
      pathname: "/workspace-settings",
      state: "inviteMember",
    });
  };
  handleClose(event) {
    if (!event || !event.target.closest("#menu-list-grow")) {
      this.setState({ open: false });
    }
  }
  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(
      state => ({
        open: state.placement !== placement || !state.open,
        placement,
      }),
      () => {
        this.props.projectDetails({
          projectDetails: {},
          projectSettings: {},
          fullView: true,
          docked: false,
          dialog: false,
          loadingState: false,
        });
      }
    );
  }
  openWorkspaceDashBoard = () => {
    this.props.projectDetails({
      projectDetails: {},
      projectSettings: {},
      fullView: true,
      docked: false,
      dialog: false,
      loadingState: false,
    });
    this.props.history.push(`/tasks`);
  };
  handleListItemClick = (event, index) => {
    this.setState({ selectedIndex: index });
  };
  getPermission = key => {
    /** function calling for getting permission */
    const { workspacePermissionsState, profileState } = this.props;
    let currentUser = profileState.data.teamMember.find(m => {
      /** finding current user from team members array */
      return m.userId == profileState.data.userId;
    });
    // return GetPermission.canDo(currentUser.isOwner, workspacePermissionsState.data.workSpace || {}, key) /** passing current user owner , workspace permission and a key to check */
    return true;
  };
  handleWorkspaceMenuChange = e => {
    this.setState({ workspaceSearchMenuValue: e.target.value });
  };
  render() {
    const { classes, theme, profileState, workspacePermissionsState, workSpacePer } = this.props;
    const { workSettingDialog, selectedIndex } = this.state;
    const permissions = workspacePermissionsState.data
      ? workspacePermissionsState.data.workSpace
      : "";
    let teams = profileState.data ? profileState.data.workspace : [];
    let selectedWorkspace = {};
    let allWorkspaces = teams.filter(x => {
      if (x.teamId === profileState.data.loggedInTeam) {
        selectedWorkspace = x;
      }
      return x.teamId !== this.props.profileState.data.loggedInTeam;
    });
    let filterData = allWorkspaces.filter(data =>
      data.teamName.toLowerCase().includes(this.state.workspaceSearchMenuValue.toLowerCase())
    );
    // const nonPersonalWorkspace = profileState.data.workspace.find(t => {
    //   return t.teamId !== "000000";
    // });

    return (
      <React.Fragment>
        {/* {profileState.data.loggedInTeam === "000000" ? (
          <MoveWorkspaceData
            switchToWorkspaceId={nonPersonalWorkspace.teamId}
            switchWorkspace={this.selectedWorkspace}
          />
        ) : null} */}
        <DefaultDialog
          title={
            <FormattedMessage
              id="workspace-settings.create-workspace.label"
              defaultMessage="Create Workspace"
            />
          }
          sucessBtnText="Create Workspace"
          open={this.state.openDialog}
          onClose={this.handleDialogClose}>
          <AddWorkspaceForm closeDialog={this.handleDialogClose} />
        </DefaultDialog>
        <Button
          // buttonRef={node => {
          //   this.anchorEl = node;
          // }}
          onClick={event => {
            this.openWorkspaceDashBoard(event);
          }}
          classes={{
            root: classes.headerLeftBtns,
            text: classes.headerLeftBtnsText,
          }}
          disableRipple={true}
          style={{ padding: "2px 0px 2px 31px", marginLeft: "-15px" }}>
          <div
            style={{ display: "flex", padding: "7px 5px 7px 0px" }}
            ref={node => {
              this.anchorEl = node;
            }}>
            <CustomAvatar loggedInTeam size="small" styles={{ marginRight: 7 }} />
            <div className={classes.teamDropdownHeadingCnt}>
              <Typography variant="body2" className={classes.teamDropdownHeading}>
                <FormattedMessage id="workspace-settings.label3" defaultMessage="Workspace" />
              </Typography>
              <span
                title={selectedWorkspace.teamName}
                className={classes.menuButtonText}
                style={{ fontWeight: theme.typography.fontWeightMedium }}>
                {selectedWorkspace.teamName || ""}
              </span>
            </div>
          </div>
          {/* <img
            src={BreadCrumbArrow}
            style={{ position: "absolute", right: -26, top: 0, zIndex: 5, height: 68 }}
          />
          <DropdownArrow className={classes.dropdownArrow} /> */}
        </Button>
        <Button
          style={{ padding: 0, minWidth: 34, borderRadius: 0 }}
          onClick={event => {
            this.handleClick(event, "bottom-start");
          }}
          className={classes.teamDDArrow}
          disableRipple={true}>
          {/* <img
              className={classes.teamDDArrow}
          /> */}
        </Button>

        <PlainMenu
          open={this.state.open}
          style={{ minWidth: 250, maxWidth: 480, width: "auto" }}
          closeAction={this.handleClose}
          placement={this.state.placement}
          anchorRef={this.anchorEl}
          list={
            <>
              <MenuList disablePadding={true} classes={{ root: classes.workspaceMenuList }}>
                {/* <MenuItem
                  classes={{ root: classes.menuHeadingWorkName }}
                  disableRipple={true}
                  disabled={true}
                >
                  <span className={classes.wsMenuHeadingText}>
                    {selectedWorkspace.teamName}
                  </span>
                </MenuItem> */}
                {workSpacePer.workspaceSetting && workSpacePer.workspaceSetting.cando ? (
                  <Fragment>
                    {workSpacePer.workspaceSetting.usersManagement.cando && (
                      <MenuItem
                        onClick={this.goToUserManagement}
                        className={classes.highlightItemTeamWork}
                        disableRipple={true}>
                        <GroupIcon
                          htmlColor={theme.palette.secondary.light}
                          classes={{ root: classes.menuItemIcon }}
                          style={{ fontSize: "24px" }}
                        />
                        <FormattedMessage
                          id="workspace-settings.user-management.label"
                          defaultMessage="User Management"
                        />
                      </MenuItem>
                    )}
                    {workSpacePer.workspaceSetting && workSpacePer.workspaceSetting.cando && (
                      <MenuItem
                        onClick={this.goToWorkspaceSetting}
                        className={classes.highlightItemTeamWork}
                        disableRipple={true}>
                        <SettingIcon
                          htmlColor={theme.palette.secondary.light}
                          classes={{ root: classes.menuItemIcon }}
                          style={{ fontSize: "24px" }}
                        />
                        <FormattedMessage
                          id="workspace-settings.label"
                          defaultMessage="Workspace Settings"
                        />
                      </MenuItem>
                    )}
                  </Fragment>
                ) : null}
                {/* workSpacePer.createWorkspaceName.cando */}
                {true && (
                  <MenuItem
                    onClick={this.handleCreateWorkspace}
                    className={classes.highlightItem}
                    disableRipple={true}
                    style={{ background: theme.palette.background.airy }}>
                    <AddCircleIcon
                      htmlColor={theme.palette.background.green}
                      classes={{ root: classes.menuItemIcon }}
                      style={{ fontSize: "24px" }}
                    />
                    <FormattedMessage
                      id="workspace-settings.create-workspace.label"
                      defaultMessage="Create Workspace"
                    />
                  </MenuItem>
                )}
                <div className={classes.workspaceSearchCnt}>
                  <DefaultTextField
                    fullWidth={false}
                    // errorState={forgotEmailError}
                    error={false}
                    // errorMessage={forgotEmailMessage}
                    formControlStyles={{ width: 250, marginBottom: 0 }}
                    defaultProps={{
                      id: "workspaceMenuSearch",
                      onChange: this.handleWorkspaceMenuChange,
                      value: this.state.workspaceSearchMenuValue,
                      placeholder: "Search Workspaces",
                      autoFocus: true,
                      inputProps: { maxLength: 50, style: { padding: "10px 14px" } },
                    }}
                  />
                </div>
                {allWorkspaces.length && (
                  <MenuItem classes={{ root: classes.menuHeading }} disableRipple={true}>
                    <FormattedMessage
                      id="workspace-settings.all-workspaces.label"
                      defaultMessage="All Workspaces"
                    />
                  </MenuItem>
                )}
              </MenuList>
              {allWorkspaces.length > 0 && (
                <Scrollbars autoHide autoHeight autoHeightMax={200}>
                  <MenuList disablePadding>
                    {filterData.map(x => {
                      return x.companyId ? (
                        <MenuItem
                          onClick={x.isActive ? event => this.selectedWorkspace(x.teamId) : null}
                          classes={{ root: classes.menuWorkspaceItem }}
                          disableRipple={true}
                          key={x.teamId}>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                            }}>
                            <div
                              className={classes.menuItemInner}
                              style={{ justifyContent: "flex-start" }}>
                              <CustomAvatar
                                styles={{ marginRight: 10 }}
                                otherWorkspace={{
                                  teamName: x.teamName,
                                  pictureUrl: x.pictureUrl,
                                  baseUrl: x.imageBasePath,
                                }}
                                size="xsmall"
                              />
                              <span
                                title={x.teamName || "Un Named"}
                                className={classes.menuItemName}>
                                {x.teamName || "Un Named"}
                              </span>
                            </div>
                            {x.isActive == false && (
                              <span title={"Block"} className={classes.blockWorkspaceLbl}>
                                {"Blocked"}
                              </span>
                            )}
                          </div>
                        </MenuItem>
                      ) : null;
                    })}
                  </MenuList>
                </Scrollbars>
              )}
            </>
          }
          menuType="workspace"
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    workspacePermissionsState: state.workspacePermissions,
    workSpacePer: state.workspacePermissions.data.workSpace,
  };
};

export default compose(
  withRouter,
  withStyles(combinedStyles(menuStyles, headerStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    FetchTasksInfo,
    FetchWorkspaceInfo,
    clearAllAppliedFilters,
    FetchUserInfo,
    clearBoardsData,
    projectDetails,
    getCustomField,
    getTasks
  })
)(WorkspaceDropdown);
