import React, { Component } from "react";
import PlainMenu from "../Menu/menu";
import buttonStyles from "../../assets/jss/components/button";
import { withStyles, withTheme } from "@material-ui/core/styles";
import { connect } from "react-redux";

import { compose } from "redux";

import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import DefaultButton from "../Buttons/DefaultButton";
import menuStyles from "../../assets/jss/components/menu";
import combinedStyles from "../../utils/mergeStyles";
import AddNewTaskForm from "../../Views/AddNewForms/addNewTaskFrom";
import DefaultDialog from "../Dialog/Dialog";
import AddNewProject from "../../Views/AddNewForms/AddNewProjectForm";
import AddIssueForm from "../../Views/AddNewForms/AddNewIssueForm";
import AddRiskForm from "../../Views/AddNewForms/AddNewRiskForm";
import AddMeetingForm from "../../Views/AddNewForms/AddMeetingForm";

import SvgIcon from "@material-ui/core/SvgIcon";

import TaskIcon from "../Icons/TaskIcon";
import MeetingsIcon from "../Icons/MeetingIcon";
import ProjectsIcon from "../Icons/ProjectIcon";
import TimesheetIcon from "../Icons/TimesheetIcon";
import IssuesIcon from "../Icons/IssueIcon";
import RiskIcon from "../Icons/RiskIcon";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import CustomButton from "../Buttons/CustomButton";
import { teamCanView } from "../PlanPermission/PlanPermission";
import { FormattedMessage, injectIntl } from "react-intl";
import NewProject from "../../Views/Project/ProjectDetails/ProjectDetails";
import CustomDialog from "../Dialog/CustomDialog";
import CreateNewProject from "../../Views/Project/CreateNewProject/CreateNewProject";
import IconAdd from "../Icons/ProjectAddMoreIcons/IconAdd";
import CustomListItem from "../ListItem/CustomListItem";
import CustomMenuList from "../MenuList/CustomMenuList";

class AddNewMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placement: null,
      open: false,
      projectDialog: false,
      taskDialog: false,
      issueDialog: false,
      riskDialog: false,
      meetingDialog: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleDialog = this.handleDialog.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }
  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }

  handleClose(event) {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  }
  handleDialogClose(event, name) {
    this.setState({ [name]: false });
  }

  handleDialog(event, name) {
    this.setState({ [name]: true, open: false });
  }
  render() {
    const {
      classes,
      theme,
      projectPer,
      taskPer,
      meetingPer,
      riskPer,
      issuePer,
      intl,
      newBtnProps = {},
      companyInfo,
    } = this.props;
    const { projectDialog, taskDialog, issueDialog, meetingDialog, riskDialog, open } = this.state;
    const taskLabelSingle = companyInfo?.task?.sName;
    const meetingAccess = companyInfo?.meeting?.isHidden ? false : true;
    const issueAccess = companyInfo?.issue?.isHidden ? false : true;
    const riskAccess = companyInfo?.risk?.isHidden ? false : true;
    const projectAccess = companyInfo?.project?.isHidden ? false : true;
    const AddNewMenuList = props => {
      return (
        <CustomMenuList style={{ padding: "4px 0" }}>
          {taskPer.createTask && taskPer.createTask.cando && (
            <CustomListItem
              rootProps={{
                className: classes.plainMenuItem,
                onClick: event => this.handleDialog(event, "taskDialog"),
              }}
              textProps={{ className: classes.menuItemStyle }}>
              <div>
                <SvgIcon viewBox="0 0 18 15.188" className={classes.plainMenuItemIcon}>
                  <TaskIcon />
                </SvgIcon>
                {taskLabelSingle || "Task"}
              </div>
              <div className={classes.keyShortcut}>T</div>
            </CustomListItem>
          )}
          {teamCanView("projectAccess") && projectPer.createProject.cando && projectAccess && (
            <CustomListItem
              rootProps={{
                className: classes.plainMenuItem,
                onClick: event => this.handleDialog(event, "projectDialog"),
              }}
              textProps={{ className: classes.menuItemStyle }}>
              <div>
                <SvgIcon viewBox="0 0 18 15.188" className={classes.plainMenuItemIcon}>
                  <ProjectsIcon />
                </SvgIcon>
                <FormattedMessage
                  id="header.add-new-button.list.project.label"
                  defaultMessage="Project"
                />
              </div>
              <div className={classes.keyShortcut}>P</div>
            </CustomListItem>
          )}
          {meetingPer.createMeeting.cando && meetingAccess && (
            <CustomListItem
              rootProps={{
                className: classes.plainMenuItem,
                onClick: event => this.handleDialog(event, "meetingDialog"),
              }}
              textProps={{ className: classes.menuItemStyle }}>
              <div>
                <SvgIcon viewBox="0 0 17.031 17" className={classes.plainMenuItemIcon}>
                  <MeetingsIcon />
                </SvgIcon>
                <FormattedMessage
                  id="header.add-new-button.list.meeting.label"
                  defaultMessage="Meeting"
                />
              </div>
              <div className={classes.keyShortcut}>M</div>
            </CustomListItem>
          )}
          {issuePer.creatIssue.cando && issueAccess && (
            <CustomListItem
              rootProps={{
                className: classes.plainMenuItem,
                onClick: event => this.handleDialog(event, "issueDialog"),
              }}
              textProps={{ className: classes.menuItemStyle }}>
              <div>
                <SvgIcon viewBox="0 0 16 17.375" className={classes.plainMenuItemIcon}>
                  <IssuesIcon />
                </SvgIcon>
                <FormattedMessage
                  id="header.add-new-button.list.issue.label"
                  defaultMessage="Issue"
                />
              </div>
              <div className={classes.keyShortcut}>I</div>
            </CustomListItem>
          )}
          {teamCanView("riskAccess") && riskPer.createRisk.cando && riskAccess && (
            <CustomListItem
              rootProps={{
                className: classes.plainMenuItem,
                onClick: event => this.handleDialog(event, "riskDialog"),
              }}
              textProps={{ className: classes.menuItemStyle }}>
              <div>
                <SvgIcon viewBox="0 0 18 15.75" className={classes.plainMenuItemIcon}>
                  <RiskIcon />
                </SvgIcon>
                <FormattedMessage
                  id="header.add-new-button.list.risk.label"
                  defaultMessage="Risk"
                />
              </div>
              <div className={classes.keyShortcut}>R</div>
            </CustomListItem>
          )}
        </CustomMenuList>
      );
    };
    return (
      <div>
        <DefaultDialog
          title={`Add ${taskLabelSingle || "Task"}`}
          open={taskDialog}
          onClose={event => this.handleDialogClose(event, "taskDialog")}>
          <AddNewTaskForm closeAction={event => this.handleDialogClose(event, "taskDialog")} />
        </DefaultDialog>
        <DefaultDialog
          title={<FormattedMessage id="issue.creation-dialog.title" defaultMessage="Add Issue" />}
          open={issueDialog}
          onClose={event => this.handleDialogClose(event, "issueDialog")}>
          <AddIssueForm
            view={"Header-Menu"}
            closeAction={event => this.handleDialogClose(event, "issueDialog")}
          />
        </DefaultDialog>

        <DefaultDialog
          title={<FormattedMessage id="risk.creation-dialog.title" defaultMessage="Add Risk" />}
          open={riskDialog}
          onClose={event => this.handleDialogClose(event, "riskDialog")}>
          <AddRiskForm
            view={"Header-Menu"}
            closeAction={event => this.handleDialogClose(event, "riskDialog")}
          />
        </DefaultDialog>

        <DefaultDialog
          title={
            <FormattedMessage
              id="meeting.creation-dialog.form.title"
              defaultMessage="Add Meeting"></FormattedMessage>
          }
          open={meetingDialog}
          onClose={event => this.handleDialogClose(event, "meetingDialog")}>
          <AddMeetingForm
            handleCloseMeeting={event => this.handleDialogClose(event, "meetingDialog")}
            isNew={true}
            isDialog={true}
            maxLength={250}
          />
        </DefaultDialog>
        {(taskPer.createTask && taskPer.createTask.cando) ||
        projectPer.createProject.cando ||
        meetingPer.createMeeting.cando ||
        issuePer.creatIssue.cando ||
        riskPer.createRisk.cando ? (
          <CustomButton
            btnType="success"
            variant="contained"
            style={{ borderRadius: 30, padding: "4px 11px" }}
            onClick={event => {
              this.handleClick(event, "bottom");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
            {...newBtnProps}>
            <SvgIcon
              classes={{ root: classes.addBtnIcon }}
              htmlColor={theme.palette.common.white}>
              <IconAdd />
            </SvgIcon>
            <div style={{ fontSize: "14px", fontFamily: "lato, sans-serif" }}> 
              <FormattedMessage id="header.add-new-button.label" defaultMessage="Add New" />
            </div>
          </CustomButton>
        ) : null}
        <PlainMenu
          style={{ width: 180, marginTop: 5 }}
          open={open}
          closeAction={this.handleClose}
          placement={this.props.placement ? this.props.placement : this.state.placement}
          anchorRef={this.anchorEl}
          list={<AddNewMenuList />}
        />
        {projectDialog && (
          <CreateNewProject
            open={projectDialog}
            handleDialogClose={this.handleDialogClose}
            intl={intl}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    taskPer: state.workspacePermissions.data.task,
    meetingPer: state.workspacePermissions.data.meeting,
    riskPer: state.workspacePermissions.data.risk,
    issuePer: state.workspacePermissions.data.issue,
    projectPer: state.workspacePermissions.data.project,
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  injectIntl,
  withStyles(combinedStyles(menuStyles, buttonStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {})
)(AddNewMenu);
