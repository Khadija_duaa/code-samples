import React from 'react';
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import classes from './styles';
import Typography from "@material-ui/core/Typography";

const paymentOverlay = (props) => {
    const { classes, theme } = props;
    return (<div id="payment" className={classes.paymentPending}>
        <div class="loader"></div>
        <Typography variant="h3" style={{marginTop: 20 }} >Your Payment is Processing ...</Typography>
        <Typography variant="h6" style={{marginTop: 8}}>Please wait! It may take a moment.</Typography> 
    </div>)
};
export default compose(
    withStyles(classes, { withTheme: true })
)(paymentOverlay);