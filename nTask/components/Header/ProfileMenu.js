import React, { Component, Fragment } from "react";
import Button from "@material-ui/core/Button";
import { withStyles, withTheme } from "@material-ui/core/styles";
import SignOutIcon from "@material-ui/icons/PowerSettingsNew";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Avatar from "@material-ui/core/Avatar";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { compose } from "redux";
import SvgIcon from "@material-ui/core/SvgIcon";
import PlayIcon from "@material-ui/icons/PlayCircleFilled";
import MessageIcon from "@material-ui/icons/Mail";
import FeedbackIcon from "@material-ui/icons/Feedback";
import PersonIcon from "@material-ui/icons/Person";
import FlagIcon from "@material-ui/icons/Flag";
import Typography from "@material-ui/core/Typography";
import { withSnackbar } from "notistack";
import Menu from "@material-ui/core/Menu";
import headerStyles from "./styles";
import { HeartIcon } from "../Icons/HeartShareIcon";
import FeedbackDialog from "../Dialog/FeedbackDialog";
import RecommendDialog from "../Dialog/RecommendDialog";
import { generateUsername } from "../../utils/common";
import CustomAvatar from "../Avatar/Avatar";
import InvitationsDialog from "../../Views/InvitationsDialog/InvitationsDialog";
import Badge from "@material-ui/core/Badge";
import RightArrow from "@material-ui/icons/ArrowRight";
import { clearGoogleCalendarState } from "../../redux/actions/googleCalendar";
import { FormattedMessage } from "react-intl";
import { ActivityIcon } from "../Icons/ActivityIcon";
import ProfileSetting from "../../Views/ProfileSetting/ProfileSetting";
import { Logout } from "../../redux/actions/logout";
import combinedStyles from "../../utils/mergeStyles";
import menuStyles from "../../assets/jss/components/menu";
import buttonStyles from "../../assets/jss/components/button";
import PlainMenu from "../Menu/menu";
import {closeProfileSetting} from '../../redux/actions/profile';

class HeaderProfileMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: null,
      openDialog: this.props.googleCalendar.googleIntegration,
      openFeedback: false,
      openRecommend: false,
      openInvitationDialog: false,
      guidedTourSubNavOpen: false,
      selectedTab: null
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleProfileSetting = this.handleProfileSetting.bind(this);
    this.handleDialogCloseAction = this.handleDialogCloseAction.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    this.handleDialogOpen = this.handleDialogOpen.bind(this);
    this.handleRecommendDialogClose = this.handleRecommendDialogClose.bind(this);
    this.handleRecommendDialogOpen = this.handleRecommendDialogOpen.bind(this);
    this.handleActivityLog = this.handleActivityLog.bind(this);
  }

  handleActivityLog() {
    this.props.history.push("/activity");
  }

  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }

  handleClose(event) {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false }, () => {
      this.props.clearGoogleCalendarState(false);
    });
  }

  handleDialogCloseAction() {
    this.setState({ openDialog: false }, () => {
      this.props.clearGoogleCalendarState(false);
      this.props.closeProfileSetting()
    });
  }

  handleProfileSetting() {
    this.setState({ openDialog: true });
  }

  handleLogout() {
    this.props.Logout("", this.props.history);
  }

  handleDialogClose() {
    this.setState({ openFeedback: false });
  }

  handleRecommendDialogClose() {
    this.setState({ openRecommend: false });
  }

  handleRecommendDialogOpen() {
    this.setState({ openRecommend: true });
  }

  handleInviteDialogOpen = () => {
    this.setState({ openInvitationDialog: true, open: false });
  };

  handleInviteDialogClose = () => {
    this.setState({ openInvitationDialog: false });
  };

  handleDialogOpen() {
    this.setState({ openFeedback: true });
  }

  showSnackBar = (snackBarMessage, closeDialog, type) => {
    const { classes, enqueueSnackbar } = this.props;
    if (closeDialog) {
      this.handleDialogCloseAction();
    }
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };

  shouldComponentUpdate(nextProps, nextState) {
    const prevprofileString = JSON.stringify(this.props);
    const nextProfileString = JSON.stringify(nextProps);
    const prevStateString = JSON.stringify(this.state);
    const nextStateString = JSON.stringify(nextState);

    if (
      prevprofileString !== nextProfileString ||
      prevStateString !== nextStateString || nextProps.profileSettingDialog !== nextState.openDialog
    ) {
      return true;
    }
    return false;
  }

  componentDidUpdate(prevProps, prevState) {
    const { props, state } = this;
    const { profileSettingDialog, googleCalendar } = props;
    if (state.open === false && state.guidedTourSubNavOpen === true) {
      // Incase the main profile menu is hidden it will close the submenu
      this.setState({ guidedTourSubNavOpen: false });
    }
    if (prevProps.googleCalendar.googleIntegration !== googleCalendar.googleIntegration) {
      this.setState({
        openDialog: googleCalendar.googleIntegration,
      });
    }
    if (profileSettingDialog && prevProps.profileSettingDialog !== this.state.openDialog) {
      this.setState({
        openDialog: profileSettingDialog,
      });
    }
  }

  handleGuidedTourMenu = event => {
    event.stopPropagation();
    this.setState({ guidedTourSubNavOpen: !this.state.guidedTourSubNavOpen });
  };

  handleGuidedTourMenuClose = () => {
    this.setState({ guidedTourSubNavOpen: false });
  };

  handleGuidedTour = (event, code) => {
    // When click on any guided tour from the list to run
    event.stopPropagation();
    this.setState({ open: false, guidedTourSubNavOpen: false });
    if (window.userpilot) {
      userpilot.trigger(code); // Triggering userpilot action
    }
  };

  render() {
    const { classes, theme, workspaceCheck, googleCalendar, companyInfo, profileSettingDialog } = this.props;
    const {
      firstName,
      fullName,
      lastName,
      email,
      userName,
      teamInvitationCount,
    } = this.props.profile.data;
    const { pictureBaseUrl } = this.props.constants.data;
    const {
      openDialog,
      openFeedback,
      openRecommend,
      openInvitationDialog,
      guidedTourSubNavOpen,
      selectedTab
    } = this.state;
    const name = companyInfo ? companyInfo.companyName : "nTask";
    return (
      <div>
        {openInvitationDialog && (
          <InvitationsDialog
            open={openInvitationDialog}
            closeAction={this.handleInviteDialogClose}
          />
        )}
        <ProfileSetting
          open={openDialog}
          closeAction={this.handleDialogCloseAction}
          showSnackBar={this.showSnackBar}
          googleCalendar={googleCalendar}
          zoom={profileSettingDialog}
          // selectedIndex={selectedTab}
        />
        <FeedbackDialog
          open={openFeedback}
          closeDialog={this.handleDialogClose}
          showSnackBar={this.showSnackBar}
        />
        {/*<RecommendDialog*/}
        {/*  open={openRecommend}*/}
        {/*  closeDialog={this.handleRecommendDialogClose}*/}
        {/*  showSnackBar={this.showSnackBar}*/}
        {/*/>*/}

        <Button
          onClick={event => {
            this.handleClick(event, "bottom-end");
          }}
          buttonRef={node => {
            this.anchorEl = node;
          }}
          classes={{
            root: classes.noPaddedButton,
            text: classes.noPaddedBtnText,
          }}
          disableRipple>
          {teamInvitationCount > 0 ? (
            <Badge
              badgeContent={teamInvitationCount}
              classes={{ badge: classes.profileAvatarBadge }}>
              <CustomAvatar personal size="small" />
            </Badge>
          ) : (
            <CustomAvatar personal size="small" />
          )}
        </Button>

        <PlainMenu
          open={this.state.open}
          closeAction={this.handleClose}
          placement={this.state.placement}
          anchorRef={this.anchorEl}
          style={{ width: 280 }}
          offset="0 15px">
          <MenuList disablePadding>
            <MenuItem className={classes.profileInfoDropdownCnt} disableRipple>
              <CustomAvatar personal size="large" hideBadge />
              <ul className={classes.profileInfoList}>
                <li className={classes.firstName}>{fullName}</li>
                <li>{email}</li>
              </ul>
            </MenuItem>
            <MenuItem
              onClick={this.handleProfileSetting}
              classes={{ root: classes.plainMenuItem }}
              style={{ marginTop: 15 }}>
              <PersonIcon
                htmlColor={theme.palette.secondary.light}
                classes={{ root: classes.plainMenuItemIcon }}
              />
              <FormattedMessage
                id="profile-settings-dialog.title"
                defaultMessage="Profile Settings"
              />
            </MenuItem>
            <MenuItem
              onClick={this.handleInviteDialogOpen}
              classes={{ root: classes.plainMenuItem }}
              style={{ display: "flex", justifyContent: "space-between" }}>
              <div style={{ display: "flex", alignItems: "center" }}>
                <MessageIcon
                  htmlColor={theme.palette.secondary.light}
                  classes={{ root: classes.plainMenuItemIcon }}
                />
                <FormattedMessage id="header.user-menu.invite.label" defaultMessage="Invitations" />
              </div>
              {teamInvitationCount > 0 && (
                <span className={classes.notificationCount}>{teamInvitationCount}</span>
              )}
            </MenuItem>
            {workspaceCheck() && (
              <MenuItem classes={{ root: classes.plainMenuItem }} onClick={this.handleActivityLog}>
                <SvgIcon viewBox="0 0 512 512" classes={{ root: classes.plainMenuItemIcon }}>
                  <path d={ActivityIcon} fill={theme.palette.secondary.light} />
                </SvgIcon>
                <FormattedMessage id="activity-log.label" defaultMessage="Activity Log" />
              </MenuItem>
            )}

            {/* <MenuItem
              button
              onClick={this.handleGuidedTourMenu}
              classes={{ root: classes.subMenuItem }}>
              {guidedTourSubNavOpen && (
                <div className={classes.subMenuCnt}>
                  <MenuList disablePadding>
                    <MenuItem
                      className={classes.plainMenuItem}
                      disableRipple
                      onClick={event => this.handleGuidedTour(event, "1588693494aWrq6403")}>
                      <FormattedMessage
                        id="header.user-menu.guided-tour.menus-list.create-task.label"
                        defaultMessage="Create a Task"
                      />
                    </MenuItem>
                    <MenuItem
                      className={classes.plainMenuItem}
                      disableRipple
                      onClick={event => this.handleGuidedTour(event, "1588694169gMpl9497")}>
                      <FormattedMessage
                        id="header.user-menu.guided-tour.menus-list.group-by.label"
                        defaultMessage="Task: Group By"
                      />
                    </MenuItem>
                    <MenuItem
                      className={classes.plainMenuItem}
                      disableRipple
                      onClick={event => this.handleGuidedTour(event, "1588694300xDur7637")}>
                      <FormattedMessage
                        id="header.user-menu.guided-tour.menus-list.quick-filter.label"
                        defaultMessage="Task: Quick Filters"
                      />
                    </MenuItem>
                    <MenuItem
                      className={classes.plainMenuItem}
                      disableRipple
                      onClick={event => this.handleGuidedTour(event, "1588694348zEhe8959")}>
                      <FormattedMessage
                        id="header.user-menu.guided-tour.menus-list.customize-list.label"
                        defaultMessage="Customize List View"
                      />
                    </MenuItem>
                  </MenuList>
                </div>
              )}
              <div className={classes.subNavMainMenuCnt}>
                <div style={{ display: "flex", alignItems: "center" }}>
                  <FlagIcon
                    htmlColor={theme.palette.secondary.light}
                    classes={{ root: classes.plainMenuItemIcon }}
                  />
                  <FormattedMessage
                    id="header.user-menu.guided-tour.label"
                    defaultMessage="Guided Tour"
                  />
                </div>
                <RightArrow
                  htmlColor={theme.palette.secondary.light}
                  classes={{ root: classes.submenuArrowBtn }}
                />
              </div>
            </MenuItem> */}

            <MenuItem onClick={this.handleDialogOpen} classes={{ root: classes.plainMenuItem }}>
              <FeedbackIcon
                htmlColor={theme.palette.secondary.light}
                classes={{ root: classes.plainMenuItemIcon }}
              />
              <FormattedMessage
                id="header.user-menu.send-feedback.label"
                defaultMessage="Send Feedback"
              />
            </MenuItem>
            <MenuItem onClick={this.handleLogout} classes={{ root: classes.plainMenuItem }}>
              <SignOutIcon
                htmlColor={theme.palette.secondary.light}
                classes={{ root: classes.plainMenuItemIcon }}
              />
              <FormattedMessage id="header.user-menu.sign-out.label" defaultMessage="Sign Out" />
            </MenuItem>
            {companyInfo ? null : (
              <MenuItem
                classes={{ root: classes.recommendMenuItem }}
                disableRipple
                // onClick={this.handleRecommendDialogOpen}
                 >
                <SvgIcon viewBox="0 0 512 512" classes={{ root: classes.recommendMenuIcon }}>
                  <path fill={theme.palette.error.main} d={HeartIcon.path1} />
                  <path fill={theme.palette.error.main} d={HeartIcon.path2} />
                </SvgIcon>
                <div>
                  <Typography variant="h5" classes={{ h5: classes.loveText }}>
                    <FormattedMessage
                      id="header.user-menu.love.label"
                      defaultMessage="Love using nTask?"
                    />
                  </Typography>
                  <Typography variant="subtitle2" classes={{ h5: classes.recommendText }}>
                    <FormattedMessage
                      id="header.user-menu.love.placeholder"
                      defaultMessage="Recommend it to a friend!"
                    />
                  </Typography>
                </div>
              </MenuItem>
            )}
          </MenuList>
        </PlainMenu>
      </div>
    );
  }
}
const mapDispatchToProps = {
  Logout,
  clearGoogleCalendarState,
  closeProfileSetting
};
function mapStateToProps(state) {
  return {
    profile: state.profile,
    constants: state.constants,
    googleCalendar: state.googleCalendar.data,
    companyInfo: state.whiteLabelInfo.data,
    profileSettingDialog: state.profile.data.openProfileSetting,
  };
}
export default compose(
  withRouter,
  withSnackbar,
  withStyles(combinedStyles(menuStyles, buttonStyles, headerStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, mapDispatchToProps)
)(HeaderProfileMenu);
