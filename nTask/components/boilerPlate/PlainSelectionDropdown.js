import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import workspaceSetting from "../styles";
import menuStyles from "../../../assets/jss/components/menu";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import CustomIconButton from "../../Buttons/IconButton";
import SelectionMenu from "../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RenameRoleDialog from "./RenameRoleDialog";
class PermissionOptionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "top-start",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      renameRoleDialogOpen: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
  }
  handleDialogOpen = () => {
    this.setState({ renameRoleDialogOpen: true });
  };
  handleDialogClose = () => {
    this.setState({ renameRoleDialogOpen: false });
  };
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }

  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }
  render() {
    const { classes, theme } = this.props;
    const { open, placement, renameRoleDialogOpen } = this.state;
    return (
        <ClickAwayListener mouseEvent="onMouseDown" touchEvent="onTouchStart" onClickAway={this.handleClose}>
          <div>
            <RenameRoleDialog
              open={renameRoleDialogOpen}
              closeAction={this.handleDialogClose}
            />
            <CustomIconButton
              btnType="condensed"
              onClick={event => {
                this.handleClick(event, "bottom-start");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}
            >
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px !important" }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              style={{ width: 200 }}
              anchorRef={this.anchorEl}
              list={
                <List disablePadding={true}>
                  <ListItem disableRipple className={classes.headingItem}>
                    <ListItemText
                      primary="Select Action"
                      classes={{ primary: classes.headingText }}
                    />
                  </ListItem>

                  <ListItem
                    button
                    disableRipple
                    classes={{ selected: classes.statusMenuItemSelected }}
                    onClick={this.handleDialogOpen}
                  >
                    <ListItemText
                      primary="Rename"
                      classes={{
                        primary: classes.statusItemText
                      }}
                    />
                  </ListItem>

                  <ListItem
                    button
                    disableRipple
                    classes={{ selected: classes.statusMenuItemSelected }}
                  // onClick={deleteMember}
                  >
                    <ListItemText
                      primary="Duplicate"
                      classes={{
                        primary: classes.statusItemText
                      }}
                    />
                  </ListItem>
                  <ListItem
                    button
                    disableRipple
                    classes={{ selected: classes.statusMenuItemSelected }}
                  // onClick={deleteMember}
                  >
                    <ListItemText
                      primary="Delete"
                      classes={{
                        primary: classes.statusItemText
                      }}
                    />
                  </ListItem>
                </List>
              }
            />
          </div>
        </ClickAwayListener>
    );
  }
}

export default withStyles(combineStyles(workspaceSetting, menuStyles), {
  withTheme: true
})(PermissionOptionDropdown);
