const environment = process.env.REACT_APP_DEFAULT_ENV || "LOCAL";
const API_VERSION = "v1";
let API_URL = `http://localhost:8080 ${API_VERSION}`;
let SITE_KEY_CAPTCHA = "6LcYwkwUAAAAAEWwT82GohbCwPpT4HnlmbgxZ3wR";
let GOOGLE_AUTH_CLIENT_ID = "302633878489-frpfjludkpmvt14sv9o8t9oehudlt81v.apps.googleusercontent.com";
let FACEBOOK_AUTH_CLIENT_ID = "281853372226895";
let LINKEDIN_AUTH_CLIENT_ID = "812mihblfdrvsg";
let WORKSPACE_URL = "https://app.ntaskmanager.com/";
switch (environment) {
    case "DEV":
        API_URL = "https://dev.naxxa.io";
        SITE_KEY_CAPTCHA = "6LcYwkwUAAAAAEWwT82GohbCwPpT4HnlmbgxZ3wR";
        GOOGLE_AUTH_CLIENT_ID = "275676491421-2kc5cl1rktpes79fkh4o50he79f23of4.apps.googleusercontent.com";
        FACEBOOK_AUTH_CLIENT_ID = "281853372226895";
        LINKEDIN_AUTH_CLIENT_ID = "81gx5etc5y001g";
        break;
    case "SQA":
        API_URL = "https://sqa.naxxa.io";
        SITE_KEY_CAPTCHA = "6LcYwkwUAAAAAEWwT82GohbCwPpT4HnlmbgxZ3wR";
        GOOGLE_AUTH_CLIENT_ID = "275676491421-2kc5cl1rktpes79fkh4o50he79f23of4.apps.googleusercontent.com";
        FACEBOOK_AUTH_CLIENT_ID = "281853372226895";
        LINKEDIN_AUTH_CLIENT_ID = "81gx5etc5y001g";
        break;
    case "UAT":
        API_URL = "https://uat.ntaskmanager.com";
        SITE_KEY_CAPTCHA = "6LcYwkwUAAAAAEWwT82GohbCwPpT4HnlmbgxZ3wR";
        GOOGLE_AUTH_CLIENT_ID = "275676491421-2kc5cl1rktpes79fkh4o50he79f23of4.apps.googleusercontent.com";
        FACEBOOK_AUTH_CLIENT_ID = "281853372226895";
        LINKEDIN_AUTH_CLIENT_ID = "81gx5etc5y001g";
        break;
    case "STG":
        API_URL = "https://stg.ntaskmanager.com";
        SITE_KEY_CAPTCHA = "6LcYwkwUAAAAAEWwT82GohbCwPpT4HnlmbgxZ3wR";
        GOOGLE_AUTH_CLIENT_ID = "275676491421-2kc5cl1rktpes79fkh4o50he79f23of4.apps.googleusercontent.com";
        FACEBOOK_AUTH_CLIENT_ID = "281853372226895";
        LINKEDIN_AUTH_CLIENT_ID = "81gx5etc5y001g";
        break;
    case "PRD":
        API_URL = "https://prd.ntaskmanager.com";
        SITE_KEY_CAPTCHA = "6LcYwkwUAAAAAEWwT82GohbCwPpT4HnlmbgxZ3wR";
        GOOGLE_AUTH_CLIENT_ID = "275676491421-2kc5cl1rktpes79fkh4o50he79f23of4.apps.googleusercontent.com";
        FACEBOOK_AUTH_CLIENT_ID = "281853372226895";
        LINKEDIN_AUTH_CLIENT_ID = "81gx5etc5y001g";
        break;
    default:
        API_URL = "https://dev.naxxa.io";
        SITE_KEY_CAPTCHA = "6LcYwkwUAAAAAEWwT82GohbCwPpT4HnlmbgxZ3wR";
        GOOGLE_AUTH_CLIENT_ID = "302633878489-frpfjludkpmvt14sv9o8t9oehudlt81v.apps.googleusercontent.com";
        FACEBOOK_AUTH_CLIENT_ID = "281853372226895";
        LINKEDIN_AUTH_CLIENT_ID = "812mihblfdrvsg";
        break;
}
export default {
    API_URL,
    SITE_KEY_CAPTCHA,
    GOOGLE_AUTH_CLIENT_ID,
    FACEBOOK_AUTH_CLIENT_ID,
    LINKEDIN_AUTH_CLIENT_ID,
    WORKSPACE_URL
};