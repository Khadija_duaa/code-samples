import { useEffect, useState } from "react";
 
function useOpenCloseDialog() {
  const [open, setOpen] = useState(false);
 
  const handleCloseDialog = () => {
    setOpen(false);
  };
  const handleOpenDialog = () => {
    setOpen(true);
  };
 
  return { open, handleOpenDialog, handleCloseDialog };
}
 
export default useOpenCloseDialog;