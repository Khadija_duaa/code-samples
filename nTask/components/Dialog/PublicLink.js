import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import CustomDialog from "./CustomDialog";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultTextField from "../Form/TextField";
import CustomButton from "../Buttons/CustomButton";
import NotificationMessage from "../NotificationMessages/NotificationMessages";
import { filterMutlipleEmailsInput, validateMultifieldEmails } from "../../utils/formValidations";
import getErrorMessages from "../../utils/constants/errorMessages";
import queryString from "query-string";

import {
  generatePublicLink,
  deletePublicLink,
  enablePublicLink,
  emailPublicLink,
} from "../../redux/actions/tasks";
import { FormattedMessage, injectIntl } from "react-intl";

class PublicLink extends Component {
  constructor(props) {
    super(props);
    this.state = {
      publicLink: {},
      EmailAddressError: "",
      EmailAddressErrorText: "",
      emailList: "",
      emailBtnQuery: "",
      deleteBtnQuery: "",
      generateBtnQuery: "",
    };
  }
  componentDidMount() {
    const success = response => {
      //Success callback of generating public link action creator
      this.setState({ publicLink: response.data.link });
    };
    //Failure callback of generating public link action creator
    const failure = response => {};

    //action creator to fetch the public link and it's enabled/disabled state when user open a public link of task dialog
    this.props.generatePublicLink(this.props.id, success, failure);
  }
  copyToClipboard = () => {
    // Function called when click on copy link to copy the link to clipboard
    this.inputEl.select();
    document.execCommand("copy");
  };
  // Function To delete public link
  deletePublicLink = () => {
    let obj = {
      // Object that includes task id to be posted to delete the public link of task
      taskId: this.props.id,
      publicLink: 0,
    };

    if (typeof taskTitle === undefined) {
      obj = {
        // Object that includes task id to be posted to delete the public link of task
        projectId: this.props.id,
        publicLink: 1,
      };
    }

    const success = response => {
      // success callback on delete publick link
      this.setState({
        publicLink: "",
        deleteBtnQuery: "",
      });
    };
    // failure callback on delete publick link
    const failure = response => {
      this.setState({ deleteBtnQuery: "" });
    };

    // action creater to delete public link
    this.setState({ deleteBtnQuery: "progress" });
    this.props.deletePublicLink(obj, success, failure);
  };
  enablePublicLink = () => {
    // Function To enable public link
    let obj = {
      // Object that includes task id to be posted to enable the public link of task
      taskId: this.props.id,
      publicLink: 0,
    };

    if (typeof taskTitle === undefined) {
      obj = {
        // Object that includes task id to be posted to delete the public link of task
        projectId: this.props.id,
        publicLink: 1,
      };
    }

    const success = response => {
      // success callback on enable publick link
      this.setState({
        publicLink: response.data.link,
        generateBtnQuery: "",
      });
    };
    // failure callback on enable publick link
    const failure = response => {
      this.setState({ generateBtnQuery: "" });
    };

    // action creater to enable public link
    this.setState({ generateBtnQuery: "progress" });
    this.props.enablePublicLink(obj, success, failure);
  };

  getTranslation = text => {
    if (
      text ==
      "There seems to be an issue with your email address, please review the spelling and try again."
    ) {
      return this.props.intl.formatMessage({
        id: "common.public-link.messaged",
        defaultMessage: text,
      });
    } else if (text == "Oops! Length must not exceed 50 characters.") {
      return this.props.intl.formatMessage({
        id: "common.public-link.messagee",
        defaultMessage: text,
      });
    } else {
      return text;
    }
  };
  handleEmailSend = () => {
    const props = this.props;
    const success = response => {
      this.setState({
        emailList: "",
        emailBtnQuery: "",
      });
    };
    const failure = response => {
      this.setState({ emailBtnQuery: "" });
    };

    let validationObj = validateMultifieldEmails(this.state.emailList);

    if (validationObj["isError"]) {
      this.setState({
        EmailAddressError: validationObj["isError"],
        EmailAddressErrorText: this.getTranslation(validationObj["message"]),
      });
    } else {
      this.setState(
        {
          EmailAddressError: false,
          EmailAddressErrorText: "",
          emailBtnQuery: "progress",
        },
        () => {
          let emailListObj = {
            emails: validationObj["data"],
            id: queryString.parseUrl(this.state.publicLink).query.Id,
            title: this.state.publicLink,
            publicLink: 0,
          };
          props.emailPublicLink(emailListObj, success, failure);
        }
      );
    }
  };
  handleInput = name => event => {
    let inputValue = event.target.value;
    inputValue = filterMutlipleEmailsInput(inputValue);
    this.setState({
      [name]: inputValue,
      EmailAddressError: false,
      EmailAddressErrorText: "",
    });
  };
  render() {
    const { classes, theme, open, closeAction, taskTitle, maxTitleWidth, companyInfo } = this.props;
    const {
      publicLink,
      EmailAddressError,
      EmailAddressErrorText,
      emailList,
      emailBtnQuery,
      deleteBtnQuery,
      generateBtnQuery,
    } = this.state;
    const taskLabelSingle = companyInfo?.task?.sName;
    return (
      <CustomDialog
        title={
          <FormattedMessage
            id="common.public-link.label"
            defaultMessage={`Public Link: ${taskTitle}`}
            values={{ l: taskTitle }}
          />
        }
        maxTitleWidth={maxTitleWidth}
        dialogProps={{
          open: open,
          onClose: closeAction,
          onClick: e => e.stopPropagation(),
        }}>
        <div className={classes.dialogContentCnt}>
          <NotificationMessage type="info" iconType="info" style={{ marginBottom: 20 }}>
            <FormattedMessage
              id="common.public-link.heading"
              defaultMessage={`Share this link with caution. Anyone with this link will have 'view' access to this {label}.`}
              values={{
                label: taskLabelSingle || 'task'
              }}
            />
          </NotificationMessage>
          <div className={classes.publicLinkInputCnt}>
            <DefaultTextField
              label={false}
              error={false}
              formControlStyles={{ marginBottom: 0 }}
              noRightBorder={true}
              defaultProps={{
                id: "publicLink",
                value: publicLink,
                readOnly: true,
                disabled: !publicLink,
                inputRef: node => {
                  this.inputEl = node;
                },
              }}
            />
            <CustomButton
              variant="contained"
              btnType="gray"
              disableRipple
              onClick={this.copyToClipboard}
              disabled={!publicLink}
              style={{
                borderRadius: "0 4px 4px 0",
                borderLeft: 0,
                minWidth: 120,
              }}>
              <FormattedMessage id="common.action.copy-link.label" defaultMessage="Copy Link" />
            </CustomButton>
          </div>
          {publicLink ? (
            <DefaultTextField
              fullWidth={true}
              label={
                <FormattedMessage
                  id="common.public-link.messageb"
                  defaultMessage="Share public link with others."
                />
              }
              labelProps={{
                shrink: true,
              }}
              errorState={EmailAddressError}
              errorMessage={EmailAddressErrorText}
              defaultProps={{
                id: "emailList",
                onChange: this.handleInput("emailList"),
                disabled: !publicLink,
                value: emailList,
                multiline: true,
                rows: 7,
                placeholder: this.props.intl.formatMessage({
                  id: "common.public-link.messagec",
                  defaultMessage: "Enter email addresses separated by commas.",
                }),
                autoFocus: true,
              }}
            />
          ) : null}
        </div>

        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="center"
          className={classes.dialogActionsCnt}>
          {publicLink ? (
            <>
              <CustomButton
                onClick={this.handleEmailSend}
                btnType="success"
                variant="contained"
                style={{ marginRight: 20 }}
                query={emailBtnQuery}
                disabled={emailBtnQuery == "progress"}>
                <FormattedMessage id="common.action.send-link.label" defaultMessage="Send Link" />
              </CustomButton>
              <CustomButton
                onClick={this.deletePublicLink}
                btnType="danger"
                variant="contained"
                query={deleteBtnQuery}
                disabled={deleteBtnQuery == "progress"}>
                <FormattedMessage
                  id="common.action.delete.confirmation.delete-button.label"
                  defaultMessage="Delete"
                />
              </CustomButton>
            </>
          ) : (
            <CustomButton
              onClick={this.enablePublicLink}
              btnType="success"
              variant="contained"
              query={generateBtnQuery}
              disabled={generateBtnQuery == "progress"}>
              <FormattedMessage id="common.generate.label" defaultMessage="Generate" />
            </CustomButton>
          )}
        </Grid>
      </CustomDialog>
    );
  }
}
function mapStateToProps(state) {
  return {
    companyInfo: state.whiteLabelInfo.data,
  }
}
export default compose(
  withRouter,
  injectIntl,
  withStyles(dialogStyles, { withTheme: true }),
  connect(mapStateToProps, {
    generatePublicLink,
    deletePublicLink,
    enablePublicLink,
    emailPublicLink,
  })
)(PublicLink);
