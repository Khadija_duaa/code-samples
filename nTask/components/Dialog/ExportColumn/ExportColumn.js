import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import SearchIcon from "@material-ui/icons/Search";
import React, { useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import { injectIntl } from "react-intl";
import { connect, useDispatch } from "react-redux";
import { compose } from "redux";
import helper from "../../../helper";
import useListSelect from "../../../helper/useListSelect.hook";
import { addPendingHandler } from "../../../redux/actions/backProcesses";
import { exportSelectedColumns, getColumnsList } from "../../../redux/actions/ImportExport";
import DefaultCheckbox from "../../Form/Checkbox";
import DefaultTextField from "../../Form/TextField";
import ButtonActionsCnt from "../ConfirmationDialogs/ButtonActionsCnt";
import CustomDialog from "../CustomDialog";
import style from "./styles";
const exportTypeApi = {
  task: "/api/export/type/UserTask",
  board: "/api/export/type/UserTask",
  issue: "/api/export/type/issue",
  risk: "/api/export/type/risk",
  project: "/api/export/type/project",
  meeting: "/api/export/type/meeting",
};

function ExportColumn({
  classes,
  theme,
  open,
  closeAction,
  exportType,
  handleClickExport,
  companyInfo,
  handleNotificationMsg,
  intl,
  projectId,
  ...props
}) {
  const exportApi = {
    task: "/api/export/UserTask1",
    board: "/api/export/UserTask1?projectId=" + projectId,
    issue: "/api/export/issue1",
    risk: "/api/export/risk1",
    project: "/api/export/project2",
    meeting: "/api/export/meeting1",
  };

  const [exportBtnQuery, setExportBtnQuery] = useState("");
  const dispatch = useDispatch();

  //Generate data to select list hook
  const generateListData = (data) => {
    return data.map((d) => ({ value: d, checked: false }));
  };
  //Handle Check and uncheck of column
  const handleCheckColumn = (item) => {
    let checkItem = { ...item };
    checkItem.checked = !checkItem.checked;
    handleSelectItem(checkItem);
  };
  //Get list of export column types
  const getList = () => {
    getColumnsList(exportTypeApi[exportType], (res) => {
      setItems(generateListData(res.data));
    });
  };

  // Function that handles export
  const handleExport = () => {
    setExportBtnQuery("progress");
    const fileName = `${companyInfo ? companyInfo.companyName : "nTask"}-${exportType}.xlsx`;
    const columnsArr = selectedItems.map((it) => it.value);
    const obj = {
      type: `${exportType}s`,
      apiType: 'post',
      data: columnsArr,
      fileName: fileName,
      apiEndpoint: exportApi[exportType],
    }
    addPendingHandler(obj, dispatch);
    closeAction()
    
    // exportSelectedColumns(
    //   exportApi[exportType],
    //   columnsArr,
    //   (res) => {
    //     setExportBtnQuery("");
    //     helper.DOWNLOAD_TEMPLATE(res.data, fileName, "application/vnd.ms-excel");
    //     handleNotificationMsg(
    //       intl.formatMessage({
    //         id: "common.successMessages.file-export",
    //         defaultMessage: "File exported successfully.",
    //       }),
    //       "success"
    //     );
    //     closeAction();
    //   },
    //   //Failure
    //   () => {
    //     let error = err.data.message ? err.data.message : "Oops! No records found to be exported.";
    //     setExportBtnQuery("");
    //     handleNotificationMsg(error, "error");
    //   }
    // );
  };
  const onClick =(e) => {
   e.stopPropagation()
  }
  const {
    items,
    handleSelectItem,
    setItems,
    selectedItems,
    search,
    searchInput,
    handleSelectAllItems,
    selectAll,
  } = useListSelect(getList, "value");
  return (
      <CustomDialog
        title="Select Columns To Export"
        dialogProps={{
          open: open,
          onClose: closeAction,
          PaperProps: {
            style: { maxWidth: 480 },
          },
          onClick: (e) => onClick(e),
          disableBackdropClick: true,
          disableEscapeKeyDown: true,
        }}>
        <div className={classes.contentCnt}>
          <Typography variant="body2">Select Columns</Typography>
          <div className={classes.searchBarCnt}>
            <DefaultCheckbox
              checked={selectAll}
              onChange={handleSelectAllItems}
              label={"Select all"}
              checkboxStyles={{ padding: "0 8px 0 0" }}
              styles={{
                marginTop: 5,
                marginBottom: 10,
                display: "flex",
                alignItems: "center",
              }}
            />

            <DefaultTextField
              formControlStyles={{ width: 150, marginBottom: 0 }}
              defaultProps={{
                id: "checklistEdit",
                onChange: (e) => search(e.target.value),
                value: searchInput,
                placeholder: "Search Columns",
                autoFocus: true,
                style: { paddingLeft: 6 },
                inputProps: { style: { padding: "9px 6px" } },
                startAdornment: <SearchIcon className={classes.searchIcon} />,
              }}
            />
          </div>
          <Scrollbars
            autoHeight
            hideTracksWhenNotNeeded={true}
            autoHeightMin={100}
            autoHeightMax={300}>
            <ul className={classes.columnNameCnt}>
              {items.map((c) => {
                return (
                  <>
                    <li>
                      <DefaultCheckbox
                        checked={c.checked}
                        onChange={() => {
                          handleCheckColumn(c);
                        }}
                        label={c.value}
                        checkboxStyles={{ padding: "0 8px 0 0" }}
                        styles={{
                          marginTop: 5,
                          display: "flex",
                          alignItems: "center",
                        }}
                      />
                    </li>{" "}
                  </>
                );
              })}
            </ul>
          </Scrollbars>
        </div>
        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={handleExport}
          successBtnText={"Export"}
          cancelBtnText={"Cancel"}
          btnType="blue"
          btnQuery={exportBtnQuery}
          disabled={selectedItems < 1}
        />
      </CustomDialog>
  );
}

ExportColumn.defaultProps = {
  /** default props for the component  */
  classes: {},
  theme: {},
  open: false,
  closeAction: () => {},
  handleClickExport: () => {},
  projectId: "",
};

const mapStateToProps = (state) => {
  return {
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  injectIntl,
  withStyles(style, { withTheme: true }),
  connect(mapStateToProps, {})
)(ExportColumn);
