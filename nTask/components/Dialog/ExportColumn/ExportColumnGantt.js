import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import SearchIcon from "@material-ui/icons/Search";
import fileDownload from "js-file-download";
import React, { useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import { injectIntl } from "react-intl";
import { connect, useDispatch } from "react-redux";
import { compose } from "redux";
import helper from "../../../helper";
import useListSelect from "../../../helper/useListSelect.hook";
import { addPendingHandler } from "../../../redux/actions/backProcesses";
import { exportToPdf, exportToExcel, exportToMSProject } from "../../../redux/actions/gantt";
import DefaultCheckbox from "../../Form/Checkbox";
import DefaultTextField from "../../Form/TextField";
import ButtonActionsCnt from "../ConfirmationDialogs/ButtonActionsCnt";
import CustomDialog from "../CustomDialog";
import style from "./styles";

function ExportColumnGantt({
  classes,
  theme,
  open,
  closeAction,
  exportType,
  handleClickExport,
  companyInfo,
  handleNotificationMsg,
  intl,
  projectId,
  projectName,
  handleBtnLoader,
  handelExportFailuer,
  ...props
}) {

  const [exportBtnQuery, setExportBtnQuery] = useState("");
  const dispatch = useDispatch();

  //Generate data to select list hook
  const generateListData = (data) => {
    return data.map((d) => ({ value: d, checked: false }));
  };
  //Handle Check and uncheck of column
  const handleCheckColumn = (item) => {
    let checkItem = { ...item };
    checkItem.checked = !checkItem.checked;
    handleSelectItem(checkItem);
  };
  //Get list of export column types
  const getList = () => {
    const columns = [
      "Planned Start",
      "Planned End",
      "Duration",
      "Predecessors",
      "Successors",
      "Actual Start",
      "Actual End",
      "Progress",
      "Resources",
    ]
    setItems(generateListData(columns))
  };
  // Function that handles export
  const handleExport = () => {
    // setExportBtnQuery("progress");
    // handleBtnLoader(true)
    const fileName = `${companyInfo ? companyInfo.companyName : "nTask"}-${exportType}.xlsx`;
    const columnsArr = selectedItems.map((it) => it.value);
    columnsArr.push('ID');
    columnsArr.push('Name');
    if (exportType == 'pdf') {
      const postObj = {
        projectId: projectId,
        columns: columnsArr
      }
      const obj = {
        type: 'projects',
        apiType: 'post',
        data: postObj,
        fileName: `${projectName}.pdf`,
        apiEndpoint: 'api/DocsFileUploader/ExportMSPDF',
      }
      addPendingHandler(obj, dispatch);
      closeAction()

      // exportToPdf({
      //   projectId: projectId,
      //   columns: columnsArr
      // },
      //   res => {
      //     handleBtnLoader(false)
      //     setExportBtnQuery("");
      //     fileDownload(res.data, `${projectName}.pdf`);
      //     closeAction();
      //     // this.props.showSnackBar('Activity Log exported successfully', "success");
      //   },
      //   err => {
      //     handleBtnLoader(false)
      //     setExportBtnQuery("");
      //     handelExportFailuer();
      //   }
      // );
    } else if (exportType == 'excel') {

      const postObj = {
        projectId: projectId,
        columns: columnsArr
      }
      const obj = {
        type: 'projects',
        apiType: 'post',
        data: postObj,
        fileName: `${projectName}.xlsx`,
        apiEndpoint: 'api/DocsFileUploader/ExportMSExcel',
      }
      addPendingHandler(obj, dispatch);
      closeAction()

      // exportToExcel({
      //   projectId: projectId,
      //   columns: columnsArr
      // },
      //   res => {
      //     setExportBtnQuery("");
      //     handleBtnLoader(false)
      //     fileDownload(res.data, `${projectName}.xlsx`);
      //     closeAction();
      //     // this.props.showSnackBar('Activity Log exported successfully', "success");
      //   },
      //   err => {
      //     handleBtnLoader(false);
      //     setExportBtnQuery("");
      //     handelExportFailuer();
      //   }
      // );
    }
  };
  const {
    items,
    handleSelectItem,
    setItems,
    selectedItems,
    search,
    searchInput,
    handleSelectAllItems,
    selectAll,
  } = useListSelect(getList, "value");

  return (
    <>
      <CustomDialog
        title="Select Columns To Export"
        dialogProps={{
          open: open,
          onClose: closeAction,
          PaperProps: {
            style: { maxWidth: 480 },
          },
          disableBackdropClick: true,
          disableEscapeKeyDown: true,
        }}>
        <div className={classes.contentCnt}>
          <Typography variant="body2">Select Columns</Typography>
          <div className={classes.searchBarCnt}>
            <DefaultCheckbox
              checked={selectAll}
              onChange={handleSelectAllItems}
              label={"Select all"}
              checkboxStyles={{ padding: "0 8px 0 0" }}
              styles={{
                marginTop: 5,
                marginBottom: 10,
                display: "flex",
                alignItems: "center",
              }}
            />

            <DefaultTextField
              formControlStyles={{ width: 150, marginBottom: 0 }}
              defaultProps={{
                id: "checklistEdit",
                onChange: (e) => search(e.target.value),
                value: searchInput,
                placeholder: "Search Columns",
                autoFocus: true,
                style: { paddingLeft: 6 },
                inputProps: { style: { padding: "9px 6px" } },
                startAdornment: <SearchIcon className={classes.searchIcon} />,
              }}
            />
          </div>
          <Scrollbars
            autoHeight
            hideTracksWhenNotNeeded={true}
            autoHeightMin={100}
            autoHeightMax={300}>
            <ul className={classes.columnNameCnt}>
              {items.map((c) => {
                return (
                  <>
                    <li>
                      <DefaultCheckbox
                        checked={c.checked}
                        onChange={() => {
                          handleCheckColumn(c);
                        }}
                        label={c.value}
                        checkboxStyles={{ padding: "0 8px 0 0" }}
                        styles={{
                          marginTop: 5,
                          display: "flex",
                          alignItems: "center",
                        }}
                      />
                    </li>{" "}
                  </>
                );
              })}
            </ul>
          </Scrollbars>
        </div>
        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={handleExport}
          successBtnText={"Export"}
          cancelBtnText={"Cancel"}
          btnType="blue"
          btnQuery={exportBtnQuery}
          disabled={selectedItems < 1}
        />
      </CustomDialog>
    </>
  );
}

ExportColumnGantt.defaultProps = {
  /** default props for the component  */
  classes: {},
  theme: {},
  open: false,
  closeAction: () => { },
  handleClickExport: () => { },
  handelExportFailuer: () => { },
  projectId: "",
  projectName: "Project Gantt",
};

const mapStateToProps = (state) => {
  return {
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  injectIntl,
  withStyles(style, { withTheme: true }),
  connect(mapStateToProps, {})
)(ExportColumnGantt);
