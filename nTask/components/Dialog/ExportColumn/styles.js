const exportColumnStyles = theme => ({
  contentCnt: {
    padding: 20,
  },
  searchBarCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    marginBottom: 15,
    paddingBottom: 15,
    "& svg": {
      fontSize: "18px !important",
    },
  },
  searchIcon: {
    color: theme.palette.icon.gray300,
  },
  columnNameCnt: {
    listStyleType: "none",
    padding: "0 15px 0 0",

    margin: 0,
    "& li": {
      display: "flex",
      alignItems: "center",
      fontFamily: theme.typography.fontFamilyLato,
      "& svg": {
        fontSize: "18px !important",
      },
    },
  },
});

export default exportColumnStyles;
