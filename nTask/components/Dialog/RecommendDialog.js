import React, { Component } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import CustomDialog from "./CustomDialog";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../Buttons/DefaultButton";
import CustomButton from "../Buttons/CustomButton";
import DefaultTextField from "../Form/TextField";
import RecommendIcon from "../Icons/RecommendIcon";
import { RecommendnTask } from "./../../redux/actions/recommend";
import ButtonActionsCnt from "./ConfirmationDialogs/ButtonActionsCnt";
import {
  filterMutlipleEmailsInput,
  validateMultifieldEmails,
} from "../../utils/formValidations";
import getErrorMessages from "../../utils/constants/errorMessages";
import InviteMember from "../InviteMember/InviteMember";
import { validateEmailAddress } from "../../redux/actions/teamMembers";
import NotificationMessage from "../NotificationMessages/NotificationMessages";
import { FormattedMessage, injectIntl } from "react-intl";

class RecommendDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 1,
      EmailAddressError: "",
      EmailAddressErrorText: "",
      btnQuery: "",
      emailList: [],
    };
    this.initialState = this.state;
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleInviteSend = this.handleInviteSend.bind(this);
  }

  onStarClick(nextValue, prevValue, name) {
    this.setState({ rating: nextValue });
  }
  handleInput = (name) => (event) => {
    let inputValue = event.target.value;
    localStorage.setItem("recommendEmails", inputValue.trim());
    inputValue = filterMutlipleEmailsInput(inputValue);
    this.setState({
      [name]: inputValue,
      EmailAddressError: false,
      EmailAddressErrorText: "",
    });
  };
  handleSelectChange(value) {
    this.setState({ feedbackType: value });
  }
  handleInviteSend() {
    const props = this.props;
    const { emailList } = this.state;
    // let validationObj = validateMultifieldEmails(this.state.emailList);

    this.setState(
      {
        btnQuery: "progress",
      },
      () => {
        let emailListObj = {
          userEmails: emailList,
        };
        props.RecommendnTask(
          emailListObj,
          (response) => {
            this.setState({ emailList: "", btnQuery: "" });
            props.closeDialog();
            props.showSnackBar("Email is sent successfully.", "success");
          },
          (error) => {
            props.showSnackBar(getErrorMessages().INVITE_FAILED, "error");
            this.setState({ btnQuery: "" });
          }
        );
      }
    );
  }
  updateEmailList = (emails, currentEmail) => {
    this.props.validateEmailAddress(
      currentEmail,
      (response) => {
        this.setState({ emailList: emails });
      },
      (error) => {
        this.setState({
          EmailAddressError: true,
          EmailAddressErrorText: error.data,
        });
      }
    );
  };
  render() {
    const { classes, theme, open, closeDialog, intl } = this.props;
    const {
      EmailAddressError,
      EmailAddressErrorText,
      emailList,
      btnQuery,
    } = this.state;

    return (
      <CustomDialog
        title={
          <FormattedMessage
            id="common.recommend-dialog.title"
            defaultMessage="Recommend nTask"
          />
        }
        dialogProps={{
          open,
          onClose: closeDialog,
          PaperProps: {
            style: { maxWidth: 500 },
          },
          onClick: (e) => e.stopPropagation(),
          onExited: () => {
            this.setState(this.initialState);
          },
          onEnter: () => {
            this.setState({
              emailList: localStorage.getItem("recommendEmails") || "",
            });
          },
          disableBackdropClick:true,
          disableEscapeKeyDown:true
        }}
      >
        <div className={classes.feedbackDialogContentCnt}>
          <div
            className={classes.centerAlignDialogContent}
            style={{ paddingBottom: 0, paddingTop: 0 }}
          >
            <SvgIcon
              viewBox="20 20 70 70"
              className={classes.recommendIcon}
              htmlColor={theme.palette.secondary.light}
            >
              <RecommendIcon />
            </SvgIcon>
            <p className={classes.dialogMessageText}>
              <FormattedMessage
                id="common.recommend-dialog.note"
                defaultMessage=" Refer nTask to your friends and get 100MB for each referral."
              />
            </p>
          </div>
          <InviteMember
            emailList={emailList}
            emailExistanceCheck={true}
            updateEmailList={this.updateEmailList}
            placeholder={this.props.intl.formatMessage({
              id: "common.recommend-dialog.email",
              defaultMessage: "Enter email address",
            })}
          />
          {/* <DefaultTextField
            fullWidth={true}
            labelProps={{
              shrink: true
            }}
            errorState={EmailAddressError}
            errorMessage={EmailAddressErrorText}
            defaultProps={{
              id: "emailList",
              onChange: this.handleInput("emailList"),
              value: emailList,
              multiline: true,
              rows: 7,
              placeholder: "Enter email addresses separated by commas.",
              autoFocus: true
            }}
          /> */}
          <NotificationMessage type="info" iconType="info">
            <FormattedMessage
              id="common.recommend-dialog.limit"
              defaultMessage="2 GB Maximum Limit"
            />
          </NotificationMessage>
        </div>
        <ButtonActionsCnt
          cancelAction={closeDialog}
          successAction={this.handleInviteSend}
          successBtnText={intl.formatMessage({
            id: "meeting.creation-dialog.form.invite-button.label",
            defaultMessage: "Send Invite",
          })}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          btnType="success"
          btnQuery={btnQuery}
          disabled={emailList.length > 0 ? false : true}
        />
      </CustomDialog>
    );
  }
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(dialogStyles, { withTheme: true }),
  connect(null, {
    RecommendnTask,
    validateEmailAddress,
  })
)(RecommendDialog);
