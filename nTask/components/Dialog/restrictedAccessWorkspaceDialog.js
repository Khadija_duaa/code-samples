import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../Buttons/CustomButton";
import CustomAvatar from "../Avatar/Avatar";
import { FormattedMessage } from "react-intl";
class RestrictedAccessWorkspace extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    const { classes, theme, open, closeAction, onSuccess, removedWorkspace } = this.props;
    if (removedWorkspace) {
      let name = removedWorkspace.teamName.charAt(0); /** Finding the first letter of workspace name */
      return (
        <Dialog
          classes={{
            paper: classes.betaDialogPaperCnt,
            scrollBody: classes.dialogCnt
          }}
          fullWidth={true}
          scroll="body"
          disableBackdropClick={true}
          disableEscapeKeyDown={true}
          open={open}
          onClose={closeAction}
        >
          <DialogContent classes={{ root: classes.betaDialogCnt }}>
            <div className={classes.avatarContainer}>
              <CustomAvatar           /** Avatar containg the Name/image of disabled workspace */
                otherWorkspace={{
                  teamName: name,
                  pictureUrl: removedWorkspace.pictureUrl,
                  baseUrl: removedWorkspace.imageBasePath
                }}
                size="xlarge"
              />
            </div>


            <Typography variant="h1" className={classes.betaDialogHeading}><FormattedMessage id="common.restricted-access-confirmation.title" defaultMessage="Restricted Access" /><br /></Typography>
            <br />
            <Typography variant="body2" className={classes.betaDialogMessage} align="center">
              <FormattedMessage id="common.restricted-access-confirmation.label-1" defaultMessage="Oops! Looks like your access is restricted." />

              <br />
              <FormattedMessage id="common.restricted-access-confirmation.label-2" defaultMessage="Please contact your Workspace Owner/Admin for further information." />

            </Typography>
            <br />
            <Typography variant="body2" className={classes.betaDialogMessage} align="center">
              <FormattedMessage id="common.restricted-access-confirmation.label-3" defaultMessage="Meanwhile, you can continue working by going to Team Dashboard." />

            </Typography>
            <CustomButton
              btnType="success"
              variant="contained"
              style={{ marginTop: 30 }}
              onClick={onSuccess}
            >
              <FormattedMessage id="common.restricted-access-confirmation.go-to-button.label" defaultMessage="Go to Team Dashboard" />
            </CustomButton>
          </DialogContent>
        </Dialog>
      );
    }
    else return null;

  }
}

export default withStyles(dialogStyles, { withTheme: true })(RestrictedAccessWorkspace);
