import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import introducingTeamStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../../Buttons/CustomButton";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
 
function MoveWorkspaceToTeam(props) {
  const { classes, theme, changeStep, getStartedAction, btnQuery } = props;
 
  return (
    <div className={classes.innerContentCnt}>
      <Typography variant="h1" className={classes.betaDialogHeading}>
        Move workspace(s) to team
      </Typography>

      <Typography
        variant="body2"
        className={classes.betaDialogMessage}
        align="center"
      >
        <br /> Just a heads up! You can only move workspace(s) to team  that you created or have an ownership of. So, let's move your workspace for a brand new productivity fueled experience!
      </Typography>
      <Typography
        variant="body2"
        className={classes.betaDialogMessage}
        align="center"
      >
        <br /> Your leftover workspaces will be moved to a default team to help you keep up with the pace.
      </Typography>
      <CustomButton
        btnType="success"
        variant="contained"
        btnQuery={btnQuery}
        style={{ marginTop: 30 }}
        onClick={getStartedAction}
      >
        Get Started!
      </CustomButton>
    </div>
  );
}

export default compose(
  withStyles(introducingTeamStyles, {
    withTheme: true
  }),
  withRouter
)(MoveWorkspaceToTeam);
