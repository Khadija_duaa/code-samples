import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import onBoardStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import DefaultTextField from "../../Form/TextField";
import TeamIcon from "@material-ui/icons/SupervisorAccount";
import GlobeIcon from "@material-ui/icons/Language";
import introducingTeamStyles from "./styles";
import CustomButton from "../../Buttons/CustomButton";
import { createFirstTeam } from "./../../../redux/actions/onboarding";
import { isEmpty } from 'validator'

class IntroCreateTeam extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teamNameBind: true
    };
  }
  handleBack = () => {
    this.props.handleBack(1);
    this.props.history.push("/welcome/company");
  };
  handleInput = fieldName => event => {
    let inputValue = event.target.value;
    const { teamNameBind } = this.state;
    //* Replacing all spaces with Dash from team url 
    let teamUrl = inputValue.replace(/[\s\+]+/g, "-").toLowerCase();
    switch (fieldName) {
      case "teamName": {
        if (teamNameBind) {
          this.setState({
            [fieldName]: inputValue,
            teamError: false,
            teamErrorMessage: "",
            teamNameBind: true,
            teamUrl: teamUrl,
            teamUrlError: false,
            teamUrlErrorMessage: ""
          });
        } else {
          /** */
          this.setState({
            [fieldName]: inputValue,
            teamError: false,
            teamErrorMessage: ""
          });
        }
        break;
      }
      case "teamUrl": {
        //Disallowing user to enter space in url
        let newTeamUrl = inputValue.replace(/ /g, "");
        this.setState({
          [fieldName]: newTeamUrl,
          teamUrlError: false,
          teamUrlErrorMessage: ""
        });
        if (this.state.teamNameBind) {
          this.setState({ teamNameBind: false });
        }
      }
    }
  };

  validInput = (key, value) => { /**Function for checking if the field is empty or not */
    let validationObj = isEmpty(value);
    if (validationObj) {
      switch (key) {
        case 'teamName':
          this.setState({ teamError: true, teamErrorMessage: 'Oops! Please enter team name.' })
          break;
        case 'teamUrl':
          this.setState({ teamUrlError: true, teamUrlErrorMessage: 'Oops! Please enter team url.' })
          break;

        default:
          break;
      }
    }
    return validationObj
  }
  handleCreateTeam = () => {
    const { teamName = "", teamUrl = "" } = this.state;
    let teamNameValidate = this.validInput('teamName', teamName);
    let teamUrlValidate = this.validInput('teamUrl', teamUrl);

    if (!teamNameValidate && !teamUrlValidate) {

      const teamObj = {
        CompanyName: teamName,
        CompanyUrl: teamUrl
      };
      createFirstTeam(
        teamObj,
        //Function to be called in case of success
        () => {
          this.props.changeStep(2)
        },
        err => {
          if (err.data.message == 'Team URL already exists.') {
            this.setState({ btnQuery: '', teamUrlError: true, teamUrlErrorMessage: err.data.message })
          } else {
            this.setState({ btnQuery: '', teamError: true, teamErrorMessage: err.data.message })
          }
        }
      );


    }

  };
  render() {
    const { activeStep, theme, classes, changeStep } = this.props;
    const {
      btnQuery = "",
      teamError = false,
      teamErrorMessage = "",
      teamName = "",
      teamUrl,
      teamUrlError,
      teamUrlErrorMessage
    } = this.state;
    return (
      <div className={classes.createTeamInnerContentCnt}>
        <Typography variant="h1" className={classes.betaDialogHeading}>
          Create your first team
        </Typography>

        <Typography
          variant="body2"
          className={classes.betaDialogMessage}
          align="center"
        >
          <br /> Choose a unique team name and URL to track your team and tasks
          on the go!
        </Typography>

        <DefaultTextField
          label="Team Name"
          // helptext={onboardingHelpText.workspaceNameHelpText}
          fullWidth={true}
          errorState={teamError}
          errorMessage={teamErrorMessage}
          defaultProps={{
            id: "teamName",
            onChange: this.handleInput("teamName"),
            placeholder: "Team Name",
            value: teamName,
            autoFocus: true,
            inputProps: {
              maxLength: 50,
              tabIndex: 1,
              style: { paddingLeft: 0 }
            },
            startAdornment: (
              <InputAdornment position="start">
                <TeamIcon
                  htmlColor={theme.palette.secondary.dark}
                  className={classes.teamIcon}
                />
              </InputAdornment>
            )
          }}
        />
        <DefaultTextField
          label="Team URL"
          // helptext={onboardingHelpText.workspaceUrlHelpText}
          fullWidth={true}
          errorState={teamUrlError}
          errorMessage={teamUrlErrorMessage}
          defaultProps={{
            id: "teamUrl",
            onChange: this.handleInput("teamUrl"),
            placeholder: "team URL",
            value: teamUrl,
            inputProps: {
              style: { paddingLeft: 0 },
              maxLength: 80,
              tabIndex: 4
            },
            startAdornment: (
              <InputAdornment
                position="start"
                classes={{ root: classes.urlAdornment }}
              >
                <GlobeIcon
                  htmlColor={theme.palette.secondary.dark}
                  className={classes.globeIcon}
                />
                <p className={classes.workspaceUrlAdornment}>
                  {window.location.origin+"/"}
                </p>
              </InputAdornment>
            )
          }}
        />
        <CustomButton
          btnType="success"
          variant="contained"
          style={{ marginTop: 30 }}
          onClick={this.handleCreateTeam}
        >
          Create Team
      </CustomButton>
      </div>
    );
  }
}

export default compose(
  withStyles(introducingTeamStyles, { withTheme: true }),
  withRouter
)(IntroCreateTeam);
