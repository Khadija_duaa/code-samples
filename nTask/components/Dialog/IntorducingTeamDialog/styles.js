import betaDialogBgImg from "../../../assets/images/bg_pattern.png";

const introducingTeamStyles = theme => ({
  betaDialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: 450
  },
  betaDialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: 520
  },
  betaDialogCnt: {
    padding: "85px 0 !important",
    background: `url(${betaDialogBgImg})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  innerContentCnt:{
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "0 72px !important"
  },
  createTeamInnerContentCnt:{
    padding: "0 56px !important",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  betaDialogMessage: {
    lineHeight: 1.8
  },
  teamImage: {
    marginBottom: 30
  },
  teamIcon: {
    fontSize: "30px !important"
  },
  workspaceUrlAdornment: {
    color: theme.palette.text.primary,
    fontSize: "14px !important",
    margin: 0
  },
  globeIcon: {
    marginRight: 5
  },
  urlAdornment: {
    marginRight: 0,
    width: 345
  },
});

export default introducingTeamStyles;
