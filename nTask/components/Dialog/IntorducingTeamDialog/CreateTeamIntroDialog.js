import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import introducingTeamStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import teamImages from "../../../assets/images/team.png";
import IntroCreateTeam from "./CreateTeam";
import CreateTeamIntro from "./IntroducingTeams";
import MoveWorkspaceToTeam from "./MoveWorkspacesToTeam";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { FetchUserInfo } from "../../../redux/actions/profile";
import { connect } from "react-redux";

class CreateTeamIntroDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 0
    };
  }
  handleUpdateStep = step => {
    this.setState({ step });
  };
  getStartedAction = () => {
    this.setState({btnQuery: "progress"})
    this.props.FetchUserInfo(data => {
      let activeTeam = data.data.teams.find(t => {
        return t.companyId == data.data.activeTeam;
      });
      this.props.closeAction();
      this.setState({getStartedBtnQuery: ""})
      this.props.history.push(`/teams/${activeTeam.companyUrl}`)
      this.props.hideLoadingState()
    });
  };
  
  render() {
    const { classes, theme, open, closeAction, acceptBtnAction } = this.props;
    const { step, getStartedBtnQuery ="" } = this.state;

    return (
      <Dialog
        classes={{
          paper: classes.betaDialogPaperCnt,
          scrollBody: classes.dialogCnt
        }}
        fullWidth={true}
        scroll="body"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={closeAction}
      >
        <DialogContent classes={{ root: classes.betaDialogCnt }}>
          <img src={teamImages} alt="teamImage" className={classes.teamImage} />
          {step == 0 ? (
            <CreateTeamIntro changeStep={this.handleUpdateStep} />
          ) : step == 1 ? (
            <IntroCreateTeam changeStep={this.handleUpdateStep} />
          ) : step == 2 ? (
            <MoveWorkspaceToTeam
              getStartedAction={this.getStartedAction}
              btnQuery={getStartedBtnQuery}
            />
          ) : null}
        </DialogContent>
      </Dialog>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {};
};
export default compose(
  withStyles(introducingTeamStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    { FetchUserInfo }
  ),
  withRouter
)(CreateTeamIntroDialog);
