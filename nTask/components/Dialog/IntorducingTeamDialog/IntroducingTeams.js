import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Typography from "@material-ui/core/Typography";
import introducingTeamStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import nTaskBetaLogo from "../../../assets/images/nTask_logo_getStarted.png";
import CustomButton from "../../Buttons/CustomButton";
import teamImages from "../../../assets/images/team.png";
import IntroCreateTeam from "./CreateTeam";

function CreateTeamIntro(props) {
  const { classes, theme, changeStep } = props;
  return (
    <div className={classes.innerContentCnt}>
      <Typography variant="h1" className={classes.betaDialogHeading}>
        Introducing Teams
      </Typography>

      <Typography
        variant="body2"
        className={classes.betaDialogMessage}
        align="center"
      >
        <br /> Hi, we introduced <span className>"teams"</span> feature as an
        added experience for professionals like you.
      </Typography>
      <Typography
        variant="body2"
        className={classes.betaDialogMessage}
        align="center"
      >
        <br /> Go ahead and take the brand new feature for a spin by making your
        team and moving your workspace(s) to it!
      </Typography>
      <CustomButton
        btnType="success"
        variant="contained"
        style={{ marginTop: 30 }}
        onClick={() => changeStep(1)}
      >
        Continue
      </CustomButton>
    </div>
  );
}

export default withStyles(introducingTeamStyles, { withTheme: true })(
  CreateTeamIntro
);
