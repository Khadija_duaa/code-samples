import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import CustomDialog from "./CustomDialog";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../Buttons/DefaultButton";
import CustomButton from "../Buttons/CustomButton";
import DefaultTextField from "../Form/TextField";
import { sendTimesheetEmail } from "../../redux/actions/timesheet";

import { filterMutlipleEmailsInput, validateMultifieldEmails } from '../../utils/formValidations';
import getErrorMessages from '../../utils/constants/errorMessages';
import {injectIntl,FormattedMessage} from "react-intl";
class TimesheetEmailDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      EmailAddressError: '',
      EmailAddressErrorText: '',
      emailList: '',
      btnQuery: ''
    };
    this.initialState = this.state;
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleInviteSend = this.handleInviteSend.bind(this);
  }
  handleInput = name => event => {
    let inputValue = event.target.value;
    localStorage.setItem("timesheetEmails", inputValue.trim());
    inputValue = filterMutlipleEmailsInput(inputValue);
    this.setState({ [name]: inputValue, EmailAddressError: false, EmailAddressErrorText: "" });
  };
  handleSelectChange(value) {
    this.setState({ feedbackType: value });
  }
  handleInviteSend() {
    let validationObj = validateMultifieldEmails(this.state.emailList);

    if (validationObj['isError']) {
      this.setState({
        EmailAddressError: validationObj['isError'],
        EmailAddressErrorText: validationObj['message'] == "There seems to be an issue with your email address, please review the spelling and try again." ? this.props.intl.formatMessage({id:"common.public-link.messaged", defaultMessage: validationObj['message'] }) : validationObj['message']
      });
    }
    else {
      const props = this.props;
      this.setState({
        EmailAddressError: false,
        EmailAddressErrorText: '',
        btnQuery: 'progress'
      }, () => {
        const data = {
          "emails": validationObj['data'],
          "currentDate": props.date.format('YYYY-MM-DDTHH:mm:ss'),
          "type": "Weekly"
        };
        props.sendTimesheetEmail(data,
          (response) => {
            this.setState({ btnQuery: '' });
            localStorage.removeItem('timesheetEmails');
            props.closeAction();
          }, (error) => {
            this.setState({ btnQuery: '' });
            if (error.status === 500) {
              // this.props.showSnackBar('Server throws error', 'error');
            }
            else {
              // this.props.showSnackBar(error.data.message, 'error');
            }
          });
      });
    }
  }
  render() {
    const { classes, theme, open, closeAction } = this.props;
    const { EmailAddressError, EmailAddressErrorText, emailList, btnQuery } = this.state;

    return (
      <CustomDialog
        title={<FormattedMessage id="timesheet.send-timesheet-confirmation.title" defaultMessage="Send Timesheet"/>}
        dialogProps={{
          open: open, onClose: closeAction,
          onClick: (e) => e.stopPropagation(),
          onExited: () => {
            this.setState(this.initialState)
          },
          onEnter: () => {
            this.setState({
              emailList: localStorage.getItem("timesheetEmails") || ""
            });
          }
        }
        }
      >
        <div className={classes.feedbackDialogContentCnt}>

          <DefaultTextField
            fullWidth={true}
            labelProps={{
              shrink: true
            }}
            errorState={EmailAddressError}
            errorMessage={EmailAddressErrorText}
            defaultProps={{
              id: "emailList",
              onChange: this.handleInput("emailList"),
              value: emailList,
              multiline: true,
              rows: 7,
              placeholder: this.props.intl.formatMessage({id:"common.enter-email.label", defaultMessage:"Enter email addresses separated by comma."}),
              autoFocus: true
            }}
          />

          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
          >

            <DefaultButton
              text={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
              onClick={closeAction}
              buttonType="Transparent"
              style={{ marginRight: 20 }}
            />
            <CustomButton
              onClick={this.handleInviteSend}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress"}
            >
              {
               <FormattedMessage id="timesheet.send-timesheet-confirmation.send-email.label" defaultMessage="Send Email"/> 
              }
            </CustomButton>
          </Grid>
        </div>
      </CustomDialog>
    );
  }
}


export default compose(
  withRouter,
  injectIntl,
  withStyles(dialogStyles, { withTheme: true }),
  connect(
   null,
    {
      sendTimesheetEmail
    }
  )
)(TimesheetEmailDialog);