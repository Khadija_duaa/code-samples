const moveProjectStyles = theme => ({
    contentCnt: {
        padding: 20
    },
    workspaceNameCnt:{
        listStyleType: "none",
        padding: "0 15px 0 0",
        margin: 0,
        "& li":{
            display: "flex",
            alignItems: "center",
            borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
            padding: "10px 0"
        }
    },
    ConfirmScreenCnt:{
        textAlign: "center",
        padding: "30px 30px 30px 30px"
    },
    radioBtn: {
        padding: 0,
        margin: "0 5px",
        color: theme.palette.border.lightBorder
    },
    moveProjectIcon:{
        fontSize: 110,
        marginBottom: 30
    },
    exceptionText:{
        color: theme.palette.error.main
    },
    searchIcon: {
        color: theme.palette.icon.gray300
      },
})

export default moveProjectStyles;