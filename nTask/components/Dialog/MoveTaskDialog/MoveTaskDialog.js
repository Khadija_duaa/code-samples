import React, { Component } from "react";
import CustomDialog from "../CustomDialog";
import { Scrollbars } from "react-custom-scrollbars";
import { withStyles } from "@material-ui/core/styles";
import ButtonActionsCnt from "../ConfirmationDialogs/ButtonActionsCnt";
import moveProjectStyles from "./style";
import NotificationMessage from "../../NotificationMessages/NotificationMessages";
import { compose } from "redux";
import { connect } from "react-redux";
import CustomAvatar from "../../Avatar/Avatar";
import Typography from "@material-ui/core/Typography";
import Radio from "@material-ui/core/Radio";
import ProjectMoveIcon from "../../Icons/ProjectMoveIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import { injectIntl, FormattedMessage } from "react-intl";
import DefaultTextField from "../../Form/TextField";
import SearchIcon from "@material-ui/icons/Search";

class MoveTaskDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isProceedClicked: false,
      checkedTeam: null,
      searchInput: "",
    };
    this.initialState = this.state;
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.open !== prevProps.open && this.props.open) {
      this.setState(this.initialState);
    }
  }

  handleWorkspaceSelect = team => {
    this.setState({ checkedTeam: team });
  };

  handleProceedClick = e => {
    const { checkedTeam } = this.state;
    const { successAction, profile } = this.props;
    if (this.state.isProceedClicked) {
      successAction(e, checkedTeam);
    } else {
      this.setState({
        isProceedClicked: true,
        checkedTeam: checkedTeam
          ? checkedTeam
          : profile.workspace.filter(team => {
              return team.teamId !== profile.loggedInTeam;
            })[0],
      });
    }
  };
  defaultMsg = workspace => {
    const { classes, isMulti = false } = this.props;
    if (isMulti)
      return (
        <>
          Are you sure you want to move this task and its related data to workspace.
          <b>{`"${workspace}"`}</b>?
        </>
      );
    return (
      <>
        Are you sure you want to move task
        <b className={classes.boldText}>{` "${this.props.taskName}" `}</b>
        and all its data to workspace. <b>{`"${workspace}"`}</b>?
      </>
    );
  };
  search = value => {
    this.setState({
      searchInput: value,
    });
  };

  render() {
    const { checkedTeam, isProceedClicked, searchInput } = this.state;
    const {
      classes,
      theme,
      open,
      cancelBtnText,
      headingText,
      closeAction,
      btnQuery,
      profile,
      exceptionText,
      isMulti = false,
      issue = {},
    } = this.props;

    let successBtnText = isProceedClicked ? (
      <FormattedMessage id="common.action.move.label2" defaultMessage="Yes, Move" />
    ) : (
      <FormattedMessage id="common.action.proceed.label" defaultMessage="Proceed" />
    );
    let workspaces = profile.workspace || [];
    let filteredWorkspace = workspaces.filter(workspace => {
      return workspace.teamId !== profile.loggedInTeam;
    });
    const filteredItems = filteredWorkspace.filter(
      obj => obj.teamName.toLowerCase().indexOf(searchInput.toLowerCase()) > -1
    );

    return (
      <CustomDialog
        title={headingText}
        dialogProps={{
          open: open,
          onClick: e => {
            e.stopPropagation();
          },
          onClose: closeAction,
          PaperProps: {
            style: { maxWidth: 480 },
          },
          disableBackdropClick: true,
          disableEscapeKeyDown: true,
        }}>
        <div className={classes.contentCnt}>
          {exceptionText ? (
            <NotificationMessage type="failure" iconType="failure" style={{ marginBottom: 20 }}>
              {exceptionText}
            </NotificationMessage>
          ) : null}
          {isProceedClicked ? (
            <>
              <div className={classes.ConfirmScreenCnt}>
                <SvgIcon viewBox="0 0 117 117" className={classes.moveProjectIcon}>
                  <ProjectMoveIcon />
                </SvgIcon>
                <Typography variant="body2">{this.defaultMsg(checkedTeam.teamName)}</Typography>
              </div>
              <NotificationMessage type="info" iconType="info">
                Moving a task across the workspaces will apply the template settings of the new
                workspace.
              </NotificationMessage>
            </>
          ) : (
            <>
              <NotificationMessage type="info" iconType="info" style={{ marginBottom: 20 }}>
                All your data including chat history, documents and attachments will be moved to
                selected workspace.
              </NotificationMessage>
              <DefaultTextField
                formControlStyles={{ marginBottom: 0 }}
                defaultProps={{
                  id: "searchWorkspace",
                  onChange: e => this.search(e.target.value),
                  value: searchInput,
                  placeholder: "Search Workspace",
                  autoFocus: true,
                  style: { paddingLeft: 6 },
                  inputProps: { style: { padding: "9px 6px" } },
                  startAdornment: <SearchIcon className={classes.searchIcon} />,
                }}
              />
              {filteredWorkspace.length > 0 ? (
                <>
                  <Typography variant="body2">
                    <FormattedMessage
                      id="workspace-settings.select.label"
                      defaultMessage="SELECT WORKSPACE"
                    />
                  </Typography>
                  <Scrollbars style={{ height: 250 }}>
                    <ul className={classes.workspaceNameCnt}>
                      {filteredItems.map((team, i) => {
                        return (
                          // Check to ignore the active workspace from the list as it's the workspace already user is present in
                          <li key={team.teamId} onClick={() => this.handleWorkspaceSelect(team)}>
                            <Radio
                              checked={checkedTeam ? checkedTeam.teamId === team.teamId : false}
                              value={team.teamId}
                              className={classes.radioBtn}
                            />
                            <CustomAvatar
                              styles={{ marginRight: 10 }}
                              otherWorkspace={{
                                teamName: team.teamName,
                                pictureUrl: team.pictureUrl,
                                baseUrl: team.imageBasePath,
                              }}
                              size="xsmall"
                            />
                            <Typography variant="body2">{team.teamName}</Typography>
                          </li>
                        );
                      })}
                    </ul>
                  </Scrollbars>
                </>
              ) : (
                <NotificationMessage type="failure" iconType="failure" style={{ marginBottom: 20 }}>
                  <FormattedMessage
                    id="workspace-settings.no-workspace.label"
                    defaultMessage="No other workspace found."
                  />
                </NotificationMessage>
              )}
            </>
          )}
        </div>
        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={this.handleProceedClick}
          successBtnText={successBtnText}
          cancelBtnText={cancelBtnText}
          classes={classes}
          btnType="success"
          btnQuery={btnQuery}
          disabled={filteredWorkspace.length == 0 || checkedTeam == null}
        />
      </CustomDialog>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.profile.data,
  };
};

export default compose(
  injectIntl,
  withStyles(moveProjectStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps)
)(MoveTaskDialog);
