import React, { Component } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../../Icons/DeleteIcon";
import CustomDialog from "./../CustomDialog";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../../Buttons/DefaultButton";
import DefaultTextField from "../../Form/TextField";
import CustomButton from "../../Buttons/CustomButton";

class DeleteConfirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDisable: false,
      workspaceNameError: "",
      workspaceNameErrorMessage: "",
      workspaceName: "",
      btnQuery: ""
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.stopPropagation();
    if (this.state.workspaceName !== this.props.loggedInTeam.teamName) {
      this.setState({
        workspaceNameError: "Error",
        workspaceNameErrorMessage: "Please enter correct workspace name."
      });
      return;
    } else {
      this.setState({
        workspaceNameError: "",
        workspaceNameErrorMessage: ""
      });
    }
    if (this.state.isDisable === false) {
      this.setState({ isDisable: true }, () => {
        this.setState({ isDisable: false, btnQuery: "progress" }, () => {
          this.props.closePreviousAction();
          this.props.deleteWorkspaceData(() => {
            this.setState({ btnQuery: "" });
            this.props.closeAction();
          });
        });
      });
    }
  }
  handleInput = name => event => {
    this.setState({
      [name]: event.target.value,
      workspaceNameError: "",
      workspaceNameErrorMessage: ""
    });
  };
  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      message,
      buttonName
    } = this.props;
    const {
      isDisable,
      workspaceNameErrorMessage,
      workspaceNameError,
      workspaceName,
      btnQuery
    } = this.state;
    return (
      <CustomDialog
        title={buttonName || "Delete"}
        dialogProps={{
          open: open,
          onClose: closeAction,
          onClick: e => e.stopPropagation()
        }}
      >
        <div className={classes.centerAlignDialogContent}>
          <div className={classes.deleteConfirmationIconCnt}>
            <SvgIcon
              viewBox="0 0 512 512"
              className={classes.deleteConfirmationIcon}
              htmlColor={theme.palette.error.main}
            >
              <DeleteIcon />
            </SvgIcon>
          </div>
          <p className={classes.dialogMessageText}>
            <React.Fragment>{message}</React.Fragment>
          </p>
          <div>
            <p className={classes.dialogMessageText}>
              WARNING: By deleting a workspace, you will permanently lose all
              your team's tasks, projects, meetings, timesheets and files
              shared.
            </p>
            <p className={classes.dialogMessageText}>
              You <b>CANNOT</b> undo this or regain access to your workspace
              once you proceed.
            </p>
            <p>
              Type{" "}
              <b>
                "
                {this.props.loggedInTeam
                  ? this.props.loggedInTeam.teamName
                  : ""}
                "
              </b>{" "}
              in the field below to permanently delete your workspace and
              everything in it.
            </p>
          </div>
          <DefaultTextField
            fullWidth={true}
            errorState={workspaceNameError}
            errorMessage={workspaceNameErrorMessage}
            defaultProps={{
              id: "workspaceNameInput",
              onChange: this.handleInput("workspaceName"),
              value: workspaceName,
              placeholder:
                "Type " +
                '"' +
                (this.props.loggedInTeam
                  ? this.props.loggedInTeam.teamName + '" here'
                  : ""),
              inputProps: { maxLength: 50 }
            }}
          />
        </div>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          classes={{
            container: classes.dialogFormActionsCnt
          }}
        >
          <DefaultButton
            text="Cancel"
            buttonType="grayBtn"
            style={{ marginRight: 20 }}
            onClick={closeAction}
          />
          <CustomButton
            onClick={this.handleClick}
            btnType="danger"
            variant="contained"
            query={btnQuery}
            disabled={btnQuery == "progress" || isDisable}
          >
            Delete
            </CustomButton>
        </Grid>
      </CustomDialog>
    );
  }
}

export default withStyles(dialogStyles, { withTheme: true })(
  DeleteConfirmation
);
