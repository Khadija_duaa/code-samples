import React, { Component } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../../Icons/DeleteIcon";
import CustomDialog from "../CustomDialog";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../../Buttons/DefaultButton";
import CustomButton from "../../Buttons/CustomButton";
import DefaultTextField from "../../Form/TextField";
class DeleteConfirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDisable: false,
      ProjectTitleError: false,
      ProjectTitleMessage: "",
      ProjectTitle: "",
      originalProjectName: (this.props.project) ? this.props.project.projectName : '',
      btnQuery: ""
    };
    this.handleClick = this.handleClick.bind(this);
  }
  componentDidMount(){
  }
  // componentDidUpdate(prevProps, prevState) {
  //   
  //   if(this.props.project){
  //   const { projectName } = this.props.project;
  //   if (projectName !== prevProps.project.projectName) {
  //     this.setState({ originalProjectName: projectName })
  //   }
  // }
  // }

  handleClick(event) {
    event.stopPropagation();
    if (this.state.ProjectTitle === this.props.project.projectName) {
      this.setState({ btnQuery: "progress" });
      this.props.DeleteProject(this.props.project.projectId, (response) => {
        this.setState({ btnQuery: "" });
        this.props.closeAction();
        if (response.status === 200)
          this.props.FetchWorkSpaceData("", () => {
            this.setState({
              ProjectTitle: "",
              ProjectTitleMessage: "",
              ProjectTitleError: false,
              originalProjectName: '',
              btnQuery: ""
            })
          });
        else {
          this.props.showSnackbar(response.data.message, 'error');
        }
      });
    } else {
      this.setState({
        ProjectTitleMessage: "Please enter correct project name.",
        ProjectTitleError: true
      });
      return;
    }
  }
  handleInput = name => event => {
    this.setState({
      [name]: event.target.value,
      ProjectTitleError: false,
      ProjectTitleMessage: ""
    });
  };
  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      message,
      buttonName
    } = this.props;
    const {
      isDisable,
      ProjectTitleMessage,
      ProjectTitle,
      originalProjectName,
      ProjectTitleError,
      btnQuery
    } = this.state;
    return (
      <CustomDialog
        title={buttonName || "Delete"}
        dialogProps={{
          open: open,
          onClose: closeAction,
          onClick: e => e.stopPropagation()
        }}
      >
        <div className={classes.centerAlignDialogContent}>
          <div className={classes.deleteConfirmationIconCnt}>
            <SvgIcon
              viewBox="0 0 512 512"
              className={classes.deleteConfirmationIcon}
              htmlColor={theme.palette.error.main}
            >
              <DeleteIcon />
            </SvgIcon>
          </div>
          <p className={classes.dialogMessageText}>
            <React.Fragment>{message}</React.Fragment>
          </p>
          <div>
            <p className={classes.dialogMessageText}>
              WARNING: By deleting a project, you will permanently lose all your
              team's tasks, meetings, timesheets and files shared in the
              project.
            </p>
            <p className={classes.dialogMessageText}>
              You <b>CANNOT</b> undo this once you proceed.
            </p>
            <p>
              Type
              {" "}
              <b>"{this.props.project ? this.props.project.projectName : null}"</b>
              {" "} in
              the field below to permanently delete your project and everything
              in it.
            </p>
          </div>
          <DefaultTextField
            fullWidth={true}
            errorState={ProjectTitleError}
            errorMessage={ProjectTitleMessage}
            defaultProps={{
              id: "projectInput",
              onChange: this.handleInput("ProjectTitle"),
              value: ProjectTitle,
              placeholder: "Type project name here",
              autoFocus: true
            }}
          />
        </div>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          classes={{
            container: classes.dialogFormActionsCnt
          }}
        >
          <DefaultButton
            text="Cancel"
            buttonType="grayBtn"
            style={{ marginRight: 20 }}
            onClick={e => closeAction(e)}
          />
          <CustomButton
            onClick={e => this.handleClick(e)}
            btnType="dangerText"
            variant="contained"
            query={btnQuery}
            disabled={btnQuery == "progress"}
          >
            Delete
          </CustomButton>
        </Grid>
      </CustomDialog>
    );
  }
}

export default withStyles(dialogStyles, { withTheme: true })(
  DeleteConfirmation
);
