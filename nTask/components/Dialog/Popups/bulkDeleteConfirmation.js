import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import DashIcon from "@material-ui/icons/Remove";
import workspaceSetting from "../../../Views/TeamSetting/styles";
import { withStyles } from "@material-ui/core/styles";
import DefaultTextField from "../../Form/TextField";
import { injectIntl, FormattedMessage } from "react-intl";

class DeleteBulkProjectDialogContent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  handleClickShowPassword = () => {
    this.setState((state) => ({ showPassword: !state.showPassword }));
  };

  render() {
    const {
      classes,
      theme,
      step,
      handleTypeProjectInput,
      project,
    } = this.props;
    return (
      <div className={classes.deleteTeamDialogContent}>
        {step == 0 ? (
          <div>
            <Typography variant="body2">
              <FormattedMessage
                id="common.action.delete.project.title"
                defaultMessage="Are you sure you want to permanently delete this project?"
              />
              <br />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage
                id="common.action.delete.project.messagea"
                defaultMessage="Permanently deleting your project will automatically erase:"
              />

              <br />
            </Typography>
            <ul>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />
                <Typography variant="body2">
                  <FormattedMessage
                    id="common.action.delete.project.messageb"
                    defaultMessage="All created tasks in the project"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage
                    id="common.action.delete.project.messagec"
                    defaultMessage="All timesheets ( pending & approved)"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage
                    id="common.action.delete.project.messagem"
                    defaultMessage="All comments history"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  {" "}
                  <FormattedMessage
                    id="common.action.delete.project.messaged"
                    defaultMessage="All file attachments"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  {" "}
                  <FormattedMessage
                    id="common.action.delete.project.messagee"
                    defaultMessage="All meetings"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  {" "}
                  <FormattedMessage
                    id="common.action.delete.project.messagef"
                    defaultMessage="All created & assigned issues and risks"
                  />
                </Typography>
              </li>
            </ul>
          </div>
        ) : step == 1 ? (
          <div>
            <Typography variant="body2">
              <b>
                {" "}
                <FormattedMessage
                  id="common.action.delete.project.messagel"
                  defaultMessage="WARNING:"
                />
                {" "}
              </b>
              <FormattedMessage
                id="common.action.delete.project.messageg"
                defaultMessage="By deleting a project, you will permanently lose all your tasks, meetings, timesheets and files shared in the project. You"
              />{" "}
              <b>
                <FormattedMessage
                  id="common.action.delete.project.messageh"
                  defaultMessage="CANNOT"
                />
              </b>{" "}
              <FormattedMessage
                id="common.action.delete.project.messagei"
                defaultMessage="undo this once you proceed."
              />
              <br />
              <br />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage
                id="common.action.delete.project.messagej"
                defaultMessage="Type"
              />
              {" "}
              <b>
                "
                {project && project.projectName
                  ? project.projectName
                  : "delete all projects here"}
                "
              </b>{" "}
              <FormattedMessage
                id="common.action.delete.project.messagek"
                defaultMessage="in the field below to permanently delete your project and everything in it."
              />
            </Typography>

            <DefaultTextField
              fullWidth={true}
              defaultProps={{
                id: "typeProjectNameHere",
                type: "text",
                onChange: handleTypeProjectInput,
                autoFocus: true,
              }}
            />
          </div>
        ) : null}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps),
  withStyles(workspaceSetting, { withTheme: true })
)(DeleteBulkProjectDialogContent);
