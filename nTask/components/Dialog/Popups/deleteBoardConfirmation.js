import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import DashIcon from "@material-ui/icons/Remove";
import workspaceSetting from "../../../Views/TeamSetting/styles";
import { withStyles } from "@material-ui/core/styles";
import DefaultTextField from "../../Form/TextField";
import { injectIntl, FormattedMessage } from "react-intl";

class DeleteBoardConfirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };
  componentWillUnmount() {
    const { setStepBack } = this.props;
    setStepBack();
  }

  render() {
    const { classes, theme, step, handleTypeProjectInput, project, setStepBack, companyInfo } = this.props;
    const style = {
      fontSize: "11px",
      marginLeft: 3,
      color: "#7E7E7E",
    };
    const taskLabelMulti = companyInfo?.task?.pName;
    const isIssueHidden = companyInfo?.issue?.isHidden;
    const isMeetingHidden = companyInfo?.meeting?.isHidden;
    const isRiskHidden = companyInfo?.risk?.isHidden;
    return (
      <div className={classes.deleteTeamDialogContent}>
        {step == 0 ? (
          <div>
            <Typography variant="body2">
              <FormattedMessage
                id="board.all-boards.delete-board-confirmation.label1"
                values={{ name: <b>{project.boardName}</b>, label: taskLabelMulti || 'task(s)' }}
                defaultMessage={`Looks like {name} has one or multiple {label}."}`}
              />
              <br />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage
                id="board.all-boards.delete-board-confirmation.label2"
                defaultMessage="Permanently deleting your board will automatically erase:"
              />
              <br />
            </Typography>
            <ul>
              <li>
                <DashIcon htmlColor={theme.palette.error.light} className={classes.dashIcon} />
                <Typography variant="body2">
                  <FormattedMessage
                    id="board.all-boards.delete-board-confirmation.label3"
                    defaultMessage="All created & assigned {label}"
                    values={{label: taskLabelMulti ? taskLabelMulti : 'tasks'}}
                  />
                </Typography>
              </li>
              <li>
                <DashIcon htmlColor={theme.palette.error.light} className={classes.dashIcon} />

                <Typography variant="body2">
                  <FormattedMessage
                    id="common.action.delete.project.messagec"
                    defaultMessage="All timesheets ( pending & approved)"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon htmlColor={theme.palette.error.light} className={classes.dashIcon} />

                <Typography variant="body2">
                  <FormattedMessage
                    id="common.action.delete.project.messagem"
                    defaultMessage="All comments history"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon htmlColor={theme.palette.error.light} className={classes.dashIcon} />

                <Typography variant="body2">
                  {" "}
                  <FormattedMessage
                    id="common.action.delete.project.messaged"
                    defaultMessage="All file attachments"
                  />
                </Typography>
              </li>
              {!isMeetingHidden &&
              <li>
                <DashIcon htmlColor={theme.palette.error.light} className={classes.dashIcon} />

                <Typography variant="body2">
                  <FormattedMessage
                    id="common.action.delete.project.messagee"
                    defaultMessage="All meetings"
                  />{" "}
                  <span style={style}>
                    (
                    <FormattedMessage
                      id="board.all-boards.delete-board-confirmation.if-applicable-label"
                      defaultMessage="if applicable"
                    />
                    )
                  </span>
                </Typography>
              </li>}
              {!isIssueHidden && !isRiskHidden &&
              <li>
                <DashIcon htmlColor={theme.palette.error.light} className={classes.dashIcon} />

                <Typography variant="body2">
                  {" "}
                  <FormattedMessage
                    id="common.action.delete.project.messagef"
                    defaultMessage="All created & assigned issues and risks"
                  />
                  <span style={style}>
                    (
                    <FormattedMessage
                      id="board.all-boards.delete-board-confirmation.if-applicable-label"
                      defaultMessage="if applicable"
                    />
                    )
                  </span>
                </Typography>
              </li>}
              {/* <li>
                <DashIcon htmlColor={theme.palette.error.light} className={classes.dashIcon} />

                <Typography variant="body2">
                  {" "}
                  <FormattedMessage
                    id="common.action.delete.project.messagef"
                    defaultMessage="All created & assigned issues and risks"
                  />
                  <span style={style}>
                    (
                    <FormattedMessage
                      id="board.all-boards.delete-board-confirmation.if-applicable-label"
                      defaultMessage="if applicable"
                    />
                    )
                  </span>
                </Typography>
              </li> */}
            </ul>
            <Typography variant="body2" style={{ marginTop: 10 }}>
              <b>
                {" "}
                <FormattedMessage
                  id="board.all-boards.delete-board-confirmation.label"
                  defaultMessage="Are you sure you want to delete this board?"
                />
              </b>
              <br />
              <b>
                {" "}
                <FormattedMessage
                  id="board.all-boards.delete-board-confirmation.label4"
                  defaultMessage="This action cannot be undone."
                />
              </b>
            </Typography>
          </div>
        ) : step == 1 ? (
          <div>
            <Typography variant="body2">
              <b>
                {" "}
                <FormattedMessage
                  id="common.action.delete.project.messagel"
                  defaultMessage="WARNING:"
                />{" "}
              </b>
              <FormattedMessage
                id="board.all-boards.delete-board-confirmation.label5"
                defaultMessage="By deleting a board, you will permanently lose all your tasks, meetings,
              timesheets and files shared in the board. You"
              />{" "}
              <b>
                <FormattedMessage
                  id="common.action.delete.project.messageh"
                  defaultMessage="CANNOT"
                />
              </b>{" "}
              <FormattedMessage
                id="common.action.delete.project.messagei"
                defaultMessage="undo this once you proceed."
              />
              <br />
              <br />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage id="common.action.delete.project.messagej" defaultMessage="Type" />{" "}
              <b>
                "{project && project.boardName ? `${project.boardName}` : "delete all boards here"}"
              </b>{" "}
              <FormattedMessage
                id="board.all-boards.delete-board-confirmation.label6"
                defaultMessage="in the field below to permanently delete your board and everything in it."
              />
            </Typography>

            <DefaultTextField
              fullWidth={true}
              defaultProps={{
                id: "typeProjectNameHere",
                type: "text",
                onChange: handleTypeProjectInput,
                autoFocus: true,
              }}
            />
          </div>
        ) : null}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    profileState: state.profile,
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps),
  withStyles(workspaceSetting, { withTheme: true })
)(DeleteBoardConfirmation);
