import React, { Component } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../../Icons/DeleteIcon";
import CustomDialog from "./../CustomDialog";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../../Buttons/DefaultButton";
import CustomButton from "../../Buttons/CustomButton";
import WorkspaceDeleteConfirmation from "./WorkspaceDeleteConfirmation";
import ProjectDeleteConfirmation from "./ProjectDeleteConfirmation";
class DeleteConfirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDisable: false,
      workspaceMember: false,
      projectFlag: false,
      btnQuery: ''
    };
    this.handleClick = this.handleClick.bind(this);
    this.closeworkspaceMember = this.closeworkspaceMember.bind(this);
    this.closePreviousAction = this.closePreviousAction.bind(this);
  }
  closePreviousAction() {
    this.props.closeAction();
  }
  componentDidUpdate(prevProps) {
    const { btnQuery } = this.props;
    if (btnQuery !== prevProps.btnQuery && btnQuery !== this.state.btnQuery) {
      this.setState({ btnQuery });
    }
  }

  handleClick(event) {
    // this.props.DeleteTask(this.props.task);
    // this.props.closeAction();
    event.stopPropagation();
    if (this.props.view === "Workspace") {
      this.setState({ workspaceMember: true }, () => { this.props.closeAction() });
    }
    else if (this.props.view === "Project") {
      this.setState({ projectFlag: true }, () => { this.props.closeAction() });
    }
    else {
      if (!this.props.btnQuery)
        this.setState({ btnQuery: 'progress' });
      if (this.props.view === "Issue") {
        this.props.DeleteIssue(
          this.props.issue.id,
          () => {
            this.setState({ btnQuery: '' }, () => {
              this.props.closeAction(true);
            });
          },
          error => {
            if (!this.props.btnQuery)
              this.setState({ btnQuery: '' });
            if (error.status === 500) {
              // this.props.showSnackBar('Server throws error','error');
            }
          }
        );
      } else if (this.props.view === "Risk") {
        //Delete Risk API should be here
        this.props.DeleteRisk(this.props.risk.id, () => {
          this.setState({ btnQuery: '' }, () => {
            this.props.closeAction(true);
          });
        }, (error) => {
          if (!this.props.btnQuery)
            this.setState({ btnQuery: '' });
          if (error.status === 500) {
            // self.showSnackBar('Server throws error','error');
          }
        });
      } else if (this.props.view === "Meeting") {
        if (this.props.DeleteMeetingSchedule) {
          this.props.DeleteMeetingSchedule(
            this.props.meeting.meetingId,
            this.props.meeting.parentId,
            () => {
              this.setState({ btnQuery: '' }, () => {
                this.props.closeAction(null, true);
              });
            }
          );
        }
      } else if (this.props.handleBulkClick && this.props.inputType) {
        this.props.handleBulkClick(this.props.inputType);
        this.setState({ btnQuery: '' });
      } else if (this.props.inputType === "Decision") {
        this.props.closeAction(this.props.currentObject, true);
        this.setState({ btnQuery: '' });
      } else if (this.props.inputType === "Discussion") {
        this.props.closeAction(this.props.currentObject, true);
        this.setState({ btnQuery: '' });
      } else if (this.props.inputType === "Agenda") {
        this.props.closeAction(this.props.currentObject, true);
        this.setState({ btnQuery: '' });
      } else if (this.props.inputType === "FollowUp") {
        this.props.closeAction(this.props.currentObject, true);
        this.setState({ btnQuery: '' });
      } else if (this.props.view === "deleteMember") {
        this.props.deleteMember();
        this.setState({ btnQuery: '' });
      }
      else {
        this.props.handleAction();
      }
    }
  }
  closeworkspaceMember() {
    this.setState({ workspaceMember: false }, () => {
      this.props.closeAction();
    });
  }
  closeProjectDeleteForm = (event) => {
    if (event)
      event.stopPropagation();
    this.setState({ projectFlag: false })
    this.props.closeAction();
  }
  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      message,
      buttonName,
      view
    } = this.props;
    const { isDisable, workspaceMember, projectFlag, btnQuery } = this.state;
    return (
      <React.Fragment>
        <CustomDialog
          title={buttonName || "Delete"}
          dialogProps={{ open: open, onClose: closeAction, onClick: (e) => e.stopPropagation() }}
        >
          <div className={classes.centerAlignDialogContent}>
            <div className={classes.deleteConfirmationIconCnt}>
              <SvgIcon
                viewBox="0 0 512 512"
                className={classes.deleteConfirmationIcon}
                htmlColor={theme.palette.error.main}
              >
                <DeleteIcon />
              </SvgIcon>
            </div>
            <p className={classes.dialogMessageText}>
              <React.Fragment>{message}</React.Fragment>
            </p>
            {view === "Workspace" ? (
              <div>
                <p className={classes.dialogMessageText}>
                  Permanently deleting your workspace will automatically erase:
              </p>
                <p>- All created & assigned tasks</p>
                <p>- All created & assigned projects</p>
                <p>- All timesheets ( pending & approved)</p>
                <p>- All comments history</p>
                <p>- All file attachments</p>
                <p>- All meetings</p>
                <p>- All created & assigned issues and risks</p>
              </div>
            ) : view === "Project" ? (
              <div>
                <p className={classes.dialogMessageText}>
                  Are you sure you want to permanently delete this project?
                  Permanently deleting your project will automatically erase:
              </p>
                <p>- All created tasks in the project</p>
                <p>- All timesheets ( pending & approved)</p>
                <p>- All comments history</p>
                <p>- All file attachments</p>
                <p>- All meetings</p>
                <p>- All created & assigned issues and risks</p>
              </div>
            ) : null}
          </div>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            classes={{
              container: classes.dialogFormActionsCnt
            }}
          >
            <DefaultButton
              text="Cancel"
              buttonType="grayBtn"
              style={{ marginRight: 20 }}
              onClick={(e) => closeAction(e)}
            />
            <CustomButton
              onClick={(e) => this.handleClick(e)}
              btnType="danger"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress"}
            >
              {
                view === "Workspace" || view === "Project"
                  ? "Delete Anyway"
                  : view === "disable"
                    ? "Disable"
                    : view === "enable"
                      ? "Enable"
                      : "Delete"
              }
            </CustomButton>
          </Grid>

        </CustomDialog>
        <ProjectDeleteConfirmation
          open={projectFlag}
          classes={classes}
          theme={theme}
          view="Projects"
          message={"THIS ACTION IS PERMANENT."}
          closeAction={this.closeProjectDeleteForm}
          DeleteProject={this.props.DeleteProject}
          buttonName="Delete Project"
          project={this.props.project}
          FetchWorkSpaceData={this.props.FetchWorkSpaceData}
          showSnackbar={this.props.showSnackbar}
        />
        <WorkspaceDeleteConfirmation
          open={workspaceMember}
          classes={classes}
          theme={theme}
          view="Workspace"
          message={"THIS ACTION IS PERMANENT."}
          closeAction={this.closeworkspaceMember}
          deleteWorkspaceData={this.props.deleteWorkspace}
          buttonName="Delete Workspace"
          loggedInTeam={this.props.loggedInTeam}
          closePreviousAction={this.closePreviousAction}
        />
      </React.Fragment>
    )

  }
}

export default withStyles(dialogStyles, { withTheme: true })(
  DeleteConfirmation
);
