//@flow

import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import isEmpty from "lodash/isEmpty";
import PublicLink from "./PublicLink";
import { publickLinkDialog, profileSettingState } from "../../redux/actions/allDialogs";
import CustomDrawer2 from "../Drawer/CustomDrawer2";
import ProjectDetailDialog from "../../Views/Project/ProjectDetail/ProjectDetailsDialog";
import { projectDetails } from "../../redux/actions/projects";
import NewRiskDetailView from "../../Views/Risk/NewRiskDetailView/NewRiskDetailView";
import NewTaskDetailView from "../../Views/Task/NewTaskDetails/NewTaskDetails";
import NewIssueDetailView from "../../Views/Issue/NewIssueDetails/NewIssueDetails";
import DeleteConfirmDialog from "../Dialog/ConfirmationDialogs/DeleteConfirmationDialogue";
import ProfileSetting from "../../Views/ProfileSetting/ProfileSetting";

type AllDialogsProps = {
  publickLinkDialog: Function,
  dialogsState: Object,
  projectDetailsDialog: Object,
};

function AllDialogs(props: AllDialogsProps) {
  const {
    publickLinkDialog,
    projectDetailsDialog: {
      projectDetails,
      projectSettings,
      docked,
      fullView,
      sidePanel,
      chatView,
      activityLogView,
      docsView,
      dialog,
      loadingState,
    },
    intl,
    profileSettingDialog,
    googleCalendar,
    profileSettingState,
    showSnackBar,
  } = props;
  const {
    publicLink,
    riskDetailDialog,
    deleteDialogue,
    taskDetailDialog,
    issueDetailDialog,
    profileSettings,
  } = props.dialogsState;

  const handleProjectDetailsDialogClose = () => {
    props.projectDetails({
      projectDetails: {},
      projectSettings: {},
      fullView: fullView ? true : false,
      docked: docked ? true : false,
      dialog: false,
      loadingState: false,
    });
  };
  
  const closePublickLinkDialog = () => {
    publickLinkDialog(
      {}
    ); /** on closing dialog call actions with empty object, when dialog gets empty object it will close */
  };

  const handleDialogCloseAction = () => {
    profileSettingState({ open: false });
  };

  useEffect(() => {
    profileSettingState({ open: googleCalendar.googleIntegration });
  }, [googleCalendar.googleIntegration]);

  useEffect(() => {
    profileSettingState({ open: profileSettingDialog });
  }, [profileSettingDialog]);

  const projectDetailsCheck = isEmpty(projectDetails) ? false : true;

  return (
    <>
      {projectDetailsCheck && docked && dialog && (
        <CustomDrawer2
          open={true}
          drawerWidth={
            sidePanel
              ? "auto"
              : loadingState
              ? 1120
              : chatView || docsView || activityLogView
              ? 1180
              : 650
          }
          hideHeader={true}
          closeDrawer={handleProjectDetailsDialogClose}
          // headerTitle={checkListTask ? checkListTask.taskTitle : ""}
        >
          <ProjectDetailDialog
            open={projectDetails}
            handleDialogClose={handleProjectDetailsDialogClose}
            selectedProject={projectDetails}
            intl={intl}
          />
        </CustomDrawer2>
      )}
      {projectDetailsCheck && fullView && dialog && (
        <ProjectDetailDialog
          open={projectDetails}
          handleDialogClose={handleProjectDetailsDialogClose}
          selectedProject={projectDetails}
          intl={intl}
        />
      )}
      {!isEmpty(publicLink) ? (
        <PublicLink
          open={true}
          id={publicLink.taskId}
          taskTitle={publicLink.taskTitle}
          closeAction={closePublickLinkDialog}
          maxTitleWidth={500}
        />
      ) : null}

      {riskDetailDialog.id !== "" && <NewRiskDetailView />}
      {taskDetailDialog.id !== "" && <NewTaskDetailView />}
      {issueDetailDialog.id !== "" && <NewIssueDetailView />}
      {deleteDialogue.open && <DeleteConfirmDialog />}
      {profileSettings.open && (
        <ProfileSetting
          open={profileSettings.open}
          closeAction={handleDialogCloseAction}
          showSnackBar={showSnackBar}
          googleCalendar={googleCalendar}
          zoom={profileSettingDialog}
        />
      )}
    </>
  );
}

AllDialogs.defaultProps = {
  /** default props , calls when props are not coming from parent class undefined */
  dialogsState: {},
  publickLinkDialog: () => {},
  profileSettingState: () => {},
  showSnackBar: () => {},
  projectDetailsDialog: {},
};

const mapStateToProps = state => {
  return {
    dialogsState: state.dialogStates,
    projectDetailsDialog: state.projectDetailsDialog,
    googleCalendar: state.googleCalendar.data,
    profileSettingDialog: state.profile.data.openProfileSetting,
  };
};

export default connect(mapStateToProps, { publickLinkDialog, projectDetails, profileSettingState })(
  AllDialogs
);
