import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../Buttons/IconButton";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";

class CustomDialog extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      classes,
      theme,
      dialogProps,
      maxTitleWidth,
      subTitle,
      handleBackClick,
      backBtn,
      title,
      headerStyles,
      scroll = "body",
      hidetitle,
      style,
    } = this.props;

    return (
      <Dialog
        classes={{ paper: classes.dialogPaperCnt, scrollBody: classes.dialogCnt }}
        fullWidth={true}
        scroll={scroll}
        {...dialogProps}>
        {!hidetitle && (
          <DialogTitle id="form-dialog-title" classes={{ root: classes.defaultDialogTitle }}>
            <Grid container direction="row" justify="space-between" alignItems="center">
              <Grid
                item
                className={classes.dialogTitleWidth ? classes.dialogTitleWidth : null}
                style={headerStyles}>
                {backBtn && (
                  <div
                    className={classes.arrowBackCnt}
                    onClick={() => {
                      handleBackClick();
                    }}>
                    <ArrowBackIosIcon className={classes.arrowBackIcon} />
                  </div>
                )}
                <Typography variant="h2">{title}</Typography>
                {subTitle && <span className={classes.subTitle}>{subTitle}</span>}
              </Grid>
              <Grid item>
                <IconButton btnType="transparent" onClick={dialogProps.onClose}>
                  <CloseIcon
                    htmlColor={theme.palette.secondary.medDark}
                    style={{ fontSize: "18px" }}
                  />
                </IconButton>
              </Grid>
            </Grid>
          </DialogTitle>
        )}
        <DialogContent classes={{ root: classes.defaultDialogContent }}>
          {this.props.children}
        </DialogContent>
      </Dialog>
    );
  }
}
CustomDialog.defaultProps = {
  handleBackClick: () => {},
  subTitle: false,
  backBtn: false,
  title: "",
  headerStyles: {},
};

export default withStyles(dialogStyles, { withTheme: true })(CustomDialog);
