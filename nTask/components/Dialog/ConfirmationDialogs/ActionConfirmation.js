import React, { Component } from "react";
import CustomDialog from "../CustomDialog";
import confirmationDialogStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import ButtonActionsCnt from "./ButtonActionsCnt";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../../Icons/DeleteIcon";
import Typography from "@material-ui/core/Typography";
import ArchieveIcon from "../../Icons/ArchiveIcon";
import ImportExport from "@material-ui/icons/ImportExport";
import RecurrenceIcon from "../../Icons/RecurrenceIcon";
import TimerIcon from "@material-ui/icons/Timer";
import SignOutIcon from "@material-ui/icons/PowerSettingsNew";
import MarkAllIcon from "../../Icons/MarkAllIcon";
import { FormattedMessage } from "react-intl";

class ActionConfirmationDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      alignment,
      successAction,
      successBtnText,
      cancelBtnText,
      deleteAction,
      children,
      headingText,
      btnQuery,
      msgText,
      iconType,
      note,
      successBtnType,
      customIcon,
      btnProps,
      disableBackdropClick = false,
      disableEscapeKeyDown = false,
      teamInvitationDialog,
      rejectInvitation,
    } = this.props;
    const icons = {
      archive: (
        <div className={classes.archiveIconCnt}>
          <SvgIcon
            viewBox="0 0 512 512"
            className={classes.archiveConfirmationIcon}
            htmlColor={theme.palette.secondary.light}
          >
            <ArchieveIcon />
          </SvgIcon>
        </div>
      ),
      unarchive: (
        <div className={classes.archiveIconCnt}>
          <SvgIcon
            viewBox="0 0 512 512"
            className={classes.archiveConfirmationIcon}
            htmlColor={theme.palette.secondary.light}
          >
            <ArchieveIcon />
          </SvgIcon>
        </div>
      ),
      importExport: (
        <div className={classes.ieIconCnt}>
          <ImportExport
            className={classes.ieConfirmationIcon}
            htmlColor={theme.palette.secondary.light}
          />
        </div>
      ),
      recurrence: (
        <div className={classes.recurrenceIconCnt}>
          <SvgIcon
            viewBox="0 0 14 12.438"
            htmlColor={theme.palette.secondary.light}
            className={classes.recurrenceIcon}
          >
            <RecurrenceIcon />
          </SvgIcon>
        </div>
      ),
      timerIcon: (
        <div className={classes.ieIconCnt}>
          <TimerIcon
            className={classes.ieConfirmationIcon}
            htmlColor={theme.palette.secondary.light}
          />
        </div>
      ),
      signOutIcon: (
        <div className={classes.ieIconCnt}>
          <SignOutIcon
            className={classes.ieConfirmationIcon}
            htmlColor={theme.palette.secondary.light}
          />
        </div>
      ),
      markAll: (
        <div className={classes.markAllIconCnt}>
          <SvgIcon
            viewBox="0 0 80.72 80.72"
            className={classes.markAllConfirmationIcon}
            htmlColor={theme.palette.secondary.light}
          >
            <MarkAllIcon />
          </SvgIcon>
        </div>
      ),
    };
    return (
      <CustomDialog
        title={headingText}
        dialogProps={{
          open: open,
          onClick: (e) => {
            e.stopPropagation();
          },
          onClose: closeAction,
          PaperProps: {
            style: { maxWidth: 480 },
          },
          disableBackdropClick: disableBackdropClick,
          disableEscapeKeyDown: disableEscapeKeyDown,
        }}
      >
        <div className={classes[`${alignment}AlignContent`]}>
          {customIcon ? customIcon : icons[iconType]}

          {children ? (
            children
          ) : (
            <>
              <Typography
                variant="h5"
                align={alignment}
                style={{
                  userSelect: "none",
                  textOverflow: 'ellipsis',
                  overflow: 'hidden',
                  maxWidth: '100%',
                }}
              >
                {msgText}
              </Typography>
              {note ? (
                <Typography variant="caption" className={classes.noteText}>
                  <FormattedMessage
                    id="meeting.confirmations-dialog.note.label"
                    defaultMessage="Note:"
                  ></FormattedMessage>{" "}
                  {note}
                </Typography>
              ) : null}
            </>
          )}
        </div>
        <ButtonActionsCnt
          cancelAction={() => {
            teamInvitationDialog ? rejectInvitation() : closeAction();
          }}
          successAction={successAction}
          deleteAction={deleteAction}
          successBtnText={successBtnText}
          cancelBtnText={cancelBtnText}
          classes={classes}
          btnType={successBtnType ? successBtnType : "success"}
          btnQuery={btnQuery}
          {...btnProps}
        />
      </CustomDialog>
    );
  }
}
ActionConfirmationDialog.defaultProps = {
  rejectInvitation: () => { },
  teamInvitationDialog: false,
};

export default withStyles(confirmationDialogStyles, { withTheme: true })(
  ActionConfirmationDialog
);
