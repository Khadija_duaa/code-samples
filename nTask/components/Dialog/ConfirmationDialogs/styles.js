const confirmationDialogStyles = theme => ({
  centerAlignContent:{
    padding: "20px 60px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  leftAlignContent:{
    padding: 20
  },
  deleteIconCnt: {
    border: `1px solid ${theme.palette.border.redBorder}`,
    borderRadius: "50%",
    width: 90,
    height: 90,
    paddingLeft: 26,
    paddingTop: 23,
    marginBottom: 20,
    background: theme.palette.background.paper
  },
  archiveIconCnt: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 90,
    height: 90,
    paddingLeft: 22,
    paddingTop: 20,
    marginBottom: 20,
    background: theme.palette.background.paper
  },
ieIconCnt: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 90,
    height: 90,
    paddingLeft: 13,
    paddingTop: 12,
    marginBottom: 20,
    background: theme.palette.background.paper
  },
  recurrenceIconCnt: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 90,
    height: 90,
    paddingLeft: 21,
    paddingTop: 20,
    marginBottom: 20,
    background: theme.palette.background.paper
  },
  recurrenceIcon:{
    fontSize: "44px !important"
  },
  ieConfirmationIcon:{
    fontSize: "60px !important"
  },
  deleteConfirmationIcon:{
    fontSize: "50px !important"
  },
  archiveConfirmationIcon:{
    fontSize: "50px !important"
  },
  dialogFormActionsCnt:{
    padding: 20,
    background: "#F6F6F6",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: '0 0 6px 6px'
  },
  noteText:{
    marginTop: 10,
    textAlign: "center",
    userSelect: 'none'
  },
  markAllIconCnt: {
    marginBottom: 20,
  },
  markAllConfirmationIcon:{
    fontSize: "80px !important"
  },
  message:{
    color : "#202020",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "15px !important",
    lineHeight: 1.6
  },
  messageCnt:{
    textAlign : "center"
  },
  IconPermissionAlert:{
    fontSize: "80px !important",
    marginBottom: 10
  }
});

export default confirmationDialogStyles;
