import React, { Component } from "react";
import CustomButton from "../../Buttons/CustomButton";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import confirmationDialogStyles from "./styles";
import Button from "@material-ui/core/Button";
function ButtonActionsCnt(props) {
  const {
    classes,
    cancelAction,
    deleteAction,
    successAction,
    cancelBtnText,
    successBtnText,
    deleteBtnText,
    btnType,
    btnQuery,
    btnQueryDelete,
    deleteBtn = false,
    align,
    successBtnProps,
    deleteBtnProps,
    cancelBtnProps,
    disabled,
    btnTypeDelete = "danger",
    btnTypeVariant = "contained",
    deletePer=false,
    customGridProps={}
  } = props;
  const alignment = {
    left: "flex-start",
    right: "flex-end",
    center: "center",
  };
  return (
    <Grid
      container
      direction="row"
      justify={alignment[align || "right"]}
      alignItems="center"
      justify="space-between"
      classes={{
        container: classes.dialogFormActionsCnt,
      }}
      {...customGridProps}
    >
      {deleteBtnText ? (
        <CustomButton
          onClick={(e) => deleteAction(e)}
          btnType={btnTypeDelete}
          variant={btnTypeVariant}
          query={btnQueryDelete}
          disabled={btnQueryDelete == "progress" || deletePer || disabled}
          {...deleteBtnProps}
        >
          {deleteBtnText}
        </CustomButton>
      ) : (
        <div></div>
      )}
      <div>
        {cancelBtnText ? (
          <CustomButton
            onClick={(e) => cancelAction(e)}
            btnType="plain"
            variant="text"
            style={{ marginRight: 20 }}
            {...cancelBtnProps}
          >
            {cancelBtnText}
          </CustomButton>
        ) : null}
        {successBtnText ? <CustomButton
          onClick={(e) => successAction(e)}
          btnType={btnType}
          variant="contained"
          query={btnQuery}
          disabled={btnQuery == "progress" || disabled}
          {...successBtnProps}
        >
          {successBtnText}
        </CustomButton> : null}
      </div>
    </Grid>
  );
}

export default withStyles(confirmationDialogStyles, { withTheme: true })(
  ButtonActionsCnt
);
