import React, { Component } from "react";
import CustomDialog from "../CustomDialog";
import confirmationDialogStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import ButtonActionsCnt from "./ButtonActionsCnt";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../../Icons/DeleteIcon";
import Typography from "@material-ui/core/Typography";

class DeleteConfirmDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      alignment,
      successAction,
      successBtnText,
      cancelBtnText,
      children,
      headingText,
      btnQuery,
      msgText,
      disabled,
      styles,
      deleteBtnText,
      deleteBtnProps
    } = this.props;

    return (
      <CustomDialog
        title={headingText}
        dialogProps={{
          open: open,
          onClick: (e) => { e.stopPropagation() },
          onClose: closeAction,
          PaperProps: {
            style: { maxWidth: 530, ...styles }
          }
        }}
      >
        <div className={classes[`${alignment}AlignContent`]}>
          <div className={classes.deleteIconCnt}>
            <SvgIcon
              viewBox="0 0 512 512"
              className={classes.deleteConfirmationIcon}
              htmlColor={theme.palette.error.main}
            >
              <DeleteIcon />
            </SvgIcon>
          </div>
          {
            (children)
              ? children
              : <Typography variant="h5" align={alignment} style={{userSelect: 'none'}}>
                {
                  msgText
                }
              </Typography>
          }
        </div>
        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={successAction}
          successBtnText={successBtnText}
          cancelBtnText={cancelBtnText}
          deleteBtnText={deleteBtnText}
          deleteBtnProps={deleteBtnProps}
          btnType="danger"
          disabled={disabled}
          btnQuery={btnQuery}
        />
      </CustomDialog>
    );
  }
}

export default withStyles(confirmationDialogStyles, { withTheme: true })(
  DeleteConfirmDialog
);
