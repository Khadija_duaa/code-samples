import React, { Component } from "react";
import { compose } from "redux";
import CustomDialog from "../CustomDialog";
import confirmationDialogStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import ButtonActionsCnt from "./ButtonActionsCnt";
import SvgIcon from "@material-ui/core/SvgIcon";
import IconPermissionAlert from "../../Icons/IconPermissionAlert";
import Typography from "@material-ui/core/Typography";
import { FormattedMessage, injectIntl } from "react-intl";

class PermissionAlert extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      successAction,
      btnQuery,
      disabled,
      deleteBtnText,
      deleteBtnProps,
      type,
      successBtnText,
      deleteView
    } = this.props;

    return (
      <CustomDialog
        title={<FormattedMessage id="common.permission.alert" defaultMessage="Permission Alert" />}
        dialogProps={{
          open: open,
          onClick: e => {
            e.stopPropagation();
          },
          onClose: closeAction,
          PaperProps: {
            style: { maxWidth: 530 },
          },
        }}>
        <div className={classes[`centerAlignContent`]}>
          <div>
            <SvgIcon
              viewBox="0 0 80 80"
              className={classes.IconPermissionAlert}
              >
              <IconPermissionAlert />
            </SvgIcon>
          </div>
          <div className={classes.messageCnt}>
            <span className={classes.message}>
              {`Looks like you don't have permission rights to perform this action on some of the ${type}.`}
            </span>
          </div>
          <div className={classes.messageCnt}>
            <span className={classes.message}>
              {`If you continue, this action will only be applied to those ${type} on which you have permission rights.`}
            </span>
          </div>
        </div>
        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={successAction}
          successBtnText={successBtnText}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          deleteBtnText={deleteBtnText}
          deleteBtnProps={deleteBtnProps}
          btnType={deleteView ? "danger" : "success"}
          disabled={disabled}
          btnQuery={btnQuery}
        />
      </CustomDialog>
    );
  }
}
PermissionAlert.defaultProps = {
  type: "tasks",
  disabled: false,
  btnQuery: "",
  successAction: () => {},
  closeAction: () => {},
  deleteBtnText: "",
  open: false,
  deleteBtnProps: null,
  successBtnText:null,
  deleteView: false
};

export default compose(withStyles(confirmationDialogStyles, { withTheme: true }))(PermissionAlert);
