import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import CustomDialog from "./CustomDialog";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../Buttons/DefaultButton";
import CustomButton from "../Buttons/CustomButton";
import DefaultTextField from "../Form/TextField";
import { PublishMomEmail } from "../../redux/actions/meetings";

import { filterMutlipleEmailsInput, validateMultifieldEmails } from '../../utils/formValidations';
import getErrorMessages from '../../utils/constants/errorMessages';

class MomEmailDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      EmailAddressError: '',
      EmailAddressErrorText: '',
      emailList: '',
      btnQuery: ''
    };
    this.initialState = this.state;
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleInviteSend = this.handleInviteSend.bind(this);
  }
  handleInput = name => event => {
    let inputValue = event.target.value;
    // localStorage.setItem("MomEmails", inputValue.trim());
    inputValue = filterMutlipleEmailsInput(inputValue);
    this.setState({ [name]: inputValue, EmailAddressError: false, EmailAddressErrorText: "" });
  };
  handleSelectChange(value) {
    this.setState({ feedbackType: value });
  }
  handleInviteSend() {
    let validationObj = validateMultifieldEmails(this.state.emailList);

    if (validationObj['isError']) {
      this.setState({
        EmailAddressError: validationObj['isError'],
        EmailAddressErrorText: validationObj['message']
      });
    }
    else {
      const props = this.props;
      this.setState({
        EmailAddressError: false,
        EmailAddressErrorText: '',
        btnQuery: 'progress'
      }, () => {
        let emailListObj = {
          emails: validationObj['data'],
          meetingId: props.currentObject.meetingId,
        };
        if (props.currentObject.taskId) {
          emailListObj.taskId = props.currentObject.taskId
        }
        props.PublishMomEmail(emailListObj,
          (response) => {
            this.setState({ btnQuery: '', emailList: '' });
            //  localStorage.removeItem("MomEmails");
            props.showSnackBar("Email is sent successfully.", 'success');
            props.closeAction();
          },
          (error) => {
            this.setState({ btnQuery: '' });
            if (error.status === 500) {
              props.showSnackBar('Server throws error', 'error');
            }
            else {
              props.showSnackBar(error.data.message, 'error');
            }
          });
      });
    }
  }
  render() {
    const { classes, theme, open, closeAction } = this.props;
    const { EmailAddressError, EmailAddressErrorText, emailList, btnQuery } = this.state;
    return (
      <CustomDialog
        title="Send Minutes of Meeting"
        dialogProps={{
          open: open, onClose: closeAction,
          onClick: (e) => e.stopPropagation(),
          onExited: () => {
            this.setState(this.initialState)
          },
          onEnter: () => {
            // this.setState({
            //   emailList: localStorage.getItem("MomEmails") || ""
            // });
          }
        }
        }
      >
        <div className={classes.feedbackDialogContentCnt}>

          <DefaultTextField
            fullWidth={true}
            labelProps={{
              shrink: true
            }}
            errorState={EmailAddressError}
            errorMessage={EmailAddressErrorText}
            defaultProps={{
              id: "emailList",
              onChange: this.handleInput("emailList"),
              value: emailList,
              multiline: true,
              rows: 7,
              placeholder: "Enter email addresses separated by commas.",
              autoFocus: true
            }}
          />

          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
          >

            <DefaultButton
              text="Cancel"
              onClick={closeAction}
              buttonType="Transparent"
              style={{ marginRight: 20 }}
            />
            <CustomButton
              onClick={this.handleInviteSend}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress"}
            >
              {"Send Email"}
            </CustomButton>
          </Grid>
        </div>
      </CustomDialog>
    );
  }
}


export default compose(
  withRouter,
  withStyles(dialogStyles, { withTheme: true }),
  connect(
  null,
    {
      PublishMomEmail
    }
  )
)(MomEmailDialog);