import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import MoveIcon from "../../Icons/MoveIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import DefaultTextField from "../../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";

class CreateNewWorkspace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: false
    };
  }
  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };
  render() {
    const {
      classes,
      theme,
      password,
      passwordError,
      passwordErrorMsg,
      showPassword,
      passwordInputAction
    } = this.props;

    return (
      <>
        <div className={classes.moveIconCnt}>
          <SvgIcon
            viewBox="0 0 40 40"
            htmlColor={theme.palette.secondary.medDark}
            className={classes.moveIcon}
          >
            <MoveIcon />
          </SvgIcon>
        </div>
        <Typography variant="h2" align="center" className={classes.mainHeading}>
          This action is permanent
        </Typography>
        <Typography variant="body2">
          <span className={classes.boldText}>WARNING:</span>
          &nbsp;You CANNOT undo this action or revert the data back to personal
          workspace.
        </Typography><br />
        <Typography variant="body2" style={{marginBottom: 20}}>
          Type your password in the field below to initiate data transition.
        </Typography>
        <DefaultTextField
          label="Password"
          errorState={passwordError}
          errorMessage={passwordErrorMsg}
          defaultProps={{
            id: "password",
            onChange: passwordInputAction,
            placeholder: "Enter Password",
            type: this.state.showPassword ? "text" : "password",
            value: password,
            inputProps: { maxLength: 40 },
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  disableRipple={true}
                  classes={{ root: classes.passwordVisibilityBtn }}
                  onClick={this.handleClickShowPassword}
                >
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            )
          }}
        />
      </>
    );
  }
}

export default withStyles(dialogStyles, { withTheme: true })(
  CreateNewWorkspace
);
