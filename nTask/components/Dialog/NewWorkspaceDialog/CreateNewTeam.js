import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import WorkspaceIcon from "../../Icons/WorkspaceIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import DefaultTextField from "../../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import TeamIcon from "@material-ui/icons/SupervisorAccount";
import GlobeIcon from "@material-ui/icons/Language";
class CreateNewTeam extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teamUrl: ""
    };
  }
  validInput = (key, value) => {
    /**Function for checking if the field is empty or not */
    let validationObj = isEmpty(value);
    if (validationObj) {
      switch (key) {
        case "teamName":
          this.setState({
            teamError: true,
            teamErrorMessage: "Oops! Please enter team name."
          });
          break;
        case "teamUrl":
          this.setState({
            teamUrlError: true,
            teamUrlErrorMessage: "Oops! Please enter team url."
          });
          break;

        default:
          break;
      }
    }
    return validationObj;
  };
  render() {
    const {
      classes,
      theme,
      teamNameErrorMessage,
      teamNameError,
      teamUrlErrorMessage,
      teamUrlError,
      teamUrl,
      teamName,
      handleInput
    } = this.props;

    return (
      <>
        <div className={classes.workspaceIconCnt}>
          <SvgIcon
            viewBox="0 0 32 31"
            htmlColor={theme.palette.secondary.medDark}
            className={classes.workspaceIcon}
          >
            <WorkspaceIcon />
          </SvgIcon>
        </div>
        <Typography variant="h2" align="center" className={classes.mainHeading}>
          Create New Team
        </Typography>

        <DefaultTextField
          label="Team Name"
          // helptext={onboardingHelpText.workspaceNameHelpText}
          fullWidth={true}
          errorState={teamNameError}
          errorMessage={teamNameErrorMessage}
          defaultProps={{
            id: "teamName",
            onChange: handleInput("teamName"),
            placeholder: "Team Name",
            value: teamName,
            autoFocus: true,
            inputProps: {
              maxLength: 50,
              tabIndex: 1,
              style: { paddingLeft: 0 }
            },
            startAdornment: (
              <InputAdornment position="start">
                <TeamIcon
                  htmlColor={theme.palette.secondary.dark}
                  className={classes.teamIcon}
                />
              </InputAdornment>
            )
          }}
        />
        <DefaultTextField
          label="Team URL"
          // helptext={onboardingHelpText.workspaceUrlHelpText}
          fullWidth={true}
          errorState={teamUrlError}
          errorMessage={teamUrlErrorMessage}
          defaultProps={{
            id: "teamUrl",
            onChange: handleInput("teamUrl"),
            placeholder: "team URL",
            value: teamUrl,
            inputProps: {
              style: { paddingLeft: 0 },
              maxLength: 80,
              tabIndex: 4
            },
            startAdornment: (
              <InputAdornment
                position="start"
                classes={{ root: classes.urlAdornment }}
              >
                <GlobeIcon
                  htmlColor={theme.palette.secondary.dark}
                  className={classes.globeIcon}
                />
                <p className={classes.workspaceUrlAdornment}>
                  {window.location.origin+"/"}
                </p>
              </InputAdornment>
            )
          }}
        />
      </>
    );
  }
}

export default withStyles(dialogStyles, { withTheme: true })(CreateNewTeam);
