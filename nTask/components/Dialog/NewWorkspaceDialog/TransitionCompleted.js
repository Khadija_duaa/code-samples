import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import TickIcon from "../../Icons/TickIcon";
import SvgIcon from "@material-ui/core/SvgIcon";

class TransitionCompleted extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      classes,
      theme,
    } = this.props;

    return (
      <>
        <div className={classes.tickIconCnt}>
          <SvgIcon
            viewBox="0 0 35.531 26.094"
            htmlColor={theme.palette.primary.light}
            className={classes.tickIcon}
          >
            <TickIcon />
          </SvgIcon>
        </div>
        <Typography variant="h2" align="center" className={classes.mainHeading}>
          Data Migration Completed
        </Typography>
        <Typography variant="body2" align="center">
          All projects, tasks, meetings, timesheets, issues and risks have been
          successfully moved.
        </Typography>
      </>
    );
  }
}

export default withStyles(dialogStyles, { withTheme: true })(
  TransitionCompleted
);
