import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import WorkspaceIcon from "../../Icons/WorkspaceIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import DefaultTextField from "../../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import GlobeIcon from "@material-ui/icons/Language";

class CreateNewWorkspace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wsUrl: ""
    };
  }

  render() {
    const {
      classes,
      theme,
      wsNameErrorMessage,
      wsNameError,
      wsUrlErrorMessage,
      wsUrlError,
      wsUrl,
      wsName,
    } = this.props;

    return (
      <>
        <div className={classes.workspaceIconCnt}>
          <SvgIcon
            viewBox="0 0 32 31"
            htmlColor={theme.palette.secondary.medDark}
            className={classes.workspaceIcon}
          >
            <WorkspaceIcon />
          </SvgIcon>
        </div>
        <Typography variant="h2" align="center" className={classes.mainHeading}>
          Create New Workspace
        </Typography>
        <DefaultTextField
          label="Workspace Name"
          errorState={wsNameError}
          errorMessage={wsNameErrorMessage}
          defaultProps={{
            id: "WorkspaceName",
            onChange: this.props.handleWsNameInput,
            placeholder: "Workspace Name",
            type: "text",
            value: wsName,
            inputProps: {
              maxLength: 80
            }
          }}
        />
        <DefaultTextField
          label="Workspace URL"
          errorState={wsUrlError}
          errorMessage={wsUrlErrorMessage}
          defaultProps={{
            id: "WorkspaceUrl",
            onChange: this.props.handleWsUrlInput,

            placeholder: "workspace URL",
            type: "text",
            value: wsUrl,
            inputProps: {
              style: { paddingLeft: 0 },
              maxLength: 80
            },
            startAdornment: (
              <InputAdornment
                position="start"
                classes={{ root: classes.urlAdornment }}
              >
                <GlobeIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.globeIcon}
                  />
                <p className={classes.workspaceUrlAdornment}>
                  {window.location.origin+"/"}
                </p>
              </InputAdornment>
            )
          }}
        />
      </>
    );
  }
}

export default withStyles(dialogStyles, { withTheme: true })(
  CreateNewWorkspace
);
