import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import NotificationMessage from "../../NotificationMessages/NotificationMessages";
import RiskIcon from "../../Icons/RiskIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import DefaultCheckbox from "../../Form/Checkbox";
import ButtonActionsCnt from "../ConfirmationDialogs/ButtonActionsCnt";
import CreateNewWorkspace from "./CreateNewWorkspace";
import CreateNewTeam from "./CreateNewTeam";
import EnterPassword from "./EnterPassword";
import TransitionCompleted from "./TransitionCompleted";
import {
  validateUrlField,
  validateTitleField,
  validatePasswordField,
} from "../../../utils/formValidations";
import {
  IsUserTeamUrlValid,
  MoveDataToWorkspace,
} from "../../../redux/actions/teamMembers";
import { isEmpty } from "validator";
import { authenticatePassword } from "../../../redux/actions/authentication";
import { FetchUserInfo } from "../../../redux/actions/profile";
import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";
import { switchTeam } from "../../../redux/actions/teamMembers";
import { FormattedMessage, injectIntl } from "react-intl";
class MoveWorkspaceData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      permissionChecked: false,
      wsUrlErrorMessage: false,
      wsUrlError: false,
      wsUrl: "",
      wsNameErrorMessage: false,
      wsNameError: false,
      wsName: "",
      wsNameBind: true,
      step: 0,
      password: "",
      passwordError: false,
      passwordErrorMsg: "",
      btnQuery: "",
      teamNameBind: true,
    };
  }
  handleCancelAction = () => {
    this.setState({ step: this.state.step - 1 });
  };
  validInput = (key, value) => {
    /**Function for checking if the field is empty or not */
    let validationObj = isEmpty(value);
    if (validationObj) {
      switch (key) {
        case "teamName":
          this.setState({
            teamError: true,
            teamErrorMessage: "Oops! Please enter team name.",
          });
          break;
        case "teamUrl":
          this.setState({
            teamUrlError: true,
            teamUrlErrorMessage: "Oops! Please enter team url.",
          });
          break;

        default:
          break;
      }
    }
    return validationObj;
  };
  handleStep = (event, step) => {
    //accepts step as a number 0,1,2 to switch the step when click on the action button
    event.stopPropagation();
    if (step === 0) {
      this.setState({ step: step });
    } else if (step === 1) {
      this.setState({ step: step });
    } else if (step == 2) {
      this.setState({ btnQuery: "progress" });
      const { teamName = "", teamUrl = "" } = this.state;
      let teamNameValidate = this.validInput("teamName", teamName);
      let teamUrlValidate = this.validInput("teamUrl", teamUrl);
      if (!teamNameValidate && !teamUrlValidate) {
        //Checking if team url or team name already exists in db
        this.props.IsUserTeamUrlValid(
          { teamName, teamUrl },
          //Success callback
          () => {
            this.setState({ step, btnQuery: "" });
          },
          //Failure Callback
          (error) => {
            //Condition to check if error is of team URL already exists
            if (error.data.message == "Team URL already exists.") {
              this.setState({
                teamUrlErrorMessage:
                  "Team URL already exists, Please choose a different url",
                teamUrlError: true,
                btnQuery: "",
              });
            } else if (error.data.message == "Team name already exists.") {
              //Condition to check if team name lready exists
              this.setState({
                teamNameErrorMessage: this.props.intl.formatMessage({
                  id: "common.team.validations.nameexists",
                  defaultMessage: error.data.message,
                }),
                teamNameError: true,
                btnQuery: "",
              });
            } else {
              this.setState({ step, btnQuery: "" });
            }
          }
        );
      }
    } else if (step === 3) {
      //Checking if workspace url format is valid
      if (!this.validateWorkspaceNameUrl()) return;
      else {
        this.setState({ step });
      }
    } else if (step === 4) {
      const obj = {
        WorkspaceName: this.state.wsName,
        WorkspaceUrl: this.state.wsUrl,
        TeamName: this.state.teamName,
        TeamUrl: this.state.teamUrl,
        MoveDataAgreement: this.state.permissionChecked,
      };

      this.setState({ btnQuery: "progress" });

      this.props.MoveDataToWorkspace(
        obj,
        //Success Callback
        (response) => {
          if (response) {
            this.props.FetchUserInfo((resp) => {
              //Fetching workspaces
              this.props.FetchWorkspaceInfo(null, () => {
                this.props.closeAction();
                this.setState({ btnQuery: "" });
              });
            });
          }
        },
        //Failure Callback
        (error) => {
          this.setState({ btnQuery: "progress" });
        }
      );
      // this.setState({ step: step });
    }
  };
  validateWorkspaceNameUrl = () => {
    let isNameError = false,
      isUrlError = false;
    //////////////////////////////////// WORKSPACE NAME ////////////////////////////////////
    let workspaceNameValue = this.state.wsName.trim();
    let validationObj = validateTitleField(
      "workspace name",
      workspaceNameValue
    );
    if (validationObj["isError"]) {
      isNameError = true;
      this.setState({
        wsNameError: validationObj["isError"],
        wsNameErrorMessage: validationObj["message"],
        isButtonDisabled: false,
      });
    } else {
      this.setState({
        wsNameError: false,
        wsNameErrorMessage: "",
      });
    }

    //////////////////////////////////// WORKSPACE URL ////////////////////////////////////
    let workspaceUrlValue = this.state.wsUrl.trim();
    validationObj = validateUrlField("workspace URL", workspaceUrlValue);
    if (validationObj["isError"]) {
      isUrlError = true;
      this.setState({
        wsUrlError: validationObj["isError"],
        wsUrlErrorMessage: validationObj["message"],
        isButtonDisabled: false,
      });
    } else {
      this.setState({
        wsUrlError: false,
        wsUrlErrorMessage: "",
      });
    }

    if (isNameError || isUrlError) {
      return false;
    } else {
      return workspaceUrlValue;
    }
  };
  handleChange = (event) => {
    // function to toggle checked and unchecked state of checkbox
    this.setState({ permissionChecked: event.target.checked });
  };
  handleWsUrlInput = (event) => {
    // function to handle on change of workspace url input
    let wsUrl = event.target.value.replace(/ /g, "");
    this.setState({ wsUrl: wsUrl, wsNameBind: false });
  };
  handlePasswordInput = (event) => {
    // function to handle on change of password input
    this.setState({ password: event.target.value });
  };
  handleWsNameInput = (event) => {
    // function to handle on change of workspace name input
    let trimUrl = event.target.value.replace(/[\s\+]+/g, "-").toLowerCase();
    this.setState({ wsName: event.target.value });
    if (this.state.wsNameBind) {
      this.setState({ wsUrl: trimUrl });
    }
  };
  //Handle change for create new team view input fields
  handleInput = (fieldName) => (event) => {
    let inputValue = event.target.value;
    const { teamNameBind } = this.state;
    //* Replacing all spaces with Dash from team url
    let teamUrl = inputValue.replace(/[\s\+]+/g, "-").toLowerCase();
    switch (fieldName) {
      case "teamName": {
        if (teamNameBind) {
          this.setState({
            [fieldName]: inputValue,
            teamError: false,
            teamErrorMessage: "",
            teamNameBind: true,
            teamUrl: teamUrl,
          });
        } else {
          /** */
          this.setState({
            [fieldName]: inputValue,
            teamError: false,
            teamErrorMessage: "",
          });
        }
        break;
      }
      case "teamUrl": {
        //Disallowing user to enter space in url
        let newTeamUrl = inputValue.replace(/ /g, "");
        this.setState({
          [fieldName]: newTeamUrl,
          teamUrlError: false,
          teamUrlErrorMessage: "",
        });
        if (this.state.teamNameBind) {
          this.setState({ teamNameBind: false });
        }
      }
    }
  };
  render() {
    const { classes, theme, open, closeAction, acceptBtnAction } = this.props;
    const {
      permissionChecked,
      wsName,
      wsNameError,
      wsNameErrorMessage,
      wsUrl,
      wsUrlError,
      wsUrlErrorMessage,
      teamName,
      teamNameError,
      teamNameErrorMessage,
      teamUrl,
      teamUrlError,
      teamUrlErrorMessage,
      step,
      passwordError,
      passwordErrorMsg,
      password,
      btnQuery,
    } = this.state;
    return (
      <Dialog
        classes={{
          paper: classes.MoveWorkspaceDialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        fullWidth={true}
        scroll="body"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={closeAction}
      >
        <DialogContent classes={{ root: classes.betaDialogCnt }}>
          {step == 0 ? (
            <>
              <div className={classes.riskIconCnt}>
                <SvgIcon
                  viewBox="0 0 18 15.75"
                  htmlColor={theme.palette.error.dark}
                  className={classes.riskIcon}
                >
                  <RiskIcon />
                </SvgIcon>
              </div>
              <Typography
                variant="h6"
                align="center"
                className={classes.moveWorkspaceMessage}
              >
                <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.personal-data.confiramtion-dialog.title"
                  defaultMessage="You are about to move your personal data to a new workspace under a team."
                />
              </Typography>
              <NotificationMessage type="info" iconType="info">
                <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.personal-data.confiramtion-dialog.label"
                  defaultMessage="All personal data created by you including projects, tasks, issues, risks, meetings, etc. will be moved to a new workspace under a team once the
                migration is complete."
                />
              </NotificationMessage>
              <div style={{ width: "100%" }}>
                <DefaultCheckbox
                  checked={permissionChecked}
                  onChange={this.handleChange}
                  label={
                    <FormattedMessage
                      id="profile-settings-dialog.apps-integrations.personal-data.confiramtion-dialog.terms.label"
                      defaultMessage="I understand and accept."
                    />
                  }
                  checkboxStyles={{ padding: "0 12px 0 0" }}
                  styles={{
                    marginTop: 20,
                    display: "flex",
                    alignItems: "center",
                  }}
                />
              </div>
            </>
          ) : step == 1 ? (
            <CreateNewTeam
              teamUrlErrorMessage={teamUrlErrorMessage}
              teamUrlError={teamUrlError}
              teamUrl={teamUrl}
              teamName={teamName}
              teamNameError={teamNameError}
              teamNameErrorMessage={teamNameErrorMessage}
              handleInput={this.handleInput}
            />
          ) : step == 2 ? (
            <CreateNewWorkspace
              wsUrlErrorMessage={wsUrlErrorMessage}
              wsUrlError={wsUrlError}
              wsUrl={wsUrl}
              wsName={wsName}
              wsNameError={wsNameError}
              wsNameErrorMessage={wsNameErrorMessage}
              handleWsUrlInput={this.handleWsUrlInput}
              handleWsNameInput={this.handleWsNameInput}
            />
          ) : // ) : step == 3 ? (
          //   <EnterPassword
          //     passwordInputAction={this.handlePasswordInput}
          //     password={password}
          //     passwordError={passwordError}
          //     passwordErrorMsg={passwordErrorMsg}
          //   />
          // )
          step == 3 ? (
            <TransitionCompleted />
          ) : null}
        </DialogContent>
        {step == 0 ? (
          <ButtonActionsCnt
            successBtnText={
              <FormattedMessage
                id="profile-settings-dialog.apps-integrations.personal-data.confiramtion-dialog.save-button.label"
                defaultMessage="Proceed"
              />
            }
            btnType="success"
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            align="right"
            successAction={(event) => this.handleStep(event, 1)}
            successBtnProps={{ disabled: !permissionChecked }}
            cancelAction={closeAction}
          />
        ) : step == 1 ? (
          <ButtonActionsCnt
            successBtnText="Create"
            btnType="success"
            cancelBtnText="Back"
            align="right"
            successAction={(event) => this.handleStep(event, 2)}
            successBtnProps={{
              disabled: !teamName ? true : !teamUrl ? true : false,
            }}
            cancelAction={this.handleCancelAction}
            btnQuery={btnQuery}
          />
        ) : step == 2 ? (
          <ButtonActionsCnt
            successBtnText="Create"
            btnType="success"
            cancelBtnText="Back"
            align="right"
            successAction={(event) => this.handleStep(event, 3)}
            successBtnProps={{
              disabled: !wsName || btnQuery ? true : !wsUrl ? true : false,
            }}
            cancelAction={this.handleCancelAction}
            btnQuery={btnQuery}
          />
        ) : step == 3 ? (
          <ButtonActionsCnt
            successBtnText={`Go to ${wsName}`}
            btnType="success"
            align="center"
            successAction={(event) => this.handleStep(event, 4)}
            cancelAction={this.handleCancelAction}
            btnQuery={btnQuery}
          />
        ) : null}
      </Dialog>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    profileState: state.profile,
  };
};
export default compose(
  withRouter,
  injectIntl,
  withStyles(dialogStyles, { withTheme: true }),
  connect(mapStateToProps, {
    IsUserTeamUrlValid,
    MoveDataToWorkspace,
    authenticatePassword,
    FetchWorkspaceInfo,
    FetchUserInfo,
  })
)(MoveWorkspaceData);
