import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import ntaskLogo from "../../assets/images/nTask_logo_getStarted.png";
import CustomButton from "../Buttons/CustomButton";
import CelebrationBg from "../../assets/images/celebrationBg.png"
class BusinessPlanNotifDialog extends Component {
  constructor(props) {
    super(props);
    this.state
  }

  render() {
    const { classes, theme, open, closeAction } = this.props;

    return (
      <Dialog
        classes={{
          paper: classes.planDialogPaperCnt,
          scrollBody: classes.dialogCnt
        }}
        fullWidth={true}
        scroll="body"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={closeAction}
      >
        <DialogContent classes={{ root: classes.planDialogCnt }}>
          <div style={{ textAlign: "center", marginBottom: 30 }}>
            <img src={ntaskLogo} />
          </div>

          <Typography variant="h1" align="center" className={classes.planDialogHeading}>
          Hey! Didn't have time to explore our business plan to date? We are extending our business plan's FREE offering which you can enjoy until it expires on the 1st of December, 2019.
          </Typography>
         <br />
          <Typography
            variant="body1"
            className={classes.planDialogSubText}
            align="center"
          >
            <b>Billing is on the house!</b>
          </Typography>

          <CustomButton
            btnType="success"
            variant="contained"
            style={{ marginTop: 20, padding: "14px 40px" }}
            onClick={closeAction}
          >
            Get Started
          </CustomButton>
          <br />
          <Typography
            variant="body2"
            className={classes.planDialogSubText}
            align="center"
          >
            Note: After the expiry of this offer you will be shifted to the free plan with the option of upgrading at your convenience.
          </Typography>
        </DialogContent>
      </Dialog>
    );
  }
}

export default withStyles(dialogStyles, { withTheme: true })(BusinessPlanNotifDialog);
