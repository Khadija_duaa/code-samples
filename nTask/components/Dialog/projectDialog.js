import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../Buttons/IconButton";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Slide from "@material-ui/core/Slide";
import SvgIcon from "@material-ui/core/SvgIcon";
import DockIcon from "../Icons/DockIcon";
import ToggleUpdateIcon from "../Icons/ToggleUpdateIcon";
import DocumentIcon from "../Icons/DocumentIcon";
import DefaultTextField from "../Form/TextField";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import ColorIcon from "../Icons/ColorIcon";
import DefaultButton from "../Buttons/DefaultButton";
import { BlockPicker } from "react-color";
import FormHelperText from "@material-ui/core/FormHelperText";
import Hotkeys from "react-hot-keys";
import StatusDropdown from "../Dropdown/StatusDropdown/Dropdown";
import CustomTooltip from "../Tooltip/Tooltip";
import ColorPickerDropdown from "../Dropdown/ColorPicker/Dropdown";
import CustomIconButton from "../Buttons/CustomIconButton";
import {FormattedMessage} from "react-intl";
import { CanAccessFeature } from "../AccessFeature/AccessFeature.cmp";
class projectDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editProjectName: true,
      statusDropDownData: [],
      selectedStatus: {},
    };
  }

  handleProjectNameEdit = () => {
    this.setState({ editProjectName: !editProjectName });
  };

  render() {
    const {
      classes,
      theme,
      dialogProps,
      newProject = false,
      fullScreen = false,
      borderRadius = "0px",
      projectErrorState = {},
      projectTitle,
      handleTextFieldChange,
      title,
      colorPicker = false,
      selectedColor = "",
      handleStatusSelect,
      projectStatus = {},
      statusDropDownData = [],
    } = this.props;
    const { editProjectName, selectedStatus } = this.state;

    return (
      <Dialog
        {...dialogProps}
        classes={{
          paper: classes.dialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        fullWidth={true}
        fullScreen={fullScreen}
        scroll="body"
      >
        <div
          id="mainContentCntProject"
          style={{ background: "white", borderRadius: borderRadius }}
        >
          <DialogTitle
            id="form-dialog-title"
            classes={{ root: classes.defaultDialogTitle }}
          >
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Grid item>
                {newProject || editProjectName ? (
                  <>
                    <OutlinedInput
                      labelWidth={150}
                      notched={false}
                      value={projectTitle}
                      onKeyDown={this.props.onKeyDownProjectName}
                      autoFocus={newProject} /** if new project true then auto focus true , otherwise auto focus false */
                      onChange={handleTextFieldChange}
                      onBlur={this.props.handleProjectNameClickAway}
                      placeholder={intl.formatMessage({id:"project.creation-dialog.project-title.placeholder", defaultMessage:"Enter project title"})}
                      style={{ width: 340 }}
                      inputProps={{ maxLength: 80 }}
                      classes={{
                        root: classes.outlinedInputCnt,
                        input: classes.outlinedProjectInput,
                        notchedOutline: classes.notchedOutlineCntt,
                        focused: classes.outlineInputFocus,
                      }}
                    />
                    {projectErrorState.projectTitleError && (
                      <FormHelperText classes={{ root: classes.errorText }}>
                        {projectErrorState.projectTitleErrorMessage || ""}
                      </FormHelperText>
                    )}
                  </>
                ) : (
                  <Typography
                    style={{ lineHeight: "47px" }}
                    variant="h2"
                    onClick={this.handleProjectNameEdit}
                  >
                    {title}
                  </Typography>
                )}
              </Grid>
              <Grid item>
              <CanAccessFeature group='project' feature='status'>
                <StatusDropdown
                  onSelect={handleStatusSelect}
                  option={projectStatus}
                  options={statusDropDownData}
                />
              </CanAccessFeature>

                <ColorPickerDropdown
                  theme={theme}
                  classes={classes}
                  selectedColor={selectedColor}
                  onSelect={this.props.colorChange}
                />

                {!newProject /** if new project false/ already task in the system then show's other icons */ && (
                  <>
                    <CustomTooltip
                      helptext={<FormattedMessage id="project.tooltips.updates" defaultMessage="Updates"/>}
                      iconType="help"
                      placement="top"
                      style={{ color: theme.palette.common.white }}
                    >
                      <CustomIconButton
                        onClick={() => {
                          this.props.openProjectDetailView();
                        }}
                        btnType="transparent"
                        variant="contained"
                      >
                        <SvgIcon
                          viewBox="-1 2 24 21"
                          htmlColor={theme.palette.secondary.medDark}
                          className={classes.fullScreenIcon}
                        >
                          <ToggleUpdateIcon />
                        </SvgIcon>
                      </CustomIconButton>
                    </CustomTooltip>

                    <CustomTooltip
                      helptext={<FormattedMessage id="project.tooltips.documents" defaultMessage="Documents"/>}
                      iconType="help"
                      placement="top"
                      style={{ color: theme.palette.common.white }}
                    >
                      <CustomIconButton
                        onClick={() => {}}
                        btnType="transparent"
                        variant="contained"
                      >
                        <SvgIcon
                          viewBox="0 5 25 17"
                          className={classes.fullScreenIcon}
                        >
                          <DocumentIcon />
                        </SvgIcon>
                      </CustomIconButton>
                    </CustomTooltip>

                    <CustomTooltip
                      helptext={<FormattedMessage id="project.tooltips.dock-view" defaultMessage="Dock View"/>}
                      iconType="help"
                      placement="top"
                      style={{ color: theme.palette.common.white }}
                    >
                      <CustomIconButton
                        onClick={() => {
                          this.props.handleFullScreen();
                        }}
                        btnType="transparent"
                        variant="contained"
                      >
                        <SvgIcon
                          viewBox="0 5 25 17"
                          className={classes.fullScreenIcon}
                        >
                          <DockIcon />
                        </SvgIcon>
                      </CustomIconButton>
                    </CustomTooltip>
                  </>
                )}

                <IconButton btnType="transparent" onClick={dialogProps.onClose}>
                  <CloseIcon
                    htmlColor={theme.palette.secondary.medDark}
                    style={{ fontSize: "18px" }}
                  />
                </IconButton>
              </Grid>
            </Grid>
          </DialogTitle>
          <DialogContent classes={{ root: classes.defaultDialogContent }}>
            {this.props.children}
          </DialogContent>
        </div>
      </Dialog>
    );
  }
}

export default withStyles(dialogStyles, { withTheme: true })(projectDialog);
