import React, { Component } from "react";
import { HotKeys } from "react-hotkeys";
import { withSnackbar } from "notistack";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../Buttons/CustomButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import { connect } from "react-redux";
import { compose } from "redux";
import { authenticateSessionPassword } from "../../redux/actions/authentication";
import { FetchUserInfo } from "../../redux/actions/profile";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { withRouter } from "react-router-dom";
import CustomAvatar from "../Avatar/Avatar";
import DefaultTextField from "../Form/TextField";
import { validatePasswordField } from "../../utils/formValidations";
import SocialLoginButtons from "../SocialLoginButtons/SocialLoginButtons";

class SessionDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      passwordErrorMessage: "",
      passwordError: "",
      userPassword: "",
      showPassword: false,
      open: true,
      btnQuery: "",
    };

    this.handlePasswordInput = this.handlePasswordInput.bind(this);
  }
  handleClickShowPassword = () => {
    this.setState((state) => ({ showPassword: !state.showPassword }));
  };
  handlePasswordInput(event) {
    this.setState({
      userPassword: event.currentTarget.value,
      passwordError: false,
      passwordErrorMessage: "",
    });
  }
  handleResumeLogin = () => {
    const authObj = {
      grant_type: "password",
      username: this.props.profileState.data.email,
      password: this.state.userPassword,
    };
    let validationObj = validatePasswordField(this.state.userPassword);

    const success = (response) => {
      // Auth password success fuction
      this.setState({ btnQuery: "", userPassword: "" });
      localStorage.setItem("token", `Bearer ${response.data.access_token}`);
      sessionStorage.setItem("token", `Bearer ${response.data.access_token}`);
      this.refreshData();
      this.props.closeAction();
    };
    const failure = (error) => {
      // Auth password failure fuction
      this.setState({ btnQuery: "", userPassword: "" });
      this.setState({
        passwordError: true,
        passwordErrorMessage: error ? error.data.error_description : null,
      });
    };
    if (validationObj["isError"]) {
      this.setState({
        passwordError: validationObj["isError"],
        passwordErrorMessage: validationObj["message"],
      });
    } else {
      this.setState({ btnQuery: "progress" });
      this.props.authenticateSessionPassword(authObj, success, failure);
    }
  };
  onEnterKeyDown = (event) => {
    if (this.state.btnQuery === "") this.handleResumeLogin();
  };
  navigateToDuplicateExtAcc = () => {
    this.props.history.push("/account/duplicateaccount");
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  handleDialogeClose = () => {
    this.refreshData();
    this.props.closeAction();
  };

  refreshData = () => {
    const { location } = this.props;
    $.connection.hub.stop();

    // To prevent app from errors, like project delete view is open and data is refreshed and that project doesnot exist

    if (
      !(
        location.pathname.indexOf("/teams/") == 0 ||
        location.pathname.indexOf("/team-settings") == 0
      )
    ) {
      this.props.FetchUserInfo((data) => {
        this.props.FetchWorkspaceInfo("", () => {});
      });
    }
  };

  render() {
    const { classes, theme, open, closeAction } = this.props;
    const hasLocalAccount = this.props.profileState.data.hasLocalAccount;
    const hasExternalLogins = this.props.profileState.data.externalLogins
      ? this.props.profileState.data.externalLogins.length
      : false;

    const {
      passwordError,
      passwordErrorMessage,
      userPassword,
      showPassword,
      btnQuery,
    } = this.state;

    const keyMap = {
      ResumeSession: "enter",
    };
    const handlers = {
      ResumeSession: this.onEnterKeyDown,
    };
    return (
      <Dialog
        classes={{
          paper: classes.sessionDialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        fullWidth={true}
        scroll="body"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={closeAction}
      >
        <DialogContent classes={{ root: classes.sessionDialogCnt }}>
          <Typography variant="h1">
            Session Timed-out
            <br />
          </Typography>
          <Typography variant="body2">
            You have been inactive for more than an hour.
          </Typography>
          <div className={classes.avatarWrap}>
            <CustomAvatar personal size="large" />
          </div>
          <div style={{ marginBottom: 30 }} className="flex_center_center_col">
            <Typography variant="h2" style={{ textAlign: "center" }}>
              {this.props.profileState.data.fullName}
            </Typography>
            <Typography variant="body2">
              {this.props.profileState.data.email}
            </Typography>
          </div>
          {hasLocalAccount ? (
            <React.Fragment>
              <HotKeys
                keyMap={keyMap}
                handlers={handlers}
                style={{ width: "100%" }}
              >
                <DefaultTextField
                  label="Enter your password to pick up where you left off."
                  fullWidth={true}
                  errorState={passwordError}
                  errorMessage={passwordErrorMessage}
                  defaultProps={{
                    id: "passwordInput",
                    type: showPassword ? "text" : "password",
                    onChange: this.handlePasswordInput,
                    value: userPassword,
                    autoFocus: true,
                    placeholder: "Enter password",
                    inputProps: { maxLength: 40 },
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          disableRipple={true}
                          classes={{ root: classes.passwordVisibilityBtn }}
                          onClick={this.handleClickShowPassword}
                        >
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </HotKeys>
              {hasLocalAccount ? (
                <CustomButton
                  btnType="success"
                  variant="contained"
                  onClick={this.handleResumeLogin}
                  style={{ width: "100%" }}
                  query={btnQuery}
                  disabled={btnQuery == "progress"}
                >
                  Resume Session
                </CustomButton>
              ) : null}
              {hasExternalLogins ? (
                <div className={classes.signInOptionText}>
                  <p>or resume with</p>
                </div>
              ) : null}
            </React.Fragment>
          ) : null}
          <SocialLoginButtons
            duplicateAccount={this.navigateToDuplicateExtAcc}
            showSnackBar={this.showSnackBar}
            closeAction={this.handleDialogeClose}
          />
        </DialogContent>
      </Dialog>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  connect(mapStateToProps, {
    authenticateSessionPassword,
    FetchUserInfo,
    FetchWorkspaceInfo,
  }),
  withStyles(dialogStyles, { withTheme: true })
)(SessionDialog);
