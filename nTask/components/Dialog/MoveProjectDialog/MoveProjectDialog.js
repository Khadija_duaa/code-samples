import React, { Component } from "react";
import CustomDialog from "../CustomDialog";
import { Scrollbars } from "react-custom-scrollbars";
import { withStyles } from "@material-ui/core/styles";
import ButtonActionsCnt from "../ConfirmationDialogs/ButtonActionsCnt";
import moveProjectStyles from "./style";
import NotificationMessage from "../../NotificationMessages/NotificationMessages";
import { compose } from "redux";
import { connect } from "react-redux";
import CustomAvatar from "../../Avatar/Avatar";
import Typography from "@material-ui/core/Typography";
import Radio from "@material-ui/core/Radio";
import ProjectMoveIcon from "../../Icons/ProjectMoveIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import { injectIntl, FormattedMessage } from "react-intl";

class MoveProjectDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isProceedClicked: false,
      checkedTeam: null,
    };
    this.initialState = this.state;
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.open !== prevProps.open && this.props.open) {
      this.setState(this.initialState);
    }
  }

  handleWorkspaceSelect = (team) => {
    this.setState({ checkedTeam: team });
  };

  handleProceedClick = (e) => {
    const { checkedTeam } = this.state;
    const { successAction, profile } = this.props;
    if (this.state.isProceedClicked) {
      successAction(e, checkedTeam);
    } else {
      this.setState({
        isProceedClicked: true,
        checkedTeam: checkedTeam
          ? checkedTeam
          : profile.workspace.filter((team) => {
              return team.teamId !== profile.loggedInTeam;
            })[0],
      });
    }
  };
  defaultMsg = (project, workspace) => {
    const { classes } = this.props;
    return (
      <>
        {" "}
        Are you sure you want to move project{" "}
        <b className={classes.boldText}>{`"${this.props.projectName}" `}</b>
        and all its data to workspace <b>{`"${workspace}"`}</b>?{" "}
      </>
    );
  };

  render() {
    const { checkedTeam, isProceedClicked } = this.state;
    const {
      classes,
      theme,
      open,
      cancelBtnText,
      headingText,
      closeAction,
      btnQuery,
      profile,
      exceptionText,
    } = this.props;

    let successBtnText = isProceedClicked ? (
      <FormattedMessage
        id="common.action.move.label2"
        defaultMessage="Yes, Move"
      />
    ) : (
      <FormattedMessage
        id="common.action.proceed.label"
        defaultMessage="Proceed"
      />
    );
    let workspaces = profile.workspace || [];
    let filteredWorkspace = workspaces.filter((workspace) => {
      return workspace.teamId !== profile.loggedInTeam;
    });

    return (
      <CustomDialog
        title={headingText}
        dialogProps={{
          open: open,
          onClick: (e) => {
            e.stopPropagation();
          },
          onClose: closeAction,
          PaperProps: {
            style: { maxWidth: 480 },
          },
        }}
      >
        <div className={classes.contentCnt}>
          {exceptionText ? (
            <NotificationMessage
              type="failure"
              iconType="failure"
              style={{ marginBottom: 20 }}
            >
              {exceptionText}
            </NotificationMessage>
          ) : null}
          {isProceedClicked ? (
            <>
              <div className={classes.ConfirmScreenCnt}>
                <SvgIcon
                  viewBox="0 0 117 117"
                  className={classes.moveProjectIcon}
                >
                  <ProjectMoveIcon />
                </SvgIcon>
                <Typography variant="body2">
                  <FormattedMessage
                    id="project.move.messageb"
                    defaultMessage={this.defaultMsg(
                      this.props.project,
                      checkedTeam.teamName
                    )}
                    values={{
                      p: 
                        <b
                          className={classes.boldText}
                        >{`"${this.props.projectName}" `}</b>
                      ,
                      w: <b>{`"${checkedTeam ? checkedTeam.teamName : ""}"`}</b>,
                    }}
                  />
                </Typography>
              </div>
            </>
          ) : (
            <>
              <NotificationMessage
                type="info"
                iconType="info"
                style={{ marginBottom: 20 }}
              >
                <FormattedMessage
                  id="project.move.message"
                  defaultMessage="All your data including tasks, meetings, issues, risks and
                  timesheets will be moved to selected workspace."
                />
              </NotificationMessage>
              {filteredWorkspace.length > 0 ? (
                <>
                  <Typography variant="body2">
                    <FormattedMessage
                      id="workspace-settings.select.label"
                      defaultMessage="SELECT WORKSPACE"
                    />
                  </Typography>
                  <Scrollbars style={{ height: 250 }}>
                    <ul className={classes.workspaceNameCnt}>
                      {filteredWorkspace.map((team, i) => {
                        return (
                          // Check to ignore the active workspace from the list as it's the workspace already user is present in
                          <li
                            key={team.teamId}
                            onClick={() => this.handleWorkspaceSelect(team)}
                          >
                            <Radio
                              checked={
                                (checkedTeam &&
                                  checkedTeam.teamId === team.teamId) ||
                                (i === 0 && !checkedTeam)
                              }
                              value={team.teamId}
                              className={classes.radioBtn}
                            />
                            <CustomAvatar
                              styles={{ marginRight: 10 }}
                              otherWorkspace={{
                                teamName: team.teamName,
                                pictureUrl: team.pictureUrl,
                                baseUrl: team.imageBasePath,
                              }}
                              size="xsmall"
                            />
                            <Typography variant="body2">
                              {team.teamName}
                            </Typography>
                          </li>
                        );
                      })}
                    </ul>
                  </Scrollbars>
                </>
              ) : (
                <NotificationMessage
                  type="failure"
                  iconType="failure"
                  style={{ marginBottom: 20 }}
                >
                  <FormattedMessage
                    id="workspace-settings.no-workspace.label"
                    defaultMessage="No other workspace found."
                  />
                </NotificationMessage>
              )}
            </>
          )}
        </div>
        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={this.handleProceedClick}
          successBtnText={successBtnText}
          cancelBtnText={cancelBtnText}
          classes={classes}
          btnType="success"
          btnQuery={btnQuery}
          disabled={filteredWorkspace.length == 0}
        />
      </CustomDialog>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.profile.data,
  };
};

export default compose(
  injectIntl,
  withStyles(moveProjectStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps)
)(MoveProjectDialog);
