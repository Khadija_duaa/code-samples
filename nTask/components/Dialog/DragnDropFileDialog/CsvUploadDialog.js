import React, { Component, Fragment } from "react";
import { compose } from "redux";
import SvgIcon from "@material-ui/core/SvgIcon";
import CircularProgress from "@material-ui/core/CircularProgress";
import ExcelIcon from "../../Icons/ExcelIcon";
import CustomDialog from "../CustomDialog";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../../Buttons/DefaultButton";
import Dropzone from "react-dropzone";
import Typography from "@material-ui/core/Typography";
import { Circle } from "rc-progress";
import helper from "../../../helper/index";
import { ImportFile } from "../../../redux/actions/ImportExport";
import apiInstance, { CancelToken } from "../../../redux/instance";
import NotificationMessage from "../../NotificationMessages/NotificationMessages";
import getErrorMessages from "../../../utils/constants/errorMessages";
import { injectIntl, FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import Axios from "axios";
import fileDownload from "js-file-download";
const fileType = [
  "application/vnd.ms-excel",
  "application/vnd.ms-excel.sheet.macroEnabled.12",
  "application/msexcel",
  "application/x-msexcel",
  "application/x-ms-excel",
  "application/x-excel",
  "application/x-dos_ms_excel",
  "application/xls",
  "application/x-xls",
  "application/x-msi",
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  "text/csv",
  ".xlsx",
  ".xls",
  "",
];
class DragDropCsv extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dnd: true,
      progress: 0,
      cancelRequest: null,
      successfullyImortedCount: 0,
      totalAdded: 0,
      errorMesseage: "",
    };
    this.initialState = this.state;
    this.handleDragOver = this.handleDragOver.bind(this);
    this.downloadTemplate = this.downloadTemplate.bind(this);
  }
  onhandleDrop = files => {
    if (files.length) {
      this.manageImports(0, files);
      this.setState({ dnd: false });
    }
  };

  manageImports = (index, files) => {
    const self = this;
    const file = files[index];
    const { type, closeAction, showMessage, handleRefresh, callBack } = self.props;
    const successApiCallback = response => {
      if (response.status === 200) {
        const successfullyImortedCount =
          type === "task" || type === "board" ? response.data.count : response.data;
        // closeAction();
        // showMessage(`${successfullyImortedCount} ${type}s have been added to System.`);
        handleRefresh();
        if (index === files.length - 1)
          self.setState({
            progress: 100,
            cancelRequest: null,
            successfullyImortedCount,
            totalAdded: self.state.totalAdded + successfullyImortedCount,
          });
        else {
          // self.manageImports(index + 1, files);
          self.setState({
            totalAdded: self.state.totalAdded + successfullyImortedCount,
          });
        }
        this.setState({ errorMesseage: "" });
        if (this.props.callBack) callBack();
      }
    };
    const failureApiCallback = error => {
      if (error.status && error.status === 500) {
        if (index === files.length - 1) self.setState(self.initialState);
        this.setState({ errorMesseage: error.data.message });
      } else if ((error.status && error.status === 422) || error.status === 406) {
        if (index === files.length - 1) self.setState(self.initialState);
        this.setState({ errorMesseage: error.data.message });
      } else {
        if (index === files.length - 1) self.setState(self.initialState);
        this.setState({ errorMesseage: error.data.message });
      }
    };

    let formdata = new FormData();
    formdata.append("file", file);
    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      cancelToken: new CancelToken(function executor(c) {
        self.setState({ cancelRequest: c });
      }),
    };
    if (type === "issue") {
      ImportFile(
        formdata,
        "api/IssueRisk/importissuestemplatedata",
        config,
        successApiCallback,
        failureApiCallback
      );
    } else if (type === "risk") {
      ImportFile(
        formdata,
        "api/issuerisk/ImportRisksTemplateData",
        config,
        successApiCallback,
        failureApiCallback
      );
    } else if (type === "project") {
      ImportFile(
        formdata,
        "api/project/ImportProjectsData",
        config,
        successApiCallback,
        failureApiCallback
      );
    } else if (type === "meeting") {
      ImportFile(
        formdata,
        "api/Meeting/ImportMeetings",
        config,
        successApiCallback,
        failureApiCallback
      );
    } else if (type === "task") {
      let url = "api/usertask/ImportTasksTemplateData";
      if (this.props.projectId) url = "api/UserTask/ImportTasks?projectId=" + this.props.projectId;
      ImportFile(formdata, url, config, successApiCallback, failureApiCallback);
    } else if (type === "board") {
      let url = "api/usertask/ImportTasksTemplateData";
      if (this.props.projectId)
        url = "api/usertask/ImportTasksTemplateData?projectId=" + this.props.projectId;
      ImportFile(formdata, url, config, successApiCallback, failureApiCallback);
    }
  };
  handleDragOver(event) {
    event.stopPropagation();
  }
  downloadTemplate() {
    let companyName = this.props.companyInfo ? this.props.companyInfo.companyName : "nTask";
    let fileName = `${companyName}-Tasks-Template.xlsx`,
      exportType = 0;
    const { type, closeAction, showMessage } = this.props;
    if (type === "issue") {
      fileName = `${companyName}-Issues-Template.xlsx`;
      apiInstance()
        .get(BASE_URL + "api/export/issuetemplate", {
          responseType: "blob",
        })
        .then(res => {
          fileDownload(res.data, fileName);
          showMessage(`File downloaded successfully.`, "success");
        });
      return;
    } else if (type === "risk") {
      fileName = `${companyName}-Risks-Template.xlsx`;
      exportType = 2;
      apiInstance()
        .get(BASE_URL + "api/export/risktemplate", {
          responseType: "blob",
        })
        .then(res => {
          fileDownload(res.data, fileName);
          showMessage(`File downloaded successfully.`, "success");
        });
      return;
    } else if (type === "task" || type == "board") {
      fileName = `${companyName}-Tasks-Template.xlsx`;
      exportType = 2;
      apiInstance()
        .get(BASE_URL + "api/export/tasktemplate", {
          responseType: "blob",
        })
        .then(res => {
          fileDownload(res.data, fileName);
          showMessage(`File downloaded successfully.`, "success");
        });
      return;
    } else if (type === "project") {
      fileName = `${companyName}-Projects-Template.xlsx`;
      exportType = 3;
      apiInstance()
        .get(BASE_URL + "api/export/projecttemplate", {
          responseType: "blob",
        })
        .then(res => {
          fileDownload(res.data, fileName);
          showMessage(`File downloaded successfully.`, "success");
        });
      return;
    } else if (type === "meeting") {
      fileName = `${companyName}-Meetings-Template.xlsx`;
      exportType = 4;
    }
    apiInstance()
      .get(
        `${
          ENV == "production1" || ENV == "beta1" ? BASE_URL_API : BASE_URL
        }api/base/DownloadTemplate`,
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
          },
          params: {
            type: exportType,
          },
          responseType: "blob",
        }
      )
      .then(response => {
        if (response.status === 200)
          helper.DOWNLOAD_TEMPLATE(response.data, fileName, "application/vnd.ms-excel");
        showMessage(`File downloaded successfully.`, "success");
      })
      .catch(error => {});
  }
  handleCancelRequest = () => {
    const { cancelRequest } = this.state;
    if (cancelRequest) cancelRequest("User cancelled upload.");
  };
  handleCloseDialogue = () => {
    const { dnd, progress } = this.state;
    if (dnd || progress === 100) {
      this.props.closeAction();
      if (!this.state.cancelRequest) this.setState(this.initialState);
    }
  };
  handleInvalidFileDrop = () => {
    this.props.showMessage(getErrorMessages().INVALID_IMPORT_EXTENSION, "error");
  };
  getTranslatedId(value) {
    switch (value) {
      case "Project":
        value = "common.import.projects.label";
        break;
      case "Task":
        value = "common.import.tasks.label";
        break;
      case "Meeting":
        value = "common.import.meetings.label";
        break;
      case "Issue":
        value = "common.import.issues.label";
        break;
      case "Risk":
        value = "common.import.risks.label";
        break;
    }
    return value;
  }
  render() {
    const { classes, theme, open, type, companyInfo } = this.props;
    const { progress, dnd, totalAdded, errorMesseage } = this.state;
    const dndStyles = {
      position: "relative",
      width: "100%",
      borderWidth: 2,
      borderColor: theme.palette.border.lightBorder,
      borderStyle: "dashed",
      borderRadius: 5,
      padding: 50,
      background: theme.palette.background.paper,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      marginBottom: 30,
    };
    const dndActiveStyle = {
      borderColor: theme.palette.border.darkBorder,
    };

    const title = type.charAt(0).toUpperCase() + type.slice(1);
    const taskLabelMulti = companyInfo?.task?.pName;
    return (
      <CustomDialog
        title={
          <FormattedMessage
            id={this.getTranslatedId(title)}
            defaultMessage={"{label}"}
            values={{
              label: taskLabelMulti ? `Import Bulk ${taskLabelMulti}` : 'Import Bulk Tasks'
            }}
          />
        }
        dialogProps={{
          open: open,
          onClose: this.handleCloseDialogue,
          onClick: e => e.stopPropagation(),
          disableBackdropClick: true,
          disableEscapeKeyDown: true,
        }}>
        <div className={classes.centerAlignDialogContent}>
          {errorMesseage ? (
            <NotificationMessage
              type="failure"
              iconType="failure"
              style={{ marginBottom: 20, width: "100%" }}>
              {errorMesseage}
            </NotificationMessage>
          ) : null}
          {dnd ? (
            <Dropzone
              onDrop={this.onhandleDrop}
              onDragOver={this.handleDragOver}
              onDropRejected={this.handleInvalidFileDrop}
              // accept=""
              accept={fileType}
              activeStyle={dndActiveStyle}
              style={dndStyles}
              // disableClick
              onClick={evt => evt.preventDefault()}>
              {({ getRootProps, getInputProps, open }) => {
                return (
                  <Fragment>
                    <div className={classes.csvConfirmationIconCnt} {...getRootProps()}>
                      <SvgIcon
                        viewBox="0 0 26 26"
                        className={classes.archieveConfirmationIcon}
                        htmlColor={theme.palette.secondary.light}>
                        <ExcelIcon />
                      </SvgIcon>
                    </div>
                    <Typography variant="h3">
                      <FormattedMessage
                        id="common.import-dialog.label"
                        defaultMessage="Drag & Drop"
                      />
                    </Typography>
                    <input {...getInputProps()} />
                    <p className={classes.dragnDropMessageText}>
                      <FormattedMessage
                        id="common.import-dialog.sentence"
                        defaultMessage="your CSV file or"
                      />{" "}
                      <u className={classes.browseText} onClick={() => open()}>
                        <FormattedMessage
                          id="common.import-dialog.browse"
                          defaultMessage="browse"
                        />{" "}
                      </u>{" "}
                      <FormattedMessage
                        id="common.import-dialog.fromcomputer"
                        defaultMessage=" from your computer"
                      />
                    </p>
                  </Fragment>
                );
              }}
            </Dropzone>
          ) : (
            <div className={classes.importBulkProgressCnt}>
              <div
                style={{
                  width: 100,
                  position: "relative",
                  marginBottom: 30,
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                }}>
                {progress < 100 ? (
                  <CircularProgress className={classes.progress} />
                ) : (
                  <>
                    <Circle
                      percent={progress}
                      strokeWidth="3"
                      trailWidth=""
                      trailColor="#dedede"
                      strokeColor="#0090ff"
                    />
                    <span className={classes.clProgressValue}>{progress}%</span>
                  </>
                )}
              </div>

              <Typography variant="h3">
                {progress < 100 ? (
                  <FormattedMessage id="common.importing.label" defaultMessage="Importing..." />
                ) : (
                  <FormattedMessage
                    id="common.congratulations.label"
                    defaultMessage="Congratulations!"
                  />
                )}
              </Typography>
              <p className={classes.dragnDropMessageText}>
                {progress < 100 ? (
                  <FormattedMessage
                    id="common.import-dialog.wait.label"
                    defaultMessage="Please wait! It will take a moment."
                  />
                ) : (
                  `${totalAdded}/${totalAdded} ${type}${this.props.intl.formatMessage({
                    id: "common.import-dialog.added.label",
                    defaultMessage: "s have been imported to",
                  })} ${
                    companyInfo ? companyInfo.companyName : "nTask"
                  } ${this.props.intl.formatMessage({
                    id: "common.import-dialog.added.success",
                    defaultMessage: "successfully.",
                  })}.`
                )}
              </p>
            </div>
          )}
        </div>
        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="center"
          classes={{
            container: classes.dialogActionsCnt,
          }}>
          {dnd ? (
            <Fragment>
              <DefaultButton
                text={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
                buttonType="Transparent"
                style={{ marginRight: 20 }}
                onClick={this.handleCloseDialogue}
              />
              <DefaultButton
                text={
                  <FormattedMessage
                    id="common.download-template.label"
                    defaultMessage="Download Template"
                  />
                }
                buttonType="Plain"
                onClick={this.downloadTemplate}
              />
            </Fragment>
          ) : progress < 100 ? null : ( // /> //   onClick={this.handleCancelRequest} //   buttonType="Plain" //   text="Cancel" // <DefaultButton
            <DefaultButton
              text={<FormattedMessage id="common.done.label" defaultMessage="Done" />}
              buttonType="Plain"
              onClick={this.handleCloseDialogue}
            />
          )}
        </Grid>
      </CustomDialog>
    );
  }
}
function mapStateToProps(state) {
  return {
    companyInfo: state.whiteLabelInfo.data,
  };
}
export default compose(
  injectIntl,
  connect(mapStateToProps, {}),
  withStyles(dialogStyles, { withTheme: true })
)(DragDropCsv);
