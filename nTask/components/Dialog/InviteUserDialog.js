import React, { Component } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import CustomDialog from "./CustomDialog";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../Buttons/DefaultButton";
import CustomButton from "../Buttons/CustomButton";
import DefaultTextField from "../Form/TextField";
// import { RecommendnTask } from "../../redux/actions/recommend";
import NotificationMessage from "../NotificationMessages/NotificationMessages";
import { filterMutlipleEmailsInput, validateMultifieldEmails } from '../../utils/formValidations';
import getErrorMessages from '../../utils/constants/errorMessages';
import { InviteTeamMembers } from "../../redux/actions/workspace";

class InviteUserDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 1,
      EmailAddressError: '',
      EmailAddressErrorText: '',
      emailList: '',
      btnQuery: ''
    };
    this.initialState = this.state;
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleInviteSend = this.handleInviteSend.bind(this);
  }

  onStarClick(nextValue, prevValue, name) {
    this.setState({ rating: nextValue });
  }
  handleInput = name => event => {
    let inputValue = event.target.value;
    localStorage.setItem("recommendEmails", inputValue.trim());
    inputValue = filterMutlipleEmailsInput(inputValue);
    this.setState({ [name]: inputValue, EmailAddressError: false, EmailAddressErrorText: "" });
  };
  handleSelectChange(value) {
    this.setState({ feedbackType: value });
  }
  handleInviteSend() {
    const props = this.props;
    let validationObj = validateMultifieldEmails(this.state.emailList);

    if (validationObj['isError']) {
      this.setState({
        EmailAddressError: validationObj['isError'],
        EmailAddressErrorText: validationObj['message']
      });
    }
    else {
      this.setState({
        EmailAddressError: false,
        EmailAddressErrorText: '',
        btnQuery: 'progress'
      }, () => {
        let emailListObj = {
          userEmails: validationObj['data']
        };
        props.InviteTeamMembers(emailListObj,
          (response) => {
            localStorage.removeItem("recommendEmails");
            this.setState({ emailList: '', btnQuery: '' });
            props.closeDialog();
            this.showSnackBar("Email is sent successfully.", 'success');
          },
          (error) => {
            this.showSnackBar(getErrorMessages().INVITE_FAILED, 'error');
            this.setState({ btnQuery: '' });
          });
      });
    }
  }
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right"
        },
        variant: type ? type : "info"
      }
    );
  };
  render() {
    const { classes, theme, open, closeDialog } = this.props;
    const { EmailAddressError, EmailAddressErrorText, emailList, btnQuery } = this.state;

    return (
      <CustomDialog
        title="Invite Members"
        dialogProps={{
          open, onClose: closeDialog,
          onClick: (e) => e.stopPropagation(),
          onExited: () => {
            this.setState(this.initialState)
          },
          onEnter: () => {
            this.setState({
              emailList: localStorage.getItem("recommendEmails") || ""
            });
          }
        }
        }
      >
        <div className={classes.feedbackDialogContentCnt}>

          <DefaultTextField
            fullWidth={true}
            labelProps={{
              shrink: true
            }}
            errorState={EmailAddressError}
            errorMessage={EmailAddressErrorText}
            defaultProps={{
              id: "emailList",
              onChange: this.handleInput("emailList"),
              value: emailList,
              multiline: true,
              rows: 7,
              placeholder: "Enter email addresses separated by commas.",
              autoFocus: true
            }}
          />
          <NotificationMessage type="info" iconType="info" style={{ marginBottom: 20 }}>
            Adding a workspace member will give access to all projects within this workspace.
              </NotificationMessage>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
          >
            <DefaultButton
              text="Cancel"
              onClick={closeDialog}
              buttonType="Transparent"
              style={{ marginRight: 20 }}
            />
            <CustomButton
              onClick={this.handleInviteSend}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress"}
            >
              Send Invite
          </CustomButton>
          </Grid>
        </div>
      </CustomDialog>
    );
  }
}


export default compose(
  withRouter,
  withSnackbar,
  withStyles(dialogStyles, { withTheme: true }),
  connect(
   null,
    {
      InviteTeamMembers
    }
  )
)(InviteUserDialog);