import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import SvgIcon from "@material-ui/core/SvgIcon";
import StarIcon from "../Icons/StarIcon";
import CustomDialog from "./CustomDialog";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../Buttons/IconButton";
import DefaultButton from "../Buttons/DefaultButton";
import Typography from "@material-ui/core/Typography";
import StarRatingComponent from "react-star-rating-component";
import Select from "react-select";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import DefaultTextField from "../Form/TextField";
import AttachIcon from "@material-ui/icons/AttachFile";
import { SendFeedback } from "./../../redux/actions/feedback";

import getErrorMessages from "../../utils/constants/errorMessages";
import helper from "../../helper";
import { injectIntl, FormattedMessage } from "react-intl";

class FeedbackDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 5,
      comments: "",
      feedbackType: { value: "Suggestion", label: this.props.intl.formatMessage({id: "feedback.form.option-dropdown.suggestion" , defaultMessage: "Suggestion"}) },
      imgName: "",
      base64: "",
      commentsError: false,
    };
    this.initialState = this.state;
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleSendFeedBack = this.handleSendFeedBack.bind(this);
    this.removeImgAttachment = this.removeImgAttachment.bind(this);
    this._handleImageChange = this._handleImageChange.bind(this);
  }

  onStarClick(nextValue, prevValue, name) {
    this.setState({ rating: nextValue });
  }
  handleInput = (name) => (event) => {
    localStorage.setItem("feedbackComment", event.target.value);
    this.setState({ [name]: event.target.value });
  };
  handleSelectChange(value) {
    this.setState({ feedbackType: value });
  }
  clickFile() {
    let fileinputbtn = document.getElementById("UploadedImage");
    fileinputbtn.click();
  }

  handleSendFeedBack(event) {
    const self = this;
    let validateEmptyText = helper.HELPER_EMPTY(
      "Comments",
      this.state.comments
    );
    if (validateEmptyText === null) {
      let feedbackObj = {
        message: this.state.comments,
        topic: this.state.feedbackType.value,
        attachment: this.state.base64 || null,
        rating: this.state.rating,
        fileName: this.state.imgName || null,
      };
      this.props.SendFeedback(
        feedbackObj,
        (succ) => {
          self.props.closeDialog();
          self.props.showSnackBar(getErrorMessages().FEEDBACK_SENT);
          localStorage.removeItem("feedbackComment");
        },
        (err) => {
          self.props.showSnackBar(err.data.message, "error");
        }
      );
    } else {
      this.setState({
        commentsError: true,
      });
    }
  }

  removeImgAttachment = () => {
    this.setState({
      imageUploadFlag: false,
      imgName: "",
      base64: "",
    });
  };

  _handleImageChange(e) {
    e.preventDefault();
    let validateEmptyText = helper.HELPER_EMPTY(
      "Comments",
      this.state.comments
    );

    this.setState({
      imageUploadFlag: true,
    });
    var data = new FormData();
    let reader = new FileReader();
    let file = e.target;
    let self = this;
    if (file.files.length > 0) {
      let type = file.files[0].name.toLowerCase().match(/[0-9a-z]{1,5}$/gm)[0];
      if (
        type != null &&
        (type === "png" ||
          type === "jpeg" ||
          type === "jpg" ||
          type === "mp4" ||
          type === "mpg" ||
          type === "svg" ||
          type === "zip" ||
          type === "rar" ||
          type === "webm" ||
          type === "wmv" ||
          type === "swf" ||
          type === "mov" ||
          type === "mkv" ||
          type === "gif" ||
          type === "bmp")
      ) {
        reader.onloadend = () => {
          this.setState({
            file: file.files[0],
            imagePreviewUrl: reader.result,
          });
          this.props.showSnackBar(
            getErrorMessages().FILE_SUCCESSFULLY_UPLOADED
          );
          this.setState({
            imageUploadFlag: false,
            imgName: file.files[0].name,
            base64: reader.result,
          });
        };
        reader.readAsDataURL(file.files[0]);
      } else {
        this.props.showSnackBar(
          getErrorMessages(type).INVALID_FEEDBACK_FILE_EXTENSION
        );
        this.setState({
          imageUploadFlag: false,
        });
      }
    }
    this.setState({
      imageUploadFlag: false,
    });
    //}
    // else{
    //   this.setState({
    //     commentsError: true,
    //     commentsErrorMessage: validateEmptyText
    //   });
    // }
  }

  render() {
    const { classes, theme, open, closeDialog } = this.props;
    const {
      rating,
      feedbackType,
      comments,
      imgName,
      commentsError,
    } = this.state;
    const hideBtn = {
      display: "none",
    };
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: 0,
        minHeight: 41,
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: "none",
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`,
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`,
        },
      }),
      input: (base, state) => ({
        color: theme.palette.text.secondary,
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "0 0 0 14px",
        fontSize: "12px",
      }),
      option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        return {
          ...styles,
          fontSize: "12px",
          padding: "2px 12px",
        };
      },
    };
    const options = [
      { value: "Suggestion", label: this.props.intl.formatMessage({id: "feedback.form.option-dropdown.suggestion" , defaultMessage: "Suggestion"}) },
      { value: "Query", label: this.props.intl.formatMessage({id: "feedback.form.option-dropdown.query" , defaultMessage: "Query"}) },
    ];
    // this.setState({
    //   comments: localStorage.getItem("feedbackComment")||""
    // });
    return (
      <CustomDialog
        title={
          <FormattedMessage id="feedback.title" defaultMessage="Feedback" />
        }
        dialogProps={{
          open: open,
          onClose: closeDialog,
          onClick: (e) => e.stopPropagation(),
          onExited: () => {
            this.setState(this.initialState);
          },
          onEnter: () => {
            this.setState({
              comments: localStorage.getItem("feedbackComment") || "",
            });
          },
        }}
      >
        <div className={classes.feedbackDialogContentCnt}>
          <Typography
            variant="body1"
            classes={{ body1: classes.starsRatingTitle }}
          >
            <FormattedMessage
              id="feedback.message"
              defaultMessage="How would you rate your experience?"
            />
          </Typography>
          <StarRatingComponent
            name="rating"
            starCount={5}
            value={rating}
            onStarClick={this.onStarClick.bind(this)}
            emptyStarColor={theme.palette.background.items}
            renderStarIcon={(index, value) => {
              return (
                <span>
                  {index <= value ? (
                    <SvgIcon
                      viewBox="0 0 19.481 19.481"
                      classes={{ root: classes.ratingStarIcon }}
                      htmlColor={theme.palette.background.star}
                    >
                      <StarIcon />
                    </SvgIcon>
                  ) : (
                    <SvgIcon
                      viewBox="0 0 19.481 19.481"
                      htmlColor={theme.palette.background.items}
                      classes={{ root: classes.ratingStarIcon }}
                    >
                      <StarIcon />
                    </SvgIcon>
                  )}
                </span>
              );
            }}
          />
          <FormControl
            fullWidth={true}
            classes={{ root: classes.selectFormControl }}
          >
            <InputLabel
              htmlFor="feedbackType"
              classes={{
                root: classes.selectLabel,
              }}
              shrink={false}
            >
              <FormattedMessage
                id="feedback.form.type.label"
                defaultMessage="Select Type"
              />
            </InputLabel>
            <Select
              styles={customStyles}
              onChange={this.handleSelectChange}
              inputId="feedbackType"
              options={options}
              value={feedbackType}
            />
          </FormControl>
          <DefaultTextField
            label={
              <FormattedMessage
                id="common.comment.label"
                defaultMessage="Comments"
              />
            }
            fullWidth={true}
            errorState={commentsError}
            defaultProps={{
              id: "comments",
              onChange: this.handleInput("comments"),
              value: comments,
              multiline: true,
              rows: 7,
              autoFocus: true,
              inputProps: { maxLength: 1500 },
            }}
          />

          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            {imgName ? (
              <div className="flex_center_start_row">
                <Typography variant={"body2"} className={classes.uploadedImgName}>
                  {`${imgName} `}
                </Typography>
                <IconButton
                  btnType="smallFilledGray"
                  onClick={this.removeImgAttachment}
                  style={{ marginLeft: 5, marginTop: 5 }}
                >
                  <CloseIcon
                    htmlColor={theme.palette.common.white}
                    className={classes.deleteIcon}
                  />
                </IconButton>
              </div>
            ) : (
              <DefaultButton
                onClick={this.clickFile}
                text={
                  <FormattedMessage
                    id="common.attach-file.label"
                    defaultMessage="Attach File"
                  />
                }
                buttonType="smallIconBtn"
                disableRipple={true}
              >
                <AttachIcon htmlColor={theme.palette.secondary.light} />
              </DefaultButton>
            )}
            <input
              style={hideBtn}
              className="fileInput"
              type="file"
              id="UploadedImage"
              onChange={this._handleImageChange}
              accept={
                "png, jpeg, jpg, mp4, mpg, svg, zip, rar, webm, wmv, swf, mov, mkv, gif, bmp"
              }
            />
            <div>
              <DefaultButton
                text={
                  <FormattedMessage
                    id="profile-settings-dialog.apps-integrations.personal-data.confiramtion-dialog.cancel-button.label"
                    defaultMessage="Cancel"
                  />
                }
                onClick={closeDialog}
                buttonType="Transparent"
                style={{ marginRight: 20 }}
              />
              <DefaultButton
                text={
                  <FormattedMessage
                    id="feedback.form.send-feedback-button.label"
                    defaultMessage="Send Feedback"
                  />
                }
                buttonType="Plain"
                onClick={this.handleSendFeedBack}
              />
            </div>
          </Grid>
        </div>
      </CustomDialog>
    );
  }
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(dialogStyles, { withTheme: true }),
  connect(null, {
    SendFeedback,
  })
)(FeedbackDialog);
