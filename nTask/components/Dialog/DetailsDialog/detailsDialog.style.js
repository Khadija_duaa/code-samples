const detailDialogStyles = theme => ({
  dialogTitleWidth: {
    maxWidth: 500,
  },
  setOpen: {
    maxWidth: "100%",
    height: "100%",
    cursor: "pointer",
    margin: "0",
  },
  dialogCnt: {
    // display: "flex",
    // alignItems: "center",
    // justifyContent: "center"
  },
  dialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    borderRadius: 4,
    maxWidth: "85%",
    minHeight: "89%"
  },
  defaultDialogTitle: {
    padding: "15px 20px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    userSelect: "none",
  },
  dialogContentCnt: {
    padding: "20px 20px 0 20px",
  },
  defaultDialogContent: {
    padding: "0",
    overflowY: "visible",
    // border: "1px solid green",
    display: "flex",
    flexDirection: "row",
    backgroundColor:"rgba(246, 246, 246, 1)",
    marginBottom:4
  },
  subTitle: {
    color: "#7E7E7E",
    fontSize: "13px !important",
    fontWeight: 400,
    marginTop: 5,
  },
  arrowBackCnt: {
    background: "#F3F3F3",
    height: "30px",
    width: "31px",
    borderRadius: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginRight: "10px",
    alignItems: "center",
    cursor: "pointer",
  },
  arrowBackIcon: {
    fontSize: "12px !important",
    marginLeft: 5,
  },
  headerContainer: {
    height: 40,
    borderBottom: `1px solid ${theme.palette.border.extraLightBorder}`,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 10,
  },
  label: {
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightLight,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    position: "relative",
    top: "-2px",
    right: "-5px",
    cursor: "pointer"
  },
  clearIcon: {
    color: "#7f8f9a",
    fontSize: "18px !important",
    cursor: "pointer",
  },
  leftContainer: {
    padding: "10px 20px 10px 15px",
    flex: 1,
    borderRight: "1px solid #E7E9EB",
    position: "relative",
    backgroundColor: 'white',
  },
  rightContainer: {
    // padding: "10px",
    width: "40%",
  },
  arrowForward: {
    fontSize: "24px",
    position: "absolute",
    top: 35,
    right: -14,
    padding: 4,
    backgroundColor: "white",
    border: "1px solid #E7E9EB",
    borderRadius: "50%",
    cursor: "pointer",
    color: "#929292",
    zIndex: 1,
    "&:hover": {
      fontSize: "24px",
      backgroundColor: "#0090ff",
      border: "1px solid #0090ff",
      color: "white"
    },
  },
  arrowBack: {
    position: "absolute",
    top: 35,
    right: -12,
    padding: "4px 0px 4px 4px",
    backgroundColor: "white",
    border: "1px solid #E7E9EB",
    borderRadius: "50%",
    cursor: "pointer",
    color: "#929292",
    zIndex: 1,
    "&:hover": {
      backgroundColor: "#0090ff",
      border: "1px solid #0090ff",
      color: "white"
    },
  },
  updateContent:{
    padding: "10px 20%",
    marginRight: 18
  },
  exitFullScreenIcon:{
    fontSize: "16px !important"
  },
  rightSideHeaderContent:{
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  iconCnt:{
    cursor: "pointer"
  },

  /* Extra small devices (phones, 600px and down and tablet) */
  "@media only screen and (max-width: 1024px)": {
    updateContent:{
      padding: "10px",
    }
  },
});

export default detailDialogStyles;
