import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Grid from "@material-ui/core/Grid";
import detailDialogStyles from "./detailsDialog.style";
import { withStyles } from "@material-ui/core/styles";
import FullscreenIcon from "../../Buttons/IconFullScreen";
import ClearIcon from "@material-ui/icons/Clear";
import ArrowForward from "@material-ui/icons/ArrowForwardIos";
import ArrowBackIcon from "@material-ui/icons/ArrowBackIos";
import SvgIcon from "@material-ui/core/SvgIcon";
import ExitFullScreen from "../../Icons/ExitFullScreen";
import { Scrollbars } from "react-custom-scrollbars";

function DetailsDialog(props) {
  const { classes, dialogProps, headerRenderer, leftSideContent, rightSideContent } = props;

  const [fullWidth, setFullWidth] = useState(false);
  const [rightContainer, setRightContainer] = useState(true);

  const setResolution = () => {
    setFullWidth(!fullWidth);
  };

  const handleRightContainerClick = () => {
    setRightContainer(!rightContainer);
  };

  return (
    <Dialog
      classes={{
        paper: fullWidth ? classes.setOpen : `${classes.dialogPaperCnt} ${classes.dialogCnt}`,
      }}
      fullWidth={true}
      // scroll="body"
      {...dialogProps}>
      <div className={classes.headerContainer}>
        <div>
          <span onClick={setResolution} className={classes.iconCnt}>
            {fullWidth ? (
              <SvgIcon viewBox="0 0 14 13.966" className={classes.exitFullScreenIcon}>
                <ExitFullScreen />
              </SvgIcon>
            ) : (
              <FullscreenIcon />
            )}
          </span>
          <span className={classes.label} onClick={setResolution}>
            {fullWidth ? "Exit Full Screen" : "Full Screen"}
          </span>
        </div>
        <div className={classes.rightSideHeaderContent}>
          {headerRenderer}
          <ClearIcon onClick={dialogProps.onClose} className={classes.clearIcon} />
        </div>
      </div>
      <DialogContent classes={{ root: classes.defaultDialogContent }}>
        <div className={`${classes.leftContainer} ${!rightContainer && classes.updateContent}`}>
          <Scrollbars
            autoHide
            renderTrackHorizontal={props => <div {...props} style={{ display: "none" }} />}>
            {leftSideContent}
          </Scrollbars>

          {rightContainer ? (
            <ArrowForward className={classes.arrowForward} onClick={handleRightContainerClick} />
          ) : (
            <ArrowBackIcon className={classes.arrowBack} onClick={handleRightContainerClick} />
          )}
        </div>
        {rightContainer && <div className={classes.rightContainer}>{rightSideContent}</div>}
      </DialogContent>
    </Dialog>
  );
}
DetailsDialog.defaultProps = {
  handleBackClick: () => {},
  onClose: () => {},
  subTitle: false,
  backBtn: false,
  title: "",
  headerStyles: {},
  variant: false,
};

export default withStyles(detailDialogStyles, { withTheme: true })(DetailsDialog);
