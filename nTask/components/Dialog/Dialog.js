import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../Buttons/IconButton";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Zoom from "@material-ui/core/Zoom";

class DefaultDialog extends Component {
  constructor(props) {
    super(props);
    this.divElement = React.createRef();
  }
  render() {
    const { classes, theme, title, ...rest } = this.props;

    return (
      <Dialog
        {...rest}
        classes={{
          paper: classes.dialogPaperCnt,
          scrollBody: classes.dialogCnt
        }}
        aria-labelledby="form-dialog-title"
        fullWidth={true}
        scroll="body"
      >
        <DialogTitle
          id="form-dialog-title"
          classes={{ root: classes.defaultDialogTitle }}
        >
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <Grid item>
              <Typography variant="h2">{title}</Typography>
            </Grid>
            <Grid item>
              <IconButton btnType="transparent" onClick={this.props.onClose}>
                <CloseIcon htmlColor={theme.palette.secondary.medDark} />
              </IconButton>
            </Grid>
          </Grid>
        </DialogTitle>
        <DialogContent classes={{ root: classes.defaultDialogContent }}>
          {this.props.children}
        </DialogContent>
      </Dialog>
    );
  }
}

export default withStyles(dialogStyles, { withTheme: true })(DefaultDialog);
