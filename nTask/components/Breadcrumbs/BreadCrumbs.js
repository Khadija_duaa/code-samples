// @flow
import React, { useState, useEffect, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import Typography from "@material-ui/core/Typography";
import ArrowRight from "@material-ui/icons/KeyboardArrowRight";
import isEmpty from "lodash/isEmpty";
import Divider from "@material-ui/core/Divider";


type BreadCrumbProps = {
  classes: Object,
  theme: Object,
  handlebreadCrumClick: Function,
  breadCrumArr: Array,
  endBreadCrumbName: String,
};

function BreadCrumbs(props: BreadCrumbProps) {
  const {
    classes,
    theme,
    handlebreadCrumClick,
    breadCrumArr,
    endBreadCrumbName,intl
  } = props;

  const handleItemClick = (b) => {
    handlebreadCrumClick(b);
  };

    return (
      <div className={classes.breadCrumbCnt}>
        {!isEmpty(breadCrumArr) && (
          <div style={{ marginBottom: 5 }}>
            {breadCrumArr.map((b,index) => 
                index == (breadCrumArr.length-1) ? 
                  <Fragment>
                  {" "}
                  <span
                    className={classes.breadCrumbLastTitle}
                    onClick={() => handleItemClick(b)}
                  >
                    {`${b.value == "Documents" ? intl.formatMessage({id:"project.dev.documents.label",defaultMessage:"Documents"}) : b.value}`}
                  </span>
                </Fragment>
                :<Fragment>
                  {" "}
                  <span
                    className={classes.breadCrumbTitle}
                    onClick={() => handleItemClick(b)}
                  >
                    {`${b.value}`}
                  </span>
                  <span>{'  /'}</span>
                </Fragment>
            )}
          </div>
        )}
        <Divider className={classes.hrLeft} />
      </div>
    );
}

BreadCrumbs.defaultProps = {
  classes: {},
  theme: {},
  handlebreadCrumClick: () => {},
  breadCrumArr: [
    { key: "Bread Crumb 01", value: 1 },
    { key: "Bread Crumb 02", value: 2 },
    { key: "Bread Crumb 03", value: 3 },
  ],
  endBreadCrumbName: "Bread Crumb Ends",
};
const mapStateToProps = (state) => {
  return {};
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(BreadCrumbs);
