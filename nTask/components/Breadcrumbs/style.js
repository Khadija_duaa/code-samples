const BreadCrumbsStyles = (theme) => ({
  breadCrumbTitle: {
    color: theme.palette.text.brightBlue,
    fontSize: "12px !important",
    textDecoration: "underline",
    cursor: "pointer",
  },
  breadCrumbLastTitle: {
    color: theme.palette.secondary.dark,
    fontSize: "12px !important",
  },
  arrowRight: {
    fontSize: "18px !important",
    marginBottom: -4,
  },
  breadCrumbCnt: {
    margin: "5px 0px",
    display: 'flex',
    alignItems: 'center',
  },
  breadCrumbEndTitle: {
    color: "#202020",
    fontSize: "18px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
  },
  hrLeft: {
    marginLeft: 5,
    flex: 1
},
});
export default BreadCrumbsStyles;
