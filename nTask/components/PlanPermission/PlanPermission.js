import { store } from "../../index";


//Getting the payment plan object of active team
const paymentPlan = () => {
   // Getting the redux store
  let state = store.getState();
  return state.profile.data
    ? state.profile.data.teams.find(item => {
        return item.companyId == state.profile.data.activeTeam;
      }).paymentPlan
    : {};
};

export function teamCanView(key) {
  return paymentPlan()[key];
}
