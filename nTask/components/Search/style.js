const searchStyles = theme => ({
  searchCnt: {
    marginLeft: 15,
    '& *': {
      fontFamily: theme.typography.fontFamilyLato
    }
  },
  searchIcon: {
    fontSize: "16px !important"
  },
  shortcutKeyCnt: {
    fontSize: "11px !important",
    color: theme.palette.text.light,
    background: theme.palette.common.white,
    padding: "2px 5px",
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`
  },
  shortcutTag: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
    position: "absolute",
    right: 10,
    fontWeight: 500
  },
  typeTag: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
    fontWeight: 500,
    padding: "2px 10px",
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`
  },
  searchMenuHeader: {
    display: "flex",
    justifyContent: "space-between",
    padding: "10px 10px 0 10px"
  },
  quickSearchList: {
    padding: 0,
    listStyleType: "none",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    background: theme.palette.background.paper,
    "& li": {
      padding: "15px 10px",
      cursor: "pointer"
    }
  },
  userNameCountCnt: {
    display: "flex",
    justifyContent: "space-between"
  },
  optionText: {
      width: 260,
      overflow: "hidden",
      whiteSpace: "nowrap",
      lineHeight: "normal",
      textOverflow: "ellipsis"
  },
  archiveIcon: {
    fontSize: "19px !important",
    marginRight: 10,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "2px 2px 2px 4px",
    borderRadius: 4
  },
  dropdownClearIcon:{
    fontSize: "20px !important"
  }
});

export default searchStyles;
