import React, { Component, Fragment } from "react";
import Select, { components, createFilter } from "react-select";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import searchStyles from "./style.js";
import { connect } from "react-redux";
import { compose } from "redux";
import ClearIcon from "@material-ui/icons/Clear";
import Typography from "@material-ui/core/Typography";
import GetUserTasks from "../../helper/getUserTasks";
import GetUserRisks from "../../helper/getUserRisks";
import GetUserIssues from "../../helper/getUserIssues";
import GetUserMeetings from "../../helper/getUserMeetings";
import GetUserUpdatedIssue from "../../helper/getUserUpdatedIssue";
import TaskDetails from "../../Views/Task/TaskDetails/TaskDetails";
import RiskDetails from "../../Views/Risk/RiskDetails/RiskDetails";
import IssueDetails from "../../Views/Issue/IssueDetails/IssueDetails";
import MeetingDetails from "../../Views/Meeting/MeetingDetails/MeetingDetails";
import { UpdateRisk } from "../../redux/actions/risks";
import { UpdateMeeting } from "../../redux/actions/meetings";
import { UpdateIssue } from "../../redux/actions/issues";
import { teamCanView } from "../PlanPermission/PlanPermission";
import { generateActiveMembers } from "../../helper/getActiveMembers";
import { injectIntl, FormattedMessage } from "react-intl";
import { riskDetailDialog } from "../../redux/actions/allDialogs";
import { taskDetailDialogState } from "../../redux/actions/allDialogs";
import { issueDetailDialogState } from "../../redux/actions/allDialogs";
import SearchIcon from "../Icons/SearchIconV2";

const generateDate = (label, value, id, actionType, type, obj, uniqueId) => {
  return {
    label: label,
    value: uniqueId + value,
    id: id,
    uniqueId: uniqueId,
    actionType: actionType,
    type,
    obj,
  };
};

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: this.constantFilters(),
      inputValue: "",
      selectedValue: [],
      showDetails: false,
      data: {},
      type: "",
      searchSelectStyle: this.searchSelectStyle(),
      meetingId: "",
    };
  }
  constantFilters = () => {
    const {companyInfo} = this.props;
    const taskLabelMulti = companyInfo?.task?.pName;
    return [
      {
        label: taskLabelMulti ? taskLabelMulti : this.props.intl.formatMessage({
          id: "task.label",
          defaultMessage: taskLabelMulti ? taskLabelMulti : "Tasks",
        }),
        value: taskLabelMulti ?'/' + taskLabelMulti : "/tasks",
        shortcut: taskLabelMulti ?'/' + taskLabelMulti : "/tasks",
      },
      {
        label: this.props.intl.formatMessage({
          id: "project.label",
          defaultMessage: "Projects",
        }),
        value: "/projects",
        shortcut: "/projects",
      },
      {
        label: this.props.intl.formatMessage({
          id: "meeting.label",
          defaultMessage: "Meetings",
        }),
        value: "/meetings",
        shortcut: "/meetings",
      },
      {
        label: this.props.intl.formatMessage({
          id: "issue.label",
          defaultMessage: "Issues",
        }),
        value: "/issues",
        shortcut: "/issues",
      },
      {
        label: this.props.intl.formatMessage({
          id: "risk.label",
          defaultMessage: "Risks",
        }),
        value: "/risks",
        shortcut: "/risks",
      },
    ].filter(f => {
      // Filtering the optiosn list based on team permission

      if (companyInfo && f.label !== taskLabelMulti && (companyInfo?.issue.isHidden || f.label !== taskLabelMulti && companyInfo?.risk.isHidden || f.label !== taskLabelMulti && companyInfo?.project.isHidden || f.label !== taskLabelMulti && companyInfo?.meeting.isHidden) ) {
        return false
      }
      if (f.label !== "Risks" && f.label !== "Projects") {
        return f;
      } else if (f.label == "Risks" && teamCanView("riskAccess")) {
        return f;
      } else if (f.label == "Projects" && teamCanView("projectAccess")) {
        return f;
      } else {
        return false;
      }
    })
    const memberFilter = [
      { key: "task", label: "Tasks assigned to" },
      { key: "risk", label: "Risks assigned to" },
      { key: "issue", label: "Issues assigned to" },
      { key: "meeting", label: "All meetings" },
    ].filter(f => {
      // Filtering the optiosn list based on team permission
      if (f.key !== "risk") {
        return f;
      } else if (f.key == "risk" && teamCanView("riskAccess")) {
        return f;
      } else {
        return false;
      }
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      this.state.meetingId &&
      JSON.stringify(prevProps.meetings) !== JSON.stringify(this.props.meetings)
    ) {
      let updatedMeeting = this.props.meetings.find(m => {
        return m.meetingId == this.state.meetingId;
      });
      this.setState({ data: updatedMeeting });
    }
    if (this.state.taskId && JSON.stringify(prevProps.tasks) !== JSON.stringify(this.props.tasks)) {
      let updatedTask = this.props.tasks.find(t => {
        return t.taskId == this.state.taskId;
      });
      this.setState({ data: updatedTask });
    }
  }

  searchSelectStyle = value => {
    const { theme, expanded = false } = this.props;
    if (value === "expand" || expanded) {
      return {
        option: (provided, state) => ({
          ...provided,
          color: theme.palette.text.primary,
          fontSize: "12px",
          fontWeight: 600,
          padding: "15px 10px",
        }),
        container: (provided, state) => ({
          ...provided,
          borderRadius: 6,
        }),
        control: (provided, state) => {
          return {
            ...provided,
            borderRadius: 6,
            minHeight: 28,
            height: 28,
            width: 300,
            transition: "width 0.2s",
            boxShadow: 'none',
              // state.menuIsOpen ? `0 0 0 3px ${theme.palette.shadow.lightShadow}` : "none",
            backgroundColor: theme.palette.background.items,
            border: 'none',
              // state.menuIsOpen
              // ? `1px solid #00CC90`
              // : `1px solid ${theme.palette.border.lightBorder}`,
            ":hover": {
              // border: state.menuIsOpen ? "1px solid #00CC90" : "1px solid #dee8ef",
              border: 'none'
            },
          };
        },
        placeholder: (provided, state) => ({
          ...provided,
          fontSize: "13px",
          marginLeft: 23,
        }),
        multiValue: (provided, state) => ({
          ...provided,
          marginLeft: 5,
        }),
        dropdownIndicator: (provided, state) => ({
          ...provided,
          padding: 0,
          paddingRight: 8,
        }),
        clearIndicator: (provided, state) => ({
          ...provided,
          padding: 0,
          paddingRight: 8,
        }),

        menu: (provided, state) => ({
          ...provided,
          boxShadow: "0 6px 12px rgba(0,0,0,0.175)",
          width: 500
        }),
        menuList: (provided, state) => ({
          ...provided,
          maxHeight: 345,
          padding: 0,
        }),
        indicatorsContainer: (provided, state) => ({
          ...provided,
        }),
      };
    }
    return {
      placeholder: (provided, state) => ({
        ...provided,
        fontSize: "13px",
        display: "none",
      }),
      control: (provided, state) => {
        return {
          ...provided,
          height: 28,
          minHeight: 28,
          borderRadius: 6,
          ":hover": {
            border: 'none'
            // border: state.menuIsOpen ? "1px solid #00CC90" : "1px solid #dee8ef",
          },
        };
      },
      indicatorsContainer: (provided, state) => ({
        ...provided,
        position: "absolute",
        right: -5,
        top: -3,
      }),
    };
  };
  closeTaskDetailsPopUp = () => {
    this.setState({ showDetails: false });
  };
  closeActionMenu = option => {
    this.setState({
      showDetails: option === "CloseDetailsDialogue" ? false : this.state.showDetails,
    });
  };
  isUpdatedRisk = (data, callback) => {
    this.props.UpdateRisk(
      data,
      data => {
        if (callback) callback();
      },
      error => {
        if (error && error.status === 500) {
        }
      }
    );
  };
  isUpdatedMeeting = data => {
    this.props.UpdateMeeting(data, (err, data) => {});
  };
  isUpdatedIssue = (data, callback) => {
    this.props.UpdateIssue(
      data,
      data => {
        if (callback) callback();
        //
      },
      error => {
        if (error && error.status === 500) {
          this.props.handleExportType("Server throws error", "error");
        }
      }
    );
  };

  //Component for rendering custom/static option at the end of the list which will always stay
  MenuList = props => {
    const { classes } = this.props;
    const data = props.getValue()[0];
    return (
      <components.MenuList {...props}>
        {data && data.actionType == "member" ? (
          <div className={classes.searchMenuHeader}>
            <Typography variant="caption">
              {" "}
              <FormattedMessage id="common.activities.label" defaultMessage="ACTIVITIES" />
            </Typography>
            <Typography variant="caption">
              <FormattedMessage id="common.results.label" defaultMessage="RESULTS" />
            </Typography>
          </div>
        ) : (
          <div className={classes.searchMenuHeader}>
            <Typography variant="caption">
              <FormattedMessage id="common.search-filter.label" defaultMessage="SEARCH FILTERS" />
            </Typography>
            <Typography variant="caption">
              <FormattedMessage id="common.shortcuts.label" defaultMessage="SHORTCUTS" />
            </Typography>
          </div>
        )}
        {props.children}
      </components.MenuList>
    );
  };
 ValueContainer = ({ children, ...props }) => {
   const {classes} = this.props
    return (
      components.ValueContainer && (
        <components.ValueContainer {...props}>
          {!!children && (
            <SearchIcon className={classes.searchIcon} />
          )}
          {children}
        </components.ValueContainer>
      )
    );
  };
  Menu = props => {
    const { classes } = this.props;
    return (
      <Fragment>
        <components.Menu {...props}>
          {props.children}
          {this.state.selectedValue.length <= 0 ? (
            <ul className={classes.quickSearchList}>
              <li onClick={this.showUpdatedByMeTask}>
                <Typography variant="body2">
                  <FormattedMessage
                    id="common.search-filter.action"
                    defaultMessage="Recently updated by me"
                  />
                </Typography>
              </li>
              <li onClick={this.showAssignedToMeTask}>
                <Typography variant="body2">
                  <FormattedMessage
                    id="common.search-filter.actionb"
                    defaultMessage="Assigned to me"
                  />
                </Typography>
              </li>
            </ul>
          ) : null}
        </components.Menu>
      </Fragment>
    );
  };
  //Component for custom rendering of option in the menu list
  CustomOptionComponent = ({ innerProps, innerRef, getStyles, children, data, ...props }) => {
    const { classes, theme, companyInfo = {} } = this.props;
    const taskLabelSingle = data.type == 'Task' && companyInfo?.task?.sName ? companyInfo?.task?.sName : data.type;
    return (
      <div ref={innerRef} {...innerProps} style={getStyles("option", props)}>
        <div className="flex_center_space_between_row">
          {data.actionType == "selectMember" ? (
            <div className="flex_center_start_row">
              <Typography variant="h6">{children}</Typography>
              <Typography variant="caption">&nbsp; {data.user}</Typography>
            </div>
          ) : (
            <div>
              <Typography variant="h6" className={classes.optionText}>
                <b>{data.uniqueId}</b> {data.uniqueId ? "-" : null} {children}
              </Typography>
            </div>
          )}

          {/* showing user full name infront of filters based on members - checking if action type is select member */}
          {data.actionType == "selectMember" ? (
            <div className={classes.userNameCountCnt}>
              <Typography variant="caption">
                {this.getUserFilteredList(data.id, data.key).length}
              </Typography>
            </div>
          ) : null}

          {/* Renders text of shortcuts e.g. /tasks, /projects */}
          <span className={classes.shortcutTag}>{data.shortcut}</span>
          {data.type ? (
            <div className="flex_center_start_row">
              <span className={classes.typeTag}>{taskLabelSingle}</span>
            </div>
          ) : null}
        </div>
      </div>
    );
  };
  showAssignedToMeTask = () => {
    const assginedTasks = GetUserTasks(this.props.tasks, this.props.profile.userId).map(task => {
      return generateDate(
        task.taskTitle,
        task.taskTitle,
        task.taskId,
        "customAction",
        "Task",
        task,
        task.uniqueId
      );
    });
    this.setState({
      options: assginedTasks,
      selectedValue: {
        label: this.props.intl.formatMessage({
          id: "common.search-filter.actionb",
          defaultMessage: "Assigned to me",
        }),
        value: "Assigned to me",
      },
    });
  };
  showUpdatedByMeTask = () => {
    this.setState({
      selectedValue: {
        label: this.props.intl.formatMessage({
          id: "common.search-filter.action",
          defaultMessage: "Recently updated by me",
        }),
        value: "Recently updated by me",
      },
    });
    const updatedByMeTasks = GetUserUpdatedIssue(this.props.tasks, this.props.profile.userId).map(
      task => {
        return generateDate(
          task.taskTitle,
          task.taskTitle,
          task.taskId,
          "customAction",
          "Task",
          task,
          task.uniqueId
        );
      }
    );
    this.setState({ options: updatedByMeTasks });
  };
  //Dropdown Icon component
  DropdownIndicator = props => {
    const {classes} = this.props;
    return (
      <components.DropdownIndicator {...props}>
        <span className={classes.shortcutKeyCnt}>S</span>
      </components.DropdownIndicator>
    );
  };
  //Clear Icon component
  ClearIndicator = props => {
    const { classes, theme } = this.props;
    return (
      <components.ClearIndicator {...props}>
        <ClearIcon
          className={classes.dropdownClearIcon}
          htmlColor={theme.palette.secondary.medDark}
        />
      </components.ClearIndicator>
    );
  };

  getUserFilteredList = (id, key) => {
    return key == "task"
      ? GetUserTasks(this.props.tasks, id)
      : key == "risk"
      ? GetUserRisks(this.props.risks, id)
      : key == "issue"
      ? GetUserIssues(this.props.issues, id)
      : key == "meeting"
      ? GetUserMeetings(this.props.meetings, id)
      : null;
  };
  getList = () => {
    return {
      task: this.props.tasks
        ? this.props.tasks.map(task => {
            return generateDate(
              task.taskTitle,
              task.taskTitle,
              task.taskId,
              "customAction",
              "Task",
              task,
              task.uniqueId
            );
          })
        : [],
      project: this.props.projects.map(project => {
        return generateDate(
          project.projectName,
          project.projectName,
          project.projectId,
          "customAction",
          "Project",
          project,
          project.uniqueId
        );
      }),
      issue: this.props.issues.map(issue => {
        return generateDate(
          issue.title,
          issue.title,
          issue.id,
          "customAction",
          "Issue",
          issue,
          issue.issueId
        );
      }),
      risk: this.props.risks.map(risk => {
        return generateDate(
          risk.title,
          risk.title,
          risk.id,
          "customAction",
          "Risk",
          risk,
          risk.uniqueId
        );
      }),
      meeting: this.props.meetings.map(meeting => {
        return generateDate(
          meeting.meetingDisplayName,
          meeting.meetingDisplayName,
          meeting.meetingId,
          "customAction",
          "Meeting",
          meeting,
          meeting.uniqueId
        );
      }),
    };
  };
  onSearchFocus = () => {
    this.setState({ searchSelectStyle: this.searchSelectStyle("expand") });
  };
  onSearchBlur = () => {
    if (this.state.selectedValue.length == 0)
      this.setState({ searchSelectStyle: this.searchSelectStyle() });
  };

  generateAssignee = data => {
    const { profileState } = this.props;
    let members = generateActiveMembers();

    if (members && data) {
      let userAttendeeList = data.attendeeList ? data.attendeeList : [];
      let assigneeLists = members.filter(id => {
        return userAttendeeList.indexOf(id.userId) > -1;
      });
      return assigneeLists;
    }
  };

  onSelectChange = (option, meta) => {
    const {companyInfo} = this.props
    if (meta.action == "select-option" && meta.option.actionType == "customAction") {
      switch (meta.option.type) {
        case "Task": {
          //Open Task Details Here
          this.setState({ showDetails: false, data: {}, type: "" }, () => {
            this.setState({
              showDetails: true,
              data: meta.option.obj,
              type: meta.option.type,
              taskId: meta.option.obj.taskId,
            });
          });
        }
        break;
        case "Issue": {
          //Open Issue Details Here
          this.setState({ showDetails: false, data: {}, type: "" }, () => {
            this.setState({
              showDetails: true,
              data: meta.option.obj,
              type: meta.option.type,
            });
          });
        }
        break;
        case "Risk": {
          //Open Risk Details Here
          this.setState({ showDetails: false, data: {}, type: "" }, () => {
            this.setState({
              showDetails: true,
              data: meta.option.obj,
              type: meta.option.type,
            });
          });
        }
        break;
        case "Meeting": {
          //Open Meeting Details Here
          this.setState({ showDetails: false, data: {}, type: "" }, () => {
            let assigneeLists = this.generateAssignee(meta.option.obj);
            this.setState({
              showDetails: true,
              data: { ...meta.option.obj, assigneeLists },
              meetingId: meta.option.obj.meetingId,
              type: meta.option.type,
            });
          });
        }
        break;
        case "Project": {
          //Open Project Gantt Here
          this.props.history.push({
            pathname: "/projects",
            search: `?projectId=${meta.option.obj.projectId}`,
          });
        }
        break;
      }
      this.selectEle.blur();
    } else {
      this.setState({ selectedValue: option });
    }
    //Checking if user selected an option from auto complete, as there are different actions called on select change
    if (meta.action == "select-option") {
      const value = meta.option.value;
      const actionType = meta.option.actionType;

      // Switch statement to check action type and if user selected any member it will show member related filters from memberFilter constant Array
      switch (actionType) {
        case "member": {
          let memberArr = memberFilter.map(memberOption => {
            return {
              label: memberOption.label,
              value: memberOption.label,
              key: memberOption.key,
              actionType: "selectMember",
              user: value,
              id: meta.option.id,
            };
          });
          //Updating state with member filters which will update the options in select
          this.setState({ options: memberArr });
          break;
        }
        case "selectMember": {
          //Updating state with member filters which will update the options in select
          switch (meta.option.key) {
            case "task": {
              let taskArr = this.getUserFilteredList(meta.option.id, meta.option.key).map(task => {
                return generateDate(
                  task.taskTitle,
                  task.taskTitle,
                  task.taskId,
                  "customAction",
                  "Task",
                  task
                );
              });
              this.setState({ options: taskArr });
              break;
            }
            case "risk": {
              let riskArr = this.getUserFilteredList(meta.option.id, meta.option.key).map(risk => {
                return generateDate(risk.title, risk.title, risk.id, "customAction", "Risk", risk);
              });
              this.setState({ options: riskArr });
              break;
            }
            case "issue": {
              let issueArr = this.getUserFilteredList(meta.option.id, meta.option.key).map(
                issue => {
                  return generateDate(
                    issue.title,
                    issue.title,
                    issue.id,
                    "customAction",
                    "Issue",
                    issue
                  );
                }
              );
              this.setState({ options: issueArr });
              break;
            }
            case "meeting": {
              let meetingArr = this.getUserFilteredList(meta.option.id, meta.option.key).map(
                meeting => {
                  return generateDate(
                    meeting.meetingDisplayName,
                    meeting.meetingDisplayName,
                    meeting.meetingId,
                    "customAction",
                    "Meeting",
                    meeting
                  );
                }
              );
              this.setState({ options: meetingArr });
              break;
            }
          }
          break;
        }
      }
      const taskLabelMulti = companyInfo?.task?.pName;
      //Switch statement to update select options based on the value entered in input of select control
      switch (value) {
        //This case will trigger if user enter /tasks and press enter
        case "/tasks": {
          //Updating state with tasks in workspace which will update the options in select
          let taskArr = this.getList().task;
          this.setState({ options: taskArr });
          break;
        }
        case "/" + taskLabelMulti: {
          //Updating state with tasks in workspace which will update the options in select
          let taskArr = this.getList().task;
          this.setState({ options: taskArr });
          break;
        }
        //This case will trigger if user enter /projects and press enter
        case "/projects": {
          let projectsArr = this.getList().project;
          this.setState({ options: projectsArr });
          break;
        }
        //This case will trigger if user enter /meetings and press enter
        case "/meetings": {
          let meetingsArr = this.getList().meeting;
          this.setState({ options: meetingsArr });
          break;
        }
        //This case will trigger if user enter /issues and press enter
        case "/issues": {
          let issuesArr = this.getList().issue;
          this.setState({ options: issuesArr });
          break;
        }
        //This case will trigger if user enter /risks and press enter
        case "/risks": {
          let risksArr = this.getList().risk;
          this.setState({ options: risksArr });
          break;
        }
      }
    } else if (meta.action == "remove-value") {
      // else if will be called if user has removed the selected value in select, this is action type of select called on onChange method of select
      const constantFilterName = [
        "/tasks",
        "/projects",
        "/meetings",
        "/issues",
        "/risks",
        "Recently updated by me",
        "Assigned to me",
      ];
      //inCase of removal of value if input gets empty it will reset the select value to default constant filters, mentioned above with the name of constantFilters
      if (option.length == 0) {
        this.setState({ options: this.constantFilters() });
      }
      if (meta.removedValue.actionType == "selectMember") {
        // The block of code executes if user has de-selected the member based actions filter
        let memberArr = memberFilter.map(memberOption => {
          return {
            label: memberOption.label,
            value: memberOption.label,
            key: memberOption.key,
            actionType: "selectMember",
            user: meta.removedValue.value,
            id: meta.removedValue.id,
          };
        });
        //Updating state with member filters which will update the options in select
        this.setState({ options: memberArr });
      }
      if (meta.removedValue.actionType == "member") {
        // The block of code executes if user has removed the selected user tag from select
        //Updating state with member filters which will update the options in select
        this.setState({ options: this.constantFilters(), selectedValue: [] });
      }
    } else if (meta.action == "pop-value") {
      if (option.length <= 0) {
        this.setState({ options: this.constantFilters() });
      }
    } else if (meta.action == "clear") {
      // Code to be executed if click on clear all button
      this.setState({ options: this.constantFilters() });
    }
  };
  handleInputChange = value => {
    this.setState({ inputValue: value });
    const memberQuery = value ? value[0] == "@" : value == "" ? "" : null;
    if (this.state.selectedValue.length <= 0) {
      switch (memberQuery) {
        case true: {
          let membersArr = generateActiveMembers()
            .filter(m => !m.isDeleted)
            .map(member => {
              return generateDate(member.fullName, `@${member.fullName}`, member.userId, "member");
            });
          this.setState({ options: membersArr });
          break;
        }
        case "": {
          this.setState({ options: this.constantFilters() });
          break;
        }
        default: {
          let taskArr = this.getList().task;
          let projectsArr = this.getList().project;
          let meetingsArr = this.getList().meeting;
          let issuesArr = this.getList().issue;
          let risksArr = this.getList().risk;
          let CombinedArr = [
            ...taskArr,
            ...projectsArr,
            ...meetingsArr,
            ...issuesArr,
            ...risksArr,
            ...this.constantFilters(),
          ];
          this.setState({ options: CombinedArr });
        }
      }
    }
  };

  getTaskPer = t => {
    const { projects, workspaceLevelTaskPer, workspaceStatus, workspaceLevelMeetingPer, workspaceLevelRiskPer, workspaceLevelIssuePer } = this.props;
    if (t.projectId) {
      let attachedProject = projects.find(p => p.projectId == t.projectId);
      if (attachedProject) {
        t.taskPermission = attachedProject.projectPermission.permission.task;
        t.meetingPermission = attachedProject.projectPermission.permission.meeting;
        t.riskPermission = attachedProject.projectPermission.permission.risk; 
        t.issuePermission = attachedProject.projectPermission.permission.issue;
      } else {
        t.taskPermission = workspaceLevelTaskPer;
        t.meetingPermission = workspaceLevelMeetingPer;
        t.riskPermission = workspaceLevelRiskPer;
        t.issuePermission = workspaceLevelIssuePer;
      }

      if (attachedProject && attachedProject.projectTemplate) {
        t.taskStatus = attachedProject.projectTemplate;
      } else {
        t.taskStatus = workspaceStatus;
      }

      return t;
    } else {
      t.taskPermission = workspaceLevelTaskPer;
      t.taskStatus = workspaceStatus;
      t.meetingPermission = workspaceLevelMeetingPer;
      t.riskPermission = workspaceLevelRiskPer;
      t.issuePermission = workspaceLevelIssuePer;
      return t;
    }
  };

  getIssuePer = i => {
    const { projects, tasks, issuePer } = this.props;
    if (i.linkedTasks && i.linkedTasks.length) {
      let attachedTask = tasks.find(t => t.taskId ==  i.linkedTasks[0]);
      if (attachedTask) {
        let attachedProject = projects.find(p => p.projectId == attachedTask.projectId);
        if (attachedProject) {
          i.issuePermission = attachedProject.projectPermission.permission.issue;
          return i;
        } else {
          i.issuePermission = issuePer;
          return i;
        }
      } else {
        i.issuePermission = issuePer;
        return i;
      }
    } else {
      i.issuePermission = issuePer;
      return i;
    }
  };
  getRiskPer = r => {
    const { projects, tasks, workspaceLevelRiskPer } = this.props;
    if (r.linkedTasks.length) {
      let attachedTask = tasks.find(t => t.taskId == r.linkedTasks[0]);
      if (attachedTask) {
        let attachedProject = projects.find(p => p.projectId == attachedTask.projectId);
        if (attachedProject) {
          r.riskPermission = attachedProject.projectPermission.permission.risk;
          return r;
        } else {
          r.riskPermission = workspaceLevelRiskPer;
          return r;
        }
      } else {
        r.riskPermission = workspaceLevelRiskPer;
        return r;
      }
    } else {
      r.riskPermission = workspaceLevelRiskPer;
      return r;
    }
  };
  getMeetingPer = m => {
    const { projects, tasks, meetingPer } = this.props;

    if (m.taskId) {
      let attachedTask = tasks.find(t => t.taskId == m.taskId);
      if (attachedTask) {
        let attachedProject = projects.find(p => p.projectId == attachedTask.projectId);
        if (attachedProject) {
          m.meetingPermission = attachedProject.projectPermission.permission.meeting;
          return m;
        } else {
          m.meetingPermission = meetingPer;
          return m;
        }
      } else {
        m.meetingPermission = meetingPer;
        return m;
      }
    } else {
      m.meetingPermission = meetingPer;
      return m;
    }
  };

  getPermission = (showDetails, type, data) => {
    if (showDetails) {
      switch (type) {
        case "Task": {
          // code block
          let task = this.getTaskPer(data);
          return task;
          break;
        }
        case "Issue": {
          // code block
          let issue = this.getIssuePer(data);
          return issue;
          break;
        }
        case "Meeting": {
          // code block
          let meeting = this.getMeetingPer(data);
          return meeting;
          break;
        } 
        case "Risk": {
          // code block
          let risk = this.getRiskPer(data);
          return risk;
          break;
        } 

        default:
          return data;
          break;
      }
    } else return data;
  };

  render() {
    const { theme, classes } = this.props;
    const {
      options,
      inputValue,
      selectedValue,
      data,
      showDetails,
      type,
      searchSelectStyle,
    } = this.state;
    let dataObject = this.getPermission(showDetails, type, data);
    return (
      <div className={classes.searchCnt}>
        {showDetails ? (
          <div>
            {type === "Task" ? (
              // <TaskDetails
              //   closeTaskDetailsPopUp={this.closeTaskDetailsPopUp}
              //   currentTask={dataObject}
              // />
              this.props.taskDetailDialogState(null,{
                id: dataObject.taskId,
                afterCloseCallBack: () => {
                  this.closeTaskDetailsPopUp()
                },
              })
            ) : null}
            {type === "Risk" ? (
              // <RiskDetails
              //   isUpdated={this.isUpdatedRisk}
              //   closeRiskDetailsPopUp={this.closeTaskDetailsPopUp}
              //   currentRisk={data}
              //   closeActionMenu={this.closeActionMenu}
              // />
              <>
              {this.props.riskDetailDialog({id :data.id})}
              {this.closeTaskDetailsPopUp()}
              </>
            ) : null}
            {type === "Meeting" ? (
              <MeetingDetails
                isUpdated={this.isUpdatedMeeting}
                closeMeetingDetailsPopUp={this.closeTaskDetailsPopUp}
                currentMeeting={data}
              />
            ) : null}
            {type === "Issue" ? (
              <>
              {this.props.issueDetailDialogState(null,{
                id: data.id,
                afterCloseCallBack: () => {
                  this.closeTaskDetailsPopUp();
                },
                isUpdated: this.isUpdatedIssue,
                closeActionMenu: this.closeActionMenu
              })}
              {this.closeTaskDetailsPopUp()}
              </>
              // <IssueDetails
              //   isUpdated={this.isUpdatedIssue}
              //   closeIssueDetailsPopUp={this.closeTaskDetailsPopUp}
              //   currentIssue={data}
              //   closeActionMenu={this.closeActionMenu}
              // />
            ) : null}
          </div>
        ) : null}
        <Select
          isMulti
          onInputChange={this.handleInputChange}
          ref={ref => {
            this.selectEle = ref;
          }}
          noOptionsMessage={() => {
            return (
              <FormattedMessage
                id="common.no-result-found.label"
                defaultMessage="No results found."
              />
            );
          }}
          cropWithEllipsis={true}
          inputValue={inputValue}
          components={{
            Option: this.CustomOptionComponent,
            MenuList: this.MenuList,
            ValueContainer: this.ValueContainer,
            DropdownIndicator: this.DropdownIndicator,
            ClearIndicator: this.ClearIndicator,
            Menu: this.Menu,
            IndicatorSeparator: () => {
              return false;
            },
          }}
          placeholder={this.props.intl.formatMessage({
            id: "common.search.label",
            defaultMessage: "Search",
          })}
          inputId={'globalSearch'}
          name="search"
          value={selectedValue}
          options={options}
          closeMenuOnSelect={false}
          autoFocus={false}
          openMenuOnClick={true}
          styles={searchSelectStyle}
          onChange={this.onSelectChange}
          onFocus={this.onSearchFocus}
          onBlur={this.onSearchBlur}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    tasks: state.tasks.data,
    projects: state.projects.data,
    issues: state.issues.data,
    risks: state.risks.data,
    meetings: state.meetings.data,
    members: state.profile.data.member.allMembers,
    profile: state.profile.data.profile,
    profileState: state.profile.data,
    workspaceLevelTaskPer: state.workspacePermissions.data.task,
    issuePer: state.workspacePermissions.data.issue,
    meetingPer: state.workspacePermissions.data.meeting,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    workspaceLevelMeetingPer: state.workspacePermissions.data.meeting,
    workspaceLevelRiskPer: state.workspacePermissions.data.risk,
    workspaceLevelIssuePer: state.workspacePermissions.data.issue,
    companyInfo: state.whiteLabelInfo.data,
  };
};
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, { UpdateRisk, UpdateMeeting, UpdateIssue, riskDetailDialog, taskDetailDialogState, issueDetailDialogState }),
  withStyles(searchStyles, { withTheme: true })
)(Search);
