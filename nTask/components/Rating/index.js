import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import style from "./style.js";

function RatingComponent({
  classes,
  theme,
  item,
  iteration,
  handleSelect,
  selectionNum,
  permission,
}) {
  const [selectedItems, setSelectedItems] = useState(selectionNum);
  const handleSelectItem = val => {
    if (selectionNum && selectionNum === val) {
      setSelectedItems(null);
      handleSelect(null);
      return;
    }
    setSelectedItems(val);
    handleSelect(val);
  };
  useEffect(() => {
    setSelectedItems(selectionNum);
  }, [selectionNum]);

  let arr = new Array(iteration);
  arr.fill("");

  return (
    <div>
      {arr.map((element, index) => {
        return (
          <span
            key={`rating_${index}`}
            data-rowClick="cell"
            onClick={() => permission && handleSelectItem(index + 1)}
            style={
              selectedItems && selectedItems - 1 >= index
                ? { filter: "none", cursor: "pointer" }
                : { filter: "grayscale(1)", cursor: "pointer" }
            }>
            {item}
          </span>
        );
      })}
    </div>
  );
}

RatingComponent.defaultProps = {
  classes: {},
  theme: {},
  iteration: 0,
  item: null,
  handleSelect: () => {},
  selectionNum: null,
  permission: true,
};

export default compose(withStyles(style, { withTheme: true }))(RatingComponent);
