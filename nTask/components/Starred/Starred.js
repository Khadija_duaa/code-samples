import React, { useState, useEffect, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import StarIcon from "@material-ui/icons/StarRate";
import CustomIconButton from "../Buttons/CustomIconButton";

function Stared(props) {
  const { theme, style, handleCallback, diabled = false } = props;
  const [isStared, setIsStared] = useState(false);
  const staredRef = useRef(null)
  const btnRef = useRef(null);

  useEffect(() => {
    btnRef.current && btnRef.current.addEventListener('click', handleStarClick, false);
    return () => {
      btnRef.current && btnRef.current.removeEventListener('click', handleStarClick, true)
    }
  }, []);

useEffect(() => {
  setIsStared(props.isStared);
  staredRef.current = props.isStared
}, [props.isStared]);

  const handleStarClick = (e) => {
    e.stopPropagation();
    if(!diabled){
      handleCallback(!staredRef.current);
      setIsStared(!staredRef.current);
      staredRef.current = !staredRef.current
    }
  };
  return (
    <CustomIconButton
      btnType="transparent"
      buttonRef={(node => btnRef.current = node)}
      id="starBtn"
      style={{
        opacity: isStared ? 1 : "",
        color: isStared ? "#FFC700" : "",
        ...style,
      }}
    >
      <StarIcon
        htmlColor={
          isStared
            ? theme.palette.background.star
            : theme.palette.secondary.light
        }
      />
    </CustomIconButton>
  );
}

export default withStyles({}, { withTheme: true })(Stared);
