export const styles = (theme) => ({
  CalendarMain: {
    padding: "1rem",
    fontFamily: theme.typography.fontFamilyLato,
  },
  rootChip: {
    fontSize: "0.69rem"
  },
  CalendarSettings: {
    borderBottom: `1px solid ${theme.palette.border.extraLightBorder}`,
  },
  workingDaySection: {
    fontSize: 12,
    display: "flex",
    justifyContent: "space-between",
    color: "#202020"
  },
  calendarHeading: {
    fontSize: 16,
    color: "#161717",
    marginBottom: 8,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightLarge,
  },
  calendarDescription: {
    fontSize: 14,
    color: "#646464",
    lineHeight: "20px",
    marginBottom: 15,
    display: 'block',
    fontFamily: theme.typography.fontFamilyLato,
    color: "#646464",
  },
  Card_inputLabel: {
    fontSize: 12,
    margin: "5px 0px !important",
    marginBottom: 10,
    color: "#161717",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  inputCenterLabel: {
    fontSize: 12,
    marginLeft: 10,
    marginRight: 10,
  },
  daysSelect: {
    marginTop: 9,
    listStyleType: "none",
    display: "flex",
    padding: 0,
    margin: 0,
    "& li:first-child": {
      borderRadius: "4px 0 0 4px",
      marginLeft: 0,
    },
    "& li:last-child": {
      borderRadius: "0 4px 4px 0",
    },
  },
  pointer: {
    cursor: "pointer",
  },
  daySlot: {
    padding: "11.5px 12px",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    marginLeft: "-1px",
    margin: 0,
    background: theme.palette.common.white,
    fontSize: 12,
    width: "100%",
    textAlign: "center",
    transition: "0.4s ease all",
    "&:hover": {
      background: "#EBF6FF",
    },
  },
  selectedDay: {
    borderColor: "#0090FF !important",
    background: "#EBF6FF",
    color: "#0090FF",
    position: "relative"
  },
  // exception stayles starts here
  Exceptions: {
    paddingTop: 15,
  },
  ExceptionHeading: {
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: 12,
    color: "#111111",
    fontWeight: theme.typography.fontWeightLarge,
    marginBottom: 15,
    paddingBottom: 15,
    borderBottom: `1px solid ${theme.palette.border.extraLightBorder}`,
  },
  toggleBtnGroup: {
    borderRadius: 4,
    background: theme.palette.common.paper,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    width: "fit-content",
  },
  leftSelected: {
    color: "#fff !important",
    backgroundColor: "#F24C4C !important",
    borderColor: "#F24C4C  !important",
    "&:after": {
      background: "#F24C4C",
    },
    "&:hover": {
      background: "#F24C4C",
    },
  },
  rightSelected: {
    color: "#fff !important",
    backgroundColor: "#F9841E !important",
    borderColor: "#F9841E !important",
    "&:after": {
      background: "#F9841E",
    },
    "&:hover": {
      background: "#F9841E",
    },
  },
  Card_toggleButton: {
    height: "auto",
    padding: "12px 25px",
    lineHeight: "normal",
    fontSize: 12,
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&[value = 'center']": {
      borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  btnContainer: {
    background: "transparent",
    borderTop: `1px solid ${theme.palette.border.extraLightBorder}`,
    paddingTop: 15,
    marginTop: 15,
  },
  Card_AddExceptionModol: {
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    paddingBottom: 16,
  },
  spanDropDown: {
    color: "#0000001F",
    fontSize: 18,
    padding: "0 2px",
  },
  dropDownChild: {
    display: "flex",
    alignItems: "center",
    marginTop: 5,
  },
  dropDownName: {
    display: "flex",
    alignItems: "center",
  },
  days: {
    fontSize: 12,
    color: "black",
  },
  horizontalLine: {
    borderLeft: '1px solid #DDDDDD',
    alignSelf: 'stretch',
    margin: "0 5px",
    width: 0,
  },
  dropDownHours: {
    backgroundColor: "#F1F1F1",
    borderRadius: "6px",
    fontSize: 13,
    padding: "2px 10px",
    color: "#333333",
    margin: "0 10px",
    display: "flex",
    alignItems: "center",
  },
  dropDownTotalHours: {
    backgroundColor: "#F1F1F1",
    borderRadius: "6px",
    fontSize: 13,
    padding: "2px 10px",
    color: "#333333",
    margin: "0 10px",
  },
  bottomDiolog: {
    borderTop: "1px solid #EAEAEA",
    display: "flex",
    justifyContent: "end",
    padding: "10px 20px",
  },
  modelText: {
    fontSize: "14px",
    color: "#202020",
    fontFamily: theme.typography.fontFamilyLato,
  },
  calenderLoader: {
    height: window.innerHeight - 325,
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  editScheduleHeading: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
  },
  editScheduletext: {
    fontWeight: "bold",
    fontSize: "18px",
  },
  configCnt: {
    maxWidth: 685
  },

  createSchedule: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
  },
  CreateScheduleText: {
    fontWeight: "bold",
    fontSize: "18px",
    fontFamily: theme.typography.fontFamilyLato,
  },
  SelectWorkScheduletext:
  {
    fontWeight: "bold",
    fontSize: "16px",
    fontFamily: theme.typography.fontFamilyLato,
  },

  editActionBtn: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  SelectWorkSchedule: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    margin: "0px 0px 0px 20px"
  },
  addIcon: {
    fontSize: "1rem",
    marginRight: 7,
    transform: "scale(1.5)",
  },
  calenderNameText: {
    fontWeight: 600,
    fontSize: 14,
    fontFamily: theme.typography.fontFamilyLato,
    marginBottom: ".5rem",
  },
  chip: {
    backgroundColor: theme.palette.background.items,
    borderRadius: "6px",
    fontSize: 13,
    padding: "2px 10px",
    color: theme.palette.text.lightGray,
    margin: "2px",
  },
  actionBtnperrnt: {
    display: "flex",
    flexWrap: "nowrap",
    alignItems: "center",
  },
  actionBtn: {
    marginRight: "9px",
    padding: "5px",
  },
  defaultChip: {
    height: "24px",
    borderRadius: "4px",
    marginLeft: "14px",
  },
  dellModel: {
    padding: "20px 20px 40px 20px",
  },
  dellModelText: {
    fontSize: "15px",
    color: "#202020",
  },
  ModelHeader: {
    borderTop: "1px solid #EAEAEA",
    display: "flex",
    justifyContent: "end",
    padding: "10px 20px",
  },
  ExceptionsCardCompoPerrent: {
    // borderTop: `1px solid ${theme.palette.border.extraLightBorder}`,
    // marginTop: 20,
    paddingBottom: 15,
  },
  modelTextPerrent: {
    padding: "20px",
  },
  WorkingHoursDropdownPerent: {
    display: "flex",
    flexWrap: "wrap",
    margin: "10px -8px 0px -8px",

  },
  WorkingHoursDropdown: {
    padding: 8,
    width: "70%",
  },
  DefaultTextFieldWorkingHours: {
    padding: '8px 8px 0px 8px',
    width: "30%",
  },
  UpdateActionsBTns: {
    textAlign: "right",
  },
  AddExceptionBtn: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    // marginTop: 20

  },
  spanDropDownDays: {
    color: "#0000001F",
    fontSize: 18,
    padding: "0 2px 0 3px",
  },
  Card_Container: {
    padding: ".5rem 1rem 1rem",
    background: "white",
    position: "relative",
    marginTop: 14,
    borderLeftWidth: "4px",
    border: '1px solid #DDDDDD',
    boxShadow: "none",
    "&:hover": {
      transition: 'box-shadow .250s ease-in-out',
      boxShadow: "0px 3px 6px #0000001A",
      "& .icondelete": {
        pointerEvents: "auto",
        opacity: "1",
        visibility: "visible"
      }
    }
  },
  Card_Heading: {
    fontSize: 14,
    fontWeight: "bold",
    fontFamily: theme.typography.fontFamilyLato,
  },
  Card_Body: {
    marginTop: 10,
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center"
  },
  Card_iconRow: {
    display: "flex",
    alignItems: "center"
  },
  Card_iconText: {
    fontSize: 14,
    marginLeft: 7,
    fontFamily: theme.typography.fontFamilyLato,
    display: "inline-block"
  },
  Card_dell_icon: {
    display: "flex",
    pointerEvents: "none",
    opacity: "0",
    visibility: "hidden"
  },
  Card_actionBtn: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between"
  },
  Card_dellBtn: {
    padding: 6,
    backgroundColor: "transparent"
  },
  Card_editIcon: {
    padding: 6,
    backgroundColor: "transparent",
    marginLeft: 5
  },
  WorkhoursDropdown_Container: {
    position: 'relative',
  },
  WorkhoursDropdown_inputWrapper: {
    color: '#202020',
    fontSize: 12,
    border: '1px solid #DDDDDD !important',
    padding: 4,
    borderRadius: 4,
    minHeight: 40,
    paddingTop: 0,
    paddingRight: 25,
    position: 'relative',
  },
  WorkhoursDropdown_inputTag: {
    padding: ' 7px 11px',
    background: theme.palette.text.brightBlue,
    borderRadius: 5,
    display: 'inline-block',
    color: '#fff',
    marginTop: "4px",
    marginRight: "4px"
  },
  WorkhoursDropdown_removeTags: {
    position: 'absolute',
    top: '50%',
    right: 5,
    transform: 'translateY(-50%)',
    '& svg': {
      fontSize: 18,
    },

  },
  WorkhoursDropdown_inputLabel: {
    fontSize: 12,
    margin: 0,
    marginBottom: 10,
    color: '#161717',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  WorkhoursDropdown_timeSlotsWrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: '3.5px',
    justifyContent: 'space-between',
  },
  WorkhoursDropdown_timeSlot: {
    width: 'calc(100% / 4)',
    minWidth: 95,
    fontSize: 14,
    padding: 0,
  },
  WorkhoursDropdown_timeSlotText: {
    background: theme.palette.background.grayLighter,
    margin: '3.5px',
    width: '100%',
    textAlign: 'center',
    padding: 10,
    transition: '0.4s ease all',
    borderRadius: 5,
    fontWeight: 300,
    cursor: 'pointer',
    "&:hover": {
      color: theme.palette.common.black,
      background: '#c9e7ff'
    }
  },
  WorkhoursDropdown_selectedSlot: {
    "& span": {
      background: `${theme.palette.text.brightBlue} !important`,
      color: '#fff !important',
    }
  },
  RepeatCheckBox: {
    display: "flex",
    alignItems: "center",
    marginTop: 12,
    borderBottom: "1px solid #DDDDDD",
    marginBottom: 14,
    paddingBottom: 14
  },
  repeatCustomSelect: {
    margin: "-8px",
    display: "flex",
    flexWrap: "wrap",
    marginBottom: 5,
    justifyContent: "space-between",
  },
  WorkingHours: {
    margin: "15px -8px",
    marginBottom: "-25px",
    display: "flex"
  },
  searchInput: {
    width: 280,
    "& $input": {
      paddingLeft: 35,
    },

  },
  searchInputCnt: {
    display: "flex",
    alignItems: "center",
    width: 200,

  },
  searchIcon: {
    marginRight: "-30px",
    color: "#969696",
    zIndex: 1000,
  },

});
