import { parse } from "date-fns";

export const ExceptionType = {
  0: 'DayOff',
  1: 'CustomDay',
  DayOff: 0,
  CustomDay: 1
}
export const ExceptionRepeat = {
  0: 'Week',
  1: 'Month',
  2: 'Year',
  Week: 0,
  Month: 1,
  Year: 2
}

export const ExceptionRepeatLimit = {
  0: 'RepeatForever',
  1: 'RepeatTill',
  RepeatForever: 0,
  RepeatTill: 1,
}

export const DAYSOPTIONS = [
  { label: "Mon", value: { day: "Monday", id: 1 } },
  { label: "Tue", value: { day: "Tuesday", id: 2 } },
  { label: "Wed", value: { day: "Wednesday", id: 3 } },
  { label: "Thu", value: { day: "Thursday", id: 4 } },
  { label: "Fri", value: { day: "Friday", id: 5 } },
  { label: "Sat", value: { day: "Saturday", id: 6 } },
  { label: "Sun", value: { day: "Sunday", id: 0 } },
];

export const TimeSlots = [
  "00:00:00",
  "01:00:00",
  "02:00:00",
  "03:00:00",
  "04:00:00",
  "05:00:00",
  "06:00:00",
  "07:00:00",
  "08:00:00",
  "09:00:00",
  "10:00:00",
  "11:00:00",
  "12:00:00",
  "13:00:00",
  "14:00:00",
  "15:00:00",
  "16:00:00",
  "17:00:00",
  "18:00:00",
  "19:00:00",
  "20:00:00",
  "21:00:00",
  "22:00:00",
  "23:00:00",
];


export const parseDate = (date) => parse(date, "HH:mm:ss", new Date());
