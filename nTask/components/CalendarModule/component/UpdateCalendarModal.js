import React from "react";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { withStyles } from "@material-ui/core/styles";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";
import Typography from "@material-ui/core/Typography";
import { compose } from "redux";
import CustomButton from "../../Buttons/CustomButton";
import CustomDialog from "../../Dialog/CustomDialog";
import { withSnackbar } from "notistack";
import { styles } from "../Calendar.style";

const UpdateCalendarModal = (props) => {
  const { open, onClose, onSubmit, classes, loading } = props;

  return (
    <CustomDialog
      title="Update Work Schedule"
      dialogProps={{
        open: open,
        onClose: onClose,
        PaperProps: { style: { maxWidth: 500 } },
      }}>
      <div className={classes.modelTextPerrent}>
        <Typography className={classes.modelText}>
          This might affect the dates and timeline of the project and resources. Are you sure you
          want to update the work schedule of this project?
        </Typography>
      </div>

      <div className={classes.bottomDiolog}>
        <CustomButton
          onClick={onClose}
          style={{ marginRight: "10px", textTransform: "capitalize" }}>
          Cancel
        </CustomButton>
        <CustomButton
          btnType="success"
          variant="contained"
          query={ loading && "progress"}
          disabled={loading}
          onClick={onSubmit}>
          Update Schedule
        </CustomButton>
      </div>
    </CustomDialog>
  );
};

export default compose(
  withSnackbar,
  withStyles(styles, { withTheme: true })
)(UpdateCalendarModal);
