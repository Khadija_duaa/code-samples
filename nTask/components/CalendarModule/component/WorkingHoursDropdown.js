import React, { useEffect, useState } from "react";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { withStyles } from "@material-ui/core/styles";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";
import { styles } from "../Calendar.style";
import InputLabel from "@material-ui/core/InputLabel";
import DropdownMenu from "../../Dropdown/DropdownMenu";
import ListItem from "@material-ui/core/ListItem";

import clsx from "clsx";
import { addDays, addHours, format, isAfter, isBefore, isEqual, subHours } from "date-fns";
import { parseDate, TimeSlots } from "../constant";

const addDate = (slots, value) => {
  let updated = false;
  const newValue = slots.reduce((acc, { timeFrom, timeTo }, index, array) => {
    const matchstart = isEqual(parseDate(value), subHours(parseDate(timeFrom), 1));
    const matchend = isEqual(parseDate(value), addDays(parseDate(timeTo), timeTo === "00:00:00" ? 1 : 0));
    const inside =
      isEqual(parseDate(array.at(index - 1)?.timeTo), parseDate(value)) &&
      isEqual(subHours(parseDate(timeFrom), 1), parseDate(value));

    updated = updated || matchstart || matchend || inside;

    if (inside) {
      const clonevalue = [...acc];
      const lastindex = clonevalue.pop();
      return [
        ...clonevalue,
        {
          timeFrom: matchend ? timeFrom : lastindex.timeFrom,
          timeTo: matchstart ? timeTo : lastindex.timeTo,
        },
      ];
    }

    return [
      ...acc,
      {
        timeFrom: matchstart ? format(subHours(parseDate(timeFrom), 1), "HH:mm:ss") : timeFrom,
        timeTo: matchend ? format(addHours(parseDate(timeTo), 1), "HH:mm:ss") : timeTo,
      },
    ];
  }, []);

  if (!updated) {
    return [
      ...newValue,
      { timeFrom: value, timeTo: format(addHours(parseDate(value), 1), "HH:mm:ss") },
    ].sort((a, b) => a.timeFrom.localeCompare(b.timeFrom));
  } else return newValue;
};

const removeDate = (selectedSlot, value) => {
  return selectedSlot.reduce((acc, { timeFrom, timeTo }) => {
    const matchstart = value === timeFrom;
    const matchend = value === format(subHours(parseDate(timeTo), 1), "HH:mm:ss");
    const inside =
      isAfter(parseDate(value), parseDate(timeFrom)) &&
      isBefore(parseDate(value), subHours(parseDate(timeTo), 1));

    if (matchstart && matchend) return acc;

    if (inside)
      return [
        ...acc,
        {
          timeFrom: timeFrom,
          timeTo: format(subHours(parseDate(value), 0), "HH:mm:ss"),
        },
        {
          timeFrom: format(addHours(parseDate(value), 1), "HH:mm:ss"),
          timeTo: timeTo,
        },
      ];

    return [
      ...acc,
      {
        timeFrom: matchstart ? format(addHours(parseDate(timeFrom), 1), "HH:mm:ss") : timeFrom,
        timeTo: matchend ? format(subHours(parseDate(timeTo), 1), "HH:mm:ss") : timeTo,
      },
    ];
  }, []);
};

const isSelected = (slot, value) => {
  return value.some(({ timeFrom, timeTo }) => {
    const inside =
      isAfter(parseDate(slot), parseDate(timeFrom)) && isBefore(parseDate(slot), parseDate(timeTo));
    return (
      inside || isEqual(parseDate(slot), parseDate(timeFrom))
      // || isEqual(parseDate(slot), parseDate(timeTo))
    );
  });
};

function WorkingHoursDropdown(props) {
  const {
    theme,
    classes,
    value: defaultValue = [
      {
        timeFrom: "08:00:00",
        timeTo: "17:00:00",
      },
    ],
    onChange,
    backgroundColor,
    icon,
    disabled,
    readOnly,
  } = props;
  const [anchorEl, setAnchorEl] = useState(null);

  const [value, setValue] = useState(defaultValue);

  const updateValue = (value) => {
    setValue(value);
    onChange?.(value);
  };

  const onSelectSlot = (slot) => {
    const selected = isSelected(slot, value);

    if (!selected) {
      const data = addDate(value, slot);
      return updateValue(data);
    }

    const newValue = removeDate(value, slot);
    updateValue(newValue);
  };
  useEffect(() => {
    if (defaultValue)
      setValue(defaultValue)


  }, [defaultValue])

  return (
    <div className={classes.WorkhoursDropdown_Container}>
      <InputLabel classes={{ root: classes.WorkhoursDropdown_inputLabel }}>
        Working Hours
      </InputLabel>
      <div
        style={backgroundColor}
        className={classes.WorkhoursDropdown_inputWrapper}
        onClick={({ currentTarget }) => !readOnly ? setAnchorEl(currentTarget) : {}}
        buttonRef={anchorEl}>
        {value.map((slot) => (
          <span className={classes.WorkhoursDropdown_inputTag}>
            {format(parseDate(slot.timeFrom), "hh:mm a")}
            {" - "}
            {format(parseDate(slot.timeTo), "hh:mm a")}
          </span>
        ))}

        {!!value.length && (
          <span
            className={classes.WorkhoursDropdown_removeTags}
            onClick={(event) => {
              if (readOnly) return;
              event.stopPropagation();
              updateValue([]);
            }}>
            {icon}
          </span>
        )}
      </div>
      <DropdownMenu
        open={!!anchorEl}
        closeAction={() => setAnchorEl(null)}
        anchorEl={anchorEl}
        size="large"
        placement="bottom-start"
        style={{ width: "100%", minWidth: 500 }}>
        <div className={classes.WorkhoursDropdown_timeSlotsWrapper}>
          {TimeSlots.map((slot) => (
            <ListItem
              className={clsx({
                [classes.WorkhoursDropdown_timeSlot]: true,
                [classes.WorkhoursDropdown_selectedSlot]: isSelected(slot, value),
              })}
              onClick={() => onSelectSlot(slot)}>
              <span className={classes.WorkhoursDropdown_timeSlotText}>
                {format(parseDate(slot), "hh:mm a")}
              </span>
            </ListItem>
          ))}
        </div>
      </DropdownMenu>
    </div>
  );
}
WorkingHoursDropdown.defaultProps = {};

export default withStyles(styles, { withTheme: true })(WorkingHoursDropdown);
