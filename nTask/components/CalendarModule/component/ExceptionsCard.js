import React from "react";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { withStyles } from "@material-ui/core/styles";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";
import { styles } from "../Calendar.style";
import { Card, IconButton } from "@material-ui/core";
import CalendarIcon from "../../Icons/CalendarIcon";
import RepeatIcon from "../../Icons/RepeatIcon";
import ClockIcon from "../../Icons/ClockIcon";
import IconDeleteAutomation from "../../Icons/IconDeleteAutomation";
import EditIcon from "@material-ui/icons/Edit";
import { ExceptionRepeat, ExceptionRepeatLimit, parseDate } from "../constant";
import { format } from "date-fns";
import { lowerCase } from "lodash";

const ExceptionsCard = (props) => {
  const { classes, onEdit, onDelete, item, readOnly } = props;
  const {
    title,
    type,
    isRepeated,
    customWorkingHours,
    customDailyCapacity,
    repeatType,
    repeatLimit,
    repeatLimitDate,
    fromDate,
    toDate,
  } = item;

  return (
    <Card
      className={classes.Card_Container}
      style={{ borderLeftColor: type === 0 ? "red" : "orange", borderRadius: "6px" }}>
      <div className={classes.Card_actionBtn}>
        <div className={classes.Card_Heading}>{title}</div>
        {!readOnly ?
          <div className={`icondelete ${classes.Card_dell_icon}`}>
            <IconButton className={classes.Card_editIcon} onClick={() => onEdit(item)}>
              <EditIcon style={{ fontSize: 18 }} color="disabled" />
            </IconButton>
            <IconButton className={classes.Card_dellBtn} onClick={() => onDelete(item)}>
              <IconDeleteAutomation />
            </IconButton>
          </div> : null}
      </div>
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        <div style={{ marginTop: 8, marginRight: 16 }} className={classes.Card_iconRow}>
          <CalendarIcon />
          <div className={classes.Card_iconText}>
            <span>{format(new Date(fromDate), "MMM dd, yyyy")}</span>

            {!!repeatLimitDate && <span>{" - "} {format(new Date(repeatLimitDate), "MMM dd, yyyy")}</span>}
            {!!toDate && <span>{" - "} {format(new Date(toDate), "MMM dd, yyyy")}</span>}
          </div>
        </div>
        {(!!customWorkingHours?.length || !!customDailyCapacity) && (
          <div style={{ marginTop: 8 }} className={classes.Card_iconRow}>
            <ClockIcon />
            <div className={classes.Card_iconText}>
              {!!customWorkingHours?.length
                ? customWorkingHours
                  ?.slice(0, 2)
                  .map(
                    ({ timeFrom, timeTo }) =>                    
                      ` ${format(parseDate(timeFrom), "hh:mm a")} - ${format(parseDate(timeTo), "hh:mm a")}`
                  )
                  .join(" | ")
                : `${customDailyCapacity} hours`}
              {customWorkingHours?.length > 2 && <span style={{ margin: "0px 5px" }}>| ...</span>}
            </div>
          </div>
        )}

        {isRepeated && (
          <div style={{ marginTop: 8 }} className={classes.Card_iconRow}>
            <RepeatIcon />
            <div className={classes.Card_iconText}>
              Repeat every {lowerCase(ExceptionRepeat[repeatType])}
              {repeatLimit === ExceptionRepeatLimit.RepeatTill && (
                <> till {format(new Date(repeatLimitDate), "MMM dd, yyyy")} </>
              )}
            </div>
          </div>
        )}
      </div>
    </Card>
  );
};
export default withStyles(styles, { withTheme: true })(ExceptionsCard);
