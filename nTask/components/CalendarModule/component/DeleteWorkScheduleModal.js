import React from "react";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { withStyles } from "@material-ui/core/styles";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";
import { compose } from "redux";
import CustomButton from "../../Buttons/CustomButton";
import CustomDialog from "../../Dialog/CustomDialog";
import { withSnackbar } from "notistack";
import { styles } from "../Calendar.style";

const DeleteWorkScheduleModal = (props) => {
  const { open, onClose, onSubmit, classes, loading } = props;

  return (
    <CustomDialog
      title="Delete Work Schedule"
      dialogProps={{
        open: open,
        onClose: onClose,
        PaperProps: { style: { maxWidth: 500 } },
      }}>
      <div className={classes.dellModel}>
        <div className={classes.dellModelText}>
          Are you sure you want to delete this work schedule?
        </div>
      </div>
      <div className={classes.ModelHeader}>
        <CustomButton
          onClick={onClose}
          style={{ marginRight: "10px", textTransform: "capitalize" }}>
          Cancel
        </CustomButton>
        <CustomButton
          query={loading && "progress"}
          btnType="danger"
          variant="contained"
          onClick={onSubmit}>
          Delete Schedule
        </CustomButton>
      </div>
    </CustomDialog>
  );
};

export default compose(
  withSnackbar,
  withStyles(styles, { withTheme: true })
)(DeleteWorkScheduleModal);
