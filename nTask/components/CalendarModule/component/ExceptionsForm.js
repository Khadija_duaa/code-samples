import React, { useEffect, useState } from "react";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { withStyles } from "@material-ui/core/styles";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";
import { styles } from "../Calendar.style";
import Typography from "@material-ui/core/Typography";
import { InputAdornment, InputLabel } from "@material-ui/core";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import StaticDatePicker from "../../DatePicker/StaticDatePicker";
import DefaultTextField from "../../Form/TextField";
import ButtonActionsCnt from "../../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import DefaultCheckbox from "../../Form/Checkbox";
import CustomSelect from "../../Dropdown/CustomSelect/CustomSelect.view";
import ClearIcon from "@material-ui/icons/Clear";
import WorkingHoursDropdown from "./WorkingHoursDropdown";
import { addDays, addYears, differenceInHours } from "date-fns";
import { clamp, isEmpty } from "lodash";
import { ExceptionRepeat, ExceptionRepeatLimit, ExceptionType, parseDate } from "../constant";
import moment from "moment";
import ToggleButton from "@material-ui/lab/ToggleButton";

const repeatTillOptions = [
  { label: "Repeat Till", value: ExceptionRepeatLimit.RepeatTill },
  { label: "Repeat For Ever", value: ExceptionRepeatLimit.RepeatForever },
];

const repeatTypeOptions = [
  { label: "Week", value: ExceptionRepeat.Week },
  { label: "Month", value: ExceptionRepeat.Month },
  { label: "Year", value: ExceptionRepeat.Year },
];

const InitialValue = {
  title: undefined,
  type: ExceptionType.DayOff,
  isRepeated: false,
  customWorkingHours: [],
  customDailyCapacity: 0,
  fromDate: new Date().toISOString(),
  toDate: new Date().toISOString(),
  repeatLimit: undefined,
  repeatType: undefined,
  repeatLimitDate: undefined,
};

const ExceptionsForm = (props) => {
  const { value, theme, classes, onCancel, onSubmit } = props;

  const [loading, setLoading] = useState(false);

  const [data, setData] = useState(value ?? InitialValue);
  const {
    title,
    type,
    isRepeated,
    customWorkingHours,
    customDailyCapacity,
    repeatLimit,
    repeatType,
    fromDate,
    toDate,
    repeatLimitDate,
  } = data;

  useEffect(() => {
    if (!isEmpty(value)) setData(value);
  }, [value]);

  const handleSubmit = async () => {
    setLoading(true);
    let obj = { ...data };
    obj.toDate = obj.isRepeated ? undefined : obj.toDate;
    await onSubmit?.(obj);
    setLoading(false);
  };

  const handleCancel = () => {
    onCancel?.();
  };
  const handleRepeat = (value) => {
    setData((prev) =>
      value
        ? {
          ...prev,
          isRepeated: true,
          // toDate: undefined,
          repeatType: ExceptionRepeat.Week,
          repeatLimit: ExceptionRepeatLimit.RepeatTill,
          repeatLimitDate: fromDate,
        }
        : {
          ...prev,
          isRepeated: false,
          // toDate: new Date().toISOString(),
          repeatLimit: undefined,
          repeatType: undefined,
        }
    )
  };

  return (
    <div className={classes.Card_ExceptionsForm}>
      <Typography variant="h4" classes={{ h4: classes.ExceptionHeading }}>
        {value ? "Edit" : "Add"} Exception
      </Typography>

      <DefaultTextField
        label="Title"
        placeholder="Enter title (Optional)"
        type="text"
        defaultValue={title}
        defaultProps={{ inputProps: { maxLength: 250 } }}
        formControlStyles={{
          marginBottom: 22
        }}
        onChange={({ target: { value } }) => setData((prev) => ({ ...prev, title: value }))}
      />

      <div style={{ display: "flex", flexWrap: "wrap", margin: "-16px -8px -8px" }}>
        <div style={{ padding: 8, width: "50%" }}>
          <StaticDatePicker
            label={isRepeated ? "Date" : "From"}
            isInput
            isCreation
            style={{ flex: 1 }}
            date={moment(fromDate)}
            selectedSaveDate={(date) =>
              setData((prev) => ({ ...prev, fromDate: date.toDate().toISOString() }))
            }
            maxDate={toDate ? new Date(toDate) : undefined}
          />
        </div>
        {!isRepeated && (
          <div style={{ padding: 8, width: "50%" }}>
            <StaticDatePicker
              label="To"
              isInput
              isCreation
              style={{ flex: 1 }}
              date={moment(toDate)}
              selectedSaveDate={(date) =>
                setData((prev) => ({ ...prev, toDate: date.toDate().toISOString() }))
              }
              minDate={toDate ? new Date(fromDate) : undefined}
            />
          </div>
        )}
      </div>

      <div className={classes.Card_AddExceptionModol}>
        <InputLabel classes={{ root: classes.Card_inputLabel }}>Type</InputLabel>
        <ToggleButtonGroup
          value={type}
          exclusive
          onChange={(_, value) =>
            value !== null &&
            setData((prev) => ({
              ...prev,
              type: value,
              customDailyCapacity: value === ExceptionType.CustomDay ? 0 : undefined,
              customWorkingHours: value === ExceptionType.CustomDay ? [] : undefined,
            }))
          }
          classes={{ root: classes.toggleBtnGroup }}>
          <ToggleButton
            value={ExceptionType.DayOff}
            classes={{ root: classes.Card_toggleButton, selected: classes.leftSelected }}>
            Day off
          </ToggleButton>
          <ToggleButton
            value={ExceptionType.CustomDay}
            classes={{ root: classes.Card_toggleButton, selected: classes.rightSelected }}>
            Custom Day
          </ToggleButton>
        </ToggleButtonGroup>

        {type === ExceptionType.CustomDay && (
          <div className={classes.WorkingHours}>
            <div style={{ padding: 8, width: "70%" }}>
              <WorkingHoursDropdown
                value={customWorkingHours}
                icon={<ClearIcon htmlColor={theme.palette.secondary.medDark} />}
                onChange={(value) =>
                  setData((prev) => ({
                    ...prev,
                    customWorkingHours: value,
                    customDailyCapacity: value.reduce((acc, item) => {
                      return (
                        acc + differenceInHours(addDays(parseDate(item.timeTo), item.timeTo === "00:00:00" ? 1 : 0), parseDate(item.timeFrom))
                      );
                    }, 0),
                  }))
                }
              />
            </div>
            <div style={{ padding: 8, width: "30%" }}>
              <DefaultTextField
                disabled={!isEmpty(customWorkingHours)}
                label="Daily Capacity"
                type="number"
                value={customDailyCapacity}
                placeholder="Enter title (Optional)"
                inputProps={{ maxLength: 250 }}
                endAdornment={<InputAdornment position="end" ><p style={{ fontSize: "12px", color: "#202020" }}>hours</p></InputAdornment>}
                onChange={({ target: { value } }) =>
                  setData((prev) => ({
                    ...prev,
                    customDailyCapacity: Number(clamp(value, 0, 24)),
                    customWorkingHours: [],
                  }))
                }
              />
            </div>
          </div>
        )}
      </div>

      <label className={classes.RepeatCheckBox}>
        <DefaultCheckbox
          onChange={(_, value) => handleRepeat(value)}
          checked={isRepeated}
          checkboxStyles={{ padding: 0, marginRight: 8 }}
        />
        <Typography>{isRepeated ? "Repeat Every" : "Repeat"}</Typography>
      </label>

      {isRepeated && (
        <div className={classes.repeatCustomSelect}>
          <div style={{ padding: 8, width: "50%" }}>
            <CustomSelect
              showCheck={false}
              bgapply={false}
              style={{
                width: "100%",
                padding: '7px 8px',
                justifyContent: "space-between",
                backgroundColor: "white",
                display: "flex",
                border: "1px solid #EAEAEA",
              }}
              value={repeatType}
              onChange={({ value }) => setData((prev) => ({ ...prev, repeatType: value }))}
              options={repeatTypeOptions}
            />
          </div>

          {repeatLimit !== ExceptionRepeatLimit.RepeatForever && (
            <div style={{ padding: 8, margin: "-20px 0", width: "50%" }}>
              <StaticDatePicker
                isInput
                isCreation
                style={{ flex: 1 }}
                date={moment(repeatLimitDate)}
                minDate={moment(fromDate)}
                maxDate={moment(addYears(new Date(fromDate), 5).toISOString())}
                selectedSaveDate={(date) =>
                  setData((prev) => ({
                    ...prev,
                    repeatLimitDate: date.toDate().toISOString(),
                  }))
                }
              />
            </div>
          )}
        </div>
      )}

      <div style={{ marginTop: 10 }}>
        <ButtonActionsCnt
          cancelAction={handleCancel}
          successAction={handleSubmit}
          successBtnText="Save Exception"
          cancelBtnText="Cancel"
          customGridProps={{ classes: { container: classes.Card_btnContainer } }}
          btnType="success"
          btnQuery={loading && "progress"}
        />
      </div>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(ExceptionsForm);
