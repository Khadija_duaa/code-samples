import React, { useEffect, useState } from "react";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { withStyles } from "@material-ui/core/styles";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";
import { styles } from "./Calendar.style";
import ExceptionsCard from "./component/ExceptionsCard";
import Typography from "@material-ui/core/Typography";
import ExceptionsForm from "./component/ExceptionsForm";
import CustomButton from "../Buttons/CustomButton";
import TextField from "../Form/TextField";
import { DAYSOPTIONS, parseDate } from "./constant";
import { InputAdornment, InputLabel } from "@material-ui/core";
import DefaultTextField from "../Form/TextField";
import { clamp, isEmpty } from "lodash";
import { addDays, differenceInHours } from "date-fns";
import { compose } from "redux";
import { withSnackbar } from "notistack";
import { deleteException, saveException, updateException } from "../../redux/actions/calendar";
import WorkingHoursDropdown from "./component/WorkingHoursDropdown";
import SearchIcon from "@material-ui/icons/Search";
import SearchInput from "react-search-input";
import ClearIcon from "@material-ui/icons/Clear";
import DeleteExceptionCardModel from "./component/DeleteExceptionCardModel";
import CustomTooltip from "../Tooltip/Tooltip";
import clsx from "clsx";

const InitialValue = {
  title: "",
  workingdays: DAYSOPTIONS.slice(0, 5).map((e) => e.value),
  workingHours: [
    {
      timeFrom: "08:00:00",
      timeTo: "17:00:00",
    },
  ],
  dailyCapacity: 9,
  isSystemCalender: false,
  projectId: null,
  exceptions: [],
};
const CalendarForm = (props) => {
  const { theme, classes, onCancel, onSubmit, defaultValue, disabled, style, enqueueSnackbar, canCreate, readOnly } = props;

  const showSnackBar = (message, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{message}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(false);
  const [selected, setSelected] = useState({ data: undefined, type: undefined, loading: false });

  const [data, setData] = useState(
    defaultValue
      ? { ...defaultValue, exceptions: [...defaultValue.exceptions]?.reverse() }
      : InitialValue
  );

  useEffect(() => {
    if (defaultValue) setData({ ...defaultValue, exceptions: [...defaultValue.exceptions]?.reverse() });
  }, [defaultValue]);

  const onSubmitException = async (exception) => {
    setSelected((prev) => ({ ...prev, loading: true }));

    if (defaultValue) {
      if (selected.type === "new") {
        await saveException(
          { calenderId: defaultValue.calenderId, ...exception },
          (resp) => {
            setData({ ...data, exceptions: resp.exceptions });
            setSelected({ data: undefined, type: undefined, loading: false });
            showSnackBar("Exception create successfully", "success");
          },
          (error) => showSnackBar(error.data.message, "error")
        );
      } else {
        await updateException(
          { calenderId: defaultValue.calenderId, ...exception },
          (resp) => {
            setData({ ...data, exceptions: resp.exceptions });
            setSelected({ data: undefined, type: undefined, loading: false });
            showSnackBar("Exception updated successfully", "success");
          },
          (error) => showSnackBar(error.data.message, "error")
        );
      }
    } else {
      const newval =
        selected.type === "new"
          ? {
            ...data,
            exceptions: [
              { ...exception, exceptionId: data.exceptions.length },
              ...data.exceptions,
            ],
          }
          : {
            ...data,
            exceptions: data.exceptions.map((item) =>
              item.exceptionId === exception.exceptionId ? exception : item
            ),
          };
      setData(newval);
      setSelected({ data: undefined, type: undefined, loading: false });
    }
  };

  const onDeleteException = async (exception) => {
    setSelected((prev) => ({ ...prev, loading: true }));
    if (defaultValue)
      await deleteException({
        calenderId: defaultValue.calenderId,
        exceptionId: exception.exceptionId,
      });
    showSnackBar("Exception deleted successfully", "success");
    setData((prev) => ({
      ...prev,
      exceptions: prev.exceptions.filter((item) => item.exceptionId !== exception.exceptionId),
    }));
    setSelected({ data: undefined, type: undefined, loading: false });
  };

  const handleDaySelect = (value) => {
    if (data.workingdays.some((item) => item.id === value.id)) {
      if (data.workingdays.length <= 1) return;

      const newvalue = data.workingdays.filter((item) => item.id !== value.id);
      setData((prev) => ({ ...prev, workingdays: newvalue }));
    } else {
      setData((prev) => ({ ...prev, workingdays: [...data.workingdays, value] }));
    }
  };

  return (
    <section  >
      {!readOnly && (
        <div className={classes.editScheduleHeading}>
          <div className={classes.editScheduletext}>
            {defaultValue ? "Edit Schedule" : "Create Schedule"}
          </div>
          <div className={classes.UpdateActionsBTns}>
            {data && (
              <CustomButton
                style={{
                  textTransform: "initial",
                  marginBottom: 0,
                  color: "#7E7E7E",
                  marginRight: 10,
                }}
                query={loading === "cancel" ? "progress" : undefined}
                variant="text"
                onClick={async () => {
                  setLoading("cancel");
                  await onCancel?.(data);
                  setLoading(undefined);
                }}>
                Cancel
              </CustomButton>
            )}
            <CustomTooltip
              placement="top"
              helptext={!canCreate && defaultValue?.isSystemCalender ? "You've reached your Work Schedule creation quota limit, please contact support to increase your quota" : ''}>
              <CustomButton
                query={loading === "submit" ? "progress" : undefined}
                btnType="success"
                variant="contained"
                style={{
                  textTransform: "initial",
                  padding: 6,
                  marginBottom: 0,
                  ...(loading && {
                    background: "none",
                    boxShadow: "none",
                  }),
                }}
                onClick={async () => {
                  if (!canCreate && defaultValue?.isSystemCalender) return;
                  if (!data.title) return showSnackBar("Please enter title", "error");
                  setLoading("submit");
                  await onSubmit?.(data);
                  setLoading(undefined);
                }}>
                {defaultValue ? (defaultValue.isSystemCalender ? "Save as Copy" : "Update") : "Save"}
              </CustomButton>
            </CustomTooltip>
          </div>
        </div>
      )}
      <div
        style={{
          // paddingTop: 10,
          borderTop: "1px solid rgb(221, 221, 221)",
        }}>
        <div style={{ marginTop: "15px" }}>
          {!readOnly && (
            <TextField
              autoFocus
              defaultValue={data.title}
              label="Schedule Title"
              variant="outlined"
              fullWidth
              inputProps={{ maxLength: 250 }}
              onChange={({ target: { value } }) => setData((prev) => ({ ...prev, title: value }))}
            />
          )}
        </div>

        <div className={classes.CalendarSettings}>
          <div style={style}>
            <InputLabel className={classes.workingDaySection}>
              Working Days{" "}
              <span style={{ color: theme.palette.text.darkGray }}>
                {data.workingdays?.length} Days
              </span>
            </InputLabel>
            <ul className={classes.daysSelect}>
              {DAYSOPTIONS.map((day, i) => (
                <li
                  key={i}
                  className={clsx({
                    [classes.pointer]: !readOnly,
                    [classes.daySlot]: true,
                    [classes.selectedDay]: ((data.workingdays?.some((value) => value.id === day.value.id)))
                  })}
                  onClick={() => !readOnly ? handleDaySelect(day.value) : {}}>
                  {day.label}
                </li>
              ))}
            </ul>
            <div className={classes.WorkingHoursDropdownPerent}>
              <div className={classes.WorkingHoursDropdown}>
                <WorkingHoursDropdown
                  backgroundColor={{ backgroundColor: readOnly && "#F1F1F1" }}
                  icon={!readOnly && <ClearIcon htmlColor={theme.palette.secondary.medDark} />}
                  value={data.workingHours}
                  onChange={(value) =>
                    setData((prev) => ({
                      ...prev,
                      workingHours: value,
                      dailyCapacity: value.reduce((acc, item) => (
                        acc + differenceInHours(addDays(parseDate(item.timeTo), item.timeTo === "00:00:00" ? 1 : 0), parseDate(item.timeFrom))
                      ), 0),
                    }))
                  }
                  readOnly={readOnly}
                />
              </div>
              <div className={classes.DefaultTextFieldWorkingHours}>
                <DefaultTextField
                  disabled={!isEmpty(data.workingHours) || readOnly}
                  label="Daily Capacity"
                  type="number"
                  error={false}
                  value={
                    !!data.workingHours?.length
                      ? data.workingHours.reduce((acc, item) => {
                        return (
                          acc + differenceInHours(addDays(parseDate(item.timeTo), item.timeTo === "00:00:00" ? 1 : 0), parseDate(item.timeFrom))
                        );
                      }, 0)
                      : data.dailyCapacity
                  }
                  placeholder="Enter title (Optional)"
                  inputProps={{ maxLength: 250 }}
                  endAdornment={<InputAdornment position="end" ><p style={{ fontSize: "12px", color: "#202020" }}>hours</p></InputAdornment>}
                  onChange={({ target: { value } }) =>
                    setData((prev) => ({
                      ...prev,
                      workingHours: [],
                      dailyCapacity: Number(clamp(value, 0, 24)),
                    }))
                  }
                />
              </div>
            </div>
          </div>
        </div>
        {(!defaultValue?.isSystemCalender) &&
          <section className={classes.Exceptions}>
            <Typography variant="h4" classes={{ h4: classes.calendarHeading }}>
              Exceptions
            </Typography>
            {/* {readOnly ?
              <>
                {data.exceptions && data.exceptions?.length ?
                  <Typography variant="h4" classes={{ h4: classes.calendarHeading }}>
                    Exceptions
                  </Typography>
                  : null
                }
              </>
              : <Typography variant="h4" classes={{ h4: classes.calendarHeading }}>
                Exceptions
              </Typography>} */}


            {readOnly ?
              <>
                {data.exceptions && data.exceptions?.length ?
                  null
                  : <Typography variant="p" classes={{ p: classes.calendarDescription }}>
                    No exceptions have been added for this Calendar
                  </Typography>
                }
              </>
              : <Typography variant="p" classes={{ p: classes.calendarDescription }}>
                Here you can create, edit, and delete exceptions in your working calendar: days-off,
                non-standard working hours, etc.
              </Typography>}

            {!readOnly && (
              <Typography variant="p" classes={{ p: classes.calendarDescription }}>
                Here you can create, edit, and delete exceptions in your working calendar: days-off,
                non-standard working hours, etc.
              </Typography>
            )}

            {(selected.type === "new" || selected.type === "edit") && (
              <ExceptionsForm
                value={selected.data}
                onCancel={() => setSelected({ data: undefined, type: undefined, loading: false })}
                onSubmit={onSubmitException}
              />
            )}
            <div className={classes.AddExceptionBtn}>
              <div>
                {!readOnly &&
                  selected.type !== "new" &&
                  selected.type !== "edit" &&
                  data?.exceptions?.length < 5 && (
                    <CustomButton
                      variant="outlined"
                      style={{ textTransform: "initial" }}
                      onClick={() => setSelected({ data: undefined, type: "new", loading: false })}>
                      Add Exception
                    </CustomButton>
                  )}
              </div>
              {!readOnly && selected.type !== "new" && selected.type !== "edit" && (
                <div className={classes.searchInputCnt}>
                  <SearchIcon className={classes.searchIcon} />
                  <SearchInput
                    className={`${classes.searchInput} HtmlInput`}
                    placeholder="Search"
                    onChange={(value) => setSearch(value)}
                  />
                </div>
              )}
            </div>
            <div className={classes.ExceptionsCardCompoPerrent}>
              {data?.exceptions
                ?.filter((item) => {
                  if (!search) return item;
                  return item.title?.toLowerCase().includes(search?.toLowerCase());
                })
                ?.map((item, index) => (
                  <ExceptionsCard
                    onEdit={(exception) =>
                      setSelected({ data: exception, type: "edit", loading: false })
                    }
                    onDelete={(exception) =>
                      setSelected({ data: exception, loading: false, type: "delete" })
                    }
                    readOnly={readOnly}
                    key={index}
                    item={item}
                  />
                ))}
              <DeleteExceptionCardModel
                open={selected.type === "delete"}
                loading={selected.loading}
                onClose={() => setSelected({ data: undefined, type: undefined, loading: false })}
                onSubmit={() => onDeleteException(selected.data)}
              />
            </div>
          </section>}
      </div>
    </section>
  );
};

export default compose(withSnackbar, withStyles(styles, { withTheme: true }))(CalendarForm);
