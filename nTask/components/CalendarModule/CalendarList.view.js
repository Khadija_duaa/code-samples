import React from "react";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { withStyles } from "@material-ui/core/styles";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";
import { styles } from "./Calendar.style";
import Typography from "@material-ui/core/Typography";
import { DAYSOPTIONS, parseDate } from "./constant";
import { Chip, IconButton } from "@material-ui/core";
import CustomTooltip from "../Tooltip/Tooltip";
import { format } from "date-fns";
import CopyWorkspaceCaledar from "../Icons/CopyWorkspaceCaledar";
import EditWorkspaceCaledar from "../Icons/EditWorkspaceCaledar";
import DeleteWorkspaceCaledar from "../Icons/DeleteWorkspaceCaledar";

const CalendarList = (props) => {
  const {
    active,
    classes,
    onCopy,
    onDelete,
    onEdit,
    onDefault,
    canEdit = true,
    loading = false,
    canCreate = false,
    data = undefined,
  } = props;

  if (!data)
    return (
      <div
        style={{
          height: window.innerHeight - 325,
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <div className="loader" />
      </div>
    );

  return (
    <div style={{ marginTop: "1rem" }}>
      {data.map((calender, index) => (
        <div
          key={index}
          style={{ display: "flex", padding: "1rem 0", borderBottom: "1px solid #f1f1f1" }}
          className="group">
          <div style={{ flex: 1 }}>
            <div className={classes.calenderNameText}>
              {calender.title.replace("nTask Calendar", "nTask Schedule")}
            </div>
            <div style={{ display: "flex", alignItems: "center" }}>
              <div style={{ display: "flex", alignItems: "center", fontSize: "13px" }}>
                {DAYSOPTIONS.map((item, index) => (
                  <Typography
                    color={
                      calender.workingdays.some((day) => day.id === item.value.id)
                        ? "secondary"
                        : undefined
                    }
                    key={index}
                    style={{ marginRight: ".5rem", fontSize: "13px" }}>
                    {item.label}
                  </Typography>
                ))}
              </div>
              {!!calender.workingHours.length ? (
                <>
                  <div className={classes.horizontalLine}>&nbsp;</div>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    {calender.workingHours.slice(0, 2).map((slot) => (
                      <div className={classes.chip}>
                        {format(parseDate(slot.timeFrom), "hh:mm a")}-
                        {format(parseDate(slot.timeTo), "hh:mm a")}
                      </div>
                    ))}
                    {calender.workingHours.length > 2 && <div className={classes.chip}>...</div>}
                  </div>
                </>
              ) : (
                !!calender.dailyCapacity && (
                  <>
                    <div className={classes.horizontalLine}>&nbsp;</div>
                    <div className={classes.chip}>{calender.dailyCapacity} hours</div>
                  </>
                )
              )}

              {!!calender?.exceptions?.length && (
                <>
                  <div className={classes.horizontalLine}>&nbsp;</div>
                  <div style={{ display: "flex", alignItems: "center" }} className={classes.chip}>
                    {calender?.exceptions?.length} exceptions
                  </div>
                </>
              )}

              {!!(calender?.projectCount || calender?.taskCount) && (
                <>
                  <div className={classes.horizontalLine}>&nbsp;</div>
                  <div style={{ display: "flex", alignItems: "center" }} className={classes.chip}>
                    {!!calender?.projectCount && <> {calender?.projectCount} Projects</>}
                    {!!(calender?.projectCount && calender?.taskCount) && (
                      <div style={{ margin: "0 2px" }}>&#183;</div>
                    )}
                    {!!calender?.taskCount && <> {calender?.taskCount} tasks</>}
                  </div>
                </>
              )}
            </div>
          </div>

          <div className={classes.actionBtnperrnt}>
            <div className="group-hover:show" style={{ display: "flex", alignItems: "center" }}>
              {canEdit && (
                <CustomTooltip
                  placement="top"
                  helptext={canCreate ? 'Copy' : "You've reached your Work Schedule creation quota limit, please contact support to increase your quota"}>
                  <IconButton
                    onClick={() => canCreate ? onCopy(calender) : {}}
                    query={loading === "copy" && "progress"}
                    className={classes.actionBtn}>
                    <CopyWorkspaceCaledar />
                  </IconButton>
                </CustomTooltip>
              )}
              {canEdit && (
                <CustomTooltip
                  placement="top"
                  helptext={'Edit'}>
                  <IconButton className={classes.actionBtn} onClick={() => onEdit(calender)}>
                    <EditWorkspaceCaledar />
                  </IconButton>
                </CustomTooltip>
              )}
              {canEdit && !calender.isSystemCalender && active !== calender.calenderId && (
                <CustomTooltip
                  placement="top"
                  helptext={'Delete'}>
                  <IconButton className={classes.actionBtn} onClick={() => onDelete(calender)}>
                    <DeleteWorkspaceCaledar />
                  </IconButton>
                </CustomTooltip>
              )}

              {/* {canEdit && <div className={classes.horizontalLine}>&nbsp;</div>} */}
              {!canEdit && active !== calender.calenderId && (
                <>
                  <Chip
                    classes={{ root: classes.rootChip }}
                    onClick={() => onDefault(calender)}
                    className={classes.defaultChip}
                    color="secondary"
                    size="small"
                    variant="outlined"
                    label="Set Default"
                  />
                </>
              )}
            </div>
            {active === calender.calenderId && (
              <Chip
                classes={{ root: classes.rootChip }}
                className={classes.defaultChip}
                color="secondary"
                size="small"
                label="Default"
              />
            )}
          </div>
        </div>
      ))}
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(CalendarList);
