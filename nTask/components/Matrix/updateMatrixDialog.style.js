const updateMatrixDialogStyles = theme => ({
    dialogContentCnt: {
        padding: 0,
        height: 440,
        padding: '36px 36px 36px 70px',
    },
    headingLightText: {
        color: theme.palette.text.light
    },
    dialogPaperCmp: {
        maxWidth: 1140
    }
  });
  
  export default updateMatrixDialogStyles;
  