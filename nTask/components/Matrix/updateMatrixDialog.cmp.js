import React, { useContext, useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import updateMatrixDialogStyles from "./updateMatrixDialog.style";
import CustomDialog from "../Dialog/CustomDialog";
import useMatrixData from "../../Views/Risk/RiskMatrix/useMatrixData.hook";
import MatrixGrid from "../../Views/CustomFieldSettings/Matrix/matrixGrid.cmp";
import moment from 'moment'


function UpdateMatrixDialog(props) {
  const { classes, theme, customFields, open, closeAction, feature, saveOption, selectedMatrix, assessMatrix, selectedValues = [], profileState } = props;
  const {matrix} = useMatrixData(customFields, profileState, feature);

  const handleCellClick = (cell, obj) => {
    saveOption([...selectedValues, {...cell, createdDate: new Date()}], obj)
  }
  const today = moment().format('MMMM DD, YYYY');
  const selectedMatrixObj = assessMatrix ? matrix.find(m => m.settings.assessment && m.settings.assessment.indexOf(profileState.loggedInTeam) >= 0) : matrix.find(m => m.fieldId === selectedMatrix);
  return (
    <CustomDialog
      title={<div><span className={classes.headingLightText}>Add Assessment:</span> {today}</div>}
      subTitle='Select the current net risk value from the given risk matrix.'
      dialogProps={{
        open: open,
        fullWidth: false,
        onClose: closeAction,
        PaperProps: { 
          className: classes.dialogPaperCmp,
        },
        disableBackdropClick: true,
        disableEscapeKeyDown: true,
      }}
      >
      <div className={classes.dialogContentCnt}>
        {selectedMatrixObj && 
        <MatrixGrid legend={true} data={selectedMatrixObj} hideDisabled={true} colorSelect={false} cellClick={handleCellClick} selectionDialog={true}/>}

      </div>
    </CustomDialog>
  );
}
UpdateMatrixDialog.defaultProps = {
  customFields:{data: []}
}
const mapStateToProps = state => {
  return {
    customFields: state.customFields,
    profileState : state.profile.data

  };
};
export default compose(
  connect(mapStateToProps),
  withStyles(updateMatrixDialogStyles, { withTheme: true })
)(UpdateMatrixDialog);
