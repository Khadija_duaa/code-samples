import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import Grid from "@material-ui/core/Grid";

import CustomIconButton from "../Buttons/IconButton";
import CustomButton from "../Buttons/CustomButton";

import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import tempBG from "../../assets/images/temporary.png";

import { GetAllTemplatesByGroup } from "../../redux/actions/boards";
import Truncate from "react-truncate";

import { FormattedMessage } from "react-intl";
import isEmpty from "lodash/isEmpty";
import Loader from "../Loader/Loader";

type BoardsTemplatesProps = {
  classes: Object,
  theme: Object,
  boardPer: Object,
  GetAllTemplatesByGroup: Function,
  handleTemplatePreview: Function,
  handleUseTemplate: Function,
  GetAllTemplatesByGroup: Function,
};

function BoardsTemplates(props: BoardsTemplatesProps) {
  const {
    classes,
    theme,
    handleTemplatePreview,
    handleUseTemplate,
    workspaceTemplates,
    templates,
    setTemplates,
    boardsTemplates,
    setBoardsTemplates,
    boardPer,
  } = props;

  const [currentView, setView] = useState("All");
  const [currentViewOptions, setCurrentViewOptions] = useState([]);
  const handleChange = (event, nextView) => {
    if (nextView !== null) {
      setView(nextView);
      setCurrentViewOptions([]);
      filterTemplates(nextView);
    }
  };
  const handleChangeOption = (event, nextView) => {
    if (nextView.length > 0) {
      setCurrentViewOptions(nextView);
      setView("");
      filterAddingTemplates(nextView);
    } else {
      filterTemplates("All");
      setView("All");
      setCurrentViewOptions([]);
    }
  };
  // const [templates, setTemplates] = useState(null);
  // const [boardsTemplates, setBoardsTemplates] = useState([]);

  const filterTemplates = type => {
    if (type == "All" || type == null) {
      setTemplates(
        boardsTemplates
      ); /** if filter type is all then show all templates coming from props */
    } else {
      /** filter type specific templates */
      let filteredTemplates = boardsTemplates.filter(t => t.groupName === type);
      setTemplates(filteredTemplates);
    }
  };
  const filterAddingTemplates = type => {
    let arr = [];
    type.map(ele => {
      boardsTemplates.map(t => {
        if (t.groupName == ele) {
          arr.push(t);
        }
      });
    });
    setTemplates(arr);
  };

  const renderTemplate = () => {
    return templates.map((t, index) => {
      return (
        <Grid
          item
          classes={{ item: classes.templateContainer }}
          xs={6}
          sm={6}
          md={4}
          lg={3}
          xl={2}
          key={`temp-${index}`}>
          <div className={classes.templateCnt}>
            <div
              style={{
                backgroundImage: t.imagePath ? `url(${t.imagePath})` : `url(${tempBG})`,
                backgroundSize: t.type == "Saved Templates" ? "contain" : "cover",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "50% center",
                width: "100%",
                minHeight: 130,
                // backgroundSize: '256px 130px',
                borderRadius: "4px 4px",
              }}></div>

            <div className={classes.templateHeader}>
              <span className={classes.btnTitle} title={t.name}>
                {" "}
                {t.name}{" "}
              </span>
            </div>
            <div className={classes.templateHeader} title={t.description}>
              <Truncate
                lines={3}
                trimWhitespace={true}
                width={225}
                ellipsis={<span>...</span>}
                className={classes.description}>
                {t.description}
              </Truncate>
              {/* <span className={classes.description}></span> */}
            </div>
            <div className={classes.templateBtnCnt}>
              <CustomIconButton
                onClick={e => {
                  handleTemplatePreview(e, t);
                }}
                btnType="filledWhite"
                style={{
                  padding: "10px 18px",
                  height: 35,
                  background: "#EAEAEA",
                  border: "none",
                  color: "#202020",
                  fontWeight: 400,
                  fontSize: "13px",
                  marginRight: 10,
                }}
                isBoardScreenAppeared={true}
                >
                {/* <span className={classes.templateBtnTitle}> */}
                <FormattedMessage id="board.template.preview.label" defaultMessage="Preview" />
                {/* </span> */}
              </CustomIconButton>
              <CustomButton
                btnType="success"
                variant="contained"
                style={{
                  height: 35,
                  padding: '8px 15px',
                  fontWeight: 400,
                  fontFamily: theme.typography.fontFamilyLato,
                  fontSize: "13px",
                }}
                onClick={event => {
                  handleUseTemplate({ isTemplate: true, template: t });
                }}
                disabled={!boardPer.creatBoard.cando}
                isBoardScreenAppeared= {true}
                >
                <FormattedMessage
                  id="board.template.use-this-template.label"
                  defaultMessage="Use this Template"
                />
              </CustomButton>
            </div>
          </div>
        </Grid>
      );
    });
  };

  return (
    <>
      <div className={classes.buttonCnt}>
        <div className={classes.toggleContainer}>
          <ToggleButtonGroup
            value={currentView}
            exclusive
            onChange={handleChange}
            classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}>
            <ToggleButton
              value="All"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage id="board.template.categories.all" defaultMessage="All" />
            </ToggleButton>
            <ToggleButton
              value="Saved Templates"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="board.template.categories.saved-templates"
                defaultMessage="Saved Templates"
              />
            </ToggleButton>
            <div className={classes.horizontalDivider}></div>
          </ToggleButtonGroup>

          <ToggleButtonGroup
            value={currentViewOptions}
            exclusive={false}
            onChange={handleChangeOption}
            classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}>
            <ToggleButton
              value="Business"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="board.template.categories.bussiness"
                defaultMessage="Business"
              />
            </ToggleButton>
            <ToggleButton
              value="Design"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage id="board.template.categories.design" defaultMessage="Design" />
            </ToggleButton>
            <ToggleButton
              value="Education"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="board.template.categories.education"
                defaultMessage="Education"
              />
            </ToggleButton>
            <ToggleButton
              value="Marketing"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="board.template.categories.marketing"
                defaultMessage="Marketing"
              />
            </ToggleButton>
            <ToggleButton
              value="HR"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="board.template.categories.hr-operations"
                defaultMessage="HR & Operations"
              />
            </ToggleButton>
            <ToggleButton
              value="Development"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="board.template.categories.Development"
                defaultMessage="Development"
              />
            </ToggleButton>
            <ToggleButton
              value="Health"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage id="board.template.categories.Health" defaultMessage="Health" />
            </ToggleButton>
            <ToggleButton
              value="Project Management"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="board.template.categories.project-management"
                defaultMessage="Project Management"
              />
            </ToggleButton>
            {/* <ToggleButton
              value="Remote Work"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="board.template.categories.remote-work"
                defaultMessage="Remote Work"
              />
            </ToggleButton>
            <ToggleButton
              value="Sales"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage id="board.template.categories.sales" defaultMessage="Sales" />
            </ToggleButton>
            <ToggleButton
              value="Others"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage id="board.template.categories.other" defaultMessage="Others" />
            </ToggleButton> */}
          </ToggleButtonGroup>
        </div>
      </div>
      <Grid container spacing={2}>
        {templates && renderTemplate()}
        {!templates && (
          <Loader />
        )}
      </Grid>
    </>
  );
}

BoardsTemplates.defaultProps = {
  classes: {},
  theme: {},
  boardPer: {},
  boardsTemplates: [],
  GetAllTemplatesByGroup: () => {},

  handleTemplatePreview: () => {},
};
const mapStateToProps = state => {
  return {
    workspaceTemplates: state.workspaceTemplates.data.allWSTemplates,
    boardPer: state.workspacePermissions.data.board,
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {
    GetAllTemplatesByGroup,
  })
)(BoardsTemplates);
