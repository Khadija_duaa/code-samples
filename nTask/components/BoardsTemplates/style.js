const boardsTemplatesStyle = (theme) => ({
  buttonCnt: {
    margin: "6px 0 15px 4px",
    display: "flex",
    alignItems: "center",
  },
  btnTitle: {
    fontSize: "16px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#202020",

    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    maxWidth: 300,
    
  },
  templateBtnTitle: {
    fontSize: "16px !important",
    fontWeight: 500,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#202020",
  },
  btn: {
    padding: 10,
    height: 38,
  },
  root: {
    fontFamily: "lato, sans-serif !important",
  },
  toggleBtnGroup: {
    display: "flex",
    flexWrap: "nowrap",
    borderRadius: 4,
    // background: theme.palette.common.white,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.linkBlue,
      backgroundColor: "white",
      border: `1px solid ${theme.palette.text.linkBlue} !important`,
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.common.white,
      },
    },
  },
  groupedHorizontal:{
    borderLeft: `1px solid ${theme.palette.border.lightBorder} !important`,
    marginLeft: "9px !important"
  }, 
  toggleButton: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    height: "auto",
    padding: "4px 20px 5px",
    borderRadius: "4px !important",
    fontSize: "12px !important",
    fontWeight: 500,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#202020",
    backgroundColor: "#EAEAEA",
    textTransform: "capitalize",
    marginLeft: "8px",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&[value = 'All']": {
      marginLeft: "0px",
    },
  },
  toggleButtonSelected: {},
  templateCnt: {
    background: `${theme.palette.common.white} 0% 0% no-repeat padding-box`,
    boxShadow: `0px 2px 0px ${theme.palette.shadow.greyShadow}`,
    borderRadius: 4,
    padding: 10,
    opacity: 1,
    margin: "0 0 8px",
    border: `1px solid transparent`,
    // width: 275,
    minHeight: 285,
    position: "relative",
    // height:"100%",
    cursor: "pointer",
    "&:hover $templateBtnCnt": {
      //   border: `1px solid ${theme.palette.border.greyDarkBorder}`,
      // display: "flex",
    },
  },
  templateContainer: {
    // display: "flex",
    // flexDirection: "row",
    // margin: "20px 15px 20px 10px",
  },
  templateHeader: {
    padding: "9px 0px 0px 0px",
    textAlign: "left",
    fontWeight: theme.typography.fontWeightLarge,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-start",
    fontSize: "14px !important",
    lineHeight: "18px",
    fontFamily: "Lato, sans-serif",
    letterSpacing: 0,
    color: `${theme.palette.text.primary}`,
  },
  description: {
    color: "#646464",
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    wordBreak:"break-word"
    // paddingBottom: 10
  },
  templateBtnCnt: {
    marginTop: 10,
    display: "flex",
    justifyContent: "space-between",
    position: "absolute",
    bottom: 10
  },
  horizontalDivider:{
    borderLeft: "1px solid #EAEAEA",
    marginLeft : 10
  },
  toggleContainer:{
    display: "flex",
    flexDirection: "row"
  },

  "@media (max-width: 1024px)": {
    
      },
});

export default boardsTemplatesStyle;
