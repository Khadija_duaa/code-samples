import React, { Fragment, useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
import Updates from "./Updates/Updates";
import ReplyLater from "./ReplyLater/ReplyLater";
import { compose } from "redux";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { FormattedMessage } from "react-intl";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";

function ChatUpdates(props) {
  const [tabValue, setTabValue] = useState("updates");
  const [showReceipt, setShowReceipt] = useState(null);

  const { classes, theme, chatConfig, chatPermission, onMount, onUnmount, intl } = props;

  const showReceiptHandler = () => {
    setShowReceipt(!showReceipt);
  };
  const handleTabChange = (event, value) => {
    /** function call when user select subscription, is it billable or non billable (button) from detail tab view */
    if (value) {
      setTabValue(value);
    }
  };
  useEffect(() => {
    onMount();
    return () => {
      onUnmount();
    };
  }, []);
  return (
    <div className={classes.detailsDiv}>
      <div className={classes.dateReceiptCnt}>
        <div style={{ display: "flex", flex: 1 }}>
          <ToggleButtonGroup
            size="small"
            exclusive
            onChange={handleTabChange}
            value={tabValue}
            classes={{ root: classes.toggleBtnGroup }}>
            <ToggleButton
              key={2}
              value="updates"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}
              disabled={false}>
              <div className={classes.toggleButtonMainDiv}>
                <span className={classes.planTypeLbl}>
                  <FormattedMessage
                    id="common.comment.all-messages"
                    defaultMessage="All Messages"
                  />
                </span>
              </div>
            </ToggleButton>
            <ToggleButton
              key={1}
              value="replyLater"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}
              disabled={false}>
              <div className={classes.toggleButtonMainDiv}>
                <span className={classes.planTypeLbl}>
                  <FormattedMessage
                    id="project.dev.reply-later.label"
                    defaultMessage="Reply Later"
                  />{" "}
                </span>
              </div>
            </ToggleButton>
          </ToggleButtonGroup>
        </div>
        {chatPermission.showReceipt && (
          <Typography
            variant="h6"
            classes={{ h6: classes.receiptCnt }}
            onClick={showReceiptHandler}>
            {showReceipt ? (
              <FormattedMessage
                id="project.dev.chat-item.receipt.hide"
                defaultMessage="Hide Receipt"
              />
            ) : (
              <FormattedMessage
                id="project.dev.chat-item.receipt.show"
                defaultMessage="Show Receipt"
              />
            )}
          </Typography>
        )}
      </div>
      {tabValue == "updates" && (
        <div className={classes.commentsTabCnt}>
          <Updates
            chatConfig={chatConfig}
            chatPermission={chatPermission}
            intl={intl}
            showReceipt={showReceipt}
            showChildReceipt={false}
          />
        </div>
      )}
      {tabValue == "replyLater" && (
        <div className={classes.commentsTabCnt}>
          <ReplyLater
            chatConfig={chatConfig}
            chatPermission={chatPermission}
            intl={intl}
            showReceipt={showReceipt}
            showChildReceipt={false}
          />
        </div>
      )}
    </div>
  );
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  connect(mapStateToProps),
  withStyles(Styles, { withTheme: true })
)(ChatUpdates);
