import React, { Fragment, useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Styles from "./Styles";
import Updates from "./Updates/Updates";
import Documents from "../Documents/Documents";
import ReplyLater from "./ReplyLater/ReplyLater";
import ChatUpdates from "./ChatUpdates";
import { compose } from "redux";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { FormattedMessage } from "react-intl";
import { projectDetails } from "../../redux/actions/projects";

function ChatBuilder(props) {
  let index = props.selectedTab ? props.selectedTab : 0;
  const [tabIndex, setTabIndex] = useState(index);

  const handleChange = (event, tabIndex) => {
    setTabIndex(tabIndex);
    if(tabIndex == 0){
      props.projectDetails({chatView: true, docsView: false, activityLogView: false})
    }
    if(tabIndex == 2){
      props.projectDetails({chatView: false, docsView: true, activityLogView: false})
    }
  };

  const {
    classes,
    theme,
    chatConfig,
    chatPermission,
    docConfig,
    docPermission,
    onMount,
    onUnmount,
    intl,
  } = props;
  useEffect(() => {
    onMount();
    return () => {
      onUnmount();
    };
  }, []);
  useEffect(() => {
    setTabIndex(props.selectedTab);
  }, [props.selectedTab]);
  return (
    <div className={classes.detailsDiv}>
      <Tabs
        value={tabIndex}
        // action={obj => {
        //   setTimeout(() => {
        //     obj.updateIndicator();
        //   }, 500);
        // }}
        onChange={handleChange}
        fullWidth
        classes={{
          root: classes.TabsRoot,
          indicator: classes.tabIndicator,
        }}>
        <Tab
          disableRipple={true}
          classes={{
            root: classes.tab,
            wrapper: classes.tabLabelCnt,
            selected: classes.tabSelected,
          }}
          label={
            <Typography variant="h6" className={classes.tabLabel}>
              <FormattedMessage id="common.comment.update-label" defaultMessage="Updates" />
            </Typography>
          }
        />
        <Tab
          disableRipple={true}
          classes={{
            root: classes.tab,
            wrapper: classes.tabLabelCnt,
            selected: classes.tabSelected,
          }}
          label={
            <Typography variant="h6" className={classes.tabLabel}>
              {/* <FormattedMessage id="common.comment.label" defaultMessage="Comments" />{" "} */}
              <FormattedMessage id="project.dev.reply-later.label" defaultMessage="Reply Later" />
            </Typography>
          }
        />
        <Tab
          disableRipple={true}
          classes={{
            root: classes.tab,
            wrapper: classes.tabLabelCnt,
            selected: classes.tabSelected,
          }}
          label={
            <Typography variant="h6" className={classes.tabLabel}>
              <FormattedMessage id="project.dev.documents.label" defaultMessage="Documents" />{" "}
            </Typography>
          }
        />
      </Tabs>
      {tabIndex === 0 && (
        <div className={classes.commentsTabCnt}>
          <Updates
            chatConfig={chatConfig}
            chatPermission={chatPermission}
            intl={intl}
            showReceipt={false}
            showChildReceipt={true}
          />
        </div>
      )}
      {tabIndex === 1 && (
        <div className={classes.commentsTabCnt}>
          {/* <ChatUpdates
            onMount={onMount}
            onUnmount={onUnmount}
            chatConfig={chatConfig}
            chatPermission={chatPermission}
            selectedTab={0}
            intl={intl}
            /> */}
          <ReplyLater
            chatConfig={chatConfig}
            chatPermission={chatPermission}
            intl={intl}
            showReceipt={false}
            showChildReceipt={true}
          />
        </div>
      )}
      {tabIndex === 2 && (
        <div className={classes.commentsTabCnt}>
          <Documents docConfig={docConfig} docPermission={docPermission} intl={intl} />
        </div>
      )}
    </div>
  );
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  connect(mapStateToProps, { projectDetails }),
  withStyles(Styles, { withTheme: true })
)(ChatBuilder);
