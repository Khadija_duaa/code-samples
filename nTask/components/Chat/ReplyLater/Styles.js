const chatUpdatesStyles = (theme) => ({
    noConversation: {
        display: 'flex',
        height: '100%',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    commentsCnt: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
    clearReply: {
        display: 'flex',
        justifyContent: 'flex-end',
        paddingRight: 12,
        paddingBottom: 12,
    },
    fontSize: {
        fontSize: "13px !important"
    },
    searchIcon: {
        fontSize: "14px !important",
    }
});

export default chatUpdatesStyles;