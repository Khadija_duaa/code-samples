const chatUpdatesStyles = (theme) => ({
    fontSize: {
        fontSize: "13px !important"
    },
    noConversation: {
        display: 'flex',
        height: '100%',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    commentsCnt: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
    searchIcon: {
        fontSize: "14px !important",
    }
});

export default chatUpdatesStyles;