import React, { useState, useEffect, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import Typography from "@material-ui/core/Typography";
import { withSnackbar } from "notistack";
import moment from "moment";
import isEmpty from "lodash/isEmpty";
import { v4 as uuidv4 } from "uuid";
import { FormattedMessage, injectIntl } from "react-intl";
import helper from "../../../helper";
import 'core-js/features/array/flat';
import {
  GetUpdatesComments,
  SetReplyLaterFlag,
  ClearReplyLaterFlag,
  ConvertToTask,
  UpdateUesrComment,
  DeleteUesrComment,
  DeleteChatAttachment,
  saveUserComment,
  ReplyUserCommentFromUpdates,
  StoreChatUpdate,
  StartTyping,
  StopTyping,
  clearStore,
} from "../../../redux/actions/chat";
import NoConversationIcon from "../../Icons/NoConversationIcon";
import ChatTextEditor from "../../TextEditor/CustomTextEditor/ChatTextEditor/ChatTextEditor";
import ChatItems from "../ChatItems/ChatItems";
import Styles from "./Styles";
import Loader from "../../Loader/Loader";

const htmlToText = require("html-to-text");

function Updates(props) {
  const [searchText, setSearchText] = useState("");
  const [formattedAllItems, setFormattedAllItems] = useState({});
  const [actuallAllItems, setActuallAllItems] = useState([]);
  const [loading, setLoading] = useState(true);

  const { intl } = props;
  useEffect(() => {
    // nChatSignalConnectionOpen(props.chatConfig.contextId);
    props.GetUpdatesComments(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      response => {
        setLoading(false);
      },
      error => {
        if (error) {
          showSnackBar("Server throws error", "error");
        }
      }
    );
    return () => {
      setFormattedAllItems({});
      setActuallAllItems({});
      props.clearStore(props.chatConfig.contextId);
    };
  }, [0]);
  useEffect(() => {
    setActuallAllItems(props.chatItems);
    setFormattedAllItems(formatedAllItems(props.chatItems));
  }, [props.chatItems]);

  // It is called to add item in the given collection
  const addRootComment = (allChatItems, chatItem) => {
    allChatItems[chatItem.comment.commentId] = formatCommentItem(chatItem);
    allChatItems[chatItem.comment.commentId].children = {};
  };

  // To format All Chat Items for UI
  const formatedAllItems = chatComments => {
    const allChatItems = {};
    chatComments.forEach(x => {
      if (validToAdd(x)) {
        if (x.comment.parentId == null || x.comment.parentId == "") {
          addRootComment(allChatItems, x);
        } else {
          const pComment = getParentComment(chatComments, x.comment.parentId);
          if (allChatItems[x.comment.parentId] == undefined) {
            addRootComment(allChatItems, pComment);
          }
          if (allChatItems[x.comment.parentId].children == undefined) {
            allChatItems[x.comment.parentId].children = {};
          }
          allChatItems[x.comment.parentId].children[x.comment.commentId] = formatCommentItem(x);
        }
      }
    });
    return allChatItems;
  };
  // It is called to get Parent Comment for the comment
  const getParentComment = (allComments, id) => {
    return allComments.find(item => item.comment.commentId == id);
  };

  // It is called to check either this chat item is valid for render then add in render list
  const validToAdd = x => {
    if (
      x.userId === props.profileState.data.userId ||
      x.comment.sharedWith.length == 0 ||
      x.comment.sharedWith[0] == "all" ||
      x.comment.sharedWith[0] == props.profileState.data.userId
    )
      return true;
    return false;
  };
  // It is called All chatItems getting for renderer
  const getAllChatItems = () => {
    const allChatItems = {};
    Object.values(formattedAllItems).map(item => {
      const threadDateTime = getLatestItemDateTime(item);
      const threadDate = helper.RETURN_CHATDATEITEM_FORMAT(threadDateTime);
      const threadTime = moment(threadDateTime).format("HH:mm:ss");
      item.threadTime = threadTime;
      if (allChatItems[threadDate] == undefined) {
        allChatItems[threadDate] = [];
      }
      allChatItems[threadDate].push(item);
    });
    let allSortChatItems = {};
    for (const [key, value] of Object.entries(allChatItems)) {
      allSortChatItems[key] = {};
      // It is called to sort chatItem children
      const sortedChatItems = value.sort(compareByTime);
      // It is called to add date item in start of the items
      sortedChatItems.unshift(formatDateItem(key));

      allSortChatItems[key] = sortedChatItems;
    }
    allSortChatItems = sortItemsByDate(allSortChatItems);

    return isEmpty(allSortChatItems)
      ? []
      : Object.values(allSortChatItems)
          .map(item => Object.values(item))
          .flat();
  };
  // It is called to get latest DateTime in mainItem and its children
  const getLatestItemDateTime = comment => {
    let { updatedDateTime } = comment;
    Object.values(comment.children).map(item => {
      new Date(item.updatedDateTime) > new Date(updatedDateTime)
        ? (updatedDateTime = item.updatedDateTime)
        : (updatedDateTime = updatedDateTime);
    });
    return updatedDateTime;
  };
  // It is called to sort all main chat items
  const sortItemsByDate = items => {
    const sortedKeys = Object.keys(items).sort((itemA, itemB) => new Date(itemA) - new Date(itemB));
    const sortedDates = {};
    sortedKeys.forEach(element => {
      sortedDates[element] = items[element];
    });
    return sortedDates;
  };
  // It is called to sort items by time
  const compareByTime = (a, b) => {
    const timeA = a.threadTime;
    const timeB = b.threadTime;

    let comparison = 0;
    if (timeA > timeB) {
      comparison = 1;
    } else if (timeA < timeB) {
      comparison = -1;
    }
    return comparison;
  };
  const formatDateItem = date => {
    const updatedItem = {};
    updatedItem.updatedDate = date;
    updatedItem.currentItem = "dateItem";
    return updatedItem;
  };
  const formatCommentItem = x => {
    const updatedItem = {
      parentId: x.comment.parentId,
      commentId: x.comment.commentId,
      Id: x.comment.id,
      // localMessageId: uuidv4(),
      avatar: x.pictureUrl,
      createdTime: moment(x.comment.createdDate).format("hh:mm a"),
      timeStamp: moment(x.comment.createdDate).fromNow(),
      commentText: x.comment.commentText,
      // htmlText: x.comment.commentText,
      // deltaText: x.comment.descriptionDelta
      //   ? JSON.parse(x.comment.descriptionDelta)
      //   : x.comment.descriptionDelta,
      commentCreatedBy: x.comment.createdBy,
      commentOwner: x.comment.createdBy == props.profileState.data.userId,
      commentType: 1,
      privateComment: !(
        x.comment.sharedWith == undefined ||
        x.comment.sharedWith.length == 0 ||
        x.comment.sharedWith[0] == "all"
      ),
      createdDate: helper.RETURN_CHATDATEITEM_FORMAT(x.comment.createdDate),
      fileCommentId: null,
      fileDownloadPath: x.comment.attachment ? x.comment.attachment.fileId : null,
      fileName: x.comment.attachment ? x.comment.attachment.name : null,
      sentimentScore: 0,
      fileSize: x.comment.attachment ? helper.convertBytes(x.comment.attachment.fileSize) : "0 kb",
      fileUrlPath: x.comment.attachment ? x.comment.attachment.thumbnail : null,
      fileType: x.comment.attachment ? x.comment.attachment.type : null,
      slackAccessToken: null,
      updatedBy: x.comment.updatedBy,
      updatedDate: helper.RETURN_CHATDATEITEM_FORMAT(x.comment.updatedDate),
      updatedDateTime: x.comment.updatedDate,
      currentItem: "chatItem",
      iconCategory: x.comment.attachment
        ? helper.getFileIconType(x.comment.attachment.type.indexOf("/") != -1 ? x.comment.attachment.type.split("/")[1] : x.comment.attachment.type)
        : null,
      // messageSeenBy: ["5bb4a6fd550cd10bb0a9bf6d"],
      sharedWith: x.comment.sharedWith ? [...x.comment.sharedWith] : ["all"],
      descriptionDelta: "",
      fileRemoveId: x.comment.attachment ? x.comment.attachment.id : null,
      descriptionText: "",
      isEdited: x.comment.isEdited,
      attachment: !!x.comment.attachment,
      replyLater: x.comment.replyLater,
      fullName: x.fullName && isEmpty(x.fullName.trim()) ? x.userName : x.fullName,
      userName: x.userName,
      userId: x.userId,
      pictureUrl: x.pictureUrl,
    };
    if (x.userId === props.profileState.data.userId || !x.userId) {
      const { fullName, userName, email } = props.profileState.data;
      updatedItem.user = "self";
      updatedItem.email = props.profileState.data.email;
      updatedItem.isOnline = true;
      updatedItem.fullName = fullName || userName || email;
    } else {
      const selectedMember =
        props.profileState.data.member.allMembers.find(member => {
          return member.userId == x.userId;
        }) || {};
      updatedItem.user = "other";
      updatedItem.email = selectedMember.email;
      updatedItem.isOnline = selectedMember.isOnline;
      updatedItem.isOwner = selectedMember.isOwner;
    }
    // updatedItem.commentOwner = x.comment.createdBy == props.profileState.data.userId;
    // updatedItem.userId = x.userId;
    // updatedItem.createdDate = helper.RETURN_CHATDATEITEM_FORMAT(x.comment.createdDate);
    // updatedItem.updatedDate = helper.RETURN_CHATDATEITEM_FORMAT(x.comment.updatedDate);
    // updatedItem.updatedDateTime = x.comment.updatedDate;
    // updatedItem.currentItem = "chatItem";
    // updatedItem.fullName = isEmpty(x.fullName.trim()) ? x.userName : x.fullName;
    // updatedItem.userName = x.userName;
    // updatedItem.avatar = x.pictureUrl;
    // updatedItem.commentId = x.comment.commentId;
    // updatedItem.Id = x.comment.id;
    // updatedItem.localMessageId = uuidv4();
    // updatedItem.parentId = x.comment.parentId;
    // updatedItem.groupId = x.comment.groupId;
    // updatedItem.groupType = x.comment.groupType;
    // updatedItem.createdTime = moment(x.comment.createdDate).format("hh:mm a");
    // updatedItem.timeStamp = moment(x.comment.createdDate).fromNow();
    // updatedItem.plainText = x.comment.descriptionText;
    // updatedItem.deltaText = x.comment.descriptionDelta
    //   ? JSON.parse(x.comment.descriptionDelta)
    //   : x.comment.descriptionDelta;
    // updatedItem.htmlText = x.comment.commentText;
    // updatedItem.isEdited = x.comment.isEdited;
    // updatedItem.attachment = x.comment.attachment ? true : false;
    // updatedItem.fileName = x.comment.attachment ? x.comment.attachment.name : null;
    // updatedItem.fileDownloadPath = x.comment.attachment ? x.comment.attachment.fileId : null;
    // updatedItem.fileRemoveId = x.comment.attachment ? x.comment.attachment.id : null;
    // updatedItem.fileSize = x.comment.attachment
    //   ? helper.convertBytes(x.comment.attachment.fileSize)
    //   : "0 kb";
    // updatedItem.fileType = x.comment.attachment ? x.comment.attachment.type : null;
    // updatedItem.iconCategory = x.comment.attachment
    //   ? helper.getFileIconType(x.comment.attachment.type.split("/")[1])
    //   : null;
    // updatedItem.fileUrlPath = x.comment.attachment ? x.comment.attachment.thumbnail : null;
    // // "https://ntaskattachments.s3.amazonaws.com/ProfileImages/a163969497a94bbfb19af962167321fe.jpg"; //x.comment.attachment ? x.comment.attachment.docsPath : null;
    // updatedItem.replyLater = x.comment.replyLater;
    // updatedItem.sharedWith = x.comment.sharedWith ? [...x.comment.sharedWith] : ["all"];
    // updatedItem.privateComment =
    //   x.comment.sharedWith == undefined ||
    //   x.comment.sharedWith.length == 0 ||
    //   x.comment.sharedWith[0] == "all"
    //     ? false
    //     : true;
    // let hasObjValue = isEmpty(x.comment.replyOf)
    //   ? null
    //   : x.comment.replyOf.userId == null
    //     ? null
    //     : {};
    // let replyContent = null;
    // if (hasObjValue) {
    //   let replyMember =
    //     props.profileState.data.member.allMembers.find((member) => {
    //       return member.userId == x.comment.replyOf.userId;
    //     }) || {};
    //   replyContent = { ...x.comment.replyOf, fullName: replyMember.fullName };
    // }
    // updatedItem.replyOf = replyContent;
    // if(x.comment.messageSeenBy.length > 0 ){

    // }
    updatedItem.delivered = !(x.comment.messageSeenBy && x.comment.messageSeenBy.length > 1);
    const seenArr = [];
    if (x.comment.messageSeenBy && x.comment.messageSeenBy.length > 1) {
      props.profileState.data.member.allMembers.filter(member => {
        if (x.comment.messageSeenBy.indexOf(member.userId) != -1) seenArr.push(member.fullName);
      });
    }
    updatedItem.messageSeenBy = seenArr;
    return updatedItem;
  };

  const handleInput = () => {};
  const showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };
  const onReplyLaterHandler = (comment, success, fail) => {
    const data = actuallAllItems.find(item => item.comment.commentId == comment.commentId);

    props.SetReplyLaterFlag(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      data,
      res => {
        success();
      },
      error => {
        fail(error);
      }
    );
  };
  const onReplyLaterClearHandler = (comment, success, fail) => {
    const data = actuallAllItems.find(item => item.comment.commentId == comment.commentId);
    props.ClearReplyLaterFlag(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      data,
      res => {
        success();
      },
      error => {
        fail(error);
      }
    );
  };
  const onConvertToTaskHandler = (comment, success, fail) => {
    let title = "";
    if (comment.plainText) {
      title = comment.plainText.substr(0, 80).trim("");
    } else {
      const text = htmlToText.fromString(comment.commentText, {
        wordwrap: 130,
      });
      title = text.substr(0, 80).trim("");
    }
    const data = {
      groupId: props.chatConfig.contextId,
      groupType: props.chatConfig.contextView,
      taskTitle: title,
    };
    props.ConvertToTask(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      data,
      result => {
        success(result);
      },
      error => {
        fail(error);
      }
    );
  };
  // const initialCommentItem = x => {
  //   const updatedItem = {};
  //   const profile = props.profileState.data;
  //   updatedItem.userId = profile.userId;
  //   updatedItem.userName = profile.userName;
  //   updatedItem.pictureUrl = "";
  //   updatedItem.fullName = profile.fullName;
  //   updatedItem.localMessageId = uuidv4();
  //   updatedItem.comment = {};
  //   updatedItem.createdBy = profile.userId;
  //   updatedItem.comment.createdDate = new Date(
  //     `${new Date().toString().split("GMT")[0]} UTC`
  //   ).toISOString();
  //   updatedItem.comment.updatedDate = updatedItem.comment.createdDate;
  //   updatedItem.comment.commentId = "";
  //   updatedItem.comment.id = "";
  //   updatedItem.comment.parentId = "";
  //   updatedItem.comment.groupId = props.chatConfig.contextId;
  //   updatedItem.comment.groupType = props.chatConfig.contextView;
  //   updatedItem.comment.descriptionText = x.descriptionText;
  //   updatedItem.comment.descriptionDelta = x.descriptionDelta;
  //   updatedItem.comment.commentText = x.commentText;
  //   updatedItem.comment.attachment = null;
  //   if (updatedItem.comment.attachment) {
  //     updatedItem.comment.attachment.name = null;
  //     updatedItem.comment.attachment.fileSize = null;
  //     updatedItem.comment.attachment.type = null;
  //     updatedItem.comment.attachment.docsPath = null;
  //   }
  //   updatedItem.comment.replyLater = false;
  //   updatedItem.comment.sharedWith = ["all"];
  //   updatedItem.comment.replyOf = {};
  //   updatedItem.comment.delivered = [];
  //   updatedItem.comment.seen = [];
  //   return updatedItem;
  // };
  /**
   * it is called when user save comment either reply comment or new comment
   * @param {Object} data
   * @param {Object} parentComment
   */
  const addCommentHandler = (data, parentComment) => {
    data.groupId = props.chatConfig.contextId;
    data.groupType = props.chatConfig.contextView;

    if (parentComment) {
      data.parentId = parentComment.commentId;
      const parentData = actuallAllItems.find(
        item => item.comment.commentId == parentComment.commentId
      );
      parentData.comment.replyLater = false;
      // It is called when user reply any comment
      props.ReplyUserCommentFromUpdates(
        props.chatConfig.contextUrl,
        props.chatConfig.contextId,
        props.chatConfig.contextView,
        data,
        parentData,
        res => {
          // let obj = {
          //   groupId: props.chatConfig.contextId,
          //   type: {
          //     context: 'chat',
          //     action: 'Create'
          //   },
          //   update: res.data
          // }
          // $.connection.notificationHub.server.publishUpdate(obj);
        },
        error => {
  
        }
      );
    } else {
      // it is called when user create new comment
      // setActuallAllItems([...actuallAllItems , initialCommentItem(data)]);
      // return;
      props.saveUserComment(
        props.chatConfig.contextUrl,
        props.chatConfig.contextId,
        props.chatConfig.contextView,
        data,
        res => {
          // let obj = {
          //   groupId: props.chatConfig.contextId,
          //   type: {
          //     context: 'chat',
          //     action: 'Create'
          //   },
          //   update: res.data
          // }
          // $.connection.notificationHub.server.publishUpdate(obj);
        },
        error => {
        }
      );
    }
  };
  // const addCommentInParentComment = (rawComment, pComment) => {
  //   let obj = JSON.parse(JSON.stringify(formattedAllItems));
  //   if (obj[pComment.commentId]["children"] == undefined) {
  //     obj[pComment.commentId]["children"] = {};
  //   }
  //   obj[pComment.commentId]["children"][rawComment.comment.commentId] = formatCommentItem(
  //     rawComment
  //   );
  //   setFormattedAllItems(obj);
  // };

  // const addCommentInDS = rawComment => {
  //   let comment = formatCommentItem(rawComment);
  //   let obj = JSON.parse(JSON.stringify(formattedAllItems));
  //   obj[comment.commentId] = comment;
  //   obj[comment.commentId]["children"] = {};
  //   setFormattedAllItems(obj);
  //   // }
  // };
  /**
   * it is called when user save comment with attachment file
   * @param {Object} data
   * @param {Object} parentComment
   * @param {Function} success
   * @param {Function} fail
   */
  const saveFileCommentDataHandler = (data, parentComment, success, fail) => {
    // data.groupId = props.chatConfig.contextId;
    // data.groupType = props.chatConfig.contextView;
    if (parentComment) {
      data.parentId = parentComment.commentId;
    }
    props.saveUserComment(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      props.chatConfig.contextView,
      data,
      res => {
        // let obj = {
        //   groupId: props.chatConfig.contextId,
        //   type: {
        //     context: 'chat',
        //     action: 'Create'
        //   },
        //   update: res.data
        // }
        // $.connection.notificationHub.server.publishUpdate(obj);
        success(res);
      },
      error => {
        fail(error);
      }
    );
  };
  const deleteUserCommentHandler = (comment, parentComment, success, fail) => {
    const data = actuallAllItems.find(item => item.comment.commentId == comment.commentId);

    props.DeleteUesrComment(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      props.chatConfig.contextView,
      data,
      res => {
        success(res);
      },
      error => {
        fail(error);
      }
    );
  };
  const onDownloadFileHandler = () => {};
  const onRemoveFileHandler = (comment, parentComment, success, fail) => {
    const data = actuallAllItems.find(item => item.comment.commentId == comment.commentId);

    props.DeleteChatAttachment(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      props.chatConfig.contextView,
      data,
      res => {
        // const newItem = formatCommentItem(res.data);
        // let newFormattedAllItems = JSON.parse(
        //   JSON.stringify(formattedAllItems)
        // );
        // if (parentComment) {
        //   newFormattedAllItems[parentComment.commentId]['children'][comment.commentId] = newItem;
        // } else {
        //   newFormattedAllItems[comment.commentId]['attachment'] = null;
        // }
        // setFormattedAllItems({ ...formattedAllItems, ...newFormattedAllItems });
        success(res);
      },
      error => {
        fail(error);
      }
    );
  };
  const updateCommentHander = (data, success, fail) => {
    props.UpdateUesrComment(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      data,
      res => {
        success();
      },
      error => {
        fail(error);
      }
    );
  };
  const StartTypingHndler = () => {
    const data = {
      groupType: props.chatConfig.contextView,
      groupId: props.chatConfig.contextId,
    };
    props.StartTyping(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      data,
      res => {
        // success();
      },
      error => {
        // fail(error);
      }
    );
  };
  const StopTypingHandler = () => {
    const data = {
      groupType: props.chatConfig.contextView,
      groupId: props.chatConfig.contextId,
    };
    props.StopTyping(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      data,
      res => {
        
      },
      error => {
        // fail(error);
      }
    );
  };

  const updateToolbarOptions = [
    ["bold", "italic", "underline", "strike"],
    [{ list: "ordered" }, { list: "bullet" }],
    ["link"],
  ];

  const { classes, theme, chatConfig, chatPermission, showReceipt, showChildReceipt } = props;
  return (
    <>
      <div className={classes.commentsCnt}>
      {loading ? 
          <Loader />
      : !loading && isEmpty(formattedAllItems) ? (
          <div className={classes.noConversation}>
            <SvgIcon
              style={{ width: 74, height: 60 }}
              viewBox="0 0 74.002 59.947"
              htmlColor={theme.palette.secondary.dark}>
              <NoConversationIcon />
            </SvgIcon>
            <Typography variant="h2" style={{ marginTop: 14 }}>
              <FormattedMessage
                id="common.comment.no-conversation.label"
                defaultMessage="No Conversations Yet"
              />
            </Typography>
            <Typography
              variant="h6"
              className= {classes.fontSize}
              style={{
                fontSize: "13px",
                marginTop: 10,
                textAlign: "center",
                color: theme.palette.text.grayDarker,
              }}>
              <FormattedMessage
                id="common.comment.no-conversation.placeholder2"
                values={{ b: <br /> }}
                defaultMessage={`Start conversation with your team members by typing
              ${(<br />)} your comment below.`}
              />
            </Typography>
          </div>
        ) : (
          <>
            {/* <DefaultTextField
              fullWidth={true}
              labelProps={{
                shrink: true,
              }}
              error={false}
              defaultProps={{
                id: "workspaceMember",
                onChange: handleInput("workspaceMember"),
                value: searchText,
                placeholder: props.intl.formatMessage({
                  id: "common.search.label",
                  defaultMessage: "Search",
                }),
                inputProps: {
                  maxLength: 80,
                  style: {
                    padding: "8px 8px 8px 0px",
                  },
                },
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon
                      viewBox="0 0 14 14"
                      classes={{ root: classes.searchIcon }}
                      htmlColor={theme.palette.text.grayDarker}>
                      <SearchTextIcon />
                    </SvgIcon>
                  </InputAdornment>
                ),
              }}
              formControlStyles={{
                width: "100%",
                padding: "10px 15px 0px",
                marginBottom: 0,
              }}
            /> */}

            <ChatItems
              allChatList={getAllChatItems()}
              chatConfig={chatConfig}
              chatPermission={chatPermission}
              replyLaterItemHandler={onReplyLaterHandler}
              replyLaterItemClearHandler={onReplyLaterClearHandler}
              convertToTaskHandler={onConvertToTaskHandler}
              deleteUserComment={deleteUserCommentHandler}
              onDownloadFile={onDownloadFileHandler}
              onRemoveFile={onRemoveFileHandler}
              showSnackBar={showSnackBar}
              saveFileCommentDataHandler={saveFileCommentDataHandler}
              addCommentHandler={addCommentHandler}
              updateComment={updateCommentHander}
              intl={intl}
              // showReceipt={showReceipt}
              parentReceipt={showReceipt}
              showChildReceipt={showChildReceipt}
            />
          </>
        )}
      </div>
      <div style={{ margin: "10px 15px 12px" }}>
        <ChatTextEditor
          chatConfig={chatConfig}
          chatPermission={chatPermission}
          id="updatesCustomToolbar"
          showSnackBar={showSnackBar}
          saveFileCommentData={(data, success, fail) =>
            saveFileCommentDataHandler(data, null, success, fail)
          }
          addComments={addCommentHandler}
          visibleUserDropDown={false}
          updateToolbarOptions={updateToolbarOptions}
          inlineEditor={false}
          startTyping={StartTypingHndler}
          stopTyping={StopTypingHandler}
          placeholder="Type a message"
          intl={intl}
        />
      </div>
    </>
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile,
    chatItems: state.chat.data.chatItems,
  };
};
export default compose(
  withSnackbar,
  injectIntl,
  connect(mapStateToProps, {
    ConvertToTask,
    UpdateUesrComment,
    DeleteUesrComment,
    DeleteChatAttachment,
    GetUpdatesComments,
    SetReplyLaterFlag,
    ClearReplyLaterFlag,
    saveUserComment,
    ReplyUserCommentFromUpdates,
    StoreChatUpdate,
    StartTyping,
    StopTyping,
    clearStore,
  }),
  withStyles(Styles, { withTheme: true })
)(Updates);
