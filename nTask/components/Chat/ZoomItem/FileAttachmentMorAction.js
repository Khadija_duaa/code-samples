import React, { Component, Fragment, useState, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
import { compose } from "redux";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../Buttons/IconButton";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import SelectionMenu from "../../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { connect } from "react-redux";
import MoreOptionsIcon from "../../Icons/MoreOptionsIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import helper from "../../../helper/index";
import { FormattedMessage } from "react-intl";
function FileAttachmentMorAction(props) {

  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState("");
  const anchorEl = useRef();


  const handleClick = (event, newPlacement) => {
    const { currentTarget } = event;
    let newOPen = placement !== newPlacement || !open;
    setOpen(newOPen);
    setPlacement(newPlacement);
  }
  const handleClose = (event) => {
    setOpen(false);
  }
  const onDownloadHandler = (comment) => {
    helper.DOWNLOAD_TEMPLATE(
      comment.fileDownloadPath,
      comment.fileName,
      null,
      "attachment"
    )
    setOpen(false);
  }
  const onRemoveHandler = comment => {
    props.onRemoveFile(comment, success => {
      setOpen(false);
    },
      fail => {

      });
  }

  const { classes, theme, Id, comment, iconColor = theme.palette.background.black } = props;
  return (
    <ClickAwayListener mouseEvent="onMouseDown" touchEvent="onTouchStart" onClickAway={handleClose}>
      <div>
        <CustomIconButton
          btnType="condensed"
          onClick={event => {
            handleClick(event, "bottom-end");
          }}
          buttonRef={node => {
            anchorEl.current = node;
          }}
        >
          <SvgIcon viewBox="0 0 4 16" style={{ fontSize: "16px !important", color: iconColor }} >
            <MoreOptionsIcon />
          </SvgIcon>
        </CustomIconButton>

        <SelectionMenu
          open={open}
          closeAction={handleClose}
          placement={placement}
          anchorRef={anchorEl.current}
          list={
            <List>
              <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onDownloadHandler.bind(this, comment)}
              >
                <ListItemText
                  primary={<FormattedMessage id="project.dev.tasks.detail.upload-file.list.download" defaultMessage="Download" />}
                  classes={{
                    primary: classes.statusItemText
                  }}
                />
              </ListItem>
              {comment.commentOwner && <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onRemoveHandler.bind(this, comment)}
              >
                <ListItemText
                  primary={<FormattedMessage id="team-settings.billing.bill-dialog.remove-button.label" defaultMessage="Remove" />}
                  classes={{
                    primary: classes.statusItemText
                  }}
                />
              </ListItem>}
            </List>
          }
        />
      </div>
    </ClickAwayListener>
  );
}
const mapStateToProps = state => {
  return {
  };
};
export default compose(
  connect(mapStateToProps, {
  }),
  withStyles(combineStyles(Styles, menuStyles), {
    withTheme: true
  })
)(FileAttachmentMorAction);
