import GradientOverly from "../../../assets/images/svgs/GradientOverly.svg"

const zoomItemStyles = (theme) => ({

  chatItemCnt: {
    display: "flex",
    // alignItems: "end",
    marginBottom: 8,
    
  },
  
    commentContentOuterCnt:{
        // minWidth: 396,
        borderRadius: 6,
        border: `1px solid ${theme.palette.border.grayLighter}`,
        background: theme.palette.background.default,
        paddingTop: 8,
        width: '90%',
        "&:hover":{
          boxShadow: `0px 1px 0px ${theme.palette.border.grayLighter}`,
        },
        
    },
    commnetContentInnerCnt: {
      "&:hover .dropDown": {
        visibility: "visible",
      },
    },
    dropDown:{
      visibility: "hidden",
      position: 'absolute',
      right: 3,
      top: '-24px',
      // background: theme.palette.common.white,
      // border: `1px solid ${theme.palette.background.contrast}`,
      // boxShadow: '0px 3px 6px #00000029',
      // borderRadius: 4,
    },
    actionIcon:{
      fontSize: "16px !important",
    },
    headerCnt: {
        fontSize: "12px !important",
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
        paddingLeft: 10,
        paddingRight: 8,
    },
    profileCnt: {
        display: 'flex',
        alignItems: 'center'
    },
    privateStatus: {
      marginLeft: 5,
      color: theme.palette.text.danger,
      fontSize: "11px !important",
      lineHeight: '1.5'
    },
    replyLaterIcon: {
      fontSize: "12px !important",
      marginLeft: 5
    },
    sendTime: {
      color: theme.palette.text.grayDarker,
      fontSize: "10px !important",
      lineHeight: '1.5'
    },
    editedCnt: {
      color: theme.palette.text.grayDarker,
      fontSize: "11px !important",
      marginLeft: 5,
    },
    commentContentCnt: {
        paddingTop: 3,
        fontSize: "13px !important",
        paddingLeft: 10,
        paddingRight: 8,
        paddingBottom: 8,
        // minWidth: 200,
        borderBottom: `1px solid ${theme.palette.border.mediumBorder}`,
        wordBreak: "break-all",
        "& p": {
            margin: 0,
            padding: 0,
            lineHeight: 1.5,
            wordBreak: "break-word"
        },
        "& ol": {
            padding: 0,
            margin: 0,
        },
        "& ul": {
            padding: 0,
            margin: 0,
        },
        "& li": {
            padding: 0,
            margin: "0 0 10px 15px",
        },
        "& li:last-child": {
            margin: "0 0 0 15px",
        },
        "& strong": {
            fontWeight: theme.palette.fontWeightMedium,
        },
    },
    fileAttachmentMainCnt:{
      display: 'flex',
      // justifyContent: 'center'
      marginTop: 8
    },
    fileAttachmentCnt: {
      position: 'relative',
      margin: 'auto'
    },
    filePreviewImage: {
      height: 168,
      borderRadius: 6,
      width: 300
    },
    filePathCnt: {
      position: 'absolute',
      bottom: 13,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      width: '100%',
      padding: '0 14px 0 11px'
    },
    fileTypeName:{
      display: 'flex',
      alignItems: 'center'
    },
    seenCnt: {
      display: 'flex',
      alignItems: 'center',
      // marginTop: 4,
      // paddingLeft: 10,
      // paddingRight: 8,
    },
    showReceiptCnt: {
      padding: '4px 10px 4px 8px'
    },
    seenList: {
      color: theme.palette.secondary.medDark,
      fontSize: "11px !important",
      marginLeft: 5,
    },
    deliveredCnt: {
      display: 'flex',
      alignItems: 'center',
    },
    deliveredTxt: {
      marginLeft: 5,
      color: theme.palette.secondary.medDark,
      fontSize: "11px !important",
    },
    replyMainCnt:{
      display: 'flex',
      borderBottomLeftRadius: 'inherit',
      borderBottomRightRadius: 'inherit',
      background: theme.palette.background.items,
      padding: '5px 8px 8px 8px',
      alignItems: 'center',
      cursor: 'pointer',
      '&:hover': {
        background: theme.palette.common.white,
        '& $replyTxt': {
          color: theme.palette.text.brightBlue,
        },
        '& $replyIcon':{
          color: theme.palette.text.brightBlue,
          stroke: theme.palette.text.brightBlue,
        }
      }
  },
  expandCollapseMainCnt: {
    display: 'flex', 
    alignItems: 'center',
    padding: '4px 0px 4px 8px',
    cursor: 'pointer'
  },
  expandIconCnt:{
    fontSize: "10px !important",
    color: theme.palette.text.brightBlue
  },
  collapseIconCnt: {
    fontSize: "10px !important",
    transform: 'rotate(-90deg)',
    color: theme.palette.text.brightBlue
  },
  expColTextCnt: {
    color: theme.palette.text.brightBlue,
    marginLeft: 5,
  },
  replyIcon: {
    width: 12.013, 
    height: 8.76,
    color: theme.palette.text.grayDarker,
    marginTop: 5,
    stroke: theme.palette.text.grayDarker
  },
  replyTextCnt: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 6,
    fontSize: "12px !important"
  },
  replyTxt:{
    marginTop: 3,
    color: theme.palette.text.darkGray,
    '& p':{
        margin: 0,
        padding: 0,
    }
},
compressFilePreview: {
    display: 'flex',
    border: `1px solid ${theme.palette.border.grayLighter}`,
    background: theme.palette.background.grayLightest,
    borderRadius: 6,
    padding: '6px 10px',
    marginTop: 3,
    justifyContent: 'space-between',
    minWidth: 300
},
gradientFileEffect: {
  background: `url(${GradientOverly})`,
  backgroundRepeat: "no-repeat",
  height: 110,
  position: 'absolute',
  width: '100%',
  bottom: 0,
  borderRadius: 6,
},
tooltip: {
  fontSize: "12px !important",
  backgroundColor: theme.palette.common.black,
  '& .chat-tooltip-body': {
    padding: '5px 15px 5px 5px',
      '& ul,li':{
          margin: 0,
          padding: 0,
          listStyleType: 'none',
          color: theme.palette.common.white
      }
    }
  },
  itemsShowCnt: {
    background: theme.palette.background.items,
    borderBottom: `1px solid ${theme.palette.border.mediumBorder}`,
  },
  joinSection: {
    marginTop: 8,
    "& a": {
      color: theme.palette.text.azure,
      fontSize: "12px !important",
    },
  },
  meetingId: {
    display: 'flex',
    marginTop: 8
  },
  passcode: {
    display: 'flex'
  }
});

export default zoomItemStyles;