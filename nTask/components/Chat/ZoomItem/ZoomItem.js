import React, { useState, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
import CustomAvatar from "../../Avatar/Avatar";
import ReactHtmlParser from 'react-html-parser';
import Icon from "./../../Icons/Icons";
import Truncate from "react-truncate";
import helper from "../../../helper/index";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Typography from "@material-ui/core/Typography";
import SeenIcon from "../../Icons/SeenIcon";
import DeliveredIcon from "../../Icons/DeliveredIcon";
import ReplyLaterIcon from "../../Icons/ReplyLaterIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import Tooltip from "@material-ui/core/Tooltip";
import ZoomItemMoreAction from "./ZoomItemMoreAction";
import ReplyThinIcon from "../../Icons/ReplyThinIcon";
import PreviewDocFileIcon from "../../Icons/PreviewDocFileIcon";
import CaretIcon from "../../Icons/CaretIcon";
import PreviewCompressFileIcon from "../../Icons/PreviewCompressFileIcon";
import PreviewAudioVideoFileIcon from "../../Icons/PreviewAudioVideoFileIcon";
import PreviewImageFileIcon from "../../Icons/PreviewImageFileIcon";
import ReplyItem from "../ReplyItem/ReplyItem";
import isEmpty from "lodash/isEmpty";
import ChatTextEditor from "../../TextEditor/CustomTextEditor/ChatTextEditor/ChatTextEditor";

function ZoomItem(props) {
  const [reply, setReply] = useState(false);
  const [expand, setExpand] = useState(false);
  const [editComment, setEditComment] = useState(null);

  const updateToolbarOptions = [["bold", "italic", "underline", "strike"], [{ list: "bullet" }]];

  const addEditCommentHandler = (data, comment, success, fail) => {
    setEditComment(null);
    props.addCommentHandler(data, comment, success, fail);
  };
  const addReplyCommentHandler = (data, comment, success, fail) => {
    setReply(null);
    props.addCommentHandler(data, comment, success, fail);
  };
  const saveFileCommentDataHandler = (data, comment, success, fail) => {
    props.saveFileCommentDataHandler(data, comment, success, fail);
  };

  const editCommentHandler = (comment, success, fail) => {
    setEditComment(comment);
    success();
  };

  const textEditorAction = value => {
    // value === "blur" && setEditComment(null);
  };

  const chatEditorAction = value => {
    value === "blur" && setEditComment(null);
  };
  const replyTextEditorAction = value => {
    // value === "blur" && setReply(false);
  };

  const replyChatEditorAction = value => {
    value === "blur" && setReply(false);
  };

  const updateCommentHandler = data => {
    props.updateComment(
      data,
      success => {
        setEditComment(null);
      },
      fail => {
        setEditComment(null);
      }
    );
  };

  const { theme, classes, id, comment, chatConfig, chatPermission, showReceipt, intl } = props;
  let text = comment.commentText.replace("<p><br></p>", "");
  if (comment.isEdited)
    text =
      comment.commentText.substr(0, comment.commentText.lastIndexOf("</p>")) +
      `<span class=${classes.editedCnt}>- Edited</span></p>`;
  let style = chatConfig["contextChat"] == "group" ? { marginLeft: 5 } : { marginRight: 5 };
  //To check reply itesm exist or not
  let haveReplyItems = isEmpty(comment.children) ? false : true;
  //To check either show collapse/expand button or not
  let showExpandDialog =
    haveReplyItems && Object.values(comment.children).length > 1 ? true : false;
  //To check length of reply items
  let replyItemsLength = haveReplyItems ? Object.values(comment.children).length : 0;
  //To get last reply item
  let lastChatItem = haveReplyItems ? Object.values(comment.children)[replyItemsLength - 1] : null;
  //To show the text in collapse/expand span
  let collapseText = haveReplyItems
    ? replyItemsLength +
      (replyItemsLength > 1 ? " replies" : " reply") +
      " from You and " +
      lastChatItem.fullName
    : "";

  return (
    <div
      key={id}
      style={{
        flexDirection: chatConfig.contextChat == "group" ? "row-reverse" : "row",
      }}
      className={classes.chatItemCnt}>
      {comment.user === "self" ? (
        <CustomAvatar
          styles={style}
          otherMember={{
            imageUrl: comment.avatar,
            fullName: comment.fullName,
            lastName: "",
            email: comment.email,
            isOnline: comment.isOnline,
          }}
          size="small"
        />
      ) : (
        <CustomAvatar
          styles={{ marginRight: 5 }}
          otherMember={{
            imageUrl: comment.avatar,
            fullName: comment.fullName,
            lastName: "",
            email: comment.email,
            isOnline: comment.isOnline,
          }}
          size="small"
        />
      )}
      <div className={classes.commentContentOuterCnt}>
        {editComment && comment.commentId == editComment.commentId ? (
          /* This editor is used when user edit any chat item*/
          <div>
            <ChatTextEditor
              chatConfig={chatConfig}
              id="zoomCustomToolbar"
              chatPermission={chatPermission}
              showSnackBar={props.showSnackBar}
              saveFileCommentData={(data, success, fail) =>
                saveFileCommentDataHandler(data, comment, success, fail)
              }
              addComments={(data, success, fail) =>
                addEditCommentHandler(
                  data,
                  null,
                  () => {},
                  () => {}
                )
              }
              replyItem={null}
              removeReply={() => {}}
              visibleUserDropDown={false}
              updateToolbarOptions={updateToolbarOptions}
              chatData={editComment.commentText}
              editComment={editComment}
              updateComment={data => updateCommentHandler(data)}
              inlineEditor={true}
              textEditorAction={e => textEditorAction(e)}
              chatEditorAction={e => chatEditorAction(e)}
              intl={intl}
            />
          </div>
        ) : (
          <>
            <div className={classes.commnetContentInnerCnt}>
              <div className={classes.headerCnt}>
                <span className={classes.profileCnt}>
                  <Typography
                    variant="h6"
                    style={{
                      fontWeight: theme.typography.fontWeightLarge,
                      lineHeight: "1.5",
                    }}>
                    {comment.fullName}
                  </Typography>

                  {comment.replyLater && (
                    <SvgIcon className={classes.replyLaterIcon} viewBox="0 0 12 12">
                      <ReplyLaterIcon />
                    </SvgIcon>
                  )}
                </span>
                <Typography variant="h6" classes={{ h6: classes.sendTime }}>
                  {comment.createdTime}
                </Typography>
                <ZoomItemMoreAction
                  comment={comment}
                  showSnackBar={props.showSnackBar}
                  // replyItemHandler = {props.replyItemHandler}
                  replyLaterItemHandler={props.replyLaterItemHandler}
                  replyLaterItemClearHandler={props.replyLaterItemClearHandler}
                  convertToTaskHandler={props.convertToTaskHandler}
                  deleteComment={(data, success, fail) =>
                    props.deleteComment(data, null, success, fail)
                  }
                  editComment={(data, success, fail) => editCommentHandler(data, success, fail)}
                />
              </div>
              <div className={classes.commentContentCnt}>
                {ReactHtmlParser(text)}
                {}
                <div className={classes.joinSection}>
                  <Typography variant="h6">Join Zoom Meeting</Typography>
                  <a href={"https://us04web.zoom.us/j/77784560354?pwd=bFR"} target="_blank">
                    {"https://us04web.zoom.us/j/77784560354?pwd=bFR"}
                  </a>
                </div>
                <div className={classes.meetingId}>
                  <Typography variant="h6">Meeting ID:</Typography>
                  <Typography variant="h6" style={{ marginLeft: 5 }}>
                    777 8456 0354
                  </Typography>
                </div>
                <div className={classes.passcode}>
                  <Typography variant="h6">Passcode:</Typography>
                  <Typography variant="h6" style={{ marginLeft: 5 }}>
                    BJjeM8
                  </Typography>
                </div>
              </div>
            </div>{" "}
          </>
        )}
        {showExpandDialog ? (
          <Fragment>
            {haveReplyItems && (
              <div
                className={classes.itemsShowCnt}
                onClick={() => {
                  setExpand(prevState => !prevState);
                }}>
                {expand ? (
                  <span className={classes.expandCollapseMainCnt}>
                    <SvgIcon viewBox="0 0 10.006 5.624" className={classes.expandIconCnt}>
                      <CaretIcon />
                    </SvgIcon>
                    <Typography variant="h6" classes={{ h6: classes.expColTextCnt }}>
                      Collapse all
                    </Typography>
                  </span>
                ) : (
                  <span className={classes.expandCollapseMainCnt}>
                    <SvgIcon viewBox="0 0 10.006 5.624" className={classes.collapseIconCnt}>
                      <CaretIcon />
                    </SvgIcon>
                    <Typography variant="h6" classes={{ h6: classes.expColTextCnt }}>
                      {collapseText}
                    </Typography>
                  </span>
                )}
              </div>
            )}
            {isEmpty(comment.children)
              ? null
              : expand &&
                Object.values(comment.children).map((replyChatItem, index) => (
                  <ReplyItem
                    id={index}
                    comment={replyChatItem}
                    chatPermission={chatPermission}
                    chatConfig={chatConfig}
                    convertToTaskHandler={props.convertToTaskHandler}
                    deleteComment={(data, success, fail) =>
                      props.deleteComment(data, comment, success, fail)
                    }
                    showReceipt={showReceipt}
                    openDeleteDialogue={props.openDeleteDialogue}
                    onDownloadFile={props.onDownloadFile}
                    onRemoveFile={(data, success, fail) =>
                      props.onRemoveFile(data, comment, success, fail)
                    }
                    updateComment={props.updateComment}
                    showSnackBar={props.showSnackBar}
                    />
                ))}
          </Fragment>
        ) : (
          Object.values(comment.children).map((replyChatItem, index) => (
            <ReplyItem
              id={index}
              comment={replyChatItem}
              chatPermission={chatPermission}
              chatConfig={chatConfig}
              convertToTaskHandler={props.convertToTaskHandler}
              deleteComment={(data, success, fail) =>
                props.deleteComment(data, comment, success, fail)
              }
              showReceipt={showReceipt}
              openDeleteDialogue={props.openDeleteDialogue}
              onDownloadFile={props.onDownloadFile}
              onRemoveFile={(data, success, fail) =>
                props.onRemoveFile(data, comment, success, fail)
              }
              updateComment={props.updateComment}
              showSnackBar={props.showSnackBar}
              />
          ))
        )}
        {/* 
          This editor is use, when user reply to any chat Item
        */}
        {reply ? (
          <div>
            <ChatTextEditor
              chatConfig={chatConfig}
              id="zoomCustomToolbar"
              chatPermission={chatPermission}
              showSnackBar={props.showSnackBar}
              saveFileCommentData={(data, success, fail) =>
                saveFileCommentDataHandler(data, comment, success, fail)
              }
              addComments={(data, success, fail) =>
                addReplyCommentHandler(data, comment, success, fail)
              }
              replyItem={null}
              removeReply={() => {}}
              visibleUserDropDown={false}
              updateToolbarOptions={updateToolbarOptions}
              inlineEditor={true}
              textEditorAction={e => replyTextEditorAction(e)}
              chatEditorAction={e => replyChatEditorAction(e)}
              intl={intl}
            />
          </div>
        ) : (
          <div
            className={classes.replyMainCnt}
            onClick={() => {
              setReply(prevState => !prevState);
            }}>
            <SvgIcon className={classes.replyIcon} viewBox="0 0 12.013 8.76">
              <ReplyThinIcon />
            </SvgIcon>
            <span className={classes.replyTextCnt}>
              <Typography
                variant="h6"
                classes={{
                  h6: classes.replyTxt,
                }}>
                Reply
              </Typography>
            </span>
          </div>
        )}
        {showReceipt ? (
          comment.delivered ? (
            <div className={classes.showReceiptCnt}>
              <div className={classes.deliveredCnt}>
                <SvgIcon
                  style={{ width: 12, height: 10 }}
                  viewBox="0 0 12 10.569"
                  htmlColor={theme.palette.secondary.dark}>
                  <DeliveredIcon />
                </SvgIcon>
                <span className={classes.deliveredTxt}>Delivered</span>
              </div>
            </div>
          ) : (
            <div className={classes.showReceiptCnt}>
              <div className={classes.seenCnt}>
                <SvgIcon
                  style={{ width: 14, height: 9 }}
                  viewBox="0 0 14.018 9.169"
                  htmlColor={theme.palette.secondary.dark}>
                  <SeenIcon />
                </SvgIcon>
                <Tooltip
                  classes={{
                    tooltip: classes.tooltip,
                  }}
                  title={
                    <div className="chat-tooltip-body">
                      <ul>
                        {comment.messageSeenBy.map((pStrength, i) => {
                          return (
                            <li key={i}>
                              <small>{pStrength}</small>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  }
                  placement="bottom">
                  <span className={classes.seenList}>
                    {comment.messageSeenBy[0]} {comment.messageSeenBy.length > 1 ? " & others" : ""}
                  </span>
                </Tooltip>
              </div>
            </div>
          )
        ) : null}
      </div>
    </div>
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile,
  };
};
export default compose(connect(mapStateToProps), withStyles(Styles, { withTheme: true }))(ZoomItem);
