const chatItemsStyles = (theme) => ({
    itemsCnt: {
        padding: "12px 14px 12px 12px",
        '& ul,li':{
            margin: 0,
            padding: 0,
            listStyleType: 'none',
        }
    },
    dateReceiptCnt: {
        display: 'flex',
        fontSize: "12px !important",
        alignItems: 'center',
        padding: '8px 16px 8px 12px',
    },
    sepratorHr: {
        marginLeft: 5,
        marginRight: 5,
        flex: 1
    },
    receiptCnt: {
        color: theme.palette.text.azure,
        cursor: 'pointer'
    },
    dateItemCnt:{
        display: 'flex',
        alignItems: 'center',
        marginBottom: 8
    },
    hrLeft: {
        marginLeft: 5,
        flex: 1
    },
});

export default chatItemsStyles;