import React, { useState, useRef, Fragment, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { Scrollbars } from "react-custom-scrollbars";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import { FormattedMessage } from "react-intl";
import ZoomItem from "../ZoomItem/ZoomItem";
import ChatItem from "../ChatItem/ChatItem";
import Styles from "./Styles";

function ChatItems(props) {
  const scrollComponent = useRef();
  const [childReceipt, setReceipt] = useState(null);

  const receiptHandler = () => {
    setReceipt(!childReceipt);
  };

  useEffect(() => {
    if (scrollComponent.current) {
      scrollComponent.current.scrollToBottom();
    }
    return () => {};
  }, [props.allChatList]);

  const {
    classes,
    allChatList,
    chatConfig,
    chatPermission,
    intl,
    parentReceipt,
    showChildReceipt,
  } = props;

  return (
    <>
      {showChildReceipt ? (
        <div className={classes.dateReceiptCnt}>
          {/* <Typography variant="h6" >Jun 19, 2020</Typography> */}
          <Divider className={classes.sepratorHr} />
          <Typography variant="h6" classes={{ h6: classes.receiptCnt }} onClick={receiptHandler}>
            {childReceipt ? (
              <FormattedMessage
                id="project.dev.chat-item.receipt.hide"
                defaultMessage="Hide Receipt"
              />
            ) : (
              <FormattedMessage
                id="project.dev.chat-item.receipt.show"
                defaultMessage="Show Receipt"
              />
            )}
          </Typography>
        </div>
      ) : null}

      {/* <div className={classes.dateReceiptCnt}>
          <Divider className={classes.sepratorHr} />
      </div> */}
      <Scrollbars
        style={{ flex: 1 }}
        ref={node => {
          scrollComponent.current = node;
        }}
      >
        <div className={classes.itemsCnt}>
          <ul>
            {allChatList.map((chatItem, i) =>
              chatItem.currentItem == "chatItem" ? (
                <li>
                  <ChatItem
                    id={i}
                    comment={chatItem}
                    chatPermission={chatPermission}
                    chatConfig={chatConfig}
                    replyItemHandler={props.replyItemHandler}
                    replyLaterItemHandler={props.replyLaterItemHandler}
                    replyLaterItemClearHandler={props.replyLaterItemClearHandler}
                    convertToTaskHandler={props.convertToTaskHandler}
                    deleteComment={props.deleteUserComment}
                    showReceipt={showChildReceipt ? childReceipt : parentReceipt}
                    openDeleteDialogue={props.openDeleteDialogue}
                    onDownloadFile={props.onDownloadFile}
                    onRemoveFile={props.onRemoveFile}
                    showSnackBar={props.showSnackBar}
                    saveFileCommentDataHandler={props.saveFileCommentDataHandler}
                    addCommentHandler={props.addCommentHandler}
                    updateComment={props.updateComment}
                    intl={intl}
                  />
                </li>
              ) : chatItem.currentItem == "zoomItem" ? (
                <li>
                  <ZoomItem
                    id={i}
                    comment={chatItem}
                    chatPermission={chatPermission}
                    chatConfig={chatConfig}
                    convertToTaskHandler={props.convertToTaskHandler}
                    deleteComment={(data, success, fail) =>
                      props.deleteComment(data, comment, success, fail)}
                    showReceipt={showChildReceipt ? childReceipt : parentReceipt}
                    openDeleteDialogue={props.openDeleteDialogue}
                    updateComment={props.updateComment}
                  />
                </li>
              ) : (
                <li>
                  <div className={classes.dateItemCnt}>
                    <Typography variant="h6">{chatItem.updatedDate}</Typography>
                    <Divider className={classes.hrLeft} />
                  </div>
                </li>
              )
            )}
          </ul>
        </div>
      </Scrollbars>
    </>
  );
}
const mapStateToProps = state => {
  return {};
};
export default compose(
  connect(mapStateToProps),
  withStyles(Styles, { withTheme: true })
)(ChatItems);
