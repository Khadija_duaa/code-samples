import GradientOverly from "../../../assets/images/svgs/GradientOverly.svg"

const replyItemStyles = (theme) => ({

  commentContentOuterCnt: {
    display: "flex",
    alignItems: "end",
    background: theme.palette.background.items,
    borderBottom: `1px solid ${theme.palette.border.mediumBorder}`,
    // marginBottom: 8,
    paddingTop: 8,
    paddingRight: 8,
    paddingBottom: 11,
    paddingLeft: 8,
    '&:before': {
      top: 0,
      left: 0,
      right: 0,
      width: 5,
      bottom: 0,
      content: "",
      position: 'absolute',
      background: theme.palette.border.brightBlue,
    },
    "&:hover .dropDown": {
      visibility: "visible",
    },
    
  },
  fontSize: {
    fontSize: "13px !important"
  },
  chatItemCnt:{
        width: '100%',
        "&:hover":{
          // boxShadow: `0px 1px 0px ${theme.palette.border.grayLighter}`,
        },
        
    },
    dropDown:{
      visibility: "hidden",
      position: 'absolute',
      right: '-5px',
      top: '-24px',
      // background: theme.palette.common.white,
      // border: `1px solid ${theme.palette.background.contrast}`,
      // boxShadow: '0px 3px 6px #00000029',
      // borderRadius: 4,
    },
    actionIcon:{
      fontSize: "16px !important",
    },
    headerCnt: {
        fontSize: "12px !important",
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
    },
    profileCnt: {
        display: 'flex',
        alignItems: 'center'
    },
    privateStatus: {
      marginLeft: 5,
      color: theme.palette.text.danger,
      fontSize: "11px !important",
      lineHeight: '1.5'
    },
    replyIcon: {
      fontSize: "12px !important",
      marginLeft: 5
    },
    sendTime: {
      color: theme.palette.text.grayDarker,
      fontSize: "10px !important",
      lineHeight: '1.5'
    },
    editedCnt: {
      color: theme.palette.text.grayDarker,
      fontSize: "11px !important",
      marginLeft: 5,
    },
    commentContentCnt: {
        paddingTop: 3,
        fontSize: "13px !important",
        // minWidth: 200,
        wordBreak: "break-all",
        "& p": {
            margin: 0,
            padding: 0,
            lineHeight: 1.5,
             wordBreak: "break-word"
        },
        "& ol": {
            padding: 0,
            margin: 0,
        },
        "& ul": {
            padding: 0,
            margin: 0,
        },
        "& li": {
            padding: 0,
            margin: "0 0 10px 15px",
        },
        "& li:last-child": {
            margin: "0 0 0 15px",
        },
        "& strong": {
            fontWeight: theme.palette.fontWeightMedium,
        },
    },
    fileAttachmentMainCnt:{
      display: 'flex',
      // justifyContent: 'center'
      marginTop: 8
    },
    fileAttachmentCnt: {
      position: 'relative',
      margin: 'auto'
    },
    filePreviewImage: {
      height: 168,
      borderRadius: 6,
      width: 300
    },
    filePathCnt: {
      position: 'absolute',
      bottom: 13,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      width: '100%',
      padding: '0 14px 0 11px'
    },
    fileTypeName:{
      display: 'flex',
      alignItems: 'center'
    },
    seenCnt: {
      display: 'flex',
      alignItems: 'center',
      marginTop: 4,
    },
    seenList: {
      marginLeft: 5,
      color: theme.palette.secondary.medDark,
      fontSize: "11px !important",
    },
    deliveredCnt: {
      display: 'flex',
      alignItems: 'center',
      marginTop: 4,
    },
    deliveredTxt: {
      marginLeft: 5,
      color: theme.palette.secondary.medDark,
      fontSize: "11px !important",
    },
    replyLeftCnt:{
      display: 'flex',
      marginTop: 3
  },
  replyCnt: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 8,
    fontSize: "12px !important"
  },
  replyTxt:{
    marginTop: 3,
    color: theme.palette.text.darkGray,
    '& p':{
        margin: 0,
        padding: 0,
    }
},
compressFilePreview: {
    display: 'flex',
    border: `1px solid ${theme.palette.border.grayLighter}`,
    background: theme.palette.background.grayLightest,
    borderRadius: 6,
    padding: '6px 10px',
    marginTop: 3,
    justifyContent: 'space-between',
    minWidth: 300
},
gradientFileEffect: {
  background: `url(${GradientOverly})`,
  backgroundRepeat: "no-repeat",
  height: 110,
  position: 'absolute',
  width: '100%',
  bottom: 0,
  borderRadius: 6,
},
tooltip: {
  fontSize: "12px !important",
  backgroundColor: theme.palette.common.black,
  '& .chat-tooltip-body': {
    padding: '5px 15px 5px 5px',
      '& ul,li':{
          margin: 0,
          padding: 0,
          listStyleType: 'none',
          color: theme.palette.common.white
      }
    }
  },
});

export default replyItemStyles;