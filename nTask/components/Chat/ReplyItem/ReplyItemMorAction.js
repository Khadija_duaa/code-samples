import React, { Component, Fragment, useState, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
import { compose } from "redux";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../Buttons/IconButton";
import SelectionMenu from "../../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { connect } from "react-redux";
import MoreOptionsIconH from "../../Icons/MoreOptionsIconH";
import SvgIcon from "@material-ui/core/SvgIcon";
import Tooltip from "@material-ui/core/Tooltip";
import ConvertToTaskIcon from "../../Icons/ConvertToTaskIcon";
import DeleteActionIcon from "../../Icons/DeleteActionIcon";
import EditReplyActionIcon from "../../Icons/EditReplyActionIcon";

function ReplyItemMorAction(props) {
  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState("");
  const anchorEl = useRef();

  const handleClick = (event, newPlacement) => {
    const { currentTarget } = event;
    let newOPen = placement !== newPlacement || !open;
    setOpen(newOPen);
    setPlacement(newPlacement);
  };
  const handleClose = event => {
    setOpen(false);
  };
  const onConvertToTaskHandler = comment => {
    props.convertToTaskHandler(
      comment,
      success => {
        props.showSnackBar("Task has been created.");
      },
      fail => {}
    );
  };
  const onDeleteHandler = comment => {
    props.deleteComment(
      comment,
      success => {
        props.showSnackBar("Item has been deleted.");
      },
      fail => {}
    );
  };
  const onEditHandler = commentId => {
    props.editComment(
      comment,
      success => {
        // setOpen(false);
      },
      fail => {}
    );
  };

  const { classes, theme, comment, Id, commentId, chatPermission } = props;
  return (
    <Fragment>
      <div className={`${classes.dropDown} dropDown`}>
        {/* <CustomIconButton
            btnType="condensed"
            onClick={event => {
              handleClick(event, "bottom-end");
            }}
            style={{ padding: "3px 5px"}}
            buttonRef={node => {
              anchorEl.current  = node;
            }}
          >
            <SvgIcon viewBox="0 0 14 3" style={{fontSize: "16px !important", color: theme.palette.text.grayDarker}} >
                <MoreOptionsIconH />
            </SvgIcon>
          </CustomIconButton> */}
        {chatPermission.convertToTask && (
          <CustomIconButton
            btnType="filledWhite"
            onClick={onConvertToTaskHandler.bind(this, comment)}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 8,
            }}
            buttonRef={node => {
              anchorEl.current = node;
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={"Convert To Task"}
              placement="bottom">
              <SvgIcon
                viewBox="0 0 14 11.003"
                htmlColor={theme.palette.secondary.medDark}
                className={classes.actionIcon}>
                <ConvertToTaskIcon />
              </SvgIcon>
            </Tooltip>
          </CustomIconButton>
        )}
        {comment.commentOwner && chatPermission.editComment && (
          <CustomIconButton
            btnType="filledWhite"
            onClick={onEditHandler.bind(this, comment)}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 8,
            }}
            buttonRef={node => {
              anchorEl.current = node;
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={"Edit"}
              placement="bottom">
              <SvgIcon
                viewBox="0 0 16 16.07"
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "16px" }}
                className={classes.actionIcon}>
                <EditReplyActionIcon />
              </SvgIcon>
            </Tooltip>
          </CustomIconButton>
        )}
        {comment.commentOwner && chatPermission.deleteComment && (
          <CustomIconButton
            btnType="filledWhite"
            onClick={onDeleteHandler.bind(this, comment)}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 8,
            }}
            buttonRef={node => {
              anchorEl.current = node;
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={"Delete"}
              placement="bottom">
              <SvgIcon
                viewBox="0 0 16 16"
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "16px" }}
                className={classes.actionIcon}>
                <DeleteActionIcon />
              </SvgIcon>
            </Tooltip>
          </CustomIconButton>
        )}
      </div>

      {/* <SelectionMenu
          open={open}
          closeAction={handleClose}
          placement={placement}
          anchorRef={anchorEl.current}
          list={
            <List>
              <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onEditHandler.bind(this, comment)}
              >
                <ListItemText
                  primary="Edit"
                  classes={{
                    primary: classes.statusItemText
                  }}
                />
              </ListItem>
              {comment.commentOwner && <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onDeleteHandler.bind(this, comment)}
              >
                <ListItemText
                  primary="Delete"
                  classes={{
                    primary: classes.statusItemText
                  }}
                />
              </ListItem>}
              <ListItem
                button
                disableRipple
                classes={{ selected: classes.statusMenuItemSelected }}
                onClick={onConvertToTaskHandler.bind(this, comment)}
              >
                <ListItemText
                  primary="Convert to Task"
                  classes={{
                    primary: classes.statusItemText
                  }}
                />
              </ListItem>
            </List>
          }
        /> */}
    </Fragment>
  );
}
const mapStateToProps = state => {
  return {};
};
export default compose(
  connect(mapStateToProps, {}),
  withStyles(combineStyles(Styles, menuStyles), {
    withTheme: true,
  })
)(ReplyItemMorAction);
