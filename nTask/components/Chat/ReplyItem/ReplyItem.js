import React, { useState, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Styles from "./Styles";
import CustomAvatar from "../../Avatar/Avatar";
import ReactHtmlParser from 'react-html-parser';
import Icon from "../../Icons/Icons";
import Truncate from "react-truncate";
import helper from "../../../helper/index";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Typography from "@material-ui/core/Typography";
import SeenIcon from "../../Icons/SeenIcon";
import DeliveredIcon from "../../Icons/DeliveredIcon";
import ReplyLaterIcon from "../../Icons/ReplyLaterIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import Tooltip from "@material-ui/core/Tooltip";
import ReplyItemMorAction from "./ReplyItemMorAction";
import FileAttachmentMorAction from "./FileAttachmentMorAction";
import PreviewDocFileIcon from "../../Icons/PreviewDocFileIcon";
import PreviewCompressFileIcon from "../../Icons/PreviewCompressFileIcon";
import PreviewAudioVideoFileIcon from "../../Icons/PreviewAudioVideoFileIcon";
import PreviewImageFileIcon from "../../Icons/PreviewImageFileIcon";
import ChatTextEditor from "../../TextEditor/CustomTextEditor/ChatTextEditor/ChatTextEditor";

function ReplyItem(props) {
  const updateToolbarOptions = [["bold", "italic", "underline", "strike"], [{ list: "bullet" }]];

  const [editComment, setEditComment] = useState(null);

  const editCommentHandler = () => {
    setEditComment(comment);
    // success();
  };
  const updateCommentHandler = data => {
    props.updateComment(
      data,
      success => {
        setEditComment(null);
      },
      fail => {
        setEditComment(null);
      }
    );
  };
  const textEditorAction = value => {
    // value === "blur" && setEditComment(null);
  };

  const chatEditorAction = value => {
    value === "blur" && setEditComment(null);
  };

  const { theme, classes, id, comment, chatConfig, chatPermission, showReceipt, intl } = props;
  let text = comment.commentText;
  if (comment.isEdited)
    text =
      comment.commentText.substr(0, comment.commentText.lastIndexOf("</p>")) +
      `<span class=${classes.editedCnt}>- Edited</span></p>`;
  let style = chatConfig["contextChat"] == "group" ? { marginLeft: 5 } : { marginRight: 5 };

  return (
    <Fragment>
      {editComment && comment.commentId == editComment.commentId ? (
        <div>
          <ChatTextEditor
            chatConfig={chatConfig}
            chatPermission={chatPermission}
            id={"replyToolbar" + comment.commentId}
            showSnackBar={props.showSnackBar}
            saveFileCommentData={() => {}}
            addComments={() => {}}
            replyItem={null}
            removeReply={() => {}}
            visibleUserDropDown={false}
            updateToolbarOptions={updateToolbarOptions}
            chatData={editComment.commentText}
            editComment={editComment}
            updateComment={data => updateCommentHandler(data)}
            inlineEditor={true}
            textEditorAction={e => textEditorAction(e)}
            chatEditorAction={e => chatEditorAction(e)}
            intl={intl}
          />
        </div>
      ) : (
        <div
          key={id}
          style={{
            flexDirection: chatConfig.contextChat == "group" ? "row-reverse" : "row",
            borderLeft:
              comment.user == "self" ? `3px solid ${theme.palette.border.greenBorder}` : 0,
          }}
          className={classes.commentContentOuterCnt}>
          {comment.user === "self" ? (
            <CustomAvatar styles={style} personal size="small" />
          ) : (
            <CustomAvatar
              styles={{ marginRight: 5 }}
              otherMember={{
                imageUrl: comment.avatar,
                fullName: comment.fullName,
                lastName: "",
                email: comment.email,
                isOnline: comment.isOnline,
              }}
              size="small"
            />
          )}
          <div className={classes.chatItemCnt}>
            <div className={classes.headerCnt}>
              <span className={classes.profileCnt}>
                <Typography
                  variant="h6"
                  style={{ fontWeight: theme.typography.fontWeightLarge, lineHeight: "1.5" }}>
                  {comment.fullName}
                </Typography>
                {comment.privateComment && (
                  <Typography variant="h6" classes={{ h6: classes.privateStatus }}>
                    (Private)
                  </Typography>
                )}
                {comment.replyLater && (
                  <SvgIcon className={classes.replyIcon} viewBox="0 0 12 12">
                    <ReplyLaterIcon />
                  </SvgIcon>
                )}
              </span>
              <Typography variant="h6" classes={{ h6: classes.sendTime }}>
                {comment.createdTime}
              </Typography>
              <ReplyItemMorAction
                comment={comment}
                convertToTaskHandler={props.convertToTaskHandler}
                deleteComment={props.deleteComment}
                editComment={(data, success, fail) => editCommentHandler(data, success, fail)}
                chatPermission={chatPermission}
                showSnackBar={props.showSnackBar}
              />
            </div>
            <div className={classes.commentContentCnt}>
              {ReactHtmlParser(text)}
              {comment.attachment ? (
                <div className={classes.fileAttachmentMainCnt}>
                  <div className={classes.fileAttachmentCnt}>
                    {comment.iconCategory == helper.COMPRESS_FORMAT ? (
                      <Fragment>
                        <div className={classes.compressFilePreview}>
                          <span style={{ display: "flex", alignItems: "center" }}>
                            <SvgIcon
                              viewBox="0 0 32 32"
                              style={{ fontSize: "32px", color: theme.palette.secondary.medDark }}>
                              <PreviewCompressFileIcon />
                            </SvgIcon>
                            <span style={{ paddingLeft: 8 }}>
                              <Typography variant="h6">{comment.fileName}</Typography>
                              <Typography
                                variant="h6"
                                style={{ color: theme.palette.background.darkGray }}>
                                {comment.fileSize}
                              </Typography>
                            </span>
                          </span>
                          <FileAttachmentMorAction
                            onDownloadFile={props.onDownloadFile}
                            onRemoveFile={props.onRemoveFile}
                            comment={comment}
                            showSnackBar={props.showSnackBar}
                          />
                        </div>
                      </Fragment>
                    ) : (
                      <Fragment>
                        {comment.fileUrlPath ? (
                          <img src={comment.fileUrlPath} className={classes.filePreviewImage} />
                        ) : (
                          <div className={classes.filePreviewImage}>
                            {/* <SvgIcon
                                            viewBox="0 0 32 32"
                                            style={{
                                                fontSize: "18px !important",
                                                color: theme.palette.common.white,
                                            }}
                                            >
                                            <PreviewImageFileIcon />
                                        </SvgIcon> */}
                          </div>
                        )}
                        <div className={classes.gradientFileEffect} />
                        <div className={classes.filePathCnt}>
                          <span className={classes.fileTypeName}>
                            <SvgIcon
                              viewBox="0 0 32 32"
                              style={{ fontSize: "18px", color: theme.palette.common.white }}>
                              {comment.iconCategory == helper.IMAGE_FORMAT ? (
                                <PreviewImageFileIcon />
                              ) : comment.iconCategory == helper.DOC_FORMAT ? (
                                <PreviewDocFileIcon />
                              ) : comment.iconCategory == helper.AUDIO_VIDEO_FORMAT ? (
                                <PreviewAudioVideoFileIcon />
                              ) : comment.iconCategory == helper.COMPRESS_FORMAT ? (
                                <PreviewCompressFileIcon />
                              ) : (
                                <PreviewDocFileIcon />
                              )}
                            </SvgIcon>
                            <Typography
                              variant="h6"
                              className= {classes.fontSize}
                              style={{
                                lineHeight: "1.5",
                                color: theme.palette.common.white,
                                paddingLeft: 8,
                                fontSize: "13px",
                              }}>
                              {comment.fileName}
                            </Typography>
                          </span>
                          <FileAttachmentMorAction
                            onDownloadFile={props.onDownloadFile}
                            onRemoveFile={props.onRemoveFile}
                            comment={comment}
                            iconColor={theme.palette.common.white}
                            showSnackBar={props.showSnackBar}
                          />
                        </div>
                      </Fragment>
                    )}
                  </div>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile,
  };
};
export default compose(
  connect(mapStateToProps),
  withStyles(Styles, { withTheme: true })
)(ReplyItem);
