const chatBuilderStyles = (theme) => ({
  detailsDiv: {
    // width: "70%", 
    display: 'flex',
    flexDirection: 'column',
    background: theme.palette.background.airy,
    // borderLeft: `1px solid ${theme.palette.background.contrast}`,
    flex: 1
    // width: '45%',
  },
  TabsRoot: {
    background: theme.palette.background.airy,
    // borderBottom: `1px solid #F6F6F6`,
    boxShadow: `0px 1px 0px ${theme.palette.background.grayLighter}`,
    padding: "0 20px",
    minHeight: 40,
  },
  tabIndicator: {
    background: theme.palette.border.blue,
    borderRadius: "10px 10px 0px 0px",
    height: 3,
  },
  tab: {
    minWidth: "unset",
    flexGrow: 1,
  },
  tabLabel: {
    fontSize: "13px !important",
  },
  tabLabelCnt: {
    padding: 0,
    fontSize: "12px !important",
    textTransform: "capitalize",
    "& $h6": {
      fontWeight: theme.typography.fontWeightMedium,
    },
    // fontWeight: theme.typography.fontWeightRegular,
  },
  tabSelected: {
    boxShadow: "none",
    background: "none",
    color: theme.palette.border.blue,
    "& $h6": {
      color: theme.palette.border.blue,
    },
  },
  commentsTabCnt: {
    display: "flex",
    flexDirection: "column",
    // height: "calc(100% - 80px)",
    flex: 1,
  },


  toggleBtnGroup: {
    // textAlign: "center",
    borderRadius: 4,
    background: "#f6f6f6",
    border: "none",
    boxShadow: "none",
    // padding: 10,
    flex: 1,
    "& $toggleButtonSelected": {
      backgroundColor: theme.palette.border.blue,
      border: `1px solid ${theme.palette.border.blue}`,
      "& $planTypeLbl": {
        color: theme.palette.common.white,
      },
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.border.blue,
        // color: theme.palette.common.black
      },
    },
  },
  toggleButton: {
    border: `1px solid #F6F6F6`,
    borderRadius: 4,
    height: 32,
    fontSize: "12px !important",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    textTransform: "capitalize",
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    background: "#F6F6F6",
    padding: "7px 20px 6px 20px",
  },
  toggleButtonSelected: {},
  toggleButtonMainDiv: {
    position: "relative",
    alignItems: "center",
    display: "flex",
  },
  planTypeLbl: {
    display: "flex",
    fontSize: "13px !important",
    // fontWeight: 700,
    color: theme.palette.text.primary,
    whiteSpace: "nowrap",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  toggleBtnCnt: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  dateReceiptCnt: {
    display: 'flex',
    fontSize: "12px !important",
    alignItems: 'center',
    padding: '8px 16px 8px 14px',
    cursror: 'pointer',
  },
  receiptCnt: {
    color: theme.palette.text.azure,
    cursor: 'pointer',
    textDecoration: 'underline'
  },
});

export default chatBuilderStyles;