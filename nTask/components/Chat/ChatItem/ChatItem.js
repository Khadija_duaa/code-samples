import React, { useState, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import ReactHtmlParser from 'react-html-parser';
import Truncate from "react-truncate";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Typography from "@material-ui/core/Typography";
import SvgIcon from "@material-ui/core/SvgIcon";
import Tooltip from "@material-ui/core/Tooltip";
import isEmpty from "lodash/isEmpty";
import { FormattedMessage } from "react-intl";
import clsx from "clsx";
import Styles from "./Styles";
import CustomAvatar from "../../Avatar/Avatar";
import Icon from "../../Icons/Icons";
import helper from "../../../helper/index";
import SeenIcon from "../../Icons/SeenIcon";
import DeliveredIcon from "../../Icons/DeliveredIcon";
import ReplyLaterIcon from "../../Icons/ReplyLaterIcon";
import ChatItemMorAction from "./ChatItemMoreAction";
import FileAttachmentMorAction from "./FileAttachmentMoreAction";
import ReplyThinIcon from "../../Icons/ReplyThinIcon";
import PreviewDocFileIcon from "../../Icons/PreviewDocFileIcon";
import CaretIcon from "../../Icons/CaretIcon";
import PreviewCompressFileIcon from "../../Icons/PreviewCompressFileIcon";
import PreviewAudioVideoFileIcon from "../../Icons/PreviewAudioVideoFileIcon";
import PreviewImageFileIcon from "../../Icons/PreviewImageFileIcon";
import ReplyItem from "../ReplyItem/ReplyItem";
import ChatTextEditor from "../../TextEditor/CustomTextEditor/ChatTextEditor/ChatTextEditor";

function ChatItem(props) {
  const [reply, setReply] = useState(false);
  const [expand, setExpand] = useState(false);
  const [editComment, setEditComment] = useState(null);

  const updateToolbarOptions = [["bold", "italic", "underline", "strike"], [{ list: "bullet" }]];

  const addEditCommentHandler = (data, comment, success, fail) => {
    setEditComment(null);
    props.addCommentHandler(data, comment, success, fail);
  };
  const addReplyCommentHandler = (data, comment, success, fail) => {
    setReply(null);
    props.addCommentHandler(data, comment, success, fail);
  };
  const saveFileCommentDataHandler = (data, comment, success, fail) => {
    props.saveFileCommentDataHandler(data, comment, success, fail);
  };

  const editCommentHandler = (comment, success, fail) => {
    setEditComment(comment);
    success();
  };

  const textEditorAction = value => {
    value === "blur" && setEditComment(null);
  };

  const chatEditorAction = value => {
    value === "blur" && setEditComment(null);
  };
  const replyTextEditorAction = value => {
    value === "blur" && setReply(false);
  };

  const replyChatEditorAction = value => {
    value === "blur" && setReply(false);
  };

  const updateCommentHandler = (data, success, fail) => {
    props.updateComment(
      data,
      result => {
        setEditComment(null);
        success(result);
      },
      error => {
        setEditComment(null);
        fail(error);
      }
    );
  };
  const getCollapseReplyText = () => {
    const { comment } = props;
    const loginUserId = props.profileState.data.userId;
    const haveReplyItems = !isEmpty(comment.children);
    let replyItemsLength;
    let userNames;
    if(haveReplyItems){
      // To check length of reply items
      replyItemsLength = haveReplyItems ? Object.values(comment.children).length : 0;
      //Get lenght of other replies
      const repliesUsers = Object.values(comment.children).map(item => { return {commentCreatedBy: item.commentCreatedBy, fullName: item.fullName} }).sort((a,b) => { if(a.commentCreatedBy == loginUserId) return -1 ; return 1});
      // it is used to unique user by id and sort login user first of array element
      const distinctRUsers = [...new Map(repliesUsers.map(item => [item['commentCreatedBy'], item])).values()].map((item,index) => { if(item.commentCreatedBy == loginUserId) return 'you'; else return item.fullName; });
      // To get all user full name and append 'and' before last user full name
      userNames =  [distinctRUsers.slice(0, -1).join(', '), distinctRUsers.slice(-1)[0]].join(distinctRUsers.length < 2 ? '' : ' and ');
    }
    
    let replyTxt = haveReplyItems
      ? replyItemsLength + ' ' +
          (replyItemsLength > 1 ? 'replies' : 'reply' )
            + ` from ${userNames}`
      : "";
    return replyTxt;
  }

  const { theme, classes, id, comment, chatConfig, chatPermission, showReceipt, intl } = props;
  const loginUserfullName = props.profileState.data.fullName;
  let text = comment.commentText;
  if (comment.isEdited)
    text = `${comment.commentText.substr(0, comment.commentText.lastIndexOf("</p>"))}<span class=${
      classes.editedCnt
    }>- Edited</span></p>`;
  const style = chatConfig.contextChat == "group" ? { marginLeft: 5 } : { marginRight: 5 };
  // To check reply itesm exist or not
  const haveReplyItems = !isEmpty(comment.children);
  // To check either show collapse/expand button or not
  const showExpandDialog = !!(haveReplyItems && Object.values(comment.children).length > 1);
  // // To check length of reply items
  // const replyItemsLength = haveReplyItems ? Object.values(comment.children).length : 0;
  // //Get lenght of other replies
  // const otherRepliesUsers = Object.values(comment.children).filter(item => item.fullName != loginUserfullName)
  // //All self reply items or have any other reply
  // const othersReply = otherRepliesUsers.length > 0 ? true: false;
  // // To get last other reply item full name
  // const lastChatItemName = othersReply ? otherRepliesUsers[otherRepliesUsers.length - 1].fullName : "";
  // To show the text in collapse/expand span
  const collapseText = getCollapseReplyText();
  // const collapseText = haveReplyItems
  //   ? `${replyItemsLength + ' ' +
  //       (replyItemsLength > 1
  //         ? `${intl.formatMessage({
  //             id: "project.dev.chat-item.replies.label",
  //             defaultMessage: "replies",
  //           })}`
  //         : `${intl.formatMessage({
  //             id: "project.dev.chat-item.reply-button.label",
  //             defaultMessage: "reply",
  //           })}`)}` +

  //           (othersReply ?
  //           `${' '}${intl.formatMessage({
  //             id: "project.dev.chat-item.replies.from",
  //             defaultMessage: "from You and",
  //           })} ${' '}${lastChatItemName}`
  //           :' from You')
  //   : "";
  // if(!comment.id){
  //   return <div>{ReactHtmlParser(comment.text)}</div>
  // }
  return (
    <div
      key={id}
      style={{
        flexDirection: chatConfig.contextChat == "group" ? "row-reverse" : "row",
      }}
      className={clsx({ [classes.chatItemCnt]: true, [classes.chatItemCntDisabled]: !comment.Id })}
    >
      {comment.user === "self" ? (
        <CustomAvatar styles={style} personal size="small" />
      ) : (
        <CustomAvatar
          styles={{ marginRight: 5 }}
          otherMember={{
            imageUrl: comment.avatar,
            fullName: comment.fullName,
            lastName: "",
            email: comment.email,
            isOnline: comment.isOnline,
          }}
          size="small"
        />
      )}
      <div className={classes.commentContentOuterCnt}>
        {editComment && comment.commentId == editComment.commentId ? (
          /* This editor is used when user edit any chat item */
          <div>
            <ChatTextEditor
              chatConfig={chatConfig}
              chatPermission={chatPermission}
              showSnackBar={props.showSnackBar}
              id="chatItemToolbar"
              saveFileCommentData={(data, success, fail) =>
                saveFileCommentDataHandler(data, comment, success, fail)}
              addComments={(data, success, fail) =>
                addEditCommentHandler(
                  data,
                  null,
                  () => {},
                  () => {}
                )}
              replyItem={null}
              removeReply={() => {}}
              visibleUserDropDown={false}
              updateToolbarOptions={updateToolbarOptions}
              chatData={editComment.commentText}
              editComment={editComment}
              updateComment={updateCommentHandler}
              inlineEditor
              textEditorAction={e => textEditorAction(e)}
              chatEditorAction={e => chatEditorAction(e)}
              intl={intl}
            />
          </div>
        ) : (
          <>
            <div className={classes.commnetContentInnerCnt}>
              <div className={classes.headerCnt}>
                <span className={classes.profileCnt}>
                  <Typography
                    variant="h6"
                    style={{
                      fontWeight: theme.typography.fontWeightLarge,
                      lineHeight: "1.5",
                    }}
                  >
                    {comment.fullName}
                  </Typography>

                  {chatPermission.replyLater && comment.replyLater && (
                    <SvgIcon className={classes.replyLaterIcon} viewBox="0 0 12 12">
                      <ReplyLaterIcon />
                    </SvgIcon>
                  )}
                </span>
                <Typography variant="h6" classes={{ h6: classes.sendTime }}>
                  {comment.createdTime}
                </Typography>
                <ChatItemMorAction
                  comment={comment}
                  showSnackBar={props.showSnackBar}
                  // replyItemHandler = {props.replyItemHandler}
                  replyLaterItemHandler={props.replyLaterItemHandler}
                  replyLaterItemClearHandler={props.replyLaterItemClearHandler}
                  convertToTaskHandler={props.convertToTaskHandler}
                  deleteComment={(data, success, fail) => props.deleteComment(data, null, success, fail)}
                  editComment={(data, success, fail) => editCommentHandler(data, success, fail)}
                  chatPermission={chatPermission}
                />
              </div>
              <div className={classes.commentContentCnt}>
                {ReactHtmlParser(text)}
                {comment.attachment ? (
                  <div className={classes.fileAttachmentMainCnt}>
                    <div className={classes.fileAttachmentCnt}>
                      {comment.iconCategory == helper.COMPRESS_FORMAT ? (
                        <>
                          <div className={classes.compressFilePreview}>
                            <span style={{ display: "flex", alignItems: "center" }}>
                              <SvgIcon
                                viewBox="0 0 32 32"
                                style={{
                                  fontSize: "32px !important",
                                  color: theme.palette.secondary.medDark,
                                }}
                              >
                                <PreviewCompressFileIcon />
                              </SvgIcon>
                              <span style={{ paddingLeft: 8 }}>
                                <Typography variant="h6">{comment.fileName}</Typography>
                                <Typography
                                  variant="h6"
                                  style={{
                                    color: theme.palette.background.darkGray,
                                  }}
                                >
                                  {comment.fileSize}
                                </Typography>
                              </span>
                            </span>
                            <FileAttachmentMorAction
                              onDownloadFile={props.onDownloadFile}
                              onRemoveFile={(data, success, fail) =>
                                props.onRemoveFile(data, null, success, fail)}
                              comment={comment}
                              showSnackBar={props.showSnackBar}
                              chatPermission={chatPermission}
                            />
                          </div>
                        </>
                      ) : (
                        <>
                          {comment.fileUrlPath ? (
                            <img src={comment.fileUrlPath} className={classes.filePreviewImage} />
                          ) : (
                            <div className={classes.filePreviewImage}>
                              {/* <SvgIcon
                                  viewBox="0 0 32 32"
                                  style={{
                                    fontSize: "18px !important",
                                    color: theme.palette.common.white,
                                  }}
                                >
                                  <PreviewImageFileIcon />
                                </SvgIcon> */}
                            </div>
                          )}
                          <div className={classes.gradientFileEffect} />
                          <div className={classes.filePathCnt}>
                            <span className={classes.fileTypeName}>
                              <SvgIcon
                                viewBox="0 0 32 32"
                                style={{
                                  fontSize: "18px !important",
                                  color: theme.palette.common.white,
                                }}
                              >
                                {comment.iconCategory == helper.IMAGE_FORMAT ? (
                                  <PreviewImageFileIcon />
                                ) : comment.iconCategory == helper.DOC_FORMAT ? (
                                  <PreviewDocFileIcon />
                                ) : comment.iconCategory == helper.AUDIO_VIDEO_FORMAT ? (
                                  <PreviewAudioVideoFileIcon />
                                ) : comment.iconCategory == helper.COMPRESS_FORMAT ? (
                                  <PreviewCompressFileIcon />
                                ) : (
                                  <PreviewDocFileIcon />
                                )}
                              </SvgIcon>
                              <Typography
                                variant="h6"
                                style={{
                                  lineHeight: "1.5",
                                  color: theme.palette.common.white,
                                  paddingLeft: 8,
                                  fontSize: "13px !important",
                                }}
                              >
                                {comment.fileName}
                              </Typography>
                            </span>
                            <FileAttachmentMorAction
                              onDownloadFile={props.onDownloadFile}
                              onRemoveFile={(data, success, fail) =>
                                props.onRemoveFile(data, null, success, fail)}
                              comment={comment}
                              iconColor={theme.palette.common.white}
                              showSnackBar={props.showSnackBar}
                              chatPermission={chatPermission}
                            />
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                ) : null}
              </div>
            </div>
            {" "}
          </>
        )}
        {showExpandDialog ? (
          <>
            {haveReplyItems && (
              <div
                className={classes.itemsShowCnt}
                onClick={() => {
                  setExpand(prevState => !prevState);
                }}
              >
                {expand ? (
                  <span className={classes.expandCollapseMainCnt}>
                    <SvgIcon viewBox="0 0 10.006 5.624" className={classes.expandIconCnt}>
                      <CaretIcon />
                    </SvgIcon>
                    <Typography variant="h6" classes={{ h6: classes.expColTextCnt }}>
                      <FormattedMessage
                        id="project.dev.chat-item.collapse-all.label"
                        defaultMessage="Collapse all"
                      />
                    </Typography>
                  </span>
                ) : (
                  <span className={classes.expandCollapseMainCnt}>
                    <SvgIcon viewBox="0 0 10.006 5.624" className={classes.collapseIconCnt}>
                      <CaretIcon />
                    </SvgIcon>
                    <Typography variant="h6" classes={{ h6: classes.expColTextCnt }}>
                      {collapseText}
                    </Typography>
                  </span>
                )}
              </div>
            )}
            {isEmpty(comment.children)
              ? null
              : expand &&
                Object.values(comment.children).map((replyChatItem, index) => (
                  <ReplyItem
                    id={index}
                    comment={replyChatItem}
                    chatPermission={chatPermission}
                    chatConfig={chatConfig}
                    convertToTaskHandler={props.convertToTaskHandler}
                    deleteComment={(data, success, fail) =>
                      props.deleteComment(data, comment, success, fail)}
                    showReceipt={showReceipt}
                    openDeleteDialogue={props.openDeleteDialogue}
                    onDownloadFile={props.onDownloadFile}
                    onRemoveFile={(data, success, fail) =>
                      props.onRemoveFile(data, comment, success, fail)}
                    updateComment={props.updateComment}
                    intl={intl}
                    showSnackBar={props.showSnackBar}
                    />
                ))}
          </>
        ) : (
          Object.values(comment.children).map((replyChatItem, index) => (
            <ReplyItem
              id={index}
              comment={replyChatItem}
              chatPermission={chatPermission}
              chatConfig={chatConfig}
              convertToTaskHandler={props.convertToTaskHandler}
              deleteComment={(data, success, fail) =>
                props.deleteComment(data, comment, success, fail)}
              showReceipt={showReceipt}
              openDeleteDialogue={props.openDeleteDialogue}
              onDownloadFile={props.onDownloadFile}
              onRemoveFile={(data, success, fail) =>
                props.onRemoveFile(data, comment, success, fail)}
              updateComment={props.updateComment}
              intl={intl}
              showSnackBar={props.showSnackBar}
              />
          ))
        )}
        {/* 
          This editor is use, when user reply to any chat Item
        */}
        {reply ? (
          <div>
            <ChatTextEditor
              chatConfig={chatConfig}
              id={`replyCustomToolbar${comment.commentId}`}
              chatPermission={chatPermission}
              showSnackBar={props.showSnackBar}
              saveFileCommentData={(data, success, fail) =>
                saveFileCommentDataHandler(data, comment, success, fail)}
              addComments={(data, success, fail) =>
                addReplyCommentHandler(data, comment, success, fail)}
              replyItem={null}
              removeReply={() => {}}
              visibleUserDropDown={false}
              updateToolbarOptions={updateToolbarOptions}
              inlineEditor
              textEditorAction={e => replyTextEditorAction(e)}
              chatEditorAction={e => replyChatEditorAction(e)}
              intl={intl}
            />
          </div>
        ) : (
          chatPermission.reply && (
            <div
              className={classes.replyMainCnt}
              onClick={() => {
                setReply(prevState => !prevState);
              }}
            >
              <SvgIcon className={classes.replyIcon} viewBox="0 0 12.013 8.76">
                <ReplyThinIcon />
              </SvgIcon>
              <span className={classes.replyTextCnt}>
                <Typography
                  variant="h6"
                  classes={{
                    h6: classes.replyTxt,
                  }}
                >
                  <FormattedMessage
                    id="project.dev.chat-item.reply-button.label"
                    defaultMessage="Reply"
                  />
                </Typography>
              </span>
            </div>
          )
        )}
        {showReceipt ? (
          comment.delivered ? (
            <div className={classes.showReceiptCnt}>
              <div className={classes.deliveredCnt}>
                <SvgIcon
                  style={{ width: 12, height: 10 }}
                  viewBox="0 0 12 10.569"
                  htmlColor={theme.palette.secondary.dark}
                >
                  <DeliveredIcon />
                </SvgIcon>
                <span className={classes.deliveredTxt}>Delivered</span>
              </div>
            </div>
          ) : (
            <div className={classes.showReceiptCnt}>
              <div className={classes.seenCnt}>
                <SvgIcon
                  style={{ width: 14, height: 9 }}
                  viewBox="0 0 14.018 9.169"
                  htmlColor={theme.palette.secondary.dark}
                >
                  <SeenIcon />
                </SvgIcon>
                <Tooltip
                  classes={{
                    tooltip: classes.tooltip,
                  }}
                  title={(
                    <div className="chat-tooltip-body">
                      <ul>
                        {comment.messageSeenBy.map((pStrength, i) => {
                          return (
                            <li key={i}>
                              <small>{pStrength}</small>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  )}
                  placement="bottom"
                >
                  <span className={classes.seenList}>
                    {comment.messageSeenBy[0]} 
                    {' '}
                    {comment.messageSeenBy.length > 1 ? " & others" : ""}
                  </span>
                </Tooltip>
              </div>
            </div>
          )
        ) : null}
      </div>
    </div>
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile,
  };
};
export default compose(connect(mapStateToProps), withStyles(Styles, { withTheme: true }))(ChatItem);
