const topBarStyles = (theme) => ({
   topNotificationBar: {
      background: theme.palette.background.blue,
      padding: "0 20px",
      position: "fixed",
      minHeight: 50,
      top: 0,
      left: 0,
      right: 0,
      zIndex: 11111,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      overflow:'hidden',
      "& > p": {
         color: theme.palette.common.white,
         fontSize: 12,
         marginRight: 10
      }
   },
   leftImage: {
      position: "absolute",
      left: 27,
      bottom: 0
   },
   newYearLogo: {
      position: "absolute",
      left: 40,
      bottom: 0
   },
   rightImage: {
      position: "absolute",
      right: 50,
      bottom: 0
   },
   headerGiftImage: {
      position: "absolute",
      left: '-40px;',
      bottom: 0
   },
   headerTreeImage: {
      position: "absolute",
      right: -29,
      bottom: 0
   },
   headerSnowImage: {
      position: "absolute",
      left: 0,
      bottom: 0
   },
   headerIcemanImage: {
      position: "absolute",
      right: 145,
      bottom: 0,
      top: -40,
      width: 134
   },
   headerBabaImage: {
      position: "absolute",
      left: 135,
      bottom: 0,
      width: '134px;',
      top: -40
   },
   headerCarImage: {
      position: "absolute",
      right: 145,
      bottom: 0
   }
})

export default topBarStyles