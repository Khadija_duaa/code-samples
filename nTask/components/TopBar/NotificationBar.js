import React, { useEffect, useState } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import headerStyles from "./styles";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { notificationBarState } from "../../redux/actions/notificationBar";
import topBarStyles from "./styles";
import { compose } from "redux";
import CustomButton from "../Buttons/CustomButton";
import CustomIconButton from "../Buttons/CustomIconButton";
import CloseIcon from "@material-ui/icons/Close";
import { showHideNotificationBar } from "../../redux/actions/notificationBar";

function NotificationBar(props) {
  const { classes, theme, notification, onClose } = props;

  // Closing the topbar when user click on cross icon
  const hideNotifBar = () => {
    props.showHideNotificationBar("");
    notification.closeAction();
  };
  return (
    <div
      className={classes.topNotificationBar}
      style={{ background: theme.palette.background.blue }}
      id="topNotifBar">
      <p style={{ fontSize: "14px", zIndex: 111 }}>{notification.message}</p>
      <CustomButton
        // onClick={this.handleCreateProfileClick}
        btnType="white"
        variant="contained"
        style={{
          marginLeft: 10,
          color: theme.palette.text.azure,
        }}
        onClick={notification.primaryAction}>
        {notification.buttonText}
      </CustomButton>
      {notification.secondaryAction && (
        <CustomButton
          // onClick={this.handleCreateProfileClick}
          btnType="white"
          variant="contained"
          style={{
            marginLeft: 10,
            color: theme.palette.text.azure,
          }}
          onClick={notification.secondaryAction}>
          {notification.secondaryButtonText}
        </CustomButton>
      )}
      {notification.closeAction ? (
        <CustomIconButton
          onClick={hideNotifBar}
          btnType="filledWhite"
          style={{
            background: theme.palette.common.white,
            padding: 4,
            position: "absolute",
            right: 20,
          }}>
          <CloseIcon htmlColor={theme.palette.secondary.medDark} style={{ fontSize: 14 }} />
        </CustomIconButton>
      ) : null}
    </div>
  );
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    notification: state.notification,
    subscriptionDetails: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    ).subscriptionDetails,
    isOwner: state.profile.data.teamMember.find(item => item.userId == state.profile.data.userId)
      .isOwner,
  };
};

export default compose(
  withStyles(topBarStyles, { withTheme: true }),
  connect(mapStateToProps, { notificationBarState, showHideNotificationBar })
)(NotificationBar);
