import withStyles from "@material-ui/core/styles/withStyles";
import moment from "moment-timezone";
import { withSnackbar } from "notistack";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { showHidePromoBar } from "../../redux/actions/promoBar";
import { UpdatePromoCodeValue } from "../../redux/actions/userBillPlan";
import PaymentPlan from "../../Views/billing/PromoPlan/PaymentPlan";
import CustomButton from "../Buttons/CustomButton";
import { DisplayPlanName, isPlanExpire, isPlanFree, isPlanTrial } from "../constants/planConstant";
import topBarStyles from "./styles";

function PromoBar(props) {
  const {
    UpdatePromoCodeValue,
    enqueueSnackbar,
    classes,
    theme,

    showHidePromoBar,
    promoBar,
    isOwner,
    activeTeam,
  } = props;

  const monthlyPrice = activeTeam.paymentPlan.unitPriceAnually;
  const planTitle = DisplayPlanName(activeTeam.paymentPlan.planTitle);
  const { currentPeriondEndDate } = activeTeam.subscriptionDetails;
  const daysRemaining = moment(currentPeriondEndDate).diff(new Date(), "days");

  const [isPaymentPlan, setPaymentPlan] = useState(false);

  const openPromoPopUp = (code) => {
    UpdatePromoCodeValue(code, () => {
      setPaymentPlan(true);
    });
  };

  const closePlanPopUp = () => {
    setPaymentPlan(false);
  };

  const showSnackBar = (snackBarMessage, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };

  useEffect(() => {
    const { paymentPlanTitle } = activeTeam.subscriptionDetails;

    const promoBarSeen = localStorage.getItem("holiday2021");
    if (
      !promoBarSeen &&
      isPlanTrial(paymentPlanTitle) &&
      !isPlanExpire(paymentPlanTitle) &&
      isOwner
    ) {
      // if (!promoBarSeen) {
      showHidePromoBar(true);
    } else {
      showHidePromoBar(false);
    }
  }, [promoBar, activeTeam]);

  if (!promoBar) return <></>;

  return (
    <div className={classes.topNotificationBar} id="topNotifBar">
      {isPaymentPlan && (
        <PaymentPlan showSnackBar={showSnackBar} closeTrialPopUp={closePlanPopUp}
        // showBilling={true}
        />
      )}
      <p style={{ fontSize: "16px", zIndex: 111, fontFamily: theme.typography.fontFamilyLato }}>
        You have {daysRemaining} days to try nTask {planTitle} Plan. Upgrade anytime for as low as $
        {monthlyPrice} USD/month.
      </p>
      <CustomButton
        btnType="white"
        variant="contained"
        style={{
          marginLeft: 10,
        }}
        onClick={() => openPromoPopUp("")}>
        Upgrade Now
      </CustomButton>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    promoBar: state.promoBar.promoBar,
    activeTeam: state.profile.data.teams.find((t) => t.companyId == state.profile.data.activeTeam),


    isOwner: state.profile.data.teamMember.find((item) => item.userId == state.profile.data.userId)
      .isOwner,
  };
};

export default compose(
  withSnackbar,
  withStyles(topBarStyles, { withTheme: true }),
  connect(mapStateToProps, { showHidePromoBar, UpdatePromoCodeValue })
)(PromoBar);
