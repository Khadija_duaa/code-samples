import React, { Component } from "react";
import securityHeaders from "../../redux/securityHeaders/index";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import { validateAuth, isWorkspaceExist } from "../../redux/actions/authentication";
import { AcceptTeamInvitation, AcceptWorkspaceInvitation } from "../../redux/actions/workspace";
import helper from "../../helper/index";
import queryString from "query-string";

class InvitedUser extends Component {
  checkAuth = () => {
    if (
      (helper.getToken() === null ||
        helper.getToken() === undefined) &&
      !securityHeaders.GETHEADERS().headers.headers.Authorization
    )
      return false;
    else return true;
  };


  addToWorkSpace = (teamID) => {
    let searchQuery = window.location.href;
    if(queryString.parseUrl(searchQuery).query.token){
    this.props.AcceptTeamInvitation(teamID, data => {
      if (data && data.status === 200) {
        sessionStorage.removeItem('teamID');
        this.props.history.replace("/tasks");
        //this.processRedirect(teamID);
      }
      else {
        this.props.history.replace("/account/login")
      }
    }, () => {
      this.props.history.push("/tasks")
     });
  } else{
    this.props.AcceptWorkspaceInvitation(teamID, data => {
      if (data && data.status === 200) {
        sessionStorage.removeItem('teamID');
        this.props.history.replace("/tasks");
        //this.processRedirect(teamID);
      }
      else {
        this.props.history.replace("/account/login")
      }
    }, () => { 
      this.props.history.push("/tasks")
    });
  }
  }

  processRedirect = (teamID) => {

    if (this.checkAuth()) {
      this.props.validateAuth(data => {
        if (!data)
          return;
        if (data && data.status === 200) {
          this.addToWorkSpace(teamID);
        } else {
          sessionStorage.setItem("teamID", teamID);
          this.props.history.replace("/account/login")
        }
      });
    } else {
      sessionStorage.setItem("teamID", teamID);
      this.props.history.replace("/account/login")
    }
  }
  componentDidMount = () => {
    let searchQuery = window.location.href; /** getting  url */
    let teamId = queryString.parseUrl(searchQuery).query.teamId ? queryString.parseUrl(searchQuery).query.teamId : queryString.parseUrl(searchQuery).query.token; /** Parsing the url and extracting the team id using query string*/

    // let teamID = helper.HELPER_GETPARAMS(window.location.href)
    //   ? helper.HELPER_GETPARAMS(window.location.href).teamId : undefined;
    if (teamId) {//              
      this.processRedirect(teamId);
    }
    else
      this.props.history.replace("/account/login")
  };

  render() {
    return <React.Fragment></React.Fragment>;
  }
}

export default compose(
  withRouter,
  connect(
    null,
    {
      validateAuth,
      isWorkspaceExist,
      AcceptTeamInvitation,
      AcceptWorkspaceInvitation
    }
  )
)(InvitedUser);
