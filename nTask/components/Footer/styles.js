const sideBarWidth = 221; //Width of sidebar when open
const sideBarCloseWidth = 0; // wi
const footerStyles = theme => ({
  footerCnt: {
    height: 44,
    position: "fixed",
    zIndex: 999,
    bottom: 0,
    left: 0,
    right: 0,
    background: theme.palette.common.white,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "0 30px",
    marginLeft: sideBarWidth,
    width: `calc(100% - ${sideBarWidth}px)`,
    transition: "ease width 0.4s",

  },
  footerCntShift: {
    marginLeft: sideBarCloseWidth,
    width: `calc(100% - ${sideBarCloseWidth}px)`,

  },
  footerContent: {
    fontSize: "13px !important",
    color: theme.palette.text.light,
    margin: 0
  },
  footerRightCnt: {
    display: "flex",
    alignItems: "center"
  },
  footerQoute: {
    marginRight: ENV == "production" ? 70 : 30,
    position: "relative",
    "&:hover $footerQouteInnerCnt": {
      opacity: 0
    },
    "&:hover $footerQouteSocialMediaShare": {
      opacity: 1,
    }
  },
  footerQouteText: {
    fontSize: "13px !important",
    color: theme.palette.text.secondary,
    textAlign: "right",
    margin: 0
  },
  footerQouteSocialMediaShare: {
    display: "flex",
    opacity: 0,
    alignItems: "center",
    position: "absolute",
    idth:140,
    top: 0,
    right: 0,
    transition: "ease all 0.5s",
    "& p": {
      margin: 0,
      fontSize: "12px !important",
      color: theme.palette.text.secondary
    },
    "& ul": {
      listStyleType: "none",
      display: "flex",
      margin:0,
      padding: 0,
      "& li": {
          width: 30,
          height: 36,
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
      }
    }
  },
  footerQouteInnerCnt: {
    opacity: 1,
    transition: "ease all 0.5s"
  },
  footerSocialIcon: {
    fontSize: "24px !important",
    transition: "ease all 0.2s",
    cursor: "pointer",
    marginLeft: 5,

    "&:hover": {
      fontSize: "28px !important"
    }
  }
});

export default footerStyles;
