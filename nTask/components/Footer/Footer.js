import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { SaveFreshchatData } from "./../../redux/actions/feedback";
import { withStyles } from "@material-ui/core/styles";
import footerStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import TwitterIcon from "../Icons/TwitterIcon";
import LinkedInIcon from "../Icons/LinkedInIcon";
import FacebookIcon from "../Icons/FacebookIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import classNames from "classnames";
import FreshChat from "react-freshchat";
import instance from "../../redux/instance";
import translate from "../../i18n/translate";
import { FacebookShareButton, LinkedinShareButton, TwitterShareButton } from "react-share";
import { FormattedMessage } from "react-intl";
class Footer extends Component {
  // _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      freshchatExternalId: "",
      freshchatRestoreId: "",
      firstName: "",
      lastName: "",
      email: "",
    };
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.profileState.data) {
      const {
        freshchatExternalId,
        freshchatRestoreId,
        firstName,
        lastName,
      } = nextProps.profileState.data.profile;
      const { email } = nextProps.profileState.data;
      return {
        freshchatExternalId,
        freshchatRestoreId,
        firstName,
        lastName,
        email,
      };
    } else {
      return {
        freshchatExternalId: "",
        freshchatRestoreId: "",
        firstName: "",
        lastName: "",
        email: "",
      };
    }
  }
  showTranslated = (total, value) => {
    let id = "";
    switch (value) {
      case "Task":
        id = "common.showing.task";
        break;
      case "Project":
        id = "common.showing.project";
        break;
      case "Meeting":
        id = "common.showing.meeting";
        break;
      case "Issue":
        id = "common.showing.issue";
        break;
      case "Risk":
        id = "common.showing.risk";
        break;
      case "Boards":
        id = "common.showing.board";
        break;
      case "List":
        id = "common.showing.list";
        break;
    }
    return id == "" ? (
      `Showing ${total} ${value}(s)`
    ) : (
      <FormattedMessage
        id={id}
        values={{ t: total }}
        defaultMessage={`Showing ${total} ${value}(s)`}
      />
    );
  };

  render() {
    const { classes, theme, display, total, type, quote = "" } = this.props;
    let splitQuote = [];
    if (quote && quote !== "") {
      splitQuote = quote.split("~");
    }

    // const quote = "If you don't prioritize your life, someone else will.";
    const shareUrl = "https://app.ntaskmanager.com";
    return (
      <div
        id="footer"
        className={classNames(classes.footerCnt, !this.props.open && classes.footerCntShift)}>
        {display && total && type ? (
          <p className={classes.footerContent}>{this.showTranslated(total, type)}</p>
        ) : (
          <p className={classes.footerContent}></p>
        )}
        <div className={classes.footerRightCnt}>
          <div className={classes.footerQoute}>
            <div className={classes.footerQouteInnerCnt}>
              <p className={classes.footerQouteText}>{splitQuote[0] || ""}</p>
              <Typography variant="body2" align="right">
                - {splitQuote[1] || ""}
              </Typography>
            </div>
            <div className={classes.footerQouteSocialMediaShare}>
              <p>
                <FormattedMessage
                  id="footer.share-thoughts.label"
                  defaultMessage="Share your thoughts"
                />
              </p>
              <ul>
                <li>
                  <LinkedinShareButton title={quote} description={quote} url={shareUrl}>
                    <SvgIcon
                      viewBox="0 0 112.196 112.196"
                      className={classes.footerSocialIcon}
                      htmlColor={theme.palette.secondary.light}>
                      <LinkedInIcon />
                    </SvgIcon>
                  </LinkedinShareButton>
                </li>
                <li>
                  <FacebookShareButton quote={quote} url={shareUrl}>
                    <SvgIcon
                      viewBox="0 0 112.196 112.196"
                      className={classes.footerSocialIcon}
                      htmlColor={theme.palette.secondary.light}>
                      <FacebookIcon />
                    </SvgIcon>
                  </FacebookShareButton>
                </li>
                <li>
                  <TwitterShareButton title={quote} url={shareUrl}>
                    <SvgIcon
                      viewBox="0 0 112.197 112.197"
                      className={classes.footerSocialIcon}
                      htmlColor={theme.palette.secondary.light}>
                      <TwitterIcon />
                    </SvgIcon>
                  </TwitterShareButton>
                </li>
              </ul>
            </div>
          </div>
          {/* <div
            style={{
              width: 46,
              height: 46

            }}
          >
            {firstChat}
          </div> */}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  withStyles(footerStyles, { withTheme: true }),
  connect(mapStateToProps, { SaveFreshchatData })
)(Footer);
