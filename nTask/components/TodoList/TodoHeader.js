import React, { useState, useRef } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import moment from "moment";
import PropTypes from "prop-types";
import classes from "./style";
import { validateTitleField } from "../../utils/formValidations";
import DefaultTextField from "../Form/TextField";
import AssigneeDropdown from "../Dropdown/AssigneeDropdown";
import CustomButton from "../Buttons/CustomButton";
import SingleDatePicker from "../DatePicker/SingleDatePicker";
import CustomDatePicker from "../DatePicker/DatePicker/DatePicker";
import AddIcon from "@material-ui/icons/Add";
import CustomIconButton from "../Buttons/CustomIconButton";
import { GetPermission } from "../permissions";
import isEmpty from "lodash/isEmpty";
import debounce from "lodash/debounce";
import { injectIntl,IntlProvider } from 'react-intl';

function TodoHeader(props) {
  const [todoListInputError, setTodoListInputError] = useState(false);
  const [todoListInputErrorMessage, setTodoListInputErrorMessage] = useState(
    false
  );
  const [todoListInput, setTodoListInput] = useState("");
  const [endDate, setEndDate] = useState("");
  const [assignee, setAssignee] = useState([]);
  const [addChecklist, setAddChecklist] = useState(false);
  const [btnAddQuery, setBtnAddQuery] = useState("");
  let checklistInputRef = useRef(null);
  const {intl} = props;
  const handleItemNameInput = (event) => {
    setTodoListInput(event.target.value);
    setTodoListInputError(false);
    setTodoListInputErrorMessage("");
    if (props.changeStatus && props.clearStatusChanged) {
      props.clearStatusChanged();
    }
  };
  const enterKeyHandler = (event) => {
    if (event.key === "Enter") {
      setBtnAddQuery("progress");
      addNewItem();
    }
  };
  const addBtnHandler = () => {
    setBtnAddQuery("progress");
    addNewItem();
  };
  const addNewItem = debounce(event => {
    const validationObj = validateTitleField(
      "to-do list item.",
      todoListInput,
      true,
      250
    );
    if (validationObj.isError) {
      setTodoListInputError(validationObj.isError);
      setTodoListInputErrorMessage(validationObj.message);
      setBtnAddQuery("");
      checklistInputRef.current.focus();
      return;
    }
    const newItem = {
      taskId: props.currentTask.id,
      description: todoListInput.trim(),
      isComplete: false,
      completedDate: "",
      endDate: moment(endDate).format("YYYY-MM-DD"),
      completedBy: "",
      assignee: assignee.map((ass) => ass.userId),
    };
    props.updateTaskAssignee(assignee);
    if (props.updateItem)
      props.updateItem(newItem, "addItem", (response) => {
        setTodoListInput("");
        setTodoListInputError(false);
        setTodoListInputErrorMessage("");
        setAssignee([]);
        setEndDate("");
        setBtnAddQuery("");
        checklistInputRef.current.focus();
      });
  },300);
  const updateAssignee = (assignedTo, currentTask) => {
    setAssignee(assignedTo);
  };

  const handleDateSave = (startDate) => {
    setEndDate(startDate);
  };

  const handleShowHideInput = (value) => {
    // Show/Hide Input on click
    if (value == false && todoListInput) return; // This will run incase user entered any value in input and try to focus out to hide the input
    setAddChecklist(value);
  };

  const {
    classes,
    theme,
    currentTask,
    closeTaskDetailsPopUp,
    isArchivedSelected,
    taskPer,
    todoItemsArr,
    style = {},
  } = props;

  const permission = taskPer ? !taskPer.taskDetail.toDoList.isAllowAdd : false;

  const btnStyles = todoItemsArr.length
    ? {
        display: "block",
        width: "100%",
        textAlign: "left",
        borderTop: "none",
        borderRight: "none",
        borderLeft: "none",
        borderRadius: 0,
        textTransform: "none",
      }
    : {
        display: "block",
        width: "100%",
        textAlign: "left",
        textTransform: "none",
      };
  return (
    <div className={classes.todoHeader}>
      {false ? ( 
        <CustomButton
          btnType="white"
          variant="contained"
          style={btnStyles}
          onClick={(event) => handleShowHideInput(true)}
          disabled={
            isArchivedSelected || permission
          }
        >
          <AddIcon
            className={classes.addIcon}
            htmlColor={theme.palette.secondary.light}
          />{" "}
          <span>{ intl.formatMessage({id:"task.detail-dialog.to-do-list.placeholder", defaultMessage:"Add a to-do list item"})}</span>
        </CustomButton>
      ) : (
        <>
          <div className={classes.todoHeaderTxtCnt}>
            <DefaultTextField
              label=""
              fullWidth
              borderBottom={todoItemsArr.length ? true : false}
              noRightBorder={false}
              noLeftBorder={false}
              outlineCnt={false}
              formControlStyles={{ marginBottom: 0 }}
              error={todoListInputError ? true : false}
              errorState={todoListInputError}
              errorMessage={todoListInputErrorMessage}
              defaultProps={{
                id:"todoNameInput",
                onChange: handleItemNameInput,
                onKeyUp: enterKeyHandler,
                onBlur: (event) => handleShowHideInput(false),
                value: todoListInput,
                placeholder: intl.formatMessage({id:"task.detail-dialog.to-do-list.placeholder", defaultMessage:"Add a to-do list item"}),
                autoFocus: false,
                // inputProps: { maxLength: 80 },
                style: style,
                disabled: isArchivedSelected || permission || btnAddQuery === "progress",
                autoComplete: "off",
                inputRef: (node) => {
                  checklistInputRef.current = node;
                },
                // disabled: {isArchivedSelected}
              }}
              transparentInput={true}
                customInputClass={{
                  input: classes.outlinedInputsmall,
                  root: classes.outlinedInputCnt,
                }}
            />
            {todoListInput ? (
              <div className={classes.todoHeaderAction}>
                <CustomDatePicker
                  date={endDate}
                  placeholder={"End Date"}
                  dateFormat="MMM DD, YYYY"
                  PopperProps={{ size: null }}
                  onSelect={(date, time) => handleDateSave(date, time)}
                  filterDate={moment()}
                  timeInput={false}
                  selectedTime={null}
                />
                {/* <SingleDatePicker
                  placeholder="End Date"
                  isInput={false}
                  selectedSaveDate={data => handleDateSave(data)}
                  isCreation
                  selectedDate={endDate}
                  dateFormat="MMM DD"
                  isArchivedSelected={isArchivedSelected}
                  classes={{
                    emptyDate: classes.emptyDate,
                    selectionDate: classes.addSelectionDate,
                  }}
                  disabled={permission}
                /> */}
                <div className={classes.addAssigneeList}>
                  <AssigneeDropdown
                    closeTaskDetailsPopUp={closeTaskDetailsPopUp}
                    assignedTo={assignee}
                    updateAction={updateAssignee}
                    isArchivedSelected={currentTask.isDeleted}
                    obj={currentTask}
                    singleSelect
                    buttonVariant="small"
                    avatarSize={"small26"}
                    disabled={permission}
                  />
                </div>
                <div style={{ marginLeft: 4 }}>
                  <CustomIconButton
                    onClick={addBtnHandler}
                    btnType="blueOutlined"
                    variant="contained"
                    disabled={
                      isArchivedSelected ||
                      permission ||
                      todoListInput.length < 1 || btnAddQuery == "progress"
                    }
                  >
                    <AddIcon
                      style={{ fontSize: "20px" }}
                      htmlColor={theme.palette.text.brightBlue}
                    />
                  </CustomIconButton>
                </div>
              </div>
            ) : null}
          </div>
        </>
      )}
    </div>
  );
}
TodoHeader.defaultProps = {
  currentTask: {},
  updateItem(e) {
    return e;
  },
  isArchivedSelected: false,
  closeTaskDetailsPopUp(e) {
    return e;
  },
};
TodoHeader.propTypes = {
  currentTask: PropTypes.object.isRequired,
  updateItem: PropTypes.func.isRequired,
  isArchivedSelected: PropTypes.bool.isRequired,
  closeTaskDetailsPopUp: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => {
  return {};
};
export default compose(
  withRouter,
  connect(mapStateToProps, {}),
  withStyles(classes, { withTheme: true })
)(TodoHeader);
