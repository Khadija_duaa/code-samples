import React, { useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import classes from "./style";
import TodoHeader from "./TodoHeader";
import TodoItems from "./TodoItems";
import {
  SaveTodoList,
  CheckTodoItem,
  CheckTodoItemInStore,
  DeleteToDoList,
  CheckAllTodoList,
  UpdateToDoListOrder,
  UpdateToDoListOrderInStore,
  UpdateTask,
  UpdateTaskProgressBar,
} from "../../redux/actions/tasks";
import moment from "moment";

function TodoList(props) {
  // useEffect(() => {
  //   return () => {
  //     updateTaskProgress(props.todoItems)
  //   }
  // },[0]);

  const updateItem = (item, mode, callback) => {
    switch (mode) {
      case "addItem":
        props.SaveTodoList(
          item,
          (response) => {
            if (response && response.status === 200) {
              callback(response);

              const selectedItem = response.data;
              const updatedTodoList = [...props.todoItems];
              updatedTodoList.unshift(selectedItem);
              props.updateTodoItemsInProjectTaskTab(updatedTodoList);

            }
          },
          () => {}
        );
        break;
      case "checkUncheckItem":
        // const selectedItem = { ...item };
        // const items = [...props.todoItems];
        // const updatedCheckItemsList = items.map((c) => {
        //   /** updating checklist array  */
        //   if (c.id == selectedItem.id) return selectedItem;
        //   return c;
        // });
        // props.updateTodoItems(updatedCheckItemsList);
        let changeItem = { ...item };
        if (changeItem.isComplete) {
          changeItem.completedDate = new Date(); //new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString();
          changeItem.completedBy = props.user.userId;
        } else {
          changeItem.completedDate = null;
          changeItem.completedBy = null;
        }
        changeItem.updatedDate = new Date(); //new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString(),
        changeItem.updatedBy = props.user.userId;
        changeItem.version = changeItem.version + 1;
        props.CheckTodoItemInStore(
          changeItem,
          (response) => {},
          (error) => {
          
          }
        );
        props.CheckTodoItem(
          item,
          (response) => {
            callback(response);
          },
          (error) => {
         
          }
        );

        const itemList = [...props.todoItems].map((c) => {
          /** updating checklist array  */
          if (c.id == changeItem.id) {
            return changeItem;
          } else {
            return c;
          }
        });
        props.updateTodoItemsInProjectTaskTab(itemList);


        break;
      case "editItem":
        props.SaveTodoList(
          item,
          (response) => {
            callback(response);

            const selectedItem = response.data;
            const items = [...props.todoItems];
            const updatedTodoList = items.map((c) => {
              /** updating checklist array  */
              if (c.id == selectedItem.id) return selectedItem;
              return c;
            });
            props.updateTodoItemsInProjectTaskTab(updatedTodoList);

          },
          (error) => {
            
          }
        );
        break;
      case "deleteItem":
        const forDeleteitems = [...props.todoItems];
        const deleteUpdatedTodoList = forDeleteitems.filter(
          (x) => x.id !== item.id
        );
        // props.updateTodoItems(deleteUpdatedTodoList);
        props.DeleteToDoList(item, (response) => {
          if (response && response.status === 200) {
            callback(response);
            props.updateTodoItemsInProjectTaskTab(deleteUpdatedTodoList);
            // updateTaskProgress(deleteUpdatedTodoList);
          }
        });
        break;
      case "reorderItems":
        const ids = item.map((x) => x.id);
        const data = {
          taskId: props.currentTask.id,
          itemIds: ids,
        };
        // const updatedTodoList = [...item];
        // props.updateTodoItems(updatedTodoList);
        props.UpdateToDoListOrderInStore(data, (response) => {
          if (response && response.status === 200) {
            callback(response);
          }
        });
        props.UpdateToDoListOrder(data, (response) => {
          if (response && response.status === 200) {
            callback(response);
            // updateTaskProgress(updatedTodoList);
            // const updatedTodoList = [...item];
            // props.updateTodoItems(updatedTodoList);
          }
        });
        break;
      case "checkMarkAll":
        const { user } = props;
        const checkData = {
          taskId: props.currentTask.id,
          checkAll: true,
        };
        props.CheckAllTodoList(
          checkData,
          (response) => {
            // const { user } = props;
            if (response && response.status === 200) {
              // const updatedTodoList = props.todoItems.map((x) => {
              //   x.completedDate = x.isComplete ? x.completedDate : moment();
              //   x.completedBy = x.isComplete ? x.completedBy : user.userId;
              //   x.updatedDate = x.isComplete ? x.updatedDate : moment();
              //   x.updatedBy = x.isComplete ? x.updatedBy : user.userId;
              //   x.isComplete = true;
              //   return x;
              // });
              // props.updateTodoItems(updatedTodoList);
              callback();
              // updateTaskProgress(updatedTodoList);
            }
          },
          () => {},
          user
        );
        break;
      case "uncheckMarkAll":
        const uncheckData = {
          taskId: props.currentTask.id,
          checkAll: false,
        };
        props.CheckAllTodoList(
          uncheckData,
          (response) => {
            if (response && response.status === 200) {
              callback();
              // const updatedTodoList = props.todoItems.map((x) => {
              //   x.completedDate = null;
              //   x.completedBy = null;
              //   x.isComplete = false;
              //   return x;
              // });
              // props.updateTodoItems(updatedTodoList);
              // updateTaskProgress(updatedTodoList);
            }
          },
          () => {},
          null
        );
        break;
    }
  };

  const updateTaskAssignee = (assignedTo) => {
    const { currentTask } = props;
    const { assigneeList } = currentTask;
    const assignedToArr = assignedTo.map((ass) => {
      return ass.userId;
    });

    const newTaskObj = {
      ...currentTask,
      assigneeList: [...new Set([...assigneeList, ...assignedToArr])],
    };

    // props.UpdateTask(newTaskObj, () => {});
  };
  const updateTaskProgress = (totalItems) => {
    // const totalItems = props.todoItems.length;
    const cmpltdItems = totalItems.filter((item) => item.isComplete).length;
    const progressPercent =
      Math.round((cmpltdItems / totalItems.length) * 100) || 0;
    const { currentTask } = props;

    const newTaskObj = { ...currentTask, progress: progressPercent };

    props.UpdateTaskProgressBar(newTaskObj, () => {});
  };

  const {
    currentTask,
    isArchivedSelected,
    closeTaskDetailsPopUp,
    permission,
    todoItems,
    taskPer,
    classes,
    progressBar,
    draggable,
    style = {},
    intl,
  } = props;
  return (
    <div className={todoItems.length ? classes.todoListCntActive : null} style={{flex: 1, padding: "0px 10px"}}>
      <TodoHeader
        currentTask={currentTask}
        updateItem={updateItem}
        closeTaskDetailsPopUp={closeTaskDetailsPopUp}
        isArchivedSelected={isArchivedSelected}
        permission={permission}
        updateTaskAssignee={updateTaskAssignee}
        taskPer={taskPer}
        todoItemsArr={todoItems}
        style={style}
        intl={intl}
      />
      <TodoItems
        todoItemsArr={todoItems}
        currentTask={currentTask}
        updateItem={updateItem}
        isArchivedSelected={isArchivedSelected}
        closeTaskDetailsPopUp={closeTaskDetailsPopUp}
        permission={permission}
        updateTaskAssignee={updateTaskAssignee}
        //updateTaskProgress={updateTaskProgress}
        taskPer={taskPer}
        progressBar={progressBar}
        draggable={draggable}
      />
    </div>
  );
}
TodoList.defaultProps = {
  todoItems: [],
  permission: {},
  isArchivedSelected: false,
  currentTask(e) {
    return e;
  },
  closeTaskDetailsPopUp(e) {
    return e;
  },
  // updateTodoItems(e) {
  //   return e;
  // },
  SaveTodoList(e) {
    return e;
  },
  DeleteToDoList(e) {
    return e;
  },
  CheckAllTodoList(e) {
    return e;
  },
  UpdateToDoListOrder(e) {
    return e;
  },
  UpdateToDoListOrderInStore(e) {
    return e;
  },
};
TodoList.propTypes = {
  todoItems: PropTypes.array.isRequired,
  isArchivedSelected: PropTypes.bool.isRequired,
  currentTask: PropTypes.func.isRequired,
  closeTaskDetailsPopUp: PropTypes.func.isRequired,
  // updateTodoItems: PropTypes.func.isRequired,
  SaveTodoList: PropTypes.func.isRequired,
  DeleteToDoList: PropTypes.func.isRequired,
  CheckAllTodoList: PropTypes.func.isRequired,
  UpdateToDoListOrder: PropTypes.func.isRequired,
  UpdateToDoListOrderInStore: PropTypes.func.isRequired,
};

TodoList.defaultProps = {
  closeTaskDetailsPopUp: () => {},
  permission: {},
  progressBar: true,
  draggable: true,
  updateTodoItemsInProjectTaskTab: () => {},
};
const mapStateToProps = (state) => {
  return {
    user: state.profile.data,
    // taskPer: state.workspacePermissions.data.task,
  };
};
export default compose(
  withRouter,
  connect(mapStateToProps, {
    SaveTodoList,
    CheckTodoItem,
    CheckTodoItemInStore,
    DeleteToDoList,
    CheckAllTodoList,
    UpdateToDoListOrder,
    UpdateToDoListOrderInStore,
    UpdateTask,
    UpdateTaskProgressBar,
  }),
  withStyles(classes, { withTheme: true })
)(TodoList);
