import React, { useEffect, useState, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Scrollbars } from "react-custom-scrollbars";
import DragIndicator from "@material-ui/icons/DragIndicator";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import SvgIcon from "@material-ui/core/SvgIcon";
import CloseIcon from "@material-ui/icons/Close";
import LinearProgress from "@material-ui/core/LinearProgress";
import cloneDeep from "lodash/cloneDeep";
import moment from "moment";
import isEmpty from "lodash/isEmpty";
import isNull from "lodash/isNull";
import PropTypes from "prop-types";
import classes from "./style";
import CheckBoxIcon from "../Icons/CheckBoxIcon";
import IconButton from "../Buttons/IconButton";
import AssigneeDropdown from "../Dropdown/AssigneeDropdown";
import SingleDatePicker from "../DatePicker/SingleDatePicker";
import CustomDatePicker from "../DatePicker/DatePicker/DatePicker";
import UserIcon from "../Icons/UserIcon";
import {
  getCompletePermissionsWithArchieve,
  getEditPermissionsWithArchieve,
  canEdit,
  canDelete,
  canAdd,
} from "../../Views/Task/permissions";
import TodoItemsActionDropDown from "./TodoItemsActionDropDown";
import helper from "../../helper";
import DefaultTextField from "../Form/TextField";
import CustomButton from "../Buttons/CustomButton";
import { validateTitleField } from "../../utils/formValidations";
import CheckkAllItemsIcon from "../Icons/CheckkAllItemsIcon";
import CheckkAllItemsSelectedIcon from "../Icons/CheckkAllItemsSelectedIcon";
import CrossIcon from "../Icons/CrossIcon";
import SaveIcon from "../Icons/SaveIcon";
import CustomAvatar from "../Avatar/Avatar";
import DefaultButton from "../Buttons/DefaultButton";
import ActionConfirmation from "../Dialog/ConfirmationDialogs/ActionConfirmation";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import { GetPermission } from "../permissions";
import CustomIconButton from "../Buttons/CustomIconButton";
import CustomTooltip from "../Tooltip/Tooltip";
import RemoveIcon2 from "../Icons/RemoveIcon2";

import IconEditSmall from "../Icons/IconEditSmall";
import {FormattedMessage, injectIntl} from "react-intl";

function TodoItems(props) {
  // const [progressPercent, setProgressPercent ] = useState(20);
  const [todoListInputError, setTodoListInputError] = useState(false);
  const [todoListInputErrorMessage, setTodoListInputErrorMessage] = useState(
    false
  );
  const [todoListInput, setTodoListInput] = useState("");
  const [edit, setEdit] = useState(-1);
  const [markAllBtnQuery, setMarkAllBtnQuery] = useState("");
  const [enableMarkAllDialog, setEnableMarkAllDialog] = useState(false);

  const handleItemNameInput = (event) => {
    setTodoListInput(event.target.value);
    setTodoListInputError(false);
    setTodoListInputErrorMessage("");
    if (props.changeStatus && props.clearStatusChanged) {
      props.clearStatusChanged();
    }
  };
  const enterKeyHandler = (event, list, index) => {
    if (event.key === "Enter") {
      saveHandler(list, index);
    }
  };

  const saveItem = (list, index) => {
    saveHandler(list, index);
  };

  const saveHandler = (list, index) => {
    // if (event.key === "Enter") {
    const validationObj = validateTitleField(
      "Checklist",
      todoListInput,
      true,
      250
    );
    if (validationObj.isError) {
      setTodoListInputError(validationObj.isError);
      setTodoListInputErrorMessage(validationObj.message);
      return;
    }

    if (
      !props.isArchivedSelected ||
      !props.taskPer.taskDetail.toDoList.isAllowAdd
    ) {
      const selectedItem = { ...list };
      selectedItem.description = todoListInput;
      props.updateItem(selectedItem, "editItem", (response) => {
        setTodoListInput("");
        setTodoListInputError(false);
        setTodoListInputErrorMessage("");
        setEdit(-1);
      });
    }
  };

  const cancelItem = () => {
    event.stopPropagation();
    setEdit(-1);
  };

  const onDragEnd = (result) => {
    const { permission, currentTask } = props;
    const canEditCheckListItem = taskPer.taskDetail.toDoList.isAllowEdit;
    if (!result.destination || !canEditCheckListItem) {
      return;
    }

    const items = reorder(
      props.todoItemsArr,
      result.source.index,
      result.destination.index
    );
    props.updateItem(items, "reorderItems", (response) => {});
  };
  const reorder = (list, startIndex, endIndex) => {
    const result = [...list];
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };
  const handleItemCheck = (list, event) => {
    event.stopPropagation();
    if (
      !props.isArchivedSelected ||
      !props.taskPer.taskDetail.toDoList.isAllowAdd
    ) {
      const selectedItem = { ...list };
      selectedItem.isComplete = !list.isComplete;
      // selectedItem.completedDate = moment(new Date()).format('MMM DD');

      props.updateItem(selectedItem, "checkUncheckItem", (response) => {});
    }
  };
  const updateAssignee = (assignedTo, list) => {
    if (
      !props.isArchivedSelected ||
      !props.taskPer.taskDetail.toDoList.isAllowAdd
    ) {
      const selectedItem = { ...list };
      selectedItem.assignee =
        assignedTo.length > 0 ? [assignedTo[0].userId] : [];
      props.updateItem(selectedItem, "editItem", (response) => {});
      props.updateTaskAssignee(assignedTo);
    }
  };
  const handleDateSave = (endDate, list) => {
    const selectedItem = { ...list };
    selectedItem.endDate = moment(endDate).format("YYYY-MM-DD");
    // selectedItem.completedDate = moment(new Date()).format('MMM DD');

    props.updateItem(selectedItem, "editItem", (response) => {});
  };
  const EditItem = (item, index) => {
    if (item.isComplete == true) return;
    setTodoListInput(item.description);
    setEdit(index);
  };
  const DeleteItem = (deleteItem) => {
    props.updateItem(deleteItem, "deleteItem", (response) => {});
  };
  const openAllItemCheck = () => {
    if (!isArchivedSelected && props.taskPer.taskDetail.toDoList.isAllowEdit) {
      const allCheckedMark = !(
        props.todoItemsArr.filter((item) => item.isComplete == false).length > 0
      );
      if (allCheckedMark) {
        props.updateItem({}, "uncheckMarkAll", (response) => {});
      } else {
        setEnableMarkAllDialog(true);
      }
    }
  };
  const handleAllItemCheck = () => {
    setMarkAllBtnQuery("progress");
    props.updateItem({}, "checkMarkAll", (response) => {
      setMarkAllBtnQuery("");
      setEnableMarkAllDialog(false);
    });
  };
  const closeAllItemCheck = () => {
    setEnableMarkAllDialog(false);
  };
  const isExpire = (date) => {
    const itemDate = moment(new Date(date), "MMMM DD, YYYY");
    const currentDate = moment().format("MMMM DD, YYYY"); // moment(new Date(), 'MMMM DD, YYYY');
    const diff = itemDate.diff(currentDate, "days");
    return diff < 0;
    // return currentDate > itemDate;
  };
  const userCompletedBy = (userId) => {
    const { members, user } = props;
    const member = members.find((m) => userId === m.userId);
    return member ? member.fullName : user.fullName;
  };
  const todoUserData = (list) => {
    const { members } = props;
    let member;
    if (list.assignee && list.assignee.length > 0) {
      member = members.find((m) => list.assignee[0] === m.userId);
    }
    return member ? [member] : [];
  };
  const todoAssigneeUserData = (list) => {
    const { members, user } = props;
    let member;
    if (list.assignee && list.assignee.length > 0) {
      member = members.find((m) => list.assignee[0] === m.userId);
    }
    // return member ? member : user;

    // let user = todoAssigneeUserData(list);
    return (
      <CustomAvatar
        styles={{ marginLeft: 10 }}
        otherMember={{
          imageUrl: member.imageUrl,
          fullName: member.fullName,
          lastName: "",
          email: member.email,
          isOwner: member.isOwner,
        }}
        size="xsmall"
      />
    );
  };


  const getEditIcon = (list, index) => {
    return (
      <div className={classes.editIconCnt}>
        <CustomTooltip
          helptext={<FormattedMessage id="task.detail-dialog.to-do-list.edit-to-do-name.label" defaultMessage="Edit To-Do Name"/>}
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}
        >
          <CustomIconButton
            onClick={(event) => {
              EditItem(list, index);
            }}
            btnType="transparent"
            variant="contained"
            style={{
              padding: "3px 3px 0px 0px",
              backgroundColor: "transparent",
            }}
            disabled={false}
          >
            <SvgIcon
              viewBox="0 0 12 11.957"
              // htmlColor={theme.palette.secondary.medDark}
              className={classes.editIcon}
            >
              <IconEditSmall />
            </SvgIcon>
          </CustomIconButton>
        </CustomTooltip>
      </div>
    );
  };

  const {
    classes,
    todoItemsArr,
    theme,
    currentTask,
    isArchivedSelected,
    closeTaskDetailsPopUp,
    permission,
    taskPer,
    progressBar,
    draggable,
  } = props;
  const totalItems = todoItemsArr.length;
  const cmpltdItems = todoItemsArr.filter((item) => item.isComplete).length;
  const allSelected = totalItems > 0 && totalItems == cmpltdItems;
  const progressPercent = Math.round((cmpltdItems / totalItems) * 100) || 0;
  const editPermission = !taskPer.taskDetail.toDoList.isAllowEdit;
  const deletePermission = !taskPer.taskDetail.toDoList.isAllowDelete;

  return (
    <Fragment>
      {progressBar ? (
        <>
          <ActionConfirmation
            open={enableMarkAllDialog}
            closeAction={closeAllItemCheck}
            cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel"/>}
            successBtnText={<FormattedMessage id="task.detail-dialog.to-do-list.mark-all.action" defaultMessage="Yes, Mark All Complete"/>}
            alignment="center"
            headingText={<FormattedMessage id="task.detail-dialog.to-do-list.mark-all.title" defaultMessage="Mark All Complete"/>}
            iconType="markAll"
            msgText={<FormattedMessage id="task.detail-dialog.to-do-list.mark-all.message" defaultMessage="Are you sure you want to mark all to-do list items as completed?" />}
            successAction={handleAllItemCheck}
            btnQuery={markAllBtnQuery}
          />

          <div className={classes.checkAllCnt}>
            <DefaultButton
              buttonType="smallIconBtn"
              onClick={openAllItemCheck}
              disableRipple
            >
              {allSelected ? (
                <SvgIcon
                  viewBox="0 0 24.44 24.44"
                  className={classes.allmarkCheckedIcon}
                >
                  <CheckkAllItemsSelectedIcon />
                </SvgIcon>
              ) : (
                <SvgIcon
                  viewBox="0 0 24.44 24.44"
                  className={classes.allmarkCheckedIcon}
                >
                  <CheckkAllItemsIcon />
                </SvgIcon>
              )}
            </DefaultButton>
            <div className={classes.progressCnt}>
              <div style={{ display: "flex", alignItems: "center" }}>
                <Typography
                  variant="h6"
                  style={{ fontWeight: theme.typography.fontWeightMedium }}
                >
               <FormattedMessage id="task.detail-dialog.to-do-list.out-of.label" values={{t2: cmpltdItems, t3: totalItems}} defaultMessage={`${cmpltdItems} out of ${totalItems} `}/>   
                </Typography>
                <Typography variant="body2" style={{ marginLeft: 5 }}>
                  <FormattedMessage id="task.detail-dialog.to-do-list.item-completed.label" defaultMessage="items completed"/>
                </Typography>
              </div>
              <LinearProgress
                variant="determinate"
                value={progressPercent}
                classes={{
                  root: classes.progressBar,
                  barColorPrimary: classes.greenBar,
                }}
                style={{ marginTop: 8 }}
              />
            </div>
          </div>
        </>
      ) : null}
      <Grid xs={12} item classes={{ item: classes.checklistCnt }}>
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="droppable">
            {(provided, snapshot) => (
              <div>
                <ul className={classes.todoItemList} ref={provided.innerRef}>
                  {todoItemsArr.map((list, index) => (
                    <Draggable
                      key={list.id}
                      draggableId={list.id}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <li
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          className={
                            edit >= 0 && index !== edit
                              ? classes.todoListItemDisabled
                              : classes.todoListItem
                          }
                        >
                          {edit == index ? (
                            <div className={classes.editCnt}>
                              <DefaultTextField
                                fullWidth
                                errorState={todoListInputError}
                                errorMessage={todoListInputErrorMessage}
                                defaultProps={{
                                  id: "todoEdit",
                                  onChange: handleItemNameInput,
                                  onKeyUp: (e) =>
                                    enterKeyHandler(e, list, index),
                                  value: todoListInput,
                                  placeholder: props.intl.formatMessage({id:"task.detail-dialog.to-do-list.placeholder", defaultMessage:"Add a to-do list item"}),
                                  autoFocus: true
                                  // inputProps: { maxLength: 250 },
                                  // disabled: isArchivedSelected
                                }}
                              />
                              <div
                                style={{
                                  marginLeft: 10,
                                  marginTop: 7,
                                  display: "flex",
                                }}
                              >
                                <DefaultButton
                                  buttonType="smallIconBtn"
                                  disableRipple
                                  onClick={cancelItem}
                                >
                                  <SvgIcon viewBox="0 0 22 22">
                                    <CrossIcon />
                                  </SvgIcon>
                                </DefaultButton>
                                <DefaultButton
                                  buttonType="smallIconBtn"
                                  disableRipple
                                  style={{ marginLeft: 10 }}
                                  onClick={(e) => saveItem(list, index)}
                                >
                                  <SvgIcon viewBox="0 0 22 22">
                                    <SaveIcon />
                                  </SvgIcon>
                                </DefaultButton>
                              </div>
                            </div>
                          ) : (
                            <div className={classes.todoListBody}>
                              <Grid
                                container
                                direction="row"
                                justify="flex-start"
                                alignItems="center"
                                wrap="nowrap"
                              >
                                {taskPer.taskDetail.toDoList.isAllowAdd &&
                                draggable ? (
                                  <span
                                    {...provided.dragHandleProps}
                                    className={classes.dragHandleCnt}
                                  >
                                    <DragIndicator
                                      htmlColor={
                                        theme.palette.secondary.light
                                      }
                                      className={classes.dragHandle}
                                    />
                                  </span>
                                ) : (
                                  <span
                                    style={{ display: draggable ? "" : "none" }}
                                    {...provided.dragHandleProps}
                                    className={classes.checkListItemSpacer}
                                  />
                                )}
                                <div className={classes.todoListItemInner}>
                                  <FormControlLabel
                                    classes={{
                                      root: classes.checklistCheckboxCnt,
                                    }}
                                    control={
                                      <Checkbox
                                        checked={list.isComplete}
                                        disabled={editPermission}
                                        style={{ padding: "0px 10px 0 0" }}
                                        onClick={(e) =>
                                          handleItemCheck(list, e)
                                        }
                                        disableRipple
                                        value={list.description}
                                        checkedIcon={
                                          <SvgIcon
                                            viewBox="0 0 426.667 426.667"
                                            htmlColor={
                                              theme.palette.status.completed
                                            }
                                            classes={{
                                              root: classes.checkedIcon,
                                            }}
                                          >
                                            <CheckBoxIcon />
                                          </SvgIcon>
                                        }
                                        icon={
                                          <>
                                            <SvgIcon
                                              viewBox="0 0 426.667 426.667"
                                              htmlColor={
                                                theme.palette.background.items
                                              }
                                              classes={{
                                                root: classes.unCheckedIcon,
                                              }}
                                            >
                                              <CheckBoxIcon />
                                            </SvgIcon>
                                            <span
                                              className={classes.emptyCheckbox}
                                            />
                                          </>
                                        }
                                        color="primary"
                                      />
                                    }
                                  />
                                  <div>
                                    <div className={classes.todoNameCnt}>
                                      <Typography
                                        variant="caption"
                                        className={classes.todoItemTxt}
                                        // onClick={(e) =>
                                        //   !editPermission
                                        //     ? EditItem(list, index)
                                        //     : null
                                        // }
                                        title={list.description}
                                        style={
                                          list.isComplete
                                            ? { textDecoration: "line-through" }
                                            : null
                                        }
                                      >
                                        {list.description}
                                      </Typography>
                                      {!editPermission &&
                                      list.isComplete == false
                                        ? getEditIcon(list, index)
                                        : null}
                                    </div>
                                    {/* <span
                                      style={
                                        list.isComplete
                                          ? { textDecoration: "line-through" }
                                          : null
                                      }
                                      className={classes.todoItemTxt}
                                    >
                                      {list.description}
                                    </span> */}
                                    {/* <br /> */}
                                    <div
                                      className={classes.todoItemCheckedDetails}
                                    >
                                      <CustomDatePicker
                                        date={
                                          isEmpty(list.endDate) ||
                                          isNull(list.endDate)
                                            ? ""
                                            : list.endDate
                                        }
                                        placeholder="End Date"
                                        dateFormat="MMM DD"
                                        style={{
                                          borderRadius: 4,
                                          minWidth: 70,
                                        }}
                                        btnProps={{ labelAlign: "left" }}
                                        btnType={"transparent"}
                                        // icon={false}
                                        PopperProps={{ size: null }}
                                        onSelect={(data, time) =>
                                          handleDateSave(data, list)
                                        }
                                        disabled={list.isComplete || editPermission}
                                        filterDate={moment()}
                                        timeInput={false}
                                        selectedTime={null}
                                      />
                                      {list.isComplete ? (
                                        <div
                                          className={
                                            classes.markCompleteDetails
                                          }
                                        >
                                          {/* <SingleDatePicker
                                          placeholder="End Date"
                                          isInput={false}
                                          selectedSaveDate={data => handleDateSave(data, list)}
                                          isCreation
                                          selectedDate={isEmpty(list.endDate) || isNull(list.endDate) ? '' : list.endDate}
                                          dateFormat="MMM DD"
                                          removeable={false}
                                          isArchivedSelected={isArchivedSelected}
                                          classes={{
                                            emptyDate: classes.emptyDate,
                                            selectionDate: classes.selectionDate,
                                            expiryDate: classes.dateExpire,
                                            StaticDatePickerPopper: classes.StaticDatePickerPopper
                                          }}
                                          isDisabled={editPermission}
                                        /> */}
                                          <div>
                                            {/* <SvgIcon viewBox="0 12 13.813" className={classes.userIcon}>
                                                <UserIcon />
                                              </SvgIcon> */}
                                            <Typography
                                              classes={{
                                                body2:
                                                  classes.checklistItemDetailsName,
                                              }}
                                              variant="body2"
                                            >
                                              {userCompletedBy(
                                                list.completedBy
                                              )}
                                            </Typography>
                                          </div>
                                          <Typography
                                            classes={{
                                              body2:
                                                classes.checklistItemDetails,
                                            }}
                                            variant="body2"
                                          >
                                            •
                                          </Typography>
                                          <Typography
                                            classes={{
                                              body2:
                                                classes.checklistItemDetails,
                                            }}
                                            variant="body2"
                                          >
                                            {list.updatedDate
                                              ? helper.RETURN_CUSTOMDATEFORMATFORCHECKLIST(
                                                  list.updatedDate
                                                )
                                              : helper.RETURN_CUSTOMDATEFORMATFORCHECKLIST(
                                                  new Date().toISOString()
                                                )}
                                          </Typography>
                                        </div>
                                      ) : null}
                                    </div>
                                  </div>
                                </div>
                              </Grid>
                              <div
                                className={
                                  list.isComplete || isArchivedSelected
                                    ? classes.actionCntCmplete
                                    : classes.actionCnt
                                }
                              >
                                {
                                  // list.isComplete ? (
                                  //   <Typography
                                  //     variant="h6"
                                  //     className={isExpire(list.endDate) ? classes.dateExpire : {}}
                                  //     style={{ whiteSpace: 'nowrap' }}
                                  //   >
                                  //     {isEmpty(list.endDate) || isNull(list.endDate) ? '' : moment(list.endDate).format('MMM DD')}
                                  //   </Typography>
                                  // ) : (
                                  // )
                                }
                                {
                                  <div className={classes.assigneeListCnt}>
                                    <AssigneeDropdown
                                      closeTaskDetailsPopUp={
                                        closeTaskDetailsPopUp
                                      }
                                      assignedTo={todoUserData(list)}
                                      updateAction={updateAssignee}
                                      isArchivedSelected={currentTask.isDeleted}
                                      obj={list}
                                      singleSelect
                                      buttonVariant="small"
                                      avatarSize={"small26"}
                                      disabled={
                                        editPermission ||
                                        list.isComplete ||
                                        isArchivedSelected
                                      }
                                    />
                                  </div>
                                }
                                <div className={classes.actionDropDown}>
                                  {/* <TodoItemsActionDropDown
                                    EditItem={(e) => EditItem(list, index)}
                                    DeleteItem={(e) => DeleteItem(list, index)}
                                    isArchivedSelected={isArchivedSelected}
                                    currentTask={currentTask}
                                    permission={permission}
                                    taskPer={taskPer}
                                  /> */}
                                  <CustomTooltip
                                    helptext={<FormattedMessage id="team-settings.billing.bill-dialog.remove-button.label" defaultMessage="Remove"/>}
                                    iconType="help"
                                    placement="top"
                                    style={{
                                      color: theme.palette.common.white,
                                    }}
                                  >
                                    <CustomIconButton
                                      onClick={() => {
                                        DeleteItem(list, index);
                                      }}
                                      btnType="transparent"
                                      variant="contained"
                                      disabled={
                                        deletePermission ||
                                        list.isComplete ||
                                        isArchivedSelected
                                      }
                                      style={{
                                        width: 20,
                                        height: 20,
                                        borderRadius: 4,
                                        color:
                                          deletePermission ||
                                          list.isComplete ||
                                          isArchivedSelected
                                            ? "#BFBFBF"
                                            : "#FF4A4A",
                                        marginLeft: 7,
                                      }}
                                    >
                                      <SvgIcon viewBox="0 0 24 24">
                                        <RemoveIcon2 />
                                      </SvgIcon>
                                    </CustomIconButton>
                                  </CustomTooltip>
                                </div>
                              </div>
                            </div>
                          )}
                        </li>
                      )}
                    </Draggable>
                  ))}
                </ul>
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </Grid>
    </Fragment>
  );
}
TodoItems.defaultProps = {
  todoItemsArr: [],
  currentTask: {},
  updateItem(e) {
    return e;
  },
  isArchivedSelected: false,
  closeTaskDetailsPopUp(e) {
    return e;
  },
  permission: false,
};
TodoItems.propTypes = {
  todoItemsArr: PropTypes.array.isRequired,
  currentTask: PropTypes.object.isRequired,
  updateItem: PropTypes.func.isRequired,
  isArchivedSelected: PropTypes.bool.isRequired,
  closeTaskDetailsPopUp: PropTypes.func.isRequired,
  permission: PropTypes.bool.isRequired,
};
const mapStateToProps = (state) => {
  return {
    members: state.profile.data.member.allMembers || [],
    user: state.profile.data,
  };
};
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {}),
  withStyles(classes, { withTheme: true })
)(TodoItems);
