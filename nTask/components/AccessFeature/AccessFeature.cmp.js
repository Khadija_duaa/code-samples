import React from 'react'
import { useSelector } from "react-redux";
import { store } from "../../index";

export function CanAccessFeature({ children = null, props = {}, feature, group }) {
  const state = useSelector(state => {
    return {
      obj: state.featureAccessPermissions
    }
  })
  const { obj } = state;
  const isAccessible = obj[group][feature]?.isHidden;
  if (isAccessible == undefined) {
    console.log(group, 's ', feature, ' < === > ', isAccessible, 'from hook');
    return <>{children}</>
  }
  return <>{!isAccessible ? children : null}</>
}

export function CanAccess({ children = null, props = {}, feature, group }) {
  const obj = store.getState().featureAccessPermissions;
  const isAccessible = obj[group][feature]?.isHidden;
  if (isAccessible == undefined) {
    // console.log(group, 's ', feature, ' < === > ', isAccessible, 'from function');
    return true;
  }
  return !isAccessible;
}
export function TeamCanAccessFeature({ children = null, workspaceId = {}, feature, group }) {
  const obj = store.getState().featureAccessPermissions;

  const isAccessible = obj[group]?.[feature]
  if (isAccessible == undefined) {
    // console.log(group, 's ', feature, ' < === > ', isAccessible, 'from function')
    return true;
  }
  if (isAccessible.settings.hiddenInWorkspaces.length) {
    return !isAccessible.settings.hiddenInWorkspaces.includes(workspaceId);
  } else if (isAccessible.isHidden && isAccessible.settings.hiddenInWorkspaces.length == 0) {
    return false
  } else {
    return true;
  }
}
