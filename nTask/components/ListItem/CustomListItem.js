import ListItem from "@material-ui/core/ListItem";
import withStyles from "@material-ui/core/styles/withStyles";
import DragIndicator from "@material-ui/icons/DragIndicator";
import ArrowRightIcon from "@material-ui/icons/KeyboardArrowDown";
import clsx from "clsx";
import React, { useState } from "react";
import DropdownMenu from "../Dropdown/DropdownMenu";
import customListItemStyles from "./customListItem.style";

function CustomListItem(props) {
  const {
    children,
    classes,
    theme,
    subNav,
    subNavRenderer,
    icon,
    lineBreak,
    isSelectedItemDropdownClicked,
    rootProps,
    popperProps,
    textProps,
    endIcon,
    isSelected,
    compactMode,
    darkmode,
    draggable,
    draggableProps,
    bgapply = true,
  } = props;
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClose = (event) => {
    // Function closes dropdown

    setAnchorEl(null);
  };
  const handleDropdownOpen = (event) => {
    // Function Opens the dropdown
    // event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const { onClick, ...rest } = rootProps;
  return (
    <>
      <ListItem
        button
        className={clsx({
          [classes.listItem]: true,
          [classes.listItemSelectedItemDropdown]: isSelectedItemDropdownClicked,
          [classes.listItemBreak]: lineBreak,
          [classes.compactListItem]: !compactMode,
          [classes.darkModeBackground]: bgapply && isSelected && darkmode,
          [classes.lightModeBackground]: bgapply && isSelected && !darkmode,
        })}
        onClick={subNav ? handleDropdownOpen : rootProps.onClick}
        ref={anchorEl}
        {...rest}>
        {isSelected && !darkmode && <span className={classes.selectedItemBorder}></span>}
        {isSelected && darkmode && <span className={classes.selectedDarkItemBorder}></span>}
        <div className={classes.listItemTextCnt}>
          {draggable && (
            <span className={classes.dragHandleCnt} {...draggableProps}>
              <DragIndicator
                htmlColor={theme.palette.secondary.light}
                className={classes.dragHandle}
              />
            </span>
          )}
          {icon && icon}
          <span
            className={clsx({ [classes.listItemText]: true, [classes.darklistItemText]: darkmode })}
            {...textProps}>
            {children}
          </span>
        </div>
        {subNav && <ArrowRightIcon className={classes.arrowIcon} />}
        {endIcon && endIcon}
      </ListItem>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size={"small"}
        placement="right-start"
        disablePortal={true}
        clickAway={false}
        {...popperProps}>
        {subNavRenderer}
      </DropdownMenu>
    </>
  );
}

CustomListItem.defaultProps = {
  value: "",
  classes: {},
  icon: null,
  rootProps: { onClick: () => { } },
  darkmode: false,
  compactMode: true,
  draggable: false,
  draggableProps: {},
};
export default withStyles(customListItemStyles, { withTheme: true })(CustomListItem);
