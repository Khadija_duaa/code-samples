const customListItemStyles = theme => ({
  listItem: {
    padding: "5px 10px",
    display: "flex",
    justifyContent: "space-between",
    minHeight: 30,
    "&:hover $dragHandleCnt": {
      visibility: "visible",
    },
  },
  listItemSelectedItemDropdown: {
    padding: "5px 10px",
    display: "flex",
    justifyContent: "space-between",
    maxHeight: 30,
    "&:hover $dragHandleCnt": {
      visibility: "visible",
    }, 
  },
  listItemBreak: {
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  listItemTextCnt: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    "& svg, & img": {
      fontSize: "16px !important",
      marginRight: 13,
    },
    "& img": {
      width: 16,
      minWidth: 16,
    },
  },
  listItemText: {
    color: theme.palette.text.lightGray,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    fontWeight: 600,
  },
  arrowIcon: {
    color: theme.palette.icon.gray600,
    marginRight: -8,
    transform: "rotate(270deg)",
  },
  selectedItemBorder: {
    background: theme.palette.background.blue,
    width: 3,
    position: "absolute",
    left: 0,
    height: "100%",
    borderRadius: "0 4px 4px 0",
  },
  selectedDarkItemBorder: {
    background: theme.palette.background.green,
    width: 3,
    position: "absolute",
    left: 0,
    height: 30,
    borderRadius: "0 4px 4px 0",
  },
  compactListItem: {
    height: 36,
    background: "transparent",
    transition: "0.4s ease all",
    "&:hover": {
      backgroundColor: "#2D3742",
    },
    "&:hover $darklistItemText": {
      color: "white",
    },
    "&:hover svg": {
      color: "white",
    },
    "& svg": {
      color: "#a5b4be",
    },
  },
  darklistItemText: {
    fontSize: "14px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    color: "#C7D4DC",
    width: "100%",
  },
  darkModeBackground: {
    backgroundColor: `#2D3742 !important`,
    "& svg": {
      color: `${theme.palette.background.green} !important`,
    },
    "& $darklistItemText": {
      color: "white !important",
    },
  },
  lightModeBackground: {
    backgroundColor: `${theme.palette.background.azureLight} !important`,
  },
  dragHandleCnt: {
    visibility: "hidden",
  },
  dragHandle: {
    fontSize: "20px !important",
    marginRight: "0px !important",
    marginBottom: -5,
    marginLeft: -10,
  },
});

export default customListItemStyles;
