const SaveRiskIssueTextComment = (data, issueRiskType, self) => {
  self.props.SaveRiskIssueComments(data, response => {
    if (response && response.status === 200)
      issueRiskType === "Issue"
        ? self.props.AddNewIssueCommentData(response.data)
        : self.props.AddNewRiskCommentData(response.data);
  });
};

const saveComment = (
  name,
  docsId,
  type,
  riskIssueId,
  text,
  issueRiskType,
  self,
  editorState,
  editorSelf
) => {
  const fileType = {
    doc: 4,
    docx: 4,
    pdf: 5,
    png: 6,
    jpg: 6,
    jpeg: 6,
    bmp: 6,
    tiff: 6,
    gif: 6,
    xlsx: 7,
    ppt: 8
  };
  const fileAttachment = self.state.fileAttachment;
  let UpdateType = fileAttachment
    ? fileType[fileAttachment.docsType]
      ? fileType[fileAttachment.docsType]
      : 9
    : 1;

  var comment = {
    riskIssueId: riskIssueId,
    type: issueRiskType,
    fileId: docsId,
    UpdateType,
    message: text.includes("<p></p>") ? "" : text
  };
  self.props.SaveUpdates(comment, data => {
    if (data && data.status === 200) {
      issueRiskType === "Issue"
        ? self.props.AddNewIssueCommentData(data.data)
        : self.props.AddNewRiskCommentData(data.data);
      self.setState({
        attachment: "",
        fileAttachment: null,
        fileAttachmentData: null
      });
      editorSelf.setState({
        fileAttachment: null,
        fileAttachmentData: null,
        attachmentArray: [],
        editorState: editorState.createEmpty(),
        editorHeight: 20,
        toolbar: false
      });
    }
  });
};

const saveAttachment = (text, issueRiskType, self, EditorState, editorSelf) => {
  self.props.saveRiskIssueAttachment(
    self.state.fileAttachment,
    data => {
      saveComment(
        data.data.docsName,
        data.data.docsId,
        data.data.docsType,
        self.state.fileAttachment.riskIssueId,
        text,
        issueRiskType,
        self,
        EditorState,
        editorSelf
      );
    },
    error => {
      if(error) self.showSnackBar(error.data, "error");
    }
  );
};

const UploadFile = (text, type, self, EditorState, editorSelf) => {
  const { fileAttachmentData } = self.state;
  if (fileAttachmentData) {
    self.props.uploadFileTextEditor(
      fileAttachmentData,
      response => {
        self.state.fileAttachment.docsPath = response.data;
        self.state.fileAttachment.type = type;
        saveAttachment(text, type, self, EditorState, editorSelf);
      },
      error => {
        if(error) self.showSnackBar(error.data, "error");
      }
    );
  }
};

const _handleImageChange = (e, id, issueRiskType, userid, saveAttachment) => {
  e.preventDefault();
  var data = new FormData();
  let reader = new FileReader();
  let file = e.target;
  let uploadedData = null;
  let flag = false;
  if (file.files.length > 0) {
    let type = file.files[0].name.toLowerCase().match(/[0-9a-z]{1,5}$/gm)[0];
    if (type != null && type !== "exe") {
      reader.onload = () => {
        if (issueRiskType === "Issue") data.append("issueid", id);
        else if (issueRiskType === "Risk") data.append("riskid", id);
        data.append("userid", userid);
        data.append("File", file.files[0]);

        let attachment = {
          docsType: type,
          docsName: file.files[0].name,
          riskIssueId: id,
          fileSize: file.files[0].size,
            docsPath: null
        };
        flag = true;
        saveAttachment({
          fileAttachmentData: data,
          fileAttachment: attachment
        });
      };
      reader.readAsArrayBuffer(file.files[0]);
    } else {
      //   setState({
      //     imageUploadFlag: false
      //   });
    }
  }
  if (flag) return uploadedData;
};

export default {
  handleImageUpload: _handleImageChange,
  UploadIssueRiskFile: UploadFile,
  SaveRiskIssueTextComment: SaveRiskIssueTextComment
};
/*********************************************************************************** */
/**
 * Notes: Generic Implementation of Issues And Risks Text/Attachment Comments Upload Section
 * Implemented By: Ahmad Shoaib
 * Date: 25-01-2019
 **/
/*********************************************************************************** */
