const inviteMemberStyles = theme => ({
  inviteMemberCnt: {
    display: "flex",
    alignItems: "flex-start"
  },
  addIcon: {
    fontSize: "18px !important"
  },
  listIcon: {
    fontSize: "8px !important",
    color: theme.palette.secondary.light,
    marginRight: 10
  },
  addMeetingDetailList: {
    listStyleType: "none",
    padding: 0,
    marginBottom: 10,
    '& > li': {
      padding: "0 10px 5px 10px",
      marginBottom: 5,
      borderBottom: `1px solid ${theme.palette.border.extraLightBorder}`,
      fontSize: "12px !important",
      color: theme.palette.text.primary,
      cursor: 'text',

      '& p': {
        margin: 0,
        padding: 0
      },
      '& $CloseIconBtn': {
        marginLeft: 10
      },
    },
    '& > li:last-child': {
      borderBottom: "none",
      marginBottom: 0
    },
    '& > $inputListItem': {
      padding: 0
    }
  },
  deleteIcon: {
    fontSize: "14px !important"
  }
});

export default inviteMemberStyles;
