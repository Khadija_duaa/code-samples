import React, { Component } from "react";
import DefaultTextField from "../Form/TextField";
import { withStyles } from "@material-ui/core/styles";
import CustomIconButton from "../Buttons/CustomIconButton";
import AddIcon from "@material-ui/icons/Add";
import inviteMemberStyles from "./style";
import RoundIcon from "@material-ui/icons/Brightness1";
import IconButton from "../Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { validEmail } from "../../utils/validator/common/email";
import { message } from "../../utils/constants/errorMessages/team";
import { compose } from "redux";
import { connect } from "react-redux";
import { validateEmailAddress } from "./../../redux/actions/teamMembers";
import Tooltip from "@material-ui/core/Tooltip";
import { SignUpRequest } from "../../redux/actions/signUp";
import { injectIntl } from "react-intl";
class InviteMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      EmailAddressError: false,
      EmailAddressErrorText: "",
      inviteEmailList: [],
      btnQuery: "",
    };
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      JSON.stringify(this.props.emailList) !==
      JSON.stringify(prevProps.emailList) &&
      this.props.emailList &&
      !this.props.emailList.length
    ) {
      this.setState({ inviteEmailList: [], email: "" });
    }
  }
  handleEmailInput = (event) => {
    let inputValue = event.target.value;
    this.setState({
      email: inputValue,
      EmailAddressError: false,
      EmailAddressErrorText: "",
    });
  };
  removeEmail = (email) => {
    const { inviteEmailList } = this.state;
    const filterDelEmail = inviteEmailList.filter((i) => i !== email); // Removing the email that's deleted
    this.setState({ inviteEmailList: filterDelEmail }, () => {
      this.props.updateEmailList(filterDelEmail); // The function updates the state of email in parent so it can be sent to backend for invitation process
    });
  };
  onEnterPress = (event) => {
    if (event.key === "Enter") {
      this.handleAddBtnClick(event);
    }
  };
  handleAddBtnClick = (event) => {
    // Function called on add email button click
    const { email, inviteEmailList } = this.state;
    const {
      profileState = {},
      ignoreEmails = [],
      invitationLimit,
      emailExistanceCheck,
      errorMessage
    } = this.props;
    const teamMembers = profileState.teamMember || [];
    if (invitationLimit == inviteEmailList.length) {
      this.setState({
        // Setting state to throw error that email limit is reached
        EmailAddressError: true,
        EmailAddressErrorText: errorMessage ? errorMessage : message.email.teamInviteLimit,
      });
      return;
    }
    const memberAlreadyExist = teamMembers.find((m) => {
      // Checking if the user with the email entered in input already exists or not
      return m.email == email && !m.isDeleted;
    });
    let validateEmail = validEmail(email); // returns if value is validated or not and error message if not validated
    if (
      validateEmail.validated &&
      !inviteEmailList.includes(email) &&
      email !== profileState.email &&
      !memberAlreadyExist &&
      ignoreEmails.indexOf(email) < 0
    ) {
      // api call

      this.setState({ btnQuery: "progress" }, () => {
        if (emailExistanceCheck) {
          // This is to check if user already is part of ntask or not
          SignUpRequest(
            email,
            () => {
              //    Checking if email is valid and does not exist in already existing list
              // Updating state by pushing new email value in the email list
              this.setState(
                (prevState) => ({
                  inviteEmailList: [...prevState.inviteEmailList, email],
                  email: "",
                  btnQuery: "",
                }),
                () => {
                  this.props.updateEmailList(this.state.inviteEmailList, email); // The function updates the state of email in parent so it can be sent to backend for invitation process
                }
              );
            },

            //Failure
            (error) => {
              this.setState({
                // Setting state to throw error that email is not valid
                EmailAddressError: true,
                EmailAddressErrorText: error.data.userExist
                  ? "Oops! The invitee is already an nTask user."
                  : error.data.message,
                btnQuery: "",
              });
            }
          );
        } else {
          this.props.validateEmailAddress(
            email,
            (response) => {
              //    Checking if email is valid and does not exist in already existing list
              // Updating state by pushing new email value in the email list
              this.setState(
                (prevState) => ({
                  inviteEmailList: [...prevState.inviteEmailList, email],
                  email: "",
                  btnQuery: "",
                }),
                () => {
                  this.props.updateEmailList(this.state.inviteEmailList, email); // The function updates the state of email in parent so it can be sent to backend for invitation process
                }
              );
            },
            (error = {}) => {
              this.setState({
                // Setting state to throw error that email is not valid
                EmailAddressError: true,
                EmailAddressErrorText: error.data.message || "",
                btnQuery: "",
              });
            }
          );
        }
      });
    } else {
      this.setState({
        // Setting state to throw error that email is not valid
        EmailAddressError: true,
        EmailAddressErrorText: this.getEmailErrorMessages(),
      });
    }
  };
  getEmailErrorMessages = () => {
    const { email, inviteEmailList } = this.state;
    const { profileState, ignoreEmails = [], intl } = this.props;
    let validateEmail = validEmail(email, intl); // returns if value is validated or not and error message if not validated
    const teamMembers = profileState.teamMember || [];

    const memberAlreadyExist = teamMembers.find((m) => {
      // Checking if the user with the email entered in input already exists or not
      return m.email == email && !m.isDeleted;
    });
    let emailErrorMessage = !validateEmail.validated
      ? validateEmail.errorMessage //Error of invalid Email format
      : inviteEmailList.includes(email)
        ? message.email.emailAlreadyExist // Error of Email already exist in the invite list
        : email == profileState.email || ignoreEmails.indexOf(email) > -1
          ? message.email.ownEmail // Error of User may not enter his own email address
          : memberAlreadyExist
            ? message.email.alreadyTeamMember // Error of User may not enter email address of member who is already part of team
            : null;

    return emailErrorMessage;
  };
  render() {
    const {
      email,
      EmailAddressError,
      EmailAddressErrorText,
      inviteEmailList,
      btnQuery,
    } = this.state;
    const {
      classes,
      theme,
      placeholder,
      label,
      disableInvited = false,
      addBtnToolTip = "",
      inputType
    } = this.props;

    return (
      <div>
        {/* Invite member input and add button container */}
        <div className={classes.inviteMemberCnt}>
          <DefaultTextField
            fullWidth={true}
            label={label}
            labelProps={{
              shrink: true,
            }}
            disabled={btnQuery === "progress" || disableInvited}
            errorState={EmailAddressError}
            errorMessage={EmailAddressErrorText}
            defaultProps={{
              id: "emailList",
              onChange: this.handleEmailInput,
              onKeyPress: this.onEnterPress,
              value: email,
              placeholder: placeholder
                ? placeholder
                : this.props.intl.formatMessage({ id: "team-settings.user-management.invite-members.form.invite.placeholder", defaultMessage: "Enter email address and press Enter or click the + button to add" }),
              autoFocus: false,
            }}
          />

          <Tooltip title={addBtnToolTip}>
            <div style={{ paddingLeft: 15, paddingTop: label ? 25 : 3 }}>
              <CustomIconButton
                btnType="blue"
                onClick={this.handleAddBtnClick}
                style={{ height: 30, width: 30, padding: 0 }}
                query={btnQuery}
                disabled={btnQuery === "progress" || disableInvited}
              >
                <AddIcon
                  htmlColor={theme.palette.common.white}
                  className={classes.addIcon}
                />
              </CustomIconButton>
            </div>
          </Tooltip>
        </div>
        {inviteEmailList.length ? (
          <ul className={classes.addMeetingDetailList}>
            {inviteEmailList.map((x, i) => {
              return (
                <li className="flex_center_space_between_row" key={x}>
                  <div className="flex_center_start_row">
                    <RoundIcon className={classes.listIcon} />
                    {x}
                  </div>
                  <div className={classes.CloseIconBtn}>
                    <CustomIconButton
                      btnType="danger"
                      style={{ padding: 3 }}
                      onClick={() => {
                        this.removeEmail(x);
                      }}
                    >
                      <CloseIcon
                        className={classes.deleteIcon}
                        htmlColor={theme.palette.common.white}
                      />
                    </CustomIconButton>
                  </div>
                </li>
              );
            })}
          </ul>
        ) : null}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    profileState: state.profile.data,
  };
}
export default compose(
  injectIntl,
  withStyles(inviteMemberStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    validateEmailAddress,
  })
)(InviteMember);
