import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import "../../assets/css/react-datepicker.css";
import datePickerStyles from "../../assets/jss/components/datePicker";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FormControl from "@material-ui/core/FormControl";
import CreateableSelectDropdown from "../Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import {
  generateDuration12HoursData,
  generateDuration24HoursData,
  generateDuration100HoursData,
  generateDuration168HoursData,
  generateDurationMinsData,
  generateTimeMinsData,
} from "../../helper/generateSelectData";
import { validateMinutes } from "../../utils/validator/time/minutes";
import { validateHours } from "../../utils/validator/time/hours";
import FormHelperText from "@material-ui/core/FormHelperText";
import { FormattedMessage } from "react-intl";

class DurationInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: "",
      hours: "",
      mins: "",
      am: "AM",
      isChanged: true,
    };
    this.handleTimeSave = this.handleTimeSave.bind(this);
    this.handleHoursInput = this.handleHoursInput.bind(this);
    this.handleMinsInput = this.handleMinsInput.bind(this);
  }

  componentDidMount() {
    const { hours, mins } = this.props;
    this.setState({
      hours:
        hours && hours > -1
          ? {
            label: `${hours}`,
            value: `${hours}`,
            uniqueId: "",
            id: "",
            obj: "",
          }
          : "",
      mins:
        mins && mins > -1
          ? { label: `${mins}`, value: `${mins}`, uniqueId: "", id: "", obj: "" }
          : "",
    });
  }
  componentDidUpdate(prevProps) {
    const { hours, mins } = this.props;
    if (hours !== prevProps.hours || mins !== prevProps.mins) {
      this.setState({
        hours:
          hours && hours > -1
            ? {
              label: `${hours}`,
              value: `${hours}`,
              uniqueId: "",
              id: "",
              obj: "",
            }
            : "",
        mins:
          mins && mins > -1
            ? {
              label: `${mins}`,
              value: `${mins}`,
              uniqueId: "",
              id: "",
              obj: "",
            }
            : "",
      });
    }
    // const { hours, mins } = this.props;
    // if (hours !== prevProps.hours || mins !== prevProps.mins)
    // this.setState({ hours: {label: `${hours}`, value: `${hours}`}, mins: {label: `${mins}`, value: `${mins}`} });
  }
  handleHoursInput(event) {
    const value = event.currentTarget.value;
    if (value.length <= 2 && /^([0-9]|[0-1][0-9]|[2][0-3]|^(?![\s\S]))$/.test(value)) {
      this.setState({ hours: value }, () => {
        this.handleTimeSave();
        this.handleUpdate();
      });
    }
  }
  //
  handleFocusOut = () => {
    const { hours, mins } = this.props;
    this.setState({
      hours: this.getFormattedValue(hours),
      mins: this.getFormattedValue(mins),
    });
  };
  handleSelect = event => {
    event.currentTarget.select();
  };
  handleMinsInput(event) {
    const value = event.currentTarget.value;
    if (value.length <= 2 && /^([0-9]|[0-5][0-9]|^(?![\s\S]))$/.test(value)) {
      this.setState({ mins: value }, () => {
        this.handleTimeSave();
        this.handleUpdate();
      });
    }
  }
  getFormattedValue = value => {
    if (value != "" && Number(value) * 1 < 10) return `0${Number(value)}`;
    return value;
  };
  handleTimeSave() {
    const { hours, mins, am } = this.state;
    const time = `${hours.length < 2 ? `0${hours}` : hours}:${mins.length < 2 ? `0${mins}` : !mins ? "00" : mins
      }`;
    this.setState({ time }, () => {
      if (this.props.handleDurationChange) {
        this.props.handleDurationChange(this.state.time);
      }
    });
  }

  handleUpdate = () => {
    const { hours, mins } = this.state;
    if (this.props.updateTime) {
      this.props.updateTime(hours, mins);
    }
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  handleOptionsSelect = (type, option) => {
    const { updateTime } = this.props;
    const { hours, mins } = this.state;
    this.setState({ [type]: option });
    if (type == "hours") {
      updateTime(option.value, mins && mins.value);
    } else {
      updateTime(hours && hours.value, option.value);
    }
  };
  //Updating local state when option is created in assignee dropdown
  handleCreateOption = (type, option) => {
    const { updateTime } = this.props;
    const { hours = {}, mins = {} } = this.state;
    this.setState({ [type]: option });
    if (type == "hours") {
      let valMin = mins ? mins.value : null;
      updateTime(option.value, valMin);
    } else {
      let valHours = hours ? hours.value : null;
      updateTime(valHours, option.value);
    }
  };
  handleClearSelect = (key, value) => {
    this.setState({ [key]: value });
    this.props.clearSelect(key, value);
  };
  //validating mins input of select
  minsValidation = value => {
    return validateMinutes(value);
  };
  //validating hours input of select
  hoursValidation = value => {
    return validateHours(value);
  };
  render() {
    const {
      classes,
      theme,
      isAm,
      label,
      error,
      requiredInput,
      disabled,
      errorMessage,
      styles,
      showLabel = false,
      hourClock,
      customMins = [],
    } = this.props;
    const { open, time, addTime, hours, am, mins, isChanged } = this.state;
    return (
      <>
        <div
          className={classes.durationInputCnt}
          style={{ marginBottom: error == false ? 22 : 10, ...styles }}
          error={error}>
          {/* <FormControl
            style={{ marginBottom: error == false ? 22 : 10, ...styles, }}
            fullWidth={true}
            error={error}> */}
            <InputLabel classes={{ root: classes.dropdownsLabel }}>{label}</InputLabel>
            <div className={classes.multipleDropdownCnt}>
              {/* <Grid item xs={6}> */}
              <CreateableSelectDropdown
                data={() => {
                  return hourClock && hourClock == 12 ? generateDuration12HoursData()
                    : hourClock == 100 ? generateDuration100HoursData()
                      : hourClock == 168 ? generateDuration168HoursData()
                        : generateDuration24HoursData();
                }}
                label={showLabel ? "Hours" : ""}
                placeholder={
                  <FormattedMessage id="common.duration.hour.label" defaultMessage="Hours" />
                }
                name="hours"
                styles={{
                  flex: 1,
                  marginTop: 0,
                  marginBottom: 0,
                }}
                selectOptionAction={this.handleOptionsSelect}
                createOptionAction={this.handleCreateOption}
                removeOptionAction={this.handleRemoveOption}
                type="hours"
                createText=""
                isDisabled={disabled}
                customStyles={{
                  control: { borderRadius: "4px 0 0 4px" },
                }}
                selectedValue={hours}
                createLabelValidation={this.hoursValidation}
                validateValue={this.hoursValidation}
                isMulti={false}
                selectClear={this.handleClearSelect}
              />
              <SelectSearchDropdown
                data={() => {
                  return generateDurationMinsData(customMins);
                }}
                styles={{ flex: 1, marginTop: 0, marginBottom: 0, lineHeight: 1.0 }}
                // isClearable={true}
                label={showLabel ? "Minutes" : ""}
                selectChange={this.handleOptionsSelect}
                selectClear={this.handleClearSelect}
                type="mins"
                isDisabled={disabled}
                selectedValue={mins}
                placeholder={
                  <FormattedMessage id="common.duration.minute.label" defaultMessage="Minutes" />
                }
                isClearable={false}
                customStyles={{
                  control: { borderRadius: "0 4px 4px 0"},
                }}
                isMulti={false}
                validateValue={this.minsValidation}
              />
            </div>
            {error && (
              <FormHelperText classes={{ root: classes.errorText }}>{errorMessage}</FormHelperText>
            )}
          {/* </FormControl> */}
        </div>
      </>
    );
  }
}
DurationInput.defaultProps = {
  hourClock: 12,
};
export default withStyles(datePickerStyles, {
  withTheme: true,
})(DurationInput);
