import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import InputLabel from "@material-ui/core/InputLabel";
import Grid from "@material-ui/core/Grid";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import DatePicker from "react-datepicker";
import moment from "moment";
import "../../assets/css/react-datepicker.css";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import ClockIcon from "@material-ui/icons/AccessTime";
import DefaultButton from "../Buttons/DefaultButton";
import datePickerStyles from "../../assets/jss/components/datePicker";
import helper from "./../../helper/";
import InputAdornment from "@material-ui/core/InputAdornment";
import CalendarIcon from "@material-ui/icons/DateRange";
import CustomButton from "../Buttons/CustomButton";
import { FormattedMessage } from "react-intl";

let initialState = {
  actualDate: null,
  plannedDate: null,
  open: false,
  value: 0,
  timeInput: true,
  hours: "",
  mins: "",
  am: "AM",
  addTime: false,
  time: "",
  plannedHours: "",
  plannedMins: "",
  plannedAm: "AM",
  plannedAddTime: false,
  plannedTime: "",
  isLoaded: true,
  isPlanned: false,
  isActual: false
};
class CustomDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this.handleActualDate = this.handleActualDate.bind(this);
    this.handlePlannedDate = this.handlePlannedDate.bind(this);
    this.handleTabChange = this.handleTabChange.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleHoursInput = this.handleHoursInput.bind(this);
    this.handleAmChange = this.handleAmChange.bind(this);
    this.handleAddTimeClick = this.handleAddTimeClick.bind(this);
    this.handleTimeSave = this.handleTimeSave.bind(this);
    this.handleMinsInput = this.handleMinsInput.bind(this);
    this.handlePlanTimeSave = this.handlePlanTimeSave.bind(this);
    this.handlePlanAddTimeClick = this.handlePlanAddTimeClick.bind(this);
  }
  componentDidUpdate(prevProps) {
    if (this.props.viewType === "taskList") {
      if (prevProps.tasksState.data.length !== this.props.tasksState.data.length) {
        this.setState(initialState, () => {
          this.componentDidMount();
        });
      }
    }
    if (this.props.viewType === "issue") {
      if (prevProps.issuesState.data.length !== this.props.issuesState.data.length) {
        this.setState(initialState, () => {
          this.componentDidMount();
        });
      }
    }
  }
  componentDidMount() {
    if (this.props.startDateString || this.props.actualStartDateString) {
      this.setState({
        actualDate: this.props.actualStartDateString
          ? moment(this.props.actualStartDateString)
          : null,
        time: this.props.actualStartDateString
          ? moment(this.props.actualStartDateString).format("hh:mm A")
          : "",
        plannedDate: this.props.startDateString
          ? moment(this.props.startDateString)
          : null,
        plannedTime: this.props.startDateString
          ? moment(this.props.startDateString).format("hh:mm A")
          : ""
      });
    }

    if (this.props.dueDateString || this.props.actualDueDateString)
      this.setState({
        actualDate: this.props.actualDueDateString
          ? moment(this.props.actualDueDateString)
          : null,
        time: this.props.actualDueDateString
          ? moment(this.props.actualDueDateString).format("hh:mm A")
          : "",
        plannedDate: this.props.dueDateString
          ? moment(this.props.dueDateString)
          : null,
        plannedTime: this.props.dueDateString
          ? moment(this.props.dueDateString).format("hh:mm A")
          : ""
      });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.isLoaded) {
      if (nextProps.actualStartDateString || nextProps.startDateString) {
        return {
          actualDate: nextProps.actualStartDateString
            ? moment(nextProps.actualStartDateString)
            : null,
          // time: moment(nextProps.actualStartDateString).format("hh:mm:ss A"),
          plannedDate: nextProps.startDateString
            ? moment(nextProps.startDateString)
            : null
          // plannedTime: moment(nextProps.startDateString).format(
          //   "hh:mm:ss A"
          // ),
        };
      }
      if (nextProps.isDueDate) {
        if (nextProps.dueDateString || nextProps.actualDueDateString) {
          return {
            actualDate: nextProps.actualDueDateString
              ? moment(nextProps.actualDueDateString)
              : null,
            //   time: moment(nextProps.actualDueDateString).format("hh:mm:ss A"),
            plannedDate: nextProps.dueDateString
              ? moment(nextProps.dueDateString)
              : null
            //   plannedTime: moment(nextProps.dueDateString).format("hh:mm:ss A"),
          };
        } else {
          return { actualDate: null, plannedDate: null };
        }
      }
    }
    return null;
  }

  handleActualDate(date) {
    let isActual = false;
    if (this.state.actualDate !== date) {
      isActual = true;
    }
    this.setState({
      actualDate: date,
      isLoaded: false,
      isActual
    });
  }
  handlePlannedDate(date) {
    let isPlanned = false;
    if (this.state.plannedDate !== date) {
      isPlanned = true;
    }
    this.setState({
      plannedDate: date,
      isLoaded: false,
      isPlanned
    });
  }
  handleTabChange = (event, value) => {
    event.stopPropagation();
    this.setState({ value });
  };
  handleToggle = e => {
    e.stopPropagation();
    const { permission,view } = this.props;
    const condition = !permission;
    if (condition && view==="issue") {
      return;
    }
    this.setState(state => ({ open: !state.open }));
  };
  handleClose = event => {
    //Need to be optimized
    let completeDateFormat = this.state.actualDate
      ? helper.RETURN_CUSTOMDATEFORMAT(this.state.actualDate, this.state.time)
      : null;
    let completePlannedDate = this.state.plannedDate
      ? helper.RETURN_CUSTOMDATEFORMAT(
          this.state.plannedDate,
          this.state.plannedTime
        )
      : null;
    if (this.props.selectedIdsList && this.props.selectedIdsList.length) {
      if (this.props.startDate) {
        if (completeDateFormat || completePlannedDate) {
          this.props.handleStartDateClose(
            completeDateFormat,
            completePlannedDate,
            this.props.selectedIdsList,
            this.state.isPlanned,
            this.state.isActual
          );
          this.setState({
            isActual: false,
            isPlanned: false
          });
        }
      } else {
        if (completeDateFormat || completePlannedDate) {
          this.props.handleDueDateClose(
            completeDateFormat,
            completePlannedDate,
            this.props.selectedIdsList,
            this.state.isPlanned,
            this.state.isActual
          );
          this.setState({
            isActual: false,
            isPlanned: false
          });
        }
      }
    } else {
      if (
        completeDateFormat !== this.props.actualDueDateString ||
        completePlannedDate !== this.props.dueDateString
      ) {
        this.props.selectedDate(
          completeDateFormat,
          this.props.startDate,
          completePlannedDate,
          this.props.taskData,
          this.state.isPlanned,
          this.state.isActual
        );
        this.setState({
          isActual: false,
          isPlanned: false
        });
      }
    }

    this.setState({ open: false });
  };
  handleHoursInput(event) {
    const value = event.currentTarget.value;
    if (
      (/^[0-9]$|^0[1-9]$|^1[0-2]$/.test(parseInt(value)) &&
        value.length <= 2 &&
        value !== "00" &&
        /^\d+$/.test(value)) ||
      !value
    ) {
      if (event.currentTarget.name == "plannedHours") {
        this.setState({ plannedHours: value });
      } else {
        this.setState({ hours: value });
      }
    }
  }
  handleMinsInput(event) {
    const value = event.currentTarget.value;
    if (
      (/^[0-9]$|^0[1-9]$|^[1-5][0-9]$|^60$/.test(parseInt(value)) &&
        value.length <= 2 &&
        /^\d+$/.test(value)) ||
      !value
    ) {
      if (event.currentTarget.name == "plannedMins") {
        this.setState({ plannedMins: value });
      } else {
        this.setState({ mins: value });
      }
    }
  }
  handleAmChange(event) {
    if (event.target.name == "plannedAm") {
      this.setState(prevState => ({
        plannedAm: prevState.plannedAm == "AM" ? "PM" : "AM"
      }));
    }
    this.setState(prevState => ({ am: prevState.am == "AM" ? "PM" : "AM" }));
  }
  handleAddTimeClick(e) {
    e.stopPropagation();
    this.setState(prevState => ({ addTime: !prevState.addTime }));
  }
  handlePlanAddTimeClick(e) {
    e.stopPropagation();
    this.setState(prevState => ({ plannedAddTime: !prevState.plannedAddTime }));
  }
  handleTimeSave(e) {
    e.stopPropagation();
    const { hours, mins, am } = this.state;
    const time = `${hours.length < 2 ? `0${hours}` : hours}:${
      mins.length < 2 ? `0${mins}` : !mins ? "00" : mins
    } ${am}`;
    if (hours) {
      this.setState({ time, addTime: false, isActual: true });
    }
  }
  handlePlanTimeSave(e) {
    e.stopPropagation();
    const { plannedHours, plannedMins, plannedAm } = this.state;
    const time = `${
      plannedHours.length < 2 ? `0${plannedHours}` : plannedHours
    }:${
      plannedMins.length < 2
        ? `0${plannedMins}`
        : !plannedMins
        ? "00"
        : plannedMins
    } ${plannedAm}`;
    if (plannedHours) {
      this.setState({
        plannedTime: time,
        plannedAddTime: false,
        isPlanned: true
      });
    }
  }
  render() {
    const {
      classes,
      theme,
      closeAction,
      type,
      popperProps,
      minDates,
      maxDates,
      showTime,
      view,
      permission
    } = this.props;
    const { actualDate, plannedDate, value, open, time } = this.state;
    const defaultProps = popperProps || { open: open };
    return (
      <div
        onClick={e => {
          e.stopPropagation();
        }}
      >
        {type == "selectDatePicker" ? (
          <div style={{ marginTop: 10 }}>
            <InputLabel classes={{ root: classes.dropdownsLabel }}>
              {this.props.InputLabel}
            </InputLabel>
            <OutlinedInput
              labelWidth={150}
              notched={false}
              disabled={view==="issue"?!permission:false}
              readOnly
              fullWidth
              inputRef={node => {
                this.anchorEl = node;
              }}
              value={
                actualDate
                  ? `${moment(actualDate).format("MMM Do, YYYY")}`
                  : <FormattedMessage id="common.date.placeholder" defaultMessage="Select Date" />
              }
              endAdornment={
                <InputAdornment variant="filled" position="end">
                  <CalendarIcon
                    htmlColor={theme.palette.secondary.light}
                    style={{ cursor: "auto" }}
                  />
                </InputAdornment>
              }
              onClick={e => this.handleToggle(e)}
              classes={{
                root: classes.outlinedInputCnt,
                input: classes.outlinedInput,
                notchedOutline: classes.notchedOutlineCnt,
                focused: classes.outlineInputFocus
              }}
              placeholder="Add a start date"
            />
          </div>
        ) : type == "SelectionOnly" ? null : (
          <CustomButton
          buttonRef={node => {
            this.anchorEl = node;
          }}
          variant="text"
          btnType="plain"
          labelAlign="left"
          onClick={e => this.handleToggle(e)}
            buttonRef={node => {
              this.anchorEl = node;
            }}
        >
          {
              actualDate
                ? `${moment(actualDate).format("MMM Do, YYYY")} ${
                    time && !showTime ? `- ${time}` : ""
                  }`
                : <u><FormattedMessage id="common.date.placeholder" defaultMessage="Select Date" /></u>
            }
        </CustomButton>

        )}
        <Popper
          transition
          disablePortal={false}
          placement="bottom-start"
          className={classes.onlyDatePopper}
          anchorEl={this.anchorEl}
          {...defaultProps}
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom"
              }}
            >
              <div>
                <ClickAwayListener
                  onClickAway={closeAction || this.handleClose}
                >
                  <Grid container>
                    <Grid item>
                      <Tabs
                        value={value}
                        onChange={this.handleTabChange}
                        classes={{
                          root: classes.menuTabCnt,
                          indicator: classes.menuTabIndicator
                        }}
                      >
                        <Tab
                          label="Actual"
                          classes={{
                            root: classes.menuTab,
                            selected: classes.menuTabSelected
                          }}
                        />
                        <Tab
                          label="Planned"
                          classes={{
                            root: classes.menuTab,
                            selected: classes.menuTabSelected
                          }}
                        />
                      </Tabs>
                      {value === 1 && (
                        <div>
                          <DatePicker
                            inline
                            selected={plannedDate ? plannedDate : null}
                            onChange={this.handlePlannedDate}
                            minDate={minDates ? minDates.planned : undefined}
                            maxDate={maxDates ? maxDates.planned : undefined}
                            showDisabledMonthNavigation
                          />
                        </div>
                      )}
                      {value === 0 && (
                        <div>
                          <DatePicker
                            inline
                            selected={actualDate ? actualDate : null}
                            onChange={this.handleActualDate}
                            //   minDate={minDates ? minDates.actual : undefined}
                            // maxDate={maxDates ? maxDates.actual : undefined}
                            showDisabledMonthNavigation
                          />
                        </div>
                      )}
                    </Grid>
                    {/* {showTime ? value === 0 ? (
                        <DateAndTimeCnt type="planned" />
                      ) : (
                        <DateAndTimeCnt />
                      )}
                       */}
                  </Grid>
                </ClickAwayListener>
              </div>
            </Grow>
          )}
        </Popper>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tasksState: state.tasks,
    issuesState:state.issues
  }
}

export default compose(
  withRouter,
  withStyles(datePickerStyles, {
    withTheme: true
  }),
  connect(
    mapStateToProps,
    {}
  )
)(CustomDatePicker);
