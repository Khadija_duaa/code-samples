import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import DatePicker from "react-datepicker";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import moment from "moment";
import "../../assets/css/react-datepicker.css";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import datePickerStyles from "../../assets/jss/components/datePicker";
import DefaultButton from "../Buttons/DefaultButton";
import DefaultTextField from "../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import CalendarIcon from "@material-ui/icons/DateRange";
import CustomTooltip from "../Tooltip/Tooltip";
import CustomButton from "../Buttons/CustomButton";
import IconButton from "../Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import {FormattedDate} from 'react-intl';
import SvgIcon from "@material-ui/core/SvgIcon";

class SingleDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: props.selectedDate,
      open: false,
      isEmpty: false,
      isChanged: true
    };
    this.dateSelect = this.dateSelect.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  componentDidUpdate(prevProps) {
  }
  componentDidMount = () => {
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      selectedDate: nextProps.selectedDate ? nextProps.selectedDate : ''
    };
  }

  dateSelect(date) {
    this.props.selectedSaveDate(date);
    this.setState({ open: false })
  }
  handleToggle = event => {
      this.setState(state => ({ open: !state.open }));
  };
  handleClose() {
    this.setState({ open: false }, () => {
      if (this.props.selectedSaveDate) {
          this.props.selectedSaveDate(this.state.selectedDate);
      }
    });
  }
  deleteDate = () => {
    this.props.selectedSaveDate('');
  }
  isExpire = date => {
    const itemDate = moment(new Date(date), 'MMMM DD, YYYY');
    const currentDate = moment().format('MMMM DD, YYYY'); // moment(new Date(), 'MMMM DD, YYYY');
    const diff = itemDate.diff(currentDate, 'days');
    return diff < 0;
  };
  render() {
    const {
      classes,
      theme,
      children,
      label,
      placeholder,
      style,
      dateFormat,
      removeable = true,
      isArchivedSelected,
      iconBtn
    } = this.props;
    const { open, selectedDate } = this.state;
    
    let isEmpty = (selectedDate == '' || selectedDate == null) ? true : false;
    let dateValue = isEmpty ? placeholder : moment(selectedDate).format(dateFormat);
    let dateCnt = isEmpty ? classes.emptyDate : classes.selectionDate;
    let dateLblStyle = this.isExpire(selectedDate) ? classes.expiryDate : {};
    let formattedDate = !isEmpty ? moment(selectedDate) : moment();
    return (
      <div className="staticDatePicker" style={style}>
        {<div className={dateCnt}>
        <IconButton
              classes={{ root: classes.datePickerIconBtn }}
              // disabled={isArchivedSelected || disabled} // disable button if archive is selected
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
              onClick={this.handleClick}
            >
               {/* <SvgIcon
            viewBox="0 0 26.805 24"
            classes={{ root: classes.addAssigneeBtnIcon }}
          >
            <AssigneeIcon />
          </SvgIcon> */}
           
            </IconButton>
          <CustomButton
            onClick={event => this.handleToggle(event)}
            btnType="plain"
            disableRipple
            buttonRef={node => {
              this.anchorEl = node;
            }}
            variant="text"
            disabled = {isArchivedSelected}
          >
            <span className={dateLblStyle}>{!isEmpty ? <FormattedDate value={selectedDate} month="short" day="numeric" /> : dateValue }</span>
          </CustomButton>
          {!isEmpty && removeable ? <div className={classes.CloseIconBtn}>
            <IconButton
              btnType="smallFilledGray"
              onClick={event =>
                this.deleteDate(event)
              }
            >
              <CloseIcon
                htmlColor={theme.palette.common.white}
                style={{ fontSize: "14px" }}
              />
            </IconButton>
          </div> : null}
        </div>}
        <Popper
          open={open}
          transition
          disablePortal
          anchorEl={this.anchorEl}
          className={classes.StaticDatePickerPopper}
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom"
              }}
            >
              <div
                onClick={event => {
                  event.stopPropagation();
                }}
                className={classes.rangeDatePickerInner}
              >
                <ClickAwayListener onClickAway={this.handleClose}>
                  <DatePicker
                    inline
                    selected={isEmpty ? null : formattedDate}
                    onChange={this.dateSelect}
                    minDate={this.props.minDate ? moment() : undefined}
                  />
                </ClickAwayListener>
              </div>
            </Grow>
          )}
        </Popper>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    meetingsState: state.meetings
  };
};

export default compose(
  withRouter,
  withStyles(datePickerStyles, {
    withTheme: true
  }),
  connect(mapStateToProps, {})
)(SingleDatePicker);
