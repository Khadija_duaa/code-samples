import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import DatePicker from "react-datepicker";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import moment from "moment";
import "../../assets/css/react-datepicker.css";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import datePickerStyles from "../../assets/jss/components/datePicker";
import DefaultButton from "../Buttons/DefaultButton";
import DefaultTextField from "../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import CalendarIcon from "@material-ui/icons/DateRange";
import CustomTooltip from "../Tooltip/Tooltip";
import CustomButton from "../Buttons/CustomButton";
import { injectIntl } from 'react-intl';
class StaticDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: moment(),
      open: false,
      isEmpty: false,
      isChanged: true
    };
    this.dateSelect = this.dateSelect.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  componentDidUpdate(prevProps) {
    const { selectedDate } = this.props;
    if (this.props.view === "meetingList") {
      if (
        prevProps.meetingsState.data.length !==
        this.props.meetingsState.data.length
      ) {
        this.setState(
          {
            selectedDate: moment(),
            open: false,
            isEmpty: false,
            isChanged: true
          },
          () => {
            this.componentDidMount();
          }
        );
      }
    }
  }
  componentDidMount = () => {
    if (this.props.view === "meeting-endBy") {
      this.setState({
        selectedDate: moment().add(5, "days")
      });
    } else {
      this.setState({
        selectedDate: this.props.date ? this.props.date : moment()
      });
    }
  };
  getIntl() {
    return this.props.intl;
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.isChanged) {
      if (nextProps.isFollowUp) {
        return {
          selectedDate: moment(nextProps.data.dueDate),
          isEmpty: false
        };
      } else if (nextProps.data) {
        if (nextProps.data && nextProps.data.startDate) {
          return {
            selectedDate: moment(nextProps.data.startDate),
            isEmpty: false
          };
        } else {
          return { selectedDate: nextProps.placeholder ? nextProps.placeholder : nextProps.intl.formatMessage({ id: "common.date.placeholder", defaultMessage: "Select Date" }), isEmpty: true };
        }
      } else if (nextProps.preFilledData) {
        return {
          selectedDate: moment(nextProps.preFilledData.meetingDate)
        };
      } else {
        if (nextProps.view === "meeting-endBy") {
          return {
            selectedDate:
              nextProps.currentMeeting && nextProps.currentMeeting.endDateString
                ? moment(nextProps.currentMeeting.endDateString)
                : moment().add(1, "days")
          };
        }
        return {
          selectedDate: nextProps.date ? nextProps.date : moment(),
          isEmpty: false
        };
      }
    } else {
      return { selectedDate: prevState.selectedDate };
    }
  }

  dateSelect(date) {
    const { onDateSelect, handleDropDown, view } = this.props;

    this.setState({ selectedDate: date, isChanged: false, isEmpty: false });
    if (!view || view !== "IssueForm") {
      if (onDateSelect) { this.props.date ? onDateSelect(date) : null }
    } else if (handleDropDown) {
      //IssueForm function handleDD
      handleDropDown({ date: date });
    }
  }
  handleToggle = event => {
    if (this.props.isDateNull) {
      const data = this.props.data;
      this.props.showDateTimeDetailsPopUp(event, data);
    } else {
      event.stopPropagation();
      this.setState(state => ({ open: !state.open }));
    }
  };
  handleClose() {
    this.setState({ open: false }, () => {
      if (this.props.updateDate) {
        this.props.updateDate(moment(this.state.selectedDate), this.props.data, true);
      }
      if (this.props.selectedSaveDate) {
        if (this.props.isCreation)
          this.props.selectedSaveDate(moment(this.state.selectedDate));
        else
          this.props.selectedSaveDate(moment(this.state.selectedDate), this.props.data);
      }
    });
  }
  render() {
    const {
      classes,
      theme,
      children,
      label,
      placeholder,
      isInput,
      requiredInput,
      isDisabled,
      view,
      helptext,
      style,
      inputStyle,
      margin,
      size = "small"
    } = this.props;
    const { open, selectedDate, isEmpty } = this.state;
    const dateValue = () => {
      if (view === "IssueForm") {
        return moment(selectedDate).format("MMM DD, YYYY");
      } else
        return isEmpty ? selectedDate : moment(selectedDate).format("MMM DD, YYYY");
    };
    return (
      <div className="staticDatePicker" style={style}>
        {isInput ? (
          <div className={classes.staticDatePickerInputCnt} style={{ marginBottom: label ? 20 : 0, margin: margin && margin }}>
            <DefaultTextField
              label={
                label ? (
                  <>
                    {label}
                    {helptext && (<CustomTooltip helptext={helptext} iconType="help" />)}
                  </>
                ) : (
                  false
                )
              }
              fullWidth={true}
              error={false}
              formControlStyles={{ marginBottom: 0, marginTop: label ? "" : 0 }}
              requiredInput={requiredInput || false}
              defaultProps={{
                id: `DatePicker${label}`,
                readOnly: true,
                placeholder: placeholder,
                disabled: isDisabled || false,
                style: { paddingLeft: 0, color: 'red' },
                inputProps: { style: inputStyle || { padding: "11px 14px" } },
                value: dateValue(),
                inputRef: node => {
                  this.anchorEl = node;
                },
                startAdornment: (
                  <InputAdornment variant="filled" position="end">
                    <CalendarIcon
                      htmlColor={theme.palette.secondary.light}
                      style={{ cursor: "auto" }}
                      fontSize={"small"}
                    />
                  </InputAdornment>
                ),
                onClick: !isDisabled ? this.handleToggle : undefined
              }}
            />
          </div>
        ) : (
          <CustomButton
            onClick={event => this.handleToggle(event)}
            btnType="plain"
            disableRipple
            buttonRef={node => {
              this.anchorEl = node;
            }}
            variant="text"
          >
            {isEmpty ? selectedDate : moment(selectedDate).format("MMMM DD, YYYY")}
          </CustomButton>
        )}
        <Popper
          open={open}
          transition
          disablePortal
          anchorEl={this.anchorEl}
          className={classes.StaticDatePickerPopper}
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom"
              }}
            >
              <div
                onClick={event => {
                  event.stopPropagation();
                }}
              >
                <ClickAwayListener
                  mouseEvent="onMouseUp"
                  touchEvent="onTouchEnd"
                  onClickAway={this.handleClose}>
                  <DatePicker
                    inline
                    selected={isEmpty ? null : moment(selectedDate).toDate()}
                    onSelect={this.dateSelect}
                    minDate={this.props.minDate === true ? moment() : this.props.minDate ?? undefined}
                    maxDate={this.props.maxDate}
                  />
                </ClickAwayListener>
              </div>
            </Grow>
          )}
        </Popper>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    meetingsState: state.meetings
  };
};

export default compose(
  injectIntl,
  withRouter,
  withStyles(datePickerStyles, {
    withTheme: true
  }),
  connect(mapStateToProps, {})
)(StaticDatePicker);
