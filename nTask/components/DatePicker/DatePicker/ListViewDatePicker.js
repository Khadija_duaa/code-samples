// @flow

import React, { useState, useEffect, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import DatePicker from "react-datepicker";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import moment from "moment";
import 'react-datepicker/dist/react-datepicker.min.css';
// import "../../../assets/css/react-datepicker.css";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import datePickerStyles from "./style";
import DefaultButton from "../../Buttons/DefaultButton";
import DefaultTextField from "../../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import CalendarIcon from "@material-ui/icons/DateRange";
import CustomTooltip from "../../Tooltip/Tooltip";
import CustomButton from "../../Buttons/CustomButton";
import IconButton from "../../Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DropdownMenu from "../../Dropdown/DropdownMenu";
import IconDate from "../../Icons/IconDate";
import SvgIcon from "@material-ui/core/SvgIcon";
import { meridiemMaskInput, setCaretPosition } from "../../../utils/common";
import ClockIcon from "@material-ui/icons/AccessTime";
import helper from "../../../helper";
import isEmpty from "lodash/isEmpty";
import DeleteIcon from "@material-ui/icons/Cancel";



type DropdownProps = {
  date: String,
  classes: Object,
  theme: Object,
  placeholder: String,
  style: Object,
  dateFormat: String,
  disabled: Boolean,
  onSelect: Function,
  icon: Boolean,
  btnProps: Object,
  PopperProps: Object,
  btnType: String,
  timeInput: Boolean,
  label: String,
  datePickerProps: Object,
  selectedTime: String,
  timeInputLabel: String,
  timeDay: String,
  closeOnDateSelect: Boolean,
  deleteIcon: Boolean,
};

function CustomDatePicker(props: DropdownProps) {
  const {
    date,
    classes,
    theme,
    placeholder,
    style,
    dateFormat,
    disabled,
    icon,
    onSelect,
    PopperProps,
    btnProps,
    btnType,
    timeInput,
    label,
    datePickerProps,
    selectedTime,
    timeDay,
    timeInputLabel,
    deleteIcon,
    containerProps,
    deleteIconProps = {},
    labelProps = {},
    closeOnDateSelect = false,
  } = props;

  const [selectedDate, setSelectedDate] = useState("");
  const [anchorEl, setAnchorEl] = useState(null);
  const [time, setTime] = useState(selectedTime);  
  const btnRef = useRef(null);

  (selectedDate: String);
  (anchorEl: Boolean);

  useEffect(() => {
    btnRef.current && btnRef.current.addEventListener("click", handleClick, false);
    return () => {
      btnRef.current && btnRef.current.removeEventListener("click", handleClick, true);
    };
  }, []);

  useEffect(() => {
    setSelectedDate(date);
    setTime(selectedTime)
  }, [date]);

  const dateSelect = (date) => {
    setSelectedDate(date);
    let time = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    setTime(time);
  };

  const handleClick = (event) => {
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const handleClose = (event) => {
    setAnchorEl(null);
    if(closeOnDateSelect){
      onSelect(selectedDate,time);
      setAnchorEl(null);
    }
    onSelect(selectedDate,time);
  };
 
  const  isExpire = date => {
    if(!date) return false
    let targetDay = new Date(date);
    targetDay.setHours(0,0,0,0);
    let today = new Date();
    today.setHours(0,0,0,0);
    return targetDay < today ;
  };

  const handleClearDates = (e) => {
    e.stopPropagation();
    setAnchorEl(null);
    setSelectedDate(null);
    setTime(null);
    onSelect(null);
  };

  let dateValue = !selectedDate ? placeholder : moment(selectedDate).format(dateFormat);
  let isDateExpire = isExpire(selectedDate);
  let timeValue = time ? time : '' ;
  const open = Boolean(anchorEl);

  return (
    <div className="staticDatePicker" style={style}>
      <div className={classes.datePickerBtnCnt} {...containerProps}>
        {label ? (
          <>
            <SvgIcon viewBox="0 0 24 24" className={classes.dateIcon}>
              <IconDate />
            </SvgIcon>
            <span className={classes.datePickerLabel} {...labelProps}>
              {label}
            </span>
            <CustomButton
              buttonRef={node => {
                btnRef.current = node;
              }}
              btnType="plain"
              disableRipple
              labelAlign="left"
              variant="text"
              disabled={disabled}
              customClasses={{ text: classes.datePickerButton }}
              style={{
                background: "transparent",
                border: "none",
                padding: 0,
                textAlign: "left",
                ...style,
              }}
              {...btnProps}>
              {icon && (
                <SvgIcon viewBox="0 0 24 24" className={classes.dateIcon}>
                  <IconDate />
                </SvgIcon>
              )}
              <span className={classes.dateTimeValue}>
                {dateValue === "Select" ? (
                  <span style={{ color: "rgba(191,191,191,1)" }}>{dateValue}</span>
                ) : (
                  `${dateValue} ${timeInput ? timeValue : ''}`
                )}
              </span>
              {deleteIcon && dateValue !== "Select" && (
                <DeleteIcon
                  onClick={e => {
                    handleClearDates(e);
                  }}
                  htmlColor={theme.palette.error.main}
                  classes={{ root: classes.deleteIcon }}
                />
              )}
            </CustomButton>
          </>
        ) : btnType == "transparent" ? (
          <CustomButton
            btnType="plain"
            disableRipple
            variant="text"
            buttonRef={node => {
              btnRef.current = node;
            }}
            disabled={disabled}
            customClasses={{ text: classes.datePickerButton }}
            style={{
              background: "transparent",
              border: "none",
              padding: 0,
              ...style,
            }}
            {...btnProps}>
            {icon && (
              <SvgIcon viewBox="0 0 24 24" className={classes.dateIcon}>
                <IconDate />
              </SvgIcon>
            )}
            <span className={isDateExpire ? classes.dateExpire : ""}>{dateValue}{timeInput && dateValue != 'Select' ? ` ${timeValue}`: ''}</span>
          </CustomButton>
        ) : (
          <CustomButton
            buttonRef={node => {
              btnRef.current = node;
            }}
            btnType="plain"
            disableRipple
            variant="text"
            disabled={disabled}
            customClasses={{ text: classes.datePickerButton }}
            style={{
              border: open ? `1px solid ${theme.palette.border.blue}` : "",
              background: open ? "transparent" : "",
              ...style,
            }}
            {...btnProps}>
            {icon && (
              <SvgIcon viewBox="0 0 24 24" className={classes.dateIcon}>
                <IconDate />
              </SvgIcon>
            )}
            <span className={classes.dateTimeValue}>
              {dateValue === "Select" ? (
                <span style={{ color: "#7E7E7E" }}>{dateValue}</span>
              ) : (
                `${dateValue} ${timeInput ? timeValue : ''}`
              )}
            </span>
            {deleteIcon && dateValue !== "Select" && (
              <DeleteIcon
                onClick={e => {
                  handleClearDates(e);
                }}
                htmlColor={theme.palette.error.main}
                classes={{ root: classes.deleteIcon }}
                {...deleteIconProps}
              />
            )}
          </CustomButton>
        )}
      </div>

      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size="xmedium"
        placement="bottom-center"
        paperProps={{className: classes.datePickerShadow }}
        {...PopperProps}>
        <div
          onClick={event => {
            event.stopPropagation();
          }}
          className={`${classes.rangeDatePickerInner} staticDatePicker`}>
          {/* <ClickAwayListener onClickAway={handleClose}> */}
          <DatePicker
            inline
            selected={selectedDate ? moment(selectedDate).toDate() : moment().toDate() }
            onChange={dateSelect}
            timeInputLabel={timeInputLabel}
            showTimeInput={timeInput && selectedDate}
            {...datePickerProps}
          />
          {/* </ClickAwayListener> */}
        </div>
      </DropdownMenu>
    </div>
  );
}

CustomDatePicker.defaultProps = {
  date: "",
  placeholder: "Select",
  style: {},
  dateFormat: "ll",
  disabled: false,
  onSelect: () => {},
  icon: true,
  PopperProps: {},
  btnType: "",
  timeInput: false,
  label: "",
  selectedTime: "",
  timeInputLabel: "Start Time",
  timeDay: "AM",
  deleteIcon: false,
  closeOnDateSelect: false,
  containerProps: {},
};
const mapStateToProps = state => {
  return {
    meetingsState: state.meetings,
  };
};

export default compose(
  withRouter,
  withStyles(datePickerStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {})
)(CustomDatePicker);
