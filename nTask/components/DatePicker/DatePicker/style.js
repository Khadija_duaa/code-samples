const datePickerStyles = (theme) => ({
  datePickerButton: {
    color: theme.palette.text.primary,
    padding: "3px 12px",
    background: theme.palette.background.items,
    borderRadius: 30,
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    lineHeight: 1.3,
    fontWeight: theme.typography.fontWeightLight,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    textTransform: "none",
    "&:hover": {
      background: "transparent",
      border: `1px solid ${theme.palette.border.blue}`,
    },
    "&:hover $deleteIcon": {
      visibility: "visible",
    },
  },
  datePickerBtnCnt: {
    display: "flex",
    alignItems: "flex-start",
  },
  dateIcon: {
    fontSize: "20px !important",
    marginRight: 2,
    marginBottom:-3
  },
  timeCnt: {
    width: 236,
    background: theme.palette.common.white,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 10,
  },
  timeCntList: {
    // width: 236,
    background: theme.palette.common.white,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "10px 10px",
  },

  timeInputLabel: {
    minWidth: 72,
    fontSize: "11px !important",
  },
  timeInputLabelList: {
    minWidth: 55,
    fontSize: "11px !important",
    marginRight:10
  },
  amPmBtn: {
    position: "absolute",
    zIndex: 1,
    right: 35,
    borderRadius: "0 4px 4px 0",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 5,
    cursor: "pointer",
    fontSize: "12px !important",
  },
  amPmBtnList: {
    position: "absolute",
    zIndex: 1,
    right: 92,
    borderRadius: "0 4px 4px 0",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 5,
    cursor: "pointer",
    fontSize: "12px !important",
  },
  addTimeBtnCnt: {
    display: "flex",
    // padding: "5px 5px",
    cursor: "pointer",
    // borderRadius: 20,
    // justifyContent: "space-between"
  },
  addTimeBtnCntList: {
    display: "flex",
    // padding: "5px 5px",
    cursor: "pointer",
    borderRadius: 20,
    justifyContent: "space-between"
  },
  addTimeBtnText: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    lineHeight: "20px !important",
    marginLeft: 5,
    textDecoration: "underline",
  },
  addTimeBtnTextList: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    lineHeight: "20px !important",
    // marginLeft: 5,
    textDecoration: "underline",
    display: "flex"
  },
  datePickerLabel: {
    fontSize: "13px !important",
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLatoWithoutImportant,
    width: 85,
    marginTop: 2,
  },
  dateTimeValue: {
    fontSize: "13px !important",
    marginTop: 2,
  },
  deleteIcon: {
    fontSize: "18px !important",
    visibility: "hidden",
  },
  dateExpire: {
    color: `${theme.palette.text.danger} !important`,
    minWidth: 48,
    textAlign: "center",
  },
  saveBtnContainer:{
    display: "flex",
    justifyContent: "end",    
    paddingBottom: 10,
    paddingRight: 10,
  },
  timeInputCntlist:{
    justifyContent: "space-between"
  },
  datePickerShadow: {
    boxShadow: 'none',
    background:'transparent'
  },
});

export default datePickerStyles;
