// @flow
import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import DatePicker from "react-datepicker";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import moment from "moment";
import 'react-datepicker/dist/react-datepicker.min.css';
// import "../../../assets/css/react-datepicker.css";
import datePickerStyles from "./style";
import CustomButton from "../../Buttons/CustomButton";
import DropdownMenu from "../../Dropdown/DropdownMenu";
import IconDate from "../../Icons/IconDate";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "@material-ui/icons/Cancel";
type DropdownProps = {
  date: String,
  classes: Object,
  theme: Object,
  placeholder: String,
  style: Object,
  dateFormat: String,
  disabled: Boolean,
  onSelect: Function,
  icon: Boolean,
  btnProps: Object,
  PopperProps: Object,
  btnType: String,
  timeInput: Boolean,
  label: String,
  datePickerProps: Object,
  selectedTime: String,
  timeInputLabel: String,
  timeDay: String,
  deleteIcon: Boolean,
  closeOnDateSelect: Boolean
};
function CustomDatePicker(props: DropdownProps) {
  const {
    date,
    classes,
    theme,
    placeholder,
    style,
    dateFormat,
    disabled,
    icon,
    onSelect,
    PopperProps,
    btnProps,
    btnType,
    timeInput,
    label,
    datePickerProps,
    selectedTime,
    timeDay,
    timeInputLabel,
    deleteIcon,
    labelProps={},
    containerProps={},
    closeOnDateSelect= false,
    deleteIconProps={},
    deleteIconWrapperProps={},
  } = props;
  const [selectedDate, setSelectedDate] = useState('');
  const [anchorEl, setAnchorEl] = useState(null);
  const [time, setTime] = useState(selectedTime);
  (selectedDate: String);
  (anchorEl: Boolean);
  useEffect(() => {
    setSelectedDate(date);
    setTime(selectedTime)
  }, [date]);
  const dateSelect = (date) => {
    setSelectedDate(date);
    let time = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    setTime(time);
  };
  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = (event) => {
    setAnchorEl(null);
    if(closeOnDateSelect){
      onSelect(selectedDate,time);
      setAnchorEl(null);
    }
    onSelect(selectedDate,time);
  };
  const  isExpire = date => {
    if(!date) return false
    let targetDay = new Date(date);
    targetDay.setHours(0,0,0,0);
    let today = new Date();
    today.setHours(0,0,0,0);
    return targetDay < today ;
  };
  const handleClearDates = (e) => {
    e.stopPropagation();
    setAnchorEl(null);
    setSelectedDate(null);
    setTime(null);
    onSelect(null);
  };
  let dateValue = !selectedDate ? placeholder : moment(selectedDate).format(dateFormat);
  let isDateExpire = isExpire(selectedDate);
  let timeValue = time ? time : '' ;
  const open = Boolean(anchorEl);
  return (
    <div className="staticDatePicker" style={style}>
      <div class={classes.datePickerBtnCnt} {...containerProps}>
        {label ? (
          <>
            <SvgIcon viewBox="0 0 24 24" className={classes.dateIcon}>
              <IconDate />
            </SvgIcon>
            <span className={classes.datePickerLabel} {...labelProps}>{label}</span>
            <CustomButton
              onClick={(event) => handleClick(event)}
              btnType="plain"
              disableRipple
              labelAlign="left"
              variant="text"
              disabled={disabled}
              customClasses={{ text: classes.datePickerButton }}
              style={{
                background: "transparent",
                border: "none",
                padding: 0,
                textAlign: "left",
                ...style,
              }}
              {...btnProps}
            >
              {icon && (
                <SvgIcon viewBox="0 0 24 24" className={classes.dateIcon}>
                  <IconDate />
                </SvgIcon>
              )}
              <span className={classes.dateTimeValue}>
              {dateValue === "Select Date" ? (
                <span style={{ color: "#7E7E7E" }}>{dateValue}</span>
              ) : (
                `${dateValue} ${timeInput ? timeValue : ''}`
              )}
              </span>
              {deleteIcon && dateValue !== "Select Date" && (
                <DeleteIcon
                  onClick={(e) => {
                    handleClearDates(e);
                  }}
                  htmlColor={theme.palette.error.main}
                  classes={{ root: classes.deleteIcon }}
                />
              )}
            </CustomButton>
          </>
        ) : btnType == "transparent" ? (
          <CustomButton
            onClick={(event) => handleClick(event)}
            btnType="plain"
            disableRipple
            variant="text"
            disabled={disabled}
            customClasses={{ text: classes.datePickerButton }}
            style={{
              background: "transparent",
              border: "none",
              padding: 0,
              ...style,
            }}
            {...btnProps}
          >
            {icon && (
              <SvgIcon viewBox="0 0 24 24" className={classes.dateIcon}>
                <IconDate />
              </SvgIcon>
            )}
            <span className={isDateExpire ? classes.dateExpire : ""}>{dateValue}{(timeInput && dateValue != 'Select Date' ) ? ` ${timeValue}`: ''}</span>
          </CustomButton>
        ) : (
          <CustomButton
            onClick={(event) => handleClick(event)}
            btnType="plain"
            disableRipple
            variant="text"
            disabled={disabled}
            customClasses={{ text: classes.datePickerButton }}
            style={{
              border: open ? `1px solid ${theme.palette.border.blue}` : "",
              background: open ? "transparent" : "",
              ...style,
            }}
            {...btnProps}
          >
            {icon && (
              <SvgIcon viewBox="0 0 24 24" className={classes.dateIcon}>
                <IconDate />
              </SvgIcon>
            )}
            <span className={classes.dateTimeValue}>
              {dateValue === "Select Date" ? (
                <span style={{ color: "#7E7E7E" }}>{dateValue}</span>
              ) : (
                `${dateValue} ${timeInput ? timeValue : ''}`
              )}
            </span>
            {deleteIcon && dateValue !== "Select Date" && (
              <span data-rowClick="cell" {...deleteIconWrapperProps}>
                <DeleteIcon
                  onClick={e => {
                    handleClearDates(e);
                  }}
                  htmlColor={theme.palette.error.main}
                  classes={{ root: classes.deleteIcon }}
                  {...deleteIconProps}
                />
              </span>
            )}
          </CustomButton>
        )}
      </div>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size="xmedium"
        placement="bottom-center"
        paperProps={{className: classes.datePickerShadow }}
        {...PopperProps}
      >
        <div
          onClick={(event) => {
            event.stopPropagation();
          }}
          className={`${classes.rangeDatePickerInner} staticDatePicker`}
        >
          <DatePicker
            inline
            selected={selectedDate ? moment(selectedDate).toDate() : moment().toDate() }
            onChange={dateSelect}
            timeInputLabel={timeInputLabel}
            showTimeInput={timeInput && selectedDate}
            {...datePickerProps}
          />
        </div>
      </DropdownMenu>
    </div>
  );
}
CustomDatePicker.defaultProps = {
  date: "",
  placeholder: "Select Date",
  style: {},
  dateFormat: "ll",
  disabled: false,
  onSelect: () => {},
  icon: true,
  PopperProps: {},
  btnType: "",
  timeInput: false,
  label: "",
  selectedTime: "",
  timeInputLabel: "Start Time",
  timeDay: "AM",
  deleteIcon: false,
  closeOnDateSelect: false,
};
const mapStateToProps = (state) => {
  return {
    meetingsState: state.meetings,
  };
};
export default compose(
  withRouter,
  withStyles(datePickerStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {})
)(CustomDatePicker);