import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import DatePicker from "react-datepicker";
import moment from "moment";
import isEqual from "lodash/isEqual";
import "../../assets/css/react-datepicker.css";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import datePickerStyles from "../../assets/jss/components/datePicker";
import DefaultTextField from "../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import CalendarIcon from "@material-ui/icons/DateRange";
import CustomButton from "../Buttons/CustomButton";
import helper from "./../../helper/";
import ClockIcon from "@material-ui/icons/AccessTime";

import { meridiemMaskInput, setCaretPosition } from "../../utils/common";
import { FormattedMessage,injectIntl } from "react-intl";

class CustomRangeDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      endDate: "",
      startDate: "",
      open: false,
      startTime: "00:00",
      endTime: "00:00",
      startTimeDayPeriod: "AM",
      endTimeDayPeriod: "AM",
      addTime: false,
    };
    this.initialState = this.state;
  }
  componentDidMount() {
    const { startTime, endTime } = this.props;
    this.setState(
      {
        startDate: this.props.startDate ? moment(this.props.startDate) : "",
        endDate: this.props.endDate ? moment(this.props.endDate) : "",
        startTime: startTime
          ? startTime.substr(0, startTime.indexOf(" "))
          : "00:00",
        endTime: this.props.endTime
          ? endTime.substr(0, endTime.indexOf(" "))
          : "00:00",
        startTimeDayPeriod: startTime
          ? startTime.substr(startTime.indexOf(" ") + 1)
          : "AM",
        endTimeDayPeriod: endTime
          ? endTime.substr(endTime.indexOf(" ") + 1)
          : "AM",
        addTime: startTime || endTime,
      },
      () => {
        this.initialState = this.state;
      }
    );
  }
  componentDidUpdate(prevProps) {
    const { startDate, endDate, startTime, endTime } = this.props;

    const customFields = (({ startDate, endDate, startTime, endTime }) => ({
      startDate,
      endDate,
      startTime,
      endTime,
    }))(this.props);

    const customFieldsOld = (({ startDate, endDate, startTime, endTime }) => ({
      startDate,
      endDate,
      startTime,
      endTime,
    }))(prevProps);

    if (JSON.stringify(customFieldsOld) !== JSON.stringify(customFields)) {
      this.setState(
        {
          startDate: startDate ? moment(startDate) : "",
          endDate: endDate ? moment(endDate) : "",
          startTime: startTime
            ? startTime.substr(0, startTime.indexOf(" "))
            : "00:00",
          endTime: endTime ? endTime.substr(0, endTime.indexOf(" ")) : "00:00",
          startTimeDayPeriod: startTime
            ? startTime.substr(startTime.indexOf(" ") + 1)
            : "AM",
          endTimeDayPeriod: endTime
            ? endTime.substr(endTime.indexOf(" ") + 1)
            : "AM",
          addTime: startTime || endTime,
        },
        () => {
          this.initialState = this.state;
        }
      );
    }
  }
  isDataChanged = () => {
    const {
      open: oldOpen,
      addTime: oldAddTime,
      endDate: initialEndDate,
      startDate: initialStartDate,
      ...initialData
    } = this.initialState;
    const { open, addTime, endDate, startDate, ...stateNow } = this.state;
    const checkInitEndDate = initialEndDate
      ? moment(initialEndDate).isSame(moment(endDate))
      : endDate
      ? false
      : initialEndDate === endDate;
    const checkInitStartDate = initialStartDate
      ? moment(initialStartDate).isSame(moment(startDate))
      : startDate
      ? false
      : initialStartDate === startDate;
    if (
      isEqual(initialData, stateNow) &&
      checkInitEndDate &&
      checkInitStartDate
    ) {
      return false;
    } else {
      return true;
    }
  };
  handleChangeEnd = (date) => {
    const startDate = this.state.startDate;
    if (!startDate) {
      this.setState({ startDate: date });
    } else if (moment(date).diff(startDate, "days") < 0) {
      this.setState({ startDate: date });
    } else {
      this.setState({ endDate: date });
    }
  };
  dateSelect = (date) => {
    const { startDate } = this.state;
    const prevStartDate = startDate ? moment(startDate).format("MMM Do YY") : "";
    if (prevStartDate == moment(date).format("MMM Do YY")) {
      this.setState({ endDate: "", startDate: "" });
    }
  };
  handleToggle = (e) => {
    e.stopPropagation();
    const { permission } = this.props;
    if (!permission) this.setState((state) => ({ open: !state.open }));
  };
  handleClose = (event) => {
    const {
      startDate,
      endDate,
      startTime,
      endTime,
      startTimeDayPeriod,
      endTimeDayPeriod,
    } = this.state;
    let start = startDate ? moment(this.state.startDate) : null;
    let end = endDate ? moment(this.state.endDate) : null;
    if (start && startTime) {
      start = helper.RETURN_CUSTOMDATEFORMAT(
        moment(startDate),
        `${startTime} ${startTimeDayPeriod}`
      );
    }
    if (end && endTime) {
      end = helper.RETURN_CUSTOMDATEFORMAT(
        moment(endDate),
        `${endTime} ${endTimeDayPeriod}`
      );
    }
    // If time range is changed in calender than update date
    if (this.isDataChanged()) {
      this.props.dateSaveAction(
        start,
        end,
        `${startTime ? startTime + " " + startTimeDayPeriod : ""}`,
        `${endTime ? endTime + " " + endTimeDayPeriod : ""}`
      );
    }
    this.setState({ open: false });
  };
  handleEndTimeChange = (event, oldValue) => {
    const inputRef = event.target;
    let cursorPosition = event.target.selectionStart;
    let inputData = meridiemMaskInput(
      event.target.value,
      oldValue,
      cursorPosition
    );
    let value = inputData.value;
    cursorPosition = inputData.cursorPosition;

    this.setState({ endTime: value }, () => {
      setCaretPosition(inputRef, cursorPosition);
    });
  };
  handleStartTimeChange = (event, oldValue) => {
    const inputRef = event.target;
    let cursorPosition = event.target.selectionStart;
    let inputData = meridiemMaskInput(
      event.target.value,
      oldValue,
      cursorPosition
    );
    let value = inputData.value;
    cursorPosition = inputData.cursorPosition;

    this.setState({ startTime: value }, () => {
      setCaretPosition(inputRef, cursorPosition);
    });
  };
  handleAmPmChange = (name) => {
    if (name)
      this.setState({
        [name]: this.state[name] == "AM" ? "PM" : "AM",
      });
  };
  handleAddTimeClick = () => {
    this.setState({ addTime: true });
  };
  render() {
    const {
      classes,
      theme,
      label,
      placeholder,
      error,
      pickerType,
      permission,
      styles,
      popperProps
    } = this.props;
    const {
      open,
      startDate,
      endDate,
      startTime,
      endTime,
      startTimeDayPeriod,
      endTimeDayPeriod,
      addTime,
    } = this.state;

    let newDate =
      endDate && startDate
        ? `${moment(startDate).format("MMM DD, YY")} - ${moment(endDate).format("MMM DD, YY")}`
        : startDate && !endDate
        ? `${moment(startDate).format("MMM DD, YY")} - ${this.props.intl.formatMessage({id:"common.date.enddate",defaultMessage:"End Date"})}`
        : !startDate && endDate
        ? `${this.props.intl.formatMessage({id:"common.date.startdate",defaultMessage:"Start Date"})} - ${moment(endDate).format("MMM DD, YY")}`
        : placeholder;

    return (
      <Fragment>
        <div
          className="rangeDatePicker"
          onClick={(event) => {
            event.stopPropagation();
          }}
          style={styles}
        >
          {pickerType == "input" ? (
            <DefaultTextField
              label={label}
              fullWidth={true}
              error={error}
              formControlStyles={{ marginBottom: 0 }}
              defaultProps={{
                id: `rangeDatePicker${label}`,
                readOnly: true,
                style: { paddingLeft: 0 },
                placeholder: placeholder,
                value: newDate,
                disabled: permission || false,
                inputRef: (node) => {
                  this.anchorEl = node;
                },
                startAdornment: (
                  <InputAdornment variant="filled" position="end">
                    <CalendarIcon
                      htmlColor={theme.palette.secondary.light}
                      style={{ cursor: "auto" }}
                    />
                  </InputAdornment>
                ),
                onClick: this.handleToggle,
              }}
            />
          ) : (
            <CustomButton
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
              variant="text"
              btnType="plain"
              labelAlign="left"
              disabled={permission || false}
              onClick={(e) => this.handleToggle(e)}
              style={{fontSize: "14px !important", fontWeight: 500, color: theme.palette.text.primary}}
            >
              {!startDate && !endDate ? <u>{newDate}</u> : newDate}
            </CustomButton>
          )}
          <Popper
            open={open}
            transition
            disablePortal
            style={{ zIndex: 99999 }}
            anchorEl={this.anchorEl}
            className={classes.RangeDatePickerPopper}
            placement="bottom-start"
            {...popperProps}
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                id="menu-list-grow"
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom",
                    border:'none',
                    background:'none',
                }}
              >
                <div className={classes.rangeDatePickerInner}>
                  <ClickAwayListener onClickAway={this.handleClose}>
                    <div>
                      <DatePicker
                        inline
                        selected={startDate ? moment(startDate).toDate() : moment().toDate()}
                        selectsStart
                        selectsEnd
                        startDate={startDate}
                        endDate={endDate}
                        onSelect={this.dateSelect}
                        onChange={this.handleChangeEnd}
                      />
                      {/* {startDate || endDate ? (
                        <div className={classes.timeCnt}>
                          {!addTime ? (
                            <div
                              className={classes.addTimeBtnCnt}
                              onClick={this.handleAddTimeClick}
                            >
                              <ClockIcon
                                fontSize="small"
                                htmlColor={theme.palette.secondary.dark}
                              />

                              <span className={classes.addTimeBtnText}>
                                <FormattedMessage
                                  id="common.time.title"
                                  defaultMessage="Add Time"
                                />
                              </span>
                            </div>
                          ) : (
                            <>
                              <div
                                className={`${classes.timeInputCnt} flex_center_start_row`}
                              >
                                <label className={classes.timeInputLabel}>
                                  <FormattedMessage
                                    id="common.time.action"
                                    defaultMessage="Start Time:"
                                  />
                                </label>
                                <DefaultTextField
                                  label={false}
                                  error={false}
                                  formControlStyles={{ marginBottom: 0 }}
                                  defaultProps={{
                                    type: "text",
                                    id: "actualDate",
                                    placeholder: "00:00",
                                    value: startTime,
                                    onChange: (event) =>
                                      this.handleStartTimeChange(
                                        event,
                                        startTime
                                      ),
                                    inputProps: {
                                      style: {
                                        padding: "3px 5px",
                                        fontSize: "10px !important",
                                      },
                                    },
                                  }}
                                />
                                <span
                                  className={classes.amPmBtn}
                                  onClick={(event) =>
                                    this.handleAmPmChange("startTimeDayPeriod")
                                  }
                                >
                                  {startTimeDayPeriod}
                                </span>
                              </div>
                              <div className="flex_center_start_row">
                                <label className={classes.timeInputLabel}>

                                 <FormattedMessage
                                    id="common.time.action2"
                                    defaultMessage="End Time:"
                                  />
                                </label>
                                <DefaultTextField
                                  label={false}
                                  error={false}
                                  formControlStyles={{ marginBottom: 0 }}
                                  defaultProps={{
                                    type: "text",
                                    disabled: !endDate,
                                    id: "actualDate",
                                    value: endTime,
                                    onChange: (event) =>
                                      this.handleEndTimeChange(event, endTime),
                                    placeholder: "00:00",
                                    inputProps: {
                                      style: {
                                        padding: "3px 5px",
                                        fontSize: "10px !important",
                                      },
                                    },
                                  }}
                                />
                                <span
                                  className={classes.amPmBtn}
                                  onClick={(event) =>
                                    this.handleAmPmChange(
                                      endDate ? "endTimeDayPeriod" : ""
                                    )
                                  }
                                >
                                  {endTimeDayPeriod}
                                </span>
                              </div>
                            </>
                          )}
                        </div>
                      ) : null} */}
                    </div>
                  </ClickAwayListener>
                </div>
              </Grow>
            )}
          </Popper>
        </div>
      </Fragment>
    );
  }
}

export default compose(injectIntl,withStyles(datePickerStyles, {
  withTheme: true,
}))(CustomRangeDatePicker);
