import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import "../../assets/css/react-datepicker.css";
import datePickerStyles from "../../assets/jss/components/datePicker";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import helper from "../../helper/index";
import moment from "moment";
class TimeInput extends Component {
  constructor(props) {
    super(props);

    if (props.data && props.data.startTime) {
      let time = moment(props.data.startTime, ["hh:mm A"]);
      this.state = {
        hours: time.format("hh"),
        mins: time.format("mm"),
        am: time.format("A")
      };
    } else {
      this.state = {
        hours: "",
        mins: "",
        am: "AM"
      };
    }
    this.handleTimeSave = this.handleTimeSave.bind(this);
    this.handleAmChange = this.handleAmChange.bind(this);
    this.handleHoursInput = this.handleHoursInput.bind(this);
    this.handleMinsInput = this.handleMinsInput.bind(this);
  }
  componentDidMount() {
    // if (this.props.data) {
    //   const { startTime } = this.props.data;
    //   if (startTime) {
    //     let timeSplitted = moment(startTime, ["h:mm A"]);
    //     this.setState(
    //       {
    //         hours: timeSplitted.format("h"),
    //         mins: timeSplitted.format("m"),
    //         am: timeSplitted.format("A")
    //       },
    //       () => {
    //         this.handleTimeSave();
    //       }
    //     );
    //   }
    // }
    if (this.props.view === "create-meeting") {
      this.setState(
        {
          hours: helper
            .RETURN_CUSTOMDATEFORMATFORTIME(moment().add(1, "hours"))
            .split(":")[0],
          mins: helper
            .RETURN_CUSTOMDATEFORMATFORTIME(moment().add(1, "hours"))
            .split(":")[1]
            .split(" ")[0],
          am: helper
            .RETURN_CUSTOMDATEFORMATFORTIME(moment().add(1, "hours"))
            .split(":")[1]
            .split(" ")[1]
        },
        () => {
          this.handleTimeSave();
        }
      );
    }
    if (this.props.preFilledData) {
      const {
        calenderView: { view },
        startTime: { hours, minutes, meridian }
      } = this.props.preFilledData;
      if (view === "day" || view === "week") {
        this.setState({
          hours: hours,
          mins: minutes,
          am: meridian
        });
      }
    }
  }
  // static getDerivedStateFromProps(nextProps, prevState) {
  //   if(prevState.isChanged){
  //     if(nextProps.data){
  //       if(nextProps.data && nextProps.data.startTime){

  //         return({time: nextProps.data.startTime});
  //       }
  //     else{
  //       return({ time: "", isEmpty:true});
  //     }
  //     }else{
  //       return({time:"", isEmpty:false});
  //     }
  //   }
  // }

  handleHoursInput(event) {
    const value = event.currentTarget.value;
    if (
      (/^[0-9]$|^0[1-9]$|^1[0-2]$/.test(parseInt(value)) &&
        value.length <= 2 &&
        value !== "00" &&
        /^\d+$/.test(value)) ||
      !value
    ) {
      this.setState({ hours: value });
    }
  }
  onBlurHoursInput = () => {
    this.setState(
      (prevState, props) => {
        return {
          hours: this.pad(prevState.hours, 2)
        };
      },
      () => {
        this.handleTimeSave();
      }
    );
  };
  handleMinsInput(event) {
    const value = event.currentTarget.value;
    if (
      (/^[0-9]$|^0[1-9]$|^[1-5][0-9]$|^60$/.test(parseInt(value)) &&
        value.length <= 2 &&
        /^\d+$/.test(value)) ||
      !value
    ) {
      this.setState({ mins: value });
    }
  }
  onBlurMinsInput = () => {
    this.setState(
      (prevState, props) => {
        return {
          mins: this.pad(prevState.mins, 2)
        };
      },
      () => {
        this.handleTimeSave();
      }
    );
  };
  handleAmChange(event) {
    this.setState(
      (prevState, prevProps) => {
        return {
          am: prevState.am == "AM" ? "PM" : "AM"
        };
      },
      () => {
        this.handleTimeSave();
      }
    );
  }
  pad(str, max) {
    return str.length < max ? this.pad("0" + str, max) : str;
  }

  handleTimeSave() {
    const { hours, mins, am } = this.state;
    const time = `${this.pad(hours, 2)}:${this.pad(mins, 2)} ${am}`;
    this.setState({}, () => {
      if (this.props.handleTimeChange) this.props.handleTimeChange(time);
    });
  }

  render() {
    const { classes, label, requiredInput, error } = this.props;
    const { hours, am, mins } = this.state;
    return (
      <>
        <div
          className={classes.timeInputCnt}
          style={{ marginBottom: error == false ? 22 : 10 }}
        >
          {label ? (
            <InputLabel classes={{ root: classes.dropdownsLabel }}>
              {label}
            </InputLabel>
          ) : null}

          <Fragment>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid item xs={4}>
                <OutlinedInput
                  error={requiredInput || false}
                  labelWidth={150}
                  notched={false}
                  value={hours}
                  onChange={this.handleHoursInput}
                  onBlur={this.onBlurHoursInput}
                  name="hours"
                  classes={{
                    root: classes.outlinedHoursInputCnt,
                    input: classes.outlinedHoursInput,
                    notchedOutline: classes.notchedOutlineHoursCnt,
                    focused: classes.outlineInputFocus,
                    disabled: classes.outlinedInputDisabled
                  }}
                  placeholder="HH"
                />
              </Grid>
              <Grid item xs={4}>
                <OutlinedInput
                  labelWidth={150}
                  notched={false}
                  error={requiredInput || false}
                  value={mins}
                  onChange={this.handleMinsInput}
                  onBlur={this.onBlurMinsInput}
                  name="Mins"
                  classes={{
                    root: classes.outlinedMinInputCnt,
                    input: classes.outlinedMinInput,
                    notchedOutline: classes.notchedOutlineMinCnt,
                    focused: classes.outlineInputFocus,
                    disabled: classes.outlinedInputDisabled
                  }}
                  placeholder="MM"
                />
              </Grid>
              <Grid item xs={4}>
                <OutlinedInput
                  labelWidth={150}
                  notched={false}
                  readOnly
                  name="am"
                  value={am}
                  onClick={this.handleAmChange}
                  classes={{
                    root: classes.outlinedAmInputCnt,
                    input: classes.outlinedAmInput,
                    notchedOutline: classes.notchedOutlineAMCnt,
                    focused: classes.outlineInputFocus,
                    disabled: classes.outlinedInputDisabled
                  }}
                  placeholder="AM"
                />
              </Grid>
            </Grid>
          </Fragment>
        </div>
      </>
    );
  }
}

export default withStyles(datePickerStyles, {
  withTheme: true
})(TimeInput);
