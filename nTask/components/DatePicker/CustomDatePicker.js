import "rc-calendar/assets/index.css";
import React from "react";
import ReactDOM from "react-dom";
import Calendar from "rc-calendar";
import DatePicker from "rc-calendar/lib/Picker";
import DefaultTextField from "../Form/TextField";

import zhCN from "rc-calendar/lib/locale/zh_CN";
import enUS from "rc-calendar/lib/locale/en_US";
// import 'rc-time-picker/assets/index.css';
// import TimePickerPanel from 'rc-time-picker/lib/Panel';

import moment from "moment";
import "moment/locale/zh-cn";
import "moment/locale/en-gb";
import {injectIntl} from "react-intl";
const format = "YYYY-MM-DD HH:mm:ss";
const cn = location.search.indexOf("cn") !== -1;

const now = moment();
if (cn) {
  now.locale("zh-cn").utcOffset(8);
} else {
  now.locale("en-gb").utcOffset(0);
}

function getFormat(time) {
  return time ? format : "YYYY-MM-DD";
}

const defaultCalendarValue = now.clone();
defaultCalendarValue.add(-1, "month");

// const timePickerElement = <TimePickerPanel />;

const SHOW_TIME = true;

class Picker extends React.Component {
  state = {
    showTime: SHOW_TIME,
    disabled: false
  };

  render() {
    const props = this.props;
    const calendar = (
      <Calendar
        locale={cn ? zhCN : enUS}
        defaultValue={now}
        //   timePicker={props.showTime ? timePickerElement : null}
        disabledDate={props.disabledDate}
      />
    );
    return (
      <DatePicker
        // animation="slide-up"
        disabled={props.disabled}
        calendar={calendar}
        value={props.value}
        align={{
          offset: [0, 63]
        }}
        onChange={props.onChange}
      >
        {({ value }) => {
          return (
            <span style={{flex: 1, marginRight: 16}}>
              <DefaultTextField
                label={props.label}
                error={false}
                formControlStyles={{marginBottom: 0}}
                defaultProps={{
                  type: "text",
                  //   disabled: this.state.disabled,
                  readOnly: true,
                  id: "actualDate",
                  placeholder: props.intl.formatMessage({id:"common.date.placeholder",defaultMessage:"Select Date"}),
                  value:
                    (value && value.format(getFormat(props.showTime))) || ""
                  //   value:
                  //     (isValidRange(value) &&
                  //       `${format(value[0])} - ${format(value[1])}`) ||
                  //     ""
                }}
              />
            </span>
          );
        }}
      </DatePicker>
    );
  }
}

class CustomDatePicker extends React.Component {
  state = {
    startValue: null,
    endValue: null
  };

  onChange = (field, value) => {
    this.setState({
      [field]: value
    });
  };

  disabledEndDate = endValue => {
    if (!endValue) {
      return false;
    }
    const startValue = this.state.startValue;
    if (!startValue) {
      return false;
    }
    return SHOW_TIME
      ? endValue.isBefore(startValue)
      : endValue.diff(startValue, "days") <= 0;
  };

  disabledStartDate = startValue => {
    if (!startValue) {
      return false;
    }
    const endValue = this.state.endValue;
    if (!endValue) {
      return false;
    }
    return SHOW_TIME
      ? endValue.isBefore(startValue)
      : endValue.diff(startValue, "days") <= 0;
  };

  render() {
    const state = this.state;
    return (
      <div className="flex_center_start_row">
        <Picker
          disabledDate={this.disabledStartDate}
          value={state.startValue}
          onChange={this.onChange.bind(this, "startValue")}
          label={this.props.startLabel}
        />

        <Picker
          disabledDate={this.disabledEndDate}
          value={state.endValue}
          onChange={this.onChange.bind(this, "endValue")}
          label={this.props.endLabel}
          intl={this.props.intl}
        />
        </div>
     
    );
  }
}

export default injectIntl(CustomDatePicker);
