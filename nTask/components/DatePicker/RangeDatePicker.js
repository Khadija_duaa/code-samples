import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import DatePicker from "react-datepicker";
import moment from "moment";
import "../../assets/css/react-datepicker.css";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import datePickerStyles from "../../assets/jss/components/datePicker";
import DefaultTextField from "../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import CalendarIcon from "@material-ui/icons/DateRange";
import { injectIntl } from "react-intl";
let object = {};
class RangeDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      endDate: "",
      startDate: "",
      open: false,
      clear: false
    };
  }

  componentDidMount() {
    const selectedDate = this.props.selectedDate;
    if (selectedDate) {
      const { endDate, startDate } = selectedDate;
      this.setState({
        endDate: endDate ? moment(endDate) : "",
        startDate: startDate ? moment(startDate) : ""
      });
      // this.setState({ endDate, startDate });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const selectedDate = this.props.selectedDate;
    if (
      JSON.stringify(selectedDate) !== JSON.stringify(prevProps.selectedDate)
    ) {
      if (selectedDate) {
        const { endDate, startDate } = selectedDate;
        this.setState({
          endDate: endDate ? moment(endDate) : "",
          startDate: startDate ? moment(startDate) : ""
        });
      }
      else
        this.setState({ endDate: "", startDate: "" });
    }
    if (this.state.clear) {
      this.props.handleClearState(true);
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isClear) {
      object = {
        endDate: "",
        startDate: "",
        open: false,
        clear: true
      };
    } else {
      object = {
        clear: false
      };
    }
    return object;
  }
  handleChangeEnd = date => {
    const startDate = this.state.startDate;
    if (!startDate) {
      this.setState({ startDate: date });
    } else if (date.diff(startDate, "days") < 0) {
      this.setState({ startDate: date });
    } else {
      this.setState({ endDate: date });
    }
  };
  dateSelect = date => {
    const { startDate } = this.state;
    const prevStartDate = startDate ? startDate.format("MMM Do YY") : "";
    if (prevStartDate == date.format("MMM Do YY")) {
      this.setState({ endDate: "", startDate: "" });
    }
  };
  handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };
  handleClose = event => {
    this.setState({ open: false }, () => {
      const selectedDate = this.props.selectedDate;
      if (this.props.handleFilterChange) {
        // if this component is recieving data from props
        if (selectedDate && typeof selectedDate === 'object' && (selectedDate.startDate !== this.state.startDate || selectedDate.endDate !== this.state.endDate)) {
          this.props.handleFilterChange(this.props.type, {
            endDate: this.state.endDate,
            startDate: this.state.startDate
          });
        }
        else if (this.state.startDate || this.state.endDate) {
          this.props.handleFilterChange(this.props.type, {
            endDate: this.state.endDate,
            startDate: this.state.startDate
          });
        }
      }
    });
  };
  render() {
    const { classes, theme, label, placeholder, inputProps } = this.props;
    const { open, startDate, endDate, clear } = this.state;

    let newDate =
      endDate && startDate
        ? `${startDate.format("MMM DD, YY")} - ${endDate.format("MMM DD, YY")}`
        : startDate && !endDate
        ? `${startDate.format("MMM DD, YY")} - ${this.props.intl.formatMessage({id:"common.date.enddate",defaultMessage:"End Date"})}`
        : placeholder;

    return (
      <Fragment>
        <div className="rangeDatePicker">
          <DefaultTextField
            {...inputProps}
            label={label}
            fullWidth={true}
            formControlStyles={{ marginBottom: 0 }}
            classes={classes}
            defaultProps={{
              id: `rangeDatePicker${label}`,
              readOnly: true,
              placeholder: placeholder,
              value: newDate,
              style:{paddingLeft: 0},
              inputRef: node => {
                this.anchorEl = node;
              },
              startAdornment: (
                <InputAdornment variant="filled" position="end">
                  <CalendarIcon
                    htmlColor={theme.palette.secondary.light}
                    style={{ cursor: "auto" }}
                  />
                </InputAdornment>
              ),
              onClick: this.handleToggle
            }}
          />
          <Popper
            open={open}
            transition
            disablePortal
            placement="bottom"
            anchorEl={this.anchorEl}
            className={classes.RangeDatePickerPopper}
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                id="menu-list-grow"
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom"
                }}
              >
                <div>
                  <ClickAwayListener onClickAway={this.handleClose}>
                    <DatePicker
                      inline
                      selected={this.state.startDate}
                      selectsStart
                      selectsEnd
                      startDate={this.state.startDate}
                      endDate={this.state.endDate}
                      onSelect={this.dateSelect}
                      onChange={this.handleChangeEnd}
                      //   onChange={this.handleChangeEnd}
                    />
                  </ClickAwayListener>
                </div>
              </Grow>
            )}
          </Popper>
        </div>
      </Fragment>
    );
  }
}

export default compose(injectIntl, withStyles(datePickerStyles, {
  withTheme: true
}))(RangeDatePicker);
