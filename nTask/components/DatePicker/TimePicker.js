import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import DatePicker from "react-datepicker";
import { withSnackbar } from "notistack";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import moment from "moment";
import "../../assets/css/react-datepicker.css";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import datePickerStyles from "../../assets/jss/components/datePicker";
import Button from "@material-ui/core/Button";
import DefaultButton from "../Buttons/DefaultButton";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import ClockIcon from "@material-ui/icons/AccessTime";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import helper from "../../helper/";
import CustomButton from "../Buttons/CustomButton";
import {FormattedMessage} from "react-intl";
class TimePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: moment(),
      open: false,
      time: "",
      addTime: false,
      hours: "",
      mins: "",
      am: "AM",
      isChanged: true,
      selectedTime: '',
      disableButton: true

    };
    this.handleTimeSave = this.handleTimeSave.bind(this);
    this.handleAmChange = this.handleAmChange.bind(this);
    this.handleHoursInput = this.handleHoursInput.bind(this);
    this.handleMinsInput = this.handleMinsInput.bind(this);
    this.handleAddTimeClick = this.handleAddTimeClick.bind(this);
  }
  componentDidUpdate(prevProps) {
    if (this.props.view === "meetingList") {
      if (prevProps.meetingsState.data.length !== this.props.meetingsState.data.length) {
        this.setState(
          {
            selectedDate: moment(),
            open: false,
            time: "",
            addTime: false,
            hours: "",
            mins: "",
            am: "AM",
            isChanged: true,
          },
          () => {
            this.componentDidMount();
          }
        );
      }
    }
  }

  componentDidMount() {
    if (this.props.data && this.props.view === "meetingList") {
      let props = this.props;
      if (props.data && props.data.startTime) {
        this.setState({
          time: helper.RETURN_CUSTOMDATEFORMATFORTIME(props.data.startTime),
          hours:
            props.data && props.data.startTime
              ? props.data.startTime.split(":")[0]
              : "",
          mins:
            props.data && props.data.startTime
              ? helper.RETURN_CUSTOMDATEFORMATFORTIME(props.data.startTime).split(":")[1].split(" ")[0]
              : "",
          am:
            props.data && props.data.startTime
              ? helper.RETURN_CUSTOMDATEFORMATFORTIME(props.data.startTime).split(":")[1].split(" ")[1]
              : ""
        });
      }
    }
  }

  handleHoursInput(event) {
    const value = event.currentTarget.value;
    if (
      (/^[0-9]$|^0[1-9]$|^1[0-2]$/.test(parseInt(value)) &&
        value.length <= 2 &&
        value !== "00" &&
        /^\d+$/.test(value)) ||
      !value
    ) {
      this.setState({ hours: value });
    }
  }
  handleMinsInput(event) {
    const value = event.currentTarget.value;
    if (
      (/^[0-9]$|^0[1-9]$|^[1-5][0-9]$|^60$/.test(parseInt(value)) &&
        value.length <= 2 &&
        /^\d+$/.test(value)) ||
      !value
    ) {
      this.setState({ mins: value });
    }
  }
  handleAmChange(event) {
    this.setState(prevState => ({ am: prevState.am == "AM" ? "PM" : "AM" }));
  }
  handleAddTimeClick() {
    this.setState({ open: false }, () => {
      this.componentDidMount();
    });
  }
  handleTimeSave() {
    const { hours, mins, am = 'AM' } = this.state;

    const time = `${hours.length < 2 ? `0${hours}` : hours}:${
      mins.length < 2 ? `0${mins}` : !mins ? "" : mins
      } ${am}`;
    if (time !== '00:00 PM' && time !== '00:00 AM' && time !== '0:0 PM' && time !== '0:0 AM' && hours !== '0') {
      this.setState({ time, addTime: false, isChanged: false, selectedTime: time }, () => {
        if (this.props.selectedSaveTime) {
          if (this.props.isCreation) {
            this.props.selectedSaveTime(this.state.time);
            this.setState({ open: false });
          }
          else {
            this.props.selectedSaveTime(this.state.time, this.props.data);
            this.setState({ open: false });
          }
        }
      });
    } else {
      this.showSnackBar(`Oops, Meeting time cannot be preset to 0.00. Please set a tie slot that's appropriate for you to proceed.`, "error");
    }

  }
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right"
        },
        variant: type ? type : "info"
      }
    );
  };
  handleToggle = event => {
    if (this.props.isDateNull) {
      const data = this.props.data;
      this.props.showDateTimeDetailsPopUp(event, data);
    } else {
      event.stopPropagation();
      this.setState(state => ({ open: !state.open }));
    }
  };
  handleClose = event => {
    this.setState({ open: false }, () => {
      this.componentDidMount();
    });
  };
  render() {
    const { classes, theme, isAm } = this.props;
    const { open, time, addTime, hours, am, mins, isChanged, disableButton } = this.state;
    let isEmpty = time;
    if (isChanged) {
      isEmpty = this.props.data ? helper.RETURN_CUSTOMDATEFORMATFORTIME(this.props.data.startTime) : false;
    }
    return (
      <>
        <CustomButton
          buttonRef={node => {
            this.anchorEl = node;
          }}
          variant="text"
          btnType="plain"
          onClick={event => this.handleToggle(event)}
        >
          {isEmpty ? isEmpty : <u><FormattedMessage id="common.time.list-label" defaultMessage="Select Time"/></u>}
        </CustomButton>


        <Popper
          open={open}
          transition
          anchorEl={this.anchorEl}
          className={classes.RangeDatePickerPopper}
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom"
              }}
            >
              <ClickAwayListener onClickAway={this.handleClose}>
                <Grid
                  item
                  classes={{ item: classes.menuTimePickerCnt }}
                  onClick={event => {
                    event.stopPropagation();
                  }}
                >
                  <div className={classes.timePickerFieldsCnt}>
                    <InputLabel classes={{ root: classes.dropdownsLabel }}>
                      Set Time
                    </InputLabel>

                    <Fragment>
                      <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                      >
                        <Grid item xs={4}>
                          <OutlinedInput
                            labelWidth={150}
                            notched={false}
                            value={hours}
                            onChange={this.handleHoursInput}
                            name="hours"
                            classes={{
                              root: classes.outlinedHoursInputCnt,
                              input: classes.outlinedHoursInput,
                              notchedOutline: classes.notchedOutlineHoursCnt,
                              focused: classes.outlineInputFocus,
                              disabled: classes.outlinedInputDisabled
                            }}
                            placeholder="HH"
                          />
                        </Grid>
                        <Grid item xs={4}>
                          <OutlinedInput
                            labelWidth={150}
                            notched={false}
                            value={mins}
                            onChange={this.handleMinsInput}
                            name="Mins"
                            classes={{
                              root: classes.outlinedMinInputCnt,
                              input: classes.outlinedMinInput,
                              notchedOutline: classes.notchedOutlineMinCnt,
                              focused: classes.outlineInputFocus,
                              disabled: classes.outlinedInputDisabled
                            }}
                            placeholder="MM"
                          />
                        </Grid>
                        {isAm ? (
                          <Grid item xs={4}>
                            <OutlinedInput
                              labelWidth={150}
                              notched={false}
                              readOnly
                              name="am"
                              // disabled={true}
                              value={am}
                              onClick={this.handleAmChange}
                              classes={{
                                root: classes.outlinedAmInputCnt,
                                input: classes.outlinedAmInput,
                                notchedOutline: classes.notchedOutlineAMCnt,
                                focused: classes.outlineInputFocus,
                                disabled: classes.outlinedInputDisabled
                              }}
                              placeholder="AM"
                            />
                          </Grid>
                        ) : null}
                      </Grid>
                      <div className={classes.addTimeActionBtnCnt}>
                        <DefaultButton
                          onClick={this.handleClose}
                          text="Cancel"
                          buttonType="Transparent"
                          style={{ marginRight: 10 }}
                        />
                        <DefaultButton
                          onClick={this.handleTimeSave}
                          text="Save"
                          // disabled={disableButton}
                          buttonType="Plain"
                        />
                      </div>
                    </Fragment>
                  </div>
                </Grid>
              </ClickAwayListener>
            </Grow>
          )}
        </Popper>
      </>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    meetingsState: state.meetings
  }
}


export default compose(
  withRouter,
  withSnackbar,
  withStyles(datePickerStyles, {
    withTheme: true
  }),
  connect(
    mapStateToProps,
    {}
  )
)(TimePicker);