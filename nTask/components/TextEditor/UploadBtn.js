import React from "react";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import textEditorStyles from "./style";
import AttachmentIcon from "@material-ui/icons/AttachFile";

// function FileUploadBtn(props){
class FileUploadBtn extends React.Component {
  uploadImage = event => {
    
    this.props.handleImageChange(event);
    //
  };
  clickFileUpload() {
    let fileinputbtn = document.getElementById("editorImageUpload");
    fileinputbtn.click();
  }
  render() {
    const hideBtn = {
      display: "none"
    };
    return (
      <div onClick={this.clickFileUpload}>
        <IconButton
          disableRipple={true}
          classes={{ root: this.props.classes.attachmentIconBtn }}
        >
          <AttachmentIcon
            classes={{ root: this.props.classes.attachmentIcon }}
          />
          <input
            style={hideBtn}
            className="fileInput"
            type="file"
            id="editorImageUpload"
            onChange={this.uploadImage}
            accept={
              "png, jpeg, jpg, mp4, mpg, svg, zip, rar, webm, wmv, swf, mov, mkv, gif, bmp, xlsx, xls, csv, pdf, PNG, JPEG, JPG, MP4, MPG, SVG, ZIP, RAR, WEBM, WMV, SWF, MOV, MKV, GIF, BMP, XLSX, XLS, CSV, PDF"
            }
          />
        </IconButton>
      </div>
    );
  }
}

export default withStyles(textEditorStyles, { withTheme: true })(FileUploadBtn);
