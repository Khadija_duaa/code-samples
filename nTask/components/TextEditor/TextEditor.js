import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Editor, { createEditorStateWithText } from "draft-js-plugins-editor";
import createEmojiPlugin from "draft-js-emoji-plugin";
import "../../assets/css/plugin.css";
import "draft-js/dist/Draft.css";
import { withStyles } from "@material-ui/core/styles";
import CustomAvatar from "../Avatar/Avatar";
import { generateUsername } from "../../utils/common";
import {
  EditorState,
  Modifier,
  ContentState,
  convertToRaw,
  convertFromRaw,
} from "draft-js";
import { defaultSuggestionsFilter } from "draft-js-mention-plugin";
import "draft-js-mention-plugin/lib/plugin.css";
import createToolbarPlugin, { Separator } from "draft-js-static-toolbar-plugin";
import HeadlinesButton from "./HeadlinesButton";
import textEditorStyles from "./style.js";
import IconButton from "@material-ui/core/IconButton";
import Emoticon from "@material-ui/icons/InsertEmoticon";
import FileUploadBtn from "./UploadBtn";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import draftToHtml from "draftjs-to-html";
import getErrorMessages from "../../utils/constants/errorMessages";
import { uploadFileTextEditor } from "../../redux/actions/constants";
import DeleteIcon from "@material-ui/icons/Close";
import {FormattedMessage, injectIntl} from 'react-intl';

const MAX_LENGTH = 3000;
const Entry = (props) => {
  const {
    mention,
    theme,
    searchValue, // eslint-disable-line no-unused-vars
    isFocused, // eslint-disable-line no-unused-vars
    ...parentProps
  } = props;

  return (
    <div {...parentProps}>
      <div className={theme.mentionSuggestionsEntryContainer}>
        <div className={theme.mentionSuggestionsEntryContainerLeft}>
          <img
            src={mention.avatar}
            className={theme.mentionSuggestionsEntryAvatar}
            role="presentation"
          />
        </div>

        <div className={theme.mentionSuggestionsEntryContainerRight}>
          <div className={theme.mentionSuggestionsEntryText}>
            {mention.name}
          </div>

          <div className={theme.mentionSuggestionsEntryTitle}>
            {mention.title}
          </div>
        </div>
      </div>
    </div>
  );
};

import {
  ItalicButton,
  BoldButton,
  UnderlineButton,
  CodeButton,
  UnorderedListButton,
  OrderedListButton,
  BlockquoteButton,
  CodeBlockButton,
} from "draft-js-buttons";
import "draft-js-static-toolbar-plugin/lib/plugin.css";
import {
  RiskMention,
  IssueMention,
  MeetingMention,
  ProjectMention,
  TaskMention,
  UserMention,
} from "./MentionPlugins";
import { UpdateTaskCommentData } from "../../redux/actions/taskComments";
import { generateActiveMembers } from "../../helper/getActiveMembers";
class TextEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty(),
      attachmentArray: [],
      fileAttachment: null,
      fileAttachmentData: null,
      fileName: "",
      mentions:
        props.profileState &&
        props.profileState.data &&
        props.profileState.data.member
          ? generateActiveMembers()
              .filter((m) => !m.isDeleted)
              .map((x) => {
                return {
                  ...x,
                  name: x.fullName,
                  avatar: x.imageUrl ? x.imageUrl : null,
                };
              })
          : [],
      tasks: props.tasksState.data
        ? props.tasksState.data.map((x) => {
            return { id: x.taskId, name: x.taskTitle };
          })
        : [],
      issues: props.issuesState.data
        ? props.issuesState.data.map((x) => {
            return { id: x.id, name: x.title };
          })
        : [],
      projects: props.projectsState.data
        ? props.projectsState.data.map((x) => {
            return { id: x.projectId, name: x.projectName };
          })
        : [],
      meetings: props.meetingsState.data
        ? props.meetingsState.data.map((x) => {
            return { id: x.meetingId, name: x.meetingDisplayName };
          })
        : [],
      risks: props.risksState.data
        ? props.risksState.data.map((x) => {
            return { id: x.id, name: x.title };
          })
        : [],
      editorHeight: 20,
      toolbar: false,
      html: "",
    };
    this.emojiPlugin = createEmojiPlugin({
      selectButtonContent: <Emoticon style={{ marginTop: 4 }} />,
      allowImageCache: true,
    });
    this.toolbarPlugin = createToolbarPlugin();
    this.editor = React.createRef();
  }

  onChange = (newState) => {
    // if (
    //   // when content change or line style change will change the state
    //   !newState.getCurrentContent().equals(this.state.editorState.getCurrentContent()) ||
    //   !newState
    //     .getCurrentInlineStyle()
    //     .equals(this.state.editorState.getCurrentInlineStyle())
    // ) {
    this.setState({
      editorState: newState,
      html: draftToHtml(convertToRaw(newState.getCurrentContent())),
    });
    // }
  };

  handleOtherMentionBtn = (mentionType) => {
    const editorState = this.state.editorState;
    const selection = editorState.getSelection();
    const contentState = editorState.getCurrentContent();
    const ncs = Modifier.insertText(contentState, selection, mentionType);
    const es = EditorState.push(editorState, ncs, "insert-fragment");
    this.setState({ editorState: es }, () => this.focus());
  };
  focus = () => {
    if (this.editor) this.editor.focus();
    if (!this.props.plainEditor) {
      this.setState({
        // editorHeight: 180,
        toolbar: true,
      });
    }
  };

  handleKeyBinding = (event) => {
    const mentionPlugin = document.querySelector(
      ".draftJsMentionPlugin__mentionSuggestions__2DWjA"
    );
    if (!event.shiftKey && !mentionPlugin) {
      // if (
      //   this.state.fileAttachmentData !== null
      // ) {
      const html = draftToHtml(
        convertToRaw(this.state.editorState.getCurrentContent())
      );
      const content = this.state.editorState.getCurrentContent();
      const isEditorEmpty = !content.hasText();
      const currentPlainText = content.getPlainText();
      const lengthOfEditorContent = currentPlainText.length;
      const lengthOfTrimmedContent = currentPlainText.trim().length;
      const isContainOnlySpaces = !isEditorEmpty && !lengthOfTrimmedContent;
      //////////////////////////////////
      if (
        (isEditorEmpty || isContainOnlySpaces) &&
        !this.props.fileAttachment
      ) {
        return;
      }

      // submit message
      if (this.props.saveAction) {
        this.setState(
          {
            editorState: EditorState.push(
              this.state.editorState,
              ContentState.createFromText("")
            ),
          },
          () => {
            this.props.saveAction("discussion", html);
          }
        );
        this.editor.blur();
        this.editor.focus();
        // return "not-handled";
      } else {
        const html = draftToHtml(
          convertToRaw(this.state.editorState.getCurrentContent())
        );
        let saveComment = {};
        ///////
        if (this.props.type) {
          saveComment = {
            Message: html,
            RiskIssueId: this.props.MenuData.id,
            Type: this.props.type,
            UpdateType: 1,
          };
        } else {
          saveComment = {
            commentText: html,
            taskId: this.props.currentTask.taskId,
            commentType: 1,
          };
        }

        ////////////////////////////
        if (
          !this.props.fileAttachment &&
          this.state.editorState
            .getCurrentContent()
            .getPlainText()
            .replace(/\s/g, "").length > 0
        ) {
          this.setState(
            {
              editorState: EditorState.push(
                this.state.editorState,
                ContentState.createFromText("")
              ),
            },
            () => {
              this.editor.blur();
              // this.editor.focus();
            }
          );

          this.props.handleNewComments(saveComment);

          return "handled";
        } else {
          this.props.UploadFileComment(
            draftToHtml(
              convertToRaw(this.state.editorState.getCurrentContent())
            ),
            EditorState,
            this
          );

          this.setState(
            {
              attachmentArray: [],
              editorState: EditorState.push(
                this.state.editorState,
                ContentState.createFromText("")
              ),
            },
            () => {
              this.editor.blur();
              // this.editor.focus();
            }
          );
          return "handled";
          // return "not-handled";
        }
      }
    }
    // this.setState({
    //   fileAttachment: null,
    //   fileAttachmentData: null,
    //   attachmentArray: [],
    //   editorState: EditorState.createEmpty() //EditorState.moveFocusToEnd(EditorState.createEmpty()),
    //   // editorHeight: 40,
    //   // toolbar: false
    // });
    // return "not-handled";

    // return "handled";
  };
  handleAttachmentDelete = () => {
    let a = this.props.fileAttachment.docsName;
  };
  blur = () => {
    this.setState({
      // editorHeight: 20,
      toolbar: false,
    });
  };
  onSearchChange = ({ value }, name) => {
    this.setState({
      [name]: defaultSuggestionsFilter(value, name),
    });
  };
  getLengthOfSelectedText = () => {
    const currentSelection = this.state.editorState.getSelection();
    const isCollapsed = currentSelection.isCollapsed();

    let length = 0;

    if (!isCollapsed) {
      const currentContent = this.state.editorState.getCurrentContent();
      const startKey = currentSelection.getStartKey();
      const endKey = currentSelection.getEndKey();
      const startBlock = currentContent.getBlockForKey(startKey);
      const isStartAndEndBlockAreTheSame = startKey === endKey;
      const startBlockTextLength = startBlock.getLength();
      const startSelectedTextLength =
        startBlockTextLength - currentSelection.getStartOffset();
      const endSelectedTextLength = currentSelection.getEndOffset();
      const keyAfterEnd = currentContent.getKeyAfter(endKey);
      if (isStartAndEndBlockAreTheSame) {
        length +=
          currentSelection.getEndOffset() - currentSelection.getStartOffset();
      } else {
        let currentKey = startKey;

        while (currentKey && currentKey !== keyAfterEnd) {
          if (currentKey === startKey) {
            length += startSelectedTextLength + 1;
          } else if (currentKey === endKey) {
            length += endSelectedTextLength;
          } else {
            length += currentContent.getBlockForKey(currentKey).getLength() + 1;
          }

          currentKey = currentContent.getKeyAfter(currentKey);
        }
      }
    }

    return length;
  };
  handleBeforeInput = () => {
    const currentContent = this.state.editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText("").length;
    const selectedTextLength = this.getLengthOfSelectedText();

    if (currentContentLength - selectedTextLength > MAX_LENGTH - 1) {
      return "handled";
    }
  };
  handlePastedText = (pastedText) => {
    const currentContent = this.state.editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText("").length;
    const selectedTextLength = this.getLengthOfSelectedText();

    if (
      pastedText &&
      currentContentLength + pastedText.length - selectedTextLength > MAX_LENGTH
    ) {
     

      return "handled";
    }
  };

  render() {
    const {
      classes,
      theme,
      plainEditor,
      permissionAttachment,
      view,
      uploadPermission,
      intl
    } = this.props;
    const {
      mentions,
      tasks,
      issues,
      projects,
      meetings,
      risks,
      editorHeight,
      toolbar,
    } = this.state;
    const { EmojiSuggestions, EmojiSelect } = this.emojiPlugin;
    const { Toolbar } = this.toolbarPlugin;
    const plugins = !plainEditor
      ? [
          RiskMention,
          IssueMention,
          MeetingMention,
          ProjectMention,
          TaskMention,
          UserMention,
          this.emojiPlugin,
          this.toolbarPlugin,
        ]
      : [
          RiskMention,
          IssueMention,
          MeetingMention,
          ProjectMention,
          TaskMention,
          UserMention,
        ];
    const Entry = (props, isAvatar) => {
      const {
        mention,
        theme,
        searchValue, // eslint-disable-line no-unused-vars
        isFocused, // eslint-disable-line no-unused-vars
        ...parentProps
      } = props;
      return (
        <div onMouseUp={this.handleAddMention} {...parentProps}>
          <div className="mentionSuggestionsEntryContainer">
            {isAvatar == true ? (
              <div className="mentionSuggestionsEntryContainerLeft">
                <CustomAvatar
                  otherMember={{
                    imageUrl: mention.avatar,
                    fullName: mention.fullName,
                    lastName: "",
                    email: mention.email,
                    isOnline: mention.isOnline,
                    isOwner: mention.isOwner,
                  }}
                  size="xsmall"
                />
              </div>
            ) : null}

            <div className="mentionSuggestionsEntryContainerRight">
              <div className="mentionSuggestionsEntryText">{mention.name}</div>
            </div>
          </div>
        </div>
      );
    };

    const UploadFileType = () => {
      if (this.props.view === "Task")
        return <FileUploadBtn handleImageChange={this.props.fileUpload} />;
      else if (this.props.view === "Issue")
        return (
          <FileUploadBtn handleImageChange={this.props.fileUploadIssues} />
        );
      else if (this.props.view === "Risk")
        return <FileUploadBtn handleImageChange={this.props.fileUploadRisk} />;
      else return <FileUploadBtn handleImageChange={this.props.fileUpload} />;
    };

    return (
      <ClickAwayListener onClickAway={this.blur}>
        <div
          className={`${
            plainEditor ? "textBoxContainerShadow" : "textBoxContainer"
          }`}
        >
          {toolbar && !plainEditor ? (
            <Toolbar>
              {// may be use React.Fragment instead of div to improve perfomance after React 16
              (externalProps) => (
                <Fragment>
                  <BoldButton {...externalProps} />
                  <ItalicButton {...externalProps} />
                  <UnderlineButton {...externalProps} />
                  <CodeButton {...externalProps} />
                  <Separator {...externalProps} />
                  <HeadlinesButton {...externalProps} />
                  <UnorderedListButton {...externalProps} />
                  <OrderedListButton {...externalProps} />
                  <BlockquoteButton {...externalProps} />
                  <CodeBlockButton {...externalProps} />
                </Fragment>
              )}
            </Toolbar>
          ) : null}

          <div
            className="textEditor"
            onFocus={this.focus}
            style={{ minHeight: 60 }}
          >
            <Editor
              editorState={this.state.editorState}
              placeholder={intl.formatMessage({id:"common.comment.comment-area.placeholder", defaultMessage:"Type your comment here..."})}
              onChange={this.onChange}
              plugins={plugins}
              handleBeforeInput={this.handleBeforeInput}
              handlePastedText={this.handlePastedText}
              handleReturn={this.handleKeyBinding}
              ref={(element) => {
                this.editor = element;
              }}
            />
            {this.props.fileAttachment ? (
              <div className={classes.attachmentNameCnt}>
                {this.props.fileAttachment.docsName}
                <DeleteIcon
                  className={classes.attachmentDeleteIcon}
                  htmlColor={theme.palette.text.primary}
                  onClick={this.props.deleteAttachment}
                />
              </div>
            ) : null}
            <EmojiSuggestions />
            <UserMention.MentionSuggestions
              onSearchChange={(value) => this.onSearchChange(value, mentions)}
              suggestions={mentions}
              entryComponent={(props) => Entry(props, true)}
            />
            <TaskMention.MentionSuggestions
              onSearchChange={(value) => this.onSearchChange(value, tasks)}
              suggestions={tasks}
              entryComponent={Entry}
              onAddMention={(a, b, c) => {}}
            />
            <IssueMention.MentionSuggestions
              onSearchChange={(value) => this.onSearchChange(value, issues)}
              suggestions={issues}
              entryComponent={Entry}
            />
            <RiskMention.MentionSuggestions
              onSearchChange={(value) => this.onSearchChange(value, risks)}
              suggestions={risks}
              entryComponent={Entry}
            />
            <ProjectMention.MentionSuggestions
              onSearchChange={(value) => this.onSearchChange(value, projects)}
              suggestions={projects}
              entryComponent={Entry}
            />
            <MeetingMention.MentionSuggestions
              onSearchChange={(value) => this.onSearchChange(value, meetings)}
              suggestions={meetings}
              entryComponent={Entry}
            />
          </div>
          {!plainEditor ? (
            <div className="options">
              <div className="flex_center_start_row">
                <EmojiSelect />

                <IconButton
                  classes={{ root: classes.mentionUserBtn }}
                  onClick={(event) => this.handleOtherMentionBtn(" @")}
                  disableRipple={true}
                >
                  @
                </IconButton>
              </div>
              <div className="flex_center_start_row">
                <IconButton
                  classes={{ root: classes.mentionOtherBtn }}
                  onClick={(event) => this.handleOtherMentionBtn(" /task")}
                  disableRipple={true}
                >
                  /
                </IconButton>
                {view === "Issue" || view === "Risk" ? (
                  permissionAttachment || permissionAttachment.add ? (
                    <UploadFileType />
                  ) : null
                ) : uploadPermission ? (
                  <UploadFileType />
                ) : null}
              </div>
            </div>
          ) : null}
        </div>
      </ClickAwayListener>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tasksState: state.tasks,
    issuesState: state.issues,
    projectsState: state.projects,
    meetingsState: state.meetings,
    risksState: state.risks,
    profileState: state.profile,
  };
};

export default compose(
  injectIntl,
  withRouter,
  withStyles(textEditorStyles, { withTheme: true }),
  connect(mapStateToProps, {
    uploadFileTextEditor,
    UpdateTaskCommentData,
  })
)(TextEditor);
