const textEditorStyles = theme => ({
    
    mentionOtherBtn: {
        width: 32,
        height: 32,
        fontSize: "20px !important",
        padding: 0,
        color: theme.palette.text.secondary,
        '&:hover': {
            background: "none"
        }
    },
    mentionUserBtn: {
        width: 32,
        height: 32,
        fontSize: "20px !important",
        padding: 0,
        color: theme.palette.text.secondary,
        '&:hover': {
            background: "none"
        }
    },
    attachmentIconBtn:{
        width: 32,
        height: 32,
        fontSize: "20px !important",
        padding: 0,
        color: theme.palette.text.secondary,
        '&:hover': {
            background: "none"
        },
        '& $attachmentIcon':{
            fontSize: "22px !important"
        }
    },
    attachmentIcon:{},
    attachmentNameCnt: {
        background: theme.palette.background.items,
        fontSize: "10px !important",
        color: theme.palette.text.primary,
        borderRadius: 4,
        display: "flex",
        alignItems: "center",
        maxWidth: 100,
        justifyContent: "space-between",
        padding: "5px 10px"
    },
    attachmentDeleteIcon :{
        cursor: "pointer",
        fontSize: "14px !important"

    },
    addMeetingDetailBtn:{
        padding: "11px 10px",
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        cursor: 'pointer'
      },
      addMeetingDetailPlus:{
        marginRight: 10,
        fontSize: "16px !important",
        color: theme.palette.primary.light
      },
      everyOneIconSvg: {
          fontSize: "30px !important"
      },
      sendTextIcon:{
        fontSize: "28px !important",
        color: theme.palette.background.contrast
    },
    sendTextIconSelected: {
        fontSize: "28px !important",
        color: theme.palette.background.brightBlue
    },
    textEditorCnt:{
        background: theme.palette.common.white,
        
    }
})

export default textEditorStyles;