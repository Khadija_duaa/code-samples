import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import sortingDropdownStyles from "./style";
import menuStyles from "../../assets/jss/components/menu";
import SelectionMenu from "../Menu/SelectionMenu";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../utils/mergeStyles";
import { Scrollbars } from "react-custom-scrollbars";
import CustomButton from "../Buttons/CustomButton";
import IconButton from "../Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import GroupingIcon from "@material-ui/icons/Layers";
import { updateGroupColumn } from "../../redux/actions/workspace";
import EveryOneIcon from "../Icons/EveryOneIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomAvatar from '../Avatar/Avatar';
import Typography from "@material-ui/core/Typography";

class ChatAssignDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "top",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  // componentDidMount() {
  //   const { selectedGroup } = this.props;
  //   this.setState({ checked: selectedGroup });
  // }
  // componentDidUpdate(prevProps, prevState) {
  //   const { selectedGroup } = this.props;
  //   if (prevProps.selectedGroup.key != selectedGroup.key) {
  //     this.setState({ checked: selectedGroup });
  //   }
  // }

  handleClose(event) {
    /** function call when close drop down */
    event.stopPropagation();
    this.setState({ open: false });
  }
  handleClick(event, placement) {
    /** function call for opens drop down  */
    event.stopPropagation();
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleMemberSelect = (e, value) => {
    /** function call when Select option from drop down  */
    e.stopPropagation();
    this.setState({
      open: false,
    },
      this.props.onSelect({ key: value.userId, value: value.fullName }));
  };

  render() {
    const { classes, theme, data, height, width, id, selectedUser, enableDropDown } = this.props;
    const { open, placement } = this.state;
    let DropDownHeight = height ? height : 250;
    let menuWidth = width ? width : 220;
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <div >
            <CustomButton /** Sort By Button  */
              onClick={(event) => {
                this.handleClick(event, "bottom-end");
              }}
              id={id}
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
              btnType="white"
              variant="contained"
              style={{ border: 0 }}
              disabled={!enableDropDown}
            >
              <Typography variant="h6" style={{ display: 'flex', color: theme.palette.text.brightBlue }}>To: {selectedUser.value}</Typography>
            </CustomButton>
          </div>
          <SelectionMenu
            open={open}
            closeAction={
              this.handleClose
            } /** function call when close drop down */
            placement={placement}
            checkedType="multi"
            anchorRef={this.anchorEl}
            style={{ width: menuWidth }}
            list={
              <Scrollbars autoHide style={{ height: DropDownHeight }}>
                <List>
                  <ListItem
                    key={'everyone'}
                    id='everyone'
                    button
                    onClick={event => this.handleMemberSelect(event, { userId: 'all', fullName: 'Everyone' })}
                    className={`${classes.menuItemavatar} ${selectedUser.key == 'all'
                      ? classes.selectedValue
                      : ""
                      } .chatAssignee`}
                    classes={{ selected: classes.statusMenuItemSelected }}
                  >
                    <SvgIcon
                      viewBox="0 0 28 28"
                      className={classes.everyOneIconSvg}
                    >
                      <EveryOneIcon />
                    </SvgIcon>
                    <ListItemText
                      primary={'Everyone'} /** displaying name property from array coming ffrom props */
                      classes={{
                        primary: classes.statusItemText,
                      }}
                    />
                  </ListItem>
                  {data.lenght > 0 && data
                    .filter(m => !m.isDeleted)
                    .map((member, i) => {
                      return (
                        <ListItem
                          key={i}
                          button
                          onClick={event => this.handleMemberSelect(event, member)}
                          className={`${classes.menuItemavatar} ${selectedUser.key == member.userId
                            ? classes.selectedValue
                            : ""
                            }`}
                          classes={{ selected: classes.statusMenuItemSelected }}
                        >
                          <CustomAvatar
                            otherMember={{
                              imageUrl: member.imageUrl,
                              firstName: member.fullName,
                              lastName: '',
                              email: member.email,
                              isOnline: member.isOnline,
                              isOwner: member.isOwner,
                            }}
                            size="xsmall"
                          />
                          <ListItemText
                            primary={member.fullName} /** displaying name property from array coming ffrom props */
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      )
                    })}
                </List>
              </Scrollbars>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

const mapStateToProps = (state) => {
  return {
  };
};

export default compose(
  withRouter,
  withStyles(combineStyles(sortingDropdownStyles, menuStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, { updateGroupColumn })
)(ChatAssignDropdown);
