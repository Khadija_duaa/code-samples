// @flow

import React, { useState, useRef, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import textEditorStyles from "./style.js";




import ClickAwayListener from "@material-ui/core/ClickAwayListener";

import "froala-editor/css/froala_editor.pkgd.min.css";
import "froala-editor/css/froala_style.css";
import "froala-editor/js/plugins.pkgd.min.js";
import "./textEditorStyle.css";
import FroalaEditor from "react-froala-wysiwyg";
import Tribute from "tributejs";

type TextEditorProps = {
  classes: Object,
  theme: Object,
  defaultValue: Object,
  onChange: Function,
  style: Object,
  editorProps: Object,
};

// let initCont;
// let editor;
// let editorState;
// const initData = [
//   { key: 'Phil', value: 'pheartman' },
//   { key: 'Gordon', value: 'gramsey' },
//   { key: 'Jacob', value: 'jacob' },
//   { key: 'Ethan', value: 'ethan' },
//   { key: 'Emma', value: 'emma' },
//   { key: 'Isabella', value: 'isabella' }
// ]
function TextEditor(props: TextEditorProps) {
  const {
    style,
    saveAction,
    onChange,
    config,
    handleManualController,
    handleModelChange,
    editorModel,
  } = props;
  // const userMention = new Tribute({
  //   values: initData,
  //   trigger: "@",
  //   selectTemplate: function (item) {
  //     return '<span class="fr-deletable fr-tribute">' + item.original.key + '</a></span>';
  //   }
  // })
  // const taskMention = new Tribute({
  //   values: initData,
  //   trigger: "/task",
  //   selectTemplate: function (item) {
  //     return '<span class="fr-deletable fr-tribute">' + item.original.key + '</a></span>';
  //   }
  // })
  // const issueMention = new Tribute({
  //   values: initData,
  //   trigger: "/issue",
  //   selectTemplate: function (item) {
  //     return '<span class="fr-deletable fr-tribute">' + item.original.key + '</a></span>';
  //   }
  // })
  // const projectMention = new Tribute({
  //   values: initData,
  //   trigger: "/project",
  //   selectTemplate: function (item) {
  //     return '<span class="fr-deletable fr-tribute">' + item.original.key + '</a></span>';
  //   }
  // })
  // const riskMention = new Tribute({
  //   values: initData,
  //   trigger: "/risk",
  //   selectTemplate: function (item) {
  //     return '<span class="fr-deletable fr-tribute">' + item.original.key + '</a></span>';
  //   }
  // })
  // const [tribute, setTribute] = useState(newTribute)
  // const handleChangeMention = () => {
  //   const tributeWithNewOption = new Tribute({
  //     values: [
  //       { key: 'Phil1', value: 'pheartman' },
  //       { key: 'Gordon1', value: 'gramsey' },
  //       { key: 'Jacob1', value: 'jacob' },
  //       { key: 'Ethan1', value: 'ethan' },
  //       { key: 'Emma1', value: 'emma' },
  //       { key: 'Isabella1', value: 'isabella' }
  //     ],
  //     trigger: "@",
  //     selectTemplate: function (item) {
  //       return '<span class="fr-deletable fr-tribute">' + item.original.key + '</a></span>';
  //     }
  //   })
  //   setTribute(tributeWithNewOption)
  //   tributeWithNewOption.attach(initCont.getEditor().el)
  // }

  // const { inputValue, onChange, onEnterPress, classes, theme, style, editorProps, onEditorFocus, onEditorBlur, isChildView, placeholder = '', } = props;
  // const [value, setValue] = useState("");
  // const [showToolbar, setShowToolbar] = useState(false);
  // let editorRef = useRef('');

  // const onEditorChange = (content, delta, source, editor) => {
  //   // on Editor Change
  //   const newDelta = editor.getContents(); // Delta term is used by quill editor to denote editor state
  //   // setValue(newDelta);
  //   // onChange(JSON.stringify(newDelta)); // Calling parent function to lift up the state
  //   onChange({
  //     contents: editor.getContents(),
  //     html: editor.getHTML(),
  //     length: editor.getLength(),
  //     selection: editor.getSelection(),
  //     text: editor.getText(),
  //   }); // Calling parent function to lift up the state
  //   // editorRef;
  // };

  // const onFocued = () => {
  //   editorRef.focus();
  //   onEditorFocus && onEditorFocus();
  // }

  // const onBlured = () => {
  //   onEditorBlur && onEditorBlur();
  // }

  // const onEditorKeyUp = (event) => {
  //   if (event.key === 'Enter' && event.shiftKey) {
  //   }
  //   else if (event.key === 'Enter') {
  //     if (onEnterPress)
  //       onEnterPress();
  //   }
  // }

  // useEffect(() => {
  //   onFocued();
  // }, [isChildView])

  // const defaultFormats = [
  //   "header",
  //   "bold",
  //   "italic",
  //   "underline",
  //   "strike",
  //   "blockquote",
  //   "list",
  //   "bullet",
  //   "indent",
  //   "link",
  //   "image",
  //   'emoji',
  //   'mention',
  //   'direction',
  //   'video',
  // ];

  // const dv = defaultValue ? JSON.parse(defaultValue) : ""

  return (
    <div {...style}>
      {/* <ReactQuill
        theme="snow"
        value={inputValue}
        onChange={onEditorChange}
        onKeyDown={onEditorKeyUp}
        onFocus={onFocued}
        onBlur={onBlured}
        placeholder={placeholder}
        
        formats={defaultFormats}
        ref={(el) => { editorRef = el }}
        {...editorProps}
      /> */}
      <FroalaEditor
        model={editorModel}
        onManualControllerReady={handleManualController}
        onModelChange={handleModelChange}
        config={{ quickInsertEnabled: false, key: FROALA_LICENSE_KEY, ...config }}
      />
    </div>
  );
}

TextEditor.defaultProps = {
  classes: {},
  theme: {},
  defaultValue: { ops: [] },
  onChange: () => {},
  onFocus: () => {},
  onBlur: () => {},
  onKeyUp: () => {},
  style: {},
  editorProps: {},
};
export default withStyles(textEditorStyles, { withTheme: true })(TextEditor);
