import React, { useState, useRef, Fragment, useEffect, useContext } from "react";
import { compose } from "redux";
import { connect, useDispatch, useSelector, shallowEqual  } from "react-redux";
import SvgIcon from "@material-ui/core/SvgIcon";
import { withStyles } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Typography from "@material-ui/core/Typography";
import FroalaEditor from "react-froala-wysiwyg";
import Tribute from "tributejs";
import TextEditor from "../TextEditor/TextEditor";
import SendTextIcon from "../../../Icons/SendTextIcon";
import CustomIconButton from "../../../Buttons/CustomIconButton";
import chatEditorStyles from "./style";
import MeetNowIcon from "../../../Icons/MeetNowIcon";
import FileAttachment from "../../../Icons/FileAttachment";
import SendMessageIcon from "../../../Icons/sendMessageIcon";
import FileUploader from "../../../FileUploader/FileUploader";
import SelectionMenu from "../../../Menu/SelectionMenu";
import { saveAttachmentToContextObject } from "../../../../redux/actions/chat";
import DeleteConfirmDialog from "../../../Dialog/ConfirmationDialogs/DeleteConfirmation";
import helper from "../../../../helper/index";
import RemoveIcon from "../../../Icons/RemoveIcon";
import PreviewDocFileIcon from "../../../Icons/PreviewDocFileIcon";
import PreviewCompressFileIcon from "../../../Icons/PreviewCompressFileIcon";
import PreviewAudioVideoFileIcon from "../../../Icons/PreviewAudioVideoFileIcon";
import PreviewImageFileIcon from "../../../Icons/PreviewImageFileIcon";
import IconButton from "../../../Buttons/IconButton";
import { createChatGroups, sendMessage, isChatGroupAlreadyExists, getGroupMessages, createChat } from '../../../../redux/actions/collaboration';
import { uploadFileToS3 } from "../../../../redux/actions/projects";
import CollaborationContext from "../../../../Views/Collaboration/Context/collaboration.context";

// const atValues = [
//   { id: 1, value: "Fredrik Sundqvist" },
//   { id: 2, value: "Patrik Sjölin" }
// ];
// const hashValues = [
//   { id: 3, value: "Fredrik Sundqvist 2" },
//   { id: 4, value: "Patrik Sjölin 2" }
// ];

// const defaultToolbarOptions = [
//   [{ header: [1, 2, 3, 4, 5, false] }],
//   [{ list: "ordered" }, { list: "bullet" }],
//   ["link", "image", 'video'],
//   [{ 'direction': 'rtl' }],
// ]

// const modules = {
//   toolbar: defaultToolbarOptions,
//   // "emoji-toolbar": true,
//   "emoji-textarea": true,
//   mention: {
//     allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
//     mentionDenotationChars: ["@", "#"],
//     source: function (searchTerm, renderList, mentionChar) {
//       let values;

//       if (mentionChar === "@") {
//         values = atValues;
//       } else {
//         values = hashValues;
//       }

//       if (searchTerm.length === 0) {
//         renderList(values, searchTerm);
//       } else {
//         const matches = [];
//         for (let i = 0; i < values.length; i++)
//           if (
//             ~values[i].value.toLowerCase().indexOf(searchTerm.toLowerCase())
//           )
//             matches.push(values[i]);
//         renderList(matches, searchTerm);
//       }
//     }
//   },
// }
let initCont;
// let editor;
let fileAttached;
function ChatTextEditor(props) {
  const dispatch = useDispatch();

  const state = useSelector(state => {
    return {
      group: state && state.collab && state.collab.group && state.collab.group,
      userMessages: state && state.collab && state.collab.group && state.collab.group &&
      state.collab.group._userMessage && state.collab.group._userMessage,
    };
  }, shallowEqual);

  const IMAGE_FORMAT = "IMAGE_FORMAT";
  const DOC_FORMAT = "DOC_FORMAT";
  const AUDIO_VIDEO_FORMAT = "AUDIO_VIDEO_FORMAT";
  const COMPRESS_FORMAT = "COMPRESS_FORMAT";
  const [fileAttachment, setFileAttachment] = useState(null);
  const [deleteDialogueStatus, setDeleteDialogueStatus] = useState(false);
  const [selectedDeleteComment, setSelectedDeleteComment] = useState(null);
  const [group, setGroup] = useState(null);
  const [btnQuery, setBtnQuery] = useState("");
  const [abilityTextEditor, setAbilityTextEditor] = useState(false);
  const [selectedChatUser, setSelectedChatUser] = useState({
    key: "all",
    value: "Everyone",
  });
  const editorRef = useRef(null);
  const {
    classes,
    theme,
    chatData,
    chatConfig,
    chatPermission,
    editComment,
    textEditorAction,
    chatEditorAction,
    startTyping,
    stopTyping,
    chatTypingMembers,
    typingUser,
    id,
    intl,
    inlineEditor,
    setMessage,
    messageList,
    stateContext
  } = props;
  // const [editorModel, setEditorModel] = useState("");
  // const [showToolbar, setShowToolbar] = useState(inlineEditor);
  const [showToolbar, setShowToolbar] = useState(false);
  const [isEditorFocued, setEditorFocued] = useState(false);
  const [placement, setPlacement] = useState("");
  const anchorEl = useRef();
  const [open, setOpen] = useState(false);

  // const [chatText, setChatText] = useState(chatData);
  const [chatHTML, setChatHTML] = useState(chatData);
  // const [chatDelta, setChatDelta] = useState(
  //   chatData
  // );
  const [openFileLoader, setOpenFileLoader] = useState(false);
  const [isEditorChildView, setEditorChildView] = useState(false);
  let _date = new Date();
  const handleFileUpload = result => {
    const item = result.successful[0].response.body[0];
    const attachment = {
      type: item.fileType,
      name: item.fileName,
      [props && props.chatConfig && props.chatConfig.contextKeyId && props.chatConfig.contextKeyId]: props && props.chatConfig && props.chatConfig.contextId,
      groupId: props && props.chatConfig && props.chatConfig.contextId,
      groupType: props && props.chatConfig && props.chatConfig.contextView,
      fileSize: item.fileSize,
      path: item.fileId,
      fileID: item.fileId,
      fileName: item.fileName,
      iconCategory: helper.getFileIconType(item.fileExtension),
      fileExtension: item.fileExtension,
    };
    setFileAttachment(attachment);
    fileAttached = attachment;
  };
  const UploadFileWithCommentHandler = editorResponse => {
    props.saveAttachmentToContextObject(
      props.chatConfig.contextUrl,
      props.chatConfig.contextId,
      [fileAttached],
      result => {
        if (result && result.status === 200) {
          mergeCommentWithFile(
            result.data[0].name,
            result.data[0].id,
            result.data[0].type,
            editorResponse
          );
        }
      },
      error => {
        props.showSnackBar("File Attahment with relative object fail");
      }
    );
    setFileAttachment(null);
    fileAttached = null;
  };

  const mergeCommentWithFile = (name, docsId, fileExtension, editorResponse) => {
    const fileType = {
      doc: 4,
      docx: 4,
      pdf: 5,
      png: 6,
      PNG: 6,
      jpg: 6,
      JPG: 6,
      jpeg: 6,
      JPEG: 6,
      bmp: 6,
      BMP: 6,
      tiff: 6,
      gif: 6,
      GIF: 6,
      xlsx: 7,
      ppt: 8,
    };

    const commentType = fileType[fileExtension] ? fileType[fileExtension] : 1;
    const key = chatConfig.contextKeyId;
    const comment = {
      commentText: name,
      [key]: chatConfig.contextId,
      commentType,
      fileCommentId: docsId,
      commentText: editorResponse.commentText,
      descriptionDelta: "",
      descriptionText: "",
      groupId: editorResponse.groupId,
      groupType: editorResponse.groupType,
      sharedWith: ["all"], // [getMessageMember().key],
      commentId: editorResponse.commentId,
    };
    props.saveFileCommentData(
      comment,
      data => {
        if (data && data.status === 200) {
          setFileAttachment(null);
          fileAttached = null;
          setChatHTML("");
        }
      },
      error => {
        if (error) {
          props.showSnackBar("Server throws error", "error");
        }
      }
    );
  };
  const handleDeleteAttachment = () => {
    setFileAttachment(null);
    fileAttached = null;
  };
  // const addComments = (data) => {
  //   data.sharedWith = ['all'];
  //   props.addComments(data);
  //   editorState = "";
  //   // setEditorModel("");
  //   setChatHTML("");
  // };
  // const openDeleteDialogue = (comment) => {
  //   setDeleteDialogueStatus(true);
  //   setSelectedDeleteComment(comment);
  // };
  const closeDeleteDialog = () => {
    setDeleteDialogueStatus(false);
    setSelectedDeleteComment(null);
  };
  const enableTextEditorHandler = value => {
    setAbilityTextEditor(value);
  };

  // const getMessageMember = () => {
  //   const { replyItem, chatConfig } = props;
  //   let obj;
  //   if (replyItem && replyItem.privateComment) {
  //     let member = props.profileState.data.member.allMembers.find((item) => {
  //       return item.userId == replyItem.userId;
  //     });
  //     if (isEmpty(member)) {
  //       return { key: "all", value: "Everyone" };
  //     } else {
  //       return { key: member.userId, value: member.fullName };
  //     }
  //   }
  //   return selectedChatUser;
  // };
  // const onSelectedChatUserHandler = (option) => {
  //   setSelectedChatUser(option);
  // };
  const handleClick = (event, newPlacement) => {
    const { currentTarget } = event;
    // setEditorChildView(true);
    // setTimeout(() => {
    //   setEditorChildView(false);
    // }, 500);
    const newOPen = placement !== newPlacement || !open;
    setOpen(newOPen);
    setPlacement(newPlacement);
  };
  const onFocus = event => {
    // setShowToolbar(true);
    // chatEditorAction && chatEditorAction("focus");
  };

  const editorFocusedHandler = () => {
    setEditorFocued(true);
    textEditorAction && textEditorAction("focus");
    startTyping && startTyping();
  };

  const editorBluredHandler = () => {
    setEditorFocued(false);
    textEditorAction && textEditorAction("blur");
    stopTyping && stopTyping();
  };
  const urlText = () => {
    return '<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>';
  };
  const isEmpty = text => {
    text = text
      .replaceAll("&nbsp;", "")
      .replaceAll(" ", "")
      .replaceAll("<p>", "")
      .replaceAll("</p>", "")
      .trim();
    // if(text.length == 0)
    // setChatHTML("");
    return text.length == 0;
  };
  String.prototype.replaceAll = function (search, replacement) {
    const target = this;
    return target.replace(new RegExp(search, "g"), replacement);
  };


  useEffect(()=>{
    
  }, [state.userMessages]);

  const sendDataHandlerCollab = () => {
    let _sortedParticipantList = stateContext && stateContext.selectedParticipantList &&
    stateContext.selectedParticipantList.sort((a, b) => a.label.localeCompare(b.label))

    if (state && state.group && JSON.stringify(state.group) !== '{}' ) {

      let messageData = {
        groupId: state.group.groupId,
        messageText: editorRef.current.html.get().trim(),
        attachmentId: ""
      }
      sendMessage(messageData, dispatch, () => {
        // props.showSnackBar("Chat room is created and message is sent successfully!", "success");
        let _messageList = [...messageList];
        _messageList.push({message: messageData.messageText, time: _date, 
          name: profileState && profileState.data.fullName, imageUrl: profileState && profileState.data.imageUrl,
          createdBy: profileState && profileState.data.userId
        });

        stateContext.handleSetMessageList(_messageList);
        editorRef.current.html.set("");

      }, error => {
        if (error) {
          props.showSnackBar("Server throws error", "error");
        }
      })
    }
    else {
      isChatGroupAlreadyExists(_sortedParticipantList, profileState, dispatch, (res) => {
        let _isExist = res && res.data && res.data.entity && res.data.entity.isExist;
        let _messages = res && res.data && res.data.entity && res.data.entity && res.data.entity.entity && res.data.entity.entity.userMessages;
        let _groupId = res && res.data && res.data.entity && res.data.entity && res.data.entity.entity && res.data.entity.entity.groupId;
        let _group = res && res.data && res.data.entity && res.data.entity && res.data.entity.entity && res.data.entity.entity;

        if (_isExist) {
          let date = new Date();
          let messageData = {
            groupId: _groupId,
            messageText: editorRef.current.html.get().trim(),
            attachmentId: ""
          }
          sendMessage(messageData, dispatch, () => {
            getGroupMessages(_group, dispatch, () => {
              stateContext.updateIsNewChatEnabled(false);
              let _groupChatList =  props && props.groupChatList && props.groupChatList;
              _groupChatList.shift();
              if(_groupChatList && _groupChatList.length > 0 )
              {
                for(let ind=0 ; ind < _groupChatList.length; ind++)
                {
                  _groupChatList[ind].isChatCardClicked = false
                }
              } 
              _group.isChatCardClicked = true; 

              _groupChatList.splice(0, 0, _group);
              props && props.updatedList(_groupChatList);
              editorRef.current.html.set("");
             }, error => {
              if (error) {
                props.showSnackBar("Server throws error", "error");
              }
            })
            
          }, error => {
            if (error) {
              props.showSnackBar("Server throws error", "error");
            }
          })
        }

        else {
          createChatGroups(dispatch, (res) => {
            let messageData = {
              groupId: res.data.entity.groupId,
              messageText: editorRef.current.html.get().trim(),
              attachmentId: ""
            }
            let _infoGroup = res && res.data && res.data.entity && res.data.entity;
            stateContext.updateIsNewChatEnabled(false);
            sendMessage(messageData, dispatch, () => {
              // props.showSnackBar("Chat room is created and message is sent successfully!", "success");
              let _messageList = [...messageList];
              _messageList.push({message: messageData.messageText, time: _date, name: profileState && profileState.data.fullName, imageUrl: profileState && profileState.data.imageUrl,
                createdBy: profileState && profileState.data.userId});
              stateContext.handleSetMessageList(_messageList);
              let _groupChatList =  props && props.groupChatList && props.groupChatList;
              _groupChatList.shift();
              if(_groupChatList && _groupChatList.length > 0 )
              {
                for(let ind=0 ; ind < _groupChatList.length; ind++)
                {
                  _groupChatList[ind].isChatCardClicked = false
                }
              } 
              _infoGroup.isChatCardClicked = true; 

              _groupChatList.splice(0, 0, _infoGroup);
              props && props.updatedList(_groupChatList);
              editorRef.current.html.set("");
              
            }, error => {
              if (error) {
                props.showSnackBar("Server throws error", "error");
              }
            })
          }, error => {
            if (error) {
              props.showSnackBar("Server throws error", "error");
            }
          })
        }
      }, error => {
        if (error) {
          props.showSnackBar("Server throws error", "error");
        }
      })

    }
  };

  const sendDataHandler = () => {
    if (fileAttached) {
      const save = {
        commentText: editorRef.current.html.get(),
        // descriptionDelta: "",
        // descriptionText: "",
        groupId: chatConfig.contextId,
        groupType: chatConfig.contextView,
        commentId: new Date().valueOf(),
      };
      UploadFileWithCommentHandler(save);
    } else if (editComment) {

      if (!isEmpty(editorRef.current.html.get())) {
        const save = {
          commentText: editorRef.current.html.get(),
          // descriptionDelta: "",
          // descriptionText: "",
          id: editComment.Id,
          groupId: chatConfig.contextId,
          groupType: chatConfig.contextView,
        };
        props.updateComment(
          save,
          success => {
            // setEditorModel("");
            setChatHTML("");
          },
          fail => { }
        );
      }
    } else if (!isEmpty(editorRef.current.html.get())) {
      const save = {
        commentText: editorRef.current.html.get(),
        // descriptionDelta: "",
        // descriptionText: "",
        [chatConfig.contextKeyId]: chatConfig.contextId,
        groupId: chatConfig.contextId,
        groupType: chatConfig.contextView,
        sharedWith: ["all"],
        commentId: new Date().valueOf(),
        parentId: null,
      };
      props.addComments(save);
      editorRef.current.html.set("");
      setChatHTML("");
    }
  };

  // const handleChatText = (data) => {
  //   setChatHTML(data.html);
  //   setChatHTML(data.text);
  //   setChatDelta(data.contents);
  // }
  const onFileUpload = result => {
    setOpen(false);
    if (result.successful.length > 0) {
      handleFileUpload(result);
      editorRef.current.el.focus();
    }
  };
  const handleClose = event => {
    // setOpen(false);
  };

  // modules.toolbar = updateToolbarOptions ? updateToolbarOptions : defaultToolbarOptions;
  // const getMentions = () => {
  //   let mentionUser = chatConfig.assigneeLists.map(item => {
  //     let user = item;
  //     user.id = item.userId;
  //     user.value = item.fullName;
  //     // user.target = '_blank';
  //     // user.link = 'https://ej2.syncfusion.com/react/documentation/api/file-manager/#toolbarsettings';
  //     return user;
  //   });
  //   return mentionUser;
  // }
  // const getTasks = () => {
  //   return props.tasksState.data
  //     ? props.tasksState.data.map((x) => {
  //       return {
  //         id: x.taskId,
  //         value: x.taskTitle,
  //       };
  //     })
  //     : [];
  // };
  // const getIssues = () => {
  //   return props.issuesState.data
  //     ? props.issuesState.data.map((x) => {
  //       return {
  //         id: x.id,
  //         value: x.title,
  //       };
  //     })
  //     : [];
  // };
  // const getProjects = () => {
  //   return props.projectsState.data
  //     ? props.projectsState.data.map((x) => {
  //       return {
  //         id: x.projectId,
  //         value: x.projectName,
  //       };
  //     })
  //     : [];
  // };
  // const getMeetings = () => {
  //   return props.meetingsState.data
  //     ? props.meetingsState.data.map((x) => {
  //       return {
  //         id: x.meetingId,
  //         value: x.meetingDisplayName
  //       };
  //     })
  //     : [];
  // }
  // const getRistks = () => {
  //   return props.risksState.data
  //     ? props.risksState.data.map((x) => {
  //       return {
  //         id: x.id,
  //         value: x.title,
  //       };
  //     })
  //     : [];
  // }
  // modules.mention = {
  //   allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
  //   mentionDenotationChars: ["@", "/task", "/issue", "/project", "/meeting", "/risk"],
  //   source: function (searchTerm, renderList, mentionChar) {
  //     let values;

  //     if (mentionChar === "@") {
  //       values = getMentions();
  //     } else if (mentionChar === "/task") {
  //       values = getTasks();
  //     } else if (mentionChar === "/issue") {
  //       values = getIssues();
  //     } else if (mentionChar === "/project") {
  //       values = getProjects();
  //     } else if (mentionChar === "/meeting") {
  //       values = getMeetings();
  //     } else if (mentionChar === "/risk") {
  //       values = getRistks();
  //     }

  //     if (searchTerm.length === 0) {
  //       renderList(values, searchTerm);
  //     } else {
  //       const matches = [];
  //       for (let i = 0; i < values.length; i++)
  //         if (
  //           ~values[i].value.toLowerCase().indexOf(searchTerm.toLowerCase())
  //         )

  //           matches.push(values[i]);
  //       renderList(matches, searchTerm);
  //     }
  //   },
  //   dataAttributes: ["id", "value", "denotationChar", "link", "target"],
  //   onOpen: function () {
  //     return true;
  //   },
  //   onClose: function () {
  //     return true;
  //   },
  //   spaceAfterInsert: true,
  //   showDenotationChar: false,
  //   offsetLeft: 15
  // };
  const { tasks, issues, projects, risks, meetings, profileState } = props;

  const taskData = tasks.map(t => {
    return { key: t.taskTitle, value: t.taskId };
  });
  const issueData = issues.map(t => {
    return { key: t.title, value: t.id };
  });
  const projectData = projects.map(t => {
    return { key: t.projectName, value: t.projectId };
  });
  const riskData = risks.map(t => {
    return { key: t.title, value: t.id };
  });
  const meetingData = meetings.map(t => {
    return { key: t.meetingDisplayName, value: t.meetingId };
  });
  let usersData = profileState.data.member.allMembers.map(u => {
    return { key: u.fullName, value: u.userId, obj: u };
  });
  usersData = usersData.filter(u => u.obj.isActive && u.obj.isDeleted == false);
  // const initData = [
  //   { key: "Phil", value: "pheartman" },
  //   { key: "Gordon", value: "gramsey" },
  //   { key: "Jacob", value: "jacob" },
  //   { key: "Ethan", value: "ethan" },
  //   { key: "Emma", value: "emma" },
  //   { key: "Isabella", value: "isabella" },
  // ];

  // An onchange function of editor, accepts a model (The value of the editor)
  const handleModelChange = model => {
    // setEditorModel(model);
    setChatHTML(model);
  };
  // function that provides the editor controls to be intialized later on
  const handleManualController = initControls => {
    initCont = initControls;
  };
  // Manually initializing the instance of editor
  useEffect(() => {
    initCont.initialize();
  }, []);

  useEffect(() => {
     setGroup(state.group)
  }, [state.group]);

  const userMention = new Tribute({
    values: usersData,
    trigger: "@",
    menuItemTemplate(item, i) {
      const getRandomColor = str => {
        let hash = 0;
        if (str) {
          for (let i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
          }
        }

        const h = hash % 360;
        return `hsl(${h}, ${60}%, ${50}%)`;
      };
      return item.original.obj.imageUrl
        ? `<div class='fr-tribute-user-selected-cnt'><img src="${item.original.obj.imageUrl}" width="32" height="32" class='fr-tribute-profile-img'/>${item.original.key}</div>`
        : `
      <div class='fr-tribute-user-selected-cnt'>
      <span class="fr-tribute-user-avatar" style="background-color: ${getRandomColor(
          item.original.key
        )}">${item.original.key[0]}
    </span><span class="fr-tribute-user-name">${item.original.key}</span></div>`;
    },
    selectTemplate(item) {
      return `<a class="fr-deletable fr-tribute">${item.original.key}</a>`;
    },
  });
  const taskMention = new Tribute({
    values: taskData,
    trigger: "/task",
    selectTemplate(item) {
      return `<a href="${BASE_URL}tasks?taskid=${item.original.value}" target="_blank" class="fr-deletable fr-tribute"> ${item.original.key} </a>`;
    },
  });
  const issueMention = new Tribute({
    values: issueData,
    trigger: "/issue",
    selectTemplate(item) {
      return `<a href="${BASE_URL}issues?issueid=${item.original.value}" target="_blank" class="fr-deletable fr-tribute"> ${item.original.key} </a>`;
    },
  });
  const projectMention = new Tribute({
    values: projectData,
    trigger: "/project",
    selectTemplate(item) {
      return `<a href="${BASE_URL}projects?projectid=${item.original.value}" target="_blank" class="fr-deletable fr-tribute"> ${item.original.key} </a>`;
    },
  });
  const riskMention = new Tribute({
    values: riskData,
    trigger: "/risk",
    selectTemplate(item) {
      return `<a href="${BASE_URL}risks?riskid=${item.original.value}" target="_blank" class="fr-deletable fr-tribute"> ${item.original.key} </a>`;
    },
  });
  const meetingMention = new Tribute({
    values: meetingData,
    trigger: "/meeting",
    selectTemplate(item) {
      return `<a href="${BASE_URL}meetings?meetingid=${item.original.value}" target="_blank" class="fr-deletable fr-tribute"> ${item.original.key} </a>`;
    },
  });
  // const [tribute, setTribute] = useState(newTribute)
  const handleChangeMention = () => {
    const tributeWithNewOption = new Tribute({
      values: [
        { key: "Phil1", value: "pheartman" },
        { key: "Gordon1", value: "gramsey" },
        { key: "Jacob1", value: "jacob" },
        { key: "Ethan1", value: "ethan" },
        { key: "Emma1", value: "emma" },
        { key: "Isabella1", value: "isabella" },
      ],
      trigger: "@",
      selectTemplate(item) {
        return `<span class="fr-deletable fr-tribute">${item.original.key}</a></span>`;
      },
    });
    setTribute(tributeWithNewOption);
    tributeWithNewOption.attach(initCont.getEditor().el);
  };

  return (
    <>
      {/* <div id={id} className={classes.customToolbar}> <CustomIconButton
                    btnType="transparent"
                    variant="contained"
                    disableRipple
                    onClick={sendDataHandler}
                  >
                    <SvgIcon
                      viewBox="0 0 28 28"
                      className={classes.sentMessageIconStyles}
                    >
                      <SendMessageIcon color={theme.palette.background.brightBlue} />
                    </SvgIcon>
                  </CustomIconButton>
                  </div> */}

      {/* <ClickAwayListener onClickAway={onBlur}> */}
      <div className={`${classes.textEditorCnt} chatTextEditor`}>
        <div className={classes.editorCnt}>
          {chatTypingMembers.length && !inlineEditor ? (
            <span className={classes.helpTextCnt}>
              <span style={{ color: theme.palette.text.brightBlue }}>
                {[chatTypingMembers.slice(0, -1).join(`, `), chatTypingMembers.slice(-1)[0]].join(chatTypingMembers.length < 2 ? '' : ` and `)}
              </span>{" "}
              <span style={{ color: theme.palette.text.medGray, paddingLeft: 5 }}>
                {chatTypingMembers.length > 1 ? ` are typing...` : ` is typing...`}
              </span>
            </span>
          ) : null}
          {fileAttachment ? (
            <div className={classes.fileAttachmentDetailCnt}>
              <SvgIcon
                viewBox="0 0 32 32"
                style={{
                  fontSize: 32,
                  color: theme.palette.text.grayDarker,
                }}>
                {fileAttachment.iconCategory == IMAGE_FORMAT ? (
                  <PreviewImageFileIcon />
                ) : fileAttachment.iconCategory == DOC_FORMAT ? (
                  <PreviewDocFileIcon />
                ) : fileAttachment.iconCategory == AUDIO_VIDEO_FORMAT ? (
                  <PreviewAudioVideoFileIcon />
                ) : fileAttachment.iconCategory == COMPRESS_FORMAT ? (
                  <PreviewCompressFileIcon />
                ) : (
                  <PreviewDocFileIcon />
                )}
              </SvgIcon>
              <div className={classes.fileAttachmentNameCnt}>
                <div>
                  <Typography variant="h6" style={{ lineHeight: "1.5" }}>
                    {fileAttachment.name}
                  </Typography>
                  <Typography
                    variant="h6"
                    style={{
                      color: theme.palette.text.grayDarker,
                      lineHeight: "1.5",
                    }}>
                    {` ${helper.convertBytes(fileAttachment.fileSize)} - Press enter to upload`}
                  </Typography>
                </div>
                <IconButton btnType="transparent" onClick={handleDeleteAttachment}>
                  <SvgIcon style={{ fontSize: 18 }} viewBox="0 0 18 18">
                    <RemoveIcon />
                  </SvgIcon>
                </IconButton>
              </div>
            </div>
          ) : // : !inlineEditor ? (
            //   <span className={classes.helpTextCnt} onClick={() => setEditorChildView(true)}>
            //     <span style={{ color: theme.palette.text.medGray }}>
            //       Pro Tip:{" "}
            //     </span>
            //     Press "@" to mention team member - Press "/" for command
            //   </span>
            // )
            null}
          <>
            {/* <NewChatTextEditor
                  chatConfig={chatConfig}
                  chatData={chatData}
                  handleNewComments={addComments}
                  fileUpload={handleFileUpload}
                  UploadFileComment={UploadFileWithCommentHandler}
                  fileAttachment={fileAttachment}
                  enableTextEditor={enableTextEditorHandler}
                  abilityTextEditor={abilityTextEditor}
                  selectedChatUser={selectedUser}
                  onSelectedChatUser={onSelectedChatUserHandler}
                  visibleUserDropDown={visibleUserDropDown}
                  enableDropDown={
                    replyItem && replyItem.privateComment ? false : true
                  }
                  updateToolbarOptions={updateToolbarOptions}
                  setHide={setHide}
                  inlineEditor={inlineEditor}
                /> */}
            <div
              id={`${id}container`}
              // onFocus={onFocus}
              className={`${classes.chatEditorCnt} ${showToolbar ? classes.showToolbar : "hideToolbar"
                }`}>
              <TextEditor
                initCont={initCont}
                handleModelChange={handleModelChange}
                handleManualController={handleManualController}
                editorModel={chatHTML}
                editor={editorRef.current}
                config={{
                  heightMax: 200,
                  placeholderText: intl && intl.formatMessage({ id: "common.comment.comment-area.placeholder", defaultMessage: "Type your comment here..." }),
                  attribution: false,
                  toolbarBottom: true,
                  toolbarContainer: `#${id}`,
                  autoFocus: true,
                  imageUploadURL: BASE_URL + 'api/docsfileuploader/UploadSingleDocFileAmazonS3',
                  requestHeaders: { Authorization: helper.getToken() },
                  toolbarButtons: {
                    // Key represents the more button from the toolbar.
                    moreText: {
                      // List of buttons used in the  group.
                      buttons: [
                        "bold",
                        "italic",
                        "underline",
                        "strikeThrough",
                        "fontFamily",
                        "fontSize",
                        "textColor",
                        "backgroundColor",
                        "clearFormatting",
                      ],

                      // Alignment of the group in the toolbar.
                      align: "left",

                      // By default, 3 buttons are shown in the main toolbar. The rest of them are available when using the more button.
                      buttonsVisible: 0,
                    },

                    moreParagraph: {
                      buttons: [
                        "alignLeft",
                        "alignCenter",
                        "alignRight",
                        "alignJustify",
                        "paragraphFormat",
                        "paragraphStyle",
                        "outdent",
                        "indent",
                        "quote",
                      ],
                      align: "left",
                      buttonsVisible: 0,
                    },

                    moreRich: {
                      buttons: ["emoticons", "insertLink"],
                      align: "left",
                      buttonsVisible: 2,
                    },
                    moreMisc: {
                      // buttons: ["html"],
                      align: "left",
                      buttonsVisible: 1,
                    },
                    htmlAllowedEmptyTags: [],
                  },
                  events: {
                    initialized: e => {
                      editorRef.current = initCont.getEditor();
                      editorRef.current.el.focus();
                      userMention.attach(editorRef.current.el);
                      taskMention.attach(editorRef.current.el);
                      issueMention.attach(editorRef.current.el);
                      riskMention.attach(editorRef.current.el);
                      projectMention.attach(editorRef.current.el);
                      meetingMention.attach(editorRef.current.el);
                      editorRef.current.events.on(
                        "keydown",
                        function (e) {
                          if (e.which === 13) {
                          }
                          if (
                            // This will check if user press enter key while mention dropdowns are open
                            e.which === 13 &&
                            (taskMention.isActive ||
                              issueMention.isActive ||
                              riskMention.isActive ||
                              meetingMention.isActive ||
                              projectMention.isActive ||
                              userMention.isActive)
                          ) {
                            return false;
                          }
                          if (
                            e.which === 13 &&
                            isEmpty(editorRef.current.html.get()) &&
                            !fileAttached
                          ) {
                            // This will run in case of empty editor and user press enter key
                            e.preventDefault();
                            return false;
                          }

                          if (e.which === 13 && !e.shiftKey) {
                            // Login on enter key will go inside this block
                            e.preventDefault();
                            if (chatPermission.addComments) sendDataHandler();
                            return false;
                          }
                        },
                        true
                      );
                    },
                    blur: e => {
                      const relatedTarget = e.originalEvent.relatedTarget;
                      const target =
                        relatedTarget && e.originalEvent.relatedTarget.closest(`#${id}container`);
                      if ((target && target.id !== `${id}container`) || target == null) {
                        editorBluredHandler();
                      }
                    },
                    // 'image.uploaded': function(img) {
                    //   return {link:  JSON.parse(img)[0].publicURL }
                    //
                    // },
                    'image.error': function (error, response) {

                    },

                    // 'image.beforePasteUpload': function ($img, response) {
                    //   fetch($img[0].src)
                    //     .then(res => res.blob()) // Gets the response and returns it as a blob
                    //     .then(blob => {
                    //       // Here's where you get access to the blob
                    //       // And you can use it for whatever you want
                    //       // Like calling ref().put(blob)
                    //
                    //       // Here, I use it to make an image appear on the page
                    //         let formdata = new FormData();
                    //
                    //         formdata.append("file", blob, 'image.png');
                    //         uploadFileToS3(formdata, null, (res) => {
                    //           $img[0].attr("src",res.data.publicUrl)
                    //         })
                    //     });
                    // },
                    focus: e => {
                      editorFocusedHandler();
                    },
                  },
                }}
              />
              <div id={id} className={classes.customToolbar}>
                {chatPermission.addAttachment
                  ? !editComment && (
                    <CustomIconButton
                      btnType="transparent"
                      variant="contained"
                      disableRipple
                      className={classes.customBtnStyle}
                      onClick={event => {
                        handleClick(event, "bottom-end");
                      }}
                      style={{
                        marginRight: 30,
                      }}
                      buttonRef={node => {
                        anchorEl.current = node;
                      }}>
                      <SvgIcon viewBox="0 0 16 17.646" className={classes.iconStyles}>
                        <FileAttachment />
                      </SvgIcon>
                    </CustomIconButton>
                  )
                  : null}
                {/* Send Message Icon */}

                {chatPermission.addComments && (
                  <CustomIconButton
                    btnType="transparent"
                    variant="contained"
                    disabled={id === "collaborationEditorToolbar" ? 
                    group ? !!(!fileAttached && !chatHTML)  : 
                      !!(!fileAttached && !chatHTML) && stateContext && stateContext.selectedParticipantList.length
                      : !!(!fileAttached && !chatHTML)}
                    disableRipple
                    onClick={event => {
                      id === "collaborationEditorToolbar" ? sendDataHandlerCollab(event) : sendDataHandler(event);
                    }}>
                    <SvgIcon viewBox="0 0 28 28" className={classes.sentMessageIconStyles}>
                      <SendMessageIcon
                        color={
                          id === "collaborationEditorToolbar" ? 
                          group 
                          ? (fileAttached || chatHTML.trim().length) ? theme.palette.primary.main  : theme.palette.secondary.light :
                          (fileAttached || chatHTML.trim().length) && (stateContext && stateContext.selectedParticipantList.length) ?
                            theme.palette.primary.main
                            : theme.palette.secondary.light :
                            fileAttached || chatHTML.trim().length
                              ? theme.palette.background.brightBlue
                              : theme.palette.secondary.light
                        }
                      />
                    </SvgIcon>
                  </CustomIconButton>
                )}
              </div>
              <SelectionMenu
                open={open}
                closeAction={handleClose}
                placement={placement}
                anchorRef={anchorEl.current}
                style={{
                  marginBottom: 50
                }}
                list={

                  <FileUploader
                    id={id}
                    onComplete={onFileUpload}
                    maxNumberOfFiles={1}
                    style={{ width: 220, height: 550, maxHeight: 550 }}
                    inline
                    customPlugin={[]}
                  />
                }
              />
            </div>
          </>
        </div>

        <DeleteConfirmDialog
          open={deleteDialogueStatus}
          closeAction={closeDeleteDialog}
          cancelBtnText="Cancel"
          successBtnText="Delete"
          alignment="center"
          headingText="Delete"
          successAction={handleDeleteAttachment}
          msgText="Are you sure you want to delete this attachment?"
          btnQuery={btnQuery}
        />
      </div>
      {/* </ClickAwayListener> */}
    </>
  );
}
const mapStateToProps = state => {
  // const typingObject = {};
  // typingObject.typing = state.chat.data.typingStatus.typing;
  // if (typingObject.typing) {
  //   const member = state.profile.data.member.allMembers.find(item => {
  //     return state.chat.data.typingStatus.userId.includes(item.userId);
  //   });
  //   typingObject.userName.push(member.fullName);
  // } else {
  //   typingObject.userName = null;
  // }
  let typingMembers = [];
  let ids = state.chat.data.typingStatus.userIds;
  const member = state.profile.data.member.allMembers.map(item => {
    if (ids.includes(item.userId)) {
      typingMembers.push(item.fullName);
    }
  })

  return {
    profileState: state.profile,
    tasks: state.tasks.data,
    issues: state.issues.data,
    projects: state.projects.data,
    meetings: state.meetings.data,
    risks: state.risks.data,
    chatTypingMembers: typingMembers,
  };
};
ChatTextEditor.defaultProps = {
  editorProps: {},
  onChange: () => { },
  chatData: "",
  classes: {},
  theme: {},
  chatPermission: { addAttachment: true, addComments: true },
};
export default compose(
  connect(mapStateToProps, {
    saveAttachmentToContextObject,
  }),
  withStyles(chatEditorStyles, { withTheme: true })
)(ChatTextEditor);