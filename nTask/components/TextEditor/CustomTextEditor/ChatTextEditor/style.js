import color from "@material-ui/core/colors/amber";

const chatEditorStyles = (theme) => ({
    textEditorCnt: {
        display: 'flex',
        flexDirection: 'column',
        // margin: '0 15px 12px',
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
        boxShadow: '0px 2px 8px #0000000F'
      },
      replyGroup: {
          display: 'flex',
          padding: '7px 12px',
          alignItems: 'center',
          justifyContent: 'space-between',
      },
      replyLeftCnt:{
          display: 'flex',
          alignItems: 'center',
      },
      replyCnt: {
          display: 'flex',
          flexDirection: 'column',
          marginLeft: 8,
          fontSize: "12px !important"
      },
      replyTxt:{
        //   marginTop: 3,
          color: theme.palette.text.darkGray,
          lineHeight: '1.15',
          '& p':{
              margin: 0,
              padding: 0,
          }
      },
      helpTextCnt: {
          display: 'flex',
          fontSize: "11px !important",
          color: theme.palette.text.grayDarker,
          padding: '6px 8px',
      },
      fileAttachmentDetailCnt: {
        display: "flex",
        padding: "5px 10px",
        alignItems: 'center',
        border: `1px solid  ${theme.palette.background.grayLighter}`,
        borderBottom: 'none',
        background: theme.palette.common.white,
    },
    fileAttachmentNameCnt: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        paddingLeft: 8,
        lineHeight: '1.15!important'
    },
    textEditorCnt: {
        // background: theme.palette.common.white,
    },
    chatEditorCnt: {
        width: "100%",
        position: "relative"
    },
    chatEditorActionBtnCnt: {
        position: "absolute",
        right: 0,
        bottom: 6,
        right: 10
    },
    iconStyles: {
        fontSize: "20px !important",
        color: theme.palette.icons.darkGray
    },
    sentMessageIconStyles: {
        fontSize: "20px !important",
    },
    customBtnStyle: {
        marginRight: 10,
    },
    showToolbar: {
        marginTop: 31,
    },
    customToolbar: {
        position: 'relative',
    '& > button': {
        position: 'absolute',
        right: 5,
        bottom: 4,
        zIndex: 11
    }
    }
});

export default chatEditorStyles
