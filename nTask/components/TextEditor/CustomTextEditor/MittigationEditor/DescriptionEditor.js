/* eslint-disable no-undef */
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Tribute from "tributejs";
import TextEditor from "../TextEditor/TextEditor";
import axios from "axios";
import helper from "../../../../helper/index";

let initCont;
let editor;
let editorState;

function DescriptionEditor(props) {
  const {
    onChange,
    defaultValue,
    handleClickAway,
    placeholder,
    tasks,
    issues,
    projects,
    risks,
    meetings,
    profileState,
    disableEdit
  } = props;

  const taskData = tasks.map(t => {
    return { key: t.taskTitle, value: t.taskId };
  });
  const issueData = issues.map(t => {
    return { key: t.title, value: t.id };
  });
  const projectData = projects.map(t => {
    return { key: t.projectName, value: t.projectId };
  });
  const riskData = risks.map(t => {
    return { key: t.title, value: t.id };
  });
  const meetingData = meetings.map(t => {
    return { key: t.meetingDisplayName, value: t.meetingId };
  });
  const usersData = profileState.data.member.allMembers.map(u => {
    return { key: u.fullName, value: u.userId, obj: u };
  });

  const [editorModel, setEditorModel] = useState("");

  const urlText = () => {
    return '<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>';
  };
  // An onchange function of editor, accepts a model (The value of the editor)
  const handleModelChange = model => {
    model = model;
    setEditorModel(model);
    onChange({ html: model });
    editorState = model;
  };
  // function that provides the editor controls to be intialized later on
  const handleManualController = initControls => {
    initCont = initControls;
  };
  // Manually initializing the instance of editor
  useEffect(() => {
    initCont.initialize();
  }, []);

  // Initializign the editor witht he default value
  useEffect(() => {
    if (editorModel !== defaultValue) {
      setEditorModel(defaultValue);
    }
  }, [defaultValue]);

  const userMention = new Tribute({
    values: usersData,
    trigger: "@",
    menuItemTemplate(item, i) {
      const getRandomColor = str => {
        let hash = 0;
        if (str) {
          for (let i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
          }
        }

        const h = hash % 360;
        return `hsl(${h}, ${60}%, ${50}%)`;
      };
      return item.original.obj.imageUrl
        ? `<div class='fr-tribute-user-selected-cnt'><img src="${item.original.obj.imageUrl}" 
        width="32" height="32" class='fr-tribute-profile-img'/>${item.original.key}</div>`
        : `
      <div class='fr-tribute-user-selected-cnt'>
      <span class="fr-tribute-user-avatar" style="background-color: ${getRandomColor(
        item.original.key
      )}">${item.original.key[0]}
    </span><span class="fr-tribute-user-name">${item.original.key}</span></div>`;
    },
    selectTemplate(item) {
      return `<a class="fr-deletable fr-tribute">${item.original.key}</a>`;
    },
  });
  const taskMention = new Tribute({
    values: taskData,
    trigger: "/task",
    selectTemplate(item) {
      return `<a href="${BASE_URL}tasks?taskid=${item.original.value}" 
      target="_blank" class="fr-deletable fr-tribute"> ${item.original.key} </a>`;
    },
  });
  const issueMention = new Tribute({
    values: issueData,
    trigger: "/issue",
    selectTemplate(item) {
      return `<a href="${BASE_URL}issues?issueid=${item.original.value}" 
      target="_blank" class="fr-deletable fr-tribute"> ${item.original.key} </a>`;
    },
  });
  const projectMention = new Tribute({
    values: projectData,
    trigger: "/project",
    selectTemplate(item) {
      return `<a href="${BASE_URL}projects?projectid=${item.original.value}" 
      target="_blank" class="fr-deletable fr-tribute"> ${item.original.key} </a>`;
    },
  });
  const riskMention = new Tribute({
    values: riskData,
    trigger: "/risk",
    selectTemplate(item) {
      return `<a href="${BASE_URL}risks?riskid=${item.original.value}" 
      target="_blank" class="fr-deletable fr-tribute"> ${item.original.key} </a>`;
    },
  });
  const meetingMention = new Tribute({
    values: meetingData,
    trigger: "/meeting",
    selectTemplate(item) {
      return `<a href="${BASE_URL}meetings?meetingid=${item.original.value}" 
      target="_blank" class="fr-deletable fr-tribute"> ${item.original.key} </a>`;
    },
  });
  return (
    <TextEditor
      initCont={initCont}
      handleModelChange={handleModelChange}
      handleManualController={handleManualController}
      editorModel={editorModel}
      config={{
        // toolbarBottom: true,
        attribution: false,
        // imageUploadURL: `${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL}/api/docsfileuploader/UploadMultipleDocsFileAmazonS3Public`,
        // imageUploadMethod: "POST",
        requestHeaders: {
          authorization: helper.getToken() || null,
        },
        imageUploadURL: BASE_URL + 'api/docsfileuploader/UploadSingleDocFileAmazonS3',
        imageUpload: true,
        imageInsertButtons: ["imageBack", "|", "imageUpload", "imageByURL"],
        tableEditButtons: ['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns', 'tableStyle', '-', 'tableCells', 'tableCellBackground', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
        toolbarButtons: {
          // Key represents the more button from the toolbar.
          moreText: {
            // List of buttons used in the  group.
            buttons: [
              "bold",
              "italic",
              "underline",
              "strikeThrough",
              "subscript",
              "superscript",
              "fontFamily",
              "fontSize",
              "textColor",
              "backgroundColor",
              "inlineClass",
              "inlineStyle",
              "clearFormatting",
            ],

            // Alignment of the group in the toolbar.
            align: "left",

            // By default, 3 buttons are shown in the main toolbar.
            // The rest of them are available when using the more button.
            buttonsVisible: 2,
          },

          moreParagraph: {
            buttons: [
              "alignLeft",
              "alignCenter",
              "alignRight",
              "alignJustify",
              "formatOL",
              "formatUL",
              "paragraphFormat",
              "paragraphStyle",
              "lineHeight",
              "outdent",
              "indent",
              "quote",
            ],
            align: "left",
            buttonsVisible: 2,
          },

          moreRich: {
            buttons: [
              "insertLink",
              "insertImage",
              "insertVideo",
              "insertTable",
              "emoticons",
              "fontAwesome",
              "specialCharacters",
              "embedly",
              "insertHR",
            ],
            align: "left",
            buttonsVisible: 3,
          },

          moreMisc: {
            buttons: [
              "undo",
              "redo",
              "fullscreen",
              "print",
              "getPDF",
              "spellChecker",
              "selectAll",
              // "html",
              "help",
            ],
            align: "right",
            buttonsVisible: 1,
          },
        },
        events: {
          initialized: e => {
            editor = initCont.getEditor();
            disableEdit && editor.edit.off();
            editor.toolbar.hide();
            userMention.attach(editor.el);
            taskMention.attach(editor.el);
            issueMention.attach(editor.el);
            riskMention.attach(editor.el);
            projectMention.attach(editor.el);
            meetingMention.attach(editor.el);
            editor.events.on(
              "keydown",
              function(event) {
                if (
                  // This will check if user press enter key while mention dropdowns are open
                  event.which === 13 &&
                  (taskMention.isActive ||
                    issueMention.isActive ||
                    riskMention.isActive ||
                    meetingMention.isActive ||
                    projectMention.isActive ||
                    userMention.isActive)
                ) {
                  return false;
                }
              },
              true
            );
          },
          focus: e => {
            editor.toolbar.show();
          },
          blur: () => {
            handleClickAway(editorState);
            editor.toolbar.hide();
          },
          "image.beforeUpload": e => {
            var data = new FormData();
            data.append("File", e[0]);
            axios
              .post(`${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL}/api/docsfileuploader/UploadMultipleDocsFileAmazonS3`, data, {
              // .post(`https://fm.naxxa.io/api/file/uploads`, data, {
                headers: {
                  accept: "application/json",
                  Authorization: helper.getToken(),
                },
              })
              .then(res => {
                let path = res.data[0].publicURL;
                editor.image.insert(path, null, null, editor.image.get());
              })
              .catch(err => {
                console.log(err);
              });
            return false;
          },
          "video.beforeUpload": e => {
            var data = new FormData();
            data.append("File", e[0]);
            axios
              .post(`${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL}/api/docsfileuploader/UploadMultipleDocsFileAmazonS3`, data, {
              // .post(`https://fm.naxxa.io/api/file/uploads`, data, {
                headers: {
                  accept: "application/json",
                  Authorization: helper.getToken(),
                },
              })
              .then(res => {
                // let path = `${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL}api/docsfileuploader/downloaddocsfilesamazons3?url=${res.data[0].fileId}&fileName=${res.data[0].fileName}`
                // let path = res.data[0].publicURL;
                // editor.video.insert(path, null, null, editor.video.get());
                let video_url = res.data[0].publicURL;
                let videoSplitArr = video_url.split(".");
                let videoextension = videoSplitArr[videoSplitArr.length - 1];
                // let videoPosterLink = video_url.replace(videoextension,'png');
                let videoTag = `<video width="320" height="240" controls >
                     <source src="${video_url}" type="video/${videoextension}">
                 </video>`; // we have to embed video tag
                editor.video.insert(videoTag);
                return false;
              })
              .catch(err => {});
            return false;
          },
          "image.uploaded": () => {},
        },
        placeholderText: placeholder,
      }}
    />
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile,
    tasks: state.tasks.data,
    issues: state.issues.data,
    projects: state.projects.data,
    meetings: state.meetings.data,
    risks: state.risks.data,
  };
};
DescriptionEditor.defaultProps = {
  editorProps: {},
  onEnterPress: () => {},
  handleClickAway: () => {},
  placeholder: "Type something...",
  disableEdit: false
};
export default connect(mapStateToProps)(DescriptionEditor);
