import React from 'react';
import createMentionPlugin from "draft-js-mention-plugin";

const mentions = [
    {
      name: "Matthew Russell",
      avatar:
        "https://pbs.twimg.com/profile_images/517863945/mattsailing_400x400.jpg"
    },
    {
      name: "Julian Krispel-Samsel",
      avatar: "https://avatars2.githubusercontent.com/u/1188186?v=3&s=400"
    },
    {
      name: "Jyoti Puri",
      avatar: "https://avatars0.githubusercontent.com/u/2182307?v=3&s=400"
    },
    {
      name: "Max Stoiber",
      avatar:
        "https://pbs.twimg.com/profile_images/763033229993574400/6frGyDyA_400x400.jpg"
    },
    {
      name: "Nik Graf",
      avatar: "https://avatars0.githubusercontent.com/u/223045?v=3&s=400"
    },
    {
      name: "Pascal Brandt",
      avatar:
        "https://pbs.twimg.com/profile_images/688487813025640448/E6O6I011_400x400.png"
    },
    {
      name: "Łukasz Bąk",
      avatar: "https://randomuser.me/api/portraits/men/36.jpg"
    }
  ];
  const tasks = [
    { id: 0, name: "task1" },
    { id: 1, name: "task2" },
    { id: 2, name: "task3" }
  ];
  const issues = [
    { id: 0, name: "issue1" },
    { id: 1, name: "issue2" },
    { id: 2, name: "issue3" }
  ];
  const projects = [
    { id: 0, name: "project1" },
    { id: 1, name: "project2" },
    { id: 2, name: "project3" }
  ];
  const risks = [
    { id: 0, name: "risk1" },
    { id: 1, name: "risk2" },
    { id: 2, name: "risk3" }
  ];
  const meetings = [
    { id: 0, name: "meeting1" },
    { id: 1, name: "meeting2" },
    { id: 2, name: "meeting3" }
  ];

export const UserMention = createMentionPlugin({
    mentions,
    entityMutability: "IMMUTABLE",
    supportWhitespace: false,
    mentionComponent: (mentionProps) => {
      return (
      <span
        className="draftMentionTag"
        // eslint-disable-next-line no-alert
        onClick={() => alert('Clicked on the Mention!')}
      >
        @{mentionProps.children}
      </span>
    )},
  });
export const TaskMention = createMentionPlugin({
    tasks,
    entityMutability: "IMMUTABLE",
    mentionTrigger: "/task",
    supportWhitespace: false,
    mentionComponent: (mentionProps) => {
      return (
      <span
        className="draftMentionTag"
        // eslint-disable-next-line no-alert
        onClick={() => alert('Clicked on the Mention!')}
      >
        Task: {mentionProps.children}
      </span>
    )},
  });
export const ProjectMention = createMentionPlugin({
    projects,
    entityMutability: "IMMUTABLE",
    mentionTrigger: "/project",
    supportWhitespace: false,
    mentionComponent: (mentionProps) => {
      return (
      <span
        className="draftMentionTag"
        // eslint-disable-next-line no-alert
        onClick={() => alert('Clicked on the Mention!')}
      >
        Project: {mentionProps.children}
      </span>
    )},
  });
  export const MeetingMention = createMentionPlugin({
    meetings,
    entityMutability: "IMMUTABLE",
    mentionTrigger: "/meeting",
    supportWhitespace: false,
    mentionComponent: (mentionProps) => {
      return (
      <span
        className="draftMentionTag"
        // eslint-disable-next-line no-alert
        onClick={() => alert('Clicked on the Mention!')}
      >
        Meeting: {mentionProps.children}
      </span>
    )},
  });
  export const IssueMention = createMentionPlugin({
    issues,
    entityMutability: "IMMUTABLE",
    mentionTrigger: "/issue",
    supportWhitespace: false,
    mentionComponent: (mentionProps) => {
      return (
      <span
        className="draftMentionTag"
        // eslint-disable-next-line no-alert
        onClick={() => alert('Clicked on the Mention!')}
      >
        Issue: {mentionProps.children}
      </span>
    )},
  });
  export const RiskMention = createMentionPlugin({
    risks,
    entityMutability: "IMMUTABLE",
    mentionTrigger: "/risk",
    supportWhitespace: false,
    mentionComponent: (mentionProps) => {
      return (
      <span
        className="draftMentionTag"
        // eslint-disable-next-line no-alert
        onClick={() => alert('Clicked on the Mention!')}
      >
        Risk: {mentionProps.children}
      </span>
    )},
  });