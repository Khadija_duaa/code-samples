import React, { Component } from 'react';
import HeadlinesPicker from "./HeadlinePicker";

class HeadlinesButton extends Component {
    handleClick = () => {
    
      // A button can call `onOverrideContent` to replace the content
      // of the toolbar. This can be useful for displaying sub
      // menus or requesting additional information from the user.
      this.props.onOverrideContent(HeadlinesPicker);
    }
    render() {
      return (
        <div className="headlineButtonWrapper">
          <button onClick={this.handleClick} className="headlineButton">
            H
          </button>
        </div>
      );
    }
  }

  export default HeadlinesButton;