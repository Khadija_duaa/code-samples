import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import BulkActionsStyle from "./BulkActions.style.js";
import IconPeopleOutLine from "../Icons/IconPeopleOutLine.js";
import IconPoriorityOutLine from "../Icons/IconPoriorityOutLine.js";
import IconColorOutLine from "../Icons/IconColorOutLine.js";
import IconExportOutLine from "../Icons/IconExportOutLine.js";
import IconArchiveOutLine from "../Icons/IconArchiveOutLine.js";
import IconDeleteOutLine from "../Icons/IconDeleteOutLine.js";
import IconCloseOutLine from "../Icons/IconCloseOutLine.js";
import AssigneeDropdown from "../Dropdown/AssigneeDropdown2";
// import FlagDropDown from "../../Views/Task/Grid/FlagDropdown";
import StatusDropdown from "../Dropdown/StatusDropdown/Dropdown";
import ColorPickerDropdown from "../Dropdown/ColorPicker/Dropdown";
import { useDispatch, useSelector } from "react-redux";
import { statusData, severity as severityData, typeData } from "../../helper/issueDropdownData";
import RoundIcon from "@material-ui/icons/Brightness1";
import RoundIconOutline from "@material-ui/icons/Brightness1Outlined";
import TypeIcon from "@material-ui/icons/Star";

import {
  exportBulkIssue,
  archiveBulkIssue,
  unArchiveBulkIssue,
  updateBulkIssue,
  deleteBulkIssue,
} from "../../redux/actions/issues";

import { FormattedMessage, injectIntl } from "react-intl";
import { compose } from "redux";
import { priorityData } from "../../helper/taskDropdownData";
import fileDownload from "js-file-download";
import { setDeleteDialogueState } from "../../redux/actions/allDialogs";
import { withSnackbar } from "notistack";
import { CanAccessFeature } from "../AccessFeature/AccessFeature.cmp.js";
import ActionConfirmation from "../Dialog/ConfirmationDialogs/ActionConfirmation.js";
import { addPendingHandler } from "../../redux/actions/backProcesses.js";

const Issuecmp = ({ classes, clearSelection, selectedIssues, theme, intl, enqueueSnackbar }) => {
  const [assigneeEl, setAssigneeEl] = useState(null);
  const [priorityEl, setPriorityEl] = useState(null);
  const [statusEl, setStatusEl] = useState(null);
  const [severityEl, setSeverityEl] = useState(null);
  const [typeEl, setTypeEl] = useState(null);
  const [colorEl, setColorEl] = useState(null);
  const { quickFilters } = useSelector(state => {
    return {
      quickFilters: state.issues.quickFilters || {},
    };
  });
  const dispatch = useDispatch();
  // handle bulk update assignee
  const updateAssignee = props => {
    const postObj = { assigneIds: [props.userId], issueIds: getSelectedIssues(), type: "array" };
    setAssigneeEl(null);
    updateBulkIssue(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} Issues assignee updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectAssignee = e => {
    e.stopPropagation();
    setAssigneeEl(e.currentTarget);
  };
  const handleCloseAssignee = () => {
    setAssigneeEl(null);
  };
  // Priority
  const updatePriority = props => {
    const postObj = { priority: props.label, issueIds: getSelectedIssues() };
    setPriorityEl(null);
    updateBulkIssue(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} Issues priority updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  // Status
  const updateStatus = props => {
    const postObj = { status: props.label, issueIds: getSelectedIssues() };
    setStatusEl(null);
    updateBulkIssue(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} Issues status updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  // Type
  const updateType = props => {
    const postObj = { type: props.label, issueIds: getSelectedIssues() };
    setTypeEl(null);
    updateBulkIssue(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} Issues type updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  // Severity
  const updateSeverity = props => {
    const postObj = { severity: props.label, issueIds: getSelectedIssues() };
    setSeverityEl(null);
    updateBulkIssue(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} Issues severity updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectPriority = e => {
    e.stopPropagation();
    setPriorityEl(e.currentTarget);
  };
  const handleSelectStatus = e => {
    e.stopPropagation();
    setStatusEl(e.currentTarget);
  };
  const handleSelectSeverity = e => {
    e.stopPropagation();
    setSeverityEl(e.currentTarget);
  };
  const handleSelectType = e => {
    e.stopPropagation();
    setTypeEl(e.currentTarget);
  };
  const handleClosePriority = () => {
    setPriorityEl(null);
  };
  const handleCloseType = () => {
    setTypeEl(null);
  };
  const handleCloseStatus = () => {
    setStatusEl(null);
  };
  const handleCloseSeverity = () => {
    setSeverityEl(null);
  };
  // Color
  const updateColor = color => {
    const postObj = { colorCode: color, issueIds: getSelectedIssues() };
    setColorEl(null);
    updateBulkIssue(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} Issues color updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectColor = e => {
    e.stopPropagation();
    setColorEl(e.currentTarget);
  };
  const handleCloseColor = () => {
    setColorEl(null);
  };
  // get issueids use map
  const getSelectedIssues = () => {
    return selectedIssues.map(item => {
      return item.data.id;
    });
  };
  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };
  // export api function
  const handleExportIssue = props => {
    const postObj = { issueIds: getSelectedIssues() };
    const obj = {
      type: 'issues',
      apiType: 'post',
      data: postObj,
      fileName: 'issues.xlsx',
      apiEndpoint: 'api/issues/exportbulk',
    }
    addPendingHandler(obj, dispatch);

    // exportBulkIssue(postObj, dispatch, res => {
    //   fileDownload(res.data, "issues.xlsx");
    //   showSnackBar(`${selectedIssues.length} issues exported successfully`, "success");
    // },
    //   (err) => {
    //     if (err.response.status == 405) {
    //       showSnackBar("You don't have sufficient rights to export issues", "error");
    //     }  else {
    //       showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //     }
    //   });
  };
  // delete api function
  const handleDeleteIssue = props => {
    dispatch(setDeleteDialogueState({ btnQuery: "progress" }));
    const postObj = { issueIds: getSelectedIssues() };
    deleteBulkIssue(
      postObj,
      dispatch,
      res => {
        // Success
        closeDeleteConfirmation();
        showSnackBar(`${res.length} issues deleted successfully`, "success");
        clearSelection();
      },
      () => {
        // failure
        closeDeleteConfirmation();
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  //Confirmation Dialog close
  const closeDeleteConfirmation = () => {
    dispatch(setDeleteDialogueState({ open: false, btnQuery: "" }));
  };
  //Open delete confirmation dialog
  const handleDeleteConfirmation = success => {
    const dialogObj = {
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: `Are you sure you want to delete these ${selectedIssues.length} issues?`,
      successAction: handleDeleteIssue,
      closeAction: closeDeleteConfirmation,
      btnQuery: "",
    };
    dispatch(setDeleteDialogueState(dialogObj));
  };
  // delete api function
  const handleArchiveIssue = props => {
    const postObj = { issueIds: getSelectedIssues() };
    setArchiveBtnQuery('progress');
    archiveBulkIssue(
      postObj,
      dispatch,
      res => {
        //Success
        setArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(`${res.length} issues archived successfully`, "success");
        clearSelection();
      },
      () => {
        // Failure
        setArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleUnArchiveIssue = props => {
    const postObj = { issueIds: getSelectedIssues() };
    setUnArchiveBtnQuery('progress');
    unArchiveBulkIssue(
      postObj,
      dispatch,
      res => {
        //Success
        setUnArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(`${res.length} issues unarchived successfully`, "success");
        clearSelection();
      },
      () => {
        // Failure
        setUnArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const [showConfirmation, setShowConfirmation] = useState(false)
  const [inputType, setInputType] = useState('')
  const [archiveBtnQuery, setArchiveBtnQuery] = useState('')
  const [unarchiveBtnQuery, setUnArchiveBtnQuery] = useState('')
  const handleArchiveConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Archive");
  };
  const handleUnArchiveConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Unarchive");
  };
  const closeConfirmationPopup = () => {
    setShowConfirmation(false)
    setStep(0)
  }
  // const projectData = generateProjectData(projects);
  const issuePriorityData = priorityData(theme, classes, intl);
  const issueStatusData = statusData(theme, classes, intl);
  const issueSeverity = severityData(theme, classes, intl);
  const issueTypeData = typeData(theme, classes, intl);
  const isArchived = quickFilters["Archived"] ? true : false;
  return (
    <>
      <div className={classes.mainTaskCmp}>
        <ul className={classes.bulkActionBtnsList}>
          <li className={classes.selectedTasks}>
            <span>{selectedIssues && selectedIssues.length}</span>Issues selected
          </li>
          {!isArchived && (
            <>
              <CanAccessFeature group='issue' feature='assignee'>
                <li className={classes.issueListItems}>
                  <AssigneeDropdown
                    updateAction={updateAssignee}
                    obj={{}}
                    showSelected={false}
                    singleSelect={true}
                    handleCloseAssignee={handleCloseAssignee}
                    customBtnRender={
                      <div className={classes.renderTask} onClick={handleSelectAssignee}>
                        <span className={classes.issueCmpIcon}>
                          <IconPeopleOutLine />
                        </span>
                        Assignee
                      </div>
                    }
                    anchorEl={assigneeEl}
                  />
                </li>
              </CanAccessFeature>
              <CanAccessFeature group='issue' feature='priority'>
                <li className={classes.issueListItems}>
                  <StatusDropdown
                    onSelect={updatePriority}
                    obj={{}}
                    singleSelect={true}
                    options={issuePriorityData}
                    handleCloseCallback={handleClosePriority}
                    customStatusBtnRender={
                      <div onClick={handleSelectPriority} className={classes.renderTask}>
                        <span className={classes.issueCmpIcon}>
                          <IconPoriorityOutLine />
                        </span>
                        Priority
                      </div>
                    }
                    dropdownProps={{
                      placement: "top-start",
                    }}
                    anchorEl={priorityEl}
                  />
                </li>
              </CanAccessFeature>
              <CanAccessFeature group='issue' feature='severity'>
                <li className={classes.issueListItems}>
                  <StatusDropdown
                    onSelect={updateSeverity}
                    obj={{}}
                    singleSelect={true}
                    options={issueSeverity}
                    handleCloseCallback={handleCloseSeverity}
                    customStatusBtnRender={
                      <div onClick={handleSelectSeverity} className={classes.renderTask}>
                        <span className={classes.issueCmpIcon}>
                          <RoundIconOutline />
                        </span>
                        Severity
                      </div>
                    }
                    dropdownProps={{
                      placement: "top-start",
                    }}
                    anchorEl={severityEl}
                  />
                </li>
              </CanAccessFeature>
              <CanAccessFeature group='issue' feature='type'>
                <li className={classes.issueListItems}>
                  <StatusDropdown
                    onSelect={updateType}
                    obj={{}}
                    singleSelect={true}
                    options={issueTypeData}
                    handleCloseCallback={handleCloseType}
                    customStatusBtnRender={
                      <div onClick={handleSelectType} className={classes.renderTask}>
                        <span className={classes.issueCmpIconType}>
                          <TypeIcon />
                        </span>
                        Type
                      </div>
                    }
                    dropdownProps={{
                      placement: "top-start",
                    }}
                    anchorEl={typeEl}
                  />
                </li>
              </CanAccessFeature>
              <CanAccessFeature group='issue' feature='status'>
                <li className={classes.issueListItems}>
                  <StatusDropdown
                    onSelect={updateStatus}
                    obj={{}}
                    singleSelect={true}
                    options={issueStatusData}
                    handleCloseCallback={handleCloseStatus}
                    customStatusBtnRender={
                      <div onClick={handleSelectStatus} className={classes.renderTask}>
                        <span className={classes.issueCmpIcon}>
                          <RoundIcon />
                        </span>
                        Status
                      </div>
                    }
                    dropdownProps={{
                      placement: "top-start",
                    }}
                    anchorEl={statusEl}
                  />
                </li>
              </CanAccessFeature>
              <li className={classes.issueListItems}>
                <ColorPickerDropdown
                  onSelect={updateColor}
                  obj={{}}
                  singleSelect={true}
                  handleCloseColor={handleCloseColor}
                  customColorBtnRender={
                    <div onClick={handleSelectColor} className={classes.renderTask}>
                      <span className={classes.issueCmpIcon}>
                        <IconColorOutLine />
                      </span>
                      Color
                    </div>
                  }
                  anchorEl={colorEl}
                />
              </li>
              <li className={classes.issueListItems} onClick={handleExportIssue}>
                <span className={classes.issueCmpIcon}>
                  <IconExportOutLine />
                </span>
                Export
              </li>
              <li className={classes.issueListItems} onClick={handleArchiveConfirmation}>
                <span className={classes.issueCmpIcon}>
                  <IconArchiveOutLine />
                </span>
                Archive
              </li>
            </>
          )}
          {isArchived && (
            <li className={classes.issueListItems} onClick={handleUnArchiveConfirmation}>
              <span className={classes.issueCmpIcon}>
                <IconArchiveOutLine />
              </span>
              UnArchive
            </li>
          )}
          <li className={classes.issueDeleteIcon} onClick={handleDeleteConfirmation}>
            <span className={classes.issueCmpIcon}>
              <IconDeleteOutLine />
            </span>
            Delete
          </li>
          <li className={classes.issueCrossIcon} onClick={clearSelection}>
            <span className={classes.iconClose}>
              <IconCloseOutLine />
            </span>
          </li>
        </ul>
      </div>
      {inputType === "Archive" /* Confirmation modal for Archive the bulk tasks  */ ? (
        <>
          <ActionConfirmation
            open={showConfirmation}
            closeAction={closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.issue.label"
                defaultMessage="Archive issue"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.issue.message"
                defaultMessage={`Are you sure you want to archive these ${selectedIssues.length} issue?`}
                values={{ l: selectedIssues.length }}
              />
            }
            successAction={handleArchiveIssue}
            btnQuery={archiveBtnQuery}
          />
        </>
      ) : inputType === "Unarchive" /* Confirmation modal for Unarchive the bulk tasks  */ ? (
        <>
          <ActionConfirmation
            open={showConfirmation}
            closeAction={closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.issue.label"
                defaultMessage="Unarchive issues"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            msgText={
              <FormattedMessage
                id="common.un-archived.issue.message"
                defaultMessage={`Are you sure you want to unarchive these ${selectedIssues.length} issues?`}
                values={{ l: selectedIssues.length }}
              />
            }
            successAction={handleUnArchiveIssue}
            btnQuery={unarchiveBtnQuery}
          />
        </>
      ) : null}
    </>
  );
};
export default compose(
  withSnackbar,
  injectIntl,
  withStyles(BulkActionsStyle, { withTheme: true })
)(Issuecmp);
