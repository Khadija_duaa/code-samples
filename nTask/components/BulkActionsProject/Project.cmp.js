import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import BulkActionsStyle from "./BulkActions.style.js";
import IconPeopleOutLine from "../Icons/IconPeopleOutLine.js";
import IconStatusOutLine from "../Icons/IconStatusOutLine.js";
import IconPoriorityOutLine from "../Icons/IconPoriorityOutLine.js";
import IconFloderOutLine from "../Icons/IconFloderOutLine..js";
import IconColorOutLine from "../Icons/IconColorOutLine.js";
import IconExportOutLine from "../Icons/IconExportOutLine.js";
import IconArchiveOutLine from "../Icons/IconArchiveOutLine.js";
import IconDeleteOutLine from "../Icons/IconDeleteOutLine.js";
import IconCloseOutLine from "../Icons/IconCloseOutLine.js";
import AssigneeDropdown from "../../components/Dropdown/AssigneeDropdown2";
// import FlagDropDown from "../../Views/Task/Grid/FlagDropdown";
import StatusDropdown from "../../components/Dropdown/StatusDropdown/Dropdown";
import ColorPickerDropdown from "../../components/Dropdown/ColorPicker/Dropdown";
import { useDispatch, useSelector } from "react-redux";
import RoundIcon from "@material-ui/icons/Brightness1";
import { FormattedMessage, injectIntl } from "react-intl";
import {
  exportBulkProject,
  updateBulkProject,
  deleteBulkProject,
  archiveBulkProject,
  unArchiveBulkProject,
} from "../../redux/actions/projects"
import { statusData } from "../../helper/projectDropdownData";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import DeleteBulkProjectDialogContent from "../../components/Dialog/Popups/bulkDeleteConfirmation";
import { compose } from "redux";
import { priorityData } from "../../helper/taskDropdownData";
import fileDownload from "js-file-download";
import { setDeleteDialogueState, updateDeleteDialogState } from "../../redux/actions/allDialogs";
import { withSnackbar } from "notistack";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import { CanAccessFeature } from "../../components/AccessFeature/AccessFeature.cmp";
import { addPendingHandler } from "../../redux/actions/backProcesses";

const Projectcmp = ({ classes, clearSelection, selectedProjects, theme, intl, enqueueSnackbar }) => {
  const [assigneeEl, setAssigneeEl] = useState(null);
  const [statusEl, setStatusEl] = useState(null);
  const [colorEl, setColorEl] = useState(null);

  const [inputType, setInputType] = useState('')
  const [deleteBtnQuery, setDeleteBtnQuery] = useState('')
  const [archiveBtnQuery, setArchiveBtnQuery] = useState('')
  const [unarchiveBtnQuery, setUnArchiveBtnQuery] = useState('')
  const [showConfirmation, setShowConfirmation] = useState(false)
  const [step, setStep] = useState(0);
  const [disabled, setDisabled] = useState(false);

  const { quickFilters, projectPer } = useSelector(state => {
    return {
      projectPer: state.workspacePermissions.data.project,
      quickFilters: state.projects.quickFilters || {},
    };
  });
  const dispatch = useDispatch();
  // handle bulk update assignee
  const updateResources = props => {
    const postObj = { resources: [props.userId], projectIds: getSelectedProjects(), type: "array" };
    setAssigneeEl(null);
    updateBulkProject(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} project Resources updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectResources = e => {
    e.stopPropagation();
    setAssigneeEl(e.currentTarget);
  };
  const handleCloseResources = () => {
    setAssigneeEl(null);
  };
  // Status
  const updateStatus = props => {
    const postObj = { status: props.value, projectIds: getSelectedProjects() };
    setStatusEl(null);
    updateBulkProject(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} Project Status updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectStatus = e => {
    e.stopPropagation();
    setStatusEl(e.currentTarget);
  };
  const handleCloseStatus = () => {
    setStatusEl(null);
  };
  // Color
  const updateColor = color => {
    const postObj = { colorCode: color, projectIds: getSelectedProjects() };
    setColorEl(null);
    updateBulkProject(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} Project color updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectColor = e => {
    e.stopPropagation();
    setColorEl(e.currentTarget);
  };
  const handleCloseColor = () => {
    setColorEl(null);
  };
  // get Project ids use map
  const getSelectedProjects = () => {
    return selectedProjects.map(item => {
      return item.data.projectId;
    });
  };
  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };
  // export api function
  const handleExportProject = props => {
    const postObj = { projectIds: getSelectedProjects() };
    const obj = {
      type: 'projects',
      apiType: 'post',
      data: postObj,
      fileName: 'projects.xlsx',
      apiEndpoint: 'api/export/bulkproject',
    }
    addPendingHandler(obj, dispatch);
    
    // exportBulkProject(postObj, dispatch,
    //   res => {
    //     fileDownload(res.data, "projects.xlsx");
    //     showSnackBar(`${selectedProjects.length} project exported successfully`, "success");
    //   },
    //   (err) => {
    //     if (err.response.status == 405) {
    //       showSnackBar("You don't have sufficient rights to export projects", "error");
    //     } else {
    //       showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //     }
    //   });
  };
  const closeConfirmationPopup = () => {
    setShowConfirmation(false)
    setStep(0)
  }


  // delete api function
  const handleDeleteProject = props => {
    const postObj = { projectIds: getSelectedProjects() };
    setDeleteBtnQuery('progress');
    deleteBulkProject(
      postObj,
      dispatch,
      res => {
        // Success
        setDeleteBtnQuery('');
        setInputType('');
        setShowConfirmation(false);
        clearSelection();
        showSnackBar(`${res.length} project deleted successfully`, "success");
      },
      () => {
        // failure
        setDeleteBtnQuery('');
        setInputType('');
        setShowConfirmation(false);
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  // archive project api
  const handleArchiveProject = props => {
    const postObj = { projectIds: getSelectedProjects() };
    setArchiveBtnQuery('progress');
    archiveBulkProject(
      postObj,
      dispatch,
      res => {
        //Success 
        setArchiveBtnQuery('');
        setShowConfirmation(false);
        clearSelection();
        showSnackBar(`${res.length} project archived successfully`, "success");
      },
      () => {
        // Failure
        setArchiveBtnQuery('');
        setShowConfirmation(false);
        clearSelection();
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  // unArchive project api
  const handleUnArchiveProject = props => {
    const postObj = { projectIds: getSelectedProjects() };
    setUnArchiveBtnQuery('progress');
    unArchiveBulkProject(
      postObj,
      dispatch,
      res => {
        //Success
        setUnArchiveBtnQuery('');
        setShowConfirmation(false);
        clearSelection();
        showSnackBar(`${res.length} project unarchived successfully`, "success");
      },
      () => {
        // Failure
        setUnArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleTypeProjectInput = (event) => {
    let value = event.target.value;
    value && value.trim().toLowerCase() === "delete all projects here"
      ? setDisabled(false)
      : setDisabled(true)
  };
  const handleDeleteConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Delete");
  };
  const handleDeleteProjectStep = (value) => {
    setStep(value)
  };
  const handleArchiveConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Archive");
  };
  const handleUnArchiveConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Unarchive");
  };


  const projectStatusData = statusData(theme, classes, intl);
  const isArchived = quickFilters["Archived"] ? true : false;
  return (
    <>
      <div className={classes.mainTaskCmp}>
        <ul className={classes.bulkActionBtnsList}>
          <li className={classes.selectedProjects}>
            <span>{selectedProjects && selectedProjects.length}</span>Project selected
          </li>
          {!isArchived && (
            <>
              {/* Manager */}
              <li className={classes.taskListItems}>
                <AssigneeDropdown
                  updateAction={updateResources}
                  obj={{}}
                  showSelected={false}
                  singleSelect={true}
                  handleCloseAssignee={handleCloseResources}
                  customBtnRender={
                    <div className={classes.renderTask} onClick={handleSelectResources}>
                      <span className={classes.taskCmpIcon}>
                        <IconPeopleOutLine />
                      </span>{" "}
                      Resources
                    </div>
                  }
                  anchorEl={assigneeEl}
                />
              </li>
              {/* status */}
              <CanAccessFeature group='project' feature='status'>
                <li className={classes.taskListItems}>
                  <StatusDropdown
                    onSelect={updateStatus}
                    obj={{}}
                    singleSelect={true}
                    options={projectStatusData}
                    handleCloseCallback={handleCloseStatus}
                    customStatusBtnRender={
                      <div onClick={handleSelectStatus} className={classes.renderTask}>
                        <span className={classes.taskCmpIcon}>
                          <RoundIcon />
                        </span>
                        Status
                      </div>
                    }
                    dropdownProps={{
                      placement: "top-start",
                    }}
                    anchorEl={statusEl}
                  />
                </li>
              </CanAccessFeature>
              {/* color */}
              <li className={classes.taskListItems}>
                <ColorPickerDropdown
                  onSelect={updateColor}
                  obj={{}}
                  singleSelect={true}
                  handleCloseColor={handleCloseColor}
                  customColorBtnRender={
                    <div onClick={handleSelectColor} className={classes.renderTask}>
                      <span className={classes.taskCmpIcon}>
                        <IconColorOutLine />
                      </span>
                      Color
                    </div>
                  }
                  anchorEl={colorEl}
                />
              </li>
              {/* export task */}
              <li className={classes.taskListItems} onClick={handleExportProject}>
                <span className={classes.taskCmpIcon}>
                  <IconExportOutLine />
                </span>
                Export
              </li>
              {/* archive projects */}
              <li className={classes.taskListItems} onClick={handleArchiveConfirmation}>
                <span className={classes.taskCmpIcon}>
                  <IconArchiveOutLine />
                </span>
                Archive
              </li>
            </>
          )}
          {isArchived && (
            <li className={classes.taskListItems} onClick={handleUnArchiveConfirmation}>
              {/*   <li className={classes.taskListItems} onClick={handleUnArchiveProject }> */}
              <span className={classes.taskCmpIcon}>
                <IconArchiveOutLine />
              </span>
              UnArchive
            </li>
          )}
          <li className={classes.taskDeleteIcon} onClick={handleDeleteConfirmation}>
            <span className={classes.taskCmpIcon}>
              <IconDeleteOutLine />
            </span>
            Delete
          </li>
          <li className={classes.taskCrossIcon} onClick={clearSelection}>
            <span className={classes.iconClose}>
              <IconCloseOutLine />
            </span>
          </li>
        </ul>
      </div>


      {inputType === "Archive" /* Confirmation modal for Archive the bulk projects  */ ? (
        <>
          <ActionConfirmation
            open={showConfirmation}
            closeAction={closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.projects.label2"
                defaultMessage="Archive Projects"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.projects.message"
                defaultMessage={`Are you sure you want to archive these ${selectedProjects.length} projects?`}
                values={{ l: selectedProjects.length }}
              />
            }
            successAction={handleArchiveProject}
            btnQuery={archiveBtnQuery}
          />
        </>
      ) : inputType === "Unarchive" /* Confirmation modal for Unarchive the bulk projects  */ ? (
        <>
          <ActionConfirmation
            open={showConfirmation}
            closeAction={closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.projects.label2"
                defaultMessage="Unarchive projects"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            // msgText={`Are you sure you want to unarchive these ${selectedProjects.length} projects?`}
            msgText={
              <FormattedMessage
                id="common.un-archived.projects.message"
                defaultMessage={`Are you sure you want to unarchive these ${selectedProjects.length} projects?`}
                values={{ l: selectedProjects.length }}
              />
            }
            successAction={handleUnArchiveProject}
            btnQuery={unarchiveBtnQuery}
          />
        </>
      ) : inputType === "Delete" /* Confirmation modal for deleting the bulk projects  */ ? (
        <DeleteConfirmDialog
          open={showConfirmation}
          closeAction={closeConfirmationPopup}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            step == 0 ? (
              <FormattedMessage
                id="common.action.delete.anyway.label"
                defaultMessage="Delete Anyway"
              />
            ) : (
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            )
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          disabled={step === 0 ? false : disabled}
          successAction={
            step === 0
              ? () => { handleDeleteProjectStep(1); }
              // : () => handleDelete(event)
              : () => handleDeleteProject()
          }
          btnQuery={deleteBtnQuery}>
          {showConfirmation ? (
            <DeleteBulkProjectDialogContent
              step={step}
              handleTypeProjectInput={handleTypeProjectInput}
            />
          ) : null}
        </DeleteConfirmDialog>
      ) : null}

    </>
  );
};
export default compose(
  withSnackbar,
  injectIntl,
  withStyles(BulkActionsStyle, { withTheme: true })
)(Projectcmp);
