const BulkActionsStyle = (theme) => ({
    mainTaskCmp: {
        background: '#434E57',
        position: 'fixed',
        zIndex: 111,
        bottom: 80,
        left: ' 36%',
        borderRadius: "8px 8px 8px 8px",
        height: "60px",

        "& $bulkActionBtnsList": {
            display: "flex",
            alignItems: " center",
            padding: 0,
        },
    },
    bulkActionBtnsList: {},
    taskListItems: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        color: theme.palette.common.white,
        fontSize: "13px !important",
        fontFamily: theme.typography.fontFamilyLato,
        padding: "10px",
        letterSpacing: " 0.4px",
        height: "60px",
        minWidth: "60px",

        "& taskCmpIcon": {
            height: "23px",
            width: 0
        },

        "&:hover": {
            background: "#4F5B66",
            cursor: "pointer",
            transition: "0.3s",
        },
    },
    taskDeleteIcon: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        color: theme.palette.common.white,
        fontSize: "13px !important",
        fontFamily: theme.typography.fontFamilyLato,
        padding: "10px",
        letterSpacing: " 0.4px",
        height: "60px",
        "&:hover": {
            background: "#F25454 !important",
            cursor: "pointer !important",
            // transition: "0.3s",
        },
        "& span": {
            height: "23px",
        },
    },
    selectedProjects: {
        display: "flex",
        flexDirection: "column",
        background: "#0090FF !important",
        borderRadius: "8px 0px 0px 8px",
        width: " 130px",
        height: "60px",
        color: theme.palette.common.white,
        fontSize: "14px !important",
        fontFamily: theme.typography.fontFamilyLato,
        fontWeight: 400,
        marginRight: "4px",
        padding: "10px 10px 10px 12px !important",
        "& span": {
            color: theme.palette.common.white,
            fontSize: "18px !important",
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: 400,
        },

        "&:hover": {
            cursor: "pointer",
        },
    },
    taskCrossIcon: {
        background: "#4F5B66 !important",
        display: "flex",
        alignItems: " center",
        justifyContent: "center",
        width: "48px",
        height: "60px",
        borderRadius: " 0px 8px 8px 0px",
        cursor: 'pointer',
        "& span": {
            display: "flex",
            alignItems: "center",
        },
    },
    taskCmpIcon: {
        height: "25px",

        "& svg": {
            fontSize: "16px !important"
        }
    },
    iconClose: {
        "& svg": {
            fontSize: "11px !important"
        }
    },
    renderTask: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },

})

export default BulkActionsStyle;