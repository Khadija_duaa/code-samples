export function canView(permissions, key){
    return project.isOwner ? true : permissions[key]
}
export function canDo(permissions, key){
 return permissions[key]
}
export function canAdd(project, permissions, key){
 return project.isOwner ? true : permissions[key].add

}
export function canEdit(project, permissions, key){
 return project.isOwner ? true : permissions[key].edit

}
export function canDelete(project, permissions, key){
 return project.isOwner ? true : permissions[key].delete

}