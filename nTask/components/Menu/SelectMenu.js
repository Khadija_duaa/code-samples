import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import combineStyles from "../../utils/mergeStyles";
import Select from "@material-ui/core/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import selectStyles from "../../assets/jss/components/select";

class SelectMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
      const {classes, theme, label, error, style, children, ...rest } = this.props;

      const MenuProps = {
        PaperProps: {
          style: {
            overflow: "visible",
            transform: "translateY(40px)",
            background:  theme.palette.common.white,
            borderRadius: 4
          }
        },
        anchorEl: this.anchorEl
      };
    return (
      <FormControl className={classes.selectFormControl} style={{ marginTop: !label ? 0 : 22, marginBottom: error == false ? 22 : 10, ...style }}>
        <InputLabel
          shrink={true}
          classes={{ root: classes.defaultInputLabel }}
          htmlFor="select-multiple-checkbox"
        >
          {label}
        </InputLabel>
        <Select
        {...rest}
          renderValue={selected => selected.join(", ")}
          input={
            <OutlinedInput
            labelWidth={150}
              notched={false}
              inputRef={node => {
                this.anchorEl = node;
              }}
              classes={{
                root: classes.outlinedSelectCnt,
                notchedOutline: classes.outlinedSelect
              }}
              id="select-multiple-checkbox"
            />
          }
          MenuProps={MenuProps}
          classes={{
           // root: classes.selectCnt,
            select: classes.singleSelectCmp,
            selectMenu: classes.selectMenu,
            icon: classes.selectIcon
          }}
        >
         {children}
        </Select>
      </FormControl>
    );
  }
}

export default withStyles(selectStyles, {withTheme: true})(SelectMenu);
