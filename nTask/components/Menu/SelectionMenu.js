import React, { Component } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import menuStyles from "../../assets/jss/components/menu";
import buttonStyles from "../../assets/jss/components/button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../utils/mergeStyles";
import isEmpty from "lodash/isEmpty";

class SelectionMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }
  render() {
    const {
      classes,
      closeAction,
      disablePortal,
      placement,
      open,
      style,
      isAddColSelectionClicked,
      keepMounted = false,
    } = this.props;
    return (
      <Popper
        open={open}
        anchorEl={this.props.anchorRef}
        placement={isEmpty(placement) ? "top-start" : placement}
        className={classes.plainPopper}
        style={style}
        keepMounted={keepMounted}
        modifiers={{
          offset: {
            enabled: true,
            offset: "0 0",
          },
          arrow: {
            enabled: false,
          },
        }}
        transition
        // onClick={e => e.stopPropagation()}
        disablePortal={disablePortal}>
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            id="menu-list-grow"
            style={{
              transformOrigin: placement === "bottom" ? "top" : "left-end",
            }}>
            <Paper
              classes={{
                root: classes.paperRoot,
              }}>
              {isAddColSelectionClicked ?
                <ClickAwayListener mouseEvent="onMouseUp"
                  touchEvent="onTouchEnd" onClickAway={closeAction}>
                  <div>
                    {this.props.list}
                  </div>
                </ClickAwayListener> :
                this.props.list
              }
            </Paper>
          </Grow>
        )}
      </Popper>
    );
  }
}
SelectionMenu.defaultProps = {
  classes: {},
  closeAction: () => { },
  disablePortal: false,
  placement: "top-start",
  open: false,
  style: {},
};

export default withStyles(combineStyles(menuStyles, buttonStyles), {
  withTheme: true,
})(SelectionMenu);
