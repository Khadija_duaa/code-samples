import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import customMenuStyles from "../../assets/jss/components/customMenu";
import classNames from "classnames"
import Fade from '@material-ui/core/Fade';

class DropdownMenu extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { classes, theme, children, closeAction, disablePortal, size,isClickAway, ...rest } = this.props;
    const popperClasses = classNames([
      classes[`popper${size}`], classes.popper
    ])

    return (
      <Popper
        {...rest}
        className={popperClasses}
        modifiers={{
          flip: {
            enabled: true,
          },
          preventOverflow: {
            enabled: true,
            boundariesElement: 'scrollParent',
          },
        }}
        transition
        onClick={e => e.stopPropagation()}
        disablePortal={disablePortal ? disablePortal : false}
      >
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={350}>
            <Paper
              classes={{
                root: classes.paperRoot
              }}
            >
              {isClickAway ? children
                : <ClickAwayListener
                  mouseEvent="onMouseDown"
                  touchEvent="onTouchStart"
                  onClickAway={closeAction}>
                  <div>
                    {children}
                  </div>
                </ClickAwayListener>}
            </Paper>
          </Fade>
        )}
      </Popper>
    );
  }
}

export default withStyles(customMenuStyles, {
  withTheme: true
})(DropdownMenu);
