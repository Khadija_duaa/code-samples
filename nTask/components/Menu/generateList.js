import React, { Component, useState, useEffect, useRef } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { withStyles } from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import DoneIcon from "@material-ui/icons/Done";
import dropdownStyles from "./style";
import SearchInput from "react-search-input";
import { Scrollbars } from "react-custom-scrollbars";

const generateList = props => {
  const {
    classes,
    theme,
    data,
    handleItemClick,
    iconType,
    renderIcon,
    checked,
    headingText,
    searchAction,
    searchPlaceholder,
    handleResize,
    menuWidth,
    menuHeight,
  } = props;
  const [dropDownWidth, setDropDownWidth] = useState(300)
  const [dropDownHeight, setDropDownHeight] = useState(320)
  const sidebarRef = useRef(null);
  const [isResizing, setIsResizing] = useState(false);
  const startResizing = React.useCallback((mouseDownEvent) => {
    setIsResizing(true);
  }, []);

  const stopResizing = React.useCallback(() => {
    setIsResizing(false);
  }, []);

  const resize = React.useCallback(
    (mouseMoveEvent) => {
      if (isResizing && handleResize) {
        handleResize(
          mouseMoveEvent.clientX -
          sidebarRef.current.getBoundingClientRect().left + +5,
          mouseMoveEvent.clientY -
          sidebarRef.current.getBoundingClientRect().top + +5
        )
      }
    },
    [isResizing]
  );

  useEffect(() => {
    window.addEventListener("mousemove", resize);
    window.addEventListener("mouseup", stopResizing);
    return () => {
      window.removeEventListener("mousemove", resize);
      window.removeEventListener("mouseup", stopResizing);
    };
  }, [resize, stopResizing]);
  return (
    <>
      <div
        ref={sidebarRef}
        className={classes.resizeableMenu}
        style={{ height: menuHeight, width: menuWidth }}>
        <Scrollbars autoHide style={{ height: '100%', width: '100%' }}>
          <List>
            <ListItem disableRipple={true} classes={{ root: classes.headingItem }}>
              <ListItemText
                primary={headingText}
                classes={{ primary: classes.headingText }}
              />
            </ListItem>

            <ListItem disableRipple={true} classes={{ root: classes.searchInputItem }}>
              <SearchInput className="HtmlInput" autoFocus={true} onChange={searchAction} placeholder={searchPlaceholder ? searchPlaceholder : "Search"} />
            </ListItem>

            {data.map((value, i, arr) => {
              return (
                <ListItem
                  key={i}
                  button
                  disableRipple={true}
                  onClick={(e) => handleItemClick(e, value.value.name, value.id, value.value.projectId)}
                  classes={{
                    root: classes[`menuItem${iconType}`],
                    selected: classes.itemSelected
                  }}
                >
                  {iconType ? renderIcon(value.value.avatar, classes, arr) : null}
                  <ListItemText
                    primary={value.value.name}
                    classes={{
                      root: classes.itemTextRoot,
                      primary: classes.itemText
                    }}
                  />
                  {/*<ListItemSecondaryAction>*/}
                  {/*  <Checkbox*/}
                  {/*    checkedIcon={*/}
                  {/*      <DoneIcon*/}
                  {/*        htmlColor={theme.palette.primary.light}*/}
                  {/*        fontSize="default"*/}
                  {/*      />*/}
                  {/*    }*/}
                  {/*    checked={checked.indexOf(value.value.name) !== -1}*/}
                  {/*    icon={false}*/}
                  {/*  />*/}
                  {/*</ListItemSecondaryAction>*/}
                </ListItem>
              )
            })}

          </List>
        </Scrollbars>
        <div className={classes.resizeMenu} onMouseDown={startResizing}></div>
      </div>
    </>
  );
};

export default withStyles(dropdownStyles, { withTheme: true })(generateList);
