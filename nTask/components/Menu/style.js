const dropdownStyles = theme => ({
  headingItem: {
    padding: "0 20px",
    outline: "none",
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "unset"
    }
  },
  headingText: {
    // Dropdown Heading Item Text
    fontSize: "12px !important",
    marginTop: 5,
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    color: theme.palette.secondary.light
  },
  item: {

  },
  searchMenuList: {
    outline: "none"
  },
  menuItemavatar: {
    padding: "8px 16px 8px 16px",
    marginBottom: 2
  },
  menuItem: {
    padding: "8px 16px 8px 16px !important",
    marginBottom: 2,
  },
  itemTextRootFollowUp:{
    padding: "0 16px",
    margin:0,
  // },
  // itemText: {
    // width: 134,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  },
  //Item text
  itemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    width: '100%',
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",

    textOverflow: "ellipsis"
  },
  itemTextRoot: {
    padding: '0 !important'
  },
  itemSelected: { // Selected Item
    background: `${theme.palette.common.white} !important`,
    "&:hover": {
      background: `${theme.palette.background.items}`
    },
    "&:focus": {
      background: `${theme.palette.background.items} !important`
    }
  },
  //Icon Styles
  statusIcon: {
    // Task Status Icon
    fontSize: "11px !important"
  },
  piriorityIcon: {
    // Task Piriority Icon
    fontSize: "14px !important"
  },
  severityIcon: { // Isue Severity Icon
    fontSize: "14px !important"
  },
  TotalAssignee: { // Assignee Dropdown
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main
  },
  menuListAvatar: { // menu List avatar
    width: 24,
    height: 24
  },
  searchInputItem: { // list item with search input
    padding: "11px 14px"
  },
  selectedValue: { //Adds left border to selected list item
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main,
    }
  },
  bugIcon: {
    fontSize: "16px !important"
  },
  ratingStarIcon: {
    fontSize: "16px !important"
  },
  ImprovmentIcon: {
    fontSize: "16px !important"
  },
  resizeableMenu: {
    minWidth: 300,
    minHeight: 320,
    position: 'relative',
    "&:hover": {
      "& $resizeMenu": {
        opacity: '1 !important',
      }
    }
  },
  resizeMenu: {
    width: '12px',
    height: '12px',
    position: 'absolute',
    right: 0,
    bottom: 0,
    background: 'transparent',
    cursor: 'nwse-resize',
    opacity: 0,
    overflow: 'hidden',
    transition: '0.4s ease all',
    "&:hover": {
      background: '#f0f0f0',
    },
    "&:before": {
      content: "''",
      position: 'absolute',
      top: '7px',
      left: '-1px',
      background: '#7c7c7c',
      width: '120%',
      height: '1px',
      transform: 'rotate(-45deg)',
    },
    "&:after": {
      content: "''",
      position: 'absolute',
      top: '11px',
      left: '-1px',
      background: '#7c7c7c',
      width: '120%',
      height: '1px',
      transform: 'rotate(-45deg)',
    }
  }
});

export default dropdownStyles;
