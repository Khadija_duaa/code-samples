import React from "react";
import { compose } from "redux";

import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { withStyles } from "@material-ui/core/styles";
import dropdownStyles from "./style";
import SearchInput from "react-search-input";
import { Scrollbars } from "react-custom-scrollbars";
import BugIcon from "../Icons/BugIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import StarIcon from "../Icons/StarIcon";
import ImprovementIcon from "../Icons/ImprovementIcon";
import { FormattedMessage, injectIntl } from "react-intl";

const GenerateMenu = (props) => {
  const {
    classes,
    theme,
    data,
    listType,
    handleToggle,
    iconType,
    idList,
    renderIcon,
    checked,
    headingText,
    searchAction,
    isType,
  } = props;

  const getTranslatedId = (value) => {
    switch (value) {
      case "Select Task":
        value = "common.bulk-action.selectTask";
        break;
      case "Select Tasks":
        value = "common.select-tasks.label";
        break;
      case "Add Attendee":
        value = "common.bulk-action.attendee";
        break;
      case "Add Participants":
        value = "common.bulk-action.addParticipant";
        break;
      case "Add Assignee":
        value = "common.bulk-action.addAssignee";
        break;
      case "Select Likelihood":
        value = "risk.common.likelihood.placeholder";
        break;
      case "Add Owner":
        value = "common.bulk-action.addOwner";
        break;
      default:
        return value;
    }
    return value;
  };

  return (
    <div
      onClick={(event) => {
        event.stopPropagation();
      }}
    >
      <Scrollbars
        autoHide
        autoHeightMax={300}
        autoHeight={listType == "search" ? false : true}
        style={{ height: listType == "search" ? 320 : null }}
      >
        <List>
          <ListItem
            disableRipple={true}
            classes={{ root: classes.headingItem }}
          >
            <ListItemText
              primary={
                <FormattedMessage
                  id={getTranslatedId(headingText)}
                  defaultMessage={headingText}
                />
              }
              classes={{ 
                root: classes.itemTextRootFollowUp,
                primary: classes.headingText }}
            />
          </ListItem>
          {listType == "search" ? (
            <ListItem
              disableRipple={true}
              classes={{ root: classes.searchInputItem }}
            >
              <SearchInput
                className="HtmlInput"
                onChange={searchAction}
                placeholder={props.intl.formatMessage({
                  id: "common.search.label",
                  defaultMessage: "Search",
                })}
              />
            </ListItem>
          ) : null}
          {data.slice(0, 20).map((value, i, arr) => {
            return listType == "search" ? (
              <ListItem
                key={value.value.id}
                button
                disableRipple={true}
                onClick={(event) =>
                  handleToggle(event, value.value.name, value.id)
                }
                className={`${classes[`menuItem${iconType}`]} ${
                  (idList && idList.length
                  ? idList.indexOf(value.id) !== -1
                  : checked.indexOf(value.value.name) !== -1)
                    ? classes.selectedValue
                    : ""
                }`}
                classes={{
                  root: classes[`menuItem${iconType}`],
                  selected: classes.itemSelected,
                }}
              >
                {iconType ? renderIcon(value.value, classes, arr) : null}
                <ListItemText
                  primary={
                    <>
                      {value.value.name}
                      <br />
                      {value.isActiveMember === null ? (
                        <Typography variant="caption">Invite sent</Typography>
                      ) : null}
                    </>
                  }
                  classes={{
                    root: classes.itemTextRootFollowUp,
                    primary: classes.itemText,
                  }}
                />
              </ListItem>
            ) : listType == "iconList" ? (
              <ListItem
                key={value.name}
                button
                disableRipple={true}
                onClick={(event) => handleToggle(event, value.name, value.id)}
                className={`${classes[`menuItem${iconType}`]} ${
                  (idList && idList.length
                  ? idList.indexOf(value.id) !== -1
                  : checked.indexOf(value.name) !== -1)
                    ? classes.selectedValue
                    : ""
                }`}
                classes={{
                  selected: classes.itemSelected,
                }}
              >
                {renderIcon(value.color, classes)}

                <ListItemText
                  primary={value.label ? value.label : value.name}
                  classes={{
                    root: classes.itemTextRootFollowUp,
                    primary: classes.itemText,
                  }}
                />
              </ListItem>
            ) : listType == "simpleList" ? (
              <ListItem
                key={value.value.name}
                button
                disableRipple={true}
                onClick={(event) =>
                  handleToggle(event, value.value.name, value.id)
                }
                className={`${classes[`menuItem${iconType}`]} ${
                  checked.indexOf(value.value.name) !== -1
                    ? classes.selectedValue
                    : null
                }`}
                classes={{
                  selected: classes.itemSelected,
                }}
              >
                {isType ? (
                  i == 0 ? (
                    <SvgIcon
                      viewBox="0 0 456.828 456.828"
                      htmlColor={theme.palette.error.light}
                      className={classes.bugIcon}
                    >
                      <BugIcon />
                    </SvgIcon>
                  ) : i == 2 ? (
                    <SvgIcon
                      viewBox="0 0 19.481 19.481"
                      classes={{ root: classes.ratingStarIcon }}
                      htmlColor={theme.palette.background.star}
                    >
                      <StarIcon />
                    </SvgIcon>
                  ) : (
                    <SvgIcon
                      viewBox="0 0 11 10.188"
                      classes={{ root: classes.ImprovmentIcon }}
                      htmlColor={theme.palette.primary.light}
                    >
                      <ImprovementIcon />
                    </SvgIcon>
                  )
                ) : null}
                <ListItemText
                  primary={
                    <>
                      {props.isLikelihood
                        ? value.value.name + " %"
                        : value.label ? value.label : value.value.name}
                      <br />
                      {!props.isLikelihood && value.isActiveMember === null ? (
                        <Typography variant="caption">Invite sent</Typography>
                      ) : null}
                    </>
                  }
                  classes={{
                    root: classes.itemTextRootFollowUp,
                    primary: classes.itemText,
                  }}
                />
              </ListItem>
            ) : null;
          })}
        </List>
      </Scrollbars>
    </div>
  );
};

export default compose(
  injectIntl,
  withStyles(dropdownStyles, { withTheme: true })
)(GenerateMenu);
