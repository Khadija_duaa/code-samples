import React, { Component } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import customMenuStyles from "../../assets/jss/components/customMenu";

class CustomMenu extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { classes, theme, children, closeAction, ...rest } = this.props;

    return (
      <React.Fragment>
          <Popper
            {...rest}
            className={classes.popper}
            modifiers={{
              offset: {
                enabled: true,
                offset: "0 0"
              },
              arrow: {
                enabled: false
              }
            }}
            transition
            onClick={e => e.stopPropagation()}
            disablePortal
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                id="menu-list-grow"
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "left bottom"
                }}
              >
                <Paper
                  classes={{
                    root: classes.paperRoot
                  }}
                >
                  {children}
                </Paper>
              </Grow>
            )}
          </Popper>
      </React.Fragment>
    );
  }
}


export default withStyles(customMenuStyles, {
  withTheme: true
})(CustomMenu);
