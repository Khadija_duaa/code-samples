import React, { Component, Fragment } from "react";
import RoundIcon from "@material-ui/icons/Brightness1";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
import { withStyles } from "@material-ui/core/styles";
import readyDropdownStyles from "./style";
import combineStyles from "../../../utils/mergeStyles";
import Select from "@material-ui/core/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import selectStyles from "../../../assets/jss/components/select";
import dropdownStyles from "../style";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import FormHelperText from "@material-ui/core/FormHelperText";
import CustomTooltip from "../../Tooltip/Tooltip";
import { getEditPermissionsWithArchieve } from "../../../Views/Issue/permissions";
import { getRisksEditPermissionsWithArchieve } from "../../../Views/Risk/permissions";
import FlagIcon from "../../Icons/FlagIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import BugIcon from "../../Icons/BugIcon";
import StarIcon from "../../Icons/StarIcon";
import ImprovementIcon from "../../Icons/ImprovementIcon";
import newProjectStyles from "../../../Views/Project/ProjectDetails/styles";

class ProjectSelectIconMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      ddData: [],
    };
  }

  handleChange = (event) => {
    const { ddData } = this.state;
    if (event.target.value && ddData) {
      const obj = ddData.find((d) => d.key === event.target.value) || {};
      this.setState({ name: obj.name });
      this.props.isUpdated(obj);
    }
  };

  componentDidUpdate(prevProps, prevState) {
    const { view = "", theme, billingMethod = "" } = this.props;
    if (
      JSON.stringify(prevProps.billingMethod) !==
      JSON.stringify(this.props.billingMethod)
    ) {
      const ddData = this.getDropDownData(view, theme.palette, billingMethod);
      this.setState({ name: "", ddData: ddData });
    }
  }

  componentDidMount() {
    const {
      selectedValue = "",
      view = "",
      theme,
      billingMethod = "",
    } = this.props;
    const ddData = this.getDropDownData(view, theme.palette, billingMethod);
    this.setState({ name: selectedValue, ddData: ddData });
  }

  getDropDownData = (view = "", color = {}, billingMethod = "") => {
    switch (view) {
      case "projectStatus":
        return [
          {
            name: "Not Started",
            color: color.projectStatus.NotStarted,
            key: "notStarted",
          },
          {
            name: "In Progress",
            color: color.projectStatus.InProgress,
            key: "inProgress",
          },
          {
            name: "On Hold",
            color: color.projectStatus.InReview,
            key: "onHold",
          },
          {
            name: "Completed",
            color: color.projectStatus.Completed,
            key: "completed",
          },
          {
            name: "Cancelled",
            color: color.projectStatus.Cancelled,
            key: "cancelled",
          },
        ];
        break;
      case "billingMethod":
        return [
          { name: "Fixed Fee", key: "fixed" },
          { name: "Fixed Fee per Task", key: "fixedPerTask" },
          { name: "Hourly Rate by Task", key: "hourlyByTask" },
          { name: "Hourly rate by Resource", key: "hourlyByResource" },
        ];
        break;
      case "currency":
        return [
          { name: "USD - US Dollar", key: "USD" },
          { name: "EUR - Euro", key: "EUR" },
          { name: "GBK - UK Pound Sterling", key: "GBK" },
          { name: "CAD - Canadian Dollar", key: "CAD" },
        ];
        break;
      case "fee":
        if (billingMethod == "fixedPerTask") {
          return [
            { name: `Same Fee for Every Task`, key: "sameEveryTask" },
            { name: "Different Fee per Task", key: "differentPerTask" },
          ];
        } else if (billingMethod == "hourlyByTask") {
          return [
            {
              name: `Same Hourly Rate for Every Task`,
              key: "sameHourlyEveryTask",
            },
            {
              name: "Different Hourly Rate per Task",
              key: "differentHourlyPerTask",
            },
          ];
        } else if (billingMethod == "hourlyByResource") {
          return [
            {
              name: `Same Hourly Rate for Every Resource`,
              key: "sameHourlyEveryResource",
            },
            {
              name: "Different Hourly Rate per Resource",
              key: "differentHourlyPerResource",
            },
          ];
        } else return [];
        break;

      case "permission":
        return [
          {
            name: "Project Manager",
            key: "projectManager",
          },
          {
            name: "Contributor",
            key: "contributor",
          },
          {
            name: "Viewer",
            key: "viewer",
          },
        ];

      default:
        return [];
        break;
    }
  };

  render() {
    const {
      classes,
      theme,
      label,
      heading,
      iconType,
      isSingle,
      isSimpleList,
      errorState,
      errorMessage,
      error,
      style,
      helptext = "",
      disabled = false,
      view = "",
      transform = "",
      billingMethod = "",
    } = this.props;
    const { name } = this.state;
    const color = theme.palette;

    const MenuProps = {
      // transformOrigin: { vertical: -40, horizontal: 0 },
      PaperProps: {
        style: {
          overflow: "visible",
          transform: transform,
          background: theme.palette.common.white,
          borderRadius: 4,
          // width: 152,
        },
      },
      anchorReference: this.anchorEl,
    };
    const RenderIcon = (color) => {
      return (
        <RoundIcon
          htmlColor={color}
          classes={{ root: classes.projectIcon }}
        />
      );
    };

    const ddData = this.getDropDownData(view, color, billingMethod);

    return (
      <Fragment>
        <FormControl
          className={classes.selectFormControl}
          style={{
            marginTop: !label ? 0 : 22,
            marginBottom: error == false ? 22 : 10,
            ...style,
          }}
          error={errorState}
        >
          {label ? (
            <InputLabel
              shrink={true}
              classes={{ root: classes.defaultInputLabel }}
              htmlFor="selectMenu"
            >
              {label}
              {helptext ? (
                <CustomTooltip helptext={helptext} iconType="help" />
              ) : null}
            </InputLabel>
          ) : null}
          <Select
            value={name}
            label=""
            multiple={isSingle ? false : true}
            onChange={this.handleChange}
            disabled={false}
            renderValue={(selected) =>
              isSingle ? selected : selected.join(", ")
            }
            input={
              <OutlinedInput
                labelWidth={150}
                notched={false}
                inputRef={(node) => {
                  this.anchorEl = node;
                }}
                classes={{
                  root: classes.outlinedSelectCnt,
                  notchedOutline: classes.outlinedSelect,
                  focused: classes.outlineInputFocus,
                  // disabled:true
                }}
                id="selectMenu"
              />
            }
            MenuProps={MenuProps}
            classes={{
              // root: classes.selectCnt,
              select: classes.singleSelectCmp,
              icon: classes.selectIcon,
              //disabled:true
            }}
          >
            <ListItem
              disableRipple={true}
              classes={{ root: classes.headingItem }}
            >
              <ListItemText
                primary={heading}
                classes={{ primary: classes.headingText }}
              />
            </ListItem>
            {ddData.map((value, i) => (
              <ListItem
                key={value.key}
                value={value.key}
                button
                disableRipple={true}
                className={`${classes[`menuItem${iconType}`]} ${
                  name == value.name ? classes.selectedValue : ""
                }`}
                classes={{
                  root: classes[`menuItem${iconType}`],
                  selected: classes.itemSelected,
                }}
              >
                {isSimpleList ? null : RenderIcon(value.color)}

                <ListItemText
                  primary={value.name}
                  classes={{
                    primary: classes.itemText,
                  }}
                />
              </ListItem>
            ))}
          </Select>
          {errorState ? <FormHelperText>{errorMessage}</FormHelperText> : null}
        </FormControl>
      </Fragment>
    );
  }
}

export default withStyles(newProjectStyles, {
  withTheme: true,
})(ProjectSelectIconMenu);
