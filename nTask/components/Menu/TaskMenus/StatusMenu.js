import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { UpdateTask, FetchTasksInfo } from "../../../redux/actions/tasks";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./style";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../Buttons/IconButton";
import SelectionMenu from "../../Menu/SelectionMenu";
import DoneIcon from "@material-ui/icons/Done";
import RoundIcon from "@material-ui/icons/Brightness1";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import combineStyles from "../../../utils/mergeStyles";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";

class StatusDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      name: [],
      selectedStatus: false,
      isChanged: false,
      selectedColor: props.theme.palette.status.notStarted,
      checked: []
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  componentDidMount() {
    let status = ["Not Started", "In Progress", "In Review", "Completed", "Cancelled"], index = 0;
    if (this.props.viewType === "issue") {
      status = ["Open", "Closed"];
      index = this.props.status - 1;
    } else if (this.props.viewType === "risk") {
      status = ["Identified", "InReview", "Agreed", "Rejected"];
      index = this.props.status - 1;
    }
    if (this.props.status >= 0)
      this.setState({ checked: [status[index]], isChanged: true })
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    let status = ["Not Started", "In Progress", "In Review", "Completed", "Cancelled"], index = 0;
    if (nextProps.viewType === "issue") {
      status = ["Open", "Closed"];
      index = nextProps.status - 1;
    } else if (nextProps.viewType === "risk") {
      status = ["Identified", "InReview", "Agreed", "Rejected"];
      index = nextProps.status - 1;
    }
    if (nextProps.status >= 0 && prevState.isChanged) {
      return ({ checked: [status[index]], isChanged: true })
    }

    return ({ isChanged: false })
  }

  handleClose(event) {
    this.setState({ open: false });
  }
  handleClick(event, placement) {
    event.stopPropagation()
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }

  handleToggle = (event, value, color) => {
    event.stopPropagation()
    let status = ["Not Started", "In Progress", "In Review", "Completed", "Cancelled"], ischeck = -1;
    let data = this.props.MenuData;
    data.status = status.indexOf(value);
    if (this.props.viewType === "issue") {
      status = ["Open", "Closed"];
      data.status = status.indexOf(value) + 1;
    }

    ischeck = this.state.checked.indexOf(value)
    this.handleClose();
    if (ischeck < 0) {
      // if (this.props.calculateProgress)
      //   this.props.calculateProgress(data, true);
      // this.props.UpdateTask(data, (response) => {
      //   if (this.props.calculateProgress)
      //     this.props.calculateProgress(data, true);
      //   if (response.status === 200) {
      //     this.setState({
      //       isChanged: true
      //     });
      //     response = response.data.CalenderDetails;
      //     ;
      //   }
      // })
      this.props.isUpdated(data);
    }

    this.setState({
      checked: [value],
      selectedColor: color,
      isChanged: false
    });
  };

  render() {
    const { classes, theme, renderText, status } = this.props;
    const { open, placement, isChanged } = this.state;
    const statusColor = theme.palette.status;
    let ddData = [
      { name: "Not Started", color: statusColor.notStarted },
      { name: "In Progress", color: statusColor.inProgress },
      { name: "In Review", color: statusColor.review },
      { name: "Completed", color: statusColor.completed },
      { name: "Cancelled", color: statusColor.cancelled }
    ];
    if (this.props.viewType === "issue") {
      ddData = [
        { name: "Open", color: statusColor.notStarted },
        { name: "Closed", color: statusColor.completed }
      ]
    }
    if (this.props.viewType === "risk") {
      ddData = [
        { name: "Identified", color: statusColor.notStarted },
        { name: "InReview", color: statusColor.review },
        { name: "Agreed", color: statusColor.completed },
        { name: "Rejected", color: statusColor.cancelled }
      ]
    }
    let taskStatusColor = this.state.selectedColor;
    if (this.props.status >= 0 && isChanged) {
      if (this.props.viewType === "issue") {
        taskStatusColor = ddData[(status === 1 || status === 2) ? status - 1 : 0].color;
      } else if (this.props.viewType === "risk") {
        taskStatusColor = ddData[(status >= 1) ? status - 1 : 0].color;
      } else {
        taskStatusColor = ddData[status].color;
      }
    }


    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomIconButton
            btnType="filledWhite"
            style={{ marginRight: 10, ...this.props.style }}
            renderText={renderText}
            onClick={event => {
              this.handleClick(event, "bottom-start");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
          >
            <RoundIcon
              classes={{ root: classes.addAssigneeBtn }}
              htmlColor={taskStatusColor}
            />

          </CustomIconButton>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            anchorRef={this.anchorEl}
            list={
              <List>
                <ListItem
                  disableRipple={true}
                  classes={{ root: classes.menuHeadingItem }}
                >
                  <ListItemText
                    primary="Select Status"
                    classes={{ primary: classes.menuHeadingItemText }}
                  />
                </ListItem>
                {ddData.map(value => (
                  <ListItem
                    key={value.name}
                    button
                    disableRipple={true}
                    onClick={this.handleToggle(event, value.name, value.color)}
                    classes={{ selected: classes.statusMenuItemSelected }}
                  >
                    <RoundIcon
                      htmlColor={value.color}
                      classes={{ root: classes.statusIcon }}
                    />
                    <ListItemText
                      primary={value.name}
                      classes={{
                        primary: classes.statusItemText
                      }}
                    />
                    <ListItemSecondaryAction>
                      <Checkbox
                        checkedIcon={
                          <DoneIcon
                            htmlColor={theme.palette.primary.light}
                          />
                        }
                        onChange={this.handleToggle(value.name)}
                        checked={this.state.checked.indexOf(value.name) !== -1}
                        icon={false}
                      />
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}


export default compose(
  withRouter,
  withStyles(combineStyles(menuStyles, itemStyles), {
    withTheme: true
  }),
  connect(
    null,
    { UpdateTask, FetchTasksInfo, UpdateCalenderTask }
  )
)(StatusDropdown);