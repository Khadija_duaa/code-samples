import React, { Component, Fragment } from "react";
import RoundIcon from "@material-ui/icons/Brightness1";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
import { withStyles } from "@material-ui/core/styles";
import readyDropdownStyles from "./style";
import combineStyles from "../../../utils/mergeStyles";
import Select from "@material-ui/core/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import selectStyles from "../../../assets/jss/components/select";
import dropdownStyles from "../style";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
let object = {};
import FormHelperText from "@material-ui/core/FormHelperText";
import CustomTooltip from "../../Tooltip/Tooltip";
import { getEditPermissionsWithArchieve } from "../../../Views/Issue/permissions";
import { getRisksEditPermissionsWithArchieve } from "../../../Views/Risk/permissions";
import FlagIcon from "../../Icons/FlagIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import BugIcon from "../../Icons/BugIcon";
import StarIcon from "../../Icons/StarIcon";
import ImprovementIcon from "../../Icons/ImprovementIcon";
class SelectIconMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: [],
      riskArray: [],
      clear: false,
      isChanged: false,
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    if (event.target.value) {
      this.setState({ name: event.target.value, isChanged: true }, () => {
        if (this.props.menuType === "projectRate") {
          this.setState({ name: [event.target.value] }, () => {
            this.props.handleHourlyRateDD(event.target.value);
          });
        }
        if (this.props.isUpdated && this.props.keyType) {
          let data = this.props.MenuData;
          if (this.props.menuType === "likelihood") {
            data[this.props.keyType] = event.target.value.replace(" %", "");
          } else {
            if (this.props.menuType == "riskStatus") {
              data[this.props.keyType] = this.state.riskArray.find(
                x => x.label == event.target.value
              ).name;
            } else data[this.props.keyType] = event.target.value;
          }

          this.props.isUpdated(data);
        }
        if (this.props.handleDropDown) {
          this.props.handleDropDown({ priority: event.target.value });
        }
        if (this.props.handleDropDownRisk) {
          if (this.props.menuType === "likelihood") {
            this.props.handleDropDownRisk({
              likelihood: event.target.value.replace(" %", ""),
            });
          } else if (this.props.menuType === "impact")
            this.props.handleDropDownRisk({ impact: event.target.value });
        }
        if (this.props.menuType === "roles") {
          const roleType = this.props.roles.isAdmin
            ? "Admin"
            : this.props.roles.isMember
            ? "Member"
            : "Limited member";
          if (roleType !== event.target.value) {
            this.props.handleSelectedRole(this.props.roles, event.target.value);
          }
        }
        if (this.props.handleFilterChange && this.props.keyType)
          this.props.handleFilterChange(this.props.keyType, this.state.name);
        if (this.props.selectedRecursiveValue) {
          this.props.selectedRecursiveValue(this.state.name, this.props.typeDate);
        }
      }); //check
    } else {
      this.setState({ name: [] });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.menuType === "projectRate" &&
      this.props.projectRateType !== prevProps.projectRateType
    ) {
      this.setState({ name: this.props.projectRateType });
    } else {
      let name = this.props.name;
      if (name && JSON.stringify(name) !== JSON.stringify(prevProps.name)) this.setState({ name });
    }
  }
  componentDidMount() {
    if (this.props.menuType === "weeks") {
      if (this.props.currentMeeting) {
        const week = ["1st week", "2nd week", "3rd week", "4th week"];
        this.setState({
          name: this.props.currentMeeting.weeklyInterval
            ? week[this.props.currentMeeting.weeklyInterval - 1]
            : ["1st week"],
        });
      }
    }
    if (this.props.menuType === "months") {
      if (this.props.currentMeeting) {
        const month = [
          "1st",
          "2nd",
          "3rd",
          "4th",
          "5th",
          "6th",
          "7th",
          "8th",
          "9th",
          "10th",
          "11th",
          "12th",
        ];
        this.setState({
          name: this.props.currentMeeting.monthlyInterval
            ? month[this.props.currentMeeting.monthlyInterval - 1]
            : [],
        });
      }
    }
    if (this.props.menuType === "days") {
      if (this.props.currentMeeting) {
        const monthlyDate = [
          "1st",
          "2nd",
          "3rd",
          "4th",
          "5th",
          "6th",
          "7th",
          "8th",
          "9th",
          "10th",
          "11th",
          "12th",
          "13th",
          "14th",
          "15th",
          "16th",
          "17th",
          "18th",
          "19th",
          "20th",
          "21st",
          "22nd",
          "23rd",
          "24th",
          "25th",
          "26th",
          "27th",
          "28th",
          "29th",
          "30th",
          "31st",
        ];
        this.setState({
          name: this.props.currentMeeting.dayOfMonthlyInterval
            ? monthlyDate[this.props.currentMeeting.dayOfMonthlyInterval - 1]
            : [],
        });
      }
    }
    if (this.props.menuType === "projectRate") {
      this.setState({ name: this.props.projectRateType });
    }

    if (this.props.keyType && this.props.MenuData) {
      this.setState({ name: [this.props.MenuData[this.props.keyType]] });
    }
    if (this.props.menuType === "roles") {
      this.setState({
        name:
          this.props.menuType === "roles"
            ? this.props.roles.isAdmin
              ? "Admin"
              : this.props.roles.isMember
              ? "Member"
              : "Limited member"
            : "Limited member",
      });
    }

    let name = this.props.name;
    if (name) this.setState({ name });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.reset) {
      nextProps.closeSelectedRolePopup(true);
      return {
        name: prevState.isChanged
          ? nextProps.menuType === "roles"
            ? nextProps.roles.isAdmin
              ? "Admin"
              : nextProps.roles.isMember
              ? "Member"
              : "Limited member"
            : "Limited member"
          : [],
      };
    }
    if (nextProps.isClear) {
      object = {
        name: [],
        clear: true,
      };
    } else {
      const color = nextProps.theme.palette;
      let riskArray = [];
      let labelName = [];
      if (nextProps.keyType && nextProps.MenuData && nextProps.menuType == "riskStatus") {
        riskArray = [
          {
            label: nextProps.intl
              ? nextProps.intl.formatMessage({
                  id: "risk.common.status.dropdown.identified",
                  defaultMessage: "Identified",
                })
              : "Identified",
            name: "Identified",
            color: color.riskStatus.Identified,
          },
          {
            label: nextProps.intl
              ? nextProps.intl.formatMessage({
                  id: "risk.common.status.dropdown.in-review",
                  defaultMessage: "In Review",
                })
              : "In Review",
            name: "In Review",
            color: color.riskStatus.InReview,
          },
          {
            label: nextProps.intl
              ? nextProps.intl.formatMessage({
                  id: "risk.common.status.dropdown.agreed",
                  defaultMessage: "Agreed",
                })
              : "Agreed",
            name: "Agreed",
            color: color.riskStatus.Agreed,
          },
          {
            label: nextProps.intl
              ? nextProps.intl.formatMessage({
                  id: "risk.common.status.dropdown.rejected",
                  defaultMessage: "Rejected",
                })
              : "Rejected",
            name: "Rejected",
            color: color.riskStatus.Rejected,
          },
        ];
        let array = [nextProps.MenuData[nextProps.keyType]];
        let item = riskArray.find(x => x.name.toLowerCase() == array[0].toLowerCase());
        labelName = item.label.split();
        riskArray = riskArray;
      }
      object = {
        clear: false,
        name: prevState.isChanged
          ? prevState.name
          : nextProps.keyType && nextProps.MenuData
          ? nextProps.menuType === "likelihood"
            ? [nextProps.MenuData[nextProps.keyType]] + " %"
            : nextProps.menuType == "riskStatus"
            ? labelName
            : [nextProps.MenuData[nextProps.keyType]]
          : nextProps.roles
          ? nextProps.menuType === "roles"
            ? nextProps.roles.isAdmin
              ? "Admin"
              : nextProps.roles.isMember
              ? "Member"
              : "Limited member"
            : "Limited member"
          : prevState.name,
        riskArray: riskArray,
      };
    }
    // if(prevState.isChanged===false){
    //   object.name=nextProps.menuType==="weeks" || nextProps.typeDate==="Week"?[ "1st week" ]:nextProps.typeDate==="Day"?[ "1st" ]:nextProps.typeDate==="Month"?[ "1st" ]:prevState.name
    // }
    return object;
  }

  render() {
    const {
      classes,
      theme,
      label,
      menuType,
      heading,
      iconType,
      isSingle,
      isSimpleList,
      errorState,
      errorMessage,
      error,
      style,
      helptext,
      view,
      permission,
      permissionKey,
      MenuData,
      disabled,
      inputProps,
    } = this.props;
    const { name } = this.state;
    const color = theme.palette;
    let canEdit = true;
    if (view === "issue" && permissionKey) {
      canEdit = getEditPermissionsWithArchieve(MenuData, permission, permissionKey);
    }
    if (view === "risk" && permissionKey) {
      canEdit = permission;
    }
    const MenuProps = {
      // transformOrigin: { vertical: -40, horizontal: 0 },
      PaperProps: {
        style: {
          overflow: "visible",
          // transform: "translateY(40px)",
          background: theme.palette.common.white,
          borderRadius: 4,
        },
      },
      anchorReference: this.anchorEl,
    };
    const RenderIcon = color => {
      return menuType == "piriority" ? (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={color}
          classes={{ root: classes.priorityIcon }}>
          <FlagIcon />
        </SvgIcon>
      ) : menuType == "severity" ? (
        <RingIcon htmlColor={color} classes={{ root: classes.severityIcon }} />
      ) : menuType == "impact" ? (
        <RingIcon htmlColor={color} classes={{ root: classes.severityIcon }} />
      ) : menuType == "status" ||
        menuType == "issueStatus" ||
        menuType == "meetingStatus" ||
        menuType == "riskStatus" ? (
        <RoundIcon htmlColor={color} classes={{ root: classes.severityIcon }} />
      ) : (
        false
      );
    };
    const ddData =
      menuType == "piriority"
        ? [
            { id: 0, name: "Critical", color: color.issuePriority.Critical },
            { id: 1, name: "High", color: color.issuePriority.High },
            { id: 2, name: "Medium", color: color.issuePriority.Medium },
            { id: 3, name: "Low", color: color.issuePriority.Low },
          ]
        : menuType == "severity"
        ? [
            { name: "Minor", color: color.severity.minor },
            { name: "Moderate", color: color.severity.moderate },
            { name: "Major", color: color.severity.major },
            { name: "Critical", color: color.severity.critical },
          ]
        : menuType == "impact"
        ? [
            { name: "Minor", color: color.riskImpact.Minor },
            { name: "Moderate", color: color.riskImpact.Moderate },
            { name: "Major", color: color.riskImpact.Major },
            { name: "Critical", color: color.riskImpact.Critical },
          ]
        : menuType == "riskStatus"
        ? this.state.riskArray
        : menuType == "issueStatus"
        ? [
            { name: "Open", color: color.issueStatus.Open },
            { name: "Closed", color: color.issueStatus.Closed },
          ]
        : menuType == "taskStatus"
        ? [
            { name: "Open", color: color.status.success },
            { name: "Closed", color: color.status.notStarted },
          ]
        : menuType == "meetingStatus"
        ? [
            { name: "Upcoming", color: color.meetingStatus.Upcoming },
            { name: "In Review", color: color.meetingStatus.InReview },
            { name: "Published", color: color.meetingStatus.Published },
            { name: "Cancelled", color: color.meetingStatus.Cancelled },
            { name: "Overdue", color: color.meetingStatus.OverDue },
          ]
        : menuType == "issueType"
        ? [{ name: "Bug" }, { name: "Feature" }, { name: "Improvement" }]
        : menuType == "likelihood"
        ? [{ name: "0-25" }, { name: "26-50" }, { name: "51-75" }, { name: "76-100" }]
        : menuType == "projectRate"
        ? [{ name: "Project" }, { name: "Task" }, { name: "Resource" }]
        : menuType == "roles"
        ? [{ name: "Admin" }, { name: "Member" }, { name: "Limited member" }]
        : menuType == "weeks"
        ? [{ name: "1st week" }, { name: "2nd week" }, { name: "3rd week" }, { name: "4th week" }]
        : menuType == "days"
        ? [
            { name: "1st" },
            { name: "2nd" },
            { name: "3rd" },
            { name: "4th" },
            { name: "5th" },
            { name: "6th" },
            { name: "7th" },
            { name: "8th" },
            { name: "9th" },
            { name: "10th" },
            { name: "11th" },
            { name: "12th" },
            { name: "13th" },
            { name: "14th" },
            { name: "15th" },
            { name: "16th" },
            { name: "17th" },
            { name: "18th" },
            { name: "19th" },
            { name: "20th" },
            { name: "21st" },
            { name: "22nd" },
            { name: "23rd" },
            { name: "24th" },
            { name: "25th" },
            { name: "26th" },
            { name: "27th" },
            { name: "28th" },
            { name: "29th" },
            { name: "30th" },
            { name: "31st" },
          ]
        : menuType == "months"
        ? [
            { name: "1st" },
            { name: "2nd" },
            { name: "3rd" },
            { name: "4th" },
            { name: "5th" },
            { name: "6th" },
            { name: "7th" },
            { name: "8th" },
            { name: "9th" },
            { name: "10th" },
            { name: "11th" },
            { name: "12th" },
          ]
        : [];
    return (
      <Fragment>
        <FormControl
          className={classes.selectFormControl}
          style={{
            marginTop: !label ? 0 : 22,
            marginBottom: error == false ? 22 : 10,
            ...style,
          }}
          error={errorState}>
          {label ? (
            <InputLabel
              shrink={true}
              classes={{ root: classes.defaultInputLabel }}
              htmlFor="selectMenu">
              {label}
              {helptext ? <CustomTooltip helptext={helptext} iconType="help" /> : null}
            </InputLabel>
          ) : null}
          <Select
            value={name}
            label="Priority"
            multiple={isSingle ? false : true}
            onChange={this.handleChange}
            disabled={disabled || (view === "issue" || view === "risk" ? !canEdit : false)}
            renderValue={selected => (isSingle ? selected : selected.join(", "))}
            input={
              <OutlinedInput
                labelWidth={150}
                notched={false}
                inputRef={node => {
                  this.anchorEl = node;
                }}
                classes={{
                  root: classes.outlinedSelectCnt,
                  // notchedOutline: classes.outlinedSelect
                  // disabled:true
                }}
                id="selectMenu"
              />
            }
            MenuProps={MenuProps}
            classes={{
              // root: classes.selectCnt,
              select: classes.singleSelectCmp,
              icon: classes.selectIcon,
              //disabled:true
            }}>
            <ListItem disableRipple={true} classes={{ root: classes.headingItem }}>
              <ListItemText primary={heading} classes={{ primary: classes.headingText }} />
            </ListItem>
            {ddData.map((value, i) => (
              <ListItem
                key={value.name}
                value={
                  menuType === "likelihood"
                    ? value.name + " %"
                    : menuType === "riskStatus"
                    ? value.label
                    : value.name
                }
                button
                disableRipple={true}
                className={`${classes[`menuItem${iconType}`]} ${
                  name.indexOf(value.name) > -1 ? classes.selectedValue : ""
                }`}
                classes={{
                  root: classes[`menuItem${iconType}`],
                  selected: classes.itemSelected,
                }}>
                {menuType == "issueType" ? (
                  i == 0 ? (
                    <SvgIcon
                      viewBox="0 0 456.828 456.828"
                      htmlColor={theme.palette.error.light}
                      className={classes.bugIcon}>
                      <BugIcon />
                    </SvgIcon>
                  ) : i == 2 ? (
                    <SvgIcon
                      viewBox="0 0 19.481 19.481"
                      classes={{ root: classes.ratingStarIcon }}
                      htmlColor={theme.palette.background.star}>
                      <StarIcon />
                    </SvgIcon>
                  ) : (
                    <SvgIcon
                      viewBox="0 0 11 10.188"
                      classes={{ root: classes.ImprovmentIcon }}
                      htmlColor={theme.palette.primary.light}>
                      <ImprovementIcon />
                    </SvgIcon>
                  )
                ) : isSimpleList ? null : (
                  RenderIcon(value.color)
                )}

                {menuType === "riskStatus" ? (
                  <ListItemText
                    primary={value.label}
                    classes={{
                      primary: classes.itemText,
                    }}
                  />
                ) : (
                  <ListItemText
                    primary={menuType === "likelihood" ? value.name + " %" : value.name}
                    classes={{
                      primary: classes.itemText,
                    }}
                  />
                )}
              </ListItem>
            ))}
          </Select>
          {errorState ? <FormHelperText>{errorMessage}</FormHelperText> : null}
        </FormControl>
      </Fragment>
    );
  }
}

export default withStyles(combineStyles(dropdownStyles, readyDropdownStyles, selectStyles), {
  withTheme: true,
})(SelectIconMenu);
