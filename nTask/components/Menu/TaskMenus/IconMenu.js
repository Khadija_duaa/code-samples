import React, { Component, Fragment } from "react";
import DropdownMenu from "../DropdownMenu";
import GenerateMenu from "../generateMenu";
import CustomIconButton from "../../Buttons/CustomIconButton";
import RoundIcon from "@material-ui/icons/Brightness1";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
// import FlagIcon from "@material-ui/icons/Flag";
import { withStyles } from "@material-ui/core/styles";
import readyDropdownStyles from "./style";
import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import ddData from "./staticMenuData";
import helper from "./../../../helper/";
import DefaultButton from "../../Buttons/DefaultButton";
import Typography from "@material-ui/core/Typography";
import { getEditPermissionsWithArchieve } from "../../../Views/Issue/permissions";
import { getRisksEditPermissionsWithArchieve } from "../../../Views/Risk/permissions";
import CustomButton from "../../Buttons/CustomButton";
import FlagIcon from "../../Icons/FlagIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import { GetPermission } from "../../permissions";
import { compose } from "redux";
import { injectIntl } from "react-intl";
class IconMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      name: [],
      selectedStatus: false,
      checked: [],
      selectedColor: props.theme.palette.status.notStarted,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  getTranslatedLabel = name => {
    var trans = {};
    switch (name) {
      case "Open":
        trans = { id: "issue.common.status.drop-down.open", defaultMessage: name };
        break;
      case "Closed":
        trans = { id: "issue.common.status.drop-down.close", defaultMessage: name };
        break;
      case "Critical":
        trans = { id: "issue.common.severity.dropdown.critical", defaultMessage: name };
        break;
      case "Major":
        trans = { id: "issue.common.severity.dropdown.major", defaultMessage: name };
        break;
      case "Minor":
        trans = { id: "issue.common.severity.dropdown.minor", defaultMessage: name };
        break;
      case "Moderate":
        trans = { id: "issue.common.severity.dropdown.moderate", defaultMessage: name };
        break;
      case "High":
        trans = { id: "issue.common.priority.dropdown.high", defaultMessage: name };
        break;
      case "Low":
        trans = { id: "issue.common.priority.dropdown.low", defaultMessage: name };
        break;
      case "Medium":
        trans = { id: "issue.common.priority.dropdown.medium", defaultMessage: name };
        break;

      case "Agreed":
        trans = { id: "risk.common.status.dropdown.agreed", defaultMessage: name };
        break;
      case "Identified":
        trans = { id: "risk.common.status.dropdown.identified", defaultMessage: name };
        break;
      case "In Review":
        trans = { id: "risk.common.status.dropdown.in-review", defaultMessage: name };
        break;
      case "Rejected":
        trans = { id: "risk.common.status.dropdown.rejected", defaultMessage: name };
        break;
    }
    return Object.keys(trans).length ? this.props.intl.formatMessage(trans) : name;
  };
  componentDidMount() {
    if (this.props.keyType && this.props.MenuData) {
      this.setState({ checked: [this.props.MenuData[this.props.keyType]] });
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.keyType && props.MenuData) {
      return { checked: [props.MenuData[props.keyType]] };
    }
  }

  handleClick(event) {
    event.stopPropagation();
    const {
      permission,
      MenuData,
      issuePermission = {},
      permissionKey,
      viewType,
      riskPermission = {},
    } = this.props;
    if (viewType === "issue" && permissionKey) {
      const canEdit = GetPermission.canEdit(MenuData.isOwner, issuePermission, permissionKey);
      this.setState(state => ({
        open: !canEdit ? false : true,
      }));
    } else if (viewType === "risk" && permissionKey) {
      const canEdit = GetPermission.canEdit(MenuData.isOwner, riskPermission, permissionKey);
      this.setState(state => ({
        open: !canEdit ? false : true,
      }));
    } else {
      this.setState({ open: true });
    }
  }
  handleClose(event) {
    this.setState({ open: false });
  }
  handleToggle = (event, value, color) => {
    event.stopPropagation();
    this.props.toggle;
    this.setState(
      {
        checked: [value],
        selectedColor: color,
      },
      () => {
        if (this.props.isBulk) {
          if (this.props.keyType) {
            const type = helper.RETURN_BULKACTIONTYPES(this.props.keyType);
            let data = {
              type,
            };
            data[this.props.keyType] = value;
            if (this.props.viewType === "issue") {
              data.issueIds = this.props.selectedIdsList;
              this.props.BulkUpdate(data);
            } else if (this.props.viewType === "risk") {
              data.riskIds = this.props.selectedIdsList;
              this.props.BulkUpdate(data);
            }
          }
        } else {
          if (this.props.keyType) this.props.MenuData[this.props.keyType] = value;
          this.props.isUpdated(this.props.MenuData, () => { });
        }
        this.handleClose();
      }
    );
  };

  render() {
    const {
      classes,
      theme,
      status,
      menuType,
      buttonStyles,
      heading,
      isBulk,
      buttonText,
      permission,
      MenuData,
      viewType,
      permissionKey,
      isArchivedSelected,
      intl,
      customButtonProps = {},
      customChildren = false
    } = this.props;
    const { open, checked, selectedColor } = this.state;
    const color = theme.palette;
    let canEdit = true;
    if (viewType === "issue" && permissionKey) {
      canEdit = permission;
    }
    if (viewType === "risk" && permissionKey) {
      canEdit = getRisksEditPermissionsWithArchieve(MenuData, permission, permissionKey);
    }
    const RenderIcon = (color, classes) => {
      return menuType == "priority" ? (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={color}
          classes={{ root: classes.piriorityIcon }}>
          <FlagIcon />
        </SvgIcon>
      ) : menuType == "severity" || menuType == "impact" ? (
        <RingIcon htmlColor={color} classes={{ root: classes.severityIcon }} />
      ) : menuType == "issueStatus" || menuType == "riskStatus" || menuType == "meetingStatus" ? (
        <RoundIcon htmlColor={color} classes={{ root: classes.statusIcon }} />
      ) : null;
    };

    let stateColor = selectedColor;
    const data = ddData(color, intl)[menuType];
    if (
      data &&
      this.props.keyType &&
      this.props.MenuData &&
      this.props.MenuData[this.props.keyType]
    ) {
      stateColor = data.find(x => this.props.MenuData[this.props.keyType] === x.name);
      stateColor = stateColor ? stateColor.color : selectedColor;
    }

    return (
      <>
        {isBulk ? (
          <Button
            variant="outlined"
            style={buttonStyles}
            classes={{ outlined: classes.BulkActionBtn }}
            disabled={isArchivedSelected}
            onClick={event => this.handleClick(event)}
            buttonRef={node => {
              this.anchorEl = node;
            }}>
            {buttonText}
          </Button>
        ) : !checked[0] ? (
          <>
            {!canEdit ? (
              <Typography variant="body2">{heading}</Typography>
            ) : (
              <CustomButton
                buttonRef={node => {
                  this.anchorEl = node;
                }}
                disabled={isArchivedSelected || !permission}
                variant="text"
                btnType="plain"
                onClick={event => {
                  this.handleClick(event);
                }}
                {...customButtonProps}>
                {heading}
              </CustomButton>
            )}
          </>
        ) : menuType == "issueStatus" || menuType == "riskStatus" || menuType == "meetingStatus" ? (
          <CustomButton
            style={{ background: stateColor }}
            disabled={isArchivedSelected || !permission}
            buttonRef={node => {
              this.anchorEl = node;
            }}
            variant="contained"
            btnType="status"
            onClick={event => {
              this.handleClick(event);
            }}
            {...customButtonProps}>
            {this.getTranslatedLabel(checked[0])}
            {customChildren ? customChildren : ""}
          </CustomButton>
        ) : menuType == "severity" || menuType == "impact" ? (
          <CustomButton
            buttonRef={node => {
              this.anchorEl = node;
            }}
            disabled={isArchivedSelected || !permission}
            variant="text"
            btnType="plain"
            labelAlign="left"
            onClick={event => {
              this.handleClick(event);
            }}
            {...customButtonProps}>
            <RingIcon htmlColor={stateColor} classes={{ root: classes.severityIcon }} />{" "}
            {this.getTranslatedLabel(checked[0])}
          </CustomButton>
        ) : (
          <CustomIconButton
            btnType="white"
            buttonRef={node => {
              this.anchorEl = node;
            }}
            disabled={isArchivedSelected || !permission}
            onClick={event => {
              this.handleClick(event);
            }}>
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={stateColor}
              classes={{ root: classes.piriorityIcon }}>
              <FlagIcon />
            </SvgIcon>
          </CustomIconButton>
        )}
        <DropdownMenu
          open={open}
          closeAction={this.handleClose}
          anchorEl={this.anchorEl}
          size="small">
          <div>
            <GenerateMenu
              data={ddData(color, intl)[menuType]}
              handleToggle={this.handleToggle}
              listType="iconList"
              headingText={heading}
              checked={checked}
              colorSelect={true}
              iconType="status"
              renderIcon={RenderIcon}
            />
          </div>
        </DropdownMenu>
      </>
    );
  }
}

export default compose(injectIntl, withStyles(readyDropdownStyles, { withTheme: true }))(IconMenu);