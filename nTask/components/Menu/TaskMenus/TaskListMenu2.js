import React, { Component, Fragment } from "react";
import Hotkeys from "react-hot-keys";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import DropdownMenu from "../DropdownMenu";
import GenerateMenu from "../generateList";
import { withStyles } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { createFilter } from "react-search-input";
import readyDropdownStyles from "./style";
import CustomButton from "../../Buttons/CustomButton";
import AddIcon from "@material-ui/icons/Add";
import { FormattedMessage, injectIntl } from "react-intl";

class TaskListMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      searchedName: "",
      checked: [],
      dropDownWidth: 300,
      dropDownHeight: 320,
    };
    this.anchorEl = React.createRef();
  }

  componentDidMount() {
    this.props.setTaskListTrigger(this.anchorEl);
  }

  handleMenuOpen = event => {
    event.stopPropagation();
    this.setState({ open: true });
  };

  handleMenuClose = () => {
    this.setState({ open: false, searchedName: "" });
  };

  searchUpdated = name => {
    this.setState({ searchedName: name });
  };

  handleItemClick = (event, value, id, projectId) => {
    if (event) event.stopPropagation();
    const { handleItemSelect } = this.props;
    handleItemSelect(id, projectId);
    this.handleMenuClose();
  };

  onKeyDown = keyName => {
    if (keyName === "alt+i") this.anchorEl.click();
    else if (keyName === "esc") this.handleMenuClose();
  };

  filterAddedTasks = () => {
    const {
      filteredList,
      tasksState: { data },
      workspaceId,
    } = this.props;

    return (
      data &&
      data.length &&
      data.reduce((r, cv) => {
        const filterSingleTask = filteredList.find(ft => ft.taskId == cv.taskId);
        if (!filterSingleTask && workspaceId == cv.teamId) {
          r.push({ id: cv.taskId, value: { name: cv.taskTitle, projectId: cv.projectId } });
        }
        return r;
      }, [])
    );
  };
  resizeWidth = (width, height) => {
    this.setState({ dropDownWidth: width });
    this.setState({ dropDownHeight: height });
  }
  render() {
    const { theme, disabled = false } = this.props;
    const { open, checked } = this.state;

    const allData = this.filterAddedTasks() || [];
    let headingText = "Select Tasks";

    const searchQuery = ["id", "value.name"];
    const searchedData = allData.filter(createFilter(this.state.searchedName, searchQuery));

    return (
      <>
        <Hotkeys keyName="esc,alt+i" onKeyDown={this.onKeyDown} />
        <CustomButton
          variant="text"
          btnType="plain"
          buttonRef={node => {
            this.anchorEl = node;
          }}
          onClick={this.handleMenuOpen}
          disabled={disabled}
          style={{
            background: "white",
            width: 90,
            padding: "4px 5px 4px 0px",
            fontWeight: theme.typography.fontWeightLight,
            fontFamily: theme.typography.fontFamilyLato,
            border: `1px solid ${theme.palette.border.lightBorder}`
          }}>
          <AddIcon htmlColor={theme.palette.primary.light} fontSize="small" />
          <FormattedMessage id="task.creation-dialog.title" defaultMessage="Add Task" />
        </CustomButton>
        <DropdownMenu
          open={open}
          closeAction={this.handleMenuClose}
          anchorEl={this.anchorEl}
          size="large"
          placement="top-start">
          <div>
            <GenerateMenu
              data={searchedData}
              handleItemClick={this.handleItemClick}
              checked={checked}
              headingText={
                <FormattedMessage id="common.select-tasks.label" defaultMessage={headingText} />
              }
              searchQuery={searchQuery}
              searchAction={this.searchUpdated}
              searchPlaceholder={this.props.intl.formatMessage({
                id: "common.search.label",
                defaultMessage: "Search",
              })}
              handleResize={this.resizeWidth}
              menuWidth={this.state.dropDownWidth}
              menuHeight={this.state.dropDownHeight}
            />
          </div>
        </DropdownMenu>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tasksState: state.tasks,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(readyDropdownStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(TaskListMenu);
