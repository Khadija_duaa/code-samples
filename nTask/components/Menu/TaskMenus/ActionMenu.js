import React, { Component, Fragment } from "react";
import DropdownMenu from "../DropdownMenu";
import GenerateMenu from "../generateMenu";
import CustomIconButton from "../../Buttons/IconButton";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";

class ActionMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      placement: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleClick(event) {
    event.stopPropagation()
    this.setState({ open: true });
  }
  handleClose(event) {
    this.setState({ open: false });
  }

  render() {
    const { classes, theme } = this.props;
    const { open } = this.state;
    
    const ddData = [
      { name: "copy" },
      { name: "color" },
      { name: "Public Link" },
      { name: "Archive" },
      { name: "Delete" }
    ];
    return (
      <Fragment>
        <CustomIconButton
          btnType="condensed"
          onClick={event => {
            this.handleClick(event, "bottom-start");
          }}
          buttonRef={node => {
            this.anchorEl = node;
          }}
        >
          <MoreVerticalIcon htmlColor={theme.palette.secondary.light} />
        </CustomIconButton>
        <DropdownMenu
          open={open}
          closeAction={this.handleClose}
          anchorEl={this.anchorEl}
          size="small"
        >
        <div>
          <GenerateMenu
            data={ddData}
            headingText="Select Action"
            listType="simpleList"
            colorSelect={false}
          />
          </div>
        </DropdownMenu>
      </Fragment>
    );
  }
}

export default ActionMenu;
