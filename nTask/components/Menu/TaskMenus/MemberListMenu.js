import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import DropdownMenu from "../DropdownMenu";
import GenerateMenu from "../generateMenu";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Avatar from "@material-ui/core/Avatar";
import CustomAvatar from "../../Avatar/Avatar";
import AddIcon from "@material-ui/icons/Add";
import { createFilter } from "react-search-input";
import readyDropdownStyles from "./style";
import helper from "./../../../helper/";
import Button from "@material-ui/core/Button/";
import CustomButton from "../../Buttons/CustomButton";
import { generateUsername } from "../../../utils/common";
import Typography from "@material-ui/core/Typography";
import { getEditPermissionsWithArchieve } from "../../../Views/Issue/permissions";
import { getRisksEditPermissionsWithArchieve } from "../../../Views/Risk/permissions";
import cloneDeep from "lodash/cloneDeep";
import sortBy from "lodash/sortBy";
import { isArrayEqual } from "../../../utils/common";
import MoreMembersMenu from "../MoreMembersMenu";
import Tooltip from "@material-ui/core/Tooltip";
import { generateActiveMembers } from "../../../helper/getActiveMembers";
import { FormattedMessage, injectIntl } from "react-intl";

class MemberListMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      searchTerm: "",
      checked: [],
      idList: [],
      isToggled: false,
      anchorEl: null,
      list: [],
      allMembers: [],
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.searchUpdated = this.searchUpdated.bind(this);
  }
  componentDidMount() {
    const { linkedTasks, checkedValues, checkedIds } = this.props;
    if (checkedValues) {
      this.setState({ idList: checkedIds, checked: checkedValues });
    } else if (linkedTasks) this.setSelectedTasks(linkedTasks);
    else {
      const allMembers = this.getAllMembers();
      this.setState({ list: this.updateAssigneesFromProps(), allMembers });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.viewType === "issue") {
      if (
        JSON.stringify(prevProps.issuesState.data) !==
        JSON.stringify(this.props.issuesState.data)
      ) {
        this.setState({
          open: false,
          searchTerm: "",
          checked: [],
          idList: [],
          isToggled: false,
          anchorEl: null,
        });
      }
    }

    const {
      assigneeList,
      profileState,
      linkedTasks,
      checkedValues,
      checkedIds,
    } = this.props;
    const { allMembers } = profileState.data.member;
    if (
      checkedValues &&
      JSON.stringify(checkedValues) !== JSON.stringify(prevProps.checkedValues)
    ) {
      this.setState({ idList: checkedIds, checked: checkedValues });
    } else if (
      linkedTasks &&
      JSON.stringify(linkedTasks) !== JSON.stringify(prevProps.linkedTasks)
    ) {
      this.setSelectedTasks(linkedTasks);
    } else if (
      assigneeList &&
      JSON.stringify(prevProps.assigneeList) !== JSON.stringify(assigneeList)
    ) {
      this.setState({ list: this.updateAssigneesFromProps() });
    } else if (
      assigneeList &&
      JSON.stringify(allMembers) !==
      JSON.stringify(prevProps.profileState.data.member.allMembers)
    ) {
      this.setState({
        allMembers: this.getAllMembers(),
        list: this.updateAssigneesFromProps(),
      });
    }
  }

  setSelectedTasks = (linkedTasks) => {
    if (linkedTasks && linkedTasks.length) {
      let checked = [],
        idList = [];
      const tasks = this.props.tasksState.data || [];
      linkedTasks.forEach((taskId) => {
        let task = tasks.filter((x) => x.taskId === taskId);
        if (task && task.length) {
          checked.push(task[0].taskTitle);
          idList.push(taskId);
        }
      });
      this.setState({ idList, checked });
    }
  };

  getAllMembers = () => {
    return this.props.profileState &&
      this.props.profileState.data &&
      this.props.profileState.data.member
      ? generateActiveMembers().map((x) => {
        const name = generateUsername(x.email, x.userName, x.fullName, null);
        return {
          id: x.userId,
          value: {
            name: name,
            avatar: x.imageUrl,
            email: x.email,
            fullName: x.fullName,
            isOnline: x.isOnline,
            isOwner: x.isOwner,
          },
          isActiveMember: x.isActive,
        };
      })
      : [];
  };

  updateAssigneesFromProps = () => {
    let checked = [],
      idList = [];
    const assigneeList = this.props.assigneeList || [];
    let assigneeListNew = assigneeList.length
      ? assigneeList.map((x) => {
        const name = generateUsername(x.email, x.userName, x.fullName, null);
        checked.push(name);
        idList.push(x.userId);
        return {
          id: x.userId,
          value: {
            name: name,
            avatar: x.imageUrl,
            email: x.email,
            fullName: x.fullName,
            isOnline: x.isOnline,
            isOwner: x.isOwner,
          },
        };
      })
      : [];

    if (this.props.viewType === "risk") this.setState({ checked, idList });

    return assigneeListNew;
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.reset) {
      nextProps.closeSelectedRolePopup(true);
      let roles =
        nextProps.permissionSettingsState.data &&
          nextProps.permissionSettingsState.data.defaultRoles &&
          nextProps.permissionSettingsState.data.defaultRoles.length
          ? nextProps.permissionSettingsState.data.defaultRoles.map((x) => {
            return { id: x.roleId, value: { name: x.roleName } };
          })
          : [];

      let data = nextProps.roles;
      if (data) {
        const roleData = roles.find((x) => {
          if (x.id === data.roleId) {
            return x;
          }
        });
        if (roleData) {
          return {
            checked: [roleData.value.name],
            idList: [roleData.id],
          };
        }
      }
    }
    if (!prevState.isToggled) {
      if (nextProps.isProject) {
        let data = nextProps.projectTaskData;
        if (data) {
          nextProps.data.map((x) => {
            if (x.id === data.projectId) {
              ({
                checked: [x.value.name],
              });
            }
          });
        }
      } else if (nextProps.isTask) {
        let checkedData = [],
          idList = [];
        let data = nextProps.MenuData;
        if (data) {
          let tasks =
            nextProps.tasksState.data && nextProps.tasksState.data.length
              ? nextProps.tasksState.data.map((x) => {
                return { id: x.taskId, value: { name: x.taskTitle } };
              })
              : [];

          tasks.map((x) => {
            if (data.linkedTasks)
              if (data.linkedTasks.indexOf(x.id) >= 0) {
                checkedData.push(x.value.name);
                idList.push(x.id);
              }
          });
          return {
            checked: checkedData,
            idList,
          };
        }
      } else if (nextProps.isRoles) {
        let roles =
          nextProps.permissionSettingsState.data &&
            nextProps.permissionSettingsState.data.defaultRoles &&
            nextProps.permissionSettingsState.data.defaultRoles.length
            ? nextProps.permissionSettingsState.data.defaultRoles.map((x) => {
              return { id: x.roleId, value: { name: x.roleName } };
            })
            : [];

        let data = nextProps.roles;
        if (data) {
          const roleData = roles.find((x) => {
            if (x.id === data.roleId) {
              return x;
            }
          });
          if (roleData) {
            return {
              checked: [roleData.value.name],
              idList: [roleData.id],
            };
          }
        }
      } else if (nextProps.isLikelihood) {
        return {
          checked: [nextProps.likelihood],
        };
      } else if (nextProps.isType) {
        return {
          checked: [nextProps.issueType],
        };
      } else if (nextProps.isLabel) {
        return {
          checked: [nextProps.issueLabel],
        };
      } else if (nextProps.isCategory) {
        return {
          checked: [nextProps.issueCategory],
        };
      } else if (nextProps.checkAssignee) {
        let checkedData = [],
          idList = [];
        let assigneeList = nextProps.assigneeList || [];
        let ids = assigneeList.map((x) => x.userId);
        let allMembers = prevState.allMembers;

        allMembers.map((x) => {
          if (ids.indexOf(x.id) >= 0) {
            checkedData.push(x.value.name), idList.push(x.id);
          }
        });
        return {
          checked: checkedData,
          idList,
        };
      }
    }
    return {
      checked: prevState.checked,
      idList: prevState.idList,
      isToggled: nextProps.showRiskDetails ? false : prevState.isToggled,
    };
  }

  handleClose(e) {
    e.stopPropagation();
    this.setState({ open: false, searchTerm: "" });
    const MenuData = cloneDeep(this.props.MenuData);
    const { checked, idList } = this.state;
    if (this.props.checkAssignee) {
      let obj = this.props.tasksState.data.find(
        (k) => k.taskId == this.props.taskId
      );
      if (this.props.viewType === "issue") {
        if (this.props.isBulk) {
          if (this.props.keyType) {
            const type = helper.RETURN_BULKACTIONTYPES(this.props.keyType);
            let data = {
              type,
            };
            data["assigneIds"] = idList;
            data.issueIds = this.props.selectedIdsList;
            this.props.BulkUpdate(data);
          }
        } else {
          let list = (cloneDeep(this.state.list) || []).map((obj) => {
            obj["userId"] = obj["id"];
            return obj;
          });
          if (!isArrayEqual(list, this.props.assigneeList, "userId")) {
            obj = cloneDeep(
              this.props.issuesState.data.find((k) => k.id == this.props.id)
            );
            delete obj.assigneeLists;
            obj.issueAssingnees = this.state.idList;
            this.props.isUpdated(obj, () => { });
          }
        }
      } else if (this.props.viewType === "meeting") {
        if (this.props.isBulk) {
          if (this.props.keyType) {
            const type = helper.MEETING_BULK_TYPES(this.props.keyType);
            let data = {
              type,
            };
            data["attendeeIds"] = idList;
            data.meetingIds = this.props.selectedIdsList;
            if (idList.length) this.props.BulkUpdate(data, idList);
            this.setState({ checked: [], idList: [] });
          }
        } else {
          let meeting = cloneDeep(MenuData);

          let list = (cloneDeep(this.state.list) || []).map((obj) => {
            obj["userId"] = obj["id"];
            return obj;
          });
          if (!isArrayEqual(list, this.props.assigneeList, "userId")) {
            if (this.props.selectAssigneeList && this.state.isToggled) {
              this.props.selectAssigneeList(list, this.props.isNewAdded);
            } else {
              meeting.attendeeList = idList;
              if (
                (!meeting.startTime || !meeting.startDateString) &&
                !this.props.action
              ) {
                this.props.showDateTimeDetailsPopUp(meeting);
              } else {
                this.props.isUpdated(meeting, () => { });
              }
            }
            this.setState({ isToggled: false });
          }
        }
      } else {
        delete obj.assigneeLists;
        obj.assigneeList =
          this.state.idList && this.state.idList.length
            ? this.state.idList.filter((e, i, s) => s.indexOf(e) === i)
            : [];

        this.props.isUpdated(obj, () => { });
      }
    } else if (this.props.viewType === "risk" && !this.props.isLikelihood) {
      let itemKeyType = "riskOwner";
      if (this.props.linkedTasks) itemKeyType = "linkedTasks";

      if (this.props.isBulk) {
        const { keyType } = this.props;
        if (keyType) {
          let types = {
            Status: "Status",
            AddTask: "LinkedTasks",
            AddOwner: "riskOwner",
            Impact: "Impact",
            Likelihood: "Likelihood",
          };
          let data = {};
          data.type = helper.RETURN_BULKACTIONTYPES(keyType);
          data[types[keyType]] = keyType === "AddTask" ? idList : idList[0];
          data.riskIds = this.props.selectedIdsList;
          if (idList.length > 0) {
            this.props.BulkUpdate(data);
            this.setState({ open: false });
          }
        }
      } else {
        if (itemKeyType === "linkedTasks") {
          MenuData[itemKeyType] = idList;
          this.props.isUpdated(MenuData, () => { });
          this.setState({ open: false });
        } else if (MenuData[itemKeyType] !== (idList[0] || null)) {
          // If current selection is different from props than send update
          MenuData[itemKeyType] = idList[0] || null;
          this.props.isUpdated(MenuData, () => { });
        }
      }
    } else if (this.props.viewType === "meeting") {
      MenuData["taskId"] = idList[0];
      this.setState({ open: false }, () => {
        this.props.isUpdated(MenuData, () => { });
      });

    } else if (this.props.isTask) {
      if (this.props.viewType === "issue") {
        if (this.props.isBulk) {
          if (this.props.keyType) {
            const type = helper.RETURN_BULKACTIONTYPES(this.props.keyType);
            let data = {
              type,
            };
            if (idList.length > 0) {
              data["linkedTasks"] = idList;
              data.issueIds = this.props.selectedIdsList;
              this.props.BulkUpdate(data);
              this.setState({ open: false });
            }
          }
        } else {
          MenuData.linkedTasks = idList;
          this.props.isUpdated(MenuData, () => { });
          this.setState({ open: false });
        }
      }
    } else if (this.props.isLikelihood) {
      if (this.props.viewType === "risk") {
        if (this.props.isBulk) {
          if (this.props.keyType) {
            const type = helper.RETURN_BULKACTIONTYPES(this.props.keyType);
            let data = {
              type,
            };
            data["likelihood"] = checked[0];
            data.riskIds = this.props.selectedIdsList;
            if (data.likelihood) {
              this.props.BulkUpdate(data);
              this.setState({ open: false });
            }
          }
        } else {
          MenuData.likelihood = checked[0];
          this.props.isUpdated(MenuData, () => { });
          this.setState({ open: false });
        }
      }
    } else if (this.props.isType) {
      if (this.props.viewType === "issue") {
        MenuData.type = checked[0];
      }
      this.props.isUpdated(MenuData, () => { });
      this.setState({ open: false });
    } else if (this.props.isLabel) {
      if (this.props.viewType === "issue") {
        MenuData.label = checked[0];
      }
      this.props.isUpdated(MenuData, () => { });
      this.setState({ open: false });
    } else if (this.props.isCategory) {
      if (this.props.viewType === "issue") {
        MenuData.category = checked[0];
      }
      this.props.isUpdated(MenuData, () => { });
      this.setState({ open: false });
    }
  }

  handleClick(event) {
    event.stopPropagation();
    const { currentTarget } = event;
    const {
      permission,
      MenuData,
      permissionKey,
      viewType,
      showDateTimeDetailsPopUp,
      action,
      isBulk,
    } = this.props;
    let meeting = cloneDeep(MenuData);

    if (viewType === "issue" && permissionKey) {
      const canEdit = getEditPermissionsWithArchieve(
        MenuData,
        permission,
        permissionKey
      );
      this.setState((state) => ({
        open: !canEdit ? false : true,
        anchorEl: currentTarget,
      }));
    } else if (viewType === "risk" && permissionKey) {
      const canEdit = getRisksEditPermissionsWithArchieve(
        MenuData,
        permission,
        permissionKey
      );
      this.setState((state) => ({
        open: !canEdit ? false : true,
        anchorEl: currentTarget,
      }));
    } else if (viewType === "meeting") {
      if (!isBulk) {
        if (
          meeting &&
          (!meeting.startTime || !meeting.startDateString) &&
          !action
        ) {
          if (showDateTimeDetailsPopUp) showDateTimeDetailsPopUp(meeting);
        } else {
          this.setState({ open: true, anchorEl: currentTarget });
        }
      } else this.setState({ open: true, anchorEl: currentTarget });
    } else {
      this.setState({ open: true, anchorEl: currentTarget });
    }
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }
  getMemberById = (id, members) => {
    const allMembers = members ? members : this.state.allMembers;
    for (let i = 0; i < allMembers.length; i++) {
      if (allMembers[i].id === id) return allMembers[i];
    }
    return null;
  };
  handleToggle = (event, value, id) => {
    event.stopPropagation();
    const { checked, idList } = this.state;
    const Risk = cloneDeep(this.props.MenuData);
    // let currentIndex = checked.indexOf(value);
    let newChecked = [...checked];
    let ids = [...idList];
    const currentIndex = id ? idList.indexOf(id) : checked.indexOf(value);
    const checkedState = currentIndex === -1 ? [value] : [];
    if (this.props.checkedType === "multi") {
      let list = this.state.list.concat();
      if (currentIndex === -1) {
        newChecked.push(value);
        ids.push(id);
        let member = this.getMemberById(id);
        if (member) {
          list.push(member);
          list = sortBy(list, [
            function (o) {
              return o.value.name;
            },
          ]);
        }
      } else {
        newChecked = newChecked.filter((x) => x !== value);
        ids = ids.filter((x) => x !== id);
        list = list.filter((x) => x.id !== id);

        // Set miminum values selection
        const { minimumSelections } = this.props;
        if (minimumSelections) {
          if (list.length < minimumSelections) return;
        }
      }
      this.setState({
        checked: newChecked,
        idList: ids,
        list,
        isToggled: true,
      });
    } else {
      if (this.props.isRoles) {
        if (checkedState.length && id) {
          const objectData = {
            MemberId: this.props.roles.userId,
            Role: checkedState[0],
            RoleId: id,
          };
          if (this.props.roles.roleId !== id) {
            this.props.handleSelectedRole(this.props.roles, objectData);
          }
        }
      } else {
        let member = this.getMemberById(id);
        if (this.props.selectAssigneeList)
          this.props.selectAssigneeList(
            [member],
            "no",
            this.props.followUpAction
          );
        this.setState({
          checked: checkedState,
          idList: checkedState.length ? [id] : [],
          list: checkedState.length && member ? [member] : [],
          isToggled: true,
          // open:false 
        });
      }
      if (
        this.props.viewType === "risk" &&
        this.props.isLikelihood &&
        this.props.selectedIdsList /** this logic is for bulk update likelihood */
      ) {
        const type = helper.RETURN_BULKACTIONTYPES(this.props.keyType);
        let data = {
          type,
        };
        data[this.props.keyType] = value;
        data.riskIds = this.props.selectedIdsList;
        this.props.BulkUpdate(data);
      }
      if (this.props.isLikelihood && this.props.viewType === "risk") {
        Risk.likelihood = checkedState[0]; /** when user select likelihood from risk list then update risk */
        this.props.isUpdated(Risk, () => { });
      }
      //this.setState({ open: false })
    }
    this.setState({ open: false });
  };
  moreMembersMenuClose = (event) => {
    this.setState({ moreMembersOpen: false });
  };
  moreMembersOpen = () => {
    this.setState({ moreMembersOpen: true });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Select Task":
        value = "common.bulk-action.selectTask";
        break;
      case "Add Assignee":
        value = "common.bulk-action.addAssignee";
        break;
      case "Add Participants":
        value = "common.bulk-action.addParticipant";
        break;
      case "Select Risk Owner":
        value = "common.action.risk.label";
        break;
      case "Set Status":
        value = "common.action.risk.label2";
        break;
      case "Set Impact":
        value = "common.action.risk.label3";
        break;
      case "Set Likelihood":
        value = "common.action.risk.label4";
        break;
      case "Select Likelihood":
        value = "risk.common.likelihood.placeholder";
        break;
      default:
        return value;
    }
    return value;
  }
  getTranslatedLabel = (type) => {
    let trans = { id: type, defaultMessage: type };
    switch (type) {
      case "Bug":
        trans = { id: "issue.common.issue-type.drop-down.bug", defaultMessage: type };
        break;
      case "Improvement":
        trans = { id: "issue.common.issue-type.drop-down.improvement", defaultMessage: type };
        break;
      case "Feature":
        trans = { id: "issue.common.issue-type.drop-down.feature", defaultMessage: type };
        break;
    }
    return type == null || type == undefined ? "" : this.props.intl.formatMessage(trans);
  }
  render() {
    const {
      classes,
      theme,
      listType,
      isType,
      iconSize,
      isCategory,
      isLabel,
      buttonText,
      isLikelihood,
      isBulk,
      buttonStyles,
      viewType,
      addButton,
      hideButton,
      currentMeeting,
      isFollowUp,
      permission,
      MenuData,
      permissionKey,
      isRoles,
      permissionSettingsState,
      isArchivedSelected,
      mode,
      disabled,
    } = this.props;
    const { open, checked, anchorEl, idList, moreMembersOpen } = this.state;
    let ddData = [],
      ids = [],
      allData = [];
    let headingText = "";
    let iconType = "",
      defaultButtonText = "Select",
      menuListType = "search";

    if (listType === "assignee") {
      iconType = "avatar";
      headingText =
        viewType === "risk"
          ? "Add Owner"
          : viewType === "meeting" && !isFollowUp
            ? "Add Attendee"
            : "Add Assignee";
      ddData = this.state.list;
      allData = this.state.allMembers;

      if (isFollowUp) {
        ids =
          currentMeeting && currentMeeting.attendees
            ? currentMeeting.attendees.map((x) => x.attendeeId)
            : [];
        allData = allData.filter((x) => {
          return ids.indexOf(x.id) >= 0;
        });
      }
    } else if (isRoles) {
      ddData =
        permissionSettingsState.data &&
          permissionSettingsState.data.defaultRoles &&
          permissionSettingsState.data.defaultRoles.length
          ? permissionSettingsState.data.defaultRoles
            .filter((x) => x.roleId !== "001")
            .map((x) => {
              return { id: x.roleId, value: { name: x.roleName } };
            })
          : [];

      allData = ddData;
      headingText = "Select Role";
      defaultButtonText =
        checked.length >= 1 && checked[0] ? checked[0] : "Member";
      menuListType = "search";
    } else if (isType) {
      let type = ["Bug", "Feature", "Improvement"];
      ddData = type.map((x, i) => {
        return { id: i, value: { name: x }, label: this.getTranslatedLabel(x) };
      });
      allData = ddData;
      headingText = this.props.intl.formatMessage({ id: "issue.common.issue-type.placeholder", defaultMessage: "Select Type" });
      defaultButtonText =
        checked.length >= 1 && this.getTranslatedLabel(checked[0]) ? this.getTranslatedLabel(checked[0]) : this.props.intl.formatMessage({ id: "issue.common.issue-type.placeholder", defaultMessage: "Select Type" });
      menuListType = "simpleList";
    } else if (isLikelihood) {
      let likelihood = ["0-25", "26-50", "51-75", "76-100"];
      ddData = likelihood.map((x, i) => {
        return { id: i, value: { name: x } };
      });
      allData = ddData;
      headingText = this.props.intl.formatMessage({ id: "risk.common.likelihood.placeholder", defaultMessage: "Select Likelihood" });
      defaultButtonText =
        checked.length >= 1 && checked[0]
          ? checked[0] + " %"
          : this.props.intl.formatMessage({ id: "risk.common.likelihood.placeholder", defaultMessage: "Select Likelihood" })
      menuListType = "simpleList";
    } else if (isCategory) {
      let category = ["Task Board", "Website Development", "Meeting Board"];
      ddData = category.map((x, i) => {
        return { id: i, value: { name: x } };
      });
      allData = ddData;
      headingText = "Select Category";
      defaultButtonText =
        checked.length >= 1 && checked[0] ? checked[0] : "Select Category";
      menuListType = "simpleList";
    } else if (isLabel) {
      let label = [
        "Black Box Testing",
        "Bug Fixing",
        "Content Writing",
        "User Interface Design",
        "Website Development",
      ];
      ddData = label.map((x, i) => {
        return { id: i, value: { name: x } };
      });
      allData = ddData;
      headingText = "Select Label";
      defaultButtonText =
        checked.length >= 1 && checked[0] ? checked[0] : "Select Label";
    } else {
      ddData =
        this.props.tasksState.data && this.props.tasksState.data.length
          ? this.props.tasksState.data.map((x) => {
            return { id: x.taskId, value: { name: x.taskTitle } };
          })
          : [];

      allData = ddData;
      headingText = viewType === "meeting" ? "Select Task" : "Select Tasks";
      defaultButtonText =
        (checked.length || idList.length) > 1
          ? "Multiple Tasks"
          : checked.length === 1
            ? checked[0]
            : viewType === "meeting"
              ? "Select Task"
              : "Select Tasks";
      menuListType = "search";
    }
    const searchQuery = ["id", "value.name"];
    const filteredData = allData.filter(
      createFilter(this.state.searchTerm, searchQuery)
    );
    const GenerateList = () => {
      return ddData.slice(0, 3).map((Assignee, i, t) => {
        return (
          <li key={i} style={{ zIndex: i, marginLeft: !i == 0 ? -5 : null }}>
            {
              <CustomAvatar
                otherMember={{
                  imageUrl: Assignee.value.avatar,
                  fullName: Assignee.value.fullName,
                  lastName: "",
                  email: Assignee.value.email,
                  isOnline: Assignee.value.isOnline,
                  isOwner: Assignee.value.isOwner,
                }}
                size={iconSize ? iconSize : "small"}
              />
            }
          </li>
        );
      });
    };

    const RenderIcon = (data, classes, length) => {
      return (
        <CustomAvatar
          otherMember={{
            imageUrl: data.avatar,
            fullName: data.fullName,
            lastName: "",
            email: data.email,
            isOnline: data.isOnline,
            isOwner: data.isOwner,
          }}
          size="xsmall"
        />
      );
    };
    let canEdit = true;
    if (viewType === "issue" && permissionKey) {
      canEdit = getEditPermissionsWithArchieve(
        MenuData,
        permission,
        permissionKey
      );
    }
    if (viewType === "risk" && permissionKey) {
      canEdit = getRisksEditPermissionsWithArchieve(
        MenuData,
        permission,
        permissionKey
      );
    }
    return (
      <>
        {listType == "assignee" && !isBulk ? (
          <ul
            className="AssigneeAvatarList"
            onClick={(e) => e.stopPropagation()}
          >
            {mode != "parentList" && <GenerateList />}
            {ddData.length > 3 ? (
              <li>
                <IconButton
                  className={classes.TotalAssignee}
                  onMouseEnter={this.moreMembersOpen}
                  // onMouseLeave={this.moreMembersMenuClose}
                  buttonRef={(node) => {
                    this.moreAssigneeCount = node;
                  }}
                >
                  +{ddData.length - 3}
                </IconButton>
              </li>
            ) : (
              ""
            )}
            <li className="AssigneeListBtnCnt">
              {viewType !== "project" && canEdit && !hideButton ? (
                <IconButton
                  classes={{ root: classes.addAssigneeBtn }}
                  disabled={isArchivedSelected || disabled}
                  onClick={this.handleClick}
                  style={buttonStyles}
                >
                  <AddIcon
                    classes={{ root: classes.addAssigneeBtnIcon }}
                    htmlColor={theme.palette.text.dark}
                  />
                </IconButton>
              ) : null}
            </li>
          </ul>
        ) : isBulk ? (
          <Button
            variant="outlined"
            style={buttonStyles}
            classes={{ outlined: classes.BulkActionBtn }}
            disabled={isArchivedSelected}
            onClick={(event) => {
              this.handleClick(event);
            }}
            buttonRef={(node) => {
              this.anchorEl = node;
            }}
          >
            <FormattedMessage
              id={this.getTranslatedId(buttonText)}
              defaultMessage={buttonText}
            />
          </Button>
        ) : addButton ? (
          <CustomButton
            btnType="dashedIcon"
            buttonRef={(node) => {
              this.anchorEl = node;
            }}
            disabled={isArchivedSelected}
            variant="contained"
            onClick={(event) => {
              this.handleClick(event);
            }}
          >
            <AddIcon
              htmlColor={theme.palette.primary.light}
              fontSize="small"
            />
            Add Task
          </CustomButton>
        ) : (
          <>
            {canEdit ? (
              <CustomButton
                buttonRef={(node) => {
                  this.anchorEl = node;
                }}
                variant="text"
                btnType="plain"
                onClick={(event) => {
                  this.handleClick(event);
                }}
                disabled={isArchivedSelected}
              >
                {checked.length == 0 || checked[0] == null ? (
                  <u>{defaultButtonText}</u>
                ) : (
                  defaultButtonText
                )}
              </CustomButton>
            ) : (
              <Typography variant="body2" align="center">
                {checked && checked.length ? (
                  checked.length === 1 ? (
                    checked[0] ? (
                      checked[0]
                    ) : viewType === "risk" ? (
                      <FormattedMessage
                        id="risk.common.likelihood.placeholder"
                        defaultMessage="Select Likelihood"
                      />
                    ) : (
                      <FormattedMessage
                        id="project.gant-view.task-dialog.form.type.label"
                        defaultMessage="Select Type"
                      />
                    )
                  ) : (
                    <FormattedMessage
                      id="task.multiple.label"
                      defaultMessage="Multiple Tasks"
                    />
                  )
                ) : viewType === "meeting" ? (
                  <FormattedMessage
                    id="risk.common.task.placeholder"
                    defaultMessage="Select Task"
                  />
                ) : (
                  <FormattedMessage
                    id="common.select-tasks.label"
                    defaultMessage={"Select Tasks"}
                  />
                )}
              </Typography>
            )}
          </>
        )}

        <DropdownMenu
          open={open}
          closeAction={this.handleClose}
          anchorEl={anchorEl}
          size="small"
          placement="top-start"
          disablePortal={this.props.disablePortal}
        >
          <div>
            <GenerateMenu
              data={filteredData}
              isLikelihood={this.props.isLikelihood}
              handleToggle={this.handleToggle}
              listType={menuListType}
              checked={checked}
              idList={idList}
              iconType={iconType}
              headingText={headingText}
              renderIcon={RenderIcon}
              isType={isType}
              searchQuery={searchQuery}
              searchAction={this.searchUpdated}
            />
          </div>
        </DropdownMenu>
        {ddData.length > 3 ? (
          <MoreMembersMenu
            open={moreMembersOpen}
            closeAction={this.moreMembersMenuClose}
            assigneeList={ddData}
            anchorEl={this.moreAssigneeCount}
          />
        ) : null}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    issuesState: state.issues,
    profileState: state.profile,
    tasksState: state.tasks,
    permissionSettingsState: state.permissionSettings,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(readyDropdownStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(MemberListMenu);
