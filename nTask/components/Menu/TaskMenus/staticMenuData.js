const ddData = (color,intl) => {
  return {
    priority: [
      { label: getTranslatedLabel(intl, "Critical"), name: "Critical", color: color.issuePriority.Critical },
      { label: getTranslatedLabel(intl, "High"), name: "High", color: color.issuePriority.High },
      { label: getTranslatedLabel(intl, "Medium"), name: "Medium", color: color.issuePriority.Medium },
      { label: getTranslatedLabel(intl, "Low"), name: "Low", color: color.issuePriority.Low }
    ],
    severity: [
      { label: getTranslatedLabel(intl, "Minor"), name: "Minor", color: color.severity.minor },
      { label: getTranslatedLabel(intl, "Moderate"), name: "Moderate", color: color.severity.moderate },
      { label: getTranslatedLabel(intl, "Major"), name: "Major", color: color.severity.major },
      { label: getTranslatedLabel(intl, "Critical"), name: "Critical", color: color.severity.critical }
    ],
    impact: [
      { label: getTranslatedLabel(intl, "Minor"), name: "Minor", color: color.riskImpact.Minor },
      { label: getTranslatedLabel(intl, "Moderate"), name: "Moderate", color: color.riskImpact.Moderate },
      { label: getTranslatedLabel(intl, "Major"), name: "Major", color: color.riskImpact.Major },
      { label: getTranslatedLabel(intl, "Critical"), name: "Critical", color: color.riskImpact.Critical }
    ],
    issueStatus: [
      { label: getTranslatedLabel(intl, "Open"), name: "Open", color: color.issueStatus.Open },
      { label: getTranslatedLabel(intl, "Closed"), name: "Closed", color: color.issueStatus.Closed }
    ],
    riskStatus: [
      { label: getTranslatedLabel(intl, "Identified"), name: "Identified", color: color.riskStatus.Identified },
      { label: getTranslatedLabel(intl, "In Review"), name: "In Review", color: color.riskStatus.InReview },
      { label: getTranslatedLabel(intl, "Agreed"), name: "Agreed", color: color.riskStatus.Agreed },
      { label: getTranslatedLabel(intl, "Rejected"), name: "Rejected", color: color.riskStatus.Rejected }
    ],
    meetingStatus: [
      { name: "Upcoming", color: color.meetingStatus.Upcoming },
      { name: "In Review", color: color.meetingStatus.InReview },
      { name: "Published", color: color.meetingStatus.Published },
      { name: "Cancelled", color: color.meetingStatus.Cancelled },
      { name: "Overdue", color: color.meetingStatus.OverDue }
    ]
  };
};
function getTranslatedLabel(intl,name)  {
  var trans = {};
  switch(name) {
     case "Open":
      trans = {id:"issue.common.status.drop-down.open", defaultMessage: name};
      break;
     case "Closed":
      trans = {id:"issue.common.status.drop-down.close", defaultMessage: name};
        break;
     case "Critical":
      trans = {id:"issue.common.severity.dropdown.critical", defaultMessage: name};
      break;
     case "Major":
      trans = {id:"issue.common.severity.dropdown.major", defaultMessage: name};
      break;   
     case "Minor":
      trans = {id:"issue.common.severity.dropdown.minor", defaultMessage: name};
      break;
     case "Moderate":
      trans = {id:"issue.common.severity.dropdown.moderate", defaultMessage: name};
      break;
      case "High":
      trans = {id:"issue.common.priority.dropdown.high", defaultMessage: name};
      break;
      case "Low":
      trans = {id:"issue.common.priority.dropdown.low", defaultMessage: name};
      break;
      case "Medium":
      trans = {id:"issue.common.priority.dropdown.medium", defaultMessage: name};
      break;

      case "Agreed":
      trans = {id:"risk.common.status.dropdown.agreed", defaultMessage: name};
      break;
      case "Identified":
      trans = {id:"risk.common.status.dropdown.identified", defaultMessage: name};
      break;
      case "In Review":
      trans = {id:"risk.common.status.dropdown.in-review", defaultMessage: name};
      break;
      case "Rejected":
      trans = {id:"risk.common.status.dropdown.rejected", defaultMessage: name};
      break;
  }
  return intl.formatMessage(trans);
};
export default ddData;
