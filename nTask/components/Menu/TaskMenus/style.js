const readyDropdownStyles = theme => ({
    AssigneeAvatar: {
        border: `2px solid ${theme.palette.common.white}`
      },
      TotalAssignee: {
        background: theme.palette.secondary.lightBlue,
        color: theme.palette.secondary.main,
        width: 40,
        height: 40,
        fontSize: "17px !important"
      },
      severityIcon:{
        fontSize: "14px !important",
        marginRight: 6
      },
      dropdownButton: {
        fontSize: "10px !important",
        fontWeight: theme.typography.fontWeightLight,
        padding: "0 10px",
        minHeight: 20,
        color: theme.palette.common.white,
        textTransform: "uppercase",
        borderRadius: 2,
        minWidth: 80,
        letterSpacing: "1px"
      },
      addAssigneeBtn:{
        background: theme.palette.common.white,
        border: `1px dashed ${theme.palette.border.lightBorder}`,
        padding: 7,
        marginLeft: 5,
        borderRadius: "100%"
      },
      addAssigneeBtnIcon: {
        fontSize: "20px !important"
      },
      emailIcon:{
        fontSize: "20px !important"
      },
      piriorityIcon:{
        fontSize: "16px !important"
      },
      // Select Menu Styles
      selectFormControl: {
        width: "100%",
        marginTop: 22
      },
        defaultInputLabel: {
        transform: "translate(2px, -17px) scale(1)",
        color: theme.palette.text.primary,
        fontSize: "12px !important",
        fontWeight: theme.typography.fontWeightRegular
      },
       outlinedSelect: {
        borderRadius: 4,
        background: theme.palette.common.white,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        "&:hover": {
          border: `1px solid ${theme.palette.border.lightBorder}`
        }
      },
       outlinedSelectCnt: {
        "&:hover $outlinedSelect": {
          border: `1px solid ${theme.palette.border.lightBorder} !important`
        }
      },
        singleSelectCmp:{
        borderRadius: 4,
        padding: "12px 28px 12px 12px",
        color: theme.palette.text.secondary,
        fontSize: theme.typography.pxToRem(16),
        "&:hover": {
          border: "none"
        },
        "&:focus": {
          background: "transparent"
        }
      },
      selectIcon: {
        color: theme.palette.secondary.light,
        right: 11,
        color: "rgba(191, 191, 191, 1)",
        fontSize: "31px !important",
        top: "calc(50% - 15px)"
      },
      BulkActionBtn: { // Bulk Action Button style
        border: `1px solid ${theme.palette.border.lightBorder}`,
        textTransform: "capitalize",
        fontWeight: theme.typography.fontWeightLight,
        color: theme.palette.text.primary,
        background: theme.palette.background.default
      },
})

export default readyDropdownStyles;