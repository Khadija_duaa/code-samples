import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { UpdateTask, FetchTasksInfo } from "../../../redux/actions/tasks";
import Avatar from "@material-ui/core/Avatar";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./style";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../Buttons/IconButton";
import SelectionMenu from "../../Menu/SelectionMenu";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import DoneIcon from "@material-ui/icons/Done";
import FlagIcon from "@material-ui/icons/Flag";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import combineStyles from "../../../utils/mergeStyles";

class LabelMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      name: [],
      selectedStatus: false,
      isChanged: false,
      checked: ["Bug"],
      selectedColor: props.theme.palette.status.notStarted
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  componentDidMount() {
    this.setState({
      checked: [this.props.label],
      isChanged: true
    });
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    return { checked: [nextProps.label], isChanged: true };
  }

  handleClose(event) {
    this.setState({ open: false });
  }
  handleClick(event, placement) {
    event.stopPropagation();
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }
  handleToggle = (event, value, color) => {
    event.stopPropagation();
    let ischeck = -1;
    let data = this.props.MenuData;
    this.handleClose();
    data.label = value.name;
    ischeck = this.state.checked.indexOf(value.name);
    if (ischeck < 0) {
      this.props.isUpdated(data);
    }
    this.setState({
      checked: [value.name],
      selectedColor: value.color,
      isChanged: false
    });
  };

  render() {
    const { classes, theme, label } = this.props;
    const { open, placement, isChanged } = this.state;
    const statusColor = theme.palette.status;
    const ddData = [
      { name: "Black Box Testing", color: statusColor.completed },
      { name: "Bug Fixing", color: statusColor.review },
      { name: "Content Writing", color: statusColor.inProgress },
      { name: "User Interface Design", color: statusColor.notStarted },
      { name: "Website Development", color: statusColor.notStarted }
    ];
    let labelColor = this.state.selectedColor;
    if (label && isChanged) {
      labelColor = ddData.find(x => x.name === label).color;
    }
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomIconButton
            btnType="filledWhite"
            id="SetStatus"
            style={this.props.style}
            onClick={event => {
              this.handleClick(event, "bottom-start");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
          >
            <FlagIcon
              classes={{ root: classes.addAssigneeBtn }}
              htmlColor={labelColor}
            />
          </CustomIconButton>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            anchorRef={this.anchorEl}
            list={
              <List>
                <ListItem
                  disableRipple={true}
                  classes={{ root: classes.menuHeadingItem }}
                >
                  <ListItemText
                    primary="Set Label"
                    classes={{ primary: classes.menuHeadingItemText }}
                  />
                </ListItem>
                {ddData.map(value => (
                  <ListItem
                    key={value.name}
                    button
                    disableRipple={true}
                    onClick={event => this.handleToggle(event, value)}
                    classes={{ selected: classes.statusMenuItemSelected }}
                  >
                    <FlagIcon
                      htmlColor={value.color}
                      classes={{ root: classes.flagIcon }}
                    />
                    <ListItemText
                      primary={value.name}
                      classes={{
                        primary: classes.statusItemText
                      }}
                    />
                    <ListItemSecondaryAction>
                      <Checkbox
                        checkedIcon={
                          <DoneIcon htmlColor={theme.palette.primary.light} />
                        }
                        onChange={event => this.handleToggle(event, value)}
                        checked={this.state.checked.indexOf(value.name) !== -1}
                        icon={false}
                      />
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}
export default compose(
  withRouter,
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true
  }),
  connect(
    null,
    { UpdateTask, FetchTasksInfo }
  )
)(LabelMenu);
