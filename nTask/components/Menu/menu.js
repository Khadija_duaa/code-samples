import React, { Component } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import menuStyles from "../../assets/jss/components/menu";
import buttonStyles from "../../assets/jss/components/button";
import combineStyles from "../../utils/mergeStyles";

class PlainMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }
  render() {
    const { classes, theme, menuType } = this.props;
    const menuClasses =
      menuType == "workspace"
        ? classes.popper
        : menuType == "dashboard"
          ? classes.plainPopperDashboard
          : classes.plainPopper;
    return (
      <React.Fragment>
        <Popper
          style={this.props.style}
          open={this.props.open}
          anchorEl={this.props.anchorRef}
          placement={this.props.placement}
          onClick={e=>e.stopPropagation()}
          className={menuClasses}
          modifiers={{
            offset: {
              enabled: true,
              offset: this.props.offset || 0
            },
            arrow: {
              enabled: false
            }
          }}
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "left bottom"
              }}
            >
              <Paper
                classes={{
                  root: classes.paperRoot
                }}
              >
                <ClickAwayListener onClickAway={this.props.closeAction}>
                  {this.props.list ? this.props.list : this.props.children}
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </React.Fragment>
    );
  }
}


export default withStyles(combineStyles(menuStyles, buttonStyles), {
  withTheme: true
})(PlainMenu);
