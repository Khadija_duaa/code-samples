import React, { Component } from "react";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { BulkUpdateTask } from "../../redux/actions/tasks";
import { withStyles, withTheme } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import menuStyles from "../../assets/jss/components/menu";
import SearchInput, { createFilter } from "react-search-input";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
import CustomAvatar from "../Avatar/Avatar";
import { Scrollbars } from "react-custom-scrollbars";
import helper from "./../../helper/index";
import { injectIntl,FormattedMessage } from "react-intl";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

class SearchDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      searchTerm: "",
      checked: [],
      selectedAssignee: "",
      idList: [],
      checkAssignee: false,
      checkBulkAssignee: false,
      taskIds: [],
    };
    this.searchUpdated = this.searchUpdated.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }

  componentDidMount() {
    if (this.props.isProject) {
      let selectedProject = this.props.selectedProject;
      if (selectedProject) {
        this.props.data.map((x) => {
          if (x.value.name === selectedProject) {
            this.setState({
              checked: selectedProject,
              idList: x.id,
            });
          }
        });
      }
    } else if (this.props.isTask) {
      let data = this.props.TaskData;
      if (data) {
        this.props.data.map((x) => {
          if (x.id === data.linkedTasks[0]) {
            this.setState({
              checked: [x.value.name],
            });
          }
        });
      }
    } else if (this.props.checkAssignee) {
      const checked = this.getSelectedAssigness();
      this.setState({
        checked: checked.checkedData,
        idList: checked.idList,
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isProject) {
      if (this.props.selectedProject !== prevProps.selectedProject) {
        let selectedProject = this.props.selectedProject;
        if (selectedProject && selectedProject !== "Select Project") {
          this.props.data.map((x) => {
            if (x.value.name === selectedProject) {
              this.setState({
                checked: selectedProject,
                idList: x.id,
              });
            }
          });
        } else {
          this.setState({
            checked: [],
            idList: [],
          });
        }
      }
    } else {
      if (
        JSON.stringify(prevProps.newAssigneeList) !==
        JSON.stringify(this.props.newAssigneeList)
      ) {
        const checked = this.getSelectedAssigness();
        this.setState({
          checked: checked.checkedData,
          idList: checked.idList,
        });
      } else if (
        prevProps.tasksState.data.length !== this.props.tasksState.data.length
      ) {
        this.setState({ checked: [] }, () => {
          this.componentDidMount();
        });
      }
    }
  }

  getSelectedAssigness = () => {
    let checkedData = [],
      idList = [];
    let ids = this.props.newAssigneeList.map((x) => x.id);
    this.props.data.map((x) => {
      if (ids.indexOf(x.id) >= 0) {
        checkedData.push(x.value.name);
        idList.push(x.id);
      }
    });
    return { checkedData, idList };
  };

  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }

  handleToggle = (event, value, id, avatar, email) => {
    event.stopPropagation();
    this.setState({ checkAssignee: false, checkBulkAssignee: false });
    const { checked, idList } = this.state;
    // const currentIndex = checked.indexOf(value);
    let newChecked = [...checked];
    let ids = [...idList];
    const currentIndex = id ? idList.indexOf(id) : checked.indexOf(value);
    const checkedState = currentIndex === -1 ? [value] : [];
    if (this.props.checkedType === "multi") {
      if (currentIndex === -1) {
        newChecked.push(value);
        ids.push(id);
      } else {
        newChecked = newChecked.filter((x) => x !== value);
        ids = ids.filter((x) => x !== id);
      }
      this.setState({
        checked: newChecked,
        idList: ids,
      });
    } else {
      this.setState({
        checked: checkedState,
      });
    }

    if (this.props.checkAssignee) {
      this.setState({
        checkAssignee: true,
      });
    }

    if (this.props.isTask) {
      let data = this.props.TaskData;
      this.props.closeAction();
      if (this.props.open) {
        this.setState({ checkAssignee: false, checkBulkAssignee: false });
        if (data.linkedTasks[0] && data.linkedTasks[0] === id) {
          this.props.selectAction("Select Task");
          data.linkedTasks = [];
        } else if (id === this.state.selectedId) {
          this.props.selectAction(value);
          this.setState({ selectedId: id });

          if (!this.state.checked.length) {
            data.linkedTasks = [id];
          } else {
            this.props.selectAction("Select Task");
            data.linkedTasks = [];
          }
        } else {
          this.props.selectAction(value);
          data.linkedTasks = [id];
          this.setState({ selectedId: id });
        }
        this.props.isUpdated(data);
        this.props.closeAction();
      }
    }

    if (this.props.isProject) {
      if (this.props.open) {
        value = this.state.checked.indexOf(value) > -1 ? "" : value;
        this.setState({
          checked: value ? [value] : [],
          idList: value ? [id] : [],
        });

        if (this.props.autoUpdate) {
          this.props.selectAction(value ? value : "Select Project");
        }
      }
    }

    if (this.props.isProjectBulk) {
      this.props.closeAction();
      if (this.props.open) {
        const type = helper.RETURN_BULKACTIONTYPES("AddProject");
        let data = {
          type,
          taskIds: this.props.taskIds,
          projectId: id,
        };

        this.props.BulkUpdateTask(data, (response) => {
          this.props.closeAction();
          this.props.isBulkUpdated();
          this.setState({ checked: [] });
        });
      }
    }

    if (this.props.isTaskBulk) {
      //
      this.props.closeAction();
      if (this.props.open) {
        const type = helper.RETURN_BULKACTIONTYPES("AddTask");
        let data = {
          type,
        };
        if (this.props.type === "issue") {
          data.issueIds = this.props.selectedIds;
          data.linkedTasks = [id];
        } else {
          data.taskIds = this.props.selectedIds;
          data.taskId = id;
        }
        this.props.handleUpdateIssue(data);
        this.props.closeAction();
        this.setState({ checked: [] });
      }
    }

    if (this.props.isAssigneeBulk) {
      this.setState({ taskIds: this.props.taskIds, checkBulkAssignee: true });
    }

    if (this.props.autoUpdate) {
      if (this.props.updateAssignees) {
        this.props.updateAssignees({
          id: id,
          value: {
            name: value,
            avatar: avatar,
            email: email,
            fullName: value,
          },
        });
      }
    }
    if (this.props.isProjectSelect) {
      this.props.selectAction(event, value, id, avatar, email);
    }
  };

  render() {
    const {
      classes,
      data,
      searchQuery,
      open,
      closeAction,
      title,
      avatar,
      placement,
      searchInputPlaceholder,
      backOption,
      backOptionTxt,
      handleClickBackBtn,
      scrollHeight,
      pooperProps = {},
    } = this.props;
    const { idList, checked, searchTerm } = this.state;

    const filteredData = data.filter(createFilter(searchTerm, searchQuery));

    return (
      <React.Fragment>
        <Popper
          open={open}
          className={classes.plainPopperDashboard}
          anchorEl={this.props.anchorRef}
          placement={placement}
          transition
          disablePortal={true}
          {...pooperProps}
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "left bottom",
              }}
            >
              <Paper
                classes={{
                  root: classes.paperRoot,
                }}
              >
                <span className={`${classes.arrow} x-arrow`} />
                <ClickAwayListener
                  onClickAway={closeAction.bind(null, this.state)}
                >
                  <div
                    onClick={(event) => {
                      event.stopPropagation();
                    }}
                  >
                    <List>
                      {backOption && (
                        <ListItem
                          onClick={handleClickBackBtn}
                          disableRipple={true}
                          classes={{ root: classes.menuHeadingBackItem }}
                        >
                          <ArrowBackIcon fontSize="small" />
                          <ListItemText
                            primary={backOptionTxt}
                            classes={{
                              primary: classes.menuHeadingItemBackText,
                              root: classes.menuHeadingItemBackTextRoot,
                            }}
                          />
                        </ListItem>
                      )}
                      <ListItem disableRipple={true}>
                        <SearchInput
                          className="HtmlInput"
                          onChange={this.searchUpdated}
                          maxLength="80"
                          placeholder={
                            searchInputPlaceholder ? searchInputPlaceholder :
                            this.props.intl.formatMessage({ id: "common.search.label", defaultMessage: "Search" })
                        }
                        />
                      </ListItem>
                      <ListItem
                        disableRipple={true}
                        classes={{ root: classes.menuHeadingItem }}
                      >
                        <ListItemText
                          primary={title}
                          classes={{ primary: classes.menuHeadingItemText }}
                        />
                      </ListItem>
                      <Scrollbars autoHide={false} style={{ height: scrollHeight }}>
                        {/* {filteredData.slice(0, 20).map((data) => { */}
                        {filteredData.map((data) => {
                          return (
                            <ListItem
                              key={data.id}
                              onClick={(event) =>
                                this.handleToggle(
                                  event,
                                  data.value.name,
                                  data.id,
                                  data.value.avatar,
                                  data.value.email
                                )
                              }
                              className={`${avatar
                                ? classes.AvatarMenuItem
                                : classes.MenuItem
                                } ${(idList && idList.length
                                  ? idList.indexOf(data.id) !== -1
                                  : checked.indexOf(data.value.name) !== -1)
                                  ? classes.selectedValue
                                  : ""
                                }`}
                              button
                              disableRipple={true}
                            >
                              {data.value.avatar || data.value.email ? (
                                <CustomAvatar
                                  otherMember={{
                                    imageUrl: data.value.avatar,
                                    fullName: data.value.fullName,
                                    lastName: "",
                                    email: data.value.email,
                                    isOnline: data.value.isOnline,
                                    isOwner: data.value.isOwner,
                                  }}
                                  size="xsmall"
                                />
                              ) : null}
                              <ListItemText
                                primary={
                                  <>
                                    {data.value.name}
                                    <br />
                                    {data.isActiveMember === null ? (
                                      <Typography variant="caption">
                                        Invite sent
                                      </Typography>
                                    ) : null}
                                  </>
                                }
                                classes={{ primary: classes.MenuItemText }}
                              />
                            </ListItem>
                          );
                        })}
                      </Scrollbars>
                    </List>
                  </div>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </React.Fragment>
    );
  }
}
SearchDropdown.defaultProps = {
  placement: "bottom-start",
  searchInputPlaceholder: null,
  backOption: false,
  backOptionTxt: "BACK",
  pooperProps: {},
  scrollHeight : 200,
  handleClickBackBtn: () => { },
};

const mapStateToProps = (state) => {
  return {
    tasksState: state.tasks,
  };
};

export default compose(
  withRouter,injectIntl,
  withStyles(menuStyles, { withTheme: true }),
  connect(mapStateToProps, { BulkUpdateTask })
)(SearchDropdown);
