import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import menuStyles from "../../assets/jss/components/menu";
import SelectionMenu from "./SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../utils/mergeStyles";
import CustomAvatar from "../Avatar/Avatar";
import { Scrollbars } from "react-custom-scrollbars";
import { FormattedMessage } from "react-intl";

class MoreMembersMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
    };
    this.handleHover = this.handleHover.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
  }
  handleDialogOpen = () => {
    this.setState({ renameRoleDialogOpen: true });
  };
  handleDialogClose = () => {
    this.setState({ renameRoleDialogOpen: false });
  };
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }

  handleHover(event, placement) {
    const { currentTarget } = event;
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  render() {
    const {
      classes,
      theme,
      assigneeList,
      open,
      anchorEl,
      closeAction,
    } = this.props;
    const { placement } = this.state;

    return (
      <Fragment>
        <SelectionMenu
          open={open}
          closeAction={closeAction}
          placement={placement}
          style={{ width: 250 }}
          anchorRef={anchorEl}
          list={
            <Scrollbars
              autoHide={false}
              autoHeightMax={200}
              autoHeight={true}
              style={{ height: 220 }}
            >
              <List disablePadding={true}>
                <ListItem disableRipple className={classes.headingItem}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="common.total.message"
                        defaultMessage={`Total ${assigneeList.length} Assignee`}
                        values={{ assigneeList: assigneeList.length }}
                      />
                    }
                    classes={{ root: classes.itemTextRootFollowUp, primary: classes.headingText }}
                    button={false}
                  />
                </ListItem>
                {assigneeList.map((assignee) => {
                  return (
                    <ListItem
                      button={false}
                      key={assignee.id}
                      disableRipple
                      className={classes.listItemAvatar}
                    >
                      <CustomAvatar
                        otherMember={{
                          imageUrl: assignee.value.avatar,
                          fullName: assignee.value.fullName,
                          lastName: "",
                          email: assignee.value.email,
                          isOnline: assignee.value.isOnline,
                          isOwner: assignee.value.isOwner,
                        }}
                        size="xsmall"
                      />
                      <ListItemText
                        primary={assignee.value.fullName}
                        classes={{
                          root: classes.itemTextRootFollowUp,
                          primary: classes.statusItemText,

                        }}
                      />
                    </ListItem>
                  );
                })}
              </List>
            </Scrollbars>
          }
        />
      </Fragment>
    );
  }
}

export default withStyles(menuStyles, {
  withTheme: true,
})(MoreMembersMenu);
