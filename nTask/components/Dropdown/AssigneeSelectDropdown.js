import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import { compose } from "redux";
import { connect } from "react-redux";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Close";
import SearchInput, { createFilter } from "react-search-input";
import { Scrollbars } from "react-custom-scrollbars";
import dropdownStyles from "./style";
import CustomAvatar from "../Avatar/Avatar";
import DropdownMenu from "./DropdownMenu";
import { UpdateFollowupAction } from "../../redux/actions/meetings";
import { generateActiveMembers } from "../../helper/getActiveMembers";
import CustomButton from "../Buttons/CustomButton";
import { FormattedMessage,injectIntl} from 'react-intl';

class AssigneeSelectDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      assignedTo: [],
      assigneeList: [],
      searchTerm: "",
    };
  }

  handleClick = (e) => {
    e.stopPropagation();
    this.setState({ open: true });
  };

  componentDidUpdate(prevProps, prevState) {
    const { members, assignedTo } = this.props;
    if (JSON.stringify(members) !== JSON.stringify(prevProps.members)) {
      this.setState({ assigneeList: this.compareMembersList(assignedTo) });
    }
    if (JSON.stringify(assignedTo) !== JSON.stringify(prevProps.assignedTo)) {
      this.setState({ assignedTo, assigneeList: this.compareMembersList(assignedTo) });
    }
  }

  compareMembersList = (assigneeList = []) => {
    const members = generateActiveMembers();
    // Filtering Members list by comparing it with members listed in assigned To section in dropdown
    return members.filter(member => {
      const assigneeListFilter =
        assigneeList.find(selectedMember => {
          return selectedMember.userId == member.userId;
        }) || {};

      return assigneeListFilter.userId !== member.userId;
    });
  };

  handleMemberSelect = (event, member) => {
    // This function handles selection of member
    const { assignedTo } = this.state;
    const { updateAction, obj } = this.props;
    const newAssignedToList = [member];

    this.setState(
      {
        assignedTo: newAssignedToList,
        assigneeList: this.compareMembersList(newAssignedToList),
        open: false,
        searchTerm: "",
      },
      () => {
        // Updates Assignee on respective document(task/issue/risk etc..)
        if (updateAction) updateAction(newAssignedToList, obj, true);
      }
    );
  };

  handleClose = event => {
    if (document.getElementById("assigneeDropdown").contains(event.target)) {
      return;
    }

    this.setState({ open: false, searchTerm: "" });
  };

  componentDidMount() {
    const { assignedTo } = this.props;

    // Updating state with the total list of members whom user can assign the document
    this.setState({
      assigneeList: this.compareMembersList(assignedTo),
      assignedTo,
    });
  }

  searchUpdated = term => {
    this.setState({ searchTerm: term });
  };

  render() {
    const { assignedTo = [], open, assigneeList, searchTerm } = this.state;
    const {
      avatarSize,
      classes,
      theme,
      isArchivedSelected,
      disabled,
      singleSelect,
      assigneeHeading = "Assigned To",
      minOneSelection,
      obj,
      buttonVariant = "",
      btnText,
      intl
    } = this.props;
    const filteredAssignees = assigneeList.filter(createFilter(searchTerm, ["fullName"]));
    return (
      <>
        <CustomButton
          onClick={this.handleClick}
          btnType="success"
          variant="text"
          disabled={isArchivedSelected || disabled} // disable button if archive is selected
          buttonRef={node => {
            this.anchorEl = node;
          }}>
          <AddIcon
            classes={{ root: classes.addAssigneeBtnIcon }}
            htmlColor={theme.palette.primary.light}
          />
          {btnText}
        </CustomButton>

        <DropdownMenu
          open={open}
          anchorEl={this.anchorEl}
          placement="bottom-start"
          id="assigneeDropdown"
          size="medium"
          closeAction={this.handleClose}>
          <div>
            <div className={classes.dropdownSearchCntCustomField}>
              <SearchInput
                className="HtmlInput"
                maxLength="80"
                onChange={this.searchUpdated}
                placeholder={intl.formatMessage({
                  id: "common.search.label",
                  defaultMessage: "Search",
                })}
              />
            </div>
            <Scrollbars autoHide autoHeightMax={250} autoHeight={false} style={{ height: 250 }}>
              {filteredAssignees.length > 0 ? (
                <Typography variant="caption" className={classes.listItemHeading}>
                  <FormattedMessage id="common.search-assignee.label" defaultMessage="Members" />
                </Typography>
              ) : null}
              {/* Other Options List */}
              <List className={classes.list}>
                {filteredAssignees
                  .filter(m => !m.isDeleted)
                  .map((member, i) => {
                    return (
                      <ListItem
                        key={i}
                        button
                        onClick={event => this.handleMemberSelect(event, member)}
                        className={classes.menuItemavatar}>
                        <CustomAvatar
                          otherMember={{
                            imageUrl: member.imageUrl,
                            fullName: member.fullName,
                            lastName: "",
                            email: member.email,
                            isOnline: member.isOnline,
                            isOwner: member.isOwner,
                          }}
                          size="xsmall"
                        />
                        <ListItemText
                          classes={{ root: classes.itemTextRoot, primary: classes.itemText }}
                          primary={
                            <>
                              {member.fullName}
                              <br />
                              {member.isActiveMember === null ? (
                                <Typography variant="caption">Invite sent</Typography>
                              ) : null}
                            </>
                          }
                        />
                      </ListItem>
                    );
                  })}
              </List>
            </Scrollbars>
          </div>
        </DropdownMenu>
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    members: state.profile.data.member.allMembers,
  };
};
export default compose(
  injectIntl,
  connect(mapStateToProps),
  withStyles(dropdownStyles, { withTheme: true })
)(AssigneeSelectDropdown);
