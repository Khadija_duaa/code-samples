const dropdownStyle = theme => ({
  dropdownLabel: {
    fontSize: "13px !important",
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  dropdownValue: {
    fontSize: "13px !important",
    color: "#202020",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    marginLeft: 5,
  },
  itemText: {
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    lineHeight: "normal",
  },
  itemTextSelected: {
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    lineHeight: "normal",
    color: theme.palette.text.azure,
  },
  itemTextCaption: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    marginTop: 7,
  },
  listItem: {
    padding: "6px 14px",
    position: "relative",
  },
  selectedItemBorder: {
    background: theme.palette.background.blue,
    width: 3,
    position: "absolute",
    left: 0,
    height: 30,
    borderRadius: "0 4px 4px 0",
  },
  selectedItemBorderWithCaption: {
    background: theme.palette.background.blue,
    width: 3,
    position: "absolute",
    left: 0,
    height: 50,
    borderRadius: "0 4px 4px 0",
  },
  itemTextCnt: {
    display: "flex",
    flexDirection: "column",
  },
  ddIcon: {
    marginBottom: -7,
    color: "#BFBFBF",
    marginLeft: -4,
  },
  chip: {
    display: "flex",
    padding: "4px 6px",
    margin: '5px -9px', //UI-fix - added margin
    fontSize: "12px !important",
    color: theme.palette.text.brightBlue,
    background: theme.palette.background.lightBlue,
    alignItems: "center",
    lineHeight: "normal",
    fontFamily: theme.typography.fontFamilyLato,
    borderRadius: 4,
  },
  removeIcon: {
    color: theme.palette.icon.brightBlue,
    fontSize: "15px !important",
    marginLeft: 5,
    cursor: "pointer",
  },
});

export default dropdownStyle;
