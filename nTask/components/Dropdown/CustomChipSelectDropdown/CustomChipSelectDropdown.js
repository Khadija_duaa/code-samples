// @flow

import React, { useState, useEffect, useContext } from "react";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import { Scrollbars } from "react-custom-scrollbars";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import CloseIcon from "@material-ui/icons/Close";
import collabContext from "../../../Views/Collaboration/Context/collaboration.context";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { statusData } from "../../../helper/projectDropdownData";
import CustomButton from "../../Buttons/CustomButton";
import DropdownMenu from "../DropdownMenu";
import dropdownStyle from "./style";
import CustomIconButton from "../../Buttons/CustomIconButton";
// Onboarding Main Component
function CustomChipSelectDropdown(props: DropdownProps) {
  const {
    classes,
    theme,
    option,
    options,
    label,
    onSelect,
    size,
    scrollHeight,
    disabled,
    style,
    iconVisible = true,
    onMouseOver,
    onMouseEnter,
    onMouseLeave,
    icon,
  } = props;
  const data = options;
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedOption, setSelectedOption] = useState(option || {});
  let stateContext = useContext(collabContext);

  useEffect(() => {
    stateContext.updateOpen(Boolean(anchorEl))
  }, [anchorEl])

  const handleClick = event => {
    // Function Opens the dropdown
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const handleItemClick = (event, option) => {
    // Fuction responsible for selecting item from the list
    setSelectedOption(option);
    setAnchorEl(null);
    onSelect(option); // Callback on item select used to lift up option to parent if needed
  };
  const open = Boolean(anchorEl);

  // Use Effect to set default option on initial render and update if option is change in parent component
  useEffect(() => {
    setSelectedOption(option || {});
  }, [option]);

  // Clear all selected option
  const handleRemoveOption = () => {
    setSelectedOption({});
  };
  return (
    <ClickAwayListener
      mouseEvent="onMouseUp"
      touchEvent="onTouchEnd"
      onClickAway={handleClose}
    >
      <div>
        {selectedOption && selectedOption.label ? (
          <span className={classes.chip}>
            {selectedOption.value}
            <CloseIcon className={classes.removeIcon} onClick={handleRemoveOption} />
          </span>
        ) : (
          <CustomIconButton
            btnType="transparent"
            ref={anchorEl}
            onMouseOver={onMouseOver}
            onMouseEnter={onMouseOver}
            onMouseLeave={onMouseLeave}
            onClick={handleClick}
            style={{ padding: 0 }}
          >
            {icon}
          </CustomIconButton>
        )}
        {/* <CustomButton
        ref={anchorEl}
        onClick={handleClick}
        btnType="plainbackground"
        variant="text"
        style={{
          padding: 7,
          borderRadius: 4,
        }}
        disabled={disabled}
      >
        <span className={classes.dropdownLabel}>{label}</span>
        {" "}
        <span className={classes.dropdownValue}>
          {" "}
          {selectedOption.label}
          {" "}
          {iconVisible ? <ArrowDropDownIcon className={classes.ddIcon} /> : ""}
        </span>
      </CustomButton> */}
        <DropdownMenu
          clickAway={false}
          open={stateContext.open}
          closeAction={handleClose}
          anchorEl={anchorEl}
          size={size}
          placement="bottom-start"
        >
          <Scrollbars autoHide={false} autoHeight autoHeightMin={0} autoHeightMax={scrollHeight}>
            <List>
              {data.map((o, i) => {
                return (
                  <ListItem
                    key={i}
                    button
                    className={classes.listItem}
                    onClick={event => handleItemClick(event, o)}
                  >
                    {o.label === selectedOption.label && (
                      <span
                        className={
                          o.caption
                            ? classes.selectedItemBorderWithCaption
                            : classes.selectedItemBorder
                        }
                      />
                    )}
                    <div className={classes.itemTextCnt}>
                      <span
                        className={
                          o.label === selectedOption.label
                            ? classes.itemTextSelected
                            : classes.itemText
                        }
                      >
                        {o.label}
                      </span>
                    </div>
                  </ListItem>
                );
              })}
            </List>
          </Scrollbars>
        </DropdownMenu>
      </div>
    </ClickAwayListener>
  );
}
CustomChipSelectDropdown.defaultProps = {
  size: "small",
  scrollHeight: 180,
  disabled: false,
  option: {},
};
export default compose(
  withRouter,
  withStyles(dropdownStyle, { withTheme: true })
)(CustomChipSelectDropdown);
