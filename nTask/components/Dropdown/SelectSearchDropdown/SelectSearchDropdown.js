import React, { Component, Fragment } from "react";
import Select, { components, createFilter } from "react-select";
import { withStyles } from "@material-ui/core/styles";
// import SearchIcon from "@material-ui/icons/Search";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import RemoveIcon from "@material-ui/icons/Cancel";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import ChevronDown from "@material-ui/icons/ArrowDropDown";
import ClearIcon from "@material-ui/icons/Clear";
import { injectIntl } from "react-intl";
import CustomAvatar from "../../Avatar/Avatar";
import { selectCustomStyles } from "./selectCustomStyles";
import searchStyles from "./style.js";
import { getContrastYIQ } from "../../../helper/index";
import isEqual from "lodash/isEqual";
import { SettingsPhoneTwoTone } from "@material-ui/icons";
import clsx from "clsx";

var classNames = require("classnames");

class SelectSearchDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: [],
      inputValue: "",
      selectedValue: [],
      data: {},
      type: "",
    };
  }

  componentDidMount() {
    const { data, selectedValue } = this.props;
    this.setState({ options: data(), selectedValue: selectedValue });
  }

  componentDidUpdate(prevProps, prevState) {
    const { data, selectedValue } = this.props;
    const test = data();

    if (!isEqual(test, this.state.options)) this.setState({ options: test });
    if (!isEqual(selectedValue, prevProps.selectedValue)) {
      this.setState({ selectedValue: selectedValue });
    }
    // if (JSON.stringify(selectedValue) !== JSON.stringify(prevProps.selectedValue)) {
    //   this.setState({ selectedValue: selectedValue });
    // }
  }

  // Component for rendering custom/static option at the end of the list which will always stay
  MenuList = props => {
    const { classes, heading } = this.props;
    return (
      <components.MenuList {...props}>
        {heading ? (
          <Typography variant="h6" className={classes.selectMenuListHeading}>
            {heading}
          </Typography>
        ) : null}
        {props.children}
      </components.MenuList>
    );
  };

  // Component for custom rendering of option in the menu list
  CustomOptionComponent = ({ innerProps, innerRef, getStyles, children, data, ...props }) => {
    const {
      classes,
      theme,
      avatar,
      icon,
      truncateOptionWidth,
      captionKey,
      optionBackground,
      isCollabEnabled
    } = this.props;
    const { intl } = this.props;
    const optionStyles = getStyles("option", props);
    return (
      <div ref={innerRef} {...innerProps} style={optionStyles}>
        {/* Custom avatar will be rendered in case user wants to render avatar along with the values, especially in case of members list */}
        <div className={classes.optionWithAvatar}>
          {avatar ? (
            <CustomAvatar
              otherMember={{
                imageUrl: data.obj.imageUrl,
                fullName: data.obj.fullName,
                lastName: "",
                email: data.obj.email,
                isOnline: false,
                isOwner: data.obj.isOwner,
              }}
              styles={{ marginRight: 10 }}
              size="xsmall"
            />
          ) : icon ? (
            data.icon
          ) : null}

          <Typography
            variant="h6"
            style={{ color: optionStyles.color, width: truncateOptionWidth }}
            className={classes.optionText}>
            <b>{data.uniqueId}</b> {data.uniqueId ? "-" : null}{" "}
            {optionBackground ? (
              <span
                style={{
                  background: data.color || "transparent",
                  color: data.color ? getContrastYIQ(data.color) : "",
                  borderRadius: 4,
                  padding: 5,
                  width: "100%",
                  whiteSpace: "break-spaces",
                }}>
                {children}
              </span>
            ) : (
              children
            )}{" "}
            {captionKey ? (
              <>
                {" "}
                <span className={classes.captionDot}>•</span>{" "}
                <span className={classes.caption}>{data.obj[captionKey]}</span>{" "}
              </>
            ) : null}
          </Typography>
        </div>
      </div>
    );
  };

  // Component that renderes delete button of select option chips
  MultiValueRemove = ({ data, ...props }) => {
    const { theme, classes, optionBackground, isCollabEnabled } = this.props;
    return (
      <components.MultiValueRemove {...props}>
        {!isCollabEnabled &&
          <RemoveIcon
            style={
              optionBackground
                ? { color: data && data.color ? getContrastYIQ(data.color) : "" }
                : null
            }
            className={classes.chipsRemoveIcon}
            htmlColor={theme.palette.secondary.medDark}
          />
        }
      </components.MultiValueRemove>
    );
  };

  // Component that renderes dropdown arrow indicator
  DropdownIndicator = props => {
    const { theme, classes } = this.props;
    return (
      <components.DropdownIndicator {...props}>
        <ChevronDown
          classes={{ root: classes.arrowDownIconSize }}
          className={classes.dropdownIndicatorIcon}
          htmlColor={theme.palette.secondary.light}
        />
      </components.DropdownIndicator>
    );
  };

  // Component that renderes single select value container
  SingleValue = ({ children, data, ...props }) => {
    const { avatar, icon, classes } = this.props;
    return (
      <components.SingleValue {...props}>
        <div className={avatar || icon ? classes.optionWithIcon : classes.singleValueLabel}
          title={children || ""}>
          {avatar ? (
            <CustomAvatar
              otherMember={{
                imageUrl: data.obj.imageUrl,
                fullName: data.obj.fullName,
                lastName: "",
                email: data.obj.email,
                isOnline: false,
                isOwner: data.obj.isOwner,
              }}
              styles={{ marginRight: 5 }}
              size="small18"
            />
          ) : icon ? (
            data.icon
          ) : null}
          {children}
        </div>
      </components.SingleValue>
    );
  };
  MultiValueContainer = ({ data, children, ...props }) => {
    const { classes, optionBackground, isCollabEnabled } = this.props;
    return (
      <components.MultiValueContainer {...props}>
        <div
          className={`${classes.multiValueCnt} ${isCollabEnabled ? classes.multiValueCntCollab : null}`} //add padding - UI fix
          style={optionBackground ? { background: (data && data.color) || "#e4e4e4" } : null}>
          {children}
        </div>
      </components.MultiValueContainer>
    );
  };
  // Component that renderes select option chips
  MultiValueLabel = ({ data, children, ...props }) => {
    const { avatar, icon, classes, optionBackground, isCollabEnabled } = this.props;
    return (
      <components.MultiValueLabel {...props}>
        <div className={classes.optionWithIcon}>
          {avatar ? (
            <CustomAvatar
              otherMember={{
                imageUrl: data.obj ? data.obj.imageUrl : "",
                fullName: data.obj ? data.obj.fullName : "",
                lastName: "",
                email: data.obj ? data.obj.email : "",
                isOnline: false,
                isOwner: data.obj ? data.obj.isOwner : false,
              }}
              styles={{ marginRight: isCollabEnabled ? 6 : 5 }}
              size={isCollabEnabled ? "small24" : "small18"}
            />
          ) : icon ? (
            data.icon
          ) : null}
          {optionBackground ? (
            <span
              style={{
                whiteSpace: "normal",
                color: data && data.color ? getContrastYIQ(data.color) : "",
              }}>
              {children}
            </span>
          ) : (
            isCollabEnabled ?
              <span
                style={{
                  fontSize: "14px", //11px //UI fix - chip font size
                  fontWeight: 900
                }}>
                {children}
              </span> :
              children
          )}
        </div>
      </components.MultiValueLabel>
    );
  };

  // Component that renders clear indicator in select
  ClearIndicator = props => {
    const { classes, theme } = this.props;
    return (
      <components.ClearIndicator {...props}>
        <ClearIcon
          className={classes.dropdownClearIcon}
          htmlColor={theme.palette.secondary.medDark}
        />
      </components.ClearIndicator>
    );
  };

    // Component that renders Label with selected options in select
    Control = ({ data, children, ...props }) => {
      const { classes, theme, isCollabEnabled } = this.props;
      return (
        <components.Control  {...props}>
          {isCollabEnabled && <Typography variant={"body2"} classes={{ body2: classes.selectedText }}>{"To:"}</Typography>}
          {children}
        </components.Control >
      );
    };

  onSelectChange = (option, meta) => {
    const { selectChange, selectClear, type, selectRemoveValue, writeFirst = false } = this.props;
    if (meta.action == "select-option" && selectChange) {
      // checks user action  and execute block of code if user selected an option && if user has sent a change function from parent
      if (writeFirst) this.setState({ selectedValue: option });
      selectChange(type, option);
    }
    if (meta.action == "clear" && selectClear) {
      // checks user action and execute block of code if user performed action to clear the select
      if (writeFirst) this.setState({ selectedValue: option });
      selectClear(type, option);
    }
    if (meta.action == "remove-value") {
      // checks user action  and execute block of code if user removed an option && if user has sent a change function from parent
      if (writeFirst) this.setState({ selectedValue: option });
      if (selectRemoveValue) {
        selectRemoveValue(type, option);
      } else {
        selectChange(type, option);
      }
    }
  };

  // function for select on input change
  handleInputChange = value => {
    const { validateValue } = this.props;
    if ((validateValue && validateValue(value)) || value == "") {
      this.setState({ inputValue: value });
    } else if (!validateValue) {
      this.setState({ inputValue: value });
    }
  };

  // function called on dropdown open
  handleMenuOpen = () => {
    this.setState({ open: true });
  };

  // function called on dropdown close
  handleMenuClose = () => {
    this.setState({ open: false });
  };

  render() {
    const {
      theme,
      classes,
      label,
      placeholder,
      isMulti = true,
      isClearable = false,
      styles,
      customStyles = {},
      isDisabled = false,
      id,
      closeMenuOnSelect = true,
      labelStyle,
      intl,
      noOptionsMessage,
      inputType,
      isCollabEnabled,
    } = this.props;
    const { options, inputValue, open = false, selectedValue } = this.state;

    return (
      <FormControl
        fullWidth
        classes={{ root: classes.reactSelectFormControl }}
        style={styles}
        id={id}>
        <InputLabel
          htmlFor="AssignedToLabel"

          className={clsx({
            [classes.selectLabel]: !open,
            [classes.selectLabelSelected]: open,
            [classes.selectLabelSelectedUnderline]: open && inputType == "underline",
            [classes.selectLabelUnderline]: !open && inputType == "underline",
          })}

          style={labelStyle}
          shrink={false}>
          {label}
        </InputLabel>
        <Select
          isMulti={isMulti}
          ref={ref => {
            this.selectEle = ref;
          }}
          isClearable={isClearable}
          noOptionsMessage={() => {
            return noOptionsMessage;
          }}
          // noOptionsMessage={() => {
          //   return intl.formatMessage({
          //     id: "common.dropdown.no-record.label",
          //     defaultMessage: ""
          //   })
          // }}
          cropWithEllipsis
          components={{
            Option: this.CustomOptionComponent,
            MultiValueRemove: this.MultiValueRemove,
            DropdownIndicator: isCollabEnabled ? () => null : this.DropdownIndicator,
            MenuList: this.MenuList,
            MultiValueLabel: this.MultiValueLabel,
            MultiValueContainer: this.MultiValueContainer,
            SingleValue: this.SingleValue,
            ClearIndicator: this.ClearIndicator,
            Control: this.Control,
            IndicatorSeparator: () => {
              return false;
            },
          }}
          placeholder={placeholder}
          name={placeholder}
          options={options}
          menuPlacement="auto"
          onMenuOpen={this.handleMenuOpen}
          onMenuClose={this.handleMenuClose}
          onInputChange={this.handleInputChange}
          closeMenuOnSelect={closeMenuOnSelect}
          autoFocus={false}
          inputValue={inputValue}
          value={selectedValue}
          styles={selectCustomStyles(theme, customStyles, inputType, isCollabEnabled)}
          isDisabled={isDisabled}
          onChange={this.onSelectChange}
        />
      </FormControl>
    );
  }
}

SelectSearchDropdown = injectIntl(SelectSearchDropdown, { withRef: true });
SelectSearchDropdown.defaultProps = {
  selectChange: () => {
  },
  selectClear: () => {
  },
  noOptionsMessage: "No results found related to your search.",
};
export default withStyles(searchStyles, { withTheme: true })(SelectSearchDropdown);
