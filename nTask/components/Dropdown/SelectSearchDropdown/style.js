const searchStyles = (theme) => ({
  shortcutTag: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
    position: "absolute",
    right: 10,
    fontWeight: 500,
  },
  selectedText: {
    marginLeft: "14px",
    color: theme.palette.text.light
  },
  arrowDownIconSize:{
    fontSize: "1.5rem !important"
  },
  typeTag: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
    fontWeight: 500,
    padding: "2px 10px",
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  searchMenuHeader: {
    display: "flex",
    justifyContent: "space-between",
    padding: "10px 10px 0 10px",
  },
  quickSearchList: {
    padding: 0,
    listStyleType: "none",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    background: theme.palette.background.paper,
    "& li": {
      padding: "15px 10px",
      cursor: "pointer",
    },
  },
  userNameCountCnt: {
    display: "flex",
    justifyContent: "space-between",
  },
  optionText: {
    width: '100%',
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    display: "flex",
    alignItems: "center",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important"
  },
  archiveIcon: {
    fontSize: "19px !important",
    marginRight: 10,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "2px 2px 2px 4px",
    borderRadius: 4,
  },
  chips: {
    background: theme.palette.background.medium,
    height: 28,
    borderRadius: 4,
    marginRight: 5,
    marginBottom: 2,
    marginTop: 2,
  },
  chipsRemoveIcon: {
    fontSize: "16px !important",
  },
  chipsLabel: {
    fontSize: "14px !important",
    color: theme.palette.text.secondary,
    paddingLeft: 4,
  },
  selectMenuListHeading: {
    padding: "5px 10px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  reactSelectFormControl: {
    marginTop: 22,
    marginBottom: 20,
  },
  selectLabel: {
    transform: "translate(2px, -17px) scale(1)",
    color: theme.palette.text.darkGray,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: theme.typography.fontFamilyLato,
  },
  selectLabelSelectedUnderline: {
    transform: "translate(2px, -17px) scale(1)",
    color: theme.palette.text.green,
    fontSize: '16px !important',
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: theme.typography.fontFamilyLato,
  },
  selectLabelSelected: {
    transform: "translate(2px, -17px) scale(1)",
    color: theme.palette.text.green,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: theme.typography.fontFamilyLato,
  },
  selectLabelUnderline: {
    fontSize: "16px !important",
    color: "#202020",
    fontWeight: theme.typography.fontWeightExtraLight,
  },
  optionWithAvatar: {
    display: "flex",
    alignItems: "center",
  },
  optionWithIcon: {
    display: "flex",
    alignItems: "center",
    whiteSpace: "normal"
  },
  singleValueLabel:{
    width: "100%",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap"
  },

  dropdownClearIcon: {
    fontSize: "13px !important",
  },
  captionDot: {
    fontSize: "22px !important",
    margin: "0 5px",
    color: theme.palette.text.medGray,
  },
  caption: {
    color: theme.palette.text.medGray,
    fontSize: "11px !important",
    fontWeight: 400,
  },
  multiValueCnt: {
    display: 'flex',
    borderRadius: 16,
    fontFamily: theme.typography.fontFamilyLato,
  },
  multiValueCntCollab: {
    background: theme.palette.background.grayLighter,
    borderRadius: "17px",
  },
});

export default searchStyles;
