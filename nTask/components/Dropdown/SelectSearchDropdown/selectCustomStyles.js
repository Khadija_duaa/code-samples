export const selectCustomStyles = (theme, { control, container, placeholder, singleValue }, inputType, isCollabEnabled) => {

  return {
    option: (provided, { data, isDisabled, isFocused, isSelected }) => {
      return {
        ...provided,
        fontSize: "12px !important",
        fontWeight: 600,
        padding: "8px 10px",
        backgroundColor: isDisabled
          ? null
          : isSelected
          ? theme.palette.background.azureLight
          : isFocused
          ? theme.palette.background.items
          : null,
        color: isDisabled
          ? theme.palette.text.primary
          : isSelected
          ? theme.palette.text.primary
          : isFocused
          ? theme.palette.text.primary
          : null,
        ":active": {
          backgroundColor: !isDisabled && (isSelected ? theme.palette.background.items : null),
          color: !isDisabled && (isSelected ? theme.palette.text.primary : null),
        },
      };
    },
    container: (provided, state) => ({
      ...provided,
      borderRadius: 4,
      width: "100%",
      marginTop: inputType == 'underline' ? 16 : 0,
      ...container,
    }),
    control: (provided, { data, isDisabled, isFocused, isSelected }) => ({
      ...provided,
      maxHeight: 120,
      overflowY: "auto",
      border: inputType == 'underline' ? 'none' : `1px solid ${theme.palette.border.lightBorder}`,
      borderBottom: inputType == 'underline' && isFocused ? `1px solid ${theme.palette.border.greenBorder}` : inputType == 'underline' ? `1px solid #202020` : '',
      borderRadius: inputType == 'underline' ? 0 : 4,
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightExtraLight,
      borderWidth: isFocused && inputType == 'underline' ? '2px !important' : '1px !important',
      boxShadow: "none",
      "&:hover": {
        borderColor: isFocused && inputType == 'underline' ? theme.palette.border.greenBorder : theme.palette.border.lightBorder,
        borderWidth: inputType == 'underline' ? 2 : 1
      },
      ...control,
    }),
    multiValueRemove: (provided, { data }) => ({
      ...provided,
      color: theme.palette.secondary.medDark,
      marginRight: isCollabEnabled ? '4px' : '', //UI fix- close icon margin
      ':hover': {
        backgroundColor: isCollabEnabled ? "transparent" : "rgb(255, 189, 173)",
        color: theme.palette.secondary.medDark
      },
    }),
    placeholder: (provided, state) => ({
      ...provided,
      fontSize: "13px !important",
      color: inputType == 'underline' ? theme.palette.text.primary : theme.palette.text.hint,

      ...placeholder
    }),
    valueContainer: (provided, state) => ({
      ...provided,
      fontSize: isCollabEnabled ? "14px !important" : "18px !important", //UI fix - input font size
      padding: inputType == 'underline' ? "2px 0" : "2px 5px",
    }),
    dropdownIndicator: (provided, state) => ({
      ...provided,
      padding: "0 5px 0 0",
    }),
    clearIndicator: (provided, state) => ({
      ...provided,
      padding: 0,
    }),
    singleValue: (provided, state) => ({
      ...provided,
      fontSize: inputType == 'underline' ? 14 : 13,
      padding: "2px 5px",
      fontWeight: 400,
      fontFamily: theme.typography.fontFamilyLato,
      whiteSpace: "break-spaces",
      paddingLeft: inputType == 'underline' ? 0 : '',
      ...singleValue
    }),

    menu: (provided, state) => ({
      ...provided,
      boxShadow: "0 6px 12px rgba(0,0,0,0.175)",
      zIndex: 2,
    }),
    menuList: (provided, state) => ({
      ...provided,
      maxHeight: 250,
      padding: 0,
    }),
    multiValue: (provided, state) => ({
      ...provided,
      background: "transparent",
      whiteSpace: "break-spaces"
    }),
  };
};
