// @flow

import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Scrollbars } from "react-custom-scrollbars";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import CustomButton from "../../Buttons/CustomButton";
import DropdownMenu from "../DropdownMenu";
import dropdownStyle from "./style";
import CustomIconButton from "../../Buttons/CustomIconButton";
import Typography from '@material-ui/core/Typography'
type DropdownProps = {
  classes: Object,
  theme: Object,
  option: Object,
  options: Function, // List of all options shown in list
  onSelect: Function,
  label: String,
  size: String,
  scrollHeight: Number,
  disabled: Boolean,
  style: Object,
  iconVisible: Boolean,
};

// Onboarding Main Component
function CustomMultiSelectDropdown(props: DropdownProps) {
  const {
    classes,
    theme,
    option,
    options,
    label,
    onSelect,
    size,
    scrollHeight,
    disabled,
    style,
    iconVisible = true,
    buttonProps,
    selectionLabel,
    iconButton,
    maxSelections,
    icon,
    heading
  } = props;
  const data = options();
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedOptions, setSelectedOptions] = useState([]);
  const handleClick = event => {
    // Function Opens the dropdown
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const handleItemClick = (event, optionSelected) => {
    // Fuction responsible for selecting item from the list

    
    const isSelected = selectedOptions.find(s => s.label === optionSelected.label);
    if (isSelected) {
      const newSelectedOptions = selectedOptions.filter(o => o.label !== optionSelected.label);
      setSelectedOptions(newSelectedOptions);
      onSelect(newSelectedOptions); // Callback on item select used to lift up option to parent if needed
    } else {
      //return in case of maximum selections reached
      if(maxSelections && selectedOptions.length === maxSelections){
        return
      }
      setSelectedOptions(
        selectedOptions.length ? [...selectedOptions, optionSelected] : [optionSelected]
      );
      onSelect(selectedOptions.length ? [...selectedOptions, optionSelected] : [optionSelected]); // Callback on item select used to lift up option to parent if needed
    }
  };
  const open = Boolean(anchorEl);
  useEffect(() => {
    setSelectedOptions(option);
  }, [option]);

  return (
    <>
      {iconButton ? (
        <CustomIconButton
          disabled={disabled}
          ref={anchorEl}
          onClick={handleClick}
          btnType="transparent"
          {...buttonProps}>
          {icon}
        </CustomIconButton>
      ) : (
        <CustomButton
          ref={anchorEl}
          onClick={handleClick}
          btnType="plainbackground"
          variant="text"
          style={{
            padding: 7,
            borderRadius: 4,
          }}
          disabled={disabled}
          {...buttonProps}>
          <span className={classes.dropdownLabel}>{label}</span>{" "}
          <span className={classes.dropdownValue}>
            {" "}
            {`${selectedOptions.length} Selected`}
            {iconVisible ? <ArrowDropDownIcon className={classes.ddIcon} /> : ""}
          </span>
        </CustomButton>
      )}
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size={size}
        placement="bottom-start">
        <Scrollbars autoHide={false} autoHeight autoHeightMin={0} autoHeightMax={scrollHeight}>
        <Typography className={classes.dropdownHeading} variant='body2'>{heading}</Typography>
          <List>
            {data.map((o, i) => {
              const isSelected = selectedOptions.find(so => o.label === so.label);
              return (
                <ListItem
                  key={i}
                  button
                  className={classes.listItem}
                  onClick={event => handleItemClick(event, o)}>
                  {isSelected && (
                    <span
                      className={
                        o.caption
                          ? classes.selectedItemBorderWithCaption
                          : classes.selectedItemBorder
                      }
                    />
                  )}
                  <div className={classes.itemTextCnt}>
                    <span className={isSelected ? classes.itemTextSelected : classes.itemText}>
                      {o.label}
                    </span>
                    {o.caption && <span className={classes.itemTextCaption}>{o.caption}</span>}
                  </div>
                </ListItem>
              );
            })}
          </List>
        </Scrollbars>
      </DropdownMenu>
    </>
  );
}
CustomMultiSelectDropdown.defaultProps = {
  size: "small",
  scrollHeight: 180,
  disabled: false,
};
export default compose(
  withRouter,
  withStyles(dropdownStyle, { withTheme: true })
)(CustomMultiSelectDropdown);
