import React, { Component } from "react";
import DropdownMenu from "./DropdownMenu";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import CustomAvatar from "../Avatar/Avatar";
import { withStyles } from "@material-ui/core/styles";
import { Scrollbars } from "react-custom-scrollbars";
import IconButton from "@material-ui/core/IconButton";
import dropdownStyles from "./style";
import { compose } from "redux";
import { connect } from "react-redux";
import {FormattedMessage} from "react-intl";
class AssigneeDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }
  handleClick = () => {
    this.setState({ open: true });
  };
  handleClose = () => {
    this.setState({ open: false });
  };
  render() {
    const { selectedValue, open } = this.state;
    const { selectedData, data, iconType, profile } = this.props;

    return (
      <>
        {iconType == "avatar" ? (
          <ul className="AssigneeAvatarList" onClick={e => e.stopPropagation()}>
            {data.slice(0, 3).map((Assignee, i, t) => {
              return (
                <li
                  key={i}
                  style={{ zIndex: i, marginLeft: !i == 0 ? -5 : null }}
                >
                  {
                    <CustomAvatar
                      otherMember={{
                        imageUrl: Assignee.value.avatar,
                        fullName: Assignee.value.fullName,
                        lastName: "",
                        email: Assignee.value.email,
                        isOnline: Assignee.value.isOnline,
                        isOwner: Assignee.value.isOwner
                      }}
                      size={iconSize ? iconSize : "small"}
                    />
                  }
                </li>
              );
            })}
            {data.length > 3 ? (
              <li>
                <IconButton
                  className={classes.TotalAssignee}
                  buttonRef={node => {
                    this.moreAssigneeCount = node;
                  }}
                >
                  + {data.length - 3}
                </IconButton>
                <div />
              </li>
            ) : (
              ""
            )}
            <li className="AssigneeListBtnCnt">
              <IconButton
                classes={{ root: classes.addAssigneeBtn }}
                // disabled={isArchivedSelected}
                onClick={this.handleClick}
              >
                <AddIcon
                  classes={{ root: classes.addAssigneeBtnIcon }}
                  htmlColor={theme.palette.text.dark}
                />
              </IconButton>
            </li>
          </ul>
        ) : null}

        <DropdownMenu open={open} size="xsmall" closeAction={this.handleClose}>
        <div>
          <Typography variant="body2"><FormattedMessage
        id="common.assigned.label"
        defaultMessage="Assigned To"
      /></Typography>
          {/* Selection List */}
          <List>
            {selectedData.map(item => {
              return (
                <ListItem>
                  {iconType == Avatar ? (
                    <CustomAvatar
                      otherMember={{
                        imageUrl: data.avatar,
                        fullName: data.fullName,
                        lastName: "",
                        email: data.email,
                        isOnline: data.isOnline,
                        isOwner: data.isOwner
                      }}
                      size="xsmall"
                    />
                  ) : null}
                  <ListItemText primary="Text" />
                </ListItem>
              );
            })}
          </List>
          <Typography variant="body2">Members</Typography>
          {/* Other Options List */}
          <List>
            {profile.map(() => {
              return (
                <ListItem>
                  <CustomAvatar
                    otherMember={{
                      imageUrl: data.avatar,
                      fullName: data.fullName,
                      lastName: "",
                      email: data.email,
                      isOnline: data.isOnline,
                      isOwner: data.isOwner
                    }}
                    size="xsmall"
                  />
                  <ListItemText primary="Text" />
                </ListItem>
              );
            })}
          </List>
          </div>
        </DropdownMenu>
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.profile.data
  };
};
export default compose(
  connect(mapStateToProps),
  withStyles(dropdownStyles, { withTheme: true })
)(AssigneeDropdown);
