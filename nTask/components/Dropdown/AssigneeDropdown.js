import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import { compose } from "redux";
import { connect } from "react-redux";
import RemoveIcon from "@material-ui/icons/Close";
import SearchInput, { createFilter } from "react-search-input";
import { Scrollbars } from "react-custom-scrollbars";
import { FormattedMessage, injectIntl } from "react-intl";
import SvgIcon from "@material-ui/core/SvgIcon";
import dropdownStyles from "./style";
import CustomAvatar from "../Avatar/Avatar";
import DropdownMenu from "./DropdownMenu";
import { UpdateFollowupAction } from "../../redux/actions/meetings";
import { generateActiveMembers } from "../../helper/getActiveMembers";
import AssigneeIcon from "../Icons/AssigneeIcon";
import MoreMembersMenu from "../Menu/MoreMembersMenu";
import isEqual from "lodash/isEqual";

class AssigneeDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      assignedTo: [],
      assigneeList: [],
      searchTerm: "",
      moreMembersOpen: false,
    };
  }

  handleClick = e => {
    e.stopPropagation();
    this.setState({ open: true });
  };

  componentDidUpdate(prevProps, prevState) {
    const { members, assignedTo } = this.props;
    const selectedMembersArr = members.filter(m => {
      return assignedTo.findIndex(a => a.userId == m.userId) > -1;
    });
    const prevMembersArr = members.filter(m => {
      return prevProps.assignedTo.findIndex(a => a.userId == m.userId) > -1;
    });
    if (JSON.stringify(members) !== JSON.stringify(prevProps.members)) {
      this.setState({ assigneeList: this.compareMembersList(selectedMembersArr) });
    }
    if (!isEqual(selectedMembersArr, prevMembersArr)) {
      this.setState({
        assignedTo: selectedMembersArr,
        assigneeList: this.compareMembersList(selectedMembersArr),
      });
    }
  }

  handleRemoveAssignee = member => {
    // Function responsible for removing assignee from list
    const { assigneeList, assignedTo } = this.state;
    const { obj, updateAction, singleSelect } = this.props;
    const newAssigneeList = singleSelect ? [member] : [member, ...assigneeList];
    const newAssignedToList = assignedTo.filter(assignee => {
      return assignee.userId !== member.userId;
    });
    this.setState(
      {
        assignedTo: newAssignedToList,
        assigneeList: newAssigneeList,
      },
      () => {
        // Updates Assignee on respective document(task/issue/risk etc..)
        updateAction(newAssignedToList, obj);
      }
    );
  };

  compareMembersList = (assigneeList = []) => {
    const members = generateActiveMembers();
    // Filtering Members list by comparing it with members listed in assigned To section in dropdown
    return members.filter(member => {
      const assigneeListFilter =
        assigneeList.find(selectedMember => {
          return selectedMember.userId == member.userId;
        }) || [];

      return assigneeListFilter.userId !== member.userId;
    });
  };

  handleMemberSelect = (event, member) => {
    // This function handles selection of member
    const { assignedTo } = this.state;
    const { updateAction, obj, singleSelect, addPermission = true } = this.props;
    if (addPermission) {
      /** if user have permission to add assignee then this code works */
      const newAssignedToList = singleSelect ? [member] : [...assignedTo, member];
      this.setState({ assignedTo: newAssignedToList }); // Updating Assignee List based on user selection
      this.setState(
        {
          assigneeList: this.compareMembersList(newAssignedToList),
          open: false,
          searchTerm: "",
        },
        () => {
          // Updates Assignee on respective document(task/issue/risk etc..)
          updateAction(newAssignedToList, obj);
        }
      );
    }
  };

  handleClose = event => {
    // if (document.getElementById("assigneeDropdown").contains(event.target)) {
    //   return;
    // }
    this.setState({ open: false, searchTerm: "" });
  };
  // componentWillUnmount() {
  //   this.anchorEl && this.anchorEl.removeEventListener("click", this.handleClick, true);
  // }

  componentDidMount() {
    const { assignedTo } = this.props;
    // this.anchorEl && this.anchorEl.addEventListener("click", this.handleClick, false);
    // Updating state with the total list of members whom user can assign the document
    this.setState({
      assigneeList: this.compareMembersList(assignedTo),
      assignedTo,
    });
  }

  searchUpdated = term => {
    this.setState({ searchTerm: term });
  };
  moreMembersMenuClose = () => {
    this.setState({ moreMembersOpen: false });
  };
  moreMembersOpen = () => {
    this.setState({ moreMembersOpen: true });
  };

  render() {
    const { assignedTo = [], open, assigneeList, searchTerm, moreMembersOpen } = this.state;
    const {
      avatarSize = "small",
      classes,
      theme,
      isArchivedSelected,
      disabled = false,
      singleSelect,
      assigneeHeading = "Assigned To",
      minOneSelection,
      obj,
      buttonVariant = "",
      deletePermission = true,
      addPermission = true,
      CustomRenderer,
      customIconButtonProps,
      totalAssigneeBtnProps = {},
      popperProps,
      style,
      customIconRenderer = false,
    } = this.props;
    const { intl } = this.props;
    const filteredAssignees = assigneeList.filter(createFilter(searchTerm, ["fullName"]));
    let assignedToList = assignedTo.map(e => {
      return {
        id: e.userId,
        value: {
          avatar: e.imageUrl,
          email: e.email,
          fullName: e.fullName,
          isOnline: e.isOnline,
          isOwner: e.teamOwner,
          name: e.userName,
        },
      };
    });
    return (
      <>
        <ul className="AssigneeAvatarList" onClick={e => e.stopPropagation()} style={style}>
          {isArchivedSelected || disabled ? null : (
            <li className={classes.assigneeListBtnCnt}>
              <IconButton
                classes={{ root: classes[`addAssigneeBtn${buttonVariant}`] }}
                // disabled={isArchivedSelected || disabled} // disable button if archive is selected
                buttonRef={node => {
                  this.anchorEl = node;
                }}
                onClick={this.handleClick}
                {...customIconButtonProps}>
                {customIconRenderer ? (
                  customIconRenderer
                ) : (
                  <SvgIcon viewBox="0 0 26.805 24" classes={{ root: classes.addAssigneeBtnIcon }}>
                    <AssigneeIcon />
                  </SvgIcon>
                )}
              </IconButton>
            </li>
          )}
          {assignedTo.slice(0, 3).map((assignee, i, t) => {
            return (
              <li key={i} style={{ zIndex: i, marginLeft: !i == 0 ? -5 : null }}>
                <CustomAvatar
                  otherMember={{
                    imageUrl: assignee.imageUrl,
                    fullName: assignee.fullName,
                    lastName: "",
                    email: assignee.email,
                    isOnline: assignee.isOnline,
                    isOwner: assignee.isOwner,
                  }}
                  size={avatarSize}
                  closeTaskDetailsPopUp={this.props.closeTaskDetailsPopUp}
                />
              </li>
            );
          })}
          {assignedTo.length > 3 ? (
            <li onMouseLeave={this.moreMembersMenuClose}>
              <IconButton
                className={classes.TotalAssignee}
                onMouseEnter={this.moreMembersOpen}
                buttonRef={node => {
                  this.moreAssigneeCount = node;
                }}
                {...totalAssigneeBtnProps}>
                +{assignedTo.length - 3}
              </IconButton>
              <div />
              {assignedTo.length > 3 ? (
                <MoreMembersMenu
                  open={moreMembersOpen}
                  closeAction={this.moreMembersMenuClose}
                  assigneeList={assignedToList}
                  anchorEl={this.moreAssigneeCount}
                />
              ) : null}
            </li>
          ) : (
            ""
          )}
        </ul>

        <DropdownMenu
          open={open}
          anchorEl={this.anchorEl}
          placement="bottom-start"
          id="assigneeDropdown"
          size="medium"
          closeAction={this.handleClose}
          {...popperProps}>
          <div>
            <div className={classes.dropdownSearchCnt}>
              <SearchInput
                className="HtmlInput"
                maxLength="80"
                onChange={this.searchUpdated}
                placeholder={intl.formatMessage({
                  id: "common.search.label",
                  defaultMessage: "Search",
                })}
              />
            </div>
            <Scrollbars
              autoHide={false}
              autoHeightMax={250}
              autoHeight={false}
              style={{ height: 250 }}>
              {assignedTo.length > 0 && !searchTerm ? (
                <>
                    <Typography  variant="body2" className={classes.listItemHeading} style={{ textTransform: "none", color: "#7e7e7e" }}>
                      {assigneeHeading}
                    </Typography>

                  {/* Selection List */}
                  <List className={classes.list}>
                    {assignedTo
                      .filter(m => !m.isDeleted)
                      .map((member, i) => { 
                        return (
                          <ListItem key={i} className={classes.menuItemavatar}>
                              <CustomAvatar
                                otherMember={{
                                  imageUrl: member.imageUrl,
                                  fullName: member.fullName,
                                  lastName: "",
                                  email: member.email,
                                  isOnline: member.isOnline,
                                  isOwner: member.isOwner,
                                }}
                                size="xsmall"
                              />
                            <ListItemText
                              primary={
                                <>
                                  {member.fullName}
                                  <br />
                                  {member.isActiveMember === null ? (
                                    <Typography variant="caption">Invite sent</Typography>
                                  ) : null}
                                </>
                              }
                              classes={{ root: classes.itemTextRoot, primary: classes.itemTextPrimary }}
                            />

                            {CustomRenderer ? (
                              minOneSelection && assignedTo.length < 2 ? null : deletePermission ? (
                                CustomRenderer(
                                  member,
                                  <RemoveIcon
                                    className={classes.removeAssigneeIcon}
                                    htmlColor={theme.palette.secondary.light}
                                    onClick={event => this.handleRemoveAssignee(member)}
                                  />
                                )
                              ) : null
                            ) : minOneSelection &&
                              assignedTo.length < 2 ? null : deletePermission ? (
                                <RemoveIcon
                                  className={classes.removeAssigneeIcon}
                                  htmlColor={theme.palette.secondary.light}
                                  onClick={event => this.handleRemoveAssignee(member)}
                                />
                              ) : null}
                          </ListItem>
                        );
                      })}
                  </List>
                </>
              ) : null}
              {filteredAssignees.length > 0 ? (
                <Typography variant="body2" className={classes.listItemHeading}>
                  <FormattedMessage id="common.search-assignee.label" defaultMessage="Members" />
                </Typography>
              ) : null}
              {/* Other Options List */}
              <List className={classes.list}>
                {filteredAssignees
                  .filter(m => !m.isDeleted)
                  .map((member, i) => {
                    return (
                      <ListItem
                        key={i}
                        button
                        onClick={event => this.handleMemberSelect(event, member)}
                        className={classes.menuItemavatar}>
                          <CustomAvatar
                            otherMember={{
                              imageUrl: member.imageUrl,
                              fullName: member.fullName,
                              lastName: "",
                              email: member.email,
                              isOnline: member.isOnline,
                              isOwner: member.isOwner,
                            }}
                            size="xsmall"
                          />
                        <ListItemText
                          classes={{ root: classes.itemTextRoot, primary: classes.itemTextPrimary }}
                          primary={
                            <>
                              {member.fullName}
                              <br />
                              {member.isActiveMember === null ? (
                                <Typography variant="caption">Invite sent</Typography>
                              ) : null}
                            </>
                          }
                        />
                      </ListItem>
                    );
                  })}
              </List>
            </Scrollbars>
          </div>
        </DropdownMenu>
      </>
    );
  }
}

AssigneeDropdown.defaultProps = {
  CustomRenderer: false,
  assignedTo: [],
  customIconRenderer: false,
};

const mapStateToProps = (state, ownProps) => {
  return {
    members: state.profile.data.member.allMembers,
  };
};
export default compose(
  injectIntl,
  connect(mapStateToProps),
  withStyles(dropdownStyles, { withTheme: true })
)(AssigneeDropdown);
