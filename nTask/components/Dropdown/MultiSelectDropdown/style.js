const sortingDropdownStyles = theme => ({
  sortBtnsCnt: {
    display: "flex",
    marginRight: 20
  },
  closeIcon: {
    fontSize: "12px !important"
  },
  asdecIcon: {
    marginRight: 10
  },
  dropdownArrow: {
    fontSize: "18px !important"
  }
});
export default sortingDropdownStyles;
