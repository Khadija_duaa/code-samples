import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import ListItemText from "@material-ui/core/ListItemText";
import { Scrollbars } from "react-custom-scrollbars";
import DownArrow from "@material-ui/icons/ArrowDropDown";
import UpArrow from "@material-ui/icons/ArrowDropUp";
import ChevronDown from "@material-ui/icons/KeyboardArrowDown";
import PropTypes from "prop-types";
import CustomButton from "../../Buttons/CustomButton";
import combineStyles from "../../../utils/mergeStyles";
import SelectionMenu from "../../Menu/SelectionMenu";
import menuStyles from "../../../assets/jss/components/menu";
import sortingDropdownStyles from "./style";

class MultiSelectDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      pickerOpen: false,
      pickerPlacement: "",
      checked: props.defaultValue ? props.defaultValue : [],
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }

  handleClose(event) {
    /** function call when close drop down */
    this.setState({ open: false, pickerOpen: false });
  }

  handleClick(event, placement) {
    /** function call for opens drop down  */
    event.stopPropagation();
    this.setState(state => ({
      open: !state.open,
    }));
  }

  handleToggle = (e, value) => {
    const { checked } = this.state;
    const { defaultValue, updateAction } = this.props;
    const checkValueExist = checked.find(c => c.value == value.value);
    if (checkValueExist) {
      // if value is already selected than remove it
      const newArray = checked.filter(d => d.value !== value.value);
      if (!newArray.length) {
        // Setting default value as first if in case there is not value
        this.setState({ checked: defaultValue });
        updateAction(defaultValue);
      } else {
        this.setState({ checked: newArray });
        updateAction(newArray);
      }
    } else {
      // if the value is not already selected than add it
      this.setState({ checked: [...checked, value] });
      updateAction([...checked, value]);
    }
  };

  render() {
    const { classes, theme, data, height, selectedValueInit = "", styles } = this.props;
    const { open, checked } = this.state;
    const DropDownHeight = height || 250;
    // let sortedValues = this.getSelectedSortedValues();
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div style={styles}>
          <CustomButton
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
            style={{
              minWidth: 98,
              padding: "7px 6px 7px 8px",
              display: "flex",
              justifyContent: "space-between",
            }}
            btnType="white"
            variant="contained"
          >
            {checked.length == 1 ? `${checked[0].value} ${selectedValueInit}` : "Multiple"}
            <ChevronDown
              htmlColor={theme.palette.secondary.medDark}
              className={classes.dropdownArrow}
            />
          </CustomButton>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose} /** function call when close drop down */
            placement="top-start"
            checkedType="multi"
            anchorRef={this.anchorEl}
            list={(
              <Scrollbars autoHide style={{ height: DropDownHeight }}>
                <List>
                  {data().map((
                    value /** displaying the drop down list array coming from props  */
                  ) => (
                    <ListItem
                      key={value.value}
                      button
                      disableRipple
                      className={`${classes.MenuItem} ${checked.filter(v => v.value == value.value).length
                          ? classes.selectedValue
                          : ""
                        }`}
                      classes={{ selected: classes.statusMenuItemSelected }}
                      onClick={e =>
                        this.handleToggle(e, value)} /** on select value from drop down , function call passing the whole 'value' object */
                    >
                      <ListItemText
                        primary={
                          value.value
                        } /** displaying name property from array coming ffrom props */
                        classes={{
                          primary: classes.statusItemText,
                        }}
                      />
                    </ListItem>
                  ))}
                </List>
              </Scrollbars>
            )}
          />
        </div>
      </ClickAwayListener>
    );
  }
}

MultiSelectDropdown.defaultProps = {
  defaultValue: [],
};
MultiSelectDropdown.propTypes = {
  defaultValue: PropTypes.array,
};
const mapStateToProps = state => {
  return {
    profileState: state.profile,
    workspaces: state.profile.data.workspace,
  };
};

export default compose(
  withRouter,
  withStyles(combineStyles(sortingDropdownStyles, menuStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps)
)(MultiSelectDropdown);
