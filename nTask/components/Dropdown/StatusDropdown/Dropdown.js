import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { withStyles } from "@material-ui/core/styles";
import RoundIcon from "@material-ui/icons/Brightness1";
import React, { useEffect, useRef, useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import { withRouter } from "react-router-dom";
import { createFilter } from "react-search-input";
import { compose } from "redux";
import { statusData } from "../../../helper/projectDropdownData";
import CustomButton from "../../Buttons/CustomButton";
import CustomIconButton from "../../Buttons/CustomIconButton";
import DefaultTextField from "../../Form/TextField";
import CustomTooltip from "../../Tooltip/Tooltip";
import DropdownMenu from "../DropdownMenu";
import statusDropdownStyle from "./style";

//Onboarding Main Component
function StatusDropdown(props ) {
  const {
    classes,
    theme,
    option,
    options,
    onSelect,
    iconButton,
    style,
    toolTipTxt,
    btnProps,
    disabled,
    legacyButton,
    legacyIconButton,
    buttonType,
    btnStyle,
    iconWithText,
    dropdownProps,
    listButton,
    writeFirst,
    customStatusBtnRender,
    handleCloseCallback,
    scrollHeight,
  } = props;
  const projectStatusData = statusData(theme, classes);
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedOption, setSelectedOption] = useState(option ? option : {});
  const btnRef = useRef(null);
  const [searchTerm, setSearchTerm] = useState("");

  const handleClick = (event) => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = (event) => {
    // Function closes dropdown
    // const {handleCloseCallback}=props
    setSearchTerm("");
    setAnchorEl(null);
    setSearchTerm("");
    // BulkAction priority close function
    if (handleCloseCallback) {
      handleCloseCallback();
    }
  };
  const handleItemClick = (event, option) => {
    //Fuction responsible for selecting item from the list
    if (writeFirst && option.value !== 3 && option.obj && !option.obj.isDoneState)
      setSelectedOption(option); /** if user have selected the completed option */
    setAnchorEl(null);
    setSearchTerm("");
    onSelect(option); // Callback on item select used to lift up option to parent if needed
  };
  const searchUpdated = (e) => {
    setSearchTerm(e.target.value);
  };

  useEffect(() => {
    setSelectedOption(option);
  }, [option]);

  // useEffect(() => {
  //   btnRef.current && btnRef.current.addEventListener("click", handleClick, false);
  //   return () => {
  //     btnRef.current && btnRef.current.removeEventListener("click", handleClick, true);
  //   };
  // });
  const open = Boolean(anchorEl);
  const filteredData = options.filter(createFilter(searchTerm, ["label"]));
  return (
    <>
      {iconButton ? (
        <CustomTooltip
          helptext={toolTipTxt}
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            buttonRef={(node) => {
              btnRef.current = node;
            }}
            onClick={handleClick}
            btnType="transparent"
            variant="contained"
            style={{ ...style }}
            disabled={disabled}>
            {selectedOption.icon}
          </CustomIconButton>
        </CustomTooltip>
      ) : legacyIconButton ? (
        <>
          <CustomIconButton
            btnType="white"
            disabled={disabled}
            buttonRef={(node) => {
              btnRef.current = node;
            }}
            onClick={handleClick}>
            <RoundIcon
              htmlColor={selectedOption.statusColor}
              classes={{ root: classes.btnStatusIcon }}
            />
          </CustomIconButton>
        </>
      ) : legacyButton ? (
        <CustomTooltip
          helptext={toolTipTxt}
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomButton
            style={{
              background: selectedOption.statusColor,
              color:
                selectedOption.statusColor == "#ffffff" ||
                selectedOption.statusColor == "rgb(255 255 255)"
                  ? theme.palette.common.black
                  : theme.palette.common.white,
              ...btnStyle,
            }}
            disabled={disabled}
            buttonRef={(node) => {
              btnRef.current = node;
            }}
            variant="contained"
            btnType="status"
            customClasses={{ disabled: classes.disabledStatusBtn }}
            onClick={handleClick}>
            <span className={classes.statusTitle}>{selectedOption.label}</span>
          </CustomButton>
        </CustomTooltip>
      ) : iconWithText ? (
        <CustomTooltip
          helptext={toolTipTxt}
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            ref={anchorEl}
            onClick={handleClick}
            btnType="transparent"
            variant="contained"
            style={{ ...style }}
            disabled={disabled}>
            {selectedOption.icon}
            <span className={classes.title}>{selectedOption.label}</span>
          </CustomIconButton>
        </CustomTooltip>
      ) : buttonType == "listIconButton" ? (
        <CustomButton
          buttonRef={(node) => {
            btnRef.current = node;
          }} 
          data-rowClick="cell"
          onClick={handleClick}
          btnType="status"
          variant="contained"
          style={{
            background: selectedOption.bgColor,
            padding: "7px 10px 7px 6px",
            borderRadius: 0,
            height: "100%",
            width: "100%",
            lineHeight: "25px",
            ...style,
          }}
          disabled={disabled}
          {...btnProps}>
          {selectedOption.icon && (
            <span className={classes.selectedIcon}>{selectedOption.icon}</span>
          )}
          <span
            style={{
              color: selectedOption.color || theme.palette.common.white,
              fontSize: "13px",
              fontFamily: theme.typography.fontFamilyLato,
              letterSpacing: "normal",
            }}>
            {" "}
            <span className={classes.statusTitle} title={selectedOption.label}>
              {selectedOption.label}
            </span>
          </span>
        </CustomButton>
      ) : buttonType == "listButton" ? (
        <CustomButton
          buttonRef={(node) => {
            btnRef.current = node;
          }}
          data-rowClick="cell"
          onClick={handleClick}
          btnType="status"
          variant="contained"
          style={{
            background: selectedOption.color,
            padding: "9px 10px 9px 6px",
            borderRadius: 0,
            height: "100%",
            width: "100%",
            ...style,
          }}
          disabled={disabled}>
          <span
            style={{
              color:
                selectedOption.color == "#ffffff" || selectedOption.color == "rgb(255 255 255)"
                  ? theme.palette.common.black
                  : theme.palette.common.white,
              width: "90%",
              textOverflow: "ellipsis",
              overflow: "hidden",
              whiteSpace: "nowrap",
              fontSize: "13px",
              fontFamily: theme.typography.fontFamilyLato,
              marginLeft: 4,
              letterSpacing: "normal",
            }}>
            {" "}
            <span className={classes.statusTitle} title={selectedOption.label}>
              {selectedOption.label}
            </span>
          </span>
        </CustomButton>
      ) : customStatusBtnRender ? (
        customStatusBtnRender
      ) : (
        <CustomButton
          buttonRef={(node) => {
            btnRef.current = node;
          }}
          onClick={handleClick}
          btnType="status"
          variant="contained"
          style={{
            background: selectedOption.bgColor,
            padding: "7px 10px 7px 6px",
            borderRadius: 30,
            height: 32,
            ...style,
          }}
          disabled={disabled}>
          <span className={classes.selectedOptionCnt}>{selectedOption.icon}</span>
          <span
            style={{
              color: selectedOption.color,
              fontSize: "13px",
              fontFamily: theme.typography.fontFamilyLato,
              marginLeft: 4,
              letterSpacing: "normal",
            }}>
            <span className={classes.statusTitle} title={selectedOption.label}>
              {selectedOption.label}
            </span>
          </span>
        </CustomButton>
      )}
      {(props.anchorEl && Boolean(props.anchorEl)) || open ? (
        <DropdownMenu
          open={props.anchorEl ? Boolean(props.anchorEl) : open}
          closeAction={handleClose}
          anchorEl={props.anchorEl ? props.anchorEl : anchorEl}
          size="small"
          placement="bottom-start"
          {...dropdownProps}>
          <div>
            <DefaultTextField
              error={false}
              errorState={false}
              errorMessage={""}
              formControlStyles={{ marginBottom: 0 }}
              styles={classes.searchInput}
              defaultProps={{
                type: "text",
                placeholder: `Search`,
                value: searchTerm,
                autoFocus: true,
                inputProps: { maxLength: 80 },
                onChange: (e) => {
                  searchUpdated(e);
                },
                onKeyDown: () => {},
                onBlur: () => {},
              }}
            />
            <List>
              <Scrollbars
                autoHide={false}
                autoHeight
                autoHeightMin={0}
                autoHeightMax={scrollHeight}>
                {filteredData.map((s, i) => {
                  return (
                    <ListItem
                      key={i}
                      button
                      className={classes.listItem}
                      onClick={(event) => handleItemClick(event, s)}>
                      {s.label == selectedOption.label && (
                        <span className={classes.selectedItemBorder}></span>
                      )}
                      <ListItemText
                        classes={{
                          root: classes.listItemTextRoot,
                          primary: classes.listItemText,
                        }}
                        primaryTypographyProps={{
                          style: {
                            color: s.label == selectedOption.label ? theme.palette.text.azure : "",
                          },
                        }}
                        primary={s.label}
                      />
                      {s.icon}
                    </ListItem>
                  );
                })}
              </Scrollbars>
            </List>
          </div>
        </DropdownMenu>
      ) : null}
    </>
  );
}
StatusDropdown.defaultProps = {
  style: {},
  toolTipTxt: "",
  disabled: false,
  legacyButton: false,
  legacyIconButton: false,
  iconButton: false,
  iconWithText: false,
  btnStyle: {},
  writeFirst: true /** pre selection means select option first and then sends value back to parent component  */,
  option: {
    label: "Select",
    value: "Select",
    statusColor: "#b4b4b4",
    icon: null,
  },
  options: [],
  customStatusBtnRender: false,
  handleCloseCallback: () => {},
  scrollHeight: 200,
};
export default compose(
  withRouter,
  withStyles(statusDropdownStyle, { withTheme: true })
)(StatusDropdown);
