const statusDropdownStyle = (theme) => ({
  listItemTextRoot: {
    padding: 0,
  },
  listItemText: {
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
  listItem: {
    padding: "0px 14px",
    position: "relative",
  },
  selectedItemBorder: {
    background: theme.palette.background.blue,
    width: 3,
    position: "absolute",
    left: 0,
    height: "100%",
    borderRadius: "0 4px 4px 0",
  },
  selectedOptionCnt: {
    display: "flex",
    alignItems: "center",
    "& svg": {
      fontSize: "22px !important",
    },
  },
  disabledStatusBtn: {
    color: `${theme.palette.text.medLight} !important`
  },
  btnStatusIcon: {
    fontSize: "16px !important"
  },
  statusTitle: {
    // maxWidth : 50,
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    fontFamily: theme.typography.fontFamilyLato,
    maxWidth: 77
  },
  title:{
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  selectedIcon: {
    lineHeight: '5px',
    marginRight: 6,
    '& svg': {
      fontSize: "14px !important",
    }
  },
  searchInput: {
    // padding: "4px 8px 5px 8px",
    // width: "100%",
    outline: "none",
    border: "none",
    fontSize: "14px !important",
    // lineHeight: "18px",
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "Lato, sans-serif",
    borderRadius: 0,
    cursor: "pointer",
    height: 35,
    margin: "10px 8px 0px 8px"
  },
});

export default statusDropdownStyle;
