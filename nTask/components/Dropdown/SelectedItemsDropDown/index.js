import React, { useState, useEffect, useRef } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import combineStyles from "../../../utils/mergeStyles";
import { withStyles } from "@material-ui/core/styles";
import menuStyles from "../../../assets/jss/components/menu";
import itemStyles from "./styles";
import IconAddColumn from "../../../components/Icons/IconAddColumn";

import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import AddIcon from "@material-ui/icons/Add";
import CustomButton from "../../../components/Buttons/CustomButton";
import { updateCustomFieldDialogState } from "../../../redux/actions/allDialogs";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import SvgIcon from "@material-ui/core/SvgIcon";

import ListItemText from "@material-ui/core/ListItemText";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { FormattedMessage, injectIntl } from "react-intl";
import { Scrollbars } from "react-custom-scrollbars";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import CustomFieldView from "../../../Views/CustomFieldSettings/CustomFields.view";
import { getColumnsList, updateColumn } from "../../../redux/actions/columns";
import { getCustomFields } from "../../../helper/customFieldsData";
import { isEqual } from "lodash/lang";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import DefaultSwitch from "../../Form/Switch";
import CustomListItem from "../../ListItem/CustomListItem";
import CustomMenuList from "../../MenuList/CustomMenuList";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import isEmpty from "lodash/isEmpty";
import searchQuery from "../../../components/CustomTable2/ColumnSettingDropdown/searchQuery";

const ColumnSelectionDropdown = React.memo(props => {
  const {
    classes,
    theme,
    intl,
    total,
    feature,
    columns,
    columnChangeCallback,
    customFields,
    profileState,
    hideColumns,
    updateCustomFieldDialogState,
    onColumnHide,
    workspaceTaskPer,
    issuePer,
    dialogsState,
    riskPer,
    projectPer,
    btnProps = {},
  } = props;
  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState("top-start");
  const [checked, setChecked] = useState([]);
  const [customFieldCreationModal, setCustomFieldCreationModal] = useState(false);
  const [columnsState, setColumnsState] = useState([]);
  const anchorEl = useRef(null);
  const columnStateRef = useRef([]);
  const { customFieldDialog } = dialogsState;

  const handleClose = event => {
    const isExist =
      event.target.id == "taskListCustomizeBtn" || event.target.closest("#taskListCustomizeBtn");
    if (isExist) {
      return;
    }
    setOpen(false);
  };
  const handleClick = (event, placementParam) => {
    setOpen(!open);
  };

  const handleToggle = (e, obj) => {
    e.stopPropagation();
    const updatedObject = {
      ...obj,
      hide: obj.hide ? false : true,
      columnId: obj.id,
      rowGroup: obj.rowGroup ? false : null,
    };
    const updatedColumns = columnStateRef.current.map(x => {
      if (x.columnKey == obj.columnKey) {
        return { ...x, hide: obj.hide ? false : true, rowGroup: obj.rowGroup ? false : null };
      } else {
        return x;
      }
    });
    setColumnsState(updatedColumns);
    columnStateRef.current = updatedColumns;
    onColumnHide(obj);
    props.updateColumn([updatedObject], feature, null);
  };

  useEffect(() => {
    setColumnsState(columns[feature]);
    columnStateRef.current = columns[feature];
    columnChangeCallback();
  }, [columns[feature]]);

  useEffect(() => {
    if (customFieldDialog.moduleViewType == "") setCustomFieldCreationModal(false);
  }, [customFieldDialog]);

  const onClickAddNewField = e => {
    e.stopPropagation();
    setCustomFieldCreationModal(true);
    updateCustomFieldDialogState({ moduleViewType: "optionsSelect" });
    setOpen(false);
  };

  const getCustomFieldsPermissions = type => {
    let permissionObj = {
      isAllowEdit: false,
      isAllowDelete: false,
      isAllowAdd: false,
      isAllowUpdate: false,
      isAllowHide: false,
      isUseField: false,
    };
    switch (type) {
      case "task":
        permissionObj = workspaceTaskPer
          ? { ...workspaceTaskPer.taskDetail.customsFields }
          : permissionObj;
        (permissionObj.isAllowUpdate = workspaceTaskPer
          ? workspaceTaskPer.taskDetail.isUpdateField.cando
          : false),
          (permissionObj.isAllowHide = workspaceTaskPer
            ? workspaceTaskPer.taskDetail.isHideField.cando
            : false),
          (permissionObj.isUseField = workspaceTaskPer
            ? workspaceTaskPer.taskDetail.isUseField.cando
            : false);
        break;
      case "issue":
        permissionObj = issuePer ? { ...issuePer.issueDetail.customsFields } : permissionObj;
        (permissionObj.isAllowUpdate = issuePer ? issuePer.issueDetail.isUpdateField.cando : false),
          (permissionObj.isAllowHide = issuePer ? issuePer.issueDetail.isHideField.cando : false),
          (permissionObj.isUseField = issuePer ? issuePer.issueDetail.isUseField.cando : false);
        break;
      case "risk":
        permissionObj = riskPer ? { ...riskPer.riskDetails.customsFields } : permissionObj;
        (permissionObj.isAllowUpdate = riskPer ? riskPer.riskDetails.isUpdateField.cando : false),
          (permissionObj.isAllowHide = riskPer ? riskPer.riskDetails.isHideField.cando : false),
          (permissionObj.isUseField = riskPer ? riskPer.riskDetails.isUseField.cando : false);
        break;
      case "project":
        permissionObj = projectPer ? { ...projectPer.projectDetails.customsFields } : permissionObj;
        (permissionObj.isAllowUpdate = projectPer ? projectPer.projectDetails.isUpdateField.cando : false),
          (permissionObj.isAllowHide = projectPer ? projectPer.projectDetails.isHideField.cando : false),
          (permissionObj.isUseField = projectPer ? projectPer.projectDetails.isUseField.cando : false);
        break;

      default:
        break;
    }
    return permissionObj;
  };

  const activeFields = getCustomFields(customFields, profileState.data, feature)
    .filter(f => !hideColumns.includes(f.fieldType.toLowerCase()))
    .map(c => c.fieldId);
  let columnsList =
    columnStateRef.current && columnStateRef.current.length
      ? columnStateRef.current.filter(
        c => activeFields.includes(c.fieldId) || !c.fieldId || c.isSystem
      )
      : [];
  columnsList =
    feature == "task"
      ? columnsList.filter(
        item =>
          item.fieldType !== "textarea" &&
          item.fieldType !== "section" &&
          item.columnKey !== "taskTitle" &&
          item.columnKey !== "workspaceName"
      )
      : feature == "project"
        ? columnsList.filter(
          item =>
            item.fieldType !== "textarea" &&
            item.fieldType !== "section" &&
            item.columnKey !== "projectName" &&
            item.columnKey !== "workspaceName"
        )
        : feature == "issue"
          ? columnsList.filter(
            item =>
              item.fieldType !== "textarea" &&
              item.fieldType !== "section" &&
              item.columnKey !== "title"
          )
          : feature == "meeting"
            ? columnsList.filter(
              item =>
                item.columnKey !== "meetingDisplayName" &&
                // item.columnKey !== "uniqueId" &&
                item.fieldType !== "textarea" &&
                item.fieldType !== "section"
            ) : feature == "risk"
              ? columnsList.filter(
                item =>
                  item.fieldType !== "textarea" &&
                  item.fieldType !== "section" &&
                  item.columnKey !== "title"
              )
              : columnsList.filter(
                item =>
                  !item.fieldType ||
                  (item.fieldType == "textfield" &&
                    item.fieldType == "dropdown" &&
                    item.fieldType == "matrix")
              );
  const selectedItems = columnsList && columnsList.filter(c => !c.hide).length;
  const customFieldsPermissions = getCustomFieldsPermissions(feature);

  return (
    <>
      <CustomIconButton
        btnType="condensed"
        onClick={event => {
          handleClick(event, "bottom-end");
        }}
        id="taskListCustomizeBtn"
        style={{ width: 37 }}
        buttonRef={node => {
          anchorEl.current = node;
        }}
        {...btnProps}>
        <SvgIcon viewBox="0 0 16 16" className={classes.columnSettingIcon}>
          <IconAddColumn />
        </SvgIcon>
      </CustomIconButton>
      {teamCanView("customFieldAccess") && customFieldCreationModal && (
        <div className={classes.addCustomFieldCnt}>
          <CustomFieldView
            feature={feature}
            onFieldAdd={() => { }}
            onFieldEdit={() => { }}
            onFieldDelete={() => { }}
            permission={customFieldsPermissions}
            disableAssessment={true}
            disabled={false}
            customBtnRenderer={<div> </div>}
          />
        </div>
      )}
      {/*{open && anchorEl.current &&*/}
      <SelectionMenu
        isAddColSelectionClicked={true}
        open={open}
        closeAction={handleClose}
        checkedType="multi"
        // keepMounted={true}
        anchorRef={anchorEl.current}
        // disablePortal={true}
        list={
          <>
            <div className={classes.columnCountContainer}>
              {teamCanView("customFieldAccess") &&
                (feature == "task" || feature == "issue" || feature == "project" || feature == "risk") &&
                isEmpty(searchQuery.quickFilters["Archived"]) && (
                  <>
                    <CustomButton
                      onClick={onClickAddNewField}
                      ref={null}
                      btnType={"plain"}
                      labelAlign="left"
                      variant="text"
                      disabled={false}
                      customClasses={{
                        text: classes.btnStyles,
                      }}>
                      <AddIcon className={classes.addIcon} />
                      <span className={classes.btnTxt}> Add/Edit Field</span>
                    </CustomButton>
                  </>
                )}
              <div className={classes.hl}></div>
              <span className={classes.columnCountLabel}>
                {`Customize Column (${selectedItems}/${columnsList.length})`}
              </span>
            </div>
            <Scrollbars autoHide style={{ height: 320 }}>
              <CustomMenuList>
                {columnsList.map(c => {
                  return (
                    <CustomListItem
                      key={c.columnKey + c}
                      isSelectedItemDropdownClicked={true}
                      darkmode={false}
                      compactMode={true}
                      rootProps={{ onClick: null, button: false }}
                      // icon={<DragIndicatorIcon />}
                      endIcon={
                        <div>
                          <DefaultSwitch
                            size={"small"}
                            checked={!c.hide}
                            onChange={e => handleToggle(e, c, c.fieldType)}
                            rootProps={{
                              button: false,
                              disableRipple: true,
                              style: { cursor: "default" },
                            }}
                          // value="darkModeChecked"
                          />
                        </div>
                      }
                    >
                      {c.columnName}
                    </CustomListItem>
                  );
                })}
              </CustomMenuList>
            </Scrollbars>
          </>
        }
      />
      {/*}*/}
    </>
  );
}, areEqual);

function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}

const mapStateToProps = state => {
  return {
    columns: state.columns,
    customFields: state.customFields,
    profileState: state.profile,
    itemOrderState: state.itemOrder,
    workspaceTaskPer: state.workspacePermissions.data.task,
    dialogsState: state.dialogStates,
    issuePer: state.workspacePermissions.data.issue,
    riskPer: state.workspacePermissions.data.risk,
    projectPer: state.workspacePermissions.data.project,
  };
};

ColumnSelectionDropdown.defaultProps = {
  columnChangeCallback: () => { },
  hideColumns: [],
  onColumnHide: () => { },
  btnProps: {},
};
export default compose(
  injectIntl,
  withRouter,
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    getColumnsList,
    updateColumn,
    updateCustomFieldDialogState,
  })
)(ColumnSelectionDropdown);
