import React, { useContext, useState, useEffect, useRef } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import combineStyles from "../../../utils/mergeStyles";
import { withStyles } from "@material-ui/core/styles";
import menuStyles from "../../../assets/jss/components/menu";
import itemStyles from "./styles";
import IconAddColumn from "../../Icons/IconAddColumn";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import CustomIconButton from "../../Buttons/IconButton";
import SelectionMenu from "../../Menu/SelectionMenu";
import AddIcon from "@material-ui/icons/Add";
import CustomButton from "../../Buttons/CustomButton";
import { updateCustomFieldDialogState } from "../../../redux/actions/allDialogs";
import { teamCanView } from "../../PlanPermission/PlanPermission";
import SvgIcon from "@material-ui/core/SvgIcon";

import ListItemText from "@material-ui/core/ListItemText";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { FormattedMessage, injectIntl } from "react-intl";
import { Scrollbars } from "react-custom-scrollbars";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import CustomFieldView from "../../../Views/CustomFieldSettings/CustomFields.view";
import { getColumnsList, updateTeamColumn } from "../../../redux/actions/columns";
import { getCustomFields } from "../../../helper/customFieldsData";
import { isEqual } from "lodash/lang";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import DefaultSwitch from "../../Form/Switch";
import CustomListItem from "../../ListItem/CustomListItem";
import CustomMenuList from "../../MenuList/CustomMenuList";

const TeamColumnSelectionDropdown = React.memo(props => {
  const {
    classes,
    theme,
    intl,
    total,
    feature,
    groupType,
    columnChangeCallback,
    customFields,
    profileState,
    hideColumns,
    updateCustomFieldDialogState,
    onColumnHide,
    workspaceTaskPer,
    dialogsState,
    allColumns,
    updateColumn,
    btnProps = {},
  } = props;
  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState("top-start");
  const [checked, setChecked] = useState([]);
  const [customFieldCreationModal, setCustomFieldCreationModal] = useState(false);
  const anchorEl = useRef(null);
  const { customFieldDialog } = dialogsState;

  // get all comlumns from context
  // const { state: { allColumns }, dispatch, } = useContext(IssueOverviewContext);
  const handleClose = event => {
    const isExist =
      event.target.id == "taskListCustomizeBtn" || event.target.closest("#taskListCustomizeBtn");
    if (isExist) {
      return;
    }
    setOpen(false);
  };
  const handleClick = (event, placementParam) => {
    setOpen(!open);
  };

  const handleToggle = (e, obj) => {
    e.stopPropagation();
    const updatedObject = {
      ...obj,
      hide: !obj.hide,
      columnId: obj.id,
      rowGroup: obj.rowGroup ? false : null,
    };
    const updatedColumns = allColumns.map(x => {
      if (x.columnKey == obj.columnKey) {
        return { ...x, hide: !obj.hide, rowGroup: obj.rowGroup ? false : null };
      } else {
        return x;
      }
    });
    // console.log(updatedColumns);
    // setColumnsState(updatedColumns);
    // (feature, groypType, data, success, fail)
    // callback to dispatch in respective context
    updateColumn(updatedColumns);
    updateTeamColumn(
      feature,
      groupType,
      [updatedObject],
      res => {
        // onColumnHide(updatedObject);
        // onColumnHide(res);
      },
      fail => {
        console.log("api fail");
      }
    );
  };

  useEffect(() => {
    // setColumnsState(allColumns);
    columnChangeCallback();
  }, [allColumns]);

  const activeFields = getCustomFields(customFields, profileState.data, feature)
    .filter(f => !hideColumns.includes(f.fieldType.toLowerCase()))
    .map(c => c.fieldId);
  // let columnsList =
  //   allColumns && allColumns.length
  //     ? allColumns.filter(
  //       c => (c.fieldId && activeFields.includes(c.fieldId)) || !c.fieldId
  //     )
  //     : [];
  // columnsList =
  //   feature == "task"
  //     ? columnsList.filter(
  //       item =>
  //         item.fieldType !== "textarea" &&
  //         item.fieldType !== "section" &&
  //         item.columnKey !== "taskTitle" &&
  //         item.columnKey !== "workspaceName"
  //     )
  //     : feature == "project"
  //       ? columnsList.filter(item => !item.fieldType)
  //       : columnsList.filter(
  //         item =>
  //           !item.fieldType ||
  //           item.fieldType == "textfield" ||
  //           item.fieldType == "dropdown" ||
  //           item.fieldType == "matrix"
  //       );
  const selectedItems = allColumns && allColumns.filter(c => !c.hide).length;

  return (
    <ClickAwayListener onClickAway={handleClose}>
      <div>
        <CustomIconButton
          btnType="condensed"
          onClick={event => {
            handleClick(event, "bottom-end");
          }}
          id="taskListCustomizeBtn"
          style={{ width: 37 }}
          buttonRef={node => {
            anchorEl.current = node;
          }}
          {...btnProps}>
          <SvgIcon viewBox="0 0 16 16" className={classes.columnSettingIcon}>
            <IconAddColumn />
          </SvgIcon>
        </CustomIconButton>

        {/*{open && anchorEl.current &&*/}
        <SelectionMenu
          open={open}
          closeAction={handleClose}
          checkedType="multi"
          // keepMounted={true}
          anchorRef={anchorEl.current}
          // disablePortal={true}
          list={
            <>
              <div className={classes.columnCountContainer}>
                <div className={classes.hl}></div>
                <span className={classes.columnCountLabel}>
                  {`Customize Column (${selectedItems}/${allColumns.length})`}
                </span>
              </div>
              <Scrollbars autoHide style={{ height: 320 }}>
                <CustomMenuList>
                  {allColumns.map((c, i) => {
                    return (
                      <CustomListItem
                        isSelectedItemDropdownClicked={true}
                        key={c.columnKey}
                        rootProps={{ onClick: null, button: false }}
                        // icon={<DragIndicatorIcon />}
                        endIcon={
                          <DefaultSwitch
                            size={"small"}
                            checked={!c.hide}
                            onChange={e => handleToggle(e, c, c.fieldType)}
                            rootProps={{
                              button: false,
                              disableRipple: true,
                              style: { cursor: "default" },
                            }}
                          // value="darkModeChecked"
                          />
                        }>
                        {c.columnName}
                      </CustomListItem>
                    );
                  })}
                </CustomMenuList>
              </Scrollbars>
            </>
          }
        />
        {/*}*/}
      </div>
    </ClickAwayListener>
  );
}, areEqual);

function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}

const mapStateToProps = state => {
  return {
    // columns: state.columns,
    customFields: state.customFields,
    profileState: state.profile,
    itemOrderState: state.itemOrder,
    workspaceTaskPer: state.workspacePermissions.data.task,
    dialogsState: state.dialogStates,
  };
};

TeamColumnSelectionDropdown.defaultProps = {
  columnChangeCallback: () => { },
  hideColumns: [],
  onColumnHide: () => { },
  btnProps: {},
};
export default compose(
  injectIntl,
  withRouter,
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    getColumnsList,
    updateCustomFieldDialogState,
  })
)(TeamColumnSelectionDropdown);
