// @flow

import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Scrollbars } from "react-custom-scrollbars";
import dropdownStyle from "./style";
import DefaultCheckbox from "../../Form/Checkbox";
import SearchInput, { createFilter } from "react-search-input";
import DefaultTextField from "../../Form/TextField";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import ArrowRight from "@material-ui/icons/ArrowRight";
import clsx from "clsx";
import CircularIcon from "@material-ui/icons/Brightness1";

// Onboarding Main Component
function CustomMultiSelectDropdown(props) {
  const {
    classes,
    option,
    options,
    onSelect,
    scrollHeight,
    maxSelections,
    placeholder = null,
    valueSelector,
    optionIcon,
  } = props;

  const optionsData = options();
  const [selectedOptions, setSelectedOptions] = useState([]);
  const [selectAll, setSelectAll] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [data, setData] = useState(optionsData);

  const handleItemClick = (event, optionSelected) => {
    // Fuction responsible for selecting item from the list
    
    const isSelected = selectedOptions.find(
      s => s[valueSelector] === optionSelected[valueSelector] && s['parentId'] === optionSelected['parentId'] && s['rootId'] === optionSelected['rootId']
    );

    if (isSelected) {
      const newSelectedOptions = selectedOptions.filter(
        o => (o[valueSelector] !== optionSelected[valueSelector] && o['parentId'] == optionSelected['parentId']) || (o['parentId'] !== optionSelected['parentId']) || (o['rootId'] !== optionSelected['rootId'])
      );
      setSelectAll(false);
      setSelectedOptions(newSelectedOptions);
      onSelect(newSelectedOptions); // Callback on item select used to lift up option to parent if needed
    } else {
      //return in case of maximum selections reached
      if (maxSelections && selectedOptions.length === maxSelections) {
        return;
      }
      setSelectAll(false);
      setSelectedOptions(
        selectedOptions.length ? [...selectedOptions, optionSelected] : [optionSelected]
      );
      onSelect(selectedOptions.length ? [...selectedOptions, optionSelected] : [optionSelected]); // Callback on item select used to lift up option to parent if needed
    }
  };

  const handleSelectAllOption = (e, keyOption) => {
    //return in case of maximum selections reached
    if (keyOption) {
      setSelectAll(false);
      setSelectedOptions([]);
      onSelect([]); // Callback on item select used to lift up option to parent if needed
    } else {
      let allSelectedOptions = data.reduce(function(res, cv) {
        let statuslist = cv.child.map(c => c.child);
        statuslist = [].concat.apply([], statuslist);
        return [...res, ...statuslist];
      }, []);
      setSelectAll(true);
      setSelectedOptions(allSelectedOptions);
      onSelect(allSelectedOptions); // Callback on item select used to lift up option to parent if needed
    }
  };

  const searchUpdated = e => {
    setSearchTerm(e.target.value);
  };

  const handleOpenParent = param => {
    const updatedData = data.map(temp => {
      if (temp.id === param.id) {
        return { ...temp, open: !temp.open };
      }
      return temp;
    });
    setData(updatedData);
  };
  const handleOpenChild = (parent, item) => {
    const updatedData = data.map(temp => {
      if (temp.id === parent.id) {
        let statusList = temp.child.map(t => {
          if (t.id === item.id) return { ...t, open: !t.open };
          return t;
        });
        return {...temp, child : statusList};
      }
      return temp;
    });
    setData(updatedData);
  };

  useEffect(() => {
    if (option) {
      setSelectedOptions(option);
      if (option.length === data.length) setSelectAll(true);
    }
  }, [option]);

  useEffect(() => {
    setData(optionsData);
    return () => {
      setData([]);
    };
  }, [optionsData]);

  const searchResults = () => {
    let filteredData = data.reduce(function(res, cv) {
      let parentElement = cv.child.map(function(item) {
        let childElement = item.child.filter(function(temp) {
          return temp.label.toLowerCase().includes(searchTerm.toLowerCase());
        });
        return { ...item, child: !searchTerm ? item.child : childElement , open : !searchTerm ? item.open : true };
      });
      res.push({
        ...cv,
        open : !searchTerm ? cv.open : true,
        child: parentElement.filter(item => item.child.length !== 0), /** filter those elements whose result not found or child length is 0 */
      });
      return res;
    }, []).filter(item => item.child.length !== 0); /** filter those parent elements whose child array length is 0*/
    return filteredData;
  };

  const filteredData = searchResults();

  return (
    <>
      <List>
        <ListItem disableRipple={true}>
          <DefaultTextField
            error={false}
            errorState={false}
            errorMessage={""}
            formControlStyles={{ marginBottom: 0 }}
            styles={classes.searchInput}
            defaultProps={{
              type: "text",
              placeholder: `Search ${placeholder || ""}`,
              value: searchTerm,
              autoFocus: true,
              inputProps: { maxLength: 80 },
              onChange: e => {
                searchUpdated(e);
              },
              onKeyDown: () => {},
              onBlur: () => {},
            }}
          />
        </ListItem>
        {filteredData.length !== 0 && (
          <ListItem
            key={"allOptions"}
            button
            className={classes.listItemSelectOption}
            onClick={e => {
              handleSelectAllOption(e, selectAll);
            }}>
            <div className={classes.itemTextCnt}>
              <DefaultCheckbox
                checkboxStyles={{ padding: "0 5px 0 0" }}
                checked={selectAll}
                onChange={() => {}}
                unCheckedIconProps={{
                  style: {
                    fontSize: "18px !important",
                  },
                }}
                checkedIconProps={{
                  style: {
                    fontSize: "18px !important",
                  },
                }}
                color={"#0090ff"}
                disabled={false}
              />
              <span className={classes.itemText}>{selectAll ? "Unselect All" : "Select All"}</span>
            </div>
          </ListItem>
        )}
        {filteredData.length == 0 && (
          <div className={classes.noDataContainer}>
            <span> No records found </span>
          </div>
        )}
        <Scrollbars autoHide={false} autoHeight autoHeightMin={0} autoHeightMax={scrollHeight}>
          <div style={{ padding: "0px 5px" }}>
            {filteredData.map((item, index) => {
              return (
                <div className={classes.statusCnt} key={index}>
                  <span className={classes.title} onClick={() => handleOpenParent(item)}>
                    {item.open ? (
                      <DropdownArrow style={{ color: "#8d8d8d" }} />
                    ) : (
                      <ArrowRight style={{ color: "#8d8d8d" }} />
                    )}
                    {item.name}
                  </span>
                  <div
                    className={clsx({
                      [classes.show]: item.open,
                      [classes.hide]: !item.open,
                    })}>
                    {item.child.map((tem, indexNum) => {
                      return (
                        <div
                          className={clsx({
                            [classes.statusCnt]: true,
                          })}>
                          <span
                            className={classes.titleLabel}
                            key={indexNum}
                            onClick={() => handleOpenChild(item, tem)}>
                            {tem.open ? (
                              <DropdownArrow style={{ color: "#8d8d8d" }} />
                            ) : (
                              <ArrowRight style={{ color: "#8d8d8d" }} />
                            )}
                            {tem.name != 'nTask Standard' ? tem.name : 'Standard'}
                            {/* {tem.name} */}
                          </span>
                          <div
                            className={clsx({
                              [classes.show]: tem.open,
                              [classes.hide]: !tem.open,
                            })}>
                            {tem.child.map((o, i) => {
                              const isSelected =
                                selectedOptions.find(
                                  so => o[valueSelector] === so[valueSelector] && so['parentId'] === tem.id && so['rootId'] === item.id
                                ) || false;
                              return (
                                <ListItem
                                  key={i}
                                  button
                                  className={classes.listItem}
                                  onClick={event => handleItemClick(event, o, tem.id, item.id)}>
                                  <div className={classes.itemTextCnt}>
                                    <DefaultCheckbox
                                      checkboxStyles={{ padding: "0 5px 0 0" }}
                                      checked={isSelected ? true : false}
                                      onChange={() => {}}
                                      unCheckedIconProps={{
                                        style: {
                                          fontSize: "18px !important",
                                        },
                                      }}
                                      checkedIconProps={{
                                        style: {
                                          fontSize: "18px !important",
                                        },
                                      }}
                                      color={"#0090ff"}
                                      disabled={false}
                                    />
                                    {optionIcon &&  <CircularIcon htmlColor={o.color || 'black'} style={{ fontSize: "12px !important", marginRight: 5 }} />}
                                    <span className={classes.itemText}>{o.label || "-"}</span>
                                  </div>
                                </ListItem>
                              );
                            })}
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              );
            })}
          </div>
        </Scrollbars>
      </List>
    </>
  );
}
CustomMultiSelectDropdown.defaultProps = {
  size: "small",
  scrollHeight: 180,
  valueSelector: "label",
};
export default compose(
  withRouter,
  withStyles(dropdownStyle, { withTheme: true })
)(CustomMultiSelectDropdown);
