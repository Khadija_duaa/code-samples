// @flow

import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import dropdownStyle from "./style";
import SearchInput, { createFilter } from "react-search-input";
import DefaultTextField from "../../Form/TextField";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import ArrowRight from "@material-ui/icons/ArrowRight";
import clsx from "clsx";
import CircularIcon from "@material-ui/icons/Brightness1";
import CustomButton from "../../Buttons/CustomButton";
import DropdownMenu from "../DropdownMenu";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Dropdown from "./Dropdown";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import RemoveIcon from "@material-ui/icons/Cancel";


// Onboarding Main Component
function CustomMultiSelectGropedDropdown(props) {
  const {
    classes,
    theme,
    option,
    options,
    onSelect,
    scrollHeight,
    maxSelections,
    placeholder = null,
    valueSelector,
    taskStatusGroup,
    optionIcon,
    label,
  } = props;



  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = (event) => {
    // Function closes dropdown
    setAnchorEl(null);
  };

  const removeItemClick = (event, item) => {
    event.preventDefault();
    // Fuction responsible for selecting item from the list 
    const newSelectedOptions = option.filter(
      o => (o[valueSelector] !== item[valueSelector] && o['parentId'] == item['parentId'])
        || (o['parentId'] !== item['parentId']) || (o['rootId'] !== item['rootId'])
    );
    onSelect(newSelectedOptions); // Callback on item select used to lift up option to parent if needed

  };

  const open = Boolean(anchorEl);
  return (
    <>
      <ClickAwayListener
        onClickAway={handleClose}
      >
        <FormControl
          fullWidth
          classes={{ root: classes.reactSelectFormControl }}>
            <InputLabel
              // ref={anchorEl}
              // onClick={handleClick}
              htmlFor="AssignedToLabel"
              className={clsx({
                [classes.dropdownLabel]: true,
                [classes.dropdownLabelOpen]: open,
              })}
              shrink={false}>
              {label}
            </InputLabel>
            <div
              className={classes.inputWrapper}
              onClick={handleClick}
              ref={anchorEl}>
              {!option.length ? <span className={classes.placeholder}>Set Status</span> : null}
              {option && option.map(item => {
                return (
                  <span className={classes.inputTag}>
                    {optionIcon && <CircularIcon htmlColor={item.color || 'black'} style={{ fontSize: "12px", marginRight: 5 }} />}
                    {item.label}
                    <span className={classes.removeBtn}
                      onClick={(e) => removeItemClick(e, item)}
                    >
                      <RemoveIcon
                        htmlColor={theme.palette.secondary.medDark}
                      />
                    </span>
                  </span>
                )
              })}
            </div>
            <DropdownArrow className={classes.dropdownArrrow}/>
            <DropdownMenu
              open={open}
              // closeAction={handleClose}
              anchorEl={anchorEl}
              clickAway={false}
              size={'large'}
              style={{ width: '100%' }}
              placement="bottom-start"
            >
              <Dropdown
                label={label}
                option={option}
                options={options}
                optionIcon={optionIcon}
                onSelect={onSelect}
                placeholder=""
                height="140px"
                width="140px"
                valueSelector={valueSelector}
                scrollHeight={300}
              />
            </DropdownMenu>
        </FormControl>
      </ClickAwayListener>
    </>
  );
}
CustomMultiSelectGropedDropdown.defaultProps = {
  size: "small",
  scrollHeight: 180,
  valueSelector: "label",
};
export default compose(
  withRouter,
  withStyles(dropdownStyle, { withTheme: true })
)(CustomMultiSelectGropedDropdown);
