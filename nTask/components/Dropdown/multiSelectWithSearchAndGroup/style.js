const dropdownStyle = theme => ({
  reactSelectFormControl: {
    marginTop: 22,
    marginBottom: 10,
  },
  dropdownLabel: {
    fontSize: "13px !important",
    color: theme.palette.text.darkGray,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    transform: 'translate(2px, -17px) scale(1)',
  },
  dropdownLabelOpen: {
    color: theme.palette.text.green,
  },
  dropdownValue: {
    fontSize: "13px !important",
    color: "#202020",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    marginLeft: 5,
    display: "flex",
    justifyContent: "space-between",
    flex: 1,
  },
  dropdownArrrow:{
    // color: theme.palette.secondary.medDark,
    color: "#BFBFBF",
    position: 'absolute',
    top: '50%',
    right: '5px',
    transform: 'translateY(-50%)',
  },
  itemText: {
    fontSize: "13px !important",
    // color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    lineHeight: "normal",
  },
  itemTextSelected: {
    fontSize: "13px !important",
    // color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    lineHeight: "normal",
    // color: theme.palette.text.azure,
  },
  itemTextCaption: {
    fontSize: "12px !important",
    // color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    marginTop: 7,
  },
  listItem: {
    padding: "0 14px 6px 14px",
    position: "relative",
  },
  listItemSelectOption: {
    padding: "0 14px 6px 14px",
    position: "relative",
    borderBottom: "1px solid #dddddd",
    marginBottom: 5,
  },
  selectedItemBorder: {
    background: theme.palette.background.blue,
    width: 4,
    position: "absolute",
    left: 0,
    height: "92%",
    borderRadius: "0 4px 4px 0",
  },
  selectedItemBorderWithCaption: {
    background: theme.palette.background.blue,
    width: 3,
    position: "absolute",
    left: 0,
    height: 50,
    borderRadius: "0 4px 4px 0",
  },
  itemTextCnt: {
    display: "flex",
    // flexDirection: "column",
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  ddIcon: {
    marginBottom: -7,
    color: "#BFBFBF",
    marginLeft: -4,
  },
  dropdownHeading: {
    padding: "8px 14px 0 14px",
    color: theme.palette.text.light,
  },
  btnContainer: {
    // display: "flex",
    // flexDirection : "column",
    // minWidth: 200,
  },
  searchInput: {
    // padding: "4px 8px 5px 8px",
    width: "100%",
    outline: "none",
    border: "none",
    fontSize: "14px !important",
    // lineHeight: "18px",
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "Lato, sans-serif",
    borderRadius: 0,
    cursor: "pointer",
    height: 35,
  },
  noDataContainer: {
    textAlign: "center",
    padding: 5,
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "Lato, sans-serif",
    fontSize: "13px !important",
  },
  statusCnt: {
    display: "flex",
    flexDirection: "column",
  },
  title: {
    fontSize: "13px !important",
    fontFamily: "'lato', sans-serif",
    fontWeight: 600,
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  titleLabel: {
    fontSize: "13px !important",
    fontFamily: "'lato', sans-serif",
    fontWeight: 600,
    display: "flex",
    alignItems: "center",
    marginLeft: 15,
    cursor: "pointer",
  },
  show: {
    display: "block",
  },
  hide: {
    display: "none",
  },
  inputWrapper: {
    color: '#202020',
    fontSize: "12px !important",
    border: '1px solid #DDDDDD !important',
    padding: "5px 24px 5px 6px",
    borderRadius: 4,
    minHeight: 40,
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  placeholder: {
    marginRight: 2,
    fontSize: "13px !important",
    color: theme.palette.text.hint
  },
  inputTag: {
    padding: ' 2px 8px',
    display: 'inline-flex',
    fontSize: "14px !important",
    alignItems: 'center',
    // color: theme.palette.text.medGray,
    marginTop: "4px",
    marginRight: "4px"
  },
  removeBtn: {
    '& svg': {
      fontSize: "16px !important",
    }
  },

});

export default dropdownStyle;
