import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import dashboardStyles from "../../Views/Project/styles";
import menuStyles from "../../assets/jss/components/menu";
import CustomIconButton from "../Buttons/IconButton";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import SelectionMenu from "../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../utils/mergeStyles";
import ImportExport from "@material-ui/icons/ImportExport";
import ActionConfirmation from "../Dialog/ConfirmationDialogs/ActionConfirmation";
import ExportColumn from "../Dialog/ExportColumn/ExportColumn";
import ImportBulkDialog from "../Dialog/DragnDropFileDialog/CsvUploadDialog";
import helper from "../../helper/index";
//import { ExportProjectsToCSV } from "../../redux/actions/projects";
import { ExportToCSV } from "../../redux/actions/ImportExport";
import { GetPermission } from "../permissions";
import { injectIntl, FormattedMessage } from "react-intl";
import MsUploadDialog from '../../Views/Project/Gantt/MsUploadDialog';

class ImportExportDD extends Component {
  constructor(props) {
    super(props); //??
    this.state = {
      selectMenuFlag: false,
      placement: "",
      exportFlag: false,
      importmsproject: false,
      importFlag: false,
      showMessage: "",
      exportBtnQuery: "",
    };
  }

  handleCloseClickAwayListener = () => {
       this.setState({ selectMenuFlag: false });
  }
  handleClose = () => {
    this.setState({ exportFlag: false });
  };

  closeImportBulkDialogue = () => {
    this.setState({ importFlag: false });
  };
  closeImportMSProjectDialogue = () => {
    this.setState({ importmsproject: false });
  };

  handleExportType = (message) => {
    let statedData = {
      selectMenuFlag: false,
      exportFlag: false,
    };
    if (message) {
      statedData.showMessage = message;
    }
    this.setState(statedData, () => {
      this.props.handleExportType(this.state.showMessage);
    });
  };

  openDialogue = (value) => {
    console.log(value);
    switch (value) {
      case "Export CSV":
        this.setState({ exportFlag: true, selectMenuFlag: false });
        break;
      case "Import Bulk Projects":
        this.setState({ importFlag: true, selectMenuFlag: false });
        break;
      case "Import from MS Project":
        this.setState({ importmsproject: true, selectMenuFlag: false });
        break;
      default:
        this.setState({ importFlag: true, selectMenuFlag: false });
        break;
    }
  };

  handleClick = (event, placement) => {
    this.setState((state) => ({
      selectMenuFlag: state.placement !== placement || !state.selectMenuFlag,
      placement,
    }));
  };

  showSnackMessage = (message, type) => {
    this.props.handleExportType(message, type);
  };

  handleExport = () => {
    const type = this.props.ImportExportType;
    let exportedObject = {},
      fileName = `${this.props.companyInfo ? this.props.companyInfo.companyName : "nTask"}-${type
        .charAt(0)
        .toUpperCase()}${type.slice(1)}.xlsx`;
    let Url = "";

    if (type == "project") {
      exportedObject = { isArchived: false };
      if (this.props.isArchived) {
        exportedObject = { isArchived: true };
      }
      Url = "api/project/ExportProjects";
    }
    if (type == "issue") {
      exportedObject = exportedObject = {
        issues: this.props.data.map((i) => i.id),
      };
      Url = "/api/IssueRisk/ExportIssues";
    }
    if (type == "risk") {
      exportedObject = { risks: this.props.data.map((r) => r.id) };
      Url = "/api/IssueRisk/ExportRisks";
    }
    if (type == "meeting") {
      exportedObject = { MeetingIds: this.props.data.map((m) => m.meetingId) };
      Url = "/api/Meeting/ExportMeetings";
    }
    if (type == "task") {
      exportedObject = {
        TaskIds: this.props.data.map((t) => t.taskId),
        projectId: this.props.projectId,
      };
      // this.getExportTaskDetailsObject(exportedObject, selectedProject);
      Url = "/api/UserTask/ExportUserTasks";
    }
    this.setState({ exportBtnQuery: "progress" }, () => {
      this.props.ExportToCSV(
        exportedObject,
        Url,
        (response) => {
          this.setState({ exportBtnQuery: "", exportFlag: false });
          this.props.handleExportType(
            this.props.intl.formatMessage({
              id: "common.successMessages.file-export",
              defaultMessage: "File exported successfully.",
            }),
            "success"
          );
          helper.DOWNLOAD_TEMPLATE(response.data, fileName, "application/vnd.ms-excel");
        },
        (err) => {
          let error = err.data.message
            ? err.data.message
            : "Oops! No records found to be exported.";
          this.setState({ exportBtnQuery: "", exportFlag: false });
          this.props.handleExportType(error, "error");
        }
      );
    });
  };

  getDropDownData = (type) => {
    const { getPermission } = this;
    const {
      projectPer = {},
      meetingPer = {},
      issuePer = {},
      riskPer = {},
      taskPer = {},
    } = this.props;

    const ddData = [];
    switch (type) {
      case "project":
        if (projectPer.exportProject.cando) {
          ddData.push("Export CSV");
        }
        if (projectPer.importProject.cando) {
          const listValue = `Import Bulk ${type.charAt(0).toUpperCase()}${type.slice(1)}s`;
          ddData.push(listValue);
        }
        ddData.push("Import from MS Project");
        return ddData;
        break;

      case "task":
        if (taskPer.exportTasks && taskPer.exportTasks.cando) {
          ddData.push("Export CSV");
        }
        if (taskPer.importTasks && taskPer.importTasks.cando) {
          const listValue = `Import Bulk ${type.charAt(0).toUpperCase()}${type.slice(1)}s`;
          ddData.push(listValue);
        }
        return ddData;
        break;

      case "board":
        if (taskPer.exportTasks && taskPer.exportTasks.cando) {
          ddData.push("Export CSV");
        }
        if (taskPer.importTasks && taskPer.importTasks.cando) {
          const listValue = `Import Bulk tasks`;
          ddData.push(listValue);
        }
        return ddData;
        break;

      case "issue":
        if (issuePer.exportIssues.cando) {
          ddData.push("Export CSV");
        }
        if (issuePer.importIssues.cando) {
          const listValue = `Import Bulk ${type.charAt(0).toUpperCase()}${type.slice(1)}s`;
          ddData.push(listValue);
        }
        return ddData;
        break;

      case "risk":
        if (riskPer.exportRisks.cando) {
          ddData.push("Export CSV");
        }
        if (riskPer.importRisks.cando) {
          const listValue = `Import Bulk ${type.charAt(0).toUpperCase()}${type.slice(1)}s`;
          ddData.push(listValue);
        }
        return ddData;
        break;

      case "meeting":
        if (meetingPer.exportMeetings.cando) {
          ddData.push("Export CSV");
        }
        if (meetingPer.importMeetings.cando) {
          const listValue = `Import Bulk ${type.charAt(0).toUpperCase()}${type.slice(1)}s`;
          ddData.push(listValue);
        }
        return ddData;
        break;

      default:
        return ddData;
        break;
    }
  };
  getPermission = (key, permission) => {
    /** function calling for getting permission */
    const { profileState } = this.props;
    let currentUser = profileState.teamMember.find((m) => {
      /** finding current user from team members array */
      return m.userId == profileState.userId;
    });
    return GetPermission.canDo(
      currentUser.isOwner,
      permission,
      key
    ); /** passing current user owner , workspace permission and a key to check */
  };
  getTranslatedId(value) {
    switch (value) {
      case "Export CSV":
        value = "common.export-csv.label";
        break;
      case "Import Bulk Projects":
        value = "common.import.projects.label";
        break;
      case "Import Bulk Tasks":
        value = "common.import.tasks.label";
        break;
      case "Import Bulk Meetings":
        value = "common.import.meetings.label";
        break;
      case "Import Bulk Issues":
        value = "common.import.issues.label";
        break;
      case "Import Bulk Risks":
        value = "common.import.risks.label";
        break;
    }
    return value;
  }

  render() {
    const {
      classes,
      theme,
      handleRefresh,
      ImportExportType,
      companyInfo,
      btnProps = {},
      labelProps = {},
    } = this.props;
    const { selectMenuFlag, placement, exportFlag, importFlag } = this.state;
    const taskLabelMulti = companyInfo?.task?.pName;
    const ddData = this.getDropDownData(ImportExportType);
    return (
      <Fragment>
        <ClickAwayListener onClickAway={this.handleCloseClickAwayListener}>
          <div>
            {ddData.length > 0 && (
              <div className={classes.importExportButtonCnt}>
                <CustomIconButton
                  onClick={(event) => {
                    this.handleClick(event, "bottom-end");
                  }}
                  buttonRef={(node) => {
                    this.anchorEl = node;
                  }}
                  btnType="filledWhite"
                  style={{ padding: "5px 7px", height: 32 }}
                  {...btnProps}>
                  <ImportExport classes={{ root: classes.importExportIcon }} />
                  <span className={classes.labelImportExport} {...labelProps}>
                    Import/Export
                  </span>
                </CustomIconButton>
              </div>
            )}

            <SelectionMenu
              open={selectMenuFlag}
              closeAction={this.handleCloseClickAwayListener}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List>
                  {ddData.map((value) => {
                    return (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={() => this.openDialogue(value)}>
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id={this.getTranslatedId(value)}
                              defaultMessage={value}
                              values={{
                                label:
                                  value == "Import Bulk Tasks" && taskLabelMulti
                                    ? taskLabelMulti
                                    : value,
                              }}
                            />
                          }
                          classes={{
                            primary: classes.filterDDText,
                          }}
                        />
                      </ListItem>
                    );
                  })}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
        {exportFlag && (
          <ExportColumn
            open={exportFlag}
            closeAction={this.handleClose}
            handleNotificationMsg={this.props.handleExportType}
            exportType={ImportExportType}
            projectId={this.props.projectId}
          />
        )}
        {/* :
            <ActionConfirmation
             open={exportFlag}
             closeAction={this.handleClose}
             cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
             successBtnText={<FormattedMessage id="common.action.yes.label" defaultMessage="Yes" />}
             alignment="center"
             iconType="importExport"
             headingText={<FormattedMessage id="common.export-dialog.label" defaultMessage="Export" />}
             msgText={<FormattedMessage id="common.export-dialog.message" defaultMessage="Are you sure you want to export CSV?" />}
             successAction={this.handleExport}
             btnQuery={exportBtnQuery}
          /> */}

        <ImportBulkDialog
          type={ImportExportType}
          open={importFlag}
          theme={theme}
          closeAction={this.closeImportBulkDialogue}
          handleExportType={this.handleExportType}
          showMessage={this.showSnackMessage}
          handleRefresh={handleRefresh}
          projectId={this.props.projectId}
          callBack={this.props.callBack}
        />
        {this.state.importmsproject && (
          <MsUploadDialog
            type={ImportExportType}
            open={this.state.importmsproject}
            theme={theme}
            closeAction={this.closeImportMSProjectDialogue}
            handleExportType={this.handleExportType}
            showMessage={this.showSnackMessage}
            handleRefresh={handleRefresh}
            projectId={this.props.projectId}
            callBack={this.props.callBack}
          />
        )}
      </Fragment>
    );
  }
}
ImportExportDD.defaultProps = {
  callBack: () => { },
};
const mapStateToProps = (state) => {
  return {
    meetingsState: state.meetings,
    issuesState: state.issues,
    risksState: state.risks,
    appliedFilters: state.appliedFilters,
    profileState: state.profile.data,
    projectPer: state.workspacePermissions.data.project,
    meetingPer: state.workspacePermissions.data.meeting,
    issuePer: state.workspacePermissions.data.issue,
    riskPer: state.workspacePermissions.data.risk,
    taskPer: state.workspacePermissions.data.task,
    companyInfo: state.whiteLabelInfo.data,
  };
};
export default compose(
  //??
  withRouter,
  injectIntl,
  withStyles(combineStyles(dashboardStyles, menuStyles), { withTheme: true }),
  connect(mapStateToProps, {
    ExportToCSV,
  })
)(ImportExportDD);
