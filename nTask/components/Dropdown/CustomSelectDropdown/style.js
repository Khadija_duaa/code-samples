const dropdownStyle = (theme) => ({
  dropdownLabel: {
    fontSize: "13px !important",
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  dropdownIconSize: {
    fontSize: "1.5rem !important"
  },
  dropdownValue: {
    fontSize: "13px !important",
    color: "#202020",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    marginLeft: 5,
    display: 'flex',
    justifyContent: 'space-between',
    flex: 1,    
  },
  dropdownValueDashboard: {
    fontSize: "13px !important",
    color: "#202020",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    marginLeft: 5,
    display: 'flex',
    justifyContent: 'space-between',
    flex: 1
  },
  itemText: {
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    lineHeight: "normal",
  },
  itemTextDashboard: {
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    lineHeight: "normal",
  },
  itemTextDashboard: {
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    lineHeight: "normal",
  },
  itemTextSelected: {
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    lineHeight: "normal",
    color: theme.palette.text.azure,
  },
  itemTextSelectedForDashboard: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    lineHeight: "normal",
  },
  itemTextCaption: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    marginTop: 7,
  },
  itemTextCaptionForDashboard: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    marginTop: 7,
  },
  listItem: {
    padding: "6px 14px",
    position: "relative",
  },
  selectedItemBorder: {
    background: theme.palette.background.blue,
    width: 3,
    position: "absolute",
    left: 0,
    height: 30,
    borderRadius: "0 4px 4px 0",
  },
  selectedItemBorderWithCaption: {
    background: theme.palette.background.blue,
    width: 3,
    position: "absolute",
    left: 0,
    height: 50,
    borderRadius: "0 4px 4px 0",
  },
  itemTextCnt: {
    display: "flex",
    flexDirection: "column",
  },
  ddIcon: {
    marginBottom: -7,
    color: '#BFBFBF',
    marginLeft: -4,
  },
});

export default dropdownStyle;
