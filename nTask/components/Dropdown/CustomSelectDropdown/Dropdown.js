// @flow

import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import { Scrollbars } from "react-custom-scrollbars";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { statusData } from "../../../helper/projectDropdownData";
import CustomButton from "../../Buttons/CustomButton";
import DropdownMenu from "../DropdownMenu";
import dropdownStyle from "./style";
import { FormattedMessage } from "react-intl";

type DropdownProps = {
  classes: Object,
  theme: Object,
  option: Object,
  options: Function, // List of all options shown in list
  onSelect: Function,
  label: String,
  size: String,
  scrollHeight: Number,
  disabled: Boolean,
  style: Object,
  iconVisible: Boolean,
};

// Onboarding Main Component
function CustomSelectDropdown(props: DropdownProps) {
  const {
    classes,
    theme,
    option,
    options,
    label,
    onSelect,
    size,
    scrollHeight,
    disabled,
    style,
    iconVisible = true,
    buttonProps,
    isMulti,
    isDashboard,
  } = props;
  const data = options();
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedOption, setSelectedOption] = useState({});
  const handleClick = (event) => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = (event) => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const handleItemClick = (event, option) => {
    // Fuction responsible for selecting item from the list
    setSelectedOption(option);
    setAnchorEl(null);
    onSelect(option); // Callback on item select used to lift up option to parent if needed
  };
  const open = Boolean(anchorEl);
  useEffect(() => {
    setSelectedOption(option || {});
  }, [option]);
  const translate = (value) => {
    let id = "";
    switch (value) {
      case "Fixed Fee":
        id = "project.dev.details.billable.billing-method.list.fixed";
        break;
      case "Fixed Fee per Task":
        id = "project.dev.details.billable.billing-method.list.fixedfeepertask";
        break;
      case "Hourly Rate by Task":
        id = "project.dev.details.billable.billing-method.list.hourlyratebytask";
        break;
      case "Hourly Rate by Resource":
        id = "project.dev.details.billable.billing-method.list.hourlyratebyresource";
        break;
      case "Same Fee for Every Task":
        id = "project.dev.details.billable.fee.list.same-fee-task";
        break;
      case "Different Fee per Task":
        id = "project.dev.details.billable.fee.list.different-fee-task";
        break;
      case "Same Hourly Rate for Every Task":
        id = "project.dev.details.billable.rate.list.same-hourly-rate-task";
        break;
      case "Different Hourly Rate per Task":
        id = "project.dev.details.billable.rate.list.different-hourly-rate-task";
        break;
      case "Same Hourly Rate for Every Resource":
        id = "project.dev.details.billable.rate.list.same-hourly-rate-resource";
        break;
      case "Different Hourly Rate per Resource":
        id = "project.dev.details.billable.rate.list.different-hourly-rate-resource";
        break;
      case "Project Manager":
        id = "project.dev.resources.permissions.project-manager.label";
        break;
      case "Contributor":
        id = "project.dev.resources.permissions.contributer.label";
        break;
      case "Viewer":
        id = "project.dev.resources.permissions.viewer.label";
        break;
      case "Can Edit & Add Comments":
        id = "project.dev.resources.permissions.contributer.permission";
        break;
        case "Can View & Add Comments":
          id = "project.dev.resources.permissions.viewer.permission";
          break;
      case "Full Access":
        id = "project.dev.resources.permissions.project-manager.permission";
        break;

    }
    return id == "" ? value : <FormattedMessage id={id} defaultMessage={value} />
  };
  return (
    <>
      <CustomButton
        ref={anchorEl}
        onClick={handleClick}
        btnType="plainbackground"
        variant="text"
        style={{
          padding: 7,
          borderRadius: 4,
        }}
        disabled={disabled}
        {...buttonProps}
      >
        <span className={classes.dropdownLabel}>{label}</span>
        {" "}
        <span className={isDashboard ? classes.dropdownValueDashboard : classes.dropdownValue}>
          {" "}
          {translate(selectedOption && selectedOption.label)}{" "}
          {iconVisible ? <ArrowDropDownIcon classes={{root: classes.dropdownIconSize}} className={classes.ddIcon} /> : ""}
        </span>
      </CustomButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size={size}
        placement="bottom-start"
      >
        <Scrollbars
          autoHide={false}
          autoHeight
          autoHeightMin={0}
          autoHeightMax={scrollHeight}
        >
          <List>
            {data.map((o, i) => {
              return (
                <ListItem
                  key={i}
                  button
                  className={classes.listItem}
                  onClick={(event) => handleItemClick(event, o)}
                >
                  {o.label == selectedOption.label && (
                    <span
                      className={
                        o.caption
                          ? classes.selectedItemBorderWithCaption
                          : classes.selectedItemBorder
                      }
                    />
                  )}
                  <div className={classes.itemTextCnt}>
                    <span
                      className={
                        o.label == selectedOption.label
                          ? isDashboard ? classes.itemTextSelectedForDashboard : classes.itemTextSelected
                          : isDashboard ? classes.itemTextDashboard : classes.itemText
                      }
                    >
                      {translate(o.label)}
                    </span>
                    {o.caption && (
                      <span className={isDashboard ? classes.itemTextCaptionForDashboard : classes.itemTextCaption}>
                        {translate(o.caption)}
                      </span>
                    )}
                  </div>
                </ListItem>
              );
            })}
          </List>
        </Scrollbars>
      </DropdownMenu>
    </>
  );
}
CustomSelectDropdown.defaultProps = {
  size: "small",
  scrollHeight: 180,
  disabled: false,
};
export default compose(
  withRouter,
  withStyles(dropdownStyle, { withTheme: true })
)(CustomSelectDropdown);
