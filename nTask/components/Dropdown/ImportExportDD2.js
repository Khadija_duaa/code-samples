import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import dashboardStyles from "../../Views/Project/styles";
import menuStyles from "../../assets/jss/components/menu";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import CustomIconButton from "../Buttons/IconButton";
import SelectionMenu from "../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../utils/mergeStyles";
import ImportExport from "@material-ui/icons/ImportExport";
import ActionConfirmation from "../Dialog/ConfirmationDialogs/ActionConfirmation";
import ExportColumn from "../Dialog/ExportColumn/ExportColumn";
import ImportBulkDialog from "../Dialog/DragnDropFileDialog/CsvUploadDialog";
import helper from "../../helper/index";
//import { ExportProjectsToCSV } from "../../redux/actions/projects";
import { ExportToCSV } from "../../redux/actions/ImportExport";
import { GetPermission } from "../permissions";
import { injectIntl, FormattedMessage } from "react-intl";

class ImportExportDD extends Component {
  constructor(props) {
    super(props); //??
    this.state = {
      selectMenuFlag: false,
      placement: "",
      exportFlag: false,
      importFlag: false,
      showMessage: "",
      exportBtnQuery: "",
    };
  }
  handleClose = (message, event) => {
    this.setState({ selectMenuFlag: false, exportFlag: false });
  };

  closeImportBulkDialogue = () => {
    this.setState({ importFlag: false });
  };

  handleExportType = (message) => {
    let statedData = {
      selectMenuFlag: false,
      exportFlag: false,
    };
    if (message) {
      statedData.showMessage = message;
    }
    this.setState(statedData, () => {
      this.props.handleExportType(this.state.showMessage);
    });
  };

  openDialogue = (value) => {
    switch (value) {
      case "Export CSV":
        this.setState({ exportFlag: true, selectMenuFlag: false });
        break;
      default:
        this.setState({ importFlag: true, selectMenuFlag: false });
    }
  };

  handleClick = (event, placement) => {
    const { currentTarget } = event;
    this.setState((state) => ({
      selectMenuFlag: state.placement !== placement || !state.selectMenuFlag,
      placement,
    }));
  };

  showSnackMessage = (message, type) => {
    this.props.handleExportType(message, type);
  };
  getExportIssueDetailsObject = (exportedObject, selectedProject) => {
    if (this.props.filterList) {
      const { severityData, priorityData, other } = this.props.filterList;
      exportedObject = {
        severity: severityData,
        priority: priorityData,
        isArchived: other ? true : false,
        recordsToExport: this.props.issuesState.data.length,
        project: selectedProject,
        isAdvanceFilter: false,
      };
    }
    if (this.props.multipleFilters) {
      let {
        actualEnd,
        actualStart,
        plannedEnd,
        plannedStart,
        type,
        status,
        priority,
        assignees,
        task,
        severity,
        label,
        category,
        createdBy,
      } = this.props.multipleFilters;
      if (actualEnd.endDate && actualEnd.startDate) {
        actualEnd = {
          startDate: helper.RETURN_CUSTOMDATEFORMAT(actualEnd.startDate),
          endDate: helper.RETURN_CUSTOMDATEFORMAT(
            actualEnd.endDate,
            "11:59:59 PM"
          ),
        };
      } else {
        actualEnd = null;
      }

      if (actualStart.endDate && actualStart.startDate) {
        actualStart = {
          startDate: helper.RETURN_CUSTOMDATEFORMAT(actualStart.startDate),
          endDate: helper.RETURN_CUSTOMDATEFORMAT(
            actualStart.endDate,
            "11:59:59 PM"
          ),
        };
      } else {
        actualStart = null;
      }

      if (plannedEnd.endDate && plannedEnd.startDate) {
        plannedEnd = {
          startDate: helper.RETURN_CUSTOMDATEFORMAT(plannedEnd.startDate),
          endDate: helper.RETURN_CUSTOMDATEFORMAT(
            plannedEnd.endDate,
            "11:59:59 PM"
          ),
        };
      } else {
        plannedEnd = null;
      }

      if (plannedStart.endDate && plannedStart.startDate) {
        plannedStart = {
          startDate: helper.RETURN_CUSTOMDATEFORMAT(plannedStart.startDate),
          endDate: helper.RETURN_CUSTOMDATEFORMAT(
            plannedStart.endDate,
            "11:59:59 PM"
          ),
        };
      } else {
        plannedStart = null;
      }

      exportedObject = {
        tasks: task,
        issueAssignees: assignees,
        creator: createdBy,
        status: status,
        priority: priority,
        category,
        label,
        severity,
        type,
        isAdvanceFilter: true,
        actualEnd,
        actualStart,
        plannedEnd,
        plannedStart,
        recordsToExport: this.props.issuesState.data.length,
      };
    }

    return exportedObject;
  };
  getExportRiskDetailsObject = (exportedObject, selectedProject) => {
    if (this.props.filterList) {
      const { statusData, likelihoodData, archived } = this.props.filterList;
      exportedObject = {
        status: statusData,
        likelihood: likelihoodData,
        isArchived: archived,
        recordsToExport: this.props.risksState.data.length,
        project: selectedProject,
        isAdvanceFilter: false,
      };
    }
    if (this.props.multipleFilters) {
      let {
        status,
        task,
        riskOwner,
        createdBy,
        likelihood,
        impact,
        category,
      } = this.props.multipleFilters;
      exportedObject = {
        tasks: task,
        creator: createdBy,
        status: status,
        riskOwner,
        category,
        likelihood,
        impact,
        isAdvanceFilter: true,
        recordsToExport: this.props.risksState.data.length,
      };
    }

    return exportedObject;
  };
  getExportMeetingDetailsObject = (exportedObject, selectedProject) => {
    if (this.props.filterList) {
      const {
        statusData,
        meetingDay,
        archived,
        starred,
      } = this.props.filterList;
      exportedObject = {
        status: statusData,
        meetingDay: meetingDay.map((x) => {
          if (x === "Today") return "today";
          if (x === "This Week") return "week";
          if (x === "This Month") return "month";
        }),
        isArchive: archived,
        recordsToExport: this.props.meetingsState.data.length,
        project: selectedProject,
        isAdvanceFilter: false,
        IsStared: starred,
      };
    }
    if (this.props.multipleFilters) {
      let {
        participant,
        task,
        status,
        createdBy,
        Recurrence,
        actualStart,
      } = this.props.multipleFilters;
      let date = null;
      if (actualStart.endDate && actualStart.startDate) {
        date = {
          startDate: helper.RETURN_CUSTOMDATEFORMAT(actualStart.startDate),
          endDate: helper.RETURN_CUSTOMDATEFORMAT(
            actualStart.endDate,
            "11:59:59 PM"
          ),
        };
      } else {
        date = null;
      }
      exportedObject = {
        task,
        creator: createdBy,
        status: status,
        participant,
        isAdvanceFilter: true,
        recordsToExport: this.props.meetingsState.data.length,
        startDateString: date ? date.startDate : null,
        endDateString: date ? date.endDate : null,
        recurrence: Recurrence,
        project: selectedProject,
      };
    }

    return exportedObject;
  };
  getExportTaskDetailsObject = (exportedObject, selectedProject) => {
    if (this.props.filterList && this.props.filterList.length > 0) {
      let tasksStateWithIndex = [
        { status: "Not Started", index: 0 },
        { status: "In Progress", index: 1 },
        { status: "In Review", index: 2 },
        { status: "Completed", index: 3 },
        { status: "Cancelled", index: 4 },
      ];

      let statusIndex = tasksStateWithIndex
        .filter((x) => this.props.filterList.indexOf(x.status) >= 0)
        .map((x) => x.index);
      let isStared = this.props.filterList.indexOf("Starred") >= 0;
      let dueToday = this.props.filterList.indexOf("Due Today") >= 0;
      let archived = this.props.filterList.indexOf("Archived") >= 0;
      exportedObject = {
        status: statusIndex,
        starred: isStared,
        dueToday,
        isArchived: archived,
        recordsToExport: this.props.tasksState.data.length,
        project: selectedProject,
        isAdvanceFilter: false,
      };
    }
    if (this.props.multipleFilters) {
      let {
        actualEnd,
        actualStart,
        plannedEnd,
        plannedStart,
        status,
        priority,
        assignees,
      } = this.props.multipleFilters;
      if (actualEnd.endDate && actualEnd.startDate) {
        actualEnd = {
          startDate: helper.RETURN_CUSTOMDATEFORMAT(actualEnd.startDate),
          endDate: helper.RETURN_CUSTOMDATEFORMAT(
            actualEnd.endDate,
            "11:59:59 PM"
          ),
        };
      } else {
        actualEnd = null;
      }

      if (actualStart.endDate && actualStart.startDate) {
        actualStart = {
          startDate: helper.RETURN_CUSTOMDATEFORMAT(actualStart.startDate),
          endDate: helper.RETURN_CUSTOMDATEFORMAT(
            actualStart.endDate,
            "11:59:59 PM"
          ),
        };
      } else {
        actualStart = null;
      }

      if (plannedEnd.endDate && plannedEnd.startDate) {
        plannedEnd = {
          startDate: helper.RETURN_CUSTOMDATEFORMAT(plannedEnd.startDate),
          endDate: helper.RETURN_CUSTOMDATEFORMAT(
            plannedEnd.endDate,
            "11:59:59 PM"
          ),
        };
      } else {
        plannedEnd = null;
      }

      if (plannedStart.endDate && plannedStart.startDate) {
        plannedStart = {
          startDate: helper.RETURN_CUSTOMDATEFORMAT(plannedStart.startDate),
          endDate: helper.RETURN_CUSTOMDATEFORMAT(
            plannedStart.endDate,
            "11:59:59 PM"
          ),
        };
      } else {
        plannedStart = null;
      }

      let priorities = [
        { priority: "Critical", index: 1 },
        { priority: "High", index: 2 },
        { priority: "Medium", index: 3 },
        { priority: "Low", index: 4 },
      ];
      let tasksStateWithIndex = [
        { status: "Not Started", index: 0 },
        { status: "In Progress", index: 1 },
        { status: "In Review", index: 2 },
        { status: "Completed", index: 3 },
        { status: "Cancelled", index: 4 },
      ];
      let statusIndex = tasksStateWithIndex
        .filter((x) => status.indexOf(x.status) >= 0)
        .map((x) => x.index);
      let isStared = status.indexOf("Starred") >= 0;
      let dueToday = status.indexOf("Due Today") >= 0;
      let priorityIndex = priorities
        .filter((x) => priority.indexOf(x.priority) >= 0)
        .map((x) => x.index);

      exportedObject = {
        status: statusIndex,
        starred: isStared,
        dueToday,
        priority: priorityIndex,
        assignee: assignees,
        recordsToExport: this.props.tasksState.data.length,
        project: selectedProject,
        isAdvanceFilter: true,
        isRecurrence: true,
        actualEnd,
        actualStart,
        plannedEnd,
        plannedStart,
      };
    }

    return exportedObject;
  };

  handleExport = () => {
    const type = this.props.ImportExportType;
    let exportedObject = {},
      fileName = `${this.props.companyInfo ? this.props.companyInfo.companyName : 'nTask'}-${type.charAt(0).toUpperCase()}${type.slice(1)}.xlsx`;
    let selectedProject = localStorage.getItem("projectId")
      ? [localStorage.getItem("projectId")]
      : [];
    let Url = "";

    if (type == "project") {
      exportedObject = { isArchived: false };
      if (this.props.isArchived) {
        exportedObject = { isArchived: true };
      }
      Url = "api/project/ExportProjects";
    }
    if (type == "issue") {
      exportedObject = exportedObject = {
        issues: this.props.data.map((i) => i.id),
      };
      Url = "/api/IssueRisk/ExportIssues";
    }
    if (type == "risk") {
      exportedObject = { risks: this.props.data.map((r) => r.id) };
      Url = "/api/IssueRisk/ExportRisks";
    }
    if (type == "meeting") {
      exportedObject = { MeetingIds: this.props.data.map((m) => m.meetingId) };
      Url = "/api/Meeting/ExportMeetings";
    }
    if (type == "task") {
      exportedObject = { TaskIds: this.props.data.map((t) => t.taskId), projectId: this.props.projectId };
      // this.getExportTaskDetailsObject(exportedObject, selectedProject);
      Url = "/api/UserTask/ExportUserTasks";
    }
    this.setState({ exportBtnQuery: "progress" }, () => {
      this.props.ExportToCSV(
        exportedObject,
        Url,
        (response) => {
          this.setState({ exportBtnQuery: "", exportFlag: false });
          this.props.handleExportType(this.props.intl.formatMessage({ id: "common.successMessages.file-export", defaultMessage: "File exported successfully." }), "success");
          helper.DOWNLOAD_TEMPLATE(
            response.data,
            fileName,
            "application/vnd.ms-excel"
          );
        },
        (err) => {
          let error = err.data.message
            ? err.data.message
            : "Oops! No records found to be exported.";
          this.setState({ exportBtnQuery: "", exportFlag: false });
          this.props.handleExportType(error, "error");
        }
      );
    });
  };
  getDropDownData = (type) => {
    const { getPermission } = this;
    const {
      projectPer = {},
      meetingPer = {},
      issuePer = {},
      riskPer = {},
      taskPer = {}
    } = this.props;

    const ddData = [];
    switch (type) {
      case "project":
        if (projectPer.exportProject.cando) {
          ddData.push("Export CSV");
        }
        if (projectPer.importProject.cando) {
          const listValue = `Import Bulk ${type
            .charAt(0)
            .toUpperCase()}${type.slice(1)}s`;
          ddData.push(listValue);
        }
        return ddData;
        break;

      case "task":
        if (taskPer.exportTasks && taskPer.exportTasks.cando) {
          ddData.push("Export CSV");
        }
        if (taskPer.importTasks && taskPer.importTasks.cando) {
          const listValue = `Import Bulk ${type
            .charAt(0)
            .toUpperCase()}${type.slice(1)}s`;
          ddData.push(listValue);
        }
        return ddData;
        break;

      case "issue":
        if (issuePer.exportIssues.cando) {
          ddData.push("Export CSV");
        }
        if (issuePer.importIssues.cando) {
          const listValue = `Import Bulk ${type
            .charAt(0)
            .toUpperCase()}${type.slice(1)}s`;
          ddData.push(listValue);
        }
        return ddData;
        break;

      case "risk":
        if (riskPer.exportRisks.cando) {
          ddData.push("Export CSV");
        }
        if (riskPer.importRisks.cando) {
          const listValue = `Import Bulk ${type
            .charAt(0)
            .toUpperCase()}${type.slice(1)}s`;
          ddData.push(listValue);
        }
        return ddData;
        break;

      case "meeting":
        if (meetingPer.exportMeetings.cando) {
          ddData.push("Export CSV");
        }
        if (meetingPer.importMeetings.cando) {
          const listValue = `Import Bulk ${type
            .charAt(0)
            .toUpperCase()}${type.slice(1)}s`;
          ddData.push(listValue);
        }
        return ddData;
        break;

      default:
        return ddData;
        break;
    }
  };
  getPermission = (key, permission) => {
    /** function calling for getting permission */
    const { profileState } = this.props;
    let currentUser = profileState.teamMember.find((m) => {
      /** finding current user from team members array */
      return m.userId == profileState.userId;
    });
    return GetPermission.canDo(
      currentUser.isOwner,
      permission,
      key
    ); /** passing current user owner , workspace permission and a key to check */
  };

  getTranslatedId(value) {
    switch (value) {
      case "Export CSV":
        value = "common.export-csv.label";
        break;
      case "Import Bulk Projects":
        value = "common.import.projects.label";
        break;
      case "Import Bulk Tasks":
        value = "common.import.tasks.label";
        break;
      case "Import Bulk Meetings":
        value = "common.import.meetings.label";
        break;
      case "Import Bulk Issues":
        value = "common.import.issues.label";
        break;
      case "Import Bulk Risks":
        value = "common.import.risks.label";
        break;
    }
    return value;
  }

  render() {
    const { classes, theme, handleRefresh, ImportExportType, id, companyInfo } = this.props;
    const {
      selectMenuFlag,
      placement,
      exportFlag,
      importFlag,
      exportBtnQuery,
    } = this.state;
    const ddData = this.getDropDownData(ImportExportType);
    const columns = [
      { name: "Id", value: true },
      { name: "Title", value: true },
      { name: "Status", value: true },
      { name: "Impact", value: true },
      { name: "Likelihood", value: true },
      { name: "Risk Owner", value: false },
      { name: "Task", value: false },
      { name: "Last Updated", value: false },
      { name: "Created By", value: false },
      { name: "Creation Date", value: false },
    ]
    return (
      <Fragment>
        <ClickAwayListener onClickAway={handleClose}>
          <div>
            {ddData.length > 0 && (
              <div className={classes.importExportButtonCnt}>
                <CustomIconButton
                  onClick={(event) => {
                    this.handleClick(event, "bottom-end");
                  }}
                  buttonRef={(node) => {
                    this.anchorEl = node;
                  }}
                  btnType="filledWhite"
                  style={{ padding: "5px 7px", height: 39 }}
                >
                  <ImportExport classes={{ root: classes.importExportIcon }} />
                </CustomIconButton>
              </div>
            )}

            <SelectionMenu
              open={selectMenuFlag}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List>
                  {ddData.map((value) => {
                    return (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={() => this.openDialogue(value)}
                      >
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id={this.getTranslatedId(value)}
                              defaultMessage={value}
                            />
                          }
                          classes={{
                            primary: classes.filterDDText,
                          }}
                        />
                      </ListItem>
                    );
                  })}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
        {ImportExportType == "risk"
          ? <ExportColumn
            open={exportFlag}
            closeAction={this.handleClose}
            columns={columns}
            handleClickExport={() => { }}
          />
          : <ActionConfirmation
            open={exportFlag}
            closeAction={this.handleClose}
            cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
            successBtnText={<FormattedMessage id="common.action.yes.label" defaultMessage="Yes" />}
            alignment="center"
            iconType="importExport"
            headingText={<FormattedMessage id="common.export-dialog.label" defaultMessage="Export" />}
            msgText={<FormattedMessage id="common.export-dialog.message" defaultMessage="Are you sure you want to export CSV?" />}
            successAction={this.handleExport}
            btnQuery={exportBtnQuery}
          />}



        <ImportBulkDialog
          type={ImportExportType}
          open={importFlag}
          theme={theme}
          closeAction={this.closeImportBulkDialogue}
          handleExportType={this.handleExportType}
          showMessage={this.showSnackMessage}
          handleRefresh={handleRefresh}
          projectId={this.props.projectId}
          callBack={this.props.callBack}
        />
      </Fragment>
    );
  }
}
ImportExportDD.defaultProps = {
  callBack: () => { }
}
const mapStateToProps = (state) => {
  return {
    meetingsState: state.meetings,
    issuesState: state.issues,
    risksState: state.risks,
    appliedFilters: state.appliedFilters,
    profileState: state.profile.data,
    projectPer: state.workspacePermissions.data.project,
    meetingPer: state.workspacePermissions.data.meeting,
    issuePer: state.workspacePermissions.data.issue,
    riskPer: state.workspacePermissions.data.risk,
    taskPer: state.workspacePermissions.data.task,
    companyInfo: state.whiteLabelInfo.data
  };
};
export default compose(
  //??
  withRouter,
  injectIntl,
  withStyles(combineStyles(dashboardStyles, menuStyles), { withTheme: true }),
  connect(mapStateToProps, {
    ExportToCSV,
  })
)(ImportExportDD);
