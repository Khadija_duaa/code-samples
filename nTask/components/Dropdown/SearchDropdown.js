import React, { Component } from "react";
import DropdownMenu from "./DropdownMenu";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import CustomAvatar from "../Avatar/Avatar";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import dropdownStyles from "./style";
import { compose } from "redux";
import { connect } from "react-redux";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Close";
import SearchInput, { createFilter } from "react-search-input";
import { Scrollbars } from "react-custom-scrollbars";
import CustomButton from "../Buttons/CustomButton";
import CustomTooltip from "../Tooltip/Tooltip";
import { FormattedMessage, injectIntl } from "react-intl";
import { getContrastYIQ } from "../../helper/index";
import isEqual from "lodash/isEqual";

class SearchDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      selectedOptions: [],
      optionsList: [],
      searchTerm: "",
    };
  }
  componentDidUpdate(prevProps, prevState) {
    const { optionsList, initSelectedOption } = this.props;
    const { selectedOptions } = this.state;
    if (!isEqual(optionsList, prevProps.optionsList)) {
      this.setState({
        optionsList: this.compareOptionsList(selectedOptions),
      });
    }
    if (!isEqual(initSelectedOption, prevProps.initSelectedOption)) {
      this.setState({ selectedOptions: initSelectedOption,  optionsList: this.compareOptionsList(initSelectedOption) });
    }
  }
  handleRemoveAssignee = option => {
    // Function responsible for removing assignee from list

    const { updateAction, isMulti, TaskView } = this.props;
    const { selectedOptions, optionsList } = this.state;
    let newOptionsList = [...optionsList, ...option];

    let selectedOptionsList = selectedOptions.filter(selectedOption => {
      return selectedOption.id !== option[0].id;
    });
    if (!TaskView) this.setState({ selectedOptions: selectedOptionsList }); // Updating selected List based on user selection
    this.setState(
      {
        optionsList: newOptionsList,
        searchTerm: "",
      },
      () => {
        // Updates on back end and store when user select an option
        updateAction(selectedOptionsList, option[0].obj);
      }
    );
  };
  compareOptionsList = (selectedOption = []) => {
    const { optionsList, isMulti, TaskView } = this.props;
    // Filtering list by comparing it with selected options list in dropdown for multiple select
    if (isMulti) {
      let newList = optionsList.filter(option => {
        let optionsListFilter =
          selectedOption.find(selected => {
            return selected.id == option.id;
          }) || {};

        return optionsListFilter.id !== option.id;
      });
      return newList;
    } else {
      // Filtering list by comparing it with selected options list in dropdown for single Select
      if (!TaskView) {
        let newList = optionsList.filter(item => {
          return item.id !== (selectedOption.length ? selectedOption[0].id : "");
        });
        return newList;
      } else return optionsList;
    }
  };

  handleOptionSelect = (value, option) => {
    const { currentIssue, task } = this.state;
    const { updateAction, isMulti, TaskView, maxSelection, showSelected } = this.props;
    const { selectedOptions } = this.state;
    // This function handles selection of option
    if (!showSelected) {
      updateAction(option);
      this.setState({ open: false });
      return;
    }
    let selectedOptionsList = isMulti ? [...selectedOptions, ...option] : option;
    //In case maximum selection are there
    if (selectedOptionsList.length > maxSelection) {
      updateAction(selectedOptionsList, option[0].obj);
      this.setState({ open: false });
      return;
    }

    if (!TaskView) this.setState({ selectedOptions: selectedOptionsList }); // Updating selected List based on user selection
    this.setState(
      {
        optionsList: this.compareOptionsList(selectedOptionsList),
        open: false,
        searchTerm: "",
      },
      () => {
        // Updates on back end and store when user select an option
        updateAction(selectedOptionsList, option[0].obj);
      }
    );
  };
  handleClose = event => {
    const { handleCloseProject } = this.props;
    if (document.getElementById("assigneeDropdown").contains(event.target)) {
      return;
    }
    this.setState({ open: false, searchTerm: "" });
    // BulkAction searchDropDown close function
    if (handleCloseProject) {
      handleCloseProject();
    }
  };
  componentDidMount() {
    const { initSelectedOption } = this.props;
    // Updating state with the total list from which user can select
    this.setState({
      optionsList: this.compareOptionsList(initSelectedOption),
      selectedOptions: initSelectedOption,
    });
    this.anchorEl &&
      this.anchorEl.addEventListener("click", e => {
        e.stopPropagation();
        const { disableDropdown } = this.props;
        if (disableDropdown) {
          return;
        } else {
          this.setState({ open: true });
        }
      });
  }
  searchUpdated = term => {
    this.setState({ searchTerm: term });
  };
  render() {
    const { selectedOptions, open, optionsList, searchTerm } = this.state;
    const {
      classes,
      theme,
      selectedOptionHead,
      allOptionsHead,
      buttonPlaceholder,
      multipleOptionsPlaceHolder = "Multiple Items",
      buttonProps,
      tooltipText,
      tooltip,
      disabled = false,
      labelAlign = "center",
      optionBackground,
      popperProps,
      customSearchRender,
      iconOption,
      isClearable
    } = this.props;

    const filteredOptions = optionsList.filter(createFilter(searchTerm, ["label"]));
    const btnText =
      selectedOptions.length == 1 ? (
        selectedOptions[0].label
      ) : selectedOptions.length > 1 ? (
        multipleOptionsPlaceHolder
      ) : (
        <u style={{ textDecoration: "none" }}>{buttonPlaceholder}</u>
      );
    const selectedOptionIcon =
      iconOption && selectedOptions.length && selectedOptions.length == 1
        ? selectedOptions[0].icon
        : "";
    return (
      <>
        {tooltip ? (
          <CustomTooltip helptext={tooltipText}>
              <CustomButton
                buttonRef={node => {
                  this.anchorEl = node;
                }}
                {...buttonProps}
                variant="text"
                btnType="plain"
                labelAlign={labelAlign}
                onClick={this.handleClick}
                disabled={disabled}>
                <span
                  style={{
                    width: 100,
                    overflow: "hidden",
                    whiteSpace: "nowrap",
                    textOverflow: "ellipsis",
                    textAlign: labelAlign,
                  }}
                  title={btnText}
                  className={`wrapText`}
                  {...this.props.btnTextProps}>
                  {btnText}
                </span>
              </CustomButton>
          </CustomTooltip>
        ) : customSearchRender ? (
          customSearchRender
        ) : (
          <CustomButton
            buttonRef={node => {
              this.anchorEl = node;
            }}
            {...buttonProps}
            variant="text"
            btnType="plain"
            labelAlign={labelAlign}
            onClick={this.handleClick}
            disabled={disabled}>
            <span
              style={{
                width: 100,
                overflow: "hidden",
                whiteSpace: "nowrap",
                textOverflow: "ellipsis",
                textAlign: labelAlign,
              }}
              title={btnText}
              {...this.props.btnTextProps}
              className={`wrapText`}>
              {iconOption && selectedOptionIcon}
              {btnText}
            </span>
          </CustomButton>
        )}

        <DropdownMenu
          open={this.props.anchorEl ? Boolean(this.props.anchorEl) : open}
          anchorEl={this.props.anchorEl ? this.props.anchorEl : this.anchorEl}
          placement="bottom-start"
          id="assigneeDropdown"
          size="small"
          closeAction={this.handleClose}
          {...popperProps}>
          <div>
            <div className={classes.dropdownSearchCntCustomField}>
              <SearchInput
                className="HtmlInput"
                maxLength="80"
                onChange={this.searchUpdated}
                placeholder={this.props.intl.formatMessage({
                  id: "common.search.label",
                  defaultMessage: "Search",
                })}
              />
            </div>
            <Scrollbars autoHide autoHeightMax={220} autoHeight={false} style={{ height: 220 }}>
              {selectedOptions.length > 0 && !searchTerm ? (
                <>
                  <Typography variant="caption" className={classes.listItemHeading}>
                    {selectedOptionHead}
                  </Typography>

                  {/* Selection List */}
                  <List className={classes.list}>
                    {selectedOptions.map((option, i) => {
                      return (
                        <ListItem key={i} className={classes.menuItemPlain}>
                          <span
                            style={
                              optionBackground
                                ? {
                                    background: option.color || "transparent",
                                    color: option.color ? getContrastYIQ(option.color) : "",
                                    borderRadius: 4,
                                    padding: 5,
                                    marginRight: 10,
                                  }
                                : null
                            }
                            className={classes.itemText}>
                            {iconOption && option.icon} {option.label}
                          </span>

                          {isClearable && <RemoveIcon
                            className={classes.removeAssigneeIcon}
                            htmlColor={theme.palette.secondary.light}
                            onClick={event => this.handleRemoveAssignee([option])}
                          />}
                        </ListItem>
                      );
                    })}
                  </List>
                </>
              ) : null}
              {filteredOptions.length > 0 ? (
                <Typography variant="caption" className={classes.listItemHeading}>
                  {allOptionsHead}
                </Typography>
              ) : null}
              {/* Other Options List */}
              <List className={classes.list}>
                {filteredOptions.map((option, i) => {
                  return (
                    <ListItem
                      key={i}
                      button
                      onClick={event => this.handleOptionSelect(event, [option])}
                      className={classes.menuItemPlain}>
                      <span
                        style={
                          optionBackground
                            ? {
                                background: option.color || "transparent",
                                color: option.color ? getContrastYIQ(option.color) : "",
                                borderRadius: 4,
                                padding: 5,
                              }
                            : null
                        }
                        className={classes.itemText}>
                        {iconOption && option.icon} {option.label}
                      </span>
                    </ListItem>
                  );
                })}
              </List>
            </Scrollbars>
          </div>
        </DropdownMenu>
      </>
    );
  }
}

SearchDropdown.defaultProps = {
  TaskView: false,
  customSearchRender: false,
  handleCloseProject: () => {},
  showSelected: true,
  btnTextProps: {},
  iconOption: false,
  isClearable: true
};

const mapStateToProps = (state, ownProps) => {
  return {
    members: state.profile.data.member.allMembers,
  };
};
export default compose(
  connect(mapStateToProps),
  injectIntl,
  withStyles(dropdownStyles, { withTheme: true })
)(SearchDropdown);
