const searchStyles = (theme) => ({
  shortcutTag: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
    position: "absolute",
    right: 10,
    fontWeight: 500,
  },
  typeTag: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
    fontWeight: 500,
    padding: "2px 10px",
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  searchMenuHeader: {
    display: "flex",
    justifyContent: "space-between",
    padding: "10px 10px 0 10px",
  },
  quickSearchList: {
    padding: 0,
    listStyleType: "none",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    background: theme.palette.background.paper,
    "& li": {
      padding: "15px 10px",
      cursor: "pointer",
    },
  },
  userNameCountCnt: {
    display: "flex",
    justifyContent: "space-between",
  },
  optionText: {
    width: 260,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
  },
  archiveIcon: {
    fontSize: "19px !important",
    marginRight: 10,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "2px 2px 2px 4px",
    borderRadius: 4,
  },
  chips: {
    background: theme.palette.background.medium,
    height: 28,
    borderRadius: 4,
    marginRight: 5,
    marginBottom: 2,
    marginTop: 2,
  },
  chipsLabel: {
    fontSize: "14px !important",
    color: theme.palette.text.secondary,
    paddingLeft: 4,
  },
  selectMenuListHeading: {
    padding: "5px 10px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  reactSelectFormControl: {
    marginTop: 22,
    marginBottom: 20,
  },
  chipsRemoveIcon: {
    fontSize: "16px !important",
  },
  selectLabel: {
    transform: "translate(2px, -17px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  selectLabelSelected: {
    transform: "translate(2px, -17px) scale(1)",
    color: theme.palette.text.green,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  optionWithAvatar: {
    display: "flex",
    alignItems: "center",
  },

  dropdownClearIcon: {
    fontSize: "13px !important",
  },
  noResultFoundTxt:{
    fontSize : 15,
    fontWeight: 500,
    fontFamily: theme.typography.fontFamilyLato,
  },
  newItemLabel:{
    fontSize: "12px !important",
    // color: theme.palette.text.light,
    fontWeight: 700,
  }
});

export default searchStyles;
