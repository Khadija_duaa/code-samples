import React, { Component, Fragment } from "react";
import { components } from "react-select";
import CreatableSelect from "react-select/lib/Creatable";

import { withStyles } from "@material-ui/core/styles";
import searchStyles from "./style.js";
// import SearchIcon from "@material-ui/icons/Search";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import RemoveIcon from "@material-ui/icons/Cancel";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import { selectCustomStyles } from "./selectCustomStyles";
import CustomAvatar from "../../Avatar/Avatar";
import ChevronDown from "@material-ui/icons/ArrowDropDown";
import ClearIcon from "@material-ui/icons/Clear";
class CreateableSelectDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: [],
      inputValue: "",
      selectedValue: [],
      data: {},
      type: "",
      menuOpen: false,
    };
  }
  componentDidMount() {
    const { data, acceptDataInFormOfFun = true, selectedValue } = this.props;
    this.setState({
      options: acceptDataInFormOfFun ? data() : data,
      selectedValue: selectedValue,
    });
  }
  componentDidUpdate(prevProps, prevState) {
    const { selectedValue, data, acceptDataInFormOfFun = true } = this.props;
    // let dataOption = acceptDataInFormOfFun ? data() : data;
    if (
      JSON.stringify(prevProps.selectedValue) !== JSON.stringify(selectedValue)
    ) {
      this.setState({ selectedValue });
    }
    if (JSON.stringify(prevProps.data) !== JSON.stringify(data)) {
      this.setState({ options: acceptDataInFormOfFun ? data() : data });
    }
  }
  //Component for rendering custom/static option at the end of the list which will always stay
  MenuList = (props) => {
    const { classes, heading } = this.props;
    return (
      <components.MenuList {...props}>
        {heading ? (
          <Typography variant="h6" className={classes.selectMenuListHeading}>
            {heading}
          </Typography>
        ) : null}
        {props.children}
      </components.MenuList>
    );
  };
  //Component that renderes single select value container
  SingleValue = ({ children, data, ...props }) => {
    const { avatar = false, icon = false, classes } = this.props;
    return (
      <components.SingleValue {...props}>
        <div className={classes.optionWithIcon}>
          {avatar ? (
            <CustomAvatar
              otherMember={{
                imageUrl: data.obj.imageUrl,
                fullName: data.obj.fullName,
                lastName: "",
                email: data.obj.email,
                isOnline: data.obj.isOnline,
                isOwner: data.obj.isOwner,
              }}
              styles={{ marginRight: 5 }}
              size="small18"
            />
          ) : icon ? (
            data.icon
          ) : null}
          {children}
        </div>
      </components.SingleValue>
    );
  };
  //Component for custom rendering of option in the menu list
  CustomOptionComponent = ({
    innerProps,
    innerRef,
    getStyles,
    children,
    data,
    ...props
  }) => {
    const { classes, theme, avatar = false, icon = false } = this.props;
    const optionStyles = getStyles("option", props);
    return (
      <div ref={innerRef} {...innerProps} style={optionStyles}>
        {/* Custom avatar will be rendered in case user wants to render avatar along with the values, especially in case of members list */}
        <div className={classes.optionWithAvatar}>
          {avatar && data.obj ? (
            <CustomAvatar
              otherMember={{
                imageUrl: data.obj.imageUrl,
                fullName: data.obj.fullName,
                lastName: "",
                email: data.obj.email,
                isOnline: data.obj.isOnline,
                isOwner: data.obj.isOwner,
              }}
              styles={{ marginRight: 10 }}
              size="xsmall"
            />
          ) : icon && data.icon ? (
            data.icon
          ) : null}

          <Typography
            variant="h6"
            style={{ color: optionStyles.color }}
            className={classes.optionText}
          >
            <b>{data.uniqueId}</b> {data.uniqueId ? "-" : null} {children}
          </Typography>
        </div>
      </div>
    );
  };
  //Component that renderes dropdown arrow indicator
  DropdownIndicator = (props) => {
    const { theme, classes } = this.props;
    return (
      <components.DropdownIndicator {...props}>
        <ChevronDown
          className={classes.dropdownIndicatorIcon}
          htmlColor={theme.palette.secondary.light}
        />
      </components.DropdownIndicator>
    );
  };
  // Component that renders clear indicator in select
  ClearIndicator = (props) => {
    const { classes, theme } = this.props;
    return (
      <components.ClearIndicator {...props}>
        <ClearIcon
          className={classes.dropdownClearIcon}
          htmlColor={theme.palette.secondary.medDark}
        />
      </components.ClearIndicator>
    );
  };

  //Component that renderes delete button of select option chips
  MultiValueRemove = (props) => {
    const { theme, classes } = this.props;
    return (
      <components.MultiValueRemove {...props}>
        <RemoveIcon
          className={classes.chipsRemoveIcon}
          htmlColor={theme.palette.secondary.medDark}
        />
      </components.MultiValueRemove>
    );
  };
  //Component that renderes select option chips
  MultiValueLabel = ({ data, children, ...props }) => {
    const {
      avatar = false,
      icon = false,
      classes,
      isMulti = true,
    } = this.props;
    return (
      <components.MultiValueLabel {...props}>
        <div className={classes.optionWithIcon}>
          {avatar && data.obj ? (
            <CustomAvatar
              otherMember={{
                imageUrl: data.obj.imageUrl,
                fullName: data.obj.fullName,
                lastName: "",
                email: data.obj.email,
                isOnline: data.obj.isOnline,
                isOwner: data.obj.isOwner,
              }}
              styles={{ marginRight: 5 }}
              size="small18"
            />
          ) : icon && data.obj ? (
            data.icon
          ) : !isMulti ? (
            <CustomAvatar
              otherMember={{
                imageUrl: "",
                fullName: data.value,
                lastName: "",
                email: "",
                isOnline: false,
                isOwner: false,
              }}
              styles={{ marginRight: 5 }}
              size="small18"
            />
          ) : null}
          {children}
        </div>
      </components.MultiValueLabel>
    );
  };
  handleSelectChange = (newValue, actionMeta) => {
    // let newValue = value == null ? {} : value;
    const {
      createLabelValidation,
      isMulti = true,
      type,
      selectClear,
      clearOptionAfterSelect = false,
    } = this.props;
    if (actionMeta.action == "create-option" && isMulti) {
      //Checking if dropdown is multiselect or single select based on props and setting validation accordingly
      let validateValue = createLabelValidation(
        newValue[newValue.length - 1].value
      );
      if (validateValue) {
        this.props.createOptionAction(type, newValue[newValue.length - 1]);
        this.setState({ selectedValue: newValue });
      } else {
        return false;
      }
    } else if (actionMeta.action == "create-option" && !isMulti) {
      let validateValue = createLabelValidation(newValue.value);
      if (validateValue) {
        this.props.createOptionAction(type, newValue);
        if (clearOptionAfterSelect) {
          this.setState({ selectedValue: null });
        } else {
          this.setState({ selectedValue: newValue });
        }
      }
    } else if (actionMeta.action == "select-option") {
      this.props.selectOptionAction(type, newValue);
      if (clearOptionAfterSelect) {
        this.setState({ selectedValue: null });
      } else {
        this.setState({ selectedValue: newValue });
      }
    } else if (actionMeta.action == "remove-value") {
      this.props.removeOptionAction(actionMeta.removedValue);
      this.setState({ selectedValue: newValue });
    } else if (actionMeta.action == "clear") {
      //checks user action and execute block of code if user performed action to clear the select
      selectClear(type, newValue);
      this.setState({ selectedValue: newValue });
    }
  };
  //function for select on input change
  handleInputChange = (value) => {
    const { validateValue, createCharacterLimit } = this.props;
    if ((validateValue && validateValue(value)) || value == "") {
      this.setState({ inputValue: value });
    } else if (!validateValue) {
      if (value.length < createCharacterLimit) this.setState({ inputValue: value });
    }
  };
  // function for Creating new task from select dropdown
  addTaskLabel = (inputValue) => {
    const { createText, classes } = this.props;
    return (
      <>
        {" "}
        <span className={classes.newItemLabel}>{createText}</span> {inputValue}{" "}
      </>
    );
  };
  //On Select Menu Open
  handleMenuOpen = () => {
    this.setState({ menuOpen: true });
  };
  //On Select Menu Open
  handleMenuClose = () => {
    this.setState({ menuOpen: false });
  };

  render() {
    const {
      theme,
      classes,
      label,
      placeholder,
      name,
      styles,
      isMulti = true,
      isDisabled = false,
      isClearable = false,
      customStyles = {},
      id,
      disableIndicator,
      clearOptionAfterSelect = false,
      noOptionMessage,
      ...rest
    } = this.props;
    const { options, selectedValue, inputValue, menuOpen } = this.state;
    return (
      <FormControl
        fullWidth={true}
        classes={{ root: classes.reactSelectFormControl }}
        style={styles}
        id={id}
      >
        <InputLabel
          htmlFor="AssignedToLabel"
          classes={{
            root: menuOpen ? classes.selectLabelSelected : classes.selectLabel,
          }}
          shrink={false}
        >
          {label}
        </InputLabel>
        <CreatableSelect
          isMulti={isMulti}
          ref={(ref) => {
            this.selectEle = ref;
          }}
          formatCreateLabel={this.addTaskLabel}
          noOptionsMessage={() => {
            <span className={classes.noResultFoundTxt}>
              {noOptionMessage}
            </span>
          }}
          onMenuOpen={this.handleMenuOpen}
          onMenuClose={this.handleMenuClose}
          cropWithEllipsis={true}
          components={{
            Option: this.CustomOptionComponent,
            MultiValueRemove: this.MultiValueRemove,
            DropdownIndicator: disableIndicator ? null : this.DropdownIndicator,
            ClearIndicator: this.ClearIndicator,
            MultiValueLabel: this.MultiValueLabel,
            MenuList: this.MenuList,
            SingleValue: this.SingleValue,
            IndicatorSeparator: () => {
              return false;
            },
          }}
          placeholder={placeholder}
          onInputChange={this.handleInputChange}
          isClearable={isClearable}
          name={name}
          options={options}
          inputValue={inputValue}
          closeMenuOnSelect={true}
          autoFocus={false}
          isDisabled={isDisabled}
          value={selectedValue}
          styles={selectCustomStyles(theme, customStyles, menuOpen)}
          onChange={this.handleSelectChange}
          {...rest}
        />
      </FormControl>
    );
  }
}
CreateableSelectDropdown.defaultProps = { 
  noOptionMessage: "No results found related to your search",
  acceptDataInFormOfFun: true,
  createCharacterLimit: 250
};

export default withStyles(searchStyles, { withTheme: true })(
  CreateableSelectDropdown
);
