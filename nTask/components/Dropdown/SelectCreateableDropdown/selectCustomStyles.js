export const selectCustomStyles = (theme, {control}, menuOpen) => {
  return {
  option: (provided, { data, isDisabled, isFocused, isSelected }) => {
    return {
      ...provided,
      fontSize: "12px !important",
      fontWeight: 600,
      padding: "8px 10px",
      backgroundColor: isDisabled
        ? null
        : isSelected
        ? theme.palette.background.azureLight
        : isFocused
        ? theme.palette.background.items
        : null,
      color: isDisabled
        ? theme.palette.text.primary
        : isSelected
        ? theme.palette.text.primary
        : isFocused
        ? theme.palette.text.primary
        : null,
      ":active": {
        backgroundColor:
          !isDisabled &&
          (isSelected ? theme.palette.background.items : null),
        color:
          !isDisabled && (isSelected ? theme.palette.text.primary : null)
      }
    };
  },
  valueContainer: (provided, state) => ({
    ...provided,
    fontSize: "16px !important",
    padding: "2px 5px",
    fontFamily: theme.typography.fontFamilyLato,

  }),
  container: (provided, state) => ({
    ...provided,
    borderRadius: 4,
    width: "100%"
  }),
  singleValue: (provided, state) => ({
    ...provided,
    fontSize: "13px !important",
    padding: "2px 5px",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  }),
  control: (provided, state) => ({
    ...provided,
    borderRadius: 4,
    maxHeight: 120,
    overflowY: "auto",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    "&:hover":{
        borderColor: theme.palette.border.blueBorder
    },
    ...control
  }),
  placeholder: (provided, state) => ({
    ...provided,
    fontSize: "13px !important",
    color: '#BFBFBF',
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  }),
  menu: (provided, state) => ({
    ...provided,
    boxShadow: "0 6px 12px rgba(0,0,0,0.175)",
    zIndex: 2
  }),
  menuList: (provided, state) => ({
    ...provided,
    maxHeight: 250,
    padding: 0
  }),
  dropdownIndicator: (provided, state) => ({
    ...provided,
    padding: "0 5px 0 0"
  }),
  clearIndicator: (provided, state) => ({
    ...provided,
    padding: 0
  })
}}