import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import classNames from "classnames";
import Fade from "@material-ui/core/Fade";
import customMenuStyles from "../../assets/jss/components/customMenu";

class DropdownMenu extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes, theme, children, closeAction, size, clickAway = true, paperProps, ...rest } = this.props;
    const popperClasses = classNames([classes[`popper${size}`], classes.popper]);

    return (
      <Popper
        className={popperClasses}
        modifiers={{
          flip: {
            enabled: true,
          },
          preventOverflow: {
            enabled: true,
            boundariesElement: "scrollParent",
          },
        }}
        onClick={e => e.stopPropagation()}
        disablePortal
        {...rest}
      >
        <Paper
          classes={{
            root: classes.paperRoot,
          }}
          {...paperProps}
        >
          {clickAway ?
            <ClickAwayListener
            mouseEvent="onMouseUp"
            touchEvent="onTouchEnd"
              onClickAway={closeAction}
            >
              <div>
                {children}
              </div>
            </ClickAwayListener> : children}
        </Paper>
      </Popper>
    );
  }
}

export default withStyles(customMenuStyles, {
  withTheme: true,
})(DropdownMenu);
