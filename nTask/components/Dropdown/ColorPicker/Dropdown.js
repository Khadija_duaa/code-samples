// @flow

import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import ColorPalette from "@material-ui/icons/PaletteOutlined";
import ColorPaletteFilled from "@material-ui/icons/Palette";
import SvgIcon from "@material-ui/core/SvgIcon";
import { FormattedMessage } from "react-intl";
import statusDropdownStyle from "./style";
import DropdownMenu from "../DropdownMenu";
import CustomIconButton from "../../Buttons/CustomIconButton";
import ColorPicker from "./ColorPicker";
import CustomTooltip from "../../Tooltip/Tooltip";
import IconColor from "../../Icons/IconColor";

type DropdownProps = {
  classes: Object,
  theme: Object,
  option: Object,
  options: Array<Object>, // List of all options shown in list
  onSelect: Function,
  style: Object,
  placement: String,
  disabled: Boolean,
};

// Onboarding Main Component
function ColorPickerDropdown(props: DropdownProps) {
  const {
    classes,
    theme,
    option,
    options,
    onSelect,
    selectedColor = "",
    style,
    placement,
    disabled,
    iconRenderer,
    colors,
    customColorBtnRender,
    handleCloseColor,
  } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [projectColor, setProjectColor] = useState(selectedColor);
  const handleClick = event => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = () => {
    // Function closes dropdown
    setAnchorEl(null);
    // BulkAction ColorDropDown Close Function
    if (handleCloseColor) {
      handleCloseColor();
    }
  };
  const handleItemClick = (event, option) => {
    // Fuction responsible for selecting item from the list
    setAnchorEl(null);
    onSelect(option); // Callback on item select used to lift up option to parent if needed
  };
  const onColorChange = color => {
    setProjectColor(color);
    onSelect(color) // Callback on item select used to lift up option to parent if needed
    handleClose();
  };
  useEffect(() => {
    setProjectColor(selectedColor);
  }, [selectedColor]);

  const open = Boolean(anchorEl);
  return (
    <>
      {customColorBtnRender ? customColorBtnRender :
        <CustomIconButton
          ref={anchorEl}
          onClick={handleClick}
          btnType="transparent"
          variant="contained"
          style={{ width: 26, height: 26, borderRadius: 4, ...style }}
          disabled={disabled}>
          {iconRenderer ? (
            <CustomTooltip
              helptext={<FormattedMessage id="common.action.color.label" defaultMessage="Color" />}
              iconType="help"
              placement="top"
              style={{ color: theme.palette.common.white }}>{iconRenderer}</CustomTooltip>
          ) : (
            <>
              {projectColor === "" || projectColor == "#FFFFFF" || projectColor == "#ffffff" || projectColor == "#fff" ? (
                <CustomTooltip
                  helptext={<FormattedMessage id="common.action.color.label" defaultMessage="Color" />}
                  iconType="help"
                  placement="top"
                  style={{ color: theme.palette.common.white }}>
                  <SvgIcon
                    classes={{ root: classes.quickFilterIconSize }}
                    viewBox="0 0 24 24" htmlColor={theme.palette.secondary.medDark} >
                    <IconColor />
                  </SvgIcon>
                </CustomTooltip>
              ) : (
                <CustomTooltip
                  helptext={<FormattedMessage id="common.action.color.label" defaultMessage="Color" />}
                  iconType="help"
                  placement="top"
                  style={{ color: theme.palette.common.white }}>
                  <ColorPaletteFilled
                    htmlColor={projectColor}
                    className={classes.projectColorIcon}
                  />
                </CustomTooltip>
              )}
            </>
          )}
        </CustomIconButton>
      }
      <DropdownMenu
        open={props.anchorEl ? Boolean(props.anchorEl) : open}
        closeAction={handleClose}
        anchorEl={props.anchorEl ? props.anchorEl : anchorEl}
        size="xsmall"
        placement={placement}>
        <ColorPicker
          triangle="hide"
          onColorChange={onColorChange}
          selectedColor={projectColor}
          colors={colors}
        />
      </DropdownMenu>
    </>
  );
}
ColorPickerDropdown.defaultProps = {
  style: {},
  placement: "bottom-start",
  disabled: false,
  customColorBtnRender: false,
  handleCloseColor: () => { },
  colors: [
    "#FF6900",
    "#FCB900",
    "#7BDCB5",
    "#8ED1FC",
    "#0693E3",
    "#ABB8C3",
    "#EB144C",
    "#F78DA7",
    "#9900EF",
    "#FFF",
    // "#d25a4b",
    // "#abbc43",
    // "#686868",
    // "#ad475e",
    // "#adefd1ff"
  ],
};

export default compose(withStyles(statusDropdownStyle, { withTheme: true }))(ColorPickerDropdown);
