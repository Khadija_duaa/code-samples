// @flow

import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import statusDropdownStyle from "./style";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { TwitterPicker } from "react-color";

function ColorPicker(props: any) {
  const { selectedColor = "#fff", onColorChange, ...rest } = props;
  const [background, setBackground] = useState(selectedColor);
  const handleChangeComplete = (color) => {
    setBackground(color.hex);
    onColorChange(color.hex);
  };
  return (
    <TwitterPicker
      onChangeComplete={handleChangeComplete}
      color={background}
      width={210}
      {...rest}
    />
  );
}

export default ColorPicker;
