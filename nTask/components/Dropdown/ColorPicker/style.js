const statusDropdownStyle = (theme) => ({
  listItemTextRoot: {
    padding: 0,
  },
  listItemText: {
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  quickFilterIconSize:{
    fontSize: "1.5rem"
  },
  listItem: {
    padding: "6px 14px",
    position: "relative",
  },
  statusIcon: {
    fontSize: "18px !important",
  },
  selectedItemBorder: {
    background: theme.palette.background.blue,
    width: 3,
    position: "absolute",
    left: 0,
    height: 30,
    borderRadius: "0 4px 4px 0",
  },
  projectColorIcon: {
    fontSize: "24px !important",
  },
});

export default statusDropdownStyle;
