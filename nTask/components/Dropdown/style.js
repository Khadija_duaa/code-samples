const dropdownStyles = theme => ({
  list: {
    padding: 0
  },
  headingItem: {
    padding: "0 20px",
    outline: "none",
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "unset"
    }
  },
  listItemHeading: {
    // Dropdown Heading Item Text
    padding: "5px 16px",
    fontSize: "12px !important",
    marginTop: 5,
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    color: theme.palette.secondary.light
  },
  item: {

  },
  searchMenuList: {
    outline: "none"
  },
  listItemRoot: {
    padding: "0 16px",
    flex: "1 1 auto",
    minWidth: 0,
    marginTop: "4px",
    marginBottom: "4px",
  },
  menuItemavatar: {
    padding: "2px 16px 2px 16px",
    marginBottom: 2
  },
  menuItemPlain: {
    padding: "8px 16px 8px 16px",
    marginBottom: 2
  },
  menuItem: {
    padding: "8px 16px 8px 16px",
    marginBottom: 2
  },
  itemText: {
    color: theme.palette.text.primary,
    fontSize: "12.5px !important",
    width: 134,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  }
  ,
  //Item text
  itemTextRoot: {
    padding: "0 16px",
    // },
    // itemText: {
    // width: 134,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  },
  itemTextPrimary: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
  },
  itemSelected: { // Selected Item
    background: `${theme.palette.common.white} !important`,
    "&:hover": {
      background: `${theme.palette.background.items}`
    },
    "&:focus": {
      background: `${theme.palette.background.items} !important`
    }
  },
  //Icon Styles
  statusIcon: {
    // Task Status Icon
    fontSize: "11px !important"
  },
  piriorityIcon: {
    // Task Piriority Icon
    fontSize: "14px !important"
  },
  severityIcon: { // Isue Severity Icon
    fontSize: "14px !important"
  },
  TotalAssignee: { // Assignee Dropdown
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main
  },
  menuListAvatar: { // menu List avatar
    width: 24,
    height: 24
  },
  dropdownSearchCnt: {
    padding: "10px 10px 0 10px"
  },

  searchInputItem: { // list item with search input
    padding: "11px 14px"
  },
  dropdownSearchCntCustomField: {
    padding: "10px 10px 10px 10px"
  },
  selectedValue: { //Adds left border to selected list item
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main,
    }
  },
  bugIcon: {
    fontSize: "16px !important"
  },
  ratingStarIcon: {
    fontSize: "16px !important"
  },
  ImprovmentIcon: {
    fontSize: "16px !important"
  },
  removeAssigneeIcon: {
    fontSize: "14px !important",
    cursor: "pointer"
  },
  addAssigneeBtn: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 7,
    marginRight: 5,
    borderRadius: "100%"
  },
  addAssigneeBtnsmall: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 2,
    marginRight: 2,
    borderRadius: "100%"
  },

  addAssigneeBtnIcon: {
    fontSize: "20px !important"
  },
  TotalAssignee: {
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main,
    width: 36,
    height: 36,
    padding: 9,
    fontSize: "17px"
  },
  assigneeChipCnt: {
    listStyleType: "none",
    padding: 0,
    maxWidth: 'calc(100% - 26px)',
    '& li': {
      display: "inline-block",
      maxWidth: '100%',
    }
  },
  chipRoot: {
    width: '100%',
    // display: 'block',
    padding: 5,
    height: 30,
    background: theme.palette.background.medium,
    // marginRight: 5,
    margin: "2px 6px 3px 0px",
    "& > div": {
      display: 'inline',
    },
    "&:focus": {
      background: theme.palette.background.medium,
    }
  },
  chipLabel: {
    padding: "0 12px 0 5px",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "12px !important",
    fontWeight: 700,
    maxWidth: "calc(100% - 36px)",
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    display: "inline-block"
  },
  crossIcon: {
    fontSize: "14px !important",
    color: theme.palette.icon.gray300
  },
});

export default dropdownStyles;
