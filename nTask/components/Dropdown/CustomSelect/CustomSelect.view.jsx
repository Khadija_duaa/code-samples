import { ClickAwayListener, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import CheckIcon from "@material-ui/icons/Check";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import React, { useRef, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import CustomButton from "../../Buttons/CustomButton";
import CustomListItem from "../../ListItem/CustomListItem";
import CustomMenuList from "../../MenuList/CustomMenuList";
import DropdownMenu from "../DropdownMenu";
import { styles } from "./CustomSelect.style";

const CustomSelect = (props) => {
  const {
    Button = (
      <CustomButton
        btnType="transparent"
        style={{
          whiteSpace: "nowrap",
          padding: "4px 8px",
          fontSize: "11px",
          background: props.theme.palette.background.items,
          textTransform: "none",
        }}
      />
    ),
    classes,
    options,
    width,
    height,
    onChange,
    defaultValue,
    value,
    onClose,
    className,
    style,
    maxWidth = "100%",
    size = "small",
    placeholder = "Select",
    showCheck = true,
    bgapply = true,
    render,
  } = props;

  const buttonref = useRef();
  const [open, setOpen] = useState(false);
  const [selected, setSelected] = useState(
    options?.find((option) => option.value === defaultValue)
  );

  function onClick(option) {
    setOpen(false);
    setSelected(option);
    onChange?.(option);
  }
  function handleClose() {
    setOpen(false);
    onClose?.();
  }

  return (
    <ClickAwayListener onClickAway={handleClose}> 
      <div style={{ position: "relative" }}>
        {{
          ...Button,
          props: {
            ...Button.props,
            className: `${Button.props.className} ${className}`,
            style: { ...Button.props.style, ...style },
            buttonRef: buttonref,
            onClick: () => setOpen(!open),
            children: (
              <>
                <span
                  title={
                    options?.find((option) => option.value === (value ?? selected?.value))?.label
                  }
                  style={{
                    maxWidth,
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                    display: "inline-block",
                    overflow: "hidden",
                  }}>
                  {options?.find((option) => option.value === (value ?? selected?.value))?.label ?? (
                    <Typography variant="body2">{placeholder}</Typography>
                  )}
                </span>
                <ExpandMoreIcon fontSize="14" className={classes.caretdown} />
              </>
            ),
          },
        }}
        <DropdownMenu
          open={open}
          closeAction={() => {
            setOpen(true);
            onClose?.();
          }}
          style={{ width: width }}
          anchorEl={buttonref.current}
          size={size}
          placement="bottom-start">
          <Scrollbars autoHide 
            autoHeight
            autoHeightMin={0}
            autoHeightMax={height}
            // style={{ height: height ?? 30 * options?.length + 16 }}
          >
            <CustomMenuList style={{ padding: "8px 0" }}>
              {options?.map((option, index) => {
                return (
                  <CustomListItem
                    bgapply={bgapply}
                    key={option.key ?? option.id ?? index}
                    darkmode={false}
                    compactMode={true}
                    className={classes.listItem}
                    isSelected={option.value === (value ?? selected?.value)}
                    endIcon={
                      showCheck &&
                      option.value === (value ?? selected?.value) && (
                        <CheckIcon fontSize="14" className={classes.checkIcon} />
                      )
                    }
                    textProps={{style:{ width: '100%'}}}
                    rootProps={{ onClick: () => onClick(option) }}>
                    {render?.(option, option.value === (value ?? selected?.value), index) ?? (
                      <div title={option.label} className={classes.option}>
                        <div>{option.icon}</div>
                        <div>
                          <div
                            style={{
                              textOverflow: "ellipsis",
                              whiteSpace: "nowrap",
                              overflow: "hidden",
                            }}>
                            {option.label}
                          </div>
                          {option.description && (
                            <Typography variant="body1">{option.description}</Typography>
                          )}
                        </div>
                      </div>
                    )}
                  </CustomListItem>
                );
              })}
            </CustomMenuList>
          </Scrollbars>
        </DropdownMenu>
      </div>
     </ClickAwayListener> 
  );
};

export default withStyles(styles, {
  withTheme: true,
})(CustomSelect);
