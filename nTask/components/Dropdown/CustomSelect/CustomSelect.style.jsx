export const styles = (theme) => ({
  checkIcon: {
    color: theme.palette.border.brightBlue,
  },
  caretdown: {
    padding: 2,
    color: theme.palette.text.light,
  },
  listItem: {
    textOverflow: "ellipsis",
    maxWidth: 190,
    whiteSpace: "nowrap",
    display: "inline-block",
    overflow: "hidden",
  },
  option: {
    display: "flex",
    alignItems: "center",
  },
});
