// @flow

import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Scrollbars } from "react-custom-scrollbars";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import CustomButton from "../../Buttons/CustomIconButton";
import DropdownMenu from "../DropdownMenu";
import dropdownStyle from "./style";
import AddIcon from "@material-ui/icons/Add";

type DropdownProps = {
  classes: Object,
  theme: Object,
  option: Object,
  options: Function, // List of all options shown in list
  onSelect: Function,
  label: String,
  size: String,
  scrollHeight: Number,
  disabled: Boolean,
  style: Object,
};

// Onboarding Main Component
function CustomSelectDropdown(props: DropdownProps) {
  const {
    classes,
    theme,
    option,
    options,
    label,
    onSelect,
    size,
    scrollHeight,
    disabled,
    style,
    buttonProps,
  } = props;
  const data = options();
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedOption, setSelectedOption] = useState({});
  const handleClick = event => {
    // Function Opens the dropdown
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const handleItemClick = (event, option) => {
    // Fuction responsible for selecting item from the list
    setSelectedOption(option);
    setAnchorEl(null);
    onSelect(option); // Callback on item select used to lift up option to parent if needed
  };
  const open = Boolean(anchorEl);
  useEffect(() => {
    setSelectedOption(option || {});
  }, [option]);

  return (
    <>
      <CustomButton
        ref={anchorEl}
        onClick={handleClick}
        btnType="blueOutlined"
        variant="contained"
        style={{
          margin: "0px 10px",
          padding: 6,
        }}
        disabled={disabled}
        {...buttonProps}>
        <span className={classes.dropdownValue}>
          <span className={classes.iconCnt}>{selectedOption.icon}</span>
        </span>
      </CustomButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size={size}
        placement="bottom-start">
        <Scrollbars autoHide={false} autoHeight autoHeightMin={0} autoHeightMax={scrollHeight}>
          <List>
            {data.map((o, i) => {
              return (
                <ListItem
                  key={i}
                  button
                  className={classes.listItem}
                  onClick={event => handleItemClick(event, o)}>
                  {o.label == selectedOption.label && (
                    <span className={classes.selectedItemBorder} />
                  )}
                  <div className={classes.itemTextCnt}>
                    {o.icon && (
                      <span className={o.label == selectedOption.label && classes.selectedIcon}>
                        {o.icon}
                      </span>
                    )}
                    <span
                      className={
                        o.label == selectedOption.label
                          ? classes.itemTextSelected
                          : classes.itemText
                      }>
                      {o.label}
                    </span>
                  </div>
                </ListItem>
              );
            })}
          </List>
        </Scrollbars>
      </DropdownMenu>
    </>
  );
}
CustomSelectDropdown.defaultProps = {
  size: "small",
  scrollHeight: 180,
  disabled: false,
};
export default compose(
  withRouter,
  withStyles(dropdownStyle, { withTheme: true })
)(CustomSelectDropdown);
