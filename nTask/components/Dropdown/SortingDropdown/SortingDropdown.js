import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import sortingDropdownStyles from "./style";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../Buttons/IconButton";
import SelectionMenu from "../../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { Scrollbars } from "react-custom-scrollbars";
import DownArrow from "@material-ui/icons/ArrowDropDown";
import UpArrow from "@material-ui/icons/ArrowDropUp";
import CustomButton from "../../Buttons/CustomButton";
import IconButton from "../../Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import AscSortIcon from "../../../assets/images/sort-ascending.svg";
import DescSortIcon from "../../../assets/images/sort-descending.svg";
import sortIcon from "../../../assets/images/icon_sort.svg";
import { teamCanView } from "../../PlanPermission/PlanPermission";
import UnPlanned from "../../../Views/billing/UnPlanned/UnPlanned";
import SortBy from "../../Icons/SortBy";
import SvgIcon from "@material-ui/core/SvgIcon";
import { injectIntl, FormattedMessage } from "react-intl";
import { TRIALPERIOD } from '../../constants/planConstant';

class SortingDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "top-start",
      pickerOpen: false,
      pickerPlacement: "",
      checked: ["None"],
      sort: true,
      sortDirection: "ASC",
      sortColum: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  componentDidMount() {
    if (this.props.getSortOrder) {
      let {
        sortColumn,
        sortDirection,
        taskColumn,
        taskDirection,
        meetingColumn,
        meetingDirection,
        issueColumn,
        issueDirection,
        riskDirection,
        riskColumn,
      } = this.props.getSortOrder;

      if (sortColumn && sortDirection) {
        let sortCol = this.props.data.find((d) => {
          return d.key == sortColumn;
        });
        if (sortCol)
          this.setState({
            checked: [sortCol.name],
            sortDirection: sortDirection,
            sortColum: sortCol.key,
          });
      } else if (taskColumn && taskDirection) {
        let sortCol = this.props.data.find((d) => {
          return d.key == taskColumn;
        });
        if (sortCol)
          this.setState({
            checked: [sortCol.name],
            sortDirection: taskDirection,
            sortColum: sortCol.key,
          });
      } else if (meetingColumn && meetingDirection) {
        let sortCol = this.props.data.find((d) => {
          return d.key == meetingColumn;
        });
        if (sortCol)
          this.setState({
            checked: [sortCol.name],
            sortDirection: meetingDirection,
            sortColum: sortCol.key,
          });
      } else if (issueColumn && issueDirection) {
        let sortCol = this.props.data.find((d) => {
          return d.key == issueColumn;
        });
        if (sortCol)
          this.setState({
            checked: [sortCol.name],
            sortDirection: issueDirection,
            sortColum: sortCol.key,
          });
      } else if (riskColumn && riskDirection) {
        let sortCol = this.props.data.find((d) => {
          return d.key == riskColumn;
        });
        if (sortCol)
          this.setState({
            checked: [sortCol.name],
            sortDirection: riskDirection,
            sortColum: sortCol.key,
          });
      }
    }
  }
  handleClose(event) {
    /** function call when close drop down */
    this.setState({ open: false, pickerOpen: false });
    // this.props.SaveColumnOrder(this.props.projectId, this.state.checked)
  }
  handleClick(event, placement) {
    /** function call for opens drop down  */
    event.stopPropagation();
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }

  handleToggle = (e, value) => {
    /** function call when Select option from drop down  */
    const { checked, sortDirection } = this.state;
    if (checked.indexOf(value) > -1) {
      /** Condition that checkes if the option is already checked and incase it's checked than mark it as unchecked */
      this.setState({ checked: [] });
    } else {
      this.setState({
        checked: [value.name],
        sortColum: value.key,
      }); /** Executes to select the value of sortBy dropdown */
      this.props.sortingList(
        value.key,
        sortDirection,
        ""
      ); /** calling function coming from prop for updating the task list */
    }
  };

  cancelSort = (e) => {
    /** functrion call when click on close icon / cancel sorting */
    e.stopPropagation();
    this.setState({ checked: [], sortDirection: "ASC", sortColum: "" });
    this.props.sortingList(
      "",
      "",
      "NONE"
    ); /** passing sort NONE for cancel sorting */
  };

  sortingDirection = (event) => {
    /** functrion call when click on selected value from drop down and changing the list order Ascending/ Descending */
    event.stopPropagation();
    const { sortColum, sortDirection } = this.state;
    if (sortDirection == "ASC") {
      this.props.sortingList(
        sortColum,
        "DESC",
        ""
      ); /** calling function coming from prop for updating the task list */
      this.setState({ sortDirection: "DESC" });
    } else {
      this.props.sortingList(
        sortColum,
        "ASC",
        ""
      ); /** calling function coming from prop for updating the task list */
      this.setState({ sortDirection: "ASC" });
    }
  };
  getSelectedSortedValues = () => {
    const { workspace, loggedInTeam } = this.props.profileState.data;
    const { data } = this.props;
    let selectedWorkspace = workspace.find((w) => {
      return w.teamId == loggedInTeam;
    });
    let sortCol = selectedWorkspace.sortColumn.project[0];
    let sortDir = selectedWorkspace.sortColumn.projectOrder;

    let sortColName = data.find((d) => {
      return d.key == sortCol;
    });
    if (sortColName && sortDir) {
      return { sortCol: sortColName.name, sortDir: sortDir };
    }
  };

  getTranslatedId = (value) => {
    switch (value) {
      case "None":
        value = "common.sort-by.none";
        break;
      case "ID":
        value = "common.sort-by.id";
        break;
      case "Title":
        value = "common.sort-by.title";
        break;
      case "Status":
        value = "common.status.label";
        break;
      case "Priority":
        value = "issue.common.priority.label";
        break;
      case "Project":
        value = "header.add-new-button.list.project.label";
        break;
      case "Progress":
        value = "common.sort-by.progress";
        break;
      case "Time Logged":
        value = "common.sort-by.timelogged";
        break;
      case "Creation Date":
        value = "common.sort-by.creationdate";
        break;
      case "Actual Start Date":
        value = "common.sort-by.actualstart";
        break;
      case "Actual End Date":
        value = "common.sort-by.actualend";
        break;
      case "Planned Start Date":
        value = "common.sort-by.plannedStart";
        break;
      case "Planned End Date":
        value = "common.sort-by.plannedend";
        break;
      case "Project Title":
        value = "common.sort-by.projecttitle";
        break;
      case "Date":
        value = "common.sort-by.date";
        break;
      case "Time":
        value = "common.sort-by.time";
        break;
      case "Duration":
        value = "common.sort-by.duration";
        break;
      case "Location":
        value = "common.sort-by.location";
        break;
      case "Severity":
        value = "issue.detail-dialog.severity.label";
        break;
      case "Type":
        value = "common.sort-by.type";
        break;
      case "Last Updated":
        value = "common.sort-by.lastupdated";
        break;
      case "Impact":
        value = "risk.creation-dialog.form.impact.label";
        break;
      case "Likelihood":
        value = "risk.common.likelihood.label";
        break;
    }
    return value;
  }

  render() {
    const { classes, theme, data, height, width } = this.props;
    const { open, placement, checked, sortDirection } = this.state;
    let DropDownHeight = height ? height : 250;
    let menuWidth = width ? width : 220;
    let checkSelected = checked.length && checked[0] != "None" ? true : false;
    const sortByLbl = checkSelected
      ? `${this.props.intl.formatMessage({
        id: "common.sort-by.label",
        defaultMessage: "Sort By",
      })} : `
      : this.props.intl.formatMessage({
        id: "common.sort-by.label",
        defaultMessage: "Sort By",
      });
    let checkedLbl = checkSelected ? this.props.intl.formatMessage({ id: this.getTranslatedId(checked), defaultMessage: checked }) : "";
    // let checkedLbl = checkSelected ? checked : "";
    // let sortedValues = this.getSelectedSortedValues();
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <div className={classes.sortBtnsCnt}>
            <CustomButton /** Sort By Button  */
              onClick={(event) => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
              style={{
                // minWidth: 95,
                padding: checkSelected ? "4px 8px 4px 4px" : "3px 4px",
                display: "flex",
                justifyContent: "space-between",
                minWidth: "auto",
                whiteSpace: "nowrap",
              }}
              btnType={checkSelected ? "lightBlue" : "white"}
              variant="contained"
            >
              <SvgIcon
                viewBox="0 0 24 24"
                className={
                  checkSelected ? classes.sortIconSelected : classes.sortIconSvg
                }
              >
                <SortBy />
              </SvgIcon>
              <span
                className={
                  checkSelected ? classes.sortLblSelected : classes.sortLbl
                }
              >
                {sortByLbl}
              </span>
              {checkSelected ? (
                <span className={classes.checkname}>
                  <FormattedMessage id={this.getTranslatedId(checkedLbl)} defaultMessage={checkedLbl} />
                  <span onClick={this.sortingDirection}>
                    {sortDirection ==
                      "ASC" /** if list is in ascending order the up arrows shows other wise down arrow descibe decending order  */ ? (
                      <img src={AscSortIcon} className={classes.asdecIcon} />
                    ) : (
                      <img src={DescSortIcon} className={classes.asdecIcon} />
                    )}
                  </span>
                </span>
              ) : null}
            </CustomButton>
          </div>
          <SelectionMenu
            open={open}
            closeAction={
              this.handleClose
            } /** function call when close drop down */
            placement={placement}
            checkedType="multi"
            anchorRef={this.anchorEl}
            style={{ width: !teamCanView("advanceSortAccess") ? 400 : menuWidth }}
            list={
              <Scrollbars
                autoHide
                style={{
                  height: !teamCanView("advanceSortAccess")
                    ? 370
                    : DropDownHeight,
                }}
              >
                {!teamCanView("advanceSortAccess") ? (
                  <div className={classes.unplannedMain}>
                    <UnPlanned
                      feature="premium"
                      titleTxt={
                        <FormattedMessage
                          id="common.discovered-dialog.premium-title"
                          defaultMessage="Wow! You've discovered a Premium feature!"
                        />
                      }
                      boldText={this.props.intl.formatMessage({
                        id: "common.discovered-dialog.list.sort-by.title",
                        defaultMessage: "Sort By",
                      })}
                      descriptionTxt={
                        <FormattedMessage
                          id="common.discovered-dialog.list.sort-by.label"
                          defaultMessage={"is available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."}
                          values={{ TRIALPERIOD: TRIALPERIOD }}
                        />
                      }
                      showDescription={true}
                      showBodyImg={false}
                      selectUpgrade={this.handleClose}
                      titleStyle={{ fontSize: "18px !important" }}
                    />
                  </div>
                ) : (
                  <List>
                    {data.map((
                      value /** displaying the drop down list array coming from props  */
                    ) => (
                      <ListItem
                        key={value.key}
                        button
                        disableRipple={true}
                        className={`${classes.MenuItem} ${this.state.checked.indexOf(value.name) !== -1
                            ? classes.selectedValue
                            : ""
                          }`}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={(e) =>
                          this.handleToggle(e, value)
                        } /** on select value from drop down , function call passing the whole 'value' object */
                      >
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id={this.getTranslatedId(value.name)}
                              defaultMessage={value.name}
                            />
                          } /** displaying name property from array coming ffrom props */
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                      </ListItem>
                    ))}
                  </List>
                )}
              </Scrollbars>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    workspaces: state.profile.data.workspace,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(combineStyles(sortingDropdownStyles, menuStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps)
)(SortingDropdown);
