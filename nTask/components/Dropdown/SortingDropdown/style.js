const sortingDropdownStyles = theme => ({
    sortBtnsCnt: {
        display: "flex",
        marginRight: 8
    },
    closeIcon: {
        fontSize: "12px !important"
    },
    asdecIcon:{
        marginLeft:5
    },
    unplannedMain: {
        padding: '24px 18px',
        display: 'flex',
        alignItems: 'center',
        height: '100%',
    },
    sortIconSvg: {
        color: theme.palette.secondary.medDark
    },
    sortIconSelected: {
        color: theme.palette.text.azure
    },
    sortLbl: {
        paddingRight: 4
    },
    sortLblSelected: {
        paddingRight: 0
    },
    checkname: {
        paddingLeft: 4,
    },
    "@media (max-width: 1024px)": {
        sortLbl: {
          display: 'none',
        },
        sortLblSelected: {
            display: 'none',
        },
        checkname: {
            paddingLeft: 0,
        },
      },
    
})
export default sortingDropdownStyles