const sortingDropdownStyles = theme => ({
    sortBtnsCnt: {
        display: "flex",
        marginRight: 8
    },
    closeIcon: {
        fontSize: "12px !important"
    },
    asdecIcon:{
        marginRight:10
    },
    unplannedMain: {
        padding: '24px 18px',
        display: 'flex',
        alignItems: 'center',
        height: '100%',
    },
    groupIconSvg:{
        color: theme.palette.secondary.medDark
    },
    groupIconSelected: {

        color: theme.palette.text.azure
    },
    groupLbl: {
        paddingRight: 4
    },
    groupLblSelected: {
        paddingRight: 0
    },
    checkname: {
        paddingLeft: 4,
    },
    "@media (max-width: 1024px)": {
        groupLbl: {
          display: 'none',
        },
        groupLblSelected: {
            display: 'none',
        },
        checkname: {
            paddingLeft: 0,
        },
      },
})
export default sortingDropdownStyles