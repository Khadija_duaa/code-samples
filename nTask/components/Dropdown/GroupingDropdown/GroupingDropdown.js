import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import sortingDropdownStyles from "./style";
import menuStyles from "../../../assets/jss/components/menu";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import SelectionMenu from "../../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../../utils/mergeStyles";
import { Scrollbars } from "react-custom-scrollbars";
import CustomButton from "../../Buttons/CustomButton";
import IconButton from "../../Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import GroupingIcon from "@material-ui/icons/Layers";
import { updateGroupColumn } from "../../../redux/actions/workspace";
import GroupBy from "../../Icons/GroupBy";
import SvgIcon from "@material-ui/core/SvgIcon";
import { injectIntl, FormattedMessage } from "react-intl";
import { TRIALPERIOD } from '../../constants/planConstant';

class GroupingDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "top-start",
      pickerOpen: false,
      pickerPlacement: "",
      checked: {},
      sort: true,
      sortColum: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  componentDidMount() {
    const { selectedGroup } = this.props;
    this.setState({ checked: selectedGroup });
  }
  componentDidUpdate(prevProps, prevState) {
    const { selectedGroup } = this.props;
    if (prevProps.selectedGroup.key != selectedGroup.key) {
      this.setState({ checked: selectedGroup });
    }
  }

  handleClose(event) {
    /** function call when close drop down */
    this.setState({ open: false, pickerOpen: false });
    // this.props.SaveColumnOrder(this.props.projectId, this.state.checked)
  }
  handleClick(event, placement) {
    /** function call for opens drop down  */
    event.stopPropagation();
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }

  handleToggle = (e, value) => {
    /** function call when Select option from drop down  */
    const { checked } = this.state;
    const { onGroupSelect } = this.props;
    if (checked.key == value.key) {
      /** Condition that checkes if the option is already checked and incase it's checked than mark it as unchecked */
      return;
    } else {
      this.setState({
        checked: value,
        open: false,
      }); /** Executes to select the value of sortBy dropdown */
      onGroupSelect(value);
      this.handleGroupingSave(value.key);
    }
  };
  handleGroupingSave = (value) => {
    // Function that handles saving Grouping to backend
    const { view, itemOrderState } = this.props;
    let groupByColumn = itemOrderState.groupByColumn;
    switch (view) {
      case "task": // Case for saving the grouping of task
        groupByColumn = { ...groupByColumn, taskGroupBy: [value] };
        break;
      case "project": // Case for saving the grouping of task
        groupByColumn = { ...groupByColumn, projectGroupBy: [value] };
        break;
      case "risk": // Case for saving the grouping of risk
        groupByColumn = { ...groupByColumn, riskGroupBy: [value] };
        break;
      case "issue": // Case for saving the grouping of issue
        groupByColumn = { ...groupByColumn, issueGroupBy: [value] };
        break;
      case "meeting": // Case for saving the grouping of meeting
        groupByColumn = { ...groupByColumn, meetingGroupBy: [value] };
        break;
      default:
        break;
    }
    this.props.updateGroupColumn(groupByColumn);
  };
  cancelGrouping = (e) => {
    /** functrion call when click on close icon / cancel Grouping */
    const { onGroupSelect } = this.props;
    e.stopPropagation();
    this.setState({ checked: {} });
    onGroupSelect({});
    this.handleGroupingSave("");
  };

  getTranslatedId(value) {
    switch (value) {
      case "ID":
        value = "common.sort-by.id";
        break;
      case "None":
        value = "common.sort-by.none";
        break;
      case "Task":
        value = "issue.detail-dialog.task.label";
        break;
      case "Status":
        value = "common.status.label";
        break;
      case "Planned Start/End":
        value = "common.group-by.plannedStart";
        break;
      case "Actual Start/End":
        value = "common.group-by.actualstartend";
        break;
      case "Priority":
        value = "issue.common.priority.label";
        break;
      case "Project":
        value = "header.add-new-button.list.project.label";
        break;
      case "Progress":
        value = "common.sort-by.progress";
        break;
      case "Assignee":
        value = "common.assigned.assignee";
        break;
      case "Time Logged":
        value = "common.sort-by.timelogged";
        break;
      case "Attachments":
        value = "project.gant-view.columns.attachment";
        break;
      case "Comments":
        value = "common.comment.label";
        break;
      case "Meetings":
        value = "profile-settings-dialog.apps-integrations.common.meeting.label";
        break;
      case "Issues":
        value = "profile-settings-dialog.apps-integrations.common.issue.label";
        break;
      case "Risks":
        value = "risk.label";
        break;
      case "Creation Date":
        value = "common.sort-by.creationdate";
        break;
      case "Creation By":
        value = "common.group-by.createdby";
        break;
      case "Location":
        value = "common.sort-by.location";
        break;
      case "Severity":
        value = "issue.detail-dialog.severity.label";
        break;
      case "Type":
        value = "common.sort-by.type";
        break;
      case "Impact":
        value = "risk.creation-dialog.form.impact.label";
        break;
      case "Likelihood":
        value = "risk.common.likelihood.label";
        break;
    }
    return value;
  }

  render() {
    const { classes, theme, data, height, width, id } = this.props;
    const { open, placement, checked = {}, sortDirection } = this.state;
    let DropDownHeight = height ? height : 250;
    let menuWidth = width ? width : 220;
    let checkSelected = checked.name && checked.name != "None" ? true : false;
    let groupByLbl = checkSelected
      ? `${this.props.intl.formatMessage({
        id: "common.group-by.label",
        defaultMessage: "Group By",
      })} : `
      : this.props.intl.formatMessage({
        id: "common.group-by.label",
        defaultMessage: "Group By",
      });
    let checkedLbl = checkSelected ? this.props.intl.formatMessage({
      id: this.getTranslatedId(checked.name), defaultMessage: checked.name
    }) : "";
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <div className={classes.sortBtnsCnt}>
            <CustomButton /** Sort By Button  */
              onClick={(event) => {
                this.handleClick(event, "bottom-end");
              }}
              id={id}
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
              style={{
                // minWidth: 95,
                padding: checkSelected ? "4px 8px 4px 4px" : "3px 4px",
                borderRadius: "4px",
                display: "flex",
                justifyContent: "space-between",
                minWidth: "auto",
                whiteSpace: "nowrap",
              }}
              btnType={checkSelected ? "lightBlue" : "white"}
              variant="contained"
            >
              <SvgIcon
                viewBox="0 0 24 24"
                className={
                  checkSelected ? classes.groupIconSelected : classes.groupIconSvg
                }
              >
                <GroupBy />
              </SvgIcon>
              <span
                className={
                  checkSelected ? classes.groupLblSelected : classes.groupLbl
                }
              >
                {groupByLbl}
              </span>
              {checkSelected ? (
                <span className={classes.checkname}>{checkedLbl}</span>
              ) : null}
            </CustomButton>
          </div>
          <SelectionMenu
            open={open}
            closeAction={
              this.handleClose
            } /** function call when close drop down */
            placement={placement}
            checkedType="multi"
            anchorRef={this.anchorEl}
            style={{ width: menuWidth }}
            list={
              <Scrollbars autoHide style={{ height: DropDownHeight }}>
                {/* {!teamCanView("advanceSortAccess") ?
                <div className={classes.unplannedMain}>
                  <UnPlanned
                    feature='premium'
                    titleTxt="Wow! You've discovered a Premium feature!"
                    descriptionTxt={"Sort By is available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."}
                    values={{TRIALPERIOD: TRIALPERIOD}}
                    showDescription={true}
                    showBodyImg={false}
                    selectUpgrade={this.handleClose}
                    titleStyle={{ fontSize: "18px !important" }}
                  />
                </div> : */}
                <List>
                  {data.map((
                    value /** displaying the drop down list array coming from props  */
                  ) => (
                    <ListItem
                      key={value.key}
                      button
                      disableRipple={true}
                      className={`${classes.MenuItem} ${this.state.checked.key == value.key
                          ? classes.selectedValue
                          : ""
                        }`}
                      classes={{ selected: classes.statusMenuItemSelected }}
                      onClick={(e) =>
                        this.handleToggle(e, value)
                      } /** on select value from drop down , function call passing the whole 'value' object */
                    >
                      <ListItemText
                        primary={
                          <FormattedMessage
                            id={this.getTranslatedId(value.name)}
                            defaultMessage={value.name}
                          />
                        } /** displaying name property from array coming ffrom props */
                        classes={{
                          primary: classes.statusItemText,
                        }}
                      />
                    </ListItem>
                  ))}
                </List>
              </Scrollbars>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    itemOrderState: state.itemOrder.data,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(combineStyles(sortingDropdownStyles, menuStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, { updateGroupColumn })
)(GroupingDropdown);
