// @flow

import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Scrollbars } from "react-custom-scrollbars";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import CustomButton from "../../Buttons/CustomButton";
import DropdownMenu from "../DropdownMenu";
import dropdownStyle from "./style";
import CustomIconButton from "../../Buttons/CustomIconButton";
import Typography from "@material-ui/core/Typography";
import DefaultCheckbox from "../../Form/Checkbox";
import SearchInput, { createFilter } from "react-search-input";
import DefaultTextField from "../../Form/TextField";

type DropdownProps = {
  classes: Object,
  theme: Object,
  option: Object,
  options: Function, // List of all options shown in list
  onSelect: Function,
  label: String,
  size: String,
  scrollHeight: Number,
  disabled: Boolean,
  style: Object,
  iconVisible: Boolean,
};

// Onboarding Main Component
function CustomMultiSelectDropdown(props: DropdownProps) {
  const {
    classes,
    theme,
    option,
    options,
    label = null,
    onSelect,
    size,
    scrollHeight,
    disabled,
    style,
    iconVisible = true,
    buttonProps,
    selectionLabel,
    iconButton,
    maxSelections,
    icon,
    heading,
    btnCntStyles,
    hideLabel,
    placeholder = null,
    inline,
    optionIcon,
    valueSelector,
    dropdownProps
  } = props;
  const data = options();
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedOptions, setSelectedOptions] = useState([]);
  const [selectAll, setSelectAll] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");

  const handleClick = event => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const handleItemClick = (event, optionSelected) => {
    // Fuction responsible for selecting item from the list

    const isSelected = selectedOptions.find(s => s[valueSelector] === optionSelected[valueSelector]);
    if (isSelected) {
      const newSelectedOptions = selectedOptions.filter(o => o[valueSelector] !== optionSelected[valueSelector]);
      setSelectAll(false);
      setSelectedOptions(newSelectedOptions);
      onSelect(newSelectedOptions); // Callback on item select used to lift up option to parent if needed
    } else {
      //return in case of maximum selections reached
      if (maxSelections && selectedOptions.length === maxSelections) {
        return;
      }
      setSelectAll(false);
      setSelectedOptions(
        selectedOptions.length ? [...selectedOptions, optionSelected] : [optionSelected]
      );
      onSelect(selectedOptions.length ? [...selectedOptions, optionSelected] : [optionSelected]); // Callback on item select used to lift up option to parent if needed
    }
  };

  const handleSelectAllOption = (e, keyOption) => {
    //return in case of maximum selections reached
    if (keyOption) {
      setSelectAll(false);
      setSelectedOptions([]);
      onSelect([]); // Callback on item select used to lift up option to parent if needed
    } else {
      setSelectAll(true);
      setSelectedOptions(data);
      onSelect(data); // Callback on item select used to lift up option to parent if needed
    }
  };

  const searchUpdated = e => {
    setSearchTerm(e.target.value);
  };

  const open = Boolean(anchorEl);
  useEffect(() => {
    if(option) {
      setSelectedOptions(option);
      if (option.length === data.length) setSelectAll(true);
    }
  }, [option]);

  const filteredData = data.filter(createFilter(searchTerm, ["label"]));

  return (
    <>
      
      {inline ? <List>
          <ListItem disableRipple={true}>
            <DefaultTextField
              error={false}
              errorState={false}
              errorMessage={""}
              formControlStyles={{ marginBottom: 0 }}
              styles={classes.searchInput}
              defaultProps={{
                type: "text",
                placeholder: `Search ${label || placeholder}`,
                value: searchTerm,
                autoFocus: true,
                inputProps: { maxLength: 80 },
                onChange: e => {
                  searchUpdated(e);
                },
                onKeyDown: () => {},
                onBlur: () => {},
              }}
            />
          </ListItem>
          {filteredData.length !== 0 && (
            <ListItem
              key={"allOptions"}
              button
              className={classes.listItemSelectOption}
              onClick={e => {
                handleSelectAllOption(e, selectAll);
              }}>
              <div className={classes.itemTextCnt}>
                <DefaultCheckbox
                  checkboxStyles={{ padding: "0 5px 0 0" }}
                  checked={selectAll}
                  onChange={() => {}}
                  unCheckedIconProps={{
                    style: {
                      fontSize: "18px",
                    },
                  }}
                  checkedIconProps={{
                    style: {
                      fontSize: "18px",
                    },
                  }}
                  color={"#0090ff"}
                  disabled={false}
                />
                <span className={classes.itemText}>
                  {selectAll ? "Unselect All" : "Select All"}
                </span>
              </div>
            </ListItem>
          )}
          {filteredData.length == 0 && (
            <div className={classes.noDataContainer}>
              <span> No records found </span>
            </div>
          )}
          <Scrollbars autoHide={false} autoHeight autoHeightMin={0} autoHeightMax={scrollHeight}>
            {filteredData.map((o, i) => {
              const isSelected = selectedOptions.find(so => o[valueSelector] === so[valueSelector]) || false;
              return (
                <ListItem
                  key={i}
                  button
                  className={classes.listItem}
                  onClick={event => handleItemClick(event, o)}>
                  <div className={classes.itemTextCnt}>
                    <DefaultCheckbox
                      checkboxStyles={{ padding: "0 5px 0 0" }}
                      checked={isSelected ? true : false}
                      onChange={() => {}}
                      unCheckedIconProps={{
                        style: {
                          fontSize: "18px",
                        },
                      }}
                      checkedIconProps={{
                        style: {
                          fontSize: "18px",
                        },
                      }}
                      color={"#0090ff"}
                      disabled={false}
                    />
                    {/* <span className={isSelected ? classes.itemTextSelected : classes.itemText}> */}
                    {optionIcon && o.icon}
                    <span className={classes.itemText}>{o.label}</span>
                    {o.caption && <span className={classes.itemTextCaption}>{o.caption}</span>}
                  </div>
                </ListItem>
              );
            })}
          </Scrollbars>
        </List> :
        <>
        {iconButton ? (
        <CustomIconButton
          disabled={disabled}
          ref={anchorEl}
          onClick={handleClick}
          btnType="transparent"
          {...buttonProps}>
          {icon}
        </CustomIconButton>
      ) : (
        <div className={classes.btnContainer} style={btnCntStyles}>
          {hideLabel ? "" : <span className={classes.dropdownLabel}>{label}</span>}
          <CustomButton
            ref={anchorEl}
            onClick={handleClick}
            btnType="plainbackground"
            variant="text"
            style={{
              padding: 7,
              borderRadius: 4,
            }}
            disabled={disabled}
            {...buttonProps}>
            <span className={classes.dropdownValue}>
              {" "}
              <span>
                {`${
                  // selectedOptions.length > 1
                  //   ? `${selectedOptions[0].label} , ${selectedOptions[1].label}`
                  //   :
                  selectedOptions.length > 0
                    ? `${selectedOptions[0].label}`
                    : `Select ${label || placeholder}`
                }`}{" "}
                {`${selectedOptions.length > 1 ? ` + ${selectedOptions.length - 1} more..` : ""}`}
                {/* {`${selectedOptions.length > 2 ? ` + ${selectedOptions.length - 2} more..` : ""}`} */}
              </span>
              {iconVisible ? <ArrowDropDownIcon className={classes.ddIcon} /> : ""}
            </span>
          </CustomButton>
        </div>
      )}
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size={size}
        placement="bottom-start"
        {...dropdownProps}
        >
        <List>
          <ListItem disableRipple={true}>
            <DefaultTextField
              error={false}
              errorState={false}
              errorMessage={""}
              formControlStyles={{ marginBottom: 0 }}
              styles={classes.searchInput}
              defaultProps={{
                type: "text",
                placeholder: `Search ${label || placeholder}`,
                value: searchTerm,
                autoFocus: true,
                inputProps: { maxLength: 80 },
                onChange: e => {
                  searchUpdated(e);
                },
                onKeyDown: () => {},
                onBlur: () => {},
              }}
            />
          </ListItem>
          {filteredData.length !== 0 && (
            <ListItem
              key={"allOptions"}
              button
              className={classes.listItemSelectOption}
              onClick={e => {
                handleSelectAllOption(e, selectAll);
              }}>
              <div className={classes.itemTextCnt}>
                <DefaultCheckbox
                  checkboxStyles={{ padding: "0 5px 0 0" }}
                  checked={selectAll}
                  onChange={() => {}}
                  unCheckedIconProps={{
                    style: {
                      fontSize: "18px",
                    },
                  }}
                  checkedIconProps={{
                    style: {
                      fontSize: "18px",
                    },
                  }}
                  color={"#0090ff"}
                  disabled={false}
                />
                <span className={classes.itemText}>
                  {selectAll ? "Unselect All" : "Select All"}
                </span>
              </div>
            </ListItem>
          )}
          {filteredData.length == 0 && (
            <div className={classes.noDataContainer}>
              <span> No records found </span>
            </div>
          )}
          <Scrollbars autoHide={false} autoHeight autoHeightMin={0} autoHeightMax={scrollHeight}>
            {filteredData.map((o, i) => {
              const isSelected = selectedOptions.find(so => o[valueSelector] === so[valueSelector]) || false;
              return (
                <ListItem
                  key={i}
                  button
                  className={classes.listItem}
                  onClick={event => handleItemClick(event, o)}>
                  <div className={classes.itemTextCnt}>
                    <DefaultCheckbox
                      checkboxStyles={{ padding: "0 5px 0 0" }}
                      checked={isSelected ? true : false}
                      onChange={() => {}}
                      unCheckedIconProps={{
                        style: {
                          fontSize: "18px",
                        },
                      }}
                      checkedIconProps={{
                        style: {
                          fontSize: "18px",
                        },
                      }}
                      color={"#0090ff"}
                      disabled={false}
                    />
                    {/* <span className={isSelected ? classes.itemTextSelected : classes.itemText}> */}
                    <span className={classes.itemText}>{o.label}</span>
                    {o.caption && <span className={classes.itemTextCaption}>{o.caption}</span>}
                  </div>
                </ListItem>
              );
            })}
          </Scrollbars>
        </List>
      </DropdownMenu>
</>
}
   </>
  );
}
CustomMultiSelectDropdown.defaultProps = {
  size: "small",
  scrollHeight: 180,
  disabled: false,
  inline: false,
  optionIcon: false,
  valueSelector: 'label'
};
export default compose(
  withRouter,
  withStyles(dropdownStyle, { withTheme: true })
)(CustomMultiSelectDropdown);
