const participantDropdownStyles = theme => ({
  addParticipantCnt: {
    padding: "14px 15px",
    "& *": {
      fontFamily: theme.typography.fontFamilyLato,
    },
    "& button": {
      maxHeight: 32,
    },
  },
  list: {
    padding: "10px 0",
  },
  headingItem: {
    padding: "0 20px",
    outline: "none",
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "unset",
    },
  },
  ///
  addPartActionBtnCnt: {
    display: "flex",
    justifyContent: "flex-end",
  },
  item: {},
  menuItemavatar: {
    padding: "3px 13px 3px 13px",
    marginBottom: 0,
  },
  menuItemPlain: {
    padding: "8px 16px 8px 16px",
    marginBottom: 2,
  },
  menuItem: {
    padding: "8px 16px 8px 16px",
    marginBottom: 2,
  },
  // Item text
  itemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    width: 134,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
  },
  itemSelected: {
    // Selected Item
    background: `${theme.palette.common.white} !important`,
    "&:hover": {
      background: `${theme.palette.background.items}`,
    },
    "&:focus": {
      background: `${theme.palette.background.items} !important`,
    },
  },
  // Icon Styles
  statusIcon: {
    // Task Status Icon
    fontSize: "11px !important",
  },
  piriorityIcon: {
    // Task Piriority Icon
    fontSize: "14px !important",
  },
  severityIcon: {
    // Isue Severity Icon
    fontSize: "14px !important",
  },
  TotalAssignee: {
    // Assignee Dropdown
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main,
  },
  menuListAvatar: {
    // menu List avatar
    width: 24,
    height: 24,
  },
  dropdownSearchCnt: {
    padding: "10px 10px 10px 10px",
  },
  searchInputItem: {
    // list item with search input
    padding: "11px 14px",
  },
  selectedValue: {
    // Adds left border to selected list item
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main,
    },
  },
  bugIcon: {
    fontSize: "16px !important",
  },
  ratingStarIcon: {
    fontSize: "16px !important",
  },
  ImprovmentIcon: {
    fontSize: "16px !important",
  },
  removeAssigneeIcon: {
    fontSize: "14px !important",
    cursor: "pointer",
  },
  addAssigneeBtn: {
    background: theme.palette.common.white,
    padding: "7.5px 5px",
    marginRight: 5,
    borderRadius: 0,
    "&:hover": {
      background: theme.palette.background.azureLight,
    },
    "&:hover svg": {
      color: theme.palette.icon.azure,
    },
  },
  addAssigneeBtnSelected: {
    background: theme.palette.background.azureLight,
    "& svg": {
      color: theme.palette.icon.azure,
    },
  },
  addAssigneeBtnIcon: {
    fontSize: "28px !important",
    color: theme.palette.icon.gray400,
  },
  addPartActionBtnIcon: {
    fontSize: "22px !important",
  },
  addParticipantBtnCnt: {
    padding: "0 13px 10px 13px",
  },
  assigneeChipCnt: {
    listStyleType: "none",
    padding: 0,
    "& li": {
      display: "inline-block",
    },
  },
  chipRoot: {
    padding: 3,
    height: 30,
    background: theme.palette.background.medium,
    // marginRight: 5,
    margin: "2px 6px 3px 0px",
    "&:focus": {
      background: theme.palette.background.medium,
    },
  },
  chipLabel: {
    padding: "0 12px 0 5px",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "12px !important",
    fontWeight: 700,
  },
  crossIcon: {
    fontSize: "14px !important",
    color: theme.palette.icon.gray300,
  },
});

export default participantDropdownStyles;
