import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import { compose } from "redux";
import { connect } from "react-redux";
import RemoveIcon from "@material-ui/icons/Close";
import SearchInput, { createFilter } from "react-search-input";
import { Scrollbars } from "react-custom-scrollbars";
import { FormattedMessage, injectIntl } from "react-intl";
import SvgIcon from "@material-ui/core/SvgIcon";
import clsx from "clsx";
import participantDropdownStyles from "./participant.style";
import CustomAvatar from "../../Avatar/Avatar";
import DropdownMenu from "../DropdownMenu";
import { generateActiveMembers } from "../../../helper/getActiveMembers";
import AssigneeIcon from "../../Icons/AssigneeIcon";
import CustomButton from "../../Buttons/CustomButton";
import SelectSearchDropdown from "../SelectSearchDropdown/SelectSearchDropdown";
import { generateAssigneeData } from "../../../helper/generateSelectData";
import DefaultCheckbox from "../../Form/Checkbox";

class AssigneeDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      assignedTo: [],
      assigneeList: [],
      searchTerm: "",
      addParticipant: false,
    };
  }

  componentDidMount() {
    const { assignedTo } = this.props;

    // Updating state with the total list of members whom user can assign the document
    this.setState({
      assigneeList: this.compareMembersList(assignedTo),
      assignedTo,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { members, assignedTo } = this.props;
    if (JSON.stringify(members) !== JSON.stringify(prevProps.members)) {
      this.setState({ assigneeList: this.compareMembersList(assignedTo) });
    }
    if (JSON.stringify(assignedTo) !== JSON.stringify(prevProps.assignedTo)) {
      this.setState({
        assignedTo,
        assigneeList: this.compareMembersList(assignedTo),
      });
    }
  }

  handleClick = () => {
    this.setState({ open: true });
  };

  handleRemoveAssignee = member => {
    // Function responsible for removing assignee from list
    const { assigneeList, assignedTo } = this.state;
    const { obj, updateAction, singleSelect } = this.props;
    const newAssigneeList = singleSelect ? [member] : [member, ...assigneeList];
    const newAssignedToList = assignedTo.filter(assignee => {
      return assignee.userId !== member.userId;
    });
    this.setState(
      {
        assignedTo: newAssignedToList,
        assigneeList: newAssigneeList,
      },
      () => {
        // Updates Assignee on respective document(task/issue/risk etc..)
        updateAction(newAssignedToList, obj);
      }
    );
  };

  compareMembersList = (assigneeList = []) => {
    const members = generateActiveMembers();
    // Filtering Members list by comparing it with members listed in assigned To section in dropdown
    return members.filter(member => {
      const assigneeListFilter =
        assigneeList.find(selectedMember => {
          return selectedMember.userId == member.userId;
        }) || [];

      return assigneeListFilter.userId !== member.userId;
    });
  };

  handleMemberSelect = (event, member) => {
    // This function handles selection of member
    const { assignedTo } = this.state;
    const { updateAction, obj, singleSelect, addPermission = true } = this.props;
    if (addPermission) {
      /** if user have permission to add assignee then this code works */
      const newAssignedToList = singleSelect ? [member] : [...assignedTo, member];
      this.setState({ assignedTo: newAssignedToList }); // Updating Assignee List based on user selection
      this.setState(
        {
          assigneeList: this.compareMembersList(newAssignedToList),
          open: false,
          searchTerm: "",
        },
        () => {
          // Updates Assignee on respective document(task/issue/risk etc..)
          updateAction(newAssignedToList, obj);
        }
      );
    }
  };

  handleClose = event => {
    if (document.getElementById("assigneeDropdown").contains(event.target)) {
      return;
    }

    this.setState({ open: false, searchTerm: "" });
  };

  // handle click on add participant button
  handleAddParticipantClick = () => {
    this.setState({ addParticipant: true });
  };

  render() {
    const { assignedTo = [], open, assigneeList, addParticipant } = this.state;
    const {
      avatarSize,
      classes,
      theme,
      isArchivedSelected,
      disabled = false,
      minOneSelection,
      obj,
      buttonVariant = "",
      deletePermission = true,
      addPermission = true,
    } = this.props;
    const { intl } = this.props;
    const filteredAssignees = [];
    return (
      <>
        <IconButton
          className={clsx({
            [classes.addAssigneeBtn]: true,
            [classes.addAssigneeBtnSelected]: open,
          })}
          // disabled={isArchivedSelected || disabled} // disable button if archive is selected
          buttonRef={node => {
            this.anchorEl = node;
          }}
          onClick={this.handleClick}>
          <SvgIcon viewBox="0 0 26.805 24" classes={{ root: classes.addAssigneeBtnIcon }}>
            <AssigneeIcon />
          </SvgIcon>
        </IconButton>

        <DropdownMenu
          open={open}
          anchorEl={this.anchorEl}
          placement="bottom-start"
          id="assigneeDropdown"
          style={{ width: addParticipant ? 380 : 200 }}
          size="medium"
          closeAction={this.handleClose}>
          <div>
            {" "}
            {addParticipant ? (
              <div className={classes.addParticipantCnt}>
                <SelectSearchDropdown
                  data={() => generateAssigneeData([])}
                  label={
                    <FormattedMessage id="common.assigned.label" defaultMessage="Participant(s)" />
                  }
                  // selectChange={this.handleFilterChange}
                  // selectedValue={assignees}
                  styles={{ marginBottom: 0 }}
                  placeholder={
                    <FormattedMessage
                      id="risk.creation-dialog.form.assign-to.placeholder"
                      defaultMessage="Select Assignee"
                    />
                  }
                  avatar
                />
                <DefaultCheckbox
                  checkboxStyles={{ padding: "5px 5px 6px 0" }}
                  styles={{ alignItems: "center" }}
                  // checked={}
                  // onChange={}
                  fontSize={20}
                  color="#0090ff"
                  label="Share all chat history"
                />
                <div className={classes.addPartActionBtnCnt}>
                  <CustomButton variant="contained" btnType="gray" style={{ marginRight: 10 }}>
                    Cancel
                  </CustomButton>
                  <CustomButton variant="contained" btnType="blue">
                    Add
                  </CustomButton>
                </div>
              </div>
            ) : (
              <Scrollbars
                autoHide
                autoHeightMax={250}
                autoHeight
                // style={{ minHeight: 100 }}
              >
                {/* {assignedTo.length > 0 ? ( */}
                <>
                  {/* Selection List */}
                  <List className={classes.list}>
                    {assignedTo
                      .filter(m => !m.isDeleted)
                      .map((member, i) => {
                        return (
                          <ListItem key={i} className={classes.menuItemavatar}>
                            <CustomAvatar
                              otherMember={{
                                imageUrl: member.imageUrl,
                                fullName: member.fullName,
                                lastName: "",
                                email: member.email,
                                isOnline: member.isOnline,
                                isOwner: member.isOwner,
                              }}
                              size="xsmall"
                            />
                            <ListItemText
                              primary={
                                <>
                                  {member.fullName}
                                  <br />
                                  {member.isActiveMember === null ? (
                                    <Typography variant="caption">Invite sent</Typography>
                                  ) : null}
                                </>
                              }
                              classes={{ primary: classes.itemText }}
                            />
                            {minOneSelection && assignedTo.length < 2 ? null : deletePermission ? (
                              <RemoveIcon
                                className={classes.removeAssigneeIcon}
                                htmlColor={theme.palette.secondary.light}
                                onClick={event => this.handleRemoveAssignee(member)}
                              />
                            ) : null}
                          </ListItem>
                        );
                      })}
                    <ListItem className={classes.menuItemavatar}>
                      <CustomAvatar
                        otherMember={{
                          imageUrl: "",
                          fullName: "James Nicklson",
                          lastName: "",
                          email: "",
                          isOnline: false,
                          isOwner: false,
                        }}
                        size="xsmall"
                      />
                      <ListItemText
                        primary="James Nicklson"
                        classes={{ primary: classes.itemText }}
                      />

                      <RemoveIcon
                        className={classes.removeAssigneeIcon}
                        htmlColor={theme.palette.secondary.light}
                        onClick={event => this.handleRemoveAssignee()}
                      />
                    </ListItem>
                    <ListItem className={classes.menuItemavatar}>
                      <CustomAvatar
                        otherMember={{
                          imageUrl: "",
                          fullName: "Elon Musk",
                          lastName: "",
                          email: "",
                          isOnline: false,
                          isOwner: false,
                        }}
                        size="xsmall"
                      />
                      <ListItemText primary="Elon Musk" classes={{ primary: classes.itemText }} />

                      <RemoveIcon
                        className={classes.removeAssigneeIcon}
                        htmlColor={theme.palette.secondary.light}
                        onClick={event => this.handleRemoveAssignee()}
                      />
                    </ListItem>
                  </List>
                </>
                {/* ) : null} */}
                {filteredAssignees.length > 0 ? (
                  <Typography variant="caption" className={classes.listItemHeading}>
                    <FormattedMessage id="common.search-assignee.label" defaultMessage="Members" />
                  </Typography>
                ) : null}
                <div className={classes.addParticipantBtnCnt}>
                  <CustomButton
                    variant="contained"
                    btnType="lightBlue"
                    style={{ fontFamily: theme.typography.fontFamilyLato, width: "100%" }}
                    onClick={this.handleAddParticipantClick}>
                    <SvgIcon
                      viewBox="0 0 26.805 24"
                      classes={{ root: classes.addPartActionBtnIcon }}>
                      <AssigneeIcon color={theme.palette.icon.brightBlue} />
                    </SvgIcon>
                    Add Participant
                  </CustomButton>
                </div>
              </Scrollbars>
            )}
          </div>
        </DropdownMenu>
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    members: state.profile.data.member.allMembers,
  };
};
export default compose(
  injectIntl,
  connect(mapStateToProps),
  withStyles(participantDropdownStyles, { withTheme: true })
)(AssigneeDropdown);
