import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../Buttons/CustomButton";
import { compose } from "redux";
import { connect } from "react-redux";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Close";
import SearchInput, { createFilter } from "react-search-input";
import { Scrollbars } from "react-custom-scrollbars";
import dropdownStyles from "./style";
import CustomAvatar from "../Avatar/Avatar";
import DropdownMenu from "./DropdownMenu";
import { UpdateFollowupAction } from "../../redux/actions/meetings";
import { generateActiveMembers } from "../../helper/getActiveMembers";
import Chip from "@material-ui/core/Chip";
import CrossIcon from "@material-ui/icons/Close";
import AssigneeIcon from "../Icons/AssigneeIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomIconButton from "../Buttons/CustomIconButton";
import { FormattedMessage, injectIntl } from "react-intl";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

class AssigneeDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      assignedTo: [],
      assigneeList: [],
      searchTerm: "",
    };
  }

  handleClick = (e) => {
    e.stopPropagation();
    this.setState({ open: true });
  };

  componentDidUpdate(prevProps, prevState) {

    const { members, assignedTo } = this.props;
    if (JSON.stringify(members) !== JSON.stringify(prevProps.members)) {
      this.setState({ assigneeList: this.compareMembersList(assignedTo) });
    }
    if (JSON.stringify(assignedTo) !== JSON.stringify(prevProps.assignedTo)) {
      this.setState({
        assignedTo,
        assigneeList: this.compareMembersList(assignedTo),
      });
    }
  }

  handleRemoveAssignee = member => {
    // Function responsible for removing assignee from list
    const { assigneeList, assignedTo } = this.state;
    const { obj, updateAction, singleSelect } = this.props;
    const newAssigneeList = singleSelect ? [member] : [member, ...assigneeList];
    const newAssignedToList = assignedTo.filter(assignee => {
      return assignee.userId !== member.userId;
    });
    this.setState(
      {
        assignedTo: newAssignedToList,
        assigneeList: newAssigneeList,
        open: false,
      },
      () => {
        // Updates Assignee on respective document(task/issue/risk etc..)
        updateAction(newAssignedToList, obj);
      }
    );
  };

  compareMembersList = (assigneeList = []) => {
    const members = generateActiveMembers();
    // Filtering Members list by comparing it with members listed in assigned To section in dropdown
    return members.filter(member => {
      const assigneeListFilter =
        assigneeList.find(selectedMember => {
          return selectedMember.userId == member.userId;
        }) || [];
      return assigneeListFilter.userId !== member.userId;
    });
  };

  handleMemberSelect = (event, member) => {
    // This function handles selection of member
    const { assignedTo } = this.state;
    const { updateAction, obj, singleSelect, addPermission = true, showSelected } = this.props;
    if (!showSelected) {
      updateAction(member);
      this.setState({ searchTerm: '', open: false })
      return
    }
    if (addPermission) {
      /** if user have permission to add assignee then this code works */
      const newAssignedToList = singleSelect ? [member] : [...assignedTo, member];
      this.setState({ assignedTo: newAssignedToList }); // Updating Assignee List based on user selection
      this.setState(
        {
          assigneeList: this.compareMembersList(newAssignedToList),
          open: false,
          searchTerm: "",
        },
        () => {
          // Updates Assignee on respective document(task/issue/risk etc..)
          updateAction(newAssignedToList, obj);
        }
      );
    }
  };

  handleClose = event => {
    const { handleCloseAssignee } = this.props
    if (document.getElementById("assigneeDropdown").contains(event.target)) {
      return;
    }
    this.setState({ open: false, searchTerm: "" });
    // BulkAction AssigneeDropdown Close function
    if (handleCloseAssignee) {
      handleCloseAssignee()
    }
  };

  componentDidMount() {
    const { assignedTo } = this.props;

    // Updating state with the total list of members whom user can assign the document
    this.setState({
      assigneeList: this.compareMembersList(assignedTo),
      assignedTo,
    });
    // this.anchorEl && this.anchorEl.addEventListener('click', (e) => {
    //   e.stopPropagation();
    //   this.setState({ open: true });
    // })
  }

  searchUpdated = term => {
    this.setState({ searchTerm: term });
  };

  render() {
    const { assignedTo = [], open, assigneeList, searchTerm } = this.state;
    const {
      classes,
      theme,
      disabled,
      singleSelect,
      assigneeHeading,
      minOneSelection,
      obj,
      buttonVariant,
      deletePermission,
      style,
      permission,
      intl,
      customBtnProps,
      assigneeIcon,
      label,
      customChipProps,
      isAllowDelete = true,
      isAllowAdd = true,
      customBtnRender,
    } = this.props;
    const filteredAssignees = assigneeList.filter(createFilter(searchTerm, ["fullName"]));
    // const isAllowDelete = permission ? permission.taskTabs.taskAssignee.isAllowDelete : true;
    // const isAllowAdd = permission ? permission.taskTabs.taskAssignee.isAllowAdd : true;

    return (
      <>
        {customBtnRender ? customBtnRender : <CustomButton
          btnType="white"
          variant="contained"
          style={{
            display: "block",
            width: "100%",
            textAlign: "left",
            textTransform: "none",
            ...style,
          }}
          iconBtn={true}
          onClick={!assignedTo.length ? this.handleClick : null}
          buttonRef={(node) => {
            if (assignedTo.length == 0) this.anchorEl = node;
          }}
          disabled={!isAllowAdd}
          {...customBtnProps}>
          {assigneeIcon && (
            <SvgIcon viewBox="0 0 26.805 24" classes={{ root: classes.assigneeIcon }}>
              <AssigneeIcon />
            </SvgIcon>
          )}
          {assignedTo.length > 0 ? (
            <ul className={classes.assigneeChipCnt} onClick={e => e.stopPropagation()}  >
              {assignedTo.map((assignee, i, t) => {
                return (
                  <li key={i} style={{ margin: "2px 6px 3px 0px" }}>
                    <Chip
                      avatar={
                        <CustomAvatar
                          otherMember={{
                            imageUrl: assignee.imageUrl,
                            fullName: assignee.fullName,
                            lastName: "",
                            email: assignee.email,
                            // isOnline: assignee.isOnline,
                            isOnline: false,
                            isOwner: assignee.isOwner,
                          }}
                          size={"small24"}
                        />
                      }
                      label={assignee.fullName}
                      onDelete={() => this.handleRemoveAssignee(assignee)}
                      className={classes.chipRoot}
                      deleteIcon={
                        isAllowDelete ? <CrossIcon className={classes.crossIcon} /> : null
                      }
                      classes={{ label: classes.chipLabel }}
                      {...customChipProps}
                    />
                  </li>
                );
              })}
            </ul>
          ) : (
            <span>
              <FormattedMessage id="common.bulk-action.addAssignee" defaultMessage="Add Assignee" />
            </span>
          )}
          {assignedTo.length > 0 && isAllowAdd && (
            <CustomIconButton
              onClick={this.handleClick}
              btnType="blueOutlined"
              variant="contained"
              buttonRef={node => {
                this.anchorEl = node;
              }}>
              <AddIcon style={{ fontSize: "20px" }} htmlColor={theme.palette.text.brightBlue} />
            </CustomIconButton>
          )}
        </CustomButton>}
        <DropdownMenu
          open={this.props.anchorEl ? Boolean(this.props.anchorEl) : open}
          anchorEl={this.props.anchorEl ? this.props.anchorEl : this.anchorEl}
          placement="bottom-start"
          id="assigneeDropdown"
          size="medium"
          closeAction={this.handleClose}>
          <div className={classes.dropdownSearchCnt}>
            <SearchInput
              className="HtmlInput"
              maxLength="80"
              onChange={this.searchUpdated}
              placeholder={intl.formatMessage({
                id: "common.search.label",
                defaultMessage: "Search",
              })}
            />
          </div>
          <Scrollbars autoHide autoHeightMax={250} autoHeight={false} style={{ height: 250 }}>
            {assignedTo.length > 0 && !searchTerm ? (
              <>
                <Typography variant="body2" className={classes.listItemHeading}>
                  {assigneeHeading}
                </Typography>

                {/* Selection List */}
                <List className={classes.list}>
                  {assignedTo
                    .filter(m => !m.isDeleted)
                    .map((member, i) => {
                      return (
                        <ListItem key={i} className={classes.menuItemavatar}>
                          <CustomAvatar
                            otherMember={{
                              imageUrl: member.imageUrl,
                              fullName: member.fullName,
                              lastName: "",
                              email: member.email,
                              isOnline: member.isOnline,
                              isOwner: member.isOwner,
                            }}
                            size="xsmall"
                          />
                          <ListItemText
                            primary={
                              <>
                                {member.fullName}
                                <br />
                                {member.isActiveMember === null ? (
                                  <Typography variant="caption">
                                    {" "}
                                    <FormattedMessage
                                      id="common.action.invite.label"
                                      defaultMessage="Invite sent"
                                    />
                                  </Typography>
                                ) : null}
                              </>
                            }
                            classes={{ root: classes.itemTextRoot, primary: classes.itemTextPrimary }}
                          />
                          {minOneSelection && assignedTo.length < 2 ? null : deletePermission ? (
                            <RemoveIcon
                              className={classes.removeAssigneeIcon}
                              htmlColor={theme.palette.secondary.light}
                              onClick={event => this.handleRemoveAssignee(member)}
                            />
                          ) : null}
                        </ListItem>
                      );
                    })}
                </List>
              </>
            ) : null}
            {filteredAssignees.length > 0 ? (
              <Typography variant="caption" className={classes.listItemHeading}>
                <FormattedMessage id="common.search-assignee.label" defaultMessage="Members" />
              </Typography>
            ) : null}
            {/* Other Options List */}
            <List className={classes.list}>
              {filteredAssignees
                .filter(m => !m.isDeleted)
                .map((member, i) => {
                  return (
                    <ListItem
                      key={i}
                      button
                      onClick={event => this.handleMemberSelect(event, member)}
                      className={classes.menuItemavatar}>
                      <CustomAvatar
                        otherMember={{
                          imageUrl: member.imageUrl,
                          fullName: member.fullName,
                          lastName: "",
                          email: member.email,
                          isOnline: member.isOnline,
                          // isOwner: member.isOwner,
                        }}
                        size="xsmall"
                      />
                      <ListItemText
                        classes={{ root: classes.itemTextRoot, primary: classes.itemTextPrimary }}
                        primary={
                          <>
                            {member.fullName}
                            <br />
                            {member.isActiveMember === null ? (
                              <Typography variant="caption">Invite sent</Typography>
                            ) : null}
                          </>
                        }
                      />
                    </ListItem>
                  );
                })}
            </List>
          </Scrollbars>
        </DropdownMenu>
      </>
    );
  }
}
AssigneeDropdown.defaultProps = {
  classes: {},
  theme: {},
  disabled: false,
  style: {},
  customBtnProps: {},
  customChipProps: {},
  singleSelect: false,
  assigneeHeading: "Assigned To",
  minOneSelection: false,
  obj: {},
  buttonVariant: "",
  deletePermission: true,
  permission: false,
  assigneeIcon: true,
  label: false,
  customBtnRender: false,
  handleCloseAssignee: () => { },
  showSelected: true
};
const mapStateToProps = (state, ownProps) => {
  return {
    members: state.profile.data.member.allMembers,
  };
};
export default compose(
  injectIntl,
  connect(mapStateToProps),
  withStyles(dropdownStyles, { withTheme: true })
)(AssigneeDropdown);
