export const useStyles = theme => ({
  buttonIcon: {
    color: theme.palette.secondary.medDark,
  },
  history: {
  fontSize: "12px !important",
  fontFamily: theme.typography.fontFamilyLato
  },
  commentInfo: {
    display: "flex",
    justifyContent: "space-between",
  },
  message: {
    backgroundColor: theme.palette.background.items,
    padding: "10px",
    borderRadius: "10px",
    marginTop: "10px",
    color: theme.palette.common.black,
  },
  dialogContentCnt: {
    padding: "1rem 1rem 1rem 2.5rem",
    maxHeight: "70vh",
    overflow: "auto",
    "& *": {
      fontFamily: theme.typography.fontFamilyLato,
    },
    "& div:last-child": {
      borderLeft: 0,
    },
  },
  comment: {
    paddingLeft: "2.5rem",
    paddingBottom: "1.5rem",
    backgroundSize: "2px 12px",
    backgroundImage: `linear-gradient(${theme.palette.border.extraLightBorder} 50%, rgba(255,255,255,0) 0%)`,
    backgroundRepeat: "repeat-y",
    backgroundPosition: "left",
    position: "relative",
    "&:last-child": {
      backgroundImage: "none",
      paddingBottom: 0,
    },
  },
  textBlack: {
    color: theme.palette.common.black,
  },
  withdrawnText: {
    color: theme.palette.common.black,
    marginLeft: ".25rem",
    fontWeight: "bolder",
  },
  approvedText: {
    color: "#19AC75",
    marginLeft: ".25rem",
    fontWeight: "bolder",
  },
  rejectedText: {
    color: "#C33D3D",
    marginLeft: ".25rem",
    fontWeight: "bolder",
  },
  commentIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    height: "2.5rem",
    width: "2.5rem",
    borderRadius: "50%",
    marginLeft: "-2.5rem",
    transform: "translateX(-50%)",
  },
  commentsCnt: {
    flex: 1,
    justifyContent: "space-between",
  },
});
