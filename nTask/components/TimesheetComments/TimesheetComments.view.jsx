import React, { useState } from "react";
import CustomDialog from "../../components/Dialog/CustomDialog";
import MessageIcon from "@material-ui/icons/Message";
import Typography from "@material-ui/core/Typography";
import find from "lodash/find";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";

import { FormattedMessage } from "react-intl";
import { useStyles } from "./TimesheetComments.style";
import { generateUsername } from "../../utils/common";
import { IconApproved } from "../../components/Icons/IconApproved";
import { IconWithdraw } from "../../components/Icons/IconWithdraw";
import { IconRejected } from "../../components/Icons/IconRejected";

import moment from "moment";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { useSelector } from "react-redux";
import CustomIconButton from "../Buttons/CustomIconButton";
import IconTimesheet from "../Icons/IconTimesheetComment";

const TimesheetComments = ({ classes, comments }) => {
  const [open, setOpen] = useState(false);
  const profileState = useSelector(state => state.profile);
  const { allMembers } = profileState.data.member || [];

  const iconEl = {
    Rejected: (
      <div className={classes.commentIcon} style={{ background: "#F6E2E2" }}>
        <IconRejected />
      </div>
    ),
    Withdrawn: (
      <div className={classes.commentIcon} style={{ background: "#E6E6E6" }}>
        <IconWithdraw />
      </div>
    ),
    Approved: (
      <div className={classes.commentIcon} style={{ background: "#D9F1E8" }}>
        <IconApproved />
      </div>
    ),
  };
  const actionEl = {
    Rejected: <Typography className={`${classes.rejectedText} ${classes.history}`} >Rejected</Typography>,
    Withdrawn: <Typography className={`${classes.withdrawnText} ${classes.history}`}>Withdrawn</Typography>,
    Approved: <Typography className={`${classes.approvedText} ${classes.history}`}>Approved</Typography>,
  };

  if (!comments?.length) return <></>;

  return (
    <>
      <CustomIconButton
        btnType="gray"
        style={{ marginLeft: 10 }}
        onClick={e => {
          e.stopPropagation();
          setOpen(true);
        }}>
        <IconTimesheet className={classes.buttonIcon} />
      </CustomIconButton>

      <CustomDialog
        scroll="paper"
        title={<FormattedMessage id="common.history.label" defaultMessage="History" />}
        dialogProps={{
          open: open,
          onClose: () => setOpen(false),
          onClick: e => e.stopPropagation(),
        }}>
        <div className={classes.dialogContentCnt}>
          {comments &&
            comments.map((obj, index) => {
              const user = find(allMembers, { userId: obj.userId });
              let name = '-'
              user ? name = generateUsername(user.email, user.userName, user.fullName) : name = 'User does not exist';
              const date = moment(obj.createdDate)
                .format("dddd, DD MMM, YYYY - hh:mma")
                .toString();
              return (
                <div key={index} className={classes.comment}>
                  {iconEl[obj.commentsStatus]}
                  <div className={classes.commentsCnt}>
                    <div className={classes.commentInfo}>
                      <div>
                        <Typography component="div" className={`${classes.textBlack} ${classes.history}`}>
                          <b>{date}</b>
                        </Typography>
                        <Typography
                          component="div"
                          className={`${classes.textBlack} ${classes.history}`}
                          style={{ display: "flex" }}>
                          Action: {actionEl[obj.commentsStatus]}
                        </Typography>
                      </div>
                      <div>
                        <Typography style={{color: "#7e7e7e"}} className={classes.history}>Action taken by:</Typography>
                        <Typography className={`${classes.textBlack} ${classes.history}`}>
                          <b>{name}</b>
                        </Typography>
                      </div>
                    </div>
                  </div>
                  {obj.reason && (
                    <div>
                      <Typography className={classes.message}>{obj.reason}</Typography>
                    </div>
                  )}
                </div>
              );
            })}
        </div>

        <ButtonActionsCnt
          successAction={() => setOpen(false)}
          successBtnText={"Done"}
          btnType="success"
          disabled={false}
          btnQuery={false}
        />
      </CustomDialog>
    </>
  );
};

export default compose(withStyles(useStyles, { withTheme: true }))(TimesheetComments);
