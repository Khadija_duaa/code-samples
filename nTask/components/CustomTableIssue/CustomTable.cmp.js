import React, { useContext, useEffect } from "react";
import styled from "styled-components";
import {
  useTable,
  useBlockLayout,
  useGroupBy,
  useExpanded,
  useAsyncDebounce,
  useGlobalFilter,
  useSortBy,
} from "react-table";
import { FixedSizeList } from "react-window";
import { Circle } from "rc-progress";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import AddIcon from "@material-ui/icons/Add";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import Avatar from "@material-ui/core/Avatar";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import clsx from "clsx";
import { priorityData, typeData, severity } from "../../helper/issueDropdownData";
import { injectIntl } from "react-intl";
import moment from "moment";
import { defaultTheme } from "../../assets/jss/theme";
import IssueOverviewContext from "../../Views/IssuesOverview/Context/issueOverview.context";
import {
  dispatchWidgetSetting,
  dispatchIssueGroupByAssignee,
  dispatchIssue,
  dispatchIssueGroupByTask,
} from "../../Views/IssuesOverview/Context/actions";
import { updateWidgetSetting } from "../../redux/actions/overviews";
import CustomMultiSelectDropdown from "../Dropdown/CustomMultiSelectDropdown/Dropdown";
import generateIssueColumnsData from "./constants";
import customTableStyles from "./customTableStyles";
import CustomAvatar from "../Avatar/Avatar";
import ColumnActionDropDown from "./ColumnActionDropdown/ColumnActionDropDown";
import CancelIcon from "@material-ui/icons/Cancel";
import EmptyState from "../EmptyStates/EmptyState";
const Styles = styled.div`
  padding: 0 15px 15px 15px;

  .table {
    display: inline-block;
    border-spacing: 0;

    .tr {
      cursor: pointer;
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
    }
    .gtr {
      .td{
        background: #eaeaea
      }
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
    }
    .childRow{
      box-shadow: inset 10px 0px 0px 0px #eaeaea, inset -10px 1px 0px 0px #eaeaea;
    }
    .glr {
      box-shadow: inset 10px 0px 0px 0px #eaeaea;
      .td {
        box-shadow: 0px 10px 0px #eaeaea;
      }
    }

    .th,
    .td {
      margin: 0;
      padding: 4px 0;
      border-bottom: none;
      border-right: none;
      display: flex !important;
      align-items: center;
      justify-content: center;
      :last-child {
        border-right: none;
      }
    }
    .th.taskTitle {
      justify-content: flex-start;
    }
    .th {
      font-size: 12px;
      padding: 0;
      margin-top: 10px;
      :first-child {
        display: flex;
        justify-content: flex-start;
      }
      .columnHeader {
        display: inline-block;
        transition: ease all 0.2s;
        padding: 3px 6px 3px 0;
        cursor: default;
        height: 22px;
      
      }
      :hover{
        .columnHeader {
        transition: ease all 0.2s;
        background: #EAEAEA;
        border-radius: 4px 0 0 4px;
        padding-left: 7px;
        border-right: 2px solid #BFBFBF;
        
      }
      button{
        display: inline-block;
      }
      }
    }
    .td {
      font-size: 12px;
      box-shadow: 0px 1px 0px #eaeaea;
      display: flex !important;
      align-items: center;
      justify-content: center;
      :first-child {
        font-size: 13px;
        justify-content: space-between;
        cursor: pointer
      }
    }
    .td.taskTitle {
      font-size: 13px;
      justify-content: flex-start;
    }
    .priorityIcon {
      width: 28px;
      height: 28px;
      border: 1px solid #dddddd;
      border-radius: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
      background: #fff;
    }
    .addIcon {
      color: white;
    }
    .arrowIcon{
      color: #7E7E7E
    },
   
  }
  .taskGroupCountTag{
    padding: 4px;
    color: white;
    background: #0090ff;
    font-size: 11px;
    margin-left: 8px;
    display: inline-block;
    border-radius: 4px;
  }
  .groupByColumnsCnt{
    border-bottom: 1px solid #dddddd;
    padding: 8px 0 8px 0;
    display: flex;
    align-items: center;
  }
  .groupByColumnChipLabel{
    font-size: 13px;
    color: #000;
    display: inline-block;
    margin-right: 8px;
  }
  .groupByColumnChip{
    font-size: 13px;
    color: #000;
    padding: 3px 8px;
    display: flex;
    background: #EAEAEA;
    border-radius: 4px;
    display: flex;
    align-items: center;
    margin-right: 10px;
    svg{
      font-size: 18px;
      color: #969696;
      cursor: pointer;
      margin-left: 8px
    }
  }
  .empty {
    padding: 0 30px 0 60px;
      flex: 1;
      display: flex;
  }
`;
// Define a default UI for filtering
function GlobalFilter({ preGlobalFilteredRows, globalFilter, setGlobalFilter }) {
  const count = preGlobalFilteredRows.length;

  const [value, setValue] = React.useState(globalFilter);
  const onChange = useAsyncDebounce(v => {
    setGlobalFilter(v || undefined);
  }, 200);

  return (
    <span
      style={{
        position: "absolute",
        right: 19,
        top: -43,
        fontFamily: "'lato', sans-serif",
        fontSize: "13px !important",
      }}>
      Search:{" "}
      <input
        value={value || ""}
        onChange={e => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
        placeholder={`${count} records...`}
        style={{
          border: "1px solid rgba(221, 221, 221, 1)",
          borderRadius: 4,
          fontFamily: "'lato', sans-serif",
          fontSize: "13px !important",
          padding: 8,
          width: 250,
        }}
      />
    </span>
  );
}
function Table({ columns, data }) {
  // Use the state and functions returned from useTable to build your UI
  const {
    state: { widgetSettings, issueWidgetData },
    dispatch,
  } = useContext(IssueOverviewContext);
  const defaultColumn = React.useMemo(
    () => ({
      width: 150,
    }),
    []
  );
  // Style for Table Cells
  const getCellStyles = (props, col) => {
    return [
      props,
      {
        style: {
          // margin: 0,
          width: col.width,
          //  flex: 1,
          minWidth: col.minWidth,
          border: "none",
        },
      },
    ];
  };
  // Style for Table Header
  const getHeaderStyles = (props, col) => {
    return [
      props,
      {
        style: {
          width: col.width,
          // height: 30,

          // flex: 1,
          // border: 'none',
          minWidth: col.minWidth,
        },
      },
    ];
  };
  const getTableStyles = props => [
    props,
    {
      style: {
        width: "100%",
        borderSpacing: 0,
      },
    },
  ];
  const headerProps = (props, rowInfo) => getHeaderStyles(props, rowInfo.column);

  const cellProps = (props, rowInfo) => getCellStyles(props, rowInfo.cell.column);
  const tableProps = props => getTableStyles(props);

  const scrollbarWidth = () => {
    // thanks too https://davidwalsh.name/detect-scrollbar-width
    const scrollDiv = document.createElement("div");
    scrollDiv.setAttribute(
      "style",
      "width: 100px; height: 100px; overflow: scroll; position:absolute; top:-9999px;"
    );
    document.body.appendChild(scrollDiv);
    const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    document.body.removeChild(scrollDiv);
    return scrollbarWidth;
  };

  const handleRowClick = rowData => {
    const { permissions, tasks, projects } = issueWidgetData;
    if (rowData) {
      let issue = { ...rowData };

      let issuePer = permissions.find(
        p => p.workSpaceId === issue.teamId
      ); /** workspace issue permission */
      if (issue.linkedTasks && issue.linkedTasks.length > 0) {
        let attachedTask = tasks.find(t => t.taskId == issue.linkedTasks[0]);
        if (attachedTask) {
          let attachedProject = projects.find(p => p.projectId == attachedTask.projectId);
          if (attachedProject && attachedProject.projectPermission) {
            issue.issuePermission =
              attachedProject.projectPermission.permission.issue; /** project attached issue permission */
          } else {
            issue.issuePermission = issuePer.issue;
          }
        } else {
          issue.issuePermission = issuePer.issue;
        }
      } else issue.issuePermission = issuePer.issue;

      dispatchIssue(dispatch, issue);
    }
  };

  const scrollBarSize = React.useMemo(() => scrollbarWidth(), []);
  // Define a default UI for filtering

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    totalColumnsWidth,
    preGlobalFilteredRows,
    state,
    prepareRow,
    setGlobalFilter,
    toggleGroupBy,
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      initialState: { groupBy: widgetSettings.groupBy },
    },
    useBlockLayout,
    useGroupBy,
    useGlobalFilter,
    useSortBy,
    useExpanded
  );

  const RenderRow = React.useCallback(
    ({ index, style }) => {
      const row = rows[index];
      const canExpanded = rows[index].canExpand;
      const { isGrouped } = row;
      const isExp = row.isExpanded;
      prepareRow(row);
      return (
        <div
          {...row.getRowProps({
            style,
          })}
          className={clsx({
            gtr: isGrouped && isExp,
            childRow: state.groupBy.length && !canExpanded,
            tr: !canExpanded,
          })}>
          {row.cells.map(cell => {
            return (
              <div
                {...cell.getCellProps(cellProps)}
                className={`td ${cell.column.id}`}
                onClick={() => handleRowClick(cell.row.original)}
                // style={{ justifyContent: "space-between", width: "10%" }}
              >
                {/* {cell.render("Cell")} */}
                {cell.isGrouped ? (
                  // If it's a grouped cell, add an expander and row count
                  <>
                    <span
                      {...row.getToggleRowExpandedProps()}
                      style={{ display: "flex", alignItems: "center" }}>
                      {row.isExpanded ? (
                        <ArrowDropDownIcon className="arrowIcon" />
                      ) : (
                        <ArrowRightIcon className="arrowIcon" />
                      )}
                      {cell.render("Cell")}
                    </span>
                    <span className="taskGroupCountTag">{row.leafRows.length} issues</span>
                  </>
                ) : cell.isAggregated ? null : cell.isPlaceholder ? null : ( // cell.render("Aggregated") // renderer for cell // If the cell is aggregated, use the Aggregated // For cells with repeated values, render null
                  // Otherwise, just render the regular cell
                  cell.render("Cell")
                )}
              </div>
            );
          })}
        </div>
      );
    },
    [prepareRow, rows]
  );
  // Render the UI for your table
  return (
    <>
      <GlobalFilter
        preGlobalFilteredRows={preGlobalFilteredRows}
        globalFilter={state.globalFilter}
        setGlobalFilter={setGlobalFilter}
        manualGlobalFilter
      />
      {state.groupBy.length ? (
        <div className="groupByColumnsCnt">
          <label className="groupByColumnChipLabel">Group by:</label>
          {state.groupBy.map(g => {
            const groupByColumn = columns.find(c => c.accessor == g);
            return groupByColumn ? (
              <span className="groupByColumnChip">
                <span>{groupByColumn.Header}</span>
                <CancelIcon
                  onClick={() => {
                    const updatedGroupBy = widgetSettings.groupBy.filter(
                      g => g !== groupByColumn.accessor
                    );
                    if (groupByColumn.accessor == "assignee") {
                      dispatchIssueGroupByAssignee(dispatch, []);
                    }
                    if (groupByColumn.accessor == "task") {
                      dispatchIssueGroupByTask(dispatch, []);
                    }
                    dispatchWidgetSetting(dispatch, { groupBy: updatedGroupBy });
                    updateWidgetSetting(
                      "Issue",
                      { ...widgetSettings, groupBy: updatedGroupBy },
                      // Success
                      res => {}
                    );
                    toggleGroupBy(groupByColumn.accessor);
                  }}
                />
              </span>
            ) : null;
          })}
        </div>
      ) : null}
      <div {...getTableProps(tableProps)} className="table">
        <div>
          {headerGroups.map(headerGroup => (
            <div {...headerGroup.getHeaderGroupProps()} className="tr">
              {headerGroup.headers.map(column => (
                <div {...column.getHeaderProps(headerProps)} className={`th ${column.id}`}>
                  {/* {column.canGroupBy ? (
                    // If the column can be grouped, let's add a toggle
                    <span {...column.getGroupByToggleProps()}>
                      {column.isGrouped ? '🛑 ' : '👊 '}
                    </span>
                  ) : null} */}
                  {column.id !== "columnOptions" &&
                  column.id !== "title" &&
                  column.id !== "issueId" &&
                  column.id !== "createdDate" &&
                  column.id !== "updatedDate" &&
                  column.id !== "startDate" &&
                  column.id !== "dueDate" &&
                  column.id !== "actualDueDate" &&
                  column.id !== "actualStartDate" ? (
                    <span className="columnHeader">
                      {column.render("Header")}

                      {!state.groupBy.includes(column.id) && (
                        <ColumnActionDropDown column={column} data={data} />
                      )}
                    </span>
                  ) : (
                    column.render("Header")
                  )}
                </div>
              ))}
            </div>
          ))}
        </div>

        <div {...getTableBodyProps()} style={{ width: "100%" }}>
          {data.length && data.length > 0 ? (
            <FixedSizeList
              height={window.innerHeight - 200}
              itemCount={rows.length}
              itemSize={50}
              width={totalColumnsWidth}>
              {RenderRow}
            </FixedSizeList>
          ) : (
            <div
              style={{
                height: window.innerHeight - 200,
              }}>
              <div className="empty" style={{ height: "100%" }}>
                <EmptyState
                  screenType="newWorkspace"
                  heading="Please select workspace."
                  button={false}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
}

function CustomTable({ data, profileState, classes, isLoading, intl }) {
  const {
    state: { widgetSettings, issueWidgetData },
    dispatch,
  } = useContext(IssueOverviewContext);
  const { tasks, projects } = issueWidgetData;
  // save selected columns
  const onSelectedColumns = options => {
    const colsArr = options.map(opt => {
      return opt.value;
    }); // set all columns
    dispatchWidgetSetting(dispatch, { selectedColumns: colsArr });
    updateWidgetSetting(
      "issue",
      { ...widgetSettings, selectedColumns: colsArr },
      // Success
      res => {}
    );
  };

  const GenerateList = ({ members }) => {
    const membersList = profileState.teamMember.filter(m => members.includes(m.userId));
    return membersList.slice(0, 3).map((member, i, t) => {
      return (
        // flex_center_center_row class is applied when there is only 1 user to style the user full name
        <li
          key={i}
          style={{ zIndex: i, marginLeft: !i == 0 ? -5 : null }}
          className={t.length === 1 ? "flex_center_center_row" : null}>
          <CustomAvatar
            otherMember={{
              imageUrl: member.imageUrl,
              fullName: member.fullName,
              lastName: "",
              email: member.email,
              isOnline: member.isOnline,
              isOwner: member.isOwner,
            }}
            size="xsmall"
          />
        </li>
      );
    });
  };
  const formatDate = date => {
    if (date) return moment(date).format("MMM DD, YY");
    return "-";
  };

  // filtering columns on the basis of items selected by the user form drop down show filter and taskTitle by default, if thers columns then do filter otherwise no need
  const selectedColumns = generateIssueColumnsData.filter(c => {
    return widgetSettings.selectedColumns.includes(c.value);
  });
  const columnWidth = `${100 / widgetSettings.selectedColumns.length}%`;
  const headerColumns = [
    {
      Header: "Issue Id",
      accessor: "uniqueId",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{row.values.uniqueId}</span>;
      },
    },
    {
      Header: "Issue Title",
      accessor: "title",
      isGrouped: true,
      width: widgetSettings.selectedColumns.length ? columnWidth : "100%",
      minWidth: 200,
      Cell: ({ row }) => {
        return (
          <div style={{ wordBreak: "break-all", padding: "3px 5px 3px 5px" }}>
            {row.values.title}
          </div>
        );
      },
    },
    {
      Header: "Workspace",
      accessor: "workSpaceName",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{row.values ? row.values.workSpaceName : "-"}</span>;
      },
    },
    {
      Header: "Status",
      accessor: "status",
      width: columnWidth,
      minWidth: 170,
      Cell: ({ row }) => {
        const selectedColor = row.values && row.values.status === "Open" ? "#ec765f" : "#4ac174";
        return (
          <div
            style={{
              background: selectedColor,
              borderRadius: 4,
              width: 80,
              fontSize: "12px !important",
              display: "inline-block",
              textAlign: "center",
              padding: "4px 5px",
              color: "white",
            }}>
            {row.values && row.values.status}
          </div>
        );
      },
    },
    {
      Header: "Priority",
      accessor: "priority",
      width: columnWidth,
      Cell: ({ row }) => {
        const selectedPriority = priorityData(defaultTheme, classes, intl).find(
          p => row.values.priority === p.value
        );
        return selectedPriority ? (
          <div className="priorityIcon" title={selectedPriority.label}>
            {selectedPriority.icon}
          </div>
        ) : (
          <> </>
        );
      },
    },
    {
      Header: "Planned Start Date",
      accessor: "startDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.values && formatDate(row.values.startDate)) || "-"}</span>;
      },
    },
    {
      Header: "Planned End Date",
      accessor: "dueDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.values && formatDate(row.values.dueDate)) || "-"}</span>;
      },
    },
    {
      Header: "Actual Start Date",
      accessor: "actualStartDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.values && formatDate(row.values.actualStartDate)) || "-"}</span>;
      },
    },
    {
      Header: "Actual End Date",
      accessor: "actualDueDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.values && formatDate(row.values.actualDueDate)) || "-"}</span>;
      },
    },
    {
      Header: "Severity",
      accessor: "severity",
      width: columnWidth,
      Cell: ({ row }) => {
        const selectedSeverity = severity(defaultTheme, classes, intl).find(
          p => row.values.severity === p.value
        );
        return selectedSeverity ? (
          <div style={{ display: "flex" }}>
            {selectedSeverity.icon} {selectedSeverity.label}
          </div>
        ) : (
          <>-</>
        );
      },
    },
    {
      Header: "Type",
      accessor: "type",
      width: columnWidth,
      Cell: ({ row }) => {
        const selectedType =
          typeData(defaultTheme, classes, intl).find(p => row.values.type === p.value) || false;
        if (row.isGrouped && !selectedType) return <>Others</>;
        return selectedType ? (
          <div style={{ display: "flex" }}>
            {selectedType.icon} {selectedType.label}
          </div>
        ) : (
          <>-</>
        );
      },
    },
    {
      Header: "Updated Date",
      accessor: "updatedDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.values && formatDate(row.values.updatedDate)) || "-"}</span>;
      },
    },
    {
      Header: "Created By",
      accessor: "createdBy",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{row.values ? row.values.createdBy : ""}</span>;
      },
    },
    {
      Header: "Created Date",
      accessor: "createdDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.values && formatDate(row.values.createdDate)) || "-"}</span>;
      },
    },
    {
      Header: "Task",
      accessor: "task",
      width: columnWidth,
      Cell: ({ row }) => {
        if (row.isGrouped) {
          return <>{row.values.task}</>;
        }
        let selectedTasks = row.original
          ? row.original.linkedTasks.reduce((result, cv) => {
              const isExist = tasks.find(t => t.taskId === cv);
              if (isExist) result.push(isExist);
              return result;
            }, [])
          : [];
        return (
          <span>
            {selectedTasks.length >= 1 ? selectedTasks.map(st => `${st.taskTitle} `) : "-"}
          </span>
        );
      },
    },
    {
      Header: "Project",
      accessor: "projectId",
      width: columnWidth,
      Cell: ({ row }) => {
        if (row.isGrouped) {
          let value = "Not Assigned Issues";
          let selectedProject = row.groupByVal
            ? projects.find(item => item.projectId === row.groupByVal)
            : null;
          if(selectedProject) value = selectedProject.projectName;
          return <>{value}</>;
        }
        let selectedProject = row.values.projectId ? projects.find(item => item.projectId === row.values.projectId) : null;
        return (
          <span>
            {selectedProject ? selectedProject.projectName : "-"}
          </span>
        );
      },
    },
    {
      Header: "Assignee",
      accessor: "assignee",
      minWidth: 170,
      Cell: ({ row }) => {
        if (row.isGrouped) {
          return <>{row.values.assignee}</>;
        }
        const assigneeList = row.original ? row.original.assignee : [];
        return assigneeList ? (
          <ul className="AssigneeAvatarList">
            <GenerateList members={assigneeList} />
            {assigneeList.length > 3 ? (
              <li>
                <Avatar classes={{ root: classes.TotalAssignee }}>
                  +{assigneeList.length - 3}
                </Avatar>
              </li>
            ) : (
              ""
            )}
          </ul>
        ) : (
          <>-</>
        );
      },
    },
    {
      Header: (
        <CustomMultiSelectDropdown
          label=""
          options={() => generateIssueColumnsData}
          option={selectedColumns}
          open={false}
          iconButton
          icon={<AddIcon style={{ color: defaultTheme.palette.common.white, fontSize: "14px !important" }} />}
          onSelect={onSelectedColumns}
          maxSelections={10}
          heading={`Customize Column (${selectedColumns.length}/10)`}
          height="140px"
          width="140px"
          scrollHeight={180}
          buttonProps={{
            btnType: "DarkGray",
            style: { padding: 2 },
          }}
        />
      ),
      accessor: "columnOptions",
      width: 50,
      Cell: ({ row }) => {
        return <></>;
      },
    },
  ];
  const filteredColumns = headerColumns.filter(c => {
    return (
      widgetSettings.selectedColumns.includes(c.accessor) ||
      c.accessor === "title" ||
      c.accessor === "columnOptions"
    );
  });

  return isLoading ? (
    <div
      style={{
        height: window.innerHeight - 325,
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <div className="loader" />
    </div>
  ) : (
    <div style={{ position: "relative" }}>
      <Styles>
        <Table columns={filteredColumns} data={data} />
      </Styles>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
  };
};

export default compose(
  withStyles(customTableStyles, { withTheme: true }),
  injectIntl,
  connect(mapStateToProps)
)(CustomTable);
