const generateIssueColumnsData = [
  {
    label: "Issue Id",
    value: "uniqueId",
  },
  {
    label: "Workspace",
    value: "workSpaceName",
  },
  {
    label: "Status",
    value: "status",
  },
  {
    label: "Priority",
    value: "priority",
  },
  {
    label: "Planned Start Date",
    value: "startDate",
  },
  {
    label: "Planned End Date",
    value: "dueDate",
  },
  {
    label: "Actual Start Date",
    value: "actualStartDate",
  },
  {
    label: "Actual End Date",
    value: "actualDueDate",
  },
  {
    label: "Severity",
    value: "severity",
  },
  {
    label: "Type",
    value: "type",
  },
  {
    label: "Task",
    value: "task",
  },
  {
    label: "Project",
    value: "projectId",
  },
  {
    label: "Updated Date",
    value: "updatedDate",
  },
  {
    label: "Assignees",
    value: "assignee",
  },
  {
    label: "Creation Date",
    value: "createdDate",
  },
  {
    label: "Created By",
    value: "createdBy",
  },
];

export default generateIssueColumnsData;
