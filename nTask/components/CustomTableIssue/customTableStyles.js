const customTableStyles = theme => ({
  TotalAssignee: {
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main,
    width: 30,
    height: 30,
  },
  priorityIcon: {
    fontSize: "16px !important",
    marginRight: -4,
  },
  severityIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  bugIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  ratingStarIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  ImprovmentIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
});

export default customTableStyles;
