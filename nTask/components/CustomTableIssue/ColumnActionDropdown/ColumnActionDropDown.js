// @flow

import React, { useState, useEffect, useContext } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import MoreHorizontalIcon from "@material-ui/icons/MoreHoriz";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import CustomIconButton from "../../Buttons/IconButton";
import DropdownMenu from "../../Dropdown/DropdownMenu";
import dropdownStyles from "./ColumnActionDropDown.style";
import FilterListIcon from "@material-ui/icons/FilterList";
import IssueOverviewContext from "../../../Views/IssuesOverview/Context/issueOverview.context";
import {
  dispatchWidgetSetting,
  dispatchIssueGroupByAssignee,
  dispatchIssueGroupByTask
} from "../../../Views/IssuesOverview/Context/actions";
import { updateWidgetSetting } from "../../../redux/actions/overviews";
// MoreActionDropDown Main Component
function ColumnActionDropDown(props) {
  const { classes, theme, handleOptionSelect, style, column, data } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const {
    state: { widgetSettings },
    dispatch,
  } = useContext(IssueOverviewContext);
  const saveGroupBy = () => {
    if (widgetSettings.groupBy && widgetSettings.groupBy.includes(column.id)) {
      const filteredGroupByArr = widgetSettings.groupBy.filter(f => f !== column.id);
      dispatchWidgetSetting(dispatch, { groupBy: filteredGroupByArr });
      updateWidgetSetting(
        "Issue",
        { ...widgetSettings, groupBy: filteredGroupByArr },
        // Success
        res => {}
      );
    } else {
      dispatchWidgetSetting(dispatch, { groupBy: [...widgetSettings.groupBy, column.id] });
      updateWidgetSetting(
        "Issue",
        { ...widgetSettings, groupBy: [...widgetSettings.groupBy, column.id] },
        // Success
        res => {}
      );
    }
  };
  const handleClick = event => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const handleItemClick = (event, option) => {
    // Fuction responsible for selecting item from the list
    handleOptionSelect(option); // Callback on item select used to lift up option to parent if needed

    if (option == "color") {
      /** if option is color then drop down will be open and color control will open */
    } else {
      setAnchorEl(null);
    }
  };

  const open = Boolean(anchorEl);
  const columnGroupProps = column.getGroupByToggleProps();
  const { onClick: onGroupingToggle, ...restColumnGroupingProps } = columnGroupProps;
  const columnSortProps = column.getSortByToggleProps();
  const { onClick: onSortingToggle, ...restColumnSortProps } = columnSortProps;

  return (
    <>
      <CustomIconButton
        btnType="transparent"
        style={{
          padding: "0 7px",
          height: 15,
          width: 15,
          marginLeft: 6,
          padding: 0,
          overflow: "hidden",
          background: theme.palette.background.grayMedium,
          ...style,
        }}
        onClick={handleClick}
        buttonRef={anchorEl}>
        <FilterListIcon className={classes.filterIcon} />
      </CustomIconButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size="small"
        placement="bottom-start">
        {/* <Scrollbars autoHide autoHeight autoHeightMin={0} autoHeightMax={130}> */}

        <List>
          {/* <ListItem
            button
            className={classes.listItem}
            onClick={event => {
              column.toggleSortBy(true, true, true)
              handleClose()
              // handleItemClick(event, "pinToTop");
            }}
          >
            Sort by Ascending (A-Z)
          </ListItem> */}
          {/* <ListItem
            button
            className={classes.listItem}
            onClick={event => {
              column.toggleSortBy(true, true, true)
              handleClose()
            }}
          >
            Sort by Descending (Z-A)
          </ListItem> */}
          <ListItem
            button
            className={classes.listItem}
            onClick={e => {
              if (column.Header == "Assignee") {
                dispatchIssueGroupByAssignee(dispatch, data);
              }
              if (column.Header == "Task") {
                dispatchIssueGroupByTask(dispatch, data);
              }
              onGroupingToggle(e);
              handleClose();
              saveGroupBy();
            }}
            {...restColumnGroupingProps}>
            Group By
          </ListItem>
        </List>
        {/* </Scrollbars> */}
      </DropdownMenu>
    </>
  );
}
ColumnActionDropDown.defaultProps = {
  classes: {},
  theme: {},
  handleOptionSelect: () => {},
  style: {},
  column: {},
};
export default compose(
  withRouter,
  withStyles(dropdownStyles, { withTheme: true })
)(ColumnActionDropDown);
