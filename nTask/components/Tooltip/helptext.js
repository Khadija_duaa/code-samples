import React from "react";
//Onboarding!
export const onboardingHelpText = {
  usernameHelpText:
    "No special characters, i.e. ~`!#$%^&*()++{}{}|:\"'<>?,/, and no spaces allowed.",
  passwordHelpText:
    "Create a strong password for your account. Should be at minimum 8 characters long. ",
  companyNameHelpText:
    "Type in the name of your company. This will help us tailor your experience.",
  workspaceNameHelpText:
    "Your workspace will be the hub of all your projects. Give your workspace a name (e.g Marketing, DevOps etc).",
  workspaceUrlHelpText:
    "Create a unique URL for your workspace. No special characters allowed.",
  noActiveWorkspaceHelpText:
    "Oops! This workspace has been deleted and is no longer active.", //not implimented yet.
  howWillYouUseThisWorkspaceHelpText:
    "Help us understand the way you’ll utilize this workspace. This will help us tailor your experience."
};

//Profile Settings!
export const profileSettingsHelpText = {
  localizationHelpText:
    "Set up your language, first day of the week and time zone to manage your local settings instantly.",
  personalizationHelpText:
    "Configure the way you want to view your workspace by setting up your preferences.",
  notificationPreferencesHelpText:
    "Choose the way you want to be notified. Turn off the notifications you don’t want to receive.",
  twoFactorAuthenticationHelpText:
    "Add an extra layer of security to your account by setting up an Authy account or by adding an email address.",
  changePasswordHelpText:
    "Change the password of your current workspace to ensure maximum protection.",
  changeEmailHelpText:
    "Change your account email using this option.",
  dlsHelpText:
    "Change your account email using this option."
};

//Create New Workspace!
export const createNewWorkspaceHelpText = {
  workspaceNameHelpText: "Give your workspace a name. It’s a mandatory field.",
  workspaceUrlHelpText:
    "Create a URL of your workspace for quick access. Only alphanumeric characters and hyphen are allowed.",
  addWorkspaceMembersHelpText:
    "Add team members to send out an invite to your workspace."
};

//Create New Project!
export const createNewProjectHelpText = {
  projectTitleHelpText: "Give your project a name. It’s a mandatory field.",
  ganttViewHelpText:
    "View your project schedule in the form of a Gantt chart. Ideal for project planning. ",
  boardViewHelpText:
    "View your project and corresponding tasks in the form of a board showing respective statuses in the form of columns."
};

//Create New Task!
export const createNewTaskHelpText = {
  taskTitleHelpText: "Give your task a name. It’s a mandatory field.",
  AssignedToHelpText:
    "Add existing or new team members to the task. Adding a new task assignee automatically sends an invite to the workspace.",
  addToProjectHelpText:
    "Choose the project you want to associate a task with by typing in the name of the project."
};

export const createNewMileStonesHelpText = {
  milestonesHelpText: "Add your milestone name. It’s a mandatory field."
};

//Create New Meeting!
export const createNewMeetingHelpText = {
  meetingTitleHelpText: "Give your meeting a name. It’s a mandatory field.",
  meetingAgendaHelpText:
    "Set key agenda points for your meeting to share with your team members.",
  meetingParticipantsHelpText:
    "Specify meeting participants by typing in their name or email addresses.",
  meetingDateHelpText: "Set your meeting date."
};

//Create New Issue!
export const createNewIssueHelpText = {
  issueTitleHelpText: "Give your issue a name. It’s a mandatory field.",
  issueAssignedToHelpText:
    "Add team members to your issue. Adding new issue assignee automatically sends an invite to the workspace.",
  issuePriorityHelpText:
    "Select a priority level and define urgency of the issue. ",
  issueDueDateHelpText:
    "Set a due date to your issue and define a deadline for your team members.",
  selectIssueTaskOrProjectHelpText:
    "Associate tasks or projects with your issue by typing in the name of the respective task or project."
};

//Create New Risk!
export const createNewRiskHelpText = {
  riskTitleHelpText: "Give your risk a name. It’s a mandatory field.",
  selectRiskOwnerHelpText:
    "Appoint a risk owner to your risks. Risk owner is responsible for taking necessary steps to mitigate the risk.",
  riskLikelihoodHelpText:
    "Classify your risk on the basis of its likelihood of occurrence.",
  riskImpactHelpText: "Identify the level of impact a risk is likely to have.",
  selectRiskTaskOrProjectHelpText:
    "Associate a relevant task or project with your risk if required."
};

//Task Details!
export const taskDetailsHelpText = {
  selectProjectHelpText:
    "Select the project you want this task to be associated with. ",
  startDateHelpText:
    "Set a start date to your tasks by defining the planned and actual dates, along with the start time.", //not implimented yet.
  endDateHelpText:
    "Set an end date to your tasks by defining the planned and actual dates, along with the end time. ", //not implimented yet.
  descriptionHelpText:
    "Provide a description for the task. Be as detailed as you wish to help team members complete it.",
  addCategoryHelpText:
    "Create categories or choose from existing ones. You can use the categories to filter tasks in various areas within your project or site.", //not implimented yet.
  startTimerHelpText: "", //'Click' + <PlayIcon className={classes.playIcon} /> + 'to start the automatic timer and record the time you spend on your tasks ' //not implimented yet.
  manualTimeEntryHelpText:
    "Manually enter the time elapsed to accurately report in timesheets.",
  addAssigneeHelpText:
    "Add existing or new team members to the task. Adding a new task assignee automatically sends an invite to the workspace.", //not implimented yet.
  slashCommandsHelpText:
    "Slash commands lets you refer to an existing task, meeting, issue and risk. ", //not implimented yet.
  mentionsHelpText:
    "Click @ and mention specific team members to timely notify them." //not implimented yet.
};

//Issue Details
export const issueDetailsHelpText = {
  startDateHelpText: "Set a start date and time to your issue.", //not implimented yet.
  endDateHelpText: "Set an end date and time to your issue.", //not implimented yet.
  issueSeverityHelpText:
    "Select a severity level for your issue to identify the level of impact it's likely to have.",
  issuePriorityHelpText:
    "Choose a priority to identify the level of urgency required to resolve an issue.",
  issueTypeHelpText:
    "Identify the issue type to correctly decide the next course of action.",
  addCategoryHelpText:
    "Associate specific issue categories to understand the nature of an issue." //not implimented yet.
};

//Risk Details
export const riskDetailsHelpText = {
  riskMatrixHelpText:
    "Quickly make relationships between the probability of occurrence and the consequent impact a risk can have on your projects.",
  mitigationPlanHelpText:
    "List down all the probable solutions for your risks which will aid you in timely mitigating their impact.",
  addCategoryHelpText:
    "Associate specific risk categories to understand the true nature of your risk." //not implimented yet.
};

//Project Settings
export const projectSettingsHelpText = {
  tasksHelpText: "Choose billable and non-billable tasks for this project.",
  resourcesHelpText:
    "Add or invite team members to your project by simply typing in their name or entering their email address.",
  projectManagerHelpText:
    "Appoint a project manager and assign the rights of timesheet approvals or rejection. If workspace owner/admins are unchecked, they will stop receiving timesheet approval emails. However, this does not affect their right to approve/reject.",
  currencyHelpText:
    "Choose your currency for setting budget and cost calculations."
};
//Project Gantt
export const projectGanttHelpText = {
  projectCost: (
    <>
      Total Project Cost is calculated as follows: <br />
      Approved Timesheet Hours x Project's Billing Method
    </>
  )
};
//Timesheets
export const timesheetsHelpText = {
  otherTasksHelpText: "These tasks are not linked to any project yet."
};
