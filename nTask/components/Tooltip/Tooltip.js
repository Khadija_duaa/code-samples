import React, { Component, Children } from "react";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import tooltipStyles from "./style";
import HelpIcon from "@material-ui/icons/HelpOutline";
import Warning from "@material-ui/icons/Warning";
import Info from "@material-ui/icons/Info";

class CustomTooltip extends Component {
  constructor(props) {
    super(props);
    this.state = { arrowRef: null };
  }
  handleArrowRef = node => {
    this.setState({
      arrowRef: node,
    });
  };
  render() {
    const {
      classes,
      theme,
      helptext,
      iconType,
      position,
      style,
      size = "",
      hideArrow=false,
      children,
      ...other
    } = this.props;
    return helptext ? (
      <Tooltip
        enterDelay={300}
        style={{ maxWidth: 300 }}
        title={
          <React.Fragment>
            {helptext}
            {!hideArrow && <span className={classes.arrow} ref={this.handleArrowRef} />}
          </React.Fragment>
        }
        classes={{
          tooltip: iconType == "info" ? classes.infoBootstrapTooltip : classes.bootstrapTooltip,
          popper: classes[`bootstrapPopper${size}`],
          tooltipPlacementLeft: classes.bootstrapPlacementLeft,
          tooltipPlacementRight: classes.bootstrapPlacementRight,
          tooltipPlacementTop: classes.bootstrapPlacementTop,
          tooltipPlacementBottom: classes.bootstrapPlacementBottom,
        }}
        PopperProps={{
          popperOptions: {
            modifiers: {
              arrow: {
                enabled: Boolean(this.state.arrowRef),
                element: this.state.arrowRef,
              },
            },
          },
        }}
        {...other}>
        {children ? (
          children
        ) : iconType == "help" ? (
          <HelpIcon
            className={position == "static" ? classes.helpIcon : classes.helpIconAbsolute}
            htmlColor={theme.palette.secondary.light}
            style={style}
          />
        ) : iconType == "info" ? (
          <Info
            className={position == "static" ? classes.helpIcon : classes.helpIconAbsolute}
            htmlColor={theme.palette.secondary.light}
            style={style}
          />
        ) : (
          <Warning
            className={position == "static" ? classes.helpIcon : classes.helpIconAbsolute}
            htmlColor={theme.palette.secondary.light}
            style={style}
          />
        )}
      </Tooltip>
    ) : (
      children
    );
  }
}

export default withStyles(tooltipStyles, { withTheme: true })(CustomTooltip);
