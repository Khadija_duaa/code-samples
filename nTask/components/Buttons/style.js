const customBtnStyles = theme => ({
  root: {
    fontFamily: "lato, sans-serif !important",
    fontSize: 12,
    textTransform: "capitalize",
  },
  containedsuccess: {
    background: theme.palette.status.success,
    color: theme.palette.common.white,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "8px 20px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.primary.dark,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
    "& $circularProgress": {
      color: `${theme.palette.status.success} !important`,
    },
  },
  containeddanger: {
    background: theme.palette.background.danger,
    color: theme.palette.common.white,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "8px 20px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.background.danger,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
    "& $circularProgress": {
      color: `${theme.palette.background.white} !important`,
    },
  },
  disableddangercontained: {
    background: `${theme.palette.background.light} !important`,
    color: "rgba(0, 0, 0, 0.26)!important",
  },
  //Status contained button
  containedstatus: {
    fontSize: 11,
    fontWeight: theme.typography.fontWeightExtraLight,
    padding: "2px 10px",
    minHeight: 20,
    color: theme.palette.common.white,
    textTransform: "capitalize",
    borderRadius: 4,
    minWidth: 95,
    letterSpacing: "1px",
    boxShadow: "none !important",
    "&:focus": {
      boxShadow: "none !important",
    },
    "& $circularProgress": {
      color: `${theme.palette.common.white} !important`,
    },
  },
  //   simple gray button
  containedgray: {
    background: theme.palette.background.paper,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    color: theme.palette.text.primary,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "7px 14px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.background.items,
    }, 
    "&:focus": {
      boxShadow: "none !important",
    },
    "& $circularProgress": {
      color: `${theme.palette.background.blue} !important`,
    },
  },
  disabledgraycontained: {
    background: `${theme.palette.background.paper} !important`,
  },
  //   simple gray button
  containedwhite: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    color: theme.palette.text.medGray,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: 12,
    padding: "7px 14px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    "&:hover": {
      boxShadow: 'none',
      // border: `1px solid ${theme.palette.border.lightBlueBorder}`,
      background: theme.palette.common.white,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
    "&:active": {
      boxShadow: "none !important",
    },
    "& $circularProgress": {
      color: `${theme.palette.background.blue} !important`,
    },
  },
  disabledwhitecontained: {
    background: `${theme.palette.background.paper} !important`,
  },
  containedblue: {
    background: theme.palette.background.blue,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    color: theme.palette.common.white,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "7px 14px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.background.darkblue,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
    "& $circularProgress": {
      color: `${theme.palette.background.blue} !important`,
    },
  },
  containedyellow: {
    background: theme.palette.background.yellow,
    color: theme.palette.common.yellowDarken,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "14px !important",
    padding: "7px 14px",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    textTransform: "none",
    width: "100%",
    "&:hover": {
      background: theme.palette.background.yellow,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
    "& $circularProgress": {
      color: `${theme.palette.background.yellow} !important`,
    },
  },
  containedgreen: {
    background: theme.palette.background.lightGreen2,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    color: theme.palette.common.white,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "13px !important",
    padding: "7px 14px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    fontFamily: theme.typography.fontFamilyLato,
    "&:hover": {
      background: theme.palette.background.lightGreen2,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
    "& $circularProgress": {
      color: `${theme.palette.background.lightGreen2} !important`,
    },
  },
  disabledbluecontained: {
    background: `${theme.palette.background.items} !important`,
    color: "rgba(0, 0, 0, 0.26)!important",
  },
  containedlightBlue: {
    background: theme.palette.background.lightBlue,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    color: theme.palette.text.azure,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "7px 14px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.background.lightBlue,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
    "& $circularProgress": {
      color: `${theme.palette.background.lightBlue} !important`,
    },
  },
  disabledlightBluecontained: {
    background: `${theme.palette.background.light} !important`,
    color: "rgba(0, 0, 0, 0.26)!important",
  },
  //--------------------Icon Button----------------------//
  iconButtonsuccess: {
    background: theme.palette.status.success,
    padding: 4,
    borderRadius: "50%",
    "&:hover": {
      background: theme.palette.primary.dark,
    },
  },
  iconButtongreen: {
    background: theme.palette.background.darkGreen,
    padding: 4,
    borderRadius: "50%",
    "&:hover": {
      background: theme.palette.background.darkGreen,
    },
  },
  iconButtonDarkGray: {
    background: theme.palette.text.light,
    padding: 4,
    borderRadius: "50%",
    "&:hover": {
      background: theme.palette.text.darkGray,
    },
  },
  iconButtonblue: {
    background: theme.palette.background.blue,
    padding: 4,
    borderRadius: "50%",
    "&:hover": {
      background: theme.palette.background.blue,
    },
  },
  iconButtontransparent: {
    background: "transparent",
    padding: 3,
    borderRadius: "20%",
    "&:hover": {
      background: theme.palette.primary.items,
    },
  },
  disabledIconButtonsuccess: {
    background: `${theme.palette.background.items} !important`,
  },
  iconButtondanger: {
    background: theme.palette.background.danger,
    padding: 4,
    borderRadius: "50%",
    "&:hover": {
      background: theme.palette.background.danger,
    },
  },
  iconButtonsucessSubmit: {
    background: theme.palette.status.success,
    padding: 8,
    borderRadius: "50%",
    "&:hover": {
      background: theme.palette.primary.dark,
    },
  },
  iconButtongray: {
    background: theme.palette.background.items,
    padding: 8,
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  iconButtontransparent: {
    background: "transparent",
    padding: 8,
    border: "none",
    "&:hover": {
      background: "transparent",
    },
  },
  iconButtonwhite: {
    background: theme.palette.common.white,
    borderRadius: 4,
    padding: 8,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  iconButtonwhiteround: {
    background: theme.palette.common.white,
    borderRadius: "50%",
    padding: 2,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    "&:hover": {
      background: theme.palette.common.white,
    },
  },
  iconButtonblueOutlined: {
    background: theme.palette.common.white,
    borderRadius: "50%",
    padding: 2,
    border: `1px solid ${theme.palette.border.brightBlue}`,
    "&:hover": {
      background: theme.palette.background.brightBlue,
      "& svg": {
        color: theme.palette.common.white,
      },
    },
  },
  outlinedsuccess: {
    border: `1px solid ${theme.palette.border.successBorder}`,
    background: theme.palette.common.white,
    color: theme.palette.text.sucess,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "5px 12px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
  },
  outlinedgray: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    background: theme.palette.common.white,
    color: theme.palette.text.primary,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "5px 12px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
    "& $circularProgress": {
      color: `${theme.palette.background.blue} !important`,
    },
  },
  outlineddanger: {
    border: `1px solid ${theme.palette.border.redBorder}`,
    background: theme.palette.common.white,
    color: theme.palette.text.sucess,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "5px 12px",
    fontWeight: theme.typography.fontWeightExtraLight,
    color: theme.palette.text.danger,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
  },
  outlinedblue: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.blue}`,
    color: theme.palette.text.azure,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "13px !important",
    padding: "7px 14px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.background.blue,
      color: theme.palette.common.white,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
  },
  disabledblueoutlined: {
    background: `${theme.palette.background.paper} !important`,
  },
  containeddangerText: {
    background: theme.palette.common.white,
    color: theme.palette.text.danger,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "5px 12px",
    fontWeight: theme.typography.fontWeightExtraLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
  },

  //Status outlined button
  outlinedstatus: {
    fontSize: "10px !important",
    fontWeight: theme.typography.fontWeightLight,
    background: "transparent",
    padding: "0 10px",
    minHeight: 20,
    color: theme.palette.common.white,
    textTransform: "uppercase",
    borderRadius: 4,
    minWidth: 80,
    letterSpacing: "1px",
    boxShadow: "none !important",
    "&:focus": {
      boxShadow: "none !important",
    },
    "&:hover": {
      boxShadow: "none !important",
      background: "transparent",
    },
  },
  //---------------------Dashed Button -------------------------//
  containeddashedIcon: {
    background: theme.palette.common.white,
    color: theme.palette.text.primary,
    border: `1px dashed ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "7px 11px 7px 7px",
    textTransform: "capitalize",
    borderRadius: 4,
    fontWeight: theme.typography.fontWeightExtraLight,
    "&:hover": {
      background: theme.palette.background.items,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
  },
  containedgraydashed: {
    background: theme.palette.common.white,
    color: theme.palette.text.primary,
    border: `1px dashed ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    boxShadow: "none !important",
    fontSize: "12px !important",
    padding: "7px 14px",
    textTransform: "capitalize",
    fontWeight: theme.typography.fontWeightExtraLight,
    "&:hover": {
      background: theme.palette.background.items,
    },
    "&:focus": {
      boxShadow: "none !important",
    },
  },
  textplain: {
    color: theme.palette.text.secondary,
    padding: 0,
    fontSize: "12px !important",
    lineHeight: "normal",
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "none",
    "&:hover": {
      background: "none",
    },
  },
  textplainbackground: {
    color: theme.palette.text.secondary,
    padding: 0,
    fontSize: "12px !important",
    lineHeight: "1.75",
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "none",
    "&:hover": {
      background: theme.palette.background.items,
    },
  },
  disabledplaintext: {
    color: `${theme.palette.text.medGray} !important`,
    cursor: "pointer",
  },
  textsuccess: {
    color: theme.palette.text.green,
    padding: 0,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    "&:hover": {
      background: "none",
    },
  },
  //Styles for label component
  labelleft: {
    justifyContent: "flex-start",
  },
  outlinedplain: {
    textTransform: "capitalize",
  },
  iconCnt: {
    display: "flex",
    lineHeight: 2,
    alignItems: "center",
  },
  circularProgress: {
    width: "20px !important",
    height: "20px !important",
    marginRight: 5,
    position: "absolute",
    color: "white",
  },
});

export default customBtnStyles;
