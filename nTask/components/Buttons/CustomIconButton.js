import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import CircularProgress from "@material-ui/core/CircularProgress";
import customBtnStyles from "./style";

function CustomIconButton(props) {
  const { classes, theme, btnType, query, children, ...rest } = props;
  const btnClasses = {
    root: classes[`iconButton${btnType}`],
    disabled: classes[`disabledIconButton${btnType}`],
  };
  return (
    <>
      <IconButton {...rest} classes={btnClasses}>
        {query == "progress" ? (
          <>
            <CircularProgress
              style={{ width: 20, height: 20, marginRight: 5, position: "absolute" }}
            />
            <span style={{ visibility: "hidden" }}>{children}</span>
          </>
        ) : (
          children
        )}
      </IconButton>
    </>
  );
}

export default withStyles(customBtnStyles, { withTheme: true })(CustomIconButton);
