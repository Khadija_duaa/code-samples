import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import customBtnStyles from "./style";

function CustomButton(props) {
  const {
    classes,
    theme,
    btnType,
    children,
    query,
    labelAlign,
    customClasses,
    iconBtn,
    isBoardScreenAppeared,
    ...rest
  } = props;
  const btnClasses = {
    root: isBoardScreenAppeared ? classes.root : classes.root,
    contained: classes[`contained${btnType}`],
    text: classes[`text${btnType}`],
    outlined: classes[`outlined${btnType}`],
    disabled: classes[`disabled${btnType}${props.variant}`],
    label: classes[`label${labelAlign}`],
    ...customClasses,
  };
  return (
    <Button {...rest} disableRipple classes={btnClasses}>
      {query == "progress" ? (
        <>
          <CircularProgress
            classes={{ root: classes.circularProgress }}
          />
          <span style={{ visibility: "hidden" }}>{children}</span>
        </>
      ) : (
        <>{iconBtn ? <div className={classes.iconCnt}>{children}</div> : children}</>
      )}
    </Button>
  );
}

export default withStyles(customBtnStyles, { withTheme: true })(CustomButton);
