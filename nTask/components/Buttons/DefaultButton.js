import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import buttonStyles from "../../assets/jss/components/button";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import CircularProgress from '@material-ui/core/CircularProgress';


class DefaultButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { classes, theme, buttonType, children, disabled, query, disableRipple } = this.props;
    const buttonClass =
      buttonType == "Icon"
        ? classes.IconBtn
        : buttonType == "Transparent"
          ? classes.TransparentBtn
          : buttonType == "Plain"
            ? classes.PlainBtn
            : buttonType == "DropdownButton"
              ? classes.DropdownButton
              : buttonType == "smallIconBtn"
                ? classes.smallIconBtn
                : buttonType == "grayBtn"
                  ? classes.grayBtn
                  : buttonType == "dangerBtn"
                    ? classes.dangerBtn
                    : buttonType == "Publish"
                    ? classes.PublishBtn :
                    buttonType == "leftIconBtnTrans"
                      ? classes.leftIconBtnTrans
                      : null;
    return (
      <React.Fragment>


        <Button
          onClick={this.props.onClick}
          variant={buttonType == "Transparent" ? undefined : "contained"}
          buttonRef={this.props.buttonRef}
          disableRipple={true}
          disabled={disabled || false}
          style={this.props.style}
          classes={{ root: buttonClass }}
          disabled={this.props.disabled}
          disableRipple={disableRipple}
        >
          {buttonType == "smallIconBtn" ? children : null}
          {buttonType == "Icon" ? (
            <Fragment>
              <AddCircleIcon
                htmlColor={theme.palette.common.white}
                classes={{ root: classes.BtnIcon }}
              />
              {this.props.text}
            </Fragment>
          ) :
            buttonType == "smallIconBtn" ? <u>{this.props.text}</u> : query == "progress" ? <><CircularProgress style={{ width: 20, height: 20, marginRight: 5, position: "absolute" }} /><span style={{visibility: "hidden"}}>{this.props.text}</span></> : this.props.text

          }
          {buttonType == "DropdownButton" ? (
            <Fragment>
              <DropdownArrow
                htmlColor={theme.palette.secondary.light}
                classes={{ root: classes.RightBtnIcon }}
              />
            </Fragment>
          ) : null}
        </Button>

      </React.Fragment>
    );
  }
}

export default withStyles(buttonStyles, { withTheme: true })(DefaultButton);
