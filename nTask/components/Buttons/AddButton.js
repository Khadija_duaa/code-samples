import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import buttonStyles from "../../assets/jss/components/button";
import AddCircleIcon from "@material-ui/icons/AddCircle";

class AddIconButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { classes, theme } = this.props;
    return (
      <React.Fragment>
        <Button onClick={this.props.clickAction} variant="contained" buttonRef={this.props.btnRef} anchor={this.anchorEl} 
        classes={{ root: this.props.buttonType == "IconBtn" ? classes.addIconButton : classes.addButton }}>
          {this.props.buttonType == "IconBtn" ?
          <AddCircleIcon
            htmlColor={theme.palette.common.white}
            classes={{ root: classes.addBtnIcon }}
          />: null}
          {this.props.text}
        </Button>
      </React.Fragment>
    );
  }
}

export default withStyles(buttonStyles, { withTheme: true })(AddIconButton);
