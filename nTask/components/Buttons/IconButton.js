import React, { Component } from "react";
import IconButton from "@material-ui/core/IconButton";
import { withStyles } from "@material-ui/core/styles";
import buttonStyles from "../../assets/jss/components/button";

class CustomIconButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { classes, btnType, disabled=false, isBoardScreenAppeared  } = this.props;
    const fontFamily = isBoardScreenAppeared ? classes.root : {};
    const IconBtnClass =
      btnType === "transparent"
        ? classes.TransIconBtn
        : btnType === "smallFilledGray"
        ? classes.smallFilledGray
        : btnType == "grayBg"
        ? classes.grayBgIconBtn
        : btnType == "condensed"
        ? classes.PlainIconBtn
        : btnType == "filledWhite"
        ? classes.filledWhiteIconBtn
        : btnType == "filledWhiteRound"
        ? classes.filledWhiteRoundIconBtn
        : classes.filledLightIconBtn
    return (
      <IconButton
        id={this.props.id}
        classes={{ root: `${fontFamily} ${IconBtnClass}`}}
        disableRipple={true}
        style={this.props.style}
        buttonRef={this.props.buttonRef}
        onClick={this.props.onClick}
        disabled={disabled}
      >
        {this.props.children}
      </IconButton>
    );
  }
}

export default withStyles(buttonStyles)(CustomIconButton);
