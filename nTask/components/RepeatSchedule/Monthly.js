import React, { Component } from "react";
import TimeSelect from "../TimePicker/TimeSelect";
import { compose } from "redux";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import {
  generateWeekNoData,
  generateWeekDaysData,
  generateRepeatStopByData,
  generateMonthRepeatTypeData,
  generateMonthNoData,
  generateMonthDaysNoData,
  generateAssigneeData
} from "../../helper/generateSelectData";
import InputLabel from "@material-ui/core/InputLabel";
import repeatTaskStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import StaticDatePicker from "../DatePicker/StaticDatePicker";
import MultiSelectDropdown from "../Dropdown/MultiSelectDropdown/MultiSelectDropdown";
import DefaultTextField from "../Form/TextField";
import moment from "moment";
import { FormattedMessage, injectIntl } from "react-intl";

//Array of Days
const days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

const weekData = generateWeekNoData();
const weekDaysData = generateWeekDaysData();
const monthData = generateMonthNoData();
const dayData = generateMonthDaysNoData();
// const repeatStopData = this.generateRepeatStopByData();
// const monthRepeatTypeData = generateMonthRepeatTypeData();

let weeks = ["First", "Second", "Third", "Fourth", "Last"];

class Monthly extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: this.generateRepeatStopByData()[0],
      weekNo: weekData[0],
      weekDayNo: weekDaysData[0],
      monthNo: monthData[0],
      repeatDayInterval: 1,
      selectedDay: [days[0]],
      timePeriod: this.monthRepeatTypeData()[0],
      afterTaskCount: 5,
      repeatMonthInterval: 1,
      dayInitial: weekDaysData[0],
      hours: "12",
      minutes: "00",
      timeFormat: "AM",
      remindTo:[]
    };
  }
  monthRepeatTypeData() {
    let ddData = [
      {
        value: "Day",
        label: this.props.intl.formatMessage({
          id: "common.day.label",
          defaultMessage: "Day",
        }),
      },
      {
        value: "The",
        label: this.props.intl.formatMessage({
          id: "common.the.label",
          defaultMessage: "The",
        }),
      },
    ];
    return ddData;
  }

  generateRepeatStopByData() {
    let ddData = [
      {
        value: "Date",
        label: this.props.intl.formatMessage({
          id: "common.date.label",
          defaultMessage: "Date",
        }),
      },
      {
        value: "After",
        label: this.props.intl.formatMessage({
          id: "common.after.label",
          defaultMessage: "After",
        }),
      },
    ];
    return ddData;
  }

  componentDidMount() {
    const { task, profile } = this.props;
    const repeatDetails = task.repeatDetails
    // Checking the repeat Type of the task if it already exist on monthly than prefill the values of input fields accordingly
    if (task.repeatType == "Monthly") {
      const dayInitial = repeatDetails.repeatMonthly.dayInitial;
      const timePeriod = repeatDetails.repeatMonthly.timePeriod;
      const weekNo = weekData.find(w => {
        return w.value == weeks[repeatDetails.repeatMonthly.weekNo - 1];
      });
      const monthNo = repeatDetails.repeatMonthly.monthNo;
      const date = moment(repeatDetails.stopBy.date);
      const type = this.generateRepeatStopByData().find(r => {
        return r.value == repeatDetails.stopBy.type;
      });
      const dayNo = repeatDetails.repeatMonthly.dayNo;
      const afterTaskCount = repeatDetails.stopBy.value;
      const hours = repeatDetails.repeatAt.hours;
      const minutes = repeatDetails.repeatAt.minutes;
      const timeFormat = repeatDetails.repeatAt.timeFormat;

      const remindToArr =
        repeatDetails.remindTo && repeatDetails.remindTo.length
          ? profile.member.allMembers.filter(m => repeatDetails.remindTo.includes(m.userId))
          : [];

      this.setState({
        dayInitial: weekDaysData.filter(w => {
          return w.value == dayInitial;
        }),
        timePeriod:
          timePeriod == "The" ? this.monthRepeatTypeData()[1] : this.monthRepeatTypeData()[0],
        weekNo: weekNo,
        repeatMonthInterval: monthNo,
        repeatDayInterval: dayNo,
        date,
        afterTaskCount: afterTaskCount ? afterTaskCount : 5,
        type: type,
        hours,
        minutes,
        timeFormat,
        remindTo: generateAssigneeData(remindToArr),

      });
    }
  }

  updateTime = (hours, minutes, timeFormat) => {
    const timeObj = { hours: hours, minutes, timeFormat };
    this.setState({ hours, minutes, timeFormat });
    this.props.updateRepeatData("repeatAt", "root", timeObj);
  };
  clearSelect = (key, value) => {
    this.setState({ [key]: value });
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  handleOptionsSelect = (type, option, field) => {
    this.setState({ [type]: option }, () => {
      this.props.updateRepeatData(field, type, option.value);
    });
  };
  //Local state is update for week dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected
  handleWeekOptionsSelect = (option, field) => {
    this.setState({ weekNo: option }, () => {
      this.props.updateRepeatData("repeatMonthly", "weekNo", option.obj.indexOf(option.value) + 1);
    });
  };

  //Saving date to local so later on can be used to send to backend
  updateDate = date => {
    this.setState({
      date,
    });
    this.props.updateRepeatData("stopBy", "date", date.format("MM/DD/YYYY"));
  };
  //Function handles on change of number field of stopBy Task Count
  handleInputChange = (event, name) => {
    let value = event.target.value;
    switch (name) {
      case "repeatDayInterval":
        //Update value only if value is greater than 0 and less than 1000
        if (value > 0 && value < 32 && value.indexOf(".") == -1) {
          this.setState({ [name]: value });
          this.props.updateRepeatData("repeatMonthly", "dayNo", value);
        }
        break;
      case "repeatMonthInterval":
        //Update value only if value is greater than 0 and less than 1000
        if (value > 0 && value < 100 && value.indexOf(".") == -1) {
          this.setState({ [name]: value });
          this.props.updateRepeatData("repeatMonthly", "monthNo", value);
        }
        break;
      case "afterTaskCount":
        //Update value only if value is greater than 0 and less than 1000
        if (value > 0 && value < 1000 && value.indexOf(".") == -1) {
          this.setState({ [name]: value });
          this.props.updateRepeatData("stopBy", "value", value);
        }
        break;
      default:
        break;
    }
  };
  translateData = (data) => {
    return data.map((item) => {
       return  {...item,label: this.translate(item.label) };
    });
}; 
translateItem = (item) => {
  if(Array.isArray(item) && item.length > 0) {
    if(item[0].label) {
      item[0].label = this.translate(item[0].label);
      return item;
    }
  }
  if (item.label) {
  item.label = this.translate(item.label);
  return item;
  }
  return item;
};
translate = (label) => {
 let id = "";
 switch(label) {
 case "Friday":
   id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.friday";
   break;
 case "Monday":
   id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.monday";
   break;
 case "Saturday":
   id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.saturday";
   break;
 case "Sunday":
   id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.sunday";
   break;
 case "Thursday":
   id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.thursday";
   break;
 case "Tuesday":
   id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.tuesday";
   break;
 case "Wednesday":
   id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.wednesday";
   break;
 case "First":
  id = "common.the.list.first";
  break;
 case "Second":
  id = "common.the.list.second";
  break;
 case "Third":
  id = "common.the.list.third";
  break;
 case "Fourth":
  id = "common.the.list.fourth";
  break;
 case "Last":
  id = "common.the.list.last";
  break;  
 }
 return id == "" ? label : this.props.intl.formatMessage({id: id, defaultMessage: label});
}
handleRemindToChange = (type, option) => {
  this.setState({ [type]: option }, () => {
    let ids = option.map(o=> o.id);
    this.props.updateRepeatData("remindTo", "root", option.length ? ids : []);
  });
};
  render() {
    const {
      type,
      selectedDay,
      weekNo,
      timePeriod,
      monthNo,
      dayNo,
      afterTaskCount,
      repeatDayInterval,
      repeatMonthInterval,
      weekDayNo,
      dayInitial,
      date,
      hours,
      minutes,
      timeFormat,
      remindTo
    } = this.state;
    const { classes, theme, members } = this.props;
    return (
      <>
        <div className={classes.repeatMonthEveryCnt}>
          {/* Month Repeat Type Dropdown  */}
          <SelectSearchDropdown
            data={() => {
              return this.monthRepeatTypeData();
            }}
            isClearable={false}
            label=""
            selectChange={(type, option) => this.handleOptionsSelect(type, option, "repeatMonthly")}
            selectClear={this.handleClearSelect}
            styles={{
              display: "inline-block",
              width: 70,
              marginRight: 10,
              marginTop: 0,
              marginBottom: 0,
            }}
            type="timePeriod"
            selectedValue={timePeriod}
            placeholder={"Month Type"}
            isMulti={false}
          />
          {/* Month Day No Dropdown  */}
          {timePeriod.value == "Day" ? (
            <DefaultTextField
              label={false}
              fullWidth={true}
              error={false}
              errorState={false}
              formControlStyles={{ marginBottom: 0, width: 80 }}
              defaultProps={{
                id: "repeatDayInterval",
                type: "number",
                onChange: e => this.handleInputChange(e, "repeatDayInterval"),
                value: repeatDayInterval,
                inputProps: {
                  style: { paddingTop: 11, paddingBottom: 11 },
                },
              }}
            />
          ) : (
            <>
              {/* Week No Dropdown   */}
              <SelectSearchDropdown
                data={() => {
                  return this.translateData(weekData);
                }}
                isClearable={false}
                label=""
                selectChange={(type, option) =>
                  this.handleWeekOptionsSelect(option, "repeatMonthly")
                }
                selectClear={this.handleClearSelect}
                styles={{
                  display: "inline-block",
                  width: 85,
                  marginTop: 0,
                  marginBottom: 0,
                  display: "inline-block",
                }}
                type="weekNo"
                selectedValue={this.translateItem(weekNo)}
                placeholder={"Week"}
                isMulti={false}
              />
              <SelectSearchDropdown
                data={() => {
                  return this.translateData(weekDaysData);
                }}
                isClearable={false}
                label=""
                selectChange={(type, option) =>
                  this.handleOptionsSelect(type, option, "repeatMonthly")
                }
                selectClear={this.handleClearSelect}
                styles={{
                  display: "inline-block",
                  width: 110,
                  marginTop: 0,
                  marginBottom: 0,
                  marginLeft: 10,
                  display: "inline-block",
                }}
                type="dayInitial"
                selectedValue={this.translateItem(dayInitial)}
                placeholder={"Week"}
                isMulti={false}
              />{" "}
            </>
          )}
          <div
            className={classes.monthDropdownOutCnt}
            style={{ marginTop: timePeriod.value == "Week" ? 15 : "" }}>
            <div className={classes.monthDropdownCnt}>
              <InputLabel
                classes={{
                  root: classes.inputCenterLabel,
                }}
                shrink={false}>
                <FormattedMessage
                  id="task.detail-dialog.repeat-task.monthly.every.label"
                  defaultMessage="of every"></FormattedMessage>
              </InputLabel>
              <DefaultTextField
                label={false}
                fullWidth={true}
                error={false}
                errorState={false}
                formControlStyles={{ marginBottom: 0, width: 80 }}
                defaultProps={{
                  id: "repeatMonthInterval",
                  type: "number",
                  onChange: e => this.handleInputChange(e, "repeatMonthInterval"),
                  value: repeatMonthInterval,
                  inputProps: {
                    style: { paddingTop: 11, paddingBottom: 11 },
                  },
                }}
              />
              <InputLabel
                classes={{
                  root: classes.inputCenterLabel,
                }}
                shrink={false}>
                <FormattedMessage
                  id="task.detail-dialog.repeat-task.monthly.month.label"
                  defaultMessage="month(s)"></FormattedMessage>
              </InputLabel>
            </div>
          </div>
        </div>
        {/* Time Select Control */}
        <TimeSelect
          clearSelect={this.clearSelect}
          updateTime={this.updateTime}
          label={
            <FormattedMessage
              id="task.detail-dialog.repeat-task.common.repeat-at.label"
              defaultMessage="Repeat at:"></FormattedMessage>
          }
          hours={hours}
          mins={minutes}
          timeFormat={timeFormat}
        />
        {/* StopBy Container */}
        <div className={classes.stopByCnt}>
          <InputLabel
            classes={{
              root: classes.selectLabel,
            }}
            shrink={false}>
            <FormattedMessage
              id="task.detail-dialog.repeat-task.common.stop-by.label"
              defaultMessage="Stop by:"></FormattedMessage>
          </InputLabel>
          {/* Stop By Dropdown  */}
          <SelectSearchDropdown
            data={() => {
              return this.generateRepeatStopByData();
            }}
            isClearable={false}
            label=""
            selectChange={(type, option) => this.handleOptionsSelect(type, option, "stopBy")}
            selectClear={this.handleClearSelect}
            styles={{ flex: 1, marginRight: 10, marginTop: 0, marginBottom: 0 }}
            type="type"
            selectedValue={type}
            placeholder={"type"}
            isMulti={false}
          />
          {/* Date picker */}
          {type.value == "Date" ? (
            <StaticDatePicker
              label=""
              placeholder={this.props.intl.formatMessage({
                id: "common.date.placeholder",
                defaultMessage: "Select Date",
              })}
              isInput={true}
              selectedSaveDate={this.updateDate}
              isCreation={true}
              style={{ width: 170 }}
              formControlStyles={{ margin: 0 }}
              date={date}
              minDate={new Date()}
              margin={0}
            />
          ) : (
            <DefaultTextField
              label={false}
              fullWidth={true}
              error={false}
              errorState={false}
              formControlStyles={{ marginBottom: 0, width: 80 }}
              defaultProps={{
                id: "afterTaskCount",
                type: "number",
                onChange: e => this.handleInputChange(e, "afterTaskCount"),
                value: afterTaskCount,
              }}
            />
          )}
        </div>
        <div className={classes.remindToCnt}>
          <InputLabel
            classes={{
              root: classes.selectLabel,
            }}
            shrink={false}>
            Remind :
          </InputLabel>
          <SelectSearchDropdown
            data={() => generateAssigneeData(members)}
            label=""
            selectChange={this.handleRemindToChange}
            type="remindTo"
            selectedValue={remindTo}
            placeholder="Remind"
            avatar={true}
            styles={{ margin: 0 }}
          /></div>
      </>
    );
  }
}
Monthly.defaultProps = {
  task: {}
}
export default compose(injectIntl, withStyles(repeatTaskStyles, { withTheme: true }))(Monthly);
