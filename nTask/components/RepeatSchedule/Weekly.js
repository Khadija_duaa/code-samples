import React, { useState, useEffect } from "react";
import { compose } from "redux";
import TimeSelect from "../TimePicker/TimeSelect";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateWeekNoData, generateRepeatStopByData, generateAssigneeData, generateProjectAssigneeData } from "../../helper/generateSelectData";
import InputLabel from "@material-ui/core/InputLabel";
import repeatTaskStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import StaticDatePicker from "../DatePicker/StaticDatePicker";
import MultiSelectDropdown from "../Dropdown/MultiSelectDropdown/MultiSelectDropdown";
import DefaultTextField from "../Form/TextField";
import moment from "moment";
import { FormattedMessage, injectIntl } from "react-intl";
//Array of Days
const days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

const weekData = generateWeekNoData();

// const repeatStopData = generateRepeatStopByData();

function Weekly(props) {
  const generateRepeatStopByData = () => {
    let ddData = [
      {
        value: "Date",
        label: props.intl.formatMessage({
          id: "common.date.label",
          defaultMessage: "Date",
        }),
      },
      {
        value: "After",
        label: props.intl.formatMessage({
          id: "common.after.label",
          defaultMessage: "After",
        })
      },
    ];
    return ddData;
  };
  const [state, setState] = useState({
    type: generateRepeatStopByData()[0],
    weekNo: weekData[0],
    selectedDay: [days[0]],
    afterTaskCount: 5,
    weekInterval: 1,
    date: moment(),
    hours: "12",
    minutes: "00",
    timeFormat: "AM",
    remindTo:[]
  });

  const updateTime = (hours, minutes, timeFormat) => {
    const timeObj = { hours: hours, minutes, timeFormat };
    setState({ ...state, hours, minutes, timeFormat });
    props.updateRepeatData("repeatAt", "root", timeObj);
  };
  const clearSelect = (key, value) => {
    setState({ ...state, [key]: value });
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  const handleOptionsSelect = (type, option, field) => {
    setState({ ...state, [type]: option });
    props.updateRepeatData(field, type, option.value);
  };
  const handleRemindToChange = (type, option) => {
    setState({ ...state, [type]: option });
    let ids = option.map(o=> o.id);
    props.updateRepeatData("remindTo", "root", option.length ? ids : []);
  };
  //Function which updates week number in repeat week object
  const updateWeekNo = value => {
    props.updateRepeatData(
      "repeatWeekly",
      "weekNo",
      value.map(v => v.obj.number),
    );
  };
  //Saving date to local so later on can be used to send to backend
  const updateDate = date => {
    setState({
      ...state,
      date,
    });
    props.updateRepeatData("stopBy", "date", date.format("MM/DD/YYYY"));
  };
  //updating week day select in state
  const handleDaySelect = value => {
    const { selectedDay } = state;
    if (selectedDay.includes(value)) {
      // if the day is already selected than remove it
      const newArray = selectedDay.filter(d => d !== value);
      if (newArray.length >= 1) {
        setState({ ...state, selectedDay: newArray });
        props.updateRepeatData("repeatWeekly", "dayInitial", newArray);
      }
    } else {
      // if the day is not already selected than add it
      setState({ ...state, selectedDay: [...selectedDay, value] });
      props.updateRepeatData("repeatWeekly", "dayInitial", [...selectedDay, value]);
    }
  };
  //Function handles on change of number field of stopBy Task Count
  const handleInputChange = (event, name) => {
    let value = event.target.value;
    switch (name) {
      case "afterTaskCount":
        //Update value only if value is greater than 0 and less than 1000
        if (value > 0 && value < 1000 && value.indexOf(".") == -1) {
          setState({ ...state, [name]: value });
          props.updateRepeatData("stopBy", "value", value);
        }
        break;
      case "weekInterval":
        //Update value only if value is greater than 0 and less than 1000
        if (value > 0 && value < 100 && value.indexOf(".") == -1) {
          setState({ ...state, [name]: value });
          props.updateRepeatData("repeatWeekly", "interval", value);
        }
        break;
      default:
        break;
    }
  };
  useEffect(() => {
    const { task, profile } = props;
    // Checking the repeat Type of the task if it already exist on weekly than prefill the values of input fields accordingly
    if (task.repeatType == "Weekly") {
      const stopByValue = generateRepeatStopByData().find(s => {
        return s.value == task.repeatDetails.stopBy.type;
      });
      const afterTaskCount = task.repeatDetails.stopBy.value;
      const date = moment(task.repeatDetails.stopBy.date);
      const interval = task.repeatDetails.repeatWeekly.interval;
      const dayInitial = task.repeatDetails.repeatWeekly.dayInitial;
      const hours = task.repeatDetails.repeatAt.hours;
      const minutes = task.repeatDetails.repeatAt.minutes;
      const timeFormat = task.repeatDetails.repeatAt.timeFormat;
      const remindToArr =
      task.repeatDetails.remindTo && task.repeatDetails.remindTo.length
          ? profile.member.allMembers.filter(m => task.repeatDetails.remindTo.includes(m.userId))
          : []; 
      setState({
        ...state,
        type: stopByValue,
        afterTaskCount: afterTaskCount ? afterTaskCount : 5,
        weekInterval: interval,
        date,
        selectedDay: dayInitial,
        hours,
        minutes,
        timeFormat,
        remindTo: generateProjectAssigneeData(remindToArr),
      });
    }
  }, []);

  const {
    type,
    selectedDay,
    weekNo,
    afterTaskCount,
    weekInterval,
    date,
    hours,
    minutes,
    timeFormat,
    remindTo
  } = state;

  const { classes, theme, members } = props;
  return (
    <>
      <div className={classes.repeatEveryCnt}>
        <InputLabel
          classes={{
            root: classes.selectLabel,
          }}
          shrink={false}>
          <FormattedMessage
            id="task.detail-dialog.repeat-task.weekly.every.label"
            defaultMessage="Every:"></FormattedMessage>
        </InputLabel>
        <DefaultTextField
          label={false}
          fullWidth={true}
          error={false}
          errorState={false}
          formControlStyles={{ marginBottom: 0, width: 80 }}
          defaultProps={{
            id: "weekInterval",
            type: "number",
            onChange: e => handleInputChange(e, "weekInterval"),
            value: weekInterval,
            inputProps: {
              style: { paddingTop: 11, paddingBottom: 11 },
            },
          }}
        />
        {/* Stop By Dropdown  */}
        {/* <MultiSelectDropdown
            data={() => {
              return weekData;
            }}
            selectedValueInit="wk"
            defaultValue={[weekData[0]]}
            updateAction={updateWeekNo}
          /> */}
        {/* <SelectSearchDropdown
            data={() => {
              return weekData;
            }}
            isClearable={false}
            label=""
            selectChange={(type, option) => handleOptionsSelect(type, option, "repeatWeekly")}
            selectClear={handleClearSelect}
            styles={{ flex: 1, marginTop: 0, marginBottom: 0 }}
            type="weekNo"
            selectedValue={weekNo}
            placeholder={"Week"}
            isMulti={false}
          /> */}
        <InputLabel
          classes={{
            root: classes.inputCenterLabel,
          }}
          shrink={false}>
          <FormattedMessage
            id="task.detail-dialog.repeat-task.weekly.week.label"
            defaultMessage="week(s) on"></FormattedMessage>
        </InputLabel>
        {/* Day Select Control */}
        <ul className={classes.daysSelect}>
          {days.map((day, i) => {
            return (
              <li
                key={i}
                onClick={event => handleDaySelect(day)}
                style={{
                  background: selectedDay.includes(day) ? theme.palette.common.white : null,
                }}>
                {day[0]}
              </li>
            );
          })}
        </ul>
      </div>
      {/* Time Select Control */}
      <TimeSelect
        clearSelect={clearSelect}
        updateTime={updateTime}
        label={
          <FormattedMessage
            id="task.detail-dialog.repeat-task.common.repeat-at.label"
            defaultMessage="Repeat at:"></FormattedMessage>
        }
        hours={hours}
        mins={minutes}
        timeFormat={timeFormat}
      />
      {/* StopBy Container */}
      <div className={classes.stopByCnt}>
        <InputLabel
          classes={{
            root: classes.selectLabel,
          }}
          shrink={false}>
          <FormattedMessage
            id="task.detail-dialog.repeat-task.common.stop-by.label"
            defaultMessage="Stop by:"></FormattedMessage>
        </InputLabel>
        {/* Stop By Dropdown  */}
        <SelectSearchDropdown
          data={() => {
            return generateRepeatStopByData();
          }}
          isClearable={false}
          label=""
          selectChange={(type, option) => handleOptionsSelect(type, option, "stopBy")}
          styles={{ flex: 1, marginRight: 10, marginTop: 0, marginBottom: 0 }}
          type="type"
          selectedValue={type}
          placeholder={"type"}
          isMulti={false}
        />
        {/* Date picker */}
        {type.value == "Date" ? (
          <StaticDatePicker
            label=""
            placeholder={props.intl.formatMessage({
              id: "common.date.placeholder",
              defaultMessage: "Select Date",
            })}
            isInput={true}
            selectedSaveDate={updateDate}
            isCreation={true}
            style={{ width: 170 }}
            date={date}
            formControlStyles={{ margin: 0 }}
            minDate={new Date()}
            margin={0}
          />
        ) : (
          <DefaultTextField
            label={false}
            fullWidth={true}
            error={false}
            errorState={false}
            formControlStyles={{ marginBottom: 0, width: 80 }}
            defaultProps={{
              id: "afterTaskCount",
              type: "number",
              onChange: e => handleInputChange(e, "afterTaskCount"),
              value: afterTaskCount,
            }}
          />
        )}
      </div>
      <div className={classes.remindToCnt}>
          <InputLabel
            classes={{
              root: classes.selectLabel,
            }}
            shrink={false}>
            Remind :
          </InputLabel>
          <SelectSearchDropdown
            data={() => generateProjectAssigneeData(members)}
            label=""
            selectChange={handleRemindToChange}
            type="remindTo"
            selectedValue={remindTo}
            placeholder="Remind"
            avatar={true}
            styles={{ margin: 0 }}
          /></div>
    </>
  );
}

Weekly.defaultProps = {
  task: {}
}
export default compose(withStyles(repeatTaskStyles, { withTheme: true }), injectIntl)(Weekly);
