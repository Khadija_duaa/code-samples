import React, { Component, useState, useEffect } from "react";
import CustomDialog from "../Dialog/CustomDialog";
import repeatTaskStyles from "./styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Typography from "@material-ui/core/Typography";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import Daily from "./Daily";
import Weekly from "./Weekly";
import Monthly from "./Monthly";
import DefaultCheckbox from "../Form/Checkbox";
import { repeatTaskInitObj } from "./repeatTaskObj";
import cloneDeep from "lodash/cloneDeep";
import { repeatTask, deleteTaskSchedule } from "../../redux/actions/tasks";
import NotificationMessage from "../NotificationMessages/NotificationMessages";
import moment from "moment";
import getInfoMessage from "./getInfoMessage";
import { withSnackbar } from "notistack";
import { FormattedMessage } from "react-intl";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import InputLabel from "@material-ui/core/InputLabel";
import useFields from "../CustomFieldsViews/useFields.hook";

function RepeatScheduleCmp(props) {
  const { customFields, groupType, customFieldData,  classes, task, label, members , profile } = props;
  const currentCustomField =
    customFields && customFields.data.find(c => c.fieldType === "reminder");
  const currentCustomFieldData =
    customFieldData && customFieldData.find(c => c.fieldType === "reminder");
  // const { fields } = useFields("reminder", groupType, customFields,groupType);
  const [state, setState] = useState({
    repeatTypeTab: "left",
    weekendTaskTab: "left",
    weekEndTaskCheck: false,
    daily: repeatTaskInitObj.daily,
    weekly: repeatTaskInitObj.weekly,
    monthly: repeatTaskInitObj.monthly,
  });

  useEffect(() => {
    const { task } = props;
    const { daily, weekly, monthly } = state;
    const cfData = currentCustomFieldData && currentCustomFieldData.fieldData.data;

    if (cfData) {
      //Pre filling form values, if repeat Task already exist, especially selected tabs
      const repeatTaskType = cfData.repeatType;
      
      const repeatTypeTab =
        repeatTaskType == "Daily" ? "left" : repeatTaskType == "Weekly" ? "center" : "right";
      const weekendOptionSelected =
        repeatTaskType == "Daily"
          ? cfData.repeatDetails.repeatDaily.weekendTask
          : repeatTaskType == "Monthly"
          ? cfData.repeatDetails.repeatMonthly.weekendTask
          : "";
      const weekendTaskTab =
        weekendOptionSelected == "3" ? "left" : weekendOptionSelected == "2" ? "center" : "right";
      const weekEndTaskCheck = weekendOptionSelected ? true : false;
      // Conditions applied to set repeat details object according to selected tab
      if (repeatTaskType == "Daily") {
        //Daily
        setState({
          ...state,
          daily: { ...daily, repeatDetails: cfData.repeatDetails },
          weekendTaskTab,
          repeatTypeTab,
          weekEndTaskCheck,
        });
      } else if (repeatTaskType == "Weekly") {
        // Weekly
        setState({
          ...state,
          weekly: { ...weekly, repeatDetails: cfData.repeatDetails },
          weekendTaskTab,
          repeatTypeTab,
          weekEndTaskCheck,
        });
      } else {
        // Monthly
        setState({
          ...state,
          monthly: { ...monthly, repeatDetails: cfData.repeatDetails },
          weekendTaskTab,
          repeatTypeTab,
          weekEndTaskCheck,
        });
      }
    }
  }, []);

  const handleWeekendTaskChange = event => {
    const { weekEndTaskCheck, repeatTypeTab } = state;
    const updatedValue = !weekEndTaskCheck;
    // function to toggle checked and unchecked state of checkbox
    setState({ ...state, weekEndTaskCheck: updatedValue });
    if (!updatedValue) {
      // Incase the weekend checkbox is unchecked the weekendTask Value in state will be empty
      if (repeatTypeTab == "right") {
        updateDailyRepeatDetails("repeatMonthly", "weekendTask", "", updatedValue);
      } else {
        updateDailyRepeatDetails("repeatDaily", "weekendTask", "", updatedValue);
      }
    } else {
      if (repeatTypeTab == "right") {
        updateDailyRepeatDetails("repeatMonthly", "weekendTask", "3", updatedValue);
      } else {
        updateDailyRepeatDetails("repeatDaily", "weekendTask", "3", updatedValue);
      }
    }
  };
  const handleTabSelect = (event, tab) => {
    if (tab) {
      setState({ ...state, repeatTypeTab: tab });
    }
  };
  const showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  const handleWeekendTaskTabSelect = (event, tab) => {
    const { repeatTypeTab } = state;
    if (tab) {
      //Setting the ENUMS based on the tab selected
      let selectedWeekendValue = tab == "left" ? "3" : tab == "center" ? "2" : "1";
      setState({ ...state, weekendTaskTab: tab });
      if (repeatTypeTab == "right") {
        updateDailyRepeatDetails("repeatMonthly", "weekendTask", selectedWeekendValue);
      } else {
        updateDailyRepeatDetails("repeatDaily", "weekendTask", selectedWeekendValue);
      }
    }
  };

  //Updating RepeatDetails
  const updateDailyRepeatDetails = (key, type, value, weekendCheckboxValue) => {
    const { repeatTypeTab, weekendTaskTab } = state;
    const selectedPeriod = // Saving reference to object depending on the selected Tab
      repeatTypeTab == "left" ? "daily" : repeatTypeTab == "center" ? "weekly" : "monthly";
    let weekendSelectedTab = weekendTaskTab;
    if(type === "weekendTask") weekendSelectedTab = value === "1" ? "right" : value === "2" ? "center" : "left";
    let clonedObj = cloneDeep(state[selectedPeriod]); // Cloning Object

    if (type == "root") {
      clonedObj["repeatDetails"][key] = value; // Updating Value on clone object
      setState({
        ...state,
        weekendTaskTab: weekendSelectedTab,
        [selectedPeriod]: clonedObj,
        weekEndTaskCheck:
          weekendCheckboxValue == false || weekendCheckboxValue == true
            ? weekendCheckboxValue
            : state.weekEndTaskCheck,
      }); // Updating state with the new object of daily repeat meeting
    } else {
      clonedObj["repeatDetails"][key][type] = value; // Updating Value on clone object
      setState({
        ...state,
        weekendTaskTab: weekendSelectedTab,
        [selectedPeriod]: clonedObj,
        weekEndTaskCheck:
          weekendCheckboxValue == false || weekendCheckboxValue == true
            ? weekendCheckboxValue
            : state.weekEndTaskCheck,
      }); // Updating state with the new object of daily repeat meeting
    }
  };
  const handleSubmitRepeatTask = () => {
    const { repeatTypeTab } = state;
    const { customFieldChange } = props;
    setState({ ...state, btnQuery: "progress" });
    const selectedPeriod = // Saving reference to object depending on the selected Tab
    repeatTypeTab == "left" ? "daily" : repeatTypeTab == "center" ? "weekly" : "monthly";
    
    customFieldChange(
      { ...state[selectedPeriod]},
      currentCustomField,
      null,
      //Success
      () => {
        setState({ ...state, btnQuery: "" });
        props.closeAction();
      },
      //failure
      () => {
        showSnackBar(error.data.message, "error");
        setState({ ...state, btnQuery: "" });
      }
    );
  };

  const deleteTaskSchedule = () => {
    /** function calls when user delete schedule  */
    const { task, returnUpdatedTask } = props;
    props.deleteTaskSchedule(
      task.taskId,
      success => {
        props.closeAction();
        if (returnUpdatedTask) returnUpdatedTask(success);
      },
      err => {}
    );
  };

  const {
    btnQuery,
    alignment,
    weekEndTaskCheck,
    repeatTypeTab,
    weekendTaskTab,
    daily,
    weekly,
    monthly,
  } = state;

  const remindToSelection =
    repeatTypeTab == "left"
      ? daily.repeatDetails.remindTo.length
      : repeatTypeTab == "center"
      ? weekly.repeatDetails.remindTo.length
      : repeatTypeTab == "right"
      ? monthly.repeatDetails.remindTo.length
      : 0; 

  return (
    <div>
      <div className={classes.topGrayBar}>
        <Typography variant="body1" className={classes.repeatTaskHeading}>
          {label}
        </Typography>
        <div className={classes.toggleContainer}>
          <ToggleButtonGroup
            value={repeatTypeTab}
            exclusive
            onChange={handleTabSelect}
            classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}>
            <ToggleButton
              value="left"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="task.detail-dialog.repeat-task.daily.label"
                defaultMessage="Daily"
              />
            </ToggleButton>
            <ToggleButton
              value="center"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="task.detail-dialog.repeat-task.weekly.label"
                defaultMessage="Weekly"
              />
            </ToggleButton>

            <ToggleButton
              value="right"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              <FormattedMessage
                id="task.detail-dialog.repeat-task.monthly.label"
                defaultMessage="Monthly"
              />
            </ToggleButton>
          </ToggleButtonGroup>
        </div>
      </div>
      <div className={classes.contentCnt}>
        {repeatTypeTab == "left" ? (
          <Daily
            data={daily}
            updateRepeatData={updateDailyRepeatDetails}
            task={currentCustomFieldData ? currentCustomFieldData.fieldData.data : {}}
            profile={profile}
            members={members}
          />
        ) : repeatTypeTab == "center" ? (
          <Weekly
            data={weekly}
            updateRepeatData={updateDailyRepeatDetails}
            task={currentCustomFieldData ? currentCustomFieldData.fieldData.data : {}}
            members={members}
            profile={profile}
          />
        ) : (
          <Monthly
            data={monthly}
            updateRepeatData={updateDailyRepeatDetails}
            task={currentCustomFieldData ? currentCustomFieldData.fieldData.data : {}}
            members={members}
            profile={profile}
          />
        )}
        {/* Weekend Check Container */}
        <div className={classes.weekendCheckCnt}>
          {(repeatTypeTab == "left" ||
            (repeatTypeTab == "right" &&
              monthly.repeatDetails.repeatMonthly.timePeriod == "Day")) && (
            <>
              <DefaultCheckbox
                checked={weekEndTaskCheck}
                onChange={handleWeekendTaskChange}
                label={"Review on weekend"}
                styles={{ display: "flex", alignItems: "center" }}
                checkboxStyles={{ paddingLeft: 0 }}
              />
              {/* Weekend Task Toggle Buttons */}
              {weekEndTaskCheck && (
                <div className={classes.toggleContainer}>
                  <ToggleButtonGroup
                    value={weekendTaskTab}
                    exclusive
                    onChange={handleWeekendTaskTabSelect}
                    classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal  }}>
                    <ToggleButton
                      value="left"
                      //   onClick={renderListView}
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      <FormattedMessage
                        id="task.detail-dialog.repeat-task.common.before-week.label"
                        defaultMessage="Before weekend"></FormattedMessage>
                    </ToggleButton>
                    <ToggleButton
                      value="center"
                      //   onClick={renderGridView}
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      <FormattedMessage
                        id="task.detail-dialog.repeat-task.common.on-weekend.label"
                        defaultMessage="On weekend"></FormattedMessage>
                    </ToggleButton>

                    <ToggleButton
                      value="right"
                      //   onClick={renderCalendarView}
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      <FormattedMessage
                        id="task.detail-dialog.repeat-task.common.after-weekend.label"
                        defaultMessage="After weekend"></FormattedMessage>
                    </ToggleButton>
                  </ToggleButtonGroup>
                </div>
              )}
            </>
          )}
        </div>

        <NotificationMessage type="info" iconType="info" style={{ marginTop: 10 }}>
          {getInfoMessage(state)}
        </NotificationMessage>
      </div>
      <ButtonActionsCnt
        // cancelAction={closeAction}
        successAction={handleSubmitRepeatTask}
        successBtnText={task.repeatTask ? "Update" : "Save"}
        deleteBtnText={task.repeatTask ? "Delete" : ""}
        deleteAction={deleteTaskSchedule}
        // cancelBtnText="Discard Changes"
        btnType="success"
        btnQuery={btnQuery}
        disabled={remindToSelection == 0}
        deletePer={true}
      />
    </div>
  );
}

RepeatScheduleCmp.defaultProps = {
  task: {},
  label: "",
};

const mapStateToProps = state => {
  return {
    members: state.profile.data ? state.profile.data.member.allMembers : [],
    customFields: state.customFields,
    profile: state.profile.data,
  };
};
export default compose(
  withSnackbar,
  withStyles(repeatTaskStyles, { withTheme: true }),
  connect(mapStateToProps, {
    repeatTask,
    deleteTaskSchedule,
  })
)(RepeatScheduleCmp);
