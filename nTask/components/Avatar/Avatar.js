import React, { Component } from "react";
import Avatar from "@material-ui/core/Avatar";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import CircularProgress from "@material-ui/core/CircularProgress";
import randomcolor from "randomcolor";
import Badge from "@material-ui/core/Badge";
import Tooltip from "@material-ui/core/Tooltip";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Card from "./Card";
import avatarStyles from "./styles";

class CustomAvatar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  getRandomColor = str => {
    let hash = 0;
    if (str) {
      for (let i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
      }
    }

    const h = hash % 360;
    return `hsl(${h}, ${60}%, ${50}%)`;
  };

  handleOpenCard = () => {
    this.setState({ open: true });
  };

  handleCloseCard = () => {
    this.setState({ open: false });
  };

  render() {
    const {
      classes,
      personalProfile,
      personal,
      constants,
      loggedInTeam,
      userActiveTeam,
      size,
      otherMember,
      otherWorkspace,
      customClass,
      styles,
      hideBadge,
      darkBg,
      disableCard,
      otherTeam,
    } = this.props;
    const { open } = this.state;
    const className = size ? classes[`avatar${size}`] : customClass; // Applying class name based on props
    const activeWorkspace = loggedInTeam
      ? personalProfile.workspace.find(x => x.teamId === personalProfile.loggedInTeam) || {}
      : null;
    const activeTeam = userActiveTeam
      ? personalProfile.teams.find(x => x.companyId === personalProfile.activeTeam) || {}
      : null;

    return activeWorkspace ? (
      <Avatar
        style={{
          background: this.getRandomColor(activeWorkspace.teamName),
          ...styles,
        }}
        src={
          activeWorkspace.pictureUrl
            ? `${activeWorkspace.imageBasePath}${activeWorkspace.pictureUrl}`
            : null
        }
        className={`${className} ${classes.avatar}`}
      >
        {!activeWorkspace.pictureUrl
          ? activeWorkspace.teamName
            ? activeWorkspace.teamName.slice(0, 1).toUpperCase()
            : "U"
          : null}
      </Avatar>
    ) : activeTeam ? (
      <Avatar
        style={{
          background: this.getRandomColor(activeTeam.companyName),
          ...styles,
        }}
        src={activeTeam.companyImage ? activeTeam.companyImage.pictureThumbnail_40X40 : null}
        className={`${className} ${classes.avatar}`}
      >
        {!activeTeam.companyImage
          ? activeTeam.companyName
            ? activeTeam.companyName.slice(0, 1).toUpperCase()
            : "T"
          : null}
      </Avatar>
    ) : otherTeam ? (
      <Avatar
        style={{
          background: this.getRandomColor(otherTeam.companyName),
          ...styles,
        }}
        src={
          otherTeam.companyImage && otherTeam.companyImage.pictureUrl
            ? otherTeam.companyImage.pictureUrl
            : null
        }
        className={`${className} ${classes.avatar}`}
      >
        {!otherTeam.companyImage ? otherTeam.companyName.slice(0, 1).toUpperCase() : null}
      </Avatar>
    ) : otherWorkspace ? (
      <Avatar
        style={{
          background: this.getRandomColor(otherWorkspace.teamName),
          ...styles,
        }}
        src={
          otherWorkspace.pictureUrl ? `${otherWorkspace.baseUrl}${otherWorkspace.pictureUrl}` : null
        }
        className={`${className} ${classes.avatar}`}
      >
        {!otherWorkspace.pictureUrl ? otherWorkspace.teamName.slice(0, 1).toUpperCase() : null}
      </Avatar>
    ) : personal ? (
      <Badge
        variant="dot"
        invisible={hideBadge}
        style={styles}
        classes={{
          root: classes.badgeRoot,
          badge: darkBg ? classes.userStatusBadgeDark : classes.userStatusBadge,
          dot: size == 'xsmall' ? classes.userStatusDotSmall : classes.userStatusDot,
        }}

      >
        <Avatar
          src={personalProfile.imageUrl ? `${personalProfile.imageUrl}` : null}
          className={`${className} ${classes.avatar} ${personalProfile.isOwner ? classes.ownerAvatar : ""
            }`}
          style={{
            background: this.getRandomColor(personalProfile.fullName),
            ...styles,
          }}
        >
          {!personalProfile.imageUrl && personalProfile.fullName
            ? personalProfile.fullName[0].toUpperCase()
            : personalProfile.email
              ? personalProfile.email[0].toUpperCase()
              : ""}
        </Avatar>
      </Badge>
    ) : otherMember ? (
      disableCard ? (
        <Badge
          variant="dot"
          style={styles}
          invisible={!otherMember.isOnline}
          classes={{
            root: classes.badgeRoot,
            badge: darkBg ? classes.userStatusBadgeDark : classes.userStatusBadge,
            dot: size == 'xsmall' ? classes.userStatusDotSmall : classes.userStatusDot,
          }}

        >
          <Avatar
            src={otherMember.imageUrl ? `${otherMember.imageUrl}` : null}
            className={`${className} ${otherMember.isOwner && darkBg
              ? classes.ownerAvatarDark
              : otherMember.isOwner
                ? classes.ownerAvatar
                : ""
              }`}
            style={{
              background: this.getRandomColor(
                otherMember.fullName ? otherMember.fullName : otherMember.email
              ),
              ...styles,
            }}
          >
            {!otherMember.imageUrl && otherMember.fullName
              ? otherMember.fullName[0].toUpperCase()
              : personalProfile.email
                ? personalProfile.email[0].toUpperCase()
                : ""}
          </Avatar>
        </Badge>
      ) : (
        <ClickAwayListener mouseEvent="onMouseDown"
          touchEvent="onTouchStart" onClickAway={this.handleCloseCard}>
          <div>
            <Tooltip
              interactive
              // PopperProps={{
              //   disablePortal: true
              // }}
              open={open}
              disableFocusListener
              disableHoverListener
              disableTouchListener
              title={(
                <Card
                  memberEmail={otherMember.email}
                  closeTaskDetailsPopUp={this.props.closeTaskDetailsPopUp}
                />
              )}
              classes={{
                tooltip: classes.userCardToolTip,
                popper: classes.userCardToolTipPop,
              }}
            >
              <Badge
                variant="dot"
                style={styles}
                invisible={!otherMember.isOnline}
                classes={{
                  root: classes.badgeRoot,
                  badge: darkBg ? classes.userStatusBadgeDark : classes.userStatusBadge,
                  dot: size == 'xsmall' ? classes.userStatusDotSmall : classes.userStatusDot,
                }}

              >
                <Avatar
                  src={otherMember.imageUrl ? `${otherMember.imageUrl}` : null}
                  onClick={this.handleOpenCard}
                  onClose={this.handleCloseCard}
                  className={`${className} ${otherMember.isOwner && darkBg
                    ? classes.ownerAvatarDark
                    : otherMember.isOwner
                      ? classes.ownerAvatar
                      : ""
                    }`}
                  style={{
                    background: this.getRandomColor(
                      otherMember.fullName ? otherMember.fullName : otherMember.email
                    ),
                  }}
                >
                  {!otherMember.imageUrl && otherMember.fullName
                    ? otherMember.fullName[0].toUpperCase()
                    : otherMember.email
                      ? otherMember.email[0].toUpperCase()
                      : ""}
                </Avatar>
              </Badge>
            </Tooltip>
          </div>
        </ClickAwayListener>
      )
    ) : null

  }
}

const mapStateToProps = state => {
  return {
    personalProfile: state.profile.data,
  };
};
export default compose(
  connect(mapStateToProps),
  withStyles(avatarStyles, { withTheme: true })
)(CustomAvatar);
