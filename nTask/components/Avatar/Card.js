import React, { Component } from "react";
import CustomAvatar from "./Avatar";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import avatarStyles from "./styles";
import GetUserPendingTask from "../../helper/getUserPendingTask";
import Typography from "@material-ui/core/Typography";
import RoundIcon from "@material-ui/icons/Brightness1";
import ChevronRight from "@material-ui/icons/KeyboardArrowRight";
import { setAppliedFilters } from "../../redux/actions/appliedFilters";
import moment from "moment";
import BrifcaseIcon from "@material-ui/icons/Work";
import TimeIcon from "@material-ui/icons/Timer";
import { teamCanView } from "../PlanPermission/PlanPermission";
import isEmpty from "lodash/isEmpty";
import { FormattedDate, FormattedNumber,FormattedMessage, FormattedTime } from "react-intl";
import { updateTaskFilter } from "../../redux/actions/tasks";
import {
  issueDetailDialogState,
  riskDetailDialog,
  taskDetailDialogState,
} from "../../redux/actions/allDialogs";

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleViewAllTasksClick = (e, userId) => {
    const { setAppliedFilters, closeTaskDetailsPopUp } = this.props;
    e.stopPropagation();
    const filterObj = {assigneeList: {type: '', selectedValues: [userId]}}
    this.props.history.push("/tasks");
    this.props.updateTaskFilter(filterObj);
    this.props.riskDetailDialog({
      id: "",
      afterCloseCallBack: null,
      type: "",
    }, null);
    this.props.issueDetailDialogState({
      id: "",
      afterCloseCallBack: null,
      type: "",
    }, null);
    if (closeTaskDetailsPopUp) closeTaskDetailsPopUp();
  };

  render() {
    const { profile, memberEmail, tasks, classes, theme, history } = this.props;
    let selectedMember = profile.member.allMembers.find(member => {
      return member.email == memberEmail;
    })||{};
    let searchQuery = history.location.pathname; //getting issue id in the url
    let viewAllTask = searchQuery == "/reports/overview" ||  searchQuery == "/reports/IssueOverview";
    return (
      <>
        <div className={classes.personalInfoCnt}>
          <CustomAvatar
            otherMember={{
              imageUrl: selectedMember ? selectedMember.imageUrl : null,
              fullName: selectedMember ? selectedMember.fullName : null,
              lastName: "",
              email: selectedMember ? selectedMember.email : null,
              isOnline: selectedMember ? selectedMember.isOnline : null,
              isOwner: selectedMember ? selectedMember.isOwner : null
            }}
            disableCard={true}
            size="small"
          />
          <div className={classes.personalInfoContentCnt}>
            <Typography variant="h5" className={classes.memberName}>
              {selectedMember.fullName}
            </Typography>
            <Typography variant="body2">{selectedMember.roleName}</Typography>
          </div>
        </div>
        <div className={classes.memberOtherDetailsCnt}>
          <Typography variant="body2" className={classes.localTime}>
            <TimeIcon className={classes.timeIcon} />
            {!isEmpty(selectedMember) 
            ?  <FormattedDate value={moment().utcOffset(selectedMember.timeZone).format('YYYY-MM-DD HH:mm')} hour= 'numeric'  minute= 'numeric' hourCycle= 'h12' /> 
            : ''}
             <FormattedMessage id="common.profile-card.local-time.label" defaultMessage="Local Time"/>
          </Typography>
          {selectedMember.jobRole ? (
            <>
              {" "}
              <Typography variant="body2" className={classes.jobTitle}>
                {" "}
                <BrifcaseIcon className={classes.brifcaseIcon} /> jkh kjh k{" "}
                {selectedMember.jobRole}
              </Typography>
            </>
          ) : null}
        </div>
        <div>
          <Typography variant="body2" className={classes.taskInfo}>
            <Typography variant="body2">
              {" "}
              <RoundIcon
                htmlColor={theme.palette.secondary.main}
                classes={{ root: classes.statusIcon }}
              />{" "}
              <span className={classes.pendingTaskCount}>
          {<FormattedNumber  value={GetUserPendingTask(tasks ? tasks :[] , selectedMember.userId).length} />}{" "}
              </span>
           {<FormattedMessage id="common.profile-card.task.pending.label" defaultMessage="Pending Tasks"/>}   
            </Typography>
          </Typography>
        </div>
        {teamCanView("advanceFilterAccess") && !viewAllTask &&
        <div
          className={classes.viewAllTaskBtn}
          onClick={e => this.handleViewAllTasksClick(e, selectedMember.userId)}
        >
          <FormattedMessage id="common.profile-card.task.view.label" defaultMessage="View All Tasks"/>
          <ChevronRight
            htmlColor={theme.palette.secondary.light}
            className={classes.chevronIcon}
          />
        </div>}
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    tasks: state.tasks.data
  };
};

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { setAppliedFilters, updateTaskFilter, riskDetailDialog, issueDetailDialogState }
  ),
  withStyles(avatarStyles, { withTheme: true })
)(Card);
