const avatarStyles = (theme) => ({
  avatarsmall18: {
    width: 18,
    height: 18,
    fontSize: "9px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
  },
  avatarsmall24: {
    width: 24,
    height: 24,
    fontSize: 16,
  },
  avatarsmall26: {
    width: 26,
    height: 26
  },
  avatarxsmall: {
    width: 28,
    height: 28,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize:'1.0714285714285714rem',
    fontWeight: 500,
  },
  avatarsmall: {
    width: 32,
    height: 32,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize:'1.0714285714285714rem',
    fontWeight: 500,
    fontSize: "20px !important",
  },
  avatarsmall36: {
    width: 36,
    height: 36,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize:'1.0714285714285714rem',
    fontWeight: 500,
    fontSize: "20px !important",
  },
  avatarxmedium: {
    width: 42,
    height: 42,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
    fontSize: "23px !important",
  },
  avatarmedium: {
    width: 48,
    height: 48,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
    fontSize: "23px !important",
  },
  avatarlarge: {
    width: 70,
    height: 70,
    fontSize: "42px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
  },
  avatarxlarge: {
    width: 70,
    height: 70,
    fontSize: "42px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
  },
  avatarxxlarge: {
    width: 100,
    height: 100,
    fontSize: "62px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
  },
  userStatusBadge: {
    top: "30px",
    right: 5,
    minWidth: 12,
    borderRadius: '50%',
    height: 12,
    border: `2px solid ${theme.palette.common.white}`,
  },
  userStatusDot: {
    backgroundColor: "#2fd679",
  },
  userStatusDotSmall: {
    backgroundColor: "#2fd679",
    minWidth: 8,
    height: 8,
    right: 6,
    top: 27
  },
  badgeRoot:{
    // paddingRight: '16px'
  },
  userStatusBadgeDark: {
    top: "88%",
    right: 5,
    minWidth: 10,
    borderRadius: '50%',
    height: 10,
    border: `1px solid ${theme.palette.secondary.dark}`,
  },
  ownerAvatar: {
    border: `2px solid ${theme.palette.border.yellowBorder}`,
    boxShadow: `inset 0px 0px 0px 2px ${theme.palette.common.white}`,
  },
  ownerAvatarDark: {
    border: `2px solid ${theme.palette.border.yellowBorder}`,
    boxShadow: `inset 0px 0px 0px 2px ${theme.palette.shadow.darkShadow}`,
  },
  userCardToolTipPop: {
    opacity: 1,
    width: 220,
    zIndex: 11111,
  },
  userCardToolTip: {
    background: theme.palette.common.white,
    padding: 0,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
  },
  personalInfoCnt: {
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "10px 0 10px 0",
    margin: "0 10px",
    display: "flex",
    alignItems: "center",
  },
  taskInfo: {
    padding: "5px 0",
    margin: "0 10px",
  },
  personalInfoContentCnt: {
    marginLeft: 10,
  },
  memberOtherDetailsCnt: {
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "5px 0",
    margin: "0 10px",
  },
  statusIcon: {
    fontSize: "11px !important",
  },
  pendingTaskCount: {
    fontSize: "14px !important",
    color: theme.palette.text.primary,
  },
  viewAllTaskBtn: {
    background: theme.palette.background.paper,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    cursor: "pointer",
    padding: "6px 10px",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightMedium,
    borderRadius: "0 0 4px 4px",
  },
  memberName: {
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    width: 130,
  },
  chevronIcon: {
    fontSize: "18px !important",
  },
  localTime: {
    display: "flex",
    alignItems: "center",
  },
  jobTitle: {
    display: "flex",
    alignItems: "center",
  },
  timeIcon: {
    marginRight: 5,
    fontSize: "16px !important",
  },
  brifcaseIcon: {
    marginRight: 5,
  },
});

export default avatarStyles;
