
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import React, { useEffect, useMemo, useState } from "react";
import CustomButton from "../Buttons/CustomButton";
import { styles } from "./HelpCenter.style";
import DefaultTextField from "../Form/TextField";
import SvgIcon from "@material-ui/core/SvgIcon";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import IconBook from "../Icons/IconBook";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import IconEmail from "../Icons/IconEmail";
import IconCallCalling from "../Icons/IconCallCalling";
import NoSupportArticles from "../Icons/NoSupportArticles";
import Loader from "../Loader/Loader";

const SupportArticles = ({ articles, flag, classes }) => {
  const [search, setSearch] = useState("");

  const filteredarticles = useMemo(
    () =>
      articles.filter(articles =>
        search
          ? articles.title.match(new RegExp(search, "gi")) ||
          articles.description.match(new RegExp(search, "gi"))
          : true
      ),
    [articles, search]
  );
  const Search = (value) => {
    setSearch(value)
  }
  return (
    <div className={classes.videoTurotial}>
      <DefaultTextField
        fullWidth={false}
        error={false}
        formControlStyles={{ width: 370, marginBottom: 8 }}
        defaultProps={{
          id: "videoTutorialsSearch",
          onChange: ({ target }) => Search(target.value),
          value: search,
          placeholder: "Search article",
          inputProps: {
            maxLength: 150,
            style: { padding: 9 },
          },
          startAdornment: (
            <InputAdornment className={classes.searchinput} position="start">
              <SearchIcon className={classes.SearchIcon} />
            </InputAdornment>
          ),
        }}
      />
      <div className={classes.helpCenterMain}>

        {articles.length && flag ?
          (<>
            {filteredarticles.length ? (
              filteredarticles.map(item => (
                <a href={item.articleURL} target="_blank" className={classes.card}>
                  <Grid container>
                    <Grid item xs={1}>
                      <div className={classes.bookIcons}>
                        <IconBook />
                      </div>
                    </Grid>
                    <Grid item xs={10} style={{ paddingLeft: 10 }}>
                      <Typography variant="body1" className={classes.articleHeading}>
                        {item.title}
                      </Typography>
                      <Typography variant="body1" className={classes.description}>
                        {item.description}
                      </Typography>
                    </Grid>
                  </Grid>
                </a>
              ))
            ) : (
              <div className={classes.noMaterial}>No Articles Found...</div>
            )
            }</>
          ) : flag ?
            <div className={classes.commingSoonWrapper}>
              <div>
                <SvgIcon
                  className={classes.CommingSoonIcon}
                  viewBox="0 0 180 117.752">
                  <NoSupportArticles />
                </SvgIcon>
                <Typography variant="h2" className={classes.commingSoonHeading}>
                  Coming Soon...
                </Typography>
                <Typography variant="p" className={classes.commingSoonText}>
                  Meanwhile, if you have any queries, feel free to send us an email or book a call.
                </Typography>
                <div className={classes.contactSupportActionContainer}>
                  <a href="mailto:support@ntaskmanager.com">
                    <CustomButton
                      style={{
                        borderRadius: 5,
                        padding: 6,
                        border: "1px solid #00ABED",
                        background: "white",
                        color: "#0089BE",
                        width: "100%",
                      }}>
                      <IconEmail color="currentColor" style={{ marginRight: 8 }} />
                      Send Us an Email
                    </CustomButton>
                  </a>
                  <a href="https://www.ntaskmanager.com/get-support/" target='_blank'>
                    <CustomButton
                      style={{
                        borderRadius: 5,
                        padding: 6,
                        border: "1px solid #00ABED",
                        background: "white",
                        color: "#0089BE",
                        width: "100%",
                      }}>
                      <IconCallCalling
                        style={{ fontSize: "1rem", marginRight: 8 }}
                        color="currentColor"
                      />
                      Book a Call
                    </CustomButton>
                  </a>
                </div>
              </div>
            </div> :
            <Loader />
        }

      </div>
      {articles.length ?
        <div className={classes.contactSupportContainer}>
          <Typography variant="body1" className={classes.contactSupportText}>
            Still can't find an answer?
          </Typography>
          <div className={classes.contactSupportActionContainer}>
            <a href="mailto:support@ntaskmanager.com">
              <CustomButton
                style={{
                  borderRadius: 5,
                  padding: 6,
                  border: "1px solid #00ABED",
                  background: "white",
                  color: "#0089BE",
                  width: "100%",
                }}>
                <IconEmail color="currentColor" style={{ marginRight: 8 }} />
                Send Us an Email
              </CustomButton>
            </a>
            <a href="https://www.ntaskmanager.com/get-support/" target='_blank'>
              <CustomButton
                style={{
                  borderRadius: 5,
                  padding: 6,
                  border: "1px solid #00ABED",
                  background: "white",
                  color: "#0089BE",
                  width: "100%",
                }}>
                <IconCallCalling
                  style={{ fontSize: "1rem", marginRight: 8 }}
                  color="currentColor"
                />
                Book a Call
              </CustomButton>
            </a>
          </div>
        </div> : null
      }
    </div>
  );
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(SupportArticles);
