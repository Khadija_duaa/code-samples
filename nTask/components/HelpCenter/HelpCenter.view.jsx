
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import React, { useEffect, useState } from "react";
import CustomDrawer from "../Drawer/CustomDrawer";
import { IconSlidePanel } from "../Icons/IconSlidePanel";
import { styles } from "./HelpCenter.style";
import { GetHelpingMaterial } from "../../redux/actions/header";
import CustomIconButton from "../Buttons/CustomIconButton";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import HelpCircle from "../Icons/HelpCircle";
import IconButton from "../Buttons/IconButton";
import SupportArticles from "./SupportArticles";
import VideoTutorials from "./VideoTutorials";
const HelpCenter = ({ classes, location, theme }) => {
  const [tab, setTab] = useState("supportArticles");
  const [videos, setVideos] = useState([]);
  const [articles, setArticles] = useState([]);
  const [flag, setFlag] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setFlag(false)
    GetHelpingMaterial(
      location.pathname == '/timesheet' ? '/timesheets' : location.pathname,
      resp => {
        const articlesArray = resp.data.entity.filter(item => item.articleURL !== null);
        const videosArray = resp.data.entity.filter(item => item.videoURL !== null);

        setVideos(videosArray);
        setArticles(articlesArray);
        setFlag(true)
      },
      () => {
        setVideos([]);
        setArticles([]);
        setFlag(true)
      }
    );
    setTab('supportArticles');
  }, [location.pathname]);
  return (
    <div className={classes.root}>
      <IconButton btnType="transparent" style={{ padding: 8 }} onClick={() => setOpen(!open)}>
        <HelpCircle className={classes.CTAicon} />
      </IconButton>
      <CustomDrawer
        hideCloseIcon
        drawerProps={{
          className: classes.drawer,
          PaperProps: {
            height: "100%",
            boxShadow: "-4px 6px 12px #00000029",
            borderTop: `1px solid ${theme.palette.border.lightBorder}`,
          },
        }}
        open={open}
        closeDrawer={() => setOpen(false)}
        hideHeader={true}
        drawerWidth={400}>
        <div className={classes.drawerContent}>
          <Grid container justify="space-between" alignItems="center">
            <Grid item>
              <Typography variant="h3">
                <b>Help Center</b>
              </Typography>
            </Grid>
            <Grid item>
              <CustomIconButton
                style={{ padding: 5, borderRadius: 0 }}
                iconBtn
                onClick={() => setOpen(false)}>
                <IconSlidePanel />
              </CustomIconButton>
            </Grid>
          </Grid>
          <Tabs
            value={tab}
            indicatorColor="primary"
            textColor="primary"
            onChange={(e, value) => {
              setTab(value);
            }}
            className={classes.tabs}>
            <Tab
              className={classes.tab}
              classes={{ wrapper: classes.tablabel }}
              value="supportArticles"
              label="Support Articles"
            />
            <Tab
              className={classes.tab}
              classes={{ wrapper: classes.tablabel }}
              value="videoTutorials"
              label="Video Tutorials"
            />
          </Tabs>
          {tab == 'supportArticles' && <SupportArticles articles={articles} flag={flag} />}
          {tab == 'videoTutorials' && <VideoTutorials videos={videos} flag={flag} />}
        </div>
      </CustomDrawer>
    </div>
  );
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(HelpCenter);
