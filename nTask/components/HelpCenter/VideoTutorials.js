
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import React, { useMemo, useState } from "react";
import CustomButton from "../Buttons/CustomButton";
import { styles } from "./HelpCenter.style";
import DefaultTextField from "../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import CustomDialog from "../Dialog/CustomDialog";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import IconEmail from "../Icons/IconEmail";
import IconCallCalling from "../Icons/IconCallCalling";
import NoVideos from "../Icons/NoVideos";
import SvgIcon from "@material-ui/core/SvgIcon";
import Loader from "../Loader/Loader";

const VideoTutorials = ({ videos, flag, classes }) => {
  const [selectedVideo, setSelectedVideo] = useState('');
  const [search, setSearch] = useState("");
  const playVideo = (videoUrl) => {
    setSelectedVideo(videoUrl)
  }
  const Search = (value) => {
    setSearch(value)
  }
  const closeVideo = () => {
    setSelectedVideo('')
  }
  const filteredvideo = useMemo(
    () => videos.filter(video => (search ? video.title.match(new RegExp(search, "gi")) : true)),
    [videos, search]
  );

  return (
    < >
      <div className={classes.videoTurotial}>
        <DefaultTextField
          fullWidth={false}
          error={false}
          formControlStyles={{ width: 370, marginBottom: 8 }}
          defaultProps={{
            id: "videoTutorialsSearch",
            onChange: ({ target }) => Search(target.value),
            value: search,
            placeholder: "Search video",
            inputProps: {
              maxLength: 150,
              style: { padding: 9 },
            },
            startAdornment: (
              <InputAdornment className={classes.searchinput} position="start">
                <SearchIcon className={classes.SearchIcon} />
              </InputAdornment>
            ),
          }}
        />
        <div className={classes.helpCenterMain}>
          {videos.length && flag ?
            (<>
              {filteredvideo.length ? (
                filteredvideo.map((item, index) => (
                  <div className={classes.card}>
                    <Grid
                      onClick={() => playVideo(item.videoURL)}
                      container
                      alignItems="center">
                      <Grid item xs={1}>
                        <Typography variant="body1" className={classes.index}>
                          {index + 1}
                        </Typography>
                      </Grid>
                      <Grid xs={2}>
                        <img
                          alt="thumbnail"
                          src={`https://img.youtube.com/vi/${item.videoURL.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/)[1]}/0.jpg`
                          }
                          className={classes.helpImage}
                        />
                      </Grid>
                      <Grid item xs={9}>
                        <Typography variant="body1" className={classes.videotitle}>
                          {item.title}
                        </Typography>
                      </Grid>
                    </Grid>
                  </div>
                ))
              ) : (
                <div className={classes.noMaterial}>No Videos Found...</div>
              )}
            </>) : flag ?
              <div className={classes.commingSoonWrapper}>
                <div>
                  <SvgIcon
                    className={classes.CommingSoonIcon}
                    viewBox="0 0 180 117.752">
                    <NoVideos />
                  </SvgIcon>
                  <Typography variant="h2" className={classes.commingSoonHeading}>
                    Coming Soon...
                  </Typography>
                  <Typography variant="p" className={classes.commingSoonText}>
                    Meanwhile, if you have any queries, feel free to send us an email or book a call.
                  </Typography><div className={classes.contactSupportActionContainer}>
                    <a href="mailto:support@ntaskmanager.com">
                      <CustomButton
                        style={{
                          borderRadius: 5,
                          padding: 6,
                          border: "1px solid #00ABED",
                          background: "white",
                          color: "#0089BE",
                          width: "100%",
                        }}>
                        <IconEmail color="currentColor" style={{ marginRight: 8 }} />
                        Send Us an Email
                      </CustomButton>
                    </a>
                    <a href="https://www.ntaskmanager.com/get-support/" target='_blank'>
                      <CustomButton
                        style={{
                          borderRadius: 5,
                          padding: 6,
                          border: "1px solid #00ABED",
                          background: "white",
                          color: "#0089BE",
                          width: "100%",
                        }}>
                        <IconCallCalling
                          style={{ fontSize: "1rem", marginRight: 8 }}
                          color="currentColor"
                        />
                        Book a Call
                      </CustomButton>
                    </a>
                  </div>
                </div>
              </div> :
              <Loader />
          }
        </div>
        {videos.length ?
          <div className={classes.contactSupportContainer}>
            <Typography variant="body1" className={classes.contactSupportText}>
              Still can't find an answer?
            </Typography>
            <div className={classes.contactSupportActionContainer}>
              <a href="mailto:support@ntaskmanager.com">
                <CustomButton
                  style={{
                    borderRadius: 5,
                    padding: 6,
                    border: "1px solid #00ABED",
                    background: "white",
                    color: "#0089BE",
                    width: "100%",
                  }}>
                  <IconEmail color="currentColor" style={{ marginRight: 8 }} />
                  Send Us an Email
                </CustomButton>
              </a>
              <a href="https://www.ntaskmanager.com/get-support/" target='_blank'>
                <CustomButton
                  style={{
                    borderRadius: 5,
                    padding: 6,
                    border: "1px solid #00ABED",
                    background: "white",
                    color: "#0089BE",
                    width: "100%",
                  }}>
                  <IconCallCalling
                    style={{ fontSize: "1rem", marginRight: 8 }}
                    color="currentColor"
                  />
                  Book a Call
                </CustomButton>
              </a>
            </div>
          </div>
          : null
        }
      </div>
      <CustomDialog
        scroll="paper"
        dialogProps={{
          open: selectedVideo,
          onClose: closeVideo,
          onClick: e => e.stopPropagation(),
          maxWidth: "lg",
        }}
        hidetitle>
        <div className={classes.dialogContentCnt}>
          <iframe
            style={{ width: "100%", minHeight: "70vh" }}
            src={selectedVideo && `https://www.youtube.com/embed/${selectedVideo.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/)[1]}`}
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          />
        </div>
      </CustomDialog>
    </>
  );
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(VideoTutorials);
