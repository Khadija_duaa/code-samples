export const styles = theme => ({
  CTAicon: {
    marginRight: 4,
    fontSize: "18px !important",
    borderRadius: 50,
  },
  dialogContentCnt: {
    marginTop: "-24px",
    maxHeight: "70vh",
  },
  root: {
    color: theme.palette.secondary.dark,
    position: "relative",
    "& *": {
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
  tab: {
    minHeight: "unset",
    fontSize: "14px !important",
    minWidth: 70,
    whiteSpace: "nowrap",
    textTransform: "none",
    marginRight: "1rem !important",
  },
  tablabel: {
    color: theme.palette.text.primary,
    padding: "6px 0 !important",
  },
  drawer: {
    position: "fixed",
    zIndex: 1000,
  },
  drawerContent: {
    height: "100%",
    padding: "1rem",
    position: "relative",
  },
  card: {
    display: "flex",
    cursor: "pointer",
    color: theme.palette.common.black,
    padding: "0 1rem",
    textDecoration: "none",

    "& > div": {
      padding: "10px 0",
      borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
    },

    "&:hover": {
      background: `#E6F7FD !important`,
    },
  },
  noMaterial: {
    marginTop: 16,
    color: theme.palette.common.black,
    padding: "0 1rem",
    textAlign: "center",
  },

  headhelp: {
    color: theme.palette.common.black,
    fontSize: "14px !important",
  },
  helpImage: {
    height: "40px",
    width: "60px",
  },
  title: {
    paddingLeft: "50px",
  },
  videoTitle: {
    color: theme.palette.common.black,
    fontSize: "14px !important",
  },
  helpCenterMain: {
    height: "calc(100vh - 323px)",
    overflow: "auto",
    margin: "0 -1rem",
  },
  tabs: {
    margin: "0 -1rem",
    "& > div": {
      padding: "0 1rem",
      borderBottom: "1px solid #DFE5E8",
    },
  },

  description: {
    marginTop: 4,
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
  index: {
    color: "#161717",
  },
  contactSupportContainer: {
    padding: 16,
    backgroundColor: "#E6F7FD",
    marginTop: 16,
    borderRadius: 6,
  },
  contactSupportText: {
    textAlign: "center",
    color: "#161717",
  },
  contactSupportActionContainer: {
    display: "flex",
    justifyContent: "space-around",
    marginTop: 16,

    "& > a": {
      width: "50%",
      padding: "0 4px",
      textDecoration: "none",
    },
  },
  SearchIcon: {
    color: theme.palette.customFieldIcons.grey,
  },
  searchinput: {
    marginRight: "unset",
    marginLeft: "-6px",
  },
  videotitle: {
    paddingLeft: 10,
    color: "#161717",
  },
  articleHeading: {
    color: "#161717",
  },

  customIcon: {
    minWidth: "unset",
    marginLeft: 5,
    padding: ".5rem",
  },
  commingSoonWrapper: {
    color: theme.palette.common.black,
    padding: "0 1rem",
    textAlign: "center",
    display: 'flex',
    alignItems: 'center',
    maxWidth: '100%',
    width: 360,
    height: '100%',
    margin: 'auto',
  },
  CommingSoonIcon: {
    width: 200,
    maxWidth: '100%',
    height: 'auto',
    display: 'block',
    margin: '30px auto',
  },
  bookIcons: {
    transform: 'translateY(4.5px)'
  },
  commingSoonHeading: {
    fontSize: "20px !important",
    fontWeight: 900,
    marginBottom: 15,
  },
});
