// @flow

import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import styles from "./styles.js";
import { withStyles } from "@material-ui/core/styles";
import Tile from "./Tile";
import CheckCircleOutline from "@material-ui/icons/CheckCircleOutline";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { attachFeatureImg } from "../../redux/actions/projects";
import classNames from "classnames";
import ColorPicker from "../Dropdown/ColorPicker/ColorPicker";
import CustomButton from "../Buttons/CustomButton";
import DropdownMenu from "../Menu/DropdownMenu";
import SearchIcon from "@material-ui/icons/Search";
import DefaultTextField from "../Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import isEmpty from "lodash/isEmpty";
import { Scrollbars } from "react-custom-scrollbars";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import DefaultCheckbox from "../../components/Form/Checkbox";
import Typography from "@material-ui/core/Typography";
import clsx from "clsx";
import { injectIntl } from "react-intl";
import CheckIcon from "@material-ui/icons/Check";
import Divider from "@material-ui/core/Divider";
import { CanAccessFeature } from "../AccessFeature/AccessFeature.cmp.js";

type ColorImagesModalProps = {
  theme: Object,
  classes: Object,
  anchorTarget: Object,
  handleColorSelect: Function,
  handleImageSelect: Function,
  handleClickStatusSelect: Function,
  useProjectStatusColor: Boolean,
  viewMode: String,
};

function ColorImagesModal(props: ColorImagesModalProps) {
  const {
    classes,
    theme,
    handleImageSelect,
    handleColorSelect,
    anchorTarget,
    intl,
    handleClickAway,
    useProjectStatusColor,
    handleClickStatusSelect,
    viewMode,
    modal,
    size,
    selectedColor = "",
  } = props;

  const [anchorEl, setAnchorEl] = useState(null);
  const [searchTxt, setSearchTxt] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [libAttachment, setLibAttachment] = useState(null);
  const [colorArr, setColorArr] = useState([
    "#ffffff",
    "#359bcf",
    "#e57451",
    "#f89B19",
    "#13be7f",
    "#5174e5",
    "#e551c7",
    "#9651e5",
    "#e55151",
    "#3cccbe",
    "#eebf03",
    "#4aaaee",
    "#195b8a",
    "#d25a4b",
    "#abbc43",
    "#686868",
    "#ad475e",
    "#ADEFD1FF"
  ]);

  const fetchImages = () => {
    fetch(
      `https://api.unsplash.com/photos?client_id=dwLptGju37-qTBvddj3f-ihKzgzdhtnL65ElFj0UBIU&query=bussiness&orientation=landscape&per_page=40&fit=clip&w=100&h=100`
    )
      .then(res => res.json())
      .then(data => {
        setSearchResults(data);
      });
  };

  const SearchImages = (event = {}, click = false, val = "") => {
    if (!isEmpty(searchTxt) && event.key == "Enter") {
      // searchImagesCall(searchTxt);
    } else if (click && !isEmpty(val)) {
      searchImagesCall(val);
    }
  };

  const searchImagesCall = txt => {
    fetch(
      `https://api.unsplash.com/search/photos/?client_id=dwLptGju37-qTBvddj3f-ihKzgzdhtnL65ElFj0UBIU&query=${txt}&orientation=landscape&per_page=40&fit=clip&w=100&h=100`
    )
      .then(res => res.json())
      .then(data => {
        setSearchResults(data.results);
      });
  };

  const handleClose = () => {
    //handleClickAway();
    setAnchorEl(null);
  };
  const handleClickBtn = event => {
    // Function Opens the dropdown
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleChangeSearchTxt = e => {
    e.stopPropagation();
    setSearchTxt(e.target.value);
    SearchImages({}, true, e.target.value);
  };
  const handleSelectImage = item => {
    handleImageSelect(item);
    handleClose();
  };

  const handleSelectColor = color => {
    handleColorSelect(color);
    handleClose();
  };

  useEffect(() => {
    fetchImages();
  }, []);

  const open = Boolean(anchorTarget);
  return (
    <>
      {modal ? (
        <DropdownMenu
          open={open}
          anchorEl={anchorTarget}
          placement="bottom-start"
          size="large"
          closeAction={handleClickAway}>
          <Scrollbars autoHide={true} autoHeight autoHeightMin={0} autoHeightMax={630}>
            <div className={classes.container}>
              <span className={classes.title}>
                {intl.formatMessage({
                  id: "board.color-photo-picker.color.label",
                  defaultMessage: "COLOR",
                })}
              </span>
              <div className={classes.ColorCnt}>
                {colorArr.map((c, index) => {
                  return (
                    <div
                      onClick={() => {
                        handleSelectColor(c);
                      }}
                      key={index}
                      className={classes.colorDiv}
                      style={{
                        backgroundColor: c,
                        border: c.includes("fff") && "1px solid #eaeaea",
                        color: c.includes("fff") ? "black" : "white",
                      }}></div>
                  );
                })}
              </div>
              <CanAccessFeature group='project' feature='status'>
              {viewMode !== "new" && (
                <div className={classes.useStatusCnt}>
                  <DefaultCheckbox
                    checkboxStyles={{ padding: "0px 5px 0 0 " }}
                    checked={useProjectStatusColor}
                    onChange={handleClickStatusSelect}
                    fontSize={20}
                    color={"#0090ff"}
                  />
                  <Typography variant="h6" className={classes.heading}>
                    Use Project Status Color
                  </Typography>
                  <hr />
                </div>
              )}
              </CanAccessFeature>

              <span className={classes.title}>
                {intl.formatMessage({
                  id: "board.color-photo-picker.photo.label",
                  defaultMessage: "PHOTOS ",
                })}
              </span>
              <DefaultTextField
                label=""
                fullWidth={true}
                errorState={null}
                errorMessage={null}
                formControlStyles={{ marginTop: "5px" }}
                defaultProps={{
                  id: "search",
                  type: "text",
                  onChange: handleChangeSearchTxt,
                  onKeyDown: SearchImages,
                  value: searchTxt,
                  autoFocus: true,
                  placeholder: intl.formatMessage({
                    id: "board.color-photo-picker.photo.placeholder",
                    defaultMessage: "Search Photos",
                  }),
                  inputProps: { maxLength: 40 },
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        disableRipple={true}
                        classes={{
                          root: classes.searchIcon,
                        }}
                        onClick={() => {
                          SearchImages({ key: "Enter" }, false, searchTxt);
                        }}>
                        <SearchIcon />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
              <div className={classes.tileBody}>
                {searchResults.map((item, index) => {
                  return (
                    <Tile
                      key={index}
                      imageUrl={item.urls.thumb}
                      handleClick={handleSelectImage}
                      item={item}
                      theme={theme}
                    />
                  );
                })}
              </div>
            </div>
          </Scrollbars>
          <div className={classes.creditCnt}>
            <span className={classes.creditTitle}>
              {intl.formatMessage({
                id: "board.color-photo-picker.photo-from.label",
                defaultMessage: "Photos from Unsplash",
              })}
            </span>
          </div>
        </DropdownMenu>
      ) : (
        <>
          <div>
            <div className={classes.ColorCnt}>
              {["#fff", ...colorArr].map((c, index) => {
                return (
                  <div
                    onClick={() => {
                      handleSelectColor(c);
                    }}
                    className={clsx({
                      [classes.colorDiv]: true,
                      [classes.colorDivlarge]: size == "large",
                    })}
                    key={index}
                    style={{
                      backgroundColor: c,
                      border: c.includes("fff") && "1px solid #eaeaea",
                      color: c.includes("fff") ? "black" : "white",
                    }}>
                    {c == selectedColor ? <CheckIcon /> : null}
                  </div>
                );
              })}
            </div>
            <CanAccessFeature group='project' feature='status'>
            {viewMode !== "new" && (
              <div className={classes.useStatusCnt}>
                <DefaultCheckbox
                  checkboxStyles={{ padding: "0px 5px 0 0 " }}
                  checked={useProjectStatusColor}
                  onChange={handleClickStatusSelect}
                  fontSize={20}
                  color={"#0090ff"}
                />
                <Typography variant="h6" className={classes.heading}>
                  Use Project Status Color
                </Typography>
              </div>
            )}
            </CanAccessFeature>

            <Divider style={{ margin: "16px 0" }} />

            <div className={classes.title}>
              Photos by <span style={{ textDecoration: "underline" }}>Unsplash</span>
            </div>

            <DefaultTextField
              label=""
              fullWidth={true}
              errorState={null}
              errorMessage={null}
              formControlStyles={{ marginTop: "5px" }}
              defaultProps={{
                id: "search",
                type: "text",
                onChange: handleChangeSearchTxt,
                onKeyDown: SearchImages,
                value: searchTxt,
                autoFocus: true,
                placeholder: intl.formatMessage({
                  id: "board.color-photo-picker.photo.placeholder",
                  defaultMessage: "Search Photos",
                }),
                inputProps: { maxLength: 40 },
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      disableRipple={true}
                      classes={{
                        root: classes.searchIcon,
                      }}
                      onClick={() => {
                        SearchImages({ key: "Enter" }, false, searchTxt);
                      }}>
                      <SearchIcon />
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <div className={classes.tileBody}>
              {searchResults.map((item, index) => {
                return (
                  <Tile
                    key={index}
                    imageUrl={item.urls.thumb}
                    handleClick={handleSelectImage}
                    item={item}
                    size={size}
                    theme={theme}
                  />
                );
              })}
            </div>
          </div>

          <div className={classes.creditCnt}>
            <span className={classes.creditTitle}>
              {intl.formatMessage({
                id: "board.color-photo-picker.photo-from.label",
                defaultMessage: "Photos from Unsplash",
              })}
            </span>
          </div>
        </>
      )}
    </>
  );
}

ColorImagesModal.defaultProps = {
  handleColorSelect: () => {},
  handleImageSelect: () => {},
  handleClickStatusSelect: () => {},
  handleClickAway: e => {},
  useProjectStatusColor: false,
  viewMode: "new",
  modal: true,
};

export default compose(
  injectIntl,
  withStyles(styles, { withTheme: true }),
  connect(null, {
    attachFeatureImg,
  })
)(ColorImagesModal);
