// @flow

import React, { useState } from "react";
import styles from "./styles.js";
import { withStyles } from "@material-ui/core/styles";
import tempBG from "../../assets/images/temporary.png";
import classNames from "classnames";

type TileProps = {
  imageUrl: String,
  title: String,
  classes: Object,
  item: Object,
  handleClick: Function,
};

const Tile = (props: TileProps) => {
  const { classes, title, imageUrl, item, handleClick, theme, size } = props;
  const [clicked, setClicked] = useState(false);

  return (
    <div style={{ marginBottom: 20 }}>
      <div
        key={item.id}
        onClick={() => {
          handleClick(item);
        }}
        style={{
          // background: `url(${imageUrl})`,
          background: `#eaeaea`,
          // backgroundRepeat: "no-repeat",
          // backgroundSize: "cover",
          // minHeight: 46,
          borderRadius: "4px 4px",
          // marginBottom: 12,
          cursor: "pointer",
          overflow: "hidden",
        }}>
        <img
          src={imageUrl}
          alt="L"
          style={{
            objectFit: "cover",
            height: 65,
            width: size == "large" ? 106 : 80,
            maxWidth: "100%",
            objectPosition: "center",
          }}></img>
      </div>
      <a
        href={item.links.html}
        style={{
          // position: "absolute",
          // bottom: 0,
          // right: 0,
          fontSize: 8,
          color: "#7E7E7E",
          // padding: "3px 4px",
          fontFamily: theme.typography.fontFamilyLato,
        }}
        target="_blank"
        onClick={e => e.stopPropagation()}>
        {`by ${item.user.name}`}
      </a>
    </div>
  );
};

Tile.defaultProps = {
  title: "",
  imageUrl: "",
  item: {},
  handleClick: () => {},
};
export default withStyles(styles)(Tile);
