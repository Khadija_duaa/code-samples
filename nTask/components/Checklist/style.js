const ChecklistStyles = theme => ({
  checklistHeader: {
    // display: "flex",
  },
  checklistHeaderTxtCnt: {
    display: "flex",
    alignItems: "flex-start",
    position: "relative",
    flexDirection: "column",
    '&:hover': {
      backgroundColor: "#F6F6F6",
      borderRadius: 4
    },
  },
  checklistHeading: {
    fontFamily: theme.typography.fontFamilyLato,
  },
  addIconCnt: {
    display: "flex",
  },
  checklistHeaderAction: {
    position: "absolute",
    right: 10,
    bottom: 5,
    display: "flex",
    alignItems: "center",
    display: "flex",
  },
  progressCnt: {
    width: "100%",
    marginLeft: 10,
  },
  editCnt: {
    display: "flex",
    alignItems: "flex-start",
    width: "100%",
    justifyContent: "inherit",
    marginLeft: 10,
  },
  checklistBody: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "100%",
    // minHeight: 36,
  },
  checkAllCnt: {
    display: "flex",
    margin: "15px 0",
  },
  checklistItemList: {
    listStyleType: "none",
    // margin: '0 0 0 -20px',
    padding: 0,
    "& li:last-child": {
      border: "none",
    },
  },
  checklistCntActive: {
    background: theme.palette.common.white,
    marginBottom: 8,
    padding: "0 10px",
    fontFamily: theme.typography.fontFamilyLato,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    borderRadius: 6,
  },
  checklistItemCheckedDetails: {
    display: "flex",
  },
  checklistItem: {
    fontSize: "17px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    fontFamily: theme.typography.fontFamilyLato,
    // paddingRight: 23,
    padding: "8px 0 8px 0",
    borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    // minHeight: 50,
    "&:hover": {
      // transition: "ease all 0.001s",
      //   background: theme.palette.background.light,
    },
    "&:hover $dragHandleCnt": {
      visibility: "visible",
    },
    "&:hover $emptyCheckbox": {
      display: "none",
    },
    "&:hover $unCheckedIcon": {
      display: "block",
    },
    "&:hover $editIconCnt": {
      // display: "inline !important",
      visibility: "visible",
    },
    // '&:hover $dateCnt': {
    //   display: 'block',
    // },
    // '&:hover $assigneeListCnt': {
    //   display: 'block',
    // },
    // '&:hover $actionDropDown': {
    //   width: 20,
    //   transition: 'ease width 0.2s',
    // },
  },
  checklistLabel: {
    display: "flex",
    alignItems: "center",
    marginBottom: 5,
    "& svg": {
      cursor: "pointer",
    },
  },
  checklistItemDisabled: {
    pointerEvents: "none",
    fontSize: "17px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    fontFamily: theme.typography.fontFamilyLato,
    // paddingRight: 23,
    padding: "5px 0 5px 0",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    // minHeight: 50,
    "&:hover $dragHandleCnt": {
      visibility: "visible",
    },
    "&:hover $emptyCheckbox": {
      display: "none",
    },
    "&:hover $unCheckedIcon": {
      display: "block",
    },
    // '&:hover $dateCnt': {
    //   display: 'block',
    // },
    // '&:hover $assigneeListCnt': {
    //   display: 'block',
    // },
    // '&:hover $actionDropDown': {
    //   width: 20,
    //   transition: 'ease width 0.2s',
    // },
  },
  dragHandleCnt: {
    visibility: "hidden",
  },
  dragHandle: {
    fontSize: "24px !important",
  },
  checkListItemSpacer: {
    marginLeft: 28,
  },
  checklistItemInner: {
    display: "flex",
    alignItems: "flex-start",
    fontFamily: theme.typography.fontFamilyLato,
    "& span": {
      fontSize: "12px !important",
      color: theme.palette.text.primary,
      lineHeight: 0,
      // display: 'block',
      wordWrap: "break-word",
    },
  },
  markCompleteDetails: {
    display: "flex",
    alignItems: "center",
  },
  checklistCheckboxCnt: {
    margin: "2px 0 0 0",
  },
  checkAllCheckboxCnt: {
    margin: 0,
    paddingLeft: 12,
  },
  checklistItemDetails: {
    marginLeft: 10,
    display: "inline",
    fontFamily: theme.typography.fontFamilyLato,
  },
  checklistItemDetailsName: {
    maxWidth: 100,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    fontFamily: theme.typography.fontFamilyLato,
    marginLeft: 3,
  },

  progressBar: {
    backgroundColor: theme.palette.background.contrast,
    borderRadius: 2,
  },
  greenBar: {
    backgroundColor: theme.palette.background.barGreen,
  },
  allmarkCheckedIcon: {
    fontSize: "24px !important",
  },
  checkedIcon: {
    fontSize: "18px !important",
  },
  emptyCheckbox: {
    width: 18,
    height: 18,
    background: theme.palette.background.paper,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  unCheckedIcon: {
    fontSize: "18px !important",
    display: "none",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  emptyDate: {
    display: "flex",
    // border: `2px dashed ${theme.palette.border.lightBorder}`,
    // borderRadius: 18,
    // padding: 5,
    textDecoration: "underline",
  },
  addSelectionDate: {
    display: "flex",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 18,
    padding: 5,
  },
  selectionDate: {
    display: "flex",
    // textDecoration: 'underline',
  },
  actionCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  actionCntCmplete: {
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  // dateCnt: {
  // display: 'none',
  // },
  // dateCntSelected: {
  // pointerEvents: 'none',
  // },
  dateExpire: {
    color: theme.palette.text.danger,
    minWidth: 48,
    textAlign: "center",
  },
  addAssigneeList: {
    marginLeft: 10,
  },
  assigneeListCnt: {
    // display: 'none',
    marginLeft: 9,
  },
  // assigneeListCntSelected: {
  //   pointerEvents: 'none',
  //   marginLeft: 9,
  // },
  actionDropDown: {
    // width: 20,
    // overflow: "hidden",
  },
  // actionDropDownSelected: {
  //   pointerEvents: 'none',
  //   width: 20,
  //   overflow: "hidden"
  // },
  outlinedInputCnt: {
    marginRight: 15,
  },
  outlinedTaskInput: {
    padding: "10px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
    fontSize: "22px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  notchedOutlineCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  outlineInputFocus: {
    "& $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: 0,
    },
    "& $notchedOutlineAMCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  StaticDatePickerPopper: {
    zIndex: 6,
    left: "0px !important",
  },
  checklistItemTxt: {
    // overflow: "hidden",
    // whiteSpace: "nowrap",
    lineHeight: "normal !important",
    // textOverflow: "ellipsis",
    maxWidth: 580,
    fontFamily: theme.typography.fontFamilyLato,
  },
  checklistNameCnt: {
    display: "flex",
    // height: 20,
  },
  editIcon: {
    fontSize: "11px !important",
    marginTop: -11,
    marginLeft: 5,
    color: "#7e7e7e",
  },
  editIconCnt: {
    visibility: "hidden",
  },
  addIcon: {
    marginBottom: -8,
  },
  notchedOutlineClasses: {
    borderTop: "none !important",
    borderLeft: "none !important",
    borderRight: "none !important",
  },
  hideOptBtn: {
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightRegular,
    textDecoration: "underline",
    color: `${theme.palette.border.blue}`,
    cursor: "pointer",
  },
  outlinedInputCnt: {
    height: 36,
  },
  outlinedInputsmall: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightMedium,
    padding: "0px 0px 1px 5px",
  },
});
export default ChecklistStyles;
