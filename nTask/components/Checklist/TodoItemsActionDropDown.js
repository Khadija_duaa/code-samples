import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./style";
import menuStyles from "../../assets/jss/components/menu";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import CustomIconButton from "../Buttons/IconButton";
import SelectionMenu from "../Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { canDelete } from "../../Views/Task/permissions";
import { GetPermission } from "../permissions";

class TodoItemsActionDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: ""
    };
  }
  handleOperations = (event, value) => {
    event.stopPropagation();
    switch (value) {
      case "Edit":
        this.props.EditItem();
        this.setState({ open: false });
        break;
      case "Delete":
        this.props.DeleteItem();
        this.setState({ open: false });
        break;
    }
  };
  handleClose = event => {
    this.setState({ open: false });
  };

  handleClick = (event, placement) => {
    event.stopPropagation();
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  };

  render() {
    const {
      classes,
      theme,
      isArchivedSelected,
      currentTask,
      permission,
      taskPer
    } = this.props;
    const { open, placement } = this.state;

    const ddData = [];
    if (taskPer.taskDetail.toDoList.isAllowEdit) {
      ddData.push("Edit");
    }
    if (taskPer.taskDetail.toDoList.isAllowDelete) {
      ddData.push("Delete");
    }

    if (ddData.length > 0) {
      return (
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={event => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}
            >
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px" }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List>
                  <ListItem
                    disableRipple={true}
                    classes={{ root: classes.menuHeadingItem }}
                  >
                    <ListItemText
                      primary="Select Action"
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>

                  {!isArchivedSelected &&
                    taskPer.taskDetail.toDoList.isAllowEdit ? (
                    <ListItem
                      key={"Edit"}
                      button
                      disableRipple={true}
                      classes={{ selected: classes.statusMenuItemSelected }}
                      onClick={e => this.handleOperations(e, "Edit")}
                    >
                      <ListItemText
                        primary={"Edit"}
                        classes={{
                          primary: classes.statusItemText
                        }}
                      />
                    </ListItem>
                  ) : null}
                  {!isArchivedSelected &&
                    taskPer.taskDetail.toDoList.isAllowDelete ? (
                    <ListItem
                      key={"Delete"}
                      button
                      disableRipple={true}
                      classes={{ selected: classes.statusMenuItemSelected }}
                      onClick={e => this.handleOperations(e, "Delete")}
                    >
                      <ListItemText
                        primary={"Delete"}
                        classes={{
                          primary: classes.statusItemText
                        }}
                      />
                    </ListItem>
                  ) : null}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
      );
    } else return null;
  }
}

export default compose(
  withRouter,
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true
  }),
  connect(null, {})
)(TodoItemsActionDropDown);
