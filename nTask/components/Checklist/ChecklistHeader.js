import React, { useState, useRef } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import moment from "moment";
import PropTypes from "prop-types";
import ChecklistStyles from "./style";
import { validateTitleField } from "../../utils/formValidations";
import DefaultTextField from "../Form/TextField";
import AssigneeDropdown from "../Dropdown/AssigneeDropdown";
import CustomButton from "../Buttons/CustomButton";
import CustomDatePicker from "../DatePicker/DatePicker/DatePicker";
import AddIcon from "@material-ui/icons/Add";
import CustomIconButton from "../Buttons/CustomIconButton";
import debounce from "lodash/debounce";

function ChecklistHeader(props) {
  const [checklistInputError, setChecklistInputError] = useState(false);
  const [checklistInputErrorMessage, setChecklistInputErrorMessage] = useState(false);
  const [checklistInput, setChecklistInput] = useState("");
  const [endDate, setEndDate] = useState("");
  const [assignee, setAssignee] = useState([]);
  const [addChecklist, setAddChecklist] = useState(false);
  const [btnAddQuery, setBtnAddQuery] = useState("");
  let checklistInputRef = useRef(null);
  const {
    classes,
    theme,
    currentTask,
    closeTaskDetailsPopUp,
    isArchivedSelected,
    checklistItemsArr,
    style = {},
    obj,
    expand,
    setExpand,
    permission,
    groupType,
    intl,
  } = props;

  const handleItemNameInput = event => {
    setChecklistInput(event.target.value);
    setChecklistInputError(false);
    setChecklistInputErrorMessage("");
    if (props.changeStatus && props.clearStatusChanged) {
      props.clearStatusChanged();
    }
  };
  const enterKeyHandler = event => {
    if (event.key === "Enter") {
      setBtnAddQuery("progress");
      addNewItem();
    }
  };
  const addBtnHandler = () => {
    setBtnAddQuery("progress");
    addNewItem();
  };
  const addNewItem = debounce(event => {
    const validationObj = validateTitleField("to-do list item.", checklistInput, true, 250);
    if (validationObj.isError) {
      setChecklistInputError(validationObj.isError);
      setChecklistInputErrorMessage(validationObj.message);
      setBtnAddQuery("");
      checklistInputRef.current.focus();
      return;
    }
    const newItem = {
      groupId: obj.groupId,
      fieldId: obj.fieldId,
      groupType: groupType,
      checkEntry: {
        // taskId: props.currentTask.id,
        description: checklistInput.trim(),
        isComplete: false,
        completedDate: "",
        endDate: moment(endDate).format("YYYY-MM-DD"),
        completedBy: "",
        assignee: assignee.map(ass => ass.userId),
      },
    };
    if (props.updateItem)
      props.updateItem(newItem, "addItem", response => {
        setChecklistInput("");
        setChecklistInputError(false);
        setChecklistInputErrorMessage("");
        setAssignee([]);
        setEndDate("");
        setBtnAddQuery("");
        checklistInputRef.current.focus();
      });
  }, 300);
  const updateAssignee = (assignedTo, currentTask) => {
    setAssignee(assignedTo);
  };

  const handleDateSave = startDate => {
    setEndDate(startDate);
  };

  const handleShowHideInput = value => {
    // Show/Hide Input on click
    if (value == false && checklistInput) return; // This will run incase user entered any value in input and try to focus out to hide the input
    setAddChecklist(value);
  };

  const addPermission = permission ? !permission.isAllowUpdate : false;

  const btnStyles = checklistItemsArr.length
    ? {
        display: "block",
        width: "100%",
        textAlign: "left",
        borderTop: "none",
        borderRight: "none",
        borderLeft: "none",
        borderRadius: 0,
        textTransform: "none",
      }
    : {
        display: "block",
        width: "100%",
        textAlign: "left",
        textTransform: "none",
      };

  return (
    <div className={classes.checklistHeader}>
      {false ? (
        <CustomButton
          btnType="white"
          variant="contained"
          style={btnStyles}
          onClick={event => handleShowHideInput(true)}
          disabled={isArchivedSelected || addPermission}>
          <AddIcon className={classes.addIcon} htmlColor={theme.palette.secondary.light} />{" "}
          <span>Add a checklist list item</span>
        </CustomButton>
      ) : (
        <>
          <div className={classes.checklistHeaderTxtCnt}>
            {/* <div className={classes.checklistLabel}>
              <CustomIconButton btnType="transparent">
                {!expand ? (
                  <ExpandLess onClick={() => setExpand(!expand)} />
                ) : (
                  <ExpandMore onClick={() => setExpand(!expand)} />
                )}
              </CustomIconButton>
              <Typography variant="h5" className={classes.checklistHeading}>
                {obj.fieldName}
              </Typography>
            </div> */}
            {/* {expand && ( */}
            <>
              <DefaultTextField
                label=""
                fullWidth
                borderBottom={checklistItemsArr.length ? true : false}
                noRightBorder={false}
                noLeftBorder={false}
                outlineCnt={false}
                formControlStyles={{ marginBottom: 0 }}
                error={checklistInputError ? true : false}
                errorState={checklistInputError}
                errorMessage={checklistInputErrorMessage}
                defaultProps={{
                  id: "checklistNameInput",
                  onChange: handleItemNameInput,
                  onKeyUp: enterKeyHandler,
                  onBlur: event => handleShowHideInput(false),
                  value: checklistInput,
                  placeholder: `Add ${obj.fieldName}`,
                  autoFocus: true,
                  // inputProps: { maxLength: 80 },
                  style: style,
                  disabled: isArchivedSelected || addPermission || btnAddQuery === "progress",
                  autoComplete: "off",
                  inputRef: (node) => {
                    checklistInputRef.current = node;
                  },
                  // disabled: {isArchivedSelected}
                }}
                transparentInput={true}
                customInputClass={{
                  input: classes.outlinedInputsmall,
                  root: classes.outlinedInputCnt,
                }}
              />
              {checklistInput ? (
                <div className={classes.checklistHeaderAction}>
                  <CustomDatePicker
                    date={endDate}
                    placeholder="Due Date"
                    dateFormat="MMM DD, YYYY"
                    PopperProps={{ size: null }}
                    onSelect={date => handleDateSave(date)}
                  />
                  <div className={classes.addAssigneeList}>
                    <AssigneeDropdown
                      closeTaskDetailsPopUp={closeTaskDetailsPopUp}
                      assignedTo={assignee}
                      updateAction={updateAssignee}
                      isArchivedSelected={currentTask.isDeleted}
                      obj={currentTask}
                      singleSelect
                      buttonVariant="small"
                      avatarSize={"small26"}
                      disabled={addPermission}
                    />
                  </div>
                  <div style={{ marginLeft: 4 }}>
                    <CustomIconButton
                      onClick={addBtnHandler}
                      btnType="blueOutlined"
                      variant="contained"
                      disabled={
                        isArchivedSelected ||
                        addPermission ||
                        checklistInput.length < 1 ||
                        btnAddQuery == "progress"
                      }>
                      <AddIcon
                        style={{ fontSize: "20px" }}
                        htmlColor={theme.palette.text.brightBlue}
                      />
                    </CustomIconButton>
                  </div>
                </div>
              ) : null}
            </>
          </div>
        </>
      )}
    </div>
  );
}
ChecklistHeader.defaultProps = {
  currentTask: {},
  updateItem(e) {
    return e;
  },
  isArchivedSelected: false,
  closeTaskDetailsPopUp(e) {
    return e;
  },
  permission: {},
  groupType: "",
};
ChecklistHeader.propTypes = {
  currentTask: PropTypes.object.isRequired,
  updateItem: PropTypes.func.isRequired,
  isArchivedSelected: PropTypes.bool.isRequired,
  closeTaskDetailsPopUp: PropTypes.func.isRequired,
};
const mapStateToProps = state => {
  return {};
};
export default compose(
  withRouter,
  connect(mapStateToProps, {}),
  withStyles(ChecklistStyles, { withTheme: true })
)(ChecklistHeader);
