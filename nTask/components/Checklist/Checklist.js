import React, { useEffect, useState } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter} from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import ChecklistStyles from "./style";
import ChecklistHeader from "./ChecklistHeader";
import ChecklistItems from "./ChecklistItems";

import {
  SaveTodoList,
  CheckTodoItem,
  CheckTodoItemInStore,
  DeleteToDoList,
  CheckAllTodoList,
  UpdateToDoListOrder,
  UpdateToDoListOrderInStore,
  UpdateTask,
  UpdateTaskProgressBar,
} from "../../redux/actions/tasks";
import {
  createChecklistItem,
  getChecklistItems,
  editChecklistItems,
  deleteChecklistItems,
  markAllChecklistItems
} from "../../redux/actions/customFields";

function Checklist(props) {
  const [checklist, setChecklist] = useState([]);
  const [expand, setExpand] = useState(true)
  useEffect(() => {
    getChecklistItems(
      obj,
      //Success Callback
      res => {
        setChecklist(res);
      }
    );
  }, [obj]);
  const updateItem = (item, mode, callback, itemObj) => {
    switch (mode) {
      case "addItem":
        props.createChecklistItem(
          item,
          response => {
            callback(response);
            const updatedChecklist = [...checklist];
            updatedChecklist.unshift(response);
            setChecklist(updatedChecklist);
            // props.updateTodoItemsInProjectTaskTab(updatedChecklist);
          },
          () => {}
        );
        break;
      case "checkUncheckItem":
        // const selectedItem = { ...item };
        // const items = [...props.checklistItems];
        // const updatedCheckItemsList = items.map((c) => {
        //   /** updating checklist array  */
        //   if (c.id == selectedItem.id) return selectedItem;
        //   return c;
        // });
        // props.updateTodoItems(updatedCheckItemsList);

        let changeItem = { ...item };
        if (changeItem.isComplete) {
          changeItem.completedDate = null;
          changeItem.completedBy = null;
          changeItem.isComplete = false;
        } else {
          changeItem.completedDate = new Date(); //new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString();
          changeItem.completedBy = props.user.userId;
          changeItem.isComplete = true;
        }
        changeItem.updatedDate = new Date(); //new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString(),
        changeItem.updatedBy = props.user.userId;
        changeItem.version = changeItem.version + 1;

        const itemList = [...checklist].map(c => {
          /** updating checklist array  */
          if (c.id == changeItem.id) {
            return changeItem;
          } else {
            return c;
          }
        });
        setChecklist(itemList);
        editChecklistItems(
          { fieldId: obj.fieldId, groupId: obj.groupId, checkEntry: changeItem, groupType: props.groupType },
          itemObj,
          () => {}
        );
        // props.updateTodoItemsInProjectTaskTab(itemList);

        break;
      case "editItem":
        editChecklistItems(
          { fieldId: obj.fieldId, groupId: obj.groupId, checkEntry: item, groupType: props.groupType },
          itemObj,
          response => {
            callback(response);
            const items = [...checklist];
            const updatedChecklist = items.map(c => {
              /** updating checklist array  */
              if (c.id == response.id) {
                return response;
              } else {
                return c;
              }
            });
            setChecklist(updatedChecklist);
            // props.updateTodoItemsInProjectTaskTab(updatedChecklist);
          },
          error => {}
        );
        break;
      case "deleteItem":
        const forDeleteitems = [...checklist];
        const deleteUpdatedChecklist = forDeleteitems.filter(x => x.id !== item.id);
        const deleteObj = { fieldId: obj.fieldId, groupId: obj.groupId, groupType: props.groupType };
        // props.updateTodoItems(deleteUpdatedChecklist);
        setChecklist(deleteUpdatedChecklist);
        deleteChecklistItems(deleteObj, item.id, response => {
          if (response && response.status === 200) {
            callback(response);

            // props.updateTodoItemsInProjectTaskTab(deleteUpdatedChecklist);
            // updateTaskProgress(deleteUpdatedChecklist);
          }
        });
        break;
      case "reorderItems":
        const ids = item.map(x => x.id);
        const data = {
          taskId: props.currentTask.id,
          itemIds: ids,
          groupType: props.groupType
        };
        // const updatedChecklist = [...item];
        // props.updateTodoItems(updatedChecklist);
        props.UpdateToDoListOrderInStore(data, response => {
          if (response && response.status === 200) {
            callback(response);
          }
        });
        props.UpdateToDoListOrder(data, response => {
          if (response && response.status === 200) {
            callback(response);
            // updateTaskProgress(updatedChecklist);
            // const updatedChecklist = [...item];
            // props.updateTodoItems(updatedChecklist);
          }
        });
        break;
      case "checkMarkAll":
        const { user } = props;
        const checkData = {
          groupId: props.obj.groupId,
          fieldId: props.obj.fieldId,
          groupType: props.groupType,
          isComplete: true
        }
        markAllChecklistItems(
          checkData,
          response => {
            // const { user } = props;
           
              // const updatedTodoList = props.checklistItems.map((x) => {
              //   x.completedDate = x.isComplete ? x.completedDate : moment();
              //   x.completedBy = x.isComplete ? x.completedBy : user.userId;
              //   x.updatedDate = x.isComplete ? x.updatedDate : moment();
              //   x.updatedBy = x.isComplete ? x.updatedBy : user.userId;
              //   x.isComplete = true;
              //   return x;
              // });
              // props.updateTodoItems(updatedTodoList);
              setChecklist(response.fieldData.data);
              callback();
              // updateTaskProgress(updatedTodoList);
   
          },
          () => {},
          user
        );
        break;
      case "uncheckMarkAll":
        const uncheckData = {
          taskId: props.currentTask.id,
          checkAll: false,
          groupType: props.groupType
        };
        props.CheckAllTodoList(
          uncheckData,
          response => {
            if (response && response.status === 200) {
              callback();
              // const updatedTodoList = props.checklistItems.map((x) => {
              //   x.completedDate = null;
              //   x.completedBy = null;
              //   x.isComplete = false;
              //   return x;
              // });
              // props.updateTodoItems(updatedTodoList);
              // updateTaskProgress(updatedTodoList);
            }
          },
          () => {},
          null
        );
        break;
    }
  };

  const updateTaskProgress = totalItems => {
    // const totalItems = props.checklistItems.length;
    const cmpltdItems = totalItems.filter(item => item.isComplete).length;
    const progressPercent = Math.round((cmpltdItems / totalItems.length) * 100) || 0;
    const { currentTask } = props;

    const newTaskObj = { ...currentTask, progress: progressPercent };

    props.UpdateTaskProgressBar(newTaskObj, () => {});
  };

  const {
    currentTask = {},
    isArchivedSelected,
    closeTaskDetailsPopUp,
    permission,
    checklistItems = [],
    classes,
    progressBar,
    draggable,
    style = {},
    intl,
    obj,
    handleClickHideOption,
    groupType
  } = props;
  
  return (
    <div className={checklistItems.length ? classes.checklistCntActive : null} style={{flex: 1, margin: "0px 3px 0px 3px"}}>
      <ChecklistHeader
        currentTask={currentTask}
        obj={obj}
        updateItem={updateItem}
        closeTaskDetailsPopUp={closeTaskDetailsPopUp}
        isArchivedSelected={isArchivedSelected}
        permission={permission}
        checklistItemsArr={checklistItems}
        style={style}
        intl={intl}
        expand={expand}
        setExpand={setExpand}
        groupType={groupType}
      />
      {expand &&
      <ChecklistItems
        checklistItemsArr={checklist}
        currentTask={currentTask}
        updateItem={updateItem}
        isArchivedSelected={isArchivedSelected}
        closeTaskDetailsPopUp={closeTaskDetailsPopUp}
        permission={permission}
        progressBar={progressBar}
        draggable={draggable}
        obj={obj}
        handleClickHideOption={handleClickHideOption}
        groupType={groupType}
      />}
    </div>
  );
}
Checklist.defaultProps = {
  checklistItems: [],
  permission: {},
  isArchivedSelected: false,
  currentTask(e) {
    return e;
  },
  closeTaskDetailsPopUp(e) {
    return e;
  },
  handleClickHideOption(e) {
    return e;
  },
  // updateTodoItems(e) {
  //   return e;
  // },
  SaveTodoList(e) {
    return e;
  },
  DeleteToDoList(e) {
    return e;
  },
  CheckAllTodoList(e) {
    return e;
  },
  UpdateToDoListOrder(e) {
    return e;
  },
  UpdateToDoListOrderInStore(e) {
    return e;
  },
};
Checklist.propTypes = {
  checklistItems: PropTypes.array.isRequired,
  isArchivedSelected: PropTypes.bool.isRequired,
  currentTask: PropTypes.func.isRequired,
  closeTaskDetailsPopUp: PropTypes.func.isRequired,
  // updateTodoItems: PropTypes.func.isRequired,
  SaveTodoList: PropTypes.func.isRequired,
  DeleteToDoList: PropTypes.func.isRequired,
  CheckAllTodoList: PropTypes.func.isRequired,
  UpdateToDoListOrder: PropTypes.func.isRequired,
  UpdateToDoListOrderInStore: PropTypes.func.isRequired,
};

Checklist.defaultProps = {
  closeTaskDetailsPopUp: () => {},
  permission: {},
  progressBar: true,
  draggable: true,
  groupType:""
  // updateTodoItemsInProjectTaskTab: () => {},
};
const mapStateToProps = state => {
  return {
    user: state.profile.data,
  };
};
export default compose(
  withRouter,
  connect(mapStateToProps, {
    SaveTodoList,
    CheckTodoItem,
    CheckTodoItemInStore,
    DeleteToDoList,
    CheckAllTodoList,
    UpdateToDoListOrder,
    UpdateToDoListOrderInStore,
    UpdateTask,
    createChecklistItem,
    UpdateTaskProgressBar,
  }),
  withStyles(ChecklistStyles, { withTheme: true })
)(Checklist);
