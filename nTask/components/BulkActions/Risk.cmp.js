import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import BulkActionsStyle from "./BulkActions.style.js";
import IconPeopleOutLine from "../Icons/IconPeopleOutLine.js";
import IconPercent from "../Icons/IconPercent";
import IconColorOutLine from "../Icons/IconColorOutLine.js";
import IconExportOutLine from "../Icons/IconExportOutLine.js";
import IconArchiveOutLine from "../Icons/IconArchiveOutLine.js";
import IconDeleteOutLine from "../Icons/IconDeleteOutLine.js";
import IconCloseOutLine from "../Icons/IconCloseOutLine.js";
import AssigneeDropdown from "../Dropdown/AssigneeDropdown2";
import RoundIconOutline from "@material-ui/icons/Brightness1Outlined";
import RoundIcon from "@material-ui/icons/Brightness1";

import StatusDropdown from "../Dropdown/StatusDropdown/Dropdown";
import ColorPickerDropdown from "../Dropdown/ColorPicker/Dropdown";
import { useDispatch, useSelector } from "react-redux";
import { FormattedMessage, injectIntl } from "react-intl";
import { compose } from "redux";
import { impactData, likelihoodData, statusData } from "../../helper/riskDropdownData";
import { setDeleteDialogueState } from "../../redux/actions/allDialogs";
import { withSnackbar } from "notistack";
import {
  updateBulkRisk,
  exportBulkRisk,
  deleteBulkRisk,
  archiveBulkRisk,
  unArchiveBulkRisk,
} from "../../redux/actions/risks";
import fileDownload from "js-file-download";
import { CanAccessFeature } from "../AccessFeature/AccessFeature.cmp.js";
import ActionConfirmation from "../Dialog/ConfirmationDialogs/ActionConfirmation.js";
import { addPendingHandler } from "../../redux/actions/backProcesses.js";

const Riskcmp = ({ classes, clearSelection, selectedRisks, theme, intl, enqueueSnackbar }) => {
  const [assigneeEl, setAssigneeEl] = useState(null);
  const [priorityEl, setPriorityEl] = useState(null);
  const [likelihoodEl, seLikelihoodEl] = useState(null);
  const [statusEl, setStatusEl] = useState(null);
  const [colorEl, setColorEl] = useState(null);
  const { quickFilters } = useSelector(state => {
    return {
      quickFilters: state.risks.quickFilters || {},
    };
  });
  const dispatch = useDispatch();
  // handle bulk update assignee
  const updateAssignee = param => {
    const postObj = { riskOwner: param.userId, riskIds: getSelectedRisks(), type: "array" };
    setAssigneeEl(null);
    updateBulkRisk(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} risks owner updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectAssignee = e => {
    e.stopPropagation();
    setAssigneeEl(e.currentTarget);
  };
  const handleCloseAssignee = () => {
    setAssigneeEl(null);
  };
  // Priority
  const updateImpact = props => {
    const postObj = { impact: props.value, riskIds: getSelectedRisks() };
    setPriorityEl(null);
    updateBulkRisk(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} risks impact updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  // Priority
  const updateLikelihood = props => {
    const postObj = { likelihood: props.value, riskIds: getSelectedRisks() };
    seLikelihoodEl(null);
    updateBulkRisk(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} risks likelihood updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectImpact = e => {
    e.stopPropagation();
    setPriorityEl(e.currentTarget);
  };
  const handleSelectLikelihood = e => {
    e.stopPropagation();
    seLikelihoodEl(e.currentTarget);
  };
  const handleCloseImpact = () => {
    setPriorityEl(null);
  };
  const handleCloseLikelihood = () => {
    seLikelihoodEl(null);
  };
  const handleSelectStatus = e => {
    e.stopPropagation();
    setStatusEl(e.currentTarget);
  };
  const handleCloseStatus = () => {
    setStatusEl(null);
  };
  const updateStatus = props => {
    const postObj = { status: props.label, riskIds: getSelectedRisks() };
    setStatusEl(null);
    updateBulkRisk(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} risks status updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };

  // Color
  const updateColor = color => {
    const postObj = { colorCode: color, riskIds: getSelectedRisks() };
    setColorEl(null);
    updateBulkRisk(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} risks color updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectColor = e => {
    e.stopPropagation();
    setColorEl(e.currentTarget);
  };
  const handleCloseColor = () => {
    setColorEl(null);
  };
  // get taskids use map
  const getSelectedRisks = () => {
    return selectedRisks.map(item => {
      return item.data.id;
    });
  };
  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };
  // export api function
  const handleExportRisk = props => {
    const postObj = { riskIds: getSelectedRisks() };
    const obj = {
      type: 'risks',
      apiType: 'post',
      data: postObj,
      fileName: 'risks.xlsx',
      apiEndpoint: 'api/risks/exportbulk',
    }
    addPendingHandler(obj, dispatch);   

    // exportBulkRisk(postObj, dispatch,
    //   res => {
    //     fileDownload(res.data, "risks.xlsx");
    //     showSnackBar(`${selectedRisks.length} risks exported successfully`, "success");
    //   },
    //   (err) => {
    //     if (err.response.status == 405) {
    //       showSnackBar("You don't have sufficient rights to export risks", "error");
    //     } else {
    //       showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //     }
    //   });
  };
  // delete api function
  const handleDeleteRisk = props => {
    dispatch(setDeleteDialogueState({ btnQuery: "progress" }));
    const postObj = { riskIds: getSelectedRisks() };
    deleteBulkRisk(
      postObj,
      dispatch,
      res => {
        // Success
        closeDeleteConfirmation();
        showSnackBar(`${res.entity.length} risks deleted successfully`, "success");
        clearSelection();
      },
      (err) => {
        // failure
        closeDeleteConfirmation();
        showSnackBar("You don't have sufficient rights to delete risks", "error");
      }
    );
  };
  //Confirmation Dialog close
  const closeDeleteConfirmation = () => {
    dispatch(setDeleteDialogueState({ open: false, btnQuery: "" }));
  };
  //Open delete confirmation dialog
  const handleDeleteConfirmation = success => {
    const dialogObj = {
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: `Are you sure you want to delete these ${selectedRisks.length} risks?`,
      successAction: handleDeleteRisk,
      closeAction: closeDeleteConfirmation,
      btnQuery: "",
    };
    dispatch(setDeleteDialogueState(dialogObj));
  };
  // delete api function
  const handleArchiveRisk = props => {
    const postObj = { riskIds: getSelectedRisks() };
    setArchiveBtnQuery('progress');
    archiveBulkRisk(
      postObj,
      dispatch,
      res => {
        //Success
        setArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(`${res.length} risks archived successfully`, "success");
        clearSelection();
      },
      () => {
        // Failure
        setArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleUnArchiveRisk = props => {
    const postObj = { riskIds: getSelectedRisks() };
    setUnArchiveBtnQuery('progress');
    unArchiveBulkRisk(
      postObj,
      dispatch,
      res => {
        //Success
        setUnArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(`${res.length} risks unarchived successfully`, "success");
        clearSelection();
      },
      () => {
        // Failure
        setUnArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };

  const [showConfirmation, setShowConfirmation] = useState(false)
  const [inputType, setInputType] = useState('')
  const [archiveBtnQuery, setArchiveBtnQuery] = useState('')
  const [unarchiveBtnQuery, setUnArchiveBtnQuery] = useState('')
  const handleArchiveConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Archive");
  };
  const handleUnArchiveConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Unarchive");
  };
  const closeConfirmationPopup = () => {
    setShowConfirmation(false)
    setStep(0)
  }

  const riskImpactData = impactData(theme, classes, intl);
  const riskLikelihoodData = likelihoodData(intl);
  const riskStatusData = statusData(theme, classes, intl);
  const isArchived = quickFilters["Archived"] ? true : false;
  return (
    <>
      <div className={classes.mainTaskCmp}>
        <ul className={classes.bulkActionBtnsList}>
          <li className={classes.selectedTasks}>
            <span>{selectedRisks && selectedRisks.length}</span>Risks selected
          </li>
          {!isArchived && (
            <>
              {/* risk owner */}
              <CanAccessFeature group='risk' feature='riskOwner'>
                <li className={classes.taskListItems}>
                  <AssigneeDropdown
                    updateAction={updateAssignee}
                    obj={{}}
                    showSelected={false}
                    singleSelect={true}
                    handleCloseAssignee={handleCloseAssignee}
                    customBtnRender={
                      <div className={classes.renderTask} onClick={handleSelectAssignee}>
                        <span className={classes.taskCmpIcon}>
                          <IconPeopleOutLine />
                        </span>
                        Risk Owner
                      </div>
                    }
                    anchorEl={assigneeEl}
                  />
                </li>
              </CanAccessFeature>
              {/* impact */}
              <CanAccessFeature group='risk' feature='impact'>
                <li className={classes.taskListItems}>
                  <StatusDropdown
                    onSelect={updateImpact}
                    obj={{}}
                    singleSelect={true}
                    options={riskImpactData}
                    handleCloseCallback={handleCloseImpact}
                    customStatusBtnRender={
                      <div onClick={handleSelectImpact} className={classes.renderTask}>
                        <span className={classes.taskCmpIcon}>
                          <RoundIconOutline />
                        </span>
                        Impact
                      </div>
                    }
                    dropdownProps={{
                      placement: "top-start",
                    }}
                    anchorEl={priorityEl}
                  />
                </li>
              </CanAccessFeature>
              {/* likelihood */}
              <CanAccessFeature group='risk' feature='likelihood'>
                <li className={classes.taskListItems}>
                  <StatusDropdown
                    onSelect={updateLikelihood}
                    obj={{}}
                    singleSelect={true}
                    options={riskLikelihoodData}
                    handleCloseCallback={handleCloseLikelihood}
                    customStatusBtnRender={
                      <div onClick={handleSelectLikelihood} className={classes.renderTask}>
                        <span className={classes.taskCmpIcon}>
                          <IconPercent />
                        </span>
                        Likelihood
                      </div>
                    }
                    dropdownProps={{
                      placement: "top-start",
                    }}
                    anchorEl={likelihoodEl}
                  />
                </li>
              </CanAccessFeature>
              {/* color */}
              <li className={classes.taskListItems}>
                <ColorPickerDropdown
                  onSelect={updateColor}
                  obj={{}}
                  singleSelect={true}
                  handleCloseColor={handleCloseColor}
                  customColorBtnRender={
                    <div onClick={handleSelectColor} className={classes.renderTask}>
                      <span className={classes.taskCmpIcon}>
                        <IconColorOutLine />
                      </span>
                      Color
                    </div>
                  }
                  anchorEl={colorEl}
                />
              </li>
              {/* status */}
              <CanAccessFeature group='risk' feature='status'>
                <li className={classes.taskListItems}>
                  <StatusDropdown
                    onSelect={updateStatus}
                    obj={{}}
                    singleSelect={true}
                    options={riskStatusData}
                    handleCloseCallback={handleCloseStatus}
                    customStatusBtnRender={
                      <div onClick={handleSelectStatus} className={classes.renderTask}>
                        <span className={classes.taskCmpIcon}>
                          <RoundIcon />
                        </span>
                        Status
                      </div>
                    }
                    dropdownProps={{
                      placement: "top-start",
                    }}
                    anchorEl={statusEl}
                  />
                </li>
              </CanAccessFeature>
              <li className={classes.taskListItems} onClick={handleExportRisk}>
                <span className={classes.taskCmpIcon}>
                  <IconExportOutLine />
                </span>
                Export
              </li>
              <li className={classes.taskListItems} onClick={handleArchiveConfirmation}>
                <span className={classes.taskCmpIcon}>
                  <IconArchiveOutLine />
                </span>
                Archive
              </li>
            </>
          )}
          {isArchived && (
            <li className={classes.taskListItems} onClick={handleUnArchiveConfirmation}>
              <span className={classes.taskCmpIcon}>
                <IconArchiveOutLine />
              </span>
              UnArchive
            </li>
          )}
          <li className={classes.taskDeleteIcon} onClick={handleDeleteConfirmation}>
            <span className={classes.taskCmpIcon}>
              <IconDeleteOutLine />
            </span>
            Delete
          </li>
          <li className={classes.taskCrossIcon} onClick={clearSelection}>
            <span className={classes.iconClose}>
              <IconCloseOutLine />
            </span>
          </li>
        </ul>
      </div>
      {inputType === "Archive" /* Confirmation modal for Archive the bulk tasks  */ ? (
        <>
          <ActionConfirmation
            open={showConfirmation}
            closeAction={closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.risk.label2"
                defaultMessage="Archive risks"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.risk.message"
                defaultMessage={`Are you sure you want to archive these ${selectedRisks.length} risks?`}
                values={{ l: selectedRisks.length }}
              />
            }
            successAction={handleArchiveRisk}
            btnQuery={archiveBtnQuery}
          />
        </>
      ) : inputType === "Unarchive" /* Confirmation modal for Unarchive the bulk tasks  */ ? (
        <>
          <ActionConfirmation
            open={showConfirmation}
            closeAction={closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.risk.label"
                defaultMessage="Unarchive risks"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            msgText={
              <FormattedMessage
                id="common.un-archived.risk.message"
                defaultMessage={`Are you sure you want to unarchive these ${selectedRisks.length} risks?`}
                values={{ l: selectedRisks.length }}
              />
            }
            successAction={handleUnArchiveRisk}
            btnQuery={unarchiveBtnQuery}
          />
        </>
      ) : null}
    </>
  );
};
export default compose(
  withSnackbar,
  injectIntl,
  withStyles(BulkActionsStyle, { withTheme: true })
)(Riskcmp);
