import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import BulkActionsStyle from "./BulkActions.style.js";
import IconPeopleOutLine from "../Icons/IconPeopleOutLine.js";
import IconStatusOutLine from "../Icons/IconStatusOutLine.js";
import IconPoriorityOutLine from "../Icons/IconPoriorityOutLine.js";
import IconFloderOutLine from "../Icons/IconFloderOutLine..js";
import IconColorOutLine from "../Icons/IconColorOutLine.js";
import IconExportOutLine from "../Icons/IconExportOutLine.js";
import IconArchiveOutLine from "../Icons/IconArchiveOutLine.js";
import IconDeleteOutLine from "../Icons/IconDeleteOutLine.js";
import IconCloseOutLine from "../Icons/IconCloseOutLine.js";
import AssigneeDropdown from "../../components/Dropdown/AssigneeDropdown2";
// import FlagDropDown from "../../Views/Task/Grid/FlagDropdown";
import StatusDropdown from "../../components/Dropdown/StatusDropdown/Dropdown";
import SearchDropdown from "../../components/Dropdown/SearchDropdown";
import ColorPickerDropdown from "../../components/Dropdown/ColorPicker/Dropdown";
import { useDispatch, useSelector } from "react-redux";
import { generateProjectData } from "../../helper/generateSelectData";
import {
  exportBulkTask,
  updateBulkTask,
  deleteBulkTask,
  archiveBulkTask,
  unArchiveBulkTask
} from "../../redux/actions/tasks.js";
import { FormattedMessage, injectIntl } from "react-intl";
import { compose } from "redux";
import { priorityData } from "../../helper/taskDropdownData";
import fileDownload from "js-file-download";
import { setDeleteDialogueState, updateDeleteDialogState } from "../../redux/actions/allDialogs";
import { withSnackbar } from "notistack";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import { CanAccess } from "../AccessFeature/AccessFeature.cmp.js";
import ActionConfirmation from "../Dialog/ConfirmationDialogs/ActionConfirmation.js";
import { addPendingHandler } from "../../redux/actions/backProcesses";

const Taskcmp = ({ classes, clearSelection, selectedTasks, theme, intl, enqueueSnackbar }) => {
  const [assigneeEl, setAssigneeEl] = useState(null);
  const [priorityEl, setPriorityEl] = useState(null);
  const [projectEl, setProjectEl] = useState(null);
  const [colorEl, setColorEl] = useState(null);
  const { projects, quickFilters, globalTimerTaskState } = useSelector(state => {
    return {
      projects: state.projects.data,
      quickFilters: state.tasks.quickFilters || {},
      globalTimerTaskState: state.globalTimerTask,
    };
  });
  const dispatch = useDispatch();
  // handle bulk update assignee
  const updateAssignee = props => {
    const postObj = { assigneeList: [props.userId], taskIds: getSelectedTasks(), type: "array" };
    setAssigneeEl(null);
    updateBulkTask(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`Tasks assignee updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectAssignee = e => {
    e.stopPropagation();
    setAssigneeEl(e.currentTarget);
  };
  const handleCloseAssignee = () => {
    setAssigneeEl(null);
  };
  // Priority
  const updatePriority = props => {
    const postObj = { priority: props.value, taskIds: getSelectedTasks() };
    setPriorityEl(null);
    updateBulkTask(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`Tasks priority updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectPriority = e => {
    e.stopPropagation();
    setPriorityEl(e.currentTarget);
  };
  const handleClosePriority = () => {
    setPriorityEl(null);
  };
  // Project
  const updateProject = props => {
    const { label, id } = props[0];
    const postObj = {
      projectId: id,
      taskIds: getSelectedTasks(),
      projectInfor: { projectId: id, projectName: label },
    };
    setProjectEl(null);
    updateBulkTask(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`${res.length} tasks project updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectProject = e => {
    e.stopPropagation();
    setProjectEl(e.currentTarget);
  };
  const handleCloseProject = () => {
    setProjectEl(null);
  };
  // Color
  const updateColor = color => {
    const postObj = { colorCode: color, taskIds: getSelectedTasks() };
    setColorEl(null);
    updateBulkTask(
      postObj,
      dispatch,
      res => {
        // Success
        showSnackBar(`Tasks color updated successfully`, "success");
      },
      () => {
        // failure
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };
  const handleSelectColor = e => {
    e.stopPropagation();
    setColorEl(e.currentTarget);
  };
  const handleCloseColor = () => {
    setColorEl(null);
  };
  // get taskids use map
  const getSelectedTasks = () => {
    return selectedTasks.map(item => {
      return item.data.taskId;
    });
  };
  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };
  // export api function
  const handleExportTask = props => {
    const postObj = { taskIds: getSelectedTasks() };
    const obj = {
      type: 'tasks',
      apiType: 'post',
      data: postObj,
      fileName: 'tasks.xlsx',
      apiEndpoint: 'api/UserTask/ExportBulkTasks',
    }
    addPendingHandler(obj, dispatch);
    
    // exportBulkTask(postObj, dispatch, res => {
    //   fileDownload(res.data, "tasks.xlsx");
    //   showSnackBar(`${selectedTasks.length} tasks exported successfully`, "success");
    // }, (err) => {
    //   if (err.response.status == 405) {
    //     showSnackBar("You don't have sufficient rights to export tasks", "error");
    //   } else {
    //     showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //   }
    // });
  };
  // delete api function
  const handleDeleteTask = props => {
    dispatch(setDeleteDialogueState({ btnQuery: "progress" }));
    const postObj = { taskIds: getSelectedTasks() };
    const task = globalTimerTaskState ? globalTimerTaskState : {};
    const startedTimerTask = selectedTasks.find(item => item.data.taskId === task.taskId);
    deleteBulkTask(
      postObj,
      dispatch,
      res => {
        // Success
        closeDeleteConfirmation();
        showSnackBar(`${res.length} tasks deleted successfully`, "success");
        clearSelection();
      },
      (err) => {
        // failure
        closeDeleteConfirmation();
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      },
      startedTimerTask
    );
  };
  //Confirmation Dialog close
  const closeDeleteConfirmation = () => {
    dispatch(setDeleteDialogueState({ open: false, btnQuery: "" }));
  };
  //Open delete confirmation dialog
  const handleDeleteConfirmation = success => {
    const dialogObj = {
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: `Are you sure you want to delete these ${selectedTasks.length} tasks?`,
      successAction: handleDeleteTask,
      closeAction: closeDeleteConfirmation,
      btnQuery: "",
    };
    dispatch(setDeleteDialogueState(dialogObj));
  };
  // delete api function
  const handleArchiveTask = props => {
    const postObj = { taskIds: getSelectedTasks() };
    const task = globalTimerTaskState ? globalTimerTaskState : {};
    const startedTimerTask = selectedTasks.find(item => item.data.taskId === task.taskId);
    setArchiveBtnQuery('progress');
    archiveBulkTask(
      postObj,
      dispatch,
      res => {
        //Success
        setArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(`${res.length} tasks archived successfully`, "success");
        clearSelection();
      },
      () => {
        // Failure
        setArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      },
      startedTimerTask
    );
  };
  const handleUnArchiveTask = props => {
    const postObj = { taskIds: getSelectedTasks() };
    setUnArchiveBtnQuery('progress');
    unArchiveBulkTask(
      postObj,
      dispatch,
      res => {
        //Success
        setUnArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(`${selectedTasks.length} tasks unarchived successfully`, "success");
        clearSelection();
      },
      () => {
        // Failure
        setUnArchiveBtnQuery('');
        setShowConfirmation(false);
        showSnackBar(
          "oops! There seems to be an issue, please contact support@ntaskmanager.com",
          "error"
        );
      }
    );
  };

  const [showConfirmation, setShowConfirmation] = useState(false)
  const [inputType, setInputType] = useState('')
  const [archiveBtnQuery, setArchiveBtnQuery] = useState('')
  const [unarchiveBtnQuery, setUnArchiveBtnQuery] = useState('')
  const handleArchiveConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Archive");
  };
  const handleUnArchiveConfirmation = success => {
    setShowConfirmation(true);
    setInputType("Unarchive");
  };
  const closeConfirmationPopup = () => {
    setShowConfirmation(false)
    setStep(0)
  }


  const projectData = generateProjectData(projects);
  const taskPriorityData = priorityData(theme, classes, intl);
  const isArchived = quickFilters["Archived"] ? true : false;
  return (
    <>
      <div className={classes.mainTaskCmp}>
        <ul className={classes.bulkActionBtnsList}>
          <li className={classes.selectedTasks}>
            <span>{selectedTasks && selectedTasks.length}</span>Tasks selected
          </li>
          {!isArchived && (
            <>
              {CanAccess({ group: 'task', feature: 'assigneeList' }) && <li className={classes.taskListItems}>
                <AssigneeDropdown
                  updateAction={updateAssignee}
                  obj={{}}
                  showSelected={false}
                  singleSelect={true}
                  handleCloseAssignee={handleCloseAssignee}
                  customBtnRender={
                    <div className={classes.renderTask} onClick={handleSelectAssignee}>
                      <span className={classes.taskCmpIcon}>
                        <IconPeopleOutLine />
                      </span>{" "}
                      Assignee
                    </div>
                  }
                  anchorEl={assigneeEl}
                />
              </li>}
              {CanAccess({ group: 'task', feature: 'priority' }) && <li className={classes.taskListItems}>
                <StatusDropdown
                  onSelect={updatePriority}
                  obj={{}}
                  singleSelect={true}
                  options={taskPriorityData}
                  handleCloseCallback={handleClosePriority}
                  customStatusBtnRender={
                    <div onClick={handleSelectPriority} className={classes.renderTask}>
                      <span className={classes.taskCmpIcon}>
                        <IconPoriorityOutLine />
                      </span>
                      Priority
                    </div>
                  }
                  dropdownProps={{
                    placement: "top-start",
                  }}
                  anchorEl={priorityEl}
                />
              </li>}
              {!teamCanView("customStatusAccess") && CanAccess({ group: 'task', feature: 'project' }) && (
                <li className={classes.taskListItems}>
                  <SearchDropdown
                    updateAction={updateProject}
                    obj={{}}
                    initSelectedOption={[]}
                    showSelected={false}
                    singleSelect={true}
                    handleCloseProject={handleCloseProject}
                    optionsList={projectData}
                    customSearchRender={
                      <div onClick={handleSelectProject} className={classes.renderTask}>
                        <span className={classes.taskCmpIcon}>
                          <IconFloderOutLine />
                        </span>
                        Project
                      </div>
                    }
                    anchorEl={projectEl}
                  />
                </li>
              )}
              <li className={classes.taskListItems}>
                <ColorPickerDropdown
                  onSelect={updateColor}
                  obj={{}}
                  singleSelect={true}
                  handleCloseColor={handleCloseColor}
                  customColorBtnRender={
                    <div onClick={handleSelectColor} className={classes.renderTask}>
                      <span className={classes.taskCmpIcon}>
                        <IconColorOutLine />
                      </span>
                      Color
                    </div>
                  }
                  anchorEl={colorEl}
                />
              </li>
              <li className={classes.taskListItems} onClick={handleExportTask}>
                <span className={classes.taskCmpIcon}>
                  <IconExportOutLine />
                </span>
                Export
              </li>
              <li className={classes.taskListItems} onClick={handleArchiveConfirmation}>
                <span className={classes.taskCmpIcon}>
                  <IconArchiveOutLine />
                </span>
                Archive
              </li>
            </>
          )}
          {isArchived && (
            <li className={classes.taskListItems} onClick={handleUnArchiveConfirmation}>
              <span className={classes.taskCmpIcon}>
                <IconArchiveOutLine />
              </span>
              UnArchive
            </li>
          )}
          <li className={classes.taskDeleteIcon} onClick={handleDeleteConfirmation}>
            <span className={classes.taskCmpIcon}>
              <IconDeleteOutLine />
            </span>
            Delete
          </li>
          <li className={classes.taskCrossIcon} onClick={clearSelection}>
            <span className={classes.iconClose}>
              <IconCloseOutLine />
            </span>
          </li>
        </ul>
      </div>
      {inputType === "Archive" /* Confirmation modal for Archive the bulk tasks  */ ? (
        <>
          <ActionConfirmation
            open={showConfirmation}
            closeAction={closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.tasks.label2"
                defaultMessage="Archive tasks"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.tasks.message"
                defaultMessage={`Are you sure you want to archive these ${selectedTasks.length} tasks?`}
                values={{ l: selectedTasks.length }}
              />
            }
            successAction={handleArchiveTask}
            btnQuery={archiveBtnQuery}
          />
        </>
      ) : inputType === "Unarchive" /* Confirmation modal for Unarchive the bulk tasks  */ ? (
        <>
          <ActionConfirmation
            open={showConfirmation}
            closeAction={closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.tasks.label"
                defaultMessage="Unarchive tasks"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            msgText={
              <FormattedMessage
                id="common.un-archived.tasks.message"
                defaultMessage={`Are you sure you want to unarchive these ${selectedTasks.length} tasks?`}
                values={{ l: selectedTasks.length }}
              />
            }
            successAction={handleUnArchiveTask}
            btnQuery={unarchiveBtnQuery}
          />
        </>
      ) : null}
    </>
  );
};
export default compose(
  withSnackbar,
  injectIntl,
  withStyles(BulkActionsStyle, { withTheme: true })
)(Taskcmp);
