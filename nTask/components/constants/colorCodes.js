const COLORCODES = [
    {key:"Color01" , value: "#ff5b5a"},
    {key:"Color02" , value: "#a1b3ff"},
    {key:"Color03" , value: "#ff985b"},
    {key:"Color04" , value: "#4fdac5"},
    {key:"Color05" , value: "#fedb43"},
    {key:"Color06" , value: "#b8b8b8"},
    {key:"Color07" , value: "#ff9c96"},
    {key:"Color08" , value: "#4e5efc"},
    {key:"Color09" , value: "#b4da6d"}
];

const STATUSCOLORS = [
    "#fc7150", // Not Started
    "#5185fc", // In Progress
    "#faa948", // In Review
    "#4dd778", // Completed
    "#ff5b5b", // Cancelled
];

export default {
    COLORCODES,
    STATUSCOLORS
}