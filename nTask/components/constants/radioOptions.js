
const JOBTITLELIST = [
    {
      "value": "Accounts and Finance",
      "label": "Accounts and Finance"
    },
    {
      "value": "Administration",
      "label": "Administration"
    },
    {
      "value": "Advisory",
      "label": "Advisory"
    },
    {
      "value": "Auditing",
      "label": "Auditing"
    },
    {
      "value": "Creative",
      "label": "Creative"
    },
    {
      "value": "Design and UI/UX",
      "label": "Design and UI/UX"
    },
    {
      "value": "Human Resources",
      "label": "Human Resources"
    },
    {
      "value": "Management",
      "label": "Management"
    },
    {
      "value": "Marketing",
      "label": "Marketing"
    },
    {
      "value": "Performance Management",
      "label": "Performance Management"
    },
    {
      "value": "Project Management",
      "label": "Project Management"
    },
    {
      "value": "Quality Control and Assurance",
      "label": "Quality Control and Assurance"
    },
    {
      "value": "Sales and Business Development",
      "label": "Sales and Business Development"
    },
    {
      "value": "Software Development",
      "label": "Software Development"
    },
    {
      "value": "Support and Customer Service",
      "label": "Support and Customer Service"
    },
    {
      "value": "Systems Administration",
      "label": "Systems Administration"
    },
    {
      "value": "Talent Acquisition",
      "label": "Talent Acquisition"
    },
    {
      "value": "Teacher",
      "label": "Teacher"
    },
    {
      "value": "Other",
      "label": "Other"
    }
  ];
  
const INDUSTRYLIST = [
    {value:"Automotive",label:"Automotive"},
    {value:"Business Services",label:"Business Services"},
    {value:"Telecom",label:"Telecom"},
    {value:"FSI",label:"FSI"},
    {value:"Education",label:"Education"},
    {value:"Public Sector",label:"Public Sector"},
    {value:"SME",label:"SME"},
    {value:"Energy",label:"Energy"},
    {value:"Retail and Manufacturing",label:"Retail and Manufacturing"},
    {value:"Realty",label:"Realty"},
    {value:"Other",label:"Other"}
    //"Real Estate / Property Management",
    //"Restaurants",
    //"Retail",
    //"Services",
    //"Telecom / Communication Services",
    //"Travel & Tourism",
    //"Wholesale"
];

const NUMBEROFMEMBERSLIST= [
    "1-5",
    "6-10",
    "11-25",
    "26-50",
    "50+"
];

const WORKSPACEUSAGE = [
    {value:"Client Services",label:"Client Services"},
    {value:"Event Management",label:"Event Management"},
    {value:"Finance",label:"Finance"},
    {value:"Human Resources",label:"Human Resources"},
    {value:"IT",label:"IT"},
    {value:"Marketing",label:"Marketing"},
    {value:"Operations",label:"Operations"},
    {value:"Project Management",label:"Project Management"},
    {value:"Sales",label:"Sales"},
    {value:"Software Development",label:"Software Development"},
    {value:"Other",label:"Other"}
];

export default {
    JOBTITLELIST,
    INDUSTRYLIST,
    NUMBEROFMEMBERSLIST,
    WORKSPACEUSAGE
};
