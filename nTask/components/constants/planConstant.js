import moment from "moment";

const FREEPLAN = 'Free';
const BASICPLAN = 'Basic';
const FREEPLAN1 = 'Free 2018';
const TRIALPLAN = 'Trial';
const PREMIUMTRIALPLAN = 'PremiumTrial';
const PREMIUMPLAN = 'Premium';
export const TRIALPERIOD = '7';

const BUSINESSTRIALPLAN = 'BusinessTrial';
const BUSINESSPLAN = 'Business';
const ENTERPRISEPLAN = 'Enterprise';
const PREMIUMPLANPRICE = 'premium';
const BUSINESSPLANPRICE = 'business';
const TRIALPLANMODE = 'Trial';
const UPGRADEPLANMODE = 'Upgrade';
const UPDATEPLANMODE = 'Update';
const PLANGRACEPERIOD = -3;
const UPGRADEFROMTRIAL = 'UpgradeFromTrial';
const UPGRADEFROMFREE = 'UpgradeFromFree';
const GRACEPERIODPLAN = 'GracePeriodPlan';
const MONTHLYPACKAGE = 'Monthly';
const BUSINESSPAYMENT = 'businessPaymentScreen'
const PREMIUMPAYMENT = 'premiumPaymentScreen'

export function isPlanFree(name) {
    if (name.toLowerCase() == FREEPLAN.toLowerCase() || name.toLowerCase() == FREEPLAN1.toLowerCase())
        return true;
    else
        return false;
}
export function isPlanPaid(name) {
    if (name.toLowerCase() == PREMIUMPLAN.toLowerCase() || name.toLowerCase() == BUSINESSPLAN.toLowerCase())
        return true;
    else
        return false;
}
export function isPlanNonPaid(name) {
    if (name.toLowerCase() == FREEPLAN.toLowerCase() ||
        name.toLowerCase() == FREEPLAN1.toLowerCase() ||
        name.toLowerCase() == TRIALPLAN.toLowerCase() ||
        name.toLowerCase() == PREMIUMTRIALPLAN.toLowerCase() ||
        name.toLowerCase() == BUSINESSTRIALPLAN.toLowerCase())
        return true;
    else
        return false;
}
export function isPlanTrial(name) {
    if (name.toLowerCase() == TRIALPLAN.toLowerCase() ||
        name.toLowerCase() == PREMIUMTRIALPLAN.toLowerCase() ||
        name.toLowerCase() == BUSINESSTRIALPLAN.toLowerCase())
        return true;
    else
        return false;
}
export function DisplayPlanName(name) {
    if (name.toLowerCase() == PREMIUMPLAN.toLowerCase())
        return PREMIUMPLAN;
    else if (name.toLowerCase() == BUSINESSPLAN.toLowerCase())
        return BUSINESSPLAN;
    else if (name.toLowerCase() == PREMIUMTRIALPLAN.toLowerCase())
        return PREMIUMPLAN + ' Trial';
    else if (name.toLowerCase() == BUSINESSTRIALPLAN.toLowerCase())
        return BUSINESSPLAN + ' Trial';
    else if (name.toLowerCase() == FREEPLAN.toLowerCase() || name.toLowerCase() == FREEPLAN1.toLowerCase())
        return BASICPLAN;
    else if (name.toLowerCase() == TRIALPLAN.toLowerCase())
        return TRIALPLAN;
}
export function isBusinessPlan(name) {
    if (name.toLowerCase() == BUSINESSPLAN.toLowerCase())
        return true;
    else
        return false;
}

export function isBusinessTrial(name) {
    if (name.toLowerCase() == BUSINESSTRIALPLAN.toLowerCase())
        return true;
    else
        return false;
}
export function isAnyBusinessPlan(name) {
    if (name.toLowerCase() == BUSINESSTRIALPLAN.toLowerCase() || name.toLowerCase() == BUSINESSPLAN.toLowerCase())
        return true;
    else
        return false;
}
export function isEnterprisePlan(name) {
    if (name.toLowerCase() == ENTERPRISEPLAN.toLowerCase() )
        return true;
    else
        return false;
}

export function isPremiumPlan(name) {
    if (name.toLowerCase() == PREMIUMPLAN.toLowerCase())
        return true;
    else
        return false;
}

export function isPremiumTrial(name) {
    if (name.toLowerCase() == PREMIUMTRIALPLAN.toLowerCase())
        return true;
    else
        return false;
}
export function isAnyPremiumPlan(name) {
    if (name.toLowerCase() == PREMIUMTRIALPLAN.toLowerCase() || name.toLowerCase() == PREMIUMPLAN.toLowerCase())
        return true;
    else
        return false;
}

export function isMonthlyPackage(name) {
    if (name.toLowerCase() == MONTHLYPACKAGE.toLowerCase())
        return true;
    else
        return false;
}

export function isPlanExpire(endDate, gracePeriod = null) {
    if (gracePeriod) {
        let expMomentDate = moment(new Date(endDate), "MMMM DD, YYYY HH.mm.ss").add(gracePeriod, 'day');;
        let nowMomentDate = moment(new Date(), "MMMM DD, YYYY HH.mm.ss");
        return nowMomentDate.isAfter(expMomentDate);
    } else {
        let expMomentDate = moment(new Date(endDate), "MMMM DD, YYYY HH.mm.ss");
        let nowMomentDate = moment(new Date(), "MMMM DD, YYYY HH.mm.ss");
        return nowMomentDate.isAfter(expMomentDate);
    }
}

export default {
    FREEPLAN,
    BASICPLAN,
    TRIALPLAN,
    PREMIUMTRIALPLAN,
    PREMIUMPLAN,
    TRIALPERIOD,
    BUSINESSTRIALPLAN,
    BUSINESSPLAN,
    ENTERPRISEPLAN,
    TRIALPLANMODE,
    UPGRADEPLANMODE,
    UPDATEPLANMODE,
    PREMIUMPLANPRICE,
    BUSINESSPLANPRICE,
    PLANGRACEPERIOD,
    UPGRADEFROMTRIAL,
    UPGRADEFROMFREE,
    GRACEPERIODPLAN,
    isPlanPaid,
    isPlanFree,
    isPlanNonPaid,
    isPlanTrial,
    DisplayPlanName,
    isBusinessPlan,
    isBusinessTrial,
    isPremiumPlan,
    isPremiumTrial,
    MONTHLYPACKAGE,
    isMonthlyPackage,
    isAnyPremiumPlan,
    isAnyBusinessPlan,
    isEnterprisePlan,
    isPlanExpire,
    PREMIUMPAYMENT,
    BUSINESSPAYMENT

}
