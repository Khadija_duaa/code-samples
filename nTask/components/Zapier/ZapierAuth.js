import React, { useEffect } from "react";
import helper from "../../helper/index";

function ZapierAuth() {
  let token = helper.getToken();
  let email = localStorage.getItem("userName");
  let zapkey = localStorage.getItem("zapkey");

  useEffect(() => {
    helper.ZAPIERREDIRECT(token, email, zapkey);
  }, []);

  return <div></div>;
}

export default ZapierAuth;
