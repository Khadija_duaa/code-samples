const customTableStyles = (theme) => ({
  fontSize: {
    fontSize: "11px !important"
  },
  TotalAssignee: {
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main,
    width: 30,
    height: 30
  },
})

export default customTableStyles;