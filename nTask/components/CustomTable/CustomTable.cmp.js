import React, { useContext, useEffect } from "react";
import styled from "styled-components";
import {
  useTable,
  useBlockLayout,
  useGroupBy,
  useExpanded,
  useAsyncDebounce,
  useGlobalFilter,
  useSortBy,
} from "react-table";
import { FixedSizeList } from "react-window";
import { Circle } from "rc-progress";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import AddIcon from "@material-ui/icons/Add";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import Avatar from "@material-ui/core/Avatar";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import clsx from "clsx";
import { statusDataNew, priorityData } from "../../helper/taskDropdownData";
import { defaultTheme } from "../../assets/jss/theme";
import TaskOverviewContext from "../../Views/TasksOverview/Context/taskOverview.context";
import {
  dispatchWidgetSetting,
  dispatchTaskGroupByAssignee,
  dispatchTask,
  dispatchOpenedGroup
} from "../../Views/TasksOverview/Context/actions";
import { updateWidgetSetting } from "../../redux/actions/overviews";
import CustomMultiSelectDropdown from "../Dropdown/CustomMultiSelectDropdown/Dropdown";
import generateTaskColumnsData from "./constants";
import customTableStyles from "./customTableStyles";
import CustomAvatar from "../Avatar/Avatar";
import ColumnActionDropDown from "./ColumnActionDropdown/ColumnActionDropDown";
import CancelIcon from "@material-ui/icons/Cancel";
import EmptyState from "../../components/EmptyStates/EmptyState";
const Styles = styled.div`
  padding: 0 15px 15px 15px;

  .table {
    display: inline-block;
    border-spacing: 0;

    .tr {
      cursor: pointer;
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
    }
    .gtr {
      .td{
        background: #eaeaea
      }
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
    }
    .childRow{
      // box-shadow: inset 10px 0px 0px 0px #eaeaea, inset -10px 1px 0px 0px #eaeaea;
    }
    .glr {
      box-shadow: inset 10px 0px 0px 0px #eaeaea;
      .td {
        box-shadow: 0px 10px 0px #eaeaea;
      }
    }

    .th,
    .td {
      margin: 0;
      padding: 4px 0;
      border-bottom: none;
      border-right: none;
      display: flex !important;
      align-items: center;
      justify-content: center;
      :last-child {
        border-right: none;
      }
    }
    .th.taskTitle {
      justify-content: flex-start;
    }
    .th {
      font-size: 12px;
      padding: 0;
      margin-top: 10px;
      :first-child {
        display: flex;
        justify-content: flex-start;
      }
      .columnHeader {
        display: inline-block;
        transition: ease all 0.2s;
        padding: 3px 6px 3px 0;
        cursor: default;
        height: 22px;
      
      }
      :hover{
        .columnHeader {
        transition: ease all 0.2s;
        background: #EAEAEA;
        border-radius: 4px 0 0 4px;
        padding-left: 7px;
        border-right: 2px solid #BFBFBF;
        
      }
      button{
        display: inline-block;
      }
      }
    }
    .td {
      font-size: 12px;
      box-shadow: 0px 1px 0px #eaeaea;
      display: flex !important;
      align-items: center;
      justify-content: center;
      :first-child {
        font-size: 13px;
        justify-content: flex-start;
        cursor: pointer
      }
    }
    .td.taskTitle {
      font-size: 13px;
      justify-content: flex-start;
    }
    .priorityIcon {
      width: 28px;
      height: 28px;
      border: 1px solid #dddddd;
      border-radius: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
      background: #fff;
    }
    .addIcon {
      color: white;
    }
    .arrowIcon{
      color: #7E7E7E
    },
   
  }
  .taskGroupCountTag{
    padding: 4px;
    color: white;
    background: #0090ff;
    font-size: 11px;
    margin-left: 8px;
    display: inline-block;
    border-radius: 4px;
  }
  .groupByColumnsCnt{
    border-bottom: 1px solid #dddddd;
    padding: 8px 0 8px 0;
    display: flex;
    align-items: center;
  }
  .groupByColumnChipLabel{
    font-size: 13px;
    color: #000;
    display: inline-block;
    margin-right: 8px;
  }
  .groupByColumnChip{
    font-size: 13px;
    color: #000;
    padding: 3px 8px;
    display: flex;
    background: #EAEAEA;
    border-radius: 4px;
    display: flex;
    align-items: center;
    margin-right: 10px;
    svg{
      font-size: 18px;
      color: #969696;
      cursor: pointer;
      margin-left: 8px
    }
  }
  .empty {
    padding: 0 30px 0 60px;
      flex: 1;
      display: flex;
  }
  .groupBySpan{
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
// Define a default UI for filtering
function GlobalFilter({ preGlobalFilteredRows, globalFilter, setGlobalFilter }) {
  const count = preGlobalFilteredRows.length;

  const [value, setValue] = React.useState(globalFilter);
  const onChange = useAsyncDebounce(v => {
    setGlobalFilter(v || undefined);
  }, 200);

  return (
    <span
      style={{
        position: "absolute",
        right: 19,
        top: -43,
        fontFamily: "'lato', sans-serif",
        fontSize: "13px !important",
      }}>
      Search:{" "}
      <input
        value={value || ""}
        onChange={e => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
        placeholder={`${count} records...`}
        style={{
          border: "1px solid rgba(221, 221, 221, 1)",
          borderRadius: 4,
          fontFamily: "'lato', sans-serif",
          fontSize: "13px !important",
          padding: 8,
          width: 250,
        }}
      />
    </span>
  );
}
function Table({ columns, data, handleSetScrollPosition }) {
  // Use the state and functions returned from useTable to build your UI
  const {
    state: { widgetSettings, taskWidgetData, openedGroup },
    dispatch,
  } = useContext(TaskOverviewContext);
  const defaultColumn = React.useMemo(
    () => ({
      width: 150,
    }),
    []
  );
  // Style for Table Cells
  const getCellStyles = (props, col) => {
    return [
      props,
      {
        style: {
          // margin: 0,
          width: col.width,
          //  flex: 1,
          minWidth: col.minWidth,
          border: "none",
        },
      },
    ];
  };
  // Style for Table Header
  const getHeaderStyles = (props, col) => {
    return [
      props,
      {
        style: {
          width: col.width,
          // height: 30,

          // flex: 1,
          // border: 'none',
          minWidth: col.minWidth,
        },
      },
    ];
  };
  const getTableStyles = props => [
    props,
    {
      style: {
        width: "100%",
        borderSpacing: 0,
      },
    },
  ];
  const headerProps = (props, rowInfo) => getHeaderStyles(props, rowInfo.column);

  const cellProps = (props, rowInfo) => getCellStyles(props, rowInfo.cell.column);
  const tableProps = props => getTableStyles(props);

  const scrollbarWidth = () => {
    // thanks too https://davidwalsh.name/detect-scrollbar-width
    const scrollDiv = document.createElement("div");
    scrollDiv.setAttribute(
      "style",
      "width: 100px; height: 100px; overflow: scroll; position:absolute; top:-9999px;"
    );
    document.body.appendChild(scrollDiv);
    const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    document.body.removeChild(scrollDiv);
    return scrollbarWidth;
  };

  const scrollBarSize = React.useMemo(() => scrollbarWidth(), []);
  // Define a default UI for filtering
  const handleRowClick = rowData => {
    const { permissions, taskDetails, templates } = taskWidgetData;
    let target = document.querySelector("#taskOverViewTable");
    let child = target.querySelector("#taskOverViewTable > div");
    handleSetScrollPosition(child.scrollTop);
    if(rowData){
      let taskObject = taskDetails.find(item => item.taskId === rowData.taskId) || null;
      let permissionObject = permissions.find(
        p => p.workSpaceId === taskObject.teamId
      ); /** workspace task permission */
      let workspaceDefaultTemplate = templates.find(t=> t.workspace === taskObject.teamId);
      let nTaskStandardTemplate = templates.find(t=> !t.workspace);
      workspaceDefaultTemplate = workspaceDefaultTemplate ? workspaceDefaultTemplate : nTaskStandardTemplate;
      let task = {...rowData, permissionObject, template : workspaceDefaultTemplate  };
      dispatchTask(dispatch, task);
    }
  };
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    totalColumnsWidth,
    preGlobalFilteredRows,
    state,
    prepareRow,
    setGlobalFilter,
    toggleGroupBy,
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      initialState: { groupBy: widgetSettings.groupBy },
    },
    useBlockLayout,
    useGroupBy,
    useGlobalFilter,
    useSortBy,
    useExpanded
  );

  const RenderRow = React.useCallback(
    ({ index, style }) => {
      const row = rows[index];
      const canExpanded = rows[index].canExpand;
      const { isGrouped } = row;
      const isExp = row.isExpanded;
      prepareRow(row);
      const expandedObj = openedGroup.find(item => item == row.id) || null;
      expandedObj && !isExp && row.toggleRowExpanded()
      return (
        <div
          {...row.getRowProps({
            style,
          })}
          className={clsx({
            gtr: isGrouped && isExp,
            childRow: state.groupBy.length && !canExpanded,
            tr: !canExpanded,
          })}>
          {row.cells.map(cell => {
            return (
              <div 
              {...cell.getCellProps(cellProps)} 
              className={`td ${cell.column.id}`} 
              onClick={() => handleRowClick(cell.row.original)}
              >
                {/* {cell.render("Cell")} */}
                {cell.isGrouped ? (
                  // If it's a grouped cell, add an expander and row count
                  <>
                   <span 
                    onClick={()=> {
                      let expanded = row.isExpanded ? openedGroup.filter(i => i !== row.id) : [...openedGroup, row.id];
                      dispatchOpenedGroup(dispatch, expanded);
                      row.toggleRowExpanded();
                    }} 
                    className="groupBySpan">
                      {row.isExpanded ? (
                        <ArrowDropDownIcon className="arrowIcon" />
                      ) : (
                        <ArrowRightIcon className="arrowIcon" />
                      )}
                    {cell.render("Cell")}
                    </span>
                    <span className="taskGroupCountTag">{row.leafRows.length} tasks</span>
                  </>
                ) : cell.isAggregated ? null : // cell.render("Aggregated") // renderer for cell // If the cell is aggregated, use the Aggregated
                cell.isPlaceholder ? null : ( // For cells with repeated values, render null
                  // Otherwise, just render the regular cell
                  cell.render("Cell")
                )}
              </div>
            );
          })}
        </div>
      );
    },
    [prepareRow, rows]
  );
  // Render the UI for your table
  return (
    <>
      <GlobalFilter
        preGlobalFilteredRows={preGlobalFilteredRows}
        globalFilter={state.globalFilter}
        setGlobalFilter={setGlobalFilter}
        manualGlobalFilter
      />
      {state.groupBy.length ? (
        <div className="groupByColumnsCnt">
          <label className="groupByColumnChipLabel">Group by:</label>
          {state.groupBy.map(g => {
            const groupByColumn = columns.find(c => c.accessor == g);
            return groupByColumn ? (
              <span className="groupByColumnChip">
                <span>{groupByColumn.Header}</span>
                <CancelIcon
                  onClick={() => {
                    const updatedGroupBy = widgetSettings.groupBy.filter(
                      g => g !== groupByColumn.accessor
                    );
                    if (groupByColumn.accessor == "assignee") {
                      dispatchTaskGroupByAssignee(dispatch, []);
                    }
                    dispatchWidgetSetting(dispatch, { groupBy: updatedGroupBy });
                    updateWidgetSetting(
                      "task",
                      { ...widgetSettings, groupBy: updatedGroupBy },
                      // Success
                      res => {}
                    );
                    toggleGroupBy(groupByColumn.accessor);
                  }}
                />
              </span>
            ) : null;
          })}
        </div>
      ) : null}
      <div {...getTableProps(tableProps)} className="table">
        <div>
          {headerGroups.map(headerGroup => (
            <div {...headerGroup.getHeaderGroupProps()} className="tr">
              {headerGroup.headers.map(column => (
                <div {...column.getHeaderProps(headerProps)} className={`th ${column.id}`}>
                  {/* {column.canGroupBy ? (
                    // If the column can be grouped, let's add a toggle
                    <span {...column.getGroupByToggleProps()}>
                      {column.isGrouped ? '🛑 ' : '👊 '}
                    </span>
                  ) : null} */}
                  {column.id !== "columnOptions" &&
                  column.id !== "taskTitle" &&
                  column.id !== "uniqueId" &&
                  column.id !== "comments" &&
                  column.id !== "meetings" &&
                  column.id !== "risks" &&
                  column.id !== "issues" &&
                  column.id !== "timeLogged" &&
                  column.id !== "attachments" &&
                  column.id !== "startDate" &&
                  column.id !== "dueDate" &&
                  column.id !== "actualStartDate" &&
                  column.id !== "plannedEffort" &&
                  column.id !== "effortToDate" &&
                  column.id !== "actualDueDate" &&
                  column.id !== "progress" &&
                  column.id !== "creationDate" ? (
                    <span className="columnHeader">
                      {column.render("Header")}

                      {!state.groupBy.includes(column.id) && (
                        <ColumnActionDropDown column={column} data={data} />
                      )}
                    </span>
                  ) : (
                    column.render("Header")
                  )}
                </div>
              ))}
            </div>
          ))}
        </div>

        <div {...getTableBodyProps()} style={{ width: "101%" }} id="taskOverViewTable">
          {data.length && data.length > 0 ? (
            <FixedSizeList
              height={window.innerHeight - 200}
              itemCount={rows.length}
              itemSize={50}
              width={totalColumnsWidth}>
              {RenderRow}
            </FixedSizeList>
          ) : (
            <div
              style={{
                height: window.innerHeight - 200,
              }}>
              <div className="empty" style={{ height: "100%" }}>
                <EmptyState
                  screenType="newWorkspace"
                  heading="Please select workspace."
                  button={false}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
}

function CustomTable({ data, profileState, classes, isLoading, handleSetScrollPosition }) {
  const {
    state: { widgetSettings, loadingState },
    dispatch,
  } = useContext(TaskOverviewContext);
  // save selected columns
  const onSelectedColumns = options => {
    const colsArr = options.map(opt => {
      return opt.value;
    }); // set all columns
    dispatchWidgetSetting(dispatch, { selectedColumns: colsArr });
    updateWidgetSetting(
      "task",
      { ...widgetSettings, selectedColumns: colsArr },
      // Success
      res => {}
    );
  };

  const GenerateList = ({ members }) => {
    const membersList = profileState.teamMember.filter(m => members.includes(m.userId));
    return membersList.slice(0, 3).map((member, i, t) => {
      return (
        // flex_center_center_row class is applied when there is only 1 user to style the user full name
        <li
          key={i}
          style={{ zIndex: i, marginLeft: !i == 0 ? -5 : null }}
          className={t.length === 1 ? "flex_center_center_row" : null}>
          <CustomAvatar
            otherMember={{
              imageUrl: member.imageUrl,
              fullName: member.fullName,
              lastName: "",
              email: member.email,
              isOnline: member.isOnline,
              isOwner: member.isOwner,
            }}
            size="xsmall"
          />
        </li>
      );
    });
  };

  // filtering columns on the basis of items selected by the user form drop down show filter and taskTitle by default, if thers columns then do filter otherwise no need
  const selectedColumns = generateTaskColumnsData.filter(c => {
    return widgetSettings.selectedColumns.includes(c.value);
  });
  const columnWidth = `${100 / widgetSettings.selectedColumns.length}%`;
  const headerColumns = [
    {
      Header: "Task Id",
      accessor: "uniqueId",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{row.values.uniqueId}</span>;
      },
    },
    {
      Header: "Task Title",
      accessor: "taskTitle",
      isGrouped: true,
      width: widgetSettings.selectedColumns.length ? columnWidth : "100%",
      minWidth: 200,
      Cell: ({ row }) => {
        return (
          <div style={{ wordBreak: "break-all", padding: "3px 5px 3px 5px" }}>
            {row.values.taskTitle}
          </div>
        );
      },
    },
    {
      Header: "Workspace",
      accessor: "workspaceName",
      width: widgetSettings.selectedColumns.length ? columnWidth : "100%",
      minWidth: 200,
      Cell: ({ row }) => {
        return (
          <span style={{ wordBreak: "break-all", padding: "3px 5px 3px 5px" }}>
            {row.values.workspaceName}
          </span>
        );
      },
    },
    {
      Header: "Project",
      accessor: "projectName",
      width: widgetSettings.selectedColumns.length ? columnWidth : "100%",
      minWidth: 200,
      Cell: ({ row }) => {
        return (
          <span style={{ wordBreak: "break-all", padding: "3px 5px 3px 5px" }}>
            {row.values.projectName ? row.values.projectName : "Others"}
          </span>
        );
      },
    },
    {
      Header: "Status",
      accessor: "statusTitle",
      width: columnWidth,
      minWidth: 170,
      Cell: ({ row }) => {
        const selectedStatus = statusDataNew(defaultTheme, {}).find(
          s => s.value === row.values.status
        );
        return (
          <div
            style={{
              // background: selectedStatus && selectedStatus.color,
              background: row.original && row.original.statusColor,
              borderRadius: 4,
              width: 80,
              fontSize: "12px !important",
              display: "inline-block",
              textAlign: "center",
              padding: "4px 5px",
              color: row.original ? "white" : "black",
            }}>
            {/* {selectedStatus && selectedStatus.label} */}
            {row.values && row.values.statusTitle}
          </div>
        );
      },
    },
    {
      Header: "Priority",
      accessor: "priority",
      width: columnWidth,
      Cell: ({ row }) => {
        const selectedPriority = priorityData(defaultTheme, {}).find(
          p => row.values.priority === p.value
        );
        return <div className="priorityIcon">{selectedPriority && selectedPriority.icon}</div>;
      },
    },
    {
      Header: "Planned Start Date",
      accessor: "startDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.original && row.original.startDate) || "-"}</span>;
      },
    },
    {
      Header: "Planned End Date",
      accessor: "dueDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.original && row.original.dueDate) || "-"}</span>;
      },
    },
    {
      Header: "Planned Effort",
      accessor: "plannedEffort",
      width: columnWidth,
      Cell: ({ row }) => {
        return (
          <span>
            {row.original && row.original.plannedEffort
              ? `${row.original.plannedEffort} Days`
              : "-"}
          </span>
        );
      },
    },
    {
      Header: "Effort To Date",
      accessor: "effortToDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return (
          <span>
            {row.original && row.original.effortToDate ? `${row.original.effortToDate} Days` : "-"}
          </span>
        );
      },
    },
    {
      Header: "Actual Start Date",
      accessor: "actualStartDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.original && row.original.actualStartDate) || "-"}</span>;
      },
    },
    {
      Header: "Actual End Date",
      accessor: "actualDueDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.original && row.original.actualDueDate) || "-"}</span>;
      },
    },
    {
      Header: "Progress",
      accessor: "progress",
      width: columnWidth,
      Cell: ({ row }) => {
        return (
          <div style={{ position: "relative" }}>
            <div style={{ width: 30 }}>
              <Circle
                percent={row.values.progress}
                strokeWidth="10"
                trailWidth=""
                trailColor="#dedede"
                strokeColor="#30d56e"
              />
            </div>
            <Typography
              variant="h6"
              align="center"
              style={{
                position: "absolute",
                top: 6,
                left: "50%",
                transform: "translateX(-50%)",
                fontSize: "11px !important",
              }}>
              {row.values.progress}
            </Typography>
          </div>
        );
      },
    },
    {
      Header: "Assignee",
      accessor: "assignee",
      minWidth: 170,
      Cell: ({ row }) => {
        if (row.isGrouped) {
          return <>{row.values.assignee}</>;
        }
        const assigneeList = row.original && row.original.assigneeList;
        return assigneeList ? (
          <ul className="AssigneeAvatarList">
            <GenerateList members={assigneeList} />
            {assigneeList.length > 3 ? (
              <li>
                <Avatar classes={{ root: classes.TotalAssignee }}>
                  +{assigneeList.length - 3}
                </Avatar>
              </li>
            ) : (
              ""
            )}
          </ul>
        ) : (
          <></>
        );
      },
    },
    {
      Header: "Comments",
      accessor: "comments",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{row.original && row.original.totalComment}</span>;
      },
    },
    {
      Header: "Time Logged",
      accessor: "timeLogged",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{row.original.timeLogged || "00:00"}</span>;
      },
    },
    {
      Header: "Attachments",
      accessor: "attachments",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{row.original && row.original.totalAttachment}</span>;
      },
    },
    {
      Header: "Meetings",
      accessor: "meetings",
      width: columnWidth,
    },
    {
      Header: "Issues",
      accessor: "issues",
      width: columnWidth,
    },
    {
      Header: "Risks",
      accessor: "risks",
      width: columnWidth,
    },
    {
      Header: "Creation Date",
      accessor: "creationDate",
      width: columnWidth,
      Cell: ({ row }) => {
        return <span>{(row.original && row.original.createdDate) || "-"}</span>;
      },
    },
    {
      Header: "Created By",
      accessor: "createdBy",
      width: columnWidth,
      Cell: ({ row }) => {
        const memberObj = profileState.teamMember.find(m => m.userId == row.values.createdBy);
        return <span>{memberObj.fullName}</span>;
      },
    },
    {
      Header: (
        <CustomMultiSelectDropdown
          label=""
          options={() => generateTaskColumnsData}
          option={selectedColumns}
          open={false}
          iconButton
          icon={<AddIcon style={{ color: defaultTheme.palette.common.white, fontSize: "14px !important" }} />}
          onSelect={onSelectedColumns}
          maxSelections={10}
          heading={`Customize Column (${selectedColumns.length}/10)`}
          height="140px"
          width="140px"
          scrollHeight={180}
          buttonProps={{
            btnType: "DarkGray",
            style: { padding: 2 },
          }}
        />
      ),
      accessor: "columnOptions",
      width: 50,
      Cell: ({ row }) => {
        return <></>;
      },
    },
  ];
  const filteredColumns = headerColumns.filter(c => {
    return (
      widgetSettings.selectedColumns.includes(c.accessor) ||
      c.accessor === "taskTitle" ||
      c.accessor === "columnOptions"
    );
  });

  return isLoading ? (
    <div
      style={{
        height: window.innerHeight - 325,
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <div className="loader" />
    </div>
  ) : (
    <>
    {loadingState && <div
      style={{
        height: window.innerHeight - 250,
        width: "88%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        zIndex: 1,
        background: "white",
        position: "absolute"
      }}>
      <div className="loader" />
    </div>}
    <div style={{ position: "relative" }}>
      <Styles>
        <Table columns={filteredColumns} data={data} handleSetScrollPosition={handleSetScrollPosition}/>
      </Styles>
    </div>
    </>
  );
}

const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
  };
};

export default compose(
  withStyles(customTableStyles, { withTheme: true }),
  connect(mapStateToProps)
)(CustomTable);
