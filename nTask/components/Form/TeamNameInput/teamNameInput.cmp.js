import React, { useEffect, useState } from "react";
import DefaultTextField from "../TextField";

function TeamNameInput(props) {
  const {defaultProps, onChangeCallback,value, ...rest} = props
  const [teamName, setTeamName] = useState();
  const handleInput = event => {
    const value = event.target.value.replace(/\s\s+/g, ' ');
    setTeamName(value);
    onChangeCallback(value, 'teamName');
  };
  useEffect(() => {
    setTeamName(value);
  }, [])
  return (
    <DefaultTextField
      label='Team Name'
      fullWidth={true}
      defaultProps={{
        id: "teamNameInput",
        value: teamName,
        placeholder: 'e.g. Acme Corporation ',
        onChange: event => handleInput(event),
        inputProps: { maxLength: 80, tabIndex: 3 },
        type: "text",
      ...defaultProps
      }}
      {...rest}
    />
  );
}

export default TeamNameInput;
