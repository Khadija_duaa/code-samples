// @flow

import React, { useState, useEffect } from "react";
import DefaultTextField from "../TextField";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../../Buttons/CustomButton";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { validateHours } from "../../../utils/validator/common//estimatedHours";
import durationInputStyles from "./style";
type DropdownProps = {
  classes: Object,
  theme: Object,
  label: String,
  onMinsInputChange: Function,
  onInputBlur: Function,
  onHoursInputChange: Function,
  placeholder: String,
  actualHours: String,
  actualMins: String,
};

//Onboarding Main Component
function DurationInput(props: DropdownProps) {
  const {
    classes,
    theme,
    label,
    onMinsInputChange,
    onHoursInputChange,
    onInputBlur,
    actualHours,
    actualMins,
    placeholder,
  } = props;
  const [hours, setHours] = useState(actualHours);
  const [mins, setMins] = useState(actualMins);
  const [editable, setEditable] = useState(false);
  const handleHoursInput = (event) => {
    // Function handles hours input
    if (validateHours(event.target.value).validated)
      setHours(event.target.value); // set hours value
    onHoursInputChange(event.target.value); // input change in parent
  };
  const handleMinsInput = (event) => {
    // Function handles mins input
    setMins(event.target.value); // set mins value
    onMinsInputChange(event.target.value); // input change in parent
  };
  const handleAddAmount = () => {
    setEditable(true);
  };
  const handleAmountClick = () => {
    setEditable(true);
  };
  const handleInputBlur = (event) => {
    setEditable(false);
    onInputBlur(hours, mins);
  };

  return (
    <>
      {editable ? (
        <ClickAwayListener onClickAway={handleInputBlur}>
          <div>
            <DefaultTextField
              label={label}
              size="small"
              error={false}
              noRightBorder={true}
              formControlStyles={{ marginBottom: 0, height: 28, maxWidth: 35 }}
              defaultProps={{
                id: "hoursInput",
                onChange: handleHoursInput,
                autoFocus: true,
                placeholder: "mm",
                value: hours,
                inputProps: {
                  maxLength: 50,
                  style: {
                    fontFamily: theme.typography.fontFamilyLato,
                    fontSize: "13px",

                    fontWeight: 500,
                  },
                },
              }}
            />
            <DefaultTextField
              label={label}
              size="small"
              error={false}
              noLeftBorder={true}
              formControlStyles={{ marginBottom: 0, height: 28, width: 35 }}
              defaultProps={{
                id: "minsInput",
                onChange: handleMinsInput,
                placeholder: "hh",
                value: mins,
                inputProps: {
                  maxLength: 50,
                  style: {
                    fontFamily: theme.typography.fontFamilyLato,
                    fontSize: "13px",
                    fontWeight: 500,
                  },
                },
              }}
            />
          </div>
        </ClickAwayListener>
      ) : (
        <CustomButton
          onClick={handleAmountClick}
          btnType="plainbackground"
          variant="text"
          style={{
            padding: 7,
            borderRadius: 4,
          }}
        >
          {hours || mins ? (
            <span onClick={handleAddAmount} className={classes.durationValue}>
              {hours}:{mins}
            </span>
          ) : (
            <span onClick={handleAddAmount} className={classes.placeholder}>
              {placeholder}
            </span>
          )}
        </CustomButton>
      )}
    </>
  );
}

DurationInput.defaultProps = {
  classes: {},
  theme: {},
  label: "",
  actualHours: "",
  actualMins: "",
  onHoursInputChange: () => {},
  onMinsInputChange: () => {},
  onInputBlur: () => {},
};

export default withStyles(durationInputStyles, { withTheme: true })(
  DurationInput
);
