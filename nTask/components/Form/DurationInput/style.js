const amountInputStyles = (theme) => ({
  currencyValue: {
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
    marginRight: 5,
  },
  placeholder: {
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
  },
  amountValue: {
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
  },
  startAdornment: {
    paddingLeft: 6,
  },
});

export default amountInputStyles;
