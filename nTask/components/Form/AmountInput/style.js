const amountInputStyles = (theme) => ({
  amountCnt: {
    display: "flex",
    alignItems: "center",
  },
  currencyValue: {
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
    marginRight: 5,
  },
  placeholder: {
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
  },
  amountValue: {
    fontSize: "13px !important",
    fontWeight: 600,
    color: "#202020",
    fontFamily: theme.typography.fontFamilyLato,
  },
  startAdornment: {
    paddingLeft: 6,
  },
  amountCmpLabel: {
    lineHeight: "normal",
    color: theme.palette.text.light,
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
});

export default amountInputStyles;
