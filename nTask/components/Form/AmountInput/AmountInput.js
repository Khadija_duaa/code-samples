// @flow

import React, { useState, useEffect } from "react";
import DefaultTextField from "../TextField";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../../Buttons/CustomButton";
import Typography from "@material-ui/core/Typography";
import { validateAmount } from "../../../utils/validator/common//rateAmountWithDecimal";
import Hotkeys from "react-hot-keys";

import amountInputStyles from "./style";
type DropdownProps = {
  classes: Object,
  theme: Object,
  label: String,
  onInputChange: Function,
  currency: String,
  placeholder: String,
  style: Object,
};

//Onboarding Main Component
function AmountInput(props: DropdownProps) {
  const {
    classes,
    theme,
    label,
    inputLabel,
    onInputChange,
    currency,
    placeholder,
    onBlur,
    savedAmount = "",
    style,
    disabled,
  } = props;
  const [amount, setAmount] = useState(savedAmount);
  const [editable, setEditable] = useState(false);
  const handleInput = (event) => {
    let inputValue = event.target.value;
    if (validateAmount(inputValue).validated) {
      // Function handles input

      let value = inputValue ? parseInt(inputValue) : "";
      setAmount(value); // set amount value
      onInputChange(value); // input change in parent
    }
  };
  const handleAmountClick = (event) => {
    event.stopPropagation();
    setEditable(true);
  };
  const handleAddAmount = () => {
    setEditable(true);
  };
  const handleInputBlur = () => {
    setEditable(false);
    onBlur(amount);
  };
  const onKeyDown = (keyName, e, handle) => {
    if (keyName == "enter") {
      onInputChange(amount);
      onBlur(amount);
      setEditable(false);
    }
  };
  useEffect(() => {
    setAmount(savedAmount);
  }, [savedAmount]);
  return (
    <div className={classes.amountCnt} style={style}>
      <Typography variant="body2" className={classes.amountCmpLabel}>
        {label}
      </Typography>
      {editable ? (
        <Hotkeys keyName="enter" onKeyDown={onKeyDown}>
          <DefaultTextField
            label={inputLabel}
            size="small"
            error={false}
            formControlStyles={{
              marginBottom: 0,
              maxWidth: 125,
              marginLeft: 3,
            }}
            customInputClass={{ adornedStart: classes.startAdornment }}
            defaultProps={{
              id: "amountInput",
              onChange: handleInput,
              autoFocus: true,
              onBlur: handleInputBlur,
              value: amount,
              inputProps: {
                style: { paddingLeft: 0 },
              },
              startAdornment: (
                <span className={classes.currencyValue}>{currency}</span>
              ),
            }}
          />
        </Hotkeys>
      ) : amount && !editable ? (
        <CustomButton
          onClick={handleAmountClick}
          btnType="plainbackground"
          variant="text"
          style={{
            padding: "4.5px 7px",
            borderRadius: 4,
          }}
          disabled={disabled}
        >
          <span className={classes.currencyValue}>{currency}</span>{" "}
          <span className={classes.amountValue}> {amount}</span>
        </CustomButton>
      ) : (
        <CustomButton
          onClick={handleAmountClick}
          btnType="plainbackground"
          variant="text"
          style={{
            padding: "4.5px 7px",
            borderRadius: 4,
          }}
          disabled={disabled}
        >
          <span onClick={handleAddAmount} className={classes.placeholder}>
            {placeholder}
          </span>
        </CustomButton>
      )}
    </div>
  );
}
AmountInput.defaultProps = {
  style: {},
  disabled: false,
};
export default withStyles(amountInputStyles, { withTheme: true })(AmountInput);
