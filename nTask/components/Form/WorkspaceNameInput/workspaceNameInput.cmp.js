import React, { useEffect, useState } from "react";
import DefaultTextField from "../TextField";

function WorkspaceNameInput(props) {
  const {defaultProps, onChangeCallback, value, inputType, ...rest} = props
  const [wsName, setWsName] = useState();
  const handleInput = event => {
    const value = event.target.value.replace(/\s\s+/g, ' ');
    setWsName(value);
    onChangeCallback(value, 'workspaceName');
  };
  useEffect(() => {
    setWsName(value);
  }, [])
  return (
    <DefaultTextField
      label='Workspace Name'
      fullWidth={true}
      inputType={inputType}
      defaultProps={{
        id: "workSpaceNameInput",
        value: wsName,
        placeholder: 'e.g. Marketing department',
        onChange: event => handleInput(event),
        inputProps: { maxLength: 80, tabIndex: 3 },
        type: "text",
      ...defaultProps
      }}
      {...rest}
    />
  );
}

export default WorkspaceNameInput;
