const radioStyles = theme => ({
  root: {
    "&:hover": {
      backgroundColor: "transparent",
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
  icon: {
    borderRadius: "50%",
    width: 20,
    height: 20,
    border: "1px solid rgba(234, 234, 234, 1)",
    backgroundColor: "#f5f8fa",
    backgroundImage: "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto rgba(19,124,189,.6)",
      outlineOffset: 2,
    },
    "input:hover ~ &": {
      backgroundColor: "#ebf1f5",
    },
    "input:disabled ~ &": {
      boxShadow: "none",
      background: "rgba(206,217,224,.5)",
    },
  },
  checkedIcon: {
    border: "none",
    backgroundColor: "#137cbd",
    backgroundImage: "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 20,
      height: 20,
      backgroundImage: "radial-gradient(#fff,#fff 28%,transparent 32%)",
      content: '""',
    },
    "input:hover ~ &": {
      backgroundColor: "#106ba3",
    },
  },
  radioLabel: {
    color: theme.palette.text.primary,
  },
  radioGroup: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
});

export default radioStyles;
