import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import CheckedIcon from "@material-ui/icons/RadioButtonChecked";
import UnCheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import withStyles from "@material-ui/core/styles/withStyles"
import radioStyles from "./style";
import PropTypes from 'prop-types';



function CustomRadio(props) { // Custom Radio Component
  const { options, classes, theme, onRadioChange, value, radioGroupProps, iconProps } = props;

  const StyledRadio = (props) => { // Radio Checked and Unchecked icons
    return (
      <Radio
        disableRipple
        checkedIcon={<CheckedIcon className={classes.radioIcon} htmlColor={theme.palette.secondary.main} {...iconProps}/>}
        icon={<UnCheckedIcon className={classes.radioIcon} htmlColor={theme.palette.secondary.medDark} {...iconProps}/>}
        className={classes.radioButton}
        {...props}
      />
    );
  }
  return (
    <FormControl component="fieldset">
      <RadioGroup value={value} {...radioGroupProps}>
        {options.map(o => {
          return (
            <FormControlLabel
              classes={{ label: classes.radioLabel }}
              key={o}
              value={o}
              control={<StyledRadio onChange={event => onRadioChange(event, o)} />}
              label={o} />
          )
        })
        }
      </RadioGroup>
    </FormControl>
  );
}

// PropTypes
CustomRadio.PropTypes = {
  value: PropTypes.string.isRequired,
  onRadioChange: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired
}

//Default Props
CustomRadio.defaultProps = {
  value: "",
  onRadioChange: () => { },
  options: []
}
export default withStyles(radioStyles, { withTheme: true })(CustomRadio)