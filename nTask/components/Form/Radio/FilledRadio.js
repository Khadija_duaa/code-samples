import React from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import CheckedIcon from "@material-ui/icons/RadioButtonChecked";
import UnCheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import withStyles from "@material-ui/core/styles/withStyles";
import radioStyles from "./filledRadioStyle";
import PropTypes from "prop-types";
import clsx from "clsx";

function FilledRadio(props) {
  // Custom Radio Component
  const { options, classes, theme, onRadioChange, value, intl } = props;

  const StyledRadio = (props) => {
    // Radio Checked and Unchecked icons
    return (
      <Radio
        className={classes.root}
        disableRipple
        color="default"
        checkedIcon={
          <span className={clsx(classes.icon, classes.checkedIcon)} />
        }
        icon={<span className={classes.icon} />}
        {...props}
      />
    );
  };
  const translate = (value) => {
    let id = "";
    switch(value) {
     case "Planned start/end dates":
       id = "profile-settings-dialog.apps-integrations.common.plan.label";
       break;
       case "Actual start/end dates":
         id = "profile-settings-dialog.apps-integrations.common.actual.label";
         break;
    }
    return id == "" ? value : intl.formatMessage({id: id, defaultMessage: value});
   };
  return (
    <FormControl component="fieldset">
      <RadioGroup value={value} className={classes.radioGroup}>
        {options.map((o, i) => {
          return (
            <FormControlLabel
              classes={{ label: classes.radioLabel }}
              key={i}
              value={o}
              control={
                <StyledRadio onChange={(event) => onRadioChange(event)} />
              }
              label={translate(o)}
            />
          );
        })}
      </RadioGroup>
    </FormControl>
  );
}

// PropTypes
FilledRadio.PropTypes = {
  value: PropTypes.string.isRequired,
  onRadioChange: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
};

//Default Props
FilledRadio.defaultProps = {
  value: "",
  onRadioChange: () => {},
  options: [],
};
export default withStyles(radioStyles, { withTheme: true })(FilledRadio);
