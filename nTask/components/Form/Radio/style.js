const radioStyles = (theme) => ({
  radioButton: {
    padding: "4px 10px 4px 15px"
  },
  sadImage: {
    marginBottom: 10
  },
  radioIcon: {

  },
  radioLabel: {
    color: "#171717"
  },
  radioGroup: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
})

export default radioStyles