const formStyles = theme => ({
  formControlLabel: {
    alignItems: "flex-start",
    margin: 0,
  },
  checkbox: {},
  checkboxDisabled: {
    opacity: .5
  },
  passwordInputCnt: {
    fontSize: "13.7143px !important",
    textAlign: "left",
    borderRadius: 4,
    marginBottom: 23,
    background: theme.palette.common.white,
    "&:hover $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
    },
    "& span": {
      width: "20%",
    },
  },
  passwordInput: {
    height: 40,
    fontSize: 13.714,
    borderRadius: "inherit",
    "&::placeholder, &:::-ms-input-placeholder": {
      color: "#a9a9a9",
    },
  },
  inputLabel: {
    transform: "translate(2px, -17px) scale(1);",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  textFieldTitle: {
    // paddingTop: "9px",
    fontSize: "20px !important",
    fontFamily: theme.typography.fontFamilyLato,
    padding: "9px 0px 10px 5px",
    // height: 39,
    marginRight: 10,
    flex: 1,
    "&:hover":{
      backgroundColor: "#F6F6F6"
    },
    "&:hover $icons":{
      visibility: "visible"
    }
  },
  outlinedInputCnt: {
    height: 39,
  },
  outlinedInputsmall: {
    fontSize: "20px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightMedium,
    padding: "0px 0px 1px 5px",
  },
  textFieldCnt:{
    flex: 1
  },
  icons:{
    visibility: "hidden"
  }
});

export default formStyles;
