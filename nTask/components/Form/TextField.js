import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import { withStyles } from "@material-ui/core/styles";
import React, { Component, Fragment } from "react";
import clsx from "clsx";
import textFieldStyles from "../../assets/jss/components/textfield";
import CustomTooltip from "../Tooltip/Tooltip";
import Input from "@material-ui/core/Input";

function DefaultTextField(props) {
  const {
    defaultProps,
    error,
    label,
    errorMessage,
    errorState,
    noBorderRadius,
    classes,
    theme,
    fullWidth,
    noRightBorder,
    noLeftBorder,
    requiredInput,
    formControlStyles,
    labelProps,
    helptext,
    size = "",
    styles,
    customInputClass,
    borderBottom,
    transparentInput,
    outlineCnt = true,
    inputType,
    ...rest
  } = props;
  const notchedOutlineClasses = clsx({
    [classes.notchedOutlineCnt]: outlineCnt,
    [classes.notchedOutlineCntNB]: noBorderRadius,
    [classes.notchedOutlineCntNRB]: noRightBorder,
    [classes.notchedOutlineCntNLB]: noLeftBorder,
    [classes.notchedOutlineCntBB]: borderBottom,
    [classes.notchedOutlineCntTransp]: transparentInput,
  });
  const rootClasses = clsx({
    [classes.outlinedInputCnt]: true,
    [classes.outlinedInputCntTransp]: transparentInput,
  });
  const focusedClasses = clsx({
    [classes.outlineInputFocus]: true,
    [classes.outlineInputFocusNoBorder]: transparentInput
  })
  return (
    <FormControl
      style={{
        marginTop: !label ? 0 : 22,
        marginBottom: error === false ? 22 : 15,
        ...formControlStyles,
      }}
      required={requiredInput}
      classes={{ root: classes.formControl }}
      fullWidth={fullWidth}
      error={errorState}>
      {label === false ? null : (
        <InputLabel
          htmlFor={defaultProps?.id}
          shrink
          classes={{ root: classes.defaultInputLabel }}
          {...labelProps}
          >
          {label}
          {helptext ? <CustomTooltip helptext={helptext} iconType="help" /> : null}
        </InputLabel>
      )}
      {inputType == 'underline' ?
        <Input
          {...rest}
          {...defaultProps}
        /> :
        <OutlinedInput
          {...rest}
          {...defaultProps}
          labelWidth={150}
          notched={false}
          className={styles}
          classes={{
            root: rootClasses,
            input: classes[`outlinedInput${size}`],
            notchedOutline: notchedOutlineClasses,
            error: classes.outlinedInputError,
            focused: focusedClasses,
            multiline: classes.outlinedInputMultiline,
            inputMultiline: classes.outlinedInputMultilineInput,
            disabled: classes.outlinedInputDisabled,
            ...customInputClass,
          }}
        />}
      {error === false ? null : (
        <FormHelperText id={defaultProps?.id} classes={{ root: classes.errorText }}>
          {errorMessage}
        </FormHelperText>
      )}
    </FormControl>
  );
}

DefaultTextField.defaultProps = {
      inputType: 'outlined'
      }
export default withStyles(textFieldStyles, {withTheme: true})(DefaultTextField);