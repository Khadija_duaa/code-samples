const passwordInputStyles = theme => ({
  profileViewContainer: {
    width: "100%",
    marginTop: "125px",
    [theme.breakpoints.between("xs", "sm")]: {
      width: "92%",
      margin: "0 auto",
      marginTop: 180,
    },
    [theme.breakpoints.between("sm", "md")]: {
      width: 569,
      margin: "0 auto",
      marginTop: 180,
    },
    [theme.breakpoints.between("md", "lg")]: {
      width: "100%",
      marginTop: 125,
      margin: 0,
    },
  },

  // continueBtn: {
  //   background: theme.palette.background.btnBlue,
  //   marginBottom: 26,
  //   fontWeight: 400,
  //   [theme.breakpoints.between("xs", "sm")]: {
  //     width: "100%",
  //     padding: 13,
  //   },
  // },
  link: {
    color: theme.palette.background.btnBlue,
    textDecoration: "underline",
    fontSize: "14px !important",
    fontWeight: 600,
  },
  signInSection: {
    marginTop: 134,
    [theme.breakpoints.between("xs", "sm")]: {
      width: "100%",
      textAlign: "center",
      marginTop: 118,
    },
    [theme.breakpoints.between("md", "lg")]: {
      marginTop: 60,
    },
  },
  profileViewWrapper: {
    width: "480px",
    marginLeft: "119px",
    [theme.breakpoints.between("xs", "sm")]: {
      margin: 0,
      width: "100%",
    },
  },
  imgContainer: {
    width: "30%",
  },
  inputFields: {
    width: "30%",
  },
  gmailInfo: {
    color: theme.palette.text.primary,
    fontSize: "16px !important",
    fontFamily: "poppins",
    fontWeight: 400,
    marginTop: "8px",
    [theme.breakpoints.between("xs", "sm")]: {
      width: "95%",
    },
    "& b": {
      fontSize: "16px !important",
      fontFamily: "poppins",
      fontWeight: 500,
    },
  },
  alreadyAcc: {
    color: theme.palette.text.primary,
    fontSize: "14px !important",
    fontWeight: 400,
    [theme.breakpoints.between("sm", "md")]: {
      textAlign: "center",
    },
  },
  welcomeTonTask: {
    color: theme.palette.text.primary,
    fontSize: "28px !important",
    fontWeight: 600,
  },
  profileViewCnt: {
    marginTop: 32,
    [theme.breakpoints.between("xs", "sm")]: {
      width: "100%",
    },
  },
  profileInputFieldLabel: {
    display: "flex",
    transform: "translate(2px, -17px) scale(1)",
    fontSize: "16px !important",
    alignItems: "center",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.black,
    lineHeight: "5px",
  },
  visibilityOffIcon: {
    "&:hover": {
      background: "none",
    },
  },
  continueBtn: {
    marginBottom: "26px",
    padding: "9px 16px",
    fontSize: "13px !important",
    fontWeight: 400,
    borderRadius: "6px",
    textTransform: "none",
    letterSpacing: "1px",

    [theme.breakpoints.between("xs", "sm")]: {
      fontSize: "14px !important",
      width: "100%",
      height: 48,
    },
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: "14px !important",
      width: "100%",
      height: 48,
    },
  },
  inputFocusCnt: {
    display: "flex",
    marginBottom: 32,
    [theme.breakpoints.between("xs", "sm")]: {
      display: "none",
    },
  },
  inputFocusinnerboxMobile: {
    display: "flex",
    marginBottom: 32,
    [theme.breakpoints.between("md", "xl")]: {
      display: "none",
    },
  },
  inputFocusinnerbox: {
    "& ul": {
      paddingLeft: 22,

      "& li": {
        fontSize: "13px !important",
        fontWeight: 400,
        color: theme.palette.common.black,
        marginBottom: 8,
      },
    },
  },
  inputFocusinnerbox1: {
    "& ul": {
      "& li": {
        fontSize: "13px !important",
        fontWeight: 400,
        color: theme.palette.common.black,
        marginBottom: 8,
      },
    },
  },
  label: {
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.common.black,
    marginBottom: 8,
    display: "flex",
    alignItems: "center",
    color: "#171717",
  },
  icon: {
    fontSize: "10px !important",
    marginRight: 6,
  },
  doneIcon: {
    fontSize: "10px !important",
    marginRight: 6,
    color: "#7E7E7E",
  },
  validateTrueColor: {
    color: "#7E7E7E !important",
  },
  focusedError: {
    color: theme.palette.text.danger
  },
});

export default passwordInputStyles;
