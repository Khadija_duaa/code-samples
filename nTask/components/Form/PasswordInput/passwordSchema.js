import * as yup from 'yup';
import { validatePasswordInput } from '../../../utils/validator/common/password';


export const passwordSchema = yup.object().shape({
    password: validatePasswordInput
  });