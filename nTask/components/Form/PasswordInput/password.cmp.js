import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import { withStyles } from "@material-ui/core/styles";
import DoneIcon from "@material-ui/icons/Done";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import clsx from "clsx";
import React, { useEffect, useState } from "react";
import { injectIntl } from "react-intl";
import { compose } from "redux";
import DefaultTextField from "../TextField";
import passwordInputStyles from "./passwordInput.style";
import { passwordSchema } from "./passwordSchema";
function PasswordInput({
  classes,
  showConfirmPassword,
  onChangeCallback,
  passwordProps,
  confirmPasswordProps,
  passwordValue,
  confirmPasswordValue,
  inputType,
}) {
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState(false);
  const [confirmPassword, setConfirmPassword] = useState("");
  const [focus, setFocus] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [yupPassValidateObj, setYupPassValidateObj] = useState([]);
  const [passwordStrength, setPasswordStrength] = useState([
    {
      label: "Lowercase character",
      value: "lowercase",
    },
    {
      label: "Uppercase character",
      value: "uppercase",
    },
    {
      label: "One number",
      value: "number",
    },
    {
      label: "Special character",
      value: "special",
    },
    {
      label: "8 characters minimum",
      value: "min",
    },
    {
      label: "40 character maxmium",
      value: "max",
    },
  ]);

  const passwordValidate = async (value) => {
    try {
      const results = await passwordSchema.validate(
        {
          password: value,
        },
        { abortEarly: false }
      );
      setYupPassValidateObj([]);
    } catch (error) {
      setYupPassValidateObj(error.errors);
    }
  };

  const handleInputChange = (e, type) => {
    const value = e.target.value.trim();
    switch (type) {
      case "password":
        {
          passwordValidate(value);
          setPassword(value);
        }
        break;
      case "confirmPassword":
        {
          setConfirmPassword(value.trim());
        }
        break;
      default: {
        return false;
      }
    }
    onChangeCallback(e, type);
  };
  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };
  const handleFocus = () => {
    setFocus(true);
  };
  useEffect(() => {
    setPassword(passwordValue);
    setConfirmPassword(confirmPasswordValue);
    passwordValidate(password);
  }, []);
  const handleInputBlur = () => {
    if (!password) {
      setFocus(false);
      return;
    }
    if (password && yupPassValidateObj.length) {
      setPasswordError(true);
    } else {
      setPasswordError(false);
      setFocus(false);
    }
  };
  return (
    <>
      <DefaultTextField
        style={{fontFamily: "poppins, sans-serif !important"}}
        labelProps={{
          classes: {
            root: classes.profileInputFieldLabel,
          },
        }}
        inputType={inputType}
        label="Password"
        errorState={passwordError}
        defaultProps={{
          onChange: (e) => handleInputChange(e, "password"),
          onBlur: (e) => handleInputBlur(),
          onFocus: (e) => {
            handleFocus(e);
          },
          id: "onBoardPasswordInputMain",
          placeholder: "Type your Password",
          value: password,
          inputProps: { maxLength: 40, tabIndex: 2 },
          type: showPassword ? "text" : "password",
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                className={classes.visibilityOffIcon}
                disableRipple={false}
                onClick={handleShowPassword}
                style={{ padding: 0 }}>
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          ),
        }}
        {...passwordProps}
      />
      {focus && (
        <>
          <Grid
            container
            spacing={2}
            className={`${classes.inputFocusCnt} ${classes.inputFocusinnerbox}`}>
            <Grid item xs={12} sm={12} md={6}>
              {passwordStrength.slice(0, 3).map((item) => {
                const validate = !yupPassValidateObj.includes(item.value);
                return (
                  <span
                    className={clsx({
                      [classes.label]: true,
                      [classes.validateTrueColor]: validate,
                      [classes.focusedError]: !validate && passwordError && password,
                    })}>
                    {validate ? (
                      <DoneIcon className={classes.doneIcon} />
                    ) : (
                      <FiberManualRecordIcon className={classes.icon} />
                    )}
                    {item.label}
                  </span>
                );
              })}
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              {passwordStrength.slice(3, 6).map((item) => {
                const validate = !yupPassValidateObj.includes(item.value);
                return (
                  <span
                    className={clsx({
                      [classes.label]: true,
                      [classes.validateTrueColor]: validate,
                      [classes.focusedError]: !validate && passwordError && password,
                    })}>
                    {validate ? (
                      <DoneIcon className={classes.doneIcon} />
                    ) : (
                      <FiberManualRecordIcon className={classes.icon} />
                    )}
                    {item.label}
                  </span>
                );
              })}
            </Grid>
          </Grid>
          <Grid
            container
            spacing={2}
            className={`${classes.inputFocusCnt} ${classes.inputFocusinnerboxMobile}`}>
            <Grid item xs={12} sm={12} md={6}>
              {passwordStrength.map((item) => {
                const validate = !yupPassValidateObj.includes(item.value);
                return (
                  <span
                    className={clsx({
                      [classes.label]: true,
                      [classes.validateTrueColor]: validate,
                      [classes.focusedError]: !validate && passwordError && password,
                    })}>
                    {validate ? (
                      <DoneIcon className={classes.doneIcon} />
                    ) : (
                      <FiberManualRecordIcon className={classes.icon} />
                    )}
                    {item.label}
                  </span>
                );
              })}
            </Grid>
          </Grid>
        </>
      )}
      {showConfirmPassword && (
        <DefaultTextField
          style={{fontFamily: "poppins, sans-serif !important"}}
          labelProps={{
            classes: {
              root: classes.profileInputFieldLabel,
            },
          }}
          label="Confirm Password"
          inputType={inputType}
          formControlStyles={{ marginTop: 32 }}
          defaultProps={{
            onChange: (e) => handleInputChange(e, "confirmPassword"),
            id: "onBoardPasswordInput",
            value: confirmPassword,
            placeholder: "Retype your Password",
            inputProps: { maxLength: 40, tabIndex: 2 },
            type: showPassword ? "text" : "password",
          }}
          {...confirmPasswordProps}
        />
      )}
    </>
  );
}
PasswordInput.defaultProps = {
  showConfirmPassword: false,
  inputType: "outlined",
};
export default compose(
  injectIntl,
  withStyles(passwordInputStyles, { withTheme: true })
)(PasswordInput);
