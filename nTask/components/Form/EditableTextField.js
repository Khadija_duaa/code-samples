import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import EditableTextFieldStyles from "./styles";
import DefaultTextField from "./TextField";
import Typography from "@material-ui/core/Typography";
import helper from "../../helper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

function EditableTextField({
  classes,
  theme,
  title,
  label,
  module,
  formStyles,
  handleEditTitle,
  editTitle,
  titleProps,
  defaultProps,
  textFieldProps,
  textFieldIcons,
  textFieldIconsProps,
  inputEl,
  permission,
  placeholder,
}) {
  const [textFieldValue, setTextFieldValue] = useState(title);
  const [titleEdit, setTitleEdit] = useState(false);
  const [errorState, setErrorState] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleTitleClick = () => {
    setTitleEdit(!titleEdit);
    setTextFieldValue(titleEdit ? "" : title);
  };

  const handleChangeTitle = e => {
    //   changing the title
    setTextFieldValue(e.target.value);
    setErrorState(false);
    setErrorMessage("");
  };

  const handleKeyDown = e => {
    //   function calls onKeyDown event and checks the validation
    if (e.keyCode === 13 && titleValidator()) {
      handleUpdateTitle(); /** on Enter click */
    } else if (e.keyCode === 27) {
      /** on ESC click */
      setErrorState(false);
      setErrorMessage(false);
      setTextFieldValue(textFieldValue);
      handleEditTitle(textFieldValue);
      setTitleEdit(false);
    }
  };

  const handleUpdateTitle = () => {
    handleEditTitle(textFieldValue);
    setTitleEdit(false);
  };

  const titleValidator = () => {
    /** validation for the title field */
    const response1 = helper.HELPER_EMPTY(`${module}`, textFieldValue);
    const response2 = helper.HELPER_CHARLIMIT250(`${module}`, textFieldValue);
    const response3 = helper.RETURN_CECKSPACES(textFieldValue, `${module}`);
    let error = null;
    if (response1) {
      error = response1;
    } else if (response2) {
      error = response2;
    } else if (response3) {
      error = response3;
    }
    if (error) {
      setErrorState(true);
      setErrorMessage(error);
      return false;
    }
    setErrorState(false);
    setErrorMessage("");
    return true;
  };

  useEffect(() => {
    setTitleEdit(editTitle);
  }, [editTitle]);

  useEffect(() => {
    setTextFieldValue(title);
  }, [title]);

  return (
    <>
      {titleEdit ? (
        <ClickAwayListener onClickAway={() => handleKeyDown({ keyCode: 27 })}>
          <div className={classes.textFieldCnt}>
            <DefaultTextField
              label={label}
              size="small"
              error={errorState}
              errorState={errorState}
              errorMessage={errorMessage}
              formControlStyles={{
                marginBottom: 0,
                ...formStyles,
              }}
              defaultProps={{
                type: "text",
                id: "title",
                placeholder: "",
                value: textFieldValue,
                autoFocus: true,
                autoComplete: "off",
                onChange: e => handleChangeTitle(e),
                onKeyDown: e => handleKeyDown(e),
                ...defaultProps,
              }}
              outlineCnt={false}
              transparentInput={true}
              customInputClass={{
                input: classes.outlinedInputsmall,
                root: classes.outlinedInputCnt,
              }}
              {...textFieldProps}
            />
          </div>
        </ClickAwayListener>
      ) : (
        <>
          <Typography
            variant="h2"
            title={textFieldValue}
            onClick={() => permission && handleTitleClick()}
            className={classes.textFieldTitle}
            {...titleProps}>
            {textFieldValue}
            {placeholder && textFieldValue === "" && (
              <span style={{ color: "#7E7E7E" }}>{placeholder}</span>
            )}
            {textFieldIcons && (
              <div className={classes.icons} {...textFieldIconsProps}>
                {textFieldIcons}
              </div>
            )}
            <input
              value={textFieldValue}
              type="text"
              style={{ opacity: "0", position: "absolute", left: 0, cursor: "pointer" }}
              {...inputEl}
            />
          </Typography>
        </>
      )}
    </>
  );
}
EditableTextField.defaultProps = {
  classes: {},
  theme: {},
  title: "",
  module: "",
  label: "",
  formStyles: {},
  handleEditTitle: () => {},
  editTitle: false,
  textFieldProps: {},
  textFieldIconsProps: {},
  defaultProps: {},
  inputEl: {},
  textFieldIcons: null,
  permission: true,
  placeholder: null,
};
export default withStyles(EditableTextFieldStyles, { withTheme: true })(EditableTextField);
