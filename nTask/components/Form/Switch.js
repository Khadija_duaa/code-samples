import React, { Component } from "react";
import Switch from "@material-ui/core/Switch";
import switchStyles from "../../assets/jss/components/switch";
import { withStyles } from "@material-ui/core/styles";
import clsx from "clsx";
class DefaultSwitch extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }


  render() {
    const { classes, color, size = 'medium', isd } = this.props;
    const switchClasses = {
      root: clsx({
        [classes.root]: true, [classes.rootSmall]: size === "small",
        [classes.rootSwitchMedium]: size === "medium",
        [classes.rootSwitch]: size !== "medium"
      }),
      switchBase: clsx({
        [classes.iOSSwitchBaseSmall]: size === "small",
        [classes.iOSSwitchBase]: size !== "medium",
        [classes.iOSSwitchBaseMedium]: size === "medium",
      }),
      track: clsx({
        [classes.iOSBarSmall]: size === "small" && color !== 'dark',
        [classes.iOSBarSmallDark]: size === "small" && color == 'dark',
        [classes.iOSBar]: size !== "medium",
        [classes.iOSBarMedium]: size === "medium",
      }),
      thumb: classes.iOSIcon,
      colorSecondary: classes.secondaryHover,
      checkedIcon: classes.iOSIconChecked,
      checked: clsx({ [classes.iOSChecked]: true, [classes.iOSCheckedSmall]: size === 'small' }),
    };
    return <Switch {...this.props} classes={switchClasses} disableRipple={true} />;
  }
}

export default withStyles(switchStyles)(DefaultSwitch);
