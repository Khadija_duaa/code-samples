import React, { Component } from "react";
import formStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import CheckBoxCheckIcon from "../Icons/CheckboxCheckedIcon";
import CheckBoxUncheckIcon from "../Icons/CheckboxUnCheckedIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";

class DefaultCheckbox extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const {
      classes,
      label,
      checked,
      onChange,
      styles,
      checkboxStyles,
      unCheckedIconProps,
      checkedIconProps,
      color,
      disabled,
      labelStyle,
      ...rest
    } = this.props;
    return (
      <FormControlLabel
        style={{ ...styles }}
        checked={checked}
        onChange={onChange}
        classes={{ root: classes.formControlLabel }}
        control={
          <Checkbox
            className={classes.checkbox}
            classes={{ disabled: classes.checkboxDisabled }}
            style={{ ...checkboxStyles }}
            icon={
              <SvgIcon viewBox="0 0 21 21" {...unCheckedIconProps}>
                <CheckBoxUncheckIcon />
              </SvgIcon>
            }
            checkedIcon={
              <SvgIcon viewBox="0 0 21 21" {...checkedIconProps}>
                <CheckBoxCheckIcon color={color} />
              </SvgIcon>
            }
            value="checkedH"
            disabled={disabled}
            {...rest}
          />
        }
        label={
          <Typography variant="body2" style={labelStyle}>
            {label}
          </Typography>
        }
      />
    );
  }
}
DefaultCheckbox.defaultProps = {
  classes: {},
  label: "",
  checked: false,
  onChange: () => {},
  styles: {},
  checkboxStyles: {},
  labelStyle: {},
};

DefaultCheckbox.defaultProps = {
  color: "#7cb147",
  disabled: false,
};
export default withStyles(formStyles)(DefaultCheckbox);
