// @flow

import React, { useState, useEffect } from "react";
import DefaultTextField from "../TextField";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../../Buttons/CustomButton";
import { validateTime } from "../../../utils/validator/common/validateTime";
import weeklyCapacityStyles from "./style";
import Hotkeys from "react-hot-keys";

type DropdownProps = {
  classes: Object,
  theme: Object,
  label: String,
  onInputChange: Function,
  actualCapacity: String,
  onInputBlur: Function,
  placeholder: String,
  initial: String,
  permission: Boolean
};

//Onboarding Main Component
function TimeInput(props: DropdownProps) {
  const {
    classes,
    theme,
    label,
    onInputChange,
    onInputBlur,
    actualCapacity,
    initial,
    permission
  } = props;
  const [capacity, setCapacity] = useState(actualCapacity);
  const [editable, setEditable] = useState(false);
  const handleInput = (event) => {
    // Function handles input
    let inputValue = event.target.value;
    if (validateTime(inputValue).validated) {
      // Function handles input

      let value = inputValue ? parseInt(inputValue) : "";

      setCapacity(value); // set capacity value
      onInputChange(value); // input change in parent
    }
  };
  const handleCapacityClick = (event) => {
    //Edit capacity of a resource
    event.stopPropagation();
    setEditable(true);
  };
  const handleInputBlur = () => {
    //Handle blur funtion of input
    if (capacity == "") setCapacity(0); // set capacity value to 0 if empty
    setEditable(false);
    onInputBlur(capacity);
  };
  const onKeyDown = (keyName, e, handle) => {
    if (keyName == "enter") {
      onInputChange(capacity);
      onInputBlur(capacity);
      setEditable(false);
      if (capacity == "") setCapacity(0); // set capacity value to 0 if empty
    }
  };
  useEffect(() => {
    setCapacity(actualCapacity);
  }, [actualCapacity]);
  return (
    <div style={{ position: "relative", maxWidth: 107, height: 28 }}>
      <span
        onClick={!editable && permission ? handleCapacityClick : null}
        kl
        className={
          editable ? classes.cpacityValueMiniCnt : classes.cpacityValueCnt
        }
        style={{}}
      >
        {!editable ? (
          <span className={classes.capacityValue}> {capacity}</span>
        ) : null}
        <span className={classes.hoursLabel}>{initial}</span>
      </span>
      {editable ? (
        <Hotkeys keyName="enter" onKeyDown={onKeyDown}>
          <DefaultTextField
            label={label}
            size="small"
            error={false}
            formControlStyles={{ marginBottom: 0, maxWidth: 107 }}
            defaultProps={{
              id: "weeklyCapacity",
              onChange: handleInput,
              autoFocus: true,
              onBlur: handleInputBlur,
              value: capacity,
              inputProps: {
                style: {
                  fontFamily: theme.typography.fontFamilyLato,
                  fontWeight: 500,
                  fontSize: "13px",
                },
              },
            }}
          />
        </Hotkeys>
      ) : null}
    </div>
  );
}
TimeInput.defaultProps = {
  classes: {},
  theme: {},
  label: "",
  initial: "h",
  onInputChange: () => {},
  actualCapacity: 40,
  onInputBlur: () => {},
  permission: true
};
export default withStyles(weeklyCapacityStyles, { withTheme: true })(TimeInput);
