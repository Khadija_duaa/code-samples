import React, { Component } from "react";
import formStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import CheckBoxCheckIcon from "../Icons/CheckboxCheckedIcon";
import CheckBoxUncheckIcon from "../Icons/CheckboxUnCheckedIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import StarIcon from "@material-ui/icons/StarRate";

class StarredCheckBox extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, label, checked, onChange, styles, checkboxStyles, theme } = this.props;
    return (
      <FormControlLabel
        style={{ ...styles, opacity: checked ? 1 : ""}}
        checked={checked}
        onChange={onChange}
        onClick={(event) => event.stopPropagation()}
        classes={{root: classes.formControlLabel}}
        control={
          <Checkbox
          className={classes.checkbox}
          style={{...checkboxStyles , padding : 0}}
            icon={
                <StarIcon
                htmlColor={theme.palette.secondary.light}
            />
            }
            checkedIcon={
                <StarIcon
                htmlColor={theme.palette.background.star}
            />
            }
            value="checkedH"
          />
        }
        label={<Typography variant="body2">{label}</Typography>}
      />
    );
  }
}

export default withStyles(formStyles, {withTheme: true})(StarredCheckBox);
