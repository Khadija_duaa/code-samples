// @flow

import React, { useState, useEffect } from "react";
import DefaultTextField from "../TextField";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../../Buttons/CustomButton";
import { validateWeeklyCapacity } from "../../../utils/validator/common//weeklyCapacity";
import weeklyCapacityStyles from "./style";
import Hotkeys from "react-hot-keys";

type DropdownProps = {
  classes: Object,
  theme: Object,
  label: String,
  onInputChange: Function,
  actualCapacity: String,
  onInputBlur: Function,
  placeholder: String,
  permission: Boolean
};

//Onboarding Main Component
function WeeklyCapcityInput(props: DropdownProps) {
  const {
    classes,
    theme,
    label,
    onInputChange,
    onInputBlur,
    actualCapacity,
    permission
  } = props;
  const [capacity, setCapacity] = useState(actualCapacity);
  const [editable, setEditable] = useState(false);
  const handleInput = (event) => {
    // Function handles input
    let inputValue = event.target.value;
    if (validateWeeklyCapacity(inputValue).validated) {
      // Function handles input

      let value = inputValue ? parseInt(inputValue) : "";

      setCapacity(event.target.value); // set capacity value
      onInputChange(event.target.value); // input change in parent
    }
  };
  const handleCapacityClick = () => {
    //Edit capacity of a resource
    setEditable(true);
  };
  const handleInputBlur = () => {
    //Handle blur funtion of input
    if (capacity == "") setCapacity(0); // set capacity value 0 if emty string
    setEditable(false);
    onInputBlur(capacity);
  };
  const onKeyDown = (keyName, e, handle) => {
    if (keyName == "enter") {
      onInputChange(capacity);
      onInputBlur(capacity);
      setEditable(false);
      if (capacity == "") setCapacity(0); // set capacity value 0 if emty string
    }
  };
  return (
    <div style={{ position: "relative", width: 70, height: 28 }}>
      <span
        onClick={!editable && permission ? handleCapacityClick : null}
        kl
        className={
          editable ? classes.cpacityValueMiniCnt : classes.cpacityValueCnt
        }
        style={{}}
      >
        {!editable ? (
          <span className={classes.capacityValue}> {capacity}</span>
        ) : null}
        <span className={classes.hoursLabel}>h</span>
      </span>
      {editable ? (
        <Hotkeys keyName="enter" onKeyDown={onKeyDown}>
          <DefaultTextField
            label={label}
            size="small"
            error={false}
            formControlStyles={{ marginBottom: 0, maxWidth: 70 }}
            defaultProps={{
              id: "weeklyCapacity",
              onChange: handleInput,
              autoFocus: true,
              onBlur: handleInputBlur,
              value: capacity,
              inputProps: {
                style: {
                  fontFamily: theme.typography.fontFamilyLato,
                  fontWeight: 500,
                  fontSize: "13px",
                },
              },
            }}
          />
        </Hotkeys>
      ) : null}
    </div>
  );
}
WeeklyCapcityInput.defaultProps = {
  classes: {},
  theme: {},
  label: "",
  onInputChange: () => {},
  actualCapacity: 40,
  onInputBlur: () => {},
  permission: false
};
export default withStyles(weeklyCapacityStyles, { withTheme: true })(
  WeeklyCapcityInput
);
