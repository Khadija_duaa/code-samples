const weeklyCapacityStyles = (theme) => ({
  hoursLabel: {
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
  },
  placeholder: {
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
  },
  cpacityValueCnt: {
    display: "inline-block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontFamily: theme.typography.fontFamilyLato,
    borderRadius: 4,
    position: "absolute",
    right: 0,
    // zIndex: 111,
    textAlign: "left",
    cursor: "pointer",
    "&:hover": {
      background: theme.palette.background.items,
    },
    padding: 7,
    minWidth: 70,
  },
  cpacityValueMiniCnt: {
    display: "inline-block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    transition: "ease all 0.2s",
    fontFamily: theme.typography.fontFamilyLato,
    borderRadius: 4,
    position: "absolute",
    right: 0,
    zIndex: 111,
    textAlign: "left",
    padding: 7,
    minWidth: 20,
  },
  capacityValue: {
    fontSize: "13px !important",
    fontWeight: 500,
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    marginRight: 3,
  },
});

export default weeklyCapacityStyles;
