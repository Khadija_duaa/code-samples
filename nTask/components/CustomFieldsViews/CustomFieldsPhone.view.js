import React, { useState, useRef, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomTooltip from "../../components/Tooltip/Tooltip";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import CopyIcon from "../../components/Icons/IconCopyContent";
import IconPhone from "../../components/Icons/IconPhone";

function CustomFieldsPhone({
  classes,
  theme,
  customFieldss,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  userPreferenceState,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const anchorEl = useRef(null);
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);
  const [phone, setPhone] = useState(currentField ? currentField.fieldData.data : "");
  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery:""
    });
  };

  const handleChange = num => {
    setPhone(`+${num}`);
  };
  const onBlur = e => {
    customFieldChange(phone, field, field.settings);
  };
  const onKeyDown = e => {
    if (e.keyCode === 13) {
      /** on Enter click */
      // customFieldChange(phone, field, field.settings);
    } else if (e.keyCode === 27) {
      /** on ESC click */
      setPhone(currentField ? currentField.fieldData.data : "");
    }
  };
  const handleCopyClick = e => {
    e.stopPropagation();
    anchorEl.current.select();
    anchorEl.current.setSelectionRange(0, 99999);
    document.execCommand("copy");
  };
  const handleCall = e => {
    e.stopPropagation();
    window.open(`tel:+${phone}`, "_blank");
  };

  useEffect(()=>{
    if(currentField) setPhone(currentField.fieldData.data)
  },[currentField])

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <PhoneInput
        country={field.settings.country}
        value={phone}
        onChange={handleChange}
        onBlur={onBlur}
        onKeyDown={onKeyDown}
        disableDropdown={true}
        disableCountryCode={false}
        autoFormat={false}
        countryCodeEditable={false}
        containerClass={classes.phoneCnt}
        inputClass={classes.phoneInputCnt}
        buttonClass={classes.selectedFlag}
        disabled={!permission || disabled}
      />
      <div className={classes.copyIconCnt}>
        <CustomTooltip
          helptext={"Call"}
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <SvgIcon
            viewBox="0 0 16 17.778"
            fontSize="inherit"
            className={classes.callIcon}
            onClick={handleCall}>
            <IconPhone color={"#999"} />
          </SvgIcon>
        </CustomTooltip>
        <CustomTooltip
          helptext={"Copy"}
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <SvgIcon
            className={classes.copyIconPhone}
            viewBox="0 0 14 16.211"
            onClick={handleCopyClick}>
            <CopyIcon />
          </SvgIcon>
        </CustomTooltip>
      </div>
      <input
        value={phone}
        type="text"
        style={{ opacity: "0", position: "absolute", left: 0, zIndex: -1 }}
        ref={anchorEl}
      />
    </div>
  );
}

CustomFieldsPhone.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
    userPreferenceState: state.userPreference,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldsPhone);
