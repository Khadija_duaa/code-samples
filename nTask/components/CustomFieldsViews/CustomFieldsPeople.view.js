import React, { useState, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import "react-phone-input-2/lib/style.css";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import AssigneeDropdown from "../../components/Dropdown/AssigneeDropdown2";
import { getMembersById } from "../../helper/getMembersById";

function CustomFieldsPeople({
  classes,
  theme,
  customFieldss,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  userPreferenceState,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);

  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery:""
    });
  };
  const updateAssignee = (assignee, obj) => {
    const idArr = assignee.map(assig => assig.userId);
    customFieldChange(idArr, field, field.settings);
  };

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <AssigneeDropdown
        assignedTo={currentField ? getMembersById(currentField.fieldData.data) : []}
        updateAction={updateAssignee}
        isArchivedSelected={false}
        obj={{}}
        style={{ marginTop: 10, marginBottom: 10 }}
        permission={false}
        assigneeIcon={false}
        label={<span style={{color : "#7E7E7E"}}>Select</span>}
        singleSelect={!field.settings.multiplePeople}
        customChipProps={{
          className: classes.chipRootClass,
        }}
        customBtnProps={{
          customClasses: {
            contained: classes.btnStylePeople,
          },
          iconBtn: false,
        }}
        isAllowDelete={permission && !disabled}
        isAllowAdd={permission && !disabled}
      />
    </div>
  );
}

CustomFieldsPeople.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
    userPreferenceState: state.userPreference,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldsPeople);
