const CustomFieldsDropdownsStyles = theme => ({
  mainCustom: {
    display: "flex",
    alignItems: "flex-start",
  },
  customFieldsCnt: {
    marginBottom: 20,
  },
  readOnlyText: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "12px 14px",
    fontSize: "12px !important",
    cursor: "text",
  },
  dispalyDropdown:{
    background: "rgba(255, 255, 255, 1)",
    boxShadow: "0px 1px 5px 0px rgb(0 0 0 / 20%) 0px 2px 2px 0px rgb(0 0 0 / 14%) 0px 3px 1px -2px rgb(0 0 0 / 12%)",
    display:"block",
    positon:"absolute",
    left:"7rem",
    top:"3rem"
  },
  customDropDownMenu: {
    boxShadow: "0px 2px 6px #00000026",
    borderRadius: "6px",
  },
  hide:{
    display:"none"
  },
  customFieldButtonStyle: {
    display: "flex",
    alignItems: "baseline",
  },
  customIcon: {
    marginRight: "5px",
    marginTop: "4px",
  },
  workSpacesContainer:{
    position: "absolute",
    left: 78,
    cursor: "pointer",
    margin: 0,
    padding: 0,
    top: "0",
    width: "100%",
    background: "white"
  },
  unorderList:{
    padding:"5px",
    boxShadow: "inset 0px 1px 5px 0px rgb(0 0 0 / 7%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 3px 1px -2px rgb(0 0 0 / 12%)",
  },
  listStyling:{
    width: "100%",
    padding: "6px"
  },
  customDropDownListItem: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    padding: "6px 15px",
    cursor: "pointer",
  },
  customDropDownListItemCalculation: {
    display: "flex",
    flexDirection: "column",
    alignItems: "end",
    padding: "0px",
    cursor: "pointer",
  },
  customDropDownText: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.black,
    marginLeft: "8px",
    fontWeight: "600",
  },
  hideDdItem: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.black,
    // marginLeft: "8px",
    fontWeight: "600",
  },
  customDropDownIcon: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    padding: "7px 0px"
  },
  customDropDownIconCalculation: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    padding: "4px 10px",
  },
  customDropDownIcon1: {
    display: "flex",
    alignItems: "center",
    marginTop: "16px",
    cursor: "pointer",
  },
  checklistCnt: {
    display: "flex",
  },
  customEmailAdornment: {
    display: "flex",
  },
  customAdornmentButton: {
    display: "none",
    "&:hover": {
      background: "none",
    },
  },
  customAdornmentButton1: {
    display: "none",
    "&:hover": {
      background: "none",
    },
  },
  editInputCnt: {
    flex: "1",

    "&:hover  $customAdornmentButton": {
      display: "block",
    },

    "&:hover $customAdornmentButton1": {
      display: "block",
    },
  },
  customtooltip: {
    background: "red",
  },
  btnStyles: {
    width: "180px",
    flex: "0 0 auto",
    height: 32,
    padding: "9px 0px",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    cursor: "pointer",
    color: "#505050",
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    textTransform: "none",
    "&:hover": {
      backgroundColor: "transparent",
    }
    // backgroundColor: "transparent !important",
    // "&:hover g": {
    //   stroke: "rgba(104, 104, 104, 1) !important",
    // },
    // "&:hover path": {
    //   fill: "rgba(104, 104, 104, 1) !important",
    // },
  },
  iconStyle: {
    marginRight: 5,
    marginTop: 8,
    marginBottom: 3,
  },
  editFieldIcon: {
    fontSize: "13px !important",
    marginRight: 5,
    color: theme.palette.background.btnBlue,
  },
  textFieldCnt: {
    display: "flex",
    flexDirection: "row",
  },
  fieldTitle: {
    height: 32,
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    padding: "9px 0px 10px 7px",
    // height: "fit-content",
    // marginRight: 10,
    fontWeight: `${theme.typography.fontWeightExtraLight} !important`,
    borderRadius: 4,
    color: "#171717",
    width: "100%",
    // marginLeft:"6px",
    flex: 1,
    position: "relative",
    "&:hover": {
      backgroundColor: "#F6F6F6",
    },

    "&:hover $iconCnt": {
      visibility: "visible",
    },
  },
  textField: {
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    height: 32,
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    padding: "9px 0px 10px 7px",
    // height: "fit-content",
    // marginRight: 10,
    fontWeight: `${theme.typography.fontWeightExtraLight} !important`,
    borderRadius: 4,
    color: "#171717",
    width: "700px",
    // marginLeft:"6px",
    flex: 1,
    position: "relative",
    "&:hover": {
      backgroundColor: "#F6F6F6",
    },

    "&:hover $iconCnt": {
      visibility: "visible",
    },
  },
  emailField: {
    height: 32,
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    padding: "9px 0px 10px 7px",
    // height: "fit-content",
    // marginRight: 10,
    fontWeight: `${theme.typography.fontWeightExtraLight} !important`,
    borderRadius: 4,
    color: theme.palette.background.blue,
    width: "100%",
    // marginLeft:"6px",
    flex: 1,
    position: "relative",
    "&:hover": {
      backgroundColor: "#F6F6F6",
    },

    "&:hover $iconCnt": {
      visibility: "visible",
    },
  },
  outlinedInputCnt: {
    height: "32px",
    borderRadius: 4,
    "&:hover": {
      backgroundColor: "#F6F6F6",
    },
  },
  outlinedInputsmall: {
    fontSize: "13px !important",
    height: "32px",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
    padding: "9px 0px 10px 7px",
    color: "#171717",
  },
  outlinedInputMoneyField: {
    fontSize: "13px !important",
    height: "32px",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
    padding: "9px 0px 10px 2px",
    color: "#171717",
  },
  deleteFieldIcon: {
    fontSize: "15px !important",
    marginRight: 2,
    // color: theme.palette.background.btnBlue,
  },
  copyIcon: {
    fontSize: "16px !important",
    color: "#999",
    cursor: "pointer",
    "&:hover": {
      color: theme.palette.background.blue,
    },
  },
  copyIconPhone: {
    fontSize: "16px !important",
    color: "#999",
    cursor: "pointer",
    top: 8,
    position: "absolute",
    right: 8,
    "&:hover": {
      color: theme.palette.background.blue,
    },
  },
  callIcon: {
    fontSize: "17px !important",
    color: "#999",
    cursor: "pointer",
    top: 9,
    position: "absolute",
    right: 32,
    // marginRight: 8,
    "&:hover": {
      color: theme.palette.background.blue,
    },
  },
  copyIconCnt: {
    visibility: "hidden",
    position: "relative",
    "&:hover": {
      visibility: "visible",
    },
  },
  editIcon: {
    fontSize: "16px !important",
    color: "#999",
    cursor: "pointer",
    marginRight: 7,
    "&:hover": {
      color: theme.palette.background.blue,
    },
  },
  formulaEditIcon: {
    fontSize: "16px !important",
    color: "#999",
    cursor: "pointer",
    marginRight: 7,
    visibility: "hidden",
    "&:hover": {
      color: theme.palette.background.blue,
    },
    position: "absolute",
    right: 5,
  },
  iconCnt: {
    visibility: "hidden",
    position: "absolute",
    top: 7,
    right: 7,
  },
  datePickerCustomStyle: {
    padding: "9px 0px 10px 8px !important",
    height: "32px !important",
    width: "100% !important",
    flex: "1 1 0% !important",
    display: "flex !important",
    justifyContent: "space-between !important",
    border: "none !important",
    background: "transparent !important",
    borderRadius: "4px !important",
    "&:hover": {
      background: `${theme.palette.background.items} !important`,
    },
  },
  datePickerContainer: {
    flex: 1,
    position: "absolute",
    top: 0,
    width: "100%",
    display: "block",
    left: 0,
  },
  moneyIconAdorned: {
    paddingLeft: 8,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
  },
  endornmentEndClass: {
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
  },
  phoneCnt: {
    flex: 1,
    height: 32,
    borderRadius: 4,
    "&:hover": {
      backgroundColor: "#F6F6F6",
    },
    "&:hover + $copyIconCnt": {
      visibility: "visible",
    },
  },
  phoneInputCnt: {
    border: "none !important",
    width: "100% !important",
    paddingLeft: "38px !important",
    background: "transparent !important",
  },
  selectedFlag: {
    border: "none !important",
    background: "transparent !important",
    bottom: "-1px !important",
  },
  textAreaCnt: {
    flex: 1,
    padding: "0px 10px 0px 0px",
    "& .fr-box": {
      border: "none !important",
    },
    "& .fr-wrapper": {
      "&:hover":{
        background: "#F6F6F6 !important",
        borderRadius: 4
      },
    },
    "& .fr-second-toolbar": {
      display: "none !important",
    },
    "& .fr-element": {
      padding: "5px 5px !important",
      // "&:hover": {
      //   background: "#F6F6F6",
      //   borderRadius: 4
      // },
      "& p": {
        background: "transparent !important",
      },
      "& span": {
        background: "transparent",
      },
    },
  },
  btnStyle: {
    marginTop: "0px !important",
    marginBottom: "0px !important",
    boxShadow: "none !important",
    border: "none !important",
    flex: "1 !important",
    background: "transparent !important",
    fontFamily: `${theme.typography.fontFamilyLato} !important`,
    fontWeight: `${theme.typography.fontWeightExtraLight} !important`,
    padding: "1px 8px !important",
    fontSize: "13px !important",
    display: "flex !important",
    justifyContent: "end !important",
    "&:hover": {
      background: "#F6F6F6 !important",
    },
  },
  btnStylePeople: {
    marginTop: "0px !important",
    marginBottom: "0px !important",
    boxShadow: "none !important",
    border: "none !important",
    flex: "1 !important",
    background: "transparent !important",
    fontFamily: `${theme.typography.fontFamilyLato} !important`,
    fontWeight: `${theme.typography.fontWeightExtraLight} !important`,
    padding: "1px 8px !important",
    fontSize: "13px !important",
    display: "flex !important",
    justifyContent: "start !important",
    "&:hover": {
      background: "#F6F6F6 !important",
    },
  },
  chipRootClass: {
    width: '100%',
    padding: 3,
    height: 30,
    background: "transparent",
    border: "1px solid #0000001F",
    // marginRight: 5,
    margin: "2px 6px 3px 0px",
    "&:focus": {
      background: theme.palette.background.medium,
    },
  },
  ratingContainer: {
    flex: 1,
    minHeight: 32,
    "&:hover": {
      background: "#F6F6F6 ",
    },
    borderRadius: 4,
    cursor: "pointer",
    padding: "0px 8px",
    display: "flex",
    alignItems: "center",
  },
  formulaContainer: {
    flex: 1,
    minHeight: 32,
    "&:hover": {
      background: "#F6F6F6 ",
    },
    "&:hover $formulaEditIcon": {
      visibility: "visible",
    },
    borderRadius: 4,
    cursor: "pointer",
    padding: "0px 8px",
    display: "flex",
    alignItems: "center",
    position: "relative",
  },
  resultValue: {
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    color: "#505050",
  },
  formulaCnt: {
    padding: 15,
  },
  fieldsCnt: {
    display: "flex",
    justifyContent: "space-between",
  },
  btnContainer: {
    paddingTop: 20,
    background: "transparent",
    // borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    // borderRadius: "0 0 6px 6px",
  },
  icon: {
    fontSize: "18px !important",
    marginRight: 5,
    marginBottom: -4,
    color: "#505050",
  },
  iconDivide: {
    fontSize: "14px !important",
    marginRight: 7,
    marginBottom: -4,
    color: "#505050",
    // "& path": {
    //   fill: "#7e7e7e",
    // },
  },
  attachmentCapsule:{
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 20,
    padding: "5px 10px",
    marginRight: 5,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    marginBottom: 3,
    display: "flex"
  },
  dropdownIndicatorIcon:{
    marginBottom: "-5px",
    position: "absolute",
    right: "3px",
    transform: "rotate(-90deg)"
  },
  attachmentCnt:{
    flex: 1,
    minHeight: 32,
    "&:hover": {
      background: "#F6F6F6 ",
    },
    borderRadius: 4,
    cursor: "pointer",
    padding: "0px 8px",
    display: "flex",
    alignItems: "center",
    position: "relative",
    flexWrap: "wrap"
  },
  labelSpan:{
    // width: "95%",
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    color:"#505050",
  },
  deleteAttachmentIcon:{
    fontSize: "15px !important",
    marginLeft: 7,
    color: "#999999",
    "&:hover":{
      color: "#ff4a4a"
    }
  },
  outlinedInputDisabled:{
    background: "transparent"
  },

  /* Extra small devices (phones, 600px and down and tablet) */
  "@media only screen and (max-width: 1024px)": {
    textFieldCnt: {
      display: "block",
    },
    checklistCnt:{
      display: "block",
    }
  },
});

export default CustomFieldsDropdownsStyles;
