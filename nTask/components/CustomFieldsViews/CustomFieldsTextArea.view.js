import React, { useState, useRef, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import DescriptionEditor from "../TextEditor/CustomTextEditor/DescriptionEditor/DescriptionEditor";

function CustomFieldsTextArea({
  classes,
  customFields,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);

  const [editorDetail, setEditorDetail] = useState(currentField ? currentField.fieldData.data : "");
  const editorDetailRef = useRef(currentField ? currentField.fieldData.data : "");
  const handleSuccessDelete = () => {
    handleDeleteField(field);
    updateDeleteDialogState({ btnQuery: "progress" });
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery: "",
    });
  };

  const handleUpdate = data => {
    setEditorDetail(data.html);
    editorDetailRef.current = data.html;
  };
  const handleClickAway = () => {
    customFieldChange(
      window.btoa(unescape(encodeURIComponent(editorDetailRef.current))),
      field,
      field.settings
    );
  };

  useEffect(() => {
    if (currentField) setEditorDetail(currentField.fieldData.data);
  }, [currentField]);

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <div className={classes.textAreaCnt}>
        <DescriptionEditor
          id={field.fieldName}
          defaultValue={editorDetail}
          onChange={handleUpdate}
          handleClickAway={handleClickAway}
          placeholder={"Enter here"}
          disableEdit={!permission || disabled}
        />
      </div>
    </div>
  );
}

CustomFieldsTextArea.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  handleSelectHideOption: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldsTextArea);
