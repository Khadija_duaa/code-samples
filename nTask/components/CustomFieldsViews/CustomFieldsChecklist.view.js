import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import Checklist from "../../components/Checklist/Checklist";
import customFieldStyles from "./CustomFields.style";
import useFields from "./useFields.hook";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";

function CustomFieldsDropdowns({
  classes,
  customFields,
  profile,
  groupType,
  handleClickHideOption,
  permission,
  groupId,
  isSystem,
  isArchivedSelected,
  customfieldlabelProps,
  renderFieldsWithoutSec = false,
  handleEdit,
  handleCopyField,
  updateDeleteDialogState,
  handleDeleteField,
  handleSelectHideOption,
  sectionId="",
  data
}) {
  const { fields } = useFields("checklist", groupType, customFields, profile);
  let filterFields = [];
  if (renderFieldsWithoutSec) {
    filterFields = data.filter(d=> d.fieldType === "checklist");
  } else {
    let checklists = fields.reduce((result, cv)=>{
      let isExist = cv.sectionId ? cv.sectionId.find(ele => ele === sectionId) : false;
      if ((isExist || cv.isSystem === isSystem) && cv.fieldType === "checklist") result.push(cv);
      return result;
    },[])
    filterFields = checklists.filter(cl => cl.isSystem || (cl.isSystem !== false || (cl.sectionId !== null && cl.sectionId.indexOf(sectionId) >= 0)));
  }

  const handleSuccessDelete = f => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(f);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = f => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: () => handleSuccessDelete(f),
      closeAction: handleClose,
      btnQuery:""
    });
  };

  return (
    <>
      {filterFields.map(f => {
        return (
          <div className={classes.checklistCnt}>
            <CustomFieldLabelCmp
              iconType="checklist"
              label={f.fieldName}
              handleEdit={() => handleEdit(f)}
              handleCopy={() => handleCopyField(f)}
              handleDelete={() => handleDelete(f)}
              handleSelectHideOption={option => handleSelectHideOption(option, f)}
              {...customfieldlabelProps}
            />
            <Checklist
              obj={{ ...f, groupId: groupId }}
              todoItems={[]}
              handleClickHideOption={handleClickHideOption}
              permission={permission}
              groupType={groupType}
              isArchivedSelected={isArchivedSelected}
            />
          </div>
        );
      })}
    </>
  );
}

CustomFieldsDropdowns.defaultProps = {
  classes: {},
  theme: {},
  customfieldlabelProps: {},
  groupType: "",
  handleClickHideOption: () => {},
  permission: {},
  isSystem: true,
  isArchivedSelected: false,
  renderFieldsWithoutSec: false,
  sectionId: "",
  data:[]
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldsDropdowns);
