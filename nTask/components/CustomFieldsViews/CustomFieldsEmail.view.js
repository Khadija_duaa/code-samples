import React, { useRef, useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import SvgIcon from "@material-ui/core/SvgIcon";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import EditableTextField from "../Form/EditableTextField";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import CopyIcon from "../../components/Icons/IconCopyContent";
import IconEdit from "../../components/Icons/IconEdit";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import isEmail from "validator/lib/isEmail";

function CustomFieldEmail({
  classes,
  theme,
  customFields,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);

  const anchorEl = useRef(null);
  const [editEmail, setEditEmail] = useState(false);
  const [errorState, setErrorState] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [email, setEmail] = useState("");

  const handleSaveInputValue = () => {
    if (isEmail(email) || email == "") {
      customFieldChange(email, field, field.settings);
      setEditEmail(false);
    } else {
      setErrorState(true);
      setErrorMessage("Please enter valid email!");
    }
  };
  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery: "",
    });
  };
  const handleCopyClick = e => {
    e.stopPropagation();
    anchorEl.current.select();
    anchorEl.current.setSelectionRange(0, 99999);
    document.execCommand("copy");
  };
  const handleClick = e => {
    e.stopPropagation();
    if (permission) {
      if (currentField && isEmail(currentField.fieldData.data)) {
        window.location.href = `mailto:${currentField.fieldData.data}`;
      } else {
        setEditEmail(true);
      }
    }
  };
  const handleEditClick = e => {
    e.stopPropagation();
    setEditEmail(true);
  };

  const handleChangeTitle = e => {
    //   changing the title
    setEmail(e.target.value);
    setErrorState(false);
    setErrorMessage("");
  };
  const handleKeyDown = e => {
    //   function calls onKeyDown event and checks the validation
    if (e.keyCode === 13) {
      handleSaveInputValue(); /** on Enter click */
    } else if (e.keyCode === 27) {
      /** on ESC click */
      setErrorState(false);
      setErrorMessage(false);
      setEmail(currentField ? currentField.fieldData.data : "");
      setEditEmail(false);
    }
  };

  useEffect(() => {
    setEmail(currentField ? currentField.fieldData.data : "");
  }, [currentField]);

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <EditableTextField
        title={email}
        module={""}
        placeholder={"Enter Here"}
        titleProps={{
          className: classes.emailField,
          onClick: handleClick,
        }}
        textFieldProps={{
          customInputClass: {
            input: classes.outlinedInputsmall,
            root: classes.outlinedInputCnt,
          },
          error: errorState,
          errorState: errorState,
          errorMessage: errorMessage,
        }}
        defaultProps={{
          value: email,
          onChange: e => handleChangeTitle(e),
          onKeyDown: e => handleKeyDown(e),
        }}
        formStyles={{
          height: "32px",
          borderRadius: 4,
        }}
        inputEl={{
          ref: anchorEl,
        }}
        textFieldIcons={
          <>
            {permission && !disabled && (
              <CustomTooltip
                helptext={"Edit"}
                iconType="help"
                placement="top"
                style={{ color: theme.palette.common.white }}>
                <SvgIcon
                  className={classes.editIcon}
                  viewBox="0 0 14 13.95"
                  onClick={handleEditClick}>
                  <IconEdit />
                </SvgIcon>
              </CustomTooltip>
            )}

            <CustomTooltip
              helptext={"Copy"}
              iconType="help"
              placement="top"
              style={{ color: theme.palette.common.white }}>
              <SvgIcon
                className={classes.copyIcon}
                viewBox="0 0 14 16.211"
                onClick={handleCopyClick}>
                <CopyIcon />
              </SvgIcon>
            </CustomTooltip>
          </>
        }
        textFieldIconsProps={{
          className: classes.iconCnt,
        }}
        editTitle={editEmail}
        permission={permission && !disabled}
        handleEditTitle={() => {
          setEmail(currentField ? currentField.fieldData.data : "");
          setEditEmail(false);
        }}
      />
    </div>
  );
}

CustomFieldEmail.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => { },
  handleEdit: () => { },
  handleCopyField: () => { },
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldEmail);
