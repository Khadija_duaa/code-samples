import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import DefaultTextField from "../Form/TextField";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { customFieldDropdownData } from "../../helper/customFieldsData";

function CustomFieldsDropDown({
  classes,
  theme,
  customFieldss,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  userPreferenceState,
  customDropDownProps,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);
  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery:""
    });
  };

  const handleSelect = (type, option) => {
    //   changing the title
    customFieldChange(option, field, field.settings);
  };
  const ddData = () => customFieldDropdownData(field.values.data);

  const selectedValue = () => {
    let selectedOptions = [];
    if (currentField) {
      let data = ddData();
      selectedOptions = data.reduce((result, currentValue) => {
        if (field.settings.multiSelect) {
          const isExist = currentField.fieldData.data.find(d => d.id === currentValue.id);
          if (isExist) result.push(currentValue);
        } else {
          if (currentField.fieldData.data && currentValue.id === currentField.fieldData.data.id)
            result.push(currentValue);
        }
        return result;
      }, []);
    }
    return selectedOptions;
  };

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <SelectSearchDropdown
        data={ddData}
        placeholder={"Select"}
        optionBackground={true}
        isMulti={field.settings.multiSelect}
        selectChange={(type, option) => {
          handleSelect(type, option);
        }}
        selectedValue={selectedValue()}
        selectRemoveValue={(type, option) => {
          handleSelect(type, option);
        }}
        styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
        customStyles={{
          control: {
            border: "none",
            // padding: "0px 5px",
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: theme.typography.fontWeightExtraLight,
            "&:hover": {
              background: `${theme.palette.background.items} !important`,
            },
            minHeight: 32,
          },
          placeholder: {
            color: "#7E7E7E",
          },
        }}
        isDisabled={!permission || disabled}
        writeFirst={true}
        {...customDropDownProps}
      />
    </div>
  );
}

CustomFieldsDropDown.defaultProps = {
  classes: {},
  theme: {},
  customDropDownProps: {},
  customfieldlabelProps: {},
  field: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
    userPreferenceState: state.userPreference,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldsDropDown);
