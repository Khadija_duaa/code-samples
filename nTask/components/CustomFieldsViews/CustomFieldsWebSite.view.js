import React, { useRef, useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import SvgIcon from "@material-ui/core/SvgIcon";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import EditableTextField from "../Form/EditableTextField";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import CopyIcon from "../../components/Icons/IconCopyContent";
import IconEdit from "../../components/Icons/IconEdit";
import CustomTooltip from "../../components/Tooltip/Tooltip";

function CustomFieldUrl({
  classes,
  theme,
  customFields,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);
  const anchorEl = useRef(null);
  const [editUrl, setEditUrl] = useState(false);
  const [errorState, setErrorState] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [url, setUrl] = useState("");

  const handleSaveInputValue = () => {
    let expressionA = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi; /** do not require HTTP protocol */
    let expressionB = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi; /** ensure URL starts with HTTP/HTTPS */
    let regexA = new RegExp(expressionA);
    let regexB = new RegExp(expressionB);
    if (url.match(regexA) || url.match(regexB)) {
      customFieldChange(url, field, field.settings);
      setErrorState(false);
      setErrorMessage(false);
      setEditUrl(false);
    } else {
      setErrorState(true);
      setErrorMessage("Please enter valid url!");
    }
  };
  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery:""
    });
  };
  const handleCopyClick = e => {
    e.stopPropagation();
    anchorEl.current.select();
    anchorEl.current.setSelectionRange(0, 99999);
    document.execCommand("copy");
  };
  const handleClick = e => {
    e.stopPropagation();
    let websiteUrl = url;
    if(websiteUrl !== "") {      
      if (!websiteUrl.match(/^https?:\/\//i)) {
        websiteUrl = "http://" + websiteUrl;
      }
      window.open(websiteUrl, "_blank");
    }
    else setEditUrl(true)
  };
  const handleEditClick = e => {
    e.stopPropagation();
    setEditUrl(true);
  };

  const handleChangeUrl = e => {
    //   changing the title
    setUrl(e.target.value);
    setErrorState(false);
    setErrorMessage("");
  };
  const handleKeyDown = e => {
    //   function calls onKeyDown event and checks the validation
    if (e.keyCode === 13) {
      if(url == "") customFieldChange("", field, field.settings);
      else handleSaveInputValue(); /** on Enter click */
    } else if (e.keyCode === 27) {
      /** on ESC click */
      setErrorState(false);
      setErrorMessage(false);
      setUrl(currentField ? currentField.fieldData.data : "");
      setEditUrl(false);
    }
  };

  useEffect(() => {
    setUrl(currentField ? currentField.fieldData.data : "");
  }, [currentField]);

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <EditableTextField
        title={url}
        module={""}
        placeholder={"Enter Here"}
        titleProps={{
          className: classes.emailField,
          onClick: handleClick,
        }}
        textFieldProps={{
          customInputClass: {
            input: classes.outlinedInputsmall,
            root: classes.outlinedInputCnt,
          },
          error: errorState,
          errorState: errorState,
          errorMessage: errorMessage,
        }}
        formStyles={{
          height: "32px",
          borderRadius: 4,
        }}
        defaultProps={{
          value: url,
          onChange: e => handleChangeUrl(e),
          onKeyDown: e => handleKeyDown(e),
        }}
        inputEl={{
          ref: anchorEl,
        }}
        textFieldIcons={
          <>
            {permission && !disabled && <CustomTooltip
              helptext={"Edit"}
              iconType="help"
              placement="top"
              style={{ color: theme.palette.common.white }}>
              <SvgIcon
                className={classes.editIcon}
                viewBox="0 0 14 13.95"
                onClick={handleEditClick}>
                <IconEdit />
              </SvgIcon>
            </CustomTooltip>}

            <CustomTooltip
              helptext={"Copy"}
              iconType="help"
              placement="top"
              style={{ color: theme.palette.common.white }}>
              <SvgIcon
                className={classes.copyIcon}
                viewBox="0 0 14 16.211"
                onClick={handleCopyClick}>
                <CopyIcon />
              </SvgIcon>
            </CustomTooltip>
          </>
        }
        textFieldIconsProps={{
          className: classes.iconCnt,
        }}
        editTitle={editUrl}
        permission={permission && !disabled}
        handleEditTitle={() => {
          setUrl(currentField ? currentField.fieldData.data : "");
          setEditUrl(false);
        }}
      />
    </div>
  );
}

CustomFieldUrl.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  handleSelectHideOption: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldUrl);
