import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";

import customFieldStyles from "./CustomFields.style";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { customFieldDropdownData } from "../../helper/customFieldsData";
import useFields from "./useFields.hook";
import DefaultTextField from "../Form/TextField";
import CustomButton from "../Buttons/CustomButton";
import isEmpty from "lodash/isEmpty";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import EditableTextField from "../Form/EditableTextField";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";

function CustomFieldsText({
  classes,
  customFields,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);

  const [inputValue, setInputValue] = useState(currentField ? currentField.fieldData.data : "");

  const handleSaveInputValue = updatedValue => {
    customFieldChange(updatedValue, field, field.settings);
  };
  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery: "",
    });
  };
  const handleChangeTitle = e => {
    //   changing the title
    e.stopPropagation();
    setInputValue(e.target.value);
  };
  const handleKeyDown = e => {
    //   function calls onKeyDown event and checks the validation
    if (e.keyCode === 13) {
      /** on Enter click */
      handleSaveInputValue(inputValue);
    } else if (e.keyCode === 27) {
      /** on ESC click */
      setInputValue(currentField ? currentField.fieldData.data : "");
    }
  };

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => {
          handleSelectHideOption(option, field);
        }}
        {...customfieldlabelProps}
      />
      <EditableTextField
        title={currentField ? currentField.fieldData.data : ""}
        module={""}
        placeholder={"Enter Here"}
        titleProps={{
          className: classes.textField,
        }}
        textFieldProps={{
          customInputClass: {
            input: classes.outlinedInputsmall,
            root: classes.outlinedInputCnt,
          },
        }}
        formStyles={{
          height: "32px",
          borderRadius: 4,
        }}
        handleEditTitle={() => {
          setInputValue(currentField ? currentField.fieldData.data : "");
        }}
        permission={permission && !disabled}
        defaultProps={{
          inputProps: { maxLength: 250 },
          value: inputValue,
          onChange: e => handleChangeTitle(e),
          onKeyDown: e => handleKeyDown(e),
        }}
      />
    </div>
  );
}

CustomFieldsText.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  handleSelectHideOption: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldsText);
