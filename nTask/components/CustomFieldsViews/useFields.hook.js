import React, { useState, useEffect } from "react";
import { getActiveFields, getModuleActiveFields } from "../../helper/getActiveCustomFields";

function useFields(type, view, customFields, profile) {
  const [fields, setFields] = useState([]);

  const renderFields = fieldsArr => {
    let desiredFields = fieldsArr.reduce((result, cv) => {
      const typeDesired = cv.fieldType === type;
      const isExist =
        cv.workspaces &&
        cv.workspaces.find(
          wf => wf.workspaceId == profile.loggedInTeam && wf.groupType.includes(view)
        );
      if (
        (isExist && typeDesired) ||
        (!isExist &&
          cv.team &&
          cv.team.groupType &&
          cv.team.groupType.includes(view) &&
          typeDesired)
      )
        result.push(cv);
      return result;
    }, []);
    setFields(desiredFields);
  };

  useEffect(() => {
    let customFieldArr = getActiveFields(customFields.data, profile, view);
    renderFields(customFieldArr);
  }, [customFields.data]);
  return {
    fields,
  };
}

useFields.defaultProps = {
  type: "",
  view: "",
  customFields: {
    data: [],
    groupCustomFields: [],
  },
  profile: {},
};

export default useFields;
