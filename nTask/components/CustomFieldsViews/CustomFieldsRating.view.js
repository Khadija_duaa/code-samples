import React, { useState, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import "react-phone-input-2/lib/style.css";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import RatingComponent from "../Rating";

function CustomFieldsRating({
  classes,
  theme,
  customFieldss,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);

  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery:""
    });
  };

  const handleSelectEmoji = value => {
    customFieldChange(value, field, field.settings);
  };

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <div className={classes.ratingContainer}>
        <RatingComponent
          item={field.settings.emoji}
          iteration={field.settings.scale}
          handleSelect={handleSelectEmoji}
          selectionNum={
            currentField && currentField.fieldData.data !== "" ? currentField.fieldData.data : null
          }
          permission={permission && !disabled}
        />
      </div>
    </div>
  );
}

CustomFieldsRating.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldsRating);
