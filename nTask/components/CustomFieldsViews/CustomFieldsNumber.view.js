import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import DefaultTextField from "../Form/TextField";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

function CustomFieldNumber({
  classes,
  theme,
  customFieldss,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  userPreferenceState,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);

  const handleThousandSeperator = x => {
    if (x === "" || !field.settings.useComma) return x;
    const givenNumber = x.replace(/,/g, "");
    const nfObject = new Intl.NumberFormat("en-US");
    const output = nfObject.format(givenNumber);
    return output;
  };

  const processValue = val => {
    const minusVal = val.indexOf("-") == 0 ? "-" : "";
    const number = val
      .replace(/[^.\d]/g, "")
      .replace(/^(\d*\.?)|(\d*)\.?/g, "$1$2"); /** remove alphabets and commas from string */
    const indexOfDot = number.indexOf("."); /** finding the index of decimal point */
    if (indexOfDot < 0)
      return minusVal + handleThousandSeperator(
        number
      ); /** if string has no decimal point then return the value with thousand seperator logic */
    let beforeDecimalNum = number.slice(0, indexOfDot);
    let afterDecimalNum = number.slice(indexOfDot + 1, number.length);
    if (afterDecimalNum.length > parseInt(field.settings.decimalPlace)) return inputValue;
    beforeDecimalNum = handleThousandSeperator(beforeDecimalNum);
    return minusVal + beforeDecimalNum + "." + afterDecimalNum;
  };

  const [inputValue, setInputValue] = useState(
    currentField ? processValue(currentField.fieldData.data) : ""
  );

  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };

  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };

  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery:""
    });
  };

  const handleChangeTitle = e => {
    //   changing the title
    e.stopPropagation();
    setInputValue(processValue(e.target.value));
  };

  const handleKeyDown = e => {
    //   function calls onKeyDown event and checks the validation
    if (e.keyCode === 13) {
      /** on Enter click */
      const number = inputValue.indexOf("-") == 0 ? "-" + inputValue.replace(/[^.\d]/g, "")  : inputValue.replace(/[^.\d]/g, "");
      customFieldChange(number, field, field.settings);
    } else if (e.keyCode === 27) {
      /** on ESC click */
      setInputValue(currentField ? processValue(currentField.fieldData.data) : "");
    }
  };

  useEffect(()=>{
    setInputValue(currentField ? processValue(currentField.fieldData.data) : "");
  },[currentField])

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <ClickAwayListener onClickAway={() => handleKeyDown({ keyCode: 27 })}>
        <DefaultTextField
          size="small"
          formControlStyles={{
            marginBottom: 0,
            height: "32px",
            borderRadius: 4,
            flex: 1,
          }}
          error={false}
          defaultProps={{
            type: "text",
            id: "numberValue",
            placeholder: " Enter here",
            value: inputValue,
            autoFocus: false,
            autoComplete: "off",
            onChange: e => handleChangeTitle(e),
            onKeyDown: e => handleKeyDown(e),
            endAdornment: field.settings.direction === "right" && field.settings.unit !== "none"  && field.settings.unit,
            startAdornment: field.settings.direction === "left" && field.settings.unit !== "none" && field.settings.unit,
            disabled: !permission || disabled,
          }}
          outlineCnt={false}
          transparentInput={true}
          customInputClass={{
            input: classes.outlinedInputMoneyField,
            root: classes.outlinedInputCnt,
            adornedStart: classes.moneyIconAdorned,
            adornedEnd: classes.endornmentEndClass,
            disabled : classes.outlinedInputDisabled
          }}
        />
      </ClickAwayListener>
    </div>
  );
}

CustomFieldNumber.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
    userPreferenceState: state.userPreference,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldNumber);
