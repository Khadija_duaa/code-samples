import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import CustomButton from "../Buttons/CustomIconButton";
import AddIcon from "@material-ui/icons/Add";
import Dropzone from "react-dropzone";
import { attachMultipleFile } from "../../redux/actions/projects";
import CancelIcon from "@material-ui/icons/Cancel";
import { withSnackbar } from "notistack";

const acceptedFormats = [
  "image/bmp",
  "image/gif",
  "image/jpeg",
  "image/png",
  "image/webp",
  "video/webm",
  "video/ogg",
  "video/mp4",
];
const maxSize = 20971520; //bytes = 20 MB

function CustomFieldsFileAndMedia({
  classes,
  theme,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  customFields,
  attachMultipleFile,
  customfieldlabelProps,
  handleSelectHideOption,
  groupId,
  enqueueSnackbar
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);
  const [percentage, setPercentage] = useState(0);
  const [filename, setFilename] = useState("");

  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery:""
    });
  };
  const getId = () => {
    if (groupType === "risk") return "riskid";
    if (groupType === "task") return "taskid";
    if (groupType === "issue") return "issueid";
    if (groupType === "project") return "projectid";
    if (groupType === "meeting") return "meetingid";
  };
  const onhandleDrop = (files, rejectedFiles) => {
    if (rejectedFiles && rejectedFiles.length > 0) {
      verifyFile(rejectedFiles);
    }

    if (files && files.length > 0) {
      const isVerified = verifyFile(files);
      setFilename(files[0].name);
      const options = {
        onUploadProgress: progressEvent => {
          const { loaded, total } = progressEvent;
          let percent = Math.floor((loaded * 100) / total);
          if (percent < 100) {
            setPercentage(percent);
          }
        },
      };

      if (isVerified) {
        var newData = new FormData();
        newData.append("File", files[0]);
        newData.append(getId(), groupId);
        newData.append("userid", profile.userId);
        attachMultipleFile(
          /** uploading file to s3 bucket */
          newData,
          data => {
            const fileData = currentField ? [...currentField.fieldData.data, ...data] : data;
            customFieldChange(
              fileData,
              field,
              field.settings,
              succ => {
                setPercentage(0);
              },
              fail => {}
            );
          },
          err => {},
          options
        );
      }
    }
  };

  const verifyFile = files => {
    if (files && files.length > 0) {
      const currentFile = files[0];
      let error = "";
      if (currentFile.hasOwnProperty("errors")) {
        currentFile.errors.forEach(err => {
          if (err.code === "file-too-large") {
            showSnackbar("!Oops Uploading failed, maximum upload size is 20MB", "error");
          }
          if (err.code === "file-invalid-type") {
            showSnackbar("File type not supported", "error");
          }
        });
      }
      if (error != "") {
        alert(error);
        return false;
      }
      return true;
    }
  };
  const handleDeleteAttachment = (e, file) => {
    e.stopPropagation();
    let updatedAttachment = currentField.fieldData.data.filter(f => f.fileId !== file.fileId);
    customFieldChange(
      updatedAttachment,
      field,
      field.settings,
      succ => {},
      fail => {}
    );
  };
  const handleDownloadFile = (e, file) => {
    e.stopPropagation();
    let a = document.createElement("a");
    a.href = file.publicURL;
    a.setAttribute("download", file.fileName);
    a.click();
  };

  const showSnackbar = (snackBarMessage, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <div className={classes.attachmentCnt}>
        {currentField
          ? currentField.fieldData.data.map(file => {
              return (
                <div
                  className={classes.attachmentCapsule}
                  onClick={e => handleDownloadFile(e, file)}>
                  <span>{`${file.fileName}`}</span>
                  {permission && !disabled && <span onClick={e => handleDeleteAttachment(e, file)}>
                    <CancelIcon className={classes.deleteAttachmentIcon} />
                  </span>}
                </div>
              );
            })
          : ""}
        {percentage > 0 && (
          <span
            className={classes.attachmentCapsule}
            style={{
              background: `linear-gradient(to right, #0090ff ${percentage}%, white ${percentage -
                100}%) `,
            }}>{`${filename}`}</span>
        )}
        {permission && !disabled && (
          <Dropzone
            onDrop={onhandleDrop}
            onDragOver={() => {}}
            onDropRejected={() => {}}
            accept={acceptedFormats}
            activeStyle={{}}
            style={{}}
            maxSize={maxSize}
            // disableClick
            onClick={evt => evt.preventDefault()}>
            {({ getRootProps, getInputProps, open }) => {
              return (
                <>
                  <div {...getRootProps()} onClick={e => e.stopPropagation()} style={{}}>
                    <input {...getInputProps()} />
                    <CustomButton
                      onClick={() => open()}
                      btnType="blueOutlined"
                      variant="contained"
                      style={{
                        padding: 0,
                      }}
                      disabled={false}>
                      <AddIcon
                        htmlColor={theme.palette.background.blue}
                        style={{ fontSize: "18px" }}
                      />
                    </CustomButton>
                  </div>
                </>
              );
            }}
          </Dropzone>
        )}
      </div>
    </div>
  );
}

CustomFieldsFileAndMedia.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  attachMultipleFile: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  permission: true,
  disabled: false,
  groupId:""
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withSnackbar,
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
    attachMultipleFile,
  })
)(CustomFieldsFileAndMedia);
