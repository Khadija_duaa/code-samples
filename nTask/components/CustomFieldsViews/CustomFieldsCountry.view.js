import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import DefaultTextField from "../Form/TextField";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { flags } from "../../helper/flags";

function CustomFieldCountry({
  classes,
  theme,
  customFieldss,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  userPreferenceState,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);
  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery:""
    });
  };

  const handleSelect = (type, option) => {
    //   changing the title
    customFieldChange(option.label, field, field.settings);
  };

  const handleCountryCode = () => {
    return flags.map(f => {
      return {
        label: f.name,
        value: f.dial_code,
        code: f.code,
        obj: f,
        icon: (
          <img
            style={{ width: "20px", height: "15px", marginRight: "5px" }}
            src={`https://flagpedia.net/data/flags/normal/${f.code}.png`}
          />
        ),
      };
    });
  };

  const selectedValue =
    currentField &&
    handleCountryCode().filter(country => country.label === currentField.fieldData.data);

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <SelectSearchDropdown
        data={() => handleCountryCode()}
        placeholder={"Select"}
        icon={true}
        label={""}
        isMulti={false}
        selectChange={(type, option) => {
          handleSelect(type, option);
        }}
        selectedValue={selectedValue || []}
        styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
        customStyles={{
          control: {
            border: "none",
            // padding: "0px 5px",
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: theme.typography.fontWeightExtraLight,
            "&:hover": {
              background: `${theme.palette.background.items} !important`,
            },
            minHeight: 32,
          },
          placeholder:{
            color:"#7E7E7E",
          }
        }}
        isDisabled={!permission || disabled}
        writeFirst={true}
      />
    </div>
  );
}

CustomFieldCountry.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
    userPreferenceState: state.userPreference,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldCountry);
