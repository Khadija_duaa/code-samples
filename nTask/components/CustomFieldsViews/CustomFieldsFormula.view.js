import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import SvgIcon from "@material-ui/core/SvgIcon";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";

import IconEdit from "../../components/Icons/IconEdit";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import DropdownMenu from "../Dropdown/DropdownMenu";
import ButtonActionsCnt from "../Dialog/ConfirmationDialogs/ButtonActionsCnt";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { getCustomFields } from "../../helper/customFieldsData";
import { generateSelectData, generateDMAS } from "../../helper/generateSelectData";
import CustomSelectIconDropdown from "../Dropdown/CustomSelectIconDropdown/Dropdown.js";

function CustomFieldsFormula({
  classes,
  theme,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  customFields,
  customfieldlabelProps,
  handleSelectHideOption,
  workspaceId = ""
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);

  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedFieldfrom, setSelectedFieldfrom] = useState([]);
  const [selectedFieldTo, setSelectedFieldTo] = useState([]);
  const [selectedOperation, setSelectedOperation] = useState(generateDMAS(classes)[0]);

  const handleClick = event => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleCloseAction = event => {
    // Function closes dropdown
    setAnchorEl(null);
    settingValuesInFields();
  };
  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery: ""
    });
  };
  const handleSelectFrom = (type, option) => {
    setSelectedFieldfrom(option);
  };
  const handleSelectTo = (type, option) => {
    setSelectedFieldTo(option);
  };
  const handleSelectOperation = option => {
    setSelectedOperation(option);
  };
  const generateResult = (a, b, operator) => {
    switch (operator) {
      case 0:
        if (a !== "" && b !== "") return parseFloat(a) + parseFloat(b);
        return "";
        break;
      case 1:
        if (a !== "" && b !== "") return parseFloat(a) - parseFloat(b);
        return "";
        break;
      case 2:
        if (a !== "" && b !== "") return parseFloat(a) * parseFloat(b);
        return "";
        break;
      case 3:
        if (a !== "" && b !== "") return parseFloat(a) / parseFloat(b);
        return "";
        break;

      default:
        return "";
        break;
    }
  };
  const handleCalRes = () => {
    let fromField = customFieldData.find(fromItem => fromItem.fieldId == selectedFieldfrom.id);
    let toField = customFieldData.find(toItem => toItem.fieldId == selectedFieldTo.id);
    let result = "";
    if (fromField && toField) {
      result = generateResult(
        fromField.fieldData.data,
        toField.fieldData.data,
        selectedOperation.value
      );
    }
    return result;
  };
  const handleCalculate = () => {
    customFieldChange(
      {
        fieldIdTo: selectedFieldTo.id,
        fieldIdFrom: selectedFieldfrom.id,
        operation: selectedOperation.value,
        result: handleCalRes(),
      },
      field,
      field.settings
    );
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  let filteredFields = getCustomFields(customFields, profile, groupType, workspaceId).filter(
    f => f.fieldType == "number" || f.fieldType == "money"
  );
  const ddData = () => {
    const fields = filteredFields.map(f => generateSelectData(f.fieldName, f.fieldId, f.fieldId, "", f));
    return fields;
  };

  const settingValuesInFields = () => {
    const dropDownData = ddData();
    const calculation = generateDMAS(classes);
    if (currentField) {
      const fieldFrom = dropDownData.find(item => item.id === currentField.fieldData.data.fieldIdFrom) || {};
      setSelectedFieldfrom(fieldFrom);

      const fieldTo = dropDownData.find(item => item.id === currentField.fieldData.data.fieldIdTo) || {}
      setSelectedFieldTo(fieldTo);

      const operator = calculation.find(item => item.value === currentField.fieldData.data.operation)
      setSelectedOperation(operator);
    }
    else {
      setSelectedFieldfrom([]);
      setSelectedFieldTo([]);
      setSelectedOperation(generateDMAS(classes)[0]);
    }
  };

  useEffect(() => {
    settingValuesInFields();
  }, [currentField]);

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <div className={classes.formulaContainer}>
        <span className={classes.resultValue}>
          {currentField ? (
            handleCalRes()
          ) : (
            <span style={{ color: "#7E7E7E" }}>Set formula</span>
          )}
        </span>
        {permission && !disabled && (
          <CustomTooltip
            helptext={"Edit"}
            iconType="help"
            placement="top"
            style={{ color: theme.palette.common.white }}>
            <SvgIcon
              className={classes.formulaEditIcon}
              viewBox="0 0 14 13.95"
              onClick={handleClick}
              ref={anchorEl}>
              <IconEdit />
            </SvgIcon>
          </CustomTooltip>
        )}
      </div>
      <DropdownMenu
        open={open}
        closeAction={handleCloseAction}
        anchorEl={anchorEl}
        size="xlarge"
        placement="top-end">
        <div className={classes.formulaCnt}>
          <div className={classes.fieldsCnt}>
            <SelectSearchDropdown
              data={ddData}
              placeholder={"Select"}
              isMulti={false}
              selectChange={(type, option) => {
                handleSelectFrom(type, option);
              }}
              selectedValue={selectedFieldfrom}
              styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
            />
            <CustomSelectIconDropdown
              options={() => generateDMAS(classes)}
              option={selectedOperation}
              onSelect={handleSelectOperation}
              size="small"
              scrollHeight={320}
              disabled={false}
            />
            <SelectSearchDropdown
              data={ddData}
              placeholder={"Select"}
              isMulti={false}
              selectChange={(type, option) => {
                handleSelectTo(type, option);
              }}
              selectedValue={selectedFieldTo}
              styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
            />
          </div>
          <ButtonActionsCnt
            cancelAction={handleCloseAction}
            successAction={handleCalculate}
            successBtnText={"Save"}
            cancelBtnText={"Cancel"}
            btnType="blue"
            disabled={false}
            btnQuery={false}
            customGridProps={{
              classes: {
                container: classes.btnContainer,
              },
            }}
          />
        </div>
      </DropdownMenu>
    </div>
  );
}

CustomFieldsFormula.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => { },
  handleEdit: () => { },
  handleCopyField: () => { },
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldsFormula);
