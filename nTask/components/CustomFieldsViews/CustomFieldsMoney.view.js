import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import DefaultTextField from "../Form/TextField";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

function CustomFieldsMoney({
  classes,
  theme,
  customFieldss,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  userPreferenceState,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);

  const [inputValue, setInputValue] = useState(currentField ? currentField.fieldData.data : "");
  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery:""
    });
  };

  const handleChangeTitle = e => {
    //   changing the title
    e.stopPropagation();
    setInputValue(e.target.value);
  };

  const handleKeyDown = e => {
    //   function calls onKeyDown event and checks the validation
    if (e.keyCode === 13) {
      /** on Enter click */
      customFieldChange(inputValue, field, field.settings);
    } else if (e.keyCode === 27) {
      /** on ESC click */
      setInputValue(currentField ? currentField.fieldData.data : "");
    }
  };
  useEffect(() => {
    setInputValue(currentField ? currentField.fieldData.data : "");
  }, [currentField]);

  const selectedCurrency =
    userPreferenceState.data.constant.currencies.find(
      c => c.isoCurrencySymbol === field.settings.currency
    ) || "";

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <ClickAwayListener onClickAway={() => handleKeyDown({ keyCode: 27 })}>
        <DefaultTextField
          size="small"
          formControlStyles={{
            marginBottom: 0,
            height: "32px",
            borderRadius: 4,
            flex: 1,
          }}
          error={false}
          defaultProps={{
            type: "number",
            id: "moneyValue",
            placeholder: " Enter here",
            value: inputValue,
            autoFocus: false,
            autoComplete: "off",
            onChange: e => handleChangeTitle(e),
            onKeyDown: e => handleKeyDown(e),
            startAdornment: selectedCurrency.currencySymbol,
            disabled: !permission || disabled,
          }}
          outlineCnt={false}
          transparentInput={true}
          customInputClass={{
            input: classes.outlinedInputMoneyField,
            root: classes.outlinedInputCnt,
            adornedStart: classes.moneyIconAdorned,
            disabled : classes.outlinedInputDisabled
          }}
        />
      </ClickAwayListener>
    </div>
  );
}

CustomFieldsMoney.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
    userPreferenceState: state.userPreference,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldsMoney);
