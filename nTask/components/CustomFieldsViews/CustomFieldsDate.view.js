import React, { useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import moment from "moment";
import CustomDatePicker from "../DatePicker/DatePicker/DatePicker";

function CustomFieldDate({
  classes,
  theme,
  customFields,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery: "",
    });
  };
  const handleSelectDate = (date, time) => {
    customFieldChange(
      { date: date, time: time},
      field,
      field.settings
    );
  };

  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);
  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <CustomDatePicker
        date={currentField && currentField.fieldData.data.date && moment(currentField.fieldData.data.date)}
        style={{ borderRadius: 4, flex: 1, padding: "9px 0px 10px 8px", position: "relative" }}
        label={null}
        icon={false}
        dateFormat="MMM DD, YYYY"
        PopperProps={{ size: null }}
        timeInput={true}
        datePickerProps={{
          maxDate: null,
        }}
        onSelect={handleSelectDate}
        selectedTime={currentField ? currentField.fieldData.data.time : false}
        filterDate={moment()}
        timeInputLabel={"Start Time"}
        btnProps={{ className: classes.datePickerCustomStyle }}
        deleteIcon={true}
        placeholder={"Select Date"}
        containerProps={{
          className: classes.datePickerContainer,
        }}
        disabled={!permission || disabled}
        deleteIconProps={{
          style: {
            marginRight: 10,
          },
        }}
      />
    </div>
  );
}

CustomFieldDate.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldDate);
