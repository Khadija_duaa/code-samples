import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";

import customFieldStyles from "./CustomFields.style";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { customFieldDropdownData } from "../../helper/customFieldsData";
import useFields from "./useFields.hook";
import DefaultTextField from "../Form/TextField";
import CustomButton from "../Buttons/CustomButton";
import isEmpty from "lodash/isEmpty";
// import InputAdornment from "@material-ui/core/InputAdornment";
import IconCopyContent from "../Icons/IconCopyContent";
import IconEdit from "../Icons/IconEdit";
import CustomIconButton from "../Buttons/CustomIconButton";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
function CustomFieldsAddress({
  classes,
  customFields,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  customfieldlabelProps
}) {
  const [editField, setEditField] = useState({});

  const { fields } = useFields("textfield", groupType, customFields, profile, groupType);

  const handleInputChange = (e, obj) => {
    const newObj = { ...obj, values: { data: e.target.value } };
    setEditField(newObj);
  };
  //Handle input edit click
  const handleInputEdit = (obj, currentField) => {
    if (currentField && currentField.fieldData) {
      setEditField({ ...obj, values: { data: currentField.fieldData.data } });
    } else {
      setEditField(obj);
    }
  };

  const handleSaveInputValue = () => {
    customFieldChange(editField.values.data, editField, editField.settings);
    setEditField({});
  };
  const handleDiscardChanges = () => {
    setEditField({});
  };
  const enterKeyHandler = event => {
    event.stopPropagation();
    if (event.key === "Enter") {
      handleSaveInputValue();
    }
    if (event.key === "Esc") {
      handleDiscardChanges();
    }
  };
  const handleBlur = event => {
    event.stopPropagation();
    editField.values && handleSaveInputValue();
  };

  return (
    <>
      <Grid container spacing={2} className={classes.customFieldsCnt}>
        {fields.map(f => {
          const currentField = customFieldData
            ? customFieldData.find(cf => cf.fieldId == f.fieldId) || null
            : {};
          return (
            !f.isSystem && (
              <Grid xs={12} item>
                <div className={classes.mainCustom}>
                  <CustomFieldLabelCmp label={f.fieldName} iconType={f.fieldType} {...customfieldlabelProps}/>
                  <div className={classes.editInputCnt}>
                    {!isEmpty(editField) && editField.fieldId === f.fieldId ? (
                      <DefaultTextField
                        fullWidth={true}
                        defaultProps={{
                          id: editField.fieldType,
                          onChange: e => handleInputChange(e, f),
                          onKeyUp: enterKeyHandler,
                          onBlur: handleBlur,
                          value: !isEmpty(editField.values.data) ? editField.values.data : "",
                          inputProps: { maxLength: 250 },
                          autoComplete: "off",

                          endAdornment: (
                            <div className={classes.customEmailAdornment}>
                              <CustomIconButton className={classes.customAdornmentButton}>
                                <IconEdit />
                              </CustomIconButton>
                              <CustomIconButton className={classes.customAdornmentButton1}>
                                <IconCopyContent />
                              </CustomIconButton>
                            </div>
                          ),
                        }}
                      />
                    ) : (
                      <DefaultTextField
                        fullWidth={true}
                        defaultProps={{
                          id: f.fieldType,
                          onFocus: () => handleInputEdit(f, currentField),
                          value:
                            currentField &&
                            currentField.fieldData &&
                            !isEmpty(currentField.fieldData.data)
                              ? currentField.fieldData.data
                              : "",
                          disabled: !permission,
                          endAdornment: (
                            <div className={classes.customEmailAdornment}>
                              <CustomIconButton className={classes.customAdornmentButton}>
                                <IconEdit />
                              </CustomIconButton>
                              <CustomIconButton className={classes.customAdornmentButton1}>
                                <IconCopyContent />
                              </CustomIconButton>
                            </div>
                          ),
                        }}
                      />
                    )}
                  </div>
                </div>
              </Grid>
            )
          );
        })}
      </Grid>{" "}
    </>
  );
}

CustomFieldsAddress.defaultProps = {
  classes: {},
  theme: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(CustomFieldsAddress);
