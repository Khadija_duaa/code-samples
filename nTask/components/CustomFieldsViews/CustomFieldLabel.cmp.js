import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import CustomButton from "../Buttons/CustomButton";
import { withStyles } from "@material-ui/core/styles";
import DropdownMenu from "../Dropdown/DropdownMenu";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import CustomFieldsDropdownsStyles from "./CustomFields.style";
import IconCopy from "../Icons/Copy";
import IconHide from "../Icons/IconHide";
import IconEditDropDown from "../Icons/IconEditDropDown";
import DeleteIcon from "../Icons/Delete";
import CustomFieldIcons from "../../Views/CustomFieldSettings/CustomFieldIcons/CustomFieldIcons";
import SvgIcon from "@material-ui/core/SvgIcon";
import ChevronDown from "@material-ui/icons/ArrowDropDown";
import { teamCanView } from "../PlanPermission/PlanPermission";

function CustomFieldLabelCmp({
  classes,
  theme,
  style,
  label,
  iconType,
  handleDelete,
  handleEdit,
  handleCopy,
  editOperation,
  copyOperation,
  hideOperation,
  deleteOperation,
  customIconProps,
  handleSelectHideOption,
  history,
}) {
  const [anchorEl, setAnchorEl] = useState(null);
  let searchQuery = history.location.pathname; //getting url

  const [isOpen, setOpen] = useState(null);
  const handleClick = event => {
    // Function Opens the dropdown
    event.stopPropagation();
    if (
      /** if user is on issue overview or task over view tab then do not open edit dropdown of custom fields */
      !searchQuery.includes("/reports/overview") &&
      !searchQuery.includes("/reports/IssueOverview") && 
      teamCanView("customFieldAccess") 
    ) {
      setAnchorEl(anchorEl ? null : event.currentTarget);
    }
  };
  const handleCloseDropDown = event => {
    // Function closes dropdown
    setOpen(null);
  };
  const handleClose = event => {
    // Function closes dropdown
    event.stopPropagation();
    setAnchorEl(null);
    // setOpen(null);
  };
  const [data, setData] = useState([
    {
      label: "Workspace",
      value: 1,
    },
    {
      label: "All Workspaces",
      value: 2,
    },
  ]);
  const open = Boolean(anchorEl);
  const openDd = Boolean(isOpen);
  const openWorkspaceMenu = e => {
    // Function Opens the dropdown
    e.stopPropagation();
    setOpen(isOpen ? null : e.currentTarget);
  };
  const hideShowdropDown = () => {
    return (
      <>
        <DropdownMenu
          open={openDd}
          closeAction={handleCloseDropDown}
          anchorEl={isOpen}
          size={"small"}
          placement="right-start">
          <div className={classes.workSpacesContainer}>
            <ul className={classes.unorderList}>
              {data.map((d, i) => {
                return (
                  <ListItem
                    key={i}
                    className={classes.listStyling}
                    onClick={e => {
                      setOpen(null);
                      handleSelectHideOption(d.value);
                    }}>
                    <span className={classes.hideDdItem}>{d.label}</span>
                  </ListItem>
                );
              })}
            </ul>
          </div>
        </DropdownMenu>
      </>
    );
  };
  return (
    <>
      <CustomButton
        variant="text"
        btnType="plain"
        onClick={handleClick}
        labelAlign="left"
        buttonRef={anchorEl}
        customClasses={{
          text: classes.btnStyles,
        }}
        disabled={!editOperation && !copyOperation && !hideOperation && !deleteOperation}>
        <CustomFieldIcons
          value={iconType}
          color={"#0090ff"}
          customProps={{ className: classes.iconStyle }}
          {...customIconProps}
        />
        <span className={classes.labelSpan} title={label}>
          {label}
        </span>
      </CustomButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size="small"
        placement="bottom-start">
        <ListItem className={classes.customDropDownListItem}>
          {editOperation && (
            <ListItemIcon
              className={classes.customDropDownIcon}
              onClick={e => {
                handleEdit();
                handleClose(e);
              }}>
              <IconEditDropDown />
              <span className={classes.customDropDownText}> Edit Field</span>
            </ListItemIcon>
          )}
          {copyOperation && (
            <ListItemIcon
              className={classes.customDropDownIcon}
              onClick={e => {
                handleCopy();
                handleClose(e);
              }}>
              <SvgIcon
                className={classes.editFieldIcon}
                color={theme.palette.icon.gray600}
                viewBox="0 0 15 15">
                <IconCopy />
              </SvgIcon>
              <span className={classes.customDropDownText}>Copy Field</span>
            </ListItemIcon>
          )}
          {/* remove hide show for current time */}
          {hideOperation && ( 
            <span ref={anchorEl} onClick={openWorkspaceMenu}>
              <ListItemIcon className={classes.customDropDownIcon}>
                <IconHide />
                <span className={classes.customDropDownText}>Hide Field</span>
                <ChevronDown
                  className={classes.dropdownIndicatorIcon}
                  htmlColor={theme.palette.secondary.light}
                />
              </ListItemIcon>
            </span>
          )}
          {deleteOperation && (
            <ListItemIcon className={classes.customDropDownIcon} onClick={handleDelete}>
              <SvgIcon className={classes.deleteFieldIcon} viewBox="0 0 14 14.005">
                <DeleteIcon />
              </SvgIcon>
              <span className={classes.customDropDownText}>Delete Field</span>
            </ListItemIcon>
          )}
        </ListItem>
      </DropdownMenu>
      {hideShowdropDown() /** render drop down component */}
    </>
  );
}
CustomFieldLabelCmp.defaultProps = {
  editOperation: true,
  copyOperation: true,
  hideOperation: true,
  deleteOperation: true,
  handleCopy: () => { },
  handleSelectHideOption: () => {},
  customIconProps: {},
  handleDelete: () => {},
  handleEdit: () => {},
  handleCopy: () => {},
};
export default compose(
  withRouter,
  withStyles(CustomFieldsDropdownsStyles, { withTheme: true })
)(CustomFieldLabelCmp);
