import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import CustomFieldsDropdownsStyles from "./CustomFields.style";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { customFieldDropdownData } from "../../helper/customFieldsData";
import useFields from "./useFields.hook";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";

function CustomFieldsDropdowns({
  classes,
  customFields,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  style,
  theme,
}) {
  const { fields } = useFields("dropdown", groupType, customFields, profile, groupType);

  return (
    <>
      <Grid container spacing={2} className={classes.customFieldsCnt}>
        {fields.map(f => {
          const ddData = () => customFieldDropdownData(f.values.data);
          const currentField =
            customFieldData && customFieldData.find(cf => cf.fieldId == f.fieldId);
          return (
            !f.isSystem && (
              <Grid xs={12} item className={classes.customFieldButtonStyle}>
                <CustomFieldLabelCmp label={f.fieldName} iconType={f.fieldType} />
                <SelectSearchDropdown /* Task multi select drop down */
                  data={ddData} /* function calling for generating task array */
                  // label={}
                  isMulti={f.settings.multiSelect}
                  optionBackground={true}
                  selectChange={(type, option) =>
                    customFieldChange(option, f, f.settings)
                  } /** function calling on select task in drop down */
                  selectRemoveValue={(type, option) => customFieldChange(option, f, f.settings)}
                  selectedValue={
                    currentField && f.settings.multiSelect
                      ? currentField.fieldData.data
                      : currentField
                      ? currentField.fieldData.data
                      : []
                  }
                  placeholder={f.placeholder}
                  isDisabled={disabled || !permission}
                  styles={{ marginBottom: 0 }}
                />
              </Grid>
            )
          );
        })}
      </Grid>{" "}
    </>
  );
}

CustomFieldsDropdowns.defaultProps = {
  classes: {},
  theme: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(CustomFieldsDropdownsStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(CustomFieldsDropdowns);
