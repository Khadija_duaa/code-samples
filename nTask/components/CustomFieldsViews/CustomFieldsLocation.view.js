import React, { useRef, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import SvgIcon from "@material-ui/core/SvgIcon";
import Grid from "@material-ui/core/Grid";

import customFieldStyles from "./CustomFields.style";
import CustomFieldLabelCmp from "./CustomFieldLabel.cmp";
import EditableTextField from "../Form/EditableTextField";
import { updateDeleteDialogState } from "../../redux/actions/allDialogs";
import CopyIcon from "../Icons/IconCopyContent";
import CustomTooltip from "../Tooltip/Tooltip";
import { flags } from "../../helper/flags";
import SelectSearchDropdown from "../Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { CopyToClipboard } from "react-copy-to-clipboard";

function CustomFieldLocation({
  classes,
  theme,
  customFields,
  profile,
  groupType,
  customFieldChange,
  permission,
  disabled,
  customFieldData,
  field,
  updateDeleteDialogState,
  handleDeleteField,
  handleEdit,
  handleCopyField,
  customfieldlabelProps,
  handleSelectHideOption,
}) {
  const anchorEl = useRef(null);
  const currentField = customFieldData && customFieldData.find(cf => cf.fieldId == field.fieldId);
  const [lineOne, setLineOne] = useState(currentField ? currentField.fieldData.data.lineOne : "");
  const [lineTwo, setLineTwo] = useState(currentField ? currentField.fieldData.data.lineTwo : "");
  const [city, setCity] = useState(currentField ? currentField.fieldData.data.city : "");
  const [province, setProvince] = useState(
    currentField ? currentField.fieldData.data.province : ""
  );
  const [zipCode, setZipCode] = useState(currentField ? currentField.fieldData.data.zipCode : "");

  const updateLineOne = updatedValue => {
    customFieldChange(
      currentField
        ? { ...currentField.fieldData.data, lineOne: updatedValue }
        : { lineOne: updatedValue },
      field,
      field.settings
    );
  };
  const updateLineTwo = updatedValue => {
    customFieldChange(
      currentField
        ? { ...currentField.fieldData.data, lineTwo: updatedValue }
        : { lineTwo: updatedValue },
      field,
      field.settings
    );
  };
  const updateCity = updatedValue => {
    customFieldChange(
      currentField
        ? { ...currentField.fieldData.data, city: updatedValue }
        : { city: updatedValue },
      field,
      field.settings
    );
  };
  const updateProvince = updatedValue => {
    customFieldChange(
      currentField
        ? { ...currentField.fieldData.data, province: updatedValue }
        : { province: updatedValue },
      field,
      field.settings
    );
  };
  const updateZipcode = updatedValue => {
    customFieldChange(
      currentField
        ? { ...currentField.fieldData.data, zipCode: updatedValue }
        : { zipCode: updatedValue },
      field,
      field.settings
    );
  };
  const handleSuccessDelete = () => {
    updateDeleteDialogState({ btnQuery: "progress" });
    handleDeleteField(field);
  };
  const handleClose = () => {
    updateDeleteDialogState({ open: false });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: handleClose,
      btnQuery: "",
    });
  };
  const handleCopyClick = e => {
    e.stopPropagation();
    anchorEl.current.select();
    anchorEl.current.setSelectionRange(0, 99999);
    document.execCommand("copy");
  };
  const handleCountryCode = () => {
    return flags.map(f => {
      return {
        label: f.name,
        value: f.dial_code,
        code: f.code,
        obj: f,
        icon: (
          <img
          style={{ marginRight: "5px"}}
          src={`https://flagcdn.com/${f.code}.svg`}
          width="20"
          height="15"
        />
        ),
      };
    });
  };
  const handleSelect = (type, option) => {
    customFieldChange(
      currentField
        ? { ...currentField.fieldData.data, countryCode: option.label }
        : { countryCode: option.label },
      field,
      field.settings
    );
  };
  const handleChange = (e, type) => {
    e.stopPropagation();
    switch (type) {
      case "lineOne":
        setLineOne(e.target.value);
        break;
      case "lineTwo":
        setLineTwo(e.target.value);
        break;
      case "city":
        setCity(e.target.value);
        break;
      case "province":
        setProvince(e.target.value);
        break;
      case "zipCode":
        setZipCode(e.target.value);
        break;

      default:
        break;
    }
  };
  const handleUpdateData = (type, clearVal) => {
    switch (type) {
      case "lineOne":
        if (clearVal) setLineOne(currentField ? currentField.fieldData.data.lineOne : "");
        else updateLineOne(lineOne);
        break;
      case "lineTwo":
        if (clearVal) setLineTwo(currentField ? currentField.fieldData.data.lineTwo : "");
        else updateLineTwo(lineTwo);
        break;
      case "city":
        if (clearVal) setCity(currentField ? currentField.fieldData.data.city : "");
        else updateCity(city);
        break;
      case "province":
        if (clearVal) setProvince(currentField ? currentField.fieldData.data.province : "");
        else updateProvince(province);
        break;
      case "zipCode":
        if (clearVal) setZipCode(currentField ? currentField.fieldData.data.zipCode : "");
        else updateZipcode(zipCode);
        break;

      default:
        break;
    }
  };

  const handleKeyDown = (e, type) => {
    e.stopPropagation();
    //   function calls onKeyDown event and checks the validation
    if (e.keyCode === 13) {
      handleUpdateData(type, false);
    } else if (e.keyCode === 27) {
      /** on ESC click */
      handleUpdateData(type, true);
    }
  };

  const selectedValue =
    currentField &&
    handleCountryCode().filter(country => country.label === currentField.fieldData.data.countryCode);

  const fullAddress = currentField
    ? `${currentField.fieldData.data.lineOne || ""} ${currentField.fieldData.data.lineTwo ||
        ""} ${currentField.fieldData.data.city || ""} ${currentField.fieldData.data.province ||
        ""} ${currentField.fieldData.data.zipCode || ""} ${
        selectedValue.length ? selectedValue[0].label : ""
      }`
    : "";

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={field.fieldName}
        iconType={field.fieldType}
        handleDelete={handleDelete}
        handleEdit={() => handleEdit(field)}
        handleCopy={() => handleCopyField(field)}
        handleSelectHideOption={option => handleSelectHideOption(option, field)}
        {...customfieldlabelProps}
      />
      <Grid container spacing={1} style={{ flex: 1 }}>
        <Grid item xs={12} sm={12} md={12}>
          <EditableTextField
            title={currentField ? currentField.fieldData.data.lineOne : ""}
            module={""}
            placeholder={"Line 1"}
            titleProps={{
              className: classes.fieldTitle,
            }}
            textFieldProps={{
              customInputClass: {
                input: classes.outlinedInputsmall,
                root: classes.outlinedInputCnt,
              },
            }}
            formStyles={{
              height: "32px",
              borderRadius: 4,
            }}
            defaultProps={{
              value: lineOne,
              onChange: e => handleChange(e, "lineOne"),
              onKeyDown: e => handleKeyDown(e, "lineOne"),
              inputProps: { maxLength: 125 },
            }}
            inputEl={{
              ref: anchorEl,
            }}
            handleEditTitle={() => {
              handleUpdateData("lineOne", true);
            }}
            textFieldIcons={
              <CopyToClipboard text={fullAddress}>
                <CustomTooltip
                  helptext={"Copy"}
                  iconType="help"
                  placement="top"
                  style={{ color: theme.palette.common.white }}>
                  <SvgIcon
                    className={classes.copyIcon}
                    viewBox="0 0 14 16.211"
                    // onClick={handleCopyClick}
                  >
                    <CopyIcon />
                  </SvgIcon>
                </CustomTooltip>
              </CopyToClipboard>
            }
            textFieldIconsProps={{
              className: classes.iconCnt,
            }}
            permission={permission && !disabled}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <EditableTextField
            title={currentField ? currentField.fieldData.data.lineTwo : ""}
            module={""}
            placeholder={"Line 2"}
            titleProps={{
              className: classes.fieldTitle,
            }}
            textFieldProps={{
              customInputClass: {
                input: classes.outlinedInputsmall,
                root: classes.outlinedInputCnt,
              },
            }}
            formStyles={{
              height: "32px",
              borderRadius: 4,
            }}
            inputEl={{
              ref: anchorEl,
            }}
            handleEditTitle={() => {
              handleUpdateData("lineTwo", true);
            }}
            textFieldIcons={null}
            textFieldIconsProps={{
              className: classes.iconCnt,
            }}
            permission={permission && !disabled}
            defaultProps={{
              value: lineTwo,
              onChange: e => handleChange(e, "lineTwo"),
              onKeyDown: e => handleKeyDown(e, "lineTwo"),
              inputProps: { maxLength: 125 },

            }}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <EditableTextField
            title={currentField ? currentField.fieldData.data.city : ""}
            module={""}
            placeholder={"City"}
            titleProps={{
              className: classes.fieldTitle,
            }}
            textFieldProps={{
              customInputClass: {
                input: classes.outlinedInputsmall,
                root: classes.outlinedInputCnt,
              },
            }}
            formStyles={{
              height: "32px",
              borderRadius: 4,
            }}
            inputEl={{
              ref: anchorEl,
            }}
            handleEditTitle={() => {
              handleUpdateData("city", true);
            }}
            textFieldIcons={null}
            textFieldIconsProps={{
              className: classes.iconCnt,
            }}
            permission={permission && !disabled}
            defaultProps={{
              value: city,
              onChange: e => handleChange(e, "city"),
              onKeyDown: e => handleKeyDown(e, "city"),
              inputProps: { maxLength: 38 },

            }}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <EditableTextField
            title={currentField ? currentField.fieldData.data.province : ""}
            module={""}
            placeholder={"State/Province/Region"}
            titleProps={{
              className: classes.fieldTitle,
            }}
            textFieldProps={{
              customInputClass: {
                input: classes.outlinedInputsmall,
                root: classes.outlinedInputCnt,
              },
            }}
            formStyles={{
              height: "32px",
              borderRadius: 4,
            }}
            inputEl={{
              ref: anchorEl,
            }}
            handleEditTitle={() => {
              handleUpdateData("province", true);
            }}
            textFieldIcons={null}
            textFieldIconsProps={{
              className: classes.iconCnt,
            }}
            permission={permission && !disabled}
            defaultProps={{
              value: province,
              onChange: e => handleChange(e, "province"),
              onKeyDown: e => handleKeyDown(e, "province"),
              inputProps: { maxLength: 38 },

            }}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <EditableTextField
            title={currentField ? currentField.fieldData.data.zipCode : ""}
            module={""}
            placeholder={"Zip code"}
            titleProps={{
              className: classes.fieldTitle,
            }}
            textFieldProps={{
              customInputClass: {
                input: classes.outlinedInputsmall,
                root: classes.outlinedInputCnt,
              },
            }}
            formStyles={{
              height: "32px",
              borderRadius: 4,
            }}
            inputEl={{
              ref: anchorEl,
            }}
            handleEditTitle={() => {
              handleUpdateData("zipCode", true);
            }}
            textFieldIcons={null}
            textFieldIconsProps={{
              className: classes.iconCnt,
            }}
            permission={permission && !disabled}
            defaultProps={{
              value: zipCode,
              onChange: e => handleChange(e, "zipCode"),
              onKeyDown: e => handleKeyDown(e, "zipCode"),
              inputProps: { maxLength: 10 },
            }}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <SelectSearchDropdown
            data={() => handleCountryCode()}
            placeholder={"Country"}
            icon={true}
            label={""}
            isMulti={false}
            selectChange={(type, option) => {
              handleSelect(type, option);
            }}
            selectedValue={selectedValue || []}
            styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
            customStyles={{
              control: {
                border: "none",
                // padding: "0px 5px",
                fontFamily: theme.typography.fontFamilyLato,
                fontWeight: theme.typography.fontWeightExtraLight,
                "&:hover": {
                  background: `${theme.palette.background.items} !important`,
                },
                minHeight: 32,
              },
            }}
            isDisabled={!permission || disabled}
            writeFirst={true}
          />
        </Grid>
      </Grid>
    </div>
  );
}

CustomFieldLocation.defaultProps = {
  classes: {},
  theme: {},
  field: {},
  customfieldlabelProps: {},
  groupType: "risk",
  customFieldData: [],
  customFieldChange: () => {},
  handleEdit: () => {},
  handleCopyField: () => {},
  permission: true,
  disabled: false,
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
  };
};

export default compose(
  withStyles(customFieldStyles, { withTheme: true }),
  connect(mapStateToProps, {
    updateDeleteDialogState,
  })
)(CustomFieldLocation);
