import React from "react";
import { render } from "react-dom";
import "../index.css";
import LoginApp from "../LoginApp";
import { Provider } from "react-redux";
import configureStore from "../redux/store/index";

export const store = configureStore();

render(
  <React.Fragment>
    <Provider store={store}>
    <App />
 </Provider>

  </React.Fragment>,
  document.getElementById("root")
);
