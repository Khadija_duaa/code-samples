export const MixPanelEvents = {
    AppStarted: "Started app",
    LoginEvent: "LoginEvent",
    SignupEvent: "SignupEvent",
    OnboardingEvent: "OnboardingEvent",
    WorkspaceCreationEvent: "WorkspaceCreationEvent",
    TaskCreationEvent: "TaskCreationEvent",
    ProjectCreationEvent: "ProjectCreationEvent",
    IssueCreationEvent: "IssueCreationEvent",
    MeetingCreationEvent: "MeetingCreationEvent",
    RiskCreationEvent: "RiskCreationEvent",
    BoardCreationEvent: "BoardCreationEvent",
    GantCreationEvent: "GantCreationEvent",
  };
  