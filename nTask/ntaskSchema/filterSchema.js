const getSchema = () => {
  const schema = {
    taskFilter: {
      project: [],
      assignees: [],
      status: [],
      priority: [],
      actualStart: { endDate: "", startDate: "" },
      plannedStart: { endDate: "", startDate: "" },
      plannedEnd: { endDate: "", startDate: "" },
      actualEnd: { endDate: "", startDate: "" }
    },
    issueFilter: {
        actualEnd: {endDate: "", startDate: ""},
        actualStart: {endDate: "", startDate: ""},
        assignees: [],
        createdBy: [],
        plannedEnd: {endDate: "", startDate: ""},
        plannedStart: {endDate: "", startDate: ""},
        priority: [],
        severity: [],
        status: [],
        task: [],
        type: []
    },
    riskFilter: {
      impact: [],
      likeHood: [],
      owner: [],
      status: [],
      task: []
    },
    meetingFilter: {
      createdBy: [],
      date: {
        startDate: "",
        endDate: ""
      },
      participants: [],
      recurrence: false,
      status: [],
      task: []
    }
  };
  return schema;
};

export default { getSchema };
