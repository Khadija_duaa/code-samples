import React, { Component, Suspense, lazy, useEffect } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
  withRouter,
} from "react-router-dom";
import "../App.css";
import { withStyles } from "@material-ui/core/styles";
import appStyles from "../appStyles";
import { useDispatch } from "react-redux";
import { DefaultWhiteLabelInfo } from "../redux/actions/teamMembers";
// import LinkedInPopUp from "../components/SocialLoginButtons/LinkedInPopUp";
import Authenticate from "./Auth";
// import VerifyEmailChange from "../components/verifyEmailChange/verifyEmailCHange";
// import SupportRedirect from "../supportRedirect";
// import Maintenance from "../Views/Maintenance/Maintenance";
import ZapierAuth from "../components/Zapier/ZapierAuth";
import MobileAppNotification from "../components/MobileAppNotification/MobileAppNotification";
// import FormComponent from '../Views/FormFill';
// import onBoardingDashboard from "../Views/OnBoarding/Dashboard";
// import ConfirmEmail from "../Views/ConfirmEmail/ConfirmEmail";
// import VerifyEmail from "../Views/VerifyEmail/VerifyEmail";
// import InvalidInvite from "../Views/InvalidInvite/InvalidInvite";
// import SocialLogin from "../Views/SocialLogin/SocialLogin";
// import SsoAuthentication from "../Views/SsoAuthentication/SsoAuthentication"; 
import VerifyUserEmail from "../Views/VerifyUserEmail/VerifyEmail";
// import VerifyEmailFromEmail from "../components/verifyEmail/verifyEmail";
// import onBoarding from '../Views/Onboarding/OnBoarding.view'
// import MasterTemplate from "../Views/masterTemplate/masterTemplate";
import loadable from '@loadable/component'

const LoginView = loadable(() => import("../Views/NewLogin/Login"));
const ConfirmEmail = loadable(() => import("../Views/ConfirmEmail/ConfirmEmail"));
const VerifyEmailChange = loadable(() => import("../components/verifyEmailChange/verifyEmailChange"));
const VerifyEmailFromEmail = loadable(() => import("../components/verifyEmail/verifyEmail"));
const VerifyEmail = loadable(() => import("../Views/VerifyEmail/VerifyEmail"));
const InvalidInvite = loadable(() => import("../Views/InvalidInvite/InvalidInvite"));
const SsoAuthentication = loadable(() => import("../Views/SsoAuthentication/SsoAuthentication"));
const SocialLogin = loadable(() => import("../Views/SocialLogin/SocialLogin"));
const SignUp = loadable(() => import("../Views/Onboarding/Signup/signup.view"));
const OnBoarding = loadable(() => import("../Views/Onboarding/OnBoarding.view"));
const MasterTemplate = loadable(() => import("../Views/masterTemplate/masterTemplate"));
const FormComponent = loadable(() =>import("../Views/FormFill"));
const Forms = loadable(() =>import("../Views/Forms/FormCreator"));
const PublicTask = loadable(() =>
  import("../Views/Task/PublicTask/PublicTask"
  )
);
const InvitedUser = loadable(() =>
  import("../components/Invite/InvitedUser"
  )
);
const NoActiveWorkspace = loadable(() =>
  import("../components/Onboarding/NoActiveWorkspace"
  )
);

const MainView = Authenticate(MasterTemplate);
function CustomRoutes(props) {
  // componentDidMount() {

  // }
  const dispatchFun = useDispatch();
  useEffect(() => {
    DefaultWhiteLabelInfo(dispatchFun);
  }, [])
  // componentDidCatch(error, errorInfo) {
  //   // if(error.name == "ChunkLoadError"){
  //   //   this.props.history.push('/maintenance-in-progress')
  //   // }
  //   // You can also log the error to an error reporting service
  // }


    const { classes, authorized, theme } = props;
    return (
      <Suspense fallback={<></>}>
        <Router>
          <Switch>
            {location.search == "?host_url=support.ntaskmanager.com" && (
              <Redirect
                exact
                from={{
                  pathname: "/",
                  search: "?host_url=support.ntaskmanager.com",
                }}
                to={{
                  pathname: authorized ? "/tasks" : "/account/login",
                  state: { support: true },
                }}
              />
            )}
            <Route path="/form/fill" component={FormComponent} />
 
            <Route path="/form" component={Forms} />
            
            <Route path="/mobileapp" component={MobileAppNotification} />
            <Route
              excat
              path="/wl/:companyName"
              component={LoginView}
            />
            <Route
              exact
              path="/zapierAuth"
              component={Authenticate(ZapierAuth)}
            />

            <Redirect
              exact
              from="/"
              to={authorized ? "/tasks" : "/account/login"}
            />
            <Redirect
              exact
              from="/account"
              to={authorized ? "/tasks" : "/account/login"}
            />
            <Route path="/account/register" component={SignUp} />
            {/*{authorized ? (*/}
            {/*  <Redirect from="/welcome/createprofile" to="/teams/" />*/}
            {/*) : null}*/}
            {authorized ? (
              <Redirect from="/account/:view" to="/teams/" />
            ) : (
                <Route path="/account/:view" component={LoginView} />
              )}
            {/* <Route
            exact
            path="/maintenance-in-progress"
            component={Maintenance}
          /> */}
            <Route
              exact
              path="/verifyemail"
              component={Authenticate(VerifyEmailChange)}
            />
            <Route
              exact
              path="/verify-user-email"
              component={VerifyEmailFromEmail}
            />
            <Route
              exact
              path="/verify-email"
              component={Authenticate(VerifyEmail)}
            />
            <Route
              exact
              path="/no-team-found"
              component={Authenticate(NoActiveWorkspace)}
            />
            <Route
              exact
              path="/welcome/invited-user"
              component={Authenticate(InvitedUser)}
            />
            <Route path="/sociallogin" component={SocialLogin} />
            <Route
              path="/welcome/createprofile"
              component={OnBoarding}
            />
            <Route
              path="/welcome/confirmuseremail"
              component={Authenticate(ConfirmEmail)}
            />
            <Route
              path="/sso"
              component={SsoAuthentication}
            />
            {/* <Route exact path="/welcome/getstarted" component={GetStarted} /> */}

            {/*<Route path="/welcome" component={OnBoard} />*/}
            <Route path="/invalid-invite" component={InvalidInvite} />

            {/*<Route exact path="/linkedin" component={LinkedInPopUp} />*/}
            <Route path="/public/task" component={PublicTask} /> 

            <Route path="/:view" component={MainView} />
          </Switch>
        </Router>
      </Suspense>
    );
}

export default withStyles(appStyles, { withTheme: true })(CustomRoutes);
