import React from "react";
import { connect } from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { defaultTheme } from "../assets/jss/theme";
import MasterTemplate from "../Views/masterTemplate/masterTemplate";
import securityHeaders from "../redux/securityHeaders/index";
import { updateAuth, validateAuth } from "../redux/actions/authentication";
import helper from "../helper/index";

export default function(WrappedComponent, history, classes, ...props) {
  class AuthorizedRoute extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        accessFlag: true
      };
    }

    componentDidMount() {
      this.checkAuthorizationAndRedirect();
    }

    componentDidUpdate(prevProps) {
      if (this.props.authState.Auth !== prevProps.authState.Auth) {
        this.checkAuthorizationAndRedirect();
      }
    }

    checkAuthorizationAndRedirect() {
      //
      if (
        (helper.getToken() === null ||
          helper.getToken() === undefined) &&
        !securityHeaders.GETHEADERS().headers.headers.Authorization
      ) {
        this.setState({
          accessFlag: false
        });
        history.push("/account/login");
      } else {
        this.props.validateAuth(data => {
          if (data.status === 200) {
            this.setState({
              accessFlag: true
            });
          } else {
            this.setState({
              accessFlag: false
            });
            history.push("/account/login");
          }
        });
      }
    }

    render() {
      //
      const { accessFlag } = this.state;
      const View = () => {
        if (props[0] === "Welcome") {
          return (
            // <MuiThemeProvider theme={defaultTheme}>
            // <CssBaseline />
            <WrappedComponent step={props[1]} authAction={this.updateAuth} />
            // </MuiThemeProvider>
          );
        } else if (props[0] === "GetStarted") {
          return (
            // <MuiThemeProvider theme={defaultTheme}>
            // <CssBaseline />
            <WrappedComponent authAction={this.updateAuth} />
            // </MuiThemeProvider>
          );
        } else {
          return (
            // <MuiThemeProvider theme={defaultTheme}>
            // <CssBaseline />
            <div className={classes.root}>
              <MasterTemplate />
              <main className={classes.content}>
                <WrappedComponent />
              </main>
            </div>
            // </MuiThemeProvider>
          );
        }
      };
      return <React.Fragment>{accessFlag ? <View /> : null}</React.Fragment>;
    }
  }

  const mapStateToProps = state => {
    return {
      authState: state.authenticationResponse
    };
  };

  return connect(
    mapStateToProps,
    {
      updateAuth,
      validateAuth
    }
  )(AuthorizedRoute);
}
