import React from "react";
import { connect } from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { defaultTheme } from "../assets/jss/theme";
import securityHeaders from "../redux/securityHeaders/index";
import { updateAuth, validateAuth } from "../redux/actions/authentication";
import helper from "../helper/index";

export default function(WrappedComponent, history, classes, ...props) {
  class UnAuthorizedRoute extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        accessFlag: false
      };
    }

    componentDidMount() {
      this.checkAuthorizationAndRedirect();
    }

    componentDidUpdate(prevProps) {
      if (this.props.authState !== prevProps.authState) {
        this.checkAuthorizationAndRedirect();
      }
    }

    checkAuthorizationAndRedirect() {
      // 
      if (
        (helper.getToken() !== null &&
          helper.getToken() !== undefined) ||
        securityHeaders.GETHEADERS().headers.headers.Authorization
      ) {
        this.props.validateAuth(data => {
          if (data && data.status === 200) {
            let zapAuth = helper.ISZAPIERCALL(
              localStorage.getItem("userName"),
              localStorage.getItem("zapkey")
            );
            if (zapAuth === 0) {
              this.setState({
                accessFlag: false
              });
              history.push("/tasks");
            } else if (zapAuth === 1) {//Needs to be changed
              this.setState({
                accessFlag: false
              });
              history.push("/tasks");
            } else if (zapAuth === 2) {
              helper.ZAPIERREDIRECT(
                helper.getToken(),                                
                localStorage.getItem("userName"),
                localStorage.getItem("zapkey")
              );
            }
          } else {
            this.setState({
              accessFlag: true
            });
          }
        });
      } else {
        this.setState({
          accessFlag: true
        });
      }
    }

    render() {
      // 
      const { accessFlag } = this.state;
      const View = () => {
        if (props[0] === "onboard") {
          return (
           
              <WrappedComponent step={props[1]} authAction={this.updateAuth} />
          
          );
        } else if (props[0] === "TwoFactorAuth") {
          return (
          
              <WrappedComponent view={props[0]} authAction={this.updateAuth} />
        
          );
        } else {
          return (
          
              <WrappedComponent view={props[0]} />
          );
        }
      };
      return <React.Fragment>{accessFlag ? <View /> : null}</React.Fragment>;
    }
  }

const mapStateToProps = (state) => {
  return {
    authState: state.authenticationResponse
  }
}

  return connect(
    mapStateToProps,
    {
      updateAuth,
      validateAuth
    }
  )(UnAuthorizedRoute);
}
