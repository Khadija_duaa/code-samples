import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import queryString from "query-string";
import helper from "../helper/index";

export default (ComposedComponent) => {
  class Authenticate extends Component {
    // Our component just got rendered
    componentDidMount() {
      this.shouldNavigateAway();
    }
    // Our component just got updated
    componentDidUpdate(prevProps) {
      const {
        location: { pathname },
      } = this.props;
      const previousLocation = prevProps.location.pathname;

      if (pathname !== previousLocation && window.userpilot) {
        window.userpilot.reload();
      }

      this.shouldNavigateAway();
    }
    shouldNavigateAway() {
      const token = helper.getToken();
      const { location, match, profileState } = this.props;
      let authRoutes = [
        "tasks",
        "projects",
        "issues",
        "risks",
        "meetings",
        "timesheet",
        "workspace-settings",
        "welcome",
      ];
      let arr = [
        "taskId",
        "issueId",
        "riskId",
        "meetingId",
        "projectId",
        "token",
        "confirmuseremail",
      ]; // Array to be used for redirection
      let q = queryString;

      let activeTeamMember = profileState.teamMember.length // Getting object of active team member
        ? profileState.teamMember.find((m) => profileState.userId == m.userId)
        : {};
        //It is to check for white label, then we just pass the code
      if(!token && this.props.match.params.companyName) {
        //this.props.history.push(location.pathname);
        //return;
      }
      else {
      if (
        (!token ||
          (this.props.auth == false &&
            arr.indexOf(
              Object.keys(queryString.parseUrl(location.search).query)[0]
            ) > -1)) &&
        (location.pathname.indexOf("/teams") !== 0 ||
          queryString.parse(location.search).upgradeplan)
      ) {
        const redirectUrl = location.pathname.slice(
          1,
          location.pathname.length
        ); // Removing "/" from the url 0 index
        this.props.history.push(
          `/account/login?redirectUrl=${redirectUrl}${this.props.location.search}`
        );
      } else if (!token) {
        this.props.history.push("/");
      } else if (this.props.auth == false) {
        this.props.history.push("/");
      }
      //Checking if there is no workspace available and user try to access the url of workspaces
      if (
        profileState &&
        ((profileState.workspace &&
          !profileState.workspace.length &&
          profileState.teams.length &&
          authRoutes.indexOf(match.params.view) > -1) ||
          (activeTeamMember.role == "Member" && // if team member with role of member try to access team settings url
            match.params.view == "team-settings"))
      ) {
        let activeTeam = profileState.teams.find((t) => {
          return t.companyId == profileState.activeTeam;
        });
        this.props.history.push(`/teams/${activeTeam ? activeTeam.companyUrl : profileState.teams[0].companyUrl}`);
      }
      }
    }

    render() {
      return this.props.auth ? <ComposedComponent {...this.props} /> : null;
    }
  }
  function mapStateToProps(state) {
    return {
      auth: state.authenticationResponse.authState,
      profileState: state.profile.data,
    };
  }
  return compose(withRouter, connect(mapStateToProps))(Authenticate);
};
