import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import "../App.css";
import { withStyles } from "@material-ui/core/styles";
import appStyles from "../appStyles";
import LoginView from "../Views/NewLogin/Login";



function CustomRoutes(props) {
  const { classes, authorized } = props;

  return (
    <Router>
      <Switch>
        <Redirect
          exact
          from="/"
          to={authorized ? "/tasks" : "/account/login"}
        />
        <Redirect
          exact
          from="/account"
          to={authorized ? "/tasks" : "/account/login"}
        />
        {authorized ? (
          <Redirect from="/account/:view" to="/tasks" />
        ) : (
          <Route path="/account/:view" component={LoginView} />
        )}
    
      </Switch>
    </Router>
  );
}

export default withStyles(appStyles, { withTheme: true })(CustomRoutes);
