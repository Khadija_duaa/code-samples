import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { defaultTheme } from "../assets/jss/theme";
import MasterTemplate from "../Views/masterTemplate/masterTemplate";
import securityHeaders from "../redux/securityHeaders/index";
import { updateAuth,  validateAuth, getUserState, getOnBoardingData } from "../redux/actions/authentication";
import { WorkspaceExists } from "./../redux/actions/workspace";
import { PopulateTempData } from "./../redux/actions/tempdata";
import helper from "../helper/index";

export default function(
  WrappedComponent,
  history,
  classes
) {
  class DashboardRoute extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        accessFlag: false
      };
    }

    componentDidMount() {
        this.checkAuthorizationAndRedirect();
    }

    componentDidUpdate(prevProps){
      if (this.props.authState.Auth !== prevProps.authState.Auth) {
        this.checkAuthorizationAndRedirect();
      }
    }

    redirectWithStatus(self, data, _status) {
      
      let companyObj = {
        passwordError: "",
        usernameError: "",
        lastNameError: "",
        firstNameError: "",
        otherCompanyIndustry: "",
        passwordErrorMessage: "",
        usernameErrorMessage: "",
        lastNameErrorMessage: "",
        firstNameErrorMessage: "",
        showPassword: false,
        companyName: "",
        jobTitle: "",
        companyIndustry: "",
        companySize: "",
        otherIndustry: false,
        errorString_companyName: null,
        otherIndustry: false,
  
        otherCompanyIndustryError: false,
        otherCompanyIndustryErrorMessage: "",
  
        otherJobTitle: "",
        otherJobTitleErrorMessage: "",
        otherJobTitleError: false,
        OtherJob: false
      };
      let workspaceObj = {
        workspaceName: "",
        workspaceUrl: "",
        workspaceUseOther: "",
        workspaceError: false,
        workspaceUrlError: false,
        workspaceErrorMessage: null,
        workspaceUrlErrorMessage: null,
        workspaceUse: "",
        otherWorkspaceOption: false
      };
      let inviteMemberObj = {
        emailAddressErrorMessage: "",
        emailAddressError: false,
        inviteEmailAddress: "",
        inviteMemberList: [],
        teamId: ""
      };
  
      companyObj.companyName = data.companyName || "";
      companyObj.jobTitle = { value: data.jobTitle, label: data.jobTitle } || "";
      companyObj.otherIndustry = false;
      companyObj.companyIndustry =
        { value: data.industry, label: data.industry } || "";
      companyObj.otherCompanyIndustry = "";
      companyObj.companySize = data.teamSize || "";
  
      if (
        companyObj.companyIndustry &&
        companyObj.companyIndustry.value &&
        companyObj.companyIndustry.value.toLowerCase() === "other"
      ) {
        companyObj.otherIndustry = true;
        companyObj.otherCompanyIndustry = data.industryDetails;
      }
  
      if (
        companyObj.jobTitle &&
        companyObj.jobTitle.value &&
        companyObj.jobTitle.value.toLowerCase() === "other"
      ) {
        companyObj.OtherJob = true;
        companyObj.otherJobTitle = data.jobTitleDetails;
      }
  
      workspaceObj.workspaceName = data.teamName || "";
      workspaceObj.workspaceUrl = data.teamName || "";
      workspaceObj.workspaceUse = { value: data.usage, label: data.usage } || "";
      if (
        companyObj.workspaceUse &&
        companyObj.workspaceUse.value &&
        companyObj.workspaceUse.value.toLowerCase() === "other"
      ) {
        workspaceObj.workspaceUseOther = data.usage;
        workspaceObj.otherWorkspaceOption = true;
      }
      localStorage.setItem("CompanyInfo", JSON.stringify(companyObj));
      localStorage.setItem("CreateWorkspace", JSON.stringify(workspaceObj));
      localStorage.setItem("InviteMembers", JSON.stringify(inviteMemberObj));
      switch (_status) {
        case 4: //Company Information
          localStorage.setItem(
            "Onboarding",
            JSON.stringify({
              activeStep: 1,
              updatesCheckbox: false
            })
          );
          self.props.history.push("/welcome/company");
          break;
        case 3: //WorkSpace Information
          localStorage.setItem(
            "Onboarding",
            JSON.stringify({
              activeStep: 2,
              updatesCheckbox: false
            })
          );
          self.props.history.push("/welcome/create-workspace");
          break;
        case 2: //Invite Members
          localStorage.setItem(
            "Onboarding",
            JSON.stringify({
              activeStep: 3,
              updatesCheckbox: false
            })
          );
          self.props.history.push("/welcome/invite-members");
          break;
        case 1: //Youtube
          self.props.history.replace("/welcome/getstarted");
        default:
          self.props.authAction(true);
          self.props.history.push("/tasks");
          break;
      }
    }

    OnBoardingData_Login = (self, _status) => {
     // 
        if (_status === 1) {
          self.props.history.replace("/welcome/getstarted");
        } else {
          self.props.getOnBoardingData(data => {
            if (data.status === 200) {
              self.redirectWithStatus(self, data.data, _status);
            }
          });
      }
     
    };

    
    checkWorkSpaceExist = () =>{
      this.props.WorkspaceExists(data=>{
        if(data && data.status === 200){
          if(data.data === true)
          {
            this.props.updateAuth(true);
              this.setState({
                accessFlag: true
              });  
          }
          else{
            this.props.PopulateTempData("Enter-WorkSpace");
            this.props.history.push("/welcome/create-workspace");
          }
          
        }
        else{
          
        }
      });
    }
    
    
    stateCheck=()=>{
     // 
        this.props.getUserState(data => {
            if (data && data.data.state > 0) {//Redirect To Onboard
              this.setState({
                accessFlag: false
              });
              
              this.props.updateAuth(false);
              this.OnBoardingData_Login(this, data.data.state);
            } else {
              this.checkWorkSpaceExist();
            }
        });
    }

    checkAuthorizationAndRedirect=()=> {
       if (
        (helper.getToken() === null ||
          helper.getToken() === undefined) &&
        !securityHeaders.GETHEADERS().headers.headers.Authorization
      ) {
        this.setState({
          accessFlag: false
        });
        this.props.updateAuth(false);
        history.push("/account/login");
      } else {
        this.props.validateAuth(data => {
          if (data.status === 200) {
            this.stateCheck();
          }
          else {
            this.setState({
              accessFlag: false
            });
            this.props.updateAuth(false);
            history.push("/account/login");
          }
        });
      }
    }

    render() {
      const { accessFlag } = this.state;
     return (
       <React.Fragment>
         {
           accessFlag? (
            <div className={classes.root}>
              <MasterTemplate />
              <main className={classes.content}>
                <WrappedComponent />
              </main>
            </div>
        ) : null
         }
       </React.Fragment>
     )
    
    }
  }

const mapStateToProps = (state) => {
  return {
    authState: state.authenticationResponse
  }
}

  return connect(
    mapStateToProps,
    {
      updateAuth,
      validateAuth,
      getUserState,
      getOnBoardingData,
      WorkspaceExists,
      PopulateTempData
    }
  )(DashboardRoute);
}