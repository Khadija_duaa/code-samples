export const LOCALES = {
    en_US: 'en-US',
    en_GB: 'en-GB',
    es_ES: 'es-ES',
    fr_FR: 'fr-FR',
    ar_SA: 'ar-SA',
    zh_CN:  'zh-CN',
    de_DE:  'de-DE',
    pt_PT:  'pt-PT',
    ur_PK:  'ur-PK',
    ru_RU:  'ru_RU'
}