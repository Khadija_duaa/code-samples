import en_US from './en-US.json';
import en_GB from './en-GB.json';
import zh_CN from './zh-CN.json';
import es_ES from './es-ES.json';
import fr_FR from './fr-FR.json';
import ar_SA from './ar-SA.json';
import de_DE from './de-DE.json';
import pt_PT from './pt-PT.json';
import ur_PK from './ur-PK.json';
import ru_RU from "./ru-RU.json";
const messages = {
    'en-US':en_US,
    'en-GB':en_GB,
    'es-ES':es_ES,
    'fr-FR':fr_FR,
    'ar-SA':ar_SA,
    'de-DE':de_DE,
    'pt-PT':pt_PT,
    'ur-PK':ur_PK,
    'zh-CN':zh_CN,
    'ru-RU':ru_RU
};
export default messages