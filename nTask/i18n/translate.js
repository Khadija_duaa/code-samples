import React from 'react';
import { FormattedMessage } from 'react-intl';
const translate = (id,defaultText={}) => <FormattedMessage id={id}  defaultMessage={{...defaultText}}  />;

export default translate