import React, { Component, memo } from "react";
import { withStyles } from "@material-ui/styles";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withSnackbar } from "notistack";

function withSnackbarNotifs(WrappedComponent) {
  class withSnackbarNotifsInternal extends Component {
    constructor(props) {
      super(props);
    }

    showSnackBar = (snackBarMessage = "", type = null) => {
      this.props.enqueueSnackbar(
        <div className={this.props.classes.snackBarHeadingCnt}>
          <p className={this.props.classes.snackBarContent}>{snackBarMessage}</p>
        </div>,
        {
          anchorOrigin: {
            vertical: "top",
            horizontal: "centerdp",
          },
          variant: type ? type : "info",
        }
      );
    };

    render() {
      return <WrappedComponent showSnackbar={this.showSnackBar} {...this.props} />;
    }
  }

  // return SS;
  // return withSnackbar(SS);

  const WithInjectedHOCs = compose(
    withSnackbar,
    withStyles({}, { withTheme: true })
  )(withSnackbarNotifsInternal);

  const Memoized = memo(WithInjectedHOCs);

  return Memoized;
}

withSnackbarNotifs.propTypes = {
  WrappedComponent: PropTypes.elementType.isRequired,
};

withSnackbarNotifs.defaultProps = {};

export default withSnackbarNotifs;
