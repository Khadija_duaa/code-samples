import instance from "../instance";

export function getColumnsList(url, callback, failure) {

    return instance()
        .get(url)
        .then(response => {
            callback(response);
        })
        .catch(err => {
            failure(err.response);
        });
}
export function exportSelectedColumns(url, data, callback, failure) {
    return instance()
        .post(url, data, { 'responseType': 'blob' })
        .then(response => {
            callback(response);
        })
        .catch(err => {
            failure(err.response);
        });
}


export function ImportFile(data, Url, config, callback, failure) {

    return instance()
        .post(Url, data, config)
        .then(response => {
            callback(response);
        })
        .catch(err => {
            failure(err.response);
        });
}

export function ImportBulkTasksFile(data, Url, config, callback, failure) {

    return instance()
        .post(Url, data, config)
        .then(response => {
            callback(response);
        })
        .catch(err => {
            failure(err.response);
        });
}


export function ExportToCSV(data, Url, callback, failure) {

    return dispatch => {
        return instance()
            .post(Url, data, {
                responseType: 'blob'
            })
            .then(response => {
                callback(response);
            })
            .catch(err => {
                failure(err.response)
                // var reader = new FileReader();
                // reader.onload = function () {
                //     callback(JSON.parse(this.result), null);
                // }
                // reader.readAsText(new Blob([err.data])
                // );
            });
    };
}
export default {
    ImportFile,
    ExportToCSV,
    ImportBulkTasksFile
};