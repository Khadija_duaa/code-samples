import constants from "./../constants/types";
import instance from "../instance";

export function publickLink(data) {
  return {
    type: constants.PUBLICLINKDIALOG,
    payload: data,
  };
}

export function publickLinkDialog(data) {
  return dispatch => {
    dispatch(publickLink(data));
  };
}

export function setRiskId(dialogState) {
  return {
    type: constants.RISKDETAILDIALOG,
    payload: dialogState,
  };
}

export function riskDetailDialog(dialogState, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(setRiskId(dialogState));
  } else {
    return dispatch => {
      dispatch(setRiskId(dialogState));
    };
  }
}

export function setDeleteDialogueState(dialogState) {
  return {
    type: constants.DELETEDIALOGUESTATE,
    payload: dialogState,
  };
}
export function updateDeleteDialogState(dialogState) {
  return dispatch => {
    dispatch(setDeleteDialogueState(dialogState));
  };
}

export function setCustomFieldDialogueState(dialogState) {
  return {
    type: constants.CUSTOMFIELDDIALOGUESTATE,
    payload: dialogState,
  };
}
export function updateCustomFieldDialogState(dialogState) {
  return dispatch => {
    dispatch(setCustomFieldDialogueState(dialogState));
  };
}

export function setTaskId(dialogState) {
  return {
    type: constants.TASKDETAILDIALOG,
    payload: dialogState,
  };
}

export function taskDetailDialogState(dispatch, dialogState) {
  if (dispatch) {
    dispatch(setTaskId(dialogState));
  } else {
    return dispatch => {
      dispatch(setTaskId(dialogState));
    };
  }
}

export function setIssueDetailDialogState(dialogState) {
  return {
    type: constants.ISSUEDETAILDIALOG,
    payload: dialogState,
  };
}

export function issueDetailDialogState(dispatch, dialogState) {
  if (dispatch) {
    dispatch(setIssueDetailDialogState(dialogState));
  } else {
    return dispatch => {
      dispatch(setIssueDetailDialogState(dialogState));
    };
  }
}
export function setProfileSettingsState(dialogState) {
  return {
    type: constants.PROFILESETTINGSDIALOGUE,
    payload: dialogState,
  };
}

export function profileSettingState(dialogState) {
  return dispatch => {
    dispatch(setProfileSettingsState(dialogState));
  };
}
