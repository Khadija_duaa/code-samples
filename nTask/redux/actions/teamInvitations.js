import constants from "../constants/types";
import instance from "../instance";

export const reduceTeamCountAction = () => {
  return {
    type: constants.MINUSTEAMCOUNT,
    payload: 1
  };
};
export const increaseTeamCountAction = () => {
  return {
    type: constants.ADDTEAMCOUNT,
    payload: 1
  };
};
export const dispatchNotificationAction = invite => {
  return {
    type: constants.ADDPENDINGINVITE,
    payload: invite
  };
};
export const removeTeamInvitationAction = invite => {
  return {
    type: constants.REMOVETEAMINVITATION
  };
};

export function getTeamInvitations(callback, failure) {
  return instance()
    .get("api/Account/GetTeamInvitations")
    .then(response => {
      callback(response);
    })
    .catch(err => {
      callback(err.response);
    });
}
export function updateTeamCount(callback, failure) {
  return dispatch => {
    dispatch(increaseTeamCountAction());
  };
}
export function acceptTeamInvitation(
  teamId,
  callback,
  failure,
  updateTeamCount
) {
  return dispatch => {
    return instance()
      .get(`api/company/AcceptTeamInvitation?TeamID=${teamId}&Action=true`)
      .then(response => {
        callback(response);
        dispatch({
          type: constants.ADDTEAMTOTEAMLIST,
          payload: response.data.entity
        });
        if (updateTeamCount) {
          dispatch(reduceTeamCountAction());
        }
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function rejectTeamInvitation(
  teamId,
  callback,
  failure,
  updateTeamCount
) {
  return dispatch => {
    return instance()
      .get(`api/company/AcceptTeamInvitation?TeamID=${teamId}&Action=false`)
      .then(response => {
        callback(response);
        if (updateTeamCount) {
          dispatch(reduceTeamCountAction());
        }
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function dispatchNotification(invite) {
  return dispatch => {
    dispatch(dispatchNotificationAction(invite));
  };
}
export function removeTeamInvitation() {
  return dispatch => {
    dispatch(removeTeamInvitationAction());
  };
}
