import instance from "../instance";
import timesheet from "../reducers/timesheet";

export function GetHelpingMaterial(location, success, fail) {
  instance()
    .get(`/api/help${location}`)
    .then(result => {
      success(result);
    })
    .catch(error => {
      fail(error.response);
    });
}
