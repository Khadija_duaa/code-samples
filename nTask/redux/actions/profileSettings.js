
import instance from "./../instance";
import axios from "axios";
import constants from "./../constants/types";
import { FetchConstants } from "./constants";
import { UpdateUserProfileImage } from './profile';

export function RemoveProfileImage(parms = '', callback) {
  //api/ntask/removeprofileimage
  return dispatch => {
    return instance()
      .get(`api/ntask/removeprofileimage${parms}`)
      .then(response => {
        dispatch(UpdateUserProfileImage(response.data));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  }
}

export function SaveProfile(data, callback) {
  instance()
    .post("/api/NTask/SaveProfile", data)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      callback(response.response);
    });
}
export function toggleLegacyData(data, callback) {
  instance()
    .post("/api/EnableLegacyData", data)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      callback(response.response);
    });
}

export function SaveLocalization(data, callback) {
  return dispatch => {
    instance()
      .post("/api/ntask/SaveLocalization", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  }
}

export function cancelEmailRequest(callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/ntask/CancelChangeEmailRequest")
      .then(response => {
        dispatch(cancelEmailAddressRequest());
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  }
}
export function cancelEmailAddressRequest() {
  return {
    type: constants.CANCELEMAILREQUEST,
    payload: {}
  };
}


export function confirmChangeEmail(callback, failure) {
  instance()
    .get("/api/ntask/ConfirmChangeEmail")
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response.response);
    });
}export function confirmUserEmail(token, callback, failure) {
  instance()
    .get(`api/account/verifyuseremail?usertoken=${token}`)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function ChangePassword(data, callback, failure) {
  instance()
    .post("/api/Account/ChangePassword", data)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function changeEmailAddress(data, callback, failure) { /** Action creator for changing the email address */

  return dispatch => {
    return instance()
      .get(`/api/ntask/ChangeUserEmail?email=${data}`) /** the data contains the new email address coming from iput field in change email section */
      .then(response => {
        dispatch(changeEmailAddressAction(response.data));
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  }
}

export function changeEmailAddressAction(data) {

  return {
    type: constants.UPDATEEMAILADDRESS,
    payload: {
      data
    }
  };
}

export function UpdatePreference(data) {
  return {
    type: constants.UPDATEPREFERENCE,
    payload: {
      data
    }
  };
}

export function UpdateUserPreferences(data, callback) {
  return dispatch => {
    return instance()
      //.post("/api/Notification/UpdateUserPreferences", data)UpdatePreferences
      .post("/api/Notification/UpdateUserPreferences", data)
      .then(response => {
        dispatch(UpdatePreference(data.updatePreferencesList));
        return { response: response };
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  }
}

export function GetAuthyQrCode(callback) {
  return dispatch => {
    return instance()
      .get("/api/account/getauthyqrcode")
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  }
}

export function VerifyAuthyCode(code, callback) {
  return dispatch => {
    return instance()
      .get("/api/account/verifyauthycode?code=" + code)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  }
}

export function EnableTwoFactorAuth(callback) {
  return dispatch => {
    return instance()
      .post("/api/Account/EnableTwoFactorAuth")
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  }
}

export function DisableTwoFactorAuth(callback, authEnum) {
  return dispatch => {
    return instance()
      .get(`api/account/disabletwofactorauth?twoFactor=${authEnum}`)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  }
}
export function DELETEACCOUNTPREFERENCE() {
  return {
    type: constants.POPULATEUSERPREFERNCE,
  }
}

export function populatePreference(data) {
  //
  return {
    type: constants.POPULATEUSERPREFERNCE,
    payload: {
      data
    }
  };
}

export function GetUserPreferences(callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/NTask/GetPreference")
      .then(response => {
        FetchConstants(dispatch, { data: response.data.constant });
        dispatch(populatePreference(response.data));
        return { response: response };
      })
      .then(response => {
        callback(response);
      })
      .catch(res => {
        failure(res);
      });
  }
}

export default {
  SaveProfile,
  SaveLocalization,
  ChangePassword,
  UpdateUserPreferences,
  GetAuthyQrCode,
  VerifyAuthyCode,
  EnableTwoFactorAuth,
  DisableTwoFactorAuth,
  GetUserPreferences,
  UpdatePreference,
  changeEmailAddress,
  confirmChangeEmail,
  cancelEmailRequest
}
