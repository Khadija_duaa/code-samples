import constants from "./../constants/types";
import instance from "./../instance";

export function getTimesheetWeekDurations(parms, callback) {
  return dispatch => {
    return instance()
      .get(`api/timesheet/GetTimeSheetdetails?timeSheetType=Weekly${parms}`)
      .then(response => {
        dispatch({
          type: constants.TIMESHEETGETWEEKDURATIONS,
          payload: response.data
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}
export function getTeamTimesheetWeekDurations(parms, callback) {
  return dispatch => {
    return instance()
      .get(`api/TeamTimeSheet/GetTimeSheetdetails?timeSheetType=Weekly${parms}`)
      .then(response => {
        dispatch({
          type: constants.TIMESHEETGETWEEKDURATIONS,
          payload: response.data
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}
export function selectTimesheetWorkspaces(data, callback) {
  return dispatch => {
    return instance()
      .put('api/TeamTimeSheet/AddWorkspaces', data).then(response => {
        dispatch({
          type: constants.TIMESHEETGETWEEKDURATIONS,
          payload: response.data.entity.timesheets
        });
        if (callback) callback()
      })
  }
}
export function getTimesheetTasks(data, callback) {
  return dispatch => {
    return instance()
      .get('api/TeamTimeSheet/GetTaskList').then(response => {

        dispatch({
          type: constants.ADDTASKS,
          payload: response.data.userTasks
        })
      })
  }
}

export function removeTask(projectId, taskId, weekDate, callback, failure) {
  return dispatch => {
    return instance()
      .delete(`api/TimeSheet/DeleteTask?taskId=${taskId}&weekDate=${weekDate}`)
      .then(response => {
        dispatch({
          type: constants.REMOVETASK,
          payload: { projectId, taskId }
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      }).catch(response => {
        failure(response.response);

      });
  };
} export function removeTaskLogInTeam(projectId, taskId, weekDate, callback, failure) {
  return dispatch => {
    return instance()
      .delete(`api/TeamTimeSheet/DeleteTask?taskId=${taskId}&weekDate=${weekDate}`)
      .then(response => {
        dispatch({
          type: constants.REMOVETASK,
          payload: { projectId, taskId }
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      }).catch(response => {
        failure(response.response);

      });
  };
}
export function addNewTask(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/TimeSheet/AddTaskInTimeSheet", data)
      .then(response => {
        const { data } = response;
        dispatch({
          type: constants.ADDNEWTASKINTIMESHEET,
          payload: {
            data
          }
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      }).catch(response => {
        failure(response.response);

      });
  };
} export function addNewTaskInTeamTimesheet(data, callback) {
  return dispatch => {
    return instance()
      .post("api/TeamTimeSheet/AddTaskInTimeSheet", data)
      .then(response => {
        const { data } = response;
        dispatch({
          type: constants.ADDNEWTASKINTIMESHEET,
          payload: {
            data
          }
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function addTaskDuration(data, callback, failure) {
  const projectId = data.projectId;
  const workspaceId = data.workspaceId;
  return dispatch => {
    return instance()
      .post("api/TimeSheet/AddTimesheetEffort", data)
      .then(response => {
        dispatch({
          type: constants.TIMESHEETADDTASKDURATION,
          payload: {
            taskId: response.data.timesheetEffort.taskId,
            duration: response.data.timesheetEffort.timestamp[0].duration,
            day: response.data.timesheetEffort.timestamp[0].date,
            projectId,
            workspaceId
          }
        });
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
} export function addTeamTaskDuration(data, callback, failure) {
  const projectId = data.projectId;
  const workspaceId = data.workspaceId;
  return dispatch => {
    return instance()
      .post("api/TeamTimeSheet/AddTimesheetEffort", data)
      .then(response => {
        dispatch({
          type: constants.TIMESHEETADDTASKDURATION,
          payload: {
            taskId: response.data.timesheetEffort.taskId,
            duration: response.data.timesheetEffort.timestamp[0].duration,
            day: response.data.timesheetEffort.timestamp[0].date,
            projectId,
            workspaceId
          }
        });
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function submitTimesheet(data, callback, failure) {
  const projectId = data.projectId;
  return dispatch => {
    return instance()
      .post("api/TimeSheet/SubmitTimeSheet", data)
      .then(response => {
        const { data } = response;
        dispatch({
          type: constants.SUBMITTIMESHEET,
          payload: {
            projectId,
            data
          }
        });
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
} export function submitTeamTimesheet(data, callback, failure) {
  const projectId = data.projectId;
  return dispatch => {
    return instance()
      .post("api/TeamTimeSheet/SubmitTimeSheet", data)
      .then(response => {
        const { data } = response;
        dispatch({
          type: constants.SUBMITTIMESHEET,
          payload: {
            projectId,
            data
          }
        });
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function getPendingTimesheetWeekDurations(parms, callback) {
  return dispatch => {
    return instance()
      .get(
        `api/TimeSheet/GetSubmittedTimeSheetdetails?timeSheetType=Weekly${parms}`
      )
      .then(response => {
        dispatch({
          type: constants.PENDINGTIMESHEETGETWEEKDURATIONS,
          payload: response.data
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}
export function getPendingTeamTimesheetWeekDurations(parms, callback) {
  return dispatch => {
    return instance()
      .get(
        `api/TeamTimeSheet/GetSubmittedTimeSheetdetails?timeSheetType=Weekly${parms}`
      )
      .then(response => {
        dispatch({
          type: constants.PENDINGTIMESHEETGETWEEKDURATIONS,
          payload: response.data
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function rejectPendingTimesheet(data, callback) {
  const projectId = data.projectId;
  return dispatch => {
    return instance()
      .post("api/TimeSheet/RejectTimeSheet", data)
      .then(response => {
        const { data } = response;
        dispatch({
          type: constants.REJECTPENDINGTIMESHEET,
          payload: {
            projectId,
            data
          }
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}
export function withdrawTimesheet(data, callback) {
  const projectId = data.projectId;
  return dispatch => {
    return instance()
      .post("api/TimeSheet/withdrawTimesheet", data)
      .then(response => {
        const { data } = response;
        dispatch({
          type: constants.REJECTPENDINGTIMESHEET,
          payload: {
            projectId,
            data
          }
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}
export function withdrawTeamTimesheet(data, callback) {
  const projectId = data.projectId;
  return dispatch => {
    return instance()
      .post("api/TeamTimeSheet/withdrawTimesheet", data)
      .then(response => {
        const { data } = response;
        dispatch({
          type: constants.REJECTPENDINGTIMESHEET,
          payload: {
            projectId,
            data
          }
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}
export function rejectTeamPendingTimesheet(data, callback) {
  const projectId = data.projectId;
  return dispatch => {
    return instance()
      .post("api/TeamTimeSheet/RejectTimeSheet", data)
      .then(response => {
        const { data } = response;
        dispatch({
          type: constants.REJECTPENDINGTIMESHEET,
          payload: {
            projectId,
            data
          }
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function approvePendingTimesheet(data, callback) {
  const projectId = data.projectId;
  return dispatch => {
    return instance()
      .post("api/TimeSheet/ApproveTimeSheet", data)
      .then(response => {
        const { data } = response;
        dispatch({
          type: constants.APPROVEPENDINGTIMESHEET,
          payload: {
            projectId,
            data
          }
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
} export function approvePendingTeamTimesheet(data, callback) {
  const projectId = data.projectId;
  return dispatch => {
    return instance()
      .post("api/TeamTimeSheet/ApproveTimeSheet", data)
      .then(response => {
        const { data } = response;
        dispatch({
          type: constants.APPROVEPENDINGTIMESHEET,
          payload: {
            projectId,
            data
          }
        });
        return response;
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function sendTimesheetEmail(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/TimeSheet/SendTimeSheetEmail", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function getDateTaskEffort(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/timesheet/GetSelectedDateEffort", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function getTeamDateTaskEffort(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/TeamTimeSheet/GetSelectedDateEffort", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function SaveTotalTime(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/TeamTimeSheet/SaveWeeklyEfforts", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function SaveTotalTimeTitle(data, callback, failure) {
  return dispatch => {
    dispatch({ type: constants.UPDATE_TOTAL_TIME_TITLE, payload: data })
    return instance()
      .post("api/TeamTimeSheet/SaveTimeSheetColumnsLabel", data)
      .then(response => {
        callback(response);

      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export async function copyTimeSheet(data, callback, failure) {
  return instance()
    .post(`api/Timesheet/CopyTimeSheet`, data)
    .then(response => callback(response))
    .catch(response => failure(response.response));
}
