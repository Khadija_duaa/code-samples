import constants from "../constants/types";
import instance from "../instance";
import { PopulateDefaultProjectTaskTemplate} from "./tasks";
import { PopulateDefaultProjectTemplate} from "./projects";

// export function SaveStatusItem(data = {}) {
//   return {
//     type: constants.SAVEPROJECTTEMPLATESTATUSITEM,
//     payload: {
//       data,
//     },
//   };
// }

// export function updateStatusAfterSettingDefaultStatus(data) {
//   return {
//     type: constants.UPDATEPROJECTSTATUSAFTERSETTINGDEFAULT,
//     payload: {
//       data
//     }
//   };
// }
// export function deleteStatus(data) {
//   return {
//     type: constants.UPDATEDELETEDPROJECTSTATUS,
//     payload: {
//       data
//     }
//   };
// }
// export function SaveProjectTemplateStatusItem(data, success, fail) {
//   return dispatch => {
//     return instance()
//       .put("api/project/updateprojectcustomstatus", data)
//       .then(response => {
//         dispatch(SaveStatusItem(response.data.customstatus));
//         success(response.data.customstatus);
//       })
//       .catch(response => {
//         fail(response.response);
//       });
//   };
// }
// export function MapTaskWithSetDefaultStatus(data, success, fail) {
//   return dispatch => {
//     return instance()
//       .post("api/project/ProjectMapTaskWithSetDefaultStatus", data)
//       .then(response => {
//         dispatch(updateStatusAfterSettingDefaultStatus(response.data));
//         dispatch(PopulateDefaultProjectTemplate({projectId: data.projectId, mappingStatus: data.mappingStatus}));
//         dispatch(PopulateDefaultProjectTaskTemplate({projectId: data.projectId, mappingStatus: data.mappingStatus}));
//         success(response.data.customstatus);
//       })
//       .catch(response => {
//         fail(response.response);
//       });
//   };
// }
// export function DeleteAndMapStatus(data, success, fail) {
//   return dispatch => {
//     return instance()
//       .put("api/project/ProjectDeleteAndMapStatus", data)
//       .then(response => {
//         dispatch(deleteStatus(response.data));
//         success(response);
//       })
//       .catch(response => {
//         fail(response.response);
//       });
//   };
// }
//it is called when user click on nTask default template and change it and click UseThisCustomTemplate button
export function UseCustomTemplateInProject(data, success, fail, callback) {
  return dispatch => {
    return instance()
      .post("api/project/SaveCustomTemplate", data)
      .then(response => {
        dispatch(PopulateDefaultProjectTemplate({projectId: data.projectId, template: response.data.template}));
        dispatch(PopulateDefaultProjectTaskTemplate({projectId: data.projectId, mapping: response.data.mapping}));
        success(response.data.template);
        if(callback) callback({projectId: data.projectId, mapping: response.data.mapping});
      })
      .catch(response => {
        fail(response.response);
      });
  };
}
//it is called when user change custome template and save that
export function UpdateProjectCustomTemplate(data, success, fail, callback) {
  return dispatch => {
    return instance()
      .post("api/project/SaveCustomTemplate", data)
      .then(response => {
        dispatch(PopulateDefaultProjectTemplate({projectId: data.projectId, template: response.data.template}));
        dispatch(PopulateDefaultProjectTaskTemplate({projectId: data.projectId, mapping: response.data.mapping}));
        success(response.data.template);
        if(callback) callback({projectId: data.projectId, mapping: response.data.mapping});
      })
      .catch(response => {
        fail(response.response);
      });
  };
}
//it is called when click on nTask default template and click UseThisTemplate
export function UseTemplateInProject(data, success, fail, callback) {
  return dispatch => {
    return instance()
      .post("api/Template/UseTemplate", data)
      .then(response => {
        dispatch(PopulateDefaultProjectTemplate({projectId: data.projectId, template: response.data.entity.template}));
        dispatch(PopulateDefaultProjectTaskTemplate({projectId: data.projectId, mapping: response.data.entity.mapping}));
        success(response.data.entity.template);
        if(callback) callback({projectId: data.projectId, mapping: response.data.entity.mapping});
      })
      .catch(response => {
        fail(response.response);
      });
  };
}

export function dispatchAddStatusItems(data, check) {
  //data.taskId

  return dispatch => {
    // if (check == "Create") dispatch(CreateStatusItem(data.statusItem,data.groupId));
    // else if (check == "Update") dispatch(SaveStatusItem(data.statusItem, data.groupId));
    // else if (check == "Delete") dispatch(DeleteStatusItem(data.statusItem, data.groupId));
    // else if (check == "updateStatusOrder") dispatch(UpdateStatusItemsOrder(data.statusItem, data.groupId));
  };
}

export default {
  // SaveProjectTemplateStatusItem,
  // SaveStatusItem,
  dispatchAddStatusItems,
  // MapTaskWithSetDefaultStatus,
  // DeleteAndMapStatus,
};
