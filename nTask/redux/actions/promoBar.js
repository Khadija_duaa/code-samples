import constants from "./../constants/types";

export const promoBarShowHideAction = (data) => ({
  type: constants.SHOWHIDEPROMOBAR,
  payload: {
    data
  }
})
export function showHidePromoBar(data) {
  
  return dispatch => {
    return dispatch(promoBarShowHideAction(data))
  };
}



