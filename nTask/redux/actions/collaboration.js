import instance from "../instance";
import constants from "./../constants/types";

let base_url = "https://chat.ntaskmanager.com/gateway/"
let _payload;

export const createChatGroups = async (dispatch, success, failure) => {

  return instance()
    .post(`${base_url}CreateChatRoom`, _payload)
    .then((res) => {
      let _res = res && res.data &&
        res.data.entity && res.data.entity;
      dispatch(createChat(_res));
      success(res)
    })
    .catch((err) => {
      failure();
    });
}

export const updateChatRoom = async (groupName, id, success, failure) => {

  return instance()
    .patch(`${base_url}UpdateRoomName?roomId=${id}&roomName=${groupName}`, _payload)
    .then((res) => {
      success(res)
    })
    .catch((err) => {
      failure(err);
    });
}

export const isChatGroupAlreadyExists = (payload, profileState, dispatch, success, failure) => {

  let _selParticipants = [];
  let { userId } = profileState && profileState.data
  if (payload && payload.length === 1) {
    if (payload && payload[0].id === userId) {
      _payload = {
        groupId: "",
        groupName: payload[0].label,
        shareChatHistory: true,
        members: [
          payload[0].id
        ],
        groupType: "self"
      }
    }
    else {
      _payload = {
        groupId: "",
        groupName: payload[0].label,
        shareChatHistory: true,
        members: [
          payload[0].id, userId
        ],
        groupType: "privateChat"
      }
    }

  }
  else {

    payload && payload.map((data, ind) => {
      _selParticipants.push(data.id)
    })

    _payload = {
      groupId: "",
      groupName: payload && payload.length === 2 ? `${payload[0].label}, ${payload[1].label}` :
        `${payload[0].label}, ${payload[1].label}...`,
      shareChatHistory: true,
      members: _selParticipants,
      groupType: "customgroup"
    }

  }

  let _data = {
    groupType: _payload.groupType,
    groupMembers: _payload.members
  }
  return instance()
    .post(`${base_url}IsChatRoomAlreadyExist`, _data)
    .then((res) => {
      success(res)
    })
    .catch((err) => {
      failure(err);
    });
}

export const getChatGroups = (pageNo, dispatch, success, failure) => {

  return instance()
    .get(`${base_url}GetRooms?groupType=&pageNumber=${pageNo}&pageSize=20`)
    .then((res) => {
      let { entities } = res && res.data;
      dispatch(populateGroupList(entities));
      success(res);
    })
    .catch((err) => {
      failure(err);
    });
}

export const getGroupMessages = (groupInfo, dispatch, success, failure) => {

  let _list = [];
  return instance()
    .get(`${base_url}GetMessages?groupId=${groupInfo.groupId}`)
    .then((res) => {
      let _group = res && res.data && res.data.entities && res.data.entities;
      if (_group && _group.length > 0) {
        for (let ind = 0; ind < _group.length; ind++) {
          _list.push({
            message: _group[ind].userMessage.message,
            time: _group[ind].userMessage.createdDate,
            name: _group[ind].fullName,
            imageUrl: _group[ind].pictureUrl,
            createdBy: _group[ind].userMessage.createdBy
          })
        }
      }
      groupInfo._userMessage = _list;
      dispatch(createChat(groupInfo));
      success()
    })
    .catch((err) => {
      failure(err);
    });
}


export const sendMessage = async (payload, dispatch, success, failure) => {

  let _payload = {
    message: payload.messageText,
    messageType: 1,
    replyOf: {
      messageId: "",
      message: "",
      userId: ""
    },
    messageSeenBy: [
      ""
    ],
    sharedWith: [
      "all"
    ],
    replyLaterBy: [
      ""
    ],
    replyLater: false,
    parentId: "",
    userAttachmentId: "",
    groupId: payload.groupId
  }

  return instance()
    .post(`${base_url}SendMessage`, _payload)
    .then((res) => {
      dispatch(sendMessageChat(res));
      success(res);
    })
    .catch((err) => {
      failure();
    });
}


export const populateGroupList = (data) => {
  return {
    type: constants.POPULATEGROUPS,
    payload:
      data
  };
}

export const createChat = (data) => {
  return {
    type: constants.NEWGROUPITEM,
    payload:
      data
  };
}

export const sendMessageChat = (data) => {
  return {
    type: constants.SENDMESSAGE,
    payload: {
      data
    }
  };
}

export const resetRedux = () => {
  return {
    type: constants.RESETREDUX,
  };
}



export function resetChatStore(dispatchFn) {
  dispatchFn(resetRedux());
}
