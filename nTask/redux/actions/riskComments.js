import constants from "./../constants/types";
import instance from "../instance";

export function PopulateRiskCommentData(data) {
    return {
        type: constants.POPULATERISKCOMMENTS,
        payload: {
            data,
        }
    };
}

export function UpdateRiskCommentData(data) {
    return {
        type: constants.UPDATERISKCOMMENTS,
        payload: {
            data,
        }
    };
}

export function DeleteRiskCommentData(data) {
    return {
        type: constants.DELETERISKCOMMENTS,
        payload: {
            data,
        }
    };
}

export function AddNewRiskCommentData(data) {
    return {
        type: constants.ADDNEWRISKCOMMENT,
        payload: {
            data,
        }
    };
}


export function PopulateRiskActivities(data) {
    return {
        type: constants.POPULATERISKCOMMENTSACTIVITIES,
        payload: {
            data,
        }
    };
}


export function PopulateRiskNotifications(data) {
    return {
        type: constants.POPULATERISKCOMMENTSNOTIFICATIONS,
        payload: {
            data,
        }
    };
}
export function ClearRiskNotifications(data) {
    return {
        type: constants.CLEARRISKCOMMENTSNOTIFICATIONS,
        payload: {
            data,
        }
    };
}

export function updateRiskNotifications(data) {
  return {
    type: constants.UPDATERISKNOTIFICATIONS,
    payload: {
      data,
    },
  };
}

export function UpdateRiskNotifications(data) {
  return (dispatch) => {
    dispatch(updateRiskNotifications(data));
  };
}
export default {
    PopulateRiskCommentData,
    UpdateRiskCommentData,
    DeleteRiskCommentData,
    AddNewRiskCommentData,
    PopulateRiskActivities,
    PopulateRiskNotifications,
    ClearRiskNotifications,
    UpdateRiskNotifications,
    updateRiskNotifications
};