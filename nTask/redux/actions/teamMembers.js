import constants from "./../constants/types";
import instance from "../instance";
import workspace from "./workspace";
import profile from "./profile";
import { store } from "../../index";
import nTaskIcon from "../../assets/images/nTask32x32.png";
import axios from "axios";

export function SwitchToTeamInfo(data) {
  return {
    type: constants.POPULATEUMEMBERSINFO,
    payload: {
      data,
    },
  };
}
export function ResetMembersInfo() {
  return {
    type: constants.RESETMEMBERS,
  };
}
export function PopulateAllMembersInfo(data) {
  return {
    type: constants.POPULATEALLMEMBERSINFO,
    payload: {
      data,
    },
  };
}
export function ResetAllMembersInfo() {
  return {
    type: constants.RESETALLMEMBERS,
  };
}

export function PopulateWhiteLabelInfo(data) {
  document.title = data ? data.companyName : document.title;
  return {
    type: constants.POPULATEWHITELABELINFO,
    payload: {
      data,
    },
  };
}

export function switchTeam(teamId, callback, failure = () => { }) {
  instance()
    .get("/api/company/switchteam?teamId=" + teamId)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response.response);
    });
}
export function DefaultWhiteLabelInfo(dispatchFn = null) {
  if (dispatchFn) {
    var favicon = document.querySelector('link[rel="shortcut icon"]');
    if (favicon) favicon.remove();
    favicon = document.createElement("link");
    favicon.setAttribute("rel", "shortcut icon");
    var head = document.querySelector("head");
    head.appendChild(favicon);
    favicon.setAttribute("href", `${nTaskIcon}?v=${new Date().getTime()}`);
    if (document.title.indexOf("Free Online Task & Project Management Software for Teams") != 0)
      document.title = `nTask - Free Online Task & Project Management Software for Teams`;
    localStorage.removeItem("companyName");
    dispatchFn(PopulateWhiteLabelInfo(null));
  } else {
    return dispatch => {
      var favicon = document.querySelector('link[rel="shortcut icon"]');
      if (favicon) favicon.remove();
      favicon = document.createElement("link");
      favicon.setAttribute("rel", "shortcut icon");
      var head = document.querySelector("head");
      head.appendChild(favicon);
      favicon.setAttribute("href", `${nTaskIcon}?v=${new Date().getTime()}`);
      if (document.title.indexOf("Free Online Task & Project Management Software for Teams") != 0)
        document.title = `nTask - Free Online Task & Project Management Software for Teams`;
      localStorage.removeItem("companyName");
      dispatch(PopulateWhiteLabelInfo(null));
    };
  }
}
export function whiteLabelInfo(teamId, callback, dispatchFn = null) {
  if (dispatchFn) {
    axios
      .get(`${constants.WHITELABELURL}?teamId=${teamId}`)
      .then(response => {
        let icon;
        if (response.data.success) {
          localStorage.setItem("companyName", response.data.data.companyName);
          icon = response.data.data.faviconImageUrl;
          dispatchFn(PopulateWhiteLabelInfo(response.data.data));
        } else {
          localStorage.removeItem("companyName");
          icon = nTaskIcon;
          dispatchFn(PopulateWhiteLabelInfo(null));
        }
        var favicon = document.querySelector('link[rel="shortcut icon"]');
        if (favicon) favicon.remove();
        favicon = document.createElement("link");
        favicon.setAttribute("rel", "shortcut icon");
        var head = document.querySelector("head");
        head.appendChild(favicon);
        favicon.setAttribute("href", `${icon}?v=${new Date().getTime()}`);
      })
      .catch(response => {
        localStorage.removeItem("companyName");
        dispatchFn(PopulateWhiteLabelInfo(null));
      });
  } else {
    return dispatch => {
      axios
        .get(`${constants.WHITELABELURL}?teamId=${teamId}`)
        .then(response => {
          let icon;
          if (response.data.success) {
            localStorage.setItem("companyName", response.data.data.companyName);
            icon = response.data.data.faviconImageUrl;
            dispatch(PopulateWhiteLabelInfo(response.data.data));
          } else {
            localStorage.removeItem("companyName");
            icon = nTaskIcon;
            dispatch(PopulateWhiteLabelInfo(null));
          }
          var favicon = document.querySelector('link[rel="shortcut icon"]');
          if (favicon) favicon.remove();
          favicon = document.createElement("link");
          favicon.setAttribute("rel", "shortcut icon");
          var head = document.querySelector("head");
          head.appendChild(favicon);
          favicon.setAttribute("href", `${icon}?v=${new Date().getTime()}`);
        })
        .catch(response => {
          localStorage.removeItem("companyName");
          dispatch(PopulateWhiteLabelInfo(null));
        });
    };
  }
}

export function FetchAllMemberInfo(callback) {
  return dispatch => {
    return instance()
      .get("/api/Team/GetAllWorkspaceMembers?sq=&getAllUsers=true")
      .then(response => {
        return dispatch(PopulateAllMembersInfo(response.data));
      })
      .then(() => {
        callback();
      });

    return null;
  };
}

export function InviteMembers(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/account/sendinvitationemail", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });

    return null;
  };
}

export function InviteFriends(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/account/inviteUsers", data)
      .then(response => {
        callback(response.data);
      })
      .catch(response => callback(response));

    return null;
  };
}

export function IsTeamUrlValid(Url, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/Team/IsWorkspaceUrlValid?workspaceUrl=" + Url)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function IsUserTeamUrlValid(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/company/IsTeamExist", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function MoveDataToWorkspace(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/WorkSpace/MovePersonalData", data)
      .then(response => {
        callback(response.data);
      })
      .catch(response => {
        failure(response ? response : "Error");
      });
  };
}

export function validateEmailAddress(email, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/account/IsValidEmail?email=" + email)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function mapTeamTaskStatus(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/payment/MapTeamTaskStatusToDefault", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export default {
  ResetMembersInfo,
  FetchAllMemberInfo,
  InviteMembers,
  InviteFriends,
  ResetAllMembersInfo,
  IsTeamUrlValid,
  validateEmailAddress,
  PopulateWhiteLabelInfo,
  mapTeamTaskStatus,
};
