import constants from "./../constants/types";
import instance from "./../instance";
import ic from "./issueComments";
import rc from "./riskComments";
import order from "./itemOrder";
import cloneDeep from "lodash/cloneDeep";
import { getTasksAction } from "./tasks";

// import { createTaskAction, editTaskAction } from "./tasks";

export function dispatchIssues(data) {
  return dispatch => {
    dispatch({
      type: constants.POPULATEISSUESINFO,
      payload: {
        data,
      },
    });
  };
}

export function StoreCopyIssueInfo(data) {
  return {
    type: constants.COPYISSUES,
    payload: {
      data,
    },
  };
}

export function PopulateIssuesInfo(data) {
  return {
    type: constants.POPULATEISSUESINFO,
    payload: {
      data,
    },
  };
}

export function UpdateStoreIssues(data) {
  return {
    type: constants.UPDATEISSUE,
    payload: {
      data,
    },
  };
}

export function UpdateBulkStoreIssue(data, isDelete) {
  return {
    type: constants.UPDATEBULKISSUEINFO,
    payload: {
      data,
      isDelete: isDelete,
    },
  };
}

export function DeleteStoreIssues(data) {
  return {
    type: constants.DELETEISSUE,
    payload: {
      data,
    },
  };
}

export function ResetIssuesInfo() {
  return {
    type: constants.RESETISSUESINFO,
  };
}

export function MarkRead(id, currentUser) {
  return {
    type: constants.MARKREADISSUE,
    payload: {
      id,
      currentUser,
    },
  };
}

export function getIssuesAction(data) {
  return {
    type: constants.SAVE_ALL_ISSUES,
    payload: data,
  };
}
export function getIssues(param, dispatchfn, callback = () => { }, failure = () => { }) {
  if (dispatchfn) {
    instance()
      .get("api/issues")
      .then(res => {
        dispatchfn(getIssuesAction(res.data.entity));
        callback(res.data.entity);
      })
      .catch(response => failure(response.response));
  } else {
    return dispatch =>
      instance()
        .get("api/issues")
        .then(res => {
          dispatch(getIssuesAction(res.data.entity));
          callback();
        })
        .catch(response => failure(response.response));
  }
}

export function FetchIssuesInfo(callback) {
  return dispatch => {
    return instance()
      .get("/api/issuerisk/getissues")
      .then(response => {
        return dispatch(PopulateIssuesInfo(response.data));
      })
      .then(() => {
        callback();
      });
    return null;
  };
}

//POST /api/IssueRisk/SaveUpdates
//GET /api/IssueRisk/DeleteAttachment

export function DeleteAttachment(id, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/IssueRisk/DeleteAttachment?id=" + id)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function createIssueAction(data) {
  return {
    type: constants.CREATE_ISSUE,
    payload: data,
  };
}

export function editTaskAction(data) {
  return {
    type: constants.CREATED_ISSUE_EDIT,
    payload: data,
  };
}

export function SaveUpdates(comment, callback) {
  return dispatch => {
    return instance()
      .post("/api/IssueRisk/SaveUpdates", comment)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function createIssue(data, dispatchFn, dispatchObj, success, failure) {
  if (!dispatchFn) {
    return dispatch => {
      dispatch(createIssueAction(dispatchObj));
      instance()
        .post("/api/issues", data)
        .then(response => {
          dispatch(editTaskAction(response.data));
          success(response.data);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  } else {
    dispatchFn(createIssueAction(dispatchObj));
    instance()
      .post("/api/issues", data)
      .then(response => {
        dispatchFn(editTaskAction(response.data));
        success(response.data);
      })
      .catch(response => {
        failure(response.response);
      });
  }
}

export function SaveIssue(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/issue", data)
      .then(response => {
        if (window.userpilot) {
          /** tracking successfully created issue in app */
          window.userpilot.track("Issue Created Successfully");
        }
        dispatch(StoreCopyIssueInfo(response.data));
        dispatch(order.FetchItemOrderInfo(() => { }));
        return response;
      })
      .then(response => {
        callback(response.data);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}

export function dispatchNewIssue(data) {
  return dispatch => {
    dispatch(StoreCopyIssueInfo(data));
  };
}

export function DeleteIssue(data, callback, failure, dispatchFn) {
  if (dispatchFn) {
    return instance()
      .get("/api/issuerisk/deleteIssue?issueId=" + data)
      .then(response => {
        dispatchFn(deleteIssueAction(data));
        return response;
      })
      .then(response => {
        callback(response.data);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .get("/api/issuerisk/deleteIssue?issueId=" + data)
        .then(response => {
          dispatch(deleteIssueAction(data));
          return response;
        })
        .then(response => {
          callback(response.data);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}

export function dispatchIssueDelete(data) {
  return dispatch => {
    dispatch(DeleteStoreIssues(data));
  };
}

export function dispatchArchiveIssue(data) {
  return dispatch => {
    dispatch(ic.PopulateIssueActivities(data.activityLog));
    dispatch(ic.PopulateIssueNotifications(data.notification));
    dispatch(UpdateStoreIssues(data));
  };
}

export function MarkIssueAsStarred(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/issuerisk/MarkIssueAsStarred", data)
      .then(response => {
        dispatch(UpdateStoreIssues(response.data));
        //dispatch(ic.PopulateIssueActivities(response.data.activityLog));
        //dispatch(ic.PopulateIssueNotifications(response.data.notifications));
        return { response: response };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => callback(response.response, null));
  };
}

export function UpdateIssue(data, callback, failure) {
  let ApiData = cloneDeep(data);
  ApiData.detail =
    ApiData.detail && ApiData.detail !== ""
      ? window.btoa(unescape(encodeURIComponent(ApiData.detail)))
      : ApiData.detail;
  return dispatch => {
    return instance()
      .put(`api/IssueRisk/UpdateIssue?id=${ApiData["id"]}`, ApiData)
      .then(response => {
        dispatch(UpdateStoreIssues(response.data));
        dispatch(ic.PopulateIssueActivities(response.data.activityLog));
        dispatch(ic.PopulateIssueNotifications(response.data.notifications));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(res => {
        let response = res.response ? res.response : null;
        failure(response);
      });
  };
}

export function UpdateIssueCustomField(obj, callback) {
  return instance()
    .put(`api/issue/id=${obj.groupId}`, [obj])
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function dispatchIssue(data) {
  return dispatch => {
    dispatch(UpdateStoreIssues(data));
    dispatch(ic.PopulateIssueActivities(data.activityLog));
    // dispatch(ic.PopulateIssueNotifications(data.notifications));
  };
}

export function ArchiveIssue(data, callback, failure, dispatchFn) {
  if (dispatchFn) {
    return instance()
      .get(`api/IssueRisk/ArchiveIssue?id=${data["id"]}`)
      .then(response => {
        dispatchFn(deleteIssueAction(response.data.id));
        // dispatchFn(ic.PopulateIssueActivities(response.data.activityLog));
        // dispatchFn(ic.PopulateIssueNotifications(response.data.notifications));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => failure(response.response));
  } else {
    return dispatch => {
      return instance()
        .get(`api/IssueRisk/ArchiveIssue?id=${data["id"]}`)
        .then(response => {
          dispatch(deleteIssueAction(response.data.id));
          // dispatch(ic.PopulateIssueActivities(response.data.activityLog));
          // dispatch(ic.PopulateIssueNotifications(response.data.notifications));
          return { response: response };
        })
        .then(_data => {
          callback(_data.response);
        })
        .catch(response => failure(response.response));
    };
  }
}

export function UnarchiveIssue(data, callback, failure, dispatchFn) {
  if (dispatchFn) {
    return instance()
      .get(`api/IssueRisk/UnarchiveIssue?id=${data["id"]}`)
      .then(response => {
        dispatchFn(updateIssueDataAction(response.data));
        // dispatchFn(ic.PopulateIssueActivities(response.data.activityLog));
        // dispatchFn(ic.PopulateIssueNotifications(response.data.notifications));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => failure(response.response));
  } else {
    return dispatch => {
      return instance()
        .get(`api/IssueRisk/UnarchiveIssue?id=${data["id"]}`)
        .then(response => {
          dispatch(updateIssueDataAction(response.data));
          // dispatch(ic.PopulateIssueActivities(response.data.activityLog));
          // dispatch(ic.PopulateIssueNotifications(response.data.notifications));
          return { response: response };
        })
        .then(_data => {
          callback(_data.response);
        })
        .catch(response => failure(response.response));
    };
  }
}

export function BulkUpdateIssue(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/IssueRisk/UpdateBulkIssues", data)
      .then(response => {
        // Type == 14, means it is delete operation
        const { type } = data;
        if (type == 14) {
          dispatch(UpdateBulkStoreIssue(response.data, true));
        } else {
          dispatch(UpdateBulkStoreIssue(response.data, false));
        }
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => failure(response.response));
  };
}

export function GetRiskIssueComments(data, callback, failure) {
  return dispatch => {
    instance()
      .get(`/api/IssueRisk/GetRiskIssueUpdates?id=${data.id}&type=${data.type}`)
      .then(response => {
        if (data.type === "Issue") {
          dispatch(ic.PopulateIssueCommentData(response.data.messages));
          dispatch(ic.PopulateIssueActivities(response.data.activityLog));
          dispatch(ic.PopulateIssueNotifications(response.data.notifications));
        } else if (data.type === "Risk") {
          dispatch(rc.PopulateRiskCommentData(response.data.issueUpdates));
          dispatch(rc.PopulateRiskActivities(response.data.activityLog));
          dispatch(rc.PopulateRiskNotifications(response.data.notifications));
        }
        return { response: response };
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function SaveRiskIssueComments(data, callback) {
  return dispatch => {
    instance()
      .post("/api/IssueRisk/SaveUpdates", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function MarkIssueNotificationsAsRead(id, currentUser, callback) {
  return dispatch => {
    return instance()
      .get("/api/Notification/MarkIssueNotificationsAsRead?issueId=" + id)
      .then(response => {
        if (response && response.data && response.data.updatedNotifications) {
          dispatch(MarkRead(id, currentUser));
          dispatch(ic.ClearIssueNotifications([]));
        }
        return { response: response };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => callback(response.response, null));
  };
}

export function saveRiskIssueAttachment(attachment, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/IssueRisk/SaveAttachment", attachment)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function bulkUpdateIssue(data) {
  return dispatch => {
    dispatch(UpdateBulkStoreIssue(data, false));
  };
}

/// SIGNAL R customfield
export function UpdateIssueCustomFieldData(data) {
  return dispatch => {
    dispatch({
      type: constants.UPDATEISSUECUSTOMFIELDDATA,
      payload: data,
    });
  };
}

export function getActivites(issueId, callback, failure) {
  return dispatch => {
    return instance()
      .get("api/issuerisk/GetIssueActivityLog?IssueId=" + issueId)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function moveIssueToWorkspace(data, sucess, failure, dispatchFn) {
  if (dispatchFn) {
    return instance()
      .post("api/issuerisk/MoveIssue", data)
      .then(response => {
        dispatchFn(deleteBulkTaskAction({issueIds : data.issuesId}));
        sucess(response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .post("api/issuerisk/MoveIssue", data)
        .then(response => {
          dispatch(deleteBulkTaskAction({issueIds : data.issuesId}));
          sucess(response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}
// filter issues

export function getSavedFilters(type, dispatch, callback = () => { }) {
  instance()
    .get("api/advancefilter/" + type)
    .then(res => {
      callback();
      dispatch(getCustomFilterAction(res.data.entity));
    });
}

export function deleteIssueFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(deleteIssueFilterAction(data));
  } else {
    return dispatch => {
      dispatch(deleteIssueFilterAction(data));
    };
  }
}

export function addNewFilter(data, type, dispatch, callback = () => { }) {
  instance()
    .post("api/advancefilter/" + type, data)
    .then(res => {
      callback();
      dispatch(addNewCustomFilterAction(res.data.entity));
    });
}

export function updateIssueFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(updateIssueFilterAction(data));
  } else {
    return dispatch => {
      dispatch(updateIssueFilterAction(data));
    };
  }
}

export function clearIssueFilter(dispatchFn) {
  if (dispatchFn) {
    dispatchFn(clearIssueFilterAction());
  } else {
    return dispatch => {
      dispatch(clearIssueFilterAction());
    };
  }
}

export function updateIssueObject(data, dispatchFn = null) {
  if (dispatchFn) {
    dispatchFn(updateIssueDataAction(data));
  } else {
    return dispatch => {
      dispatch(updateIssueDataAction(data));
    };
  }
}

export function updateIssueDataAction(data) {
  return {
    type: constants.UPDATE_ISSUE_DATA,
    payload: data,
  };
}

export function updateIssueData(
  data,
  dispatchFn,
  callback = () => { },
  error = () => { },
  notifyUser
) {
  if (dispatchFn) {
    instance()
      .patch(`/api/issue/${data.issue.id}`, data.obj)
      .then(res => {
        dispatchFn(updateIssueDataAction(res.data.entity));
        callback(res.data.entity);
      })
      .catch(err => {
        error(err.response);
        dispatchFn(updateIssueDataAction(data.issue));
      });
  } else {
    return dispatch => {
      instance()
        .patch(`/api/issue/${data.issue.id}`, data.obj)
        .then(res => {
          dispatch(updateIssueDataAction(res.data.entity));
          callback(res.data.entity);
        })
        .catch(err => {
          error(err.response);
          dispatch(updateIssueDataAction(data.issue));
        });
    };
  }
}
export function bulkIssueAction(data) {
  return {
    type: constants.UPDATE_BULK_ISSUES,
    payload: data,
  };
}
export function updateBulkIssue(data, dispatch, callback, failure) {
  const { keyType, ...rest } = data;
  instance()
    .put("api/issues/updatebulk", rest)
    .then(res => {
      dispatch(bulkIssueAction(res.data));
      callback(res.data);
    })
    .catch(() => {
      failure();
    });
}

export function deleteBulkIssueAction(data) {
  return {
    type: constants.DELETE_BULK_ISSUE,
    payload: data,
  };
}

export function archiveBulkIssue(data, dispatch, callback, failure) {
  instance()
    .put("api/issues/archivebulk", data)
    .then(res => {
      let issueIds = res.data.map(i => i.id);
      callback(issueIds);
      dispatch(deleteBulkIssueAction({ issueIds }));
    })
    .catch(() => {
      failure();
    });
}
export function unArchiveBulkIssue(data, dispatch, callback, failure) {
  instance()
    .put("api/issues/unarchivebulk", data)
    .then(res => {
      let issueIds = res.data.map(i => i.id);
      callback(res.data);
      dispatch({
        type: constants.UNARCHIVE_BULK_ISSUE,
        payload: {issueIds},
      });
    })
    .catch(() => {
      failure();
    });
}
export function deleteBulkTaskAction(data) {
  return {
    type: constants.DELETE_BULK_ISSUE,
    payload: data,
  };
}
export function deleteBulkIssue(data, dispatch, callback, failure) {
  instance()
    .put("api/issues/deletebulk", data)
    .then(res => {
      callback(res.data);
      dispatch(deleteBulkTaskAction({ issueIds: res.data }));
    })
    .catch(() => {
      failure();
    });
}

export function exportBulkIssue(data, dispatch, callback, failure) {
  instance()
    .put("api/issues/exportbulk", data, {
      responseType: "blob",
    })
    .then(res => {
      // dispatch(updateTaskDataAction(res.data.entity));
      callback(res);
    })
    .catch((res) => {
      failure(res)
    });
}

export function addNewCustomFilterAction(data) {
  return {
    type: constants.ADD_NEW_ISSUE_CUSTOM_FILTER,
    payload: data,
  };
}

export function getCustomFilterAction(data) {
  return {
    type: constants.SET_ISSUE_CUSTOM_FILTER,
    payload: data,
  };
}

export function deleteIssueFilterAction(data) {
  return {
    type: constants.DELETE_ISSUE_FILTER,
    payload: data,
  };
}

export function updateIssueFilterAction(data) {
  return {
    type: constants.UPDATE_ISSUE_FILTER,
    payload: data,
  };
}

export function clearIssueFilterAction(data) {
  return {
    type: constants.CLEAR_ISSUE_FILTER,
  };
}
export function deleteIssueAction(data) {
  return {
    type: constants.DELETE_ISSUE,
    payload: {
      data,
    },
  };
}
export function showAllIssuesAction() {
  return {
    type: constants.SHOW_ALL_ISSUE,
    payload: {},
  };
}

export function showAllIssues(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(showAllIssuesAction(data));
  } else {
    return dispatch => {
      dispatch(showAllIssuesAction(data));
    };
  }
}
export function updateIssueQuickFilterAction(data) {
  return {
    type: constants.UPDATE_ISSUE_QUICK_FILTER,
    payload: data,
  };
}

export function removeIssueQuickFilterAction(data) {
  return {
    type: constants.REMOVE_ISSUE_QUICK_FILTER,
    payload: data,
  };
}
export function removeIssueQuickFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(removeIssueQuickFilterAction(data));
  } else {
    return dispatch => {
      dispatch(removeIssueQuickFilterAction(data));
    };
  }
}

export function updateQuickFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(updateIssueQuickFilterAction(data));
  } else {
    return dispatch => {
      dispatch(updateIssueQuickFilterAction(data));
    };
  }
}
function getArchivedDataAction(data) {
  return {
    type: constants.ADD_BULK_ISSUE,
    payload: data,
  };
}
export function getArchivedData(type, callback) {
  // Project = 1,
  //   Task = 2,
  //   Meeting = 3,
  //   Issue = 4,
  //   Risk = 5
  return dispatch => {
    instance()
      .get(`api/workspace/GetArchivedData?type=${type}`)
      .then(response => {
        dispatch(getArchivedDataAction(response.data));
        callback(response);
      })
      .catch(error => {
        callback(error.response);
      });
  }
}
export default {
  FetchIssuesInfo,
  ResetIssuesInfo,
  SaveIssue,
  UpdateIssue,
  UpdateStoreIssues,
  BulkUpdateIssue,
  GetRiskIssueComments,
  SaveRiskIssueComments,
  saveRiskIssueAttachment,
  SaveUpdates,
  ArchiveIssue,
  UnarchiveIssue,
  dispatchArchiveIssue,
  PopulateIssuesInfo,
};
