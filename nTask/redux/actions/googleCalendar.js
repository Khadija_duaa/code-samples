import constants from "./../constants/types";
import instance from "../instance";

export function connectGoogleCalender(succ, fail) {
  return (dispatch) => {
    return instance()
      .get("/api/Calendar/ConnectGoogleCalendar")
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}
export function GetGoogleAccounts(succ, fail) {
  return (dispatch) => {
    return instance()
      .get("/api/Calendar/GetGoogleAccounts")
      .then((response) => {
        dispatch(addSyncCalendarList(response.data));
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}

export function LinkGoogleCalendarInfo(code, succ, fail) {
  return (dispatch) => {
    return instance()
      .get("api/Calendar/GoogleAuthorization?code=" + code)
      .then((response) => {
        dispatch(updateGoogleCalendarInfo(response.data));
        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}
export function EnableSyncCalendar(obj, succ, fail) {
  return (dispatch) => {
    return instance()
      .get(
        "/api/Calendar/EnableSyncCalendar?AccountId=" +
          obj.accountId +
          "&Sync=" +
          obj.syncEnabled
      )
      .then((response) => {
        dispatch(updateSyncEnabled(obj));
        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}
export function GoogleDeauthorization(obj, succ, fail) {
  return (dispatch) => {
    return instance()
      .get("/api/Calendar/GoogleDeauthorization?AccountId=" + obj.accountId)
      .then((response) => {
        dispatch(filterSyncCalendList(obj));
        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}
export function GoogleDeauthorizationWithData(obj, succ, fail) {
  return (dispatch) => {
    return instance()
      .get("/api/Calendar/GoogleDeauthorization?AccountId=" + obj.accountId + "&RemoveEvents=true" )
      .then((response) => {
        dispatch(filterSyncCalendList(obj));
        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}

export function GetCalendarSettings(id, succ, fail) {
  return (dispatch) => {
    return instance()
      .get("/api/Calendar/GetCalendarSettings?AccountId=" + id)
      .then((response) => {
        dispatch(updateSynCalendr(response.data));
        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}

export function syncCalendar(obj, succ, fail) {
  return (dispatch) => {
    return instance()
      .post("api/Calendar/PushSyncCalendar", obj)
      .then((response) => {
        dispatch(updateSynCalendr(response.data));
        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}
export function saveCalendarDetails(obj, succ, fail) {
  return (dispatch) => {
    return instance()
      .post("api/Calendar/AddCalendarSettings", obj)
      .then((response) => {
        dispatch(AddUpdateSyncCalendar(response.data));
        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}

export function clearGoogleCalendarState(bool) {
  return (dispatch) => {
    dispatch(clearCalendarState(bool));
  };
}

export function clearCalendarState(bool) {
  return {
    type: constants.CLEARGOOGLECALENDARSTATE,
    payload: bool,
  };
}
export function updateGoogleCalendarInfo(data) {
  return {
    type: constants.UPDATEGOOGLECALENDARINFO,
    payload: {
      data,
    },
  };
}
export function filterSyncCalendList(data) {
  return {
    type: constants.FILTERSYNCCALENDARLIST,
    payload: {
      data,
    },
  };
}

export function AddUpdateSyncCalendar(data) {
  return {
    type: constants.ADDUPDATESYNCCALENDAR,
    payload: {
      data,
    },
  };
}
export function updateSyncEnabled(data) {
  return {
    type: constants.UPDATESYNCENABLED,
    payload: {
      data,
    },
  };
}

export function updateSynCalendr(data) {
  return {
    type: constants.UPDATESYNCCALENDARINFO,
    payload: {
      data,
    },
  };
}
export function addSyncCalendarList(data) {
  return {
    type: constants.ADDSYNCCALENDARLIST,
    payload: {
      data,
    },
  };
}

export default {
  connectGoogleCalender,
  LinkGoogleCalendarInfo,
  syncCalendar,
  saveCalendarDetails,
  GoogleDeauthorization,
  GetGoogleAccounts,
  GetCalendarSettings,
  clearGoogleCalendarState,
  GoogleDeauthorizationWithData
};
