import constants from "../constants/types";
import instance from "../instance";
import COLORCODES from "../../components/constants/colorCodes";
import calender from "./calenderTasks";
import helper from "../../helper/index";
import { updateTeamImageURL, UpdateMember } from "./profile";
import {
  setGlobalTaskTime,
  removeGlobalTaskTime,
  updateGlobalTimeTask
} from "./globalTimerTask";
import { getTasks } from "./tasks";
import { getColumnsList } from "./columns";

export function WorkspaceExists(callback) {
  let response = null;
  return dispatch => {
    return instance()
      .get("api/WorkSpace/IsWorkspaceExist")
      .then(response => {
        callback(response);
      })
      .catch(err => {
        callback(err.response);
      });
  };
}
export function PopulateWorkspacePermissionSettings(data) {
  return {
    type: constants.PERMISSIONSETTINGS,
    payload: {
      data
    }
  };
}
export function CopyWorkspacePermissionSettings(data) {
  return {
    type: constants.COPYPERMISSION,
    payload: {
      data
    }
  };
}

export function RenameWorkspacePermissionSettings(data) {
  return {
    type: constants.UPDATEPERMISSION,
    payload: {
      data
    }
  };
}

export function DeleteWorkspacePermissionSettings(data) {
  return {
    type: constants.DELETEPERMISSION,
    payload: {
      data
    }
  };
}

export function PopulateWorkspaceInfo(data) {
  return {
    type: constants.POPULATEWORKSPACEINFO,
    payload: {
      data
    }
  };
}
export function PopulateProjects(data) {
  PopulateProjects;
  return {
    type: constants.POPULATEPROJECTINFO,
    payload: {
      data
    }
  };
}
export function UpdateTeamMember(data) {
  return {
    type: constants.UPDATETEAMMEMBER,
    payload: data
  };
}

export function UpdateStoreTasks(data) {
  return [
    {
      type: constants.UPDATETASKINFO,
      payload: {
        data
      }
    },
    updateGlobalTimeTask(data)
  ];
}

export function UpdateTaskColorFromGridItem(obj, callback) {
  return dispatch => {
    //
    return instance()
      .post("/api/UserTask/UpdateUserTask", obj)
      .then(response => {
        response = response.data;
        const actions = UpdateStoreTasks(response);
        dispatch(actions[0]);
        dispatch(actions[1]);
        return { response: response };
      })
      .then(data => {
        let currentDate = new Date();
        let dueDate = new Date(data.response.dueDate);
        let obj = {
          id: data.response.taskId,
          title: data.response.taskTitle,
          start: new Date(data.response.startDateString),
          end: new Date(data.response.dueDateString),
          desc: data.response.description,
          //taskColor: data.response.colorCode,
          taskColor: COLORCODES.STATUSCOLORS[data.response.status],
          status: data.response.status,
          isDueToday:
            currentDate.toDateString() == dueDate.toDateString() ? true : false,
          isStared: data.response.isStared,
          userColors:
            data.response.userColors.length > 0 ? data.response.userColors : [] //colorCode
        };
        callback(obj);
        return obj;
      });
  };
}

export function exportActivityLog(callback) {
  instance()
    .get("api/export/activitylog", {
      responseType: "blob",
    })
    .then(res => {
      // dispatch(updateTaskDataAction(res.data.entity));
      callback(res);
    })
    .catch(() => {
      // dispatch(updateTaskDataAction(data.task));
    });
}
export async function exportTeamActivityLog(callback) {
  await instance()
    .get("/api/export/teamactivitylog", {
      responseType: "blob",
    })
    .then((res) => {
      // dispatch(updateTaskDataAction(res.data.entity));
      callback(res);
    })
    .catch(() => {
      // dispatch(updateTaskDataAction(data.task));
    });
}
export function UpdateTasksFromCalender(obj, callback) {
  let response = null;
  return dispatch => {
    return instance()
      .post("/api/UserTask/UpdateUserTask", obj)
      .then(response => {
        //
        response = response.data;
        const actions = UpdateStoreTasks(response);
        dispatch(actions[0]);
        dispatch(actions[1]);
        return { response: response };
      })
      .then(data => {
        //
        callback(data.response);
        return data.Dobj;
      });
  };
}


export function isProjectManager(callback) {
  return instance()
    .get("/api/Permission/IsProjectManagerInTeam")
    .then(response => {
      callback(response.data.entity.isProjectManager);
      return;
    }).catch(response => {
      failure(response);
    });
}

export function isTeamUrlValid(name, callback) {
  return instance()
    .get(`/api/account/isTeamExist?teamName=${name}`)
    .then(response => {
      callback(response);
    });
}

// export function AcceptTeamInvitation(teamID, success, error) {
//   return dispatch => {
//     return instance()
//       .get("/api/team/AcceptWorkspaceInvitation?teamId=" + teamID)
//       .then(response => {
//         success(response);
//       })
//       .catch(err => {
//         error(err.response);
//       });
//   };
// }

export function getArchivedData(type, callback) {
  // Project = 1,
  //   Task = 2,
  //   Meeting = 3,
  //   Issue = 4,
  //   Risk = 5
  return instance()
    .get(`api/workspace/GetArchivedData?type=${type}`)
    .then(response => {
      callback(response);
    })
    .catch(error => {
      callback(error.response);
    });
}

export function getWorkspaceRoles(success, failure) {
  return dispatch => {
    return instance()
      .get("/api/permission/getworkspaceroles")
      .then(response => {
        dispatch(PopulateWorkspacePermissionSettings(response.data));
        return response;
      })
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response);
      });
  };
}
export function DeleteWorkspaceRole(roleId, success, failure) {
  if (roleId)
    return dispatch => {
      return instance()
        .get("/api/permission/DeleteRole?roleId=" + roleId)
        .then(response => {
          dispatch(DeleteWorkspacePermissionSettings(response.data));
          return response;
        })
        .then(response => {
          success(response);
        })
        .catch(response => {
          failure(response);
        });
    };
}
export function addNewRole(data, success, failure) {
  const isEdit = data.isEdit;
  let saveData = data;
  if (isEdit) {
    delete data.isEdit;
    saveData = data.roleData;
  }
  return dispatch => {
    return instance()
      .post("/api/permission/SaveRole", saveData)
      .then(response => {
        if (isEdit) {
          dispatch(RenameWorkspacePermissionSettings(response.data));
        } else {
          dispatch(CopyWorkspacePermissionSettings(response.data));
        }

        return response;
      })
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response);
      });
  };
}

export function getOrder(data, id) {
  let position = data.find(x => {
    if (x.itemId === id) {
      return x.position;
    }
  });
  return position.position || 0;
}
export function PopulateTasks(data) {
  let tasks = data.tasks || [];
  let issues = data.issues || [];
  let risks = data.risks || [];
  let meetings = data.meetings || [];
  let notifications = data.notifications || [];
  let item = data.itemOrder ? data.itemOrder.itemOrder : {};
  //let ids = item.map(x => {x.itemId)
  tasks = tasks.map(x => {
    let position = helper.RETURN_ITEMORDER(item, x.id);
    x.taskOrder = position || 0;
    return x;
  });
  let list = [];
  let completeDetails = tasks.map(x => {
    x.issues = [];
    x.risks = [];
    x.meetings = [];
    x.notifications = [];
    return x;
  });
  let completeTaskDetails = completeDetails;
  let withIssuesDetails = issues.map(x => {
    return completeTaskDetails.map(m => {
      list = m.issues || [];
      if (x.linkedTasks.indexOf(m.taskId) >= 0) {
        list.push(x);
      }
      m.issues = list;
      return m;
    });
  });
  if (
    withIssuesDetails &&
    withIssuesDetails.length &&
    withIssuesDetails[0].length
  ) {
    completeTaskDetails = withIssuesDetails[0];
  }
  let withRisksDetails = risks.map(x => {
    return completeTaskDetails.map(m => {
      list = m.risks || [];
      if (x.linkedTasks.indexOf(m.taskId) >= 0) {
        list.push(x);
      }
      m.risks = list;
      return m;
    });
  });
  if (
    withRisksDetails &&
    withRisksDetails.length &&
    withRisksDetails[0].length
  ) {
    completeTaskDetails = withRisksDetails[0];
  }

  let withMeetingDetails = meetings.map(x => {
    return completeTaskDetails.map(m => {
      list = m.meetings || [];
      if (x.taskId === m.taskId) {
        list.push(x);
      }
      m.meetings = list;
      return m;
    });
  });
  if (
    withMeetingDetails &&
    withMeetingDetails.length &&
    withMeetingDetails[0].length
  ) {
    completeTaskDetails = withMeetingDetails[0];
  }

  let withNotificationsDetails = completeTaskDetails.map(x => {
    x.notifications = notifications.filter(n => n.objectId === x.taskId);
    return x;
  });

  if (withNotificationsDetails && withNotificationsDetails.length) {
    completeTaskDetails = withNotificationsDetails;
  }

  completeTaskDetails.forEach(function (element) {
    let color =
      element.userColors && element.userColors.length > 0
        ? element.userColors[0]["colorCode"]
        : "#16a5de";
    let value = [],
      _color = "#16a5de"; //color="#EE0077";

    if (color && color.indexOf("#") < 0) {
      value = COLORCODES.COLORCODES.filter(x => x.key === color);
    } else {
      _color = color;
    }
    value = value.length ? value[0].value : _color;
    let currentDate = new Date();
    let dueDate = new Date(element.dueDate);

    //
    //
    let obj = {
      id: element.taskId,
      title: element.taskTitle,
      start: new Date(element.startDateString),
      end: new Date(element.dueDateString),
      desc: element.description,
      actualDueDateString: element.actualDueDateString,
      actualStartDateString: element.actualStartDateString,
      dueDateString: element.dueDateString,
      startDateString: element.startDateString,
      priority: element.priority,
      //taskColor: element.colorCode, //value,//colorCode #3229898
      taskColor: COLORCODES.STATUSCOLORS[element.status],
      status: element.status,
      isDueToday:
        currentDate.toDateString() == dueDate.toDateString() ? true : false,
      isStared: element.isStared,
      userColors:
        element.userColors && element.userColors.length > 0
          ? element.userColors
          : []
    };

    element.CalenderDetails = obj;
  });
  completeTaskDetails = completeTaskDetails.sort(
    (a, b) => a.taskOrder - b.taskOrder
  );
  // return {
  //   type: constants.POPULATETASKSINFO,
  //   payload: {
  //     data: localStorage.getItem("projectId")
  //       ? completeTaskDetails.filter(
  //           x => x.projectId === localStorage.getItem("projectId")
  //         )
  //       : completeTaskDetails
  //   }
  // };
}

export function PopulateMeetings(data) {
  return {
    type: constants.POPULATEMEETINGSINFO,
    payload: {
      data
    }
  };
}
export function PopulateRisks(data) {
  return {
    type: constants.POPULATERISKSINFO,
    payload: {
      data
    }
  };
}

export function PopulateIssues(data) {
  return {
    type: constants.POPULATEISSUESINFO,
    payload: {
      data
    }
  };
}

// export function PopulateNotifications(data) {
//   return {
//     type: constants.POPULATENOTIFICATIONS,
//     payload: {
//       data
//     }
//   };
// }

export function ResetWorkspaceInfo() {
  return {
    type: constants.RESETWORKSPACE
  };
}
export function PopulateItemOrder(data) {
  return {
    type: constants.POPULATEITEMORDER,
    payload: {
      data
    }
  };
}
export function PopulatePermissions(data) {
  return {
    type: constants.WORKSPACEPERMISSIONS,
    payload: {
      data
    }
  };
}

export function RemoveWorkspace(data) {
  return {
    type: constants.REMOVETEAM,
    payload: {
      data
    }
  };
}
export function ChangeMemberRoleInWorkspace(data, isInvited) {
  return {
    type: constants.CHANGEUSERROLE,
    payload: {
      data,
      isInvited
    }
  };
}
export function ChangeMemberRoleInTeam(data, isInvited) {
  return {
    type: constants.CHANGEUSERROLEINTEAM,
    payload: {
      data,
      isInvited
    }
  };
}

export function AddNewTeam(team) {
  return {
    type: constants.ADDTEAM,
    payload: {
      team
    }
  };
}

export function UpdateTeam(team) {
  return {
    type: constants.UPDATETEAM,
    payload: {
      team
    }
  };
}

export function RemoveTeamFromList(teamId) {
  return {
    type: constants.DELETETEAM,
    payload: {
      teamId
    }
  };
}

export function PopulateTeamData(data) {
  return {
    type: constants.UPDATETEAMS,
    payload: {
      data
    }
  };
}
export function AddMemberToWorkSpace(data) {
  return {
    type: constants.UPDATEINVITATIONMEMBERS,
    payload: {
      data
    }
  };
}
export function AddMemberToInviteHistory(data) {
  return {
    type: constants.UPDATEINVITEHISTORY,
    payload: {
      data
    }
  };
}
export function teamMemberCrudAction(data, operationType) {
  return {
    type: constants.CRUDTEAMMEMBER,
    operationType,
    payload: data
  };
}
export function workspaceMemberCrudAction(data, operationType) {
  return {
    type: constants.CRUDWORKSPACEMEMBER,
    operationType,
    payload: data
  };
}
export function workspaceMemberCrud(data, operationType, workspaceId) {
  return {
    type: constants.WORKSPACEMEMBERCRUD,
    operationType,
    workspaceId,
    payload: data
  };
}
export function UpdateTeamImage(obj, callback, failure) {
  let data = new FormData();
  data.append("PrevImageName", obj["PrevImageName"]);
  data.append("UploadedImage", obj["UploadedImage"]);
  const teamId = obj["companyId"];
  return dispatch => {
    return instance()
      .post("api/company/UploadTeamLogo", data)
      .then(response => {
        dispatch(updateTeamImageURL(teamId, response.data));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}

export function RemoveTeamLogo(teamId, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/company/RemoveTeamLogo", {})
      .then(response => {
        dispatch(updateTeamImageURL(teamId, response.data));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}

export function UpdateWorkSpaceData(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/team/UpdateWorkspace", data)
      .then(response => {
        // dispatch(PopulateTeamData(response.data));
        return response;
      })
      .then(response => {
        callback(response.data);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function DeleteWorkSpaceData(workspaceId, callback) {
  return dispatch => {
    return instance()
      .get("api/team/DeleteWorkspace?id=" + workspaceId)
      .then(response => {
        callback(null, response);
      })
      .catch(err => {
        callback(err.response, null);
      });
  };
}
export function dispatchTeamMember(obj, action) {
  return dispatch => {
    dispatch(teamMemberCrudAction(obj, "update"));
  };
}
export function InviteTeamMembers(emails, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/Account/InviteTeamMembers", emails)
      .then(response => {
        dispatch(teamMemberCrudAction(response.data.teamMembers, "create"));
        dispatch(
          workspaceMemberCrudAction(response.data.workspaceMembers, "create")
        );
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}

export function ResendInviteTeamMembers(email, callback, failure) {
  return dispatch => {
    return instance()
      .get(`api/account/ResendTeamInvite?email=${email}`)
      .then(response => {
        dispatch(AddMemberToInviteHistory(response.data));

        callback(response);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}

export function EnableDisableMembers(object, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/team/EnableAndDisableMember", object)
      .then(response => {
        const member = response.data;
        dispatch(UpdateMember(member, "update"));
        return response;
      })
      .then(x => {
        callback(x);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function DisableTeamMember(memberId, membersObj, callback, failure) {
  return dispatch => {
    return instance()
      .get(`api/company/DisableTeamMember?memberId=${memberId}`)
      .then(response => {
        dispatch(teamMemberCrudAction(response.data.teamMember, "update"));
        if (membersObj) {
          dispatch(workspaceMemberCrudAction(membersObj, "update"));
        }
      })
      .then(() => {
        callback();
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function EnableTeamMember(memberId, membersObj, callback, failure) {
  return dispatch => {
    return instance()
      .get(`api/company/EnableTeamMember?memberId=${memberId}`)
      .then(response => {
        dispatch(teamMemberCrudAction(response.data.teamMember, "update"));
        if (membersObj) {
          dispatch(workspaceMemberCrudAction(membersObj, "update"));
        }
      })
      .then(() => {
        callback();
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function RemoveTeamMember(memberId, callback, failure) {
  return dispatch => {
    return instance()
      .get(`api/company/RemoveTeamMember?memberId=${memberId}`)
      .then(response => {
        dispatch(teamMemberCrudAction(memberId, "delete"));
        dispatch(workspaceMemberCrudAction(memberId, "delete"));
      })
      .then(() => {
        callback();
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function blockWorkspace(workspaceId, callback) {
  instance()
    .get("api/workspace/BlockWorkspace?workspaceId=" + workspaceId)
    .then(response => {
      callback(response);
    });
}
export function deleteTeam(teamId, callback) {
  instance()
    .get(`api/team/DeleteTeam?id=${teamId}`)
    .then(response => {
      callback(response);
    });
}
export function activateWorkspace(workspaceId, callback) {
  instance()
    .get("api/workspace/ActivateWorkspace?workspaceId=" + workspaceId)
    .then(response => {
      callback(response);
    });
}

export function ChangeUserRole(object, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/team/UpdateMemberRole", object)
      .then(response => {
        dispatch(ChangeMemberRoleInWorkspace(response.data));
        return response;
      })
      .then(x => {
        callback(x);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function ChangeTeamUserRole(object, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/company/UpdateMemberRole", object)
      .then(response => {
        dispatch(teamMemberCrudAction(response.data.teamMember, "update"));
        dispatch(
          workspaceMemberCrudAction(response.data.workspaceMember, "update")
        );

        return response;
      })
      .then(x => {
        callback(x);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function UpdateUserIsOnline(obj) {
  return dispatch => {
    dispatch({
      type: constants.UPDATEUSERISONLINE,
      payload: obj
    });
  };
}

// getConstants: () => {
//   return axios.get(config.API_URL + "/api/ntask/getConstants", headers.GETHEADERS().headers);
// },

// export function GetConstants(callback) {
//   return dispatch => {
//     return instance()
//       .get("/api/ntask/getConstants")
//       .then(response => {
//         if(response.status === 200)
//         {
//
//
//         }
//       })
//       .then(() => {
//         callback();
//       });

//     return null;
//   };
// }

function getGlobalTimerTask(tasks, timer) {
  if (timer) {
    const { taskId, createdDate, currentDate } = timer;
    let task = tasks.filter(x => x.taskId === taskId);
    if (task.length === 1)
      return setGlobalTaskTime(task[0], createdDate, currentDate);
    else return removeGlobalTaskTime();
  }
}

export function FetchWorkspaceInfo(teamId, callback) {
  let api = "/api/workspace/getworkspacedata";
  if (teamId) api = "/api/workspace/getworkspacedata?teamId=" + teamId;
  return dispatch => {
    return instance()
      .get(api)
      .then(response => {
        getTasks(null, dispatch)
        dispatch(PopulateIssues(response.data.issues));
        // dispatch(PopulateNotifications(response.data.notifications));
        dispatch(PopulateRisks(response.data.risks));
        dispatch(PopulateMeetings(response.data.meetings));
        dispatch(PopulateItemOrder(response.data.itemOrder));
        dispatch(PopulateProjects(response.data.projects));
        dispatch(PopulatePermissions(response.data.permission));
        // // dispatch(PopulateTasks(response.data));
        // if (response.data.timer) {
        //   dispatch(
        //     getGlobalTimerTask(response.data.tasks, response.data.timer)
        //   );
        // } else {
        dispatch(removeGlobalTaskTime());
        // }
        return { response: response };
      })
      .then(response => {
        callback(response.response);
      })
      .catch(error => {
        callback(error.response ? error.response : "Error");
      });
  };
}
export function AddWorkspace(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/team/createworkspace", data)
      .then(response => {
        dispatch(PopulateProjects([]));
        // dispatch(PopulateTasks([]));
        dispatch(PopulateMeetings([]));
        dispatch(PopulateRisks([]));
        dispatch(PopulateIssues([]));
        // dispatch(PopulateNotifications([]));
        dispatch(PopulatePermissions([]));
        return response;
      })
      .then(data => {
        callback(data);
      })
      .catch(error => {
        failure(error.response);
      });
  };
}

export function SetFeatureAccessPermissions(data) {
  return {
    type: constants.SETFEATUREACCESSPERMISSIONS,
    payload: {
      data
    }
  };
}
export function updateFeatureAccess(data, feature) {
  return {
    type: constants.UPDATEFEATUREACCESS,
    payload: {
      data,
      feature
    }
  };
}
export function GetFeatureAccessPermissions(dispatchFn = null) {
  if (dispatchFn) {
    return instance()
      .get('/api/customfield/featurefields')
      .then(response => {
        // console.log(response.data.entity);
        dispatchFn(SetFeatureAccessPermissions(response.data.entity));
      })
      .catch(error => {
        console.log('error', error);
        // failure(error);
        // callback(error.response ? error.response : "Error");
      });
  } else {
    return dispatch => {
      return instance()
        .get('/api/customfield/featurefields')
        .then(response => {
          // console.log(response.data.entity);
          dispatch(SetFeatureAccessPermissions(response.data.entity));
        })
        .catch(error => {
          console.log('error', error);
          // failure(error);
          // callback(error.response ? error.response : "Error");
        });
    };
  }
}

export function updateFeatureAccessPermissions(feature, obj, callback, failure, dispatchFn = null,) {
  if (dispatchFn) {
    return instance()
      .put(`/api/hidesystemfield/${feature}`, obj)
      .then(response => {
        // dispatchFn({ type: constants.UPDATEBULKTASKSTATUS, payload: { project, response } });
        getColumnsList(() => { }, dispatchFn);
        callback(response);
      }).catch(error => {
        failure(error)
      });
  }
  else {
    return dispatch => {
      return instance()
        .put(`/api/hidesystemfield/${feature}`, obj)
        .then(response => {
          getColumnsList(() => { }, dispatch);
          dispatch(updateFeatureAccess(response.data.entity, feature));
          // dispatch({ type: constants.UPDATEBULKTASKSTATUS, payload: { project, response } });
          callback(response);
        }).catch(error => {
          failure(error)
        });
    };
  }
}

export function getTeamProjectStatus(success, failure) {
  return dispatch => {
    return instance()
      .get(`api/team/GetTeamProjectStatus`)
      .then((response) => {
        success(response.data);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function configUpdate(data) {
  return {
    type: constants.UPDATECONFIG,
    payload: data
  };
}
export function updateLTDRequest(data) {
  return {
    type: constants.UPDATELTDUPGRADEREQUEST,
    payload: data
  };
}
export function updateWeekends(data) {
  return {
    type: constants.UPDATEWEEKENDS,
    payload: data
  };
}

export function updateTeamConfig(dispatch, data, callback, failure) { /** updation in project mandatory  */
  instance()
    .post("api/company/SetProjectFieldMandatory", data)
    .then(response => {
      dispatch(configUpdate(data))
      callback(response.data);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function updateTaskEffortTeamConfig(dispatch, data, callback, failure) { /** updation in project mandatory  */
  instance()
    .post("api/company/SetUserTasksEffortMandatory", data)
    .then(response => {
      dispatch(configUpdate(data))
      callback(response.data);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function updateTimeSettingTeamConfig(dispatch, data, callback, failure) { 
  instance()
    .post("api/company/SetTeamCustomMinutesForTimeLog", data)
    .then(response => {
      dispatch(configUpdate(data))
      callback(response.data);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function updateWeekendsConfig(dispatch, data, callback, failure) { /** updation in project mandatory  */
  instance()
    .post("api/company/UpdateWeekends", data)
    .then(response => {
      dispatch(updateWeekends(data))
      callback(response.data);
    })
    .catch(response => {
      failure(response.response);
    });
}
export function getAutomationRules(data, success, failure) {
  instance(AUTOMATION_BASE_URL)
    .get(`api/automation/rules/${data}`)
    .then(response => success?.(response.data))
    .catch(response => failure?.(response.response));
}

export function getAutomationRule(data, success, failure) {
  instance(AUTOMATION_BASE_URL)
    .get(`api/automation/rule/${data.id}`)
    .then(response => success?.(response.data))
    .catch(response => failure?.(response.response));
}

export function createAutomationRule(data, success, failure) {
  instance(AUTOMATION_BASE_URL)
    .post("api/automation/rule", data)
    .then(response => success?.(response.data))
    .catch(response => failure?.(response.response));
}

export function updateAutomationRule(data, success, failure) {
  instance(AUTOMATION_BASE_URL)
    .put(`api/automation/rule/${data._id}`, data)
    .then(response => success?.(response.data))
    .catch(response => failure?.(response.response));
}

export function deleteAutomationRule(data, success, failure) {
  instance(AUTOMATION_BASE_URL)
    .delete(`api/automation/rule/${data}`)
    .then(response => success?.(response.data))
    .catch(response => failure?.(response.response));
}

export function updateAutomationRuleStatus({ id, data }, success, failure) {
  instance(AUTOMATION_BASE_URL)
    .post(`api/automation/rule/status/${id}`, { isActive: data })
    .then(response => success?.(response.data))
    .catch(response => failure?.(response.response));
}

export function upgradeStorage(data, success, failure) {
  instance()
    .post('api/payment/UpgradeStorage', data)
    .then(response => success?.(response.data))
    .catch(response => failure?.(response.response));
}
export function upgradeSubscription(data, success, failure) {
  instance()
    .post('api/payment/UpgradeSubscription', data)
    .then(response => success?.(response.data))
    .catch(response => failure?.(response.response));
}
export function refundSubscription(data, success, failure) {
  instance()
    .post('api/payment/RefundSubscription', data)
    .then(response => success?.(response.data))
    .catch(response => failure?.(response.response));
}

export default {
  PopulateIssues,
  // PopulateNotifications,
  PopulateRisks,
  PopulateMeetings,
  PopulateProjects,
  PopulateItemOrder,
  PopulatePermissions,
  PopulateTasks,
  getGlobalTimerTask,
  removeGlobalTaskTime,
  getWorkspaceRoles,
  FetchWorkspaceInfo,
  GetFeatureAccessPermissions,
  updateFeatureAccessPermissions,
  ResetWorkspaceInfo,
  UpdateStoreTasks,
  AddWorkspace,
  // AcceptTeamInvitation,
  ChangeMemberRoleInWorkspace,
  getTeamProjectStatus
};
