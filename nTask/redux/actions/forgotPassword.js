import constants from "./../constants/types";
import instance from "./../instance";
import { callbackify } from "util";
export function responseMessage(data) {
    return {
        type: constants.FORGOTPASSWORD,
        payload: {
            data,
        }
    };
}


export function forgotPasswordRequest(data, callback, failure) {
    return dispatch => {
        return instance().post("/api/account/forgotpassword", data)
            .then((response) => {
                const obj = {
                    statusCode: response.status,
                    data: response.data
                };
                return dispatch(responseMessage(obj));
            })
            .then((response) => {
                callback(response);
            })
            .catch(response => {
                let res = response.response;
                let newResponse = {
                    error: true,
                    statusCode: res ? res.status : null,
                    data: res ? res.data : null
                };
                failure(dispatch(responseMessage(newResponse)));
            });
    };
}

export function resetPassword(data, callback, failure) {
    instance().post("/api/account/resetpassword", data).then((response) => {
        callback(response)
    }).catch((response) => {
        failure(response.response)
    });
}

export function ResetForgotPassword(data) {
    return {
        type: constants.DELETEFORGOTPASS
    };
}

export default {
    forgotPasswordRequest,
    resetPassword,
    ResetForgotPassword
};