import instance from "../instance";

export function getWidgetAnalyticsData(query, success) {
  instance()
    .get("/api/widget/projectportfolio")
    .then(res => {
      success(res);
    });
}

export function getProjectAnalyticsData(success) {
  instance()
    .get("/api/widgetSettting/projectportfolio")
    .then(res => {
      success(res.data.entity);
    });
}
export function updateProjectAnalyticsData(data, success) {
  instance()
    .put("/api/widgetSettting/projectportfolio", data)
    .then(res => {
      success(res.data.entity);
    });
}
