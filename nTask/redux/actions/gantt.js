import constants from "./../constants/types";
import instance from "./../instance";
import action from "./tasks";
import { UpdateBulkStoreTasks, updateTaskDataAction } from "../actions/tasks";
import updateAction from "./workspace";
export function dispatchGantt(gantt, plannedVsActual) {

  return dispatch => {
    dispatch({
      type: constants.GANTTCOMPONENT,
      payload: { gantt, plannedVsActual }
    });
  };
}
export function getGanttTasks(projectId, callback) {
  return dispatch => {
    return instance()
      .get(`/api/Gantt/GetGanttTasks?projectId=${projectId}`)
      .then(response => {
        dispatch({
          type: constants.GETGANTTTASKS,
          payload: response.data
        });
        return response;
      })
      .then(response => {
        callback(response);
      });
  };
}
export function createGanttTask(payload, callback) {
  return dispatch => {
    return instance()
      .post(`/api/Gantt/CreateTask`, payload)
      .then(response => {
        if (window.userpilot) { /** tracking successfully created task in app */
          window.userpilot.track("Task Created Successfully");
        }
        callback(response.data.taskDto);
        if (response.data.task) {
          dispatch(action.StoreCopyTaskInfo(response.data.task));
        }
        return dispatch({
          type: constants.CREATEGANTTASK,
          payload: response.data.taskDto
        });
      });
  };
}
export function removeGanttTask(taskId) {
  return dispatch => {
    dispatch({
      type: constants.REMOVEGANTTASK,
      payload: { taskId }
    });
  };
}
export function updateGanttTaskOrder(payload, succ = () => { }, err = () => { }) {
  return dispatch => {
    return instance()
      .post(`/api/Gantt/UpdateTaskOrder`, payload)
      .then((res) => {
        succ();
      })
      .catch((err) => {
        err();
      });
  };
}
export function dispatchNewGanttTask(data) {
  return dispatch => {
    if (data.task) {
      dispatch(action.StoreCopyTaskInfo(data.task));
    }
    return dispatch({
      type: constants.CREATEGANTTASK,
      payload: data.taskDto
    });
  };
}
export function updateGanttTask(payload) {
  return dispatch => {
    return instance()
      .post(`/api/Gantt/EditTask`, payload)
      .then(response => {
        if (response.data && response.data.taskDto.type == "task") {
          const actions = updateAction.UpdateStoreTasks(response.data.task);
          dispatch(actions[0]);
        }
        return dispatch({
          type: constants.UPDATEGANTTASK,
          payload: response.data.taskDto
        });
      });
  };
}
export function updateMultipleGanttTask(payload, succ = () => { }, fail = () => { }) {
  return dispatch => {
    return instance()
      .post(`/api/gantt/BulkUpdateGanttTask`, payload)
      .then(response => {
        succ();
        dispatch(UpdateBulkStoreTasks(response.data.tasks))
        return dispatch({
          type: constants.BULKUPDATEGANTT,
          payload: response.data
        });
      }).catch((err) => {
        fail(err)
      })
  };
}
export function updateGanttTaskPatch(payload, succ = () => { }, fail = () => { }) {
  return dispatch => {
    return instance()
      .patch(`/api/task/${payload.taskId}`, payload.obj)
      .then(res => {
        succ();
        dispatch(updateTaskDataAction(res.data.entity))
        return dispatch({
          type: constants.UPDATEGANTTASKVALUE,
          payload: res.data.entity
        });
      }).then(() => {
        fail()
      })
  };
}

export function updateSortOrder(payload, ganttTasks) {
  return dispatch => {
    return instance()
      .post(`/api/Gantt/EditTask`, payload)
      .then(response => {
        return dispatch({
          type: constants.SAVESORTORDER,
          payload: ganttTasks
        });
      });
  };
}
export function SaveColumnOrder(projectId, columnOrder) {
  return dispatch => {
    return instance()
      .post(`/api/Gantt/SaveColumnOrder`, {
        projectId: projectId,
        GanttTaskColumnOrder: columnOrder
      })
      .then(response => {
        return dispatch({
          type: constants.SAVEGANTTCOLUMNORDER,
          payload: response.data
        });
      });
  };
}
export function dispatchGanttUpdate(data) {
  return dispatch => {
    if (data.taskDto.type == "task") {
      const actions = updateAction.UpdateStoreTasks(data.task);
      dispatch(actions[0]);
    }
    dispatch({
      type: constants.UPDATEGANTTASK,
      payload: data.taskDto
    });
  };
}
export function taskComplete(payload) {
  return dispatch => {
    return instance()
      .put(`/api/Gantt/MarkTaskAsCompleted?taskId=${payload}`)
      .then(response => {
        const actions = updateAction.UpdateStoreTasks(response.data.task);
        dispatch(actions[0]);
        dispatch(actions[1]);
        return dispatch({
          type: constants.UPDATEGANTTASK,
          payload: response.data.taskDto
        });
      });
  };
}
export function dispatchTaskComplete(data) {
  return dispatch => {
    const actions = updateAction.UpdateStoreTasks(data.task);
    dispatch(actions[0]);
    dispatch(actions[1]);
    return dispatch({
      type: constants.UPDATEGANTTASK,
      payload: data.taskDto
    });
  };
}
export function taskInComplete(payload) {
  return dispatch => {
    return instance()
      .put(`/api/Gantt/MarkTaskAsUnCompleted?taskId=${payload}`)
      .then(response => {
        const actions = updateAction.UpdateStoreTasks(response.data.task);
        dispatch(actions[0]);
        dispatch(actions[1]);
        return dispatch({
          type: constants.UPDATEGANTTASK,
          payload: response.data.taskDto
        });
      });
  };
}

export function saveTasksLink(link, callback) {
  return dispatch => {
    return instance()
      .post(`/api/Gantt/SaveTaskLink`, link)
      .then(response => {
        return dispatch({
          type: constants.SAVEGANTTLINKS,
          payload: response.data
        });
      });
  };
}
export function dispatchTasksLink(link) {
  return dispatch => {
    return dispatch({
      type: constants.SAVEGANTTLINKS,
      payload: link
    });
  };
}

export function deleteTasksLink(linkId, callback) {
  return dispatch => {
    return instance()
      .get(`/api/Gantt/DeleteTaskLink/${linkId}`)
      .then(response => {
        return dispatch({
          type: constants.DELETETASKLINK,
          linkId
        });
      })
      .then(() => {
        callback();
      });
  };
}
export function dispatchDeleteTasksLink(linkId) {
  return dispatch => {
    return dispatch({
      type: constants.DELETETASKLINK,
      linkId
    });
  };
}

export function createBulkGanttTask(obj, success) {
  return dispatch => {
    return instance()
      .post("api/Gantt/CreateGanttBulkTask", obj)
      .then(response => {
        if (window.userpilot) { /** tracking successfully created task in app */
          window.userpilot.track("Task Created Successfully");
        }
        //Extracting updated task added for traditional task
        const updatedTask = response.data.updatedTasks.map(task => {
          return task.task
        })
        if (updatedTask.length) {
          dispatch(action.UpdateBulkStoreTasks(updatedTask));
        }
        dispatch(action.addBulkTask(response.data));
        dispatch({
          type: constants.ADDGANTTBULKTASK,
          payload: response.data
        });
        success(response.data)
      });
  };
}
// export functions here
export function exportToPdf(data, success, failure) {
  return instance()
    .post(`api/DocsFileUploader/ExportMSPDF`, data, { 'responseType': 'blob' })
    .then(response => {
      success(response)
    }).catch(error => {
      failure(error)
    });
}
export function exportToExcel(data, success, failure) {
  return instance()
    .post(`api/DocsFileUploader/ExportMSExcel`, data, { 'responseType': 'blob' })
    .then(response => {
      success(response)
    }).catch(error => {
      failure(error)
    });
}
export function exportToMSProject(data, success, failure) {
  return instance()
    .post("api/DocsFileUploader/ExportMSProjects", data, {
      responseType: "blob",
    })
    .then(response => {
      success(response)
    }).catch(error => {
      failure(error)
    });
}

export default {
  getGanttTasks
};
