import constants from "./../constants/types";
import instance from "../instance";

export function PopulateNotificationsInfo(data) {
    return {
        type: constants.POPULATENOTIFICATIONS,
        payload: {
            data,
        }
    };
}
export function AddNotificationsInfo(data) {
    return {
        type: constants.NEWNOTIFICATION,
        payload: {
            data,
        }
    };
}

export function MarkRead(currentUser) {
    return {
        type: constants.MARKREADALL,
        payload: {
            currentUser
        }
    };
}

export function ResetNotificationsInfo() {
    return {
        type: constants.RESETNOTIFICATIONS
    };
}

export function PopulateNotifications(data) {
    return {
      type: constants.POPULATENOTIFICATIONS,
      payload: {
        data
      }
    };
  }

// export function FetchNotificationsInfo(callback) {
//     return dispatch => {
//         return instance().get("/api/notification/getnotifications")
//             .then((response) => {
//                 return dispatch(PopulateNotificationsInfo(response.data));
//             }).then(() => {
//                 callback()
//             });
//         return null;
//     };
// }
export function MarkAllNotificationsAsRead(currentUser, callback) {
    return dispatch => {
        return instance()
            .post("/api/Notification/MarkAllNotificationsAsRead")
            .then(response => {
                dispatch(MarkRead(currentUser))
                return { response: response };
            }).then((_data) => {
                callback(null, _data.response);
            })
            .catch(response => callback(response.response, null));
    };
}
export function FetchUserNotification(success, fail) {
    return (dispatch) => {
      return instance()
        .get("/api/account/userNotification")
        .then((response) => {
          dispatch(PopulateNotifications(response.data));
          success(response);
        })
        .catch(response => fail(response.response, null));
    };
  }

export function getNotifications(pageNumber, success, error) {
    return instance()
        .get(`api/Notification/GetAllUserNotifications?pageNo=${pageNumber ? pageNumber : 0}`)
        .then((response) => {
            success(response.data, null);
        })
        .catch(response => error(null, response.response));
}

export default {
    // FetchNotificationsInfo,
    ResetNotificationsInfo,
    FetchUserNotification
};