import cloneDeep from "lodash/cloneDeep";
import instance from "../instance";
import constants from "./../constants/types";
import { StoreCopyTaskInfo } from "./tasks";
import action, { PopulateWSTemplates } from "./workspace";

export function singalRBoards(key, data) {
  return (dispatch) => {
    if (key === "Create") {
      dispatch(addBoardToStore(data));
    }
    if (key === "Update") {
      dispatch(UpdateBoard(data));
    }
    if (key === "Delete") {
      dispatch(deleteBoardFromStore(data));
    }
  };
}
export function singalRKanban(key, data) {
  return (dispatch) => {
    if (key === "Create") {
    }
    if (key === "Update") {
      dispatch(updateKanbanObject(data));
    }
    if (key === "Delete") {
    }
  };
}
export function singalRKanbanTask(key, data) {
  return (dispatch) => {
    if (key === "Create" || key === "CopyTask") {
      dispatch(AddTaskInList(data));
    }
    if (key === "Update") {
      dispatch(updateKanbanTaskObject(data));
    }
    if (key === "Delete" || key === "Archive") {
      dispatch(DeleteArchiveTaskFromBoard(data));
    }
  };
}
export function UpdateKanbanLogsInstore(key, data) {
  return (dispatch) => {
    if (key === "Create") {
      dispatch(addActivityLog(data));
    }
  };
}
export function updateBoardBackgroundSettings(key, data) {
  return (dispatch) => {
    if (key === "Update") {
      dispatch(updateBoardSettings(data));
    }
  };
}
export function updateTaskCommentCountKanban(key, taskId) {
  return (dispatch) => {
    if (key === "Add") {
      dispatch(updateKanbanTaskObjectCount({ taskId, key }));
    }
  };
}

export function updateKanbanTaskObject(data) {
  return {
    type: constants.UPDATEKANBANTASKOBJECT,
    payload: {
      data,
    },
  };
}
export function updateKanbanTaskObjectCount(data) {
  return {
    type: constants.UPDATEKANBANTASKOBJECTCOUNT,
    payload: {
      data,
    },
  };
}
export function PopulateKanbanListing(data) {
  return {
    type: constants.POPULATEKANBANLISTING,
    payload: {
      data,
    },
  };
}
export function ClearKanban(data) {
  return {
    type: constants.CLEARKANBANDATA,
    payload: {
      data,
    },
  };
}

export function UpdateListOrder(data) {
  return {
    type: constants.UPDATEKANBANLISTORDER,
    payload: {
      data,
    },
  };
}
export function UpdateBoardSubTaskFilter(data) {
  return {
    type: constants.UPDATESUBTASKFILTER,
    payload: {
      data,
    },
  };
}
export function UpdateBoard(data) {
  return {
    type: constants.UPDATEBOARD,
    payload: {
      data,
    },
  };
}
export function UpdateKanbanColumn(data) {
  return {
    type: constants.UPDATEKANBANCOLUMN,
    payload: {
      data,
    },
  };
}
export function UpdateKanbanStatusRule(data) {
  return {
    type: constants.UPDATEKANBANSTATUSRULE,
    payload: {
      data,
    },
  };
}
export function UpdatePillShow(data) {
  return {
    type: constants.UPDATEPILLSHOW,
    payload: {
      data,
    },
  };
}
export function UpdateActivityLog(data) {
  return {
    type: constants.UPDATEACTIVITYLOG,
    payload: {
      data,
    },
  };
}
export function populateAllBoards(data) {
  return {
    type: constants.POPULATEALLBOARDS,
    payload: {
      data,
    },
  };
}

export function UpdateKanbanColumnAndTask(data) {
  return {
    type: constants.UPDATEFEATUREIMAGE,
    payload: data,
  };
}
export function updateKanbanObject(data) {
  return {
    type: constants.UPDATEKANBAN,
    payload: data,
  };
}
export function updateKanbanObjectAfterStatusDelete(data) {
  return {
    type: constants.UPDATEKANBANAFTERSTATUSDELETE,
    payload: data,
  };
}
export function addBoardToStore(data) {
  return {
    type: constants.ADDBOARDTOSTORE,
    payload: {
      data,
    },
  };
}
export function deleteBoardFromStore(data) {
  return {
    type: constants.DELETEBOARD,
    payload: {
      data,
    },
  };
}
export function AddTaskInList(data) {
  return {
    type: constants.ADDTASKINLIST,
    payload: {
      data,
    },
  };
}
export function AddAllTaskInList(data) {
  return {
    type: constants.ADDALLTASKINLIST,
    payload: {
      data,
    },
  };
}
export function DeleteArchiveTaskFromBoard(data) {
  return {
    type: constants.DELETEARCHIVETASKFROMBOARD,
    payload: {
      data,
    },
  };
}
export function addActivityLog(data) {
  return {
    type: constants.ADDACTIVITYLOG,
    payload: {
      data,
    },
  };
}
export function updateBoardSettings(data) {
  return {
    type: constants.UPDATEBOARDSETTINGS,
    payload: {
      data,
    },
  };
}
export function clearActivityLogg(data) {
  return {
    type: constants.CLEARACTIVITYLOG,
    payload: null,
  };
}
export function GetKanbanListingByProjectId(projectId, success, failure, permision) {
  return (dispatch) => {
    return instance()
      .get(`api/Board/GetBoardByProjectId?projectId=${projectId}`)
      .then((response) => {
        dispatch(PopulateKanbanListing(response.data.entity, permision));
        dispatch(UpdateBoard(response.data.entity.kanbanBoard));
        // dispatch(dispatchProjectViewDate(response.data));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function GetKanbanTemplate(template, success, failure) {
  return (dispatch) => {
    return instance()
      .get(`api/Template/GetTemplate?templateId=${template.templateId}`)
      .then((response) => {
        let data = {
          kanbanBoard: response.data.entity,
          unsortedTask: [],
        };
        dispatch(PopulateKanbanListing(data));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function setBoardBackground(data, dispatch, success, failure) {
  const { boardId, ...rest } = data;
  instance()
    .put(`api/Board/SetBackground/${boardId}`, rest)
    .then((response) => {
      dispatch(UpdateBoard(response.data.kanbanBoard));
      dispatch(updateKanbanObject(response.data.kanbanBoard));
      success(response);
    })
    .catch((response) => {
      failure(response);
    });
}

export function clearKanbanStoreData(dispatchFn) {
  if (dispatchFn) {
    dispatchFn(ClearKanban({}));
  } else {
    return (dispatch) => {
      dispatch(ClearKanban({}));
    };
  }
}
export function clearActivityLog() {
  return (dispatch) => {
    dispatch(clearActivityLogg());
  };
}
export function clearBoardsData(data) {
  return (dispatch) => {
    dispatch(populateAllBoards(data));
  };
}
export function updateTaskKanbanStore(data) {
  return (dispatch) => {
    dispatch({
      type: constants.UPDATETASKKANBANSTORE,
      payload: { data },
    });
  };
}
export function updateBoardCardSetting(dispatch, projectId, setting, success, failure) {
  instance()
    .put(`api/Board/SetCardSetting/${projectId}`, setting)
    .then((response) => {
      dispatch(UpdateBoard(response.data.kanbanBoard));
      dispatch(updateKanbanObject(response.data.kanbanBoard));
      success(response);
    })
    .catch((response) => {
      failure(response);
    });
}

export function getBoardEmailtotaskMail(workspaceId, projectId, success, failure) {
  instance()
    .get(`api/generateprojectemail/${workspaceId}/project/${projectId}/get`)
    .then((response) => {
      success(response);
    })
    .catch((response) => {
      failure(response);
    });
}

export function updateBoardEmailtotaskMail(workspaceId, projectId, success, failure) {
  instance()
    .get(`api/generateprojectemail/${workspaceId}/project/${projectId}/put`)
    .then((response) => {
      success(response);
    })
    .catch((response) => {
      failure(response);
    });
}

export function getBoardImages(success, failure) {
  instance()
    .get(`api/board/GetAllTeamBoardImages`)
    .then((response) => {
      success(response);
    })
    .catch((response) => {
      failure(response);
    });
}

export function deleteBoardImage(id, success, failure) {
  instance()
    .delete(`api/board/DeleteTeamBoardImage/${id}`)
    .then((response) => {
      success(response);
    })
    .catch((response) => {
      failure(response);
    });
}

export function updateTaskPositionsInStore(data) {
  return (dispatch) => {
    dispatch({
      type: constants.UPDATETASKPOSITIONSINSTORE,
      payload: data,
    });
  };
}
export function AddUnsortedTask(data) {
  return (dispatch) => {
    dispatch(AddTaskInList(data));
  };
}

export function UpdateKanban(data, success, failure, objectData) {
  return (dispatch) => {
    dispatch(updateKanbanObject(objectData));
    return instance()
      .post("api/Board/ReorderBoardStatus", data)
      .then((response) => {
        // dispatch(PopulateKanbanListing(response.data.entity));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function UpdateSubtaskFilterStatus(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("/api/Board/UpdateKanbanSubtaskFilter", data)
      .then((response) => {
        dispatch(UpdateBoardSubTaskFilter(data));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function UpdateKanbanTask(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("/api/project/UpdateKanbanTaskList", data)
      .then((response) => {
        dispatch(UpdateKanbanColumn(response.data));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}

export function SaveFeatureImage(data, task, column, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/board/SaveFeatureImage", data)
      .then((response) => {
        let updatedTask = { ...task };
        updatedTask.featureImagePath = response.data.featureimageUrl;
        dispatch({
          type: constants.UPDATETASKKANBANSTORE,
          payload: { data: updatedTask },
        });
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function uploadCustomBoardImage(data, success, failure) {
  instance()
    .post("api/board/UploadBoardImage", data)
    .then((response) => {
      success(response);
    })
    .catch((response) => {
      failure(response);
    });
}
export function SaveLibraryImage(data, task, column, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/board/SaveSplashImage", data)
      .then((response) => {
        let updatedTask = { ...task };
        updatedTask.featureImagePath = response.data.featureimageUrl;
        dispatch({
          type: constants.UPDATETASKKANBANSTORE,
          payload: { data: updatedTask },
        });
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function UpdateTaskDragDrop(data, success, failure) {
  let ApiData = cloneDeep(data.objTask);
  ApiData.description =
    ApiData.description && ApiData.description !== ""
      ? window.btoa(unescape(encodeURIComponent(ApiData.description)))
      : ApiData.description;
  ApiData.activityLog = [];
  ApiData.CalenderDetails = [];

  return (dispatch) => {
    return instance()
      .post("api/Board/AddTask", { objTask: ApiData, itemOrder: data.itemOrder })
      .then((response) => {
        const actions = action.UpdateStoreTasks(response.data.entity);
        dispatch(actions[0]);
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function UpdateKanbanTitleColor(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("/api/project/UpdateKanbanTitleColor", {
        id: data.id,
        listName: data.listName,
        colorCode: data.colorCode,
      })
      .then((response) => {
        dispatch(UpdateKanbanColumn(response.data));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function updateColumSettings(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("/api/project/UpdateKanbanListSetting", data)
      .then((response) => {
        dispatch(UpdateKanbanColumn(response.data));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function updateStatusRule(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("/api/Board/SaveBoardStatusRule", data)
      .then((response) => {
        dispatch(UpdateKanbanStatusRule(response.data.savedboard));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export async function updateShowpill(data, success, failure) {
  return instance()
    .put("api/board/setUserListLabelState", data)
    .then((response) => {
      success(response);
    })
    .catch((response) => {
      failure(response);
    });
}
export async function GetAddTaskLabelOptions(success, failure) {
  return instance()
    .get("api/board/GetBoardListMetaValues")
    .then((response) => {
      success(response);
    })
    .catch((response) => {
      failure(response);
    });
}
export function UseTheTemplate(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("/api/project/UseTheTemplate", data)
      .then((response) => {
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function getActivityLog(data, success, failure) {
  return (dispatch) => {
    return instance()
      .get("api/Board/GetBoardActivityLog?projectId=" + data)
      .then((response) => {
        dispatch(UpdateActivityLog(response.data));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}

export function GetAllTemplatesByGroup(success, failure) {
  return (dispatch) => {
    return instance()
      .get("api/Template/GetAllTemplatesByGroup")
      .then((response) => {
        dispatch(PopulateWSTemplates(response.data.entity));
        success(response.data.entity);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function getAllBoards(success, failure) {
  return (dispatch) => {
    return instance()
      .get("api/Board/GetAllBoards")
      .then((response) => {
        dispatch(populateAllBoards(response.data.entity));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function CreateNewCustomStatus(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/Board/AddBoardStatus", data)
      .then((response) => {
        dispatch(updateKanbanObject(response.data.entity));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function DeleteStatus(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/Board/RemoveBoardStatus", data)
      .then((response) => {
        dispatch(updateKanbanObjectAfterStatusDelete(response.data.entity));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function DuplicateStatus(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/Board/DublicateBoardStatus", data)
      .then((response) => {
        dispatch(updateKanbanObject(response.data.entity.updateboard));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function CreateBoard(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/Board/CreateBoard", data)
      .then((response) => {
        dispatch(addBoardToStore(response.data.entity));
        success(response);
      })
      .catch((response) => {
        failure(response.response.data);
      });
  };
}
export function UseTemplate(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/Board/UseTemplate", data)
      .then((response) => {
        dispatch(addBoardToStore(response.data.entity));
        success(response);
      })
      .catch((response) => {
        failure(response.response.data);
      });
  };
}
//used for updating board
export function UpdateKanbanBoard(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/Board/UpdateBoard", data)
      .then((response) => {
        dispatch(UpdateBoard(response.data.entity));
        dispatch(updateKanbanObject(response.data.entity));
        success(response);
      })
      .catch((response) => {
        failure(response.response.data);
      });
  };
}
export function DeleteBoard(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/Board/DeleteBoard", data)
      .then((response) => {
        dispatch(deleteBoardFromStore(data));
        success(response);
      })
      .catch((response) => {
        failure(response.response);
      });
  };
}
export function SaveAsTemplate(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/Board/SaveAsTemplate", data)
      .then((response) => {
        // dispatch(PopulateWSTemplates(response.data.entity));
        success(response);
      })
      .catch((response) => {
        failure(response.response.data);
      });
  };
}
export function SaveTask(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("api/Board/CreateTask", data)
      .then((response) => {
        // dispatch(AddTaskInList(response.data.entity.boardtasks));
        dispatch(AddAllTaskInList(response.data.entity.boardtasks));
        dispatch(StoreCopyTaskInfo(response.data.entity.task));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function CopyTask(data, success, failure) {
  return (dispatch) => {
    return instance()
      .get("/api/Board/copyusertask?taskId=" + data.taskId)
      .then((response) => {
        dispatch(AddTaskInList(response.data.entity.boardtasks));
        dispatch(StoreCopyTaskInfo(response.data.entity.task));
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export function UpdateBoardTaskOrder(data, success, failure) {
  return (dispatch) => {
    return instance()
      .post("/api/Board/UpdateBoardTaskOrder", data)
      .then((response) => {
        success(response);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
export default {
  GetKanbanListingByProjectId,
  PopulateKanbanListing,
  UpdateKanban,
  UpdateListOrder,
  UpdateKanbanTask,
  updateColumSettings,
  getActivityLog,
  UpdateTaskDragDrop,
  UpdateSubtaskFilterStatus,
  /*GetTemplates,*/
  GetAllTemplatesByGroup,
  GetKanbanTemplate,
  UseTheTemplate,
  SaveFeatureImage,
  SaveLibraryImage,
  CreateNewCustomStatus,
  DeleteStatus,
  DuplicateStatus,
  getAllBoards,
  CreateBoard,
  addBoardToStore,
  UpdateKanbanBoard,
  UpdateBoardSubTaskFilter,
  SaveTask,
  DeleteArchiveTaskFromBoard,
  AddUnsortedTask,
  UseTemplate,
  CopyTask,
  UpdateBoardTaskOrder,
};
