import instance from "../instance";
import constants from "./../constants/types";

function saveColumnPositionAction(data) {
  return {
    type: constants.UPDATE_COLUMN_POSITION,
    payload: data
  }
}


export function saveColumnPostion(data, type, dispatch) {
  instance()
    .put("api/savecolumnspositon/" + type, data)
    .then((response) => {
      const action = saveColumnPositionAction(data)
      dispatch(action)
    }).then(() => {
      callback()
    })
    .catch((response) => {
    });

}
export function saveReportingColumnPostion(data, type, groupType, dispatch) {
  instance()
    .put("api/saveteamcolumnspositon/" + type + "/" + groupType, data)
    .then((response) => {
      const action = saveColumnPositionAction(data)
      dispatch(action)
    }).then(() => {
      callback()
    })
    .catch((response) => {
    });

}
