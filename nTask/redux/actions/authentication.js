import constants from "../constants/types";
import instance from "../instance";
import querystring from "querystring";
import axios from "axios";
import API_URL from "./../../config";
import workspace from "./workspace";
import helper from "../../helper/index";

export function responseMessage(data) {
  return {
    type: constants.AUTHENTICATEEMAIL,
    payload: {
      data
    }
  };
}
//api/account/sendcodetoverify
//https://app.ntaskmanager.com/api/account/sendcode
//api/account/verifycode
//api/Account/GetUserState

export function updateAuth(Auth) {
  return {
    type: constants.AUTHENTICATEEMAIL,
    payload: {
      Auth
    }
  };
}
//IsWorkspaceExist

export function isWorkspaceExist(teamID, callback) {
  return dispatch => {
    return instance()
      .get("/api/team/isWorkspaceExist?teamId=" + teamID)
      .then(response => {
        //
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}
export function signinWithSso(token, callback, failure) {
  return dispatch => {
    return instance()
      //.get(`https://auth.ntaskmanager.com/api/v1/User/AuthenticateEmail?userName=${email}&SSOAuthenticate=true`)
      .get(`/api/account/GenerateSSOToken?SSOInfo=${token}`)
      .then(response => {
        callback(response);
        dispatch({
          type: constants.CHECKAUTH,
          payload: true
        });
      })
      .catch(response => {
        failure(response);
      });
    }
}

export function getUserState(callback) {
  return dispatch => {
    return instance()
      .get("/api/Account/GetUserState")
      .then(response => {
        //
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function verifycode(data, callback) {
  return dispatch => {
    return instance()
      .post("api/account/verifycode", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function sendcode(obj, callback) {
  return dispatch => {
    return instance()
      .get(
        "api/account/sendcode?userName=" +
          obj.userName +
          "&returnUrl=" +
          obj.returnUrl +
          "&rememberMe=" +
          obj.rememberMe
      )
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function sendcodetoverify(data, callback) {
  return dispatch => {
    return instance()
      .post("api/account/sendcodetoverify", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function getOnBoardingData(callback) {
  return dispatch => {
    return instance()
      .get("/api/Account/GetOnBoardingData")
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function authenticateEmailRequest(data, callback, failure) {
  return dispatch => {
    return instance()
      .get(
        "/api/account/authenticateemail?userName=" +
          data.email.replace("+", "%2B")
      )
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function authenticatePassword(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/token", querystring.stringify(data))
      .then(response => {
        callback(response.data);
        if (response.data.requireOTP == "false" && sessionStorage.getItem("isMicrosftTeamsAuth") == null) {
          return dispatch({
            type: constants.CHECKAUTH,
            payload: true
          });
        }
        if(sessionStorage.getItem("isMicrosftTeamsAuth") != null) {
          sessionStorage.removeItem("isMicrosftTeamsAuth");
        }
      })
      .catch(response => {
        failure(response);
      });
  };
}
export function authenticateOnlyPassword(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/token", querystring.stringify(data))
      .then(response => {
        callback();
      })
      .catch(response => {
        failure();
      });
  };
}
export function authenticateSessionPassword(data, success, failure) {
  return dispatch => {
    return instance()
      .post("/token", querystring.stringify(data))
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function authenticateUser(state) {
  return dispatch => {
    return dispatch({
      type: constants.CHECKAUTH,
      payload: state
    });
  };
}
export function getRandomQuotes(callback) {
  axios.get("https://0yahalgupg.execute-api.us-east-1.amazonaws.com/dev/getWelcomeNote")
    .then(response => {
      callback(response);
    });
}
// export function getRandomQuotes(callback) {
//   instance()
//     .get("api/ntask/GetWelcomeNote")
//     .then(response => {
//       callback(response);
//     });
// }
export function isUserPresent(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/account/sendinvitationemail", data)
      .then(response => {
        dispatch(workspace.ChangeMemberRoleInWorkspace(response.data, true));
        return response;
      })
      .then(response => {
        callback({ data: response.data[0] });
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function CheckAndAddUser(data, callback) {
  //CheckAndAddUser

  return dispatch => {
    return instance()
      .post("/api/account/CheckAndAddUser", data)
      .then(response => {
        callback(response);
      })
      .then(response => {
        return dispatch({
          type: constants.CHECKAUTH,
          payload: true
        });
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

function getLocalInstance() {
  const instance = axios.create({
    baseURL: API_URL,
    headers: {
      Authorization: helper.getToken() || null,
      "Content-Type": "application/json"
    }
  });
  return instance;
}
export function validateAuth(callback) {
  return dispatch => {
    return getLocalInstance()
      .get(`${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL }api/account/validateauth`)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}
export function validateToken(success, failure) {
  return dispatch => {
    return instance()
      .get(`${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL }api/account/validateauth`)
      .then(response => {
        success(response);
        return dispatch({
          type: constants.CHECKAUTH,
          payload: true
        });
      })
      .catch(response => {
        failure(response.response);
        return dispatch({
          type: constants.CHECKAUTH,
          payload: false
        });
      });
  };
}

export default {
  authenticateEmailRequest,
  authenticatePassword,
  isUserPresent,
  validateAuth,
  getUserState,
  updateAuth,
  CheckAndAddUser,
  isWorkspaceExist
};
