import instance from "../instance";
import constants from "./../constants/types";

function getColumnsListAction(data) {
  return {
    type: constants.SET_COLUMN_LIST,
    payload: data
  }
}
export function updateColumnAction(data) {
  return {
    type: constants.UPDATE_COLUMN,
    payload: data
  }
}
export function updateTeamColumnAction(data) {
  return {
    type: constants.UPDATE_TEAM_COLUMN,
    payload: data
  }
}

export function getColumnsList(callback, dispatchFn, failure = () => { }) {
  if (dispatchFn) {
    instance()
      .get("api/columns/GetAllSystemColumn")
      .then((response) => {
        const action = getColumnsListAction(response.data.entity)
        dispatchFn(action)
      }).then(() => {
        callback()
      })
      .catch((response) => {
        failure()
      });
  } else {
    return (dispatch) => {
      instance()
        .get("api/columns/GetAllSystemColumn")
        .then((response) => {
          const action = getColumnsListAction(response.data.entity)
          dispatch(action)
        }).then(() => {
          callback()
        })
        .catch((response) => {
          failure()
        });
    };
  }
}

export function updateColumn(data, type, dispatchFn, callback) {
  if (dispatchFn) {
    const action = updateColumnAction(data)
    dispatchFn(action)
    instance()
      .put("api/savecolumnsInfo/" + type, data).then((res) => {
        callback()
      })
      .catch((response) => {
      });
  } else {
    return (dispatch) => {
      const action = updateColumnAction(data)
      dispatch(action)
      instance()
        .put("api/savecolumnsInfo/" + type, data)
        .then((response) => {

        })
        .catch((response) => {
        });
    };
  }
}


export function getTeamColumn(feature, groupType, success) {
  instance()
    // api/columns/TeamSystemColumn/{feature}/{groupType}
    .get(`/api/columns/TeamSystemColumn/${feature}/${groupType}`)
    .then(res => {
      success(res);
    }).catch(err => {
    });

}
export function updateTeamColumn(feature, groupType, data, callback, failure) {
  instance()
    .put(`/api/columns/TeamSystemColumn/${feature}/${groupType}`, data)
    .then((res) => {
      callback(res)
    })
    .catch((response) => {
      failure(response)
    });

}

export function addGridColumns(key, data) {
  return dispatch => {
    if (key === "Create") {
      dispatch({
        type: constants.ADD_GRID_COLUMN,
        payload: data
      });
    }
    if (key === "Update") {
      dispatch({
        type: constants.UPDATE_GRID_COLUMN,
        payload: data
      });
    }
    if (key === "Delete") {
    }
  };
}