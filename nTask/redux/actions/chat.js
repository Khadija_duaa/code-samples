import constants from "./../constants/types";
import instance from "../instance";
import taskActions from "./tasks";

export function GetUpdatesComments(contextUrl, id, success, fail) {
  return dispatch => {
    return instance()
      .get("/api/" + contextUrl + "/GetComments?id=" + id)
      .then(result => {
        let comments =
          result.data.messages && result.data.messages.length > 0 ? result.data.messages : [];
        dispatch(PopulateChatData(id, comments));
        success(result);
      })
      .catch(error => {
        fail(error.response);
      });
  };
}
export function GetReplyLaterComments(contextUrl, id, success, fail) {
  return dispatch => {
    return instance()
      .get("/api/" + contextUrl + "/GetComments?id=" + id + "&replyLater=true")
      .then(result => {
        let comments =
          result.data.messages && result.data.messages.length > 0 ? result.data.messages : [];
        dispatch(PopulateChatData(id, comments));
        success(result);
      })
      .catch(error => {
        fail(error.response);
      });
  };
}
export function ClearAllReplyLaterFlag(contextUrl, contextId, data, success, fail) {
  return dispatch => {
    return instance()
      .get("/api/" + contextUrl + "/RemoveReplyLaterAll?id=" + contextId)
      .then(result => {
        dispatch(PopulateChatData(contextId, []));
        success(result);
      })
      .catch(error => {
        fail(error.response);
      });
  };
}
export function SetReplyLaterFlag(contextUrl, contextId, data, success, fail) {
  return dispatch => {
    return instance()
      .get("/api/" + contextUrl + "/ReplyLater?id=" + data.comment.commentId)
      .then(result => {
        data.comment.replyLater = true;
        dispatch(StoreChatUpdate(contextId, data));
        success(result);
      })
      .catch(error => {
        fail(error.response);
      });
  };
}
export function ClearReplyLaterFlag(contextUrl, contextId, data, success, fail) {
  return dispatch => {
    return instance()
      .get("/api/" + contextUrl + "/ClearReplyLater?id=" + data.comment.commentId)
      .then(result => {
        data.comment.replyLater = false;
        dispatch(StoreChatUpdate(contextId, data));
        success(result);
      })
      .catch(error => {
        fail(error.response);
      });
  };
}

export function ReplyUserCommentFromUpdates(contextUrl, contextId, contextView, comment, parentComment, success, fail) {
  return dispatch => {
    const updatedComment = {...comment};
    updatedComment.commentText = window.btoa(unescape(encodeURIComponent(updatedComment.commentText)));
      dispatch(StoreChatNew(contextId, { comment }));
    return instance()
      .post("/api/" + contextUrl + "/replyusercomment", updatedComment)
      .then(result => {
        dispatch(StoreChatUpdate(contextId, parentComment));
        dispatch(StoreChatUpdate(contextId, result.data));
        success(result);
        if(contextView == 'Task')
        dispatch(taskActions.updateTaskCommentsInfo({
          taskId: contextId,
          totalAttachements: result.data.totalAttachements,
          totalComments: result.data.totalComments,
          totalUnreadComment: result.data.totalUnreadComment,
        }))
      })
      .catch(error => {
        fail(error.response);
      });
  };
}
export function ReplyUserCommentFromLater(contextUrl, contextId, contextView, comment, parentComment, success, fail) {
  return dispatch => {
    const updatedComment = {...comment};
    updatedComment.commentText = window.btoa(unescape(encodeURIComponent(updatedComment.commentText)));
    return instance()
      .post("/api/" + contextUrl + "/replyusercomment", updatedComment)
      .then(result => {
        dispatch(StoreChatDelete(contextId, parentComment));
        success(result);
        if(contextView == 'Task')
        dispatch(taskActions.updateTaskCommentsInfo({
          taskId: contextId,
          totalAttachements: result.data.totalAttachements,
          totalComments: result.data.totalComments,
          totalUnreadComment: result.data.totalUnreadComment,
        }))
      })
      .catch(error => {
        fail(error.response);
      });
  };
}
export function saveUserComment(contextUrl, contextId, contextView, comment, success, fail) {
  return dispatch => {
    const updatedComment = {...comment};
    updatedComment.commentText =window.btoa(unescape(encodeURIComponent(updatedComment.commentText)));
    dispatch(StoreChatNew(contextId, { comment }));
    return instance()
      .post("/api/" + contextUrl + "/SaveUserComment", updatedComment)
      .then(result => {
        dispatch(StoreChatUpdate(contextId, result.data));
        success(result);
        if(contextView == 'Task')
        dispatch(taskActions.updateTaskCommentsInfo({
          taskId: contextId,
          totalAttachements: result.data.totalAttachements,
          totalComments: result.data.totalComments,
          totalUnreadComment: result.data.totalUnreadComment,
        }))
      })
      .catch(error => {
        fail(error.response);
      });
  };
}

export function ConvertToTask(contextUrl, contextId, data, success, fail) {
  return dispatch => {
    return instance()
      .post("/api/" + contextUrl + "/converttotask", data)
      .then(result => {
        dispatch(StoreCopyTaskInfo(result.data.task));
        success(result);
      })
      .catch(error => {
        fail(error.response);
      });
  };
}
export function DeleteUesrComment(contextUrl, contextId, contextView, data, success, fail) {
  return dispatch => {
    return instance()
      .get("/api/" + contextUrl + "/deleteusercomment?id=" + data.comment.commentId)
      .then(result => {
        dispatch(StoreChatDelete(contextId, data));
        success(result);
        if(contextView == 'Task')
        dispatch(taskActions.updateTaskCommentsInfo({
          taskId: contextId,
          totalAttachements: result.data.counts.totalAttachements,
          totalComments: result.data.counts.totalComments,
          totalUnreadComment: result.data.counts.totalUnreadComment,
        }))
      })
      .catch(error => {
        fail(error.response);
      });
  };
}
export function DeleteChatAttachment(contextUrl, contextId, contextView, data, success, failure) {
  return dispatch => {
    return instance()
      .get("/api/" + contextUrl + "/DeleteUserAttachment?id=" + data.comment.attachment.id)
      .then(result => {
        data.comment.attachment = null;
        dispatch(StoreChatUpdate(contextId, data));
        success(result);
        if(contextView == 'Task')
        dispatch(taskActions.updateTaskCommentsInfo({
          taskId: contextId,
          totalAttachements: result.data.counts.totalAttachements,
          totalComments: result.data.counts.totalComments,
          totalUnreadComment: result.data.counts.totalUnreadComment,
        }))
      })
      .catch(error => {
        failure(error.response);
      });
  };
}

export function UpdateUesrComment(contextUrl, contextId, comment, success, fail) {
  return dispatch => {
    const updatedComment = {...comment};
    updatedComment.commentText = window.btoa(unescape(encodeURIComponent((updatedComment.commentText))));
    return instance()
      .post("/api/" + contextUrl + "/updateusercomment", updatedComment)
      .then(result => {
        dispatch(StoreChatUpdate(contextId, result.data));
        success(result);
      })
      .catch(error => {
        fail(error.response);
      });
  };
}

export function uploadFileToAmazon(contextUrl, _FormData, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/docsfileuploader/UploadDocsFileAmazonS3", _FormData)
      .then(response => {
        callback(response);
      })
      .catch(error => {
      });
  };
}

export function saveAttachmentToContextObject(contextUrl, contextId, attachment, success, fail) {
  return dispatch => {
    return instance()
      .post("/api/" + contextUrl + "/SaveAttachment", attachment)
      .then(response => {
        return response;
      })
      .then(response => {
        success(response);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}
export function StartTyping(contextUrl, contextId, data, success, fail) {
  return dispatch => {
    return instance()
      .post("/api/" + contextUrl + "/StartTyping", data)
      .then(response => {
        success(response);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}
export function StopTyping(contextUrl, contextId, data, success, fail) {
  return dispatch => {
    return instance()
      .post("/api/" + contextUrl + "/StopTyping", data)
      .then(response => {
        success(response);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}

export function StoreCopyTaskInfo(data) {
  return {
    type: constants.COPYTASKS,
    payload: {
      data,
    },
  };
}

export function StoreChatUpdate(groupId, data) {
  return {
    type: constants.UPDATECHATITEM,
    payload: {
      chat: data,
      groupId: groupId,
    },
  };
}
export function StoreChatDelete(groupId, data) {
  return {
    type: constants.DELETECHATITEM,
    payload: {
      chat: data,
      groupId: groupId,
    },
  };
}
export function StoreChatNew(groupId, data) {
  return {
    type: constants.NEWCHATITEM,
    payload: {
      chat: data,
      groupId: groupId,
    },
  };
}
export function StoreChatTyping(groupId, data) {
  return {
    type: constants.CHATTYPINGSTATUS,
    payload: {
      data: data,
      groupId: groupId,
    },
  };
}
export function PopulateChatData(groupId, data) {
  return {
    type: constants.POPULATECHAT,
    payload: {
      chat: data,
      groupId: groupId,
    },
  };
}
export function ClearStoreChat(groupId) {
  return {
    type: constants.CLEARCHAT,
    payload: {
      groupId: groupId,
    },
  };
}
export function dispatchChatUpdate(groupId, data) {
  return dispatch => {
    dispatch(StoreChatUpdate(groupId, data));
  };
}
export function dispatchChatDelete(groupId, data) {
  return dispatch => {
    dispatch(StoreChatDelete(groupId, data));
  };
}
export function dispatchChatNew(groupId, data) {
  return dispatch => {
    dispatch(StoreChatNew(groupId, data));
  };
}
export function dispatchChatTyping(groupId, data) {
  return dispatch => {
    dispatch(StoreChatTyping(groupId, data));
  };
}
export function clearStore(groupId) {
  return dispatch => {
    dispatch(ClearStoreChat(groupId));
  };
}
