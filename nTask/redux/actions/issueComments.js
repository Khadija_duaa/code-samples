import constants from "./../constants/types";
import instance from "../instance";

export function PopulateIssueCommentData(data) {
  return {
    type: constants.POPULATEISSUECOMMENTS,
    payload: {
      data,
    },
  };
}

export function UpdateIssueCommentData(data) {
  return {
    type: constants.UPDATEISSUECOMMENTS,
    payload: {
      data,
    },
  };
}

export function DeleteIssueCommentData(data) {
  return {
    type: constants.DELETEISSUECOMMENTS,
    payload: {
      data,
    },
  };
}

export function AddNewIssueCommentData(data) {
  return {
    type: constants.ADDNEWISSUECOMMENTS,
    payload: {
      data,
    },
  };
}

export function PopulateIssueActivities(data) {
  return {
    type: constants.POPULATEISSUECOMMENTSACTIVITIES,
    payload: {
      data,
    },
  };
}

export function PopulateIssueNotifications(data) {
  return {
    type: constants.POPULATEISSUECOMMENTSNOTIFICATIONS,
    payload: {
      data,
    },
  };
}
export function ClearIssueNotifications(data) {
  return {
    type: constants.CLEARISSUECOMMENTSNOTIFICATIONS,
    payload: {
      data,
    },
  };
}

export function updateIssueNotifications(data) {
  return {
    type: constants.UPDATEISSUENOTIFICATIONS,
    payload: {
      data,
    },
  };
}

export function UpdateIssueNotifications(data) {
  return (dispatch) => {
    dispatch(updateIssueNotifications(data));
  };
}

export default {
  PopulateIssueCommentData,
  UpdateIssueCommentData,
  DeleteIssueCommentData,
  PopulateIssueActivities,
  PopulateIssueNotifications,
  ClearIssueNotifications,
  UpdateIssueNotifications,
  updateIssueNotifications
};
