import constants from "./../constants/types";
import instance from "../instance";
import order from "./itemOrder";
import rc from "./riskComments";
import cloneDeep from "lodash/cloneDeep";

export function dispatchRisks(data) {
  return dispatch => {
    dispatch({
      type: constants.POPULATERISKSINFO,
      payload: {
        data,
      },
    });
  };
}

export function PopulateRisksInfo(data) {
  return {
    type: constants.POPULATERISKSINFO,
    payload: {
      data,
    },
  };
}

export function UpdateStoreRisk(data) {
  return {
    type: constants.UPDATERISK,
    payload: {
      data,
    },
  };
}
export function bulkUpdateRisks(data) {
  return {
    type: constants.BULKUPDATERISK,
    payload: data,
  };
}

export function StoreCopyRiskInfo(data) {
  return {
    type: constants.COPYRISK,
    payload: {
      data,
    },
  };
}

export function UpdateBulkStoreRisk(data, isDelete) {
  return {
    type: constants.UPDATEBULKRISKINFO,
    payload: {
      data,
      isDelete: isDelete,
    },
  };
}

export function deleteStoreRisk(data) {
  return {
    type: constants.DELETERISK,
    payload: {
      data,
    },
  };
}

export function ResetRisksInfo() {
  return {
    type: constants.RESETRISKSINFO,
  };
}

export function MarkRead(id, currentUser) {
  return {
    type: constants.MARKREADRISK,
    payload: {
      id,
      currentUser,
    },
  };
}
export function GetRiskMatrix(callback, failure) {
  return instance()
    .get("api/issuerisk/GetRiskAnalytics")
    .then(response => {
      callback(response.data);
    })
    .catch(response => failure(response.response));
}
export function updateRisk(obj, callback = () => {}, failure = () => {}, dispatchFn) {
  if (dispatchFn) {
    instance()
      .put(`api/risk/id=${obj.groupId}`, [obj])
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .put(`api/risk/id=${obj.groupId}`, [obj])
        .then(response => {
          callback(response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}
export function FetchRisksInfo(callback) {
  return dispatch => {
    return instance()
      .get("/api/issuerisk/getrisks")
      .then(response => {
        //
        return dispatch(PopulateRisksInfo(response.data));
      })
      .then(() => {
        callback();
      });
    return null;
  };
}
export function DeleteRisk(data, callback, failure, dispatchfn) {
  if (dispatchfn) {
    instance()
      .get("/api/issueRisk/deleteRisk?riskId=" + data)
      .then(response => {
        dispatchfn(deleteStoreRisk(data));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => failure(response.response));
  } else {
    return dispatch => {
      return instance()
        .get("/api/issueRisk/deleteRisk?riskId=" + data)
        .then(response => {
          dispatch(deleteStoreRisk(data));
          return { response: response };
        })
        .then(_data => {
          callback(_data.response);
        })
        .catch(response => failure(response.response));
    };
  }
}
export function dispatchRiskDelete(data) {
  return dispatch => {
    dispatch(deleteStoreRisk(data));
  };
}
export function SaveRisk(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/issuerisk/saverisk", data)
      .then(response => {
        if (window.userpilot) {
          /** tracking successfully created risk in app */
          window.userpilot.track("Risk Created Successfully");
        }
        dispatch(StoreCopyRiskInfo(response.data));
        dispatch(order.FetchItemOrderInfo(() => {}));
        return { response: response };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => callback(response.response, null));
  };
}
export function dispatchNewRisk(data) {
  return dispatch => {
    dispatch(StoreCopyRiskInfo(data));
  };
}
export function UpdateRisk(data, callback, failure) {
  let ApiData = cloneDeep(data);
  ApiData.detail =
    ApiData.detail && ApiData.detail !== ""
      ? window.btoa(unescape(encodeURIComponent(ApiData.detail)))
      : ApiData.detail;
  ApiData.mittigation =
    ApiData.mittigation && ApiData.mittigation !== ""
      ? window.btoa(unescape(encodeURIComponent(ApiData.mittigation)))
      : ApiData.mittigation;
  return dispatch => {
    return instance()
      .put(`api/IssueRisk/UpdateRisk?id=${ApiData["id"]}`, ApiData)
      .then(response => {
        dispatch(UpdateStoreRisk(response.data));
        dispatch(rc.PopulateRiskActivities(response.data.activityLog));
        dispatch(rc.PopulateRiskNotifications(response.data.notifications));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function MarkRiskAsStarred(data, callback, failure, dispatchFn) {
  if (dispatchFn) {
    instance()
      .post("api/issuerisk/MarkRiskAsStarred", data)
      .then(response => {
        dispatchFn(UpdateStoreRisk(response.data));
        return { response: response };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .post("api/issuerisk/MarkRiskAsStarred", data)
        .then(response => {
          dispatch(UpdateStoreRisk(response.data));
          return { response: response };
        })
        .then(_data => {
          callback(null, _data.response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}

export function dispatchRisk(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(UpdateStoreRisk(data));
    dispatchFn(rc.PopulateRiskActivities(data.activityLog));
    // dispatchFn(rc.PopulateRiskNotifications(data.notifications));
  } else {
    return dispatch => {
      dispatch(UpdateStoreRisk(data));
      dispatch(rc.PopulateRiskActivities(data.activityLog));
      // dispatch(rc.PopulateRiskNotifications(data.notifications));
    };
  }
}
export function bulkUpdateRisk(data) {
  return dispatch => {
    dispatch(bulkUpdateRisks(data));
  };
}

export function ArchiveRisk(data, callback, failure, dispatchFn) {
  if (dispatchFn) {
    instance()
      .get(`api/IssueRisk/ArchiveRisk?id=${data}`)
      .then(response => {
        dispatchFn(deleteStoreRisk(data));
        // dispatchFn(UpdateStoreRisk(response.data));
        // dispatchFn(rc.PopulateRiskActivities(response.data.activityLog || []));
        // dispatchFn(rc.PopulateRiskNotifications(response.data.notifications || []));
        return response;
      })
      .then(_data => {
        callback(_data);
      })
      .catch(response => failure(response.response));
  } else {
    return dispatch => {
      return instance()
        .get(`api/IssueRisk/ArchiveRisk?id=${data}`)
        .then(response => {
          dispatch(deleteStoreRisk(data));
          // dispatch(UpdateStoreRisk(response.data));
          // dispatch(rc.PopulateRiskActivities(response.data.activityLog));
          // dispatch(rc.PopulateRiskNotifications(response.data.notifications));
          return response;
        })
        .then(_data => {
          callback(_data);
        })
        .catch(response => failure(response.response));
    };
  }
}

export function UnarchiveRisk(data, callback, failure, dispatchFn) {
  if (dispatchFn) {
    instance()
      .get(`api/IssueRisk/UnarchiveRisk?id=${data["id"]}`)
      .then(response => {
        dispatchFn(UpdateStoreRisk(response.data));
        dispatchFn(rc.PopulateRiskActivities(response.data.activityLog));
        dispatchFn(rc.PopulateRiskNotifications(response.data.notifications));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => failure(response.response));
  } else {
    return dispatch => {
      return instance()
        .get(`api/IssueRisk/UnarchiveRisk?id=${data["id"]}`)
        .then(response => {
          dispatch(UpdateStoreRisk(response.data));
          dispatch(rc.PopulateRiskActivities(response.data.activityLog));
          dispatch(rc.PopulateRiskNotifications(response.data.notifications));
          return response;
        })
        .then(response => {
          callback(response);
        })
        .catch(response => failure(response.response));
    };
  }
}

export function BulkUpdateRisk(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/IssueRisk/UpdateBulkRisks", data)
      .then(response => {
        // Type == 14, means it is delete operation
        const { type, riskIds } = data;
        if (type == 14) {
          dispatch(UpdateBulkStoreRisk(riskIds, true));
        } else {
          dispatch(UpdateBulkStoreRisk(response.data));
        }
        return { response: response };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => callback(response.response, null));
  };
}

export function MarkRiskNotificationsAsRead(id, currentUser, callback) {
  return dispatch => {
    return instance()
      .get("/api/Notification/MarkRiskNotificationsAsRead?riskId=" + id)
      .then(response => {
        if (response && response.data && response.data.updatedNotifications) {
          dispatch(MarkRead(id, currentUser));
          dispatch(rc.ClearRiskNotifications([]));
        }
        return { response: response };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => callback(response.response, null));
  };
}

export function dispatchRiskArchive(data) {
  return dispatch => {
    dispatch(UpdateStoreRisk(data));
    dispatch(rc.PopulateRiskActivities(data.activityLog));
    dispatch(rc.PopulateRiskNotifications(data.notification)); //  need to check the notification paylod name
  };
}

export function bulkUpdateSelectedCustomField(obj, succ, fail) {
  return instance()
    .put(`api/risk/bulk`, obj)
    .then(response => {
      succ(response);
    })
    .catch(response => {
      fail(response.response);
    });
}

/// SIGNAL R customfield
export function UpdateRiskCustomFieldData(data) {
  return dispatch => {
    dispatch({
      type: constants.UPDATERISKCUSTOMFIELDDATA,
      payload: data,
    });
  };
}

export function getActivites(riskId, callback, failure) {
  return dispatch => {
    return instance()
      .get("api/issuerisk/GetRiskActivityLog?RiskId=" + riskId)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function getRisksAction(data) {
  return {
    type: constants.SAVE_ALL_RISKS,
    payload: data,
  };
}
export function getRisks(dispatchfn, callback = () => {}, failure = () => {}) {
  if (dispatchfn) {
    instance()
      .get("api/risk")
      .then(res => {
        dispatchfn(getRisksAction(res.data.entity));
        callback(res.data.entity);
      })
      .catch(res => {
        failure(res.response);
      });
  } else {
    return dispatch =>
      instance()
        .get("api/risk")
        .then(res => {
          dispatch(getRisksAction(res.data.entity));
          callback(res.data.entity);
        })
        .catch(res => {
          failure(res.response);
        });
  }
}
export function updateRiskFilterAction(data) {
  return {
    type: constants.UPDATE_RISK_FILTER,
    payload: data,
  };
}
export function updateRiskFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(updateRiskFilterAction(data));
  } else {
    return dispatch => {
      dispatch(updateRiskFilterAction(data));
    };
  }
}
export function clearRiskFilterAction(data) {
  return {
    type: constants.CLEAR_RISK_FILTER,
  };
}
export function clearRiskFilter(dispatchFn) {
  if (dispatchFn) {
    dispatchFn(clearRiskFilterAction());
  } else {
    return dispatch => {
      dispatch(clearRiskFilterAction());
    };
  }
}
export function addNewCustomFilterAction(data) {
  return {
    type: constants.ADD_NEW_CUSTOM_FILTER,
    payload: data,
  };
}
export function getCustomFilterAction(data) {
  return {
    type: constants.SET_RISK_CUSTOM_FILTER,
    payload: data,
  };
}

export function addNewFilter(data, type, dispatch, callback = () => {}) {
  instance()
    .post("api/advancefilter/" + type, data)
    .then(res => {
      callback();
      dispatch(addNewCustomFilterAction(res.data.entity));
    });
}

export function getSavedFilters(type, dispatch, callback = () => {}) {
  instance()
    .get("api/advancefilter/" + type)
    .then(res => {
      callback();
      dispatch(getCustomFilterAction(res.data.entity));
    });
}

export function updateRiskQuickFilterAction(data) {
  return {
    type: constants.UPDATE_RISK_QUICK_FILTER,
    payload: data,
  };
}

export function updateQuickFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(updateRiskQuickFilterAction(data));
  } else {
    return dispatch => {
      dispatch(updateRiskQuickFilterAction(data));
    };
  }
}
export function showAllRisksAction() {
  return {
    type: constants.SHOW_ALL_RISKS,
    payload: {},
  };
}
export function showAllRisks(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(showAllRisksAction(data));
  } else {
    return dispatch => {
      dispatch(showAllRisksAction(data));
    };
  }
}
export function deleteRiskFilterAction(data) {
  return {
    type: constants.DELETE_RISK_FILTER,
    payload: data,
  };
}
export function deleteRiskFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(deleteRiskFilterAction(data));
  } else {
    return dispatch => {
      dispatch(deleteRiskFilterAction(data));
    };
  }
}
export function bulkRiskAction(data) {
  return {
    type: constants.UPDATE_BULK_RISKS,
    payload: data,
  };
}
export function updateBulkRisk(data, dispatch, callback, failure) {
  const { keyType, ...rest } = data;
  instance()
    .put("api/risks/updatebulk", rest)
    .then(res => {
      dispatch(bulkRiskAction(res.data));
      callback(res.data);
    })
    .catch(() => {
      failure();
    });
}

export function exportBulkRisk(data, dispatch, callback, failure) {
  instance()
    .put("api/risks/exportbulk", data, {
      responseType: "blob",
    })
    .then(res => {
      callback(res);
    })
    .catch((res) => {
      failure(res);
    });
}

export function deleteBulkRiskAction(data) {
  return {
    type: constants.DELETE_BULK_RISKS,
    payload: data,
  };
}
export function deleteBulkRisk(data, dispatch, callback, failure) {
  instance()
    .put("api/risks/deletebulk", data)
    .then(res => {
      callback(res.data);
      dispatch(deleteBulkRiskAction({ riskIds: res.data.entity }));
    })
    .catch((res) => {
      failure(res);
    });
}
export function archiveBulkRisk(data, dispatch, callback, failure) {
  instance()
    .put("api/risks/archivebulk", data)
    .then(res => {
      let riskIds = res.data.map(i => i.id);
      callback(riskIds);
      dispatch(deleteBulkRiskAction({ riskIds }));
    })
    .catch(() => {
      failure();
    });
}
export function unArchiveBulkRisk(data, dispatch, callback, failure) {
  instance()
    .put("api/risks/unarchivebulk", data)
    .then(res => {
      let riskIds = res.data.map(i => i.id);
      callback(res.data);
      dispatch({
        type: constants.UNARCHIVE_BULK_RISK,
        payload: { riskIds },
      });
    })
    .catch(() => {
      failure();
    });
}

export function createRiskAction(data) {
  return {
    type: constants.CREATE_RISK,
    payload: data,
  };
}

export function editRiskAction(data) {
  return {
    type: constants.CREATED_RISK_EDIT,
    payload: data,
  };
}

export function createRisk(data, dispatchFn, dispatchObj, success, failure) {
  if (!dispatchFn) {
    return dispatch => {
      dispatch(createRiskAction(dispatchObj));
      instance()
        .post("/api/issuerisk/saverisk", data)
        .then(response => {
          if (window.userpilot) {
            /** tracking successfully created risk in app */
            window.userpilot.track("Risk Created Successfully");
          }
          dispatch(editRiskAction(response.data));
          success(response.data);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  } else {
    dispatchFn(createRiskAction(dispatchObj));
    instance()
      .post("/api/issuerisk/saverisk", data)
      .then(response => {
        if (window.userpilot) {
          /** tracking successfully created risk in app */
          window.userpilot.track("Risk Created Successfully");
        }
        dispatchFn(editRiskAction(response.data));
        success(response.data);
      })
      .catch(response => {
        failure(response.response);
      });
  }
}

export function updateRiskDataAction(data) {
  return {
    type: constants.UPDATE_RISK_DATA,
    payload: data,
  };
}
export function updateRiskData(
  data,
  dispatchFn,
  callback = () => {},
  error = () => {},
  notifyUser
) {
  if (dispatchFn) {
    instance()
      .patch(`/api/risk/${data.risk.id}`, data.obj)
      .then(res => {
        dispatchFn(updateRiskDataAction(res.data.entity));
        callback(res.data.entity);
      })
      .catch(err => {
        error(err.response);
        dispatchFn(updateRiskDataAction(data.risk));
      });
  } else {
    return dispatch => {
      instance()
        .patch(`/api/risk/${data.risk.id}`, data.obj)
        .then(res => {
          dispatch(updateRiskDataAction(res.data.entity));
          callback(res.data.entity);
        })
        .catch(err => {
          error(err.response);
          dispatch(updateRiskDataAction(data.risk));
        });
    };
  }
}

export function removeRiskQuickFilterAction(data) {
  return {
    type: constants.REMOVE_RISK_QUICK_FILTER,
    payload: data,
  };
}
export function removeRiskQuickFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(removeRiskQuickFilterAction(data));
  } else {
    return dispatch => {
      dispatch(removeRiskQuickFilterAction(data));
    };
  }
}

function getArchivedDataAction(data) {
  return {
    type: constants.ADD_BULK_RISKS,
    payload: data,
  };
}
export function getArchivedData(type, callback) {
  // Project = 1,
  //   Task = 2,
  //   Meeting = 3,
  //   Issue = 4,
  //   Risk = 5
  return dispatch => {
    instance()
      .get(`api/workspace/GetArchivedData?type=${type}`)
      .then(response => {
        dispatch(getArchivedDataAction(response.data));
        callback(response);
      })
      .catch(error => {
        callback(error.response);
      });
  };
}

export default {
  FetchRisksInfo,
  ResetRisksInfo,
  SaveRisk,
  UpdateRisk,
  BulkUpdateRisk,
  ArchiveRisk,
  UnarchiveRisk,
  dispatchRiskArchive,
  updateRiskData,
};
