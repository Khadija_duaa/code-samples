import constants from "./../constants/types";
import instance from "../instance";
import axios from "axios";

export function ShowHideNoInternetView(value) {
  return {
    type: constants.UPDATENOINTERNETVIEW,
    payload: value,
  };
}

export function noInternetAction(callback, failure) {
  return (dispatch) => {
    return axios
      .get("https://content.googleapis.com/discovery/v1/apis/analytics/v3/rest")
      .then((response) => {
        window.location.reload(false);
      })
      .catch((response) => {
        failure(response);
      });
  };
}
