import constants from "./../constants/types";
import instance from "../instance";
import order from "./itemOrder";
import cloneDeep from "lodash/cloneDeep";
import { createTaskAction, editTaskAction, getTasksAction } from "./tasks";

export function PopulateDefaultProjectTemplate(data) {
  return {
    type: constants.PROJECTDEFAULTTEMPLATE,
    payload: {
      data,
    },
  };
}
export function PopulateWSProjectDefaultTemplate(data) {
  return {
    type: constants.WORKSPACEANDPROJECTDEFAULTTEMPLATE,
    payload: {
      data,
    },
  };
}

export function PopulateProjectsInfo(data) {
  //Populate
  return {
    type: constants.POPULATEPROJECTINFO,
    payload: {
      data,
    },
  };
}
export function dispatchProjects(data) {
  return dispatch => {
    dispatch({
      type: constants.POPULATEPROJECTINFO,
      payload: {
        data,
      },
    });
  };
}
export function DeleteProjectFromStore(data) {
  return {
    type: constants.DELETEPROJECT,
    payload: {
      data,
    },
  };
}

export function EditProjectFromStore(data) {
  return {
    type: constants.EDITPROJECTNAME,
    payload: {
      data,
    },
  };
}

export function ResetProjectsInfo() {
  return {
    type: constants.RESETPROJECTINFO,
  };
}

export function UpdateProjectColorToStore(data) {
  return {
    type: constants.CHANGEPROJECTCOLOR,
    payload: {
      data,
    },
  };
}
export function UpdateProjectPayload(data) {
  return {
    type: constants.UPDATEPROJECT,
    payload: {
      data,
    },
  };
}

export function UpdateProjectPartially(data) {
  return {
    type: constants.UPDATEPROJECTPARTIALLY,
    payload: {
      data,
    },
  };
}

export function addProjectToStore(data) {
  return {
    type: constants.ADDPROJECTTOSTORE,
    payload: {
      data,
    },
  };
}
export function updateProjectViewDate(data) {
  return {
    type: constants.UPDATEPROJECTVIEWDATE,
    payload: data,
  };
}

export function toggleProjectInStore(data) {
  return {
    type: constants.TOGGLEPROJECT,
    payload: {
      data,
    },
  };
}

export function UpdateDeletedBulkStoreProjects(data) {
  /* updated delete bulk store projects */
  return {
    type: constants.UPDATEDELETEDBULKPROJECTINFO,
    payload: {
      data,
    },
  };
}

export function bulkProjectAction(data) {
  return {
    type: constants.UPDATE_BULK_PROJECT,
    payload: data,
  };
}

export function deleteBulkProjectAction(data) {
  return {
    type: constants.DELETE_BULK_PROJECT,
    payload: data,
  };
}

export function UpdateBulkStoreProjects(data) {
  return {
    type: constants.UPDATEBULKPROJECTINFO,
    payload: {
      data,
    },
  };
}
export function getProjectsAction(data) {
  return {
    type: constants.SAVE_ALL_PROJECTS,
    payload: data,
  };
}
function getArchivedDataAction(data) {
  return {
    type: constants.ADD_BULK_PROJECTS,
    payload: data,
  };
}
export function getArchivedData(type, callback, dispatchFn) {
  // Project = 1,
  //   Task = 2,
  //   Meeting = 3,
  //   Issue = 4,
  //   Risk = 5
  if (dispatchFn) {
    instance()
      .get(`api/workspace/GetArchivedData?type=${type}`)
      .then(response => {
        dispatchFn(getArchivedDataAction(response.data));
        callback();
      })
      .catch(error => {
        callback(error.response);
      });

  } else {

    return dispatch =>
      instance()
        .get(`api/workspace/GetArchivedData?type=${type}`)
        .then(response => {
          dispatch(getArchivedDataAction(response.data));
          callback();
        })
        .catch(error => {
          callback(error.response);
        });
  }
}
export function getProjects(param, dispatchfn, callback = () => { }, failure = () => { }) {
  if (dispatchfn) {
    instance()
      .get("api/projects")
      .then(res => {
        dispatchfn(getProjectsAction(res.data.entity));
        callback(res.data.entity);
      }).catch((res) => {
        if (res.response.status == '405') {
          dispatchfn(getProjectsAction([]));
          failure()
        }
      });
  } else {
    return dispatch =>
      instance()
        .get("api/projects")
        .then(res => {
          dispatch(getProjectsAction(res.data.entity));
          callback();
        }).catch((res) => {
          if (res.response.status == '405') {
            dispatch(getProjectsAction([]));
          }
        });
  }
}

export function ImportMsFile(formdata, callback = () => { }, failure = () => { }) {
  return instance('https://rapi.ntaskmanager.com')
    .post("gantt", formdata)
    .then(response => {
      callback(response);
    }).catch((err) => {
      failure(err);
    })
}
export function ImportMsProject(formdata, callback = () => { }, failure = () => { }) {
  return instance()
    .post("/api/DocsFileUploader/ImportMSProjectAspose", formdata)
    .then(response => {
      callback(response);
    }).catch((err) => {
      failure(err);
    })
}

export function FetchProjectsInfo(callback) {
  return dispatch => {
    return instance()
      .get("/api/project/getprojects")
      .then(response => {
        return dispatch(PopulateProjectsInfo(response.data));
      })
      .then(() => {
        callback();
      });
    return null;
  };
}

export function SaveProject(data, callback) {
  let flag = false;
  if (data.projectId) {
    flag = true;
  } else {
    flag = false;
  }
  let ApiData = cloneDeep(data);
  ApiData.description =
    ApiData.description && ApiData.description !== ""
      ? window.btoa(unescape(encodeURIComponent(ApiData.description)))
      : ApiData.description;
  ApiData.activityLog = [];
  ApiData.CalenderDetails = [];
  return dispatch => {
    return instance()
      .post("/api/project/saveproject", ApiData)
      .then(response => {
        if (flag) dispatch(UpdateProjectPayload(data));
        else dispatch(addProjectToStore(response.data));
        dispatch(order.FetchItemOrderInfo(() => { }));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => {
        callback(response.response);
      });
    return null;
  };
}
export function SaveProjectAsTemplate(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/project/saveprojectastemplate", data)
      .then(response => {
        callback(response.data);
      })
      .catch(response => {
        failure(response);
      });
  };
}
export function dispatchNewProject(data) {
  return dispatch => {
    dispatch(addProjectToStore(data));
  };
}
export function dispatchProjectViewDate(data) {
  return dispatch => {
    dispatch(updateProjectViewDate(data));
  };
}
export function dispatchRenameProject(data) {
  return dispatch => {
    dispatch(UpdateProjectPayload(data));
  };
}

export function dispactchProjectPartially(data) {
  return dispatch => {
    dispatch(UpdateProjectPartially(data));
  };
}

export function dispatchArchiveProject(data) {
  return dispatch => {
    dispatch(DeleteProjectFromStore(data));
  };
}
export function markProjectCompleted(project, callback, dispatchFn = null) {
  if (dispatchFn) {
    return instance()
      .get(`/api/project/MarkProjectCompleted?projectId=${project.projectId}`)
      .then(response => {
        dispatchFn({ type: constants.UPDATEBULKTASKSTATUS, payload: { project, response } });
        callback(response);
      });
  } else {
    return dispatch => {
      return instance()
        .get(`/api/project/MarkProjectCompleted?projectId=${project.projectId}`)
        .then(response => {
          dispatch({ type: constants.UPDATEBULKTASKSTATUS, payload: { project, response } });
          callback(response);
        });
    };
  }
}
export function UpdateProject(data, dispatchFn = null) {
  if (dispatchFn) {
    return instance()
      .post("/api/project/MarkProjectAsStarred", data)
      .then(response => {
        dispatchFn(UpdateProjectPayload(response.data));
      });
  } else {
    return dispatch => {
      return instance()
        .post("/api/project/MarkProjectAsStarred", data)
        .then(response => {
          dispatch(UpdateProjectPayload(response.data));
        });
    };
  }
}
//GET /api/Project/DeleteProject
//GET /api/Project/CopyProject
// https://app.ntaskmanager.com/api/project/editproject?projectId=f78147edcd4544989ff7b3276c6c0bf3&editProjectName=a33
// GET /api/Project/ArchiveProject
// api/project/UnarchiveProject
//api/project/UpdateBulkProjects

/////////////////////// Newwwww //////////////////////////////
export function updateBulkProject(data, dispatch, callback, failure) {
  const { type, ...rest } = data;
  instance()
    .put("/api/project/UpdateBulkProjects", rest)
    .then(res => {
      dispatch(bulkProjectAction(res.data.entity));
      callback(res.data.entity);
    })
    .catch(() => {
      failure();
    });
}

export function deleteBulkProject(data, dispatch, callback, failure) {
  instance()
    .post("/api/project/bulkdelete", data)
    .then(res => {
      callback(res.data);
      dispatch(deleteBulkProjectAction({ projectIds: res.data }));
    })
    .catch(() => {
      failure();
    });
}

export function archiveBulkProject(data, dispatch, callback, failure) {
  instance()
    .put("/api/project/ArchiveBulkProjects", data)
    .then(res => {
      let projectIds = res.data.entity;
      dispatch(deleteBulkProjectAction({ projectIds }));
      callback(projectIds);
    })
    .catch(() => {
      failure();
    });
}

export function unArchiveBulkProject(data, dispatch, callback, failure) {
  instance()
    .put("/api/project/UnArchiveBulkProjects", data)
    .then(res => {
      let projectIds = res.data.entity.map(item => item.projectId);
      dispatch({
        type: constants.UNARCHIVE_BULK_PROJECT,
        payload: { projectIds },
      });
      return res;
    })
    .then(res => {
      callback(res.data.entity);
    })
    .catch((err) => {
      failure(err);
    });
}

export function updateProjectDataAction(data) {
  return {
    type: constants.UPDATE_PROJECT_DATA,
    payload: data,
  };
}

export function updateProjectData(
  data,
  dispatchFn,
  callback = () => { },
  error = () => { },
  notifyUser
) {
  if (dispatchFn) {
    instance()
      .patch(`/api/projects/${data.project.projectId}`, data.obj)
      .then(res => {
        dispatchFn(updateProjectDataAction(res.data));
        callback(res.data);
      })
      .catch(err => {
        error(err.response);
        dispatchFn(updateProjectDataAction(data.project));
      });
  } else {
    return dispatch => {
      instance()
        .patch(`/api/projects/${data.project.projectId}`, data.obj)
        .then(res => {
          dispatch(updateProjectDataAction(res.data.entity));
          callback(res.data.entity);
        })
        .catch(err => {
          error(err.response);
          dispatch(updateProjectDataAction(data.project));
        });
    };
  }
}

///////////////////////////////////////////////////////
export function BulkDeleteProject(data, callback) {
  /** Deleting bulk action creator */
  return dispatch => {
    return instance()
      .post("api/project/UpdateBulkProjects", data)
      .then(response => {
        /** response contains the delete projectsId  */
        // if result is empty no updates done
        if (response.data && response.data.length)
          dispatch(UpdateDeletedBulkStoreProjects(response.data));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => callback(response.response));
    return null;
  };
}

export function exportBulkProject(data, dispatch, callback, failure) {
  instance()
    .post("api/export/bulkproject", data, {
      responseType: "blob",
    })
    .then(res => {
      // dispatch(updateTaskDataAction(res.data.entity));
      callback(res);
    })
    .catch((res) => {
      failure(res);
      // dispatch(updateTaskDataAction(data.task));
    });
}
export function BulkUpdateProject(data, callback) {
  return dispatch => {
    return instance()
      .post("api/project/UpdateBulkProjects", data)
      .then(response => {
        // if result is empty no updates done
        if (response.data && response.data.length) dispatch(UpdateBulkStoreProjects(response.data));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => callback(response.response));
    return null;
  };
}

export function moveProjectToWorkspace(data, sucess, failure, dispatchFn) {
  if (dispatchFn) {
    instance()
      .post("api/project/MoveProjectToWorkspace", data)
      .then(response => {
        sucess(response);
        dispatchFn(DeleteProjectFromStore(data.projectId));
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .post("api/project/MoveProjectToWorkspace", data)
        .then(response => {
          sucess(response);
          dispatch(DeleteProjectFromStore(data.projectId));
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}

export function UnarchiveProject(data, callback = () => { }, failure = () => { }, dispatchFn = null) {
  if (dispatchFn) {
    instance()
      .get("api/project/UnarchiveProject?projectId=" + data.projectId)
      .then(response => {
        dispatchFn(updateProjectDataAction(response.data));
        return { response: response };
      })
      .then(_data => {
        callback(_data);
      })
      .catch(response => {
        error(response.response);
      });
    return null;
  } else {
    return dispatch => {
      return instance()
        .get("api/project/UnarchiveProject?projectId=" + data)
        .then(response => {
          //dispatch(updateProjectDataAction(ProjectID));
          return { response: response };
        })
        .then(_data => {
          callback(_data.response);
        })
        .catch(response => {
          error(response.response);
        });
      return null;
    };
  }
}

export function ArchiveProject(ProjectID, callback, fail, dispatchFn) {
  if (dispatchFn) {
    instance()
      .get("/api/Project/ArchiveProject?projectId=" + ProjectID)
      .then(response => {
        dispatchFn(DeleteProjectFromStore(ProjectID));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => {
        fail(response.response);
      });
    return null;
  } else {
    return dispatch => {
      instance()
        .get("/api/Project/ArchiveProject?projectId=" + ProjectID)
        .then(response => {
          dispatch(DeleteProjectFromStore(ProjectID));
          return { response: response };
        })
        .then(_data => {
          callback(_data.response);
        })
        .catch(response => {
          callback(response.response);
        });
      return null;
    };
  }
}

export function DeleteProject(data, callback, failure, dispatchFn) {
  if (dispatchFn) {
    instance()
      .get("api/Project/DeleteProject?projectId=" + data.projectId)
      .then(response => {
        dispatchFn(DeleteProjectFromStore(data.projectId));
        callback(data, response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      instance()
        .get("api/Project/DeleteProject?projectId=" + data.projectId)
        .then(response => {
          dispatch(DeleteProjectFromStore(data.projectId));
          callback(data, response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}
export function dispatchProjectDelete(ProjectID) {
  return dispatch => {
    dispatch(DeleteProjectFromStore(ProjectID));
  };
}

export function dispatchProjectCopy(data) {
  return dispatch => {
    dispatch(addProjectToStore(data));
  };
}

export function CopyProject(data, callback, error, dispatchFn = null) {
  if (dispatchFn) {
    instance()
      .post("/api/Project/CreateCopyProject", data)
      .then(response => {
        dispatchFn(addProjectToStore(response.data));
        callback(response);
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => {
        error(response.response);
      });
  } else {
    return dispatch => {
      instance()
        .post("/api/Project/CreateCopyProject", data)
        .then(response => {
          dispatch(addProjectToStore(response.data));
          return { response: response };
        })
        .then(_data => {
          callback(_data.response);
        })
        .catch(response => {
          callback(response.response);
        });
    };
  }
}
// export function CopyProject(dispatchFn, projectId, obj, success = () => { }, failure = () => { }) {
//   if (dispatchFn) {
//     dispatchFn(createProjectAction(obj));
//     instance()
//       .post("/api/Project/CreateCopyProject", projectId)
//       .then(response => {
//         dispatch(addProjectToStore(response.data));
//         success(response.response);
//       })
//       .catch(response => {
//         failure(response);
//       });
//   } else {
//     return dispatch => {
//       dispatch(createProjectAction(obj));
//       instance()
//         .post("/api/Project/CreateCopyProject", projectId)
//         .then(response => {
//           dispatch(addProjectToStore(response.data));
//           success(response.response);
//         })
//         .catch(response => {
//           failure(response.response);
//         });
//     };
//   }
// }

// export function CopyProject(dispatchFn, taskId, obj, success = () => { }, failure = () => { }) {
//   return dispatch => {
//     return (
//       instance()
//         .post("/api/Project/CreateCopyProject", data)
//         // .get(
//         //   "/api/Project/CopyProject?projectId=" +
//         //   data.ProjectID +
//         //   "&copiedProjectName=" +
//         //   data.CopiedProjectName +
//         //   "&appendTask=" +
//         //   data.AppendTask
//         // )
//         .then(response => {
//           dispatch(addProjectToStore(response.data));
//           return { response: response };
//         })
//         .then(_data => {
//           callback(_data.response);
//         })
//         .catch(response => {
//           callback(response.response);
//         })
//     );
//     return null;
//   };
// }
export function getProjectSetting(projectId, sucess, failure) {
  return dispatch => {
    return instance()
      .get(`/api/project/getprojectsetting?projectId=${projectId}`)
      .then(response => {
        sucess(response.data);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function saveProjectSetting(data, success, failure) {
  let ApiData = cloneDeep(data);
  ApiData.description =
    ApiData.description && ApiData.description !== ""
      ? window.btoa(unescape(encodeURIComponent(ApiData.description)))
      : ApiData.description;
  ApiData.activityLog = [];
  ApiData.CalenderDetails = [];
  return dispatch => {
    return instance()
      .post("api/project/saveprojectsetting", ApiData)
      .then(response => {
        success(response.data);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function EditProject(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/project/editproject", data)
      .then(response => {
        dispatch(UpdateProjectPayload(response.data));
        callback(response);
      })
      .catch(response => {
        callback(response.response);
        if (failure) failure(response.response);
      });
  };
}
export function dispatchProject(data, dispatchfn) {
  if (dispatchfn) {
    dispatchfn(UpdateProjectPayload(data));
  } else {
    return dispatch => {
      dispatch(UpdateProjectPayload(data));
    };
  }
}
export function saveSortingProjectList(obj, success, failure) {
  return dispatch => {
    return instance()
      .post("api/team/SaveSortColumn", obj)
      .then(response => {
        dispatch(UpdateSortingOrder(response.data));
        success(response.data);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}
export function UpdateSortingOrder(data) {
  /*  */
  return {
    type: constants.UPDATESORTINGORDER,
    payload: {
      data,
    },
  };
}
export function createProjectAction(data) {
  return {
    type: constants.CREATE_PROJECT,
    payload: data,
  };
}
export function editProjectAction(data) {
  return {
    type: constants.CREATED_PROJECT_EDIT,
    payload: data,
  };
}
export function createProject(data, dispatch, dispatchObj, success, failure) {
  dispatch(createProjectAction(dispatchObj));
  if (data.description) {
    data.description = window.btoa(unescape(encodeURIComponent(data.description)));
  }
  instance()
    .post("api/project/SaveProject", data)
    .then(response => {
      if (window.userpilot) {
        /** tracking successfully created task in app */
        window.userpilot.track("Project Created Successfully");
      }
      dispatch(editProjectAction(response.data));
      success(response.data);
    })
    .catch(response => {
      failure(response);
    });
}
export function saveNewProject(data, success, failure, projectSettings = {}) {
  let flag = false;
  if (data.projectId) {
    flag = true;
  } else {
    flag = false;
  }
  let ApiData = cloneDeep(data);
  ApiData.description =
    ApiData.description && ApiData.description !== ""
      ? window.btoa(unescape(encodeURIComponent(ApiData.description)))
      : ApiData.description;
  ApiData.activityLog = [];
  ApiData.CalenderDetails = [];
  return dispatch => {

    // old code with save project api 
    return instance()
      .post("api/project/SaveProject", ApiData)
      .then(response => {
        if (flag) {
          let obj = {
            projectDetails: response.data,
            projectSettings: projectSettings,
          };
          dispatch(UpdateProjectDetailDialog(obj));
          dispatch(UpdateProjectPayload(response.data));
        } else {
          dispatch(addProjectToStore(response.data));
        }
        dispatch(order.FetchItemOrderInfo(() => { }));

        return { response: response };
      })
      .then(res => {
        success(res.response);
      })
      .catch(res => {
        failure(res.response);
      });

    // now working with path api

    // return instance()
    //   .patch(`/api/projects/${ApiData.projectId}`, ApiData)
    //   .then(response => {
    //     if (flag) {
    //       let obj = {
    //         projectDetails: response.data,
    //         projectSettings: projectSettings,
    //       };
    //       dispatch(UpdateProjectDetailDialog(obj));
    //       // dispatch(UpdateProjectPayload(response.data));
    //       dispatch(updateProjectDataAction(response.data));
    //     } else {
    //       dispatch(addProjectToStore(response.data));
    //     }
    //     dispatch(order.FetchItemOrderInfo(() => { }));

    //     return { response: response };
    //   })
    //   .then(res => {
    //     success(res.response);
    //   })
    //   .catch(res => {
    //     failure(res.response);
    //   });
  };
}
export function updateProject(data, success, failure) {
  let ApiData = cloneDeep(data);
  ApiData.description =
    ApiData.description && ApiData.description !== ""
      ? window.btoa(unescape(encodeURIComponent(ApiData.description)))
      : ApiData.description;
  ApiData.activityLog = [];
  ApiData.CalenderDetails = [];
  return dispatch => {
    return instance()
      .post("api/project/SaveProject", ApiData)
      .then(response => {
        dispatch(UpdateProjectPayload(response.data));
        dispatch(order.FetchItemOrderInfo(() => { }));
        return { response: response };
      })
      .then(res => {
        success(res.response);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}

export function UpdateProjectDetailDialog(data) {
  return {
    type: constants.PROJECTDETAILSDIALOG,
    payload: {
      data,
    },
  };
}
export function UpdateProjectManagersInProjectStore(data) {
  return {
    type: constants.UPDATEPROJECTMANAGERLIST,
    payload: data,
  };
}

export function toggleProjectViewAction(data) {
  return {
    type: constants.TOGGLEPROJECTVIEW,
    payload: {
      data,
    },
  };
}
export function updateStateForLoadingProjectDetails(bool) {
  return {
    type: constants.GETSETPROJECTLOADINGSTATE,
    payload: bool,
  };
}

export function getProjectSettingById(projectId, sucess, failure, projectData = {}) {
  return dispatch => {
    return instance()
      .get(`/api/project/getprojectsetting?projectId=${projectId}`)
      .then(res => {
        if (res.data) {
          let data = {
            projectDetails: projectData.data,
            projectSettings: res.data,
            docked: projectData.docked,
            fullView: projectData.fullView,
          };
          dispatch(UpdateProjectDetailDialog(data));
        }
        return { response: res };
      })
      .then(res => {
        sucess(res.response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function getSetLoadingProjectDetails(bool) {
  return dispatch => {
    dispatch(updateStateForLoadingProjectDetails(bool));
  };
}
export function projectDetails(data, dispatchFn = null) {
  if (dispatchFn) {
    dispatchFn(UpdateProjectDetailDialog(data));
  } else {
    return dispatch => {
      dispatch(UpdateProjectDetailDialog(data));
    };
  }
}
export function toggleProjectView(data) {
  return dispatch => {
    dispatch(toggleProjectViewAction(data));
  };
}

export function UpdateProjectDetailListing(dispatchFn, data, id) {
  if (dispatchFn) {
    dispatchFn({
      type: constants.UPDATEPROJECTDETAILSLISTING,
      payload: {
        data,
        id
      },
    })
  } else {
    return dispatch => {
      dispatch({
        type: constants.UPDATEPROJECTDETAILSLISTING,
        payload: {
          data,
          id
        },
      })
    }
  }
}
export function saveProjectSettings(data, sucess, failure, selectedProject) {
  let ApiData = cloneDeep(data);
  ApiData.description =
    ApiData.description && ApiData.description !== ""
      ? window.btoa(unescape(encodeURIComponent(ApiData.description)))
      : ApiData.description;
  return dispatch => {
    return instance()
      .post("api/project/SaveProjectSetting", ApiData)
      .then(res => {
        if (res.data) {
          let data = {
            projectDetails: selectedProject,
            projectSettings: res.data,
          };
          dispatch(UpdateProjectDetailDialog(data));
          dispatch(UpdateProjectManagersInProjectStore(res.data));
          // dispatch(UpdateProjectDetailListing(res.data))
        }
        return { response: res };
      })
      .then(res => {
        sucess(res.response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function saveSplitProjectSettings(data, sucess, failure, selectedProject) {
  return dispatch => {
    if (data) {
      let resData = {
        projectDetails: selectedProject,
        projectSettings: data,
      };
      dispatch(UpdateProjectDetailDialog(resData));
    }
    return instance()
      .post("api/project/SaveProjectSetting", data)
      .then(res => {
        return { response: res };
      })
      .then(res => {
        sucess(res.response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function saveBillableSetting(data, updatedObj, sucess, failure, selectedProject) {
  return dispatch => {
    if (updatedObj) {
      let resData = {
        projectDetails: selectedProject,
        projectSettings: updatedObj,
      };
      dispatch(UpdateProjectDetailDialog(resData));
    }
    return instance()
      .put("api/project/taskbillablestatus", data)
      .then(res => {
        return { response: res };
      })
      .then(res => {
        sucess(res.response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function saveBulkTasksInProject(data, sucess, failure, projectData) {
  return dispatch => {
    return instance()
      .post("api/project/SaveProjectTasks", data)
      .then(res => {
        if (res.data) {
          let data = {
            projectDetails: projectData.data,
            projectSettings: res.data,
            docked: projectData.docked,
            fullView: projectData.fullView,
          };
          dispatch(UpdateProjectDetailDialog(data));
        }
        return { response: res };
      })
      .then(res => {
        sucess(res.response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function saveMilestone(data, success, failure, selectedProject) {
  return dispatch => {
    return instance()
      .post("api/project/SaveProjectMilestones", data)
      .then(res => {
        if (res.data) {
          let data = {
            projectDetails: selectedProject,
            projectSettings: res.data,
          };
          dispatch(UpdateProjectDetailDialog(data));
        }
        return { response: res };
      })
      .then(res => {
        success(res.response);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}

export function checkDescriptionStatus(id, success, failure) {
  return dispatch => {
    return instance()
      .get(`api/project/ProjectDescriptionStatus?projectId=${id}`)
      .then(res => {
        success(res.data);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}
`${ENV == "production1" || ENV == "beta1" ? BASE_URL_API : BASE_URL
  }/api/docsfileuploader/UploadMultipleDocsFileAmazonS3`;
export function attachMultipleFile(_FormData, success, failure, options) {
  return dispatch => {
    return instance()
      .post("api/docsfileuploader/UploadMultipleDocsFileAmazonS3", _FormData, options)
      .then(res => {
        success(res.data);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}
export function attachFile(_FormData, success, failure, options) {
  return dispatch => {
    return instance()
      .post("api/docsfileuploader/uploaddocsfileamazons3", _FormData, options)
      .then(res => {
        success(res.data);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}
export function uploadFileToS3(_FormData, dispatch, success = () => { }, failure = () => { }) {
  instance()
    .post("api/docsfileuploader/UploadMultipleDocsFileAmazonS3", _FormData)
    .then(res => {
      success(res.data);
    })
    .catch(res => {
      failure(res.response);
    });
}
export function attachFeatureImg(_FormData, success, failure) {
  return dispatch => {
    return instance()
      .post("api/docsfileuploader/UploadMultipleDocsFileAmazonS3", _FormData)
      .then(res => {
        success(res.data);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}
export function saveAttachments(data, success, failure) {
  return dispatch => {
    return instance()
      .post("api/communication/SaveAttachment", data)
      .then(res => {
        success(res.data);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}
export function saveMultipleAttachment(data, success, failure) {
  return dispatch => {
    return instance()
      .post("api/communication/SaveAttachment", data)
      .then(res => {
        success(res.data);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}
export function saveUserComment(data, success, failure) {
  return dispatch => {
    return instance()
      .post("api/UserTask/SaveUserComment", data)
      .then(res => {
        success(res.data);
      })
      .catch(res => {
        failure(res.response);
      });
  };
}
export function deleteAttachment(id, callback, failure) {
  return dispatch => {
    return instance()
      .get("api/UserTask/DeleteTaskAttachment?id=" + id)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function deleteAttachmentFile(id, callback, failure) {
  return dispatch => {
    return instance()
      .get("api/communication/DeleteUserAttachment?id=" + id)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function projectActivityLogs(id, success, failure) {
  return dispatch => {
    return instance()
      .get("api/project/GetProjectActivityLog?projectId=" + id)
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function updateProjectCf(obj, callback, failure, dispatchfn = null) {
  if (dispatchfn) {
    return instance()
      .put(`api/projectcf/id=${obj.groupId}`, [obj])
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .put(`api/projectcf/id=${obj.groupId}`, [obj])
        .then(response => {
          callback(response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}

/// SIGNAL R customfield
export function UpdateProjectCustomFieldData(data) {
  return dispatch => {
    dispatch({
      type: constants.UPDATEPROJECTCUSTOMFIELDDATA,
      payload: data,
    });
  };
}

// filter action starts here

export function updateProjectQuickFilterAction(data) {
  return {
    type: constants.UPDATE_PROJECT_QUICK_FILTER,
    payload: data,
  };
}

export function updateProjectFilterAction(data) {
  return {
    type: constants.UPDATE_PROJECT_FILTER,
    payload: data,
  };
}

export function clearProjectFilterAction(data) {
  return {
    type: constants.CLEAR_PROJECT_FILTER,
  };
}

export function deleteProjectFilterAction(data) {
  return {
    type: constants.DELETE_PROJECT_FILTER,
    payload: data,
  };
}

export function getCustomFilterAction(data) {
  return {
    type: constants.SET_PROJECT_CUSTOM_FILTER,
    payload: data,
  };
}

export function addNewCustomFilterAction(data) {
  return {
    type: constants.ADD_NEW_PROJECT_CUSTOM_FILTER,
    payload: data,
  };
}
// filter functions

export function removeProjectQuickFilterAction(data) {
  return {
    type: constants.REMOVE_PROJECT_QUICK_FILTER,
    payload: data,
  };
}

export function removeProjectQuickFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(removeProjectQuickFilterAction(data));
  } else {
    return dispatch => {
      dispatch(removeProjectQuickFilterAction(data));
    };
  }
}

export function updateQuickFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(updateProjectQuickFilterAction(data));
  } else {
    return dispatch => {
      dispatch(updateProjectQuickFilterAction(data));
    };
  }
}

export function updateProjectFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(updateProjectFilterAction(data));
  } else {
    return dispatch => {
      dispatch(updateProjectFilterAction(data));
    };
  }
}

export function clearProjectFilter(dispatchFn) {
  if (dispatchFn) {
    dispatchFn(clearProjectFilterAction());
  } else {
    return dispatch => {
      dispatch(clearProjectFilterAction());
    };
  }
}

export function deleteProjectFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(deleteProjectFilterAction(data));
  } else {
    return dispatch => {
      dispatch(deleteProjectFilterAction(data));
    };
  }
}

export function getSavedFilters(type, dispatch, callback = () => { }) {
  instance()
    .get("api/advancefilter/" + type)
    .then(res => {
      callback();
      dispatch(getCustomFilterAction(res.data.entity));
    });
}

export function addNewFilter(data, type, dispatch, callback = () => { }) {
  instance()
    .post("api/advancefilter/" + type, data)
    .then(res => {
      callback();
      dispatch(addNewCustomFilterAction(res.data.entity));
    });
}

export default {
  FetchProjectsInfo,
  ResetProjectsInfo,
  SaveProject,
  DeleteProject,
  CopyProject,
  EditProject,
  ArchiveProject,
  UnarchiveProject,
  dispatchProjectCopy,
  BulkDeleteProject,
  saveSortingProjectList,
  saveNewProject,
  getProjectSettingById,
  projectDetails,
  saveProjectSettings,
  UpdateProjectDetailListing,
  saveMilestone,
  checkDescriptionStatus,
  attachFile,
  saveAttachments,
  saveUserComment,
  deleteAttachment,
  saveBulkTasksInProject,
  getSetLoadingProjectDetails,
  updateProject,
  attachFeatureImg,
  PopulateDefaultProjectTemplate,
  PopulateWSProjectDefaultTemplate,
  projectActivityLogs,
  deleteAttachmentFile,
  updateProjectCf,
  updateProjectFilter,
  clearProjectFilter,
  deleteProjectFilter,
  getSavedFilters,
  addNewFilter,
  // bulk action
  updateBulkProject,
};
