import constants from "./../constants/types";

//Actions
export const notificationBarStateAction = data => ({ // Tell app to push down to create space for the notification bar
  type: constants.SETNOTIFICATIONBARSTATE,
  payload: {
    data
  }
});
export const showHideNotificationBarAction = data => ({ // Action to trigger notification bar show hide
  type: constants.SHOWHIDENOTIFICATIONBAR,
  payload: {
    data
  }
});

//Action Creators
export function notificationBarState(data) {
  return dispatch => {
    return dispatch(notificationBarStateAction(data));
  };
}
export function showHideNotificationBar(data) {
  return dispatch => {
  
      dispatch(showHideNotificationBarAction(data));
      dispatch(notificationBarStateAction(data.show))
    
  };
}
