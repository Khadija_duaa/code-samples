
import constants from "./../constants/types";
import instance from "./../instance";

export function updateReportingFilterAction(data, key) {
    return {
        type: constants.UPDATE_REPORTING_FILTER,
        payload: data, key,
    };
}
export function clearReportingFilterAction(data) {
    return {
        type: constants.CLEAR_REPORTING_FILTER,
        payload: data,
    };
}

export function deleteReportingFilterAction(data, key) {
    return {
        type: constants.DELETE_REPORTING_FILTER,
        payload: data, key,
    };
}
export function deleteAllReportingFilterAction(data) {
    return {
        type: constants.DELETE_ALL_REPORTING_FILTER,
        payload: data,
    };
}

export function updateReportingFilter(data, key, dispatchFn) {
    if (dispatchFn) {
        dispatchFn(updateReportingFilterAction(data, key));
    } else {
        return dispatch => {
            dispatch(updateReportingFilterAction(data, key));
        };
    }
}

export function clearReportingFilter(key, dispatchFn) {
    if (dispatchFn) {
        dispatchFn(clearReportingFilterAction(key));
    } else {
        return dispatch => {
            dispatch(clearReportingFilterAction(key));
        };
    }
}
export function deleteReportingFilter(data, key, dispatchFn) {
    if (dispatchFn) {
        dispatchFn(deleteReportingFilterAction(data, key));
    } else {
        return dispatch => {
            dispatch(deleteReportingFilterAction(data, key));
        };
    }
}
export function deleteAllReportingFilter(feature, dispatchFn) {
    if (dispatchFn) {
        dispatchFn(deleteAllReportingFilterAction(feature));
    } else {
        return dispatch => {
            dispatch(deleteAllReportingFilterAction(feature));
        };
    }
}
export function exportBulkItems(data, apiKey, callback, fail, dispatch) {
    instance()
        .post(`api/${apiKey}`, data, {
            responseType: "blob",
        })
        .then(res => {
            callback(res);
        })
        .catch((res) => {
            fail(res.response)
        });
}

export default {
    updateReportingFilter,
    deleteReportingFilter
};
