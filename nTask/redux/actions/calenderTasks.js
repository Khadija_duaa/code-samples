import constants from "./../constants/types";
import instance from "../instance";




export function DeleteCalenderTask(data) {
  return {
    type: constants.DELETECALENDERTASK,
    payload: {
      data
    }
  };
}

export function CopyCalenderTask(data) {
  let obj = {
    id: data.taskId,
    title: data.taskTitle,
    start: new Date(data.startDateString),
    end: new Date(data.dueDateString),
    desc: data.description,
    taskColor: data.colorCode //colorCode
  };
  return {
    type: constants.COPYCALENDERTASK,
    payload: obj
  };
}

export function UpdateCalenderTask(data) {
  let currentDate = new Date();
  let dueDate = new Date(data.dueDate);
  //
  data = data.CalenderDetails == undefined? data: data.CalenderDetails;
  return {
    type: constants.UPDATECALENDERTASKS,
    payload: data
  };
}
export function UpdateCalenderTaskInfo(data) {
  //
  //   let obj = {
  //     taskid: data.id,
  //     taskTitle: data.title,
  //     createdDate: start.toISOString(),
  //     dueDate: end.toISOString(),
  //     description: data.dec,
  //     colorCode: data.taskColor //colorCode
  //   };
  //   // 
  //   return dispatch => {
  //     return instance()
  //       .post("/api/UserTask/UpdateUserTask", obj)
  //       .then(response => {
  //         let obj = {
  //           id: response.data.taskId,
  //           title: response.data.taskTitle,
  //           start: new Date(response.data.createdDate),
  //           //start: new Date(2018, 9, 28),
  //           end: new Date(response.data.dueDate),
  //           //end: new Date(2018, 9, 30),
  //           desc: response.data.description,
  //           taskColor: response.data.colorCode //colorCode
  //         };
  //         dispatch(UpdateCalenderTask(obj));
  //         dispatch(UpdateTasks(response.data));
  //         // 
  //         // return null;
  //       });
  //   };
}
export function ResetTasksInfo() {
  return {
    type: constants.RESETCALENDERTASKS
  };
}

export default {
  UpdateCalenderTaskInfo,
  CopyCalenderTask,
  DeleteCalenderTask,
  ResetTasksInfo
};
