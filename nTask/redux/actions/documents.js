import constants from "../constants/types";
import instance from "../instance";
import taskActions from "./tasks";

  export function GetAllFolders(contextUrl, groupId, contextView, success, fail) {
    return dispatch => {
        return instance().get("/api/" + contextUrl + "/GetAllFolders?groupId=" + groupId + "&groupType=" + contextView)
            .then((result) => {
                dispatch(PopulateDocumentsData(result.data));
                success(result);
            })
            .catch(error => {
                fail(error.response);
            });
    };
  }
  export function uploadFileToAmazon(contextUrl, _FormData, success, fail) {
    return dispatch => {
      return instance().post("api/docsfileuploader/UploadDocsFileAmazonS3", _FormData)
      .then(response => {
        success(response);
      })
      .catch(error => {
  
      });
    };
  }
  export function saveDocToContextObject(contextUrl, contextId, contextView, attachment, success, fail) {
    return (dispatch) => {
      return instance()
        .post("/api/" + contextUrl + "/SaveDocsAttachment", attachment)
        .then((result) => {
          return result;
        })
        .then((result) => {       
          dispatch(StoreFileNew(result.data.attachments)); 
          success(result);
          if(contextView == 'Task')
          dispatch(taskActions.updateTaskCommentsInfo({
            taskId: contextId,
            totalAttachements: result.data.counts.totalAttachements,
            totalComments: result.data.counts.totalComments,
            totalUnreadComment: result.data.counts.totalUnreadComment,
          }))
        })
        .catch((error) => {
            fail(error.response);
        });
    };
  }
  // export function saveUserComment(contextUrl, data, success, fail) {
 
  //   return (dispatch) => {
  //     return instance()
  //       .post("/api/" + contextUrl + "/SaveUserComment", data)
  //       .then((result) => {
  //         dispatch(StoreFileNew(result.data)); 
  //         success(result);
  //       })
  //       .catch((error) => {
  //         fail(error.response);
  //       });fail
  //   };
  // }

  export function createFolder(contextUrl, data, success, fail) {
    return dispatch => {
      return instance()
      .post("/api/" + contextUrl + "/createFolder", data)
      .then(response => {
        dispatch(StoreFolderNew(response.data));
        success(response);
      })
      .catch(error => {
        fail(error);
      });
    };
  }
  export function OpenFolder(contextUrl, data, success, fail) {
    return dispatch => {
      return instance()
      .post("/api/" + contextUrl + "/openFolder", data)
      .then(response => {
         dispatch(PopulateDocumentsData(response.data));
        success(response);
      })
      .catch(error => {
        fail(error);
      });
    };
  }
  export function RenameFolder(contextUrl, data, success, fail) {
    return dispatch => {
      return instance()
      .post("/api/" + contextUrl + "/renamefolder", data)
      .then(response => {
         dispatch(StoreFolderUpdate(response.data));
        success(response);
      })
      .catch(error => {
        fail(error);
      });
    };
  }
  export function DownloadFolder(contextUrl, folderId, success, fail) {
    return dispatch => {
      return instance()
      .get("/api/" + contextUrl + "/downloadfolderbyId?folderId=" + folderId)
      .then(response => {
        success(response);
      })
      .catch(error => {
        fail(error);
      });
    };
  }
  export function DeleteFolder(contextUrl, data, success, fail) {
    return dispatch => {
      return instance()
      .get("/api/" + contextUrl + "/deletefolder?id=" + data.id)
      .then(response => {
         dispatch(StoreFolderDelete(data));
         success(response);
      })
      .catch(error => {
        fail(error);
      });
    };
  }
  export function RemoveFilesFromFolder(contextUrl, contextId, contextView, data, success, fail) {
    return dispatch => {
      return instance()
      .get("/api/" + contextUrl + "/DeleteUserAttachment?id=" + data.id)
      .then(result => {
         dispatch(StoreFileDelete(data));
         success(result);
         if(contextView == 'Task')
        dispatch(taskActions.updateTaskCommentsInfo({
          taskId: contextId,
          totalAttachements: result.data.counts.totalAttachements,
          totalComments: result.data.counts.totalComments,
          totalUnreadComment: result.data.counts.totalUnreadComment,
        }))
      })
      .catch(error => {
        fail(error);
      });
    };
  }

  export function StoreFolderUpdate(data) {
    return {
      type: constants.UPDATEFOLDERITEM,
      payload: {
        data,
      },
    };
  }
  export function StoreFolderDelete(data) {
    return {
      type: constants.DELETEFOLDERITEM,
      payload: {
        data,
      },
    };
  }
  export function StoreFileDelete(data) {
    return {
      type: constants.DELETEFILEITEM,
      payload: {
        data,
      },
    };
  }
  export function StoreFolderNew(data) {
    return {
      type: constants.NEWFOLDERITEM,
      payload: {
        data,
      },
    };
  }
  export function StoreFileNew(data) {
    return {
      type: constants.NEWFILEITEM,
      payload: {
        data,
      },
    };
  }
  export function PopulateDocumentsData(data) {
    return {
        type: constants.POPULATEDOCUMENTS,
        payload: {
            data,
        }
    };
}
  export function dispatchFolderUpdate(data) {
    return (dispatch) => {
      dispatch(StoreFolderUpdate(data));
    };
  }
  export function dispatchFolderDelete(data) {
    return (dispatch) => {
      dispatch(StoreFolderDelete(data));
    };
  }
  export function dispatchFileDelete(data) {
    return (dispatch) => {
      dispatch(StoreFileDelete(data));
    };
  }
  export function dispatchFolderNew(data) {
    return (dispatch) => {
      dispatch(StoreFolderNew(data));
    };
  }
  export function dispatchFileNew(data) {
    return (dispatch) => {
      dispatch(StoreFileNew(data));
    };
  }
  export function clearDocuments() {
    return (dispatch) => {
      dispatch(PopulateDocumentsData({
        attachments: [],
        folders: [],
      }));
    };
  }