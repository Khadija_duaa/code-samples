import constants from "./../constants/types";
import instance from "../instance";
import COLORCODES from "../../components/constants/colorCodes";
import calender from "./calenderTasks";
import helper from "./../../helper/index";
import { updateWorkSpaceImageURL, UpdateMember } from "./profile";
import {
  setGlobalTaskTime,
  removeGlobalTaskTime,
  updateGlobalTimeTask,
  getGlobalTaskTime,
} from "./globalTimerTask";
import { workspaceMemberCrudAction, workspaceMemberCrud, GetFeatureAccessPermissions } from "./team";
import isEmpty from "lodash/isEmpty";
import permissionData from "../../utils/permissionsData";
import { getTasks } from "./tasks";
import { getColumnsList } from "./columns";
import { grid } from "../../components/CustomTable2/gridInstance";
import { getIssues } from "./issues";
import { getCustomField } from "./customFields";
import { getProjects } from "./projects";
import { getRisks } from "./risks";

export function WorkspaceExists(callback) {
  let response = null;
  return dispatch => {
    return instance()
      .get("api/WorkSpace/IsWorkspaceExist")
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}
export function PopulateWorkspacePermissionSettings(data) {
  return {
    type: constants.PERMISSIONSETTINGS,
    payload: {
      data,
    },
  };
}
export function CopyWorkspacePermissionSettings(data) {
  return {
    type: constants.COPYPERMISSION,
    payload: {
      data,
    },
  };
}
export function UpdateTeamMember(data) {
  return {
    type: constants.UPDATETEAMMEMBER,
    payload: data,
  };
}
export function RenameWorkspacePermissionSettings(data) {
  return {
    type: constants.UPDATEPERMISSION,
    payload: {
      data,
    },
  };
}

export function DeleteWorkspacePermissionSettings(data) {
  return {
    type: constants.DELETEPERMISSION,
    payload: {
      data,
    },
  };
}

export function PopulateWorkspaceInfo(data) {
  return {
    type: constants.POPULATEWORKSPACEINFO,
    payload: {
      data,
    },
  };
}
export function PopulateProjects(data) {
  if (!isEmpty(data)) {
    let item = data.itemOrder ? data.itemOrder.itemOrder : {};
    let projects = data.projects.map(x => {
      let position = helper.RETURN_ITEMORDER(item, x.id);
      x.taskOrder = position || 0;
      return x;
    });
    let completeMeetingDetails = projects.sort((a, b) => a.taskOrder - b.taskOrder);

    return {
      type: constants.POPULATEPROJECTINFO,
      payload: {
        data: completeMeetingDetails || [],
      },
    };
  } else {
    return { type: constants.POPULATEPROJECTINFO, payload: { data: [] } };
  }
}

export function UpdateStoreTasks(data) {
  return [
    {
      type: constants.UPDATETASKINFO,
      payload: {
        data,
      },
    },
    updateGlobalTimeTask(data),
  ];
}

export function UpdateTaskColorFromGridItem(obj, callback) {
  return dispatch => {
    //
    return instance()
      .post("/api/UserTask/UpdateUserTask", obj)
      .then(response => {
        response = response.data;
        const actions = UpdateStoreTasks(response);
        dispatch(actions[0]);
        dispatch(actions[1]);
        return { response: response };
      })
      .then(data => {
        let currentDate = new Date();
        let dueDate = new Date(data.response.dueDate);
        let obj = {
          id: data.response.taskId,
          title: data.response.taskTitle,
          start: new Date(data.response.startDateString),
          end: new Date(data.response.dueDateString),
          desc: data.response.description,
          //taskColor: data.response.colorCode,
          taskColor: COLORCODES.STATUSCOLORS[data.response.status],
          status: data.response.status,
          isDueToday: currentDate.toDateString() == dueDate.toDateString() ? true : false,
          isStared: data.response.isStared,
          userColors: data.response.userColors.length > 0 ? data.response.userColors : [], //colorCode
        };
        callback(obj);
        return obj;
      });
  };
}

export function copyRoleToWorkspace(data, callback) {
  instance()
    .post("/api/permission/CopyRole", data)
    .then(() => {
      callback();
    });
}
export function UpdateTasksFromCalender(obj, callback) {
  let response = null;
  return dispatch => {
    return instance()
      .post("/api/UserTask/UpdateUserTask", obj)
      .then(response => {
        //
        response = response.data;
        const actions = UpdateStoreTasks(response);
        dispatch(actions[0]);
        dispatch(actions[1]);
        return { response: response };
      })
      .then(data => {
        //
        callback(data.response);
        return data.Dobj;
      });
  };
}

export function AcceptTeamInvitation(teamID, success, error) {
  return dispatch => {
    return instance()
      .get("/api/company/AcceptTeamInvitation?teamId=" + teamID)
      .then(response => {
        success(response);
      })
      .catch(response => {
        error(response.response);
      });
  };
}
export function AcceptWorkspaceInvitation(teamID, success, error) {
  return dispatch => {
    return instance()
      .get("/api/team/AcceptWorkspaceInvitation?teamId=" + teamID)
      .then(response => {
        success(response);
      })
      .catch(response => {
        error(response.response);
      });
  };
}

export function getArchivedData(type, callback) {
  // Project = 1,
  //   Task = 2,
  //   Meeting = 3,
  //   Issue = 4,
  //   Risk = 5
  return instance()
    .get(`api/workspace/GetArchivedData?type=${type}`)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      callback(response.response);
    });
}

export function getWorkspaceRoles(success, failure) {
  return dispatch => {
    return instance()
      .get("/api/permission/getworkspaceroles")
      .then(response => {
        dispatch(PopulateWorkspacePermissionSettings(response.data));
        return response;
      })
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function getWorkspaceTemplates(success, failure) {
  return dispatch => {
    return instance()
      .get("/api/Template/GetAllTemplatesByGroup")
      .then(response => {
        dispatch(PopulateWSTemplates(response.data.entity));
        return response;
      })
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function getProjectTemplates(projectId, success, failure) {
  return dispatch => {
    return (
      instance()
        .get("/api/Template/GetAllTemplatesByGroup")
        // .get("/api/workspace/GetworkspaceTaskStatus")
        .then(response => {
          dispatch(PopulateWSTemplates(response.data.entity));
          return response;
        })
        .then(response => {
          success(response);
        })
        .catch(response => {
          failure(response.response);
        })
    );
  };
}
export function DeleteWorkspaceRole(roleId, success, failure) {
  if (roleId)
    return dispatch => {
      return instance()
        .get("/api/permission/DeleteRole?roleId=" + roleId)
        .then(response => {
          dispatch(DeleteWorkspacePermissionSettings(response.data));
          return response;
        })
        .then(response => {
          success(response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
}
export function duplicateRole(data, success, failure) {
  return dispatch => {
    return instance()
      .post("/api/permission/SaveDublicateRole", data)
      .then(response => {
        dispatch(CopyWorkspacePermissionSettings(response.data));
        return response;
      })
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function CreateCustomRole(data, success, failure) {
  return dispatch => {
    return instance()
      .post("/api/permission/CreateCustomRole", data)
      .then(response => {
        dispatch(CopyWorkspacePermissionSettings(response.data));
        return response;
      })
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function RenameRole(data, success, failure) {
  return dispatch => {
    return instance()
      .post("/api/permission/RenameRole", data)
      .then(response => {
        dispatch(RenameWorkspacePermissionSettings(response.data));
        return response;
      })
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function saveRole(data, success, failure) {
  return dispatch => {
    return instance()
      .post("/api/permission/SaveRole", data)
      .then(response => {
        dispatch(RenameWorkspacePermissionSettings(response.data));
        return response;
      })
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function getOrder(data, id) {
  let position = data.find(x => {
    if (x.itemId === id) {
      return x.position;
    }
  });
  return position.position || 0;
}
export function PopulateTasks(data) {
  if (!isEmpty(data)) {
    let tasks = data.tasks || [];
    let issues = data.issues || [];
    let risks = data.risks || [];
    let meetings = data.meetings || [];
    let notifications = data.notifications || [];
    let item = data.itemOrder ? data.itemOrder.itemOrder : {};
    //let ids = item.map(x => {x.itemId)
    tasks = tasks.map(x => {
      let position = helper.RETURN_ITEMORDER(item, x.id);
      x.taskOrder = position || 0;
      return x;
    });

    let list = [];
    let completeDetails = tasks.map(x => {
      x.issues = [];
      x.risks = [];
      x.meetings = [];
      x.notifications = [];
      return x;
    });
    let completeTaskDetails = completeDetails;
    let withIssuesDetails = issues.map(x => {
      return completeTaskDetails.map(m => {
        list = m.issues || [];
        if (x.linkedTasks.indexOf(m.taskId) >= 0) {
          list.push(x);
        }
        m.issues = list;
        return m;
      });
    });
    if (withIssuesDetails && withIssuesDetails.length && withIssuesDetails[0].length) {
      completeTaskDetails = withIssuesDetails[0];
    }
    let withRisksDetails = risks.map(x => {
      return completeTaskDetails.map(m => {
        list = m.risks || [];
        if (x.linkedTasks.indexOf(m.taskId) >= 0) {
          list.push(x);
        }
        m.risks = list;
        return m;
      });
    });
    if (withRisksDetails && withRisksDetails.length && withRisksDetails[0].length) {
      completeTaskDetails = withRisksDetails[0];
    }

    let withMeetingDetails = meetings.map(x => {
      return completeTaskDetails.map(m => {
        list = m.meetings || [];
        if (x.taskId === m.taskId) {
          list.push(x);
        }
        m.meetings = list;
        return m;
      });
    });
    if (withMeetingDetails && withMeetingDetails.length && withMeetingDetails[0].length) {
      completeTaskDetails = withMeetingDetails[0];
    }

    let withNotificationsDetails = completeTaskDetails.map(x => {
      x.notifications = notifications.filter(n => n.objectId === x.taskId);
      return x;
    });

    if (withNotificationsDetails && withNotificationsDetails.length) {
      completeTaskDetails = withNotificationsDetails;
    }

    completeTaskDetails.forEach(function (element) {
      let color =
        element.userColors && element.userColors.length > 0
          ? element.userColors[0]["colorCode"]
          : "#16a5de";
      let value = [],
        _color = "#16a5de"; //color="#EE0077";

      if (color && color.indexOf("#") < 0) {
        value = COLORCODES.COLORCODES.filter(x => x.key === color);
      } else {
        _color = color;
      }
      value = value.length ? value[0].value : _color;
      let currentDate = new Date();
      let dueDate = new Date(element.dueDate);

      //
      //
      let obj = {
        id: element.taskId,
        title: element.taskTitle,
        start: new Date(element.startDateString),
        end: new Date(element.dueDateString),
        desc: element.description,
        actualDueDateString: element.actualDueDateString,
        actualStartDateString: element.actualStartDateString,
        dueDateString: element.dueDateString,
        startDateString: element.startDateString,
        priority: element.priority,
        taskColor: COLORCODES.STATUSCOLORS[element.status],
        status: element.status,
        isDueToday: currentDate.toDateString() == dueDate.toDateString() ? true : false,
        isStared: element.isStared,
        userColors: element.userColors && element.userColors.length > 0 ? element.userColors : [],
      };

      element.CalenderDetails = obj;
    });
    completeTaskDetails = completeTaskDetails.sort((a, b) => a.taskOrder - b.taskOrder);
    // return {
    //   type: constants.POPULATETASKSINFO,
    //   payload: {
    //     data: localStorage.getItem("projectId")
    //       ? completeTaskDetails.filter(
    //           x => x.projectId === localStorage.getItem("projectId")
    //         )
    //       : completeTaskDetails
    //   }
    // };
  } else {
    // return { type: constants.POPULATETASKSINFO, payload: { data: [] } };
  }
}

export function PopulateMeetings(data) {
  if (!isEmpty(data)) {
    let item = data.itemOrder ? data.itemOrder.itemOrder : {};
    let meetings = data.meetings.map(x => {
      let position = helper.RETURN_ITEMORDER(item, x.id);
      x.taskOrder = position || 0;
      return x;
    });
    let completeMeetingDetails = meetings.sort((a, b) => a.taskOrder - b.taskOrder);
    return {
      type: constants.POPULATEMEETINGSINFO,
      payload: {
        data: completeMeetingDetails || [],
      },
    };
  } else {
    return { type: constants.POPULATEMEETINGSINFO, payload: { data: [] } };
  }
}
export function PopulateRisks(data) {
  if (!isEmpty(data)) {
    let item = data.itemOrder ? data.itemOrder.itemOrder : {};
    let risks = data.risks.map(x => {
      let position = helper.RETURN_ITEMORDER(item, x.id);
      x.taskOrder = position || 0;
      return x;
    });
    let completeMeetingDetails = risks.sort((a, b) => a.taskOrder - b.taskOrder);
    return {
      type: constants.POPULATERISKSINFO,
      payload: {
        data: completeMeetingDetails || [],
      },
    };
  } else {
    return { type: constants.POPULATERISKSINFO, payload: { data: [] } };
  }
}

export function PopulateIssues(data) {
  if (!isEmpty(data)) {
    let item = data.itemOrder ? data.itemOrder.itemOrder : {};
    let issues = data.issues.map(x => {
      let position = helper.RETURN_ITEMORDER(item, x.id);
      x.taskOrder = position || 0;
      return x;
    });
    let completeMeetingDetails = issues.sort((a, b) => a.taskOrder - b.taskOrder);
    return {
      type: constants.POPULATEISSUESINFO,
      payload: {
        data: completeMeetingDetails || [],
      },
    };
  } else {
    return { type: constants.POPULATEISSUESINFO, payload: { data: [] } };
  }
}

// export function PopulateNotifications(data) {
//   return {
//     type: constants.POPULATENOTIFICATIONS,
//     payload: {
//       data
//     }
//   };
// }

export function ResetWorkspaceInfo() {
  return {
    type: constants.RESETWORKSPACE,
  };
}
export function PopulateItemOrder(data) {
  return {
    type: constants.POPULATEITEMORDER,
    payload: {
      data,
    },
  };
}
export function PopulatePermissions(data) {
  return {
    type: constants.WORKSPACEPERMISSIONS,
    payload: {
      data,
    },
  };
}
export function PopulateWSTemplates(data) {
  return {
    type: constants.WORKSPACEALLTEMPLATES,
    payload: {
      data,
    },
  };
}
// export function PopulateProjectTemplates(data) {
//   return {
//     type: constants.PROJECTALLTEMPLATES,
//     payload: {
//       data
//     }
//   };
// }
export function PopulateDefaultWSTemplate(data, dispatchFn = null) {
  if (dispatchFn) {
    dispatchFn({
      type: constants.WORKSPACEDEFAULTTEMPLATE,
      payload: {
        data,
      },
    });
  } else {
    return {
      type: constants.WORKSPACEDEFAULTTEMPLATE,
      payload: {
        data,
      },
    };
  }
}

export function RemoveWorkspace(data) {
  return {
    type: constants.REMOVETEAM,
    payload: {
      data,
    },
  };
}
export function ChangeMemberRoleInWorkspace(data, isInvited) {
  return {
    type: constants.CHANGEUSERROLE,
    payload: {
      data,
      isInvited,
    },
  };
}

export function AddNewTeam(team) {
  return {
    type: constants.ADDTEAM,
    payload: {
      team,
    },
  };
}

export function UpdateTeam(team) {
  return {
    type: constants.UPDATETEAM,
    payload: {
      team,
    },
  };
}

export function RemoveTeamFromList(teamId) {
  return {
    type: constants.DELETETEAM,
    payload: {
      teamId,
    },
  };
}

export function PopulateTeamData(data) {
  return {
    type: constants.UPDATETEAMS,
    payload: {
      data,
    },
  };
}

export function AddMemberToWorkSpace(data) {
  return {
    type: constants.UPDATEINVITATIONMEMBERS,
    payload: {
      data,
    },
  };
}
export function UpdateGroupColumnAction(data) {
  return {
    type: constants.UPGRADEGROUPCOLUMN,
    payload: {
      data,
    },
  };
}

export function UpdateWorkSpaceImage(obj, callback, failure) {
  let data = new FormData();
  data.append("PrevImageName", obj["PrevImageName"]);
  data.append("UploadedImage", obj["UploadedImage"]);
  const teamId = obj["teamId"];
  return dispatch => {
    return instance()
      .post("api/team/UploadWorkspaceLogo", data)
      .then(response => {
        dispatch(updateWorkSpaceImageURL(teamId, response.data));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function updateGroupColumn(data, failure) {
  return dispatch => {
    return instance()
      .post("/api/team/SaveGroupByColumn", data)
      .then(response => {
        dispatch(UpdateGroupColumnAction(response.data));
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function RemoveWorkspaceLogo(teamId, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/team/RemoveWorkspaceLogo", {})
      .then(response => {
        dispatch(updateWorkSpaceImageURL(teamId, null));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function UpdateWorkSpaceData(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/team/UpdateWorkspace", {
        teamId: data.teamId,
        teamName: data.teamName,
        teamUrl: data.teamUrl,
      })
      .then(response => {
        dispatch(PopulateTeamData(response.data));
        return response;
      })
      .then(response => {
        callback(response.data);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function DeleteWorkSpaceData(workspaceId, callback, failure) {
  return dispatch => {
    return instance()
      .get("api/team/DeleteWorkspace?id=" + workspaceId)
      .then(response => {
        // dispatch(RemoveWorkspace(workspaceId))
        // return response;
        callback(null, response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function InviteTeamMembers(emails, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/Account/InviteTeamMembers", emails)
      .then(response => {
        dispatch(ChangeMemberRoleInWorkspace(response.data, true));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function AddMembersToWorkSpace(obj, callback, failure, workspaceId = "") {
  /** function for sending invitation to members in workspace  */

  return dispatch => {
    return instance()
      .post("/api/team/AddWorkspaceMember", { MemberIds: obj })
      .then(response => {
        dispatch(workspaceMemberCrudAction(response.data, "create"));
        // dispatch(workspaceMemberCrud(obj, "create", workspaceId));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function ResendInviteTeamMembers(email, callback, failure) {
  return dispatch => {
    return instance()
      .get(`api/account/ResendTeamInvite?email=${email}`)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function RemoveMembers(object, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/team/RevokeTeamInvite", object)
      .then(response => {
        const member = { userId: response.data };
        dispatch(UpdateMember(member, "delete"));
        return response;
      })
      .then(x => {
        callback(x);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function RemoveWorkspaceMembers(id, callback, failure) {
  return dispatch => {
    return instance()
      .get(`api/team/RemoveWorkspaceMember?memberId=${id}`)
      .then(response => {
        callback();
        dispatch({
          type: constants.REMOVEWORKSPACEMEMBER,
          payload: id,
        });
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function EnableDisableMembers(object, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/team/EnableAndDisableMember", object)
      .then(response => {
        const member = response.data;
        dispatch(UpdateMember(member, "update"));
        return response;
      })
      .then(x => {
        callback(x);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function ChangeUserRole(object, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/team/UpdateMemberRole", object)
      .then(response => {
        dispatch(ChangeMemberRoleInWorkspace(response.data));
        return response;
      })
      .then(x => {
        callback(x);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function UpdateUserIsOnline(obj) {
  return dispatch => {
    dispatch({
      type: constants.UPDATEUSERISONLINE,
      payload: obj,
    });
  };
}

// getConstants: () => {
//   return axios.get(config.API_URL + "/api/ntask/getConstants", headers.GETHEADERS().headers);
// },

// export function GetConstants(callback) {
//   return dispatch => {
//     return instance()
//       .get("/api/ntask/getConstants")
//       .then(response => {
//         if(response.status === 200)
//         {
//
//
//         }
//       })
//       .then(() => {
//         callback();
//       });

//     return null;
//   };
// }

function getGlobalTimerTask(tasks, timer) {
  if (timer) {
    const { taskId, createdDate, currentDate } = timer;
    let task = tasks.filter(x => x.taskId === taskId);
    if (task.length === 1) return setGlobalTaskTime(task[0], createdDate, currentDate);
    else return removeGlobalTaskTime();
  }
}
export function clearAllTaskAction() {
  return {
    type: constants.CLEAR_ALL_TASKS,
  };
}
export function FetchWorkspaceInfo(teamId, callback, failure, dispatchFn = null) {
  let api = "/api/workspace/getworkspacedata";
  if (teamId) api = "/api/workspace/getworkspacedata?teamId=" + teamId;
  if (dispatchFn) {
    dispatchFn(clearAllTaskAction());
    instance()
      .get(api)
      .then(response => {
        getColumnsList(() => { }, dispatchFn);
        dispatchFn(getCustomField(succ => { }, fail => { }));
        getTasks(null, dispatchFn, tasks => {
          getGlobalTaskTime(dispatchFn, tasks);
        });
        getIssues(null, dispatchFn)
        getProjects(null, dispatchFn)
        getRisks(dispatchFn);
        GetFeatureAccessPermissions(dispatchFn);

        // dispatchFn(PopulateIssues([]));
        // dispatchFn(PopulateNotifications(response.data.notifications));
        dispatchFn(PopulateRisks(response.data));
        // dispatchFn(PopulateMeetings(response.data));
        dispatchFn(PopulateItemOrder(response.data.itemOrder));
        // dispatchFn(PopulateProjects(response.data));
        dispatchFn(PopulatePermissions(response.data.permission));
        // dispatchFn(PopulateTasks(response.data));
        dispatchFn(PopulateDefaultWSTemplate(response.data.defaultStatus));
        dispatchFn(removeGlobalTaskTime());
        return { response: response };
      })
      .then(response => {
        callback(response.response);
      })
      .catch(response => {
        if (failure) {
          failure(response);
        } else {
          callback(response ? response.response : "Error");
        }
      });
  } else {
    return dispatch => {
      dispatch(clearAllTaskAction());
      return instance()
        .get(api)
        .then(response => {
          getColumnsList(() => { }, dispatch);
          getCustomField(succ => { }, fail => { });
          getTasks(null, dispatch, tasks => {
            getGlobalTaskTime(dispatch, tasks);
          });
          getIssues(null, dispatch);
          getProjects(null, dispatch);
          getRisks(dispatch);
          GetFeatureAccessPermissions(dispatch);
          // dispatch(PopulateIssues([]));
          // dispatch(PopulateNotifications(response.data.notifications));
          dispatch(PopulateRisks(response.data));
          // dispatch(PopulateMeetings(response.data));
          dispatch(PopulateItemOrder(response.data.itemOrder));
          // dispatch(PopulateProjects(response.data));
          dispatch(PopulatePermissions(response.data.permission));

          // dispatch(PopulateTasks(response.data));
          dispatch(PopulateDefaultWSTemplate(response.data.defaultStatus));
          dispatch(removeGlobalTaskTime());

          return { response: response };
        })
        .then(response => {
          callback(response.response);
        })
        .catch(response => {
          if (failure) {
            failure(response);
          } else {
            callback(response ? response.response : "Error");
          }
        });
    };
  }
}
export function AddWorkspace(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/team/createworkspace", data)
      .then(response => {
        dispatch(PopulateProjects({}));
        // dispatch(PopulateTasks({}));
        dispatch(PopulateMeetings({}));
        dispatch(PopulateRisks({}));
        // dispatch(PopulateIssues({}));
        // dispatch(PopulateNotifications([]));
        // dispatch(PopulatePermissions([]));
        return response;
      })
      .then(data => {
        callback(data);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function workspaceEmailNtask(workspaceId, type, callback) {
  instance()
    .get(`api/workspaceemailid/${workspaceId}/${type}`)
    .then(response => {
      callback(response.data)
    })
    .catch(response => {
      error(response.message);
    });

}
export function getCityByCountryCode(code, success, failure) {
  instance("https://nformsapi.naxxa.io/")
    .get(`api/common/cities/${code}`)
    .then(response => {
      success(response.data)
    })
    .catch(response => {
      failure(response.message);
    });

}
export function configUpdate(data) {
  return {
    type: constants.UPDATECONFIGWORKSPACE,
    payload: data
  };
}

export function updateCurrentWorkspace(data) {
  return {
    type: constants.UPDATECURRENTWORKSPACE,
    payload: data
  };
}

export function updateCurrentWorkspaceCalender(data) {
  return {
    type: constants.UPDATECURRENTWORKSPACECALENDAR,
    payload: data
  };
}

export function updateProgress(data) {
  return {
    type: constants.UPDATEPROGRESS,
    payload: data
  };
}

export function updateWorkspaceConfig(dispatch, data, callback, failure) { /** updation in project mandatory  */
  instance()
    .post("api/team/SetProjectFieldMandatory", data)
    .then(response => {
      dispatch(configUpdate(data))
      callback(response.data);
    })
    .catch(response => failure(response.response));
}
export function updateTaskEffortWorkspaceConfig(dispatch, data, callback, failure) { /** updation in project mandatory  */
  instance()
    .post("api/team/SetUserTasksEffortMandatory", data)
    .then(response => {
      dispatch(configUpdate(data))
      callback(response.data);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function updateTimeSettingWorkspaceConfig(dispatch, data, callback, failure) { 
  instance()
    .post("api/team/ConfigureWorkspaceCustomMins", data)
    .then(response => {
      dispatch(configUpdate(data))
      callback(response.data);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function updateProgressConfig(dispatch, data, callback, failure) { /** updation in project mandatory  */
  instance()
    .post("api/workspace/SetTaskWorkspaceTaskProgressMethod", data)
    .then(response => {
      dispatch(updateProgress(data))
      callback(response.data);
    })
    .catch(response => {
      failure(response.response);
    });
}

export default {
  PopulateIssues,
  // PopulateNotifications,
  PopulateRisks,
  PopulateMeetings,
  PopulateProjects,
  PopulateItemOrder,
  PopulatePermissions,
  PopulateWSTemplates,
  // PopulateProjectTemplates,
  PopulateTasks,
  getGlobalTimerTask,
  removeGlobalTaskTime,
  getWorkspaceRoles,
  FetchWorkspaceInfo,
  ResetWorkspaceInfo,
  UpdateStoreTasks,
  AddWorkspace,
  AcceptTeamInvitation,
  ChangeMemberRoleInWorkspace,
  AddMembersToWorkSpace,
  workspaceEmailNtask,
  getCityByCountryCode
};
