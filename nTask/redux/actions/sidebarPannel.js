import constants from "../constants/types";
import instance from "../instance";

export function updateSideBarStateAction(data) {
  return {
    type: constants.UPDATESIDEBARSTATE,
    payload: data,
  };
}
export function showHideSideBar(data) {
  return dispatch => {
    dispatch({
      type: constants.SHOW_HIDE_SIDEBAR,
      payload: data,
    });
  };
}

export function setMenuItems(data) {
  return {
    type: constants.UPDATESIDEBARSTATE,
    payload: { data: data },
  };
}
export function setMenuItemsOrdering(data) {
  return {
    type: constants.UPDATESIDEBARORDER,
    payload: data,
  };
}

export function updateSidebarState(dispatchFn, data) {
  if (dispatchFn) {
    dispatchFn(updateSideBarStateAction(data));
  } else {
    return dispatch => {
      dispatch(updateSideBarStateAction(data));
    };
  }
}
export function getMenuItems(dispatchFn, callback, failure) {
  instance()
    .get("api/menus")
    .then(response => {
      dispatchFn(setMenuItems(response.data.entity));
      return response;
    })
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function saveMenuItems(dispatchFn, data, callback, failure) {
  instance()
    .put(`api/menus/${data.userId}`, data)
    .then(response => {
      dispatchFn(
        setMenuItemsOrdering({
          teamMenu: response.data.entity.teamMenu,
          workspaceMenu: response.data.entity.workspaceMenu,
        })
      );
      return response;
    })
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function updateFileDescription(url, data, callback, failure) {
  instance(url)
    .put(`api/file/updatedescription`, data)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response);
    });
}
export function updateFolderDescription(url, data, callback, failure) {
  instance(url)
    .put(`api/filemanager/updatedescription`, data)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response);
    });
}
export function downloadFile(url, data, callback, failure) {
  instance(url)
    .post(`api/file/download`, data)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response);
    });
}
export function previewFile(url, data, callback, failure) {
  instance(url)
    .post(`api/file/preview`, data)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response);
    });
}
export function getStorageLimit(url, callback, failure) {
  instance(url)
    .get(`api/team/totalsize`)
    .then(response => {
      callback(response.data);
    })
    .catch(response => {
      failure(response);
    });
}
export function getFileDescription(url, id, callback, failure) {
  instance(url)
    .get(`api/file/description/${id}`)
    .then(response => {
      callback(response.data.data);
    })
    .catch(response => {
      failure(response);
    });
}
export function getFolderDescription(url, id, callback, failure) {
  instance(url)
    .get(`api/filemanager/folder/description/${id}`)
    .then(response => {
      callback(response.data.data);
    })
    .catch(response => {
      failure(response);
    });
}
export function getThumbnail(url, path, callback, failure) {
  instance(url)
    .get(`api/file/${path}`)
    .then(response => {
      callback(response.data);
    })
    .catch(response => {
      failure(response);
    });
}
