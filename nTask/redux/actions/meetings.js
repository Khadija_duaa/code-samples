import constants from "./../constants/types";
import instance from "../instance";
import order from "./itemOrder";
import cloneDeep from "lodash/cloneDeep";
import unionBy from "lodash/unionBy";
import { grid } from "../../components/CustomTable2/gridInstance";

export function PopulateMeetingsInfo(data) {
  return {
    type: constants.POPULATEMEETINGSINFO,
    payload: {
      data,
    },
  };
}

export function ResetMeetingsInfo() {
  return {
    type: constants.RESETMEETINGSINFO,
  };
}

export function StoreCopyMeetingInfo(data) {
  return {
    type: constants.COPYMEETING,
    payload: {
      data,
    },
  };
}

export function StoreCopyMeetingScheduleInfo(data, id) {
  return {
    type: constants.COPYMEETINGSCHEDULE,
    payload: {
      data,
      meetingId: id,
    },
  };
}
export function UpdateStoreMeetings(data) {
  return {
    type: constants.UPDATEMEETINGSINFO,
    payload: {
      data,
    },
  };
}

export function AddMeetingToStore(data) {
  return {
    type: constants.ADDMEETING,
    payload: {
      data,
    },
  };
}
export function UpdateFollowupAction(meetingId, followupAction) {
  return {
    type: constants.UPDATEFOLLOWUPACTION,
    payload: {
      meetingId,
      followupAction,
    },
  };
}
export function deleteStoreMeeting(data) {
  return {
    type: constants.DELETEMEETING,
    payload: {
      data,
    },
  };
}

export function archiveStoreMeetingData(data) {
  return {
    type: constants.ARCHIVEMEETINGS,
    payload: {
      data,
    },
  };
}

export function unarchiveStoreMeetings(data) {
  return {
    type: constants.UNARCHIVEMEETINGS,
    payload: {
      data,
    },
  };
}

export function cancelStoreMeetingData(data) {
  return {
    type: constants.CANCELMEETINGSDATA,
    payload: {
      data,
    },
  };
}
//GET /api/Meeting/UnarchiveMeeting

export function UnArchiveMeeting(data, callback = () => { }, failure = () => { }, dispatchFn = null) {
  let parentId = data.parentId ? data.parentId : 'none'
  if (dispatchFn) {
    instance()
      .get(`/api/Meeting/UnarchiveMeeting/${data.meetingId}/${parentId}`)
      .then(response => {
        // bulkMeetingAction
        dispatchFn(bulkMeetingAction(response.data));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response.data);
      })
      .catch(response => callback(response.response, null));
  } else {
    return dispatch => {
      return instance()
        .get(`/api/Meeting/UnarchiveMeeting/${data.meetingId}/${parentId}`)
        .then(response => {
          dispatch(bulkMeetingAction(response.data));
          return { response: response };
        })
        .then(_data => {
          callback(_data.response.data);
        })
        .catch(response => callback(response.response, null));
    };
  }

}

export function CancelMeeting(data, callback, dispatchFn) {
  let parentId = data.parentId ? data.parentId : 'none'
  if (dispatchFn) {
    instance()
      .get(`/api/Meeting/CancelMeeting/${data.meetingId}/${parentId}`)
      .then(response => {
        dispatchFn(cancelStoreMeetingData(response.data));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => callback(response.response, null));
  } else {
    return dispatch => {
      return instance()
        .get(`/api/Meeting/CancelMeeting/${data.meetingId}/${parentId}`)
        .then(response => {
          dispatch(cancelStoreMeetingData(response.data));
          return { response: response };
        })
        .then(_data => {
          callback(_data.response);
        })
        .catch(response => callback(response.response, null));
    };
  }
}

export function ArchiveMeeting(data, callback, failure, dispatchFn) {
  let parentId = data.parentId ? data.parentId : 'none'
  if (dispatchFn) {
    return instance()
      .get(`/api/Meeting/ArchiveMeeting/${data.meetingId}/${parentId}`)
      .then(response => {
        let meetingIds = response.data.map(meeting => {
          return meeting.meetingId;
        });
        // deleteBulkMeetingAction change to this
        dispatchFn(deleteBulkMeetingAction({ meetingIds: meetingIds }));
        // dispatchFn(ic.PopulateIssueActivities(response.data.activityLog));
        // dispatchFn(ic.PopulateIssueNotifications(response.data.notifications));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => failure(response.response));
  } else {
    return dispatch => {
      return instance()
        .get(`/api/Meeting/ArchiveMeeting/${data.meetingId}/${parentId}`)
        .then(response => {
          dispatch(deleteMeetingAction(data.meetingId));
          // dispatch(ic.PopulateIssueActivities(response.data.activityLog));
          // dispatch(ic.PopulateIssueNotifications(response.data.notifications));
          return { response: response };
        })
        .then(_data => {
          callback(_data.response);
        })
        .catch(response => failure(response.response));
    };
  }
}
export function deleteMeetingAction(data) {
  return {
    type: constants.DELETE_MEETING,
    payload: {
      data,
    },
  };
}
export function DeleteMeeting(data, callback, failure, dispatchFn) {
  if (dispatchFn) {
    return instance()
      .delete("/api/Meeting/DeleteMeeting?meetingId=" + data)
      .then(response => {
        dispatchFn(deleteMeetingAction(data));
        return response;
      })
      .then(response => {
        callback(response.data);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .delete("/api/Meeting/DeleteMeeting?meetingId=" + data)
        .then(response => {
          dispatch(deleteMeetingAction(data));
          return response;
        })
        .then(response => {
          callback(response);
        })
        .catch(response => failure(response.response));
    };
  }
}
export function dispatchMeetingDelete(data) {
  return dispatch => {
    dispatch(deleteStoreMeeting(data));
  };
}

export function UpdateBulkStoreMeeting(data, isDelete) {
  return {
    type: constants.UPDATEBULKMEETINGINFO,
    payload: {
      data,
      isDelete: isDelete,
    },
  };
}

export function RemoveMeetingSchedule(meetingId, parentId) {
  return {
    type: constants.DELETEMEETINGSCHEDULE,
    payload: {
      meetingId,
      parentId,
    },
  };
}
export function Update(data) {
  return {
    type: constants.CREATEREPEATMEETING,
    payload: {
      data,
    },
  };
}

export function getMeetingsAction(data) {
  return {
    type: constants.SAVE_ALL_MEETINGS,
    payload: data,
  };
}
export function getMeetings(param, dispatchfn, callback = () => { }, failure = () => { }) {
  if (dispatchfn) {
    instance()
      .get("api/meetings")
      .then(res => {
        dispatchfn(getMeetingsAction(res.data.entity));
        callback(res.data.entity);
      })
      .catch(response => failure(response.response));
  } else {
    return dispatch =>
      instance()
        .get("api/meetings")
        .then(res => {
          dispatch(getMeetingsAction(res.data.entity));
          callback(res.data.entity);
        })
        .catch(response => failure(response.response));
  }
}

export function FetchMeetingsInfo(callback) {
  return dispatch => {
    return instance()
      .get("/api/meeting/getmeetings")
      .then(response => {
        return dispatch(PopulateMeetingsInfo(response.data));
      })
      .then(() => {
        callback();
      });
  };
}
export function CreateInLineMeeting(data, callback) {
  return dispatch => {
    return instance()
      .post("api/Meeting/CreateInLineMeeting", data)
      .then(response => {
        if (window.userpilot) { /** tracking successfully created meeting in app */
          window.userpilot.track("Meeting Created Successfully");
        }
        dispatch(StoreCopyMeetingInfo(response.data));
        // dispatch(order.FetchItemOrderInfo(() => {}));
        return response;
      })
      .then(response => {
        callback(null, response.data);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function SaveMeeting(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/CreateMeeting", data)
      .then(response => {
        if (window.userpilot) { /** tracking successfully created meeting in app */
          window.userpilot.track("Meeting Created Successfully");
        }
        dispatch(StoreCopyMeetingInfo(response.data));
        return response;
      })
      .then(response => {
        callback(null, response.data);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}
export function dispatchNewMeeting(data) {
  return dispatch => {
    dispatch(StoreCopyMeetingInfo(data));
  };
}
export function SaveAgenda(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/AddMeetingAgenda", data)
      .then(response => {
        // dispatch(StoreCopyMeetingInfo(response.data));
        return response;
      })
      .then(response => {
        callback(null, response.data);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function UpdateAgenda(data, meetingData, callback) {
  return dispatch => {
    return instance()
      .put("/api/Meeting/UpdateMeetingAgenda", data)
      .then(response => {
        let agenda =
          meetingData && meetingData.meetingAgendas && meetingData.meetingAgendas.length
            ? meetingData.meetingAgendas
            : [];
        agenda = agenda.map(x => {
          if (x.agendaId === response.data.agendaId) {
            x.description = response.data.description;
          }
          return x;
        });
        dispatch(UpdateStoreMeetings(agenda));
        return { response: agenda };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => callback(response.response, null));
  };
} //POST /api/Meeting/UpdateMeeting

export function DeleteDiscussionPoint(data, meetingData, callback) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/DeleteDiscussionPoint", data)
      .then(response => {
        let meeting = cloneDeep(meetingData);
        let discussionPoints = meeting.discussionPoints || [];
        meeting.discussionPoints = discussionPoints.filter(x => {
          return x.discussionPointId !== data.discussionPointId;
        });
        dispatch(UpdateStoreMeetings(meeting));
        return response;
      })
      .then(response => {
        callback(null, response.data);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function DeleteMeetingAgenda(data, meetingData, callback) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/DeleteMeetingAgenda", data)
      .then(response => {
        let meeting = cloneDeep(meetingData);
        let meetingAgendas = meeting.meetingAgendas || [];
        meeting.meetingAgendas = meetingAgendas.filter(x => {
          return x.agendaId !== data.agendaId;
        });
        dispatch(UpdateStoreMeetings(meeting));
        return response;
      })
      .then(response => {
        callback(null, response.data);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function SaveMeetingDecision(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/AddMeetingDecision", data)
      .then(response => {
        // dispatch(StoreCopyMeetingInfo(response.data));
        return response;
      })
      .then(response => {
        callback(null, response.data);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function UpdateMeetingDecision(data, meetingData, callback) {
  return dispatch => {
    return instance()
      .put("/api/Meeting/UpdateMeetingDecision", data)
      .then(response => {
        let decisions =
          meetingData && meetingData.decisions && meetingData.decisions.length
            ? meetingData.decisions
            : [];
        decisions = decisions.map(x => {
          if (x.decisionId === response.data.decisionId) {
            x.description = response.data.description;
          }
          return x;
        });
        meetingData.decisions = decisions;
        dispatch(UpdateStoreMeetings(meetingData));
        return response;
      })
      .then(response => {
        callback(null, response.data);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function RemoveMeetingDecision(data, meetingData, callback) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/DeleteMeetingDecision", data)
      .then(response => {
        let meeting = cloneDeep(meetingData);
        let decisions = meeting.decisions || [];
        meeting.decisions = decisions.filter(x => {
          return x.decisionId !== data.decisionId;
        });
        dispatch(UpdateStoreMeetings(meeting));
        return response;
      })
      .then(response => {
        callback(null, response.data);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function AddMeetingFollowUpAction(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/AddMeetingFollowUpAction", data)
      .then(response => {
        // dispatch(StoreCopyMeetingInfo(response.data));
        return response;
      })
      .then(response => {
        callback(null, response.data);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function DeleteMeetingFollowUpAction(data, meetingData, callback) {
  // followUpActionId
  return dispatch => {
    return instance()
      .post("/api/Meeting/DeleteMeetingFollowUpAction", data)
      .then(response => {
        let meeting = cloneDeep(meetingData);
        let followUpActions = meeting.followUpActions || [];
        meeting.followUpActions = followUpActions.filter(x => {
          return x.followUpActionId !== data.followUpActionId;
        });
        dispatch(UpdateStoreMeetings(meeting));
        return response;
      })
      .then(response => {
        callback(null, response.data);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function UpdateMeetingFollowUpAction(data, meetingData, callback) {
  return dispatch => {
    return instance()
      .put("/api/Meeting/UpdateFollowUpAction", data)
      .then(response => {
        let followUpActions =
          meetingData && meetingData.followUpActions && meetingData.followUpActions.length
            ? meetingData.followUpActions
            : [];
        followUpActions = followUpActions.map(x => {
          if (x.followUpActionId === response.data.followUpActionId) {
            x.description = response.data.description;
            x.assignee = response.data.assignee;
            x.dueDateString = response.data.dueDateString;
            x.dueDate = response.data.dueDate;
            x.assigneeList = response.data.assigneeList;
          }
          return x;
        });
        meetingData.followUpActions = followUpActions;
        dispatch(UpdateStoreMeetings(meetingData));
        return { response: agenda };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => callback(response.response, null));
  };
}

export function PublishMeeting(meetingData, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/Meeting/PublishMeeting/" + meetingData.meetingId)
      .then(response => {
        response.data.status = "Published";
        let object = { ...meetingData, ...response.data };
        dispatch(UpdateStoreMeetings(object));
        return { response: response.data };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => failure(response.response));
  };
}

export function UnPublishMeeting(meetingData, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/Meeting/UnPublishMeeting/" + meetingData.meetingId)
      .then(response => {
        response.data.status = "UnPublished";
        let object = { ...meetingData, ...response.data };
        dispatch(UpdateStoreMeetings(object));
        return { response: response.data };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => failure(response.response));
  };
}

export function InReviewMeeting(meetingData, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/Meeting/SubmitMeetingForReview/" + meetingData.meetingId)
      .then(response => {
        response.data.status = "InReview";
        let object = { ...meetingData, ...response.data };
        dispatch(UpdateStoreMeetings(object));
        return { response: response.data };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => failure(response.response));
  };
}
export function MarkMeetingAsStarted(data, callback, dispatchFn) {
  if (dispatchFn) {
    instance()
      .post("/api/meeting/MarkMeetingAsStarred", data)
      .then(response => {
        dispatchFn(UpdateStoreMeetings(response.data));
        return { response: response };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => callback(response.response, null));
  } else {
    return dispatch => {
      return instance()
        .post("/api/meeting/MarkMeetingAsStarred", data)
        .then(response => {
          dispatch(UpdateStoreMeetings(response.data));
          return { response: response };
        })
        .then(_data => {
          callback(null, _data.response);
        })
        .catch(response => callback(response.response, null));
    };
  }
}
export function UpdateMeeting(data, callback, dispatchFn) {
  if (dispatchFn) {
    instance()
      .post("/api/Meeting/UpdateMeeting", data)
      .then(response => {
        dispatchFn(UpdateStoreMeetings(response.data));
        return { response: response };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => callback(response.response, null));
  } else {
    return dispatch => {
      return instance()
        .post("/api/Meeting/UpdateMeeting", data)
        .then(response => {
          dispatch(UpdateStoreMeetings(response.data));
          return { response: response };
        })
        .then(_data => {
          callback(null, _data.response);
        })
        .catch(response => callback(response.response, null));
    };
  }
} //POST /api/Meeting/UpdateMeeting
export function dispatchMeeting(data) {
  const rowNode = grid.grid && grid.grid.getRowNode(data.id);
  rowNode && rowNode.setData(data);
  return dispatch => {
    dispatch(UpdateStoreMeetings(data));
  };
} //POST /api/Meeting/UpdateMeeting
export function UpdateMeetingCalender(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/UpdateMeeting", data)
      .then(response => {
        dispatch(UpdateStoreMeetings(response.data));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => callback(response.response));
  };
}
export function UpdateMeetingFollowupAction(meetingId, followupAction) {
  return dispatch => {
    dispatch(UpdateFollowupAction(meetingId, followupAction));
  };
}
export function BulkUpdateMeeting(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/BulkMeetingUpdate", data)
      .then(response => {
        // Type == 5, means it is delete operation
        if (data.type == 5) {
          dispatch(UpdateBulkStoreMeeting(response.data, true));
        } else {
          dispatch(UpdateBulkStoreMeeting(response.data));
        }
        return response;
      })
      .then(_data => {
        callback(null, _data.data);
      })
      .catch(response => callback(response.response, null));
  };
}
//////////////Schedules Archive Un-Archive ////////////////////
export function ArchiveMeetingSchedule(data, parentId, callback) {
  return dispatch => {
    return instance()
      .get("/api/Meeting/ArchiveMeeting?meetingId=" + data + "&parentMeetingId=" + parentId)
      .then(response => {
        dispatch(UpdateBulkStoreMeeting(response.data));
        return response;
      })
      .then(_data => {
        callback(null, _data.data);
      })
      .catch(response => callback(response.response, null));
  };
}

export function UnArchiveMeetingSchedule(data, parentId, callback) {
  return dispatch => {
    return instance()
      .get("/api/Meeting/UnarchiveMeeting?meetingId=" + data + "&parentMeetingId=" + parentId)
      .then(response => {
        dispatch(UpdateBulkStoreMeeting(response.data));
        return response;
      })
      .then(_data => {
        callback(null, _data.data);
      })
      .catch(response => callback(response.response, null));
  };
}
export function repeatMeeting(data, callback, failure, updateRepeatMeeting) {
  const API = updateRepeatMeeting ? "api/Meeting/UpdateRepeatMeeting" : "/api/meeting/CreateRepeatMeeting";
  return dispatch => {
    return instance()
      .post(API, data)
      .then(response => {
        dispatch(UpdateStoreMeetings(response.data));
        return response;
      })
      .then(response => {
        if(grid.grid){
          const rowNode = grid.grid && grid.grid.getRowNode(response.data.id);
          rowNode && rowNode.setData(response.data);
        }
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function deleteMeetingSchedule(meetingId, success, failure) {
  return dispatch => {
    return instance()
      .get("/api/meeting/DeleteMeetingSchedule/" + meetingId)
      .then(response => {
        dispatch(UpdateStoreMeetings(response.data));
        return response;
      })
      .then(response => {
        if(grid.grid){
          const rowNode = grid.grid && grid.grid.getRowNode(response.data.id);
          rowNode && rowNode.setData(response.data);
        }
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function CancelMeetingSchedule(data, parentId, callback) {
  return dispatch => {
    return instance()
      .get("/api/Meeting/CancelMeeting?meetingId=" + data + "&parentMeetingId=" + parentId)
      .then(response => {
        dispatch(UpdateBulkStoreMeeting(response.data));
        return response;
      })
      .then(_data => {
        callback(null, _data.data);
      })
      .catch(response => callback(response.response, null));
  };
}

export function DeleteMeetingSchedule(data, parentId, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/Meeting/DeleteMeeting?meetingId=" + data + "&parentMeetingId=" + parentId)
      .then(response => {
        dispatch(RemoveMeetingSchedule(data, parentId));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => failure(response.response));
  };
}

export function CreateMeetingSchedule(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/CreateMeetingSchedule", data)
      .then(response => {
        dispatch(StoreCopyMeetingScheduleInfo(response.data, data.meetingId));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => failure(response.response));
  };
}

export function PublishMomEmail(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/Meeting/PublishMomEmail", data)
      .then(response => {
        callback(response);
      })
      .catch(response => failure(response.response));
  };
}

export function createNextOccurrence(data, success, failure) {
  return dispatch => {
    return instance()
      .post("/api/meeting/CreateMeetingScheduleManual", data)
      .then(response => {
        let { oldMeeting, newMeeting } = response.data;
        dispatch(UpdateStoreMeetings(oldMeeting));
        // dispatch(AddMeetingToStore(newMeeting));

        // dispatch(tc.PopulateTaskActivities(oldTask.activityLog));
        // dispatch(tc.PopulateTaskNotifications(oldTask.notification));

        dispatch(StoreCopyMeetingInfo(newMeeting));
        dispatch(order.FetchItemOrderInfo(() => { }));
        success(oldMeeting);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
// bulk actions starts here

export function deleteBulkMeetingAction(data) {
  return {
    type: constants.DELETE_BULK_MEETING,
    payload: data,
  };
}
export function deleteBulkMeeting(data, dispatch, callback, failure) {
  instance()
    .post("api/meeting/bulkdelete", data)
    .then(res => {
      callback(res.data);
      dispatch(deleteBulkMeetingAction({ meetingIds: res.data }));
    })
    .catch(err => {
      failure(err.response.data);
    });
}

export function exportBulkMeeting(data, dispatch, callback, failure) {
  instance()
    .post("api/export/bulkmeeting", data, {
      responseType: "blob",
    })
    .then(res => {
      // dispatch(updateMeetingDataAction(res.data.entity));
      callback(res);
    })
    .catch((res) => {
      failure(res);
      // dispatch(updateMeetingDataAction(data.task));
    });
}
export function bulkMeetingAction(data) {
  return {
    type: constants.UPDATE_BULK_MEETINGS,
    payload: data,
  };
}
// bulk update for color and attendee
export function updateBulkMeeting(data, dispatch, callback, failure) {
  const { keyType, ...rest } = data;
  instance()
    .post("api/meeting/meetingbulkupdate", rest)
    .then(res => {
      dispatch(bulkMeetingAction(res.data));
      callback(res.data);
      if (grid.grid) grid.grid.redrawRows();
    })
    .catch((err) => {
      failure(err.data);
    });
}
export function archiveBulkMeeting(data, dispatch, callback, failure) {
  instance()
    .post("api/meeting/bulkarchive", data)
    .then(res => {
      let meetingIds = res.data;
      callback(meetingIds);
      dispatch(deleteBulkMeetingAction({ meetingIds }));
    })
    .catch((err) => {
      failure(err.data);
    });
}
export function unArchiveBulkMeeting(data, dispatch, callback, failure) {
  instance()
    .post("/api/meeting/bulkUnarchive", data)
    .then(res => {
      callback(res.data);
      let meetingIds = res.data.map(x => {
        return x.meetingId;
      });
      dispatch({
        type: constants.UNARCHIVE_BULK_MEETING,
        payload: { 'meetingIds': meetingIds },
      });
    })
    .catch((err) => {
      failure(err.data);
    });
}
// quick filters

// filters
export function updateMeetingFilterAction(data) {
  return {
    type: constants.UPDATE_MEETING_FILTER,
    payload: data,
  };
}

export function clearMeetingFilterAction(data) {
  return {
    type: constants.CLEAR_MEETING_FILTER,
  };
}
export function getCustomFilterAction(data) {
  return {
    type: constants.SET_MEETING_CUSTOM_FILTER,
    payload: data,
  };
}
export function deleteMeetingFilterAction(data) {
  return {
    type: constants.DELETE_MEETING_FILTER,
    payload: data,
  };
}
export function addNewCustomFilterAction(data) {
  return {
    type: constants.ADD_NEW_MEETING_CUSTOM_FILTER,
    payload: data,
  };
}
export function editMeetingAction(data) {
  return {
    type: constants.CREATED_MEETING_EDIT,
    payload: data,
  };
}
// filters action starts here
export function getSavedFilters(type, dispatch, callback = () => { }) {
  instance()
    .get("api/advancefilter/" + type)
    .then(res => {
      callback();
      dispatch(getCustomFilterAction(res.data.entity));
    });
}

export function deleteMeetingFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(deleteMeetingFilterAction(data));
  } else {
    return dispatch => {
      dispatch(deleteMeetingFilterAction(data));
    };
  }
}
export function addNewFilter(data, type, dispatch, callback = () => { }) {
  instance()
    .post("api/advancefilter/" + type, data)
    .then(res => {
      callback();
      dispatch(addNewCustomFilterAction(res.data.entity));
    });
}
export function updateMeetingFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(updateMeetingFilterAction(data));
  } else {
    return dispatch => {
      dispatch(updateMeetingFilterAction(data));
    };
  }
}

export function clearMeetingFilter(dispatchFn) {
  if (dispatchFn) {
    dispatchFn(clearMeetingFilterAction());
  } else {
    return dispatch => {
      dispatch(clearMeetingFilterAction());
    };
  }
}

export function createMeetingAction(data) {
  return {
    type: constants.CREATE_MEETING,
    payload: data,
  };
}


export function createMeeting(data, dispatch, dispatchObj, success, failure) {
  dispatch(createMeetingAction(dispatchObj));
  instance()
    .post("/api/meeting/createnewmeeting", data)
    .then(response => {
      if (window.userpilot) {
        /** tracking successfully created Meeting in app */
        window.userpilot.track("Meeting Created Successfully");
      }
      dispatch(editMeetingAction(response.data));
      success(response.data);
    })
    .catch(response => {
      failure(response);
    });
}

export function updateMeetingDataAction(data) {
  return {
    type: constants.UPDATE_MEETING_DATA,
    payload: data,
  };
}

export function updateMeetingData(
  data,
  dispatchFn,
  callback = () => { },
  error = () => { },
  notifyUser
) {
  if (dispatchFn) {
    instance()
      .patch(`/api/meeting/${data.meeting.meetingId}`, data.obj)
      .then(res => {
        dispatchFn(updateMeetingDataAction(res.data.entity));
        callback(res.data.entity);
      })
      .catch(err => {
        error(err.response);
        // dispatchFn(updateMeetingDataAction(data.meeting));
      });
  } else {
    return dispatch => {
      instance()
        .patch(`/api/meeting/${data.meeting.meetingId}`, data.obj)
        .then(res => {
          dispatch(updateMeetingDataAction(res.data.entity));
          callback(res.data.entity);
        })
        .catch(err => {
          error(err.response);
          // dispatch(updateMeetingDataAction(data.meeting));
        });
    };
  }
}

export function showAllMeetingsAction() {
  return {
    type: constants.SHOW_ALL_MEETING,
    payload: {},
  };
}

export function showAllMeetings(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(showAllMeetingsAction(data));
  } else {
    return dispatch => {
      dispatch(showAllMeetingsAction(data));
    };
  }
}
export function updateMeetingQuickFilterAction(data) {
  return {
    type: constants.UPDATE_MEETING_QUICK_FILTER,
    payload: data,
  };
}

export function removeMeetingQuickFilterAction(data) {
  return {
    type: constants.REMOVE_MEETING_QUICK_FILTER,
    payload: data,
  };
}
export function removeMeetingQuickFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(removeMeetingQuickFilterAction(data));
  } else {
    return dispatch => {
      dispatch(removeMeetingQuickFilterAction(data));
    };
  }
}

export function updateQuickFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(updateMeetingQuickFilterAction(data));
  } else {
    return dispatch => {
      dispatch(updateMeetingQuickFilterAction(data));
    };
  }
}
function getArchivedDataAction(data) {
  return {
    type: constants.ADD_BULK_MEETING,
    payload: data,
  };
}
export function getArchivedData(type, callback) {
  // Project = 1,
  //   Task = 2,
  //   Meeting = 3,
  //   Issue = 4,
  //   Risk = 5
  return dispatch => {
    instance()
      .get(`api/workspace/GetArchivedData?type=${type}`)
      .then(response => {
        dispatch(getArchivedDataAction(response.data));
        callback(response);
      })
      .catch(error => {
        callback(error.response);
      });
  }
}
export default {
  FetchMeetingsInfo,
  ResetMeetingsInfo,
  BulkUpdateMeeting,
  DeleteMeeting,
  UpdateMeeting,
  repeatMeeting,
  updateMeetingFilter,
  deleteMeetingFilter,
  clearMeetingFilter,
  createMeeting,
  updateMeetingData,
  deleteBulkMeeting,
  getArchivedData,
};
