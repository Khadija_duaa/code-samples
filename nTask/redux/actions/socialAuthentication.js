import constants from "./../constants/types";
import instance from "../instance";

export function handleSocialLogin(
  provider,
  token,
  tokenSecret,
  callback,
  failure
) {
  return (dispatch) => {
    return instance()
      .post(`${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL }api/account/SocialLogin`, {
        provider,
        token,
        tokenSecret,
      })
      .then((response) => {
        callback(response);
        return response;
      })
      .then((response) => {
        if (response.data.isRegistered) {
          dispatch({
            type: constants.CHECKAUTH,
            payload: true,
          });
        }
      })
      .catch((error) => {
        failure(error);
      });
  };
}

export function handleAccountAttachment(data, callback, failure) {
  return (dispatch) => {
    return instance()
      .post(`${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL }api/account/AttachExternalAccount`, data)
      .then((response) => {
        callback(response);
      })
      .catch((error) => {
        failure(error);
      });
  };
}

export function handleAccountDeattachment(providerName, callback, failure) {
  return (dispatch) => {
    return instance()
      .get(
        `${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL}api/account/DeattachExternalAccount?provider=${providerName}`
      )
      .then((response) => {
        callback(response);
      })
      .catch((error) => {
        failure(error);
      });
  };
}
