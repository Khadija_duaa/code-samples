import constants from "./../constants/types";

export function setSideBarTheme(data) {
    return dispatch => {
        dispatch({
                type: constants.SETSIDEBARTHEME,
                payload: data
            })
        
      }

}
