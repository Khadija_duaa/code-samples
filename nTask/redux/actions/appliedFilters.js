import instance from "../instance";
import constants from "../constants/types";
import { PopulateItemOrder } from "./itemOrder";

export function setAppliedFilters(filters, type, dispatchFn = null) {
  if (dispatchFn) {
    dispatchFn({
      type: constants.SETAPPLIEDFILTERS,
      payload: {
        filters,
        type,
      },
    });
  } else {
    return {
      type: constants.SETAPPLIEDFILTERS,
      payload: {
        filters,
        type,
      },
    };
  }
}

export function clearFilterType(filters, type) {
  return {
    type: constants.CLEARSPECIFICFILTER,
    payload: {
      filters,
      type,
    },
  };
}

export function clearAllAppliedFilters(filters, type) {
  return {
    type: constants.CLEARAPPLIEDFILTERS,
    payload: {
      filters,
      type,
    },
  };
}

export function deleteUserCustomFilters(id) {
  return {
    type: constants.DELETE_TASK_CUSTOM_FILTER,
    payload: id,
  };
}
export function deleteUserIssueCustomFilters(id) {
  return {
    type: constants.DELETE_ISSUE_CUSTOM_FILTER,
    payload: id,
  };
}
export function deleteUserProjectCustomFilters(id) {
  return {
    type: constants.DELETE_PROJECT_CUSTOM_FILTER,
    payload: id,
  };
}
export function deleteUserRiskCustomFilters(id) {
  return {
    type: constants.DELETE_RISK_CUSTOM_FILTER,
    payload: id,
  };
}
export function deleteUserMeetingCustomFilters(id) {
  return {
    type: constants.DELETE_MEETING_CUSTOM_FILTER,
    payload: id,
  };
}
export function deleteUserFilter(filter, filterType, callback, failure, dispatchFn = null) {
  if (dispatchFn) {
    return instance()
      .post("api/workspace/DeleteFilterFromWorkspace", {
        userFilterId: filter.filterId,
        filters: filterType,
      })
      .then(response => {
        dispatchFn(clearFilterType(filter, filterType));
        dispatchFn(PopulateItemOrder(response.data));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .post("api/workspace/DeleteFilterFromWorkspace", {
          userFilterId: filter.filterId,
          filters: filterType,
        })
        .then(response => {
          dispatch(clearFilterType(filter, filterType));
          dispatch(PopulateItemOrder(response.data));
          return response;
        })
        .then(response => {
          callback(response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}
export function deleteUserCustomFilter(filter, filterType, callback, failure, dispatchFn = null) {
  const filterTypeView = filterType.toLowerCase();
  if (dispatchFn) {
    return instance()
      .delete(`api/advancefilter/${filterTypeView}/${filter.filterId}`)
      .then(response => {
        switch (filterTypeView) {
          case "task":
            dispatchFn(deleteUserCustomFilters(filter.filterId));
            return;
            break;
          case "issue":
            dispatchFn(deleteUserIssueCustomFilters(filter.filterId));
            return;
            break;
          case "project":
            dispatchFn(deleteUserProjectCustomFilters(filter.filterId));
            return;
          case "risk":
            dispatchFn(deleteUserRiskCustomFilters(filter.filterId));
            return;
            break;
          case "meeting":
            dispatchFn(deleteUserMeetingCustomFilters(filter.filterId));
            return;
            break;
        }
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .delete(`api/advancefilter/${filterTypeView}/${filter.filterId}`)
        .then(response => {
          switch (filterTypeView) {
            case "task":
              dispatch(deleteUserCustomFilters(filter.filterId));
              return;
              break;
            case "issue":
              dispatch(deleteUserIssueCustomFilters(filter.filterId));
              return;
              break;
            case "project":
              dispatch(deleteUserProjectCustomFilters(filter.filterId));
              return;
            case "risk":
              dispatch(deleteUserRiskCustomFilters(filter.filterId));
              return;
              break;
            case "meeting":
              dispatch(deleteUserMeetingCustomFilters(filter.filterId));
              return;
              break;
          }
          return response;
        })
        .then(response => {
          callback(response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}
export function setDefaultfilter(
  filter,
  filterType,
  value,
  callback = () => { },
  failure = () => { },
  dispatchFn = null
) {
  const filterTypeView = filterType.toLowerCase();
  const type = `${filterType.toLowerCase()}Filter`;
  if (dispatchFn) {
    return instance()
      .put(`/api/advancefilter/${filterTypeView}`, {
        [type]: { filterId: filter.filterId, defaultFilter: value },
      })
      .then(response => {
        switch (filterTypeView) {
          case "task":
            dispatchFn({
              type: constants.SET_TASK_CUSTOM_FILTER,
              payload: response.data.entity,
            });
            return;
            break;
          case "issue":
            dispatchFn({
              type: constants.SET_ISSUE_CUSTOM_FILTER,
              payload: response.data.entity,
            });
            return;
            break;
          case "project":
            dispatchFn({
              type: constants.SET_PROJECT_CUSTOM_FILTER,
              payload: response.data.entity,
            });
            return;
          case "risk":
            dispatchFn({
              type: constants.SET_RISK_CUSTOM_FILTER,
              payload: response.data.entity,
            });
            return;
            break;
          case "meeting":
            dispatchFn({
              type: constants.SET_MEETING_CUSTOM_FILTER,
              payload: response.data.entity,
            });
            return;
            break;
        }

        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .put(`/api/advancefilter/${filterTypeView}`, {
          [type]: { filterId: filter.filterId, defaultFilter: true },
        })
        .then(response => {
          switch (filterTypeView) {
            case "task":
              dispatch({
                type: constants.SET_TASK_CUSTOM_FILTER,
                payload: response.data.entity,
              });
              return;
              break;
            case "issue":
              dispatch({
                type: constants.SET_ISSUE_CUSTOM_FILTER,
                payload: response.data.entity,
              });
              return;
              break;
            case "project":
              dispatch({
                type: constants.SET_PROJECT_CUSTOM_FILTER,
                payload: response.data.entity,
              });
              return;
            case "risk":
              dispatch({
                type: constants.SET_RISK_CUSTOM_FILTER,
                payload: response.data.entity,
              });
              return;
              break;
            case "meeting":
              dispatch({
                type: constants.SET_MEETING_CUSTOM_FILTER,
                payload: response.data.entity,
              });
              return;
              break;
          }

          return response;
        })
        .then(response => {
          callback(response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}
