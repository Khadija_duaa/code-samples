import constants from "./../constants/types";
import instance from "./../instance";
import axios from "axios";
import { UpdateUserProfileImage } from "./profile";
import { isMobileDevice } from "../../utils/common";
import { PopulateWhiteLabelInfo } from "./teamMembers";

export const getCompanyData = (teamId, success, failure) => {
  axios.get(`${constants.WHITELABELURL}?teamId=${teamId}`)
    .then(response => {
      success(response.data.data)
      dispatch(PopulateWhiteLabelInfo(response.data.data));
    }).catch((response) => {
   failure(response)
  });
};
export function createProfile(data, callback, failure) {

  return (dispatch) => {
    return instance()
      .post("/api/Account/onboarding", data)
      .then((response) => {
        switch(data.plan) {
          case 'business':
            if(window.dataLayer){
              window.dataLayer.push({'event':'BusinessTrial-TeamOwner'});
            }
            break;
          case 'premium':
            if(window.dataLayer){
              window.dataLayer.push({'event':'PremiumTrial-TeamOwner'});
            }
            break;
          case 'premiumtrial':
            if(window.dataLayer){
              window.dataLayer.push({'event':'PremiumTrial-TeamOwner'});
            }
            break;
          case 'businesstrial':
            if(window.dataLayer){
              window.dataLayer.push({'event':'BusinessTrial-TeamOwner'});
            }
            break;
          case '':
            if(window.dataLayer){
              window.dataLayer.push({'event':'Free'});
            }
            break;
          default:
          // code block
        }
        callback(response);
        /**
         * when we enable Go to App from MobileDevice component then enable token
         */
        if(!isMobileDevice()){
          localStorage.setItem(
            "token",
            `Bearer ${response.data.data.access_token}`
          );
          dispatch({
            type: constants.CHECKAUTH,
            payload: true,
          });
        }
      })
      .catch((res) => {
        failure(res.response);
      });
  };
}
export function createSocialProfile(data, token, callback, failure, dispatchFn) {
  if(dispatchFn){
    instance()
      .post("/api/Account/onboardingsocial", data)
      .then((response) => {
        callback(response);
        /**
         * when we enable Go to App from MobileDevice component then enable token
         */
        if(!isMobileDevice()){
          localStorage.setItem(
            "token",
            `Bearer ${response.data.data.access_token}`
          );
          dispatchFn({
            type: constants.CHECKAUTH,
            payload: true,
          });
        }
        // localStorage.setItem(
        //   "token",
        //   `Bearer ${response.data.data.access_token}`
        // );
        // dispatch({
        //   type: constants.CHECKAUTH,
        //   payload: true,
        // });
      })
      .catch((res) => {
        failure(res.response);
      });
  } else {
    return (dispatch) => {
      return instance()
        .post("/api/Account/onboardingsocial", data, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((response) => {
          callback(response);
          /**
           * when we enable Go to App from MobileDevice component then enable token
           */
          if (!isMobileDevice()) {
            localStorage.setItem(
              "token",
              `Bearer ${response.data.data.access_token}`
            );
            dispatch({
              type: constants.CHECKAUTH,
              payload: true,
            });
          }
          // localStorage.setItem(
          //   "token",
          //   `Bearer ${response.data.data.access_token}`
          // );
          // dispatch({
          //   type: constants.CHECKAUTH,
          //   payload: true,
          // });
        })
        .catch((res) => {
          failure(res.response);
        });
    };
  }
}

export function uploadprofileimage(
  FormData,
  callback,
  failure,
  isStoreUpdated = true
) {
  return (dispatch) => {
    return instance()
      .post("/api/FileUpload/UploadProfileImage", FormData)
      .then((response) => {
        if (isStoreUpdated) {
          dispatch(UpdateUserProfileImage(response.data));
        }
        return response;
      })
      .then((response) => {
        callback(response);
      })
      .catch((res) => {
        failure(res.response);
      });
  };
}

export function IsValidInvitedUser(data, callback, failure) {
  instance()
    .post("api/account/IsValidInvitedUser", data)
    .then((response) => {
      callback();
    })
    .catch((response) => {
      failure(response.data);
    });
}
export function validateInvitationToken(token, callback, failure) {
  instance()
    .get("api/Account/validateInvitationToken?token=" + token)
    .then((response) => {
      callback(response);
    })
    .catch((response) => {
      failure(response.data);
    });
}
export function getProfileImage(email, callback, failure) {
  return (dispatch) => {
    return instance()
      .get(`api/account/GetProfileImageByEmail?email=${email}`)
      .then((response) => {
        callback(response);
      })
      .catch((response) => {
        failure(response.data);
      });
  };
}

export function ValidateToken(token, callback) {
  return (dispatch) => {
    return instance()
      .post("/api/Account/ValidateToken", token)
      .then((response) => {
        callback(response);
      })
      .catch((response) => {
        callback(response.data);
      });
  };
}

export function isUserValid(email, callback, failure) {
  return (dispatch) => {
    return instance()
      .get(`/api/Account/IsUserValid?email=${email}`)
      .then((response) => {
        callback(response);
      })
      .catch((response) => {
        failure(response.data);
      });
  };
}

export function isUserNameUnique(userName, callback, failure) {
  return (dispatch) => {
    return instance()
      .get("/api/NTask/IsUserNameUnique?userName=" + userName)
      .then((response) => {
        callback(response);
      })
      .catch((err) => {
        let res = err.response.data ? err.response.data : null;
        failure(res);
      });
  };
}

export function updateExternalProfile(user, callback) {
  return (dispatch) => {
    return instance()
      .post("/api/account/UpdateExternalProfile", user)
      .then((response) => {
        callback(response);
      })
      .catch((response) => {
        callback(response.data);
      });
  };
}

export function addCompanyInformation(request, callback, failure) {
  return (dispatch) => {
    instance()
      .post("/api/Account/AddCompanyInformation", request)
      .then((response) => {
        callback(response);
      })
      .catch((response) => {
        failure(response.data);
      });
  };
}
//Create team from onboarding process
export function addTeam(payload, callback, failure) {
  instance()
    .post("/api/account/CreateUserTeam", payload)
    .then((response) => {
      callback(response);
    })
    .catch((err) => {
      failure(err.response);
    });
}
//Create team from dashboard when user already have teams
export function createTeam(payload, callback, failure) {
  instance()
    .post("/api/company/CreateTeam", payload)
    .then((response) => {
      callback(response);
    })
    .catch((err) => {
      failure(err.response);
    });
}
//Create team for the user who have workspaces and no team e.g. existing user
export function createFirstTeam(payload, callback, failure) {
  instance()
    .post("/api/company/CreateUserTeam", payload)
    .then((response) => {
      callback(response);
    })
    .catch((err) => {
      failure(err.response);
    });
}
// export function createTeam(payload, callback, failure) {
//   instance()
//       .post("/api/company/CreateUserTeam", payload)
//       .then(response => {
//         callback(response);
//       })
//       .catch(err => {
//         failure(err.response);
//       });
// }

export function IsWorkspaceUrlValid(Url, callback) {
  return (dispatch) => {
    return instance()
      .get("/api/Team/IsWorkspaceUrlValid?workspaceUrl=" + Url)
      .then((response) => {
        callback(response);
      })
      .catch((response) => {
        callback(response.data);
      });
  };
}

export function createusercompany(object, callback, failure) {
  return (dispatch) => {
    return instance()
      .post("/api/account/CreateUserCompany", object)
      .then((response) => {
        callback(response);
      })
      .catch((response) => {
        failure(response.response.data);
      });
  };
}

export function createWorkspace(object, callback) {
  instance()
    .post("/api/team/saveteam", object)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      callback(response.data);
    });
}

export function getYoutubeVideos(callback) {
  return (dispatch) => {
    return instance()
      .get("api/video/getvideos")
      .then((response) => {
        callback(response);
      })
      .catch((response) => {
        callback(response.data);
      });
  };
}

export function UpdateUserState(callback) {
  return (dispatch) => {
    return instance()
      .post("api/account/UpdateUserState")
      .then((response) => {
        callback(response);
      })
      .catch((response) => {
        callback(response.data);
      });
  };
}

///api/account/UpdateUserState
//api/video/getvideos
export default {
  uploadprofileimage,
  isUserNameUnique,
  addCompanyInformation,
  IsWorkspaceUrlValid,
  createusercompany,
  createWorkspace,
  UpdateUserState,
  getYoutubeVideos,
};
