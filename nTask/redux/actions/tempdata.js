import constants from "./../constants/types";

export function PopulateTempData(data) {
  //
  return {
    type: constants.POPULATETEMPDATA,
    payload: {
      data,
    }
  };
}


export function UpdateTempData(data) {
    return {
      type: constants.UpdateTempData,
      payload: {
        data,
      }
    };
  }


  export function DeleteTempData() {
    return {
      type: constants.DELETETETEMPDATA
    };
  }

  export default {
    DeleteTempData,
    UpdateTempData,
    PopulateTempData
};