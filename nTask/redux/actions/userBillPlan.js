import constants from "../constants/types";
import instance from "../instance";

export function upgradeStartTrialPlanBilling(data) {
    return {
        type: constants.STARTTRIALPLANBILLING,
        payload: {
            data
        }
    };
}

export function upgradeStartFreePlanBilling(data) {
    return {
        type: constants.STARTFREEPLANBILLING,
        payload: {
            data
        }
    };
}

export function UpgradeToPremiumPlan(data) {
    return {
        type: constants.UPGRADEPREMIUMBILLPLAN,
        payload: {
            data
        }
    };
}

export function UpgradeToBusinessPlan(data) {
    return {
        type: constants.UPGRADEBUSINESSBILLPLAN,
        payload: {
            data
        }
    };
}

export function DowngradeToFreePlan(data) {
    return {
        type: constants.DOWNGRADEFREEBILLPLAN,
        payload: {
            data
        }
    };
}

export function CancelSubscription(data) {
    return {
        type: constants.CANCELSUBSCRIPTION,
        payload: {
            data
        }
    };
}

export function UpdateSubscriptionPlan(data) {
    return {
        type: constants.UPDATESUBSCRIPTION,
        payload: {
            data
        }
    };
}

export function UpdatePromo(data) {
    return {
        type: constants.UPDATEPROMOCODE,
        payload: {
            data
        }
    };
}

export const upgradePlanShowHideAction = (data) => ({
    type: constants.VISIBLEUPGRADEPLAN,
    payload: {
        data
    }
})

export function StartFreePlanBilling(data, success, failure) {
    return dispatch => {
        return instance().post("/api/payment/StartBasic", data)
            .then((response) => {
                dispatch(DowngradeToFreePlan({
                    companyInfo: response.data.company
                }));
                return success(response);
            })
            .catch(error => {
                failure(error.response)
            });
    };
}


export function StartTrialPlanBilling(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/StartFreeTrial", data)
        .then((response) => {
            switch(data.mode) {
                case 'Business':
                    if(window.dataLayer){
                        window.dataLayer.push({'event':'Business Trial Users'});
                    }
                    break;
                case 'Premium':
                    if(window.dataLayer){
                        window.dataLayer.push({'event':'Premium Trial Users'});
                    }
                    break;
                default:
              // code block
            }

                return success(response);
            })
            .catch(error => {
                fail(error.response)
            });
    };
}
export function UpgradeTrailInStore(response,success) {
    return dispatch => {
        dispatch(upgradePlanShowHideAction({show: false, mode: null}))
        dispatch(upgradeStartTrialPlanBilling({
            companyInfo: response.data.company
        }));
        success();
    }
}


export function UpgradePremiumPlan(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/UpgradePlan", data)
            .then((response) => {
                success(response);
            })
            .catch(error => {
                fail(error.response)
            });
    };
}
export function UpgradePremiumPlanState(data) {
    return dispatch => {
        dispatch(UpgradeToPremiumPlan(data));
    };
}

export function FetchPaymentHistory(data, success, fail) {
    return dispatch => {
        return instance().get("/api/payment/getinvoices?teamId=" + data)
            .then((response) => {
                success(response);
            })
            .catch(error => {
                fail(error.response);
            });
    };
}

export function GetInvoiceById(data, success, fail) {
    return dispatch => {
        return instance().get("/api/payment/GetInvoiceById?invoiceId=" + data)
            .then((response) => {
                success(response);
            })
            .catch(error => {
                fail(error.response);
            });
    };
}

export function FetchPaymentMethod(success, fail) {
    return dispatch => {
        return instance().get("/api/payment/GetCustomerDetails")
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response);
            });
    };
}

export function FetchPackagePlans(success, fail) {
    return dispatch => {
        return instance().get("/api/payment/GetPaymentPlans")
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response);
            });
    };
}
export function GetPackagePlans(success, fail) {
        instance().get("/api/payment/GetPaymentPlans")
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response);
            });

}

export function FetchPricePlans(success, fail) {
    return dispatch => {
        return instance().get("/api/payment/GetPricePlans")
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response);
            });
    };
}

export function AddCustomerPaymentMethod(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/AddPaymentMethod", data)
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response);
            });
    };
}

export function RemoveCustomerPaymentMethod(cardId, success, fail) {
    return dispatch => {
        return instance().get("/api/payment/DeletePaymentMethod?cardId=" + cardId)
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response);
            });
    };
}

export function CancelCustomerSubscription(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/CancelSubscription", data)
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response)
            });
    };
}

export function CancelCustomerSubscriptionState(data, success, fail) {
    return dispatch => {
        dispatch(CancelSubscription(data));
        let response = { data: true }
        return success(response);
    }
}

export function FetchCustomerSubscription(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/CalculateAmount", data)
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response)
            });
    };
}

export function FetchCustomerAddress(success, fail) {
    return dispatch => {
        return instance().get("/api/payment/GetCustomerAddress")
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response)
            });
    };
}

export function UpdateBillingInfo(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/UpdateBillingInfo", data)
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response)
            });
    };
}

export function UpdateCustomerSubscription(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/UpdateSubscription", data)
            .then((response) => {
                dispatch(UpdateSubscriptionPlan({
                    companyInfo: response.data.company
                }));
                success(response);
            })
            .catch(error => {
                fail(error.response);
            });
    };
}

export function SetCustomerCardDefault(data, success, fail) {
    return dispatch => {
        return instance().get("/api/payment/SetCardAsDefault?cardId=" + data)
            .then((response) => {
                success(response)
            })
            .catch(error => {
                fail(error.response);
            });
    };
}

export function FetchPromotionCode(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/GetPromotionCode", data)
            .then((result) => {
                success(result);
            })
            .catch(error => {
                fail(error.response);
            });
    };
}

export function MakePaymentPlan(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/PayDueInvoice", data)
            .then((response) => {
                success(response);
            })
            .catch(error => {
                fail(error.response)
            });
    };
}

export function FetchDueInvoice(data, success, fail) {
    return dispatch => {
        return instance().get("/api/payment/GetDueInvoice?teamId=" + data)
            .then((response) => {
                success(response);
            })
            .catch(error => {
                fail(error.response)
            });
    };
}

export function UpdatePromoCodeValue(data, success, fail) {
    return dispatch => {
        dispatch(UpdatePromo(data));
        let response = { data: true }
        return success(response);
    }
}

export function showUpgradePlan(data) {
    return dispatch => {
        return dispatch(upgradePlanShowHideAction(data))
    };
}
export function GetRatesForLocation(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/GetRatesForLocation", data)
            .then((response) => {
                success(response);
            })
            .catch(error => {
                fail(error.response)
            });
    };
}
export function ValidateVAT(data, success, fail) {
    return dispatch => {
        return instance().get("/api/payment/ValidateVAT?vat=" + data)
            .then((response) => {
                success(response);
            })
            .catch(error => {
                fail(error.response)
            });
    };
}
export function GetSecretIntent(data, success, fail) {
    return dispatch => {
        return instance().post("/api/payment/getintent",data)
            .then((response) => {
                success(response);
            })
            .catch(error => {
                fail(error.response)
            });
    };
}