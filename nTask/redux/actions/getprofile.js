import constants from "./../constants/types";
import instance from "./../instance";

export function PopulateGetProfile(data) { //Populate
  return {
    type: constants.POPULATEGETPROFILE,
    payload: {
      data,
    }
  };
}
export function ResetGetProfile() {
  return {
    type: constants.RESETGETPROFILE
  };
}

export function FetchGetProfile(callback) {
  return dispatch => {
    return instance().get("/api/ntask/getprofile")
      .then((response) => {
        return dispatch(PopulateGetProfile(response.data));
      }).then(() => {
        callback()
      });
    return null;
  };
}

export default {
  FetchGetProfile,
  ResetGetProfile
};