import constants from "../constants/types";
import instance from "../instance";


export function getWorkspaceStats(callback) {
    
    return dispatch => {
        return instance().get("/api/wssr/workspacestatistic")
        .then((response) => {
            // return dispatch({
            //     type: constants.GETWORKSPACESTATS,
            //     payload: response.data
            //   });
            callback(response.data)
        })
    }
}
export function exportOverviewIssues(callback) {
    instance()
      .get("api/export/issueoverview", {
          responseType: "blob",
      })
      .then(res => {
          callback(res);
      })
      .catch(() => {
      });
}export function exportOverviewTasks(callback) {
    instance()
      .get("api/export/taskoverview", {
          responseType: "blob",
      })
      .then(res => {
          callback(res);
      })
      .catch(() => {
      });
}
export function getWorkspaceListCols(callback) {
    return dispatch => {
        return instance().get("api/workspace/GetWorkspaceColumn")
        .then((response) => {
            return dispatch({
                type: constants.WORKSPACEREPORTCOLUMNORDER,
                payload: response.data
              });
        }).then(()=>{
            callback()
        })
    }
}

export function saveWorkspaceReportColumnOrder(payload) {
    return dispatch => {
        instance().post("api/workspace/SaveWorkspaceColumn", payload)
        return dispatch({
            type: constants.UPDATECOLUMNORDER,
            payload: payload.reportColumnOrder
          });
        
    }
}
