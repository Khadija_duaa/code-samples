import constants from "./../constants/types";
import instance from "../instance";

export function PopulateWorkspaceActivityInfo(data,pageNo) {
  return {
    type: constants.POPULATEWORKSPACEACTIVITY,
    payload: {
      data,
      pageNo
    }
  };
}

export function GetWorkspaceActivityLog(pageNo, callback) {
  return dispatch => {
    return instance()
      .get("api/activitylog/getuserlog?pageNo=" + pageNo)
      .then(response => {
        dispatch(PopulateWorkspaceActivityInfo(response.data.results,pageNo));
        return { response };
      })
      .then(data => {
        callback(null, data.response.data);
      })
      .catch(response => {
        callback(response ? response.response.data : "Error", null);
      });
  };
}
export default {
  GetWorkspaceActivityLog
};
