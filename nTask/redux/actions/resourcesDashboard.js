import instance from "../instance";

export function getWidgetAnalyticsData(query, success) {
  instance()
    .get("/api/widget/projectresource")
    .then(res => {
      success(res);
    });
}

export function getProjectAnalyticsData(success) {
  instance()
    .get("/api/widgetSettting/projectresource")
    .then(res => {
      success(res.data.entity);
    });
}
export function updateProjectAnalyticsData(data, success) {
  instance()
    .put("/api/widgetSettting/projectresource", data)
    .then(res => {
      success(res.data.entity);
    });
}
