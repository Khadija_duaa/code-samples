//import constants from "./../constants/types";
import instance from "../instance";

export function SendFeedback(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/NTask/SendFeedbackEmail", data)
      .then(response => {
        //
        //history.replace("/tasks")
        callback(response);
      }).catch(response => {
        failure(response.response);
      });
  };
}
export function SaveFreshchatData(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/ntask/SaveFreshchatData", data)
      .then(response => {
        callback(response);
      }).catch(response => {
        callback(response.response);
      });
  };
}

export default {
  SendFeedback,
  SaveFreshchatData
};
