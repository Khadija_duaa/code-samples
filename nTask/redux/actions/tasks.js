import constants from "./../constants/types";
import instance from "../instance";
import action from "./workspace";
import calender from "./calenderTasks";
import order from "./itemOrder";
import tc from "./taskComments";
import todo from "./todoItems";
import cloneDeep from "lodash/cloneDeep";
import { DeleteArchiveTaskFromBoard } from "../actions/boards";
import { updateGlobalTimeTask, removeGlobalTaskTime } from "./globalTimerTask";
import data from "../../utils/permissionsData";
import { grid } from "../../components/CustomTable2/gridInstance";
import axios from "axios";

export function getTasksAction(data) {
  return {
    type: constants.SAVE_ALL_TASKS,
    payload: data,
  };
}

export function createTaskAction(data) {
  return {
    type: constants.CREATE_TASK,
    payload: data,
  };
}

export function editTaskAction(data) {
  return {
    type: constants.CREATED_TASK_EDIT,
    payload: data,
  };
}

export function addNewCustomFilterAction(data) {
  return {
    type: constants.ADD_NEW_CUSTOM_FILTER,
    payload: data,
  };
}

export function getCustomFilterAction(data) {
  return {
    type: constants.SET_TASK_CUSTOM_FILTER,
    payload: data,
  };
}

export function getTasks(param, dispatchfn, callback = () => { }, failure = () => { }) {
  if (dispatchfn) {
    instance()
      .get("api/tasks?records=" + 1)
      .then(res => {
        dispatchfn(getTasksAction(res.data.entity));
        callback(res.data.entity);
      });
  } else {
    return dispatch =>
      instance()
        .get("api/tasks?records=" + 1)
        .then(res => {
          dispatch(getTasksAction(res.data.entity));
          callback();
        });
  }
}
export function updateTaskCountAction(data) {
  return {
    type: constants.UPDATE_TASK_COUNT,
    payload: data,
  };
}
export function addNewFilter(data, type, dispatch, callback = () => { }) {
  instance()
    .post("api/advancefilter/" + type, data)
    .then(res => {
      callback();
      dispatch(addNewCustomFilterAction(res.data.entity));
    });
}

export function getSavedFilters(type, dispatch, callback = () => { }) {
  instance()
    .get("api/advancefilter/" + type)
    .then(res => {
      callback();
      dispatch(getCustomFilterAction(res.data.entity));
    });
}

export function PopulateDefaultWSTaskTemplate(data) {
  return {
    type: constants.UPDATE_TASK_STATUS_AFTER_SETTING_WORKSPACE_DEFAULT,
    payload: {
      data,
    },
  };
}

export function PopulateDefaultProjectTaskTemplate(data) {
  return {
    type: constants.UPDATE_TASK_STATUS_AFTER_SETTING_PROJECT_DEFAULT,
    payload: {
      data,
    },
  };
}

export function PopulateTasksInfo(data) {
  return {
    type: constants.POPULATETASKSINFO,
    payload: {
      data,
    },
  };
}

export function dispatchTasks(data) {
  return dispatch => {
    dispatch({
      type: constants.POPULATETASKSINFO,
      payload: {
        data,
      },
    });
  };
}

export function UpdateBulkStoreTasks(data) {
  return {
    type: constants.UPDATEBULKTASKINFO,
    payload: {
      data,
    },
  };
}

export function ArchivedBulkStoreTasks(data) {
  return {
    type: constants.ARCHIVEDBULKTASK,
    payload: {
      data,
    },
  };
}

export function DeleteBulkStoreTasks(data) {
  /* dispatching action for bulk delete task  */
  return {
    type: constants.DELETEBULKTASK,
    payload: {
      data,
    },
  };
}

export function ResetTasksInfo() {
  return {
    type: constants.RESETTASKSINFO,
  };
}

export function StoreCopyTaskInfo(data) {
  return {
    type: constants.COPYTASKS,
    payload: {
      data,
    },
  };
}

export function StoreDeleteTaskInfo(data) {
  return {
    type: constants.DELETETASK,
    payload: {
      data,
    },
  };
}

export function deleteTaskAction(data) {
  return {
    type: constants.DELETETASK,
    payload: {
      data,
    },
  };
}

export function StoreArchiveTaskInfo(data) {
  return {
    type: constants.ARCHIVETASK,
    payload: {
      data,
    },
  };
}

//
export function updateTaskRiskData(data) {
  //Continue From Here
  return {
    type: constants.UPDATETASKRISK,
    payload: {
      data,
    },
  };
}

export function updateTaskMeetingData(data) {
  //Continue From Here
  return {
    type: constants.UPDATETASKMEETING,
    payload: {
      data,
    },
  };
}

export function MarkRead(taskId, currentUser) {
  return {
    type: constants.MARKREADTASK,
    payload: {
      taskId,
      currentUser,
    },
  };
}

export function updateTaskIssuesData(data) {
  //Continue From Here
  return {
    type: constants.UPDATETASKISSUE,
    payload: {
      data,
    },
  };
}

export function addBulkTask(data) {
  return {
    type: constants.ADDBULKTASKS,
    payload: data,
  };
}

export function updateTaskAction(data) {
  return {
    type: constants.UPDATETASK,
    payload: data,
  };
}

export function updateTaskDataAction(data) {
  return {
    type: constants.UPDATE_TASK_DATA,
    payload: data,
  };
}

export function saveTaskGridInstance(data) {
  return {
    type: constants.SAVE_TASK_GRID_INSTANCE,
    payload: data,
  };
}

export function updateTaskFilterAction(data) {
  return {
    type: constants.UPDATE_TASK_FILTER,
    payload: data,
  };
}

export function clearTaskFilterAction(data) {
  return {
    type: constants.CLEAR_TASK_FILTER,
  };
}

export function deleteTaskFilterAction(data) {
  return {
    type: constants.DELETE_TASK_FILTER,
    payload: data,
  };
}

export function updateTaskOrder(data) {
  return {
    type: constants.UPDATE_TASK_ORDER,
    payload: data,
  };
}
export function updateIssueOrder(data) {
  return {
    type: constants.UPDATE_ISSUE_ORDER,
    payload: data,
  };
}
export function updateProjectOrder(data) {
  return {
    type: constants.UPDATE_PROJECT_ORDER,
    payload: data,
  };
}
export function updateMeetingOrder(data) {
  return {
    type: constants.UPDATE_MEETING_ORDER,
    payload: data,
  };
}
export function updateRiskOrder(data) {
  return {
    type: constants.UPDATE_RISK_ORDER,
    payload: data,
  };
}

export function bulkTaskAction(data) {
  return {
    type: constants.UPDATE_BULK_TASK,
    payload: data,
  };
}

export function deleteBulkTaskAction(data) {
  return {
    type: constants.DELETE_BULK_TASK,
    payload: data,
  };
}

export function updateTaskQuickFilterAction(data) {
  return {
    type: constants.UPDATE_TASK_QUICK_FILTER,
    payload: data,
  };
}

export function removeTaskQuickFilterAction(data) {
  return {
    type: constants.REMOVE_TASK_QUICK_FILTER,
    payload: data,
  };
}

export function showAllTasksAction() {
  return {
    type: constants.SHOW_ALL_TASK,
    payload: {},
  };
}

export function updateQuickFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(updateTaskQuickFilterAction(data));
  } else {
    return dispatch => {
      dispatch(updateTaskQuickFilterAction(data));
    };
  }
}

export function updateTaskFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(updateTaskFilterAction(data));
  } else {
    return dispatch => {
      dispatch(updateTaskFilterAction(data));
    };
  }
}

export function clearTaskFilter(dispatchFn) {
  if (dispatchFn) {
    dispatchFn(clearTaskFilterAction());
  } else {
    return dispatch => {
      dispatch(clearTaskFilterAction());
    };
  }
}

export function deleteTaskFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(deleteTaskFilterAction(data));
  } else {
    return dispatch => {
      dispatch(deleteTaskFilterAction(data));
    };
  }
}

export function removeTaskQuickFilter(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(removeTaskQuickFilterAction(data));
  } else {
    return dispatch => {
      dispatch(removeTaskQuickFilterAction(data));
    };
  }
}

export function showAllTasks(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(showAllTasksAction(data));
  } else {
    return dispatch => {
      dispatch(showAllTasksAction(data));
    };
  }
}

export function uploadDescriptionImage(_FormData) {
  return instance().post("api/docsfileuploader/UploadMultipleDocsFileAmazonS3", _FormData); // old api integration
  // return axios.post("https://fm.naxxa.io/api/file/uploads", _FormData); // New Api Integration
}

export function saveDescriptionImage(_FormData) {
  return instance().post("api/communication/SaveAttachment", _FormData);
}

export function createTask(data, dispatch, dispatchObj, success, failure) {
  dispatch(createTaskAction(dispatchObj));
  instance()
    .post("/api/usertask/createusertask", data)
    .then(response => {
      if (window.userpilot) {
        /** tracking successfully created task in app */
        window.userpilot.track("Task Created Successfully");
      }
      dispatch(editTaskAction(response.data.task));
      success(response.data.task);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function updateTaskData(
  data,
  dispatchFn,
  callback = () => { },
  error = () => { },
  notifyUser
) {
  if (dispatchFn) {
    instance()
      .patch(`/api/task/${data.task.taskId}`, data.obj)
      .then(res => {
        dispatchFn(updateTaskDataAction(res.data.entity));
        callback(res.data.entity);
      })
      .catch(err => {
        error(err.response);
        dispatchFn(updateTaskDataAction(data.task));
      });
  } else {
    return dispatch => {
      instance()
        .patch(`/api/task/${data.task.taskId}`, data.obj)
        .then(res => {
          dispatch(updateTaskDataAction(res.data.entity));
          callback(res.data.entity);
        })
        .catch(err => {
          error(err.response);
          dispatch(updateTaskDataAction(data.task));
        });
    };
  }
}

export function updateBulkTask(data, dispatch, callback, failure) {
  const { type, ...rest } = data;
  instance()
    .put("/api/usertask/updateBulkTasks", rest)
    .then(res => {
      dispatch(bulkTaskAction(res.data));
      callback(res.data);
    })
    .catch(() => {
      failure();
    });
}

export function exportBulkTask(data, dispatch, callback, failure) {
  instance()
    .put("api/UserTask/ExportBulkTasks", data, {
      responseType: "blob",
    })
    .then(res => {
      // dispatch(updateTaskDataAction(res.data.entity));
      callback(res);
    })
    .catch((res) => {
      failure(res)
    });
}
export function exportBulkOverviewTask(data, callback, fail) {
  return dispatch => {
    return instance()
      .put("api/UserTask/ExportBulkTasks", data, {
        responseType: "blob",
      })
      .then(res => {
        callback(res);
      })
      .catch(res => {
        fail(res.response);
      });
  };
}

export function deleteBulkTask(data, dispatch, callback, failure, startedTimerTask) {
  instance()
    .put("/api/usertask/deleteBulkTasks", data)
    .then(res => {
      callback(res.data);
      dispatch(deleteBulkTaskAction({ taskIds: res.data }));
      if (startedTimerTask) dispatch(removeGlobalTaskTime());
    })
    .catch((res) => {
      failure(res);
    });
}

export function archiveBulkTask(data, dispatch, callback, failure, startedTimerTask) {
  instance()
    .put("/api/usertask/ArchiveBulkTasks", data)
    .then(res => {
      let taskIds = res.data.map(i => i.taskId);
      callback(taskIds);
      dispatch(deleteBulkTaskAction({ taskIds }));
      if (startedTimerTask) dispatch(removeGlobalTaskTime());
    })
    .catch(() => {
      failure();
    });
}
export function unArchiveBulkTask(data, dispatch, callback, failure) {
  instance()
    .put("/api/usertask/UnArchiveBulkTasks", data)
    .then(res => {
      callback(res);
      dispatch({
        type: constants.UNARCHIVE_BULK_TASK,
        payload: data,
      });
    })
    .catch(() => {
      failure();
    });
}

export function saveRowOrder(type, data, dispatch) {
  instance()
    .put("api/ItemOrders/" + type, data)
    .then(res => {
      switch (type) {
        case "task":
          dispatch(updateTaskOrder(res.data.entity.itemsOrdering));
          break;
        case "issue":
          dispatch(updateIssueOrder(res.data.entity.itemsOrdering));
          break;
        case "project":
          dispatch(updateProjectOrder(res.data.entity.itemsOrdering));
          break;
        case "meeting":
          dispatch(updateMeetingOrder(res.data.entity.itemsOrdering));
          break;
        case "risk":
          dispatch(updateRiskOrder(res.data.entity.itemsOrdering));
          break;
        default:
          break;
      }
    });
}

export function saveGridSettings(data, dispatch, callback) {
  instance().put("api/GridSetting/task", { settings: { columnSettings: data } });
}

export function getGridSettings(callback) {
  instance()
    .get("api/GridSetting/task")
    .then(res => {
      callback(res.data.entity.settings.columnSettings);
    });
}

export function DeleteTaskAttachment(id, taskData, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/UserTask/DeleteTaskAttachment?id=" + id)
      .then(response => {
        const obj = taskData;
        obj.totalAttachment = response.data.totalAttachements;
        // // obj.totalComment = response.data.totalComments;
        const actions = action.UpdateStoreTasks(obj);
        dispatch(actions[0]);
        dispatch(actions[1]);
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function deleteTaskAttachmentComment(comment, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/UserTask/DeleteTaskAttachment", comment)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function updateTask(obj, callback, failure) {
  return dispatch => {
    dispatch(updateTaskAction(obj));
  };
}

export function saveUserComment(comment, taskData, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/UserTask/SaveUserComment", comment)
      .then(response => {
        const obj = taskData;
        obj.totalAttachment = response.data.totalAttachements;
        // obj.totalComment = response.data.totalComments;
        const actions = action.UpdateStoreTasks(obj);
        dispatch(actions[0]);
        dispatch(actions[1]);
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function getTaskAttachments(taskId, callback, failure) {
  return dispatch => {
    return instance()
      .get("/api/UserTask/GetTaskAttachments?taskId=" + taskId)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function saveTaskAttachment(attachment, taskData, callback) {
  return dispatch => {
    return instance()
      .post("/api/UserTask/SaveTaskAttachment", attachment)
      .then(response => {
        const obj = taskData;
        obj.totalAttachment = response.data.totalAttachements;
        // obj.totalComment = response.data.totalComments;
        const actions = action.UpdateStoreTasks(obj);
        dispatch(actions[0]);
        dispatch(actions[1]);
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function getTaskComments(data, callback, failure) {
  return dispatch => {
    instance()
      .get("/api/usertask/GetTaskComments?taskId=" + data.taskId)
      .then(response => {
        dispatch(tc.PopulateTaskCommentData(response.data.messages));
        dispatch(tc.PopulateTaskActivities(response.data.activities));
        dispatch(tc.PopulateTaskNotifications(response.data.notification));
        dispatch(todo.PopulateTaskToDoItem(response.data.toDoList, data.todoTaskId));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function removeTaskComments() {
  return dispatch => {
    dispatch(tc.PopulateTaskCommentData([]));
    dispatch(tc.PopulateTaskActivities([]));
    dispatch(tc.ClearTaskNotifications([]));
  };
}

export function SaveTaskComments(data, taskData, callback) {
  return dispatch => {
    instance()
      .post("/api/UserTask/SaveUserComment", data)
      .then(response => {
        const obj = taskData;
        obj.totalAttachment = response.data.totalAttachements;
        // obj.totalComment = response.data.totalComments;
        const actions = action.UpdateStoreTasks(obj);
        dispatch(actions[0]);
        dispatch(actions[1]);
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function updateStoreTask(data) {
  //
  return dispatch => {
    const actions = action.UpdateStoreTasks(data);
    return dispatch(actions[0]);
  };
}

// export function CopyTask(data, success, failure) {
//   //replicate task
//   return dispatch => {
//     instance()
//       .get("/api/usertask/copyusertask?taskId=" + data.taskId)
//       .then(response => {
//         //
//         if (response.status === 200) {
//           dispatch(StoreCopyTaskInfo(response.data.entity.task));
//           dispatch(order.FetchItemOrderInfo(() => {}));
//         }
//         return { response: response };
//       })
//       .then(response => {
//         success(response.response);
//         alert('Node is copied successfully!')
//       })
//       .catch(response => {
//         failure(response.response);
//       });
//   };
// }
export function CopyTask(dispatchFn, taskId, obj, success = () => { }, failure = () => { }) {
  if (dispatchFn) {
    dispatchFn(createTaskAction(obj));
    instance()
      .get("/api/usertask/copyusertask?taskId=" + taskId)
      .then(response => {
        dispatchFn(editTaskAction({ ...response.data.entity.task, clientId: obj.clientId }));
        success({ ...response.data.entity.task, clientId: obj.clientId });
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      dispatch(createTaskAction(obj));
      instance()
        .get("/api/usertask/copyusertask?taskId=" + taskId)
        .then(response => {
          dispatch(editTaskAction({ ...response.data.entity.task, clientId: obj.clientId }));
          success({ ...response.data.entity.task, clientId: obj.clientId });
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}

export function getTaskById(taskId, callback, failure) {
  instance()
    .get("api/usertask/GetProjectTaskById?taskId=" + taskId)
    .then(response => {
      callback(response.data);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function UnArchiveTask(data, callback = () => { }, failure = () => { }, dispatchFn = null) {
  if (dispatchFn) {
    instance()
      .get("api/usertask/unarchiveusertask?taskId=" + data.taskId)
      .then(response => {
        dispatchFn(updateTaskDataAction(response.data));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      instance()
        .get("api/usertask/unarchiveusertask?taskId=" + data.taskId)
        .then(response => {
          dispatch(updateTaskDataAction(response.data));
          return response;
        })
        .then(response => {
          callback(response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}

export function ArchiveTask(taskId, callback, failure, dispatchFn) {
  if (dispatchFn) {
    instance()
      .get("api/usertask/archiveusertask?taskId=" + taskId)
      .then(res => {
        dispatchFn(deleteTaskAction(res.data.taskId));
        dispatchFn(DeleteArchiveTaskFromBoard(res.data));
        dispatchFn(updateGlobalTimeTask(res.data));
        callback(res);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      instance()
        .get("api/usertask/archiveusertask?taskId=" + taskId)
        .then(res => {
          dispatch(deleteTaskAction(res.data.taskId));
          dispatch(DeleteArchiveTaskFromBoard(res.data));
          dispatch(updateGlobalTimeTask(res.data));
          callback(res);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}

// export function DeleteTask(data, callback, failure) {
//   return dispatch => {
//     instance()
//       .get(
//         "api/usertask/deleteusertask?taskId=" + data.taskId + "&isChildDelete=" + data.isChildDelete,
//       )
//       .then(response => {
//         callback(data);
//         if (response.status === 200) {
//           dispatch(StoreDeleteTaskInfo(data.taskId));
//           dispatch(DeleteArchiveTaskFromBoard(data));
//           dispatch(updateGlobalTimeTask({ taskId: data.taskId, isDeleted: true }));
//         }
//         return { response: response };
//       })
//       .then(_data => {
//         dispatch(calender.DeleteCalenderTask(data.taskId));
//         return _data;
//       })
//       .catch(response => {
//         failure(response.response);
//       });
//   };
// }
export function DeleteTask(data, callback, failure, dispatchFn) {
  if (dispatchFn) {
    instance()
      .get("api/usertask/deleteusertask?taskId=" + data.taskId)
      .then(response => {
        dispatchFn(deleteTaskAction(data.taskId));
        dispatchFn(DeleteArchiveTaskFromBoard(data));
        dispatchFn(updateGlobalTimeTask({ taskId: data.taskId, isDeleted: true }));
        callback(data, response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      instance()
        .get("api/usertask/deleteusertask?taskId=" + data.taskId)
        .then(response => {
          dispatch(deleteTaskAction(data.taskId));
          dispatch(DeleteArchiveTaskFromBoard(data));
          dispatch(updateGlobalTimeTask({ taskId: data.taskId, isDeleted: true }));
          callback(data);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}

export function MoveTask(data, callback, failure, dispatchFn) {
  instance()
    .post("api/usertask/MoveTaskToWorkspace", data)
    .then(response => {
      dispatchFn(deleteTaskAction(data.taskId));
      callback(response);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function DeleteArchiveTask(taskId, success = () => { }, failure = () => { }) {
  instance()
    .get("api/usertask/deleteusertask?taskId=" + taskId)
    .then(response => {
      success(response);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function dispatchTaskDelete(data) {
  return dispatch => {
    dispatch(deleteTaskAction(data.taskId));
    dispatch(updateGlobalTimeTask({ taskId: data, isDeleted: true }));
  };
}

export function dispatchCopyTask(data) {
  return dispatch => {
    dispatch(StoreCopyTaskInfo(data));
  };
}

export function updateTaskObject(data, dispatchFn = null) {
  if (dispatchFn) {
    dispatchFn(updateTaskDataAction(data));
  } else {
    return dispatch => {
      dispatch(updateTaskDataAction(data));
    };
  }
}

export function FetchTasksInfo(callback) {
  return dispatch => {
    return instance()
      .get("/api/usertask/getTasks")
      .then(response => {
        let ResponseData = localStorage.getItem("projectId")
          ? response.data.filter(x => x.projectId === localStorage.getItem("projectId"))
          : response.data;
        ResponseData.map(function (element) {
          let currentDate = new Date();
          let dueDate = new Date(element.dueDate);
          let obj = {
            id: element.taskId,
            title: element.taskTitle,
            start: new Date(element.startDateString),
            end: new Date(element.dueDateString),
            desc: element.description,
            taskColor: element.colorCode,
            userColors: element.userColors,

            status: element.status,
            isDueToday: currentDate.toDateString() == dueDate.toDateString() ? true : false,
            isStared: element.isStared,
            isDeleted: element.isDeleted, //colorCode //colorCode
          };

          element.CalenderDetails = obj;
        });
        // return dispatch(PopulateTasksInfo(ResponseData));
      })
      .then(() => {
        callback();
      });
  };
}

export function SaveTask(data, success, failure, urlParam) {
  return dispatch => {
    return instance()
      .post("/api/usertask/createusertask", data)
      .then(response => {
        if (window.userpilot) {
          /** tracking successfully created task in app */
          window.userpilot.track("Task Created Successfully");
        }
        dispatch(StoreCopyTaskInfo(response.data.task ? { ...response.data.task, isNew: true } : response.data));
        dispatch(order.FetchItemOrderInfo(() => { }));
        if (urlParam && urlParam.projectId && response.data.taskDTo) {
          // Dispatching for gantt task this scenario occures when task is added to a specific project
          dispatch({
            type: constants.CREATEGANTTASK,
            payload: response.data.taskDTo,
          });
        }
        success(response.data);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function createNextOccurrence(data, success, failure) {
  return dispatch => {
    return instance()
      .post("/api/usertask/CreateRepeatTaskManual", data)
      .then(response => {
        let { oldTask, newTask } = response.data;
        const actions = action.UpdateStoreTasks(oldTask);
        dispatch(actions[0]);
        // dispatch(actions[1]);
        dispatch(tc.PopulateTaskActivities(oldTask.activityLog));
        dispatch(tc.PopulateTaskNotifications(oldTask.notification));

        dispatch(StoreCopyTaskInfo(newTask));
        dispatch(order.FetchItemOrderInfo(() => { }));
        success(oldTask);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function createTaskFromFollowup(id, data, success, failure) {
  return dispatch => {
    return instance()
      .post(`api/usertask/CreateTaskFromFollowupAction?followupId=${id}`, data)
      .then(response => {
        dispatch(StoreCopyTaskInfo(response.data));
        dispatch(order.FetchItemOrderInfo(() => { }));
        return response.data;
      })
      .then(data => {
        success();
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function dispatchNewTask(data) {
  return dispatch => {
    dispatch(createTaskAction(data));
  };
}

export function UpdateGantTask(data, callback) {
  return dispatch => {
    return instance()
      .post("api/gantt/UpdateGanttTask", data)
      .then(response => {
        const actions = action.UpdateStoreTasks(response.data.task);
        dispatch(actions[0]);
        dispatch(actions[1]);
        dispatch(tc.PopulateTaskActivities(response.data.task.activityLog));
        dispatch(tc.PopulateTaskNotifications(response.data.task.notification));
        dispatch({
          type: constants.UPDATEGANTTASK,
          payload: response.data.taskDto,
        });
        return { response: response };
      })
      .then(_data => {
        callback(null, _data.response.data.task);
      })
      .catch(response => callback(response.response, null));
  };
}

export function repeatTask(data, callback, failure, updateRepeatTask) {
  // const API = updateRepeatTask ? "api/UserTask/UpdateRepeatTask" : "/api/usertask/RepeatTask";
  const API = "/api/usertask/RepeatTask";

  return dispatch => {
    return instance()
      .post(API, data)
      .then(response => {
        const rowNode = grid.grid && grid.grid.getRowNode(response.data.id);
        rowNode && rowNode.setData(response.data);
        const actions = action.UpdateStoreTasks(response.data);
        dispatch(actions[0]);
        // dispatch(actions[1]);
        dispatch(tc.PopulateTaskActivities(response.data.activityLog));
        dispatch(tc.PopulateTaskNotifications(response.data.notification));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function UpdateTaskProgressBar(data, callback) {
  //
  return dispatch => {
    const actions = action.UpdateStoreTasks(data);
    return dispatch(actions[0]);
  };
}

export function UpdateTask(data, callback) {
  let ApiData = cloneDeep(data);
  ApiData.description =
    ApiData.description && ApiData.description !== ""
      ? window.btoa(unescape(encodeURIComponent(ApiData.description)))
      : ApiData.description;
  ApiData.activityLog = [];
  ApiData.CalenderDetails = [];
  //
  return dispatch => {
    return instance()
      .post("/api/UserTask/UpdateUserTask", ApiData)
      .then(response => {
        const actions = action.UpdateStoreTasks(response.data);
        dispatch(actions[0]);
        // dispatch(actions[1]);
        dispatch(tc.PopulateTaskActivities(response.data.activityLog));
        dispatch(tc.PopulateTaskNotifications(response.data.notification));
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function UpdateTaskCustomField(
  obj,
  callback = () => { },
  failure = () => { },
  dispatchFn = null
) {
  if (dispatchFn) {
    return instance()
      .put(`api/task/id=${obj.groupId}`, [obj])
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .put(`api/task/id=${obj.groupId}`, [obj])
        .then(response => {
          callback(response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}

export function createCheckList(data, taskId) {
  return {
    type: constants.CREATECHECKLIST,
    payload: {
      data,
      taskId,
    },
  };
}

export function CreateCheckList(data, callback) {
  return dispatch => {
    return instance()
      .post("api/usertask/CreateChecklist", data)
      .then(response => {
        dispatch(createCheckList(response.data, data.taskId));
        // callback(response.data);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function TodoList(data, taskId) {
  return {
    type: constants.SAVETODOLIST,
    payload: {
      data,
      taskId,
    },
  };
}

export function getTaskWorkspace(taskId, callback, failure) {
  return instance()
    .get("api/usertask/GetTaskWorkspace?taskId=" + taskId)
    .then(response => {
      callback(response.data);
    })
    .catch(response => {
      failure(response.response);
    });
}

export function SaveTodoList(data, success, fail) {
  return dispatch => {
    return instance()
      .post("api/usertask/SaveToDo", data)
      .then(response => {
        dispatch(todo.SaveTaskToDoItem(response.data, response.data.taskId));
        success(response);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}

export function CheckTodoItem(data, success, fail) {
  return dispatch => {
    return instance()
      .post("api/usertask/SaveToDo", data)
      .then(response => {
        dispatch(todo.SaveTaskToDoItem(response.data));
        success(response);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}

export function CheckTodoItemInStore(data, success, fail) {
  return dispatch => {
    dispatch(todo.SaveTaskToDoItem(data, data.taskId));
  };
}

export function CheckAllTodoList(data, success, fail, currentUser, dispatchFn = null) {
  if (dispatchFn) {
    return instance()
      .post("api/usertask/CheckAll", data)
      .then(response => {
        dispatchFn(todo.AllToDoItemsComplete(data.checkAll, currentUser, data.taskId));
        success(response);
      })
      .catch(response => {
        fail(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .post("api/usertask/CheckAll", data)
        .then(response => {
          dispatch(todo.AllToDoItemsComplete(data.checkAll, currentUser, data.taskId));
          success(response);
        })
        .catch(response => {
          fail(response.response);
        });
    };
  }
}

export function DeleteToDoList(item, success = () => { }, fail = () => { }) {
  return dispatch => {
    return instance()
      .get("api/usertask/DeleteToDo?id=" + item.id)
      .then(response => {
        dispatch(todo.DeleteTaskToDoItem(item, item.taskId));
        success(response);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}

export function UpdateToDoListOrderInStore(data, success, fail) {
  return dispatch => {
    dispatch(todo.UpdateToDoItemsOrder(data.itemIds, data.taskId));
  };
}

export function UpdateToDoListOrder(data, success, fail = () => { }) {
  return dispatch => {
    return instance()
      .post("api/usertask/UpdateTodoOrder", data)
      .then(response => {
        dispatch(todo.UpdateToDoItemsOrder(data, data.taskId));
        success(response);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}

export function updateChecklist(data, success, failure) {
  return dispatch => {
    return instance()
      .post("api/usertask/UpdateChecklist", data)
      .then(response => {
        dispatch(updateCheckList(response.data, data.taskId));
        callback(response.data);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function updateCheckList(data, taskId) {
  return {
    type: constants.UPDATECHECKLIST,
    payload: {
      data,
      taskId,
    },
  };
}

export function deleteCheckListItem(data) {
  return {
    type: constants.DELETECHECKLIST,
    payload: {
      data,
    },
  };
}

export function deleteCheckList(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/usertask/DeleteChecklist", data)
      .then(response => {
        if (response && response.status === 200) {
          dispatch(deleteCheckListItem(data));
        }
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function MarkTaskAsStarted(data, callback) {
  return dispatch => {
    return instance()
      .post("api/usertask/MarkTaskAsStarred", data)
      .then(response => {
        dispatch(dispatchTask(response.data));
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        callback(response.response);
      });
  };
}

export function dispatchTask(data, callback) {
  const rowNode = grid.grid && grid.grid.getRowNode(data.id);
  rowNode && rowNode.setData(data);
  return dispatch => {
    dispatch(updateTaskDataAction(data));
  };
}

export function dispatchStartTimer(data, task) {
  const { createdDate, currentDate } = data;
  return dispatch => {
    dispatch({
      type: constants.STARTTIMER,
      payload: {
        data: data,
      },
    });
    dispatch({
      type: constants.SETGLOBALTASKTIME,
      payload: {
        task,
        createdDate,
        currentDate,
      },
    });
  };
}

export function dispatchStopTimer(data) {
  return dispatch => {
    dispatch({
      type: constants.STOPTIMER,
      payload: {
        data: data,
      },
    });
    dispatch({
      type: constants.REMOVEGLOBALTASKTIME,
    });
  };
}

export function BulkUpdateTask(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/UserTask/UpdateBulkTasks", data)
      .then(response => {
        dispatch(UpdateBulkStoreTasks(response.data));
        return { response: response };
      })
      .then(_data => {
        if (callback) {
          callback(_data.response);
        }
      })
      .catch(response => {
        if (callback) {
          callback(response.response);
        }
      });
  };
}

export function bulkUpdateTask(data) {
  return dispatch => {
    dispatch(UpdateBulkStoreTasks(response.data));
  };
}

export function BulkArchivedTask(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/UserTask/UpdateBulkTasks", data)
      .then(response => {
        dispatch(ArchivedBulkStoreTasks(response.data.map(item => item.taskId)));
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => callback(response.response));
  };
}

export function BulkDeleteTask(data, callback) {
  /* action creator for bulk delete task */
  return dispatch => {
    return instance()
      .post("/api/UserTask/UpdateBulkTasks", data)
      .then(response => {
        dispatch(DeleteBulkStoreTasks(data.taskIds)); /* calling action */
        return { response: response };
      })
      .then(_data => {
        callback(_data.response);
      })
      .catch(response => callback(response.response));
  };
}

export function ExportTasksToCSV(data, callback) {
  return dispatch => {
    return instance()
      .post("/api/UserTask/ExportTasks", data, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        },
        responseType: "blob",
      })
      .then(response => {
        callback(null, response);
      })
      .catch(response => {
        callback(response.response, null);
      });
  };
}

export function generatePublicLink(data, success, failure) {
  return dispatch => {
    return instance()
      .get(`/api/usertask/GenerateTaskPublicLink?text=${data}`)
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response);
      });
  };
}

export function deletePublicLink(data, success, failure) {
  return dispatch => {
    return instance()
      .post("/api/ntask/RemovePublicLink", data)
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function enablePublicLink(data, success, failure) {
  return dispatch => {
    return instance()
      .post("/api/ntask/EnablePublicLink", data)
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function emailPublicLink(data, success, failure) {
  return dispatch => {
    return instance()
      .post("api/ntask/emailpublishlink", data)
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function MarkTaskNotificationsAsRead(id, currentUser, callback) {
  return dispatch => {
    return instance()
      .get("/api/Notification/MarkTaskNotificationsAsRead?taskId=" + id)
      .then(response => {
        if (response && response.data && response.data.updatedNotifications) {
          dispatch(tc.ClearTaskNotifications([]));
          dispatch(MarkRead(id, currentUser));
        }
        return { response: response };
      })
      .then(_data => {
        callback(null, _data.response);
      })
      .catch(response => callback(response.response, null));
  };
}

export function updateTasksTimesheetStatus(taskIds, status) {
  //Continue From Here
  return {
    type: constants.UPDATETASKSTIMESHEETSTATUS,
    payload: {
      taskIds,
      status,
    },
  };
}

export function startTimer(data, callback, failure, dispatchFn) {
  if (dispatchFn) {
    instance()
      .post("api/UserTask/StartTaskTime", data)
      .then(response => {
        dispatchFn({
          type: constants.STARTTIMER,
          payload: {
            data: response.data,
          },
        });
        return response;
      })
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .post("api/UserTask/StartTaskTime", data)
        .then(response => {
          dispatch({
            type: constants.STARTTIMER,
            payload: {
              data: response.data,
            },
          });
          return response;
        })
        .then(response => {
          callback(response);
        })
        .catch(response => {
          failure(response.response);
        });
    };
  }
}

export function stopTimer(data, callback = () => { }, failure = () => { }, dispatchFn) {
  if (dispatchFn) {
    instance()
      .post("api/UserTask/StopTaskTime", data)
      .then(response => {
        dispatchFn({
          type: constants.STOPTIMER,
          payload: {
            data: response.data,
          },
        });
        dispatchFn(removeGlobalTaskTime());
        callback(response.data);
      })
      .catch(response => {
        failure(response.response);
        dispatchFn(removeGlobalTaskTime());
      });
  }
  return dispatch => {
    return instance()
      .post("api/UserTask/StopTaskTime", data)
      .then(response => {
        dispatch({
          type: constants.STOPTIMER,
          payload: {
            data: response.data,
          },
        });
        dispatch(removeGlobalTaskTime());
        callback(response.data);
      })
      .catch(response => {
        failure(response.response);
        dispatch(removeGlobalTaskTime());
      });
  };
}

export function getTimerValues(taskId, callback, failure) {
  return dispatch => {
    return instance()
      .get(`api/usertask/GetStartedTaskTime?taskId=${taskId}`)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function removeUserEffort(taskId, taskEffortId, callback, failure) {
  return dispatch => {
    return instance()
      .delete(`api/usertask/RemoveUserTaskEffort?taskEffortId=${taskEffortId}`)
      .then(response => {
        dispatch({
          type: constants.REMOVEUSEREFFORT,
          payload: { data: response.data },
        });
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function addUserEffort(obj, callback, failure) {
  return dispatch => {
    return dispatch({
      type: constants.ADDUSERTASKEFFORT,
      payload: {
        data: obj,
      },
    });
  };
}

export function updateTaskCommentsInfo(data) {
  return {
    type: constants.UPDATETASKCOMMENT,
    payload: {
      data,
    },
  };
}

export function assosiateRemoveTaskArchivedMethod(method) {
  return {
    type: constants.REMOVETASKFROMARCHIIVED,
    payload: {
      method,
    },
  };
}

export function updateTasksInStore(data) {
  return {
    type: constants.UPDATETASKSINSTORE,
    payload: {
      data,
    },
  };
}

export function deleteTasksInStore(data) {
  return {
    type: constants.DELETETASKSINSTORE,
    payload: {
      data,
    },
  };
}

export function dispatchArchiveTask(data) {
  return dispatch => {
    dispatch(StoreDeleteTaskInfo(data.taskId));
    dispatch(updateGlobalTimeTask(data));
  };
}

export function deleteTaskSchedule(taskId, success, failure) {
  return dispatch => {
    return instance()
      .get("/api/usertask/DeleteTaskSchedule?taskId=" + taskId)
      .then(response => {
        const rowNode = grid.grid && grid.grid.getRowNode(response.data.id);
        rowNode && rowNode.setData(response.data);
        const actions = action.UpdateStoreTasks(response.data);
        dispatch(actions[0]);
        dispatch(tc.PopulateTaskActivities(response.data.activityLog));
        dispatch(tc.PopulateTaskNotifications(response.data.notification));
        return response;
      })
      .then(response => {
        success(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function saveSubTask(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/usertask/SaveSubTask", data)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function UpdateTasksInstore(key, data) {
  return dispatch => {
    if (key === "UpdateStatus") {
      dispatch(updateTasksInStore(data));
    }
  };
}

export function DeleteBulkStoreTask(data) {
  return dispatch => {
    dispatch(deleteTasksInStore(data));
  };
}
export function BulkAddTask(data) {
  return dispatch => {
    dispatch({
      type: constants.BULK_ADD_TASK,
      payload: data,
    });
  };
}

/// SIGNAL R customfield
export function UpdateTaskCustomFieldData(data) {
  return dispatch => {
    dispatch({
      type: constants.UPDATETASKCUSTOMFIELDDATA,
      payload: data,
    });
  };
}
function getArchivedDataAction(data) {
  return {
    type: constants.ADD_BULK_TASKS,
    payload: data,
  };
}
export function getArchivedData(type, callback) {
  // Project = 1,
  //   Task = 2,
  //   Meeting = 3,
  //   Issue = 4,
  //   Risk = 5
  return dispatch =>
    instance()
      .get(`api/workspace/GetArchivedData?type=${type}`)
      .then(response => {
        dispatch(getArchivedDataAction(response.data));
        callback();
      })
      .catch(error => {
        callback(error.response);
      });
}
export function getActivites(taskId, callback, failure) {
  return dispatch => {
    return instance()
      .get("api/usertask/GetTaskActivityLog?TaskId=" + taskId)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export default {
  FetchTasksInfo,
  ResetTasksInfo,
  SaveTask,
  addBulkTask,
  UpdateTaskProgressBar,
  UpdateTask,
  CreateCheckList,
  UpdateBulkStoreTasks,
  dispatchStartTimer,
  dispatchStopTimer,
  BulkUpdateTask,
  BulkArchivedTask,
  BulkDeleteTask,
  getTaskComments,
  SaveTaskComments,
  StoreCopyTaskInfo,
  deleteTaskAttachmentComment,
  UpdateGantTask,
  updateTaskCommentsInfo,
  updateChecklist,
  deleteTaskSchedule,
  updateStoreTask,
  saveSubTask,
  PopulateDefaultProjectTaskTemplate,
  getTaskById,
};
