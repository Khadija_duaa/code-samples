import constants from "./../constants/types";
import instance from "./../instance";

export function PopulateConstants(data) { //Populate
  return {
    type: constants.POPULATECONSTANTS,
    payload: {
      data,
    }
  };
}
export function ResetConstants() {
  return {
    type: constants.RESETCONSTANTS
  };
}
// /POST /api/UserTask/SaveTaskAttachment
// GET /api/DocsFileUploader/DownloadDocsFilesAmazonS3

export function downloadDocs(url,fileName,callback){
  return dispatch => {
    return instance()
    .get("/api/DocsFileUploader/DownloadDocsFilesAmazonS3?url="+url+"&fileName="+fileName)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      callback(response.response);
    });
  };
}

export function uploadFileTextEditor(_FormData, callback, failure) {
  return dispatch => {
    return instance()
    .post("api/docsfileuploader/uploaddocsfileamazons3", _FormData)
    .then(response => {
      callback(response);
    })
    .catch(response => {
      failure(response.response);
    });
  };
}

export function FetchConstants(dispatch, response) {
  let dayList = [];
  response.data.dayOfWeek.forEach(function (obj) {
    dayList = [{
      value: obj,
      label: obj
    }, ...dayList];
  })
  response.data.dayOfWeek = dayList;

  let languageList = [];
  // response.data.languages.forEach(function (obj) {
  //   languageList = [{
  //     value: obj,
  //     label: obj
  //   }, ...languageList];
  // })
  languageList.push({value:"ar-SA",label:"Arabic"});
  languageList.push({value:"zh-CN",label:"Chinese"});
  languageList.push({value:"de-DE",label:"German"});
  languageList.push({value:"en-US",label:"English"});
  // languageList.push({value:"en-GB",label:"English - United Kingdom"});

  languageList.push({value:"fr-FR",label:"French"});
  languageList.push({value:"pt-PT",label:"Portuguese"});
  //languageList.push({value:"ru-RU",label:"Russian"});
  languageList.push({value:"es-ES",label:"Spanish"});
  // languageList.push({value:"ur-PK",label:"Urdu"});

  response.data.languages = languageList;
  
  dispatch(PopulateConstants(response.data));
}

export function FetchConstantsInfo(callback) {
  return dispatch => {
    return instance().get("/api/ntask/getConstants")
      .then((response) => {
        if(response.status === 200)
        {
          let dayList = [];
          response.data.dayOfWeek.forEach(function(obj){
            dayList = [{
              value: obj,
              label: obj
            }, ...dayList];
          })
          
          response.data.dayOfWeek  = dayList;

          let lamguageList = [];
          response.data.languages.forEach(function(obj){
            lamguageList = [{
              value: obj,
              label: obj
            }, ...lamguageList];
          })
          
          response.data.languages  = lamguageList;

          return dispatch(PopulateConstants(response.data));
        }
      }).then(() => {
        callback()
      });
    return null;
  };
}


export default {
  FetchConstantsInfo,
  ResetConstants
};