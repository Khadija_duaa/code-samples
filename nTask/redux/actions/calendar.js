import instance from "../instance";

export const getCalenderList = async (success, failure) => {
    return instance()
        .get(`/api/NTaskCalender/calender`)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const getCalender = async (data, success, failure) => {
    const { calenderId } = data
    return instance()
        .get(`/api/NTaskCalender/calender/${calenderId}`)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const deleteCalender = async (calenderId, success, failure) => {
    return instance()
        .delete(`/api/NTaskCalender/calender/${calenderId}`)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const setDefaultWorkspaceCalender = async (calenderId, success, failure) => {
    return instance()
        .put(`/api/NTaskCalender/workspace/${calenderId}`)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const saveCalender = async (data, success, failure) => {
    return instance()
        .post(`/api/NTaskCalender/calender`, data)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const copyCalender = async (data, success, failure) => {
    return instance()
        .post(`/api/NTaskCalender/calender/Copy`, data)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const updateCalender = async (data, success, failure) => {
    const { calenderId } = data
    return instance()
        .put(`/api/NTaskCalender/calender/${calenderId}`, data)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const updateWorkspaceCalender = async (data, success, failure) => {
    const { calenderId, } = data
    return instance()
        .put(`/api/NTaskCalender/workspace/${calenderId}`, data)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const updateProjectCalender = async (data, success, failure) => {
    const { calenderId, projectId } = data
    return instance()
        .put(`/api/NTaskCalender/project/${calenderId}/${projectId}`, data)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const updateTeamCalender = async (data, success, failure) => {
    const { calenderId, } = data
    return instance()
        .put(`/api/NTaskCalender/team/${calenderId}`, data)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const saveException = async (data, success, failure) => {
    const { calenderId, } = data
    return instance()
        .post(`/api/NTaskCalender/exceptions/${calenderId}`, data)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const updateException = async (data, success, failure) => {
    const { calenderId, } = data
    return instance()
        .put(`/api/NTaskCalender/exceptions/${calenderId}`, data)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};

export const deleteException = async (data, success, failure) => {
    const { exceptionId, calenderId } = data
    return instance()
        .delete(`/api/NTaskCalender/exceptions/${calenderId}/${exceptionId}`)
        .then((response) => success?.(response.data))
        .catch((response) => failure?.(response.response));
};
