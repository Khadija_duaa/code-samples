import constants from "./../constants/types";

export function PopulateTaskToDoItem(data = [], taskId) {
  return {
    type: constants.POPULATETASKTODOITEMS,
    payload: {
      data,
      taskId: taskId,
    },
  };
}

export function SaveTaskToDoItem(data = [], taskId) {
  return {
    type: constants.SAVETASKTODOITEMS,
    payload: {
      data,
      taskId: taskId,
    },
  };
}
export function CreateToDoItem(data = [], taskId) {
  return {
    type: constants.CREATETODOITEM,
    payload: {
      data,
      taskId: taskId,
    },
  };
}

export function DeleteTaskToDoItem(data = [], taskId) {
  return {
    type: constants.DELETETASKTODOITEM,
    payload: {
      data,
      taskId: taskId,
    },
  };
}
export function ClearTaskToDoItem(data = [], taskId) {
  return {
    type: constants.CLEARTASKTODOITEMS,
    payload: {
      data,
      taskId: taskId,
    },
  };
}

// export function DeleteSingleToDoItem(data = [], taskId) {
//   return {
//     type: constants.DELETESINGLETODOITEMS,
//     payload: {
//       data,
//       taskId: taskId,
//     },
//   };
// }

export function AllToDoItemsComplete(check = false, currentUser = {}, taskId) {
  return {
    type: constants.ALLTODOITEMSCOMPLETE,
    payload: {
      check: check,
      user: currentUser,
      taskId: taskId,
    },
  };
}
export function UpdateToDoItemsOrder(data = [], taskId) {
  return {
    type: constants.UPDATETODOITEMSORDER,
    payload: {
      data,
      taskId: taskId,
    },
  };
}

export function dispatchAddTodoItems(data, check) {
  //data.taskId
  
  return (dispatch) => {
    if (check == "Create") dispatch(CreateToDoItem(data.todoitem,data.taskId));
    else if (check == "Update") dispatch(SaveTaskToDoItem(data.todoitem, data.taskId));
    if (check == "ClearTodoItems") dispatch(ClearTaskToDoItem([], data.taskId));
    else if (check == "Delete") dispatch(DeleteTaskToDoItem(data.todoitem, data.taskId));
    else if (check == "checkAll")
      dispatch(AllToDoItemsComplete(data.todoitem.value, { userId: data.todoitem.userId }, data.taskId));
    else if (check == "updateTodoOrder") dispatch(UpdateToDoItemsOrder(data.todoitem, data.taskId));
  };
}

export default {
  PopulateTaskToDoItem,
  SaveTaskToDoItem,
  DeleteTaskToDoItem,
  dispatchAddTodoItems,
  ClearTaskToDoItem,
  AllToDoItemsComplete,
  UpdateToDoItemsOrder,
};
