import constants from "../constants/types";
import instance from "../instance";
import authentication from "./authentication";
import calenderTasks from "./calenderTasks";
import cons from "./constants";
import forgotPassword from "./forgotPassword";
import issues from "./issues";
import itemOrder from "./itemOrder";
import meetings from "./meetings";
import notifications from "./notifications";
import profile from "./profile";
import project from "./projects";
import risks from "./risks";
import signUp from "./signUp";
import tasks from "./tasks";
import teamMembers from "./teamMembers";
import tempdata from "./tempdata";
import workspace from "./workspace";

// import initial from "./../store/initialState";
// import { Redirect } from "react-router-dom";
export function LogoutResponse(data) {
  return {
    type: constants.LOGOUT,
    payload: {
      data,
    },
  };
}
export function sessionLogout(sucess) {
  return (dispatch) => {
    return instance()
      .post("/api/account/logout", null)
      .then((response) => {
        localStorage.removeItem("token");
        sessionStorage.removeItem("token");
        localStorage.removeItem("zapkey");
        localStorage.removeItem("userName");
        sucess();
      });
  };
}

export const logoutUser = () => (dispatch) => {
  dispatch({ type: constants.USER_LOGOUT });
};

export function Logout(allDevicesQuery, redirect, sucess) {
  return (dispatch) => {
    return instance()
      .post(
        `/api/account/logout${allDevicesQuery ? allDevicesQuery : ""}`,
        null
      )
      .then((response) => {
        localStorage.removeItem("token");
        sessionStorage.removeItem("token");
        localStorage.removeItem("zapkey");
        localStorage.removeItem("userName");
      })
      .then(() => {
        dispatch(logoutUser());
        window.fcWidget && window.fcWidget.destroy();
      })
      .then(() => {
        $.connection.hub.stop();
        if (redirect) {
          if(localStorage.getItem("companyName") != null) {
            redirect.push("/wl/"+ localStorage.getItem("companyName"));
            // localStorage.removeItem("companyName");
          }
          else
          redirect.push("/account/login");
        
      }
      })
      .catch(() => {});
  };
}

export default {
  Logout,
};
