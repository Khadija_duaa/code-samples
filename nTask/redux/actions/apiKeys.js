import instance from "../instance";

export function saveApi(data, success, failure, dispatch) {
  return instance("https://rapi.ntaskmanager.com")
    .post("/api/settings/apikey", data)
    .then(response => {
      success(response);
    })
    .catch(response => {
      failure(response);
    });
}

export function getApis(teamId, success, failure, dispatch) {
  return instance("https://rapi.ntaskmanager.com")
    .get("/api/settings/apikey?teamId=" + teamId)
    .then(response => {
      success(response);
    })
    .catch(response => {
      failure(response);
    });
}

export function deleteApis(id, success, failure, dispatch) {
  return instance("https://rapi.ntaskmanager.com")
    .delete("/api/settings/apikey?id=" + id)
    .then(response => {
      success(response);
    })
    .catch(response => {
      failure(response);
    });
}

export function disableApis(id, success, failure, dispatch) {
  return instance("https://rapi.ntaskmanager.com")
    .put("/api/settings/apikey/disable?id=" + id)
    .then(response => {
      success(response);
    })
    .catch(response => {
      failure(response);
    });
}

export function updateChangesApis(data, success, failure, dispatch) {
  return instance("https://rapi.ntaskmanager.com")
    .put("/api/settings/apikey", data)
    .then(response => {
      success(response);
    })
    .catch(response => {
      failure(response);
    });
}
