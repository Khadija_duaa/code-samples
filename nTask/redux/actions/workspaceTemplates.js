import constants from "../constants/types";
import instance from "../instance";
import { PopulateWSProjectDefaultTemplate } from "./projects";
import { PopulateDefaultWSTaskTemplate} from "./tasks";

// export function SaveStatusItem(data = {}) {
//   return {
//     type: constants.SAVEWSTEMPLATESTATUSITEM,
//     payload: {
//       data,
//     },
//   };
// }
export function CreateTemplate(data = {}) {
  return {
    type: constants.CREATEWSTEMPLATEITEM,
    payload: {
      data,
    },
  };
}
// export function addNewWSTemplate(data) {
//   return {
//     type: constants.ADDWSTEMPLATE,
//     payload: {
//       data,
//     },
//   };
// }
export function UpdateSaveTemplate(data = {}) {
  return {
    type: constants.UPDATESAVEDTEMPLATEITEM,
    payload: {
      data,
    },
  };
}
export function updateTemplateAfterSettingDefaultTemplate(data) {
  return {
    type: constants.WSSETTINGDEFATUL,
    payload: {
      data,
    },
  };
}
export function deleteTemplate(data) {
  return {
    type: constants.DELETETEMPLATE,
    payload: {
      data
    },
  };
}
/**
 * it is called when user change default template or any template and then save in workspace context
 * @param {object} data 
 * @param {function} success 
 * @param {function} fail 
 */
export function CreateWSTemplate(data, success, fail) {
  return dispatch => {
    return instance()
      .post("api/Template/SaveAsNewTemplate", data)
      .then(response => {
        dispatch(CreateTemplate(response.data.template));
        success(response.data.template);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}
/**
 * 
 * @param {object} data 
 * @param {function} success 
 * @param {function} fail 
 */
export function AddWSTemplate(data, success, fail) {
  return dispatch => {
    return instance()
      .post("api/Template/SaveTemplate", data)
      .then(response => {
        dispatch(CreateTemplate(response.data.entity));
        success(response.data.entity);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}
/**
 * it is called when user update saved templates in workspace
 * @param {object} data 
 * @param {function} success 
 * @param {function} fail 
 */
export function UpdateWSSavedTemplate(data, success, fail) {
  return dispatch => {
    return instance()
      .post("api/Template/SaveAsNewTemplate", data)
      .then(response => {
        dispatch(UpdateSaveTemplate(response.data.template));
        dispatch(PopulateWSProjectDefaultTemplate({oldTemplateId: response.data.template.templateId, newTemplate: response.data.template}));
        dispatch(PopulateDefaultWSTaskTemplate(response.data));
        success(response.data.template);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}
export function MapTaskWithSetDefaultTemplate(data, success, fail) {
  return dispatch => {
    return instance()
      .post("api/Template/SetDefaultTemplate", data)
      .then(response => {
        dispatch(updateTemplateAfterSettingDefaultTemplate(response.data.entity.template));
        dispatch(PopulateWSProjectDefaultTemplate({oldTemplateId: data.fromTemplateID, newTemplate: response.data.entity.template}));
        dispatch(PopulateDefaultWSTaskTemplate(response.data.entity));
        success(response);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}
export function DeleteSavedTemplate(data, success, fail) {
  return dispatch => {
    return instance()
      .post("api/Template/DeleteTemplate", data)
      .then(response => {
        dispatch(deleteTemplate(data));
        success(data);
      })
      .catch(response => {
        fail(response.response);
      });
  };
}
export function dispatchTemplatesActions(action, data) {

  return dispatch => {
    if (action == "CreateTemplate") dispatch(CreateTemplate(data.template));
    else if (action == "SetDefaultTemplate") {
      dispatch(updateTemplateAfterSettingDefaultTemplate(data.template));
      dispatch(PopulateWSProjectDefaultTemplate({oldTemplateId: data.oldTemplateId, newTemplate: data.template}));
      dispatch(PopulateDefaultWSTaskTemplate(data));
    }
    else if (action == "UpdateTemplate") {
      dispatch(UpdateSaveTemplate(data.template));
    }
    else if (action == "Delete") dispatch(DeleteSavedTemplate(data.template));
  };
}

export default {
  AddWSTemplate,
  CreateWSTemplate,
  UpdateWSSavedTemplate,
  MapTaskWithSetDefaultTemplate,
  DeleteSavedTemplate,
  dispatchTemplatesActions,
};
