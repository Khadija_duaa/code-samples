import constants from "../constants/types";
import instance from "../instance";
import { store } from "../../index";
import { getColumnsList } from "./columns";

export function getCustomFieldTypes(callback) {
  instance()
    .get("api/CustomField/customFieldTypes")
    .then(response => {
      callback(response.data.entity);
      // dispatch(PopulateWorkspaceActivityInfo(response.data.results,pageNo));
      // return { response };
    })
    .catch(response => {
      // callback(response ? response.response.data : "Error", null);
    });
}
export function createChecklistItem(item, callback) {
  return dispatch => {
    return instance()
      .post("api/cfieldchecklist", item)
      .then(response => {
        callback(response.data.entity);
      })
      .catch(response => {});
  };
}
export function markAllChecklistItems(item, callback) {
  instance()
    .post("api/cfieldchecklist/markall", item)
    .then(response => {
      callback(response.data.entity);
    })
    .catch(response => {});
}
export function getChecklistItems(obj, callback) {
  return instance()
    .get(`api/cfieldchecklist/${obj.fieldId}/${obj.groupId}`)
    .then(response => {
      callback(response.data.entity.fieldData.data);
    })
    .catch(response => {});
}
export function deleteChecklistItems(obj, checklistId, callback) {
  return instance()
    .delete(
      `api/cfieldchecklist?id=${checklistId}&groupid=${obj.groupId}&fieldId=${obj.fieldId}&groupType=${obj.groupType}`
    )
    .then(response => {
      callback(response.data.entity);
    })
    .catch(response => {});
}
export function editChecklistItems(obj, itemObj, callback) {
  return instance()
    .put("api/cfieldchecklist/" + itemObj.id, obj)
    .then(response => {
      callback(response.data.entity);
    })
    .catch(response => {});
}
export function addCustomField(data, succ, fail) {
  /** post api for saving the custom field settings  */
  return dispatch => {
    return instance()
      .post("api/CustomField", data)
      .then(res => {
        getColumnsList(() => {}, dispatch);
        dispatch({
          type: constants.ADDCUSTOMFIELD,
          payload: res.data.entity,
        });
        succ(res.data.entity);
      })
      .catch(res => {
        fail(res.response);
      });
  };
}
export function editCustomField(data, id, succ, fail) {
  /** put api for saving the custom field changings  */
  return dispatch => {
    return instance()
      .put("api/CustomField/" + id, data)
      .then(res => {
        dispatch({
          type: constants.EDITCUSTOMFIELD,
          payload: res.data.entity,
        });
        getColumnsList(() => {}, dispatch);
        succ(res.data.entity);
      })
      .catch(res => {
        fail(res.res);
      });
  };
}
export function hideCustomField(data, succ, fail) {
  /** put api for saving the custom field changings  */
  return dispatch => {
    return instance()
      .post("api/CustomField/hide", data)
      .then(res => {
        getColumnsList(() => {}, dispatch);
        dispatch({
          type: constants.EDITCUSTOMFIELD,
          payload: res.data.entity,
        });
        succ(res.data.entity);
      })
      .catch(res => {
        fail(res.res);
      });
  };
}

export function getCustomField(callback, failure) {
  return dispatch => {
    return instance()
      .get("api/CustomField")
      .then(response => {
        dispatch({
          type: constants.FETCHCUSTOMFIELDS,
          payload: response.data.entity ? response.data.entity : [],
        });
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}

export function useExistingCustomField(data, succ, fail) {
  /** post api for saving the custom field settings  */
  return dispatch => {
    return instance()
      .put(`api/CustomField/${data.fieldId}`, data)
      .then(res => {
        getColumnsList(() => {}, dispatch);
        dispatch({
          type: constants.ADDCUSTOMFIELD,
          payload: res.data.entity,
        });
        succ(res.data.entity);
      })
      .catch(res => {
        fail(res.res);
      });
  };
}

export function dispatchDeleteField(data, succ, fail, deletedFieldObj) {
  /** post api for deleting the custom field settings  */
  return dispatch => {
    return instance()
      .post(`api/customfield/deletefields`, data)
      .then(res => {
        dispatch({
          type: constants.DELETECUSTOMFIELDFILTER,
          payload: data,
        });
        getColumnsList(() => {
          dispatch({
            type: constants.DELETECUSTOMFIELD,
            payload: data,
          });
          succ(data);
          return res;
        }, dispatch);
      })
      .then(res => {
        if (deletedFieldObj.fieldType === "section")
          dispatch({
            type: constants.UPDATECHILDFIELDS,
            payload: deletedFieldObj,
          });
      })
      .catch(res => {
        fail(res.res);
      });
  };
}

/// SIGNAL R
export function UpdateCustomFieldsSignalR(key, data) {
  return dispatch => {
    if (key === "Create") {
      dispatch({
        type: constants.ADDCUSTOMFIELD,
        payload: data,
      });
    }
    if (key === "Delete") {
      const storeState = store.getState();

      dispatch({
        type: constants.UPDATEITEMORDERSIGNALR,
        payload: { fieldIds: data.fieldIds, storeState },
      });
      dispatch({
        type: constants.DELETECUSTOMFIELD,
        payload: data.fieldIds,
      });
    }
    if (key === "Update") {
      dispatch({
        type: constants.EDITCUSTOMFIELD,
        payload: data,
      });
    }
  };
}

export function updateCustomFieldData(data) {
  // type here (data.fieldName , "success")
  return dispatch => {
    dispatch({
      type: constants.UPDATE_RISK_CUSTOM_FIELD,
      payload: data,
    });
    dispatch({
      type: constants.UPDATE_ISSUE_CUSTOM_FIELD,
      payload: data,
    });
    dispatch({
      type: constants.UPDATE_TASK_CUSTOM_FIELD,
      payload: data,
    });
  };
}
