import { browserHistory } from "react-router";
import constants from "../constants/types";
import instance from "../instance";

//
// history = history();
//
// import history from './../../components/history/index';

export function SetUserProfile(profile, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/account/SetUserProfile", profile)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function confirmEmail(userId, callback, failure) {
  return dispatch => {
    return instance()
      .get(`/api/account/ConfirmUserEmail?token=${userId}`)
      .then(response => {
        callback(response);
      })
      .catch(response => {
        failure(response.response);
      });
  };
}
export function resendEmailVerification(callback, failure) {
  return instance()
    .post(`/api/account/ResendConfirmationEmail`)
    .then(response => {
      callback(response);
    });
}
export function isTocAccepted(callback) {
  return instance()
    .get("api/ntask/AcceptTermAndCondition")
    .then(response => {
      callback();
    })
    .catch(err => {});
}

export function PopulateFacebook(data) {
  return {
    type: constants.POPULATEFACEBOOK,
    payload: {
      data,
    },
  };
}

export function PopulateGooglePlus(data) {
  return {
    type: constants.POPULATEGOOGLEPLUS,
    payload: {
      data,
    },
  };
}

export function PopulateLinkedIn(data) {
  return {
    type: constants.POPULATELINKEDIN,
    payload: {
      data,
    },
  };
}

export function PopulateUserInfo(data) {
  return {
    type: constants.POPULATEUSERINFO,
    payload: {
      data,
    },
  };
}

export function UpdateMember(data, actionType) {
  return {
    type: constants.UPDATEMEMBER,
    payload: {
      data,
      actionType,
    },
  };
}

export function ResetUserInfo() {
  return {
    type: constants.DELETEPROFILE,
  };
}

export function UpdateLocalization(data) {
  return {
    type: constants.UPDATELOCALIZATION,
    payload: {
      data,
    },
  };
}

export function UpdateUserProfile(data) {
  return dispatch => {
    dispatch({
      type: constants.UPDATEUSERPROFILE,
        payload: {
      data,
    }})
};
}

export function SocialMediaSignInRequest(type, data) {
  return type + data;
  //
  //
  //
  //
  // data = {
  //   eMail: data.Email,
  //   fIrstName: data.firstName,
  //   lAstName: data.lastName
  // };

  // return dispatch => {
  //   //
  //   return utils.SignUp.Register(data).then((response) => {

  //     return dispatch(PopulateData(response.data));

  //   })
  //     .catch((error) => {
  //       return dispatch(PopulateData(error.message));

  //     });

  // };
}
// UPDATEIMAGEUSERPROFILE
export function UpdateProfileImage(data) {
  return {
    type: constants.UPDATEIMAGEUSERPROFILE,
    payload: {
      data,
    },
  };
}

export function AsyncRequest() {
  //
  //   return dispatch => {
  //
  //      return axios.get(config.API_ROOT)
  //       .then((response) => {

  //         return dispatch(PopulateData(response.data))

  //       })

  return null;
}

function fillLocalStorageDataForRedirect(data, _status) {
  //

  const companyObj = {
    passwordError: "",
    usernameError: "",
    lastNameError: "",
    firstNameError: "",
    otherCompanyIndustry: "",
    passwordErrorMessage: "",
    usernameErrorMessage: "",
    lastNameErrorMessage: "",
    firstNameErrorMessage: "",
    showPassword: false,
    companyName: "",
    jobTitle: "",
    companyIndustry: "",
    companySize: "",
    otherIndustry: false,
    errorString_companyName: null,
    otherIndustry: false,

    otherCompanyIndustryError: false,
    otherCompanyIndustryErrorMessage: "",

    otherJobTitle: "",
    otherJobTitleErrorMessage: "",
    otherJobTitleError: false,
    OtherJob: false,
  };
  const workspaceObj = {
    workspaceName: "",
    workspaceUrl: "",
    workspaceUseOther: "",
    workspaceError: false,
    workspaceUrlError: false,
    workspaceErrorMessage: null,
    workspaceUrlErrorMessage: null,
    workspaceUse: "",
    otherWorkspaceOption: false,
  };
  const inviteMemberObj = {
    emailAddressErrorMessage: "",
    emailAddressError: false,
    inviteEmailAddress: "",
    inviteMemberList: [],
    teamId: "",
  };

  companyObj.companyName = data.companyName || "";
  companyObj.jobTitle = { value: data.jobTitle, label: data.jobTitle } || "";
  companyObj.otherIndustry = false;
  companyObj.companyIndustry = { value: data.industry, label: data.industry } || "";
  companyObj.otherCompanyIndustry = "";
  companyObj.companySize = data.numberOfMembers || "";

  if (
    companyObj.companyIndustry &&
    companyObj.companyIndustry.value &&
    companyObj.companyIndustry.value.toLowerCase() === "other"
  ) {
    companyObj.otherIndustry = true;
    companyObj.otherCompanyIndustry = data.industryDetails;
  }

  if (
    companyObj.jobTitle &&
    companyObj.jobTitle.value &&
    companyObj.jobTitle.value.toLowerCase() === "other"
  ) {
    companyObj.OtherJob = true;
    companyObj.otherJobTitle = data.jobTitleDetails;
  }

  workspaceObj.workspaceName = data.teamName || "";
  workspaceObj.workspaceUrl = data.teamName || "";
  workspaceObj.workspaceUse = { value: data.usage, label: data.usage } || "";
  if (
    companyObj.workspaceUse &&
    companyObj.workspaceUse.value &&
    companyObj.workspaceUse.value.toLowerCase() === "other"
  ) {
    workspaceObj.workspaceUseOther = data.usage;
    workspaceObj.otherWorkspaceOption = true;
  }
  localStorage.setItem("CompanyInfo", JSON.stringify(companyObj));
  localStorage.setItem("CreateWorkspace", JSON.stringify(workspaceObj));
  localStorage.setItem("InviteMembers", JSON.stringify(inviteMemberObj));
  switch (_status) {
    case 4: // Company Information
      localStorage.setItem(
        "Onboarding",
        JSON.stringify({
          activeStep: 1,
          updatesCheckbox: false,
        })
      );
      // self.props.history.push("/account/setpassword");
      // browserHistory.replace("/welcome/getstarted");
      break;
    case 3: // WorkSpace Information
      localStorage.setItem(
        "Onboarding",
        JSON.stringify({
          activeStep: 2,
          updatesCheckbox: false,
        })
      );
      // browserHistory.replace("/welcome/getstarted");
      break;
    case 2: // Invite Members
      localStorage.setItem(
        "Onboarding",
        JSON.stringify({
          activeStep: 3,
          updatesCheckbox: false,
        })
      );
      // browserHistory.replace("/welcome/getstarted");
      break;
    case 1: // Youtube
    // browserHistory.replace("/welcome/getstarted");
    // <Redirect to="/account/setpassword"/>
    default:
      break;
  }
}
export function updateTeam(object, callback, failure) {
  return dispatch => {
    return instance()
      .post("api/company/UpdateTeam", {
        companyId: object.companyId,
        companyName: object.companyName,
        companyUrl: object.companyUrl,
      })
      .then(res => {
        dispatch({
          type: constants.UPDATEUSERTEAM,
          payload: object,
        });
        callback(res);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function SwitchTeamWorkspace(objectId, objectType, success, fail) {
  return dispatch => {
    return instance()
      .get(`/api/company/switchteamworkspace?objectId=${  objectId  }&objectType=${  objectType}`)
      .then(response => {
        success(response);
      })
      .catch(error => {
        fail(error);
      });
  };
}
export function FetchUserInfo(callback) {
  return dispatch => {
    return instance()
      .get("/api/account/userinfo")
      .then(response => {
        // state
        //
        //

        // if (response.data.state > 0) {//12-10-2018
        //   fillLocalStorageDataForRedirect(response.data.profile, response.data.state);
        // } else {
        callback(response);
        dispatch(PopulateUserInfo(response.data));
        dispatch(PopulateSyncGoogleCalendar(response.data.syncGoogleCalendar));
        dispatch(PopulateOutlookCalendar(response.data.calendarsIntegration));
        dispatch(PopulateAppleCalendar(response.data.calendarsIntegration));
        // dispatch(PopulateNotifications(response.data.notifications));
        // }//12-10-2018
        // return {
        //   response: response
        // };
        return response;
      });

    // .then(response => {
    //   //history.replace("/tasks")
    //   callback(response.response);
    // });
  };
}

export function updateWorkSpaceImageURL(teamId, data) {
  return {
    type: constants.UPDATEWORKSPACEIMAGE,
    payload: {
      teamId,
      data,
    },
  };
}
export function PopulateSyncGoogleCalendar(data) {
  return {
    type: constants.ADDSYNCCALENDAR,
    payload: {
      data,
    },
  };
}
export function PopulateOutlookCalendar(data) {
  return {
    type: constants.ADDOUTLOOKCALENDAR,
    payload: {
      data,
    },
  };
}
export function PopulateAppleCalendar(data) {
  return {
    type: constants.ADDAPPLECALENDAR,
    payload: {
      data,
    },
  };
}

export function updateTeamImageURL(teamId, data) {
  return {
    type: constants.UPDATETEAMIMAGE,
    payload: {
      teamId,
      data,
    },
  };
}

export function UpdateUserProfileImage(imageUrl) {
  return {
    type: constants.UPDATEUSERPROFILEIMAGE,
    payload: {
      imageUrl,
    },
  };
}

export function LinkSlackInfo(code, callback, failure=()=>{}) {
  return dispatch => {
    return instance()
      .get(`api/ntask/slackauthaccess?code=${  code}`)
      .then(response => {
        dispatch(updateSlack(response.data));
        return {
          response,
        };
      })
      .then(response => {
        callback(response);
      })
      .catch(err => {
        failure(err.response);
      });
  };
}

export function UnlinkSlackInfo(callback) {
  return dispatch => {
    return instance()
      .get("/api/ntask/UnlinkSlackProfile")
      .then(response => {
        dispatch(updateSlack(response.data));
        return {
          response,
        };
      })
      .then(response => {
        callback(response);
      });
  };
}

export function UnlinkZoomInfo(callback) {
  return dispatch => {
    return instance()
      .get("/api/zoom/UnlinkZoomProfile")
      .then(response => {
        dispatch(updateZoom(response.data));
        return {
          response,
        };
      })
      .then(response => {
        callback(response);
      });
  };
}

export function UnlinkTeamsInfo(callback) {
  return dispatch => {
    return instance()
    .get("/api/Calendar/MicrosoftUnLinkTeam")
      .then(response => {
        dispatch(updateMicrosoftTeams({ isTeamLinked: false })); 
        return {
          response,
        };
      })
      .then(response => {
        callback(response);
      });
  };
}

export function LinkZoomInfo(code, callback, failure=()=>{}) {
  return dispatch => {
    return instance()
      .get(`api/zoom/zoomauthaccess?code=${code}`)
      .then(response => {
        callback();
        dispatch(updateZoom({ isZoomLinked: true }));
      })
      .catch(err => {
        failure(err.response);
      });
  };
}
export function LinkMicrosoftTeamsInfo(code, callback=()=>{}, failure=()=>{}) {
  return dispatch => {
    return instance()
      .get(`api/Calendar/MicrosoftAuthorization?code=${code}`)
      .then(response => {
        callback();
        dispatch(updateMicrosoftTeams(response.data));
      })
      .catch(err => {
        failure(err.response);
      });
  };
}

export function updateMicrosoftTeams(data) {
  return {
    type: constants.UPDATETEAMINFO,
    payload: {
      data,
    },
  };
}
export function closeProfileSetting() {
  return dispatch => {
    dispatch(closeProfileSettingAction());
  };
}

export function updateSlack(data) {
  return {
    type: constants.UPDATESLACKINFO,
    payload: {
      data,
      data,
    },
  };
}
export function updateZoom(data) {
  return {
    type: constants.UPDATEZOOMINFO,
    payload: {
      data,
    },
  };
}
export function closeProfileSettingAction() {
  return {
    type: constants.CLOSE_PROFILE_SETTING_DIALOG,
  };
}

export default {
  AsyncRequest,
  PopulateUserInfo,
  SocialMediaSignInRequest,
  FetchUserInfo,
  ResetUserInfo,
  UpdateLocalization,
  UpdateUserProfile,
};
