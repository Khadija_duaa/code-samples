import axios from "axios";
import instance from "../instance";

const url = "https://nformsapi.naxxa.io";

export function markCurrentVersion(data, callback, failure) {
  instance(FORMS_BASE_URL)
    .put(`api/forms/version/markAsCurrentVersion`, data)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function versionNameUpdate(data, callback, failure) {
  instance(FORMS_BASE_URL)
    .put(`api/forms/version/name`, data)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function addNewForm(data, callback, failure) {
  instance(FORMS_BASE_URL)
    .post(`api/forms`, data)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function addNewVersion(data, callback, failure) {
  instance(FORMS_BASE_URL)
    .post(`api/forms/new/version`, data)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function getAllForms(callback, failure) {
  instance(FORMS_BASE_URL)
    .get(`api/forms`)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function getAllFormVersions(formId, callback, failure) {
  instance(FORMS_BASE_URL)
    .get(`api/forms/version/${formId}`)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function getAllAssignedForms(callback, failure) {
  instance(FORMS_BASE_URL)
    .get(`api/forms/assigned/me`)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function getAllTemplatesForms(callback, failure) {
  instance(FORMS_BASE_URL)
    .get(`api/forms/templates`)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function updateForm(data, callback, failure) {
  instance(FORMS_BASE_URL)
    .put(`api/forms`, data)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function deleteForm(id, callback, failure) {
  instance(FORMS_BASE_URL)
    .delete(`api/forms/${id}`)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function deleteFormVersion(id, callback, failure) {
  instance(FORMS_BASE_URL)
    .delete(`api/forms/version/${id}`)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function getFormById(id, callback, failure) {
  instance(FORMS_BASE_URL)
    .get(`api/forms/${id}`)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function getFormResponseData(id, callback, failure) {
  instance(FORMS_BASE_URL)
    .get(`api/forms/data/${id}`)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function saveFormFillData(data, callback, failure) {
  instance(FORMS_BASE_URL)
    .post(`api/forms/data`, data)
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response);
    });
}
export function getLocationAPI(latitude, longitude, callback, failure) {
  let options = {
    method: "GET",
    url: "https://trueway-geocoding.p.rapidapi.com/ReverseGeocode",
    params: { location: `${latitude},${longitude}`, language: "en" },
    headers: {
      "x-rapidapi-key": "d85a105815msh944bf7ea6ca9f7dp1dee5ejsnf960e4cc8d57",
      "x-rapidapi-host": "trueway-geocoding.p.rapidapi.com",
    },
  };

  axios
    .request(options)
    .then(function (response) {
      callback(response.data.results[0]);
    })
    .catch(function (error) {
      console.error(error);
      failure(error);
    });
}
