import constants from "./../constants/types";
import taskActions from "../actions/tasks";

export function PopulateTaskCommentData(data) {
  return {
    type: constants.POPULATETASKCOMMENTS,
    payload: {
      data,
    },
  };
}

export function UpdateTaskCommentData(data) {
  return {
    type: constants.UPDATETASKCOMMENTS,
    payload: {
      data,
    },
  };
}

export function DeleteTaskCommentData(data) {
  return {
    type: constants.DELETETASKCOMMENTS,
    payload: {
      data,
    },
  };
}

export function AddNewTaskCommentData(data) {
  const { taskId, totalAttachements, totalComments, totalUnreadComment } = data;

  return (dispatch) => {
    dispatch(
      taskActions.updateTaskCommentsInfo({
        taskId,
        totalAttachements,
        totalComments,
        totalUnreadComment,
      })
    );
    dispatch({
      type: constants.ADDNEWTASKCOMMENTS,
      payload: {
        data,
      },
    });
  };
}

export function PopulateTaskActivities(data) {
  return {
    type: constants.POPULATETASKCOMMENTSACTIVITIES,
    payload: {
      data,
    },
  };
}

export function PopulateTaskNotifications(data) {
  return {
    type: constants.POPULATETASKCOMMENTSNOTIFICATIONS,
    payload: {
      data,
    },
  };
}
export function ClearTaskNotifications(data) {
  return {
    type: constants.CLEARTASKCOMMENTSNOTIFICATIONS,
    payload: {
      data,
    },
  };
}

export function updateTaskNotifications(data) {
  return {
    type: constants.UPDATETASKNOTIFICATIONS,
    payload: {
      data,
    },
  };
}

export function UpdateTaskNotifications(data) {
  return (dispatch) => {
    dispatch(updateTaskNotifications(data));
  };
}

export default {
  PopulateTaskCommentData,
  UpdateTaskCommentData,
  DeleteTaskCommentData,
  PopulateTaskActivities,
  PopulateTaskNotifications,
  ClearTaskNotifications,
  UpdateTaskNotifications,
  updateTaskNotifications,
};
