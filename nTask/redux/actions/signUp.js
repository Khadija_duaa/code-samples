import constants from "./../constants/types";
import instance from "../instance";
export function PopulateData(data) {
  return {
    type: constants.POPULATEDATA,
    payload: {
      data,
    },
  };
}

export function DeleteSignUpData(data) {
  return {
    type: constants.DELETESIGNUPDATA,
  };
}

//constants.DELETESIGNUPDATA

// export function SignUpRequest(data) {

//   // data = {
//   //   eMail: data.Email,
//   //   fIrstName: data.firstName,
//   //   lAstName: data.lastName,
//   //   // Redundant or Extra Fields starts
//   //   userName: data.email,
//   //   //utmSource: "",
//   //   userPlan: "TeamAndOrganizations",
//   //   companyId: "",
//   //   deviceId: "Google Chrome",
//   //   isSubscriptionEnabled: data.isSubscriptionEnabled
//   //   // Redundant or Extra Fields end
//   // };
//   return dispatch => {
//     return instance().post("/api/account/register", data).then((response) => {
//       return dispatch(PopulateData(response.data));
//     }).catch((response) => {
//       let response = {
//         error: true,
//         statusCode: response.status,
//         data: response.data
//       };
//       return dispatch(PopulateData(response));
//     });
//   };
// }
// export function SignUpRequest(email, plan, callback, failure) {
//   return instance()
//     .get(`/api/account/validateuserEmail?email=${email}&plan=${plan}`)
//     .then((response) => {
//       callback(response);
//     })
//     .catch((res) => {
//       failure(res.response);
//     });
// }
export function SignUpRequest(email, plan, callback, failure) {
instance()
    .get(`/api/account/ValidateUserEmailWithDetail?email=${email}&plan=${plan}`)
    .then((response) => {
      callback(response);
    })
    .catch((res) => {
      failure(res.response.data);
    });
}
export default {
  DeleteSignUpData,
};
