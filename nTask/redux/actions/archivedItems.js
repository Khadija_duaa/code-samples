import constants from "../constants/types";

export function setArchivedItems(items) {
  return {
    type: constants.UPDATEARCHIVEDITEMS,
    payload: items,
  };
}

export function updateArchivedItems(items) {
  return dispatch => {
    dispatch(setArchivedItems(items));
  };
}
