import instance from "../instance";
import constants from "./../constants/types";

export function getWidgetSetting(query, success) {
  instance()
    .get("/api/widgetSettting/" + query)
    .then(res => {
      success(res.data.entity.widgetSettings);
    });
}

export function updateWidgetSetting(query, payload, success) {
  
  instance()
    .put("/api/widgetSettting/" + query, {widgetSettings: payload})
    .then(res => {
      success(res);
    });
}
export function getWidget(query, success, error, dispatchFn = null) {
  instance()
    .get("/api/widget/" + query)
    .then(res => {
      success(res);
      if(dispatchFn) {
        dispatchFn({
        type: constants.FETCHCUSTOMFIELDS,
        payload: res.data?.entity?.customFields || [],
      });
    }
    }).catch(err => {
      error(err);
    });
}
export function getTaskStatusList(query, success, error) {
  instance()
    .get("/api/widget/getteamtaskstatus/" + query)
    .then(res => {
      success(res.data.entity);
    }).catch(err => {
      error(err);
    });
}
