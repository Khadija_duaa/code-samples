import constants from "./../constants/types";
import instance from "./../instance";

export function PopulateItemOrder(data) {
  return {
    type: constants.POPULATEITEMORDER,
    payload: {
      data,
    },
  };
}

export function ResetItemOrderInfo() {
  return {
    type: constants.RESETITEMORDERINFO,
  };
}

export function FetchItemOrderInfo(callback) {
  return dispatch => {
    return instance()
      .get("/api/WorkSpace/GetItemOrder")
      .then(response => {
        return dispatch(PopulateItemOrder(response.data));
      })
      .then(() => {
        callback();
      });
  };
}
export function SaveItemOrder(data, success, failure, sortedRows, key, dispatchFun = null) {
  if (dispatchFun) {
    return instance()
      .post("/api/WorkSpace/SaveItemOrder", data)
      .then(response => {
        dispatchFun(PopulateItemOrder(response.data));
        if (sortedRows) dispatch(sortingRows(sortedRows, key));
        return response;
      })
      .then(data => {
        success(data);
      })
      .catch(response => {
        if (failure) failure(response.response);
      });
  } else {
    return dispatch => {
      return instance()
        .post("/api/WorkSpace/SaveItemOrder", data)
        .then(response => {
          dispatch(PopulateItemOrder(response.data));
          if (sortedRows) dispatch(sortingRows(sortedRows, key));
          return response;
        })
        .then(data => {
          success(data);
        })
        .catch(response => {
          if (failure) failure(response.response);
        });
    };
  }
}

export function sortingRows(rows, key) {
  if (rows) {
    switch (key) {
      case "project":
        return {
          type: constants.UPDATEPROJECTSROWS,
          payload: rows,
        };
        break;

      case "task":
        return {
          type: constants.UPDATETASKSROWS,
          payload: rows,
        };
        break;

      case "meeting":
        return {
          type: constants.UPDATEMEETINGROWS,
          payload: rows,
        };
        break;

      case "issue":
        return {
          type: constants.UPDATEISSUEROWS,
          payload: rows,
        };
        break;

      case "risk":
        return {
          type: constants.UPDATERISKROWS,
          payload: rows,
        };
        break;

      default:
        break;
    }
  }
}

export default {
  FetchItemOrderInfo,
  ResetItemOrderInfo,
  SaveItemOrder,
};
