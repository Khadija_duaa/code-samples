import cloneDeep from "lodash/cloneDeep";
import instance from "../instance";
import constants from "./../constants/types";
import { StoreCopyTaskInfo } from "./tasks";
import action, { PopulateWSTemplates } from "./workspace";

export function addPendingProcess(data, type) {
  return {
    type: constants.ADDPENDINGPROCESS,
    payload: {
      data, type
    },
  };
}
export function updatePendingProcess(data, type, toUpdate) {
  return {
    type: constants.UPDATEPENDINGPROCESS,
    payload: {
      data, type, toUpdate
    },
  };
}
export function removePendingProcess(data) {
  return {
    type: constants.REMOVEPENDINGPROCESS,
    payload: {
      data
    },
  };
}
export function removeProcessById(data,type) {
  return {
    type: constants.REMOVEPROCESSBYID,
    payload: {
      data,
      type
    },
  };
}
// add pending handeler
export function addPendingHandler(data, dispatchFn) {
  if (dispatchFn) {
    dispatchFn(addPendingProcess(data, 'export'));
  } else {
    return (dispatch) => {
      dispatch(addPendingProcess(data, 'export'));
    };
  }
};

export function exportPostApi(data, apiEndpoint, processId, dispatchFn, callback, failure) {
  instance()
    .post(apiEndpoint, data, {
      responseType: "blob",
    })
    .then(res => {
      callback(res);
    })
    .catch((err) => {
      failure(err);
    });
  // dispatchFn(addPendingProcess(data));
};




export default {};
