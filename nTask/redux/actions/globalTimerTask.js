import instance from "../instance";
import constants from "../constants/types";

export function getGlobalTaskTime(dispatch, tasks, callback, failure) {

        instance().get("api/usertask/GetStartedTaskTime")
            .then((response) => {
              if(response.data) {
                const task = tasks.find(t => t.taskId == response.data.taskId)
                const data = {
                  task,
                  createdDate: response.data.createdDate,
                  currentDate: response.data.currentDate
                }
                dispatch(setGlobalTaskTime(data.task, data.createdDate, data.currentDate))
              }
            }).catch((response) => {
                failure(response.response);
            });
}

export function setGlobalTaskTime(task, createdDate, currentDate) {
    return {
        type: constants.SETGLOBALTASKTIME,
        payload: {
            task, createdDate, currentDate
        }
    };
}

export function updateGlobalTimeTask(task) {
    return {
        type: constants.UPDATEGLOBALTIMETASK,
        payload: {
            task
        }
    };
}
export function StoreDeleteTaskInfo(data) {
    return {
      type: constants.DELETETASK,
      payload: {
        data,
      },
    };
  }
export function removeGlobalTaskTime() {
    return {
        type: constants.REMOVEGLOBALTASKTIME
    };
}