import constants from "./../constants/types";
import instance from "../instance";

export function getCalendarDetails(code, MailServerType, succ, fail) {
  return (dispatch) => {
    return instance()
      .get(
        "api/Calendar/GetOtherCalendarsSetting?code=" +
          code +
          "&mailServerType=" +
          MailServerType
      )
      .then((response) => {
        if (MailServerType == 1) {
          /** 1 is for outlook calendar and 2 is for apple */
          dispatch(updateOutlookCalendarInfo(response.data));
        } else {
          dispatch(updateAppleCalendarInfo(response.data));
        }

        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}
export function CalendarDeauthorize(item, succ, fail) {
  return (dispatch) => {
    return instance()
      .get(
        "api/Calendar/CalendarDeauthorize?code=" +
          item.code +
          "&mailServerType=" +
          item.mailServerType
      )
      .then((response) => {
        if (item.mailServerType == 1) {
          /** 1 is for outlook calendar and 2 is for apple */
          dispatch(DeleteOutlookCalendar(item));
        } else {
          dispatch(DeleteAppleCalendar(item));
        }
        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}
export function generateUrl(data, succ, fail) {
  return (dispatch) => {
    return instance()
      .post("api/Calendar/AddOtherCalendarSettings", data)
      .then((response) => {
        if (data.mailServerType == 1) {
          /** 1 is for outlook calendar and 2 is for apple */
          dispatch(addUpdateOutlookAccDetails(response.data));
        } else {
          dispatch(addUpdateAppleAccDetails(response.data));
        }

        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}

export function EnableDisableSyncCalendar(data, succ, fail) {
  return (dispatch) => {
    return instance()
      .get(
        "api/Calendar/EnableOtherSync?code=" +
          data.code +
          "&mailServerType=" +
          data.mailServerType +
          "&sync=" +
          data.syncEnabled
      )
      .then((response) => {
        if (data.mailServerType == 1) {
          /** 1 is for outlook calendar and 2 is for apple */
          dispatch(addUpdateOutlookAccDetails(data));
        } else {
          dispatch(addUpdateAppleAccDetails(data));
        }
        return {
          response: response,
        };
      })
      .then((response) => {
        succ(response);
      })
      .catch((response) => {
        fail(response);
      });
  };
}

export function PopulateOutlookCalendar(data) {
  return {
    type: constants.ADDOUTLOOKCALENDAR,
    payload: {
      data,
    },
  };
}
export function DeleteOutlookCalendar(data) {
  return {
    type: constants.DELETEOUTLOOKCALENDAR,
    payload: {
      data,
    },
  };
}
export function DeleteAppleCalendar(data) {
  return {
    type: constants.DELETEAPPLECALENDAR,
    payload: {
      data,
    },
  };
}

export function PopulateAppleCalendar(data) {
  return {
    type: constants.ADDAPPLECALENDAR,
    payload: {
      data,
    },
  };
}

export function updateOutlookCalendarInfo(data) {
  return {
    type: constants.UPDATEOUTLOOKCALENDARINFO,
    payload: {
      data,
    },
  };
}
export function updateAppleCalendarInfo(data) {
  return {
    type: constants.UPDATEAPPLECALENDARINFO,
    payload: {
      data,
    },
  };
}
export function addUpdateOutlookAccDetails(data) {
  return {
    type: constants.ADDUPDATEOUTLOOKACCDETAILS,
    payload: {
      data,
    },
  };
}
export function addUpdateAppleAccDetails(data) {
  return {
    type: constants.ADDUPDATEAPPLEACCDETAILS,
    payload: {
      data,
    },
  };
}

export default {
  getCalendarDetails,
  generateUrl,
  CalendarDeauthorize,
  EnableDisableSyncCalendar,
};
