import instance from "../instance";

export function RecommendnTask(data, callback, failure) {
  return dispatch => {
    return instance()
      .post("/api/account/inviteUsers", data)
      .then(response => {
        callback(response);
      }).catch(response => {
        failure(response.response);
      });
  };
}

export default {
  RecommendnTask
};
