import constants from "./../constants/types";
import instance from "./../instance";
import { ResourceWorkloadViewType } from "../../utils/constants/ResourceWorkloadViewType";
import { CalendarTimelinePeriods } from "../../utils/constants/CalendarTimelinePeriods";
import { ResourceWorkloadUnits } from "../../utils/constants/ResourceWorkloadUnits";
import { TaskTimelineColor } from "./../../utils/constants/TaskTimelineColor";

export function dispatchResources(data) {
  return (dispatch) => {
    dispatch({
      type: constants.DUMMY,
      payload: {
        data,
      },
    });
  };
}

// data

export function setGridCalendarDatesAction(list = []) {
  return {
    type: constants.SET_GRID_CALENDAR_DATES,
    payload: list,
  };
}

export function resetGridCalendarDatesAction() {
  return {
    type: constants.RESET_GRID_CALENDAR_DATES,
  };
}

export function setGridCalendarDatesFlattened(list = []) {
  return {
    type: constants.SET_GRID_CALENDAR_DATES_FLATTENED,
    payload: list,
  };
}

export function resetGridCalendarDatesFlattenedAction() {
  return {
    type: constants.SET_GRID_CALENDAR_DATES_FLATTENED,
  };
}

export function setGridResourcesDataListAction(list = []) {
  return {
    type: constants.SET_GRID_RESOURCES_DATA_LIST,
    payload: list,
  };
}

export function appendGridResourcesDataListAction(list = []) {
  return {
    type: constants.APPEND_GRID_RESOURCES_DATA_LIST,
    payload: list,
  };
}

export function resetGridResourcesDataListAction() {
  return {
    type: constants.RESET_GRID_RESOURCES_DATA_LIST,
  };
}

export function updateResourceTotalCapacityAction(resourceId = null, capacity = 0) {
  return {
    type: constants.UPDATE_RESOURCE_TOTAL_CAPACITY,
    payload: { resourceId, capacity },
  };
}

export function setGridProjectsDataListAction(list = []) {
  return {
    type: constants.SET_GRID_PROJECTS_DATA_LIST,
    payload: list,
  };
}

export function resetGridProjectsDataListAction() {
  return {
    type: constants.RESET_GRID_PROJECTS_DATA_LIST,
  };
}

export function setGridProjectResourcesDataListAction(projectId = null, list = []) {
  return {
    type: constants.SET_GRID_PROJECT_RESOURCES_DATA_LIST,
    payload: { projectId, resourcesList: list },
  };
}

export function appendGridProjectResourcesDataListAction(list = []) {
  return {
    type: constants.APPEND_GRID_PROJECT_RESOURCES_DATA_LIST,
    payload: list,
  };
}

export function unsetGridProjectResourcesDataListAction(projectId = null) {
  return {
    type: constants.UNSET_GRID_PROJECT_RESOURCES_DATA_LIST,
    payload: projectId,
  };
}

export function resetGridProjectResourcesDataListAction() {
  return {
    type: constants.RESET_GRID_PROJECT_RESOURCES_DATA_LIST,
  };
}

export function setGridResourceTasksDataListAction(resourceId = null, list = []) {
  return {
    type: constants.SET_GRID_RESOURCE_TASKS_DATA_LIST,
    payload: { resourceId, tasksList: list },
  };
}

export function unsetGridResourceTasksDataListAction(resourceId = null) {
  return {
    type: constants.UNSET_GRID_RESOURCE_TASKS_DATA_LIST,
    payload: resourceId,
  };
}

export function resetGridResourceTasksDataListAction() {
  return {
    type: constants.RESET_GRID_RESOURCE_TASKS_DATA_LIST,
  };
}

export function setGridProjectResourceTasksDataListAction(
  projectId = null,
  resourceId = null,
  list = []
) {
  return {
    type: constants.SET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST,
    payload: { resourceId, projectId, list },
  };
}

export function unsetGridProjectResourceTasksDataListAction(projectId = null, resourceId = null) {
  return {
    type: constants.UNSET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST,
    payload: { resourceId, projectId },
  };
}

export function resetGridProjectResourceTasksDataListAction() {
  return {
    type: constants.RESET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST,
  };
}

export function setUnassignedTasksDataListAction(list = []) {
  return {
    type: constants.SET_UNASSIGNED_TASKS_DATA_LIST,
    payload: list,
  };
}

export function resetUnassignedTasksDataListAction() {
  return {
    type: constants.RESET_UNASSIGNED_TASKS_DATA_LIST,
  };
}

export const updateResourceTaskAction = (resourceId, taskId, task = {}) => ({
  type: constants.UPDATE_RESOURCE_TASK,
  payload: { resourceId, taskId, task },
});

export const updateResourceTaskDayWorkloadAction = (
  resourceId,
  taskId,
  dayIndex = -1,
  updatedHours = 0
) => ({
  type: constants.UPDATE_RESOURCE_TASK_DAY_WORKLOAD,
  payload: { resourceId, taskId, dayIndex, updatedHours },
});

export const updateResourceTaskDayWorkloadByDateAction = (
  resourceId,
  taskId,
  date = null,
  updatedHours = 0
) => ({
  type: constants.UPDATE_RESOURCE_TASK_DAY_WORKLOAD_BY_DATE,
  payload: { resourceId, taskId, date, updatedHours },
});

export const updateResourceTaskDayWorkloadIdByDateAction = (
  resourceId,
  taskId,
  date = null,
  dayId = null
) => ({
  type: constants.UPDATE_RESOURCE_TASK_DAY_WORKLOAD_ID_BY_DATE,
  payload: { resourceId, taskId, date, dayId },
});

export const deleteResourceTaskDayWorkloadByDateAction = (resourceId, taskId, dayDate = null) => ({
  type: constants.DELETE_RESOURCE_TASK_DAY_WORKLOAD_BY_DATE,
  payload: { resourceId, taskId, dayDate },
});

export const updateResourceTaskWorkloadsAction = (resourceId, taskId, workloads = null) => ({
  type: constants.UPDATE_RESOURCE_TASK_WORKLOADS,
  payload: { resourceId, taskId, workloads },
});

export const updateResourceTaskPlannedDatesAction = (
  resourceId,
  taskId,
  startDate = null,
  endDate = null
) => ({
  type: constants.UPDATE_RESOURCE_TASK_PLANNED_DATES,
  payload: { resourceId, taskId, startDate, endDate },
});

export const updateResourceTaskPlannedStartDateAction = (resourceId, taskId, startDate = null) => ({
  type: constants.UPDATE_RESOURCE_TASK_PLANNED_START_DATE,
  payload: { resourceId, taskId, startDate },
});

export const updateResourceTaskPlannedEndDateAction = (resourceId, taskId, endDate = null) => ({
  type: constants.UPDATE_RESOURCE_TASK_PLANNED_END_DATE,
  payload: { resourceId, taskId, endDate },
});

export const reassignResourceTaskAction = (
  fromResourceId,
  toResourceId,
  fromIndex = -1,
  toIndex = -1
) => ({
  type: constants.REASSIGN_RESOURCE_TASK,
  payload: { fromResourceId, toResourceId, fromIndex, toIndex },
});

export const updateProjectResourceTaskWorkloadsAction = (projectId, resourceId, taskId) => ({
  type: constants.UPDATE_PROJECT_RESOURCE_TASK_WORKLOADS,
  payload: { projectId, resourceId, taskId },
});

export const updateUnassignedTaskDayWorkloadAction = (taskId, dayIndex = -1, updatedHours = 0) => ({
  type: constants.UPDATE_UNASSIGNED_TASK_DAY_WORKLOAD,
  payload: { taskId, dayIndex, updatedHours },
});

export const updateUnassignedTaskDayWorkloadIdByDateAction = (
  taskId,
  date = null,
  dayId = null
) => ({
  type: constants.UPDATE_UNASSIGNED_TASK_DAY_WORKLOAD_ID_BY_DATE,
  payload: { taskId, date, dayId },
});

export const deleteUnassignedTaskDayWorkloadByDateAction = (taskId, dayDate = null) => ({
  type: constants.DELETE_UNASSIGNED_TASK_DAY_WORKLOAD_BY_DATE,
  payload: { taskId, dayDate },
});

export const updateUnassignedTaskWorkloadsAction = (taskId, workloads = null) => ({
  type: constants.UPDATE_UNASSIGNED_TASK_WORKLOADS,
  payload: { taskId, workloads },
});

export const updateUnassignedTaskPlannedDatesAction = (
  taskId,
  startDate = null,
  endDate = null
) => ({
  type: constants.UPDATE_UNASSIGNED_TASK_PLANNED_DATES,
  payload: { taskId, startDate, endDate },
});

export const assignUnassignedTaskToResourceAction = (
  toResourceId,
  fromIndex = -1,
  toIndex = -1
) => ({
  type: constants.ASSIGN_UNASSIGNED_TASK_TO_RESOURCE,
  payload: { toResourceId, fromIndex, toIndex },
});

export const setProjectsOfWorkspacesFilterDataListAction = (list = []) => ({
  type: constants.SET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST,
  payload: list,
});

export const setResourcesOfWorkspacesFilterDataListAction = (list = []) => ({
  type: constants.SET_RESOURCES_OF_WORKSPACES_FILTER_DATA_LIST,
  payload: list,
});

export const setUnscheduledTasksDataListAction = (list = []) => ({
  type: constants.SET_UNSCHEDULED_TASKS_DATA_LIST,
  payload: list,
});

export const resetUnscheduledTasksDataListAction = () => ({
  type: constants.RESET_UNSCHEDULED_TASKS_DATA_LIST,
});

// data-records-count

export const setGridResourcesDataRecordsCountAction = (count = 0) => ({
  type: constants.SET_GRID_RESOURCES_DATA_RECORDS_COUNT,
  payload: count,
});

export const resetGridResourcesDataRecordsCountAction = () => ({
  type: constants.RESET_GRID_RESOURCES_DATA_RECORDS_COUNT,
});

export const setGridProjectResourcesDataRecordsCountAction = (projectId = null, count = 0) => ({
  type: constants.SET_GRID_PROJECT_RESOURCES_DATA_RECORDS_COUNT,
  payload: { projectId, count },
});

export const resetGridProjectResourcesDataRecordsCountAction = (projectId = null) => ({
  type: constants.RESET_GRID_PROJECT_RESOURCES_DATA_RECORDS_COUNT,
  payload: projectId,
});

// data-flags

export function setGridResourcesDataListExistsAction(exists = false) {
  return {
    type: constants.SET_GRID_RESOURCES_DATA_LIST_EXISTS,
    payload: exists,
  };
}

export function resetGridResourcesDataListExistsAction() {
  return {
    type: constants.RESET_GRID_RESOURCES_DATA_LIST_EXISTS,
  };
}

export function setGridProjectsDataListExistsAction(exists = false) {
  return {
    type: constants.SET_GRID_PROJECTS_DATA_LIST_EXISTS,
    payload: exists,
  };
}

export function resetGridProjectsDataListExistsAction() {
  return {
    type: constants.RESET_GRID_PROJECTS_DATA_LIST_EXISTS,
  };
}

export const setGridProjectResourcesDataListExistsAction = (projectId = null, exists = false) => ({
  type: constants.SET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS,
  payload: { projectId, exists },
});

export const unsetGridProjectResourcesDataListExistsAction = (projectId = null) => ({
  type: constants.UNSET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS,
  payload: projectId,
});

export const resetGridProjectResourcesDataListExistsAction = () => ({
  type: constants.RESET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS,
});

export const setGridResourceTasksDataListExistsAction = (resourceId = null, exists = false) => ({
  type: constants.SET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS,
  payload: { resourceId, exists },
});

export const unsetGridResourceTasksDataListExistsAction = (resourceId = null) => ({
  type: constants.UNSET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS,
  payload: resourceId,
});

export const resetGridResourceTasksDataListExistsAction = () => ({
  type: constants.RESET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS,
});

export const setGridProjectResourceTasksDataListExistsAction = (
  projectId = null,
  resourceId = null,
  exists = false
) => ({
  type: constants.SET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS,
  payload: { projectId, resourceId, exists },
});

export const unsetGridProjectResourceTasksDataListExistsAction = (
  projectId = null,
  resourceId = null
) => ({
  type: constants.UNSET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS,
  payload: { projectId, resourceId },
});

export const resetGridProjectResourceTasksDataListExistsAction = () => ({
  type: constants.RESET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS,
});

export const setUnassignedTasksDataListExistsAction = (exists = false) => ({
  type: constants.SET_UNASSIGNED_TASKS_DATA_LIST_EXISTS,
  payload: exists,
});

export const resetUnassignedTasksDataListExistsAction = () => ({
  type: constants.RESET_UNASSIGNED_TASKS_DATA_LIST_EXISTS,
});

export const setProjectsOfWorkspacesFilterDataListExistsAction = (exists = false) => ({
  type: constants.SET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST_EXISTS,
  payload: exists,
});

export const resetProjectsOfWorkspacesFilterDataListExistsAction = () => ({
  type: constants.RESET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST_EXISTS,
});

export const setUnscheduledTasksDataListExistsAction = (exists = false) => ({
  type: constants.SET_UNSCHEDULED_TASKS_DATA_LIST_EXISTS,
  payload: exists,
});

export const resetUnscheduledTasksDataListExistsAction = () => ({
  type: constants.RESET_UNSCHEDULED_TASKS_DATA_LIST_EXISTS,
});

// ui-grid

export function setUiSidebarCollapsedAction(isCollapsed = false) {
  return {
    type: constants.SET_UI_SIDEBAR_COLLAPSED,
    payload: isCollapsed,
  };
}

export function resetUiSidebarCollapsed() {
  return {
    type: constants.RESET_UI_SIDEBAR_COLLAPSED,
  };
}

export function setDayProgressHourglassWidthAction(dayProgressHourglassWidth = "") {
  return {
    type: constants.SET_DAY_PROGRESS_HOURGLASS_WIDTH,
    payload: dayProgressHourglassWidth,
  };
}

export function resetDayProgressHourglassWidth() {
  return {
    type: constants.RESET_DAY_PROGRESS_HOURGLASS_WIDTH,
  };
}

export function setGridColumnsCount(count = 0) {
  return {
    type: constants.SET_GRID_COLUMNS_COUNT,
    payload: count,
  };
}

export function resetGridColumnsCount() {
  return {
    type: constants.RESET_GRID_COLUMNS_COUNT,
  };
}

export function setResourceWorkloadViewTypeAction(type = ResourceWorkloadViewType.resources.value) {
  return {
    type: constants.SET_WORKLOAD_VIEW_TYPE,
    payload: type,
  };
}

export function resetResourceWorkloadViewType() {
  return {
    type: constants.RESET_WORKLOAD_VIEW_TYPE,
  };
}

export function setCalendarTimelinePeriodStartDateAction(
  type = ResourceWorkloadViewType.resources.value
) {
  return {
    type: constants.SET_CALENDAR_TIMELINE_PERIOD_START_DATE,
    payload: type,
  };
}

export function resetCalendarTimelinePeriodStartDateAction() {
  return {
    type: constants.RESET_CALENDAR_TIMELINE_PERIOD_START_DATE,
  };
}

export const setCalendarTimelinePeriodEndDateAction = (
  type = ResourceWorkloadViewType.resources.value
) => ({
  type: constants.SET_CALENDAR_TIMELINE_PERIOD_END_DATE,
  payload: type,
});

export const resetCalendarTimelinePeriodEndDateAction = () => ({
  type: constants.RESET_CALENDAR_TIMELINE_PERIOD_END_DATE,
});

export function increaseOpenCollapsiblesCountAction() {
  return {
    type: constants.INCREASE_OPEN_COLLAPSIBLES_COUNT,
  };
}

export function decreaseOpenCollapsiblesCountAction() {
  return {
    type: constants.DECREASE_OPEN_COLLAPSIBLES_COUNT,
  };
}

// settings

export function setSettingsObjectAction(settings = {}) {
  return {
    type: constants.SET_SETTINGS_OBJECT,
    payload: settings,
  };
}

export function resetSettingsObjectAction() {
  return {
    type: constants.RESET_SETTINGS_OBJECT,
  };
}

export function setSettingsShowTaskNameWithTimelineAction(isShown = true) {
  return {
    type: constants.SET_SETTINGS_SHOW_TASK_NAME_WITH_TIMELINE,
    payload: isShown,
  };
}

export function resetSettingsShowTaskNameWithTimelineAction() {
  return {
    type: constants.RESET_SETTINGS_SHOW_TASK_NAME_WITH_TIMELINE,
  };
}

export function setSettingsTaskColorAction(color = TaskTimelineColor.taskStatus.value) {
  return {
    type: constants.SET_SETTINGS_TASK_COLOR,
    payload: color,
  };
}

export function resetSettingsTaskColorAction() {
  return {
    type: constants.RESET_SETTINGS_TASK_COLOR,
  };
}

// filters

export const setFilterCalendarTimelinePeriodAction = (
  calendarTimelinePeriod = CalendarTimelinePeriods.week.value
) => ({
  type: constants.SET_FILTER_CALENDAR_TIMELINE_PERIOD,
  payload: calendarTimelinePeriod,
});

export const resetFilterCalendarTimelinePeriod = () => ({
  type: constants.RESET_FILTER_CALENDAR_TIMELINE_PERIOD,
});

export const setFilterProgressHourglassUnitAction = (
  progressHourglassUnit = ResourceWorkloadUnits.hour.value
) => ({
  type: constants.SET_FILTER_PROGRESS_HOURGLASS_UNIT,
  payload: progressHourglassUnit,
});

export const resetFilterProgressHourglassUnit = () => ({
  type: constants.RESET_FILTER_PROGRESS_HOURGLASS_UNIT,
});

export const setFilterWorkspacesAction = (ids = []) => ({
  type: constants.SET_FILTER_WORKSPACES,
  payload: ids,
});

export const resetFilterWorkspacesAction = () => ({
  type: constants.RESET_FILTER_WORKSPACES,
});

export const setFilterResourcesAction = (ids = []) => ({
  type: constants.SET_FILTER_RESOURCES,
  payload: ids,
});

export const resetFilterResourcesAction = () => ({
  type: constants.RESET_FILTER_RESOURCES,
});

export const setFilterProjectsAction = (ids = []) => ({
  type: constants.SET_FILTER_PROJECTS,
  payload: ids,
});

export const resetFilterProjectsAction = () => ({
  type: constants.RESET_FILTER_PROJECTS,
});

// extra

export const updateProfileUrlsFromUserInfoReduxObjectToResourcesGridListingAction = (
  idImageUrlDataMap = []
) => ({
  type: constants.UPDATE_PROFILE_URLS_FROM_USER_INFO_REDUX_OBJECT_TO_RESOURCES_GRID_LISTING,
  payload: idImageUrlDataMap,
});

export const updateProfileUrlsFromUserInfoReduxObjectToResourcesFilterListingAction = (
  idImageUrlDataMap = []
) => ({
  type: constants.UPDATE_PROFILE_URLS_FROM_USER_INFO_REDUX_OBJECT_TO_RESOURCES_FILTER_LISTING,
  payload: idImageUrlDataMap,
});

export function ResourcesApiSyntax(data, callback, failure, dispatchFn) {
  return instance()
    .get("/api_url_here", data)
    .then((response) => {
      dispatchFn(dispatchResources(data));
      return response;
    })
    .then((response) => {
      callback(response.data);
    })
    .catch((response) => {
      failure(response.response);
    });
}
