
import helper from "../../helper/index";

const GETHEADERS = function () {
    let token = helper.getToken();

    return {
        headers: { "headers": { "Authorization": token, "Content-Type": "application/json" } }
    };
};

export default {
    GETHEADERS
};