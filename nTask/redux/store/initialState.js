import resourcesInitialState from "../initialStates/resources";

const getInitialState = () => {
  const initialState = {
    registerResponse: {
      data: "",
    },
    forgotPassword: {
      data: "",
    },
    authenticationResponse: {
      Auth: false,
    },
    socialAuthenticationResponse: {
      data: "",
    },
    reports: {
      data: [],
    },
    reportingFilters: {

    },
    quickReportingFilters: {

    },
    profile: {
      data: { teams: [], teamMember: [] },
    },
    teamMembers: {
      data: "",
    },
    projects: {
      data: [],
      projectFilter: {
      },
      savedFilters: [],
      quickFilters: {},
    },
    columns: {
      issue: [],
      meeting: [],
      project: [],
      risk: [],
      task: [],
      allColumns: []
    },
    noInternet: false,
    loadingProjectDetails: false,
    projectDetailsDialog: {
      fullView: true,
      dockedView: false,
      sidePanel: false,
      chatView: false,
      docsView: false,
      activityLogView: false,
      dialog: true,
      loadingState: false,
    },
    dialogStates: {
      publicLink: {},
      riskDetailDialog: {
        id: "",
        afterCloseCallBack: () => { },
        isArchived: false,
      },
      deleteDialogue: {
        open: false,
        successAction: () => { },
        alignment: "center",
        successBtnText: "",
        cancelBtnText: "",
        children: null,
        headingText: "",
        btnQuery: "",
        msgText: "",
        disabled: false,
        styles: null,
        deleteBtnText: "",
        deleteBtnProps: null,
      },
      customFieldDialog: {
        moduleViewType: "",
        data: {},
        mode: "",
      },
      taskDetailDialog: {
        id: "",
        afterCloseCallBack: () => { },
        onDeleteTask: () => { },
        isGantt: false,
        isTaskOverview: false,
        GanttCmp: null,
        childTasks: [],
        type: "",
        filterUnArchiveTask: () => { },
        deleteTaskFromArchiveList: () => { },
        isArchived: false,
        workspaceStatus: null,
        taskPermission: null
      },
      issueDetailDialog: {
        id: "",
        afterCloseCallBack: () => { },
        isUpdated: () => { },
        closeActionMenu: () => { },
        issueWorkspacePer: null,
        isArchived: false,
      },
      profileSettings: {
        open: false,
      },
    },
    workspace: {},
    constants: {
      data: "",
    },
    getProfile: {
      data: "",
    },
    logout: {},
    todoItemsData: {
      data: [],
    },
    notifications: {
      data: "",
    },
    tasks: {
      data: [],
      taskFilter: {},
      savedFilters: [],
      quickFilters: {},
      removeArchivedTask: null,
    },
    issues: {
      data: [],
      issueFilter: {},
      savedFilters: [],
      quickFilters: {},
    },
    meetings: {
      data: [],
      meetingFilter: {},
      savedFilters: [],
      quickFilters: {},
    },
    risks: {
      data: [] /*In case of old data if payload is null, "" , or undefined then intialState for risk will be empty array*/,
      riskFilter: {
      },
      savedFilters: [],
      quickFilters: {},
      removeArchivedRisk: null,
    },
    userPreference: {
      data: "",
    },
    itemOrder: {
      data: {},
    },
    tempData: {
      data: "",
    },
    filteredTasksData: {
      data: "",
    },
    filteredMeetingsData: {
      data: "",
    },
    filteredIssuesData: {
      data: "",
    },
    filteredRisksData: {
      data: "",
    },
    taskCommentsData: {
      data: "",
    },
    taskNotifications: { data: "" },
    taskActivities: { data: "" },
    issueNotifications: { data: "" },
    issueActivities: { data: "" },
    riskNotifications: { data: "" },
    riskActivities: { data: "" },
    issueCommentsData: {
      data: "",
    },
    timesheet: {
      timeEntry: {
        projectTimesheetVM: [],
      },
      pendingApproved: {},
    },
    activityLogs: {
      data: "",
    },
    workspacePermissions: {
      data: { workSpace: {} },
    },
    featureAccessPermissions: {
      project: {},
      task: {},
      issue: {},
      risk: {},
    },
    backProcesses: {
      import: [],
      export: [],
      delete: [],
    },
    workspaceTemplates: {
      data: {
        allWSTemplates: [],
        defaultWSTemplate: "",
      },
    },
    projectTemplates: {
      data: {
        allProjectTemplates: [],
        defaultProjectTemplate: "",
      },
    },
    sidebar: "",
    globalTimerTask: null,
    permissionSettings: {
      data: "",
    },
    appliedFilters: {
      Project: null,
      Task: null,
      Meeting: null,
      Issue: null,
      Risk: null,
      Board: null,
    },
    userBillPlan: {
      data: "",
    },
    googleCalendar: {
      data: {
        accountsAndCalendar: [],
        googleIntegration: false,
        syncCalender: {},
        teams: [],
        projects: [],
        workspaces: [],
        syncCalenderList: [],
      },
    },
    outlookCalendar: {
      data: {
        syncOtherCalendar: {},
        teams: [],
        projects: [],
        workspaces: [],
        syncedCalendar: [],
      },
    },
    appleCalendar: {
      data: {
        syncOtherCalendar: {},
        teams: [],
        projects: [],
        workspaces: [],
        syncedCalendar: [],
      },
    },
    chats: {
      data: "",
    },
    promoBar: { promoBar: false },
    notificationBar: { notificationBar: false },
    notification: {
      show: false,
      buttonText: "",
      primaryAction: () => { },
      secondaryAction: () => { },
      message: "",
    },
    boards: {
      data: {
        boards: false,
        kanban: {},
        unsortedTask: [],
        activitylog: {},
      },
    },
    customFields: {
      data: [],
      groupCustomFields: [],
      sections: [],
    },
    archivedItems: {
      data: [],
    },
    sidebarPannel: {
      oldSidebar: localStorage.getItem("oldSidebarView") == 'true',
      open: true,
      width: 221,
      data: {
        teamMenu: [],
        workspaceMenu: [],
        analyticsMenu: []
      },
    },
    resources: resourcesInitialState,
  };
  return initialState;
};
export default {
  getInitialState,
};
