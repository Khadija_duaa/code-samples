import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { routerReducer, routerMiddleware } from "react-router-redux";
import registerResponse from "./../reducers/signUp";
import forgotPassword from "./../reducers/forgotPassword";
import authenticationResponse from "./../reducers/authentication";
import socialAuthenticationResponse from "./../reducers/socialAuthentication";
import profile from "./../reducers/profile";
import logout from "./../reducers/logout";
import teamMembers from "./../reducers/teamMembers";
import projects from "./../reducers/projects";
import workspace from "./../reducers/workspace";
import constants from "./../reducers/constants";
import getProfile from "./../reducers/getProfile";
import tasks from "./../reducers/tasks";
import notifications from "./../reducers/notifications";
import meetings from "./../reducers/meetings";
import issues from "./../reducers/issues";
import risks from "./../reducers/risks";
import columns from "./../reducers/columns";
import calenderTasks from "./../reducers/calenderTasks";
import userPreference from "./../reducers/userPreference";
import itemOrder from "./../reducers/itemOrder";
import tempData from "./../reducers/tempdata";

import filteredTasks from "./../reducers/filteredTasks";
import filteredIssues from "./../reducers/filteredIssues";
import filteredRisks from "./../reducers/filteredRisks";
import filteredMeetings from "./../reducers/filteredMeetings";
import taskComments from "../reducers/taskComments";
import issueComments from "../reducers/issueComments";
import riskComments from "../reducers/riskComments";
import taskActivities from "../reducers/taskActivities";
import taskNotifications from "../reducers/taskNotifications";
import issueActivities from "../reducers/issueActivities";
import issueNotifications from "../reducers/issueNotifications";
import riskActivities from "../reducers/riskActivities";
import riskNotifications from "../reducers/riskNotifications";
import reports from "../reducers/reports";
import initial from "./initialState";
import ganttTasks from "../reducers/gantt";
import ganttComponent from "../reducers/ganttComponent";
import timesheet from "../reducers/timesheet";
import globalTimerTask from "../reducers/globalTimerTask";
import activityLogs from "../reducers/activityLogs";
import permissions from "../reducers/permissions";
import featureAccessPermissions from "../reducers/featureAccessPermission";
import permissionSettings from "../reducers/permissionSettings";
import { loadingBarReducer } from "react-redux-loading-bar";
import sidebar from "../reducers/sidebar";
import appliedFilters from "../reducers/appliedFilters";
import userBillPlan from "./../reducers/userBillPlan";
import promoBar from "../reducers/promoBar";
import notificationBar from "../reducers/notificationBar";
import notification from "../reducers/notification";
import noInternet from "../reducers/noInternet";
import todoItems from "../reducers/todoItems";
import googleCalendar from "../reducers/googleCalendar";
import outlookCalendar from "../reducers/outlookCalendar";
import appleCalendar from "../reducers/appleCalendar";
import whiteLabelInfo from "../reducers/whiteLabelInfo";
import projectDetailsDialog from '../reducers/projectDetailsDialog'
import chat from '../reducers/chat'
import collab from '../reducers/collaboration'
import documents from '../reducers/documents'
import dialogStates from '../reducers/allDialogs';
import boards from '../reducers/boards';
import backprocesses from '../reducers/backProcesses';
import loadingProjectDetails from '../reducers/projectDetailsOpenClose';
import workspaceTemplates from '../reducers/workspaceTemplates';
import projectTemplates from '../reducers/projectTemplates';
import customFields from '../reducers/customFields';
import archivedItems from '../reducers/archivedItems';
import reportingFilters from '../reducers/reportingFilters';
import sidebarPannel from "../reducers/sidebarPannel";
import resources from "./../reducers/resources";
const appReducer = combineReducers({
  ...registerResponse,
  ...forgotPassword,
  ...authenticationResponse,
  ...socialAuthenticationResponse,
  ...profile,
  ...logout,
  ...teamMembers,
  ...projects,
  ...resources,
  ...workspace,
  ...constants,
  ...getProfile,
  ...tasks,
  ...reportingFilters,
  ...notifications,
  ...meetings,
  ...issues,
  ...risks,
  ...userPreference,
  ...itemOrder,
  ...tempData,
  ...filteredTasks,
  ...filteredIssues,
  ...filteredRisks,
  ...filteredMeetings,
  ...taskComments,
  ...issueComments,
  ...riskComments,
  ...permissions,
  ...featureAccessPermissions,
  ...taskNotifications,
  ...taskActivities,
  ...riskActivities,
  ...riskNotifications,
  ...issueActivities,
  ...issueNotifications,
  ganttTasks,
  ...timesheet,
  ...globalTimerTask,
  ...sidebar,
  ...activityLogs,
  ...permissionSettings,
  ...appliedFilters,
  reports,
  loadingBar: loadingBarReducer,
  router: routerReducer,
  ...userBillPlan,
  ganttComponent,
  notification,
  promoBar,
  notificationBar,
  noInternet,
  ...todoItems,
  ...googleCalendar,
  ...outlookCalendar,
  ...appleCalendar,
  ...whiteLabelInfo,
  projectDetailsDialog,
  chat,
  collab,
  documents,
  dialogStates,
  columns,
  ...boards,
  ...backprocesses,
  ...loadingProjectDetails,
  ...workspaceTemplates,
  ...projectTemplates,
  ...customFields,
  ...archivedItems,
  ...sidebarPannel,
});

const rootReducer = (state, action) => {
  if (action.type === "USER_LOGOUT") {
    state = initial.getInitialState();
  }
  return appReducer(state, action);
};

const configureStore = opts => {
  const store = createStore(rootReducer, initial.getInitialState(), applyMiddleware(thunk));

  return store;
  
};


export default configureStore;
