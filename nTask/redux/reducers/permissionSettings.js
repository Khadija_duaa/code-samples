import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
const permissionSettings = (state = {}, action) => {
  switch (action.type) {
    case constants.PERMISSIONSETTINGS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.COPYPERMISSION:
      let tempPermission = cloneDeep(state.data);
      let newPermission = tempPermission.defaultRoles || [];
      newPermission.push(action.payload.data);
      return {
        ...state,
        data: { defaultRoles: newPermission }
      };
      break;

    case constants.UPDATEPERMISSION:
      tempPermission = cloneDeep(state.data);
      newPermission = tempPermission.defaultRoles || [];
      const modifiedData = newPermission.map(element => {
        if (action.payload.data.roleId == element.roleId) {
          return action.payload.data;
        } else {
          return element;
        }
      });
      return {
        ...state,
        data: { defaultRoles: modifiedData }
      };
      break;

    case constants.DELETEPERMISSION:
      tempPermission = cloneDeep(state.data);
      newPermission = tempPermission.defaultRoles || [];
      const updatedData = newPermission.filter(element => {
        return element.id !== action.payload.data;
      });
      return {
        ...state,
        data: { defaultRoles: updatedData }
      };
      break;

    // case constants.DELETEPERMISSION:
    //   // tempPermission = cloneDeep(state.data);
    //   // newPermission = tempPermission.defaultRoles || [];
    //   // const updatedData = newPermission.filter(element => {
    //   //   return element.id !== action.payload.data;
    //   // });
    //   return {
    //     ...state,
    //     data: action.payload.data
    //   };
    default:
      return state;
  }
};

export default {
  permissionSettings
};
