import constants from "../constants/types";
const initialState = {
  data: {
    activeRoomId: "",
    chatItems: [],
    typingStatus: {
      typing: false,
      userIds: [],
    },
  },
};

const chat = (state = initialState, action) => {
  let newData;
  switch (action.type) {
    case constants.POPULATECHAT:
      return {
        ...state,
        data: {
          activeRoomId: action.payload.groupId,
          chatItems: action.payload.chat,
          typingStatus: {
            typing: false,
            userIds: [],
          },
        },
      };
      break;

    case constants.CHATTYPINGSTATUS: {
      if (state.data.activeRoomId != action.payload.groupId) return state;
      const userId = state.data.typingStatus.userIds;
      newData = { ...state.data };
      if (action.payload.data.update == true) {
        newData.typingStatus.userIds = [...userId, action.payload.data.userId];
      } else if (action.payload.data.update == false) {
        newData.typingStatus.userIds = newData.typingStatus.userIds.filter(item => item != action.payload.data.userId)
      }
      // newData.typingStatus = {
      //   typing: action.payload.data.update,
      //   userId: [...userId, action.payload.data.userId],
      // };
      return {
        ...state,
        data: newData,
      };
    }
      break;

    case constants.UPDATECHATITEM:
      if (state.data.activeRoomId != action.payload.groupId) return state;
      newData = { ...state.data };
      newData.chatItems = state.data.chatItems.map(function (element) {
        if (element.comment.commentId == action.payload.chat.comment.commentId) {
          return {
            ...action.payload.chat,
          };
        }
        return element;
      });
      return {
        ...state,
        data: newData,
      };
      break;

    case constants.DELETECHATITEM:
      if (state.data.activeRoomId != action.payload.groupId) return state;
      newData = { ...state.data };
      if (action.payload.chat.comment.parentId) {
        // it is for filter child item
        newData.chatItems = newData.chatItems.filter(element =>
          element.comment.commentId !== action.payload.chat.comment.commentId
        );
      } else {
        //it is for filter parent comment and all child item that has this parend id
        newData.chatItems = newData.chatItems.filter(element =>
          !(element.comment.commentId == action.payload.chat.comment.commentId ||
            element.comment.parentId == action.payload.chat.comment.commentId)
        );
      }
      return {
        ...state,
        data: newData,
      };
      break;

    case constants.NEWCHATITEM:
      if (state.data.activeRoomId != action.payload.groupId) return state;
      newData = { ...state.data };
      if (state.data.chatItems && state.data.chatItems.length) {
        newData.chatItems = [...state.data.chatItems, action.payload.chat];
        return {
          ...state,
          data: newData,
        };
      } else {
        newData.chatItems = [action.payload.chat];
        return {
          ...state,
          data: newData,
        };
      }
      break;

    case constants.CLEARCHAT:
      if (state.data.activeRoomId != action.payload.groupId) return state;
      return {
        ...state,
        data: {
          activeRoomId: "",
          chatItems: [],
          typingStatus: {
            typing: false,
            userIds: [],
          },
        },
      };
      break;
      
    default:
      return state;
  }
};

export default chat;
