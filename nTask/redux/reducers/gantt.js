import consts from "./../constants/types";

export default function (state = {}, action) {

  switch (action.type) {
    case consts.GETGANTTTASKS:
      return { ...state, ...action.payload };
      break;

    case consts.SAVEGANTTLINKS: {
      return { ...state, links: [action.payload, ...state.links] };
    }
      break;

    case consts.UPDATEGANTTASK: {
      const newTaskArray = state.data
        ? state.data.map(task => {
          if (task.id == action.payload.id) {
            return action.payload;
          } else {
            return task;
          }
        })
        : [];

      return { ...state, data: newTaskArray };
    }
      break;
    case consts.UPDATEGANTTASKVALUE: {
      const newTaskArray = state.data
        ? state.data.map(task => {
          if (task.id == action.payload.taskId) { 
            return {...task, progress : action.payload.progress};
          } else {
            return task;
          }
        })
        : [];

      return { ...state, data: newTaskArray };
    }
      break;

    case consts.SAVESORTORDER: {
      let newTaskArray = JSON.parse(JSON.stringify(state.data));
      newTaskArray.forEach(task => {
        let filteredGanttTask = action.payload.find(ganttTask => {
          return ganttTask.id == task.id;
        });
        if (filteredGanttTask) {
          task.sortorder = filteredGanttTask.$index;
        }
      });

      return { ...state, data: newTaskArray };
    }
      break;

    case consts.BULKUPDATEGANTT: {
      const newTaskArray = state.data.map(function (task) {
        let updatedTask = action.payload.taskDto.filter(t => {
          return t.id == task.id;
        })[0];
        if (updatedTask) {
          return updatedTask;
        } else {
          return task;
        }
      });
      return { ...state, data: newTaskArray };
    }
      break;

    case consts.CREATEGANTTASK: {
      return { ...state, data: [action.payload, ...state.data] };
    }
      break;

    case consts.REMOVEGANTTASK: {
      let data = state.data.filter(t => {
        return t.taskId !== action.payload.taskId;
      });
      return { ...state, data };
    }
      break;

    case consts.SAVEGANTTCOLUMNORDER: {
      return { ...state, ganttTaskColumn: action.payload };
    }
      break;

    case consts.DELETETASKLINK: {
      const links =
        state && state.links
          ? state.links.filter(ele => {
            return ele.id != action.linkId;
          })
          : [];
      return { ...state, links: links };
    }
      break;

    case consts.ADDGANTTBULKTASK: {
      //Extracting new task added for gantt task
      const newGanttTask = action.payload.newTasks.map(task => {
        return task.taskDto;
      });
      //Extracting updated task added for gantt task
      const updatedGanttTask = action.payload.updatedTasks.map(task => {
        return task.taskDto;
      });
      //Combining both updated and new tasks arrays
      const allTasksArr = [...newGanttTask, ...updatedGanttTask];

      return { ...state, data: [...state.data, ...allTasksArr] };
    }
      break;


    default:
      return state;
  }
}
