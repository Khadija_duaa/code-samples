import constants from "../constants/types";

const columns = (state = {}, action) => {
  switch (action.type) {
    case constants.SET_COLUMN_LIST:
      let columns = action.payload
        .sort((pv, cv) => {
          return pv.position - cv.position;
        })
        .reduce((r, cv) => {
          if (r[cv.groupType]) {
            r[cv.groupType].push(cv);
          } else {
            r[cv.groupType] = [cv];
          }

          return r;
        }, {});
      columns.allColumns = action.payload;
      return columns;
      break;

    case constants.ADD_GRID_COLUMN: {
      const groupType = action.payload[0].groupType;
      const updateColumn = [...state[groupType], ...action.payload];
      return { ...state, [groupType]: updateColumn };
    }
      break;

    case constants.UPDATE_COLUMN: {
      const groupType = action.payload[0].groupType;
      const updatedColumns = state[groupType]
        .reduce((r, cv) => {
          const colToUpdate = action.payload.find(c => c.id == cv.id);
          if (colToUpdate) {
            r.push(colToUpdate);
          } else {
            r.push(cv);
          }
          return r;
        }, [])
        .sort((pv, cv) => {
          return pv.position - cv.position;
        });
      return { ...state, [groupType]: updatedColumns };
    }
      break;

    case constants.UPDATE_TEAM_COLUMN: {
      const groupType = action.payload[0].groupType;
      const updatedColumns = state[groupType]
        .reduce((r, cv) => {
          const colToUpdate = action.payload.find(c => c.id == cv.id);
          if (colToUpdate) {
            r.push(colToUpdate);
          } else {
            r.push(cv);
          }
          return r;
        }, [])
        .sort((pv, cv) => {
          return pv.position - cv.position;
        });
      return { ...state, [groupType]: updatedColumns };
    }
      break;

    case constants.UPDATE_GRID_COLUMN: {
      const groupType = action.payload.groupType;
      const updatedColumns = state[groupType]
        .reduce((r, cv) => {
          const colToUpdate = cv.id === action.payload.id;
          if (colToUpdate) {
            r.push({ ...cv, columnName: action.payload.columnName, sectionId: action.payload.sectionId });
          } else {
            r.push(cv);
          }
          return r;
        }, [])
        .sort((pv, cv) => {
          return pv.position - cv.position;
        });
      return { ...state, [groupType]: updatedColumns };
    }
      break;
    default:
      return state;
  }
};

export default columns;
