import unionBy from 'lodash/unionBy';
import xorBy from 'lodash/xorBy';
import cloneDeep from 'lodash/cloneDeep';
import findIndex from 'lodash/findIndex';
import sortBy from 'lodash/sortBy';
import constants from "./../constants/types";
import searchQuery from "../../components/CustomTable2/ColumnSettingDropdown/searchQuery";

const meetings = (state = {}, action) => {
  switch (action.type) {
    case constants.POPULATEMEETINGSINFO:
      return {
        ...state,
        ...action.payload
      };
      break;

    case constants.UPDATEMEETINGSINFO:
      let modifiedMeetings = state.data.map(function (element) {
        if (element.meetingId === action.payload.data.meetingId) {
          return {
            ...action.payload.data
          };
        }
        return element;
      });
      return {
        ...state,
        data: modifiedMeetings
      };
      break;

    case constants.UPDATEFOLLOWUPACTION: {
      let modifiedMeetings = state.data.map((element) => {
        if (element.meetingId === action.payload.meetingId) {
          let meeting = cloneDeep(element);
          if (meeting.followUpActions && meeting.followUpActions.length) {
            const followupActionIndex = findIndex(meeting.followUpActions, function (o) {
              return o.followUpActionId === action.payload.followupAction.followUpActionId;
            });
            meeting.followUpActions[followupActionIndex] = action.payload.followupAction;
          }
          return meeting;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedMeetings
      };
    }
      break;

    case constants.ARCHIVEMEETINGS: {
      //
      let modifiedMeetings = state.data.map(element => {
        if (element.meetingId == action.payload.data.meetingId) {
          return action.payload.data;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedMeetings
      };
    }
      break;

    case constants.UNARCHIVEMEETINGS: {
      let modifiedMeetings = [];
      modifiedMeetings = state.data.map(element => {
        if (element.meetingId == action.payload.data.meetingId) {
          return action.payload.data;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedMeetings
      };
    }
      break;

    case constants.CANCELMEETINGSDATA: {
      let modifiedMeetings = state.data.map(element => {
        if (element.meetingId == action.payload.data.meetingId) {
          return action.payload.data;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedMeetings
      };
    }
      break;

    case constants.DELETEMEETINGSCHEDULE: {
      let updateMeeting = [];
      updateMeeting = state.data.filter(
        x => x.parentId !== action.payload.parentId
      );
      return {
        ...state,
        data: updateMeeting
      };
    }
      break;

    case constants.UPDATEBULKMEETINGINFO: {
      if (action.payload.isDelete) {
        // Formating action data risk's id array, with id object
        let inputData = action.payload.data.map((meetingId) => { return { 'meetingId': meetingId } });
        let data = xorBy(inputData || [], state.data || [], 'meetingId');
        return {
          ...state,
          data
        };
      } else {
        // Merge new risks data with store risks,
        // In this case action data risks will be prefered by store data risks
        let data = unionBy(action.payload.data || [], state.data || [], 'id');
        return {
          ...state,
          data
        };
      }
    }
      break;

    case constants.COPYMEETING: {
      let data = unionBy([{ ...action.payload.data, isNew: true }] || [], state.data || [], 'id');
      return {
        ...state,
        data
      };
    }
      break;

    case constants.COPYMEETINGSCHEDULE: {
      let parentId = action.payload.meetingId || null;
      let modifiedUpdatedBulkMeetings = state.data
        .filter(function (element) {
          return parentId !== element.meetingId;
        })
        .filter(x => parentId !== x.parentId);
      return {
        ...state,
        data: [...action.payload.data, ...modifiedUpdatedBulkMeetings]
      };
    }
      break;

    case constants.RESETMEETINGSINFO: {
      return {
        ...state,
        meetings: ""
      };
    }
      break;

    case constants.DELETEMEETING: {
      return {
        ...state,
        data: state.data.filter(function (element) {
          return element.meetingId !== action.payload.data;
        })
      };
    }
      break;

    case constants.ADDMEETING: {
      let obj = action.payload.data;

      return {
        ...state,
        data: [obj, ...state.data]
      };
    }
      break;

    case constants.UPDATEMEETINGROWS: {
      let updatedRows = action.payload;
      return {
        ...state,
        data: updatedRows
      };
    }
      break;

    case constants.SAVE_ALL_MEETINGS: {
      return { ...state, data: action.payload, };
    }
      break;
    // new reducers

    case constants.CREATE_MEETING: {
      return { ...state, data: [action.payload, ...state.data] };
    }
      break;

    case constants.CREATED_MEETING_EDIT: {
      const updatedMeetingsArr = state.data.map(t => {
        if (t.clientId == action.payload.clientId) {
          return { ...action.payload, isNew: true };
        }
        return t;
      });
      return { ...state, data: updatedMeetingsArr };
    }
      break;

    case constants.UPDATE_MEETING_ORDER:
      const sortedArray = sortBy(state.data, function (obj) {
        return findIndex(action.payload, { itemId: obj.meetingId });
      });
      return { ...state, data: sortedArray };
      break;

    // bulk actions 

    case constants.UPDATE_BULK_MEETINGS: {
      let updatedMeetingData = state.data.reduce((result, cv) => {
        const isExist = action.payload.find(item => item.meetingId === cv.meetingId);
        if (isExist) result.push(isExist);
        else result.push(cv);
        return result;
      }, []);
      return { ...state, data: updatedMeetingData };
    }
      break;

    case constants.DELETE_BULK_MEETING:
      {
        let meetings = state.data;
        let updatedMeeting = meetings.filter(x => {
          return !action.payload.meetingIds.includes(x.meetingId);
        });
        return { ...state, data: updatedMeeting };
      }
      break;

    case constants.UNARCHIVE_BULK_MEETING:
      {
        let meetings = state.data;
        let updatedMeeting = meetings.map(x => {
          if (action.payload.meetingIds.includes(x.meetingId)) {
            return { ...x, isDeleted: false };
          } else return x;
        });
        return { ...state, data: updatedMeeting };
      }
      break;

    // filters
    case constants.ADD_NEW_MEETING_CUSTOM_FILTER: {
      let updatedFilters = action.payload.reduce((res, cv) => {
        let currentValue = cloneDeep(cv);
        if (cv.customFieldsFilters) {
          let customFilters = Object.keys(cv.customFieldsFilters);
          customFilters.map(item => (currentValue[item] = cv.customFieldsFilters[item]));
          delete currentValue.customFieldsFilters;
          res.push(currentValue);
        } else res.push(cv);
        return res;
      }, []);
      return { ...state, savedFilters: updatedFilters };
    }
      break;

    case constants.UPDATE_MEETING_FILTER: {
      const cloneState = cloneDeep(state.meetingFilter);
      let updateFilters;
      const payloadLength = Object.keys(action.payload).length;
      if (payloadLength) {
        if (state.meetingFilter && state.meetingFilter.filterId !== action.payload.filterId) {
          updateFilters = { meetingFilter: action.payload }
        } else {
          updateFilters = { meetingFilter: { ...cloneState, ...action.payload } };
        }
      } else {
        updateFilters = { meetingFilter: {} }
      }
      searchQuery.meetingFilter = updateFilters.meetingFilter;
      return { ...state, ...updateFilters };
    }
      break;

    case constants.DELETE_MEETING:
      return {
        ...state,
        data: state.data
          ? state.data.filter(function (element) {
            return element.meetingId !== action.payload.data;
          })
          : [],
      };
      break;

    case constants.CLEAR_MEETING_FILTER: {
      searchQuery.meetingFilter = {};
      return { ...state, meetingFilter: {} };
    }
      break;

    case constants.SET_MEETING_CUSTOM_FILTER:
      {
        let updatedFilters = action.payload.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (cv.customFieldsFilters) {
            let customFilters = Object.keys(cv.customFieldsFilters);
            customFilters.map(item => (currentValue[item] = cv.customFieldsFilters[item]));
            delete currentValue.customFieldsFilters;
            res.push(currentValue);
          } else res.push(cv);
          return res;
        }, []);
        const defaultFilter = updatedFilters.find(f => f.defaultFilter);
        searchQuery.meetingFilter = defaultFilter || searchQuery.meetingFilter;
        return {
          ...state,
          savedFilters: updatedFilters,
          meetingFilter: defaultFilter || searchQuery.meetingFilter,
        };
      }
      break;

    case constants.DELETE_MEETING_FILTER: {
      const stateCopy = cloneDeep(state.meetingFilter);
      // stateCopy[action.payload].selectedValues = [];
      // stateCopy[action.payload].type = "";
      delete stateCopy[action.payload];
      const updatedFilterState = { ...state, meetingFilter: stateCopy };
      searchQuery.meetingFilter = updatedFilterState.meetingFilter;
      return updatedFilterState;
    }
      break;

    case constants.DELETE_MEETING_CUSTOM_FILTER: {
      let updatedFilter = state.savedFilters.filter(item => item.filterId !== action.payload);
      searchQuery.meetingFilter = {};
      return { ...state, savedFilters: updatedFilter, meetingFilter: {} };
    }
      break;

    case constants.UPDATE_MEETING_DATA: {
      let updatedMeetings = state.data.map(x => {
        if (x.meetingId === action.payload.meetingId) {
          return { ...x, ...action.payload };
        } else {
          return x;
        }
      });
      return {
        ...state,
        data: updatedMeetings,
      };
    }
      break;

    case constants.SHOW_ALL_MEETING: {
      searchQuery.quickFilters = {};
      return { ...state, quickFilters: {}, data: state.data.filter(t => !t.isDeleted) };
    }
      break;

    case constants.UPDATE_MEETING_QUICK_FILTER: {
      const updatedFilters = { ...state.quickFilters, ...action.payload };
      searchQuery.quickFilters = updatedFilters;
      return { ...state, quickFilters: updatedFilters };
    }
      break;

    case constants.REMOVE_MEETING_QUICK_FILTER: {
      const { [action.payload]: value, ...rest } = state.quickFilters;
      const archiveFreeMeetings =
        value.type == "Archived" ? state.data.filter(t => !t.isDeleted) : state.data;
      searchQuery.quickFilters = rest ? rest : {};
      return { ...state, data: archiveFreeMeetings, quickFilters: rest ? rest : {} };
    }
      break;

    case constants.ADD_BULK_MEETING: {
      return { ...state, data: [...action.payload, ...state.data] };
    }
      break;

    case constants.UPDATE_MEETING_ORDER: {
      const sortedArray = sortBy(state.data, function (obj) {
        return findIndex(action.payload, { itemId: obj.id });
      });
      return { ...state, data: sortedArray };
    }
      break;

    default:
      return state;
  }
};

export default {
  meetings
};
