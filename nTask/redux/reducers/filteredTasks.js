import constants from "./../constants/types";
const filteredTasksData = (state = {}, action) => {

  switch (action.type) {
    case constants.POPULATEFILTEREDTASKS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.UPDATEFILTEREDTASKS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.DELETEFILTEREDTASKS:
      return {
        ...state,
        data: ""
      };
      break;

    default:
      return state;
  }
};

export default {
  filteredTasksData: filteredTasksData
};
