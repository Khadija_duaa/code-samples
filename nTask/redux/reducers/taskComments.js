import constants from "./../constants/types";
const taskCommentsData = (state = {}, action) => {

  switch (action.type) {
    case constants.POPULATETASKCOMMENTS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.UPDATETASKCOMMENTS:

      let modifiedTasks = state.data.map(function (element) {
        if (element.comment.id === action.payload.data.id) {

          return {
            ...action.payload.data.response
          };
        }
        return element;
      });
      return {
        ...state,
        data: modifiedTasks
      };
      break;

    case constants.ADDNEWTASKCOMMENTS:
      if (state.data && state.data.length) {
        return {
          ...state,
          data: [...state.data, action.payload.data]
        };
      }
      else {
        return {
          ...state,
          data: [action.payload.data]
        };
      }
      break;

    case constants.DELETETASKCOMMENTS:
      return {
        ...state,
        data: state.data.filter(function (element) {
          return element.comment.id !== action.payload.data;
        })
      };
      break;

    default:
      return state;
  }
};

export default {
  taskCommentsData: taskCommentsData
};
