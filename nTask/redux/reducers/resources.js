import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import initialStoreState from "../store/initialState";
import concat from "lodash/concat";
import resourcesInitialState from "../initialStates/resources";
import { formatMomentToDate, getCombinedHashMapKey } from "./../../Views/Resources/temp-utils";
import { groupBy, isArray, uniqBy } from "lodash";

// const initialState = initialStoreState.getInitialState().resources;
const initialState = resourcesInitialState;

const resources = (state = initialState, action) => {
  switch (action.type) {
    case constants.DUMMYDATA:
      return {
        ...state,
        ...action.payload,
      };

    // data

    case constants.SET_GRID_CALENDAR_DATES: {
      const calendarDates = action.payload;

      const flattenDays = concat([], ...calendarDates.map((k) => k.dates));

      const start = flattenDays.at(0);
      const end = flattenDays.at(-1);

      // create this to a hash Map
      const keywiseObject = Object.assign(
        {},
        ...flattenDays.map((k) => ({ [`${k.weekdayShort} ${k.dateOfMonth}`]: k }))
      );

      const _dataState = cloneDeep(state.data);

      _dataState.calendarDates = calendarDates;
      _dataState.calendarDatesFlattened = keywiseObject;

      const _uiState = cloneDeep(state.uiGrid);

      _uiState.gridColumnsCount = flattenDays.length || 0;

      _uiState.calendarTimelinePeriodStartDate = start;
      _uiState.calendarTimelinePeriodEndDate = end;

      return { ...state, data: _dataState, uiGrid: _uiState };
    }

    case constants.RESET_GRID_CALENDAR_DATES: {
      const _dataState = cloneDeep(state.data);

      _dataState.calendarDates = initialState.data.calendarDates;

      return { ...state, data: _dataState };
    }

    case constants.SET_GRID_CALENDAR_DATES_FLATTENED: {
      const _dataState = cloneDeep(state.data);

      _dataState.calendarDatesFlattened = action.payload;

      return { ...state, data: _dataState };
    }

    case constants.RESET_GRID_CALENDAR_DATES_FLATTENED: {
      const _dataState = cloneDeep(state.data);

      _dataState.calendarDatesFlattened = initialState.data.calendarDatesFlattened;

      return { ...state, data: _dataState };
    }

    case constants.SET_GRID_RESOURCES_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      // const _dataFlagsState = cloneDeep(state.dataFlags);

      _dataState.resourcesDataList = action.payload;
      // _dataFlagsState.resourcesDataListExists = true;

      return { ...state, data: _dataState /*, dataFlags: _dataFlagsState*/ };
    }

    case constants.APPEND_GRID_RESOURCES_DATA_LIST: {
      const _dataState = cloneDeep(state.data);

      // _dataState.resourcesDataList = [..._dataState.resourcesDataList, ...action.payload];
      _dataState.resourcesDataList = concat(_dataState.resourcesDataList, action.payload);

      return { ...state, data: _dataState };
    }

    case constants.RESET_GRID_RESOURCES_DATA_LIST: {
      const _dataState = cloneDeep(state.data);

      _dataState.resourcesDataList = initialState.data.resourcesDataList;

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_RESOURCE_TOTAL_CAPACITY: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, capacity = 0 } = action.payload;

      if (!resourceId) {
        return;
      }

      const resourcesList = _dataState.resourcesDataList;

      const resourceInx = resourcesList.findIndex((k) => k.id === resourceId);

      const resource = resourcesList.at(resourceInx);

      resource.totalCapacity = capacity;

      resourcesList.splice(resourceInx, 1, resource);

      _dataState.resourcesDataList = resourcesList;

      return { ...state, data: _dataState };
    }

    case constants.SET_GRID_PROJECTS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);

      _dataState.projectsDataList = action.payload;

      return { ...state, data: _dataState };
    }

    case constants.RESET_GRID_PROJECTS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);

      _dataState.projectsDataList = initialState.data.projectsDataList;

      return { ...state, data: _dataState };
    }

    case constants.SET_GRID_PROJECT_RESOURCES_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const { projectId, resourcesList /*, loading*/ } = action.payload;

      if (_dataState.projectResourcesDataList.has(projectId)) {
        _dataState.projectResourcesDataList.delete(projectId);
      }

      _dataState.projectResourcesDataList.set(projectId, resourcesList);
      // console.info(
      //   "yo yo3:",
      //   projectId,
      //   resourcesList,
      //   _dataState.projectResourcesDataList.get(projectId)
      // );

      return { ...state, data: _dataState };
    }

    case constants.APPEND_GRID_PROJECT_RESOURCES_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const { projectId, resourcesList } = action.payload;

      if (_dataState.projectResourcesDataList.has(projectId)) {
        const list = _dataState.projectResourcesDataList.get(projectId);

        list = concat(list, resourcesList);
        _dataState.projectResourcesDataList.set(projectId, list);
      } else {
        _dataState.projectResourcesDataList.set(projectId, resourcesList);
      }

      return { ...state, data: _dataState };
    }

    case constants.UNSET_GRID_PROJECT_RESOURCES_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const projectId = action.payload;

      _dataState.projectResourcesDataList.delete(projectId);

      return { ...state, data: _dataState };
    }

    case constants.RESET_GRID_PROJECT_RESOURCES_DATA_LIST: {
      const _dataState = cloneDeep(state.data);

      _dataState.projectResourcesDataList.clear();
      _dataState.projectResourcesDataList = new Map(initialState.data.projectResourcesDataList);

      return { ...state, data: _dataState };
    }

    case constants.SET_GRID_RESOURCE_TASKS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, tasksList } = action.payload;

      if (_dataState.resourceTasksDataList.has(resourceId)) {
        _dataState.resourceTasksDataList.delete(resourceId);
      }
      _dataState.resourceTasksDataList.set(resourceId, tasksList);

      _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);

      return { ...state, data: _dataState };
    }

    case constants.UNSET_GRID_RESOURCE_TASKS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const { resourceId } = action.payload;

      _dataState.projectResourcesDataList.delete(resourceId);

      return { ...state, data: _dataState };
    }

    case constants.RESET_GRID_RESOURCE_TASKS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);

      _dataState.resourceTasksDataList.clear();
      _dataState.resourceTasksDataList = initialState.data.resourceTasksDataList;

      _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);

      return { ...state, data: _dataState };
    }

    case constants.SET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const { projectId, resourceId, tasksList } = action.payload;

      const key = getCombinedHashMapKey(projectId, resourceId);

      if (_dataState.projectResourceTasksDataList.has(key)) {
        _dataState.projectResourceTasksDataList.delete(key);
      }
      _dataState.projectResourceTasksDataList.set(key, tasksList);

      return { ...state, data: _dataState };
    }

    case constants.UNSET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const { projectId, resourceId } = action.payload;

      const key = getCombinedHashMapKey(projectId, resourceId);

      _dataState.projectResourceTasksDataList.delete(key);

      return { ...state, data: _dataState };
    }

    case constants.RESET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);

      _dataState.projectResourceTasksDataList.clear();
      _dataState.projectResourceTasksDataList = new Map(
        initialState.data.projectResourceTasksDataList
      );

      return { ...state, data: _dataState };
    }

    case constants.SET_UNASSIGNED_TASKS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const list = action.payload;

      _dataState.unassignedTasksDataList = list;

      return { ...state, data: _dataState };
    }

    case constants.RESET_UNASSIGNED_TASKS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);

      _dataState.unassignedTasksDataList = initialState.data.unassignedTasksDataList;

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_RESOURCE_TASK: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, taskId, task } = action.payload;

      const _resourceTasks = _dataState.resourceTasksDataList.get(resourceId);

      const taskIndex = _resourceTasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        _resourceTasks.splice(taskIndex, 1, task);

        _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);

        _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);
      }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_RESOURCE_TASK_DAY_WORKLOAD: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, taskId, dayIndex, updatedHours } = action.payload;

      if (dayIndex <= -1) {
        // return;
        return { ...state };
      }

      const _resourceTasks = _dataState.resourceTasksDataList.has(resourceId)
        ? _dataState.resourceTasksDataList.get(resourceId)
        : [];

      const taskIndex = _resourceTasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _resourceTasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }

        const loads = task.dailyWorkload || [];

        const dayObject = loads.at(dayIndex);
        // const updatedDayObject = { moment: null, date: null, load: null };

        // loads.splice(dayIndex, 1, { ...loads[dayIndex], load: updatedHours });
        loads.splice(dayIndex, 1, { ...dayObject, load: updatedHours });

        task.dailyWorkload = loads;

        _resourceTasks.splice(taskIndex, 1, task);

        _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);
      }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_RESOURCE_TASK_DAY_WORKLOAD_BY_DATE: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, taskId, date, updatedHours } = action.payload;

      if (!date) {
        // return;
        return { ...state };
      }

      const _resourceTasks = _dataState.resourceTasksDataList.has(resourceId)
        ? _dataState.resourceTasksDataList.get(resourceId)
        : [];

      const taskIndex = _resourceTasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _resourceTasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }

        const loads = task.dailyWorkload || [];

        const dayIndex = loads.findIndex((k) => k.date === formatMomentToDate(date));
        const dayObject = loads.at(dayIndex);
        // const updatedDayObject = { moment: null, date: null, load: null };

        // loads.splice(dayIndex, 1, { ...loads[dayIndex], load: updatedHours });
        loads.splice(dayIndex, 1, { ...dayObject, load: updatedHours });

        task.dailyWorkload = loads;

        _resourceTasks.splice(taskIndex, 1, task);

        _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);

        _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);
      }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_RESOURCE_TASK_DAY_WORKLOAD_ID_BY_DATE: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, taskId, date, dayId } = action.payload;

      if (!date) {
        // return;
        return { ...state };
      }

      const _resourceTasks = _dataState.resourceTasksDataList.has(resourceId)
        ? _dataState.resourceTasksDataList.get(resourceId)
        : [];

      const taskIndex = _resourceTasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _resourceTasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }

        const loads = task.dailyWorkload || [];

        const dayIndex = loads.findIndex((k) => k.date === formatMomentToDate(date));
        const dayObject = loads.at(dayIndex);
        // const updatedDayObject = { moment: null, date: null, load: null };

        // loads.splice(dayIndex, 1, { ...loads[dayIndex], load: updatedHours });
        loads.splice(dayIndex, 1, { ...dayObject, id: dayId });

        task.dailyWorkload = loads;

        _resourceTasks.splice(taskIndex, 1, task);

        _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);

        _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);
      }

      return { ...state, data: _dataState };
    }

    case constants.DELETE_RESOURCE_TASK_DAY_WORKLOAD_BY_DATE: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, taskId, dayDate } = action.payload;

      if (!dayDate) {
        // return;
        return { ...state };
      }

      const _resourceTasks = _dataState.resourceTasksDataList.has(resourceId)
        ? _dataState.resourceTasksDataList.get(resourceId)
        : [];

      const taskIndex = _resourceTasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _resourceTasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }

        const loads = task.dailyWorkload || [];

        const dayIndex = loads.findIndex((k) => k.date === formatMomentToDate(dayDate));

        loads.splice(dayIndex, 1);

        task.dailyWorkload = loads;

        _resourceTasks.splice(taskIndex, 1, task);

        _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);

        _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);
      }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_RESOURCE_TASK_WORKLOADS: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, taskId, workloads } = action.payload;

      const _resourceTasks = _dataState.resourceTasksDataList.get(resourceId);

      const taskIndex = _resourceTasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _resourceTasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }
        // const loads = task.dailyWorkload;

        if (!!workloads && isArray(workloads)) {
          // here update/replace the dailyWorkload array
          task.dailyWorkload = workloads;
        }

        _resourceTasks.splice(taskIndex, 1, task);

        _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);

        _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);
      }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_RESOURCE_TASK_PLANNED_DATES: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, taskId, startDate, endDate } = action.payload;

      const _resourceTasks = _dataState.resourceTasksDataList.get(resourceId);

      const taskIndex = _resourceTasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _resourceTasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }
        task.plannedStartDate = startDate;
        task.plannedEndDate = endDate;
        // task.plannedStartDate = formatMomentToDate(startDate);
        // task.plannedEndDate = formatMomentToDate(endDate);

        _resourceTasks.splice(taskIndex, 1, task);

        _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);

        _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);
      }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_RESOURCE_TASK_PLANNED_START_DATE: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, taskId, startDate } = action.payload;

      const _resourceTasks = _dataState.resourceTasksDataList.get(resourceId);

      const taskIndex = _resourceTasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _resourceTasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }
        task.plannedStartDate = startDate;

        _resourceTasks.splice(taskIndex, 1, task);

        _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);

        _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);
      }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_RESOURCE_TASK_PLANNED_END_DATE: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, taskId, endDate } = action.payload;

      const _resourceTasks = _dataState.resourceTasksDataList.get(resourceId);

      const taskIndex = _resourceTasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _resourceTasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }
        task.plannedEndDate = endDate;

        _resourceTasks.splice(taskIndex, 1, task);

        _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);

        _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);
      }

      return { ...state, data: _dataState };
    }

    case constants.REASSIGN_RESOURCE_TASK: {
      const _dataState = cloneDeep(state.data);
      const { fromResourceId, toResourceId, fromIndex, toIndex } = action.payload;

      if (
        !fromResourceId ||
        !toResourceId ||
        isNaN(fromIndex) ||
        fromIndex < 0 ||
        isNaN(toIndex) ||
        toIndex < 0
      ) {
        return { ...state };
      }

      const _fromResourceTasks = _dataState.resourceTasksDataList.has(fromResourceId)
        ? _dataState.resourceTasksDataList.get(fromResourceId)
        : [];

      const removedOnes = _fromResourceTasks.splice(fromIndex, 1);

      _dataState.resourceTasksDataList.set(fromResourceId, _fromResourceTasks);

      const _toResourceTasks = _dataState.resourceTasksDataList.has(toResourceId)
        ? _dataState.resourceTasksDataList.get(toResourceId)
        : [];

      _toResourceTasks.splice(toIndex, 0, removedOnes.at(0));

      _dataState.resourceTasksDataList.set(toResourceId, _toResourceTasks);

      _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_PROJECT_RESOURCE_TASK_WORKLOADS: {
      const _dataState = cloneDeep(state.data);
      const { resourceId, taskId } = action.payload;

      // const _resourceTasks = _dataState.resourceTasksDataList.get(resourceId);

      // const taskIndex = _resourceTasks.findIndex((k) => k.id === taskId);
      // if (taskIndex > -1) {
      //   const task = _resourceTasks.at(taskIndex);

      //   const loads = task.dailyWorkload;

      //   // here update/replace the dailyWorkload array
      //   task.dailyWorkload = [...loads];

      //   _resourceTasks.splice(taskIndex, 1, task);

      //   _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);

      // _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);
      // }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_UNASSIGNED_TASK_DAY_WORKLOAD: {
      const _dataState = cloneDeep(state.data);
      const { taskId, dayIndex, updatedHours } = action.payload;

      if (dayIndex <= -1) {
        // return;
        return { ...state };
      }

      const _tasks = _dataState.unassignedTasksDataList;

      const taskIndex = _tasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _tasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }

        const loads = task.dailyWorkload || [];

        const dayObject = loads.at(dayIndex);
        // const updatedDayObject = { moment: null, date: null, load: null };

        // loads.splice(dayIndex, 1, { ...loads[dayIndex], load: updatedHours });
        loads.splice(dayIndex, 1, { ...dayObject, load: updatedHours });

        task.dailyWorkload = loads;

        _tasks.splice(taskIndex, 1, task);

        _dataState.unassignedTasksDataList = _tasks;
      }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_UNASSIGNED_TASK_DAY_WORKLOAD_ID_BY_DATE: {
      const _dataState = cloneDeep(state.data);
      const { taskId, date, dayId } = action.payload;

      if (!date) {
        // return;
        return { ...state };
      }

      const _tasks = _dataState.unassignedTasksDataList;

      const taskIndex = _tasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _tasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }

        const loads = task.dailyWorkload || [];

        const dayIndex = loads.findIndex((k) => k.date === formatMomentToDate(date));
        const dayObject = loads.at(dayIndex);
        // const updatedDayObject = { moment: null, date: null, load: null };

        // loads.splice(dayIndex, 1, { ...loads[dayIndex], load: updatedHours });
        loads.splice(dayIndex, 1, { ...dayObject, id: dayId });

        task.dailyWorkload = loads;

        _tasks.splice(taskIndex, 1, task);

        _dataState.unassignedTasksDataList = _tasks;
      }

      return { ...state, data: _dataState };
    }

    case constants.DELETE_UNASSIGNED_TASK_DAY_WORKLOAD_BY_DATE: {
      const _dataState = cloneDeep(state.data);
      const { taskId, dayDate } = action.payload;

      if (!dayDate) {
        // return;
        return { ...state };
      }

      const _tasks = _dataState.unassignedTasksDataList;

      const taskIndex = _tasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _tasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }

        const loads = task.dailyWorkload || [];

        const dayIndex = loads.findIndex((k) => k.date === formatMomentToDate(dayDate));

        loads.splice(dayIndex, 1);

        task.dailyWorkload = loads;

        _tasks.splice(taskIndex, 1, task);

        _dataState.resourceTasksDataList = _tasks;
      }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_UNASSIGNED_TASK_WORKLOADS: {
      const _dataState = cloneDeep(state.data);
      const { taskId, workloads } = action.payload;

      const _tasks = _dataState.unassignedTasksDataList;

      const taskIndex = _tasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _tasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }
        // const loads = task.dailyWorkload;

        if (!!workloads && isArray(workloads)) {
          // here update/replace the dailyWorkload array
          task.dailyWorkload = workloads;
        }

        _tasks.splice(taskIndex, 1, task);

        _dataState.unassignedTasksDataList = _tasks;
      }

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_UNASSIGNED_TASK_PLANNED_DATES: {
      const _dataState = cloneDeep(state.data);
      const { taskId, startDate, endDate } = action.payload;

      const _tasks = _dataState.unassignedTasksDataList;

      const taskIndex = _tasks.findIndex((k) => k.id === taskId);
      if (taskIndex > -1) {
        const task = _tasks.at(taskIndex);

        if (!task) {
          return { ...state };
        }
        task.plannedStartDate = startDate;
        task.plannedEndDate = endDate;

        _tasks.splice(taskIndex, 1, task);

        _dataState.unassignedTasksDataList = _tasks;
      }

      return { ...state, data: _dataState };
    }

    case constants.ASSIGN_UNASSIGNED_TASK_TO_RESOURCE: {
      const _dataState = cloneDeep(state.data);
      const { toResourceId, fromIndex, toIndex } = action.payload;

      if (!toResourceId || isNaN(fromIndex) || fromIndex < 0 || isNaN(toIndex) || toIndex < 0) {
        return { ...state };
      }

      const _unassignedTasks = _dataState.unassignedTasksDataList;

      const removedOnes = _unassignedTasks.splice(fromIndex, 1);

      _dataState.unassignedTasksDataList = _unassignedTasks;

      const _resourceTasks = _dataState.resourceTasksDataList.has(toResourceId)
        ? _dataState.resourceTasksDataList.get(toResourceId)
        : [];

      _resourceTasks.splice(toIndex, 0, removedOnes.at(0));

      _dataState.resourceTasksDataList.set(toResourceId, _resourceTasks);

      _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);

      return { ...state, data: _dataState };
    }

    case constants.SET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const list = action.payload;

      const grouped = groupBy(list, (k) => k.workspaceId);

      // console.info("grouped:", grouped);

      const hashmap = new Map(
        // NOTE: removing 'workspaceId' field
        // Object.entries(grouped).map(([k, v]) => [k, v.map((j) => ({ id: j.id, name: j.name }))])
        Object.entries(grouped).map(([k, v]) => [k, v.map(({ id, name }) => ({ id, name }))])

        // NOTE: not removing 'workspaceId' field
        // Object.entries(grouped).map(([k, v]) => [k, v])
      );

      _dataState.projectsOfWorkspacesFilterDataList = hashmap;

      return { ...state, data: _dataState };
    }

    case constants.SET_RESOURCES_OF_WORKSPACES_FILTER_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const list = action.payload;

      const grouped = groupBy(list, (k) => k.workspaceId);

      // console.info("grouped:", grouped);

      const hashmap = new Map(
        // NOTE: removing 'workspaceId' field
        // Object.entries(grouped).map(([k, v]) => [k, v.map((j) => ({ id: j.id, name: j.name }))])
        // Object.entries(grouped).map(([k, v]) => [k, v.map(({ id, name }) => ({ id, name }))])

        // NOTE: not removing 'workspaceId' field
        Object.entries(grouped).map(([k, v]) => [k, v])
      );

      _dataState.resourcesOfWorkspacesFilterDataList = hashmap;

      return { ...state, data: _dataState };
    }

    case constants.SET_UNSCHEDULED_TASKS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);
      const list = action.payload;

      const simplifiedList = list.map((k) => ({
        taskTitle: k.taskTitle,
        startDate: k.startDate,
        endDate: k.endDate,
        projectId: k.projectId,
        projectName: k.projectName,
        workspaceId: k.workspaceId,
        taskId: k.taskId,
        teamId: k.teamId,
        estimates: k.estimates,
      }));

      const groupByProject = groupBy(simplifiedList, (k) => k.projectId);

      const projectList = uniqBy(
        simplifiedList.map((k) => ({ id: k.projectId, name: k.projectName })),
        (k) => k.id
      );

      const groupsWithTasks = projectList.map((k) => ({
        ...k,
        tasks: (groupByProject[k.id] || []).map((t) => ({
          id: t.taskId,
          title: t.taskTitle,
          // startDate: t.startDate,
          // endDate: t.endDate,
          // workspaceId: t.workspaceId,
          // teamId: t.teamId,
          // estimates: parseTaskEstimates(t.estimates, ...)
        })),
      }));

      _dataState.unscheduledTasksDataList = groupsWithTasks;

      return { ...state, data: _dataState };
    }

    case constants.RESET_UNSCHEDULED_TASKS_DATA_LIST: {
      const _dataState = cloneDeep(state.data);

      _dataState.unscheduledTasksDataList = initialState.data.unscheduledTasksDataList;

      return { ...state, data: _dataState };
    }

    // data-records-count

    case constants.SET_GRID_RESOURCES_DATA_RECORDS_COUNT: {
      const _countState = cloneDeep(state.dataRecordsCount);

      _countState.resourcesDataRecordsCount =
        action.payload || initialState.dataRecordsCount.resourcesDataRecordsCount;

      return { ...state, dataRecordsCount: _countState };
    }

    case constants.RESET_GRID_RESOURCES_DATA_RECORDS_COUNT: {
      const _countState = cloneDeep(state.dataRecordsCount);

      _countState.resourcesDataRecordsCount =
        initialState.dataRecordsCount.resourcesDataRecordsCount;

      return { ...state, dataRecordsCount: _countState };
    }

    case constants.SET_GRID_PROJECT_RESOURCES_DATA_RECORDS_COUNT: {
      const _countState = cloneDeep(state.dataRecordsCount);

      _countState.projectResourcesDataRecordsCount =
        action.payload || initialState.dataRecordsCount.projectResourcesDataRecordsCount;

      return { ...state, dataRecordsCount: _countState };
    }

    case constants.RESET_GRID_PROJECT_RESOURCES_DATA_RECORDS_COUNT: {
      const _countState = cloneDeep(state.dataRecordsCount);

      _countState.projectResourcesDataRecordsCount =
        initialState.dataRecordsCount.projectResourcesDataRecordsCount;

      return { ...state, dataRecordsCount: _countState };
    }

    // data-flags

    case constants.SET_GRID_RESOURCES_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.resourcesDataListExists = action.payload;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.RESET_GRID_RESOURCES_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.resourcesDataListExists = initialState.dataFlags.resourcesDataListExists;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.SET_GRID_PROJECTS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.projectsDataListExists = action.payload;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.RESET_GRID_PROJECTS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.projectsDataListExists = initialState.dataFlags.projectsDataListExists;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.SET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);
      const { projectId, exists } = action.payload;

      if (_flagsState.projectResourcesDataListExists.has(projectId)) {
        _flagsState.projectResourcesDataListExists.delete(projectId);
      }

      _flagsState.projectResourcesDataListExists.set(projectId, exists);

      return { ...state, dataFlags: _flagsState };
    }

    case constants.UNSET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);
      const projectId = action.payload;

      _flagsState.projectResourcesDataListExists.delete(projectId);

      return { ...state, dataFlags: _flagsState };
    }

    case constants.RESET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.projectResourcesDataListExists.clear();
      _flagsState.projectResourcesDataListExists =
        initialState.dataFlags.projectResourcesDataListExists;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.SET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);
      const { resourceId, exists } = action.payload;

      if (_flagsState.resourceTasksDataListExists.has(resourceId)) {
        _flagsState.resourceTasksDataListExists.delete(resourceId);
      }

      _flagsState.resourceTasksDataListExists.set(resourceId, exists);

      return { ...state, dataFlags: _flagsState };
    }

    case constants.UNSET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);
      const resourceId = action.payload;

      _flagsState.resourceTasksDataListExists.delete(resourceId);

      return { ...state, dataFlags: _flagsState };
    }

    case constants.RESET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.resourceTasksDataListExists.clear();
      _flagsState.resourceTasksDataListExists = initialState.dataFlags.resourceTasksDataListExists;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.SET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);
      const { projectId, resourceId, exists } = action.payload;

      const key = getCombinedHashMapKey(projectId, resourceId);

      if (_flagsState.projectResourceTasksDataListExists.has(key)) {
        _flagsState.projectResourceTasksDataListExists.delete(key);
      }

      _flagsState.projectResourceTasksDataListExists.set(key, exists);

      return { ...state, dataFlags: _flagsState };
    }

    case constants.UNSET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);
      const { projectId, resourceId } = action.payload;

      const key = getCombinedHashMapKey(projectId, resourceId);
      _flagsState.projectResourceTasksDataListExists.delete(key);

      return { ...state, dataFlags: _flagsState };
    }

    case constants.RESET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.projectResourceTasksDataListExists.clear();
      _flagsState.projectResourceTasksDataListExists =
        initialState.dataFlags.projectResourceTasksDataListExists;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.SET_UNASSIGNED_TASKS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);
      const exists = action.payload;

      _flagsState.unassignedTasksDataListExists = exists;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.RESET_UNASSIGNED_TASKS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.unassignedTasksDataListExists =
        initialState.dataFlags.unassignedTasksDataListExists;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.SET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.projectsOfWorkspacesFilterDataList = action.payload;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.RESET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.projectsOfWorkspacesFilterDataList =
        initialState.dataFlags.projectsOfWorkspacesFilterDataList;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.SET_UNSCHEDULED_TASKS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.unscheduledTasksDataListExists = action.payload;

      return { ...state, dataFlags: _flagsState };
    }

    case constants.RESET_UNSCHEDULED_TASKS_DATA_LIST_EXISTS: {
      const _flagsState = cloneDeep(state.dataFlags);

      _flagsState.unscheduledTasksDataListExists =
        initialState.dataFlags.unscheduledTasksDataListExists;

      return { ...state, dataFlags: _flagsState };
    }

    // ui-grid

    case constants.SET_UI_SIDEBAR_COLLAPSED: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.sidebarCollapsed = action.payload;

      return { ...state, uiGrid: _uiState };
    }

    case constants.RESET_UI_SIDEBAR_COLLAPSED: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.sidebarCollapsed = initialState.uiGrid.sideBarCollapsed;

      return { ...state, uiGrid: _uiState };
    }

    case constants.SET_DAY_PROGRESS_HOURGLASS_WIDTH: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.dayProgressHourglassWidth = action.payload;

      return { ...state, uiGrid: _uiState };
    }

    case constants.RESET_DAY_PROGRESS_HOURGLASS_WIDTH: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.dayProgressHourglassWidth = initialState.uiGrid.dayProgressHourglassWidth;

      return { ...state, uiGrid: _uiState };
    }

    case constants.SET_GRID_COLUMNS_COUNT: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.gridColumnsCount = action.payload;

      return { ...state, uiGrid: _uiState };
    }

    case constants.RESET_GRID_COLUMNS_COUNT: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.gridColumnsCount = initialState.uiGrid.gridColumnsCount;

      return { ...state, uiGrid: _uiState };
    }

    case constants.SET_WORKLOAD_VIEW_TYPE: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.workloadViewType = action.payload;

      return { ...state, uiGrid: _uiState };
    }

    case constants.RESET_WORKLOAD_VIEW_TYPE: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.workloadViewType = initialState.uiGrid.workloadViewType;

      return { ...state, uiGrid: _uiState };
    }

    case constants.SET_CALENDAR_TIMELINE_PERIOD_START_DATE: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.calendarTimelinePeriodStartDate = action.payload;

      return { ...state, uiGrid: _uiState };
    }

    case constants.RESET_CALENDAR_TIMELINE_PERIOD_START_DATE: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.calendarTimelinePeriodStartDate =
        initialState.uiGrid.calendarTimelinePeriodStartDate;

      return { ...state, uiGrid: _uiState };
    }

    case constants.SET_CALENDAR_TIMELINE_PERIOD_END_DATE: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.calendarTimelinePeriodEndDate = action.payload;

      return { ...state, uiGrid: _uiState };
    }

    case constants.RESET_CALENDAR_TIMELINE_PERIOD_END_DATE: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.calendarTimelinePeriodEndDate = initialState.uiGrid.calendarTimelinePeriodEndDate;

      return { ...state, uiGrid: _uiState };
    }

    case constants.INCREASE_OPEN_COLLAPSIBLES_COUNT: {
      const _uiState = cloneDeep(state.uiGrid);

      _uiState.openCollapsiblesCount += 1;

      return { ...state, uiGrid: _uiState };
    }

    case constants.DECREASE_OPEN_COLLAPSIBLES_COUNT: {
      const _uiState = cloneDeep(state.uiGrid);
      let count = _uiState.openCollapsiblesCount;

      if (count <= 0) {
        return { ...state };
      }

      _uiState.openCollapsiblesCount = count - 1;

      return { ...state, uiGrid: _uiState };
    }

    // settings

    case constants.SET_SETTINGS_OBJECT: {
      let _settingsState = cloneDeep(state.settings);

      _settingsState = action.payload;

      return { ...state, settings: _settingsState };
    }

    case constants.RESET_SETTINGS_OBJECT: {
      const _settingsState = cloneDeep(state.settings);

      _settingsState = initialState.settings;

      return { ...state, settings: _settingsState };
    }

    case constants.SET_SETTINGS_SHOW_TASK_NAME_WITH_TIMELINE: {
      const _settingsState = cloneDeep(state.settings);

      _settingsState.showTaskNameWithTimeline = action.payload;

      return { ...state, settings: _settingsState };
    }

    case constants.RESET_SETTINGS_SHOW_TASK_NAME_WITH_TIMELINE: {
      const _settingsState = cloneDeep(state.settings);

      _settingsState.showTaskNameWithTimeline = initialState.settings.showTaskNameWithTimeline;

      return { ...state, settings: _settingsState };
    }

    case constants.SET_SETTINGS_TASK_COLOR: {
      const _settingsState = cloneDeep(state.settings);

      _settingsState.taskTimelineColor = action.payload;

      return { ...state, settings: _settingsState };
    }

    case constants.RESET_SETTINGS_TASK_COLOR: {
      const _settingsState = cloneDeep(state.settings);

      _settingsState.taskTimelineColor = initialState.settings.taskTimelineColor;

      return { ...state, settings: _settingsState };
    }

    // filters

    case constants.SET_FILTER_CALENDAR_TIMELINE_PERIOD: {
      const filtersState = cloneDeep(state.filters);

      filtersState.calendarTimelinePeriod = action.payload;

      return { ...state, filters: filtersState };
    }

    case constants.RESET_FILTER_CALENDAR_TIMELINE_PERIOD: {
      const filtersState = cloneDeep(state.filters);

      filtersState.calendarTimelinePeriod = initialState.filters.calendarTimelinePeriod;

      return { ...state, filters: filtersState };
    }

    case constants.SET_FILTER_PROGRESS_HOURGLASS_UNIT: {
      const filtersState = cloneDeep(state.filters);

      filtersState.progressHourglassUnit = action.payload;

      return { ...state, filters: filtersState };
    }

    case constants.RESET_FILTER_PROGRESS_HOURGLASS_UNIT: {
      const filtersState = cloneDeep(state.filters);

      filtersState.progressHourglassUnit = initialState.filters.progressHourglassUnit;

      return { ...state, filters: filtersState };
    }

    case constants.SET_FILTER_WORKSPACES: {
      const filtersState = cloneDeep(state.filters);

      filtersState.workspaces = action.payload;

      return { ...state, filters: filtersState };
    }

    case constants.RESET_FILTER_WORKSPACES: {
      const filtersState = cloneDeep(state.filters);

      filtersState.workspaces = initialState.filters.workspaces;

      return { ...state, filters: filtersState };
    }

    case constants.SET_FILTER_RESOURCES: {
      const filtersState = cloneDeep(state.filters);

      filtersState.resources = action.payload;

      return { ...state, filters: filtersState };
    }

    case constants.RESET_FILTER_RESOURCES: {
      const filtersState = cloneDeep(state.filters);

      filtersState.resources = initialState.filters.resources;

      return { ...state, filters: filtersState };
    }

    case constants.SET_FILTER_PROJECTS: {
      const filtersState = cloneDeep(state.filters);

      filtersState.projects = action.payload;

      return { ...state, filters: filtersState };
    }

    case constants.RESET_FILTER_PROJECTS: {
      const filtersState = cloneDeep(state.filters);

      filtersState.projects = initialState.filters.projects;

      return { ...state, filters: filtersState };
    }

    // extra

    case constants.UPDATE_PROFILE_URLS_FROM_USER_INFO_REDUX_OBJECT_TO_RESOURCES_GRID_LISTING: {
      const _dataState = cloneDeep(state.data);
      const userIdImageurlDataMapped = action.payload;

      const list = _dataState.resourcesDataList;

      const updated = list.map((k) => ({
        ...k,
        imageUrl: userIdImageurlDataMapped.get(k.id) || null,
      }));

      _dataState.resourcesDataList = updated;

      return { ...state, data: _dataState };
    }

    case constants.UPDATE_PROFILE_URLS_FROM_USER_INFO_REDUX_OBJECT_TO_RESOURCES_FILTER_LISTING: {
      const _dataState = cloneDeep(state.data);
      const userIdImageurlDataMapped = action.payload;

      const newMap = new Map();
      const map = _dataState.resourcesOfWorkspacesFilterDataList;

      map.forEach((workspaceResources, workspaceId) => {
        const updated = workspaceResources.map((k) => ({
          ...k,
          imageUrl: userIdImageurlDataMapped.get(k.id) || null,
        }));

        newMap.set(workspaceId, updated);
      });

      _dataState.resourcesOfWorkspacesFilterDataList.clear();

      _dataState.resourcesOfWorkspacesFilterDataList = newMap;

      return { ...state, data: _dataState };
    }

    // temp

    case "TEMP_REORDER_UNASSIGNED_TASK": {
      const _dataState = cloneDeep(state.data);
      const { fromIndex, toIndex } = action.payload;

      if (isNaN(fromIndex) || fromIndex < 0 || isNaN(toIndex) || toIndex < 0) {
        return { ...state };
      }

      const _tasks = _dataState.unassignedTasksDataList;

      const removedOnes = _tasks.splice(fromIndex, 1);

      _tasks.splice(toIndex, 0, removedOnes.at(0));

      return { ...state, data: _dataState };
    }

    case "TEMP_REORDER_RESOURCE_TASK": {
      const _dataState = cloneDeep(state.data);
      const { resourceId, fromIndex, toIndex } = action.payload;

      if (!resourceId || isNaN(fromIndex) || fromIndex < 0 || isNaN(toIndex) || toIndex < 0) {
        return { ...state };
      }

      const _resourceTasks = _dataState.resourceTasksDataList.has(resourceId)
        ? _dataState.resourceTasksDataList.get(resourceId)
        : [];

      const removedOnes = _resourceTasks.splice(fromIndex, 1);

      _resourceTasks.splice(toIndex, 0, removedOnes.at(0));

      _dataState.resourceTasksDataList.set(resourceId, _resourceTasks);

      _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);

      return { ...state, data: _dataState };
    }

    case "TEMP_UNASSIGN_RESOURCE_TASK": {
      const _dataState = cloneDeep(state.data);
      const { fromResourceId, fromIndex, toIndex } = action.payload;

      if (!fromResourceId || isNaN(fromIndex) || fromIndex < 0 || isNaN(toIndex) || toIndex < 0) {
        return { ...state };
      }

      const _resourceTasks = _dataState.resourceTasksDataList.has(fromResourceId)
        ? _dataState.resourceTasksDataList.get(fromResourceId)
        : [];

      const removedOnes = _resourceTasks.splice(fromIndex, 1);

      _dataState.resourceTasksDataList.set(fromResourceId, _resourceTasks);

      _dataState.resourceTasksDataList = new Map(_dataState.resourceTasksDataList);

      const _unassignedTasks = _dataState.unassignedTasksDataList;

      _unassignedTasks.splice(toIndex, 0, removedOnes.at(0));

      _dataState.unassignedTasksDataList = _unassignedTasks;

      return { ...state, data: _dataState };
    }

    default:
      return state;
  }
};

export default { resources };
