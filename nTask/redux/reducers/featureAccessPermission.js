import cloneDeep from "lodash/cloneDeep";
import constants from "./../constants/types";


const featureAccessPermissions = (state = {}, action) => {
    switch (action.type) {
        case constants.SETFEATUREACCESSPERMISSIONS: {
            // const data = action.payload.data;
            // console.log(action.payload.data); 
            // let test = Object.values(data).reduce((res, cv) => {
            //     console.log(cv);
            //     return { ...res, 'test': cv }
            // }, {}); 


            // let obj = {}
            // for (let key in data) {
            //     obj[key] = data[key].reduce((res, cv) => (
            //         { ...res, [cv.fieldKey]: cv }
            //     ), {});
            // }
            // console.log(obj);
            // obj.project = data.project.reduce((res, cv) => (
            //     { ...res, [cv.fieldKey]: cv }
            // ), {})
            // obj.task = data.task.reduce((res, cv) => (
            //     { ...res, [cv.fieldKey]: cv }
            // ), {})
            // obj.issue = data.issue.reduce((res, cv) => (
            //     { ...res, [cv.fieldKey]: cv }
            // ), {})
            // obj.risk = data.risk.reduce((res, cv) => (
            //     { ...res, [cv.fieldKey]: cv }
            // ), {})
            return { ...action.payload.data };
        }
            break;
        case constants.UPDATEFEATUREACCESS: {
            const data = action.payload.data;
            let permissionObj = cloneDeep(state);
            permissionObj[action.payload.feature][action.payload.data.fieldKey] = data;
            return { ...permissionObj };
        }
            break;

        default:
            return state;
    }
};
export default {
    featureAccessPermissions
};
