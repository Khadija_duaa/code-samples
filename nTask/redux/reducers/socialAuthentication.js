import constants from "./../constants/types";
const socialAuthenticationReducer = (state = {}, action) => {
    switch (action.type) {
        case constants.SOCIALAUTHENTICATION:
            return {
                ...state,
                data: action.payload.data,
            };
            break;

        case constants.DELETESOCIALAUTH:
            return {
                ...state,
                data: "",
            };
            break;

        default:
            return state;
    }
};

export default {
    socialAuthenticationResponse: socialAuthenticationReducer
};

