import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
const initialState = {
  data: []
}
const taskNotifications = (state = initialState, action) => {
  switch (action.type) {

    case constants.POPULATETASKCOMMENTSNOTIFICATIONS: {
      return {
        ...state,
        data: action.payload.data,
      };
    }
      break;

    case constants.CLEARTASKCOMMENTSNOTIFICATIONS: {
      return {
        ...state,
        data: [],
      };
    }
      break;

    case constants.UPDATETASKNOTIFICATIONS: {
      let taskNoti = cloneDeep(state.data);
      let updatedTaskNoti = [action.payload.data, ...taskNoti];
      return {
        ...state,
        data: updatedTaskNoti,
      };
    }
      break;

    default:
      return state;
  }
};

export default {
  taskNotifications,
};
