import constants from "./../constants/types";
const filteredIssuesData = (state = {}, action) => {

  switch (action.type) {
    case constants.POPULATEFILTEREDISSUES:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.UPDATEFILTEREDISSUES:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.DELETEFILTEREDISSUES:
      return {
        ...state,
        data: ""
      };
      break;

    default:
      return state;
  }
};

export default {
  filteredIssuesData: filteredIssuesData
};
