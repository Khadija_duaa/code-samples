import constants from "./../constants/types";
const riskActivities = (state = {}, action) => {
    switch (action.type) {
        case constants.POPULATERISKCOMMENTSACTIVITIES:
            return {
                ...state,
                data: action.payload.data
            };
            break;

        default:
            return state;
    }
};

export default {
    riskActivities
};

