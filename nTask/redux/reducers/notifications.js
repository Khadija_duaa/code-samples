import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import isArray from "lodash/isArray";

const initialState = {
  data: []
}
const notifications = (state = initialState, action) => {
  let newData;
  switch (action.type) {
    case constants.POPULATENOTIFICATIONS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    // {
    //   let notifications = cloneDeep(state.data);
    //   let updatedNotifications = [];
    //   if (isArray(action.payload.data)) {
    //     updatedNotifications = [...notifications, ...action.payload.data];
    //   } else {
    //     updatedNotifications = [action.payload.data , ...notifications];
    //   }
    //   return {
    //     ...state,
    //     data: updatedNotifications,
    //   };
    // }
    case constants.NEWNOTIFICATION:
      newData = cloneDeep(...state.data);
      if (state.data && state.data.length) {
        newData = [action.payload.data, ...state.data]
        return {
          ...state,
          data: newData
        };
      }
      else {
        newData = [action.payload.data]
        return {
          ...state,
          data: newData
        };
      };
      break;

    case constants.RESETNOTIFICATIONS:
      return {
        ...state,
        data: [],
      };
      break;

    case constants.MARKREADALL:
      let notifications = cloneDeep(state.data)
      notifications = notifications.filter((x) => {
        return x.users.map((y) => {
          if (y.userId === action.payload.currentUser && y.isViewed === false) {
            y.isViewed = true;
          }
          return y;
        });
      });
      return {
        ...state,
        data: notifications, //[notifications, ...state.data]
      };
      break;

    default:
      return state;
  }
};

export default {
  notifications,
};
