import constants from "./../constants/types";
const sidebar = (state = {}, action) => {

  switch (action.type) {

    case constants.SETSIDEBARTHEME:
      return {
        ...state,
        sidebarMode: action.payload
      };
      break;

    default:
      return state;
  }
};

export default { sidebar };