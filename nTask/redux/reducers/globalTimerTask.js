
import consts from "../constants/types";

const globalTimerTask = (state = {}, action) => {
    switch (action.type) {
        case consts.SETGLOBALTASKTIME: {
            const { task, createdDate, currentDate } = action.payload;
            return { ...state, 'task': task, 'createdDate': createdDate, 'currentDate': currentDate };
        }
            break;
        case consts.UPDATEGLOBALTIMETASK: {
            const { task } = action.payload;
            // state === null, if no timer task exists
            if (state && state.task.taskId === task.taskId) {
                // If global Timer Task is archived or deleted than remove timer
                if (task.isDeleted)
                    return null;
                return { ...state, ...{ 'task': { ...task } } };
            }
            else return state;
        }
            break;

        case consts.REMOVEGLOBALTASKTIME: {
            return null;
        }
            break;

        default:
            return state;
    }
}

export default {
    globalTimerTask
}