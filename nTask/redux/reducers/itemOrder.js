import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import differenceBy from "lodash/difference";

const itemOrder = (state = {}, action) => {
  switch (action.type) {
    case constants.POPULATEITEMORDER:
      return {
        ...state,
        ...action.payload,
      };
      break;

    case constants.RESETITEMORDER:
      return {
        ...state,
        itemOrder: {},
      };
      break;

    case constants.UPGRADEGROUPCOLUMN:
      let stateCopy = cloneDeep(state);
      stateCopy.data.groupByColumn = action.payload.data;
      return stateCopy;
      break;

    case constants.UPDATEITEMORDERSIGNALR: {
      let stateCopy = cloneDeep(state);
      let { fieldIds, storeState } = action.payload;
      let findedCustomField = fieldIds.reduce((result, cv) => {
        let isExists = storeState.customFields.data.find(cf => cf.fieldId === cv);
        if (isExists) result.push(isExists.fieldName)
        return result
      }, []);
      let diff = differenceBy(stateCopy.data.riskColumnOrder, findedCustomField); /** getting diff in risk column order / removing selected column if other user delete custom field and get updated from signalR */
      stateCopy.data.riskColumnOrder = diff;
      return stateCopy;
    }
      break;

    default:
      return state;
  }
};

export default {
  itemOrder,
};
