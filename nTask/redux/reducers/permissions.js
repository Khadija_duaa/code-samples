import constants from "./../constants/types";
const workspacePermissions = (state = {}, action) => {
  switch (action.type) {
    case constants.WORKSPACEPERMISSIONS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    default:
      return state;
  }
};

export default {
  workspacePermissions
};
