import consts from "./../constants/types";
const constants = (state = {}, action) => {
  switch (action.type) {
    case consts.POPULATECONSTANTS:
      return {
        ...state,
        ...action.payload,
      };
      break;

    case consts.RESETCONSTANTS:
      return {
        ...state,
        constants: ""
      };
      break;

    default:
      return state;
  }
};

export default {
  constants
};

