import constants from "./../constants/types";

const workspace = (state = {}, action) => {
    switch (action.type) {
        case constants.POPULATEWORKSPACEINFO:
            return {
                ...state,
                data: action.payload.data,
            };
            break;

        case constants.UPDATE_CUSTOM_FIELD_DATA:
            return {
                ...state
            }
            break;

        case constants.RESETWORKSPACE:
            return {
                ...state,
                workspace: ""
            };
            break;

        default:
            return state;
    }
};

export default {
    workspace
};

