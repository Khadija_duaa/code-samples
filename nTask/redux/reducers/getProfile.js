import consts from "./../constants/types";
const getProfile = (state = {}, action) => {
  switch (action.type) {
    case consts.POPULATEGETPROFILE:
      return {
        ...state,
        ...action.payload,
      };
      break;

    case consts.RESETGETPROFILE:
      return {
        ...state,
        getProfile: ""
      };
      break;

    default:
      return state;
  }
};

export default {
  getProfile
};

