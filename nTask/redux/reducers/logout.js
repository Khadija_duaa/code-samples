import constants from "./../constants/types";
import initial from "./../store/initialState";
const logout = (state = initial.getInitialState(1), action) => {
    switch (action.type) {
        case constants.LOGOUT:
            return {
                ...state
            };
            break;

        default:
            return state;
    }
};

export default {
    logout
};

