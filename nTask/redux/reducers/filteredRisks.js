import constants from "./../constants/types";
const filteredRisksData = (state = {}, action) => {

  switch (action.type) {
    case constants.POPULATEFILTEREDRISKS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.UPDATEFILTEREDRISKS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.DELETEFILTEREDRISKS:
      return {
        ...state,
        data: ""
      };
      break;

    default:
      return state;
  }
};

export default {
  filteredRisksData: filteredRisksData
};
