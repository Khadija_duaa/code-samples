import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";
import isUndefined from "lodash/isUndefined";
import constants from "./../constants/types";

const boards = (state = {}, action) => {
  let updatedState = {};
  switch (action.type) {
    case constants.POPULATEKANBANLISTING:
      {
        return {
          ...state,
          data: {
            ...state.data,
            kanban: action.payload.data.kanbanBoard,
            unsortedTask: action.payload.data.unsortedTask,
          },
        };
      }
      break;
    case constants.POPULATEALLBOARDS:
      {
        return {
          ...state,
          data: { ...state.data, boards: action.payload.data },
        };
      }
      break;

    case constants.UPDATEBOARD:
      {
        updatedState = cloneDeep(state.data);
        if (updatedState.boards) {
          updatedState.boards = updatedState.boards.map((ele) => {
            if (ele.boardId == action.payload.data.boardId) {
              return action.payload.data;
            } else {
              return ele;
            }
          });
          return {
            ...state,
            data: updatedState,
          };
        } else return { ...state };
      }
      break;

    case constants.CLEARKANBANDATA:
      {
        return {
          ...state,
          data: { ...state.data, kanban: action.payload.data, unsortedTask: [] },
        };
      }
      break;

    case constants.UPDATESUBTASKFILTER:
      {
        return {
          ...state,
          data: {
            ...state.data,
            kanban: { ...state.data.kanban, subtaskFilter: action.payload.data.subtaskFilter },
          },
        };
      }
      break;

    case constants.ADDTASKINLIST:
      {
        if (
          !state.data.kanban.isTemplate &&
          state.data.kanban.projectId == action.payload.data.projectId
        ) {
          return {
            ...state,
            data: {
              ...state.data,
              kanban: {
                ...state.data.kanban,
                tasksList: [...state.data.kanban.tasksList, action.payload.data],
              },
            },
          };
        } else return { ...state };
      }
      break;
    case constants.ADDALLTASKINLIST:
      {
        if (!isEmpty(state.data.kanban)) {
          return {
            ...state,
            data: {
              ...state.data,
              kanban: {
                ...state.data.kanban,
                tasksList: [action.payload.data, ...state.data.kanban.tasksList],
              },
            },
          };
        } else return { ...state };
      }
      break;

    case constants.UPDATETASKKANBANSTORE:
      {
        updatedState = cloneDeep(state.data);
        if (!isEmpty(updatedState.kanban) && updatedState.kanban.tasksList) {
          updatedState.kanban.tasksList = updatedState.kanban.tasksList.map((ele) => {
            if (ele.taskId == action.payload.data.taskId) {
              return action.payload.data;
            } else {
              return ele;
            }
          });
          return {
            ...state,
            data: updatedState,
          };
        } else return { ...state };
      }
      break;

    case constants.UPDATEBOARDSETTINGS:
      {
        updatedState = cloneDeep(state.data);
        if (
          !isEmpty(updatedState.kanban) &&
          updatedState.kanban.projectId == action.payload.data.projectId
        ) {
          updatedState.kanban[action.payload.data.itemKey] =
            action.payload.data[action.payload.data.itemKey];
          return {
            ...state,
            data: updatedState,
          };
        } else return { ...state };
      }
      break;

    // case constants.UPDATEKANBANLISTORDER: {
    //   let Updatedstate = cloneDeep(state.data);
    //   Updatedstate.kanbanboard = action.payload.data;

    //   return {
    //     ...state,
    //     data: Updatedstate,
    //   };
    // }

    // case constants.UPDATEKANBANCOLUMN: {
    //   let OldState = cloneDeep(state.data);
    //   let payload = action.payload.data.kanbanboard;
    //   OldState.kanbanboard = OldState.kanbanboard.map(k => {
    //     if (k.id == payload.id) return payload;
    //     else return k;
    //   });
    //   return {
    //     ...state,
    //     data: OldState,
    //   };
    // }
    case constants.UPDATEKANBANSTATUSRULE:
      {
        let OldState = cloneDeep(state.data);
        if (!isEmpty(OldState.kanban)) {
          let payload = action.payload.data;
          // OldState.kanban = OldState.kanban.map(k => {
          //   if (k.id == payload.id) return payload;
          //   else return k;
          // });
          OldState.kanban = action.payload.data;
          return {
            ...state,
            data: OldState,
          };
        } else return { ...state };
      }
      break;

    case constants.UPDATEACTIVITYLOG:
      {
        return {
          ...state,
          data: { ...state.data, activitylog: action.payload.data.activitylog },
        };
      }
      break;

    case constants.CLEARACTIVITYLOG:
      {
        return {
          ...state,
          data: { ...state.data, activitylog: [] },
        };
      }
      break;

    case constants.ADDACTIVITYLOG:
      {
        if (!isUndefined(state.data.activitylog.results)) {
          updatedState = cloneDeep(state.data);
          updatedState.activitylog.results = updatedState.activitylog.results.map((e, index) => {
            if (
              index == 0 &&
              e.activities[0].activity.entityId == action.payload.data.activity.entityId
            ) {
              e.activities.unshift(action.payload.data);
              return e;
            } else {
              return e;
            }
          });
          return {
            ...state,
            data: updatedState,
          };
        } else return { ...state };
      }
      break;

    // case constants.UPDATEFEATUREIMAGE: {
    //   let OldStateData = cloneDeep(state.data);
    //   let payload = action.payload;

    //   let columnToBeUpdated = OldStateData.kanbanboard.find(k => k.id == payload.column.id) || null;

    //   if (columnToBeUpdated) {
    //     columnToBeUpdated.tasksList = columnToBeUpdated.tasksList.map(t => {
    //       if (t.taskId == payload.task.taskId) {
    //         t.featureImagePath = payload.data.featureimageUrl;
    //         return t;
    //       } else return t;
    //     });
    //   }

    //   OldStateData.kanbanboard = OldStateData.kanbanboard.map(k => {
    //     if (k.id == columnToBeUpdated.id) {
    //       return columnToBeUpdated;
    //     } else return k;
    //   });

    //   return {
    //     ...state,
    //     data: OldStateData,
    //   };
    // }

    case constants.UPDATEKANBAN:
      {
        updatedState = cloneDeep(state.data);
        if (
          !isEmpty(updatedState.kanban) &&
          updatedState.kanban.boardId == action.payload.boardId
        ) {
          updatedState.kanban = action.payload;
        }
        return {
          ...state,
          data: updatedState,
        };
      }
      break;

    case constants.UPDATEKANBANAFTERSTATUSDELETE:
      {
        updatedState = cloneDeep(state.data);
        updatedState.kanban = action.payload.updateboard;
        updatedState.unsortedTask = action.payload.unsortedTask;
        return {
          ...state,
          data: updatedState,
        };
      }
      break;

    case constants.UPDATETASKPOSITIONSINSTORE:
      {
        updatedState = cloneDeep(state.data);
        updatedState.kanban.tasksList = updatedState.kanban.tasksList.map((ele) => {
          let task = action.payload.find((f) => f.itemId == ele.taskId) || false;
          if (task) {
            ele.taskPosition = task.position;
            return ele;
          } else return ele;
        });
        return {
          ...state,
          data: updatedState,
        };
      }
      break;

    case constants.ADDBOARDTOSTORE:
      {
        return {
          ...state,
          data: { ...state.data, boards: [action.payload.data, ...state.data.boards] },
        };
      }
      break;

    case constants.DELETEBOARD:
      if (state.data.boards) {
        return {
          ...state,
          data: {
            ...state.data,
            boards: state.data.boards.filter(function (element) {
              //
              return element.projectId !== action.payload.data.projectId;
            }),
          },
        };
      } else return { ...state };
      break;

    case constants.DELETEARCHIVETASKFROMBOARD:
      {
        if (!isEmpty(state.data.kanban) && state.data.kanban.tasksList) {
          return {
            ...state,
            data: {
              ...state.data,
              kanban: {
                ...state.data.kanban,
                tasksList: state.data.kanban.tasksList.filter(function (element) {
                  return element.taskId !== action.payload.data.taskId;
                }),
              },
            },
          };
        } else return { ...state };
      }
      break;

    case constants.UPDATEKANBANTASKOBJECT:
      {
        updatedState = cloneDeep(state.data);
        if (!isEmpty(updatedState.kanban) && updatedState.kanban.tasksList) {
          updatedState.kanban.tasksList = updatedState.kanban.tasksList.map((ele) => {
            if (ele.taskId === action.payload.data.taskId) return action.payload.data;
            else return ele;
          });
          return {
            ...state,
            data: updatedState,
          };
        } else return { ...state };
      }
      break;

    case constants.UPDATEKANBANTASKOBJECTCOUNT:
      {
        updatedState = cloneDeep(state.data);
        if (!isEmpty(updatedState.kanban) && updatedState.kanban.tasksList) {
          updatedState.kanban.tasksList = updatedState.kanban.tasksList.map((ele) => {
            if (ele.taskId === action.payload.data.taskId) {
              if (action.payload.data.key === "Add") {
                ele.totalComments = ele.totalComments + 1;
              }
              if (action.payload.data.key === "Delete") {
                ele.totalComments = ele.totalComments - 1;
              }
              return ele;
            } else return ele;
          });
          return {
            ...state,
            data: updatedState,
          };
        } else return { ...state };
      }
      break;

    default:
      return state;
  }
};

export default {
  boards,
};
