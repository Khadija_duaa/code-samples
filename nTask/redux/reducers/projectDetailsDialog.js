import consts from "../constants/types";
import cloneDeep from "lodash/cloneDeep";

export default function (state = {}, action) {
  switch (action.type) {
    case consts.PROJECTDETAILSDIALOG: {
      let stateCopy = cloneDeep(state);
      stateCopy = { ...stateCopy, ...action.payload.data }
      return stateCopy;
    }
      break;
    case consts.TOGGLEPROJECTVIEW: {
      return { ...state, ...action.payload.data }
    }
      break;

    default:
      return state;
  }
}
