import constants from "./../constants/types";
const loadingProjectDetails = (state = {}, action) => {
  switch (action.type) {
    case constants.GETSETPROJECTLOADINGSTATE: {
      return action.payload;
    }
      break;

    default:
      return state;
  }
};

export default {
  loadingProjectDetails,
};
