import constants from "../constants/types";
const authenticationResponse = (state = {}, action) => {
  switch (action.type) {
    case constants.POPULATEAUTH:
      return {
        ...state,
        Auth: action.payload.data
      };
      break;

      case constants.CHECKAUTH:
      return {
        ...state, authState: action.payload
      };
      break;

    case constants.UPDATEAUTH:
    //
      return {
        ...state,
        Auth: action.payload.data
      };
      break;


      case constants.DELETETEAUTH:
      return {
        ...state,
        Auth:""
      }
      break;


    default:
      return state;
  }
};

export default {
  authenticationResponse: authenticationResponse
};
