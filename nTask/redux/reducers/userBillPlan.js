import constants from "../constants/types";

const userBillPlan = (state = {}, action) => {
  switch (action.type) {

    case constants.VISIBLEUPGRADEPLAN: {
      return {
        visibleUpgradePlan: action.payload.data.show,
        modeUpgradePlan: action.payload.data.mode
      };
    }
      break;

    default:
      return state;
  }
};
export default {
  userBillPlan: userBillPlan
};

