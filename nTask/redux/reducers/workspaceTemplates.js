import constants from "../constants/types";
import cloneDeep from "lodash/cloneDeep";
import templatesConstants from "../../utils/constants/templatesConstants";

const initialState = {
  data: {
    allWSTemplates: [],
    defaultWSTemplate: "",
  },
};
const workspaceTemplates = (state = initialState, action) => {
  let newData;
  let updatedData;
  switch (action.type) {
    case constants.WORKSPACEALLTEMPLATES: {
      newData = cloneDeep(state.data);
      newData.allWSTemplates = action.payload.data;
      return {
        ...state,
        data: newData,
      };
    }
      break;

    case constants.WORKSPACEDEFAULTTEMPLATE: {
      newData = cloneDeep(state.data);
      newData.defaultWSTemplate = action.payload.data;
      return {
        ...state,
        data: newData,
      };
    }
      break;

    case constants.WSSETTINGDEFATUL: {
      let defautlTemplate = action.payload.data;
      updatedData = cloneDeep(state.data);
      updatedData.allWSTemplates = updatedData.allWSTemplates.map(d => {
        d.templates = d.templates.map(s => {
          if (s.templateId == defautlTemplate.templateId)
            s.isWorkspaceDefault = true;
          else
            s.isWorkspaceDefault = false;
          return s
        });
        return d;
      });
      updatedData.defaultWSTemplate = defautlTemplate;
      return {
        ...state,
        data: updatedData,
      };
    }
      break;

    case constants.CREATEWSTEMPLATEITEM: {
      newData = cloneDeep(state.data);
      updatedData = action.payload.data;
      newData.allWSTemplates = newData.allWSTemplates.map(groupItem => {
        if (groupItem.groupName == templatesConstants.SAVED_TEMPLATES) {
          groupItem.templates = [...groupItem.templates, updatedData];
        }
        return groupItem;
      });
      return {
        ...state,
        data: newData,
      };
    }
      break;

    // case constants.ADDWSTEMPLATE:
    //   newData = cloneDeep(state.data);
    //   updatedData = action.payload.data;
    //   newData.allWSTemplates = newData.allWSTemplates.map(groupItem => {
    //     if (groupItem.groupName == templatesConstants.SAVED_TEMPLATES) {
    //       groupItem.templates = [...groupItem.templates, updatedData];
    //     }
    //     return groupItem;
    //   });
    //   return {
    //     ...state,
    //     data: newData,
    //   };
    case constants.UPDATESAVEDTEMPLATEITEM: {
      newData = cloneDeep(state.data);
      updatedData = action.payload.data;
      newData.allWSTemplates = newData.allWSTemplates.map(groupItem => {
        if (groupItem.groupName == templatesConstants.SAVED_TEMPLATES) {
          groupItem.templates = groupItem.templates.map(element => {
            if (element.templateId === updatedData.templateId) {
              return {
                ...updatedData
              };
            }
            return element;
          })
        }
        return groupItem;
      });
      if (newData.defaultWSTemplate.templateId == updatedData.templateId)
        newData.defaultWSTemplate = updatedData;
      return {
        ...state,
        data: newData,
      };
    }
      break;

    case constants.DELETETEMPLATE: {
      newData = cloneDeep(state.data);
      newData.allWSTemplates = newData.allWSTemplates.map(groupItem => {
        if (groupItem.groupName == templatesConstants.SAVED_TEMPLATES) {
          groupItem.templates = groupItem.templates.filter(s => s.templateId !== action.payload.data.templateId);
        }
        return groupItem;
      });
      return {
        ...state,
        data: newData,
      };
    }
      break;

    default:
      return state;
  }
};

export default {
  workspaceTemplates,
};
