import constants from "./../constants/types";
const taskActivities = (state = {}, action) => {
    switch (action.type) {
        case constants.POPULATETASKCOMMENTSACTIVITIES:
            return {
                ...state,
                data: action.payload.data
            };
            break;

        default:
            return state;
    }
};

export default {
    taskActivities
};

