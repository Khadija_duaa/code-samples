import constants from "./../constants/types";
const riskCommentsData = (state = {}, action) => {

  switch (action.type) {
    case constants.POPULATERISKCOMMENTS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.UPDATERISKCOMMENTS:
      let modifiedTasks = state.data.map(function (element) {
        if (element.update.id === action.payload.data.id) {
          return {
            ...action.payload.data.response
          };
        }
        return element;
      });
      return {
        ...state,
        data: modifiedTasks
      };
      break;

    case constants.ADDNEWRISKCOMMENT:
      return {
        ...state,
        data: [...state.data, action.payload.data]
      };
      break;

    case constants.DELETERISKCOMMENTS:
      return {
        ...state,
        data: state.data.filter(function (element) {
          return element.update.id !== action.payload.data;
        })
      };
      break;

    default:
      return state;
  }
};

export default {
  riskCommentsData: riskCommentsData
};
