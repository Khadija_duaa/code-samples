import consts from "../constants/types";

export default function (state = {}, action) {

  switch (action.type) {
    case consts.GANTTCOMPONENT: {
      return { gantt: action.payload.gantt, plannedVsActual: action.payload.plannedVsActual }
    }
      break;

    case consts.GANTTMODECHANGE: {
      return { gantt: action.payload.gantt, plannedVsActual: action.payload.plannedVsActual }
    }
      break;

    default:
      return state;
  }
}
