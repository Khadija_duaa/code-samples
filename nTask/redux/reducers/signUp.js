import constants from "./../constants/types";
const signUpReducer = (state = {}, action) => {
  switch (action.type) {
    case constants.POPULATEDATA:
      return {
        ...state,
        data: action.payload.data,
      };
      break;

    case constants.DELETESIGNUPDATA:
      return {
        ...state,
        data: "",
      };
      break;

    default:
      return state;
  }
};

export default {
  registerResponse: signUpReducer
};

