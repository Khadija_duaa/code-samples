import consts from "../constants/types";

export default function(state = {}, action) {
  switch (action.type) {
    case consts.SHOWHIDENOTIFICATIONBAR: {
      return  action.payload.data;
    }
    break;
    
    default:
      return state;
  }
}
