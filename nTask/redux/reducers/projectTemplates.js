import constants from "../constants/types";
import cloneDeep from "lodash/cloneDeep";

const initialState = {
  data: {
    allProjectTemplates: [],
    defaultProjectTemplate: '',
  }
}
const projectTemplates = (state = initialState, action) => {
  let newData;
  let updatedData;
  switch (action.type) {
    // case constants.PROJECTALLTEMPLATES:
    //   newData = cloneDeep(state.data);
    //   newData.allProjectTemplates = action.payload.data;
    //   return {
    //     ...state,
    //     data: newData,
    //   };
    // case constants.PROJECTDEFAULTTEMPLATE:{
    //   newData = cloneDeep(state.data);
    //   newData.allProjectTemplates = newData.allProjectTemplates.map(groupItem => {
    //     let gItem = [...groupItem.templates];
    //     groupItem.templates = gItem.map(sItem => {
    //       if(action.payload.data.response.templateId == sItem.templateId)
    //         return action.payload.data.response;
    //       else
    //        return sItem;
    //     });
    //     return groupItem;
    //   });
    //   // newData.defaultWSTemplate = action.payload.data.response;
    //   return {
    //     ...state,
    //     data: newData,
    //   };
    // }
    // case constants.SAVEPROJECTTEMPLATESTATUSITEM:
    //   newData = cloneDeep(state.data);
    //   updatedData = action.payload.data;
    //   newData.allProjectTemplates = newData.allProjectTemplates.map(groupItem => {
    //     let gItem = [...groupItem.statusList];
    //     groupItem.statusList = gItem.map(sItem => {
    //       if (sItem.statusCode == updatedData.statusCode) return updatedData;
    //       return sItem;
    //     });
    //     return groupItem;
    //   });
    //   return {
    //     ...state,
    //     data: newData,
    //   };
    // case constants.UPDATEPROJECTSTATUSAFTERSETTINGDEFAULT:{
    //   newData = cloneDeep(state.data);
    //   let updatedData = action.payload.data;
    //   newData.allProjectTemplates = newData.allProjectTemplates.map(d => {
    //     d.statusList = d.statusList.map(s => {
    //       if (s.id == updatedData.fromStatus.id) {
    //         return updatedData.fromStatus;
    //       } else if (s.id == updatedData.toStatus.id) {
    //         return updatedData.toStatus;
    //       } else {
    //         return s;
    //       }
    //     });
    //     return d;
    //   });
    //   return {
    //     ...state,
    //     data: newData,
    //   };
    // }
    // case constants.UPDATEDELETEDPROJECTSTATUS: {
    //   newData = cloneDeep(state.data);
    //   newData.allProjectTemplates = newData.allProjectTemplates.map(d => {
    //     d.statusList = d.statusList.map(s => {
    //       if (s.statusCode == action.payload.data.fromStatus.statusCode) {
    //         return action.payload.data.fromStatus;
    //       } else {
    //         return s;
    //       }
    //     });
    //     return d;
    //   });
    //   return {
    //     ...state,
    //     data: newData,
    //   };
    // }
    default:
      return state;
  }
};

export default {
  projectTemplates,
};
