import constants from "./../constants/types";
import { debug } from "util";
const userPreference = (state = {}, action) => {
  //
  switch (action.type) {
    case constants.POPULATEUSERPREFERNCE:
      //
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.UPDATEPREFERENCE:
      // 
      //  
      // // 
      // // 
      //  

      let modifiedPreferences = state.data.preferences.map(element => {
        let updatedPreference = action.payload.data.find(x => {
          if (x.NotificationId == element.notificationId) {
            element.isTurnedOn = x.isTurnedOn;
            return element;
          }
        });
        //if(updatedPreference)
        if (element.subNotifications.length > 0) {
          element.subNotifications = element.subNotifications.map(innerElement => {
            let updatedSubNotification = action.payload.data.find(x => {
              if (x.NotificationId === innerElement.id) {
                // 
                //  
                innerElement.isTurnedOn = x.isTurnedOn;
                // 
                return innerElement;
              }
            });
            return { ...innerElement, ...updatedSubNotification };
          });
        }
        return { ...element, ...updatedPreference };
      });
      return {
        ...state,
        data: { preferences: modifiedPreferences, background: state.data.background }
      };
      break;

    case constants.POPULATEUSERPREFERNCE:
      return {
        ...state,
        data: ""
      };
      break;

    default:
      return state;
  }
};

export default {
  userPreference: userPreference
};
