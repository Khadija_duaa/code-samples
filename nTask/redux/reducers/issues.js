import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import unionBy from "lodash/unionBy";
import xorBy from "lodash/xorBy";
import searchQuery from "../../components/CustomTable2/ColumnSettingDropdown/searchQuery";
import updateCFSelectedOption from "../../helper/updateCFSelectedOption";
import findIndex from "lodash/findIndex";
import sortBy from "lodash/sortBy";

const issues = (state = {}, action) => {
  switch (action.type) {
    case constants.POPULATEISSUESINFO:
      return {
        ...state,
        ...action.payload,
      };
      break;
    case constants.CREATE_ISSUE:
      return { ...state, data: [action.payload, ...state.data] };
      break;

    case constants.CREATED_ISSUE_EDIT:
      const updatedIssuesArr = state.data.map(t => {
        if (t.clientId == action.payload.clientId) {
          return { ...action.payload, isNew: true };
        }
        return t;
      });
      return { ...state, data: updatedIssuesArr };
      break;
    case constants.SAVE_ALL_ISSUES:
      return {
        ...state,
        data: action.payload,
      };
      break;
    case constants.UPDATE_ISSUE_CUSTOM_FIELD:
      return { ...state, data: updateCFSelectedOption(state.data, action.payload) };
      break;

    case constants.UPDATEISSUECUSTOMFIELDDATA:
      {
        let payload = action.payload;
        let allIssues = cloneDeep(state.data);
        if (payload.type === "Create") {
          allIssues = allIssues.map(r => {
            if (r.id === payload.obj.groupId) {
              return {
                ...r,
                customFieldData: r.customFieldData
                  ? [...r.customFieldData, payload.obj]
                  : [payload.obj],
              };
            }
            return r;
          });
        }
        if (payload.type === "Update") {
          allIssues = allIssues.map(r => {
            if (r.id === payload.obj.groupId) {
              let selectedIssueData = cloneDeep(r.customFieldData);
              selectedIssueData = selectedIssueData.map(sd => {
                if (sd.fieldId === payload.obj.fieldId) return payload.obj;
                return sd;
              });
              return { ...r, customFieldData: selectedIssueData };
            }
            return r;
          });
        }

        return {
          ...state,
          data: allIssues,
        };
      }
      break;

    case constants.UPDATEISSUE:
      let tempIssues = cloneDeep(state.data);
      let modifiedIssue = tempIssues.map(function (element) {
        if (element.id === action.payload.data.id) {
          return {
            ...action.payload.data,
          };
        }
        return { ...element, modifiedIssue };
      });
      return {
        ...state,
        data: modifiedIssue,
      };
      break;

    case constants.UPDATEBULKISSUEINFO:
      if (action.payload.isDelete) {
        // Formating action data issue's id array, with id object
        let inputData = action.payload.data.map(issueId => {
          return { id: issueId };
        });
        let data = xorBy(inputData || [], state.data || [], "id");
        return {
          ...state,
          data,
        };
      } else {
        // Merge new issues data with store issues,
        const updatedState = state.data.map(x => {
          const itemToUpdate = action.payload.data.find(i => i.id == x.id);
          if (itemToUpdate) {
            return itemToUpdate;
          }
          return x;
        });
        return { ...state, data: updatedState };
      }
      break;

    case constants.COPYISSUES:
      return {
        ...state,
        data: [action.payload.data, ...state.data],
      };
      break;

    case constants.DELETEISSUE:
      return {
        ...state,
        data: state.data.filter(function (element) {
          //
          return element.id !== action.payload.data;
        }),
      };
      break;
    case constants.SET_ISSUE_CUSTOM_FILTER:
      {
        let updatedFilters = action.payload.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (cv.customFieldsFilters) {
            let customFilters = Object.keys(cv.customFieldsFilters);
            customFilters.map(item => (currentValue[item] = cv.customFieldsFilters[item]));
            delete currentValue.customFieldsFilters;
            res.push(currentValue);
          } else res.push(cv);
          return res;
        }, []);
        const defaultFilter = updatedFilters.find(f => f.defaultFilter);
        searchQuery.issueFilter = defaultFilter || searchQuery.issueFilter;
        return {
          ...state,
          savedFilters: updatedFilters,
          issueFilter: defaultFilter || searchQuery.issueFilter,
        };
      }
      break;
    case constants.DELETECUSTOMFIELDFILTER:
      {
        let stateData = cloneDeep(state);
        stateData.savedFilters = stateData.savedFilters.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (currentValue[action.payload[0]]) {
            delete currentValue[action.payload[0]];
            res.push(currentValue);
          } else res.push(currentValue);
          return res;
        }, []);
        if (stateData.issueFilter[action.payload[0]]) {
          delete stateData.issueFilter[action.payload[0]];
        }
        return stateData;
      }
      break;
    case constants.DELETE_ISSUE_FILTER:
      const stateCopy = cloneDeep(state.issueFilter);
      // stateCopy[action.payload].selectedValues = [];
      // stateCopy[action.payload].type = "";
      delete stateCopy[action.payload];
      const updatedFilterState = { ...state, issueFilter: stateCopy };
      searchQuery.issueFilter = updatedFilterState.issueFilter;
      return updatedFilterState;
      break;

    case constants.ADD_NEW_ISSUE_CUSTOM_FILTER:
      {
        let updatedFilters = action.payload.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (cv.customFieldsFilters) {
            let customFilters = Object.keys(cv.customFieldsFilters);
            customFilters.map(item => (currentValue[item] = cv.customFieldsFilters[item]));
            delete currentValue.customFieldsFilters;
            res.push(currentValue);
          } else res.push(cv);
          return res;
        }, []);
        return { ...state, savedFilters: updatedFilters };
      }
      break;

    case constants.DELETE_ISSUE_CUSTOM_FILTER:
      let updatedFilter = state.savedFilters.filter(item => item.filterId !== action.payload);
      searchQuery.issueFilter = {};
      return { ...state, savedFilters: updatedFilter, issueFilter: {} };
      break;

    case constants.UPDATE_ISSUE_FILTER: {
      const cloneState = cloneDeep(state.issueFilter);
      let updateFilters;
      const payloadLength = Object.keys(action.payload).length;
      if (payloadLength) {
        if (state.issueFilter && state.issueFilter.filterId !== action.payload.filterId) {
          updateFilters = { issueFilter: action.payload }
        } else {
          updateFilters = { issueFilter: { ...cloneState, ...action.payload } };
        }
      } else {
        updateFilters = { issueFilter: {} }
      }
      searchQuery.issueFilter = updateFilters.issueFilter;
      return { ...state, ...updateFilters };
    }
      break;

    case constants.CLEAR_ISSUE_FILTER:
      searchQuery.issueFilter = {};
      return { ...state, issueFilter: {} };
      break;
    case constants.RESETISSUESINFO:
      return {
        ...state,
        issues: "",
      };
      break;

    case constants.MARKREADISSUE:
      let mark = "";
      let notifications = [];
      let allData = state.data.filter(function (element) {
        return element.id !== action.payload.id;
      });
      mark = state.data.find(function (element) {
        return element.id === action.payload.id;
      });
      if (mark) {
        notifications = mark.notification.filter(x => {
          return x.users.map(y => {
            if (y.userId === action.payload.currentUser && y.isViewed === false) {
              y.isViewed = true;
            }
            return y;
          });
        });
      }
      mark.notification = notifications;
      return {
        ...state,
        data: [mark, ...allData],
      };
      break;

    case constants.UPDATEISSUEROWS:
      {
        let updatedRows = action.payload;
        return {
          ...state,
          data: updatedRows,
        };
      }
      break;

    case constants.UPDATE_BULK_ISSUES:
      {
        let updatedIssueData = state.data.reduce((result, cv) => {
          const isExist = action.payload.find(item => item.id === cv.id);
          if (isExist) result.push(isExist);
          else result.push(cv);
          return result;
        }, []);
        return { ...state, data: updatedIssueData };
      }
      break;
    case constants.DELETE_BULK_ISSUE:
      {
        let issues = state.data;
        let updatedIssue = issues.filter(x => {
          return !action.payload.issueIds.includes(x.id);
        });
        return { ...state, data: updatedIssue };
      }
      break;
    case constants.UNARCHIVE_BULK_ISSUE:
      {
        let issues = state.data;
        let updatedIssue = issues.map(x => {
          if (action.payload.issueIds.includes(x.id)) {
            return { ...x, isArchive: false };
          } else return x;
        });
        return { ...state, data: updatedIssue };
      }
      break;

    case constants.DELETE_ISSUE:
      return {
        ...state,
        data: state.data
          ? state.data.filter(function (element) {
            return element.id !== action.payload.data;
          })
          : [],
      };
      break;
    case constants.UPDATE_ISSUE_DATA:
      {
        let updatedIssues = state.data.map(x => {
          if (x.id === action.payload.id) {
            return { ...x, ...action.payload };
          } else {
            return x;
          }
        });
        return {
          ...state,
          data: updatedIssues,
        };
      }
      break;

    case constants.SHOW_ALL_ISSUE:
      {
        searchQuery.quickFilters = {};
        return { ...state, quickFilters: {}, data: state.data.filter(t => !t.isArchive) };
      }
      break;
    case constants.UPDATE_ISSUE_QUICK_FILTER:
      {
        const updatedFilters = { ...state.quickFilters, ...action.payload };
        searchQuery.quickFilters = updatedFilters;
        return { ...state, quickFilters: updatedFilters };
      }
      break;
    case constants.REMOVE_ISSUE_QUICK_FILTER:
      {
        const { [action.payload]: value, ...rest } = state.quickFilters;
        const archiveFreeIssues =
          value.type == "Archived" ? state.data.filter(t => !t.isArchive) : state.data;
        searchQuery.quickFilters = rest ? rest : {};
        return { ...state, data: archiveFreeIssues, quickFilters: rest ? rest : {} };
      }
      break;
    case constants.DELETE_ISSUE:
      return {
        ...state,
        data: state.data
          ? state.data.filter(function (element) {
            return element.id !== action.payload.data;
          })
          : [],
      };
      break;
    case constants.ADD_BULK_ISSUE: {
      return { ...state, data: [...action.payload, ...state.data] };
    }
      break;
    case constants.UPDATE_ISSUE_ORDER:
      const sortedArray = sortBy(state.data, function (obj) {
        return findIndex(action.payload, { itemId: obj.id });
      });
      return { ...state, data: sortedArray };
      break;
    default:
      return state;
  }
};

export default {
  issues,
};
