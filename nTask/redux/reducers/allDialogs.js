import consts from "../constants/types";

export default function(state = {}, action) {
  switch (action.type) {
    case consts.PUBLICLINKDIALOG: {
      return { ...state, publicLink: action.payload };
    }
    break;

    case consts.RISKDETAILDIALOG: {
      return { ...state, riskDetailDialog: { ...state.riskDetailDialog, ...action.payload } };
    }
    break;

    case consts.DELETEDIALOGUESTATE: {
      return { ...state, deleteDialogue: { ...state.deleteDialogue, ...action.payload } };
    }
    break;

    case consts.CUSTOMFIELDDIALOGUESTATE: {
      return { ...state, customFieldDialog: { ...state.customFieldDialog, ...action.payload } };
    }
    break;

    case consts.TASKDETAILDIALOG: {
      return { ...state, taskDetailDialog: { ...state.taskDetailDialog, ...action.payload } };
    }
    break;

    case consts.ISSUEDETAILDIALOG: {
      return { ...state, issueDetailDialog: { ...state.issueDetailDialog, ...action.payload } };
    }
    case consts.PROFILESETTINGSDIALOGUE: {
      return { ...state, profileSettings: { ...state.profileSettings, ...action.payload } };
    }
    break;


    default:
      return state;
  }
}
