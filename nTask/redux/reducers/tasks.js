import constants from "./../constants/types";
import COLORCODES from "../../components/constants/colorCodes";
import cloneDeep from "lodash/cloneDeep";
import differenceBy from "lodash/differenceBy";
import unionBy from "lodash/unionBy";
import xorBy from "lodash/xorBy";
import updateCFSelectedOption from "../../helper/updateCFSelectedOption";
import { emptyTask } from "../../utils/constants/emptyTask";
import searchQuery from "../../components/CustomTable2/ColumnSettingDropdown/searchQuery";
import findIndex from "lodash/findIndex";
import sortBy from "lodash/sortBy";

import { grid } from "../../components/CustomTable2/gridInstance";

const tasks = (state = {}, action) => {
  let updatedTasks;
  let newMapping;
  switch (action.type) {
    case constants.CLEAR_ALL_TASKS:
      {
        searchQuery.taskFilter = {};
        searchQuery.quickFilters = {};
        return { ...state, data: [], taskCount: 0, taskFilter: {}, quickFilters: {} };
      }
      break;
    case constants.SAVE_ALL_TASKS:
      return {
        ...state,
        data: action.payload,
        taskCount: state.data.length ? state.taskCount : action.payload.length,
      };
      break;
    case constants.CREATE_TASK:
      return { ...state, data: [action.payload, ...state.data] };
      break;

    case constants.CREATED_TASK_EDIT:
      const updatedTasksArr = state.data.map(t => {
        if (t.clientId == action.payload.clientId) {
          return { ...action.payload, isNew: true };
        }
        return t;
      });
      return { ...state, data: updatedTasksArr };
      break;
    case constants.UPDATE_TASK_DATA:
      const newTasksArray = state.data.map(t => {
        if (t.taskId == action.payload.taskId) {
          return action.payload;
        }
        return t;
      });
      return { ...state, data: newTasksArray };
      break;

    case constants.UPDATE_TASK_ORDER:
      const sortedArray = sortBy(state.data, function (obj) {
        return findIndex(action.payload, { itemId: obj.taskId });
      });
      return { ...state, data: sortedArray };
      break;

    case constants.UPDATE_BULK_TASK: {
      let updatedTaskData = state.data.reduce((result, cv) => {
        const isExist = action.payload.find(item => item.taskId === cv.taskId);
        if (isExist) result.push(isExist);
        else result.push(cv);
        return result;
      }, []);
      return { ...state, data: updatedTaskData };
    }
      break;

    case constants.DELETE_BULK_TASK: {
      let tasks = state.data;
      let updatedTask = tasks.filter(x => {
        return !action.payload.taskIds.includes(x.taskId);
      });
      return { ...state, data: updatedTask };
    }
      break;

    case constants.UNARCHIVE_BULK_TASK: {
      let tasks = state.data;
      let updatedTask = tasks.map(x => {
        if (action.payload.taskIds.includes(x.taskId)) {
          return { ...x, isDeleted: false };
        } else return x;
      });
      return { ...state, data: updatedTask };
    }
      break;

    case constants.UPDATE_TASK_COUNT:
      return { ...state, taskCount: action.payload };
      break;
    case constants.SAVE_TASK_GRID_INSTANCE:
      return { ...state, grid: action.payload };
      break;

      case constants.UPDATE_TASK_FILTER: {
        const cloneState = cloneDeep(state.taskFilter);
        /***** Functionality To Handle Remove Filter without clicking clear btn (Commented for future use) *****/
        // const key1 = Object.keys(action.payload)[0];        
        // const key2 = Object.keys(action.payload[key1])[1];
        // let payloadLength;                                                    
        // if(action.payload[key1].type ==''){             //We have three types of payload this check handle these types      
        //   payloadLength = action.payload[key1][key2].length;          //Checking if action.payload.key.key has type='' then it calculates length of array of selectedvalue 
        // }
        // else{
        //   payloadLength = Object.keys(action.payload).length;          //Else calculates length of action.payload 
        // }
        let updateFilters;
        const payloadLength = Object.keys(action.payload).length;
        // if(!payloadLength){                                             //if filter is removed then this check is executed and filter that removed filter from state
        //   const updated = Object.fromEntries(Object.entries(cloneState).filter(([key]) => key!=key1));
        //   updateFilters = { taskFilter: { ...updated } };
        // }
        // else{
          if (payloadLength) {
            if (state.taskFilter && state.taskFilter.filterId !== action.payload.filterId) {
              updateFilters = { taskFilter: action.payload }
            } else {
              updateFilters = { taskFilter: { ...cloneState, ...action.payload } };
            }
          }
           else {
            updateFilters = { taskFilter: {} }
          }
        // }
        searchQuery.taskFilter = updateFilters.taskFilter;
        return { ...state, ...updateFilters };
      }
      break;

    case constants.SET_TASK_CUSTOM_FILTER:
      {
        let updatedFilters = action.payload.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (cv.customFieldsFilters) {
            let customFilters = Object.keys(cv.customFieldsFilters);
            customFilters.map(item => (currentValue[item] = cv.customFieldsFilters[item]));
            delete currentValue.customFieldsFilters;
            res.push(currentValue);
          } else res.push(cv);
          return res;
        }, []);
        const defaultFilter = updatedFilters.find(f => f.defaultFilter);
        searchQuery.taskFilter = defaultFilter || searchQuery.taskFilter;
        return {
          ...state,
          savedFilters: updatedFilters,
          taskFilter: defaultFilter || searchQuery.taskFilter,
        };
      }
      break;
    case constants.DELETECUSTOMFIELDFILTER:
      {
        let stateData = cloneDeep(state);
        stateData.savedFilters = stateData.savedFilters.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (currentValue[action.payload[0]]) {
            delete currentValue[action.payload[0]];
            res.push(currentValue);
          } else res.push(currentValue);
          return res;
        }, []);
        if (stateData.taskFilter[action.payload[0]]) {
          delete stateData.taskFilter[action.payload[0]];
        }
        return stateData;
      }
      break;
    case constants.DELETE_TASK_FILTER:
      const stateCopy = cloneDeep(state.taskFilter);
      // stateCopy[action.payload].selectedValues = [];
      // stateCopy[action.payload].type = "";
      delete stateCopy[action.payload];
      const updatedFilterState = { ...state, taskFilter: stateCopy };
      searchQuery.taskFilter = updatedFilterState.taskFilter;
      return updatedFilterState;
      break;

    case constants.ADD_NEW_CUSTOM_FILTER:
      {
        let updatedFilters = action.payload.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (cv.customFieldsFilters) {
            let customFilters = Object.keys(cv.customFieldsFilters);
            customFilters.map(item => (currentValue[item] = cv.customFieldsFilters[item]));
            delete currentValue.customFieldsFilters;
            res.push(currentValue);
          } else res.push(cv);
          return res;
        }, []);
        return { ...state, savedFilters: updatedFilters };
      }
      break;

    case constants.DELETE_TASK_CUSTOM_FILTER:
      let updatedFilter = state.savedFilters.filter(item => item.filterId !== action.payload);
      searchQuery.taskFilter = {};
      return { ...state, savedFilters: updatedFilter, taskFilter: {} };
      break;

    case constants.CLEAR_TASK_FILTER:
      searchQuery.taskFilter = {};
      return { ...state, taskFilter: {} };
      break;

    case constants.UPDATE_TASK_QUICK_FILTER:
      {
        const updatedFilters = { ...state.quickFilters, ...action.payload };
        searchQuery.quickFilters = updatedFilters;
        return { ...state, quickFilters: updatedFilters };
      }
      break;
    case constants.ADD_BULK_TASKS: {
      return { ...state, data: [...action.payload, ...state.data] };
    }
      break;

    case constants.REMOVE_TASK_QUICK_FILTER: {
      const { [action.payload]: value, ...rest } = state.quickFilters;
      const archiveFreeTasks =
        value.type == "Archived" ? state.data.filter(t => !t.isDeleted) : state.data;
      searchQuery.quickFilters = rest ? rest : {};
      return { ...state, data: archiveFreeTasks, quickFilters: rest ? rest : {} };
    }
      break;
    case constants.SHOW_ALL_TASK: {
      searchQuery.quickFilters = {};
      return { ...state, quickFilters: {}, data: state.data.filter(t => !t.isDeleted) };
    }
      break;

    case constants.POPULATETASKSINFO:
      return {
        ...state,
        ...action.payload,
      };
      break;

    case constants.UPDATE_TASK_CUSTOM_FIELD:
      return { ...state, data: updateCFSelectedOption(state.data, action.payload) };
      break;

    case constants.ADDTASKS: {
      return { ...state, data: action.payload };
    }
      break;

    case constants.UPDATE_TASK_STATUS_AFTER_SETTING_WORKSPACE_DEFAULT: {
      updatedTasks = cloneDeep(state.data);
      newMapping = action.payload.data.mapping;
      let projectList = action.payload.data.projects;
      let modifiedTasks = updatedTasks
        ? updatedTasks.map(el => {
          if (el.projectId == null || projectList.includes(el.projectId)) {
            let statusItem = newMapping.find(item => item.statusIDFrom == el.status);
            if (statusItem) {
              el.status = statusItem.statusIDTo;
              el.statusTitle = statusItem.toStatusTitle;
            }
            // newMapping.map(item => {
            //   if(item.statusIDFrom == el.status){
            //     el.status = item.statusIDTo;
            //     el.statusTitle = item.toStatusTitle;
            //   }
            // })
          }
          return el;
        })
        : [];
      setTimeout(() => {
        grid.grid.redrawRows();
      }, 0);

      return {
        ...state,
        data: modifiedTasks,
      };
    }
      break;

    case constants.UPDATE_TASK_STATUS_AFTER_SETTING_PROJECT_DEFAULT: {
      updatedTasks = cloneDeep(state.data);
      newMapping = action.payload.data.mapping;
      let modifiedTasks = updatedTasks
        ? updatedTasks.map(el => {
          if (el.projectId == action.payload.data.projectId) {
            let statusItem = newMapping.find(item => item.statusIDFrom == el.status);
            if (statusItem) {
              el.status = statusItem.statusIDTo;
              el.statusTitle = statusItem.toStatusTitle;
            }
            // newMapping.map(item => {
            //   if(item.statusIDFrom == el.status){

            //   }
            // })
          }
          return el;
        })
        : [];
      return {
        ...state,
        data: modifiedTasks,
      };
    }
      break;

    case constants.UPDATEBULKTASKSTATUS: {
      const project = action.payload.project;
      const statusObject = project.projectTemplate.statusList.find(s => s.isDoneState);
      const { statusColor, statusTitle, statusId } = statusObject;
      const updatedTasks = state.data.map(t => {
        if (t.projectId === project.projectId) {
          return { ...t, status: statusId, statusColor, statusTitle, progress: 100 };
        }
        return t;
      });
      return {
        ...state,
        data: updatedTasks,
      };
    }
      break;

    case constants.UPDATETASKINFO:
      let data = cloneDeep(action.payload.data);
      let tempProject = cloneDeep(state.data);
      let modifiedTasks = tempProject
        ? tempProject.map(function (element) {
          if (element.taskId === data.taskId) {
            let currentDate = new Date();
            let dueDate = new Date(element.dueDate);
            let obj = {
              id: data.taskId,
              title: data.taskTitle,
              start: new Date(data.createdDate),
              end: new Date(data.dueDate),
              desc: data.description,
              actualDueDateString: data.actualDueDateString,
              actualStartDateString: data.actualStartDateString,
              dueDateString: data.dueDateString,
              startDateString: data.startDateString,
              projectId: data.projectId ? data.projectId : null,
              //taskColor: data.colorCode,
              taskColor: COLORCODES.STATUSCOLORS[data.status],
              userColors: data.userColors,
              priority: data.priority,
              status: data.status,
              statusColor: data.statusColor,
              statusTitle: data.statusTitle,
              isDueToday: currentDate.toDateString() == dueDate.toDateString() ? true : false,
              isStared: data.isStared,
            };
            data.CalenderDetails = obj;
            return {
              ...data,
            };
          }
          return element;
        })
        : [];
      return {
        ...state,
        data: modifiedTasks,
      };
      break;

    case constants.UPDATEBULKTASKINFO:
      {
        let tasksArr = action.payload.data.filter(t => {
          return t.type !== "milestone";
        });
        const updatedTasks = state.data.map(task => {
          const currentTask = tasksArr.find(t => t.taskId == task.taskId);
          if (currentTask) {
            return currentTask;
          } else {
            return task;
          }
        }, []);
        return { ...state, data: updatedTasks };
      }
      break;

    case constants.ARCHIVEDBULKTASK:
      let tasks = state.data.filter(element => {
        return action.payload.data.indexOf(element.taskId) == -1;
      });
      return {
        ...state,
        data: tasks,
      };
      break;

    case constants.COPYTASKS:
      let obj = action.payload.data;
      return {
        ...state,
        data: [obj, ...state.data],
      };
      break;

    case constants.DELETETASK:
      return {
        ...state,
        data: state.data
          ? state.data.filter(function (element) {
            return element.taskId !== action.payload.data;
          })
          : [],
      };
      break;

    case constants.ARCHIVETASK:
      modifiedTasks = state.data.map(function (element) {
        if (element.taskId === action.payload.data.taskId) {
          return {
            ...action.payload.data,
          };
        }
        return element;
      });
      return {
        ...state,
        data: modifiedTasks,
      };
      break;

    case constants.ADDBULKTASKS:
      //Extracting new task added for traditional task views
      const newTasksArr = action.payload.newTasks.map(task => {
        return task.task;
      });
      return {
        ...state,
        data: [...newTasksArr, ...state.data],
      };
      break;

    //Adding user task effort
    case constants.ADDUSERTASKEFFORT:
      let taskObj = state.data.find(t => {
        return t.taskId == action.payload.data.taskId;
      });
      let taskEffortObj = [...taskObj.userTaskEffort, action.payload.data];
      taskObj = { ...taskObj, userTaskEffort: taskEffortObj };
      let updatedState = state.data.map(t => {
        if (t.taskId == taskObj.taskId) {
          return taskObj;
        } else {
          return t;
        }
      });
      return { data: updatedState };
      break;

    case constants.UPDATETASKRISK:
      tempProject = cloneDeep(state.data);
      modifiedTasks = tempProject.map(function (element) {
        if (action.payload.data.linkedTasks.indexOf(element.taskId) >= 0) {
          if (element.risks) {
            let riskArray = [action.payload.data, ...element.risks];
            element.risks = riskArray;
          } else element.risks = [action.payload.data];
          return element;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedTasks,
      };
      break;

    case constants.UPDATETASKISSUE:
      tempProject = cloneDeep(state.data);
      modifiedTasks = tempProject.map(function (element) {
        if (action.payload.data.linkedTasks.indexOf(element.taskId) >= 0) {
          if (element.issues) {
            let issueArray = [action.payload.data, ...element.issues];
            element.issues = issueArray;
          } else element.issues = [action.payload.data];
          return element;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedTasks,
      };
      break;

    case constants.UPDATETASKMEETING:
      tempProject = cloneDeep(state.data);
      modifiedTasks = tempProject.map(function (element) {
        if (action.payload.data && action.payload.data.taskId) {
          if (action.payload.data.taskId === element.taskId) {
            if (element.meetings) {
              let meetingArray = [action.payload.data, ...element.meetings];
              element.meetings = meetingArray;
            } else element.meetings = [action.payload.data];
            return element;
          }
        }
        return element;
      });
      return {
        ...state,
        data: modifiedTasks,
      };
      break;

    case constants.RESETTASKSINFO:
      //
      //
      return {
        ...state,
        data: "",
      };
      break;

    case constants.MARKREADTASK:
      let mark = "";
      let notifications = [];
      let allData = state.data.filter(function (element) {
        return element.taskId !== action.payload.taskId;
      });
      mark = state.data.find(function (element) {
        return element.taskId === action.payload.taskId;
      });
      if (mark) {
        notifications = mark.notification.filter(x => {
          return x.users.map(y => {
            if (y.userId === action.payload.currentUser && y.isViewed === false) {
              y.isViewed = true;
            }
            return y;
          });
        });
      }
      mark.notification = notifications;
      return {
        ...state,
        data: [mark, ...allData],
      };
      break;

    case constants.UPDATETASKSTIMESHEETSTATUS: {
      const status = action.payload.status;
      const taskIds = action.payload.taskIds;
      modifiedTasks = state.data.map(function (element) {
        if (
          taskIds.findIndex(id => {
            return id === element.taskId;
          }) > -1
        ) {
          element.timesheetStatus = status;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedTasks,
      };
    }
      break;

    case constants.STARTTIMER: {
      return {
        ...state,
      };
    }
      break;

    case constants.STOPTIMER: {
      let stateTasks = cloneDeep(state.data);
      const updatedTaskStateData = stateTasks.map(t => {
        if (t.taskId == action.payload.data.taskId) {
          return action.payload.data;
        } else return t;
      });

      return {
        ...state,
        data: updatedTaskStateData,
      };
    }
      break;

    case constants.REMOVEUSEREFFORT: {
      // const taskId = action.payload.taskId;
      // const taskEffortId = action.payload.taskEffortId;

      // let tasks = cloneDeep(state.data);
      // const taskIndex = tasks.findIndex((el) => el.taskId === taskId);
      // if (taskIndex > -1) {
      //   let userEfforts = tasks[taskIndex].userTaskEffort;
      //   tasks[taskIndex].userTaskEffort = differenceBy(
      //     userEfforts,
      //     [{ id: taskEffortId }],
      //     "id"
      //   );
      // }
      let Tasks = cloneDeep(state.data);
      const UpdatedTasks = Tasks.map(t => {
        if (t.taskId == action.payload.data.taskId) {
          return action.payload.data;
        } else return t;
      });
      return {
        ...state,
        data: UpdatedTasks,
      };
    }
      break;

    case constants.UPDATETASKCOMMENT: {
      const { taskId, totalAttachements, totalComments, totalUnreadComment } = action.payload.data;
      let tasks = cloneDeep(state.data);
      const updatedTasks = tasks
        ? tasks.map((item, index) => {
          if (taskId === item.taskId) {
            return {
              ...item,
              totalAttachment: totalAttachements,
              comments: totalComments,
            };
          } else {
            return item;
          }
        })
        : [];
      return {
        ...state,
        data: updatedTasks,
      };
    }
      break;

    case constants.REMOVETASKFROMARCHIIVED: {
      return {
        ...state,
        removeArchivedTask: action.payload.method,
      };
    }
      break;

    case constants.DELETEBULKTASK: {
      /* reducer for bulk delete tasks  */
      let updatedtasks = state.data.filter(task => {
        return action.payload.data.indexOf(task.taskId) == -1;
      });
      return {
        ...state,
        data: updatedtasks,
      };
    }
      break;

    case constants.DELETECHECKLIST: {
      let copyTask = cloneDeep(state.data);
      const {
        TaskId,
        CheckList: { CheckListId },
      } = action.payload.data;
      copyTask = copyTask.map(task => {
        if (task.taskId === TaskId) {
          let checkList = task.checkList;
          let filterCheckList = checkList.filter(c => {
            return c.checkListId !== CheckListId;
          });
          task.checkList = filterCheckList;
          return task;
        }
        return task;
      });
      return {
        ...state,
        data: copyTask,
      };
    }
      break;

    case constants.CREATECHECKLIST: {
      let tasksList = cloneDeep(state.data);
      let filteredTask = tasksList.find(t => {
        return t.taskId == action.payload.taskId;
      });
      filteredTask.checkList.push(action.payload.data.checklist);
      filteredTask.progress = action.payload.data.progress;
      let newUpdatedTask = tasksList.map(t => {
        if (t.taskId == action.payload.taskId) {
          return filteredTask;
        } else {
          return t;
        }
      });
      return {
        ...state,
        data: newUpdatedTask,
      };
    }
      break;

    case constants.SAVETODOLIST: {
      let tasksList = cloneDeep(state.data);
      let filteredTask = tasksList.find(t => {
        return t.taskId == action.payload.taskId;
      });
      filteredTask.checkList.push(action.payload.data.checklist);
      filteredTask.progress = action.payload.data.progress;

      let newUpdatedTask = tasksList.map(t => {
        if (t.taskId == action.payload.taskId) {
          return filteredTask;
        } else {
          return t;
        }
      });
      return {
        ...state,
        data: newUpdatedTask,
      };
    }
      break;

    case constants.UPDATECHECKLIST: {
      let tasksListArray = cloneDeep(state.data);
      let filteredTask = tasksListArray.find(t => {
        return t.taskId == action.payload.taskId;
      });
      filteredTask.checkList = filteredTask.checkList.map(c => {
        if (c.checkListId == action.payload.data.checklist.checkListId) {
          return action.payload.data.checklist;
        } else {
          return c;
        }
      });
      filteredTask.progress = action.payload.data.progress;
      filteredTask.status = action.payload.data.status;

      let newArr = tasksListArray.map(t => {
        if (t.taskId == action.payload.taskId) {
          return filteredTask;
        } else {
          return t;
        }
      });

      return {
        ...state,
        data: newArr,
      };
    }
      break;

    case constants.UPDATETASKSROWS: {
      let updatedRows = action.payload;
      return {
        ...state,
        data: updatedRows,
      };
    }
      break;

    case constants.UPDATETASK: {
      let updatedTasks = state.data.map(x => {
        if (x.taskId === action.payload.taskId) {
          return { ...x, ...action.payload };
        } else {
          return x;
        }
      });
      return {
        ...state,
        data: updatedTasks,
      };
    }
      break;

    case constants.UPDATETASKSINSTORE:
      {
        // const tasksArray = cloneDeep(state.data);
        let payload = action.payload.data;
        const tasksArray = state.data.map(ele => {
          let t = payload.find(e => e.taskId == ele.taskId) || false;
          if (t) {
            const data = {
              ...ele,
              status: t.status,
              statusColor: t.statusColor,
              statusTitle: t.statusName,
              projectId: t.projectId,
            };
            return data;
          } else {
            return ele;
          }
        });

        setTimeout(() => {
          if (grid.grid) grid.grid.redrawRows(tasksArray);
        }, 0);

        return {
          ...state,
          data: tasksArray,
        };
      }
      break;
    case constants.DELETETASKSINSTORE: {
      let taskIds = action.payload.data.taskIds;
      let updatedTaskArr = state.data.reduce((res, cv) => {
        let isExistTask = taskIds.find(taskId => taskId === cv.taskId);
        if (!isExistTask) {
          res.push(cv);
        }
        return res;
      }, []);
      return {
        ...state,
        data: updatedTaskArr,
      };
    }
      break;

    case constants.BULK_ADD_TASK: {
      return {
        ...state,
        data: [...state.data, ...action.payload],
      };
    }
      break;

    case constants.UPDATETASKCUSTOMFIELDDATA: {
      let payload = action.payload;
      let allTasks = cloneDeep(state.data);
      if (payload.type === "Create") {
        allTasks = allTasks.map(r => {
          if (r.taskId === payload.obj.groupId) {
            return {
              ...r,
              customFieldData: r.customFieldData
                ? [...r.customFieldData, payload.obj]
                : [payload.obj],
            };
          }
          return r;
        });
      }
      if (payload.type === "Update") {
        allTasks = allTasks.map(r => {
          if (r.taskId === payload.obj.groupId) {
            let selectedTaskData = cloneDeep(r.customFieldData);
            selectedTaskData = selectedTaskData.map(sd => {
              if (sd.fieldId === payload.obj.fieldId) return payload.obj;
              return sd;
            });
            return { ...r, customFieldData: selectedTaskData };
          }
          return r;
        });
      }
      return {
        ...state,
        data: allTasks,
      };
    }
      break;

    default:
      return state;
  }
};

export default {
  tasks,
};
