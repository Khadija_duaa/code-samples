import consts from "../constants/types";

export default function (state = {}, action) {

  switch (action.type) {
    case consts.SHOWHIDEPROMOBAR: {
      return { promoBar: action.payload.data };
    }
      break;

    default:
      return state;
  }
}
