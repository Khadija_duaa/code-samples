import constants from "./../constants/types";
const issueActivities = (state = {}, action) => {
    switch (action.type) {
        case constants.POPULATEISSUECOMMENTSACTIVITIES:
            return {
                ...state,
                data: action.payload.data
            };
            break;

        default:
            return state;
    }
};

export default {
    issueActivities
};

