import consts from "../constants/types";

export default function (state = {}, action) {

  switch (action.type) {

    case consts.GETWORKSPACESTATS:
      return { data: [...state.data, ...action.payload] };
      break;

    case consts.WORKSPACEREPORTCOLUMNORDER:
      return { ...state, columnOrder: action.payload };
      break;

    case consts.UPDATECOLUMNORDER:
      let colOrderCopy = JSON.parse(JSON.stringify(state.columnOrder))
      colOrderCopy.reportColumnOrder = action.payload
      return { ...state, columnOrder: colOrderCopy };
      break;

    default:
      return state;
  }
}
