import constants from "./../constants/types";
const activityLogs = (state = {}, action) => {
    switch (action.type) {
        case constants.POPULATEWORKSPACEACTIVITY:
        if (action.payload.pageNo === 1) {
            return {
                ...state,
                data:action.payload.data,
            };
          }else{
            return {
                ...state,
                data:[...state.data,...action.payload.data],
            };
          }
        break;
           
        default:
            return state;
    }
};

export default {
    activityLogs
};

