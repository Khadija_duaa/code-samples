import constants from "./../constants/types";
const teamMembers = (state = {}, action) => {
  switch (action.type) {
    case constants.POPULATEUMEMBERSINFO:
      return {
        ...state,
        ...action.payload.data,
      };
      break;

    case constants.POPULATEALLMEMBERSINFO:
      return {
        ...state,
        allTeamMembers: action.payload.data,
      };
      break;

    case constants.RESETMEMBERS:
      return {
        ...state,
        teamMembers: ""
      };
      break;

    case constants.RESETALLMEMBERS:
      return {
        ...state,
        allTeamMembers: ""
      };
      break;

    default:
      return state;
  }
};

export default {
  teamMembers
};

