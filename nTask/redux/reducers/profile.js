import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import findIndex from "lodash/findIndex";

const profile = (state = {}, action) => {
  switch (action.type) {
    case constants.CRUDTEAMMEMBER:
      {
        const stateCopy = cloneDeep(state.data); // Cloning whole state object
        const teamMemberArr = stateCopy.teamMember; // Team members array
        switch (action.operationType) {
          case "create":
            {
              const newMemberList = [...teamMemberArr, ...action.payload]; //Merging array of invited team members into existing team members
              stateCopy.teamMember = newMemberList; // update copy of state with new team members list
            }
            break;
          case "update":
            {
              const updatedMember = action.payload;
              const newMemberList = teamMemberArr.map((m) => {
                return m.userId == updatedMember.userId ? updatedMember : m;
              });
              stateCopy.teamMember = newMemberList;
            }
            break;
          case "delete":
            {
              const updatedMemberId = action.payload;

              const newMemberList = teamMemberArr.map((m) => {
                if (m.userId == updatedMemberId) {
                  return { ...m, isDeleted: true };
                } else {
                  return m;
                }
              });
              stateCopy.teamMember = newMemberList;
            }
            break;
        }
        return { data: stateCopy };
      }
    case constants.CRUDWORKSPACEMEMBER:
      {
        const stateCopy = cloneDeep(state.data); // Cloning whole state object
        const workspaceMemberArr = stateCopy.member.allMembers; // Team members array
        switch (action.operationType) {
          case "create":
            {
              const newMemberList = [...workspaceMemberArr, ...action.payload]; //Merging array of invited team members into existing team members
              stateCopy.member.allMembers = newMemberList; // update copy of state with new team members list
            }
            break;
          case "update":
            {
              let updatedMember = action.payload;
              let newMemberList = workspaceMemberArr.map((m) => {
                if (m.userId == updatedMember.userId) {
                  return updatedMember;
                } else return m;
              });
              stateCopy.member.allMembers = newMemberList;
            }
            break;
          case "delete":
            {
              const updatedMemberId = action.payload;
              const newMemberList = workspaceMemberArr.map((m) => {
                if (m.userId == updatedMemberId) {
                  return { ...m, isDeleted: true };
                } else {
                  return m;
                }
              });
              stateCopy.member.allMembers = newMemberList;
            }
            break;
        }
        return { data: stateCopy };
      }
    case constants.WORKSPACEMEMBERCRUD:
      {
        const stateCopy = cloneDeep(state.data); // Cloning whole state object
        const id = action.workspaceId; // Team members array
        stateCopy.workspace = stateCopy.workspace.map((ele) => {
          if (ele.teamId == id) {
            switch (action.operationType) {
              case "create":
                ele.members = [...ele.members, ...action.payload];
                break;
              case "delete":
                ele.members = ele.members.filter((e) => e !== action.payload);
                break;
            }
            return ele;
          } else return ele;
        });

        return { data: stateCopy };
      }
    case constants.STARTTRIALPLANBILLING:
    case constants.UPGRADEPREMIUMBILLPLAN:
    case constants.UPGRADEBUSINESSBILLPLAN:
    case constants.DOWNGRADEFREEBILLPLAN:
    case constants.UPDATESUBSCRIPTION:
    case constants.CANCELSUBSCRIPTION:
      let _state = cloneDeep(state.data);
      for (let i = 0; i < _state.teams.length; i++) {
        let item = _state.teams[i];
        if (item.companyId == action.payload.data.companyInfo.companyId) {
          _state.teams[i] = action.payload.data.companyInfo;
          break;
        }
      }
      return {
        ...state,
        data: _state,
      };

    // case  constants.DOWNGRADEFREEBILLPLAN:
    //   _state = cloneDeep(state.data);
    //   _state.userPlan = action.payload.data.userPlan;
    //   return {
    //     ...state,
    //     data: _state
    //   };
    case constants.UPDATEPROMOCODE:
      let _promoState = cloneDeep(state.data);
      _promoState.promocode = action.payload.data;
      return {
        ...state,
        data: _promoState,
      };

    case constants.ADDTEAMTOTEAMLIST:
      return {
        data: { ...state.data, teams: [...state.data.teams, action.payload] },
      };

    case constants.MINUSTEAMCOUNT:
      return {
        data: {
          ...state.data,
          teamInvitationCount: state.data.teamInvitationCount - action.payload,
        },
      };

    case constants.ADDTEAMCOUNT:
      return {
        data: {
          ...state.data,
          teamInvitationCount: state.data.teamInvitationCount + action.payload,
        },
      };

    case constants.ADDPENDINGINVITE:
      const previousInvites = state.data.pendingInvites ? state.data.pendingInvites : [];
      return {
        data: {
          ...state.data,
          pendingInvites: [...previousInvites, action.payload],
        },
      };

    case constants.REMOVETEAMINVITATION:
      let invites = state.data.pendingInvites || [];
      return {
        data: {
          ...state.data,
          pendingInvites: invites.filter((o, i) => i > 0),
        },
      };

    case constants.POPULATEFACEBOOK:
      return {
        ...state,
        facebook: action.payload.data,
      };

    case constants.UPDATEUSERTEAM:
      {
        let stateCopy = cloneDeep(state);
        let updatedTeams = stateCopy.data.teams.map((t) => {
          if (t.companyId == action.payload.companyId) {
            return action.payload;
          } else {
            return t;
          }
        });
        return { data: { ...stateCopy.data, teams: updatedTeams } };
      }

    case constants.POPULATEGOOGLEPLUS:
      return {
        ...state,
        googleplus: action.payload.data,
      };
    case constants.POPULATELINKEDIN:
      return {
        ...state,
        linkedin: action.payload.data,
      };

    case constants.POPULATEUSERINFO:
      return {
        data: {
          ...state.data,
          ...action.payload.data,
        },
      };

    case constants.UPDATELOCALIZATION:
      {
        let _state = cloneDeep(state.data);
        _state.profile.timeZone = action.payload.data.timeZone;
        _state.profile.startDayOfWeek = action.payload.data.startDayOfWeek;
        _state.profile.language = action.payload.data.language;
        return {
          ...state,
          data: _state,
        };
      }

    case constants.UPDATEUSERPROFILE:
      {
        let _state = cloneDeep(state.data);
        _state.fullName = action.payload.data.fullName;
        _state.profile.fullName = action.payload.data.fullName;
        _state.profile.country = action.payload.data.country;
        _state.profile.phoneNumber = action.payload.data.phoneNumber;
        _state.profile.industry = action.payload.data.industry;
        _state.profile.industryDetails = action.payload.data.industryDetails;
        _state.profile.jobTitle = action.payload.data.jobTitle;
        _state.profile.jobTitleDetails = action.payload.data.jobTitleDetails;
        _state.profile.pictureUrl = action.payload.data.pictureUrl;
        _state.profile.autoLogoutTime = action.payload.data.autoLogoutTime;
        _state.profile.autoLogoutEnabled = action.payload.data.autoLogoutEnabled;
        return {
          ...state,
          data: _state,
        };
      }

    case constants.UPDATESLACKINFO:
      {
        let _state = cloneDeep(state.data);
        _state.isSlackLinked = action.payload.data.isSlackLinked ? true : false;
        return {
          ...state,
          data: _state,
        };
      }

    case constants.UPDATEZOOMINFO:
      {
        let _state = cloneDeep(state.data);
        _state.isZoomLinked = action.payload.data.isZoomLinked ? true : false;
        _state.openProfileSetting = true;
        return {
          ...state,
          data: _state,
        };
      }

    case constants.UPDATETEAMINFO:
      {
        let _state = cloneDeep(state.data);
        _state.isTeamLinked = action.payload.data.isTeamLinked;
        _state.allowedOnlineMeetingProviders = action.payload.data.allowedOnlineMeetingProviders;
        _state.openProfileSetting = true;
        return {
          ...state,
          data: _state,
        };
      }

    case constants.CLOSE_PROFILE_SETTING_DIALOG:
      {
        let _state = cloneDeep(state.data);
        _state.openProfileSetting = false;
        return {
          ...state,
          data: _state,
        };
      }

    case constants.UPDATEIMAGEUSERPROFILE:
      {
        let _state = cloneDeep(state.data);
        _state.profile.pictureUrl = action.payload.data;
        return {
          ...state,
          data: _state,
        };
      }

    case constants.ADDTEAM:
      {
        let _state = cloneDeep(state);
        _state.data.workspace.push(action.payload.team);
        return _state;
      }

    case constants.UPDATETEAM:
      {
        let _state = cloneDeep(state);
        const teamIndex = findIndex(_state.data.workspace || [], {
          teamId: action.payload.team.teamId,
        });
        if (teamIndex > -1) _state.data.workspace[teamIndex] = action.payload.team;
        return _state;
      }

    case constants.DELETETEAM:
      {
        let _state = cloneDeep(state);
        const teamIndex = findIndex(_state.data.workspace || [], {
          teamId: action.payload.teamId,
        });
        if (teamIndex > -1) _state.data.workspace.splice(teamIndex, 1);
        return _state;
      }

    case constants.UPDATETEAMS:
      {
        let _state = cloneDeep(state.data);
        let updatedWorkspaces = _state.workspace.map((t) => {
          if (t.teamId == action.payload.data.teamId) {
            return action.payload.data;
          } else {
            return t;
          }
        });
        _state.workspace = updatedWorkspaces;
        return {
          ...state,
          data: _state,
        };
      }

    case constants.REMOVETEAM:
      {
        let _state = cloneDeep(state.data);
        _state.teams = action.payload.data.workspace;
        _state.member = action.payload.data.members;
        return {
          ...state,
          data: _state,
        };
      }

    case constants.CHANGEUSERROLE:
      {
        let _state = cloneDeep(state.data);
        let workspaceMember = _state.member.allMembers;
        let teamMember = _state.teamMember;
        let newMemberObj = action.payload.data[0];
        if (action.payload.data) {
          if (action.payload.isInvited && action.payload.data.length) {
            let teamMemberExist = teamMember.find((m) => {
              return m.userId == newMemberObj.userId;
            });
            if (teamMemberExist) {
              let newTeamMemberArr = teamMember.map((m) => {
                return m.userId == newMemberObj.userId ? newMemberObj : m;
              });

              teamMember = newTeamMemberArr;
            } else {
              teamMember = [...action.payload.data, ...teamMember];
            }
            _state.teamMember = teamMember;
          } else {
            workspaceMember = workspaceMember.map((x) => {
              if (x.userId == action.payload.data.userId && !x.isDeleted) {
                return action.payload.data;
              }
              return x;
            });
            _state.member.allMembers = workspaceMember;
          }
        }

        return {
          ...state,
          data: _state,
        };
      }

    case constants.CHANGEUSERROLEINTEAM:
      {
        let _state = cloneDeep(state.data);
        let teamMember = _state.teamMember;
        let workspaceMember = _state.member.allMembers;
        if (action.payload.data.teamMember) {
          /** updating object in team members array / team users */
          teamMember = teamMember.map((x) => {
            if (x.userId === action.payload.data.teamMember.userId) {
              return action.payload.data.teamMember;
            }
            return x;
          });

          _state.teamMember = teamMember;
        }
        if (action.payload.data.workspaceMember) {
          /** updating object in all members array / workspace users */
          workspaceMember = workspaceMember.map((x) => {
            if (x.userId === action.payload.data.workspaceMember.userId) {
              return action.payload.data.workspaceMember;
            }
            return x;
          });
          _state.member.allMembers = workspaceMember;
        }

        return {
          ...state,
          data: _state,
        };
      }

    case constants.UPDATEINVITATIONMEMBERS:
      {
        let userInfoState = cloneDeep(state.data);
        let members = userInfoState.member.allMembers;
        let newMemberObj = action.payload.data[0] || {};

        let workspaceMemberExist = members.find((m) => {
          return m.userId == newMemberObj.userId;
        });
        if (workspaceMemberExist) {
          let newWorkspaceMemberArr = members.map((m) => {
            return m.userId == newMemberObj.userId ? newMemberObj : m;
          });

          members = newWorkspaceMemberArr;
        } else {
          members = [...members, ...action.payload.data];
        }

        userInfoState.member.allMembers = members;

        return {
          ...state,
          data: userInfoState,
        };
      }

    case constants.UPDATETEAMMEMBER:
      {
        let userInfo = cloneDeep(state.data);
        let membersObj = state.data.teamMember.map((m) => {
          if (m.userId == action.payload.userId) {
            return action.payload;
          } else {
            return m;
          }
        });
        return {
          data: { ...state.data, teamMember: membersObj },
        };
      }

    case constants.UPDATEINVITEHISTORY:
      {
        let userInfo = cloneDeep(state.data);
        let membersObj = state.data.teamMember.map((m) => {
          if (m.userId == action.payload.data.memberId) {
            m.inviteHistory = action.payload.data.inviteHistory;
            return m;
          } else {
            return m;
          }
        });
        return {
          data: { ...state.data, teamMember: membersObj },
        };
      }

    case constants.REMOVETEAMMEMBER:
      {
        let membersObj = state.data.teamMember.filter((m) => {
          return m.userId !== action.payload;
        });
        let workspaceMembersObj = state.data.member.allMembers.filter((m) => {
          return m.userId !== action.payload;
        });
        return {
          data: {
            ...state.data,
            teamMember: membersObj,
            member: { allMembers: workspaceMembersObj },
          },
        };
      }

    case constants.UPDATEMEMBER:
      {
        let userInfo = cloneDeep(state.data);
        let allMembers = userInfo.member.allMembers;
        const { userId } = action.payload.data;
        const actionType = action.payload.actionType;

        switch (actionType) {
          case "insert":
            {
              userInfo.member.allMembers.push(action.payload.data);
            }
            break;
          case "update":
            {
              const userIndex = findIndex(allMembers || [], {
                userId: userId,
                isDeleted: false,
              });
              if (userIndex > -1) userInfo.member.allMembers[userIndex] = action.payload.data;
            }
            break;
          case "updatePartially":
            {
              // action.payload.data can contain limited fields instead of complete object
              const userIndex = findIndex(allMembers || [], { userId: userId });
              if (userIndex > -1) {
                const member = {
                  ...userInfo.member.allMembers[userIndex],
                  ...action.payload.data,
                };

                userInfo.member.allMembers[userIndex] = member;
              }
            }
            break;
          case "delete": {
            const userIndex = findIndex(allMembers || [], { userId: userId });
            if (userIndex > -1) userInfo.member.allMembers.splice(userIndex, 1);
          }
        }

        return {
          ...state,
          data: userInfo,
        };
      }

    case constants.UPDATEUSERISONLINE:
      let prevState = cloneDeep(state);

      if (prevState.data.member && prevState.data.member.allMembers) {
        prevState.data.member.allMembers = prevState.data.member.allMembers.map((member) => {
          if (member.userId === action.payload.userId) {
            return { ...member, isOnline: action.payload.isOnline };
          }
          return member;
        });
      }
      return prevState;

    case constants.REMOVEWORKSPACEMEMBER:
      const profileData = cloneDeep(state.data);
      const updatedMembers = profileData.member.allMembers.forEach((m) => {
        if (action.payload == m.userId) {
          m.isDeleted = true;
        }
      });
      return { data: profileData };

    case constants.DELETEPROFILE:
      return {
        ...state,
        data: "",
        facebook: "",
        googleplus: "",
        linkedin: "",
      };

    case constants.UPDATEWORKSPACEIMAGE:
      {
        const teamId = action.payload.teamId;
        let data = action.payload.data;
        let prevState = cloneDeep(state);
        const teamIndex = findIndex(prevState.data.workspace || [], {
          teamId: teamId,
        });
        if (data) {
          prevState.data.workspace[teamIndex].imageBasePath = data.imageBasePath;
          prevState.data.workspace[teamIndex].pictureUrl = data.fileName;
          prevState.data.workspace[teamIndex].pictureThumbnail_40X40 = data.pictureThumbnail_40X40;
          prevState.data.workspace[teamIndex].pictureThumbnail_70X70 = data.pictureThumbnail_70X70;
        } else {
          prevState.data.workspace[teamIndex].pictureUrl = data;
          prevState.data.workspace[teamIndex].pictureThumbnail_40X40 = data;
          prevState.data.workspace[teamIndex].pictureThumbnail_70X70 = data;
        }
        return {
          ...state,
          ...prevState,
        };
      }

    case constants.UPDATETEAMIMAGE:
      {
        const teamId = action.payload.teamId;
        let data = action.payload.data;
        let prevState = cloneDeep(state);
        const teamIndex = findIndex(prevState.data.teams || [], {
          companyId: teamId,
        });
        prevState.data.teams[teamIndex] = data;
        return {
          ...state,
          ...prevState,
        };
      }

    case constants.UPDATETEAMAVATAR:
      {
        const teamId = action.payload.teamId;
        let data = action.payload.data;
        let newTeamsObj = state.data.teams.map((t) => {
          return t.companyId == teamId ? action.payload.data : t;
        });
        return {
          data: { ...state.data, teams: newTeamsObj },
        };
      }

    case constants.UPDATEUSERPROFILEIMAGE:
      {
        const { imageUrl } = action.payload;
        let prevState = cloneDeep(state);
        prevState.data.member.allMembers.forEach((member) => {
          if (member.userId === state.data.userId) member.imageUrl = imageUrl;
        });
        prevState.data.imageUrl = imageUrl;
        return prevState;
      }

    case constants.UPDATEEMAILADDRESS:
      let userInfo = cloneDeep(state.data);
      userInfo.changeEmailRequest = action.payload.data;
      return {
        ...state,
        data: userInfo,
      };

    case constants.CANCELEMAILREQUEST:
      let updatedEmailInfo = cloneDeep(state.data);
      updatedEmailInfo.changeEmailRequest.isConfirm = true;
      return {
        ...state,
        data: updatedEmailInfo,
      };

    case constants.UPDATESORTINGORDER:
      const stateData = cloneDeep(state.data);
      let teamID = action.payload.data.teamId;
      let sortColumnData = action.payload.data.sortColumn;
      let updatedSortedWorkspaces = stateData.workspace.map((w) => {
        if (w.teamId == teamID) {
          w.sortColumn = sortColumnData;
          return w;
        } else return w;
      });
      stateData.workspace = updatedSortedWorkspaces;
      return {
        ...state,
        data: stateData,
      };

    case constants.UPDATECONFIG: {
      const stateData = cloneDeep(state.data);
      stateData.teams = stateData.teams.map((item) => {
        if (item.companyId === action.payload.companyId) {
          return {
            ...item,
            config: {
              ...item.config,
              // isProjectFieldMandatory:  action.payload.IsProjectFieldMandatory
              ...action.payload,
            },
          };
        }
        return item;
      });
      stateData.workspace = stateData.workspace.map((ws) => {
        return {
          ...ws,
          config: {
            ...ws.config,
            // isProjectFieldMandatory: action.payload.IsProjectFieldMandatory
            ...action.payload,
          },
        };
      });
      return {
        ...state,
        data: stateData,
      };
    }
    case constants.UPDATELTDUPGRADEREQUEST:
      {
        const stateData = cloneDeep(state.data);
        stateData.teams = stateData.teams.map((item) => {
          if (item.companyId === action.payload.companyId) {
            return {
              ...item,
              ...action.payload,
            };
          }
          return item;
        });
        return {
          ...state,
          data: stateData,
        };
      }
    // update weekends for team level
    case constants.UPDATEWEEKENDS:
      {
        const stateData = cloneDeep(state.data);
        stateData.teams = stateData.teams.map((item) => {
          if (item.companyId === action.payload.companyId) {
            return {
              ...item,
              ...action.payload,
            };
          }
          return item;
        });
        return {
          ...state,
          data: stateData,
        };
      }

    case constants.UPDATECONFIGWORKSPACE:
      {
        const stateData = cloneDeep(state.data);
        stateData.workspace = stateData.workspace.map((item) => {
          if (item.teamId === action.payload.teamId) {
            return {
              ...item,
              config: {
                ...item.config,
                // isProjectFieldMandatory:  action.payload.IsProjectFieldMandatory,
                // taskProgressCalculation:  action.payload.taskProgressCalculation
                ...action.payload,
              },
            };
          }
          return item;
        });
        return {
          ...state,
          data: stateData,
        };
      }

    case constants.UPDATECURRENTWORKSPACE:
      {
        const stateData = cloneDeep(state.data);
        const workspace = stateData.workspace.map((item) => item.teamId === stateData.loggedInTeam ? action.payload : item);

        return {
          ...state,
          data: { ...stateData, workspace: workspace },
        };
      }

    case constants.UPDATECURRENTWORKSPACECALENDAR:
      {
        const stateData = cloneDeep(state.data);
        const workspace = stateData.workspace.map((item) => item.teamId === stateData.loggedInTeam ? { ...item, calendar: action.payload } : item);


        return {
          ...state,
          data: { ...stateData, workspace: workspace },
        };
      }

    //T0 update progress enabling
    case constants.UPDATEPROGRESS:
      {
        const stateData = cloneDeep(state.data);
        stateData.workspace = stateData.workspace.map((item) => {
          if (item.teamId === action.payload.teamId) {
            return {
              ...item,
              ...action.payload,
            };
          }
          return item;
        });
        return {
          ...state,
          data: stateData,
        };
      }

    default:
      return state;
  }
};

export default {
  profile: profile,
};
