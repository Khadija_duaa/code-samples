import constants from "../constants/types";

const whiteLabelInfo = (state = {}, action) => {
    switch (action.type) {
        case constants.POPULATEWHITELABELINFO:
            return {
                ...state,
                data: action.payload.data,
            };
            break;

        default:
            return state;
    }
};

export default {
    whiteLabelInfo
};

