import constants from "../constants/types";

const initialState = {
  data: {
    activeRoomId: "",
    groupChat: [],
    chatItems: [],
    typingStatus: {
      typing: false,
      userIds: [],
    },
  },
};

const collab = (state = initialState, action) => {
  switch (action.type) {
    case constants.POPULATECHAT:
      return {
        ...state,
        groupChat: [action.payload, ...state]
      };
      break;
    case constants.NEWGROUPITEM:
      return {
        ...state,
        group: action.payload
      };
      break;
    case constants.RESETREDUX:
      return {
        ...state,
        group: {}
      };
      break;
      case constants.NEWGROUPITEM:
        return {
          ...state,
          group: action.payload 
        };
        break;
        case constants.RESETREDUX:
          return {
            ...state,
            group: {} 
          };
          break;
    default:
      return state;
  }
};


export default collab;