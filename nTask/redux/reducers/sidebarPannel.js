import constants from "../constants/types";

const sidebarPannel = (state = {}, action) => {
  switch (action.type) {
    case constants.UPDATESIDEBARSTATE:
      return { ...state, ...action.payload };
      break;
      case constants.SHOW_HIDE_SIDEBAR:
      return { ...state, oldSidebar: action.payload };
      break;

    case constants.UPDATESIDEBARORDER:
      return { ...state, data: { ...state.data, ...action.payload } };
      break;

    default:
      return { ...state };
  }
};

export default {
  sidebarPannel,
};
