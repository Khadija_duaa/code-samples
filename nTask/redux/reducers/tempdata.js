import constants from "./../constants/types";
const tempData = (state = {}, action) => {
  switch (action.type) {
    case constants.POPULATETEMPDATA:
      // 
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.UPDATETETEMPDATA:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.DELETETETEMPDATA:
      return {
        ...state,
        data: ""
      };
      break;

    default:
      return state;
  }
};

export default {
  tempData: tempData
};
