import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import searchQuery from "../../components/CustomTable2/ColumnSettingDropdown/searchQuery";

const reportingFilter = (state = {}, action) => {
    switch (action.type) {

        case constants.UPDATE_REPORTING_FILTER: {
            const key = action.key;
            const cloneState = state[key] ? cloneDeep(state[key]) : state;
            let updateFilters;
            updateFilters = { [key]: { ...cloneState, ...action.payload } };
            searchQuery[key] = updateFilters[key];
            return { ...state, ...updateFilters };
        }
            break;


        case constants.CLEAR_REPORTING_FILTER:
            searchQuery[action.payload] = {};
            return {};
            break;
        case constants.DELETE_REPORTING_FILTER: {
            const key = action.key;
            const stateCopy = cloneDeep(state[key]);
            // stateCopy[action.payload].selectedValues = [];
            // stateCopy[action.payload].type = "";
            delete stateCopy[action.payload];
            const updatedFilterState = { ...state, [key]: stateCopy };
            searchQuery[key] = updatedFilterState[key];
            return updatedFilterState;
        }
            break;
        case constants.DELETE_ALL_REPORTING_FILTER: {
            const stateCopy = state;
            if (stateCopy) {
                searchQuery[action.payload] = {};
                return {};
            }
        }
            break;
        default:
            return state;
    }
};

export default {
    reportingFilter,
};
