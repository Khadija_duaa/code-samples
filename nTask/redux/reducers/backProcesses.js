import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";
import isUndefined from "lodash/isUndefined";
import constants from "./../constants/types";
import { v4 as uuidv4 } from "uuid";

const backProcesses = (state = {}, action) => {
  switch (action.type) {
    // add new pending process
    case constants.ADDPENDINGPROCESS: {
      const finalstate = cloneDeep(state);
      const currentAction = {
        ...action.payload.data,
        isNew: true,
        isDone: false,
        isFail: false,
        processId: uuidv4(),
      }
      finalstate['export'].unshift(currentAction);
      return { ...finalstate };
    }
      break;
    // update pending process
    case constants.UPDATEPENDINGPROCESS: {
      const actionType = action.payload.type;
      const updateAction = action.payload.toUpdate;
      const updatedActionsArr = state[actionType].map(item => {
        if (item.processId == action.payload.data) {
          return { ...item, ...updateAction }
        } else {
          return { ...item }
        }
      });
      return { ...state, [actionType]: updatedActionsArr };
    }
      break;
    // remove pending process
    case constants.REMOVEPENDINGPROCESS: {
      const tempArray = state[action.payload.data].filter(p => !p.isDone && !p.isFail);
      return { ...state, [action.payload.data]: tempArray };
    }
      break;

    case constants.REMOVEPROCESSBYID: {
      const tempArray = state[action.payload.type].filter(p => p.processId != action.payload.data);
      return { ...state, [action.payload.type]: tempArray };
    }
      break;

    default:
      return state;
  }
};

export default {
  backProcesses,
};
