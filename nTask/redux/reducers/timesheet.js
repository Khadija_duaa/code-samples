import findIndex from "lodash/findIndex";
import cloneDeep from "lodash/cloneDeep";
import consts from "./../constants/types";

const timesheet = (state = {}, action) => {
  switch (action.type) {
    case consts.TIMESHEETGETWEEKDURATIONS:
      {
        const timeEntry = {
          timeEntry: { ...action.payload },
        };
        return { ...state, ...timeEntry };
      }
      break;

    case consts.REMOVETASK:
      {
        const projectId = action.payload.projectId;
        const taskId = action.payload.taskId;
        const data = cloneDeep(state);
        const projectIndex = findIndex(data["timeEntry"]["projectTimesheetVM"], {
          projectId: projectId,
        });
        const taskIndex = findIndex(
          data["timeEntry"]["projectTimesheetVM"][projectIndex]["taskEffortVM"],
          { taskId: taskId }
        );
        data["timeEntry"]["projectTimesheetVM"][projectIndex]["taskEffortVM"].splice(taskIndex, 1);
        if (data["timeEntry"]["projectTimesheetVM"][projectIndex]["taskEffortVM"].length === 0)
          data["timeEntry"]["projectTimesheetVM"].splice(projectIndex, 1);
        return { ...data };
      }
      break;

    case consts.ADDNEWTASKINTIMESHEET:
      {
        const s = action.payload.data["status"];
        const d = action.payload.data["projectTimesheetVM"];
        let pi = null;
        if (d["projectId"] !== null) {
          pi = findIndex(state["timeEntry"]["projectTimesheetVM"], {
            projectId: d["projectId"],
          });
        } else {
          pi = findIndex(state["timeEntry"]["projectTimesheetVM"], {
            workspaceId: d["workspaceId"],
            projectId: d["projectId"],
          });
        }
        const na3 = { ...state };
        if (pi !== -1) {
          const ti = findIndex(state["timeEntry"]["projectTimesheetVM"][pi]["taskEffortVM"], {
            taskId: d["taskEffortVM"][0]["taskId"],
          });
          if (ti !== -1) {
            na3["timeEntry"]["projectTimesheetVM"][pi]["taskEffortVM"][ti] = d["taskEffortVM"][0];
          } else {
            na3["timeEntry"]["projectTimesheetVM"][pi]["taskEffortVM"].push(d["taskEffortVM"][0]);
          }
        } else {
          na3["timeEntry"]["projectTimesheetVM"].push(d);
        }
        // Changing status of Project if Timesheet is Approved
        // if (s && pi > -1) na3["timeEntry"]["projectTimesheetVM"][pi]["status"] = "In Progress";
        return { ...state, ...na3 };
      }
      break;

    case consts.TIMESHEETADDTASKDURATION:
      {
        const pId = action.payload.projectId;
        const wId = action.payload.workspaceId;
        const tId = action.payload.taskId;
        const dur = action.payload.duration;
        const selectedDay = action.payload.day;
        let pIndex = null;
        if (pId) {
          pIndex = findIndex(state["timeEntry"]["projectTimesheetVM"], {
            projectId: pId,
          });
        } else {
          pIndex = findIndex(state["timeEntry"]["projectTimesheetVM"], {
            workspaceId: wId,
          });
        }
        const tIndex = findIndex(state["timeEntry"]["projectTimesheetVM"][pIndex]["taskEffortVM"], {
          taskId: tId,
        });
        const fIndex = findIndex(
          state["timeEntry"]["projectTimesheetVM"][pIndex]["taskEffortVM"][tIndex]["timestamp"],
          { date: selectedDay }
        );
        const newArr = { ...state };
        newArr["timeEntry"]["projectTimesheetVM"][pIndex]["taskEffortVM"][tIndex]["timestamp"][
          fIndex
        ].duration = dur;
        return { ...state, ...newArr };
      }
      break;

    case consts.SUBMITTIMESHEET:
      {
        const projectId = action.payload.projectId;
        const { data } = action.payload;
        let projectIndex = -1;
        if (projectId)
          projectIndex = findIndex(state["timeEntry"]["projectTimesheetVM"], {
            projectId: projectId,
          });
        else
          projectIndex = findIndex(state["timeEntry"]["projectTimesheetVM"], {
            projectId: null,
          });
        const newState = { ...state };
        newState["timeEntry"]["projectTimesheetVM"][projectIndex]["status"] = data["status"];
        newState["timeEntry"]["projectTimesheetVM"][projectIndex]["rejectReason"] =
          data["rejectReasons"];
        return { ...state, ...newState };
      }
      break;

    case consts.PENDINGTIMESHEETGETWEEKDURATIONS:
      {
        const pendingApproved = {
          pendingApproved: { ...action.payload },
        };
        return { ...state, ...pendingApproved };
      }
      break;

    case consts.REJECTPENDINGTIMESHEET:
      {
        const projectId = action.payload.data.projectId;
        const d = action.payload.data;
        const newArray2 = { ...state };

        let userIndex = findIndex(state["pendingApproved"]["userSubmittedTimesheet"], {
          submitUser: d.createdBy,
        });
        const ts =
          state["pendingApproved"]["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"];
        let pind;
        if (projectId)
          pind = findIndex(ts, t => {
            return t.workspaceId
              ? t.workspaceId == d.teamId && t.projectId == projectId
              : t.projectId == projectId;
          });
        else
          pind = findIndex(ts, t => {
            if (t.workspaceId) {
              return t.workspaceId == d.teamId && !t.projectId;
            } else {
              return !t.projectId;
            }
          });
        newArray2["pendingApproved"]["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"][
          pind
        ]["status"] = d["status"];
        newArray2["pendingApproved"]["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"][
          pind
        ]["rejectReason"] = d["rejectReasons"];
        return { ...state, ...newArray2 };
      }
      break;

    case consts.UPDATE_TOTAL_TIME_TITLE:
      {
        const stateCopy = cloneDeep(state);
        const updatedPendingTimesheets = {
          ...stateCopy.pendingApproved,
          lableTimeLogged: action.payload.LabelLoggedHours,
        };
        return { ...stateCopy, pendingApproved: updatedPendingTimesheets };
      }
      break;

    case consts.APPROVEPENDINGTIMESHEET:
      {
        const projectId = action.payload.data.projectId;
        const data = action.payload.data;
        const newArr2 = { ...state };

        let userIndex = findIndex(state["pendingApproved"]["userSubmittedTimesheet"], {
          submitUser: data.createdBy,
        });
        const ts =
          state["pendingApproved"]["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"];
        let projectIndex;
        if (projectId)
          projectIndex = findIndex(ts, t => {
            return t.workspaceId
              ? t.workspaceId == data.teamId && t.projectId == projectId
              : t.projectId == projectId;
          });
        else
          projectIndex = findIndex(ts, t => {
            if (t.workspaceId) {
              return t.workspaceId == data.teamId && !t.projectId;
            } else {
              return !t.projectId;
            }
          });
        newArr2["pendingApproved"]["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"][
          projectIndex
        ]["status"] = data["status"];
        newArr2["pendingApproved"]["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"][
          projectIndex
        ]["rejectReason"] = data["rejectReasons"];
        return { ...state, ...newArr2 };
      }
      break;

    default:
      return state;
  }
};

export default {
  timesheet,
};
