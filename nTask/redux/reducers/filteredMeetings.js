import constants from "./../constants/types";
const filteredMeetingsData = (state = {}, action) => {

  switch (action.type) {
    case constants.POPULATEFILTEREDMEETINGS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.UPDATEFILTEREDMEETINGS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.DELETEFILTEREDMEETINGS:
      return {
        ...state,
        data: ""
      };
      break;

    default:
      return state;
  }
};

export default {
  filteredMeetingsData: filteredMeetingsData
};
