import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";

const googleCalendar = (state = {}, action) => {
  switch (action.type) {
    case constants.UPDATEGOOGLECALENDARINFO: {
      let stateData = cloneDeep(state.data);
      stateData.accountsAndCalendar = action.payload.data;
      stateData.googleIntegration = true;

      return {
        ...state,
        data: stateData,
      };
    }
      break;
    case constants.UPDATESYNCCALENDARINFO: {
      let oldState = cloneDeep(state.data);
      let paylod = action.payload.data;
      oldState.projects = paylod.projects;
      oldState.workspaces = paylod.workspaces;
      oldState.syncCalender = paylod.syncCalender;
      // oldState.googleIntegration = false;
      oldState.teams = paylod.teams;
      return {
        ...state,
        data: oldState,
      };
    }
      break;

    case constants.ADDUPDATESYNCCALENDAR: {
      let oldState = cloneDeep(state.data);
      let paylod = action.payload.data;
      if (oldState.syncCalenderList.length > 0) {
        let alreadyExist =
          oldState.syncCalenderList.find((cal) => cal.id == paylod.id) || {};
        if (!isEmpty(alreadyExist)) {
          oldState.syncCalenderList.map((s) => {
            if (s.id == paylod.id) {
              return action.payload.data;
            } else return s;
          });
        } else oldState.syncCalenderList.push(paylod);
      } else oldState.syncCalenderList.push(paylod);
      return {
        ...state,
        data: oldState,
      };
    }
      break;

    case constants.ADDSYNCCALENDAR: {
      let oldState = cloneDeep(state.data);
      oldState.syncCalenderList = action.payload.data;
      return {
        ...state,
        data: oldState,
      };
    }
      break;

    case constants.ADDSYNCCALENDARLIST: {
      let oldState = cloneDeep(state.data);
      let paylod = action.payload.data;
      oldState.syncCalenderList = paylod;
      return {
        ...state,
        data: oldState,
      };
    }
      break;

    case constants.CLEARGOOGLECALENDARSTATE: {
      let oldStatee = cloneDeep(state.data);
      oldStatee.googleIntegration = action.payload;
      return {
        ...state,
        data: oldStatee,
      };
    }
      break;
    case constants.UPDATESYNCENABLED: {
      let oldState = cloneDeep(state.data);
      let paylod = action.payload.data;
      oldState.syncCalenderList = oldState.syncCalenderList.map((s) => {
        if (s.id == paylod.id) {
          return paylod;
        } else return s;
      });
      return {
        ...state,
        data: oldState,
      };
    }
      break;
    case constants.FILTERSYNCCALENDARLIST: {
      let oldSt = cloneDeep(state.data);
      let updatedSyncList = oldSt.syncCalenderList.filter((s) => {
        return s.id !== action.payload.data.id;
      });
      oldSt.syncCalenderList = updatedSyncList;
      return {
        ...state,
        data: oldSt,
      };
    }
      break;

    default:
      return state;
  }
};

export default {
  googleCalendar,
};
