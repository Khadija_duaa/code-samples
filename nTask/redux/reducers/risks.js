import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import findIndex from "lodash/findIndex";
import sortBy from "lodash/sortBy";
import unionBy from "lodash/unionBy";
import xorBy from "lodash/xorBy";
import searchQuery from "../../components/CustomTable2/ColumnSettingDropdown/searchQuery";
import updateCFSelectedOption from "../../helper/updateCFSelectedOption";

const risks = (state = {}, action) => {
  switch (action.type) {
    case constants.UPDATE_RISK_CUSTOM_FIELD:
      const updatedRisks = updateCFSelectedOption(state.data, action.payload);
      return { ...state, data: updatedRisks };
      break;

    case constants.POPULATERISKSINFO:
      return {
        ...state,
        ...action.payload,
      };
      break;
    case constants.UPDATE_RISK_ORDER:
      const sortedArray = sortBy(state.data, function (obj) {
        return findIndex(action.payload, { itemId: obj.id });
      });
      return { ...state, data: sortedArray };
      break;
    case constants.UPDATERISK:
      const tempRisks = cloneDeep(state.data);
      let index = findIndex(tempRisks, function (o) {
        return o.id === action.payload.data.id;
      });
      if (index > -1) tempRisks[index] = action.payload.data;
      return {
        ...state,
        data: tempRisks,
      };
      break;

    case constants.UPDATEBULKRISKINFO:
      if (action.payload.isDelete) {
        // Formating action data risk's id array, with id object
        let inputData = action.payload.data.map(riskId => {
          return { id: riskId };
        });
        let data = xorBy(inputData || [], state.data || [], "id");
        return {
          ...state,
          data,
        };
      } else {
        // Merge new risks data with store risks,
        // In this case action data risks will be prefered by store data risks
        // let data = unionBy(action.payload.data || [], state.data || [], "id");
        let data = state.data.map(risk => {
          let findIndex = action.payload.data.find(r => r.id === risk.id) || false;
          if (findIndex) return findIndex;
          return risk;
        });
        return {
          ...state,
          data,
        };
      }
      break;

    case constants.COPYRISK:
      return {
        ...state,
        data: [action.payload.data, ...state.data],
      };
      break;

    case constants.DELETERISK:
      return {
        ...state,
        data: state.data.filter(function (element) {
          return element.id !== action.payload.data;
        }),
      };
      break;

    case constants.RESETRISKSINFO:
      return {
        ...state,
        risks: "",
      };
      break;

    case constants.MARKREADRISK:
      let mark = "";
      let notifications = [];
      let allData = state.data.filter(function (element) {
        return element.id !== action.payload.id;
      });
      mark = state.data.find(function (element) {
        return element.id === action.payload.id;
      });
      if (mark) {
        notifications = mark.notification.filter(x => {
          return x.users.map(y => {
            if (y.userId === action.payload.currentUser && y.isViewed === false) {
              y.isViewed = true;
            }
            return y;
          });
        });
      }
      mark.notification = notifications;
      return {
        ...state,
        data: [mark, ...allData],
      };
      break;

    case constants.UPDATERISKROWS:
      {
        let updatedRows = action.payload;
        return {
          ...state,
          data: updatedRows,
        };
      }
      break;

    case constants.BULKUPDATERISK:
      {
        let payload = action.payload;
        const allRisks = cloneDeep(state.data);
        let selectedArr = allRisks.map(r => {
          let findRisk = payload.find(p => p.riskId === r.riskId) || false;
          if (findRisk) return findRisk;
          else return r;
        });

        return {
          ...state,
          data: selectedArr,
        };
      }
      break;

    case constants.UPDATE_RISK_FILTER: {
      // const cloneState = cloneDeep(state.riskFilter);
      // const parent = Object.keys(action.payload)[0];
      // let updateFilters;
      // if (state.riskFilter && state.riskFilter.filterId !== action.payload.filterId) {
      //   updateFilters = { riskFilter: action.payload };
      // } else {
      //   if (action.payload[parent]?.selectedValues?.length || action.payload[parent]?.type) {
      //     updateFilters = { riskFilter: { ...cloneState, ...action.payload } };
      //   } else {
      //     delete cloneState[parent]
      //     updateFilters = {
      //       riskFilter: { ...cloneState }
      //     }
      //   }
      // }
      // searchQuery.riskFilter = updateFilters.riskFilter;
      // return { ...state, ...updateFilters };

      const cloneState = cloneDeep(state.riskFilter);
      let updateFilters;
      const payloadLength = Object.keys(action.payload).length;
      if (payloadLength) {
        if (state.riskFilter && state.riskFilter.filterId !== action.payload.filterId) {
          updateFilters = { riskFilter: action.payload }
        } else {
          updateFilters = { riskFilter: { ...cloneState, ...action.payload } };
        }
      } else {
        updateFilters = { riskFilter: {} }
      }
      searchQuery.riskFilter = updateFilters.riskFilter;
      return { ...state, ...updateFilters };
    }
      break;

    case constants.CLEAR_RISK_FILTER:
      searchQuery.riskFilter = {};
      return { ...state, riskFilter: {} };
      break;

    case constants.UPDATERISKCUSTOMFIELDDATA:
      {
        let payload = action.payload;
        let allRisks = cloneDeep(state.data);
        if (payload.type === "Create") {
          allRisks = allRisks.map(r => {
            if (r.id === payload.obj.groupId) {
              return {
                ...r,
                customFieldData: r.customFieldData
                  ? [...r.customFieldData, payload.obj]
                  : [payload.obj],
              };
            }
            return r;
          });
        }
        if (payload.type === "Update") {
          allRisks = allRisks.map(r => {
            if (r.id === payload.obj.groupId) {
              let selectedRiskData = cloneDeep(r.customFieldData);
              selectedRiskData = selectedRiskData.map(sd => {
                if (sd.fieldId === payload.obj.fieldId) return payload.obj;
                return sd;
              });
              return { ...r, customFieldData: selectedRiskData };
            }
            return r;
          });
        }
        return {
          ...state,
          data: allRisks,
        };
      }
      break;
    case constants.SAVE_ALL_RISKS:
      return {
        ...state,
        data: action.payload,
      };
      break;
    case constants.SET_RISK_CUSTOM_FILTER:
      {
        let updatedFilters = action.payload.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (cv.customFieldsFilters) {
            let customFilters = Object.keys(cv.customFieldsFilters);
            customFilters.map(item => (currentValue[item] = cv.customFieldsFilters[item]));
            delete currentValue.customFieldsFilters;
            res.push(currentValue);
          } else res.push(cv);
          return res;
        }, []);
        const defaultFilter = updatedFilters.find(f => f.defaultFilter);
        searchQuery.riskFilter = defaultFilter || searchQuery.riskFilter;
        return {
          ...state,
          savedFilters: updatedFilters,
          riskFilter: defaultFilter || searchQuery.riskFilter,
        };
      }
      break;
    case constants.ADD_NEW_CUSTOM_FILTER:
      {
        let updatedFilters = action.payload.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (cv.customFieldsFilters) {
            let customFilters = Object.keys(cv.customFieldsFilters);
            customFilters.map(item => (currentValue[item] = cv.customFieldsFilters[item]));
            delete currentValue.customFieldsFilters;
            res.push(currentValue);
          } else res.push(cv);
          return res;
        }, []);
        return { ...state, savedFilters: updatedFilters };
      }
      break;
    case constants.DELETE_RISK_FILTER:
      const stateCopy = cloneDeep(state.riskFilter);
      // stateCopy[action.payload].selectedValues = [];
      // stateCopy[action.payload].type = "";
      delete stateCopy[action.payload];
      const updatedFilterState = { ...state, riskFilter: stateCopy };
      searchQuery.riskFilter = updatedFilterState.riskFilter;
      return updatedFilterState;
      break;

    case constants.UPDATE_RISK_QUICK_FILTER:
      {
        const updatedFilters = { ...state.quickFilters, ...action.payload };
        searchQuery.quickFilters = updatedFilters;
        return { ...state, quickFilters: updatedFilters };
      }
      break;
    case constants.SHOW_ALL_RISKS:
      {
        searchQuery.quickFilters = {};
        return { ...state, quickFilters: {}, data: state.data.filter(t => !t.isArchive) };
      }
      break;

    case constants.DELETE_RISK_CUSTOM_FILTER:
      let updatedFilter = state.savedFilters.filter(item => item.filterId !== action.payload);
      searchQuery.riskFilter = {};
      return { ...state, savedFilters: updatedFilter, riskFilter: {} };
      break;
    case constants.UPDATE_BULK_RISKS:
      {
        let updatedRiskData = state.data.reduce((result, cv) => {
          const isExist = action.payload.find(item => item.id === cv.id);
          if (isExist) result.push(isExist);
          else result.push(cv);
          return result;
        }, []);
        return { ...state, data: updatedRiskData };
      }
      break;
    case constants.DELETE_BULK_RISKS:
      {
        let risks = state.data;
        let updatedRisk = risks.filter(x => {
          return !action.payload.riskIds.includes(x.id);
        });
        return { ...state, data: updatedRisk };
      }
      break;
    case constants.UNARCHIVE_BULK_RISK:
      {
        let risks = state.data;
        let updatedRisks = risks.map(x => {
          if (action.payload.riskIds.includes(x.id)) {
            return { ...x, isArchive: false };
          } else return x;
        });
        return { ...state, data: updatedRisks };
      }
      break;

    case constants.CREATE_RISK:
      return { ...state, data: [action.payload, ...state.data] };
      break;

    case constants.CREATED_RISK_EDIT:
      const updatedRisksArr = state.data.map(t => {
        if (t.clientId == action.payload.clientId) {
          return { ...action.payload, isNew: true };
        }
        return t;
      });
      return { ...state, data: updatedRisksArr };
      break;
    case constants.UPDATE_RISK_DATA:
      const newRisksArray = state.data.map(t => {
        if (t.id == action.payload.id) {
          return action.payload;
        }
        return t;
      });
      return { ...state, data: newRisksArray };
      break;
    case constants.REMOVE_RISK_QUICK_FILTER:
      {
        const { [action.payload]: value, ...rest } = state.quickFilters;
        const archiveFreeRisks =
          value.type == "Archived" ? state.data.filter(t => !t.isArchive) : state.data;
        searchQuery.quickFilters = rest ? rest : {};
        return { ...state, data: archiveFreeRisks, quickFilters: rest ? rest : {} };
      }
      break;
    case constants.ADD_BULK_RISKS:
      {
        return { ...state, data: [...action.payload, ...state.data] };
      }
      break;
    default:
      return state;
  }
};

export default {
  risks,
};
