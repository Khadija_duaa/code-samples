import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";

const outlookCalendar = (state = {}, action) => {
  switch (action.type) {
    case constants.UPDATEOUTLOOKCALENDARINFO: {
      let oldState = cloneDeep(state.data);
      let paylod = action.payload.data;
      oldState.projects = paylod.projects;
      oldState.workspaces = paylod.workspaces;
      oldState.syncOtherCalendar = paylod.syncOtherCalendar;
      oldState.teams = paylod.teams;
      return {
        ...state,
        data: oldState,
      };
    }
    break;
    case constants.ADDUPDATEOUTLOOKACCDETAILS: {
      let oldStatee = cloneDeep(state.data);
      let alreadyAdded = oldStatee.syncedCalendar.some(
        (s) => s.code == action.payload.data.code
      );
      if (alreadyAdded) {
        oldStatee.syncedCalendar = oldStatee.syncedCalendar.map((c) => {
          if (c.code == action.payload.data.code) {
            return action.payload.data;
          } else return c;
        });
      } else {
        oldStatee.syncedCalendar = [
          ...oldStatee.syncedCalendar,
          action.payload.data,
        ];
      }

      return {
        ...state,
        data: oldStatee,
      };
    }
    break;

    case constants.ADDOUTLOOKCALENDAR: {
      let stateData = cloneDeep(state.data);
      let calendarsIntegration = action.payload.data;
      let outlookData =
        calendarsIntegration.length > 0
          ? calendarsIntegration.filter((c) => c.mailServerType == 1)
          : null;
      if (outlookData) stateData.syncedCalendar = outlookData;
      else stateData.syncedCalendar = [];
      return {
        ...state,
        data: stateData,
      };
    }
    break;

    case constants.DELETEOUTLOOKCALENDAR: {
      let stateDataa = cloneDeep(state.data);
      stateDataa.syncedCalendar = stateDataa.syncedCalendar.filter(
        (c) => c.code !== action.payload.data.code
      );
      return {
        ...state,
        data: stateDataa,
      };
    }
    break;

    default:
      return state;
  }
};

export default {
  outlookCalendar,
};
