import constants from "../constants/types";
import initialState from "../../redux/store/initialState";
const appliedFilters = (state = {}, action) => {
  switch (action.type) {
    case constants.SETAPPLIEDFILTERS:
      {
        const { filters, type } = action.payload;
        let copiedObject = JSON.parse(JSON.stringify(state));
        copiedObject[type] = copiedObject[type]
          ? copiedObject[type]["filterId"] == (filters ? filters.filterId : "")
            ? null
            : filters
          : filters || null;
        return copiedObject;
      }
      break;

    case constants.CLEARSPECIFICFILTER:
      if (
        state &&
        state[action.payload.type] &&
        state[action.payload.type].filterId &&
        state[action.payload.type].filterId === action.payload.filters.filterId
      )
        return { ...state, [action.payload.type]: null };
      return state;
      break;

    case constants.CLEARAPPLIEDFILTERS:
      {
        return initialState.getInitialState().appliedFilters;
      }
      break;

    default:
      return state;
  }
};

export default {
  appliedFilters,
};
