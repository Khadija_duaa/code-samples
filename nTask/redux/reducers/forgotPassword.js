import constants from "./../constants/types";
const forgotPassword = (state = {}, action) => {
    switch (action.type) {
        case constants.FORGOTPASSWORD:
            return {
                ...state,
                data: action.payload.data,
            };
            break;

        case constants.DELETEFORGOTPASS:
            return {
                ...state,
                data: "",
            };
            break;

        default:
            return state;
    }
};

export default {
    forgotPassword: forgotPassword
};

