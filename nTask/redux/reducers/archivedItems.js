import consts from "../constants/types";

const archivedItems = (state = {}, action) => {
  switch (action.type) {
    case consts.UPDATEARCHIVEDITEMS: {
      return { ...state, ...action.payload };
    }
    break;

    default:
      return state;
  }
};

export default {
  archivedItems,
};
