import constants from "./../constants/types";
import { groupBy } from "../../helper/customFieldsData";
import cloneDeep from "lodash/cloneDeep";
import { grid } from "../../components/CustomTable2/gridInstance";

const customFields = (state = {}, action) => {
  let stateCopy = cloneDeep(state);

  switch (action.type) {
    case constants.FETCHCUSTOMFIELDS: {
      let payload = action.payload;
      const fieldsGroupByType = groupBy(payload, "fieldType");
      const sections = payload.filter(field => field.fieldType === "section");
      return {
        ...state,
        data: payload,
        groupCustomFields: fieldsGroupByType,
        sections: sections,
      };
    }
      break;

    case constants.ADDCUSTOMFIELD: {
      let payload = action.payload;
      const fieldsGroupByType = groupBy([...state.data, payload], "fieldType");
      const sections = [...state.data, payload].filter(field => field.fieldType === "section");
      return {
        ...state,
        data: [...state.data, payload],
        groupCustomFields: fieldsGroupByType,
        sections: sections,
      };
    }
      break;

    case constants.EDITCUSTOMFIELD: {
      let payload = action.payload;
      stateCopy.data = stateCopy.data.map(ele => {
        /** if already exist in array then replace the item */
        if (ele.fieldId === payload.fieldId) return payload;
        else return ele;
      });

      stateCopy.groupCustomFields = stateCopy.groupCustomFields.map(item => {
        /** if already exist in array then replace the item */
        item.fields = item.fields.map(elementItem => {
          if (elementItem.fieldId === payload.fieldId) return payload;
          else return elementItem;
        });
        return item;
      });

      stateCopy.sections = stateCopy.sections.map(ele => {
        /** if already exist in array then replace the item */
        if (ele.fieldId === payload.fieldId) return payload;
        else return ele;
      });
      setTimeout(() => {
        if (grid.grid) grid.grid.redrawRows();
      }, 0);
      return {
        ...stateCopy,
      };
    }
      break;

    case constants.DELETECUSTOMFIELD: {
      let payload = action.payload;
      stateCopy.data = stateCopy.data.filter(ele => ele.fieldId !== payload[0]);
      stateCopy.sections = stateCopy.sections.filter(ele => ele.fieldId !== payload[0]);

      stateCopy.groupCustomFields = stateCopy.groupCustomFields.map(item => {
        /** if already exist in array then replace the item */
        item.fields = item.fields.filter(elementItem => elementItem.fieldId !== payload[0]);
        return item;
      });

      return {
        ...stateCopy,
      };
    }
      break;

    case constants.UPDATECHILDFIELDS: {
      let sectionId = action.payload.fieldId;
      stateCopy.data = stateCopy.data.reduce((res, cv) => {
        if (cv.sectionId === sectionId) {
          cv.sectionId = null;
        }
        res.push(cv);
        return res;
      }, []);
      stateCopy.groupCustomFields = stateCopy.groupCustomFields.map(item => {
        item.fields = item.fields.reduce((res, cv) => {
          if (cv.sectionId === sectionId) {
            cv.sectionId = null;
          }
          res.push(cv);
          return res;
        }, []);
        return item;
      });

      return {
        ...stateCopy,
      };
    }
      break;

    default:
      return state;
  }
};

export default {
  customFields: customFields,
};
