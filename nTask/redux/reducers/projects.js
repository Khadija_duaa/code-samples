import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import unionBy from "lodash/unionBy";
import xorBy from "lodash/xorBy";
import merge from "lodash/merge";
import { sortListData } from "../../helper/sortListData";
import searchQuery from "../../components/CustomTable2/ColumnSettingDropdown/searchQuery";
import findIndex from "lodash/findIndex";
import sortBy from "lodash/sortBy";

const projects = (state = {}, action) => {
  let modifiedProjects;
  switch (action.type) {
    case constants.CREATE_PROJECT:
      return { ...state, data: [action.payload, ...state.data] };
      break;
    case constants.CREATED_PROJECT_EDIT: {
      const updatedProjectsArr = state.data.map(p => {
        if (p.clientId == action.payload.clientId) {
          return { ...action.payload, isNew: true };
        }
        return p;
      });
      return { ...state, data: updatedProjectsArr };
    }
      break;
    case constants.SAVE_ALL_PROJECTS:
      return { ...state, data: action.payload, projectCount: state.data.length ? state.projectCount : action.payload.length };
      break;
    case constants.POPULATEPROJECTINFO:
      return {
        ...state,
        ...action.payload,
      };
      break;

    case constants.ADDPROJECTTOSTORE: {
      return {
        ...state,
        data: [{ ...action.payload.data, isNew: true }, ...state.data],
      };
    }
      break;

    case constants.PROJECTDEFAULTTEMPLATE: {
      let tempProject = cloneDeep(state.data);
      modifiedProjects = tempProject.map(function (element) {
        if (element.projectId === action.payload.data.projectId) {
          element.projectTemplate = action.payload.data.template;
          element.projectTemplateId = action.payload.data.template.templateId;
          return element;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedProjects,
      };
    }
      break;

    case constants.WORKSPACEANDPROJECTDEFAULTTEMPLATE: {
      let tempProject = cloneDeep(state.data);
      modifiedProjects = tempProject.map(element => {
        if (element.projectTemplateId === action.payload.data.oldTemplateId) {
          element.projectTemplate = action.payload.data.newTemplate;
          element.projectTemplateId = action.payload.data.newTemplate.templateId;
          return element;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedProjects,
      };
    }
      break;

    case constants.TOGGLEPROJECT: {
      return {
        ...state,
        data: xorBy(state.data, [action.payload.data], "projectId"),
      };
    }
      break;

    case constants.DELETEPROJECT: {
      const statData = state.data;
      return {
        ...state,
        data: statData.filter(function (element) {
          return element.projectId !== action.payload.data;
        }),
      };
    }
      break;

    case constants.RESETPROJECTINFO:
      return {
        ...state,
        data: "",
      };
      break;

    case constants.EDITPROJECTNAME: {
      let tempProject = cloneDeep(state.data);
      modifiedProjects = tempProject.map(function (element) {
        if (element.projectId === action.payload.data.projectId) {
          element.projectName = action.payload.data.editProjectName;
          return element;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedProjects,
      };
    }
      break;

    case constants.UPDATEPROJECT: {
      // const {
      //   status,
      //   projectName,
      //   colorCode,
      //   projectManager,
      //   resources,
      //   projectTemplate,
      //   isStared,
      //   description,
      //   projectPermission,
      //   projectTemplateId,
      //   customFieldData,
      // } = action.payload.data;
      let tempArray = cloneDeep(state.data);
      modifiedProjects = tempArray.map(function (element) {
        if (element.projectId === action.payload.data.projectId) {
          return {
            ...element,
            ...action.payload.data,
          };
        }
        return element;
      });
      return {
        ...state,
        data: modifiedProjects,
      };
    }
      break;

    case constants.UPDATEPROJECTMANAGERLIST: {
      let tempProject = cloneDeep(state.data);
      let pms = action.payload.resources.filter(r => r.isProjectManager);
      pms = pms.map(p => p.userId);
      modifiedProjects = tempProject.map(function (element) {
        if (element.projectId === action.payload.projectId) {
          element.projectManager = pms;
          return element;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedProjects,
      };
    }
      break;
    case constants.UPDATEPROJECTDETAILSLISTING: {
      let tempProjectss = cloneDeep(state.data);
      modifiedProjects = tempProjectss.map(function (element) {
        if (element.projectId === action.payload.id) {
          return { ...element, ...action.payload.data };
        }
        return element;
      });
      return {
        ...state,
        data: modifiedProjects,
      };
    }
      break;

    case constants.UPDATEPROJECTPARTIALLY: {
      let tempProject = cloneDeep(state.data);
      modifiedProjects = tempProject.map(function (element) {
        if (element.projectId === action.payload.data.projectId) {
          return merge(element, action.payload.data);
        }
        return element;
      });
      return {
        ...state,
        data: modifiedProjects,
      };
    }
      break;

    case constants.UPDATEBULKPROJECTINFO:
      // Merge new projects data with store projects,
      // In this case action data projects will be prefered by store data projects
      let data = unionBy(action.payload.data || [], state.data || [], "id");
      return {
        ...state,
        data,
      };
      break;

    case constants.UPDATEDELETEDBULKPROJECTINFO /* reducer delete bulk store projects */:
      let updateddata = state.data.filter(project => {
        return action.payload.data.indexOf(project.projectId) == 0;
      });

      return {
        ...state,
        data: updateddata,
      };
      break;

    case constants.UPDATEPROJECTSROWS: {
      let updatedRows = action.payload;
      return {
        ...state,
        data: updatedRows,
      };
    }
      break;

    case constants.UPDATEPROJECTVIEWDATE: {
      let tempProject = cloneDeep(state.data);
      modifiedProjects = tempProject.map(element => {
        if (element.projectId === action.payload.projectId) {
          element.lastViewedDate = action.payload.projectLastViewedDate;
          return element;
        }
        return element;
      });
      return {
        ...state,
        data: modifiedProjects,
      };
    }
      break;

    case constants.UPDATEPROJECTCUSTOMFIELDDATA: {
      let payload = action.payload;
      let allProjects = [];
      if (payload.type === "Create") {
        allProjects = state.data.map(p => {
          if (p.projectId === payload.obj.groupId) {
            return { ...p, customFieldData: p.customFieldData ? [...p.customFieldData, payload.obj] : [payload.obj] }
          }
          return p;
        })
      }
      if (payload.type === "Update") {
        allProjects = state.data.map(p => {
          if (p.projectId === payload.obj.groupId) {
            let selectedProjectData = [];
            selectedProjectData = p.customFieldData.map(sd => {
              if (sd.fieldId === payload.obj.fieldId) return payload.obj
              return sd
            })
            return { ...p, customFieldData: selectedProjectData }
          }
          return p;
        })
      }
      return {
        ...state,
        data: allProjects,
      };
    }
      break;
    case constants.UPDATE_PROJECT_ORDER:
      const sortedArray = sortBy(state.data, function (obj) {
        return findIndex(action.payload, { itemId: obj.projectId });
      });
      return { ...state, data: sortedArray };
      break;
    // filter reducers starts here 
    case constants.UPDATE_PROJECT_FILTER: {
      const cloneState = cloneDeep(state.projectFilter);
      let updateFilters;
      const payloadLength = Object.keys(action.payload).length;
      if (payloadLength) {
        if (state.projectFilter && state.projectFilter.filterId !== action.payload.filterId) {
          updateFilters = { projectFilter: action.payload }
        } else {
          updateFilters = { projectFilter: { ...cloneState, ...action.payload } };
        }
      } else {
        updateFilters = { projectFilter: {} }
      }
      searchQuery.projectFilter = updateFilters.projectFilter;
      return { ...state, ...updateFilters };
    }
      break;
    case constants.CLEAR_PROJECT_FILTER:
      searchQuery.projectFilter = {};
      return { ...state, projectFilter: {} };
      break;
    case constants.DELETE_PROJECT_FILTER: {
      const stateCopy = cloneDeep(state.projectFilter);
      // stateCopy[action.payload].selectedValues = [];
      // stateCopy[action.payload].type = "";
      delete stateCopy[action.payload];
      const updatedFilterState = { ...state, projectFilter: stateCopy };
      searchQuery.projectFilter = updatedFilterState.projectFilter;
      return updatedFilterState;
    }
      break;
    case constants.SET_PROJECT_CUSTOM_FILTER:
      {
        let updatedFilters = action.payload.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (cv.customFieldsFilters) {
            let customFilters = Object.keys(cv.customFieldsFilters);
            customFilters.map(item => (currentValue[item] = cv.customFieldsFilters[item]));
            delete currentValue.customFieldsFilters;
            res.push(currentValue);
          } else res.push(cv);
          return res;
        }, []);
        const defaultFilter = updatedFilters.find(f => f.defaultFilter);
        searchQuery.projectFilter = defaultFilter || searchQuery.projectFilter;
        return {
          ...state,
          savedFilters: updatedFilters,
          projectFilter: defaultFilter || searchQuery.projectFilter,
        };
      }
      break;
    case constants.ADD_NEW_PROJECT_CUSTOM_FILTER:
      {
        let updatedFilters = action.payload.reduce((res, cv) => {
          let currentValue = cloneDeep(cv);
          if (cv.customFieldsFilters) {
            let customFilters = Object.keys(cv.customFieldsFilters);
            customFilters.map(item => (currentValue[item] = cv.customFieldsFilters[item]));
            delete currentValue.customFieldsFilters;
            res.push(currentValue);
          } else res.push(cv);
          return res;
        }, []);
        return { ...state, savedFilters: updatedFilters };
      }
      break;
    case constants.DELETE_PROJECT_CUSTOM_FILTER:
      let updatedFilter = state.savedFilters.filter(item => item.filterId !== action.payload);
      searchQuery.projectFilter = {};
      return { ...state, savedFilters: updatedFilter, projectFilter: {} };
      break;

    case constants.ADD_BULK_PROJECTS:
      {
        return { ...state, data: [...action.payload, ...state.data] };
      }
      break;

    case constants.UPDATE_PROJECT_DATA:
      const newProjectsArray = state.data.map(t => {
        if (t.projectId == action.payload.projectId) {
          return action.payload;
        }
        return t;
      });
      return { ...state, data: newProjectsArray };
      break;
    // bulk actions reduceres starts here

    case constants.REMOVE_PROJECT_QUICK_FILTER: {
      const { [action.payload]: value, ...rest } = state.quickFilters;
      const archiveFreeProjects =
        value.type == "Archived" ? state.data.filter(t => !t.isDeleted) : state.data;
      searchQuery.quickFilters = rest ? rest : {};
      return { ...state, data: archiveFreeProjects, quickFilters: rest ? rest : {} };
    }

    case constants.UPDATE_PROJECT_QUICK_FILTER: {
      const updatedFilters = { ...state.quickFilters, ...action.payload };
      searchQuery.quickFilters = updatedFilters;
      return { ...state, quickFilters: updatedFilters };
    }
      break;

    case constants.UPDATE_BULK_PROJECT: {
      let updatedProjectData = state.data.reduce((result, cv) => {
        const isExist = action.payload.find(item => item.projectId === cv.projectId);
        if (isExist) result.push(isExist);
        else result.push(cv);
        return result;
      }, []);
      return { ...state, data: updatedProjectData };
    }
      break;

    case constants.DELETE_BULK_PROJECT: {
      let projects = state.data;
      let updatedProject = projects.filter(x => {
        return !action.payload.projectIds.includes(x.projectId);
      });
      return { ...state, data: updatedProject };
    }
      break;

    case constants.UNARCHIVE_BULK_PROJECT: {
      let projects = state.data;
      let updatedProject = projects.map(x => {
        if (action.payload.projectIds.includes(x.projectId)) {
          return { ...x, isDeleted: false };
        } else return x;
      });
      return { ...state, data: updatedProject };
    }
      break;



    default:
      return state;
  }
};

export default {
  projects,
};
