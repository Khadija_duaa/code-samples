import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";

const initialState = {
  data: []
}
const issueNotifications = (state = initialState, action) => {
  switch (action.type) {
    case constants.POPULATEISSUECOMMENTSNOTIFICATIONS: {
      return {
        ...state,
        data: action.payload.data,
      };
    }
    break;
    
    case constants.CLEARISSUECOMMENTSNOTIFICATIONS: {
      return {
        ...state,
        data: [],
      };
    }
    break;
    
    case constants.UPDATEISSUENOTIFICATIONS: {
      let issueNoti = cloneDeep(state.data);
      let updatedNoti = [action.payload.data , ...issueNoti, ];
      return {
        ...state,
        data: updatedNoti,
      };
    }
    break;
    
    default:
      return state;
  }
};

export default {
  issueNotifications,
};
