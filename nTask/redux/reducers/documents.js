import constants from "../constants/types";
const initialState = {
  data: {
    attachments: [],
    folders: []
  }
}

const documents = (state = initialState, action) => {
  let newData;
  switch (action.type) {
    case constants.POPULATEDOCUMENTS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.UPDATEFOLDERITEM:
      newData = { ...state.data };
      newData.folders = state.data.folders.map(element => {
        if (element.id === action.payload.data.id) {
          return {
            ...action.payload.data
          };
        }
        return element;
      });
      return {
        ...state,
        data: newData
      };
      break;

    case constants.DELETEFOLDERITEM:
      newData = { ...state.data };
      newData.folders = newData.folders.filter(element => {
        return element.id !== action.payload.data.id;
      })
      return {
        ...state,
        data: newData
      };
      break;

    case constants.NEWFOLDERITEM:
      newData = { ...state.data };
      newData.folders = [...newData.folders, action.payload.data];
      return {
        ...state,
        data: newData
      };
      break;

    case constants.DELETEFILEITEM:
      newData = { ...state.data };
      newData.attachments = newData.attachments.filter(element => {
        return element.id !== action.payload.data.id;
      })
      return {
        ...state,
        data: newData
      };
      break;

    case constants.NEWFILEITEM:
      newData = { ...state.data };
      newData.attachments = [...newData.attachments, ...action.payload.data];
      return {
        ...state,
        data: newData
      };
      break;

    // if (state.data.attachments && state.data.attachments.length) {
    //   return {
    //     ...state,
    //     [data.attachments]: [...state.data.attachments, action.payload.data]
    //   };
    // }
    // else{
    //   return {
    //     ...state,
    //     [data.attachments]: [action.payload.data]
    //   };
    // }

    default:
      return state;
  }
};

export default documents;
