import constants from "./../constants/types";
const calenderTasks = (state = {}, action) => {
  switch (action.type) {

    case constants.RESETCALENDERTASKS:
      return {
        ...state,
        calenderTasks: []
      };
      break;
    case constants.UPDATECALENDERTASKS:
      let modifiedCalender = state.calenderTasks.map(function (element) {
        if (element.id === action.payload.id) {
          return {
            ...action.payload
          };
        }
        return element;
      });
      return {
        ...state,
        calenderTasks: modifiedCalender
      };
      break;
    case constants.COPYCALENDERTASK:
      return {
        ...state,
        calenderTasks: [action.payload, ...state.calenderTasks]
      };
      break;
    case constants.DELETECALENDERTASK:
      const filteredTasks = state.calenderTasks.filter((task) => {
        return task.id !== action.payload.data
      })
      return {
        ...state,
        calenderTasks: filteredTasks
      }
      break;
    default:
      return state;
  }
};

export default {
  calenderTasks
};
