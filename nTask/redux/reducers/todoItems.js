import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";
import moment from "moment";
const initialState = {
  data: {
    activeTaskId: '',
    todoItems: []
  }
}
const todoItemsData = (state = initialState, action) => {
  let newData;
  switch (action.type) {
    case constants.POPULATETASKTODOITEMS: {
      let todoItemsArr = action.payload.data;
      return {
        ...state,
        data: {
          activeTaskId: action.payload.taskId,
          todoItems: todoItemsArr
        }
      };
    }
      break;

    case constants.CREATETODOITEM:
      if (state.data.activeTaskId != action.payload.taskId) return state;
      newData = { ...state.data };
      if (state.data.todoItems && state.data.todoItems.length) {
        newData.todoItems = [...state.data.todoItems, action.payload.data]
        return {
          ...state,
          data: newData
        };
      }
      else {
        newData.todoItems = [action.payload.data]
        return {
          ...state,
          data: newData
        };
      };
      break;

    case constants.SAVETASKTODOITEMS: {
      if (state.data.activeTaskId != action.payload.taskId) return state;
      newData = { ...state.data };
      let TodoItemsArr = cloneDeep(newData.todoItems);
      let updatedTodoItemsArr = [];
      let itemExists =
        TodoItemsArr.find((t) => t.id == action.payload.data.id) || {};
      if (!isEmpty(itemExists)) {
        updatedTodoItemsArr = TodoItemsArr.map((t) => {
          if (t.id == action.payload.data.id) {
            return action.payload.data;
          } else return t;
        });
      } else {
        updatedTodoItemsArr = [...TodoItemsArr, action.payload.data];
      }
      newData.todoItems = updatedTodoItemsArr;
      return {
        ...state,
        data: newData
      };
    }
      break;

    case constants.DELETETASKTODOITEM: {
      if (state.data.activeTaskId != action.payload.taskId) return state;
      newData = { ...state.data };
      let TodoItemsArr = cloneDeep(newData.todoItems);
      let updatedTodoItemsArr = TodoItemsArr.filter(
        (t) => t.id !== action.payload.data.id
      );
      newData.todoItems = updatedTodoItemsArr;
      return {
        ...state,
        data: newData,
      };
    }
      break;

    case constants.CLEARTASKTODOITEMS: {
      // if(state.data.activeTaskId != action.payload.taskId) return state;
      return {
        ...state,
        data: {
          activeTaskId: '',
          todoItems: []
        }
      };
    }
      break;

    case constants.DELETESINGLETODOITEMS: {
      if (state.data.activeTaskId != action.payload.taskId) return state;
      newData = { ...state.data };
      let TodoItemsArr = cloneDeep(newData.todoItems);
      let updatedTodoItemsArr = TodoItemsArr.filter(
        (t) => t.id !== action.payload.data
      );
      newData.todoItems = updatedTodoItemsArr;
      return {
        ...state,
        data: newData,
      };
    }
      break;

    case constants.ALLTODOITEMSCOMPLETE: {
      if (state.data.activeTaskId != action.payload.taskId) return state;
      newData = { ...state.data };
      let TodoItemsArr = cloneDeep(newData.todoItems);
      let updatedTodoItemsArr = [];
      if (action.payload.check) {
        /** check all todo list item */
        updatedTodoItemsArr = TodoItemsArr.map((x) => {
          x.completedDate = x.isComplete ? x.completedDate : moment();
          x.completedBy = x.isComplete
            ? x.completedBy
            : action.payload.user.userId;
          x.updatedDate = x.isComplete ? x.updatedDate : moment();
          x.updatedBy = x.isComplete ? x.updatedBy : action.payload.user.userId;
          x.isComplete = true;
          return x;
        });
      } else {
        /** un check all todo items */
        updatedTodoItemsArr = TodoItemsArr.map((x) => {
          x.completedDate = null;
          x.completedBy = null;
          x.isComplete = false;
          return x;
        });
      }
      newData.todoItems = updatedTodoItemsArr;
      return {
        ...state,
        data: newData,
      };
    }
      break;

    case constants.UPDATETODOITEMSORDER: {
      if (state.data.activeTaskId != action.payload.taskId) return state;
      newData = { ...state.data };
      let TodoItemsArr = cloneDeep(newData.todoItems);
      let updatedPositionedArr = [];
      let idsArr = action.payload.data;
      let updatedTodoItemsArr = idsArr.map((itemId, index) => TodoItemsArr.find(item => item.id == itemId))
      // let updatedTodoItemsArr = idsArr.map((i, key) => {
      //   TodoItemsArr.map((t) => {
      //     if (t.id == i) {
      //       updatedPositionedArr.push(t);
      //     }
      //   });
      // });
      newData.todoItems = updatedTodoItemsArr;
      return {
        ...state,
        data: newData,
      };
    }
      break;

    default:
      return state;
  }
};

export default {
  todoItemsData: todoItemsData,
};
