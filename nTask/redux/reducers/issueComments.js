import constants from "./../constants/types";
const issueCommentsData = (state = {}, action) => {

  switch (action.type) {
    case constants.POPULATEISSUECOMMENTS:
      return {
        ...state,
        data: action.payload.data
      };
      break;

    case constants.UPDATEISSUECOMMENTS:
      let modifiedTasks = state.data.map(function (element) {
        if (element.update.id === action.payload.data.id) {
          return {
            ...action.payload.data.response
          };
        }
        return element;
      });
      return {
        ...state,
        data: modifiedTasks
      };
      break;

    case constants.ADDNEWISSUECOMMENTS:

      return {
        ...state,
        data: [...state.data, action.payload.data]
      };
      break;

    case constants.DELETEISSUECOMMENTS:
      return {
        ...state,
        data: state.data.filter(function (element) {
          return element.update.id !== action.payload.data;
        })
      };
      break;

    default:
      return state;
  }
};

export default {
  issueCommentsData: issueCommentsData
};
