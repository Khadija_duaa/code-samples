import constants from "./../constants/types";
import cloneDeep from "lodash/cloneDeep";

const initialState = {
  data: []
}

const riskNotifications = (state = initialState, action) => {
  switch (action.type) {
    case constants.POPULATERISKCOMMENTSNOTIFICATIONS: {
      return {
        ...state,
        data: action.payload.data,
      };
    }
      break;

    case constants.CLEARRISKCOMMENTSNOTIFICATIONS: {
      return {
        ...state,
        data: [],
      };
    }
      break;

    case constants.UPDATERISKNOTIFICATIONS: {
      let riskNoti = cloneDeep(state.data);
      let updatedNoti = [action.payload.data, ...riskNoti];
      return {
        ...state,
        data: updatedNoti,
      };
    }
      break;

    default:
      return state;
  }
};

export default {
  riskNotifications,
};
