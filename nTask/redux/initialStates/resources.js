import { ResourceWorkloadUnits } from "./../../utils/constants/ResourceWorkloadUnits";
import { CalendarTimelinePeriods } from "./../../utils/constants/CalendarTimelinePeriods";
import { ResourceWorkloadViewType } from "../../utils/constants/ResourceWorkloadViewType";
import { TaskTimelineColor } from "../../utils/constants/TaskTimelineColor";

const resourcesInitialState = {
  initialData: {},
  data: {
    calendarDates: [],
    calendarDatesFlattened: [],
    resourcesDataList: [],
    projectsDataList: [],
    // new below
    // Map: ID => list
    projectResourcesDataList: new Map(),
    resourceTasksDataList: new Map(),
    projectResourceTasksDataList: new Map(),

    unassignedTasksDataList: [],

    projectsOfWorkspacesFilterDataList: new Map(),
    resourcesOfWorkspacesFilterDataList: new Map(),

    unscheduledTasksDataList: [],
  },
  dataRecordsCount: {
    resourcesDataRecordsCount: -1,
    // projectsDataListExists: 0,

    projectResourcesDataRecordsCount: new Map(),
    // resourceTasksDataRecordsCount: new Map(),
    // projectResourceTasksDataRecordsCount: new Map(),
  },
  dataFlags: {
    resourcesDataListExists: false,
    projectsDataListExists: false,

    projectResourcesDataListExists: new Map(),
    resourceTasksDataListExists: new Map(),
    projectResourceTasksDataListExists: new Map(),

    unassignedTasksDataListExists: false,

    projectsOfWorkspacesFilterDataListExists: false,

    unscheduledTasksDataListExists: false,
  },
  uiGrid: {
    sidebarCollapsed: false,
    dayProgressHourglassWidth: "",
    gridColumnsCount: 0,
    workloadViewType: ResourceWorkloadViewType.resources.value,
    calendarTimelinePeriodStartDate: null,
    calendarTimelinePeriodEndDate: null,
    openCollapsiblesCount: 0,
  },
  filters: {
    calendarTimelinePeriod: CalendarTimelinePeriods.week.value, // 1w, 2w, 1m, etc
    progressHourglassUnit: ResourceWorkloadUnits.hour.value, // h, %, FTE, etc
    workspaces: [],
    resources: [],
    projects: [],
  },
  settings: {
    settingsObjectId: null,
    showTaskNameWithTimeline: false,
    taskTimelineColor: TaskTimelineColor.taskStatus.value,
  },
};

export default resourcesInitialState;
