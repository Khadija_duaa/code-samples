import {
  setGridResourcesDataListAction,
  setGridProjectsDataListAction,
  setGridProjectResourcesDataListAction,
  setGridResourcesDataListExistsAction,
  setGridProjectsDataListExistsAction,
  setGridProjectResourcesDataListExistsAction,
  setGridResourceTasksDataListAction,
  setGridResourceTasksDataListExistsAction,
  appendGridResourcesDataListAction,
  setGridResourcesDataRecordsCountAction,
  deleteResourceTaskDayWorkloadByDateAction,
  updateResourceTaskDayWorkloadIdByDateAction,
  setProjectsOfWorkspacesFilterDataListAction,
  setResourcesOfWorkspacesFilterDataListAction,
  setUnassignedTasksDataListAction,
  setUnassignedTasksDataListExistsAction,
  deleteUnassignedTaskDayWorkloadByDateAction,
  updateUnassignedTaskDayWorkloadIdByDateAction,
  setSettingsObjectAction,
  setProjectsOfWorkspacesFilterDataListExistsAction,
  updateProfileUrlsFromUserInfoReduxObjectToResourcesGridListingAction,
  updateProfileUrlsFromUserInfoReduxObjectToResourcesFilterListingAction,
  setUnscheduledTasksDataListAction,
  setUnscheduledTasksDataListExistsAction,
  assignUnassignedTaskToResourceAction,
  reassignResourceTaskAction,
} from "./../actions/resources";
import instance from "../instance";
import moment from "moment";
import {
  parseApiTaskRecord,
  parseProjectsApiResponse,
  parseResourcesApiResponse,
  parseResourceOrUnassignedTasksApiResponse,
} from "../../Views/Resources/api-helpers";
import { baseURL, dayComparisonDateFormat } from "./../../Views/Resources/constants";
import { isValidFunction } from "../../Views/Resources/temp-utils";

export const fetchGridResourcesDataListThunk = ({
  pageNo = 1,
  pageSize = 20,
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    const startDate =
      getState().resources.uiGrid.calendarTimelinePeriodStartDate?.momentObj.format(
        dayComparisonDateFormat
      );
    const endDate =
      getState().resources.uiGrid.calendarTimelinePeriodEndDate?.momentObj.format(
        dayComparisonDateFormat
      );
    const workspaceIds = getState().resources.filters.workspaces;
    const projectIds = getState().resources.filters.projects;
    const resourceIds = getState().resources.filters.resources;

    if (!startDate || !endDate) {
      return;
    }

    const params = { pageNo, pageSize };

    const body = Object.assign(
      {},
      workspaceIds && { workspaceId: workspaceIds },
      projectIds && { projectId: projectIds },
      resourceIds && { resourceId: resourceIds },
      startDate && { startDate },
      endDate && { endDate }
    );
    // const params = {};

    return instance(baseURL)
      .post("/api/resources/workspaces/estimates", body, { params })
      .then((response) => ({
        list: response.data?.data || [],
        totalRecords: response.data?.totalRecords,
      }))
      .then(({ list, totalRecords = 201 }) => {
        const calendarDates = getState().resources.data.calendarDatesFlattened;
        const parsed = parseResourcesApiResponse(list, calendarDates);

        if (pageNo === 1) {
          dispatchFn(setGridResourcesDataListAction(parsed));
          dispatchFn(setGridResourcesDataRecordsCountAction(totalRecords));
          dispatchFn(setGridResourcesDataListExistsAction(true));
        } else {
          dispatchFn(appendGridResourcesDataListAction(parsed));
        }

        const userInfoObjectList = getState().profile.data.member.allMembers || [];

        const userInfoListMapped = new Map(
          userInfoObjectList.filter((k) => k.imageUrl).map((k) => [k.userId, k.imageUrl])
        );

        dispatchFn(
          updateProfileUrlsFromUserInfoReduxObjectToResourcesGridListingAction(userInfoListMapped)
        );

        return list;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const fetchGridProjectsDataListThunk = ({
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    // const timelinePeriod = getState().resources.filters.calendarTimelinePeriod;

    // return getProjectsList({ timelinePeriod }).then((list) => {
    //   dispatchFn(setGridProjectsDataListAction(list));
    // });

    // TODO: hard-coded values
    // const startDate = "2022-12-01";
    // const endDate = "2022-12-30";
    const workspaceIds = [
      // local workspace id
      "d5ef44923a7042e4826294db904c20f2",
      // // sample workspace ids
      // "feba5279a3ff48c2a40c4187977e66f9",
      // "19dbdb4bcf3c41cab97b5c71ee920818",
      // "bba8d96263b544078fe31cbb70be0434",
      // "7b15bf0da58a4f30a848bda3df8e4f10",
      // "64139d858e9c47bdb6627b628dce78fc",
    ];

    const startDate =
      getState().resources.uiGrid.calendarTimelinePeriodStartDate?.momentObj.format(
        dayComparisonDateFormat
      );
    const endDate =
      getState().resources.uiGrid.calendarTimelinePeriodEndDate?.momentObj.format(
        dayComparisonDateFormat
      );
    // const workspaceIds = undefined;

    // if (!startDate || !endDate) {
    //   return;
    // }
    // const startDate = undefined;
    // const endDate = undefined;

    // const body = Object.assign({}, workspaceIds && { workspaceId: workspaceIds });
    // const params = Object.assign({}, startDate && { startDate }, endDate && { endDate });
    const body = Object.assign(
      {},
      workspaceIds && { workspaceId: workspaceIds },
      startDate && { startDate },
      endDate && { endDate }
    );
    const params = {};

    return instance(baseURL)
      .post("/api/resources/projects/estimates", body, { params })
      .then((response) => response?.data?.data || [])
      .then((list) => {
        const calendarDates = getState().resources.data.calendarDatesFlattened;
        const parsed = parseProjectsApiResponse(list, calendarDates);

        dispatchFn(setGridProjectsDataListAction(parsed));
        dispatchFn(setGridProjectsDataListExistsAction(true));
        return list;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const fetchGridProjectResourcesDataListThunk = ({
  projectId,
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    if (!projectId) {
      return;
    }

    const startDate =
      getState().resources.uiGrid.calendarTimelinePeriodStartDate?.momentObj.format(
        dayComparisonDateFormat
      );
    const endDate =
      getState().resources.uiGrid.calendarTimelinePeriodEndDate?.momentObj.format(
        dayComparisonDateFormat
      );

    // if (!startDate || !endDate) {
    //   return;
    // }

    const body = Object.assign(
      {},
      projectId && { projectId },
      startDate && { startDate },
      endDate && { endDate }
    );
    const params = {};

    return instance(baseURL)
      .post("/api/resources/project", body, { params })
      .then((response) => response?.data?.data || [])
      .then((list) => {
        const calendarDates = getState().resources.data.calendarDatesFlattened;
        const parsedList = parseResourcesApiResponse(list, calendarDates);

        // dispatchFn(setGridResourcesDataListAction(parsed));
        dispatchFn(setGridProjectResourcesDataListAction(_projectId, parsedList));
        dispatchFn(setGridProjectResourcesDataListExistsAction(_projectId, true));
        return list;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const fetchResourceTasksDataListThunk = ({
  resourceId,
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    const startDate =
      getState().resources.uiGrid.calendarTimelinePeriodStartDate?.momentObj.format(
        dayComparisonDateFormat
      );
    const endDate =
      getState().resources.uiGrid.calendarTimelinePeriodEndDate?.momentObj.format(
        dayComparisonDateFormat
      );
    const workspaceIds = getState().resources.filters.workspaces;
    const projectIds = getState().resources.filters.projects;

    if (!startDate || !endDate || !resourceId) {
      return;
    }

    const body = Object.assign(
      { userId: resourceId },
      workspaceIds && { workspaceId: workspaceIds },
      projectIds && { projectId: projectIds },
      startDate && { startDate },
      endDate && { endDate }
    );
    const params = {};

    return instance(baseURL)
      .post("/api/resources/tasks/estimates", body, { params })
      .then((response) => response?.data?.data || [])
      .then((list) => {
        const parsedList = parseResourceOrUnassignedTasksApiResponse(list);

        dispatchFn(setGridResourceTasksDataListAction(resourceId, parsedList));
        dispatchFn(setGridResourceTasksDataListExistsAction(resourceId, true));
        return list;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const updateResourceOrUnassignedTaskPlannedDatesThunk = ({
  taskId,
  startDate = null,
  endDate = null,
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    const DATE_FORMAT = "MM/DD/YYYY hh:mm A";
    const TIME_FORMAT = "hh:mm A";

    const _startDate = moment(startDate);
    const startDateFormatted = _startDate.format(DATE_FORMAT);
    const startTimeFormatted = _startDate.format(TIME_FORMAT);

    const _endDate = moment(endDate);
    const endDateFormatted = _endDate.format(DATE_FORMAT);
    const endTimeFormatted = _endDate.format(TIME_FORMAT);

    const body = Object.assign(
      {},
      startDate && { startDate: startDateFormatted, startTime: startTimeFormatted },
      endDate && { dueDate: endDateFormatted, dueTime: endTimeFormatted }
    );
    const params = {};

    // return instance(baseURL)
    // return instance("https://devapi.naxxa.io")
    return (
      instance()
        // .patch(`/api/task/ecc1881f273246dabc6dc8b60c60d93a`, body, { params })
        .patch(`/api/task/${taskId}`, body, { params })
        .then((response) => response.status === 200 && response.data)
        .then((data) => {
          if (!data.success || data.recordsEffected <= 0) {
            return;
          }

          const affectedEntity = data.entity;
          return affectedEntity;
        })
        .then((updatedTask) => {
          const affectedTask = parseApiTaskRecord(updatedTask);

          return affectedTask;
        })
        .then((data) => {
          if (isValidFunction(successCallback)) {
            successCallback(data);
          }
        })
        .catch((error) => {
          console.error("exception error:", error);
          if (isValidFunction(failureCallback)) {
            failureCallback(error.response || error);
          }
        })
        .finally(() => {
          if (isValidFunction(finallyCallback)) {
            finallyCallback();
          }
        })
    );
  };
};

export const updateResourceTaskDayWorkloadThunk = ({
  // taskId,
  taskDayId,
  updatedWorkloadHours,
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    if (updatedWorkloadHours < 0) {
      return;
    }

    const hours = Math.floor(updatedWorkloadHours);
    const minutes = parseInt((updatedWorkloadHours - hours) * 60);

    const body = Object.assign({}, { hours, mins: minutes });
    const params = {};

    return (
      instance(baseURL)
        // instance("https://devapi.naxxa.io")
        // instance()
        .patch(`/api/task/estimate/${taskDayId}`, body, { params })
        // .patch(`/api/task/ecc1881f273246dabc6dc8b60c60d93a`, body, { params })
        .then((response) => response.status === 200 && response.data)
        .then((data) => {
          if (!data.message || !data.message?.toLowerCase().includes("success")) {
            return;
          }

          // if (!data.success || data.recordsEffected <= 0) {
          //   return;
          // }

          // const affectedEntity = data.entity;
          // return affectedEntity;
        })
        // .then((updatedTask) => {
        //   const affectedTask = parseApiTaskRecord(updatedTask);

        //   // dispatchFn(updateResourceTaskAction(resourceId, taskId, affectedTask));
        //   return affectedTask;
        // })
        .then((data) => {
          if (isValidFunction(successCallback)) {
            successCallback(data);
          }
        })
        .catch((error) => {
          console.error("exception error:", error);
          if (isValidFunction(failureCallback)) {
            failureCallback(error.response || error);
          }
        })
        .finally(() => {
          if (isValidFunction(finallyCallback)) {
            finallyCallback();
          }
        })
    );
  };
};

export const deleteResourceTaskDayWorkloadThunk = ({
  resourceId = null,
  taskId,
  taskDayId,
  dayDate,
  isUnassignedTask = false,
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    if (!taskDayId) {
      return;
    }

    const body = {};
    const params = {};

    return (
      instance(baseURL)
        // instance("https://devapi.naxxa.io")
        // instance()
        .delete(`/api/task/estimate/${taskDayId}`, body, { params })
        .then((axiosResponse) => axiosResponse.status === 200 && axiosResponse.data)
        .then((response) => {
          if (!response.success || !response.message?.toLowerCase().includes("deleted")) {
            return;
          }

          return response;
        })
        .then((response) => {
          // sample response
          //   {
          //     "statusCode": "S001",
          //     "message": "estimation deleted",
          //     "success": true,
          //     "data": []
          // }

          if (isUnassignedTask) {
            dispatchFn(deleteUnassignedTaskDayWorkloadByDateAction(taskId, dayDate));
          } else {
            dispatchFn(deleteResourceTaskDayWorkloadByDateAction(resourceId, taskId, dayDate));
          }
        })
        .then((data) => {
          if (isValidFunction(successCallback)) {
            successCallback(data);
          }
        })
        .catch((error) => {
          console.error("exception error:", error);
          if (isValidFunction(failureCallback)) {
            failureCallback(error.response || error);
          }
        })
        .finally(() => {
          if (isValidFunction(finallyCallback)) {
            finallyCallback();
          }
        })
    );
  };
};

export const createResourceTaskDayWorkloadThunk = ({
  workspaceId,
  resourceId = null,
  taskId,
  dateMoment,
  updatedWorkloadHours,
  isUnassignedTask = false,
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    if (updatedWorkloadHours < 0 || !dateMoment) {
      return;
    }

    // TODO: hard-coded values
    // const workspaceId =
    //   // local workspace id
    //   "d5ef44923a7042e4826294db904c20f2";
    // const workspaceId = getState().profile.data.loggedInTeam;

    // // TODO: here make the resource-id required if not unassigned
    // if (!isUnassignedTask && (!resourceId)) {
    //   return;
    // }

    const hours = Math.floor(updatedWorkloadHours);
    const minutes = parseInt((updatedWorkloadHours - hours) * 60);

    const body = Object.assign(
      {},
      { hours, mins: minutes, date: dateMoment.toISOString() },
      { workspaceId, taskId, resourceId, isUnassignedTask }
    );
    const params = {};

    return (
      instance(baseURL)
        // instance("https://devapi.naxxa.io")
        // instance()
        .post(`/api/task/estimate`, body, { params })
        .then((axiosResponse) => axiosResponse.status === 200 && axiosResponse.data)
        .then((response) => {
          if (!response.success || !response.message?.toLowerCase().includes("created")) {
            return;
          }

          return response?.data;
        })
        .then((data) => {
          // TODO: update in redux state
          // sample response
          //   {
          //     "statusCode": "S001",
          //     "message": "estimation created",
          //     "success": true,
          //     "data": {
          //         "CreatedDate": "2023-01-30T08:33:25.511Z",
          //         "UpdatedDate": null,
          //         "CreatedBy": "6322f487533cc51f534caa76",
          //         "OwnedBy": "6322f487533cc51f534caa76",
          //         "Version": 1,
          //         "UpdatedBy": null,
          //         "WorkSpaceId": "d5ef44923a7042e4826294db904c20f2",
          //         "TeamId": "3f03f1e0711e45179862d1a3a80170b5",
          //         "DataLocation": "UK",
          //         "TaskId": "test123",
          //         "ResourceId": "test123",
          //         "EstimatedHours": 123,
          //         "EstimatedMins": 123,
          //         "EstimationDate": "0123-01-01T00:00:00.000Z",
          //         "_id": "63d8e6e0871b0c543de7a0ff",
          //         "__v": 0
          //     }
          // }

          // TODO: here set the state to updated day workload
          if (isUnassignedTask) {
            dispatchFn(updateUnassignedTaskDayWorkloadIdByDateAction(taskId, dateMoment, data._id));
          } else {
            dispatchFn(
              updateResourceTaskDayWorkloadIdByDateAction(resourceId, taskId, dateMoment, data._id)
            );
          }

          // if (!data.success || data.recordsEffected <= 0) {
          //   return;
          // }
          // const affectedEntity = data.entity;
          // return affectedEntity;
        })
        .then((data) => {
          if (isValidFunction(successCallback)) {
            successCallback(data);
          }
        })
        .catch((error) => {
          console.error("exception error:", error);
          if (isValidFunction(failureCallback)) {
            failureCallback(error.response || error);
          }
        })
        .finally(() => {
          if (isValidFunction(finallyCallback)) {
            finallyCallback();
          }
        })
    );
  };
};

export const fetchFilterProjectsOfWorkspacesDataListThunk = ({
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    const workspaceIds = getState().resources.filters.workspaces;

    const body = Object.assign({}, workspaceIds?.length > 0 && { workspaceId: workspaceIds });
    const params = {};

    return instance(baseURL)
      .post("/api/common/workspaces/projects", body, { params })
      .then((response) => {
        return response.data || [];
      })
      .then((list) => {
        const parsed = list.map((k) => ({
          id: k.projectId,
          name: k.projectName,
          workspaceId: k.workspaceId,
        }));

        dispatchFn(setProjectsOfWorkspacesFilterDataListAction(parsed));
        dispatchFn(setProjectsOfWorkspacesFilterDataListExistsAction(!!parsed));

        return parsed;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const fetchFilterResourcesOfWorkspacesDataListThunk = ({
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    const workspaceIds = getState().resources.filters.workspaces;

    const body = Object.assign({}, workspaceIds?.length > 0 && { workspaceId: workspaceIds });
    const params = {};

    return instance(baseURL)
      .post("/api/common/workspaces/resources", body, { params })
      .then((response) => {
        return response.data || [];
      })
      .then((list) => {
        const parsed = list.map((k) => ({
          workspaceId: k.workspaceId,
          id: k.userId,
          fullName: k.fullName,
          email: k.email,
          imageUrl: k.pictureUrl,
          designation: k.designation,
        }));

        dispatchFn(setResourcesOfWorkspacesFilterDataListAction(parsed));

        const userInfoObjectList = getState().profile.data.member.allMembers || [];

        const userInfoListMapped = new Map(
          userInfoObjectList.filter((k) => k.imageUrl).map((k) => [k.userId, k.imageUrl])
        );

        dispatchFn(
          updateProfileUrlsFromUserInfoReduxObjectToResourcesFilterListingAction(userInfoListMapped)
        );

        return parsed;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const fetchUnassignedTasksDataListThunk = ({
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    const startDate =
      getState().resources.uiGrid.calendarTimelinePeriodStartDate?.momentObj.format(
        dayComparisonDateFormat
      );
    const endDate =
      getState().resources.uiGrid.calendarTimelinePeriodEndDate?.momentObj.format(
        dayComparisonDateFormat
      );
    const workspaceIds = getState().resources.filters.workspaces;
    const projectIds = getState().resources.filters.projects;

    if (!startDate || !endDate) {
      return;
    }

    const body = Object.assign(
      { startDate, endDate },
      workspaceIds && { workspaceId: workspaceIds },
      projectIds && { projectId: projectIds }
    );
    const params = {};

    return instance(baseURL)
      .post("/api/task/unassigned", body, { params })
      .then((response) => response?.data?.data || [])
      .then((list) => {
        const parsedList = parseResourceOrUnassignedTasksApiResponse(list);

        dispatchFn(setUnassignedTasksDataListAction(parsedList));
        dispatchFn(setUnassignedTasksDataListExistsAction(true));
        return list;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const fetchUserSettingsThunk = ({
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    const params = {};

    return instance(baseURL)
      .get("/api/resources/settings", { params })
      .then((axiosResponse) => axiosResponse.status === 200 && axiosResponse.data)
      .then((response) => {
        if (!response.success || !response.message?.toLowerCase().includes("found")) {
          return;
        }

        return response?.data;
      })
      .then((data) => {
        if (!data._id) {
          return;
        }

        const parsed = {
          settingsObjectId: data._id,
          showTaskNameWithTimeline: data.IsShowTaskName,
          taskTimelineColor: data.TaskColor,
        };

        dispatchFn(setSettingsObjectAction(parsed));

        return parsed;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const createOrUpdateUserSettingsThunk = ({
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    const settings = getState().resources.settings;
    const _isUpdate = settings.settingsObjectId;

    const params = {};
    const body = {
      TaskColor: settings.taskTimelineColor,
      IsShowTaskName: settings.showTaskNameWithTimeline,
    };

    // Updating setting object
    if (_isUpdate) {
      body._id = settings.settingsObjectId;

      return instance(baseURL)
        .put("/api/resources/settings", body, { params })
        .then((axiosResponse) => axiosResponse.status === 200 && axiosResponse.data)
        .then((response) => {
          // Sample Response
          //  {
          //   statusCode: "S001",
          //   message: "Updated",
          //   success: true,
          //   data: null,
          // };
          if (!response.success || !response.message?.toLowerCase().includes("updated")) {
            return;
          }

          return response?.data;
        })
        .then((data) => {
          if (isValidFunction(successCallback)) {
            successCallback(data);
          }
        })
        .catch((error) => {
          console.error("exception error:", error);
          if (isValidFunction(failureCallback)) {
            failureCallback(error.response || error);
          }
        })
        .finally(() => {
          if (isValidFunction(finallyCallback)) {
            finallyCallback();
          }
        });
    }

    // Creating new setting object
    return instance(baseURL)
      .post("/api/resources/settings", body, { params })
      .then((axiosResponse) => axiosResponse.status === 200 && axiosResponse.data)
      .then((response) => {
        // Sample Response
        // {
        //   statusCode: "S001",
        //   message: "Created",
        //   success: true,
        //   data: {
        //     CreatedDate: "2023-02-22T06:36:06.559Z",
        //     UpdatedDate: null,
        //     CreatedBy: "6246f11d900959f0208539c3",
        //     UpdatedBy: null,
        //     TeamId: "bfc41e9ba04b41dbb601b792948ce942",
        //     Location: "US",
        //     IsShowTaskName: true,
        //     TaskColor: "status",
        //     _id: "63fc73209f6fb0de437dd06c",
        //     __v: 0,
        //   },
        // };

        if (!response.success || !response.message?.toLowerCase().includes("created")) {
          return;
        }

        return response?.data;
      })
      .then((data) => {
        const parsed = {
          settingsObjectId: data._id,
          showTaskNameWithTimeline: data.IsShowTaskName,
          taskTimelineColor: data.TaskColor,
        };

        dispatchFn(setSettingsObjectAction(parsed));

        return parsed;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const fetchUnscheduledTasksDataListThunk = ({
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    const stateFilters = getState().resources.filters;

    const workspaceIds = stateFilters.workspaces;
    const projectIds = stateFilters.projects;

    const body = { workspaceId: workspaceIds, projectId: projectIds };
    const params = {};

    return instance(baseURL)
      .post("/api/task/unscheduled", body, { params })
      .then((axiosResponse) => axiosResponse.status === 200 && axiosResponse.data)
      .then((response) => {
        // Sample Response
        // {
        //   data: [
        //     {
        //       taskTitle: "subTaskFN",
        //       startDate: "2023-02-20T07:39:00.000Z",
        //       endDate: null,
        //       projectId: "d775898a653d4052af530ba9723a17cf",
        //       projectName: "TestProject1",
        //       createdDate: "2023-02-22T07:37:17.290Z",
        //       updatedDate: "2023-02-24T06:24:22.649Z",
        //       createdBy: "6322f487533cc51f534caa76",
        //       ownedBy: null,
        //       workspaceId: "feba5279a3ff48c2a40c4187977e66f9",
        //       taskId: "01bda8e479164375b602938a250bdabe",
        //       teamId: "c5fe3eabba304f87a27585da12ae1188",
        //       estimates: [],
        //     },
        //   ],
        // }

        // Note: API missing these properties
        // if (!response.success || !response.message?.toLowerCase().includes("created")) {
        //   return;
        // }

        return response?.data;
      })
      .then((data = []) => {
        dispatchFn(setUnscheduledTasksDataListAction(data));
        dispatchFn(setUnscheduledTasksDataListExistsAction(!!data));

        return data;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const assignUnassignedTaskToResourceThunk = ({
  toResourceId = null,
  fromIndex = null,
  toIndex = null,
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    if (!toResourceId || isNaN(fromIndex) || isNaN(toIndex) || fromIndex < 0 || toIndex < 0) {
      return;
    }

    const unassignedTasksDataList = getState().resources.data.unassignedTasksDataList;

    const task = unassignedTasksDataList.at(fromIndex);

    if (!task) {
      return;
    }

    dispatchFn(assignUnassignedTaskToResourceAction(toResourceId, fromIndex, toIndex));

    const body = {
      workspaceId: [task.workspaceId],
      taskId: task.id,
      // removeAssignee: removeResourceId,
      // addAssignee: addResourceId,
      // removeAssignee: null,
      removeAssignee: "",
      addAssignee: toResourceId,
    };
    const params = {};

    return instance(baseURL)
      .post(`/api/resources/tasks/assign`, body, { params })
      .then((response) => response.status === 200 && (response.data || {}))
      .then((data) => {
        if (!data.success) {
          throw new Error(data?.message);
        }

        return data.data;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error?.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};

export const reassignResourceTaskThunk = ({
  fromResourceId = null,
  toResourceId = null,
  fromIndex = null,
  toIndex = null,
  successCallback = null,
  failureCallback = null,
  finallyCallback = null,
} = {}) => {
  return (dispatchFn, getState) => {
    if (
      !fromResourceId ||
      !toResourceId ||
      isNaN(fromIndex) ||
      isNaN(toIndex) ||
      fromIndex < 0 ||
      toIndex < 0
    ) {
      return;
    }
    const reduxData = getState().resources.data;

    const _fromResourceTasks = reduxData.resourceTasksDataList.has(fromResourceId)
      ? reduxData.resourceTasksDataList.get(fromResourceId)
      : [];

    const task = _fromResourceTasks.at(fromIndex);

    if (!task) {
      return;
    }

    dispatchFn(reassignResourceTaskAction(fromResourceId, toResourceId, fromIndex, toIndex));

    const body = {
      workspaceId: [task.workspaceId],
      taskId: task.id,
      removeAssignee: fromResourceId,
      addAssignee: toResourceId,
    };
    const params = {};

    return instance(baseURL)
      .post(`/api/resources/tasks/assign`, body, { params })
      .then((response) => response.status === 200 && (response.data || {}))
      .then((data) => {
        if (!data.success) {
          throw new Error(data?.message);
        }

        return data.data;
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error?.response || error);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };
};
