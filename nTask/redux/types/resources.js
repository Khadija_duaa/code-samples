// RESOURCE MANAGEMENT
export const DUMMY = "DUMMY";

// data

export const SET_GRID_CALENDAR_DATES = "SET_GRID_CALENDAR_DATES";
export const RESET_GRID_CALENDAR_DATES = "RESET_GRID_CALENDAR_DATES";

export const SET_GRID_CALENDAR_DATES_FLATTENED = "SET_GRID_CALENDAR_DATES_FLATTENED";
export const RESET_GRID_CALENDAR_DATES_FLATTENED = "RESET_GRID_CALENDAR_DATES_FLATTENED";

export const SET_GRID_RESOURCES_DATA_LIST = "SET_GRID_RESOURCES_DATA_LIST";
export const APPEND_GRID_RESOURCES_DATA_LIST = "APPEND_GRID_RESOURCES_DATA_LIST";
export const RESET_GRID_RESOURCES_DATA_LIST = "RESET_GRID_RESOURCES_DATA_LIST";

export const UPDATE_RESOURCE_TOTAL_CAPACITY = "UPDATE_RESOURCE_TOTAL_CAPACITY";

export const SET_GRID_PROJECTS_DATA_LIST = "SET_GRID_PROJECTS_DATA_LIST";
export const RESET_GRID_PROJECTS_DATA_LIST = "RESET_GRID_PROJECTS_DATA_LIST";

export const SET_GRID_PROJECT_RESOURCES_DATA_LIST = "SET_GRID_PROJECT_RESOURCES_DATA_LIST";
// export const SET_GRID_PROJECT_RESOURCES_DATA_LIST_LOADING = "SET_GRID_PROJECT_RESOURCES_DATA_LIST_LOADING";
export const APPEND_GRID_PROJECT_RESOURCES_DATA_LIST = "APPEND_GRID_PROJECT_RESOURCES_DATA_LIST";
export const UNSET_GRID_PROJECT_RESOURCES_DATA_LIST = "UNSET_GRID_PROJECT_RESOURCES_DATA_LIST";
export const RESET_GRID_PROJECT_RESOURCES_DATA_LIST = "RESET_GRID_PROJECT_RESOURCES_DATA_LIST";

export const SET_GRID_RESOURCE_TASKS_DATA_LIST = "SET_GRID_RESOURCE_TASKS_DATA_LIST";
export const UNSET_GRID_RESOURCE_TASKS_DATA_LIST = "UNSET_GRID_RESOURCE_TASKS_DATA_LIST";
export const RESET_GRID_RESOURCE_TASKS_DATA_LIST = "RESET_GRID_RESOURCE_TASKS_DATA_LIST";

export const SET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST =
  "SET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST";
export const UNSET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST =
  "UNSET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST";
export const RESET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST =
  "RESET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST";

export const SET_UNASSIGNED_TASKS_DATA_LIST = "SET_UNASSIGNED_TASKS_DATA_LIST";
export const RESET_UNASSIGNED_TASKS_DATA_LIST = "RESET_UNASSIGNED_TASKS_DATA_LIST";

export const UPDATE_RESOURCE_TASK = "UPDATE_RESOURCE_TASK";
export const UPDATE_RESOURCE_TASK_DAY_WORKLOAD = "UPDATE_RESOURCE_TASK_DAY_WORKLOAD";
export const UPDATE_RESOURCE_TASK_DAY_WORKLOAD_BY_DATE =
  "UPDATE_RESOURCE_TASK_DAY_WORKLOAD_BY_DATE";
export const UPDATE_RESOURCE_TASK_DAY_WORKLOAD_ID_BY_DATE =
  "UPDATE_RESOURCE_TASK_DAY_WORKLOAD_ID_BY_DATE";
export const DELETE_RESOURCE_TASK_DAY_WORKLOAD_BY_DATE =
  "DELETE_RESOURCE_TASK_DAY_WORKLOAD_BY_DATE";
export const UPDATE_RESOURCE_TASK_WORKLOADS = "UPDATE_RESOURCE_TASK_WORKLOADS";
export const UPDATE_RESOURCE_TASK_PLANNED_DATES = "UPDATE_RESOURCE_TASK_PLANNED_DATES";
export const UPDATE_RESOURCE_TASK_PLANNED_START_DATE = "UPDATE_RESOURCE_TASK_PLANNED_START_DATE";
export const UPDATE_RESOURCE_TASK_PLANNED_END_DATE = "UPDATE_RESOURCE_TASK_PLANNED_END_DATE";

export const REASSIGN_RESOURCE_TASK = "REASSIGN_RESOURCE_TASK";

export const UPDATE_PROJECT_RESOURCE_TASK_WORKLOADS = "UPDATE_PROJECT_RESOURCE_TASK_WORKLOADS";

export const UPDATE_UNASSIGNED_TASK_DAY_WORKLOAD = "UPDATE_UNASSIGNED_TASK_DAY_WORKLOAD";
export const UPDATE_UNASSIGNED_TASK_DAY_WORKLOAD_ID_BY_DATE =
  "UPDATE_UNASSIGNED_TASK_DAY_WORKLOAD_ID_BY_DATE";
export const DELETE_UNASSIGNED_TASK_DAY_WORKLOAD_BY_DATE =
  "DELETE_UNASSIGNED_TASK_DAY_WORKLOAD_BY_DATE";
export const UPDATE_UNASSIGNED_TASK_WORKLOADS = "UPDATE_UNASSIGNED_TASK_WORKLOADS";
export const UPDATE_UNASSIGNED_TASK_PLANNED_DATES = "UPDATE_UNASSIGNED_TASK_PLANNED_DATES";

export const ASSIGN_UNASSIGNED_TASK_TO_RESOURCE = "ASSIGN_UNASSIGNED_TASK_TO_RESOURCE";

export const SET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST =
  "SET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST";
export const SET_RESOURCES_OF_WORKSPACES_FILTER_DATA_LIST =
  "SET_RESOURCES_OF_WORKSPACES_FILTER_DATA_LIST";

export const SET_UNSCHEDULED_TASKS_DATA_LIST = "SET_UNSCHEDULED_TASKS_DATA_LIST";
export const RESET_UNSCHEDULED_TASKS_DATA_LIST = "RESET_UNSCHEDULED_TASKS_DATA_LIST";

// data-records-count

export const SET_GRID_RESOURCES_DATA_RECORDS_COUNT = "SET_GRID_RESOURCES_DATA_RECORDS_COUNT";
export const RESET_GRID_RESOURCES_DATA_RECORDS_COUNT = "RESET_GRID_RESOURCES_DATA_RECORDS_COUNT";

export const SET_GRID_PROJECT_RESOURCES_DATA_RECORDS_COUNT =
  "SET_GRID_PROJECT_RESOURCES_DATA_RECORDS_COUNT";
export const RESET_GRID_PROJECT_RESOURCES_DATA_RECORDS_COUNT =
  "RESET_GRID_PROJECT_RESOURCES_DATA_RECORDS_COUNT";

// data-flags

export const SET_GRID_RESOURCES_DATA_LIST_EXISTS = "SET_GRID_RESOURCES_DATA_LIST_EXISTS";
export const RESET_GRID_RESOURCES_DATA_LIST_EXISTS = "RESET_GRID_RESOURCES_DATA_LIST_EXISTS";

export const SET_GRID_PROJECTS_DATA_LIST_EXISTS = "SET_GRID_PROJECTS_DATA_LIST_EXISTS";
export const RESET_GRID_PROJECTS_DATA_LIST_EXISTS = "RESET_GRID_PROJECTS_DATA_LIST_EXISTS";

export const SET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS =
  "SET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS";
export const UNSET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS =
  "UNSET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS";
export const RESET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS =
  "RESET_GRID_PROJECT_RESOURCES_DATA_LIST_EXISTS";

export const SET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS = "SET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS";
export const UNSET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS =
  "UNSET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS";
export const RESET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS =
  "RESET_GRID_RESOURCE_TASKS_DATA_LIST_EXISTS";

export const SET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS =
  "SET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS";
export const UNSET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS =
  "UNSET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS";
export const RESET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS =
  "RESET_GRID_PROJECT_RESOURCE_TASKS_DATA_LIST_EXISTS";

export const SET_UNASSIGNED_TASKS_DATA_LIST_EXISTS = "SET_UNASSIGNED_TASKS_DATA_LIST_EXISTS";
export const RESET_UNASSIGNED_TASKS_DATA_LIST_EXISTS = "RESET_UNASSIGNED_TASKS_DATA_LIST_EXISTS";

export const SET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST_EXISTS =
  "SET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST_EXISTS";
export const RESET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST_EXISTS =
  "RESET_PROJECTS_OF_WORKSPACES_FILTER_DATA_LIST_EXISTS";

export const SET_UNSCHEDULED_TASKS_DATA_LIST_EXISTS = "SET_UNSCHEDULED_TASKS_DATA_LIST_EXISTS";
export const RESET_UNSCHEDULED_TASKS_DATA_LIST_EXISTS = "RESET_UNSCHEDULED_TASKS_DATA_LIST_EXISTS";

// ui-grid

export const SET_UI_SIDEBAR_COLLAPSED = "SET_UI_SIDEBAR_COLLAPSED";
export const RESET_UI_SIDEBAR_COLLAPSED = "RESET_UI_SIDEBAR_COLLAPSED";

export const SET_DAY_PROGRESS_HOURGLASS_WIDTH = "SET_DAY_PROGRESS_HOURGLASS_WIDTH";
export const RESET_DAY_PROGRESS_HOURGLASS_WIDTH = "RESET_DAY_PROGRESS_HOURGLASS_WIDTH";

export const SET_GRID_COLUMNS_COUNT = "SET_GRID_COLUMNS_COUNT";
export const RESET_GRID_COLUMNS_COUNT = "RESET_GRID_COLUMNS_COUNT";

export const SET_WORKLOAD_VIEW_TYPE = "SET_WORKLOAD_VIEW_TYPE";
export const RESET_WORKLOAD_VIEW_TYPE = "RESET_WORKLOAD_VIEW_TYPE";

export const SET_CALENDAR_TIMELINE_PERIOD_START_DATE = "SET_CALENDAR_TIMELINE_PERIOD_START_DATE";
export const RESET_CALENDAR_TIMELINE_PERIOD_START_DATE =
  "RESET_CALENDAR_TIMELINE_PERIOD_START_DATE";

export const SET_CALENDAR_TIMELINE_PERIOD_END_DATE = "SET_CALENDAR_TIMELINE_PERIOD_END_DATE";
export const RESET_CALENDAR_TIMELINE_PERIOD_END_DATE = "RESET_CALENDAR_TIMELINE_PERIOD_END_DATE";

export const INCREASE_OPEN_COLLAPSIBLES_COUNT = "INCREASE_OPEN_COLLAPSIBLES_COUNT";
export const DECREASE_OPEN_COLLAPSIBLES_COUNT = "DECREASE_OPEN_COLLAPSIBLES_COUNT";

// settings

export const SET_SETTINGS_OBJECT = "SET_SETTINGS_OBJECT";
export const RESET_SETTINGS_OBJECT = "RESET_SETTINGS_OBJECT";

export const SET_SETTINGS_SHOW_TASK_NAME_WITH_TIMELINE =
  "SET_SETTINGS_SHOW_TASK_NAME_WITH_TIMELINE";
export const RESET_SETTINGS_SHOW_TASK_NAME_WITH_TIMELINE =
  "RESET_SETTINGS_SHOW_TASK_NAME_WITH_TIMELINE";

export const SET_SETTINGS_TASK_COLOR = "SET_SETTINGS_TASK_COLOR";
export const RESET_SETTINGS_TASK_COLOR = "RESET_SETTINGS_TASK_COLOR";

// filters

export const SET_FILTER_CALENDAR_TIMELINE_PERIOD = "SET_FILTER_CALENDAR_TIMELINE_PERIOD";
export const RESET_FILTER_CALENDAR_TIMELINE_PERIOD = "RESET_FILTER_CALENDAR_TIMELINE_PERIOD";

export const SET_FILTER_PROGRESS_HOURGLASS_UNIT = "SET_FILTER_PROGRESS_HOURGLASS_UNIT";
export const RESET_FILTER_PROGRESS_HOURGLASS_UNIT = "RESET_FILTER_PROGRESS_HOURGLASS_UNIT";

export const SET_FILTER_WORKSPACES = "SET_FILTER_WORKSPACES";
export const RESET_FILTER_WORKSPACES = "RESET_FILTER_WORKSPACES";

export const SET_FILTER_RESOURCES = "SET_FILTER_RESOURCES";
export const RESET_FILTER_RESOURCES = "RESET_FILTER_RESOURCES";

export const SET_FILTER_PROJECTS = "SET_FILTER_PROJECTS";
export const RESET_FILTER_PROJECTS = "RESET_FILTER_PROJECTS";

// extra

export const UPDATE_PROFILE_URLS_FROM_USER_INFO_REDUX_OBJECT_TO_RESOURCES_GRID_LISTING =
  "UPDATE_PROFILE_URLS_FROM_USER_INFO_REDUX_OBJECT_TO_RESOURCES_GRID_LISTING";
export const UPDATE_PROFILE_URLS_FROM_USER_INFO_REDUX_OBJECT_TO_RESOURCES_FILTER_LISTING =
  "UPDATE_PROFILE_URLS_FROM_USER_INFO_REDUX_OBJECT_TO_RESOURCES_FILTER_LISTING";
