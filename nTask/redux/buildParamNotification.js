import { store } from "../index";
import {
  notificationBarStateAction,
  showHideNotificationBarAction,
} from "./actions/notificationBar";

const notificationObj = {
  show: true,
  buttonText: "Refresh",
  primaryAction: () => {
    location.reload();
  },
  message:
    'We just rolled out a new update for you. Please refresh your browser window or click "Refresh" button for changes to take effect!',
};

const buildParamNotification = (response) => {
  let email = store.getState().profile.data
    ? store.getState().profile.data.email
    : "";
  if (email !== "google@ryda.com.au") {
    if(response.headers.buildparam == -1){  //less than 0 build param comes for non-dot net api's
      return
    }
    let buildParam = localStorage.getItem("buildParam");
    const loginApiBuildParamCheck =
      !response.headers.buildparam && buildParam !== response.data.buildparam; // In case of login API build params are not exposed in headers so we get it in response data so we check it accordingly
    // in case of all other API's and in case of build params less than 0
    const buildParamCheck =
      (response.headers.buildparam && buildParam !== response.headers.buildparam);
    const buildparam = response.headers.buildparam
      ? response.headers.buildparam
      : response.data.buildparam;
    if (buildParam && (buildParamCheck || loginApiBuildParamCheck)) {
      store.dispatch(notificationBarStateAction(true));
      store.dispatch(showHideNotificationBarAction(notificationObj));
      localStorage.setItem("buildParam", buildparam);
    } else {
      localStorage.setItem("buildParam", buildparam);
    }
  }
};

export default buildParamNotification;
