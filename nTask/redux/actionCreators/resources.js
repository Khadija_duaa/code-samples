import {
  resetGridResourcesDataListAction,
  resetGridProjectsDataListAction,
  setFilterCalendarTimelinePeriodAction,
  setFilterProgressHourglassUnitAction,
  resetGridResourceTasksDataListAction,
  resetGridProjectResourceTasksDataListAction,
  resetGridProjectResourcesDataListAction,
  resetGridProjectsDataListExistsAction,
  resetGridResourcesDataListExistsAction,
  resetGridProjectResourcesDataListExistsAction,
  resetGridProjectResourceTasksDataListExistsAction,
  resetGridResourceTasksDataListExistsAction,
  resetUnassignedTasksDataListAction,
  resetUnassignedTasksDataListExistsAction,
} from "./../actions/resources";
import { ResourceWorkloadUnits } from "./../../utils/constants/ResourceWorkloadUnits";
import { CalendarTimelinePeriods } from "./../../utils/constants/CalendarTimelinePeriods";

export const resetGridDataActionCreator = () => {
  return (dispatchFn) => {
    dispatchFn(resetGridProjectsDataListAction());
    dispatchFn(resetGridResourcesDataListAction());
    dispatchFn(resetGridProjectResourcesDataListAction());

    dispatchFn(resetGridProjectsDataListExistsAction());
    dispatchFn(resetGridResourcesDataListExistsAction());
    dispatchFn(resetGridProjectResourcesDataListExistsAction());
  };
};

export const setInitialFiltersActionCreator = () => {
  return (dispatchFn) => {
    dispatchFn(setFilterCalendarTimelinePeriodAction(CalendarTimelinePeriods.week.value));
    dispatchFn(setFilterProgressHourglassUnitAction(ResourceWorkloadUnits.hour.value));
  };
};

export const resetGridTasksDataActionCreator = () => {
  return (dispatchFn) => {
    dispatchFn(resetGridResourceTasksDataListAction());
    dispatchFn(resetGridProjectResourceTasksDataListAction());

    dispatchFn(resetGridResourceTasksDataListExistsAction());
    dispatchFn(resetGridProjectResourceTasksDataListExistsAction());

    dispatchFn(resetUnassignedTasksDataListAction());
    dispatchFn(resetUnassignedTasksDataListExistsAction());
  };
};
