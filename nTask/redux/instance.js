import axios from "axios";
import { logoutUser } from "./actions/logout";
import { ShowHideNoInternetView } from "./actions/noInternet";
import buildParamNotification from "./buildParamNotification";
import { store } from "../index";
import helper from "../helper/index";


export const CancelToken = axios.CancelToken;

function instance(url) {
  const AUTH_TOKEN = helper.getToken() || null;
  let reqInstance = axios.create({
    baseURL: url ? url : ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL ,
    headers: {
      Authorization: AUTH_TOKEN,
      ConnectionId: $.connection ? $.connection.hub.id : "", // Ternary operator used Incase the signalR file is dropped due to any network issue and there is no connection established
    },
    // timeout: 10000, // Kazim make this comment
  });
  reqInstance.interceptors.response.use(
    function(response) {
      buildParamNotification(response);

      return response;
    },
    function(response) {
      if (response.message == "Network Error") {
        axios
          .get(
            "https://content.googleapis.com/discovery/v1/apis/analytics/v3/rest"
          )
          .catch((response) => {
            store.dispatch(ShowHideNoInternetView(true));
          });
      }
      if (response.response && response.response.status === 401 && !url) {
        localStorage.removeItem("token");
        localStorage.removeItem("zapkey");
        localStorage.removeItem("userName");
        localStorage.removeItem("CompanyInfo");
        localStorage.removeItem("CreateProfile");
        localStorage.removeItem("CreateWorkspace");
        localStorage.removeItem("InviteMembers");
        localStorage.removeItem("Onboarding");
        localStorage.removeItem("teamId");
        localStorage.removeItem("recommendEmails");
        localStorage.removeItem("timesheetEmails");
        $.connection.hub.stop();
        store.dispatch(logoutUser());
        window.location.href = window.location.origin + "/account/login";
      }
      return Promise.reject(response);
    }
  );

  return reqInstance;
}

export default instance;
