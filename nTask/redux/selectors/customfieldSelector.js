// Reselect selector
// Takes a list of custom fields and generates
// the key value pair of fieldId's containig field object

import { createSelector } from "reselect";

// Select function to pick of the custom fields from state
const fieldSelector = state => state.customFields.data;

const getCustomFieldObject = fieldSelector => {
    /** function return the oject with key value pair of fieldId and its object */
  let customfieldObj = fieldSelector.reduce((result, currentValue) => {
    result[currentValue.fieldId] = currentValue;
    return result;
  }, {});
  return customfieldObj;
};

export default createSelector(fieldSelector, getCustomFieldObject);
