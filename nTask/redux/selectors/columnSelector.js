// Reselect selector
// Take the columns of all modules and returns the object
// containing the sorted general columns and sections columns as well

import { createSelector } from "reselect";
import isNull from "lodash/isNull";
import includes from "lodash/includes";
import differenceBy from "lodash/differenceBy";
import { getActiveFields } from "../../helper/getActiveCustomFields";

// Select function to pick tasks, issues, meetings, risks columns from state
const taskColumnSelector = state =>
  state.columns.task &&
  state.columns.task.sort((pv, cv) => {
    return pv.position - cv.position;
  });
const profileSelector = state => state.profile.data;
const customfieldSelector = state => state.customFields.data;
const issueColumnSelector = state =>
  state.columns.issue &&
  state.columns.issue.sort((pv, cv) => {
    return pv.position - cv.position;
  });

 const meetingColumnSelector = state =>
  state.columns.meeting && state.columns.meeting.sort((pv, cv) => {
    return pv.position - cv.position;
   });
const riskColumnSelector = state =>
  state.columns.risk && state.columns.risk.sort((pv, cv) => {
    return pv.position - cv.position;
  });

const projectColumnSelector = state =>
  state.columns.project &&
  state.columns.project.sort((pv, cv) => {
    return pv.position - cv.position;
  });

const systemColumns = (columns) => {
  /** system columns doesnot contain the field type and field id */
  return columns?.filter(c => c.isSystem) || [];
};

const getChildComponents = (id, columns) => {
  let childCmp = columns.reduce((res, cv) => {
    let isExist = cv.sectionId ? cv.sectionId.find(ele => ele === id) : false;
    if (isExist) res.push(cv);
    return res;
  }, []);
  return childCmp;
};

const nonSectionColumns = (columns, profile, feature, allCustomfields) => {
   //in case there are no columns just return empty array instead of null
  if(!columns){
    return []
  }
  const getActiveCustomFields = getActiveFields(allCustomfields, profile, feature);

  const sections = columns.filter(
    c => !isNull(c.fieldType) && c.fieldType === "section"
  ); /** sections Array */
  const fieldsWithoutSections = columns.filter(
    c =>
      !c.isSystem && isNull(c.sectionId) && c.fieldType !== "section"
  ); /** Fields without sections and not system fields */
  let nonSectionFields = columns.filter(
    c =>
      !isNull(c.fieldType) &&
      !isNull(c.fieldId) &&
      !isNull(c.sectionId) &&
      c.fieldType !== "section"
  );
  let childFilds = [];
  sections.map(cf => {
    childFilds = [...childFilds, ...getChildComponents(cf.fieldId, columns)];
  });
  let Bfiltered = differenceBy(nonSectionFields, childFilds, "fieldId");
  Bfiltered = [...Bfiltered, ...fieldsWithoutSections];

  let activeFields = Bfiltered.reduce((res, currentItem) => {
    let isExist = getActiveCustomFields.find(field => field.fieldId === currentItem.fieldId);
    if (isExist) res.push({ ...currentItem, settings: isExist.settings })
    return res
  }, []);

  // Bfiltered = getActiveFields(Bfiltered, profile, feature);
  return activeFields;
};

const sectionColumns = (columns, profile, feature, allCustomfields) => {
  //in case there are no columns just return empty array instead of null
  if(!columns){
    return []
  }
  const getActiveCustomFields = getActiveFields(allCustomfields, profile, feature);

  const sections = columns.filter(c => !isNull(c.fieldType) && c.fieldType === "section");

  let sectionFields = sections.reduce((result, cv) => {
    let currentSecFields = columns.filter(
      c => !isNull(c.sectionId) && includes(c.sectionId, cv.fieldId)
    );

    let activeFields = currentSecFields.reduce((res, currentItem) => {
      let isExist = getActiveCustomFields.find(field => field.fieldId === currentItem.fieldId);
      if (isExist)
        res.push(
          isExist.fieldType == "section"
            ? { ...currentItem, color: isExist.settings.color, settings: isExist.settings }
            : { ...currentItem, settings: isExist.settings }
        );
      return res;
    }, []);

    const currentSection = allCustomfields.find(f => f.fieldId == cv.fieldId);
    currentSection &&
      result.push({
        ...cv,
        color: currentSection.settings.color,
        fields: activeFields,
        settings: currentSection.settings,
      });
    return result;
  }, []);

  return sectionFields;
};

const getColumns = (
  taskColumnSelector,
  profileSelector,
  customfieldSelector,
  projectColumnSelector,
  issueColumnSelector,
  meetingColumnSelector,
riskColumnSelector
) => {
  let object = {
    /** initial state object */
    task: {
      columns: [],
      sections: [],
      nonSectionFields: [],
    },
    project: {
      columns: [],
      sections: [],
      nonSectionFields: [],
    },
    meeting: {
      columns: [],
      sections: [],
      nonSectionFields: []
    },
    risk: {
      columns: [],
      sections: [],
      nonSectionFields: []
    },
    issue: {
      columns: [],
      sections: [],
      nonSectionFields: []
    },
  };
  object.task.columns = systemColumns(taskColumnSelector);
  object.task.sections = sectionColumns(taskColumnSelector, profileSelector, "task", customfieldSelector);
  object.task.nonSectionFields = nonSectionColumns(taskColumnSelector, profileSelector, "task", customfieldSelector);
  object.project.columns = systemColumns(projectColumnSelector);
  object.project.sections = sectionColumns(projectColumnSelector, profileSelector, "project", customfieldSelector);
  object.project.nonSectionFields = nonSectionColumns(projectColumnSelector, profileSelector, "project", customfieldSelector);
  // object.issue.columns = systemColumns(issueColumnSelector);
  // object.issue.sections = sectionColumns(issueColumnSelector);
  // object.issue.nonSectionFields = nonSectionColumns(issueColumnSelector);
  object.issue.columns = systemColumns(issueColumnSelector);
  object.issue.sections = sectionColumns(
    issueColumnSelector,
    profileSelector,
    "issue",
    customfieldSelector
  );
  object.issue.nonSectionFields = nonSectionColumns(
    issueColumnSelector,
    profileSelector,
    "issue",
    customfieldSelector
  );
 object.meeting.columns = systemColumns(meetingColumnSelector);
  object.meeting.sections = sectionColumns(meetingColumnSelector, profileSelector, "meeting", customfieldSelector);
  object.meeting.nonSectionFields = nonSectionColumns(meetingColumnSelector, profileSelector, "meeting", customfieldSelector);
  object.risk.columns = systemColumns(riskColumnSelector);
  object.risk.sections = sectionColumns(riskColumnSelector, profileSelector, "risk", customfieldSelector);
  object.risk.nonSectionFields = nonSectionColumns(riskColumnSelector, profileSelector, "risk", customfieldSelector);

  return object;
};

export default createSelector(
  taskColumnSelector,
  profileSelector,
  customfieldSelector,
  // issueColumnSelector,
  projectColumnSelector,
  issueColumnSelector,
  meetingColumnSelector,
  riskColumnSelector,
  getColumns
);
