// Reselect selector
// Takes a list of projects and generates
// the key value pair of projectId's containig project permissions

import { createSelector } from "reselect";

// Select function to pick of the projects from state
const projectsSelector = state => state.projects.data;

const getPermissionsObject = projectsSelector => {
    /** function return the oject with key value pair of projectId and its permissions */
  let permissionObj = projectsSelector.reduce((result, currentValue) => {
    result[currentValue.projectId] = currentValue.projectPermission.permission;
    return result;
  }, {});
  return permissionObj;
};

export default createSelector(projectsSelector, getPermissionsObject);
