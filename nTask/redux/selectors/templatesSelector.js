// Reselect selector
// Takes a list of projects and generates
// the key value pair of projectId's containig templates
import React from 'react';
import { createSelector } from "reselect";
import CircularIcon from "@material-ui/icons/Brightness1";

// Select function to pick of the projects from state
const projectsSelector = state => state.projects.data;
const worksapceDefaultTemplateSelector = state => state.workspaceTemplates.data.defaultWSTemplate;

const statusData = statusArr => {
  if (statusArr) {
    return statusArr.map(item => {
      return {
        label: item.statusTitle,
        value: item.statusId,
        icon: <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "20px" }} />,
        color: item.statusColor,
        obj: item,
        statusTitle: item.statusTitle,
        statusColor: item.statusColor,
      };
    });
  }
  return [];
};

const getTemplates = (projectsSelector, defaultTemplate) => {
  /** function return the oject with key value pair of projectId and its templates */
  let templatesObj = projectsSelector.reduce((result, currentValue) => {
    result[currentValue.projectId] = {...currentValue.projectTemplate, dropDownData : statusData(currentValue.projectTemplate.statusList)};
    return result;
  }, {});
  templatesObj["workspaceDefaultTemplate"] = {...defaultTemplate, dropDownData : statusData(defaultTemplate.statusList)};
  return templatesObj;
};

export default createSelector(projectsSelector, worksapceDefaultTemplateSelector, getTemplates);
