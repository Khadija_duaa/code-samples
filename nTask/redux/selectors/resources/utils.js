import { createSelector, createSelectorCreator, defaultMemoize } from "reselect";
import { isEqual, memoize } from "lodash";

// Note: here we can pass 'lodash.memoize' instead of 'defaultMemoize'
export const createDeepCompareSelector = createSelectorCreator(defaultMemoize, isEqual);

export const createParameterSelector = (selector) => {
  return (_, params) => selector(params);
};

export const selectParam = createParameterSelector;

// create factories like for 'data', 'flags', etc.
// REF: https://github.com/reduxjs/reselect/issues/18#issuecomment-127169903
