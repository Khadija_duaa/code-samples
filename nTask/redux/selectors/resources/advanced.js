import { createSelector } from "reselect";
import { ResourceWorkloadViewType } from "./../../../utils/constants/ResourceWorkloadViewType";
import { concat, groupBy, sumBy } from "lodash";
import { generateDateRange } from "../../../helper/dates/dates";
import { TaskTimelineColor } from "../../../utils/constants/TaskTimelineColor";
import { priorityData } from "../../../helper/taskDropdownData";
import {
  selectCalendarDatesFlattened,
  selectFilterWorkspaces,
  selectProjectResourceTasksDataList,
  selectProjectResourceTasksDataListExists,
  selectProjectResourcesDataList,
  selectProjectResourcesDataListExists,
  selectProjectResourcesDataRecordsCount,
  selectProjectsDataList,
  selectProjectsGroupedByWorkspaces,
  selectResourceTasksDataList,
  selectResourceTasksDataListExists,
  selectResourcesDataList,
  selectResourcesDataListExists,
  selectResourcesDataRecordsCount,
  selectResourcesGroupedByWorkspaces,
  selectTaskColorSetting,
  selectTeamMembers,
  selectTeamWorkspaces,
  selectUnassignedTasksDataList,
  selectUnassignedTasksDataListExists,
  selectWorkloadViewType,
} from "./basic";
import { createDeepCompareSelector, selectParam } from "./utils";
import { formatMomentToDate, getCombinedHashMapKey } from "../../../Views/Resources/temp-utils";

// selectors from resource workload module go here

// advanced selectors for computations

export const selectDataListCount = createSelector(
  [selectWorkloadViewType, selectResourcesDataList, selectProjectsDataList],

  (viewType, resources, projects) => {
    switch (viewType) {
      case ResourceWorkloadViewType.resources.value:
        return resources.length;
      case ResourceWorkloadViewType.projects.value:
        return projects.length;
      default:
        return 0;
    }
  }
);

export const selectResourceById = createSelector(
  [
    selectResourcesDataList,

    // (s, resourceId) => resourceId,
    selectParam((params) => params.resourceId),
  ],

  (dataList, resourceId) => {
    return dataList.find((k) => k.id === resourceId);
  }
);

export const selectResourceTotalCapacityForSelectedTimelinePeriod = createSelector(
  [selectResourceById],

  (resource) => {
    if (!resource) {
      return 0;
    }
    const val = resource.dailyWorkload.reduce((acc, curr) => acc + curr.capacity, 0);

    return Math.round(val);
  }
);

export const selectIfProjectsView = createSelector(
  [selectWorkloadViewType],

  (workloadViewType) => workloadViewType === ResourceWorkloadViewType.projects.value
);

export const selectGridResourcesList = createSelector(
  [
    selectIfProjectsView,
    selectResourcesDataList,
    selectProjectResourcesDataList,

    // (s, projectId) => projectId,
    selectParam((params) => params.projectId),
  ],

  (isProjectsView, resourcesDataList, projectResourcesDataList, projectId = null) => {
    if (!isProjectsView) {
      return resourcesDataList;
    }

    if (!projectId) {
      return [];
    }
    const projectResourcesHashMap = projectResourcesDataList;

    if (projectResourcesHashMap.has(projectId)) {
      return projectResourcesHashMap.get(projectId);
      // const list = projectResourcesHashMap.get(projectId);
      // // console.info("yo yo2:", list);
      // return list;
    }
    return [];
  }
);

export const selectGridResourcesListExists = createSelector(
  [
    selectIfProjectsView,
    selectResourcesDataListExists,
    selectProjectResourcesDataListExists,

    // (s, projectId) => projectId,
    selectParam((params) => params.projectId),
  ],

  (isProjectsView, resourcesDataListExists, projectResourcesDataListExists, projectId = null) => {
    if (!isProjectsView) {
      return resourcesDataListExists;
    }

    if (!projectId) {
      return [];
    }
    const projectResourcesExistsHashMap = projectResourcesDataListExists;

    if (projectResourcesExistsHashMap.has(projectId)) {
      return projectResourcesExistsHashMap.get(projectId);
      // const list = projectResourcesHashMap.get(projectId);
      // // console.info("yo yo2:", list);
      // return list;
    }
    return [];
  }
);

export const selectGridResourcesListRecordsCount = createSelector(
  [
    selectIfProjectsView,
    selectResourcesDataRecordsCount,
    selectProjectResourcesDataRecordsCount,

    // (_, projectId) => projectId,
    selectParam((params) => params.projectId),
  ],

  (
    isProjectsView,
    resourcesDataRecordsCount,
    projectResourcesDataRecordsCount,
    projectId = null
  ) => {
    if (!isProjectsView) {
      return resourcesDataRecordsCount;
    }

    if (!projectId) {
      return [];
    }

    if (projectResourcesDataRecordsCount.has(projectId)) {
      return projectResourcesDataRecordsCount.get(projectId);
    }
    return [];
  }
);

export const selectGridResourceTasksList = createSelector(
  [
    selectIfProjectsView,
    selectResourceTasksDataList,
    selectProjectResourceTasksDataList,

    // (_, resourceId) => resourceId,
    // (_, projectId) => projectId,
    selectParam((params) => params.resourceId),
    selectParam((params) => params.projectId),
  ],

  (
    isProjectsView,
    resourceTasksDataList,
    projectResourceTasksDataList,
    resourceId = null,
    projectId = null
  ) => {
    if (!resourceId) {
      return [];
    }

    if (!isProjectsView) {
      if (resourceTasksDataList.has(resourceId)) {
        return resourceTasksDataList.get(resourceId);
      }
      return [];
    }

    if (!projectId) {
      return [];
    }
    const projectResourceTasksHashMap = projectResourceTasksDataList;
    const key = getCombinedHashMapKey(projectId, resourceId);

    if (projectResourceTasksHashMap.has(key)) {
      return projectResourceTasksHashMap.get(key);
    }
    return [];
  }
);

export const selectGridResourceTasksListExists = createSelector(
  [
    selectIfProjectsView,
    selectResourceTasksDataListExists,
    selectProjectResourceTasksDataListExists,

    // (_, resourceId) => resourceId,
    // (_, projectId) => projectId,
    selectParam((params) => params.resourceId),
    selectParam((params) => params.projectId),
  ],

  (
    isProjectsView,
    resourceTasksDataListExists,
    projectResourceTasksDataListExists,
    resourceId = null,
    projectId = null
  ) => {
    if (!resourceId) {
      return false;
    }
    if (!isProjectsView) {
      if (resourceTasksDataListExists.has(resourceId)) {
        return resourceTasksDataListExists.get(resourceId);
      }
      return false;
    }

    if (!projectId) {
      return false;
    }
    const projectResourceTasksExistsHashMap = projectResourceTasksDataListExists;
    const key = getCombinedHashMapKey(projectId, resourceId);

    if (projectResourceTasksExistsHashMap.has(key)) {
      return projectResourceTasksExistsHashMap.get(key);
      // const list = projectResourcesHashMap.get(projectId);
      // // console.info("yo yo2:", list);
      // return list;
    }
    return false;
  }
);

export const selectGridResourceDailyWorkloads = createDeepCompareSelector(
  [
    selectIfProjectsView,
    selectCalendarDatesFlattened,
    selectResourceById,
    selectResourceTasksDataListExists,
    selectResourceTasksDataList,
    // TODO: here handle for project's resource's tasks

    selectParam((params) => params.resourceId),
    selectParam((params) => params.projectId),
  ],

  (
    isProjectsView,
    calendarDatesFlattened,
    resource,
    resourceTasksDataListExists,
    resourceTasksDataList,
    resourceId = null,
    projectId = null
  ) => {
    if (!resourceId) {
      return;
    }

    if (!isProjectsView) {
      const resourceWorkloads = resource?.dailyWorkload || [];

      if (resourceTasksDataListExists.has(resourceId) && resourceTasksDataList.has(resourceId)) {
        const resourceTasks = resourceTasksDataList.get(resourceId);

        // here sum up all the task's workload w.r.t date

        const dateObjectsList = Object.values(calendarDatesFlattened);
        const datesList = dateObjectsList.map((k) => formatMomentToDate(k.momentObj));
        const outputArray = [];

        const resourceTaskLoads = resourceTasks.map((k) => {
          const taskDates = generateDateRange(k.plannedStartDate, k.plannedEndDate);

          // const loads = cloneDeep(k.dailyWorkload);
          const loads = [];

          (k.dailyWorkload || []).forEach((e, i) => {
            const itm = taskDates[i];

            loads.push({
              ...e,
              moment: itm,
              date: formatMomentToDate(itm),
            });
          });

          return loads;
        });

        const combinedTaskLoads = concat([], ...resourceTaskLoads);

        const groupByDate = groupBy(combinedTaskLoads, (k) => k.date);

        const sumByDate = (arr) => sumBy(arr, (k) => k.load || 0);

        // const hashMap = new Map(Object.entries(groupByDate).map(([k, v]) => [k, v.load || 0]));
        const hashMap = new Map(Object.entries(groupByDate).map(([k, v]) => [k, sumByDate(v)]));
        // const groupByDate = groupBy(resourceTaskLoads, (k) => k.date);
        // const resourcesHashMap = new Map(resourceTasks.map(k => [k.id, k.dailyWorkload]));

        datesList.forEach((date, inx) => {
          const value = hashMap.has(date) ? hashMap.get(date) : 0;
          const resourceLoad = resourceWorkloads[inx];

          outputArray.push({ ...resourceLoad, load: value });
        });

        return outputArray;
      }
      return resourceWorkloads;
    }

    if (!projectId) {
      return [];
    }

    // const projectResourcesExistsHashMap =

    return [];
  }
);

export const selectResourceTotalWorkloadForSelectedTimelinePeriod = createSelector(
  [selectGridResourceDailyWorkloads],

  (workloads = []) => {
    const val = workloads.reduce((acc, curr) => acc + curr.load, 0);

    return Math.round(val);
  }
);

export const selectResourceTask = createSelector(
  [
    selectResourceTasksDataList,
    selectResourceTasksDataListExists,

    selectParam((params) => params.resourceId),
    selectParam((params) => params.taskId),
  ],

  (resourceTasksDataList, resourceTasksDataListExists, resourceId, taskId) => {
    if (!resourceId) {
      return;
    }

    if (!resourceTasksDataListExists.has(resourceId) || !resourceTasksDataList.has(resourceId)) {
      return;
    }

    const resourceTasks = resourceTasksDataList.get(resourceId);

    const task = resourceTasks.find((k) => k.id === taskId);

    return task;
  }
);

export const selectResourceTaskWorkloads = createSelector(
  [selectResourceTask],

  (task) => {
    return task?.dailyWorkload || [];
  }
);

export const selectTeamWorkspacesList = createSelector(
  [selectTeamWorkspaces],

  (teamWorkspaces) => {
    // // SAMPLE OUTPUT
    // [
    //   {
    //     teamId: "d5ef44923a7042e4826294db904c20f2",
    //     teamName: "Development Department",
    //     teamDescription: null,
    //     config: {
    //       isProjectFieldMandatory: false,
    //       isUserTasksEffortMandatory: false,
    //       isCustomTime: false,
    //       isCustomTimeApplyOnTimer: false,
    //       timespan: 0,
    //     },
    //     calendar: {
    //       calenderId: "3fa07eaddac64785a067881e536bb328",
    //       title: "nTask Schedule",
    //       workingdays: [
    //         {
    //           day: "Sunday",
    //           id: 0,
    //         },
    //         {
    //           day: "Monday",
    //           id: 1,
    //         },
    //         {
    //           day: "Tuesday",
    //           id: 2,
    //         },
    //         {
    //           day: "Wednesday",
    //           id: 3,
    //         },
    //         {
    //           day: "Thursday",
    //           id: 4,
    //         },
    //         {
    //           day: "Friday",
    //           id: 5,
    //         },
    //         {
    //           day: "Saturday",
    //           id: 6,
    //         },
    //       ],
    //       workingHours: [],
    //       dailyCapacity: 9,
    //       isEndDateCalculate: true,
    //       isSystemCalender: true,
    //       projectCount: 0,
    //       taskCount: 0,
    //       projectId: null,
    //       exceptions: [],
    //       createdDate: "2022-10-20T08:13:36.394Z",
    //       updatedDate: "2022-10-20T08:13:31.195Z",
    //       createdBy: "6242aca7df1605d08dcc4e85",
    //       ownedBy: null,
    //       version: 1,
    //       updatedBy: null,
    //       workSpaceId: null,
    //       teamId: null,
    //       isOwner: false,
    //       position: 0,
    //       id: "6351032b346d1b2f95ed4232",
    //     },
    //     teamOwner: "6375df71d15b9077d98c98a4",
    //     isActive: true,
    //     companyId: "c5fe3eabba304f87a27585da12ae1188",
    //     timeZone: "(UTC-12:00) International Date Line West",
    //     dayOfWeek: "Monday",
    //     teamUrl: "6375df71d15b9077d98c98a4",
    //     pictureUrl: null,
    //     pictureThumbnail_40X40: null,
    //     pictureThumbnail_70X70: null,
    //     isCorporateUser: true,
    //     isMembersInvitationAllowed: false,
    //     isTeamOwner: true,
    //     taskProgressCalculation: 0,
    //     imageBasePath: "https://d108go4nu9st7k.cloudfront.net/",
    //     members: ["6375df71d15b9077d98c98a4", "6232eaf64163938f5d3efd4f"],
    //     projects: 2,
    //     tasks: 7,
    //     issues: 0,
    //     risks: 0,
    //     sortColumn: {
    //       userId: null,
    //       project: [],
    //       projectOrder: "ASC",
    //       task: [],
    //       taskOrder: "ASC",
    //       meeting: [],
    //       meetingOrder: "ASC",
    //       issue: [],
    //       issueOrder: "ASC",
    //       risk: [],
    //       riskOrder: "ASC",
    //       type: 0,
    //     },
    //   },
    // ];

    return teamWorkspaces.map((k) => ({
      id: k.teamId,
      name: k.teamName,
    }));
  }
);

export const selectTeamResourcesList = createSelector(
  [selectTeamMembers, selectFilterWorkspaces],

  (teamMembers, filterWorkspaces) => {
    // // SAMPLE OUTPUT
    // [
    //   {
    //     userId: "6375df71d15b9077d98c98a4",
    //     userName: "rizwan.ali@naxxa.io",
    //     email: "rizwan.ali@naxxa.io",
    //     fullName: "Rizwan Ali",
    //     imageUrl: "",
    //     teamId: "d5ef44923a7042e4826294db904c20f2",
    //     isAdmin: true,
    //     isActive: true,
    //     isMember: false,
    //     lastInvitedDate: "2022-11-17T07:14:57.69Z",
    //     inviteHistory: [],
    //     roleId: "000",
    //     roleName: "TeamOwner",
    //     isCustomRole: false,
    //     isOnline: true,
    //     jobTitle: "",
    //     timeZone: "UTC+05:00",
    //     teamOwner: true,
    //     isDeleted: false,
    //   },
    //   {
    //     userId: "6232eaf64163938f5d3efd4f",
    //     userName: "kazim.hussain@naxxa.io",
    //     email: "kazim.hussain@naxxa.io",
    //     fullName: "Kazim Hussain",
    //     imageUrl:
    //       "https://d1yxkj8tr89l78.cloudfront.net/fca47b9fb9324d058dd25eff332bd619_40X40.gif",
    //     teamId: "d5ef44923a7042e4826294db904c20f2",
    //     isAdmin: false,
    //     isActive: true,
    //     isMember: true,
    //     lastInvitedDate: "2022-12-27T13:09:51.125Z",
    //     inviteHistory: [
    //       {
    //         invitedBy: "6375df71d15b9077d98c98a4",
    //         status: "Sent",
    //         inviteDate: "2022-12-27T18:08:14",
    //       },
    //     ],
    //     roleId: "003",
    //     roleName: "Member",
    //     isCustomRole: false,
    //     isOnline: false,
    //     jobTitle: "",
    //     timeZone: "UTC+05:00",
    //     teamOwner: false,
    //     isDeleted: false,
    //   },
    // ];

    const filtered = teamMembers.filter((k) => filterWorkspaces.includes(k.teamId));

    return filtered.map((k) => ({
      email: k.email,
      fullName: k.fullName || "",
      imageUrl: k.imageUrl,
      id: k.userId,
      jobTitle: k.jobTitle || "",
      isOnline: k.isOnline || false,
      isOwner: k.isOwner || false,
    }));
  }
);

export const selectProjectsOfWorkspaces = createSelector(
  [selectProjectsGroupedByWorkspaces],

  (projectsOfWorkspaces) => {
    // here goes the logic

    // return [];
    // returning hashmap
    return projectsOfWorkspaces;
  }
);

export const selectResourcesOfWorkspaces = createSelector(
  [selectResourcesGroupedByWorkspaces],

  (resourcesOfWorkspaces) => {
    // here goes the logic

    // return [];
    // returning hashmap
    return resourcesOfWorkspaces;
  }
);

export const selectUnassignedTask = createSelector(
  [
    selectUnassignedTasksDataList,
    selectUnassignedTasksDataListExists,

    selectParam((params) => params.taskId),
  ],

  (unassignedTasksDataList, unassignedTasksDataListExists, taskId) => {
    if (!unassignedTasksDataListExists) {
      return null;
    }

    const task = unassignedTasksDataList.find((k) => k.id === taskId);

    return task;
  }
);

export const selectUnassignedTaskWorkloads = createSelector(
  [selectUnassignedTask],

  (task) => {
    return task?.dailyWorkload || [];
  }
);

// TODO: apply lodash.memoize on function bcz of expensive computation
// OR try using 'createDeepCompareSelector'
export const selectTaskPriorityColor = createSelector(
  [
    // selectParam((params) => params.themePriorityColors),
    selectParam((params) => params.theme),
    selectParam((params) => params.taskPriority),
  ],

  (theme, taskPriority) => {
    const priorityObject = priorityData(theme).find((k) => k.value === taskPriority);

    return priorityObject?.color;
  }
);

// TODO: apply lodash.memoize on function bcz of expensive computation
export const selectTaskTimelineColor = createSelector(
  [
    selectTaskColorSetting,
    selectTaskPriorityColor,

    selectParam((params) => params.taskStatusColor),
    selectParam((params) => params.taskColor),
  ],

  (taskColorSetting, taskPriorityColor, taskStatusColor, taskColor) => {
    switch (taskColorSetting) {
      case TaskTimelineColor.taskStatus.value:
        return taskStatusColor;

      case TaskTimelineColor.taskPriority.value:
        return taskPriorityColor;

      case TaskTimelineColor.taskColor.value:
        return taskColor;

      default:
        return taskColor;
    }
  }
);

export const selectTaskData = createSelector(
  [selectParam((params) => params.isUnassignedTask), selectUnassignedTask, selectResourceTask],

  (isUnassignedTask, unassignedTask, resourceTask) => {
    return isUnassignedTask ? unassignedTask : resourceTask;
  }
);
