// basic selectors from redux

// data

export const selectCalendarDatesFlattened = (s) => s.resources.data.calendarDatesFlattened;

export const selectResourcesDataList = (s) => s.resources.data.resourcesDataList;

export const selectProjectsDataList = (s) => s.resources.data.projectsDataList;

export const selectProjectResourcesDataList = (s) => s.resources.data.projectResourcesDataList;

export const selectProjectResourceTasksDataList = (s) =>
  s.resources.data.projectResourceTasksDataList;

export const selectResourceTasksDataList = (s) => s.resources.data.resourceTasksDataList;

export const selectProjectsGroupedByWorkspaces = (s) =>
  s.resources.data.projectsOfWorkspacesFilterDataList;

export const selectResourcesGroupedByWorkspaces = (s) =>
  s.resources.data.resourcesOfWorkspacesFilterDataList;

export const selectUnassignedTasksDataList = (s) => s.resources.data.unassignedTasksDataList;

// data-records-count

export const selectResourcesDataRecordsCount = (s) =>
  s.resources.dataRecordsCount.resourcesDataRecordsCount;

export const selectProjectResourcesDataRecordsCount = (s) =>
  s.resources.dataRecordsCount.projectResourcesDataRecordsCount;

// data-flags

export const selectResourcesDataListExists = (s) => s.resources.dataFlags.resourcesDataListExists;

export const selectResourceTasksDataListExists = (s) =>
  s.resources.dataFlags.resourceTasksDataListExists;

export const selectProjectResourceTasksDataListExists = (s) =>
  s.resources.dataFlags.projectResourceTasksDataListExists;

export const selectProjectResourcesDataListExists = (s) =>
  s.resources.dataFlags.projectResourcesDataListExists;

export const selectUnassignedTasksDataListExists = (s) =>
  s.resources.dataFlags.unassignedTasksDataListExists;

// ui-grid

export const selectWorkloadViewType = (s) => s.resources.uiGrid.workloadViewType;

// filters

export const selectFilterWorkspaces = (s) => s.resources.filters.workspaces;

// settings

export const selectShowTaskNameWithTimelineSetting = (s) =>
  s.resources.settings.showTaskNameWithTimeline;

export const selectTaskColorSetting = (s) => s.resources.settings.taskTimelineColor;

// others

export const selectTeamMembers = (s) => s.profile.data.member.allMembers;

export const selectTeamWorkspaces = (s) => s.profile.data.workspace;
