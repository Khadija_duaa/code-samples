export const headerTranslations = (text, intl) => {
  let id = "";
  switch (text) {
    //workspaces
    case "ID":
      id = "common.ellipses-columns.id";
      break;
    case "Status":
      id = "common.ellipses-columns.status";
      break;
    case "Planned Start/End":
      id = "common.ellipses-columns.planned-start-end";
      break;
    case "Actual Start/End":
      id = "common.ellipses-columns.actual-start-end";
      break;
    case "Priority":
      id = "common.ellipses-columns.priority";
      break;
    case "Project":
      id = "common.ellipses-columns.project";
      break;
    case "Progress":
      id = "common.ellipses-columns.progress";
      break;
    case "Assignee":
      id = "common.ellipses-columns.assignee";
      break;
    case "Time Logged":
      id = "common.ellipses-columns.timelogged";
      break;
    case "Comments":
      id = "common.ellipses-columns.comment";
      break;
    case "Attachments":
      id = "common.ellipses-columns.attachment";
      break;
    case "Meetings":
      id = "common.ellipses-columns.meetings";
      break;
    case "Issues":
      id = "common.ellipses-columns.issues";
      break;
    case "Risks":
      id = "common.ellipses-columns.risks";
      break;
    case "Creation Date":
      id = "common.ellipses-columns.creationdate";
      break;
    case "Created By":
      id = "common.ellipses-columns.createdby";
      break;
    case "Task":
      id = "common.ellipses-columns.task";
      break;
    case "Tasks":
      id = "common.ellipses-columns.tasks";
      break;
    case "Project Title":
      id = "common.ellipses-columns.projecttitle";
      break;
    case "Resources":
      id = "common.ellipses-columns.resources";
      break;
    case "Date":
      id = "common.date.label";
      break;
    case "Location":
      id = "common.ellipses-columns.location";
      break;
    case "Time":
      id = "common.sort-by.time";
      break;
    case "Title":
      id = "common.ellipses-columns.title";
      break;
    case "Attendee":
      id = "common.ellipses-columns.attendee";
      break;
    case "Type":
      id = "common.ellipses-columns.type";
      break;
    case "Severity":
      id = "common.ellipses-columns.severity";
      break;
    case "Last Updated":
      id = "common.ellipses-columns.lastupdated";
      break;
      case "Risk Owner":
      id = "common.ellipses-columns.riskowner";
      break;
      case "Impact":
        id = "common.ellipses-columns.impact";
        break;
      case "Likelihood":
        id = "common.ellipses-columns.likelihood";
        break;    
  }
  return id == "" ? text : intl.formatMessage({ id: id, defaultMessage: text });
}