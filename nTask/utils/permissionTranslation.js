export const translatePermission = (text, intl) => {
    let id = "";
    switch (text) {
        //workspaces
        case "Workspaces":
            id = "workspace-settings.roles-and-permission.list.workspace";
            break;
        case "Workspace Setting":
            id = "workspace-settings.roles-and-permission.list.workspace-settings";
            break;
        case "General Information":
            id = "workspace-settings.roles-and-permission.list.general-information";
            break;
        case "Workspace Image":
            id = "workspace-settings.roles-and-permission.list.workspace-image";
            break;
        case "Delete Workspace":
            id = "workspace-settings.roles-and-permission.list.delete-workspace";
            break;
        case "Workspace Name":
            id = "workspace-settings.roles-and-permission.list.workspace-name";
            break;
        case "Workspace Url":
            id = "workspace-settings.roles-and-permission.list.worksapce-url";
            break;
        case "User Management":
            id = "workspace-settings.roles-and-permission.list.user-management";
            break;
        case "Add Members to workspace":
            id = "workspace-settings.roles-and-permission.list.add-members-to-workspace";
            break;
        case "Remove Members from workspace":
            id = "workspace-settings.roles-and-permission.list.remove-members-workspace";
            break;
        case "Enable Members in workspace":
            id = "workspace-settings.roles-and-permission.list.enable-members-workspace";
            break;
        case "Disable Members in workspace":
            id = "workspace-settings.roles-and-permission.list.disable-members-workspace";
            break;
        case "Update Role in workspace":
            id = "workspace-settings.roles-and-permission.list.update-workspace";
            break;
        case "Roles & Permission":
            id = "workspace-settings.roles-and-permission.list.role-permission";
            break;
        case "Custom Role":
            id = "workspace-settings.roles-and-permission.list.custom-role";
            break;
        case "Rename Custom Role":
            id = "workspace-settings.roles-and-permission.list.rename-custom-role";
            break;
        //Projects
        case "Projects":
            id = "workspace-settings.roles-and-permission.list.projects";
            break;
        case "Project Details":
            id = "workspace-settings.roles-and-permission.list.project-details";
            break;
        case "View Project Detail Tab":
            id = "workspace-settings.roles-and-permission.list.view-project-tab";
            break;
        case "Billable/Non-Billable":
            id = "workspace-settings.roles-and-permission.list.billable-non-billable";
            break;
        case "Billing Method":
            id = "workspace-settings.roles-and-permission.list.billing-method";
            break;
        case "View Project Resources Tab":
            id = "workspace-settings.roles-and-permission.list.view-project-resources";
            break;
        case "Add Project Resources":
            id = "workspace-settings.roles-and-permission.list.add-project-resources";
            break;
        case "Job Role":
            id = "workspace-settings.roles-and-permission.list.job-role";
            break;
        case "Weekly Capacity":
            id = "workspace-settings.roles-and-permission.list.weekly-capacity";
            break;
        case "Add Resource Rate":
            id = "workspace-settings.roles-and-permission.list.add-resource-rate";
            break;
        case "Project Permissions":
            id = "workspace-settings.roles-and-permission.list.project-permissions";
            break;
        case "Delete Resource":
            id = "workspace-settings.roles-and-permission.list.delete-resource";
            break;
        case "View Project Task Tab":
            id = "workspace-settings.roles-and-permission.list.view-project-task-tab";
            break;
        case "Add More":
            id = "workspace-settings.roles-and-permission.list.add-more";
            break;
        case "Add Project Tasks":
            id = "workspace-settings.roles-and-permission.list.add-project-tasks";
            break;
        case "Import Bulk Tasks":
            id = "workspace-settings.roles-and-permission.list.import-bulk-tasks";
            break;
        case "Estimated Time":
            id = "workspace-settings.roles-and-permission.list.estiamted-time";
            break;
        case "Add Task Rate":
            id = "workspace-settings.roles-and-permission.list.add-task-rate";
            break;
        case "Public Link":
            id = "common.action.public-link.label";
            break;
        case "Color":
            id = "common.action.color.label";
            break;
        case "Copy Task":
            id = "common.action.copy-task.label";
            break;
        case "Archive Task":
            id = "common.action.archive-task.label";
            break;
        case "Unlink Task":
            id = "common.action.unlink-task.label";
            break;
        case "To-Do List":
            id = "task.detail-dialog.to-do-list.label";
            break;
        case "Delete Task":
            id = "common.action.delete-task.label";
            break;
        case "Planned Start Date":
            id = "common.sort-by.plannedStart";
            break;
        case "Planned End Date":
            id = "common.sort-by.plannedend";
            break;
        case "Actual Start Date":
            id = "common.sort-by.actualstart";
            break;
        case "Actual End Date":
            id = "common.sort-by.actualend";
            break;
        case "View Project Milestones Tab":
            id = "workspace-settings.roles-and-permission.list.view-project-milestone-tab";
            break;
        case "Add Project Milestone":
            id = "workspace-settings.roles-and-permission.list.add-project-milestone";
            break;
        case "Milestone Name":
            id = "workspace-settings.roles-and-permission.list.milestone-name";
            break;
        case "Set Milestone Date":
            id = "workspace-settings.roles-and-permission.list.set-milestone-date";
            break;
        case "Complete Milestone":
            id = "workspace-settings.roles-and-permission.list.complete-milestone";
            break;
        case "Milestone Billable/Unbillable":
            id = "workspace-settings.roles-and-permission.list.milestone-billable";
            break;
        case "Delete Milestone":
            id = "workspace-settings.roles-and-permission.list.delete-milestone";
            break;
        case "View Project Financial Summary Tab":
            id = "workspace-settings.roles-and-permission.list.view-project-financial-summary-tab";
            break;
        case "View Billing Details":
            id = "workspace-settings.roles-and-permission.list.view-billing-details";
            break;
        case "View Estimated Cost":
            id = "workspace-settings.roles-and-permission.list.view-estiamted-cost";
            break;
        case "View Actual Cost":
            id = "workspace-settings.roles-and-permission.list.view-actual-cost";
            break;
        case "Select Email Alerts":
            id = "workspace-settings.roles-and-permission.list.select-email-alerts";
            break;
        case "Project Status":
            id = "workspace-settings.roles-and-permission.list.project-status";
            break;
        case "Project Color":
            id = "workspace-settings.roles-and-permission.list.project-color";
            break;
        case "Project Updates":
            id = "workspace-settings.roles-and-permission.list.project-updates";
            break;
        case "Project Documents":
            id = "workspace-settings.roles-and-permission.list.project-documents";
            break;

        case "Project Description":
            id = "workspace-settings.roles-and-permission.list.project-description";
            break;
        case "Project Settings":
            id = "workspace-settings.roles-and-permission.list.project-settings";
            break;
        case "Project Name":
            id = "workspace-settings.roles-and-permission.list.project-name";
            break;
        case "Project resource":
            id = "workspace-settings.roles-and-permission.list.proejct-resource";
            break;
        case "Currency":
            id = "workspace-settings.roles-and-permission.list.currency";
            break;
        case "Project Budget":
            id = "workspace-settings.roles-and-permission.list.project-budget";
            break;
        case "Create Projects":
            id = "workspace-settings.roles-and-permission.list.create-projects";
            break;
        case "View all projects":
            id = "workspace-settings.roles-and-permission.list.view-all-projects";
            break;
        case "Export projects":
            id = "workspace-settings.roles-and-permission.list.export-projects";
            break;
        case "Import projects":
            id = "workspace-settings.roles-and-permission.list.import-projects";
            break;
        case "Archive projects":
            id = "workspace-settings.roles-and-permission.list.archive-project";
            break;
        case "Unarchive projects":
            id = "workspace-settings.roles-and-permission.list.unarchive-project";
            break;
        case "Delete projects":
            id = "workspace-settings.roles-and-permission.list.delete-projects";
            break;
        case "Copy project within workspace":
            id = "workspace-settings.roles-and-permission.list.copy-project";
            break;
        case "Project Gantt access":
            id = "workspace-settings.roles-and-permission.list.project-gant-access";
            break;
        case "Move project":
            id = "workspace-settings.roles-and-permission.list.move-project";
            break;
        case "Task Documents":
            id = "workspace-settings.roles-and-permission.list.task-documents";
            break;
        case "Delete Document":
            id = "workspace-settings.roles-and-permission.list.delete-document";
            break;
        case "Open Folder":
            id = "workspace-settings.roles-and-permission.list.open-folder";
            break;
        case "Delete Folder":
            id = "workspace-settings.roles-and-permission.list.delete-folder";
            break;
        case "View Document":
            id = "workspace-settings.roles-and-permission.list.view-document";
            break;
        case "Add Folder":
            id = "workspace-settings.roles-and-permission.list.add-folder";
            break;
        case "Upload Document":
            id = "workspace-settings.roles-and-permission.list.upload-document";
            break;
        case "Rename Folder":
            id = "workspace-settings.roles-and-permission.list.rename-folder";
            break;
        case "Download Document":
            id = "workspace-settings.roles-and-permission.list.download-attachement";
            break;
        case "Task Attachments":
            id = "workspace-settings.roles-and-permission.list.task-attachments";
            break;
        case "Add Comments":
            id = "workspace-settings.roles-and-permission.list.add-comments";
            break;
        case "Convert Comment to Task":
            id = "workspace-settings.roles-and-permission.list.convert-comment-to-task";
            break;
        case "Show Receipt":
            id = "workspace-settings.roles-and-permission.list.show-reciept";
            break;
        case "Clear All Replies":
            id = "workspace-settings.roles-and-permission.list.clear-all-replies";
            break;
        case "Clear Reply":
            id = "workspace-settings.roles-and-permission.list.clear-reply";
            break;
        case "View Attachment":
            id = "workspace-settings.roles-and-permission.list.view-attachment";
            break;
        case "Download Attachment":
            id = "workspace-settings.roles-and-permission.list.download-attachment";
            break;
        case "Reply Later":
            id = "workspace-settings.roles-and-permission.list.reply-later";
            break;
        case "Reply":
            id = "workspace-settings.roles-and-permission.list.reply";
            break;
        case "Resource Billable":
            id = "workspace-settings.roles-and-permission.list.resource-billable";
            break;
        case "Task Billable":
            id = "workspace-settings.roles-and-permission.list.task-billable";
            break;
        case "Project Comments":
            id = "workspace-settings.roles-and-permission.list.project-comments";
            break;
        case "Project attachments":
            id = "workspace-settings.roles-and-permission.list.project-attachements";
            break;
        //tasks
        case "Tasks":
            id = "workspace-settings.roles-and-permission.list.tasks";
            break;
        case "Task Details":
            id = "workspace-settings.roles-and-permission.list.task-details";
            break;
        case "Task Project":
            id = "workspace-settings.roles-and-permission.list.task-project";
            break;
        case "Task Planned start/end date":
            id = "workspace-settings.roles-and-permission.list.task-planned";
            break;
        case "Task Actual start/end date":
            id = "workspace-settings.roles-and-permission.list.task-actual";
            break;
        case "Task description":
            id = "workspace-settings.roles-and-permission.list.task-description";
            break;
        case "Task Description":
            id = "workspace-settings.roles-and-permission.list.task-description";
            break;
        case "Todo list":
            id = "workspace-settings.roles-and-permission.list.tod-list";
            break;
        case "Task effort":
            id = "workspace-settings.roles-and-permission.list.task-effort";
            break;
        case "Task Estimation":
            id = "workspace-settings.roles-and-permission.list.task-estimation";
            break;
        case "Repeat Task":
            id = "workspace-settings.roles-and-permission.list.repeat-task";
            break;
        case "Task Assignee":
            id = "workspace-settings.roles-and-permission.list.task-assignee";
            break;
        case "Task Comments":
            id = "workspace-settings.roles-and-permission.list.task-comments";
            break;
        // case "Task Comments":
        //     id = "Task Comments";
        //     break;
        case "Task attachments":
            id = "workspace-settings.roles-and-permission.list.task-attachements";
            break;
        case "Task Name":
            id = "workspace-settings.roles-and-permission.list.task-name";
            break;
        case "Task priority":
            id = "workspace-settings.roles-and-permission.list.task-priority";
            break;
        case "Task Priority":
            id = "workspace-settings.roles-and-permission.list.task-priority";
            break;
        case "Create repeat task occurances":
            id = "workspace-settings.roles-and-permission.list.create-repeat-task-occurrances";
            break;
        case "Task Status":
            id = "workspace-settings.roles-and-permission.list.task-status";
            break;
        case "Create Task":
            id = "workspace-settings.roles-and-permission.list.create-task";
            break;
        case "View all tasks":
            id = "workspace-settings.roles-and-permission.list.view-all-tasks";
            break;
        case "Import tasks":
            id = "workspace-settings.roles-and-permission.list.import-tasks";
            break;
        case "Export tasks":
            id = "workspace-settings.roles-and-permission.list.export-tasks";
            break;
        case "Archive task":
            id = "workspace-settings.roles-and-permission.list.archive-task";
            break;
        case "Unarchive task":
            id = "workspace-settings.roles-and-permission.list.unarchive-task";
            break;
        case "Delete task":
            id = "workspace-settings.roles-and-permission.list.delete-task";
            break;
        case "Copy task within workspace":
            id = "workspace-settings.roles-and-permission.list.copy-task";
            break;
        case "Set color":
            id = "workspace-settings.roles-and-permission.list.set-color";
            break;
        case "Create public link of task":
            id = "workspace-settings.roles-and-permission.list.create-public-link-task";
            break;
        //Meeting    
        case "Meetings":
            id = "workspace-settings.roles-and-permission.list.meetings";
            break;
        case "Meeting Details":
            id = "workspace-settings.roles-and-permission.list.meeting-details";
            break;
        case "Meeting agenda":
            id = "workspace-settings.roles-and-permission.list.meeting-agenda";
            break;
        // case "Meeting agenda":
        //     id = "Meeting agenda";
        //     break;
        case "Meeting discussion notes":
            id = "workspace-settings.roles-and-permission.list.meeting-discussion-notes";
            break;
        case "Meeting follow up actions":
            id = "workspace-settings.roles-and-permission.list.meeting-follow-up-actions";
            break;
        case "Meeting key decisions":
            id = "workspace-settings.roles-and-permission.list.meeting-key-decisions";
            break;
        case "Repeat Meeting":
            id = "workspace-settings.roles-and-permission.list.repeat-meeting";
            break;
        // case "Repeat Meeting":
        //     id = "Repeat Meeting";
        //     break;
        case "Meeting Date":
            id = "workspace-settings.roles-and-permission.list.meeting-date";
            break;
        case "Meeting task":
            id = "workspace-settings.roles-and-permission.list.meeting-task";
            break;
        case "Meeting duration":
            id = "workspace-settings.roles-and-permission.list.meeting-duration";
            break;
        case "Meeting location":
            id = "workspace-settings.roles-and-permission.list.meeting-location";
            break;
        case "Meeting attendee":
            id = "workspace-settings.roles-and-permission.list.meeting-attendee";
            break;
        case "Meeting name":
            id = "workspace-settings.roles-and-permission.list.meeting-name";
            break;
        case "Meeting Submit for review":
            id = "workspace-settings.roles-and-permission.list.submit-for-review";
            break;
        case "Publish meeting":
            id = "workspace-settings.roles-and-permission.list.publish-meeting";
            break;
        case "Create repeat meeting occurances":
            id = "workspace-settings.roles-and-permission.list.create-repeat-meeting-occurrances";
            break;
        case "Meeting time":
            id = "workspace-settings.roles-and-permission.list.meeting-time";
            break;
        case "Create Meeting":
            id = "workspace-settings.roles-and-permission.list.create-meeting";
            break;
        case "View all meetings":
            id = "workspace-settings.roles-and-permission.list.view-all-meetings";
            break;
        case "Import meeting":
            id = "workspace-settings.roles-and-permission.list.import-meeting";
            break;
        case "Export meeting":
            id = "workspace-settings.roles-and-permission.list.export-meeting";
            break;
        case "Archive meeting":
            id = "workspace-settings.roles-and-permission.list.archive-meeting";
            break;
        case "Unarchive meeting":
            id = "workspace-settings.roles-and-permission.list.unarchive-meeting";
            break;
        case "Delete meeting":
            id = "workspace-settings.roles-and-permission.list.delete-meeting";
            break;
        case "Cancel meeting":
            id = "workspace-settings.roles-and-permission.list.cancel-meeting";
            break;
        //Issues
        case "Issues":
            id = "workspace-settings.roles-and-permission.list.issues";
            break;
        case "Issue Details":
            id = "workspace-settings.roles-and-permission.list.issue-details";
            break;
        case "Issue task":
            id = "workspace-settings.roles-and-permission.list.issue-task";
            break;
        case "Issue planned start/end date":
            id = "workspace-settings.roles-and-permission.list.issue-planned";
            break;
        case "Issue actual start/end date":
            id = "workspace-settings.roles-and-permission.list.issue-actual";
            break;
        case "Issue description":
            id = "workspace-settings.roles-and-permission.list.issue-description";
            break;
        case "Issue Type":
            id = "workspace-settings.roles-and-permission.list.issue-type";
            break;
        case "Issue Assignee":
            id = "workspace-settings.roles-and-permission.list.issue-assignee";
            break;
        case "Issue comments/updates":
            id = "workspace-settings.roles-and-permission.list.issue-comments-updates";
            break;
        case "Issue Attachment":
            id = "workspace-settings.roles-and-permission.list.issue-attachment";
            break;
        case "Issue Name":
            id = "workspace-settings.roles-and-permission.list.issue-name";
            break;
        case "Issue status":
            id = "workspace-settings.roles-and-permission.list.issue-status";
            break;
        case "Issue priority":
            id = "workspace-settings.roles-and-permission.list.issue-priority";
            break;
        case "Issue Severity":
            id = "workspace-settings.roles-and-permission.list.issue-severity";
            break;
        case "Create Issue":
            id = "workspace-settings.roles-and-permission.list.create-issue";
            break;
        case "View all issues":
            id = "workspace-settings.roles-and-permission.list.view-all-issues";
            break;
        case "Import issues":
            id = "workspace-settings.roles-and-permission.list.import-issues";
            break;
        case "Export issues":
            id = "workspace-settings.roles-and-permission.list.export-issues";
            break;
        case "Archive issues":
            id = "workspace-settings.roles-and-permission.list.archive-issues";
            break;
        case "Unarchive issues":
            id = "workspace-settings.roles-and-permission.list.unarchive-issues";
            break;
        case "Delete issues":
            id = "workspace-settings.roles-and-permission.list.delete-issues";
            break;
        case "Set color":
            id = "workspace-settings.roles-and-permission.list.set-color";
            break;
        case "Risks":
            id = "workspace-settings.roles-and-permission.list.risks";
            break;
        case "Risk Details":
            id = "workspace-settings.roles-and-permission.list.risk-details";
            break;
        case "Risk Task":
            id = "workspace-settings.roles-and-permission.list.risk-task";
            break;
        case "Risk description":
            id = "workspace-settings.roles-and-permission.list.risk-description";
            break;
        case "Risk mitigation plan":
            id = "workspace-settings.roles-and-permission.list.risk-mittigation-plan";
            break;
        case "Risk owner":
            id = "workspace-settings.roles-and-permission.list.risk-owner";
            break;
        case "Risk comments/updates":
            id = "workspace-settings.roles-and-permission.list.risk-comment-updates";
            break;
        case "Risk attachments":
            id = "workspace-settings.roles-and-permission.list.risk-attachments";
            break;
        case "Risk Name":
            id = "workspace-settings.roles-and-permission.list.risk-name";
            break;
        case "Risk status":
            id = "workspace-settings.roles-and-permission.list.risk-status";
            break;
        case "Risk likelihood":
            id = "workspace-settings.roles-and-permission.list.risk-likelihood";
            break;
        case "Risk Impact":
            id = "workspace-settings.roles-and-permission.list.risk-impact";
            break;
        case "Create Risk":
            id = "workspace-settings.roles-and-permission.list.create-risk";
            break;
        case "View all risks":
            id = "workspace-settings.roles-and-permission.list.view-all-risks";
            break;
        case "Import risks":
            id = "workspace-settings.roles-and-permission.list.import-risks";
            break;
        case "Export risks":
            id = "workspace-settings.roles-and-permission.list.export-risks";
            break;
        case "Archive risks":
            id = "workspace-settings.roles-and-permission.list.archive-risks";
            break;
        case "Unarchive risks":
            id = "workspace-settings.roles-and-permission.list.unarchive-risks";
            break;
        case "Delete risks":
            id = "workspace-settings.roles-and-permission.list.delete-risks";
            break;
        case "Risk documents":
            id = "workspace-settings.roles-and-permission.list.risk-documents";
            break;
        case "Force Project permission":
            id = "workspace-settings.roles-and-permission.list.force-project-permission";
            break;    
        //timesheet
        case "Timesheet":
            id = "workspace-settings.roles-and-permission.list.timesheet";
            break;
        case "Add Task":
            id = "workspace-settings.roles-and-permission.list.add-task";
            break;
        case "Add time entry":
            id = "workspace-settings.roles-and-permission.list.add-time-entry";
            break;
        case "Delete time entry":
            id = "workspace-settings.roles-and-permission.list.delete-time-entry";
            break;
        case "Delete timesheet":
            id = "workspace-settings.roles-and-permission.list.delete-timesheet";
            break;
        case "Submit timesheet for approval":
            id = "workspace-settings.roles-and-permission.list.submit-timesheet";
            break;
        case "Resubmit timesheet for approval":
            id = "workspace-settings.roles-and-permission.list.resubmit-timesheet";
            break;
        case "Send timesheet":
            id = "workspace-settings.roles-and-permission.list.send-timesheet";
            break;
        case "Add notes":
            id = "workspace-settings.roles-and-permission.list.add-notes";
            break;
        case "Accept timesheet":
            id = "workspace-settings.roles-and-permission.list.accept-timesheet";
            break;
        case "Reject timesheet":
            id = "workspace-settings.roles-and-permission.list.reject-timesheet";
            break;
    }
    return id == "" ? text : intl.formatMessage({ id: id, defaultMessage: text });
}