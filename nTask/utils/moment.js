import moment from "moment";

export const getDurationParts = (time) => {
  // time in milliseconds = 4512645
  const duration = moment.duration(time);

  let days = parseInt(duration.asDays());
  let totalHours = parseInt(duration.asHours());
  let minutes = parseInt(duration.asMinutes());
  let seconds = parseInt(duration.asSeconds() % 60);
  days = days < 10 ? "0" + days : days;
  minutes = minutes - totalHours * 60;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  let hours = totalHours - days * 24;
  hours = hours < 10 ? "0" + hours : hours;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  return { days, hours, totalHours, mins: minutes, seconds };
};
