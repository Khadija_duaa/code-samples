
import React from 'react';
import SvgIcon from "@material-ui/core/SvgIcon";
import FlagIcon from "../../components/Icons/FlagIcon";
import RoundIcon from "@material-ui/icons/Brightness1";

export function statusData(theme, classes, intl){
  const statusColor = theme.palette.taskStatus;

  return [
    {
      label: intl.formatMessage({id:"task.common.status.dropdown.not-started", defaultMessage:"Not Started"}),
      value: "Not Started",
      icon: (
        <RoundIcon
          htmlColor={statusColor.NotStarted}
          className={classes.statusIcon}
        />
      )
    },
    {
      label: intl.formatMessage({id:"task.common.status.dropdown.in-progress", defaultMessage:"In Progress"}),
      value: "In Progress",
      icon: (
        <RoundIcon
          htmlColor={statusColor.InProgress}
          className={classes.statusIcon}
        />
      )
    },
    {
      label: intl.formatMessage({id:"task.common.status.dropdown.in-review", defaultMessage:"In Review"}),
      value: "In Review",
      icon: (
        <RoundIcon
          htmlColor={statusColor.InReview}
          className={classes.statusIcon}
        />
      )
    },
    {
      label: intl.formatMessage({id:"task.common.status.dropdown.completed", defaultMessage:"Completed"}),
      value: "Completed",
      icon: (
        <RoundIcon
          htmlColor={statusColor.Completed}
          className={classes.statusIcon}
        />
      )
    },
    {
      label: intl.formatMessage({id:"task.common.status.dropdown.cancel", defaultMessage:"Cancelled"}),
      value: "Cancelled",
      icon: (
        <RoundIcon
          htmlColor={statusColor.Cancelled}
          classes={{ root: classes.statusIcon }}
        />
      )
    }
  ];
}
export function priorityData(theme, classes, intl){
    const priorityColor = theme.palette.taskPriority;

      return [
        {
          label: intl.formatMessage({id:"task.common.priority.dropdown.critical", defaultMessage:"Critical"}),
          value: "Critical",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.Critical}
              className={classes.priorityIcon}
            >
              <FlagIcon />{" "}
            </SvgIcon>
          )
        },
        {
          label: intl.formatMessage({id:"task.common.priority.dropdown.high", defaultMessage:"High"}),
          value: "High",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.High}
              className={classes.priorityIcon}
            >
              <FlagIcon />{" "}
            </SvgIcon>
          )
        },
        {
          label: intl.formatMessage({id:"task.common.priority.dropdown.medium", defaultMessage:"Medium"}),
          value: "Medium",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.Medium}
              className={classes.priorityIcon}
            >
              <FlagIcon />{" "}
            </SvgIcon>
          )
        },
        {
          label: intl.formatMessage({id:"task.common.priority.dropdown.low", defaultMessage:"Low"}),
          value: "Low",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.Low}
              className={classes.priorityIcon}
            >
              <FlagIcon />{" "}
            </SvgIcon>
          )
        }
      ];
}