import React from 'react';
import { Link } from "react-router-dom";

const getErrorMessages = (...params) => {
    return {
        ACCOUNT_ALREADY_EXISTS: <span>Email {params[0]} already exists. Please <Link to="/account/login">click here</Link> to sign in.</span>,
        ACCOUNT_BLOCKED: `Your account has been locked for 5 minutes.`,
        CHARACTERS_NAME_FIELD: `Oops! ${params[0]} can only include alphanumeric, hyphen(-).`,
        CONFIRM_PASSWORD_FIELD: `Oops! Both passwords must be same.`,
        EMPTY_FIELD: `Oops! Please enter ${params[0]}.`,
        EMAIL_NOT_FOUND: `Oops! The email or username does not exist. Let's sign you up.`,
        EMPTY_TITLE: `Oops! Please specify ${params[0]}.`,
        FEEDBACK_SENT: `Thank you for your feedback. We will get back to you.`,
        FILE_SUCCESSFULLY_UPLOADED: `File is successfully uploaded.`,
        LETS_SIGN_UP: <span>Email address or username {params[0]} does not exist. Please <Link to={`/account/register?email=${params[0]}`}>click here</Link> to sign up.</span>,
        WL_SIGN_UP: <span>Email address or username {params[0]} does not exist.</span>,
        MAX_LENGTH: `Oops! Length must not exceed ${params[0]} characters.`,
        MIN_LENGTH: `Oops! Length must be greater than ${params[0]} characters.`,
        INTERNAL_SERVER_ERROR: `There is an error while ${params[0]}. Please refresh the page & try again.`,
        INVALID: `Oops! Invalid ${params[0]} entered.`,
        INVALID_EMAIL: `There seems to be an issue with your email address, please review the spelling and try again.`,
        INVALID_FEEDBACK_FILE_EXTENSION: `Oops! File format is invalid. Please use PNG, GIF, JPEG, JPG, MP4, MPG, SVG, ZIP, RAR, WEBM, WMV, SWF, MOV, MKV or BMP file.`,
        INVALID_IMAGE_EXTENSION: `Oops! File format is invalid. Please use PNG, GIF or JPEG file.`,
        INVALID_IMPORT_EXTENSION: `Oops! File format is invalid. Please use XLS, XLSX or CSV file.`,
        INVALID_MS_IMPORT_EXTENSION: `Oops! File format is invalid. Please use MPP file.`,
        INVALID_PHONE: `Please enter a valid phone number.`,
        INVITE_FAILED: `System is unable to send invite. Please try again.`,
        SIGNIN_ACCOUNT_EXISTS: <span>You have already signed in using social account. If you want to sign in here, please <a onClick={params[0]}>click here</a> to set nTask password.</span>,
        SIGNUP_REGISTERED_EMAIL: <span>Email {params[0]} already exists. Please <a onClick={params[1]}>click here</a> to reset password.</span>,
        UNIQUE_WORKSPACE_URL: `This URL already exists. Please enter different URL.`,
        UNVERFIED_EMAIL: <span>Email {params[0]} already exists. Please <a onClick={params[1]}>click here</a> to set password.</span>,
        USER_NAME: `Oops! ${params[0]} can only include alphanumeric, hyphen(-), underscore(_), period(.).`,
        VERIFY_EMAIL: <span>Email {params[0]}  is not verified. Please <a onClick={params[1]}>click here</a> to verify email address.</span>,
        UPLOAD_FAILED: `Unable to upload ${params[0]}. Please try again.`
    }
}
export default getErrorMessages;
