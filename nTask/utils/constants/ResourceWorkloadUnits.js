export const ResourceWorkloadUnits = {
  hour: {
    uiText: "Hours (h)",
    uiSelectedText: "Hours",
    symbol: "h",
    value: "hour",
    selected: true,
  },
  percentage: {
    uiText: "Percentages (%)",
    uiSelectedText: "Percentages",
    symbol: "%",
    value: "percentage",
    selected: false,
  },
  fullTimeEquivalents: {
    uiText: "Full-time equivalents (FTE)",
    uiSelectedText: "FTE",
    symbol: "FTE",
    value: "fte",
    selected: false,
  },
};
