export const ResourceWorkloadViewType = {
  projects: {
    uiText: "Projects",
    value: "projects",
    selected: false,
  },
  resources: {
    uiText: "Resources",
    value: "resources",
    selected: true,
  },
};
