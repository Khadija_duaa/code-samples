export const ResourceManagerFiltersTabs = {
  workspaces: {
    uiText: "Workspaces",
    value: "workspaces",
    selected: true,
  },
  projects: {
    uiText: "Projects",
    value: "projects",
    selected: false,
  },
  resources: {
    uiText: "Resources",
    value: "resources",
    selected: false,
  },
};
