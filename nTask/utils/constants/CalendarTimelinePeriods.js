export const CalendarTimelinePeriods = {
  week: {
    uiText: "1 week",
    value: "1-week",
    selected: true,
  },
  twoWeeks: {
    uiText: "2 weeks",
    value: "2-weeks",
    selected: false,
  },
  month: {
    uiText: "1 month",
    value: "1-month",
    selected: false,
  },
};
