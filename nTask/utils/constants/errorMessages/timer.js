/** Contains all the error messages of the validation which shows on fronted if condition doesnt met */
export const message = {
    timeFallShort: "Oops! Effort not saved. Please add atleast 1 minute task effort!",
    successMessage : "Task Effort added successfully!"
};
