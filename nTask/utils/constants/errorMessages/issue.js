export const message = {
    issueTitle: {
        maxLength: "Please enter less than 80 characters",
        empty: "Please enter issue title"
    }
}