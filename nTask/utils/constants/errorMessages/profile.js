/** Contains all the error messages of the validation which shows on fronted if condition doesnt met related to profile related validations */
export const message = {
  email: {
    invalidEmail: "Invalid Email Address.",
    emailEmpty: "Oops! Please enter email."
  },
  fullName: {
    invalidName: "Invalid fullName. ",
    fullNameEmpty: "Oops! Please enter Full Name."
  },
  password: {
    characterLimit: "Oops! Length must be greater than 8 characters.",
    passwordEmpty: "Oops! Please enter password."
  }
};
