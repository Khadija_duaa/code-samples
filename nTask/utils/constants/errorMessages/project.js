export const message = {
    projectTitle: {
        maxLength: "Please enter less than 80 characters",
        empty: "Please enter project title"
    }
}