/** Contains all the error messages of the validation which shows on fronted if condition doesnt met */
export const message = {
  email: {
    invalidEmail: "Invalid Email Address.",
    emailEmpty: "Oops! Please enter email."
  },
  fullName: {
    invalidName: "Invalid Full Name. ",
    fullNameEmpty: "Oops! Please enter Full Name."
  }
};
