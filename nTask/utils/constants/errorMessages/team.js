/** Contains all the error messages of the validation which shows on fronted if condition doesnt met */
export const message = {
  email: {
    emailAlreadyExist: "Member already added in the invitation list",
    alreadyTeamMember: "Member is already part of the team.",
    ownEmail: "You cannot enter your own email address.",
    teamInviteLimit:
      "Your team has reached maximum limit (5 users) on nTask Basic plan"
  },
  name: {
    empty: "Oops! Please enter team name."
  }
};
