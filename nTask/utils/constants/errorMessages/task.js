export const message = {
    taskTitle: {
        maxLength: "Please enter less than 80 characters",
        empty: "Please enter task title"
    }
}