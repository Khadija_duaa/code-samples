export const TaskTimelineColor = {
  taskStatus: {
    uiText: "Status",
    value: "taskStatus",
    selected: true,
  },
  taskPriority: {
    uiText: "Priority",
    value: "taskPriority",
    selected: false,
  },
  taskColor: {
    uiText: "Task Color",
    value: "taskColor",
    selected: false,
  },
};
