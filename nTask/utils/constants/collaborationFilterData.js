const filterData = [
    {label: 'Reply Later', value: 'Reply Later'},
    {label: 'Group Conversations', value: 'Group'},
    {label: 'Private Conversations', value: 'Private'},
    {label: 'Unread Conversations', value: 'Unread'},
    {label: 'Muted Conversations', value: 'Muted'},
]

export default filterData;