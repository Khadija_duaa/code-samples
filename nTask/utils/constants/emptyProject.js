export const emptyProject = {
  "createdDate": new Date(),
  "updatedDate": new Date(),
  "createdBy": "",
  "createdById": "",
  "id": "",
  "teamId": "",
  "position": 0,
  "projectId": "",
  "projectTemplateId": "",
  "projectPermission": {
    "roleId": "001",
    "userId": "5bb4a6fd550cd10bb0a9bf6d",
    "roleName": "Owner",
    "projectId": null,
    "description": null,
    "isDefault": true,
    "permission": {
      "workSpaceId": null,
      "workSpace": {
        "label": "Workspaces",
        "description": "Complete autonomy to create projects, assign tasks, meetings, timesheets, risks & issues",
        "workspaceSetting": {
          "generalInformation": {
            "workspaceImage": {
              "label": "Workspace Image",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": null,
              "isAllowDelete": true
            },
            "label": "General Information",
            "cando": true,
            "isDeleteWorkSpace": {
              "label": "Delete Workspace",
              "cando": true
            },
            "isWorkSpaceName": {
              "label": "Workspace Name",
              "isReadonly": null,
              "isAllowAdd": null,
              "isAllowEdit": true,
              "isAllowDelete": null
            },
            "isWorkspaceURL": {
              "label": "Workspace Url",
              "isReadonly": null,
              "isAllowAdd": null,
              "isAllowEdit": true,
              "isAllowDelete": null
            }
          },
          "label": "Workspace Setting",
          "cando": true,
          "usersManagement": {
            "label": "User Management",
            "cando": true,
            "addMembers": {
              "label": "Add Members to workspace",
              "cando": true
            },
            "removeMembers": {
              "label": "Remove Members from workspace",
              "cando": true
            },
            "enableMembers": {
              "label": "Enable Members in workspace",
              "cando": true
            },
            "disableMembers": {
              "label": "Disable Members in workspace",
              "cando": true
            },
            "updateRole": {
              "label": "Update Role in workspace",
              "cando": true
            }
          },
          "rolesPermissions": {
            "customRoles": {
              "label": "Custom Role",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": true
            },
            "label": "Roles & Permission",
            "cando": true,
            "renameCustomRole": {
              "label": "Rename Custom Role",
              "cando": true
            }
          },
          "tasksStatus": {
            "label": "Task Status",
            "cando": true,
            "createRenameTemplate": {
              "label": "Create/Rename Template",
              "cando": true
            },
            "setWorkspaceDefault": {
              "label": "Set Template as Workspace Default",
              "cando": true
            },
            "saveNewTemplate": {
              "label": "Save as New Template",
              "cando": true
            },
            "changeDefaultStatus": {
              "label": "Change Default Status",
              "cando": true
            },
            "changeDoneStatus": {
              "label": "Change Done Status",
              "cando": true
            },
            "deleteSavedTempplate": {
              "label": "Delete Saved Template",
              "cando": true
            },
            "templateStatus": {
              "label": "Template Status",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": true
            }
          }
        }
      },
      "project": {
        "projectDetails": {
          "projectDocuments": {
            "deleteDocument": {
              "label": "Delete Document",
              "cando": true
            },
            "openFolder": {
              "label": "Open Folder",
              "cando": true
            },
            "renameFolder": {
              "label": "Rename Folder",
              "cando": true
            },
            "viewDocument": {
              "label": "View Document",
              "cando": true
            },
            "label": "Project Documents",
            "cando": true,
            "addFolder": {
              "label": "Add Folder",
              "cando": true
            },
            "uploadDocument": {
              "label": "Upload Document",
              "cando": true
            },
            "downloadDocument": {
              "label": "Download Document",
              "cando": true
            }
          },
          "detailsTabs": {
            "label": "View Project Detail Tab",
            "cando": true,
            "billingMode": {
              "label": "Billable/Non-Billable",
              "cando": true
            },
            "billingMethode": {
              "label": "Billing Method",
              "cando": true
            },
            "currency": {
              "label": "Currency",
              "cando": true
            }
          },
          "resourceTabs": {
            "label": "View Project Resources Tab",
            "cando": true,
            "addProjectResources": {
              "label": "Add Project Resources",
              "cando": true
            },
            "jobRole": {
              "label": "Job Role",
              "cando": true
            },
            "weeklyCapacity": {
              "label": "Weekly Capacity",
              "cando": true
            },
            "resourceBillable": {
              "label": "Resource Billable",
              "cando": true
            },
            "addRate": {
              "label": "Add Resource Rate",
              "cando": true
            },
            "projectPermissions": {
              "label": "Project Permissions",
              "cando": true
            },
            "deleteResource": {
              "label": "Delete Resource",
              "cando": true
            }
          },
          "taskTabs": {
            "addMore": {
              "label": "Add More",
              "cando": true,
              "createAddSubTask": {
                "label": "Create/ Add subtask",
                "cando": true
              },
              "attachment": {
                "label": "Attachment",
                "isReadonly": null,
                "isAllowAdd": true,
                "isAllowEdit": null,
                "isAllowDelete": true
              },
              "repeatTask": {
                "label": "Repeat Task",
                "isReadonly": null,
                "isAllowAdd": true,
                "isAllowEdit": true,
                "isAllowDelete": true
              }
            },
            "label": "View Project Task Tab",
            "cando": true,
            "addProjectTask": {
              "label": "Add Project Tasks",
              "cando": true
            },
            "taskDetails": {
              "label": "Task Details",
              "cando": true
            },
            "importBulkTasks": {
              "label": "Import Bulk Tasks",
              "cando": true
            },
            "taskBillable": {
              "label": "Task Billable",
              "cando": true
            },
            "taskPriority": {
              "label": "Task Priority",
              "isReadonly": null,
              "isAllowAdd": null,
              "isAllowEdit": true,
              "isAllowDelete": null
            },
            "taskStatus": {
              "label": "Task Status",
              "isReadonly": null,
              "isAllowAdd": null,
              "isAllowEdit": true,
              "isAllowDelete": null
            },
            "taskName": {
              "label": "Task Name",
              "isReadonly": null,
              "isAllowAdd": null,
              "isAllowEdit": true,
              "isAllowDelete": null
            },
            "estimatedTime": {
              "label": "Estimated Time",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": true
            },
            "addRate": {
              "label": "Add Task Rate",
              "cando": true
            },
            "publicLink": {
              "label": "Public Link",
              "cando": true
            },
            "color": {
              "label": "Color",
              "cando": true
            },
            "copyTask": {
              "label": "Copy Task",
              "cando": true
            },
            "archiveTask": {
              "label": "Archive Task",
              "cando": true
            },
            "unlinkTask": {
              "label": "Unlink Task",
              "cando": true
            },
            "deleteTask": {
              "label": "Delete Task",
              "cando": true
            },
            "toDoList": {
              "label": "To-Do List",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": true
            },
            "taskAssignee": {
              "label": "Task Assignee",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": null,
              "isAllowDelete": true
            },
            "plannedStartDate": {
              "label": "Planned Start Date",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": null
            },
            "plannedEndDate": {
              "label": "Planned End Date",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": null
            },
            "actualStartDate": {
              "label": "Actual Start Date",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": null
            },
            "actualEndDate": {
              "label": "Actual End Date",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": null
            },
            "taskDescription": {
              "label": "Task Description",
              "cando": true
            }
          },
          "mileStoneTab": {
            "label": "View Project Milestones Tab",
            "cando": true,
            "addProjectMilestone": {
              "label": "Add Project Milestone",
              "cando": true
            },
            "milestoneName": {
              "label": "Milestone Name",
              "isReadonly": null,
              "isAllowAdd": null,
              "isAllowEdit": true,
              "isAllowDelete": null
            },
            "setMilestoneDate": {
              "label": "Set Milestone Date",
              "cando": true
            },
            "completeMilestone": {
              "label": "Complete Milestone",
              "cando": true
            },
            "billingMode": {
              "label": "Milestone Billable/Unbillable",
              "cando": true
            },
            "deleteMilestone": {
              "label": "Delete Milestone",
              "cando": true
            }
          },
          "financialSummaryTab": {
            "label": "View Project Financial Summary Tab",
            "cando": true,
            "billingDetails": {
              "label": "View Billing Details",
              "cando": true
            },
            "estimatedCost": {
              "label": "View Estimated Cost",
              "cando": true
            },
            "actualCost": {
              "label": "View Actual Cost",
              "cando": true
            },
            "emailsAlerts": {
              "label": "Select Email Alerts",
              "cando": true
            }
          },
          "calenderTab": {
            "label": "View Work Calendar Tab",
            "cando": true,
          },
          "projectComments": {
            "projectattachment": {
              "label": "Project attachments",
              "isReadonly": false,
              "isAllowAdd": true,
              "isAllowEdit": null,
              "isAllowDelete": true
            },
            "comments": {
              "label": "Add Comments",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": true
            },
            "convertToTask": {
              "label": "Convert Comment to Task",
              "cando": true
            },
            "showReceipt": {
              "label": "Show Receipt",
              "cando": true
            },
            "clearAllReplies": {
              "label": "Clear All Replies",
              "cando": true
            },
            "clearReply": {
              "label": "Clear Reply",
              "cando": true
            },
            "viewAttachment": {
              "label": "View Attachment",
              "cando": true
            },
            "label": "Project Comments",
            "cando": true,
            "downloadAttachment": {
              "label": "Download Attachment",
              "cando": true
            },
            "replyLater": {
              "label": "Reply Later",
              "cando": true
            },
            "reply": {
              "label": "Reply",
              "cando": true
            }
          },
          "customsFields": {
            "label": "Add Custom Fields",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "isUseField": {
            "label": "Use This Field",
            "cando": true
          },
          "isHideField": {
            "label": "Hide This Field",
            "cando": true
          },
          "isUpdateField": {
            "label": "Filled Custom Fields",
            "cando": true
          },
          "label": "Project Details",
          "cando": true,
          "editProjectName": {
            "label": "Project Name",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "editProjectStatus": {
            "label": "Project Status",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "projectColor": {
            "label": "Project Color",
            "cando": true
          },
          "projectUpdates": {
            "label": "Project Updates",
            "cando": true
          },
          "projectDescription": {
            "label": "Project Description",
            "cando": true
          }
        },
        "label": "Projects",
        "description": "Assign deliverables to team member and delegate permissions to define tasks with milestones",
        "cando": true,
        "createProject": {
          "label": "Create Projects",
          "cando": true
        },
        "viewAllProject": {
          "label": "View all projects",
          "cando": true
        },
        "exportProject": {
          "label": "Export projects",
          "cando": true
        },
        "importProject": {
          "label": "Import projects",
          "cando": true
        },
        "archive": {
          "label": "Archive projects",
          "cando": true
        },
        "unarchive": {
          "label": "Unarchive projects",
          "cando": true
        },
        "delete": {
          "label": "Delete projects",
          "cando": true
        },
        "copyInWorkSpace": {
          "label": "Copy project within workspace",
          "cando": true
        },
        "accessGantt": {
          "label": "Project Gantt access",
          "cando": true
        },
        "forceProjectPermission": {
          "label": "Force Project Permission",
          "cando": false
        },
        "moveProject": {
          "label": "Move project",
          "cando": true
        }
      },
      "board": {
        "boardDetail": {
          "label": "Board Details",
          "cando": true,
          "addNewStatus": {
            "label": "Add New Status/List",
            "cando": true
          },
          "duplicateStatus": {
            "label": "Duplicate Status",
            "cando": true
          },
          "deleteStatus": {
            "label": "Delete Status/List",
            "cando": true
          },
          "renameStatus": {
            "label": "Rename Status/List",
            "cando": true
          },
          "changeStatusColor": {
            "label": "Change Status Color",
            "cando": true
          },
          "listsetting": {
            "label": "Status Settings",
            "cando": true,
            "prioritysetting": {
              "label": "Status Task Priority",
              "cando": true
            },
            "endDatesetting": {
              "label": "Status Task End Date",
              "cando": true
            },
            "maxTasksetting": {
              "label": "Status Tasks Maximum",
              "cando": true
            }
          },
          "addNewTask": {
            "label": null,
            "cando": true
          },
          "deleteTask": {
            "label": null,
            "cando": true
          },
          "copyTask": {
            "label": null,
            "cando": true
          },
          "archiveTask": {
            "label": null,
            "cando": true
          },
          "addFeatureImage": {
            "label": "Add Featured Image",
            "cando": true
          },
          "deleteFeatureImage": {
            "label": "Delete Featured Image",
            "cando": true
          },
          "changeStatus": {
            "label": null,
            "cando": true
          },
          "viewTaskdetail": {
            "label": null,
            "cando": true
          },
          "importExport": {
            "label": null,
            "cando": true
          },
          "listreorder": {
            "label": "List re-order",
            "cando": true
          },
          "addUnsortedTask": {
            "label": "Add Tasks from Unsorted task to Status/List",
            "cando": true
          },
          "taskreorderInmultiList": {
            "label": "Task re-order within multiple list",
            "cando": true
          }
        },
        "label": "Boards",
        "description": "Input permissions for build kanban boards with projects and tasks.",
        "cando": true,
        "creatBoard": {
          "label": null,
          "cando": true
        },
        "boardName": {
          "label": null,
          "cando": true
        },
        "boardDescription": {
          "label": "Board Description",
          "cando": true
        },
        "backgroundBoardColor": {
          "label": "Background color of board",
          "cando": true
        },
        "saveBoardTemplate": {
          "label": "Save Board as Template",
          "cando": true
        },
        "deleteBoard": {
          "label": null,
          "cando": true
        }
      },
      "task": {
        "taskDetail": {
          "taskProject": {
            "label": "Task Project",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "taskPlannedStartEndDate": {
            "label": "Task Planned start/end date",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "taskActualStartEndDate": {
            "label": "Task Actual start/end date",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "taskDescription": {
            "label": "Task description",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "toDoList": {
            "label": "Todo list",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "taskEfforts": {
            "label": "Task effort",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": null,
            "isAllowDelete": true
          },
          "taskEstimation": {
            "label": "Task Estimation",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": null,
            "isAllowDelete": true
          },
          "repeatTask": {
            "label": "Repeat Task",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "taskAssign": {
            "label": "Task Assignee",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": null,
            "isAllowDelete": true
          },
          "customsFields": {
            "label": "Add Custom Fields",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "isUseField": {
            "label": "Use This Field",
            "cando": true
          },
          "isHideField": {
            "label": "Hide This Field",
            "cando": true
          },
          "isUpdateField": {
            "label": "Filled Custom Fields",
            "cando": true
          },
          "taskDocuments": {
            "deleteDocument": {
              "label": "Delete Document",
              "cando": true
            },
            "openFolder": {
              "label": "Open Folder",
              "cando": true
            },
            "renameFolder": {
              "label": "Rename Folder",
              "cando": true
            },
            "viewDocument": {
              "label": "View Document",
              "cando": true
            },
            "label": "Task Documents",
            "cando": true,
            "addFolder": {
              "label": "Add Folder",
              "cando": true
            },
            "uploadDocument": {
              "label": "Upload Document",
              "cando": true
            },
            "downloadDocument": {
              "label": "Download Document",
              "cando": true
            }
          },
          "taskComments": {
            "taskattachment": {
              "label": "Task Attachments",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": null,
              "isAllowDelete": true
            },
            "comments": {
              "label": "Add Comments",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": true
            },
            "convertToTask": {
              "label": "Convert Comment to Task",
              "cando": true
            },
            "showReceipt": {
              "label": "Show Receipt",
              "cando": true
            },
            "clearAllReplies": {
              "label": "Clear All Replies",
              "cando": true
            },
            "clearReply": {
              "label": "Clear Reply",
              "cando": true
            },
            "viewAttachment": {
              "label": "View Attachment",
              "cando": true
            },
            "label": "Task Comments",
            "cando": true,
            "downloadAttachment": {
              "label": "Download Attachment",
              "cando": true
            },
            "replyLater": {
              "label": "Reply Later",
              "cando": true
            },
            "reply": {
              "label": "Reply",
              "cando": true
            }
          },
          "label": "Task Details",
          "cando": true,
          "editTaskName": {
            "label": "Task Name",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "editTaskPriority": {
            "label": "Task priority",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "repeatTaskOccurances": {
            "label": "Create repeat task occurances",
            "cando": true
          },
          "editTaskStatus": {
            "label": "Task Status",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          }
        },
        "label": "Tasks",
        "description": "Delegate permissions to define tasks with deadlines and priority ratings, comment on work done and hold task-related meetings",
        "cando": true,
        "createTask": {
          "label": "Create Task",
          "cando": true
        },
        "viewalltasks": {
          "label": "View all tasks",
          "cando": true
        },
        "importTasks": {
          "label": "Import tasks",
          "cando": true
        },
        "exportTasks": {
          "label": "Export tasks",
          "cando": true
        },
        "archive": {
          "label": "Archive task",
          "cando": true
        },
        "unarchive": {
          "label": "Unarchive task",
          "cando": true
        },
        "delete": {
          "label": "Delete task",
          "cando": true
        },
        "copyInWorkSpace": {
          "label": "Copy task within workspace",
          "cando": true
        },
        "setColor": {
          "label": "Set color",
          "cando": true
        },
        "publicLink": {
          "label": "Create public link of task",
          "cando": true
        }
      },
      "meeting": {
        "meetingDetail": {
          "meetingAgenda": {
            "label": "Meeting agenda",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "meetingDiscussionNotes": {
            "label": "Meeting discussion notes",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": null,
            "isAllowDelete": true
          },
          "meetingFollowUpActions": {
            "label": "Meeting follow up actions",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "meetingKeyDecisions": {
            "label": "Meeting key decisions",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "repeatMeeting": {
            "label": "Repeat Meeting",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "meetingDate": {
            "label": "Meeting Date",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "meetingTask": {
            "label": "Meeting task",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "meetingProject": {
            "label": "Meeting Project",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "meetingDuration": {
            "label": "Meeting duration",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "meetingLocation": {
            "label": "Meeting location",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "meetingAttendee": {
            "label": "Meeting attendee",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": null,
            "isAllowDelete": true
          },
          "customsFields": {
            "label": "Add Custom Fields",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "isUseField": {
            "label": "Use This Field",
            "cando": true
          },
          "isHideField": {
            "label": "Hide This Field",
            "cando": true
          },
          "isUpdateField": {
            "label": "Filled Custom Fields",
            "cando": true
          },
          "label": "Meeting Details",
          "cando": true,
          "canEditName": {
            "label": "Meeting name",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "canSubmitMeeting": {
            "label": "Meeting Submit for review",
            "cando": true
          },
          "canPublishMeeting": {
            "label": "Publish meeting",
            "cando": true
          },
          "canCreateRepeatMeeting": {
            "label": "Create repeat meeting occurances",
            "cando": true
          },
          "canEditMeetingTime": {
            "label": "Meeting time",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          }
        },
        "label": "Meetings",
        "description": "Set roles for members to hold location specific and time-bound team meetings with workspace members",
        "cando": true,
        "createMeeting": {
          "label": "Create Meeting",
          "cando": true
        },
        "viewAllMeetings": {
          "label": "View all meetings",
          "cando": true
        },
        "importMeetings": {
          "label": "Import meeting",
          "cando": true
        },
        "exportMeetings": {
          "label": "Export meeting",
          "cando": true
        },
        "archiveMeetings": {
          "label": "Archive meeting",
          "cando": true
        },
        "unarchiveMeetings": {
          "label": "Unarchive meeting",
          "cando": true
        },
        "deleteMeeting": {
          "label": "Delete meeting",
          "cando": true
        },
        "cancelMeeting": {
          "label": "Cancel meeting",
          "cando": true
        }
      },
      "issue": {
        "issueDetail": {
          "issueProject": {
            "label": "Issue Project",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "issueTask": {
            "label": "Issue task",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "issuePlanndStartEndDate": {
            "label": "Issue planned start/end date",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "issueActualstartEndDate": {
            "label": "Issue actual start/end date",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "issueDescription": {
            "label": "Issue description",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "issueType": {
            "label": "Issue Type",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "issueAssignee": {
            "label": "Issue Assignee",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": null,
            "isAllowDelete": true
          },
          "customsFields": {
            "label": "Add Custom Fields",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "isUseField": {
            "label": "Use This Field",
            "cando": true
          },
          "isHideField": {
            "label": "Hide This Field",
            "cando": true
          },
          "isUpdateField": {
            "label": "Filled Custom Fields",
            "cando": true
          },
          "issuecomments": {
            "issueattachment": {
              "label": "Issue Attachment",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": null,
              "isAllowDelete": true
            },
            "comments": {
              "label": "Add Comments",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": true
            },
            "convertToTask": {
              "label": "Convert Comment to Task",
              "cando": true
            },
            "showReceipt": {
              "label": "Show Receipt",
              "cando": true
            },
            "clearAllReplies": {
              "label": "Clear All Replies",
              "cando": true
            },
            "clearReply": {
              "label": "Clear Reply",
              "cando": true
            },
            "viewAttachment": {
              "label": "View Attachment",
              "cando": true
            },
            "label": "Issue comments/updates",
            "cando": true,
            "downloadAttachment": {
              "label": "Download Attachment",
              "cando": true
            },
            "replyLater": {
              "label": "Reply Later",
              "cando": true
            },
            "reply": {
              "label": "Reply",
              "cando": true
            }
          },
          "issueDocuments": {
            "deleteDocument": {
              "label": "Delete Document",
              "cando": true
            },
            "openFolder": {
              "label": "Open Folder",
              "cando": true
            },
            "renameFolder": {
              "label": "Rename Folder",
              "cando": true
            },
            "viewDocument": {
              "label": "View Document",
              "cando": true
            },
            "label": "Issue Documents",
            "cando": true,
            "addFolder": {
              "label": "Add Folder",
              "cando": true
            },
            "uploadDocument": {
              "label": "Upload Document",
              "cando": true
            },
            "downloadDocument": {
              "label": "Download Document",
              "cando": true
            }
          },
          "label": "Issue Details",
          "cando": true,
          "editIssueName": {
            "label": "Issue Name",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "moveIssue": {
            "label": "Move Issue",
            "cando": true
          },
          "issueStatus": {
            "label": "Issue status",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "issuePriority": {
            "label": "Issue priority",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "issueSeverity": {
            "label": "Issue Severity",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          }
        },
        "label": "Issues",
        "description": "Input permissions for reporting issues with tasks and other bugs",
        "cando": true,
        "creatIssue": {
          "label": "Create Issue",
          "cando": true
        },
        "viewAllIssues": {
          "label": "View all issues",
          "cando": true
        },
        "importIssues": {
          "label": "Import issues",
          "cando": true
        },
        "exportIssues": {
          "label": "Export issues",
          "cando": true
        },
        "archive": {
          "label": "Archive issues",
          "cando": true
        },
        "unarchives": {
          "label": "Unarchive issues",
          "cando": true
        },
        "delete": {
          "label": "Delete issues",
          "cando": true
        },
        "setColor": {
          "label": "Set color",
          "cando": true
        }
      },
      "risk": {
        "riskDetails": {
          "riskTask": {
            "label": "Risk Task",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "riskProject": {
            "label": "Risk Project",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "riskDescription": {
            "label": "Risk description",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "riskMitigationPlan": {
            "label": "Risk mitigation plan",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "riskOwner": {
            "label": "Risk owner",
            "cando": true
          },
          "isUseField": {
            "label": "Use This Field",
            "cando": true
          },
          "isHideField": {
            "label": "Hide This Field",
            "cando": true
          },
          "isUpdateField": {
            "label": "Filled Custom Fields",
            "cando": true
          },
          "customsFields": {
            "label": "Add Custom Fields",
            "isReadonly": null,
            "isAllowAdd": true,
            "isAllowEdit": true,
            "isAllowDelete": true
          },
          "riskDocuments": {
            "deleteDocument": {
              "label": "Delete Document",
              "cando": true
            },
            "openFolder": {
              "label": "Open Folder",
              "cando": true
            },
            "renameFolder": {
              "label": "Rename Folder",
              "cando": true
            },
            "viewDocument": {
              "label": "View Document",
              "cando": true
            },
            "label": "Risk Documents",
            "cando": true,
            "addFolder": {
              "label": "Add Folder",
              "cando": true
            },
            "uploadDocument": {
              "label": "Upload Document",
              "cando": true
            },
            "downloadDocument": {
              "label": "Download Document",
              "cando": true
            }
          },
          "riskComments": {
            "riskAttachment": {
              "label": "Risk attachments",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": null,
              "isAllowDelete": true
            },
            "comments": {
              "label": "Add Comments",
              "isReadonly": null,
              "isAllowAdd": true,
              "isAllowEdit": true,
              "isAllowDelete": true
            },
            "convertToTask": {
              "label": "Convert Comment to Task",
              "cando": true
            },
            "showReceipt": {
              "label": "Show Receipt",
              "cando": true
            },
            "clearAllReplies": {
              "label": "Clear All Replies",
              "cando": true
            },
            "clearReply": {
              "label": "Clear Reply",
              "cando": true
            },
            "viewAttachment": {
              "label": "View Attachment",
              "cando": true
            },
            "label": "Risk comments/updates",
            "cando": true,
            "downloadAttachment": {
              "label": "Download Attachment",
              "cando": true
            },
            "replyLater": {
              "label": "Reply Later",
              "cando": true
            },
            "reply": {
              "label": "Reply",
              "cando": true
            }
          },
          "label": "Risk Details",
          "cando": true,
          "editRiskName": {
            "label": "Risk Name",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "editRiskStatus": {
            "label": "Risk status",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "editlikelihood": {
            "label": "Risk likelihood",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          },
          "editRiskImpact": {
            "label": "Risk Impact",
            "isReadonly": null,
            "isAllowAdd": null,
            "isAllowEdit": true,
            "isAllowDelete": null
          }
        },
        "label": "Risks",
        "description": "List down anticipated risks with tasks and their likelihood",
        "cando": true,
        "createRisk": {
          "label": "Create Risk",
          "cando": true
        },
        "viewAllRisks": {
          "label": "View all risks",
          "cando": true
        },
        "importRisks": {
          "label": "Import risks",
          "cando": true
        },
        "exportRisks": {
          "label": "Export risks",
          "cando": true
        },
        "archive": {
          "label": "Archive risks",
          "cando": true
        },
        "unarchives": {
          "label": "Unarchive risks",
          "cando": true
        },
        "delete": {
          "label": "Delete risks",
          "cando": true
        },
        "color": {
          "label": "Set color",
          "cando": true
        }
      },
      "timesheet": {
        "label": "Timesheet",
        "description": "Establish accountability of project members through enforcing weekly timesheet entries",
        "cando": true,
        "addTask": {
          "label": "Add Task",
          "cando": true
        },
        "addTimeEntry": {
          "label": "Add time entry",
          "cando": true
        },
        "deleteTimeEntry": {
          "label": "Delete time entry",
          "cando": true
        },
        "deleteTimesheet": {
          "label": "Delete timesheet",
          "cando": true
        },
        "submitTimesheet": {
          "label": "Submit timesheet for approval",
          "cando": true
        },
        "resubmitTimeSheet": {
          "label": "Resubmit timesheet for approval",
          "cando": true
        },
        "sendTimesheet": {
          "label": "Send timesheet",
          "cando": true
        },
        "addNotes": {
          "label": "Add notes",
          "cando": true
        },
        "acceptTimesheet": {
          "label": "Accept timesheet",
          "cando": true
        },
        "rejectTimesheet": {
          "label": "Reject timesheet",
          "cando": true
        }
      }
    },
  },
  "uniqueId": "-",
  "projectName": "",
  "description": "",
  "budget": 0,
  "currency": "USD",
  projectTemplate: {},
  "slackChannelId": null,
  "status": 0,
  "colorCode": "",
  "projectManager": [],
  "isStared": false,
  "customFieldData": null,
  "resources": [],
  "tasks": 0,
  "meetings": 0,
  "issues": 0,
  "risks": 0,
  "progress": 0,
  "billingType": 0,
  "billingMethod": 0,
  "pendingTasks": 4,
  "completedTasks": 0,
  "projectStatusTemplate": "nTask Standard",
  "milestones": 0,
  "documents": 0,
  "comments": 0,
  "estimatedTime": "00:00:00",
  "totalTimeLogged": "00:00:00",
  "ApprovedTime": "00:00:00"
}