export const nTaskSignalConnectionOpen = (userId) => {
  $.connection.hub
    .start({ jsonp: true, transport: ["webSockets", "longPolling"] })
    .done(() => {
      $.connection.notificationHub.server.joinGroup(
        userId
      );
    });
};

export default { nTaskSignalConnectionOpen };
