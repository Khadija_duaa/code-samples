import firebase from 'firebase/app';
import auth from 'firebase/auth';

import { LINKEDIN_CLIENT_ID, FIREBASE_CONFIG } from './constants/externalConfig';
import { apiInstance } from './common';

firebase.initializeApp(
    FIREBASE_CONFIG
);

export const facebookProvider = new firebase.auth.FacebookAuthProvider();
export const googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.addScope('https://www.googleapis.com/auth/userinfo.email');
export const twitterProvider = new firebase.auth.TwitterAuthProvider();


export const getSocialLoginInfo = (provider) => {
    if (provider === 'LinkedIn')
        return linkedInSignIn();
    return firebase.auth().signInWithPopup(provider);
}

const linkedInSignIn = () => {
    let linkedInPopup = null;
    let code = null;
    return new Promise(function (resolve, reject) {
        
        let linkedInAuthenLink = `https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${LINKEDIN_CLIENT_ID}&redirect_uri=${window.location.origin}/signin-linkedin&state=fdsf78fyds7fm&scope=w_member_social`;

        const receiveMessage = function (event) {
            if (event.origin === window.location.origin) {

                if (event.data.errorMessage && event.data.from === 'Linked In') {
                    linkedInPopup = null;
                    reject({
                        message: event.data.errorMessage
                    });
                } else if (event.data.access_token && event.data.from === 'Linked In') {
                    if (code === null) {
                        code = event.data.access_token
                        resolve({
                            credential: {
                                accessToken: event.data.access_token
                            }
                        });
                    }
                    linkedInPopup = null;
                }
            }
        }

        if (linkedInPopup)
            linkedInPopup.close();
        let dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
        let dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

        let width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        let height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        let systemZoom = width / window.screen.availWidth;
        let left = (width - 600) / 2 / systemZoom + dualScreenLeft
        let top = (height - 600) / 2 / systemZoom + dualScreenTop
        linkedInPopup = window.open(linkedInAuthenLink, '_blank', `toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=${600 / systemZoom},height=${600 / systemZoom},top=${top},left=${left}`);
        window.removeEventListener('message', receiveMessage, false);
        code = null;
        window.addEventListener('message', receiveMessage, false);
    });
}