export const translateNotification = (text, intl) => {
    let id = "";
    switch (text) {
        //workspaces
        case "Application Notifications":
            id = "profile-settings-dialog.notification-preferences.list.application-notifications";
            break;

        case "Desktop Notifications":
            id = "profile-settings-dialog.notification-preferences.list.desktop-notifications";
            break;

        case "Task Activities Email":
            id = "profile-settings-dialog.notification-preferences.list.task-activities-email";
            break;

        case "Task Assignment":
            id = "profile-settings-dialog.notification-preferences.list.task-assignment";
            break;

        case "Task Comments":
            id = "profile-settings-dialog.notification-preferences.list.task-comments";
            break;

        case "Task Due Date":
            id = "profile-settings-dialog.notification-preferences.list.task-due-date";
            break;


        case "Task Over Due Date":
            id = "profile-settings-dialog.notification-preferences.list.task-over-due-date";
            break;

        case "Task Reminder":
            id = "profile-settings-dialog.notification-preferences.list.task-reminder";
            break;

        case "Task Status":
            id = "profile-settings-dialog.notification-preferences.list.task-status";
            break;


        case "Task Todo":
            id = "profile-settings-dialog.notification-preferences.list.task-todo";
            break;


        case "Task Meetings Email":
            id = "profile-settings-dialog.notification-preferences.list.task-meetings-email";
            break;

        case "Cancel Meeting":
            id = "profile-settings-dialog.notification-preferences.list.cancel-meeting";
            break;

        case "Meeting Reminder":
            id = "profile-settings-dialog.notification-preferences.list.meeting-reminder";
            break;

        case "Publish MOM":
            id = "profile-settings-dialog.notification-preferences.list.publish-mom";
            break;

        case "Review MOM":
            id = "profile-settings-dialog.notification-preferences.list.review-mom";
            break;

        case "Update Meeting":
            id = "profile-settings-dialog.notification-preferences.list.update-meeting";
            break;

        case "Timesheet Activities Email":
            id = "profile-settings-dialog.notification-preferences.list.timesheet-activities-email";
            break;

        case "Timesheet Approval":
            id = "profile-settings-dialog.notification-preferences.list.timesheet-approval";
            break;

        case "Timesheet Rejection":
            id = "profile-settings-dialog.notification-preferences.list.timesheet-rejection";
            break;

        case "Timesheet Submission":
            id = "profile-settings-dialog.notification-preferences.list.timesheet-submission";
            break;

        case "Issues Notifications":
            id = "profile-settings-dialog.notification-preferences.list.issues-notifications";
            break;

        case "Issue Assignments":
            id = "profile-settings-dialog.notification-preferences.list.issue-assignments";
            break;

        case "Issue Comments":
            id = "profile-settings-dialog.notification-preferences.list.issue-comments";
            break;

        case "Issue Due Date":
            id = "profile-settings-dialog.notification-preferences.list.issue-due-date";
            break;

        case "Issue Reminder":
            id = "profile-settings-dialog.notification-preferences.list.issue-reminder";
            break;

        case "Issue Status":
            id = "profile-settings-dialog.notification-preferences.list.issue-status";
            break;

        case "Issues Update":
            id = "profile-settings-dialog.notification-preferences.list.issues-update";
            break;

        case "Risks Notifications":
            id = "profile-settings-dialog.notification-preferences.list.risks-notifications";
            break;

        case "Risk Assignments":
            id = "profile-settings-dialog.notification-preferences.list.risk-assignments";
            break;

        case "Risk Detail":
            id = "profile-settings-dialog.notification-preferences.list.risk-detail";
            break;

        case "Risk Impact":
            id = "profile-settings-dialog.notification-preferences.list.risk-impact";
            break;

        case "Risk Linked Projects":
            id = "profile-settings-dialog.notification-preferences.list.risk-limited-projects";
            break;

        case "Risk Linked Tasks":
            id = "profile-settings-dialog.notification-preferences.list.risk-linked-tasks";
            break;

        case "Risk Mittigation":
            id = "profile-settings-dialog.notification-preferences.list.risk mittigation";
            break;

        case "Risk Status":
            id = "profile-settings-dialog.notification-preferences.list.risk-status";
            break;

        case "Risk Title":
            id = "profile-settings-dialog.notification-preferences.list.risk-title";
            break;


        case "Risks Comments":
            id = "profile-settings-dialog.notification-preferences.list.risks-comments";
            break;

        case "Chat Notifications":
            id = "profile-settings-dialog.notification-preferences.list.chat-notifications";
            break;

        case "Chat All":
            id = "profile-settings-dialog.notification-preferences.list.chat-all";
            break;

        case "Chat Un Read":
            id = "profile-settings-dialog.notification-preferences.list.chat-un-read";
            break;

        case "None":
            id = "profile-settings-dialog.notification-preferences.list.none";
            break;
    }
    return id == "" ? text : intl.formatMessage({ id: id, defaultMessage: text });
}

