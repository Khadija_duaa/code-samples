import moment from "moment";

export const convertDayToWeekday = (day) => {
    switch (day) {
        case 0:
            return "Sunday";
        case 1:
            return "Monday";
        case 2:
            return "Tuesday";
        case 3:
            return "Wednesday";
        case 4:
            return "Thursday";
        case 5:
            return "Friday";
        case 6:
            return "Saturday";
        case 7:
            return "Sunday";
        default:
            return "Monday";
    }
}

export const convertWeekdayToDay = (weekDay) => {
    switch (weekDay) {
        case "Sunday":
            return 0;
        case "Monday":
            return 1;
        case "Tuesday":
            return 2;
        case "Wednesday":
            return 3;
        case "Thursday":
            return 4;
        case "Friday":
            return 5;
        case "Saturday":
            return 6;
        default:
            return 1;
    }
}

// convert time to "x date/time ago"
export const getChatTime = (date) => {
        let _time;
        if (moment(date).fromNow() === "a few seconds ago" || moment(date).fromNow() === "a minute ago") {
            _time = "Just now"
        }
        else if (moment(date).fromNow().includes("minutes")) {
            if (moment(date).fromNow()[0] >= 2 && moment(date).fromNow()[0] <= 5) {
                _time = "5 minutes ago"
            }
            else if (moment(date).fromNow()[0] > 5 && moment(date).fromNow()[0] <= 10) {
                _time = "10 minutes ago"
            }
            else if (moment(date).fromNow()[0] > 10 && moment(date).fromNow()[0] <= 15) {
                _time = "15 minutes ago"
            }
            else {
                _time = moment(date).format('hh:mm A')
            }
        }
        else if (moment(date).fromNow().includes("hour") || moment(date).fromNow().includes("hours")) {
            _time = moment(date).format('hh:mm A')
        }
        else {
            _time = moment(date).format("LLL")
        }
        return _time;
    }

