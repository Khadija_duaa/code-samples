import { trim, isNumeric, isInt } from "validator";

//Validating hours
export function validateHours(value = "") {
  
  let trimValue = trim(value); // removing whitespaces from left and right of string
  let hoursValueCheck = isInt(trimValue, { min: 0, max: 24,allow_leading_zeroes: true }); // checking if number in range of 1 and 59
  let numericCheck = isNumeric(trimValue, {no_symbols: true}) // Checking if the string value is number
  let validated = numericCheck && hoursValueCheck ? true : false // Return true if the value is validated else false
  return validated 
}
