import { trim, isNumeric, isInt } from "validator";

//Validating minutes
export function validateMinutes(value = "") {
  let trimValue = trim(value); // removing whitespaces from left and right of string
  let minsValueCheck = isInt(trimValue, { min: 0, max: 59, allow_leading_zeroes: true }); // checking if number in range of 0 and 59
  let numericCheck = isNumeric(trimValue, {no_symbols: true})
  let validated = numericCheck && minsValueCheck ? true : false
  return validated 
}
