import { isEmpty, isEmail } from "validator";
import { message } from "../../constants/errorMessages/profile";
import * as yup from "yup";

//Validating Password field
export function validPassword(value = "") {
  const { password } = message;
  if (isEmpty(value)) {
    // Checking if the value is empty or not
    return { validated: false, errorMessage: password.passwordEmpty };
  } else {
    let validateValue = value.length > 7; /** minimum password character limit validation  */

    return {
      validated: validateValue,
      errorMessage: !validateValue ? password.characterLimit : null,
    }; // errorMessage: if not validated than return invalid email error message
  }
}

export const validatePassword = yup
  .string()
  .min(8, "Password must be Atleast 8 Characters")
  .required("Password is required");
export const validateConfirmPassword = yup
  .string()
  .required("Confirm Password is required")
  .oneOf([yup.ref("password"), null], "Passwords must match");
export const validateFullName = yup.string().required("Full Name is Required").matches(/^[a-zA-Z ]*$/, "Name should contain letters only");

export const numberFieldSchema = yup.object().shape({
  options: yup.array().of(
    yup
      .object()
      .shape({
        value: yup.string().required("${path} Field required"),
      })
      .required()
  ),
});

export const validatePasswordInput = yup
  .string()
  .min(8, "min")
  .max(40, "max")
  .required("max")
  .matches(RegExp("(.*[a-z].*)"), "lowercase")
  .matches(RegExp("(.*[A-Z].*)"), "uppercase")
  .matches(RegExp("(.*\\d.*)"), "number")
  .matches(RegExp('[!@#$%^&*(),.?":{}|<>]'), "special");
