import { isInt, isEmpty, trim, isLength } from "validator";

//Validating rate amount input field
export function validateAmount(value) {
  let intCheck = isInt(value, {
    gt: 0,
    lt: 99999999999,
    allow_leading_zeroes: false,
  });
  let validated = intCheck || isEmpty(value);
  return { validated };
}
