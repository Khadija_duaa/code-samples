import { isInt, isEmpty, trim, isLength } from "validator";

//Validating rate amount input field
export function validateAlertLimit(value) {
  let intCheck = isInt(value, {
    gt: 0,
    lt: 169,
    allow_leading_zeroes: false,
  });
  let validated = intCheck || isEmpty(value);
  return { validated };
}
