// @flow

import { isAlphanumeric } from "validator";
import { message } from "../../constants/errorMessages/profile";
import { isEmpty } from "validator";

//Validating full name
export function validName(value: string) {
  const { fullName } = message;

  let UpdatedValue = value.replace(
    /\s/g,
    ""
  ); /** removing whitespaces and then check the value */

  if (isEmpty(UpdatedValue)) {
    // Validating if value might not be empty
    let validated = false;
    return {
      validated,
      errorMessage: !validated ? fullName.fullNameEmpty : null
    };
  } else {
    var regex = /^[a-zA-Z0-9À-ž-]+$/g;
    let validateValue = regex.test(UpdatedValue);

    let validated = validateValue ? true : false;

    return {
      validated,
      errorMessage: !validated ? fullName.invalidName : null
    };
  }
}
