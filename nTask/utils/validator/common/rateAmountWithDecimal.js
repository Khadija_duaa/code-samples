
//Validating rate amount input field
export function validateAmount(value) {
  let num = parseFloat(value); /** converting sting into number with decimal if any */
  var regex = /^[0-9]\d{0,9}(\.\d{1,3})?%?$/g; /** regex allowing numbers 0  to 9 and decimal with 3 digit length e.g 12.356 */
  let validateValue = regex.test(num);
  let validated = validateValue || value == "";
  return { validated };
}
