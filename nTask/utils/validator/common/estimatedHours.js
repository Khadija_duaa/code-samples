import { isNumeric, isEmpty, trim, isLength } from "validator";

//Validating estimated Hours field
export function validateHours(value) {
  let trimValue = trim(value); // removing whitespaces from left and right of string
  let numberCheck = isNumeric(value);
  let lengthCheck = isLength(trimValue, { min: 1, max: 6 }); // checking if string length is with in range of 1 and 6
  let validated = (trimValue && numberCheck && lengthCheck) || isEmpty(value);
  return { validated };
}
