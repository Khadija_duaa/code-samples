import { isFloat, isInt, isEmpty, trim, isLength } from "validator";

//Validating rate amount input field
export function validateTime(value) {
  let floatCheck = isFloat(value, {
    gt: 0,
    lt: 9999999,
    allow_leading_zeroes: false,
  });
  let intCheck = isInt(value, {
    gt: 0,
    lt: 9999999,
    allow_leading_zeroes: false,
  });
  let validated = intCheck || floatCheck || isEmpty(value);
  return { validated };
}
