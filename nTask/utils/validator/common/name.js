import { isAlphanumeric } from "validator";
import { message } from "../../constants/errorMessages/signUp";
import { isEmpty } from "validator";

//Validating name field of the sign up
export function validName(value = "", key) {
  const { fullName } = message;

  let UpdatedValue = value.replace(
    /\s/g,
    ""
  ); /** removing whitespaces and then check the value */

  if (isEmpty(UpdatedValue) && key === "fullName") {
    let validated = false;
    return {
      validated,
      errorMessage: !validated ? fullName.fullNameEmpty : null
    };
  } else {
    var regex = /^[a-zA-Z0-9À-ž-]+$/g;
    let validateValue = regex.test(UpdatedValue);

    // let validateValue = isAlphanumeric(UpdatedValue); /** only letters, no special characters */

    let validated = validateValue ? true : false;

    if (key === "fullName") {
      return {
        validated,
        errorMessage: !validated ? fullName.invalidName : null
      };
    } else null;
  }
}
