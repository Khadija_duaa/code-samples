import { isEmpty, isEmail } from 'validator';
import { message } from "../../constants/errorMessages/signUp";
import * as yup from "yup";

//Validating Email field of the sign up 
export function validEmail(value = "",intl) {
    const { email } = message;
    if (isEmpty(value)) { // Checking if the value is empty or not
        return { validated: false, errorMessage: intl != undefined ? intl.formatMessage({id:"team-settings.user-management.invite-members.form.validation.invite.empty", defaultMessage: email.emailEmpty}) : email.emailEmpty }
    }
    else {
        let validateValue = isEmail(value); /** validation the email format  */
        let validated = validateValue ? true : false; // if value is valid email it will return true else validated as false

        return { validated, errorMessage: !validated ? intl != undefined ? intl.formatMessage({id:"team-settings.user-management.invite-members.form.validation.invite.invalid-email", defaultMessage: email.invalidEmail}) : email.invalidEmail : null } // errorMessage: if not validated than return invalid email error message
    }
}

export const validateEmail = (intl) => yup.string().email(message.email.invalidEmail).required(message.email.emailEmpty)
export const validateEmailSchema = () => yup.object().shape({email : yup.string().email().required()})