import { isInt, isEmpty, trim, isLength } from "validator";

//Validating rate amount input field
export function validateDailyWorkloadCapacity(value) {
  let intCheck = isInt(value, {
    min: 0,
    max: 24,
    allow_leading_zeroes: false,
  });
  let validated = intCheck || isEmpty(value);
  return { validated };
}
