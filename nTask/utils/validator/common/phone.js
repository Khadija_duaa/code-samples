import { isNumeric } from 'validator';
import { message } from "../../constants/errorMessages/profileInformation";

//Validating phone field 
export function validPhone(value) {
    const { phone } = message;
    if (isNumeric(value)) {
        let validated = true;
        return { validated, errorMessage: phone.invalidPhone }
    }
    else {
        let validated =  false;
        return { validated, errorMessage: null }
    }
}
