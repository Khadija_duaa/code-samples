import { isEmpty, trim, isLength } from "validator";
import {message} from "../../constants/errorMessages/risk";

//Validating risk title
export function validRiskTitle(value = "",intl) {
const {riskTitle} = message;
  let trimValue = trim(value); // removing whitespaces from left and right of string
  let lengthCheck = isLength(trimValue, { min: 1, max: 250 }); // checking if string length is with in range of 1 and 80
  let emptyCheck = isEmpty(value, { ignore_whitespace: true }); // checking if string is not empty
  let error = emptyCheck ? intl.formatMessage({id:"risk.creation-dialog.form.validation.risk-title.empty", defaultMessage:riskTitle.empty}) : !lengthCheck ? riskTitle.maxLength : false
  return error 
}
