import { isEmpty, trim, isLength } from "validator";
import {message} from "../../constants/errorMessages/project";

//Validating project title
export function validProjectTitle(value = "",intl) {
const {projectTitle} = message;
  let trimValue = trim(value); // removing whitespaces from left and right of string
  let lengthCheck = isLength(trimValue, { min: 1, max: 80 }); // checking if string length is with in range of 1 and 80
  let emptyCheck = isEmpty(value, { ignore_whitespace: true }); // checking if string is not empty
  let error = emptyCheck ? formatMessage(intl,"project.creation-dialog.validation.title",projectTitle.empty) : !lengthCheck ? formatMessage(intl,"project.creation-dialog.validation.length",projectTitle.maxLength) : false
  return error 
}
function formatMessage(intl,id,dMessage) {
  return intl.formatMessage({ id:id , defaultMessage: dMessage});
}