import { isEmpty, trim, isLength } from "validator";
import {message} from "../../constants/errorMessages/task";
import { IntlProvider } from 'react-intl';
import messages from '../../../i18n/messages';

// const { formatMessage } = useIntl();

//Validating task title
// export const useCountry = () => {
//   const [country] = useIntl();
//   return country
// }
export function validTaskTitle(value = "",intl) {
  
const {taskTitle} = message;
// const intlProvider = new IntlProvider({ locale : locale == "English" ? "en-GB" : locale, messages: messages[locale]  }, {});
// const intl = intlProvider.state.intl;
// // const country = useCountry();
// // const {formatMessage} = useIntl();
//  locale = "English" ? "en-GB" : locale;

// const { intl } = intlProvider.getChildContext();
// const label = formatMessage({ id: 'login', defaultMessage: 'Login' })
  let trimValue = trim(value); // removing whitespaces from left and right of string
  let lengthCheck = isLength(trimValue, { min: 1, max: 250 }); // checking if string length is with in range of 1 and 80
  let emptyCheck = isEmpty(value, { ignore_whitespace: true }); // checking if string is not empty
  let error = emptyCheck ? intl.formatMessage({ id:"task.creation-dialog.form.validation.title.empty" , defaultMessage:taskTitle.empty}): !lengthCheck ? intl.formatMessage({ id:"task.creation-dialog.form.validation.title.length" , defaultMessage:taskTitle.maxLength}) : false;
  // /Please enter less than 80 characters
  return error 
}
