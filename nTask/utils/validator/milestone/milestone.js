import { isEmpty, trim, isLength } from "validator";

//Validating milestone title
export function validMilestoneTitle(value = "") {
  let trimValue = trim(value); // removing whitespaces from left and right of string
  let emptyCheck = isEmpty(trimValue, { ignore_whitespace: true }); // checking if string is not empty
  return emptyCheck;
}
