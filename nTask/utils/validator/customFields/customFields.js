import {string, array} from 'yup';

export const optionsSchema = array(string().required().length(150).nullable(true));