import { isEmpty, trim, isLength } from "validator";
import {message} from "../../constants/errorMessages/issue";

//Validating issue title
export function validIssueTitle(value = "",intl) {
const {issueTitle} = message;
  let trimValue = trim(value); // removing whitespaces from left and right of string
  let lengthCheck = isLength(trimValue, { min: 1, max: 250 }); // checking if string length is with in range of 1 and 250
  let emptyCheck = isEmpty(value, { ignore_whitespace: true }); // checking if string is not empty
  let error = emptyCheck ? intl.formatMessage({id:"issue.creation-dialog.form.validation.title.empty", defaultMessage:issueTitle.empty}) : !lengthCheck ? intl.formatMessage({id:"issue.creation-dialog.form.validation.title.length", defaultMessage:issueTitle.maxLength}) : false
  return error 
}
