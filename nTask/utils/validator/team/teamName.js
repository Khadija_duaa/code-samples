// @flow

import { isEmpty, trim, isLength } from "validator";
import { message } from "../../constants/errorMessages/team";
import * as yup from "yup";

//Validating task title
export function validTeamName(value: string = "") {
  const { name } = message;
  let trimValue = trim(value); // removing whitespaces from left and right of string
  let emptyCheck = isEmpty(value, { ignore_whitespace: true }); // checking if string is not empty

  let validated = emptyCheck ? false : true;

  return {
    validated,
    errorMessage: !validated ? name.empty : null
  };
}

export const validateTeamName = yup.string().required('Team Name is Required');
export const validateWorkspaceName = yup.string().required('Workspace Name is Required');