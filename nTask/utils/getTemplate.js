import { store } from "../index";
import cloneDeep from "lodash/cloneDeep";

export const getTemplate = element => {
  const state = store.getState();
  let projects = cloneDeep(state.projects.data); /** geting all projects from store */
  let WSTemplates = cloneDeep(
    state.workspaceTemplates.data
  ); /** getting all workspace templates and default template from store */
  /** flat workspace tempaltes */
  let flatAllTemplate = WSTemplates.allWSTemplates.reduce((total, item) => total.concat(item.templates),[])

  if (element && element.projectId && element.projectId !== "") {
    /** if element contains the projectId then perform operations other wise return the default workspace template */
    let project = getProject(element, projects);

    if (project) {
      if (project.projectTemplate && project.projectTemplate.category === 3) {
        /** 3 means this project template is the custom template */
        return project.projectTemplate;
      } else {
        if (flatAllTemplate.length > 0) {
          /** if Workspace template is available then find the template and return */
          let findTemplate =
          flatAllTemplate.find(t => t.templateId === project.projectTemplateId) ||
            null;
          return findTemplate ? findTemplate : project.projectTemplate;

        } else {
          /** else return the project Template */
          return project.projectTemplate;
        }
      }
    }
    else return WSTemplates.defaultWSTemplate;

  } 
  else {
    /** returning default template if condition is false */
    return WSTemplates.defaultWSTemplate;
  }


  function getProject(ele, projects) {
    let attachedProject = projects.find(p => p.projectId === ele.projectId) || null;
    return attachedProject;
  }
};
