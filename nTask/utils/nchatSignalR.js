export const nChatSignalConnectionOpen = (groupId, obj) => {
  $.connection.hub
    .start({ jsonp: true, transport: ["webSockets", "longPolling"] })
    .done(() => {
      $.connection.notificationHub.server.joinChatGroup(
        groupId
      );
      $.connection.notificationHub.server.publishUpdate(
        obj
      );
    });
};

export default { nChatSignalConnectionOpen };
