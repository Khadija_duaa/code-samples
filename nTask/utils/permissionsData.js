const data = {
  defaultRoles: [
    {
      roleId: "002",
      userId: null,
      roleName: "Admin",
      isDefault: true,
      permission: {
        workSpace: {
          label: "Workspaces",
          description: "Description goes here",
          createWorkspaceName: {
            label: " Create Work Space",
            cando: true
          },
          workspaceSetting: {
            generalInformation: {
              workspaceImage: {
                label: "Workspace Image",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: false
              },
              label: "General Information",
              cando: true,
              isDeleteWorkSpace: {
                label: "Delete Workspace",
                cando: false
              },
              isWorkSpaceName: {
                label: "Workspace Name",
                isReadonly: null,
                isAllowAdd: null,
                isAllowEdit: true,
                isAllowDelete: null
              },
              isWorkspaceURL: {
                label: "Workspace Url",
                isReadonly: null,
                isAllowAdd: null,
                isAllowEdit: true,
                isAllowDelete: null
              }
            },
            label: "Workspace Setting",
            cando: true,
            usersManagement: {
              label: "User Management",
              cando: true,
              addMembers: {
                label: "Add Members to workspace",
                cando: true
              },
              removeMembers: {
                label: "Remove Members from workspace",
                cando: false
              },
              enableMembers: {
                label: "Enable Members in workspace",
                cando: false
              },
              disableMembers: {
                label: "Disable Members in workspace",
                cando: false
              },
              updateRole: {
                label: "Update Role in workspace",
                cando: true
              }
            },
            rolesPermissions: {
              customRoles: {
                label: "Custome Role",
                isReadonly: false,
                isAllowAdd: true,
                isAllowEdit: true,
                isAllowDelete: true
              },
              label: "Roles & Permission",
              cando: true,
              renameCustomRole: {
                label: "Rename Custom Role",
                cando: true
              }
            }
          }
        },
        project: {
          projectSetting: {
            label: "Project settings",
            cando: true,
            editResources: {
              label: "Project resource",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editCurrency: {
              label: "Currency",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editBudget: {
              label: "Project Budget",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            }
          },
          label: "Projects",
          description: "Description goes here",
          cando: true,
          viewAllProject: {
            label: "View all projects",
            cando: true
          },
          exportProject: {
            label: "Export projects",
            cando: true
          },
          importProject: {
            label: "Import projects",
            cando: true
          },
          archive: {
            label: "Archive projects",
            cando: true
          },
          unarchive: {
            label: "Unarchive projects",
            cando: true
          },
          delete: {
            label: "Delete projects",
            cando: true
          },
          copyInWorkSpace: {
            label: "Copy project within workspace",
            cando: true
          },
          accessGantt: {
            label: "Project Gantt access",
            cando: true
          },
          editProjectName: {
            label: "Project Name",
            isReadonly: null,
            isAllowAdd: null,
            isAllowEdit: true,
            isAllowDelete: null
          },
          moveProject: {
            label: "Move project",
            cando: true
          }
        },
        task: {
          taskDetail: {
            taskProject: {
              label: "Task Project",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            taskPlannedStartEndDate: {
              label: "Task Planned start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: false,
              isAllowDelete: null
            },
            taskActualStartEndDate: {
              label: "Task Actual start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: false,
              isAllowDelete: null
            },
            taskDescription: {
              label: "Task description",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            toDoList: {
              label: "Todo list",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            taskEfforts: {
              label: "Task effort",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: true
            },
            taskEstimation: {
              label: "Task Estimation",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: false,
              isAllowDelete: true
            },
            repeatTask: {
              label: "Repeat Task",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            taskAssign: {
              label: "Task Assigne",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            addMeetingSchedule: {
              label: "Add Meeting schedule",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: true
            },
            taskAttachment: {
              taskattachment: {
                label: "Task attachments",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: true
              },
              label: "Task Comments"
            },
            label: "Task Name",
            cando: true,
            editTaskName: {
              label: "Task Name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editTaskPriority: {
              label: "Task priority",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            repeatTaskOccurances: {
              label: "Create public link of task",
              cando: true
            },
            editTaskStatus: {
              label: "Task Status",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            addIssue: {
              label: "Add Issue",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: null
            },
            addRisk: {
              label: "Add Risk",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: null
            },
            addComments: {
              label: "Add Comments",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: null
            }
          },
          label: "Tasks",
          description: "Description goes here",
          cando: true,
          viewalltasks: {
            label: "View all tasks",
            cando: true
          },
          importTasks: {
            label: "Import tasks",
            cando: true
          },
          exportTasks: {
            label: "Export tasks",
            cando: true
          },
          archieve: {
            label: "Archive task",
            cando: true
          },
          unarchieve: {
            label: "Unarchive task",
            cando: true
          },
          delete: {
            label: "Delete task",
            cando: true
          },
          copyInWorkSpace: {
            label: "Copy task within workspace",
            cando: true
          },
          setColor: {
            label: "Set color",
            cando: true
          },
          publicLink: {
            label: "Create public link of task",
            cando: true
          }
        },
        meeting: {
          meetingDetail: {
            meetingAgenda: {
              label: "Meeting agenda",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            meetingDiscussionNotes: {
              label: "Meeting discussion notes",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: true
            },
            meetingFollowUpActions: {
              label: "Meeting follow up actions",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            meetingKeyDecisions: {
              label: "Meeting key decisions",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            repeatMeeting: {
              label: "Repeat Meeting",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            meetingDate: {
              label: "Meeting Date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            meetingTask: {
              label: "Meeting task",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            meetingDuration: {
              label: "Meeting duration",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            meetingLocation: {
              label: "Meeting location",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            meetingAttendee: {
              label: "Meeting attendee",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            label: "Meeting Details",
            cando: true,
            canEditName: {
              label: "Meeting name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            canSubmitMeeting: {
              label: "Meeting Submit for review",
              cando: true
            },
            canPublishMeeting: {
              label: "Publish meeting",
              cando: true
            },
            canUnpublishMeeting: {
              label: "Unpublish meeting",
              cando: true
            },
            canCreateRepeatMeeting: {
              label: "Create repeat meeting occurances",
              cando: true
            },
            canEditMeetingTime: {
              label: "Meeting time",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            }
          },
          label: "Meetings",
          description: "Description goes here",
          cando: true,
          viewAllMeetings: {
            label: "View all meetings",
            cando: true
          },
          importMeetings: {
            label: "Import meeting",
            cando: true
          },
          exportMeetings: {
            label: "Export meeting",
            cando: true
          },
          archiveMeetings: {
            label: "Archive meeting",
            cando: true
          },
          unarchiveMeetings: {
            label: "Unarchive meeting",
            cando: true
          },
          deleteMeeting: {
            label: "Delete meeting",
            cando: true
          },
          cancelMeeting: {
            label: "Cancel meeting",
            cando: true
          }
        },
        issue: {
          issueDetail: {
            issueTask: {
              label: "Issue task",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            issuePlanndStartEndDate: {
              label: "Issue planned start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueActualstartEndDate: {
              label: "Issue actual start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueDescription: {
              label: "Issue description",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueType: {
              label: "Issue Type",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueAssignee: {
              label: "Issue Assignee",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            issueAttachment: {
              issueattachment: {
                label: "Issue Attachment",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: true
              },
              label: "Issue Attachments"
            },
            label: "Issue Details",
            cando: true,
            editIssueName: {
              label: "Issue Name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueStatus: {
              label: "Issue status",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issuePriority: {
              label: "Issue priority",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueSeverity: {
              label: "Issue Severity",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueCommentsUpdates: {
              label: "Issue comments/updates",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            }
          },
          label: "Issues",
          description: "Description goes here",
          cando: true,
          viewAllIssues: {
            label: "View all issues",
            cando: true
          },
          importIssues: {
            label: "Import issues",
            cando: true
          },
          exportIssues: {
            label: "Export issues",
            cando: true
          },
          archive: {
            label: "Archive issues",
            cando: true
          },
          unarchives: {
            label: "Unarchive issues",
            cando: true
          },
          delete: {
            label: "Delete issues",
            cando: true
          },
          setColor: {
            label: "set color",
            cando: true
          }
        },
        risk: {
          riskDetails: {
            riskTask: {
              label: "Risk Task",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            riskDescription: {
              label: "Risk description",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            riskMitigationPlan: {
              label: "Risk mitigation plan",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            riskOwner: {
              label: "Risk owner",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            riskAttachment: {
              riskAttachment: {
                label: "Risk attachments",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: true
              },
              label: "Risk Attachments"
            },
            label: "Risk Details",
            cando: true,
            editRiskName: {
              label: "Risk Name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editRiskStatus: {
              label: "Risk status",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editlikelihood: {
              label: "Risk likelihood",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editRiskImpact: {
              label: "Issue Impact",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            riskCommentsUpdates: {
              label: "Risk comments/updates",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            }
          },
          label: "Risks",
          description: "Description goes here",
          cando: true,
          viewAllRisks: {
            label: "View all risks",
            cando: true
          },
          importRisks: {
            label: "Import risks",
            cando: true
          },
          exportRisks: {
            label: "Export risks",
            cando: true
          },
          archive: {
            label: "Archive risks",
            cando: true
          },
          unarchives: {
            label: "Unarchive risks",
            cando: true
          },
          delete: {
            label: "Delete risks",
            cando: true
          },
          color: {
            label: "Set color",
            cando: true
          }
        },
        timesheet: {
          approveTimesheet: true,
          rejectTimesheet: true
        }
      },
      createdDate: "2020-03-16T05:59:53.0116271Z",
      updatedDate: "2020-03-16T05:59:53.0116271Z",
      createdBy: null,
      ownedBy: null,
      version: 0,
      updatedBy: null,
      workSpaceId: null,
      teamId: null,
      isOwner: false,
      position: 0,
      id: "000000000000000000000000"
    },
    {
      roleId: "003",
      userId: null,
      roleName: "Member",
      isDefault: true,
      permission: {
        workSpace: {
          label: "Workspaces",
          description: "Description goes here",
          createWorkspaceName: {
            label: " Create Work Space",
            cando: false
          },
          workspaceSetting: {
            generalInformation: {
              workspaceImage: {
                label: "Workspace Image",
                isReadonly: null,
                isAllowAdd: false,
                isAllowEdit: null,
                isAllowDelete: false
              },
              label: "General Information",
              cando: false,
              isDeleteWorkSpace: {
                label: "Delete Workspace",
                cando: false
              },
              isWorkSpaceName: {
                label: "Workspace Name",
                isReadonly: null,
                isAllowAdd: null,
                isAllowEdit: false,
                isAllowDelete: null
              },
              isWorkspaceURL: {
                label: "Workspace Url",
                isReadonly: null,
                isAllowAdd: null,
                isAllowEdit: false,
                isAllowDelete: null
              }
            },
            label: "Workspace Setting",
            cando: false,
            usersManagement: {
              label: "User Management",
              cando: false,
              addMembers: {
                label: "Add Members to workspace",
                cando: false
              },
              removeMembers: {
                label: "Remove Members from workspace",
                cando: false
              },
              enableMembers: {
                label: "Enable Members in workspace",
                cando: false
              },
              disableMembers: {
                label: "Disable Members in workspace",
                cando: false
              },
              updateRole: {
                label: "Update Role in workspace",
                cando: false
              }
            },
            rolesPermissions: {
              customRoles: {
                label: "Custome Role",
                isReadonly: false,
                isAllowAdd: true,
                isAllowEdit: true,
                isAllowDelete: true
              },
              label: "Roles & Permission",
              cando: true,
              renameCustomRole: {
                label: "Rename Custom Role",
                cando: true
              }
            }
          }
        },
        project: {
          projectSetting: {
            label: "Project settings",
            cando: true,
            editResources: {
              label: "Project resource",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editCurrency: {
              label: "Currency",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editBudget: {
              label: "Project Budget",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            }
          },
          label: "Projects",
          description: "Description goes here",
          cando: true,
          viewAllProject: {
            label: "View all projects",
            cando: true
          },
          exportProject: {
            label: "Export projects",
            cando: true
          },
          importProject: {
            label: "Import projects",
            cando: true
          },
          archive: {
            label: "Archive projects",
            cando: true
          },
          unarchive: {
            label: "Unarchive projects",
            cando: true
          },
          delete: {
            label: "Delete projects",
            cando: true
          },
          copyInWorkSpace: {
            label: "Copy project within workspace",
            cando: true
          },
          accessGantt: {
            label: "Project Gantt access",
            cando: true
          },
          editProjectName: {
            label: "Project Name",
            isReadonly: null,
            isAllowAdd: null,
            isAllowEdit: true,
            isAllowDelete: null
          },
          moveProject: {
            label: "Move project",
            cando: true
          }
        },
        task: {
          taskDetail: {
            taskProject: {
              label: "Task Project",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            taskPlannedStartEndDate: {
              label: "Task Planned start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: false,
              isAllowDelete: null
            },
            taskActualStartEndDate: {
              label: "Task Actual start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: false,
              isAllowDelete: null
            },
            taskDescription: {
              label: "Task description",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            toDoList: {
              label: "Todo list",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            taskEfforts: {
              label: "Task effort",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: true
            },
            taskEstimation: {
              label: "Task Estimation",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: false,
              isAllowDelete: true
            },
            repeatTask: {
              label: "Repeat Task",
              isReadonly: null,
              isAllowAdd: false,
              isAllowEdit: true,
              isAllowDelete: true
            },
            taskAssign: {
              label: "Task Assigne",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            addMeetingSchedule: {
              label: "Add Meeting schedule",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: true
            },
            taskAttachment: {
              taskattachment: {
                label: "Task attachments",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: true
              },
              label: "Task Comments"
            },
            label: "Task Name",
            cando: true,
            editTaskName: {
              label: "Task Name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editTaskPriority: {
              label: "Task priority",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            repeatTaskOccurances: {
              label: "Create public link of task",
              cando: true
            },
            editTaskStatus: {
              label: "Task Status",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            addIssue: {
              label: "Add Issue",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: null
            },
            addRisk: {
              label: "Add Risk",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: null
            },
            addComments: {
              label: "Add Comments",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: null
            }
          },
          label: "Tasks",
          description: "Description goes here",
          cando: true,
          viewalltasks: {
            label: "View all tasks",
            cando: true
          },
          importTasks: {
            label: "Import tasks",
            cando: true
          },
          exportTasks: {
            label: "Export tasks",
            cando: true
          },
          archieve: {
            label: "Archive task",
            cando: true
          },
          unarchieve: {
            label: "Unarchive task",
            cando: true
          },
          delete: {
            label: "Delete task",
            cando: true
          },
          copyInWorkSpace: {
            label: "Copy task within workspace",
            cando: true
          },
          setColor: {
            label: "Set color",
            cando: true
          },
          publicLink: {
            label: "Create public link of task",
            cando: true
          }
        },
        meeting: {
          meetingDetail: {
            meetingAgenda: {
              label: "Meeting agenda",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            meetingDiscussionNotes: {
              label: "Meeting discussion notes",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: true
            },
            meetingFollowUpActions: {
              label: "Meeting follow up actions",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            meetingKeyDecisions: {
              label: "Meeting key decisions",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            repeatMeeting: {
              label: "Repeat Meeting",
              isReadonly: null,
              isAllowAdd: false,
              isAllowEdit: false,
              isAllowDelete: false
            },
            meetingDate: {
              label: "Meeting Date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            meetingTask: {
              label: "Meeting task",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            meetingDuration: {
              label: "Meeting duration",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            meetingLocation: {
              label: "Meeting location",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            meetingAttendee: {
              label: "Meeting attendee",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            label: "Meeting Details",
            cando: true,
            canEditName: {
              label: "Meeting name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            canSubmitMeeting: {
              label: "Meeting Submit for review",
              cando: true
            },
            canPublishMeeting: {
              label: "Publish meeting",
              cando: true
            },
            canUnpublishMeeting: {
              label: "Unpublish meeting",
              cando: true
            },
            canCreateRepeatMeeting: {
              label: "Create repeat meeting occurances",
              cando: true
            },
            canEditMeetingTime: {
              label: "Meeting time",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            }
          },
          label: "Meetings",
          description: "Description goes here",
          cando: true,
          viewAllMeetings: {
            label: "View all meetings",
            cando: true
          },
          importMeetings: {
            label: "Import meeting",
            cando: true
          },
          exportMeetings: {
            label: "Export meeting",
            cando: true
          },
          archiveMeetings: {
            label: "Archive meeting",
            cando: true
          },
          unarchiveMeetings: {
            label: "Unarchive meeting",
            cando: true
          },
          deleteMeeting: {
            label: "Delete meeting",
            cando: true
          },
          cancelMeeting: {
            label: "Cancel meeting",
            cando: true
          }
        },
        issue: {
          issueDetail: {
            issueTask: {
              label: "Issue task",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            issuePlanndStartEndDate: {
              label: "Issue planned start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueActualstartEndDate: {
              label: "Issue actual start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueDescription: {
              label: "Issue description",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueType: {
              label: "Issue Type",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueAssignee: {
              label: "Issue Assignee",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            issueAttachment: {
              issueattachment: {
                label: "Issue Attachment",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: true
              },
              label: "Issue Attachments"
            },
            label: "Issue Details",
            cando: true,
            editIssueName: {
              label: "Issue Name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueStatus: {
              label: "Issue status",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issuePriority: {
              label: "Issue priority",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueSeverity: {
              label: "Issue Severity",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueCommentsUpdates: {
              label: "Issue comments/updates",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            }
          },
          label: "Issues",
          description: "Description goes here",
          cando: true,
          viewAllIssues: {
            label: "View all issues",
            cando: true
          },
          importIssues: {
            label: "Import issues",
            cando: true
          },
          exportIssues: {
            label: "Export issues",
            cando: true
          },
          archive: {
            label: "Archive issues",
            cando: true
          },
          unarchives: {
            label: "Unarchive issues",
            cando: true
          },
          delete: {
            label: "Delete issues",
            cando: true
          },
          setColor: {
            label: "set color",
            cando: true
          }
        },
        risk: {
          riskDetails: {
            riskTask: {
              label: "Risk Task",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            riskDescription: {
              label: "Risk description",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            riskMitigationPlan: {
              label: "Risk mitigation plan",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            riskOwner: {
              label: "Risk owner",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            riskAttachment: {
              riskAttachment: {
                label: "Risk attachments",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: true
              },
              label: "Risk Attachments"
            },
            label: "Risk Details",
            cando: true,
            editRiskName: {
              label: "Risk Name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editRiskStatus: {
              label: "Risk status",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editlikelihood: {
              label: "Risk likelihood",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editRiskImpact: {
              label: "Issue Impact",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            riskCommentsUpdates: {
              label: "Risk comments/updates",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            }
          },
          label: "Risks",
          description: "Description goes here",
          cando: true,
          viewAllRisks: {
            label: "View all risks",
            cando: true
          },
          importRisks: {
            label: "Import risks",
            cando: true
          },
          exportRisks: {
            label: "Export risks",
            cando: true
          },
          archive: {
            label: "Archive risks",
            cando: true
          },
          unarchives: {
            label: "Unarchive risks",
            cando: true
          },
          delete: {
            label: "Delete risks",
            cando: true
          },
          color: {
            label: "Set color",
            cando: true
          }
        },
        timesheet: {
          approveTimesheet: true,
          rejectTimesheet: true
        }
      },
      createdDate: "2020-03-16T05:59:53.030615Z",
      updatedDate: "2020-03-16T05:59:53.030615Z",
      createdBy: null,
      ownedBy: null,
      version: 0,
      updatedBy: null,
      workSpaceId: null,
      teamId: null,
      isOwner: false,
      position: 0,
      id: "000000000000000000000000"
    },
    {
      roleId: "004",
      userId: null,
      roleName: "Limited Member",
      isDefault: false,
      permission: {
        workSpace: {
          label: "Workspaces",
          description: "Description goes here",
          createWorkspaceName: {
            label: " Create Work Space",
            cando: true
          },
          workspaceSetting: {
            generalInformation: {
              workspaceImage: {
                label: "Workspace Image",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: false
              },
              label: "General Information",
              cando: true,
              isDeleteWorkSpace: {
                label: "Delete Workspace",
                cando: false
              },
              isWorkSpaceName: {
                label: "Workspace Name",
                isReadonly: null,
                isAllowAdd: null,
                isAllowEdit: true,
                isAllowDelete: null
              },
              isWorkspaceURL: {
                label: "Workspace Url",
                isReadonly: null,
                isAllowAdd: null,
                isAllowEdit: true,
                isAllowDelete: null
              }
            },
            label: "Workspace Setting",
            cando: true,
            usersManagement: {
              label: "User Management",
              cando: true,
              addMembers: {
                label: "Add Members to workspace",
                cando: true
              },
              removeMembers: {
                label: "Remove Members from workspace",
                cando: false
              },
              enableMembers: {
                label: "Enable Members in workspace",
                cando: false
              },
              disableMembers: {
                label: "Disable Members in workspace",
                cando: false
              },
              updateRole: {
                label: "Update Role in workspace",
                cando: true
              }
            },
            rolesPermissions: {
              customRoles: {
                label: "Custome Role",
                isReadonly: false,
                isAllowAdd: true,
                isAllowEdit: true,
                isAllowDelete: true
              },
              label: "Roles & Permission",
              cando: true,
              renameCustomRole: {
                label: "Rename Custom Role",
                cando: true
              }
            }
          }
        },
        project: {
          projectSetting: {
            label: "Project settings",
            cando: true,
            editResources: {
              label: "Project resource",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editCurrency: {
              label: "Currency",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editBudget: {
              label: "Project Budget",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            }
          },
          label: "Projects",
          description: "Description goes here",
          cando: true,
          viewAllProject: {
            label: "View all projects",
            cando: true
          },
          exportProject: {
            label: "Export projects",
            cando: true
          },
          importProject: {
            label: "Import projects",
            cando: true
          },
          archive: {
            label: "Archive projects",
            cando: true
          },
          unarchive: {
            label: "Unarchive projects",
            cando: true
          },
          delete: {
            label: "Delete projects",
            cando: true
          },
          copyInWorkSpace: {
            label: "Copy project within workspace",
            cando: true
          },
          accessGantt: {
            label: "Project Gantt access",
            cando: true
          },
          editProjectName: {
            label: "Project Name",
            isReadonly: null,
            isAllowAdd: null,
            isAllowEdit: true,
            isAllowDelete: null
          },
          moveProject: {
            label: "Move project",
            cando: true
          }
        },
        task: {
          taskDetail: {
            taskProject: {
              label: "Task Project",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            taskPlannedStartEndDate: {
              label: "Task Planned start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: false,
              isAllowDelete: null
            },
            taskActualStartEndDate: {
              label: "Task Actual start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: false,
              isAllowDelete: null
            },
            taskDescription: {
              label: "Task description",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            toDoList: {
              label: "Todo list",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            taskEfforts: {
              label: "Task effort",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: true
            },
            taskEstimation: {
              label: "Task Estimation",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: false,
              isAllowDelete: true
            },
            repeatTask: {
              label: "Repeat Task",
              isReadonly: null,
              isAllowAdd: false,
              isAllowEdit: true,
              isAllowDelete: true
            },
            taskAssign: {
              label: "Task Assigne",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            addMeetingSchedule: {
              label: "Add Meeting schedule",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: true
            },
            taskAttachment: {
              taskattachment: {
                label: "Task attachments",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: true
              },
              label: "Task Comments"
            },
            label: "Task Name",
            cando: true,
            editTaskName: {
              label: "Task Name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editTaskPriority: {
              label: "Task priority",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            repeatTaskOccurances: {
              label: "Create public link of task",
              cando: true
            },
            editTaskStatus: {
              label: "Task Status",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            addIssue: {
              label: "Add Issue",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: null
            },
            addRisk: {
              label: "Add Risk",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: null
            },
            addComments: {
              label: "Add Comments",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: null
            }
          },
          label: "Tasks",
          description: "Description goes here",
          cando: true,
          viewalltasks: {
            label: "View all tasks",
            cando: true
          },
          importTasks: {
            label: "Import tasks",
            cando: true
          },
          exportTasks: {
            label: "Export tasks",
            cando: true
          },
          archieve: {
            label: "Archive task",
            cando: true
          },
          unarchieve: {
            label: "Unarchive task",
            cando: true
          },
          delete: {
            label: "Delete task",
            cando: true
          },
          copyInWorkSpace: {
            label: "Copy task within workspace",
            cando: true
          },
          setColor: {
            label: "Set color",
            cando: true
          },
          publicLink: {
            label: "Create public link of task",
            cando: true
          }
        },
        meeting: {
          meetingDetail: {
            meetingAgenda: {
              label: "Meeting agenda",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            meetingDiscussionNotes: {
              label: "Meeting discussion notes",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: null,
              isAllowDelete: true
            },
            meetingFollowUpActions: {
              label: "Meeting follow up actions",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            meetingKeyDecisions: {
              label: "Meeting key decisions",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            repeatMeeting: {
              label: "Repeat Meeting",
              isReadonly: null,
              isAllowAdd: false,
              isAllowEdit: false,
              isAllowDelete: false
            },
            meetingDate: {
              label: "Meeting Date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            meetingTask: {
              label: "Meeting task",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            meetingDuration: {
              label: "Meeting duration",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            meetingLocation: {
              label: "Meeting location",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            meetingAttendee: {
              label: "Meeting attendee",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            label: "Meeting Details",
            cando: true,
            canEditName: {
              label: "Meeting name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            canSubmitMeeting: {
              label: "Meeting Submit for review",
              cando: true
            },
            canPublishMeeting: {
              label: "Publish meeting",
              cando: true
            },
            canUnpublishMeeting: {
              label: "Unpublish meeting",
              cando: true
            },
            canCreateRepeatMeeting: {
              label: "Create repeat meeting occurances",
              cando: true
            },
            canEditMeetingTime: {
              label: "Meeting time",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            }
          },
          label: "Meetings",
          description: "Description goes here",
          cando: true,
          viewAllMeetings: {
            label: "View all meetings",
            cando: true
          },
          importMeetings: {
            label: "Import meeting",
            cando: true
          },
          exportMeetings: {
            label: "Export meeting",
            cando: true
          },
          archiveMeetings: {
            label: "Archive meeting",
            cando: true
          },
          unarchiveMeetings: {
            label: "Unarchive meeting",
            cando: true
          },
          deleteMeeting: {
            label: "Delete meeting",
            cando: true
          },
          cancelMeeting: {
            label: "Cancel meeting",
            cando: true
          }
        },
        issue: {
          issueDetail: {
            issueTask: {
              label: "Issue task",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            issuePlanndStartEndDate: {
              label: "Issue planned start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueActualstartEndDate: {
              label: "Issue actual start/end date",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueDescription: {
              label: "Issue description",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueType: {
              label: "Issue Type",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueAssignee: {
              label: "Issue Assignee",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            issueAttachment: {
              issueattachment: {
                label: "Issue Attachment",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: true
              },
              label: "Issue Attachments"
            },
            label: "Issue Details",
            cando: true,
            editIssueName: {
              label: "Issue Name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueStatus: {
              label: "Issue status",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issuePriority: {
              label: "Issue priority",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueSeverity: {
              label: "Issue Severity",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            issueCommentsUpdates: {
              label: "Issue comments/updates",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            }
          },
          label: "Issues",
          description: "Description goes here",
          cando: true,
          viewAllIssues: {
            label: "View all issues",
            cando: true
          },
          importIssues: {
            label: "Import issues",
            cando: true
          },
          exportIssues: {
            label: "Export issues",
            cando: true
          },
          archive: {
            label: "Archive issues",
            cando: true
          },
          unarchives: {
            label: "Unarchive issues",
            cando: true
          },
          delete: {
            label: "Delete issues",
            cando: true
          },
          setColor: {
            label: "set color",
            cando: true
          }
        },
        risk: {
          riskDetails: {
            riskTask: {
              label: "Risk Task",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            riskDescription: {
              label: "Risk description",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            riskMitigationPlan: {
              label: "Risk mitigation plan",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: null
            },
            riskOwner: {
              label: "Risk owner",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            },
            riskAttachment: {
              riskAttachment: {
                label: "Risk attachments",
                isReadonly: null,
                isAllowAdd: true,
                isAllowEdit: null,
                isAllowDelete: true
              },
              label: "Risk Attachments"
            },
            label: "Risk Details",
            cando: true,
            editRiskName: {
              label: "Risk Name",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editRiskStatus: {
              label: "Risk status",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editlikelihood: {
              label: "Risk likelihood",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            editRiskImpact: {
              label: "Issue Impact",
              isReadonly: null,
              isAllowAdd: null,
              isAllowEdit: true,
              isAllowDelete: null
            },
            riskCommentsUpdates: {
              label: "Risk comments/updates",
              isReadonly: null,
              isAllowAdd: true,
              isAllowEdit: true,
              isAllowDelete: true
            }
          },
          label: "Risks",
          description: "Description goes here",
          cando: true,
          viewAllRisks: {
            label: "View all risks",
            cando: true
          },
          importRisks: {
            label: "Import risks",
            cando: true
          },
          exportRisks: {
            label: "Export risks",
            cando: true
          },
          archive: {
            label: "Archive risks",
            cando: true
          },
          unarchives: {
            label: "Unarchive risks",
            cando: true
          },
          delete: {
            label: "Delete risks",
            cando: true
          },
          color: {
            label: "Set color",
            cando: true
          }
        },
        timesheet: {
          approveTimesheet: true,
          rejectTimesheet: true
        }
      },
      createdDate: "2020-03-16T05:59:53.030615Z",
      updatedDate: "2020-03-16T05:59:53.030615Z",
      createdBy: null,
      ownedBy: null,
      version: 0,
      updatedBy: null,
      workSpaceId: null,
      teamId: null,
      isOwner: false,
      position: 0,
      id: "000000000000000000000000"
    }
  ]
};
export default data;
