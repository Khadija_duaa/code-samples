import validator from 'validator';

import getErrorMessages from './constants/errorMessages'

// const emailReg = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const emailReg = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const userNameReg = /^(?=.{3,50}$)([a-zA-Z0-9]+(?:[_.-]?[a-zA-Z0-9]))*$/;
const UserReq = /^([A-Z0-9.\-_]+)$/i;
const firstnameLastnameReq = /^([A-Z0-9-.]+)$/i;
const passwordReg = /^.{8,}$/
const EMAIL_REG = /\w+\@{1}\w+\.{1}\w+/i;
const PHONE_REG = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/i;
const WORKSPACE_URL = /[^a-z0-9-]+/i;

export const validateEmail = value => {
  if (validator.isEmail(value)) {
    return true
  } else {
    return false
  };
}
export const validateUserName = value => {
  if (userNameReg.test(value)) {
    return true
  } else {
    return false
  };
}
export const validatePassword = value => {
  if (passwordReg.test(value)) {
    return true
  } else {
    return false
  };
}

export const validateTitleField = (fieldName, value, isRequired = true, length, intl) => {
  if (length === undefined)
    length = 80;

  if (typeof value === "string")
    value = value.trim();

  if (isRequired && !value) {
    return {
      isError: true,
      message: intl != undefined && fieldName == "milestone title" ? intl.formatMessage({id:"project.gant-view.milestone-dialog.form.validation.milestone-title.empty",defaultMessage:getErrorMessages(fieldName).EMPTY_FIELD})  : getErrorMessages(fieldName).EMPTY_FIELD
    }
  }
  else if (value.toString().trim().length > length) {
    return {
      isError: true,
      message: getErrorMessages(length).MAX_LENGTH
    }
  }
  else {
    return {
      isError: false
    }
  }
}

export const validatePhoneField = (fieldName, value, isRequired = true) => {
  if (isRequired && (!value || value === '')) {
    return {
      isError: true,
      message: getErrorMessages(fieldName).EMPTY_FIELD
    }
  }
  else if (value && value !== '' && !validator.isMobilePhone(value)) {
    return {
      isError: true,
      message: getErrorMessages(fieldName).INVALID_PHONE
    }
  }
  else {
    return {
      isError: false
    }
  }
}

export const validateUrlField = (fieldName, value) => {
  if (!value || value === '') {
    return {
      isError: true,
      message: getErrorMessages(fieldName).EMPTY_FIELD
    }
  }
  else if (WORKSPACE_URL.test(value)) {
    return {
      isError: true,
      message: getErrorMessages(fieldName).CHARACTERS_NAME_FIELD
    }
  }
  else {
    return {
      isError: false
    }
  }
}

export const validateMultiWordNameField = (fieldName, value, isRequired = true) => {
  if (isRequired && (!value || value === '' || !value.replace(/\s/g, '').length)) {
    return {
      isError: true,
      message: getErrorMessages(fieldName).EMPTY_FIELD
    }
  }
  else if (value) {
    const words = value.trim().replace(/( +)/gi, " ").split(' ');
    let index;
    for (index = 0; index < words.length; ++index) {
      if ((!(firstnameLastnameReq.test(words[index]))))
        return {
          isError: true,
          message: getErrorMessages(fieldName).CHARACTERS_NAME_FIELD
        }
    }
  }
  else if (value.toString().trim().length > 40) {
    return {
      isError: true,
      message: getErrorMessages(40).MAX_LENGTH
    }
  }
  return {
    isError: false
  }
}

export const validateNameField = (fieldName, value, isRequired = true) => {
  if (isRequired && (!value || value === '')) {
    return {
      isError: true,
      message: getErrorMessages(fieldName).EMPTY_FIELD
    }
  }
  else if (value && value !== '' && !validator.isAlphanumeric(value)) {
    return {
      isError: true,
      message: getErrorMessages(fieldName).CHARACTERS_NAME_FIELD
    }
  }
  else if (value.toString().trim().length > 40) {
    return {
      isError: true,
      message: getErrorMessages(40).MAX_LENGTH
    }
  }
  else {
    return {
      isError: false
    }
  }
}

export const validateEmailField = (value, isRequired = true) => {
  value = (value) ? value : ''
  if (isRequired && (!value || value === '')) {
    return {
      isError: true,
      message: getErrorMessages('email').EMPTY_FIELD
    }
  }
  else if (value && value !== '' && !validator.isEmail(value)) {
    return {
      isError: true,
      message: getErrorMessages().INVALID_EMAIL
    }
  }
  else if (value.toString().trim().length > 50) {
    return {
      isError: true,
      message: getErrorMessages(50).MAX_LENGTH
    }
  }
  // else if (emailReg.test(value)) {
  //   return {
  //     isError: true,
  //     message: getErrorMessages().INVALID_EMAIL
  //   }
  // }
  else {
    return {
      isError: false
    }
  }
}

export const validatePasswordField = (value, fieldName = 'password', intl = null) => {
  // Replace all spaces with empty string
  var id = fieldName.replace(" ","-");
  if (!value || value === '' || !value.replace(/\s/g, '').length) {
    return {
      isError: true,
      message: intl!=null ? intl.formatMessage({id: "profile-settings-dialog.signin-security.change-password.form.validation."+id+".empty" , defaultMessage: getErrorMessages(fieldName).EMPTY_FIELD }) : getErrorMessages(fieldName).EMPTY_FIELD
    }
  }
  else if (value.toString().length < 8) {
    let t1 = 8 ;
    return {
      isError: true,
      message: intl!=null ? intl.formatMessage({id: "profile-settings-dialog.signin-security.change-password.form.validation.length.min" , defaultMessage: getErrorMessages(8).MIN_LENGTH},{t1}) : getErrorMessages(8).MIN_LENGTH 
    }
  }
  else if (value.toString().length > 40) {
    let t1 = 40;
    return {
      isError: true,
      message: intl!=null ? intl.formatMessage({id: "profile-settings-dialog.signin-security.change-password.form.validation.length.max", defaultMessage: getErrorMessages(40).MAX_LENGTH},{t1}) : getErrorMessages(40).MAX_LENGTH
    }
  }
  else {
    return {
      isError: false
    }
  }
}

export const validateGenericField = (fieldName, value, isRequired = true, length) => {
  if (isRequired && (!value || value === '')) {
    return {
      isError: true,
      message: getErrorMessages(fieldName).EMPTY_FIELD
    }
  }
  else if (length && value.toString().trim().length > length) {
    return {
      isError: true,
      message: getErrorMessages(length).MAX_LENGTH
    }
  }
  else {
    return {
      isError: false
    }
  }
}

export const validateUserNameAndEmailField = (fieldName, value) => {
  if (!value || value === '') {
    return {
      isError: true,
      message: getErrorMessages(fieldName).EMPTY_FIELD
    }
  }
  else if (value.toString().trim().length < 3) {
    return {
      isError: true,
      message: getErrorMessages(3).MIN_LENGTH
    }
  }
  else {
    return {
      isError: false
    }
  }
}

export const validateUserNameField = (fieldName, value, isRequired = true) => {
  if (isRequired && (!value || value === '')) {
    return {
      isError: true,
      message: getErrorMessages(fieldName).EMPTY_FIELD
    }
  }
  else if (!(UserReq.test(value))) {
    return {
      isError: true,
      message: getErrorMessages(fieldName).USER_NAME
    }
  }
  else if (value.toString().trim().length > 50) {
    return {
      isError: true,
      message: getErrorMessages(50).MAX_LENGTH
    }
  }
  else {
    return {
      isError: false
    }
  }
}

export const validateMultifieldEmails = (mailList) => {
  const singleEmailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g;
  // const multipleEmailReg = /^([\s?\,?]?[\,?\s?]?[^\s,]+@[^\s,]+\.[^\s,]+\s?,)*(\s?[^\s,]+@[^\s,]+\.[^\s,]+[\s?\,?]?[\,?\s?]?)+$/g;
  const multipleEmailReg = /^\S+(\s?\,\s?\S+)*$/g;
  mailList = mailList.trim();
  if (!mailList)
    return {
      isError: true,
      message: ''
    }

  let userEmails = mailList.split(/[\s,]+/) || [];
  for (let i = 0; i < userEmails.length; i++) {
    // if item is empty throw it to next check of comma format error
    if (!userEmails[i])
      break;
    let regEx = new RegExp(singleEmailReg);
    if (userEmails[i].length > 50) {
      return {
        isError: true,
        message: getErrorMessages(50).MAX_LENGTH
      }
    }
    else if (!regEx.test(userEmails[i])) {
      return {
        isError: true,
        message: getErrorMessages().INVALID_EMAIL
      }
    }
  }

  let regEx = new RegExp(multipleEmailReg);
  if (!regEx.test(mailList)) {
    return {
      isError: true,
      message: `Please separate email addresses by comma.`
    }
  }
  return {
    isError: false,
    message: ``,
    // data: mailList.replace(/\s*/gi, '').split(',') || []
    data: userEmails || []
  }
}

export const filterUserNameInputValue = (inputValue) => {
  //// Allowing to input only (alphaNumeric,-,_)
  return inputValue.replace(' ', '').replace(/[^a-z0-9_.-]+/gi, '');
}

export const filterNameInputValue = (inputValue) => {
  //// Allowing to input only (alphaNumeric,-,_)
  return inputValue.replace(/  +/g, ' ').replace(/[^a-z0-9-. ]+/gi, '');
}

export const filterMutlipleEmailsInput = (inputValue) => {
  //// Allowing to input only (alphaNumeric,_,@)
  return inputValue.replace(/  +/g, ' ').replace(/[^-a-z0-9.@_ ,]+/gi, '');
}