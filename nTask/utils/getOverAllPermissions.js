
import cloneDeep from "lodash/cloneDeep";
const getOverAllPermissions = (key, value) => {
  const object = {
    workSpace: {
      workspaceName: "Workspace name",
      workspaceImage: "Workspace image",
      inviteMembers: "Invite member to workspace",
      reInviteMembers: "Resend invite member to workspace",
      removeMembers: "Remove member from workspace",
      enableMember: "Enable member in workspace",
      disableMember: "Disable member in workspace",
      updateRole: "Update member role in workspace",
      delete: "Delete workspace",
      accessManageMembers: "Manage members",
      accessSetting: "Workspace settings",
      accessRoleManagement: "Workspace role management"
    },

    project: {
      viewAllProjects: "View all projects",
      exportProject: "Export projects",
      importProject: "Import projects",
      projectName: "Project name",
      resources: "Project resource",
      currency: "Currency",
      budget: "Project budget",
      archive: "Archive project",
      unarchive: "Unarchive project",
      delete: "Delete project",
      copyInWorkSpace: "Copy project within Workspace",
      copyOutWorkSpace: "Copy project outside Workspace",
      publicLink: "Public link",
      accessSettings: "Project settings",
      accessGantt: "Project Gantt"
    },

    task: {
      viewAllTasks: "View all tasks",
      viewAssignedTasks: "View assigned tasks to user",
      viewAssignedTasksToOthers: "View assigned tasks to others",
      viewOwnedTasksByUser: "View owned tasks by the user",
      viewUnassignedTasks: "View unassigned tasks",
      exportTasks: "Export tasks",
      importTasks: "Import tasks",
      taskTitle: "Task title",
      taskDescription: "Task description",
      taskProject: "Task project",
      startDate: "Start date",
      dueDate: "Due date",
      checkListItem: "Checklist item",
      status: "Status",
      priority: "Priority",
      assignee: "Assignee",
      taskEffort: "Task effort",
      taskEstimation: "Task estimation",
      meeting: "Meeting Schedule",
      repeatTask: "Repeat task Settings",
      comment: "Comments",
      attachment: "Task attachments",
      archieve: "Archive task",
      unarchieve: "Unarchive task",
      delete: "Delete task",
      copyInWorkSpace: "Copy task within Workspace",
      copyOutWorkSpace: "Copy task to another Workspace",
      setColor: "Set color",
      publicLink: "Public link"
    },

    issue: {
      viewAllIssues: "View all issues",
      viewAssignedIssues: "View assigned issues to user",
      viewAssignedIssuesToOthers: "View assigned issues to others",
      viewOwnedIssuesByUser: "View owned issues by the user",
      viewUnassignedIssues: "View unassigned issues",
      exportIssues: "Export issues",
      importIssues: "Import issues",
      issueTitle: "Issue title",
      issueDescription: "Issue description",
      issueTask: "Issue task",
      startDate: "Start date",
      endDate: "End date",
      status: "Status",
      priority: "Priority",
      severity: "Severity",
      assignee: "Assignee",
      issueType: "Issue type",
      issueUpdates: "Comment/Updates",
      attachments: "Attachments",
      archive: "Archive issue",
      unarchives: "Unarchive issue",
      delete: "Delete issue",
      copyInWorkspace: "Copy issue within Workspace",
      copyOutSideWorkspace: "Copy issue outside Workspace",
      color: "Set color"
    },

    risk: {
      viewAllRisks: "View all risks",
      viewAssignedRisks: "View assigned risks to user",
      viewAssignedRisksToOthers: "View assigned risks to others",
      viewOwnedRisksByUser: "View owned risks by the user",
      viewUnassignedRisks: "View unassigned risks",
      exportRisks: "Export risks",
      importRisks: "Import risks",
      riskTitle: "Risk title",
      riskDescription: "Risk description",
      mitigationPlan: "Risk mitigation plan",
      riskTask: "Risk task",
      status: "Status",
      likelihood: "Likelihood",
      impact: "Impact",
      riskOwner: "Risk owner",
      riskUpdates: "Updates/ Comments",
      attachments: "Attachments",
      archive: "Archive risk",
      unarchives: "Unarchive risk",
      delete: "Delete risk",
      copyInWorkspace: "Copy risk within Workspace",
      copyOutSideWorkspace: "Copy risk outside Workspace",
      color: "Set color"
    },

    meeting: {
      viewAllMeetings: "View all meetings",
      viewInvitedMeetingsToUser: "View invited meetings to user",
      viewInvitedMeetingsToOthers: "View invited meetings to others",
      viewOwnedMeetingsByUser: "View owned meetings by the user",
      exportMeetings: "Export meetings",
      importMeetings: "Import meetings",
      meetingTitle: "Meeting title",
      meetingLocation: "Meeting location",
      meetingDate: "Meeting date",
      meetingTime: "Meeting time",
      meetingDuration: "Meeting duration",
      meetingAttendee: "Meeting attendee",
      meetingAgenda: "Agenda",
      meetingDecision: "Discussion points",
      meetingFollowupAction: "Followup actions",
      meetingDiscussion: "Decistion points",
      cancelMeeting: "Cancel meeting",
      archiveMeetings: "Archive meeting",
      unarchiveMeetings: "Unarchive meeting",
      deleteMeeting: "Delete meeting",
      updateSchdule: "Update Schedule",
      submitMeeting: "Submit to review",
      publishMeetings: "Publish",
      unpublishMeetings: "Unpublish"
    }
  };
  return object[key][value];
};

const getHandledPermissions = (permission, key) => {
  const workspaceArray = [];
  if (permission) {
    const workSpacePermissions = permission[key];
    for (let i = 0; i < Object.keys(workSpacePermissions).length; i++) {
      workspaceArray.push({
        name: getOverAllPermissions(key, Object.keys(workSpacePermissions)[i]),
        object: workSpacePermissions[Object.keys(workSpacePermissions)[i]],
        key: Object.keys(workSpacePermissions)[i],
        parent: key
      });
    }
  }
  const canDoData = workspaceArray.filter(x => {
    if (x.object) return Object.keys(x.object).length === 0;
    else return !x.object;
  });
  const canViewData = workspaceArray.filter(x => {
    if (x.object) return Object.keys(x.object).length === 1;
  });
  const canCrudData = workspaceArray.filter(x => {
    if (x.object) return Object.keys(x.object).length > 1;
  });

  return {
    canDoData,
    canViewData,
    canCrudData
  };
};
const updatePermissions = (permission, type, object, value) => {

  if (permission && type && object) {
    //const currentPermission=permission[object.parent][object.key];
    if (type === "canDo") {
      permission[object.parent][object.key] = value;
    }
    if (type === "view") {
      permission[object.parent][object.key] = { view: value };
    }
    if (type === "add") {
      const data = cloneDeep(object.object);
      data["add"] = value;
      permission[object.parent][object.key] = data;
    }
    if (type === "edit") {
      const data = cloneDeep(object.object);
      data["edit"] = value;
      permission[object.parent][object.key] = data;
    }
    if (type === "delete") {
      const data = cloneDeep(object.object);
      data["delete"] = value;
      permission[object.parent][object.key] = data;
    }
  }
  return permission;
};

export { getOverAllPermissions, getHandledPermissions, updatePermissions };
