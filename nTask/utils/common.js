import axios from "axios";
import isEmpty from "lodash/isEmpty";
import findIndex from "lodash/findIndex";
import omit from "lodash/omit";
import assign from "lodash/assign";
import { UpdateUserState } from "../redux/actions/onboarding";
import headerHeight from "../helper/getHeaderHeight";
import footerHeight from "../helper/getFooterHeight";
import headingBarHeight from "../helper/getHeadingBarHeight";
import { store } from "../index";
import promoBar from "../redux/reducers/promoBar";
import templateTypes from "./constants/templatesTypes";

export const apiInstance = () => {
  return axios.create();
};

export const generateUsername = (
  email,
  username,
  firstname,
  lastname,
  returnEmailIfUndefined = true
) => {
  if (firstname && lastname) {
    return `${firstname} ${lastname}`;
  } else if (firstname) {
    return firstname;
  } else if (lastname) {
    return lastname;
  } else if (username) {
    return `${username}`;
  } else if (returnEmailIfUndefined) {
    return `${email}`;
  } else {
    return "";
  }
};
export const isMobileDevice = () => {
  if (sessionStorage.desktop) // desktop storage 
        return false;
    else if (localStorage.mobile) // mobile storage
        return true;

    // alternative
    let mobile = ['iphone','ipod','ipad','android','blackberry','nokia','opera mini','windows mobile','windows phone','iemobile','tablet','mobi']; 
    let ua=navigator.userAgent.toLowerCase();
    for (let i in mobile) if (ua.indexOf(mobile[i]) > -1) return true;

    // nothing found.. assume desktop
    return false;
}

export const getModifiedState = (changesObj, fieldname, prevValue, changedValue) => {
  if (!prevValue) prevValue = "";
  if (prevValue === changedValue) {
    const obj = omit(changesObj, fieldname);
    if (isEmpty(obj)) return { profileModified: false, fieldsChanged: obj };
    else return { fieldsChanged: obj };
  } else {
    const obj = assign(changesObj, { [fieldname]: changedValue });
    return { profileModified: true, fieldsChanged: obj };
  }
};

export function calculateContentHeight(ignoreFooter = false) {
  const newSidebarHeight = localStorage.getItem("oldSidebarView") == "true" ? 0 : 42;
  let state = store.getState();
  let promoBar = state.promoBar.promoBar;
  let notificationBar = state.notificationBar.notificationBar;
  let promoBarHeight = promoBar || notificationBar ? 50 : 0;
  let totalFooterHeight = ignoreFooter ? 0 : footerHeight
  let height =
    window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

  return height - headerHeight - totalFooterHeight - headingBarHeight - promoBarHeight
}
export function calculateSidebarHeight() {
  let state = store.getState();
  let promoBar = state.promoBar.promoBar;
  let notificationBar = state.notificationBar.notificationBar;
  let promoBarHeight = promoBar || notificationBar ? 50 : 0;
  let height =
    window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

  return height - promoBarHeight;
}
export function calculateAdvancedFilterHeight() {
  let state = store.getState();
  let promoBar = state.promoBar.promoBar;
  let notificationBar = state.notificationBar.notificationBar;
  let promoBarHeight = promoBar || notificationBar ? 50 : 0;
  let height =
    window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

  return height - promoBarHeight;
}
export function calculateHeaderHeight() {
  let state = store.getState();
  let promoBar = state.promoBar.promoBar;
  let notificationBar = state.notificationBar.notificationBar;
  let promoBarHeight = promoBar || notificationBar ? 50 : 0;

  return headerHeight + promoBarHeight;
}
export function calculateFilterContentHeight() {
  let state = store.getState();
  let promoBar = state.promoBar.promoBar;
  let notificationBar = state.notificationBar.notificationBar;
  let promoBarHeight = promoBar || notificationBar ? 50 : 0;
  let height =
    window.outerHeight || document.documentElement.clientHeight || document.body.clientHeight;

  return height - headerHeight - footerHeight - headingBarHeight - promoBarHeight - 148;
}
export function calculateHeight() {
  let height =
    window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

  return height;
}

export function isScrollBottom(e) {
  const bottom = Math.floor(e.target.clientHeight + e.target.scrollTop) >= e.target.scrollHeight;
  return bottom;
}

export function getTaskName(id, tasks) {
  const task = tasks.find(t => {
    return t.id == id;
  });
  if (task) {
    return task.taskTitle;
  }
}

export function getUserDetail(id, users, key) {
  const user = users.find(u => {
    return u.userId == id;
  });
  if (user) {
    return user[key];
  } else {
    return false;
  }
}

export function getUser(id, users, key) {
  const user = users.find(u => {
    return u.userId == id;
  });
  return user;
}

export const isArrayEqual = (arr1 = [], arr2 = [], comparisonKey) => {
  if (arr1.length === arr2.length) {
    for (let i = 0; i < arr1.length; i++) {
      const itemIndex = findIndex(arr2, function(o) {
        return o[comparisonKey] == arr1[i][comparisonKey];
      });
      if (itemIndex === -1) return false;
    }
    return true;
  } else return false;
};

export const isImageFileSizeValid = fileSize => {
  return parseFloat(fileSize / 1024 / 1024) < parseFloat(1.51);
};

export const setCaretPosition = (elemRef, caretPos) => {
  if (elemRef != null) {
    if (elemRef.createTextRange) {
      var range = elemRef.createTextRange();
      range.move("character", caretPos);
      range.select();
    } else {
      elemRef.value = elemRef.value;
      if (elemRef.selectionStart) {
        elemRef.focus();
        elemRef.setSelectionRange(caretPos, caretPos);
      } else elemRef.focus();
    }
  }
};

export const maskInputValue = (newValue, oldValue, cursorPosition) => {
  // 456:123
  const validTimeEntry = new RegExp("^\\d{1,3}:\\d{1,3}$");
  // 24:00
  const oneDayDurationInput = new RegExp("^([0-2][0-9]:[0-5]\\d)|(24:00)$");

  function setIndexValue(insertionString, index, endIndex) {
    return newValue.substr(0, index) + insertionString + newValue.substr(endIndex);
  }

  if (!newValue) return { value: "00:00", cursorPosition: 0 };
  else if (!validTimeEntry.test(newValue))
    return { value: oldValue, cursorPosition: cursorPosition - 1 };

  let targetValue;
  // case when new value is inserted in input, than length will be greater than 5
  if (newValue.length > 5) {
    targetValue = newValue;
    switch (cursorPosition) {
      case 1:
        if (/[0-2]/.test(targetValue[0])) targetValue = setIndexValue(targetValue[0], 0, 2);
        else {
          cursorPosition--;
          targetValue = oldValue;
        }
        break;
      case 2:
        targetValue = setIndexValue(
          targetValue[0] !== "2"
            ? /[0-9]/.exec(targetValue[1]) || 0
            : /[0-4]/.exec(targetValue[1]) || 0,
          1,
          3
        );
        // skipping cursor for colon  value
        cursorPosition = 3;
        break;
      case 3:
        if (/:/.test(targetValue[2]))
          targetValue = setIndexValue(/:/.exec(targetValue[2]) || ":", 2, 3);
        else {
          targetValue = oldValue;
        }
        break;
      case 4:
        if (/[0-5]/.test(targetValue[3]))
          targetValue = setIndexValue(/[0-5]/.exec(targetValue[3]) || 0, 3, 5);
        else {
          cursorPosition--;
          targetValue = oldValue;
        }
        break;
      case 5:
        if (/[0-9]/.test(targetValue[4]))
          targetValue = setIndexValue(/[0-9]/.exec(targetValue[4]) || 0, 4, 6);
        else {
          cursorPosition--;
          targetValue = oldValue;
        }
        break;
      default:
        targetValue = setIndexValue("", 5, 7);
    }
  } else {
    // case when something is removed from value
    targetValue = newValue;
    const isColonExistsInValue = /:/g.exec(targetValue);
    if (!isColonExistsInValue && targetValue.length === 4)
      targetValue = setIndexValue(":", cursorPosition, cursorPosition);
    else {
      targetValue = setIndexValue("0", cursorPosition, cursorPosition);
      if (cursorPosition === 3) cursorPosition = 2;
    }
  }

  if (!oneDayDurationInput.test(targetValue)) {
    return { value: oldValue, cursorPosition: cursorPosition - 1 };
  }

  return { value: targetValue, cursorPosition };
};

export const meridiemMaskInput = (newValue, oldValue, cursorPosition) => {
  // 456:123
  const validTimeEntry = new RegExp("^\\d{1,3}:\\d{1,3}$");
  // 12:00
  const meridiemInput = new RegExp("^(([0][\\d]|[1][0-1]):[0-5][\\d])|(12:00)$");

  function setIndexValue(insertionString, index, endIndex) {
    return newValue.substr(0, index) + insertionString + newValue.substr(endIndex);
  }

  if (!newValue) return { value: "00:00", cursorPosition: 0 };
  else if (!validTimeEntry.test(newValue))
    return { value: oldValue, cursorPosition: cursorPosition - 1 };

  let targetValue;
  // case when new value is inserted in input, than length will be greater than 5
  if (newValue.length > 5) {
    targetValue = newValue;
    switch (cursorPosition) {
      case 1:
        if (/[0-1]/.test(targetValue[0])) targetValue = setIndexValue(targetValue[0], 0, 2);
        else {
          cursorPosition--;
          targetValue = oldValue;
        }
        break;
      case 2:
        targetValue = setIndexValue(/[0-9]/.test(targetValue[1]) ? targetValue[1] : 0, 1, 3);
        // skipping cursor for colon  value
        cursorPosition = 3;
        break;
      case 3:
        if (/:/.test(targetValue[2]))
          targetValue = setIndexValue(/:/.exec(targetValue[2]) || ":", 2, 4);
        else {
          targetValue = oldValue;
        }
        break;
      case 4:
        if (/[0-5]/.test(targetValue[3]))
          targetValue = setIndexValue(/[0-5]/.exec(targetValue[3]) || 0, 3, 5);
        else {
          cursorPosition--;
          targetValue = oldValue;
        }
        break;
      case 5:
        if (/[0-9]/.test(targetValue[4]))
          targetValue = setIndexValue(/[0-9]/.exec(targetValue[4]) || 0, 4, 6);
        else {
          cursorPosition--;
          targetValue = oldValue;
        }
        break;
      default:
        targetValue = setIndexValue("", 5, 7);
    }
  } else {
    // case when something is removed from value
    targetValue = newValue;
    const isColonExistsInValue = /:/g.exec(targetValue);
    if (!isColonExistsInValue && targetValue.length === 4)
      targetValue = setIndexValue(":", cursorPosition, cursorPosition);
    else {
      targetValue = setIndexValue("0", cursorPosition, cursorPosition);
      if (cursorPosition === 3) cursorPosition = 2;
    }
  }

  if (!meridiemInput.test(targetValue))
    return { value: oldValue, cursorPosition: cursorPosition - 1 };

  return { value: targetValue, cursorPosition };
};

export const arrayRemoveCharacter = (value, position) => {
  return value.slice(0, position) + value.slice(position + 1, value.length);
};

export const getTemplateType = (selectedItem) => {
  // let arr = ['WorkspaceTemplate :1', 'SavedTemplate : 2', 'ProjectSpecificTemplate : 3'];
  if(selectedItem.category == 1){
    return templateTypes.NTASKDEFAULT;
  }else if(selectedItem.category == 2 ){
    return templateTypes.WORKSPACE_SAVED_TEMPLATE;
  }else if(selectedItem.category == 3){
    return templateTypes.PROJECT_CUSTOM_TEMPLATE;
  }
};



export const useTemplatePermission = () => {
  return {
    boardDetail: {
      label: "Board Details",
      cando: true,
      addNewStatus: {
        label: "Add New Status/List",
        cando: false,
      },
      changeStatusColor: {
        label: "Change Status Color",
        cando: true,
      },
      listsetting: {
        label: "Access List setting",
        cando: true,
      },
      addNewTask: {
        label: "Can do",
        cando: false,
      },
      viewTaskdetail: {
        label: "View Task Detail",
        cando: true,
      },
      editTask: {
        label: "Edit Task",
        cando: true,
      },
      accessProjectDropdown: {
        label: "Access Project List",
        cando: true,
      },
      subTaskToggle: {
        label: "Subtask Toggle",
        cando: true,
      },
      showFilters: {
        label: "Show Filter",
        cando: true,
      },
      activityLog: {
        label: "Activity Log",
        cando: true,
      },
      importExport: {
        label: "Import/Export",
        cando: true,
      },
      listreorder: {
        label: "List re-order",
        cando: true,
      },
      addUnsortedTask: {
        label: "Unsorted add task to other list",
        cando: true,
      },
      taskreorderInmultiList: {
        label: "Task re-order within multiple list",
        cando: true,
      },
    },
    label: "Boards",
    description: "Input permissions for build kanban boards with projects and tasks.",
    cando: true,
    creatBoard: {
      label: "Create New Board",
      cando: true,
    },
    backgroundBoardColor: {
      label: "Background color of board",
      cando: true,
    },
    saveBoardTemplate: {
      label: "Save as Template",
      cando: true,
    },
    deleteBoard: {
      label: "Delete Board",
      cando: true,
    },
  };
};
