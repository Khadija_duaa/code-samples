import React, { Component, Fragment } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import CustomIconButton from "../components/Buttons/IconButton";
import SelectionMenu from "../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";

class PaymentActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: ""
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose(event) {
    this.setState({ open: false });
  }

  handleClick(event, placement) {
    event.stopPropagation();
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }
  handleToggle(key, id) {
    this.props.changePaymentHandler(key, id)
    this.setState({ open: false });
  }

  render() {
    const { classes, theme, ddData, id, changePaymentHandler } = this.props;
    const { open, placement } = this.state;
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomIconButton
            btnType="condensed"
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
          >
            <MoreVerticalIcon
              htmlColor={theme.palette.secondary.medDark}
              style={{ fontSize: "24px" }}
            />
          </CustomIconButton>
          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            anchorRef={this.anchorEl}
            list={
              <List
                onClick={event => {
                  event.stopPropagation();
                }}
              >
                {Object.keys(ddData).map(key =>
                  <ListItem
                    key={key}
                    button
                    disableRipple={true}
                    onClick={event => this.handleToggle(key, id)}
                  >
                    <ListItemText
                      primary={ddData[key]}
                      classes={{
                        primary: classes.paymentMenuItem
                      }}
                    />

                  </ListItem>

                )}
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}
const mapStateToProps = state => {
  return {
  };
};
export default compose(
  connect(
    mapStateToProps,
    {}
  )
)(PaymentActionDropdown);
