import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../components/Buttons/CustomButton";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import {CardNumberElement, CardExpiryElement, CardCVCElement, injectStripe} from 'react-stripe-elements';
import IconButton from "../components/Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import ButtonActionsCnt from "../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Divider from "@material-ui/core/Divider";
import DefaultSwitch from "../components/Form/Switch";
import PaymentActionDropDown from "./PaymentActionDropDown";
import { FetchPaymentMethod, AddCustomerPaymentMethod, RemoveCustomerPaymentMethod, SetCustomerCardDefault } from "../redux/actions/userBillPlan";
import Constants from "../components/constants/planConstant";
import billingIcon from "../assets/images/billing_icon.png"
import Visa from "../assets/images/visacard.png";
import Master from "../assets/images/mastercard.png";
import upgradePro from "../assets/images/upgrade_pro.png";
import {FormattedMessage} from "react-intl";
class Payment extends Component {
    constructor(){
        super();
        this.state = {
            addCard: false, 
            primaryCard: false,
            errorMessage: '',
            cards: [],
            defaultSource: '',
            loading: true,
            btnQueryAdd: '',
            btnQueryRemove: '',
            showRemoveConfirmation: false,
            removedIndex: -1
        }
    }
    componentDidMount() {
        this.props.FetchPaymentMethod(result => {
            if(result.data){
                this.setState({
                    cards: result.data.sources.data, 
                    default_source: result.data.default_source,
                    loading: false, 
                })
            }
            else {
                // this.props.showSnackBar('No Payment Method');
                    this.setState({
                    cards: [], 
                    default_source: '',
                    loading: false, 
                })
            }
        },
        fail => {
            this.setState({
                cards: [], 
                default_source: '',
                loading: false, 
            })
            this.props.showSnackBar(fail.data);
        });
    }
    // errorMessage = () => {
    //     if (error) {
    //         this.setState({errorMessage: error.message});
    //       }
    // }
    addPaymentMethod = () => {
        const { paymentPlan} = this.props;
        const {expiryDate} = paymentPlan;
        const { primaryCard} = this.state;
        if (this.props.stripe) {
            this.setState({ btnQueryAdd: "progress" }, () => {
                this.props.stripe.createToken()
                .then(result => {
                if (result.error) {
                    this.setState({errorMessage: result.error.message, btnQueryAdd: ''});
                    }
                else if(result.token){
                        let data = {};
                        data.token = result.token.id;
                        data.isDefault = primaryCard;
    
                        this.props.AddCustomerPaymentMethod(data, result => {
                                let allCards = [ ...this.state.cards];
                                
                                const {primaryCard} = this.state;
                                let default_source = this.state.default_source;
                                if(primaryCard) {
                                    default_source = result.data.id;
                                    allCards.unshift(result.data);
                                }else{
                                    allCards.splice(1, 0, result.data)
                                }
                                this.setState({btnQueryAdd: '',cards: allCards, addCard: false, primaryCard: false,errorMessage: '',default_source});
                            
                        },
                        fail => {
                            this.props.showSnackBar(fail.data);
                            this.setState({addCard: false, btnQueryAdd: '',errorMessage: ''})
                        });
                    }
                })
                .catch(err => {
                    this.setState({errorMessage: err.message, btnQueryAdd: ''});
                });
            });
          } else {
            
          }
    }
    exitPaymentDialog = () => {
        this.setState({ primaryCard: false, addCard: false, errorMessage:''});
    }
    showPaymentDlg = () => {
        this.setState({addCard: true});
    }
    handleSwitch = (event) => {
        this.setState({primaryCard: event.currentTarget.checked})
    }
    handleChange = ({error}) => {
        if (error) {
          this.setState({errorMessage: error.message});
        }else this.setState({errorMessage: ''});
      };

    changePaymentHandler = (value, index) => {
        let card = { ...this.state.cards[index] }
        if(value == 'remove'){
            this.setState({showRemoveConfirmation : true, removedIndex: index});
        }
        else if(value == 'makePrimary'){
            let allCards = this.state.cards;
            let card = { ...allCards[index] }
            this.props.SetCustomerCardDefault(card.id, result => {
                allCards.splice(0,0,allCards.splice(index,1)[0]);
                this.setState({default_source: result.data.default_source});
            },
            fail => {
                this.props.showSnackBar(fail.data);
            });
        } 
    }
    addCardInfo = () => {
        const dlgTitle = 'New Payment Method';
        
        const {classes, theme} = this.props;
        
        const {primaryCard, btnQueryAdd} = this.state;
        return (
        <Dialog
            classes={{
                paper: classes.dialogPaperCnt,
                scrollBody: classes.dialogCnt
                }}
                fullWidth={true}
                scroll="body"
                disableBackdropClick={true}
                disableEscapeKeyDown={true}
                open={true}
                onClose={this.exitPaymentDialog}
                >
                    <DialogTitle
                            id="form-dialog-title"
                            classes={{ root: classes.paymentDefaultDialogTitle }}
                            >
                            <div className={classes.paymentTitle}>
                                <Typography variant="h2">
                                    <FormattedMessage id="team-settings.payment-method.creation.title" defaultMessage={dlgTitle}/>
                                </Typography>
                                <IconButton
                                btnType="transparent"
                                onClick={this.exitPaymentDialog}
                                >
                                    <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                                </IconButton>
                            </div>
                    </DialogTitle>
                    <DialogContent className={classes.defaultDialogContent }>
                            <form>
                                <div className={this.state.errorMessage.length?classes.error:''} role="alert">
                                    {this.state.errorMessage}
                                </div>
                                <div className={classes.paymentDialogContent } displayName='aaa'>
                                    <label>
                                        <span className={classes.lblStyle}><FormattedMessage id="team-settings.payment-method.creation.form.card-no.label" defaultMessage="Card number"/></span>
                                        <CardNumberElement className={classes.cardNumberCnt}
                                            onChange={this.handleChange}
                                        />
                                    </label>
                                    <div className={classes.container}>
                                        <label>
                                            <span className={classes.lblStyle}><FormattedMessage id="team-settings.payment-method.creation.form.expiry-date.label" defaultMessage="Expiration date"/></span> 
                                            <CardExpiryElement className={classes.expiryElement}
                                                onChange={this.handleChange}
                                            />
                                        </label>
                                        <label>
                                            <span className={classes.lblStyle}><FormattedMessage id="team-settings.payment-method.creation.form.cvc.label" defaultMessage="CVC"/></span>
                                            <CardCVCElement className={classes.cvcElement}
                                                onChange={this.handleChange} />
                                        </label>
                                    
                                    </div>
                                    <Divider style={{marginTop: 20}}/>
                                    <div  style={{marginTop: 20, display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                                        <Typography variant="h6" 
                                        className= {classes.fontSize}
                                        style={{fontWeight: theme.typography.fontWeightMedium, fontSize: "11px"}}>
                                           <FormattedMessage id="team-settings.payment-method.creation.form.primarylabel" defaultMessage="Make this card my primary method"/> 
                                        </Typography>
                                        <DefaultSwitch
                                            checked={primaryCard}
                                            color = 'Dark'
                                            onChange={event => {
                                            this.handleSwitch(event);
                                            }}
                                        />
                                    </div>
                                </div>
                            </form>
                    
                        <ButtonActionsCnt
                            cancelAction={this.exitPaymentDialog}
                            successAction={this.addPaymentMethod}
                            successBtnText={<FormattedMessage id="team-settings.payment-method.creation.form.add-button.label" defaultMessage="Add Payment Method"/>}
                            cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
                            btnType={'success'}
                            btnQuery={btnQueryAdd}
                            disabled={btnQueryAdd === "progress"}
                        />   
                        

                    </DialogContent>
        </Dialog>)

    }

    removeAddedCard = () => {
        const {removedIndex} = this.state;
        let card = { ...this.state.cards[removedIndex] }
        this.setState({ btnQueryRemove: "progress" }, () => {
            this.props.RemoveCustomerPaymentMethod(card.id, result => {
                let allCards = this.state.cards;
                allCards.splice(removedIndex,1);
                this.setState({cards: allCards, showRemoveConfirmation: false, removedIndex: -1, btnQueryRemove: ''});
            },
            fail => {
                this.props.showSnackBar(fail.data);
                this.setState({showRemoveConfirmation: false, removedIndex: -1, btnQueryRemove: ''});
            });
        });
    }

    confirmationDialog = () => {
        
        const {classes, theme} = this.props;
        const {removedIndex} = this.state;
        let card = { ...this.state.cards[removedIndex] }
        
        const {btnQueryRemove} = this.state;
        return (
        <Dialog
            classes={{
                paper: classes.dialogPaperCnt,
                scrollBody: classes.dialogCnt
                }}
                fullWidth={true}
                scroll="body"
                disableBackdropClick={true}
                disableEscapeKeyDown={true}
                open={true}
                onClose={this.exitConfirmationDialog}
                >
                    <DialogTitle
                            id="form-dialog-title"
                            classes={{ root: classes.removeDefaultDialogTitle }}
                            >
                            <div className={classes.paymentTitle}>
                                <Typography variant="h2">
                                    Remove Payment Method
                                </Typography>
                                <IconButton
                                btnType="transparent"
                                onClick={this.exitConfirmationDialog}
                                >
                                    <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                                </IconButton>
                            </div>
                    </DialogTitle>
                    <DialogContent className={classes.defaultDialogContent }>
                            <div className={classes.confirmationCnt}>
                                <img
                                    src={upgradePro}
                                    alt="Pro Plan Image"
                                    className={classes.removeIcon}
                                />
                                <Typography variant="h6" 
                                className= {classes.fontSize}
                                style={{marginTop: 24, fontSize: "13px", textAlign: 'center'}}>
                                    Are you sure you want to remove this visa card ending in {card.last4}?
                                </Typography>
                            </div>
                    
                        <ButtonActionsCnt
                            cancelAction={this.exitConfirmationDialog}
                            successAction={() => this.removeAddedCard()}
                            successBtnText={'Remove'}
                            cancelBtnText="Cancel"
                            btnType={'danger'}
                            btnQuery={btnQueryRemove}
                            disabled={btnQueryRemove === "progress"}
                        />   
                        

                    </DialogContent>
        </Dialog>)

    }

    exitConfirmationDialog = () => {
        this.setState({ showRemoveConfirmation: false, removedIndex: -1});
    }

    getAllCardsDetails = () => {
        const {classes, theme} = this.props;
        const withPrimary = {remove: 'Remove', makePrimary: 'Make Primary'};
        const {cards, default_source, loading} = this.state;
        if(!loading && cards.length == 0)
        return  <div className={classes.noPaymentMethodCnt}>
                    <img
                        src={billingIcon}
                        alt="No Payment Image"
                        className={classes.noPaymentIcon}
                    />
                    <Typography variant="h4" className={classes.noPaymentTitle}>
                        No Payment Method
                    </Typography>
                    <Typography variant="body2" className={classes.noPaymentCnt}>
                        There is no Payment method available at this moment
                    </Typography>
                </div>
        else
            return cards.map((item,index) =>

                        <div key={index} className={classes.cardItemMain}>
                            <div className={classes.cardItemCnt}>
                                <div className={classes.cardIconCnt} >
                                    <img
                                        src={item.brand == 'Visa'?Visa:Master}
                                        alt="Credit Card Image"
                                        className={classes.cardIcon}
                                    />
                                    
                                </div>
                                <div className={classes.cardDetail}>
                                    <Typography variant="h6" className={classes.cardDetailNumber}>
                                        <FormattedMessage id="team-settings.payment-method.list.visa-ending" defaultMessage={`Visa card ending in ${item.last4}`}/>
                                    </Typography>
                                    <Typography variant="body2" className={classes.cardDetailExpiry}>
                                        <FormattedMessage id="team-settings.payment-method.list.expiry" defaultMessage="Expiry" /> {item.exp_month}/{item.exp_year}
                                    </Typography>
                                </div>
                                {item.id == default_source &&
                                    <div className={classes.primaryCard}>
                                        <span><FormattedMessage id="team-settings.payment-method.list.primary" defaultMessage="PRIMARY"/></span>
                                    </div>
                                }
                            </div>
                            {item.id != default_source && <div className={classes.cardBtns}>
                                <PaymentActionDropDown classes={classes} theme={theme} ddData={withPrimary} id={index} changePaymentHandler= {(type,index) => this.changePaymentHandler(type,index)} />
                            </div>}
                        </div>
            
        )
    }

    render(){
        const {classes} = this.props;
        const {addCard, showRemoveConfirmation, loading} = this.state;
        return(
            <Fragment>
            {addCard && this.addCardInfo() }
            {showRemoveConfirmation && this.confirmationDialog() }
            <div style={{height: '100%'}}>
                <div className={classes.paymentHeaderCnt} >
                    <Typography variant="h2" className={classes.bodyTitle}>
                       <FormattedMessage id="team-settings.payment-method.saved-methods.label" defaultMessage="Saved Payment Method(s)"/> 
                    </Typography>
                    <CustomButton
                        btnType="white"
                        variant="contained"
                        style={{textTransform: 'none', fontSize: "14px" }}
                        onClick={this.showPaymentDlg}
                    >
                       <FormattedMessage id="team-settings.payment-method.add-payment-button.label" defaultMessage="Add Payment Method"/> 
                    </CustomButton>
                </div>
                    <div className={classes.paymentBodyCnt} style={{height: 600, overflowY: 'auto', }}>
                    {loading && 
                        <div style={{height: '100%', width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                            <div class="loader"></div>
                        </div>}
                    {!loading && this.getAllCardsDetails()}
                    </div>
            </div>
            </Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
        paymentPlan: state.profile.data.teams.filter(item => item.companyId == state.profile.data.activeTeam)[0].paymentPlan
    };
  };
export default compose(
    withRouter,
    connect(
      mapStateToProps,{   
        FetchPaymentMethod,
        AddCustomerPaymentMethod,
        RemoveCustomerPaymentMethod,
        SetCustomerCardDefault
         }
    ),
    withStyles(classes, { withTheme: true })
  )(injectStripe(Payment));