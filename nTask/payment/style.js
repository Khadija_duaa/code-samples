const PaymentStyles = theme => ({
    paymentDetailsDialog: {
        // overflowY: "auto",
    },
    fontSize: {
        fontSize: "11px !important"
      },
    dialogPaperCnt: {
        overflowY: "visible",
        background: theme.palette.common.white,
        width: "400px!important",
      },
    paymentHeaderCnt:{
        // padding: '12px 18px',
        // borderRadius: 4,
        // background: theme.palette.common.white,
        // border: `1px solid ${theme.palette.border.lightBorder}`,
        marginBottom: 10,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    paymentBodyCnt:{
        borderRadius: 4,
        background: theme.palette.common.white,
        border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    cardItemMain: {
        display: 'flex', 
        justifyContent: 'space-between', 
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        padding: '14px 16px 14px 20px'
    },
    cardItemCnt: {
        display: 'flex',
    },
    cardIconCnt: {
        // width: 45,
        // height: 36,
        alignItems: 'center',
        display: 'flex',
    },
    cardDetail: {
        marginLeft: 25,
    },
    cardDetailNumber: {
        fontSize: "13px !important"
    },
    cardDetailExpiry: {
        fontSize: "11px !important"
    },
    primaryCard: {
        padding: '8px 12px',
        backgroundColor: theme.palette.background.lightBlue,
        color: theme.palette.text.azure,
        borderRadius: 4,
        marginLeft: 15,
        fontSize: "10px !important",
        height: 'fit-content',
        fontWeight: theme.typography.fontWeightMedium,
        letterSpacing: 1,
    },
    
      paymentDefaultDialogTitle:{
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        background: theme.palette.background.light,
        // padding: 26,
        padding: '20px 30px 20px 34px',
        borderTopLeftRadius: 'inherit',
        borderTopRightRadius: 'inherit',
      },
    removeDefaultDialogTitle:{
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        background: theme.palette.background.light,
        // padding: 26,
        padding: '20px 20px 20px 20px',
        borderTopLeftRadius: 'inherit',
        borderTopRightRadius: 'inherit',
    },
    paymentTitle: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        fontWeight: theme.typography.fontWeightRegular,
    },
    defaultDialogContent: {
        padding: 0,
    },
    paymentDialogContent: {
        padding: '15px 34px 40px 34px',
    },
    lblStyle: {
        display: 'flex',
        marginBottom: 9,
        fontSize: "10px !important",
        color: theme.palette.secondary.light,
    },
    cardNumberCnt: {
        padding: 10,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,  
    },
    expiryElement: {
        padding: 10,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
    },
    cvcElement: {
        padding: 10,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
    },
    container: {
        display: 'flex',
        marginTop: 15,
        width: '100%',
        justifyContent: 'space-between',
        '& label': {
            width: '45%',
        }
    },
    paymentMenuItem: {
        color: theme.palette.text.primary,
    },
    error : {
        // marginTop: 10,
        margin: '10px 34px 0px',
        fontSize: "13px !important",
        color: `${theme.palette.text.danger}!important`,
        padding: 10,
        backgroundColor: `${theme.palette.background.dangerLight}!important`,
        border: '1px!important',
        borderRadius: 4,
    },
    noPaymentMethodCnt:{
        marginTop: 126,
        marginBottom: 228,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        flexDirection: 'column',
    },
    noPaymentIcon: {
        width: 77,
        height: 88
    },
    noPaymentTitle: {
        marginTop: 30,
    },
    noPaymentCnt: {
        marginTop: 9, 
        fontSize: "13px !important"
    },
    confirmationCnt: {
        padding: '30px 60px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    removeIcon: {
        height: 80,
    }
});
export default PaymentStyles;