import { SET_SELECTED_MESSAGE, START_NEW_CONVERSATION } from "./constants";

export const dispatchSelectedMsg = (dispatch, obj) => {
  dispatch({ type: SET_SELECTED_MESSAGE, payload: obj });
};
export const dispatchNewConversation = (dispatch, obj) => {
  dispatch({ type: START_NEW_CONVERSATION, payload: obj });
};
