import {SET_SELECTED_MESSAGE, START_NEW_CONVERSATION} from './constants';

function reducer(state, action) {
    switch (action.type) {
      case SET_SELECTED_MESSAGE:
        return {...state,  selectedMessage: action.payload};
      case START_NEW_CONVERSATION:
        return {...state, newConversation: action.payload}
      default:
        throw new Error();
    }
  }

  export default reducer