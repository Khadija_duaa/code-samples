const collaborationStyles = (theme) => ({
  collaborationViewCnt: {
    padding: '20px 20px 20px 46px'
  },
  collaborationHeaderCnt:{
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 12
  },
  toggleContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  toggleBtnGroup: {
    display: 'flex',
    flexWrap: 'nowrap',
    borderRadius: 4,
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: 'none',
    '& $toggleButtonSelected': {
      color: theme.palette.text.secondary,
      backgroundColor: theme.palette.common.white,
      '&:after': {
        background: theme.palette.common.white,
      },
      '&:hover': {
        background: theme.palette.common.white,
      },
    },
  },
  toggleButton: {
    height: 'auto',
    padding: '4px 20px 5px',
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: 'capitalize',
    '&:hover': {
      background: theme.palette.common.white,
    },
    "&[value = 'center']": {
      borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  toggleButtonSelected: {},
  recordingIcon: {
    color: theme.palette.icon.gray400,
    fontSize: "20px !important",
    marginRight: 3
  },
  addMessageIcon: {
    color: theme.palette.icon.white,
    fontSize: "20px !important",
    marginRight: 6
  }
})

export default collaborationStyles