import React, { useState, useReducer } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import collaborationStyles from "./collaboration.style";
import ContentArea from "./ContentArea/ContentArea";
import CollaborationContext from "./Context/collaboration.context";
import reducer from "./Context/reducer";
import initialState from "./Context/initialState";
import CollaborationViewHeader from "./CollaborationViewHeader";

function CollaborationView({ classes }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <CollaborationContext.Provider value={{ dispatch, state }}>
      <div className={classes.collaborationViewCnt}>
        <CollaborationViewHeader />
        <ContentArea />
      </div>
    </CollaborationContext.Provider>
  );
}

export default compose(withStyles(collaborationStyles, { withTheme: true }))(CollaborationView);
