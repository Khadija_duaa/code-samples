import React, { useState, useContext } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import RecordingIcon from "@material-ui/icons/RadioButtonChecked";
import AddMessage from "@material-ui/icons/AddComment";
import collaborationStyles from "./collaboration.style";
import CustomButton from "../../components/Buttons/CustomButton";
import ContentArea from "./ContentArea/ContentArea";
import CollaborationContext from "./Context/collaboration.context";

function CollaborationViewHeader({ classes }) {
  const [activeTab, setActiveTab] = useState("left");
  const context = useContext(CollaborationContext);
  //Handle Tab switch
  const handleAlignment = (event, tab) => {
    setActiveTab(tab);
  };

  return (
    <div className={classes.collaborationHeaderCnt}>
      <div className={classes.toggleContainer}>
        <ToggleButtonGroup
          value={activeTab}
          exclusive
          onChange={handleAlignment}
          classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}>
          <ToggleButton
            value="left"
            // onClick={this.renderListView}
            classes={{
              root: classes.toggleButton,
              selected: classes.toggleButtonSelected,
            }}>
            Conversations
          </ToggleButton>
          <ToggleButton
            value="center"
            // onClick={this.renderGridView}
            classes={{
              root: classes.toggleButton,
              selected: classes.toggleButtonSelected,
            }}>
            Files
          </ToggleButton>
        </ToggleButtonGroup>
      </div>
      <div>
        <CustomButton btnType="white" variant="contained" style={{ marginRight: 10 }}>
          <RecordingIcon className={classes.recordingIcon} />
          New Recording
        </CustomButton>
        <CustomButton btnType="blue" variant="contained">
          <AddMessage className={classes.addMessageIcon} />
          New Conversation
        </CustomButton>
      </div>
    </div>
  );
}

export default compose(withStyles(collaborationStyles, { withTheme: true }))(
  CollaborationViewHeader
);
