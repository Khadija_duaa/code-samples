const messageDetailHeaderStyles = theme => ({
  messageDetailsHeaderCnt: {
    padding: "6px 8px",
    height: 44,
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    borderLeft: "none",
    borderBottom: "none",
    borderRadius: "0 20px 0 0",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    zIndex: 1,
    boxShadow: "0px 2px 0px rgba(234, 234, 234, 1)",
    "& input": {
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
  messageDetailsHeaderLeft: {
    width: 250,
    display: "flex",
    alignItems: "center",
    "& h2": {
      marginLeft: 6,
      fontFamily: theme.typography.fontFamilyLato,
    },
    "& svg": {
      fontSize: "14px !important",
    },
  },
  messageDetailsHeaderRight: {
    display: "flex",
    alignItems: "center",
  },
  audioCallIcon: {
    fontSize: "16px !important",
    color: theme.palette.common.white,
  },
  videoCallIcon: {
    fontSize: "16px !important",
    color: theme.palette.common.white,
  },
  callIconsCnt: {
    display: "flex",
    paddingRight: 8,
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
  },
});

export default messageDetailHeaderStyles;
