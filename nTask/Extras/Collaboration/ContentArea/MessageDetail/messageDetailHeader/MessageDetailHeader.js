import React, { useState, useContext } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import SvgIcon from "@material-ui/core/SvgIcon";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import messageDetailHeaderStyles from "./messageDetailHeader.style";
import DefaultTextField from "../../../../../components/Form/TextField";
import CollaborationContext from "../../../Context/collaboration.context";
import CustomIconButton from "../../../../../components/Buttons/CustomIconButton";
import EditIcon from "../../../../../components/Icons/EditIcon";
import MoreActionDropDown from "../../MessageList/moreActionDropdown/MoreActionDropDown";
import CustomTooltip from "../../../../../components/Tooltip/Tooltip";
import AudioCallIcon from "../../../../../components/Icons/AudioCallIcon";
import VideoCallIcon from "../../../../../components/Icons/VideoCallIcon";
import ParticipantDropdown from "../../../../../components/Dropdown/ParticipantDropdown/ParticipantDropdown";
import SelectSearchDropdown from "../../../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateAssigneeData } from "../../../../../helper/generateSelectData";

function MessageDetailHeader({ classes, theme }) {
  const [edit, setEdit] = useState(false);
  const [groupName, setGroupName] = useState("");
  const context = useContext(CollaborationContext);
  const {
    state: { selectedMessage },
  } = context;

  // Handle Show Input
  const handleShowInput = () => {
    setEdit(true);
  };
  // Handle Hide Input
  const handleHideInput = () => {
    if (!groupName) {
      setEdit(false);
    }
  };
  // Handle group name input
  const handleGroupNameInput = e => {
    setGroupName(e.target.value);
  };
  return (
    <div className={classes.messageDetailsHeaderCnt}>
      <div className={classes.messageDetailsHeaderLeft}>
        <SelectSearchDropdown
          data={() => generateAssigneeData([])}
          label=""
          // selectChange={this.handleFilterChange}
          // selectedValue={assignees}
          styles={{ margin: 0 }}
          customStyles={{control: {border: 'none'}}}
          placeholder="Select Participants"
          avatar
        />
        {selectedMessage.groupName ? (
          <>
            {edit ? (
              <ClickAwayListener onClickAway={handleHideInput}>
                <DefaultTextField
                  label={false}
                  fullWidth
                  error={false}
                  // error={!!templateInputError}
                  // errorState={templateInputError}
                  // errorMessage={templateInputErrorMessage}
                  formControlStyles={{ marginBottom: 0 }}
                  defaultProps={{
                    id: "templateName",
                    type: "text",
                    // onBlur: handleInputBlur,
                    onChange: handleGroupNameInput,
                    autoFocus: true,
                    // onKeyUp: enterKeyHandler,
                    // value: templateName,
                    placeholder: "Enter template name",
                    inputProps: {
                      maxLength: 80,
                      style: { fontSize: "16px !important", padding: "6px 7px", fontWeight: 700 },
                    },
                  }}
                />
              </ClickAwayListener>
            ) : (
              <>
                <Typography variant="h2">{selectedMessage.groupName}</Typography>
                <CustomTooltip
                  helptext="Edit group name"
                  placement="top"
                  style={{ color: theme.palette.common.white }}>
                  <CustomIconButton
                    btnType="transparent"
                    onClick={handleShowInput}
                    style={{ padding: 4, marginLeft: 5 }}>
                    <SvgIcon
                      className={classes.editFieldIcon}
                      color={theme.palette.icon.gray600}
                      viewBox="0 0 14 13.95">
                      <EditIcon />
                    </SvgIcon>
                  </CustomIconButton>
                </CustomTooltip>
              </>
            )}
          </>
        ) : null}
      </div>
      <div className={classes.messageDetailsHeaderRight}>
        <div className={classes.callIconsCnt}>
          <CustomTooltip
            helptext="Audio Call"
            placement="top"
            style={{ color: theme.palette.common.white }}>
            <CustomIconButton
              btnType="green"
              // onClick={handleShowInput}
              style={{ padding: 8 }}>
              <SvgIcon className={classes.audioCallIcon} viewBox="0 0 15 15.032">
                <AudioCallIcon />
              </SvgIcon>
            </CustomIconButton>
          </CustomTooltip>
          <CustomTooltip helptext="Video Call" placement="top">
            <CustomIconButton
              btnType="blue"
              // onClick={handleShowInput}
              style={{ marginLeft: 8, padding: 8 }}>
              <SvgIcon className={classes.videoCallIcon} viewBox="0 0 16.01 11.201">
                <VideoCallIcon />
              </SvgIcon>
            </CustomIconButton>
          </CustomTooltip>
        </div>
        <ParticipantDropdown
        // assignedTo={issueAssigneeList}
        // updateAction={this.updateAssignee}
        // isArchivedSelected={isArchivedSelected}
        // obj={currentIssue}
        // closeIssueDetailsPopUp={this.props.closeIssueDetailsPopUp}
        // addPermission={canAddAsignee}
        // deletePermission={canDeleteAsignee}
        />
        <MoreActionDropDown />
      </div>
    </div>
  );
}

export default compose(withStyles(messageDetailHeaderStyles, { withTheme: true }))(
  MessageDetailHeader
);
