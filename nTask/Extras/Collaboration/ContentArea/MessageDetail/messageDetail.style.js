const messageDetailStyles = theme => ({
  messageDetailCnt: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    minHeight: '100%'
  },
  messageDetailContentCnt: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    padding: '0 30px 10px 62px',
    background: theme.palette.background.airy,
    borderRight: `1px solid ${theme.palette.border.grayLighter}`,
    borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
    borderRadius: '0 0 20px 0'
  },
  messageDetailListCnt: {
    flex: 1,
  },
  messageTextEditorCnt: {},
});

export default messageDetailStyles;
