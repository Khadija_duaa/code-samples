import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import messageDetailStyles from "./messageDetail.style";
import MessageDetailHeader from "./messageDetailHeader/MessageDetailHeader";
import MessageDetailList from "./messageDetailList/MessageDetailList";
import ChatTextEditor from "../../../../components/TextEditor/CustomTextEditor/ChatTextEditor/ChatTextEditor";

function MessageDetail({ classes }) {
  return (
    <div className={classes.messageDetailCnt}>
      <MessageDetailHeader />
      <div className={classes.messageDetailContentCnt}>
        <div className={classes.messageDetailListCnt}>
          <MessageDetailList />
        </div>
        <div className={classes.messageTextEditorCnt}>
          <ChatTextEditor id='collaborationEditorToolbar'/>
        </div>
      </div>
    </div>
  );
}

export default compose(withStyles(messageDetailStyles, { withTheme: true }))(MessageDetail);
