const messageDetailListStyles = theme => ({
    MessageDetailsList: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
    }
});

export default messageDetailListStyles;
