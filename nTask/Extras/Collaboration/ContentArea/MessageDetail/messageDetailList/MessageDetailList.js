import React, { useState, useContext } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import messageDetailListStyles from "./messageDetailList.style";
import EmptyState from "../../../../../components/EmptyStates/EmptyState";

function MessageDetailList({ classes, theme }) {
  return (
    <div className={classes.MessageDetailsList}>
      <EmptyState
        screenType="conversation"
        heading={(
          <>
            Start conversation by 
            {' '}
            <br />
            {' '}
            typing your first message below.
          </>
        )}
      />
    </div>
  );
}

export default compose(withStyles(messageDetailListStyles, { withTheme: true }))(MessageDetailList);
