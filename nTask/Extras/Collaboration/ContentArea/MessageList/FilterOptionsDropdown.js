// @flow

import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ListItemText from "@material-ui/core/ListItemText";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Scrollbars } from "react-custom-scrollbars";
import CustomIconButton from "../../../Buttons/IconButton";
import DropdownMenu from "../../../Dropdown/DropdownMenu";
import dropdownStyle from "./style";
import ColorPicker from "../../../Dropdown/ColorPicker/ColorPicker";

type DropdownProps = {
  classes: Object,
  theme: Object,
  size: String,
  handleOptionSelect: Function,
  selectedColor: String,
  handleColorSelect: Function,
  permission: Object,
};

// MoreActionDropDown Main Component
function MoreActionDropDown(props: DropdownProps) {
  const {
    classes,
    theme,
    size,
    handleOptionSelect,
    selectedColor,
    handleColorSelect,
    permission,
  } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [colorPicker, setColorPicker] = useState(false);

  const handleClick = event => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
    setColorPicker(false);
  };
  const handleItemClick = (event, option) => {
    // Fuction responsible for selecting item from the list
    handleOptionSelect(option); // Callback on item select used to lift up option to parent if needed

    if (option == "color") {
      /** if option is color then drop down will be open and color control will open */
      setColorPicker(true);
    } else {
      setAnchorEl(null);
      setColorPicker(false);
    }
  };
  const onColorChange = color => {
    /** function calls when user change color */
    handleColorSelect(color);
    setColorPicker(false);
  };
  const open = Boolean(anchorEl);

  return (
    <>
      <CustomIconButton
        btnType="condensed"
        style={{ padding: 0 }}
        onClick={handleClick}
        buttonRef={anchorEl}>
        <MoreVerticalIcon htmlColor={theme.palette.secondary.medDark} style={{ fontSize: "24px" }} />
      </CustomIconButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size={size}
        placement="bottom-end">
        {/* <Scrollbars autoHide autoHeight autoHeightMin={0} autoHeightMax={130}> */}
        <List>
          <ListItem disableRipple classes={{ root: classes.menuHeadingItem }}>
            <ListItemText
              primary="Select Action"
              classes={{ primary: classes.menuHeadingListItemText }}
            />
          </ListItem>
          <ListItem
            button
            className={classes.listItem}
            onClick={event => {
              // handleItemClick(event, "publickLink");
            }}>
            <span>Reply Later</span>
          </ListItem>
          <ListItem
            button
            className={classes.listItem}
            onClick={event => {
              // handleItemClick(event, "publickLink");
            }}>
            <span>Group Conversations</span>
          </ListItem>
          <ListItem
            button
            className={classes.listItem}
            onClick={event => {
              // handleItemClick(event, "publickLink");
            }}>
            <span>Private Conversations</span>
          </ListItem>
          <ListItem
            button
            className={classes.listItem}
            onClick={event => {
              // handleItemClick(event, "publickLink");
            }}>
            <span>Unread Conversations</span>
          </ListItem>
          <ListItem
            button
            className={classes.listItem}
            onClick={event => {
              // handleItemClick(event, "publickLink");
            }}>
            <span>Muted Conversations</span>
          </ListItem>
        </List>
        {/* </Scrollbars> */}
      </DropdownMenu>
    </>
  );
}
MoreActionDropDown.defaultProps = {
  classes: {},
  theme: {},
  size: "small",
  handleOptionSelect: () => {},
  selectedColor: "",
  handleColorSelect: () => {},
  permission: {},
};
export default compose(
  withRouter,
  withStyles(dropdownStyle, { withTheme: true })
)(MoreActionDropDown);
