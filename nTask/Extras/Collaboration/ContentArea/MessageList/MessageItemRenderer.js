import React, { useState, memo, useContext } from "react";
import moment from "moment";
import clsx from "clsx";
import MuteIcon from "@material-ui/icons/VolumeOff";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import MoreActionDropDown from "./moreActionDropdown/MoreActionDropDown";
import messageListStyles from "./messageList.styles";
import CollaborationContext from "../../Context/collaboration.context";
import { dispatchSelectedMsg } from "../../Context/actions";
import ToggleUpdateIcon from "../../../../components/Icons/ToggleUpdateIcon";
import SvgIcon from "@material-ui/core/SvgIcon";

function MessageItemRenderer({ itemProps, list, classes }) {
  const { key, index, style } = itemProps;
  const context = useContext(CollaborationContext);
  const {
    state: { selectedMessage },
  } = context;
  const { dispatch } = context;

  // handle Message select
  const handleMessageSelect = (event, message) => {
    dispatchSelectedMsg(dispatch, message);
  };
  const item = list[index];
  const lastMessageFromName = item && item.lastMessageFrom.split(" ")[0];
  const lastMessageFrom = item && item.lastMessage.substring(0, 20);
  const lastMessageTime = item && moment(item.updatedDate).format("MMM DD");
  const isMessaageSelected = item && selectedMessage && item.name === selectedMessage.name;
  return (
    <>
      {index === 0 && (
        <ListItem
          style={style}
          // onClick={event => handleMessageSelect(event, item)}
          className={clsx({
            [classes.messageItem]: true,
            [classes.messageItemSelected]: true,
          })}>
          <span className={classes.newMessageIconCnt}>
            <SvgIcon
              viewBox="-1 2 24 21"
              // htmlColor={theme.palette.secondary.medDark}
              className={classes.newMessageIcon}>
              <ToggleUpdateIcon />
            </SvgIcon>
          </span>
          <div className={classes.messageItemContentCnt}>
            <div className={classes.lastMessageFromCnt}>
              <Typography
                variant="body1"
                className={clsx({
                  [classes.messageFromName]: true,
                  [classes.unreadMsgStyle]: true,
                })}>
                New Conversation
              </Typography>
            </div>
          </div>
        </ListItem>
      )}
      {item && index > 0 && (
        <ListItem
          key={key}
          style={style}
          onClick={event => handleMessageSelect(event, item)}
          className={clsx({
            [classes.messageItem]: true,
            [classes.messageItemSelected]: isMessaageSelected,
          })}>
          {item.isMuted ? (
            <div className={classes.multiUserAvatar}>
              <span className={classes.leftAvatar}>A</span>
              <span className={classes.rightAvatar}>B</span>
            </div>
          ) : (
            <CustomAvatar personal size="small36" />
          )}
          <div className={classes.messageItemContentCnt}>
            <div className={classes.lastMessageFromCnt}>
              <Typography
                variant="body1"
                className={clsx({
                  [classes.messageFromName]: true,
                  [classes.unreadMsgStyle]: !item.isRead,
                })}>
                {item.name}
              </Typography>
              <Typography variant="body2" className={classes.lastMessageTime}>
                {lastMessageTime}
              </Typography>
              <span className={classes.messageActionDropdownCnt}>
                <MoreActionDropDown />
              </span>
            </div>
            <Typography
              variant="caption"
              className={clsx({
                [classes.lastMessageFrom]: true,
                [classes.unreadMsgStyle]: !item.isRead,
              })}>
              {lastMessageFromName}
              :&nbsp;
              {lastMessageFrom}
              ...
            </Typography>
          </div>
          {item.isMuted && (
            <CustomIconButton
              variant="transparent"
              style={{ position: "absolute", right: 8, bottom: 7, padding: 2 }}>
              <MuteIcon className={classes.muteIcon} />
            </CustomIconButton>
          )}
          {!item.isRead && !item.isMuted && <span className={classes.notificationDot} />}
        </ListItem>
      )}
    </>
  );
}

export default withStyles(messageListStyles, { withTheme: true })(memo(MessageItemRenderer));
