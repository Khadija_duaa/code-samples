import React, { useState, useEffect, useContext } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
// import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";
import { InfiniteLoader, List } from "react-virtualized";
import MuteIcon from "@material-ui/icons/VolumeOff";
import "react-virtualized/styles.css"; // only needs to be imported once
import moment from "moment";
import clsx from "clsx";
import mockData from "./mockData";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import MessageListHeader from "./MessageListHeader";
import messageListStyles from "./messageList.styles";
import { calculateContentHeight } from "../../../../utils/common";
import MoreActionDropDown from "./moreActionDropdown/MoreActionDropDown";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import MessageItemRenderer from "./MessageItemRenderer";
// This example assumes you have a way to know/load this information
const remoteRowCount = 200;

let list = [];

function isRowLoaded({ index }) {
  return !!list[index];
}

function loadMoreRows({ startIndex, stopIndex }) {
  return Promise.resolve(mockData).then(response => {
    const dummyData = response.filter((d, i) => {
      return i >= startIndex && i <= stopIndex;
    });
    list = [...list, ...dummyData];
  });
}

function MessageList({ classes, theme }) {
  // Function render the custom markup of row
  const rowRenderer = props => {
    return <MessageItemRenderer itemProps={props} list={list} />;
  };
  return (
    <div className={classes.messageListCnt}>
      <MessageListHeader />
      <div
        style={{
          borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
          borderLeft: `1px solid ${theme.palette.border.grayLighter}`,
          borderRight: `1px solid ${theme.palette.border.grayLighter}`,
          borderRadius: "0 0 0 20px",
          overflow: "hidden",
        }}>
        <InfiniteLoader
          isRowLoaded={isRowLoaded}
          loadMoreRows={loadMoreRows}
          rowCount={remoteRowCount}>
          {({ onRowsRendered, registerChild }) => (
            <List
              height={calculateContentHeight() - 70}
              onRowsRendered={onRowsRendered}
              minimumBatchSize={20}
              ref={registerChild}
              rowCount={remoteRowCount}
              rowHeight={56}
              rowRenderer={rowRenderer}
              width={300}
            />
          )}
        </InfiniteLoader>
      </div>
    </div>
  );
}

export default compose(withStyles(messageListStyles, { withTheme: true }))(MessageList);
