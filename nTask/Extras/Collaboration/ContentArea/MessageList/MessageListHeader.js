import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import FilterListIcon from "@material-ui/icons/FilterList";
import messageListStyles from "./messageList.styles";
import DefaultTextField from "../../../../components/Form/TextField";
import CustomChipSelectDropdown from "../../../../components/Dropdown/CustomChipSelectDropdown/CustomChipSelectDropdown";
import filterData from "../../../../utils/constants/collaborationFilterData";

function MessageListHeader({ classes }) {
  const [search, setSearch] = useState("");

  // Handle Search Input On Change
  const handleSearchInput = event => {
    setSearch(event.target.value);
  };

  return (
    <div className={classes.messageListHeaderCnt}>
      <div className={classes.filterCnt}>
        <DefaultTextField
          label={false}
          fullWidth
          error={false}
          errorState={false}
          formControlStyles={{ marginBottom: 0 }}
          defaultProps={{
            type: "text",
            onChange: e => handleSearchInput(e),
            value: search,
            placeholder: "Search",
            inputProps: { style: { padding: "8px 14px" } },
          }}
        />

        <CustomChipSelectDropdown
          label=""
          options={filterData}
          option={null}
          icon={<FilterListIcon />}
          height="110px"
          scrollHeight={180}
        />
      </div>
    </div>
  );
}

export default compose(withStyles(messageListStyles, { withTheme: true }))(MessageListHeader);
