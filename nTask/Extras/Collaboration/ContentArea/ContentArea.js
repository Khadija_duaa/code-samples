import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import contentAreaStyles from "./contentArea.styles";
import MessageList from './MessageList/MessageList';
import MessageDetail from './MessageDetail/MessageDetail';
function ContentArea({ classes }) {
//   const [activeTab, setActiveTab] = useState("left");


  return (
    <div className={classes.contentAreaCnt}>
      <MessageList />
      <MessageDetail />
    </div>
  );
}

export default compose(withStyles(contentAreaStyles, { withTheme: true }))(ContentArea);
