import React, { Component, Fragment } from "react";
import Slider from "@material-ui/core/Slider";
import { withStyles } from "@material-ui/core/styles";
import gantStyles from "./style";
import AddIcon from "@material-ui/icons/AddCircle";
import CheckIcon from "@material-ui/icons/CheckCircle";
import IssuesIcon from "@material-ui/icons/ListAlt";
import RiskIcon from "@material-ui/icons/ReportProblem";
import MoneyIcon from "@material-ui/icons/MonetizationOn";
import Typography from "@material-ui/core/Typography";
import DefaultTextField from "../../../components/Form/TextField";
import Hotkeys from "react-hot-keys";

class Toolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addTask: false,
      addMileStone: false
    };
    this.handleZoomChange = this.handleZoomChange.bind(this);
    this.handleAddClick = this.handleAddClick.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onMiestoneKeyDown = this.onMiestoneKeyDown.bind(this);
    this.handleInputBlur = this.handleInputBlur.bind(this);
  }

  handleZoomChange(e, value) {
    if (this.props.onZoomChange) {
      this.props.onZoomChange(value);
    }
  }
  handleInputBlur(event, name, inputValue){
    this.setState({[name]: false, [inputValue]: ''})
  }
  handleInputChange(event, name) {
    this.setState({ [name]: event.target.value });
  }
  handleAddClick(event, name) {
    this.setState({ [name]: true });
  }
  onKeyDown(event) {
    if (event === "enter") {
      this.props.addTaskAction({text: this.state.taskTitle,  type:'task', projectId: this.props.projectId});
      this.setState({ taskTitle: '' });
    } else if (event == "esc") {
      this.setState({ addTask: false });
      this.setState({ taskTitle: '' });
    }
  }
  onMiestoneKeyDown(event) {
    if (event === "enter") {
      this.props.addTaskAction({text: this.state.mileStoneTitle, type:'milestone', projectId: this.props.projectId});
      this.setState({ mileStoneTitle: '' });
    } else if (event == "esc") {
      this.setState({ addMileStone: false });
      this.setState({ mileStoneTitle: '' });
    }
  }
  render() {
    const { classes, theme, zoom, tasks } = this.props;
    const { addMileStone, addTask, taskTitle, mileStoneTitle } = this.state;
    return (
      <div className={classes.toolbarCnt}>
        {/* Add Task start */}

        <div className={`${classes.addTaskCnt} flex_center_space_between_row`}>
          {addTask ? (
             <Hotkeys keyName="enter,esc" onKeyDown={this.onKeyDown}>
            <DefaultTextField
              fullWidth={true}
              formControlStyles={{ marginBottom: 0, marginTop: 0 }}
              error={false}
              noBorderRadius={true}
              label={false}
              defaultProps={{
                type: "text",
                autoFocus: true,
                placeholder: "Enter Task Title",
                onChange: event => this.handleInputChange(event, "taskTitle"),
                onBlur: event => this.handleInputBlur(event, "addTask", "taskTitle"),
                value:taskTitle,
                inputProps: {
                  style: { padding: "17px 14px" }
                }
               
              }}
            />
            </Hotkeys>
          ) : addMileStone ? 
          <Hotkeys keyName="enter,esc" onKeyDown={this.onMiestoneKeyDown}>
          <DefaultTextField
              fullWidth={true}
              formControlStyles={{ marginBottom: 0, marginTop: 0 }}
              error={false}
              noBorderRadius={true}
              label={false}
              defaultProps={{
                type: "text",
                autoFocus: true,
                placeholder: "Enter Milestone Title",
                onChange: event => this.handleInputChange(event, "mileStoneTitle"),
                onBlur:event => this.handleInputBlur(event, "addMileStone", "mileStoneTitle"),
                value: mileStoneTitle,
                inputProps: {
                  style: { padding: "17px 14px" }
                }
              }}
            /> </Hotkeys>: (
            <Fragment>
              <div
                onClick={event => this.handleAddClick(event, "addTask")}
                className={`${classes.addTaskInnerCnt} flex_center_start_row`}
              >
                <AddIcon
                  htmlColor={theme.palette.secondary.light}
                  className={classes.toolbarIcon}
                />
                <Typography variant="body2">Add new Task</Typography>
              </div>
              <div
                onClick={event => this.handleAddClick(event, "addMileStone")}
                className={`${
                  classes.addMilestoneInnerCnt
                } flex_center_start_row`}
              >
                <AddIcon
                  htmlColor={theme.palette.secondary.light}
                  className={classes.toolbarIcon}
                />
                <Typography variant="body2">Add new Milestone </Typography>
              </div>
            </Fragment>
          )}
        </div>
        {/* Add Task End */}

        {/* Progress Start */}
        <div className={`${classes.progressCnt} flex_center_space_between_row`}>
          <div className="flex_center_start_col">
            <Typography variant="body2" className={classes.smallHeading}>
              completion
            </Typography>
            <Typography variant="h3" className={classes.greenCount}>
            {tasks.completion ? `${tasks.completion}%` : "0%"}
            </Typography>
          </div>
          <CheckIcon
            htmlColor={theme.palette.secondary.light}
            className={classes.toolbarIcon}
          />
        </div>
        {/* Progress End */}

        {/* Issue Starts */}
        <div className={`${classes.issuesCnt} flex_center_space_between_row`}>
          <div className="flex_center_start_col">
            <Typography variant="body2" className={classes.smallHeading}>
              issues
            </Typography>
            <Typography variant="h3" className={classes.redCount}>
              {tasks.issues}
            </Typography>
          </div>
          <IssuesIcon
            htmlColor={theme.palette.secondary.light}
            className={classes.toolbarIcon}
          />
        </div>
        {/* Issue Ends */}

        {/* Risk Starts */}
        <div className={`${classes.riskCnt} flex_center_space_between_row`}>
          <div className="flex_center_start_col">
            <Typography variant="body2" className={classes.smallHeading}>
              risks
            </Typography>
            <Typography variant="h3" className={classes.redCount}>
              {tasks.risks}
            </Typography>
          </div>
          <RiskIcon
            htmlColor={theme.palette.secondary.light}
            className={classes.toolbarIcon}
          />
        </div>
        {/* Risk Ends */}

        {/* Cost Starts */}
        <div className={`${classes.costCnt} flex_center_space_between_row`}>
          <div className="flex_center_start_col">
            <Typography variant="body2" className={classes.smallHeading}>
              cost
            </Typography>
            <Typography variant="h3" className={classes.greenCount}>
              {tasks.cost ? `$${tasks.cost}` : 0}
            </Typography>
          </div>
          <MoneyIcon
            htmlColor={theme.palette.secondary.light}
            className={classes.toolbarIcon}
          />
        </div>
        {/* Cost Ends */}

        {/* Zoom Starts */}
        <div className={`${classes.sliderCnt} flex_center_start_row`}>
          <Typography variant="body2" className={classes.sliderHeading}>
            zoom
          </Typography>
          <Slider
            value={zoom}
            min={0}
            max={2}
            step={1}
            onChange={this.handleZoomChange}
          />
        </div>
        {/* Zoom Ends */}
      </div>
    );
  }
}

export default withStyles(gantStyles, { withTheme: true })(Toolbar);
