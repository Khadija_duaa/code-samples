import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import cloneDeep from "lodash/cloneDeep";
import menuStyles from "../../../assets/jss/components/menu";
import CustomButton from "../../../components/Buttons/CustomButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import SvgIcon from "@material-ui/core/SvgIcon";
import SlackIcon from "../../../components/Icons/SlackIcon";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { Scrollbars } from "react-custom-scrollbars";
import { saveWorkspaceReportColumnOrder } from "../../../redux/actions/reports";

let total = 5;

class SlackChannelIntegration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "top-start",
      pickerPlacement: "",
      checked: [],
      isLoaded: false,
      loggedInTeam: "",
      isFirstLoad: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose(event) {
    this.setState({ open: false });
  }

  handleClick(event, placement) {
    event.stopPropagation();
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }

  componentDidMount() {
    const { task } = this.props;
    this.setState({ currentTask: task })
  }
  componentDidUpdate(prevProps, prevState) {
    const { task } = this.props;
    if (JSON.stringify(prevProps.task) !== JSON.stringify(task)) {
      this.setState({ currentTask: task })
    }
  }

  handleChannelClick = (e, slackItem) => {
    e.stopPropagation();
    const { currentTask } = this.state;
    let task = cloneDeep(currentTask);
    task.slackChannelId =
      currentTask.slackChannelId === slackItem.id ? null : slackItem.id;
    this.setState({ currentTask: task, open: false }, () => {
      this.props.updateTask("slackChannelId", currentTask.slackChannelId === slackItem.id ? "" : slackItem.id);
    })

  };
  //Function returns which channel is selected
  getIntegratedSlackChannel = () => {
    const { slackChannels = [] } = this.props;
    const { currentTask } = this.state;
    if (currentTask && currentTask.slackChannelId) {
      const channel = slackChannels.filter(
        channel => channel.id === currentTask.slackChannelId
      );
      return channel && channel.length ? channel[0].name : null
    }
    return "";
  };

  render() {
    const { classes, theme, slackChannels, task, isDisabled, customButtonProps = {} } = this.props;
    const { open, placement } = this.state;

    const selectedChannel = this.getIntegratedSlackChannel();

    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomButton
            variant="text"
            btnType="plain"
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            title={selectedChannel}
            buttonRef={node => {
              this.anchorEl = node;
            }}
            disabled={isDisabled}
            {...customButtonProps}
          >
            <SvgIcon viewBox="70 70 160 160" className={classes.slackIcon} style={{ opacity: isDisabled ? 0.4 : 1 }}>
              <SlackIcon />
            </SvgIcon>
            <span className={classes.slackSelectedChannel}>
              {selectedChannel}
            </span>
          </CustomButton>
          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            checkedType="multi"
            anchorRef={this.anchorEl}
            list={
              <Scrollbars autoHide style={{ height: 250 }}>
                <List>
                  <ListItem
                    disableRipple={true}
                    classes={{ root: classes.menuHeadingItem }}
                  >
                    <ListItemText
                      primary={`Select Channel`}
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {slackChannels.map((item, index) => (
                    <ListItem
                      key={index}
                      button
                      disableRipple={true}
                      className={`${classes.MenuItem} ${item.id === task.slackChannelId
                          ? classes.selectedValue
                          : ""
                        }`}
                      classes={{ selected: classes.statusMenuItemSelected }}
                      onClick={e => this.handleChannelClick(e, item)}
                    >
                      <ListItemText
                        primary={item.name}
                        classes={{
                          primary: classes.statusItemText
                        }}
                      />
                    </ListItem>
                  ))}
                </List>
              </Scrollbars>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    slackChannels: state.profile.data.slackChannel || []
  };
};

export default compose(
  withRouter,
  withStyles(menuStyles, {
    withTheme: true
  }),

  connect(
    mapStateToProps,
    { saveWorkspaceReportColumnOrder }
  )
)(SlackChannelIntegration);
