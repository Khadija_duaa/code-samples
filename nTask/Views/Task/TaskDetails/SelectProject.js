import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "react-select";
import { components } from "react-select";
import autoCompleteStyles from "../../../assets/jss/components/autoComplete";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import { UpdateTask, UpdateGantTask } from "../../../redux/actions/tasks";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import { taskDetailsHelpText } from "../../../components/Tooltip/helptext";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import ChevronDown from "@material-ui/icons/KeyboardArrowDown";
import Typography from "@material-ui/core/Typography";
import {FormattedMessage} from 'react-intl';
import TaskStatusChangeDialog from '../../../components/Templates/TaskStatusChangeDialog';
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";

class SelectProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AssignedToLabel: false,
      AddToProjectLabel: false,
      value: [],
      projectOptions: [],
      AssignEmailError: false,
      taskTitle: undefined,
      selectedProjectOption: null,
      openStatusDialog:false,
      newTemplateItem: null,
      oldStatusItem: null
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }
  DropdownIndicator = props => {
    const { theme, classes } = this.props;
    return (
      <components.DropdownIndicator {...props}>
        <ChevronDown
          className={classes.dropdownIndicatorIcon}
          htmlColor={theme.palette.secondary.medDark}
        />
      </components.DropdownIndicator>
    );
  };
  CustomOptionComponent = ({
    innerProps,
    innerRef,
    getStyles,
    children,
    data,
    ...props
  }) => {
    const { classes, theme, avatar, icon } = this.props;
    const optionStyles = getStyles("option", props);

    return (
      <div ref={innerRef} {...innerProps} style={optionStyles}>
        <Typography
          variant="h6"
          style={{ color: optionStyles.color }}
          className={classes.optionText}
        >
          <b>{data.uniqueId}</b> {data.uniqueId ? "-" : null} {children}
        </Typography>
      </div>
    );
  };
  componentDidMount() {
    let projectOptions =
      this.props.projectsState &&
      this.props.projectsState.data &&
      this.props.projectsState.data.length
        ? this.props.projectsState.data
        : [];

    let selectedProjectOption = null;
    if (this.props.projectId)
      selectedProjectOption = projectOptions.find(
        x => x.projectId === this.props.projectId
      );
    if (projectOptions.length)
      projectOptions.forEach(function(obj) {
        obj.Active = "false";
        obj.value = obj.projectName;
        obj.label = obj.projectName;
      });

    projectOptions = projectOptions || [];
    this.setState({ projectOptions, selectedProjectOption });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.projectId !== prevProps.projectId) {
      let selectedProjectOption = this.state.projectOptions.find(
        x => x.projectId === this.props.projectId
      );
      this.setState({ selectedProjectOption });
    }
    if (
      JSON.stringify(this.props.projectsState.data) !==
      JSON.stringify(prevProps.projectsState.data)
    ) {
      let projectOptions =
        this.props.projectsState &&
        this.props.projectsState.data &&
        this.props.projectsState.data.length
          ? this.props.projectsState.data
          : [];
      this.setState({ projectOptions });
    }
  }
  showMappingDialog = (projectId) => {
    const {workspaceStatus} = this.props;
    let attachedProject = this.props.projectsState.data.find(p => p.projectId == projectId);
    return attachedProject && attachedProject.projectTemplateId != workspaceStatus.templateId
  }
  getDefaultTemplateProject = (item) => {
    const {workspaceStatus} = this.props;
    if (item) {
      let attachedProject = this.props.projectsState.data.find(p => p.projectId == item.projectId);
      if (attachedProject && attachedProject.projectTemplate) {
        return attachedProject.projectTemplate;
      } else {
        return workspaceStatus;
      }
    } else {
      return workspaceStatus;
    }
  }
  // handleSelectChange1(newValue, actionMeta) {
  //   this.setState(
  //     {
  //       selectedProjectOption: newValue || null
  //     },
  //     () => {
  //       let updatedTask = { ...this.props.taskData };
  //       updatedTask.projectId = newValue ? newValue.projectId : "";
  //       if (this.props.isGantt) {
  //         this.props.UpdateGantTask(updatedTask, (err, response) => {
  //           if (response) {
  //             response = response.data.CalenderDetails;
  //           }
  //         });
  //       } else {
  //         this.props.UpdateTask(updatedTask, response => {
  //           // if (response.status === 200) {
  //           //   response = response.data.CalenderDetails;
  //           // }
  //         });
  //       }
  //       //
  //     }
  //   );
  // }

  handleSelectChange(newValue, actionMeta) {
    const projectId = newValue ? newValue.projectId : this.props.taskData.projectId;
    if(!this.showMappingDialog(projectId)){
      const newTaskObj = {
        ...this.props.taskData,
        projectId: newValue ? newValue.projectId : null,
      };
      this.props.UpdateTask(newTaskObj, () => {});
    }else{
    let taskStatus = this.props.taskData.taskStatus.statusList.find(el => el.statusId == this.props.taskData.status)
    let template = this.getDefaultTemplateProject(newValue);
    this.setState(
      {
        tempProject: newValue || null,
        openStatusDialog: true, 
        newTemplateItem: template,
        oldStatusItem: taskStatus
      });
    }
  }
  handleCloseDialog = () => {
    this.setState({openStatusDialog: false, newTemplateItem: null, oldStatusItem: null});
  }
  handleSaveTemplate = (newStatusItem) => {
    const {tempProject} = this.state;
    let updatedTask = { ...this.props.taskData };
    updatedTask.projectId = tempProject ? tempProject.projectId : null;
    updatedTask.status = newStatusItem.statusId;
    updatedTask.statusTitle = newStatusItem.statusTitle;
    this.setState(
      {
        selectedProjectOption: tempProject || null,
        openStatusDialog: false, 
        newTemplateItem: null,
        oldStatusItem: null
      },
      () => {
        if (this.props.isGantt) {
          this.props.UpdateGantTask(updatedTask, (err, response) => {
            if (response) {
              response = response.data.CalenderDetails;
            }
          });
        } else {
          this.props.UpdateTask(updatedTask, response => {
            // if (response.status === 200) {
            //   response = response.data.CalenderDetails;
            // }
          });
        }
        
      }
    );//selectedProjectOption
  }

  render() {
    const { classes, theme, permission, taskData, isDisabled } = this.props;
    const { openStatusDialog, newTemplateItem, oldStatusItem } = this.state;
    const DropdownIndicator = props => {
      return (
        components.DropdownIndicator && (
          <components.DropdownIndicator {...props}>
            <DropdownArrow
              htmlColor={theme.palette.secondary.light}
              fontSize="default"
            />
          </components.DropdownIndicator>
        )
      );
    };
    // Styles of react-select autocomplete
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: "2.5px 10px 2px 10px",
        fontSize: theme.typography.pxToRem(16),
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: "none",
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        }
      }),
      dropdownIndicator: (base, state) => ({
        padding: 0
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "5px 0"
      })
    };
    //some times task effort comes in string and some times empty array so to check that below line of code is added
    const taskEffort = taskData.userTaskEffort
      ? taskData.userTaskEffort.length
      : taskData.userTaskEffort;
    return (
      <Fragment>
        {openStatusDialog && (
          <TaskStatusChangeDialog
            open={openStatusDialog}
            handleClose={this.handleCloseDialog}
            oldStatus={oldStatusItem}
            newTemplateItem={newTemplateItem}
            handleSaveAsTemplate={this.handleSaveTemplate}
          />
        )}
        <CustomTooltip
          placement="right"
          helptext={<FormattedMessage id="task.detail-dialog.project.hint1" defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"/>}
        >
          <FormControl
            classes={{ root: classes.formControl }}
            fullWidth={true}
            style={{ marginBottom: 19 }}
          >
            <InputLabel
              htmlFor="AddToProject"
              classes={{
                root: classes.autoCompleteLabel,
                shrink: classes.shrinkLabel
              }}
              shrink={false}
            >
             <FormattedMessage id="task.detail-dialog.project.label"  defaultMessage="Select Project"/> 
              <CustomTooltip
                helptext={<FormattedMessage id="task.detail-dialog.project.hint" defaultMessage={taskDetailsHelpText.selectProjectHelpText}/>}
                iconType="help"
              />
            </InputLabel>
            <Select
              isClearable
              styles={customStyles}
              onChange={this.handleSelectChange}
              inputId="AddToProject"
              placeholder={<FormattedMessage id="task.detail-dialog.project.placeholder"  defaultMessage="Select Project"/>}
              isDisabled={isDisabled}
              components={{
                Option: this.CustomOptionComponent,
                DropdownIndicator: this.DropdownIndicator,
                IndicatorSeparator: () => {
                  return false;
                }
              }}
              options={this.state.projectOptions}
              value={this.state.selectedProjectOption}
              noOptionsMessage={() => {
                return "No Project";
              }}
            />
          </FormControl>
        </CustomTooltip>
        
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    projectsState: state.projects,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
  };
};

export default compose(
  withRouter,
  withStyles(autoCompleteStyles, {
    withTheme: true
  }),
  connect(
    mapStateToProps,
    { UpdateTask, UpdateCalenderTask, UpdateGantTask }
  )
)(SelectProject);
