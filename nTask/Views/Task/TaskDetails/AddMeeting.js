import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import AddMeetingForm from "../../AddNewForms/AddMeetingForm";
import Grid from "@material-ui/core/Grid";
import { Scrollbars } from "react-custom-scrollbars";
import Typography from "@material-ui/core/Typography";
import AlarmIcon from "@material-ui/icons/AlarmOn";
import ReOccureIcon from "@material-ui/icons/Autorenew";
import ErrorIcon from "@material-ui/icons/Error";
import CheckedIcon from "@material-ui/icons/CheckCircle";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import Edit from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import helper from "../../../helper";
import AddIcon from "@material-ui/icons/Add";
import SvgIcon from "@material-ui/core/SvgIcon";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import ReminderIcon from "../../../components/Icons/ReminderIcon";
import { canAdd } from "../permissions";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { DeleteMeeting, DeleteMeetingAgenda } from "../../../redux/actions/meetings";
import { GetPermission } from "../../../components/permissions";
import { FormattedMessage } from "react-intl";
import MeetingDetails from "../../../Views/Meeting/MeetingDetails/MeetingDetails";
import { UpdateMeeting } from "../../../redux/actions/meetings";
import isEqual from 'lodash/isEqual';
let showRecords = 10;
class AddMeeting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addMeeting: false,
      count: showRecords,
      openEditDialogue: false,
      openDeleteDialogue: false,
      deleteMeetingBtnQuery: "",
      selectedActionMeeting: null,
    };
    this.handleAddMeeting = this.handleAddMeeting.bind(this);
    this.handleCloseMeeting = this.handleCloseMeeting.bind(this);
    this.handleViewMore = this.handleViewMore.bind(this);
  }
  handleViewMore() {
    this.setState({ count: this.state.count + showRecords });
  }
  handleAddMeeting() {
    this.setState({ addMeeting: true });
  }
  handleCloseMeeting() {
    this.setState({
      addMeeting: false, 
      openEditDialogue: false,
      selectedActionMeeting: null,
    });
  }
  componentDidUpdate(prevProps, prevState) {
    const { storeMeetings } = this.props;
    if (!isEqual(prevProps.storeMeetings, storeMeetings) && this.state.meetingDialoge) {
      const updatedMeeting = this.props.storeMeetings.find(r => r.meetingId === this.state.meetingDialoge.meetingId);
      if (!updatedMeeting) {
        this.setState({ meetingDialoge: null });
      } else {
        const meetingWithPermissions = this.getMeetingPer(updatedMeeting);
        this.setState({ meetingDialoge: meetingWithPermissions });
      }
    }
  }
  updateMeetingAttendee = (assignedTo, meeting, add = false) => {
    let assignedToArr = assignedTo.map((ass) => {
      return ass.userId;
    });
    let newMeetingObj;
    if (add) {
      newMeetingObj = {
        ...meeting,
        attendeeList: [...meeting.attendeeList, ...assignedToArr],
        // attendeeList: assignedToArr /** added updated attendee id's to attendeelist (array)   */,
      };
    } else {
      newMeetingObj = {
        ...meeting,
        // attendeeList: [...meeting.attendeeList, ...assignedToArr]
        attendeeList: assignedToArr /** added updated attendee id's to attendeelist (array)   */,
      };
    }
    this.props.UpdateMeeting(newMeetingObj, () => {});
  };

  isUpdatedMeeting = data => {
    this.props.UpdateMeeting(data, (err, data) => { });
  };
  closeTaskDetailsPopUp = () => {
    this.setState({ meetingDialoge: false });
  };
  openEditDialogue = meeting => {
    this.setState({ openEditDialogue: true, selectedActionMeeting: meeting });
  };
  openDeleteDialogue = meeting => {
    this.setState({ openDeleteDialogue: true, selectedActionMeeting: meeting });
  };
  closeDeleteDialogue = () => {
    this.setState({ openDeleteDialogue: false });
  };
  openMeetingDetailsPopUp = (meeting) => {
    let updatedObject = this.getMeetingPer(meeting);
    this.setState({ meetingDialoge: updatedObject });
  };
  getMeetingPer = m => {
    const { projects, tasks, meetingPer } = this.props;

    if (m.taskId) {
      let attachedTask = tasks.find(t => t.taskId == m.taskId);
      if (attachedTask) {
        let attachedProject = projects.find(p => p.projectId == attachedTask.projectId);
        if (attachedProject) {
          m.meetingPermission = attachedProject.projectPermission.permission.meeting;
          return m;
        } else {
          m.meetingPermission = meetingPer;
          return m;
        }
      } else {
        m.meetingPermission = meetingPer;
        return m;
      }
    } else {
      m.meetingPermission = meetingPer;
      return m;
    }
  };
  deleteMeeting = () => {
    this.setState({ deleteMeetingBtnQuery: "progress" });
    this.props.DeleteMeeting(
      this.state.selectedActionMeeting.meetingId,
      response => {
        this.setState({
          openDeleteDialogue: false,
          selectedActionMeeting: null,
          deleteMeetingBtnQuery: "",
        });
      },
      error => {
        this.setState({
          openDeleteDialogue: false,
          selectedActionMeeting: null,
          deleteMeetingBtnQuery: "",
        });
      }
    );
  };

  render() {
    const {
      classes,
      theme,
      meetings,
      permissions,
      taskData,
      isArchivedSelected,
      taskPer,
      meetingPer,
    } = this.props;
    const intl = this.props.intl;
    const {
      addMeeting,
      openEditDialogue,
      count,
      openDeleteDialogue,
      deleteMeetingBtnQuery,
      selectedActionMeeting,
    } = this.state;
    const slicedMeetings = meetings.slice(0, count);
    const meetingCount = slicedMeetings.length;
    const deletePermission = taskData.meetingPermission ?  taskData.meetingPermission.deleteMeeting.cando : true;
    return (
      <>
      {this.state.meetingDialoge &&
        <MeetingDetails
          isUpdated={this.isUpdatedMeeting}
          closeMeetingDetailsPopUp={this.closeTaskDetailsPopUp}
          currentMeeting={this.state.meetingDialoge}
          meetingPer={this.props.meetingPer}
          closeActionMenu={this.closeTaskDetailsPopUp}
          updateMeetingAttendee={this.updateMeetingAttendee}
        />
      }
        <DeleteConfirmDialog
          open={openDeleteDialogue}
          closeAction={this.closeDeleteDialogue}
          cancelBtnText={intl.formatMessage({
            id: "common.action.cancel.label",
            defaultMessage: "Cancel",
          })}
          successBtnText={
            <FormattedMessage
              id="common.action.delete.meeting.messageb"
              defaultMessage="Delete meeting"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.delete.meeting.messageb"
              defaultMessage="Delete meeting"
            />
          }
          successAction={this.deleteMeeting}
          msgText={
            <FormattedMessage
              id="common.action.delete.meeting.message"
              defaultMessage="Are you sure you want to delete this meetings?"
            />
          }
          btnQuery={deleteMeetingBtnQuery}
        />
        <Scrollbars autoHide={false} style={{ height: "100%" }}>
          <div className={classes.tabInnerContentCnt}
        
          >
            {addMeeting || openEditDialogue ? (
              <div className={classes.tabContentFormCnt}>
                <AddMeetingForm
                  taskId={taskData.taskId}
                  currentMeeting={selectedActionMeeting}
                  handleCloseMeeting={this.handleCloseMeeting}
                  linkMeetingToTask={true}
                  maxLength={250}
                />
              </div>
            ) : meetingCount < 1 && meetingPer.createMeeting.cando ? (
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                classes={{ container: classes.tabContentGrid }}>
                <EmptyState
                  screenType="meeting"
                  heading={
                    <FormattedMessage
                      id="task.detail-dialog.no-meeting.label"
                      defaultMessage="No Meeting Scheduled"
                    />
                  }
                  style={{ width: "90%" }}
                  message={
                    !taskData.isDeleted && meetingPer.createMeeting.cando ? (
                      <FormattedMessage
                        id="task.detail-dialog.no-meeting.palceholder"
                        defaultMessage="Click on button below to add a meeting."
                      />
                    ) : (
                      ""
                    )
                  }
                  clickAction={this.handleAddMeeting}
                  button={!taskData.isDeleted && meetingPer.createMeeting.cando}
                />
              </Grid>
            ) : (
              <Fragment>
                {slicedMeetings.map(x => {
                  return (
                    <div className={classes.meetingTileCnt}
                    onClick={()=>{this.openMeetingDetailsPopUp(x)} }>
                      <div
                        className={`${classes.meetingTileContentCnt} flex_center_space_between_row`}>
                        <div className="flex_center_start_row">
                          <div className={`${classes.meetingDateCnt} flex_center_center_col`}>
                            {!x.startDateString ? (
                              <SvgIcon
                                viewBox="0 0 17.031 17"
                                htmlColor={theme.palette.secondary.medDark}>
                                <MeetingsIcon />
                              </SvgIcon>
                            ) : (
                              <>
                                <span className={classes.meetingDate}>
                                  {helper.RETURN_DAY_DATE(x.startDateString)}
                                </span>
                                <span clasName={classes.meetingMonth}>
                                  {helper.RETURN_MONTH_NAME(x.startDateString)}
                                </span>
                              </>
                            )}
                          </div>
                          <div>
                            {x.startDateString ? (
                              <Typography variant="body2">
                                {`${helper.RETURN_REMAINING_DAYS_DIFFERENCE(x.startDateString) === 1
                                  ? "Tomorrow"
                                  : helper.RETURN_REMAINING_DAYS_DIFFERENCE(x.startDateString) ===
                                    0
                                    ? "Today"
                                    : helper.RETURN_Day_Name(x.startDateString)
                                  }`}{" "}
                                {`${x.durationHours} hrs ${x.durationMins} mins`}{" "}
                              </Typography>
                            ) : (
                              <Typography variant="body2"> - </Typography>
                            )}
                            <Typography variant="h4">{x.meetingDisplayName}</Typography>
                            <Typography variant="body2">
                              {x.startTime} {x.location ? "at " + x.location : ""}
                            </Typography>
                          </div>
                        </div>

                        <div className="flex_center_start_row">
                          {x.status === "Cancelled" ? (
                            <span className={classes.cancelText}>CANCELLED</span>
                          ) : x.meetingFrequency !== "NoRecurrence" ? (
                            <span className={classes.meetingBorderedIcon}>
                              <SvgIcon
                                viewBox="0 0 14 12.438"
                                htmlColor={theme.palette.primary.light}
                                className={classes.recurrenceIcon}>
                                <RecurrenceIcon />
                              </SvgIcon>
                            </span>
                          ) : x.status === "Overdue" ? (
                            <ErrorIcon htmlColor={theme.palette.error.main} />
                          ) : x.status === "Published" ? (
                            <CheckedIcon htmlColor={theme.palette.primary.light} />
                          ) : x.meetingFrequency === "NoRecurrence" ? (
                            <span className={classes.meetingBorderedIcon}>
                              <SvgIcon viewBox="0 0 13.12 13.125" className={classes.reminderIcon}>
                                <ReminderIcon />
                              </SvgIcon>
                            </span>
                          ) : (
                            ""
                          )}
                          {/* <CustomIconButton
                                onClick={() => this.openEditDialogue(x)}
                                btnType="condensed"
                                disabled={isArchivedSelected || x.status == 'Published' ? true : false}
                              >
                                <Edit />
                              </CustomIconButton> */}
                          {deletePermission && (
                            <CustomIconButton
                              onClick={() => this.openDeleteDialogue(x)}
                              btnType="condensed"
                              disabled={
                                isArchivedSelected || x.status == "Published" ? true : false
                              }>
                              <Delete />
                            </CustomIconButton>
                          )}
                        </div>
                      </div>
                    </div>
                  );
                })}

                {meetingCount !== slicedMeetings.length ? (
                  <div className={`${classes.viewMoreCnt} flex_center_center_row`}>
                    <Typography variant="h5" onClick={this.handleViewMore}>
                      View More
                    </Typography>
                    <DropdownArrow htmlColor={theme.palette.secondary.medDark} />
                  </div>
                ) : null}
              </Fragment>
            )}
          </div>
        </Scrollbars>
        {!taskData.isDeleted && meetingPer.createMeeting.cando && meetingCount > 0 ? (
          <CustomIconButton
            onClick={this.handleAddMeeting}
            btnType="success"
            style={{ position: "absolute", right: 30, bottom: 30 }}>
            <AddIcon htmlColor={theme.palette.common.white} />
          </CustomIconButton>
        ) : null}
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    permissions: state.workspacePermissions.data.task,
    // meetingPer: state.workspacePermissions.data.meeting,
    tasks: state.tasks.data,
    projects: state.projects.data,
    storeMeetings: state.meetings.data,
  };
};

export default withRouter(
  connect(mapStateToProps, {
    DeleteMeeting,
    DeleteMeetingAgenda,
    UpdateMeeting
  })(AddMeeting)
);
