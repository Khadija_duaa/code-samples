import React, { Component, Fragment } from "react";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import Grid from "@material-ui/core/Grid";
import AddRiskForm from "../../AddNewForms/AddNewRiskForm";
import Typography from "@material-ui/core/Typography";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import AddIcon from "@material-ui/icons/Add";
import { Scrollbars } from "react-custom-scrollbars";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import { UpdateRisk } from "../../../redux/actions/risks";
import { compose } from "redux";
import RiskDetails from "../../Risk/RiskDetails/RiskDetails";
import isEqual from "lodash/isEqual";
import {UpdateStoreTasks } from "../../../redux/actions/workspace";
import { riskDetailDialog } from "../../../redux/actions/allDialogs";

let showRecords = 10;
class AddRisk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addRisk: false,
      count: showRecords,
    };
    this.handleCreateRisk = this.handleCreateRisk.bind(this);
    this.handleViewMore = this.handleViewMore.bind(this);
  }
  handleViewMore() {
    this.setState({ count: this.state.count + showRecords });
  }
  handleCreateRisk() {
    this.setState({ addRisk: true });
  }
  disableCreateRisk = () => {
    this.setState({ addRisk: false });
  };
  componentDidUpdate(prevProps, prevState) {
    const { risksState } = this.props;

    if (!isEqual(prevProps.risksState.data, risksState.data) && this.state.riskDialog) {
      const updatedRisk = this.props.risksState.data.find(
        r => r.id === this.state.riskDialog.id
      );
      if (!updatedRisk) {
        this.setState({ riskDialog: null });
      } else {
        const riskWithPermissions = this.getRiskPer(updatedRisk);
        this.setState({ riskDialog: riskWithPermissions });
      }
    }
  }

  translate = value => {
    let id = "";
    switch (value) {
      case "Identified":
        id = "risk.common.status.dropdown.identified";
        break;
      case "In Review":
        id = "risk.common.status.dropdown.in-review";
        break;
      case "Agreed":
        id = "risk.common.status.dropdown.agreed";
        break;
      case "Rejected":
        id = "risk.common.status.dropdown.rejected";
        break;
      case "Moderate":
        id = "risk.common.impact.dropdown.moderate";
        break;
      case "Minor":
        id = "risk.common.impact.dropdown.minor";
        break;
      case "Major":
        id = "risk.common.impact.dropdown.major";
        break;
      case "Critical":
        id = "risk.common.impact.dropdown.critical";
        break;
    }
    return id == "" ? value : this.props.intl.formatMessage({ id: id, defaultMessage: value });
  };
  isUpdatedRisk = (data, callback) => {
    this.props.UpdateRisk(
      data,
      data => {
        if (callback) callback();
      },
      error => {
        if (error && error.status === 500) {
        }
      }
    );
  };
  closeTaskDetailsPopUp = obj => {
    this.setState({ riskDialog: null });
  };
  openTaskDetailsPopUp = risk => {
    let updatedObject = this.getRiskPer(risk);
    this.setState({ riskDialog: updatedObject });
  };
  getRiskPer = r => {
    const { projects, tasks, riskPer } = this.props;
    if (r.linkedTasks.length) {
      let attachedTask = tasks.find(t => t.taskId == r.linkedTasks[0]);
      if (attachedTask) {
        let attachedProject = projects.find(p => p.projectId == attachedTask.projectId);
        if (attachedProject) {
          r.riskPermission = attachedProject.projectPermission.permission.risk;
          return r;
        } else {
          r.riskPermission = riskPer;
          return r;
        }
      } else {
        r.riskPermission = riskPer;
        return r;
      }
    } else {
      r.riskPermission = riskPer;
      return r;
    }
  };
  render() {
    const { classes, theme, currentTask, risksState, taskPer, riskPer, data } = this.props;
    const { addRisk, count } = this.state;
    const status = "Identified"; // Remove this after making it dynamic
    const likelihood = "26-50%"; // Remove this after making it dynamic
    const riskImpact = "Critical"; // Remove this after making it dynamic
    let allRisks = risksState.data ? risksState.data : [];
    allRisks = allRisks.filter(x => x.linkedTasks.indexOf(currentTask.taskId) >= 0 && !x.isArchive);
    const riskCount = allRisks.length;
    let riskData = allRisks.slice(0, count);
    const color = theme.palette;
    const riskStatus = {
      Identified: color.riskStatus.Identified,
      "In Review": color.riskStatus.InReview,
      Agreed: color.riskStatus.Agreed,
      Rejected: color.riskStatus.Rejected,
    };
    const impact = {
      Minor: color.riskImpact.Minor,
      Moderate: color.riskImpact.Moderate,
      Major: color.riskImpact.Major,
      Critical: color.riskImpact.Critical,
    };

    return (
      <>
        {/* {this.state.riskDialog && (
          <RiskDetails
            isUpdated={this.isUpdatedRisk}
            closeRiskDetailsPopUp={this.closeTaskDetailsPopUp}
            currentRisk={this.state.riskDialog}
            riskPer={this.props.riskPer}
            closeActionMenu={this.closeTaskDetailsPopUp}
          />
        )} */}
        <Scrollbars autoHide={false} style={{ height: "100%" }}>
          <div className={classes.tabInnerContentCnt}>
            {addRisk ? (
              <div className={classes.tabContentFormCnt}>
                <AddRiskForm
                  currentTask={this.props.currentTask}
                  disableCreateRisk={this.disableCreateRisk}
                  view={"Task-Risk"}
                />
              </div>
            ) : riskCount < 1 && this.props.riskPer.createRisk.cando ? (
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                classes={{ container: classes.tabContentGrid }}>
                <EmptyState
                  screenType="risk"
                  heading={
                    <FormattedMessage
                      id="task.detail-dialog.no-risk.label"
                      defaultMessage="No Risk Created Yet"
                    />
                  }
                  style={{ width: "90%" }}
                  message={
                    !currentTask.isDeleted && this.props.riskPer.createRisk.cando ? (
                      <FormattedMessage
                        id="task.detail-dialog.no-risk.placeholder"
                        defaultMessage="Click on button below to add a risk."
                      />
                    ) : (
                      ""
                    )
                  }
                  clickAction={this.handleCreateRisk}
                  button={!currentTask.isDeleted && this.props.riskPer.createRisk.cando}
                />
              </Grid>
            ) : (
              <Fragment>
                <div>
                  {riskData.map(risk => {
                    return (
                      <div
                        key={risk.riskId}
                        className={classes.issueTileCnt}
                        onClick={() => {
                          // this.openTaskDetailsPopUp(risk);
                          this.props.riskDetailDialog({id : risk.id})
                        }}>
                        <div
                          className={`${classes.issueTileContentCnt} flex_center_space_between_row`}>
                          <div>
                            <Typography variant="body2">{risk.riskId}</Typography>
                            <Typography variant="h4">{risk.title}</Typography>
                          </div>
                          <span
                            className={classes.statusText}
                            style={{ background: riskStatus[risk.status] }}>
                            {this.translate(risk.status)}
                          </span>
                        </div>
                        <div className={`${classes.issueTileFooter} flex_center_space_between_row`}>
                          <div className="flex_center_start_row">
                            <Typography variant="body2" classes={{ body2: classes.footerLabel }}>
                              <FormattedMessage
                                id="risk.common.likelihood.label"
                                defaultMessage="Likelihood"
                              />
                              :
                            </Typography>
                            <Typography variant="h6">
                              {risk.likelihood ? risk.likelihood : "0"}%
                            </Typography>
                          </div>
                          {risk.impact ? (
                            <div className="flex_center_start_row">
                              <Typography variant="body2" classes={{ body2: classes.footerLabel }}>
                                <FormattedMessage
                                  id="risk.creation-dialog.form.impact.label"
                                  defaultMessage="Impact"
                                />
                                :
                              </Typography>
                              <RingIcon
                                className={classes.footerIcon}
                                htmlColor={impact[risk.impact]}
                              />
                              <Typography variant="h6">{this.translate(risk.impact)}</Typography>
                            </div>
                          ) : null}
                        </div>
                      </div>
                    );
                  })}
                </div>
                {riskCount !== riskData.length ? (
                  <div className={`${classes.viewMoreCnt} flex_center_center_row`}>
                    <Typography variant="h5" onClick={this.handleViewMore}>
                      View More
                    </Typography>
                    <DropdownArrow htmlColor={theme.palette.secondary.medDark} />
                  </div>
                ) : null}
              </Fragment>
            )}
          </div>
        </Scrollbars>
        {!currentTask.isDeleted && this.props.riskPer.createRisk.cando && riskCount > 0 ? (
          <CustomIconButton
            onClick={this.handleCreateRisk}
            btnType="success"
            style={{ position: "absolute", right: 30, bottom: 30 }}>
            <AddIcon htmlColor={theme.palette.common.white} />
          </CustomIconButton>
        ) : null}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    risksState: state.risks,
    // riskPer: state.workspacePermissions.data.risk,
    tasks: state.tasks.data,
    projects: state.projects.data,
  };
};

//export default AddRisk;

export default withRouter(connect(mapStateToProps, { UpdateRisk, riskDetailDialog })(AddRisk));
