import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import AddIssueForm from "../../AddNewForms/AddNewIssueForm";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import FlagIcon from "@material-ui/icons/Flag";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
import ErrorIcon from "@material-ui/icons/Error";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import AddIcon from "@material-ui/icons/Add";
import { Scrollbars } from "react-custom-scrollbars";
import helper from "../../../helper";
import { FormattedMessage } from 'react-intl';
import IssueDetails from "../../../Views/Issue/IssueDetails/IssueDetails";
import { UpdateIssue } from "../../../redux/actions/issues";
import { issueDetailDialogState } from "../../../redux/actions/allDialogs";
import isEqual from "lodash/isEqual";
let showRecords = 10;
class AddIssue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addIssue: false,
      count: showRecords,
    };
    this.handleCreateIssue = this.handleCreateIssue.bind(this);
    this.handleViewMore = this.handleViewMore.bind(this);
  }
  handleViewMore() {
    this.setState({ count: this.state.count + showRecords });
  }
  handleCreateIssue() {
    this.setState({ addIssue: true });
  }
  disableCreateIssue = () => {
    this.setState({ addIssue: false });
  };
  isUpdatedIssue = (data, callback) => {
    this.props.UpdateIssue(
      data,
      data => {
        if (callback) callback();
        //
      },
      error => {
        if (error && error.status === 500) {
          this.props.handleExportType("Server throws error", "error");
        }
      }
    );
  };
  closeTaskDetailsPopUp = (issue) => {
    this.setState({ issueDetail: null });
  };
  openIsssueDetailsPopUp = (issue) => {
    let updatedObject = this.getIssuePer(issue);
    this.setState({ issueDetail: updatedObject },()=>{
      this.closeTaskDetailsPopUp();
    });
  };
  componentDidUpdate(prevProps, prevState) {
    const { issues } = this.props;
    if (!isEqual(prevProps.issues, issues) && this.state.issueDetail) {
      const updatedIssue = this.props.issues.find(r => r.issueId === this.state.issueDetail.issueId);
      if (!updatedIssue) {
        this.setState({ issueDetail: null });
      } else {
        const issueWithPermissions = this.getIssuePer(updatedIssue);
        this.setState({ issueDetail: issueWithPermissions });
      }
    }
  }
  getIssuePer = i => {
    const { projects, tasks, issuePer } = this.props;
    if (i.linkedTasks && i.linkedTasks.length) {
      let attachedTask = tasks.find(t => t.taskId == i.linkedTasks[0]);
      if (attachedTask) {
        let attachedProject = projects.find(p => p.projectId == attachedTask.projectId);
        if (attachedProject) {
          i.issuePermission = attachedProject.projectPermission.permission.issue;
          return i;
        } else {
          i.issuePermission = issuePer;
          return i;
        }
      } else {
        i.issuePermission = issuePer;
        return i;
      }
    } else {
      i.issuePermission = issuePer;
      return i;
    }
  };

  render() {
    const { classes, theme, currentTask, issuesState, taskPer, issuePer, intl } = this.props;
    const { addIssue, count } = this.state; // Remove issueCount when its dynamic
    const color = theme.palette;
    let allIssues = issuesState.data ? issuesState.data : [];
    allIssues = allIssues.filter(
      (x) => x.linkedTasks && x.linkedTasks.indexOf(currentTask.taskId) >= 0 && !x.isArchive
    );
    const issueCount = allIssues.length;
    let issueData = allIssues.slice(0, count);
    const statusColor = {
      Open: color.issueStatus.Open,
      Closed: color.issueStatus.Closed,
    };
    const piriorityColor = {
      Critical: color.issuePriority.Critical,
      High: color.issuePriority.High,
      Medium: color.issuePriority.Medium,
      Low: color.issuePriority.Low,
    };
    const severityColor = {
      Minor: color.severity.minor,
      Moderate: color.severity.moderate,
      Major: color.severity.major,
      Critical: color.severity.critical,
    };
    const translate = (text) => {
      let id = "";
      switch (text) {
        case "Open":
          id = "issue.common.status.drop-down.open";
          break;
        case "Closed":
          id = "issue.common.status.drop-down.close";
          break;
        case "Critical":
          id = "issue.common.priority.dropdown.critical";
          break;
        case "High":
          id = "issue.common.priority.dropdown.high";
          break;
        case "Medium":
          id = "issue.common.priority.dropdown.medium";
          break;
        case "Low":
          id = "issue.common.priority.dropdown.low";
          break;
        case "Minor":
          id = "issue.common.priority.dropdown.low";
          break;
        case "Moderate":
          id = "issue.common.priority.dropdown.low";
          break;
        case "Major":
          id = "issue.common.priority.dropdown.low";
          break;
        case "Critical":
          id = "issue.common.priority.dropdown.low";
          break;
      }
      return id == "" ? text : intl.formatMessage({ id: id, defaultMessage: text });
    };
    return (
      <>
        {this.state.issueDetail &&
          // <IssueDetails
          //   isUpdated={this.isUpdatedIssue}
          //   closeIssueDetailsPopUp={this.closeTaskDetailsPopUp}
          //   currentIssue={this.state.issueDetail}
          //   closeActionMenu={this.closeTaskDetailsPopUp}
          //   issuePer={this.props.issuePer}
          // />
          this.props.issueDetailDialogState(null,{
            id: this.state.issueDetail.id,
            afterCloseCallBack: () => {
              this.closeTaskDetailsPopUp();
            },
            isUpdated: this.isUpdatedIssue,
            closeActionMenu: this.closeTaskDetailsPopUp
          })
          }
        <Scrollbars autoHide={false} style={{ height: "100%" }}>
          <div className={classes.tabInnerContentCnt}>
            {addIssue ? (
              <div className={classes.tabContentFormCnt}>
                <AddIssueForm
                  disableCreateIssue={this.disableCreateIssue}
                  currentTask={this.props.currentTask}
                  isDialog={false}
                  view={"Task-Issue"}
                />
              </div>
            ) : issueCount < 1 && issuePer.creatIssue.cando ? (
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                classes={{ container: classes.tabContentGrid }}
              >
                <EmptyState
                  screenType="issue"
                  style={{ width: "90%" }}
                  heading={<FormattedMessage id="task.detail-dialog.no-issue.label" defaultMessage="No Issue Created Yet" />}
                  message={
                    !currentTask.isDeleted && issuePer.creatIssue.cando
                      ? <FormattedMessage id="task.detail-dialog.no-issue.placeholder" defaultMessage="Click on button below to add an issue." />
                      : ""
                  }
                  clickAction={this.handleCreateIssue}
                  button={
                    !currentTask.isDeleted && issuePer.creatIssue.cando
                  }
                />
              </Grid>
            ) : (
              <Fragment>
                <React.Fragment>
                  {issueData.map((issue) => {
                    return (
                      <div key={issue.issueId} className={classes.issueTileCnt}
                        onClick={() => { this.openIsssueDetailsPopUp(issue) }}
                      >
                        <div
                          className={`${classes.issueTileContentCnt} flex_center_space_between_row`}
                        >
                          <div>
                            <Typography variant="body2">
                              {issue.issueId}
                            </Typography>
                            <Typography variant="h4">{issue.title}</Typography>
                          </div>
                          <span
                            className={classes.statusText}
                            style={{ background: statusColor[issue.status] }}
                          >
                            {translate(issue.status)}
                          </span>
                        </div>
                        <div
                          className={`${classes.issueTileFooter} flex_center_space_between_row`}
                        >
                          <div className="flex_center_start_row">
                            <Typography
                              variant="body2"
                              classes={{ body2: classes.footerLabel }}
                            >
                              <FormattedMessage id="issue.common.priority.label" defaultMessage="Priority" />:
                            </Typography>
                            <FlagIcon
                              className={classes.footerIcon}
                              htmlColor={piriorityColor[issue.priority]}
                            />
                            <Typography variant="h6">
                              {translate(issue.priority)}
                            </Typography>
                          </div>

                          <div className="flex_center_start_row">
                            {issue.severity ? (
                              <Fragment>
                                <Typography
                                  variant="body2"
                                  classes={{ body2: classes.footerLabel }}
                                >
                                  <FormattedMessage id="issue.detail-dialog.severity.label" defaultMessage="Severity" />:
                                </Typography>
                                <RingIcon
                                  className={classes.footerIcon}
                                  htmlColor={severityColor[issue.severity]}
                                />
                                <Typography variant="h6">
                                  {translate(issue.severity)}
                                </Typography>
                              </Fragment>
                            ) : null}
                          </div>
                          {issue.actualDueDateString ? (
                            <div className="flex_center_start_row">
                              <Typography
                                variant="body2"
                                classes={{ body2: classes.footerLabel }}
                              >
                                Due:
                              </Typography>

                              <Fragment>
                                {helper.RETURN_REMAINING_DAYS_DIFFERENCE(
                                  issue.actualDueDateString
                                ) < 0 ? (
                                  <ErrorIcon
                                    className={classes.footerIcon}
                                    htmlColor={theme.palette.error.light}
                                  />
                                ) : null}
                              </Fragment>

                              {issue.actualDueDateString ? (
                                <Typography variant="h6">
                                  {helper.RETURN_REMAINING_DAYS_STATUS(
                                    issue.actualDueDateString
                                  )}
                                </Typography>
                              ) : (
                                "-"
                              )}
                            </div>
                          ) : null}
                        </div>
                      </div>
                    );
                  })}
                </React.Fragment>
                {issueCount !== issueData.length ? (
                  <div
                    className={`${classes.viewMoreCnt} flex_center_center_row`}
                  >
                    <Typography variant="h5" onClick={this.handleViewMore}>
                      View More
                    </Typography>
                    <DropdownArrow
                      htmlColor={theme.palette.secondary.medDark}
                    />
                  </div>
                ) : null}
              </Fragment>
            )}
          </div>
        </Scrollbars>
        {!currentTask.isDeleted && issuePer.creatIssue.cando && issueCount > 0 ? (
          <CustomIconButton
            onClick={this.handleCreateIssue}
            btnType="success"
            style={{ position: "absolute", right: 30, bottom: 30 }}
          >
            <AddIcon htmlColor={theme.palette.common.white} />
          </CustomIconButton>
        ) : null}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    issuesState: state.issues,
    // issuePer: state.workspacePermissions.data.issue,
    tasks: state.tasks.data,
    projects: state.projects.data,
    issues: state.issues.data,

  };
};

export default withRouter(connect(mapStateToProps, { UpdateIssue, issueDetailDialogState })(AddIssue));
