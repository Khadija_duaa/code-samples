import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../../components/Buttons/IconButton";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import taskDetailsStyles from "./styles";
import CommentsTab from "./CommentsTab";
import { Scrollbars } from "react-custom-scrollbars";
import AddIssue from "./AddIssue";
import AddRisk from "./AddRisk";
import AddMeeting from "./AddMeeting";
import ActivityList from "./TaskActivity";
import Typography from "@material-ui/core/Typography";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import { FormattedMessage, injectIntl } from "react-intl";
import ChatUpdates from "../../../components/Chat/ChatUpdates";
import Documents from "../../../components/Documents/Documents";
import {
  clearDocuments
} from "../../../redux/actions/documents";
import { getCustomFields } from "../../../helper/customFieldsData";
import { CanAccess } from "../../../components/AccessFeature/AccessFeature.cmp";

class SideTabs extends React.Component {
  state = {
    value: 0,
    showActivity: false,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };
  componentDidMount() {
    const { type } = this.props;
    const { value } = this.state;
    let currenTab = value;
    if (type === "meeting") {
      currenTab = 2;
    } else if (type === "issue") {
      currenTab = 3;
    } else if (type === "risk") {
      currenTab = 4;
    } else {
      currenTab = value;
    }
    this.setState({ value: currenTab });
  }
  render() {
    const {
      classes,
      theme,
      issuesState,
      risksState,
      meetingsState,
      taskData,
      isArchivedSelected,
      taskPer,
      intl,
      allMembers,
      customFields,
      profile,
      meetingPermission,
      issuePermission,
      riskPermission,
      companyInfo
    } = this.props;
    const { value } = this.state;
    let allIssues = issuesState.data ? issuesState.data : [];
    allIssues = allIssues.filter(x => x.linkedTasks && x.linkedTasks.indexOf(taskData.taskId) >= 0 && !x.isArchive);
    let allRisks = risksState.data ? risksState.data : [];
    allRisks = allRisks.filter(x => x.linkedTasks.indexOf(taskData.taskId) >= 0 && !x.isArchive);
    let allMeetings = meetingsState.data ? meetingsState.data : [];
    allMeetings = allMeetings.filter(x => {
      if (x.taskId) {
        return x.taskId == taskData.taskId && !x.isArchive;
      }
    });
    const issueCount = allIssues.length;
    const meetingCount = allMeetings.length;
    const riskCount = allRisks.length;

    // let permissionAttachment = getCompletePermissionsWithArchieve(
    //   taskData,
    //   this.props.permission,
    //   "attachment"
    // );
    const deleteAttachment = taskPer.taskDetail
      ? taskPer.taskDetail.taskComments.taskattachment.isAllowDelete
      : true;
    // permissionAttachment === true ? true : permissionAttachment.delete;
    let customFieldArrRisk = getCustomFields(customFields, profile, "risk").filter(
      f => f.fieldType == "dropdown"
    );
    let customFieldArrIssue = getCustomFields(customFields, profile, "issue").filter(
      f => f.fieldType == "dropdown"
    );
    const taskDropdownRisk = CanAccess({ group: 'risk', feature: 'tasks' });
    const taskDropdownIssue = CanAccess({ group: 'issue', feature: 'tasks' });
    const meetingAccess = companyInfo?.meeting?.isHidden ? false : true
    const issueAccess = companyInfo?.issue?.isHidden ? false : true
    const riskAccess = companyInfo?.risk?.isHidden ? false : true

    return (
      <Fragment>
        {!this.props.showActivity ? (
          <Tabs
            value={value}
            // action={obj => {
            //   setTimeout(() => {
            //     obj.updateIndicator();
            //   }, 500);
            // }}
            onChange={this.handleChange}
            variant="fullWidth"
            classes={{
              root: classes.TabsRoot,
              indicator: classes.tabIndicator,
            }}>
            <Tab
              value={0}
              disableRipple={true}
              classes={{
                root: classes.tab,
                wrapper: classes.tabLabelCnt,
                selected: classes.selectedTab,
              }}
              label={<FormattedMessage id="common.comment.label" defaultMessage="Comments" />}
            />
            <Tab
              value={1}
              disableRipple={true}
              classes={{
                root: classes.tab,
                wrapper:  classes.tabLabelCnt,
                selected: classes.selectedTab,
              }}
              label={
                <FormattedMessage id="project.dev.documents.label" defaultMessage="Documents" />
              }
            />
            {meetingAccess && (
              <Tab
                value={2}
                disableRipple={true}
                classes={{
                  root: classes.tab,
                  wrapper:  classes.tabLabelCnt,
                  selected: classes.selectedTab,
                }}
                label={
                  <Typography variant="h6">
                    <FormattedMessage id="meeting.label" defaultMessage="Meetings" />{" "}
                    {meetingCount > 0 ? <span className={classes.count}>{meetingCount}</span> : null}
                  </Typography>
                }
              />
            )}
            {issueAccess && taskDropdownIssue && (
              <Tab
                value={3}
                disableRipple={true}
                classes={{
                  root: classes.tab,
                  wrapper:  classes.tabLabelCnt,
                  selected: classes.selectedTab,
                }}
                label={
                  <Typography variant="h6">
                    <FormattedMessage id="issue.label" defaultMessage="Issues" />{" "}
                    {issueCount > 0 ? <span className={classes.count}>{issueCount}</span> : null}
                  </Typography>
                }
              />
            )}
            {teamCanView("riskAccess") && riskAccess && taskDropdownRisk && (
              <Tab
                value={4}
                disableRipple={true}
                classes={{
                  root: classes.tab,
                  wrapper:  classes.tabLabelCnt,
                  selected: classes.selectedTab,
                }}
                label={
                  <Typography variant="h6">
                    <FormattedMessage id="risk.label" defaultMessage="Risks" />{" "}
                    {riskCount > 0 ? <span className={classes.count}>{riskCount}</span> : null}
                  </Typography>
                }
              />
            )}
          </Tabs>
        ) : (
          <div className={classes.activityIconCnt}>
            <IconButton
              btnType="transparent"
              onClick={this.props.hideActivityLog}
              style={{ margin: "0 5px" }}>
              <CloseIcon
                htmlColor={theme.palette.secondary.medDark}
                className={classes.closeIconActivity}
              />
            </IconButton>
            <Tab
              disableRipple={true}
              classes={{
                root: classes.tab,
                wrapper:  classes.tabLabelCnt,
                selected: classes.selectedTab,
              }}
              label="Activity"
            />
          </div>
        )}
        {value === 0 && !this.props.showActivity && (
          <div className={`${classes.TabContentCnt} ${classes.commentsTabCnt}`}>
            <ChatUpdates
              onMount={this.props.onChatMount}
              onUnmount={this.props.onChatUnMount}
              chatConfig={this.props.chatConfig}
              chatPermission={this.props.chatPermission}
              selectedTab={0}
              intl={intl}
            />
          </div>
        )}
        {value === 1 && !this.props.showActivity && (
          <div className={`${classes.TabContentCnt} ${classes.commentsTabCnt}`}>
            <Documents
              onMount={this.props.onChatMount}
              onUnmount={this.props.onChatUnMount}
              docConfig={this.props.docConfig}
              docPermission={this.props.docPermission}
              intl={intl}
            />
          </div>
        )}
        {this.props.showActivity && (
          <div className={classes.TabContentCntActivity}>
            <Scrollbars style={{ height: "100%" }}>
              <ActivityList
                classes={classes}
                MenuData={this.props.taskData}
                theme={theme}
                members={this.props.members}
                taskActivities={
                  this.props.taskActivitiesState ? this.props.taskActivitiesState : []
                }
                allMembers={allMembers}
                intl={intl}
              />
            </Scrollbars>
          </div>
        )}
        {value === 2 && !this.props.showActivity && (
          <div className={classes.TabContentCnt}>
            <AddMeeting
              theme={theme}
              classes={classes}
              isArchivedSelected={isArchivedSelected}
              permission={this.props.permission}
              taskData={this.props.taskData}
              meetings={allMeetings}
              taskPer={taskPer}
              meetingPer={meetingPermission}
              intl={intl}
            />
          </div>
        )}
        {value === 3 && !this.props.showActivity && taskDropdownIssue && (
          <div className={classes.TabContentCnt}>
            <AddIssue
              classes={classes}
              currentTask={this.props.taskData}
              theme={theme}
              taskPer={taskPer}
              issuePer={issuePermission}
              intl={intl}
            />
          </div>
        )}
        {value === 4 && !this.props.showActivity && taskDropdownRisk && (
          <div className={classes.TabContentCnt}>
            <AddRisk
              classes={classes}
              currentTask={this.props.taskData}
              taskPer={taskPer}
              theme={theme}
              riskPer={riskPermission}
              intl={intl}
            />
          </div>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    issuesState: state.issues,
    risksState: state.risks,
    meetingsState: state.meetings,
    taskActivitiesState: state.taskActivities,
    allMembers: state.profile.data.member.allMembers,
    customFields: state.customFields,
    profile: state.profile.data,
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  injectIntl,
  connect(mapStateToProps, { clearDocuments }),
  withStyles(taskDetailsStyles, { withTheme: true })
)(SideTabs);
