import React, { Component } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import SvgIcon from "@material-ui/core/SvgIcon";
import IconButton from "../../../components/Buttons/IconButton";
import taskDetailStyles from "./styles";
import SelectProject from "./SelectProject";
import SideTabs from "./SideTabs";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import TaskDescriptionEditor from "./TaskDetailsDescription";
import TodoList from "../../../components/TodoList/TodoList";
import { getGanttTasks } from "../../../redux/actions/gantt";
import Timer from "../../../components/Timer/Timer";
import TaskDetailsActionDropDown from "./TaskDetailsActionDropDown";
import SlackChannelIntegration from "./SlackChannelIntegration";
import helper from "../../../helper";
import { UpdateTaskColorFromGridItem } from "../../../redux/actions/workspace";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import RepeatTask from "../RepeatTask/RepeatTask";
import CustomIconButton from "../../../components/Buttons/IconButton";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import { setGlobalTaskTime, removeGlobalTaskTime } from "../../../redux/actions/globalTimerTask";
import { dispatchAddTodoItems } from "../../../redux/actions/todoItems";
import CustomRangeDatePicker from "../../../components/DatePicker/CustomRangeDatePicker";
import { generateUsername, calculateHeight } from "../../../utils/common";
import {
  UpdateTask,
  FetchTasksInfo,
  CopyTask,
  DeleteTask,
  ArchiveTask,
  UnArchiveTask,
  startTimer,
  stopTimer,
  removeUserEffort,
  UpdateGantTask,
  MarkTaskNotificationsAsRead,
  getTaskComments,
  removeTaskComments,
  CheckAllTodoList,
  UpdateTaskCustomField,
  dispatchTask,
  bulkUpdateTask,
} from "../../../redux/actions/tasks";
import NotificationMenu from "../../../components/Header/NotificationsMenu";
import { UpdateCalenderTask, CopyCalenderTask } from "../../../redux/actions/calenderTasks";
import { getTasksPermissions, getTasksPermissionsWithoutArchieve } from "../permissions";
import { priorityData } from "../../../helper/issueDropdownData";
import CustomButton from "../../../components/Buttons/CustomButton";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { GetPermission } from "../../../components/permissions";
import { injectIntl, FormattedMessage } from "react-intl";
import CircularIcon from "@material-ui/icons/Brightness1";
import { clearDocuments } from "../../../redux/actions/documents";
import moment from "moment";
import CustomFieldView from "../../CustomFieldSettings/CustomFields.view";
import { editCustomField, updateCustomFieldData, hideCustomField } from "../../../redux/actions/customFields";
import { updateCustomFieldDialogState } from "../../../redux/actions/allDialogs";
import SectionsView from "../../../components/Sections/sections.view";

const priorities = ["", "Critical", "High", "Medium", "Low"];
class TaskDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      name: [],
      editTaskName: false,
      taskName: "Task No.1",
      currentTask: this.getTaskAssignees(this.props.currentTask),
      currentTaskOriginal: {},
      currentUser: props.profileState.data.userId,
      changeStatus: false,
      changeStatus: false,
      validateTaskName: "",
      taskNameError: false,
      showActivity: false,
      readAll: true,
      repeatDropdown: false,
      unArchiveFlag: false,
      unarchiveBtnQuery: "",
      markChecklistActionConf: false,
      markChecklistActionBtnQuery: "",
      // EditMode: false
      // comments: this.props.taskTodoItems,
    };
    this.handleTaskNameEdit = this.handleTaskNameEdit.bind(this);
    this.handleTaskNameSave = this.handleTaskNameSave.bind(this);
    this.handleTaskNameInput = this.handleTaskNameInput.bind(this);
    this.statusChanged = this.statusChanged.bind(this);
    this.clearStatusChanged = this.clearStatusChanged.bind(this);
    this.activityLog = this.activityLog.bind(this);
    this.hideActivityLog = this.hideActivityLog.bind(this);
    this.handleAllRead = this.handleAllRead.bind(this);
  }

  componentDidMount() {
    const { taskTodoItems } = this.props;
    this.setState(
      {
        currentTask: this.props.currentTask ? this.getTaskAssignees(this.props.currentTask) : {},
        taskNameOriginal: this.props.currentTask.taskTitle,
        taskName: this.props.currentTask.taskTitle,
      },
      () => {
        this.props.getTaskComments(
          {
            taskId: this.props.currentTask.taskId,
            todoTaskId: this.props.currentTask.id,
          },
          response => {
            if (response && response.status === 200) {
              // this.setState({ comments: response.data.toDoList });
            }
          },
          error => {
            if (error) {
              // self.showSnackBar('Server throws error','error');
            }
          }
        );
      }
    );
    document.querySelector("#root").style.filter = "blur(8px)";
  }

  componentWillUnmount() {
    document.querySelector("#root").style.filter = "";
    this.props.removeTaskComments();
    if (this.state.currentTask.taskId)
      this.props.dispatchAddTodoItems(
        this.state.currentTask.taskId,
        "ClearTodoItems"
      ); /** clearing the todo item from store when closing task details */
    this.props.clearDocuments(); //clear Documents
  }

  componentDidUpdate(prevProps, prevState) {
    const { currentTask, taskTodoItems } = this.props;

    // if currentTask is deleted from other users
    if (!currentTask) {
      this.closeTaskDetailsHandler();
    } else if (JSON.stringify(prevProps.currentTask) !== JSON.stringify(currentTask)) {
      this.setState({
        currentTask: currentTask ? this.getTaskAssignees(currentTask) : {},
        taskNameOriginal: this.props.currentTask.taskTitle,
        taskName: this.props.currentTask.taskTitle,
      });
    }
    // if (
    //   JSON.stringify(prevProps.taskTodoItems) !== JSON.stringify(taskTodoItems)
    // ) {
    //   const filterUnrelatedtaskTodoItems = taskTodoItems.data.filter((c) => {
    //     return c.taskId == currentTask.id;
    //   });
    //   this.setState({ comments: filterUnrelatedtaskTodoItems });
    // }
  }

  getTaskAssignees = task => {
    if (task.assigneeLists) {
      let updatedTask = this.getTaskPermission(task);
      return updatedTask;
    }

    const obj = {};
    const members =
      this.props.profileState && this.props.profileState.data && this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [];

    // assigneeLists from createdBy field
    obj.assigneeLists = members.filter(member => {
      // createdByName from createdBy field
      if (task.createdBy && member.userId === task.createdBy)
        obj.createdByName = generateUsername(member.email, member.userName, member.fullName);

      if (task.assigneeList && task.assigneeList.length >= 0)
        return task.assigneeList.indexOf(member.userId) >= 0;
    });
    let updatedTask = { ...task, ...obj };
    updatedTask = this.getTaskPermission(updatedTask);
    return updatedTask;
  };

  getTaskPermission = task => {
    /** pushing the task permission object in the task , if task has linked with any project i-e projectId !== null then find the project and fetch the task permission in project object */
    const {
      projects,
      taskPer,
      workspaceLevelMeetingPer,
      workspaceLevelRiskPer,
      workspaceLevelIssuePer,
      workspaceStatus,
    } = this.props;
    if (task.projectId) {
      let attachedProject = projects.find(p => p.projectId == task.projectId);
      if (attachedProject && attachedProject.projectPermission) {
        task.taskPermission = attachedProject.projectPermission.permission.task;
        task.meetingPermission = attachedProject.projectPermission.permission.meeting;
        task.riskPermission = attachedProject.projectPermission.permission.risk;
        task.issuePermission = attachedProject.projectPermission.permission.issue;
      } else {
        task.taskPermission = taskPer;
        task.meetingPermission = workspaceLevelMeetingPer;
        task.riskPermission = workspaceLevelRiskPer;
        task.issuePermission = workspaceLevelIssuePer;
      }
      if (attachedProject && attachedProject.projectTemplate) {
        task.taskStatus = attachedProject.projectTemplate;
      } else {
        task.taskStatus = workspaceStatus;
      }
      return task;
    } else {
      task.taskPermission = taskPer;
      task.meetingPermission = workspaceLevelMeetingPer;
      task.riskPermission = workspaceLevelRiskPer;
      task.issuePermission = workspaceLevelIssuePer;
      task.taskStatus = workspaceStatus;
      return task;v
    }
  };

  handleAllRead() {
    const currentUser = this.props.profileState.data ? this.props.profileState.data.userId : [];
    if (this.state.readAll) {
      this.setState({ readAll: false }, () => {
        this.props.MarkTaskNotificationsAsRead(
          this.state.currentTask.taskId,
          currentUser,
          (err, data) => {
            this.setState({ readAll: true });
          }
        );
      });
    }
  }

  activityLog() {
    this.setState({ showActivity: true });
  }

  hideActivityLog() {
    this.setState({ showActivity: false });
  }

  statusChanged() {
    this.setState({ changeStatus: true });
  }

  clearStatusChanged() {
    this.setState({ changeStatus: false });
  }

  handleDateSave = (startDate, endDate, dateType, startTime, endTime) => {
    const { currentTask } = this.state;
    const obj =
      dateType == "planned"
        ? {
            startDateString: helper.RETURN_CUSTOMDATEFORMAT(startDate),
            dueDateString: helper.RETURN_CUSTOMDATEFORMAT(endDate),
            startTime,
            dueTime: endTime,
          }
        : {
            actualStartDateString: helper.RETURN_CUSTOMDATEFORMAT(startDate),
            actualDueDateString: helper.RETURN_CUSTOMDATEFORMAT(endDate),
            actualStartTime: startTime,
            actualDueTime: endTime,
          };
    this.props.UpdateTask({ ...currentTask, ...obj }, response => {
      if (response && response.status === 200) {
        response = response.data.CalenderDetails;
      }
    });
  };

  handleTaskNameEdit(event) {
    const { getPermission } = this;
    if (getPermission("taskTitle")) this.setState({ editTaskName: true });
    else this.setState({ editTaskName: false });
  }

  handleTaskNameSave(event, isClick) {
    if (
      (event.key === "Enter" || isClick) &&
      this.taskNameValidator() &&
      this.state.taskName &&
      this.state.taskNameOriginal !== this.state.taskName
    ) {
      const { currentTask } = this.state;
      const self = this;
      this.setState({ taskName: self.state.taskName }, function() {
        currentTask.taskTitle = self.state.taskName;
        if (this.props.isGantt) {
          this.props.UpdateGantTask(currentTask, (err, response) => {
            if (response) {
              response = response.data.CalenderDetails || [];
              self.setState({
                taskNameError: false,
                validateTaskName: "",
                taskNameOriginal: this.state.currentTask,
                editTaskName: false,
                canEditTaskTitle: false,
              });
              self.props.UpdateCalenderTask(response);
            }
          });
        } else {
          self.props.UpdateTask(currentTask, response => {
            if (response && response.status === 200) {
              response = response.data.CalenderDetails || [];
              // if (this.props.isGantt)
              //   this.props.getGanttTasks(currentTask.projectId);
              self.setState({
                taskNameError: false,
                validateTaskName: "",
                taskNameOriginal: this.state.currentTask,
                editTaskName: false,
                canEditTaskTitle: false,
              });
              self.props.UpdateCalenderTask(response);
            }
          });
        }
      });
    }
    // this.setState({editTaskName: false});
  }

  handleTaskNameInput(event) {
    this.setState({ taskName: event.currentTarget.value });
  }

  taskNameValidator = () => {
    // if (event === "enter") {
    const response1 = helper.HELPER_EMPTY("Task name", this.state.taskName);
    const response2 = helper.HELPER_CHARLIMIT250("Task name", this.state.taskName);
    const response3 = helper.RETURN_CECKSPACES(this.state.taskName, "Task name");
    let error = null;
    if (response1) {
      error = response1;
    } else if (response2) {
      error = response2;
    } else if (response3) {
      error = response3;
    }
    if (error) {
      this.setState({
        taskNameError: true,
        validateTaskName: error,
      });
      return false;
    }
    return true;
  };

  handleTaskNameClickAway = event => {
    if (this.taskNameValidator()) {
      this.setState({ editTaskName: false }, () => {
        this.handleTaskNameSave(event, true);
      });
    }
  };

  updateTask = taskData => {
    this.props.UpdateTask(taskData, response => {
      this.setState({
        currentTask: this.getTaskAssignees(response.data),
      });
    });
  };

  startTime = (time, start) => {
    const data = {
      taskId: this.state.currentTask.taskId,
      startTime: time,
      isStart: true,
    };
    this.props.startTimer(
      data,
      response => {
        const { createdDate, currentDate } = response.data;
        this.props.setGlobalTaskTime(this.state.currentTask, createdDate, currentDate);
        start();
      },
      error => {
        const err = error.data.message;
        const message = err || "Server throws error";
        this.showSnackBar(message, "error");
      }
    );
  };

  stopTime = time => {
    const data = {
      taskId: this.state.currentTask.taskId,
      endTime: time,
      isStart: false,
    };
    this.props.stopTimer(
      data,
      suss => {},
      err => {
        // this.showSnackBar(`Oops! Effort not saved .${err.data}`, "error");
      }
    );
  };

  handleRemoveUserTaskEffort = taskEffortId => {
    this.props.removeUserEffort(
      this.state.currentTask.taskId,
      taskEffortId,
      () => {},
      error => {
        if (error.status === 500) {
          // self.showSnackBar('Server throws error','error');
        }
      }
    );
  };

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };

  // Function that updates assignee of respective document (e.g. task/risk/issue/meeting)
  updateAssignee = (assignedTo, task) => {
    const assignedToArr = assignedTo.map(ass => {
      return ass.userId;
    });
    const newTaskObj = { ...task, assigneeList: assignedToArr };

    this.props.UpdateTask(newTaskObj, () => {});
  };

  handleOptionsSelect = (key, value) => {
    const { currentTask } = this.state;
    this.setState({ currentTask: { ...currentTask, [key]: value.value } });
    const newIssueObj = {
      ...currentTask,
      [key]: priorities.indexOf(value.value),
    };
    this.props.UpdateTask(newIssueObj, () => {});
  };

  // Open Repeat Dialog Function
  repeatDropdownOpen = () => {
    this.setState({ repeatDropdown: true });
  };

  // Close Repeat Dialog Function
  closeRepeatDropdown = () => {
    this.setState({ repeatDropdown: false });
  };

  // It is called to open unarchived dialog
  openPopUP = event => {
    this.setState({ unArchiveFlag: true });
  };

  // It is called to unarchived task
  handleUnArchived = e => {
    const { currentTask } = this.state;
    if (e) e.stopPropagation();
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.UnArchiveTask(
        currentTask,
        // success
        () => {
          this.setState({
            unarchiveBtnQuery: "",
            unArchiveFlag: false,
          });
          this.closeTaskDetailsHandler();
          // it will unarchive the task from local state
          this.props.filterUnArchiveTask(currentTask.taskId);
        },
        // failure
        error => {
          this.setState({
            unarchiveBtnQuery: "",
            unArchiveFlag: false,
          });
          this.showSnackBar(error.data.message);
          // this.props.closeTaskDetailsPopUp();
        }
      );
    });
  };

  // It is called to close unarchived dialog
  handleDialogClose = event => {
    this.setState({ unArchiveFlag: false });
  };

  updateComments = (items = []) => {
    // this.setState({ comments: items });
  };
  closeTaskDetailsHandler = () => {
    const { currentTask } = this.props;
    const { taskTodoItems } = this.props;
    this.props.closeTaskDetailsPopUp(currentTask, taskTodoItems);
  };

  getPermission = key => {
    /** function calling for getting permission */
    const { permissions, currentTask } = this.props;
    return GetPermission.canEdit(
      currentTask.isOwner,
      permissions,
      key
    ); /** passing current user owner , workspace permission and a key to check */
  };

  notificationsData = () => {
    const taskNotificationsData = this.props.taskNotificationsState.data
      ? this.props.taskNotificationsState.data
      : [];
    const members =
      this.props.profileState && this.props.profileState.data && this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [];
    let notifications = taskNotificationsData
      ? taskNotificationsData.filter(x => {
          return x.users.filter(y => y.userId === this.state.currentUser && y.isViewed === false)
            .length;
        })
      : [];
    notifications = notifications
      .map(x => {
        x.createdName = members.find(m => m.userId === x.createdBy);
        x.users = x.users.map(u => ({
          userId: u.userId,
          isViewed: u.isViewed,
        }));
        return x;
      })
      .map(x => ({
        id: x.notificationId,
        // title: x.createdName
        //   ? `${x.createdName.fullName ||
        //       x.createdName.userName ||
        //       x.createdName.email} ${x.description}`
        //   : " ",
        title: x.description,
        date: helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(x.updatedDate || x.createdDate),
        time: helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate || x.createdDate),
        profilePic: x.createdName ? x.createdName.imageUrl : "",
        createdName: x.createdName,
        notificationUrl: x.notificationUrl,
        teamName: x.teamName || "N/A",
        teamId: x.teamId,
        workspaceName: x.workSpaceName || "N/A",
        workspaceId: x.workSpaceId,
      }));
    return notifications;
  };
  onChatMount = () => {};
  onChatUnMount = () => {};

  handleStatusChange = status => {
    const { currentTask } = this.state;
    const { profileState } = this.props;
    currentTask.status = status.value;
    currentTask.statusTitle = status.label;

    if (status.obj.isDoneState) {
      this.setState({ markChecklistActionBtnQuery: "progress" });
      const checkData = {
        taskId: currentTask.id,
        checkAll: true,
      };
      this.props.CheckAllTodoList(
        checkData,
        succ => {},
        fail => {},
        profileState.data
      );
    }

    this.props.UpdateTask(currentTask, response => {
      if (response && response.status == 200) {
        this.setState({
          currentTask: currentTask,
          markChecklistActionBtnQuery: "",
          markChecklistActionConf: false,
        });
      } else {
        this.showSnackBar(response.data.message, "error");
        this.setState({ markChecklistActionBtnQuery: "" });
      }
    });
  };

  handleUpdateStatus = status => {
    const { loggedInTeam, workspace } = this.props.profileState;
    const currentWorkspace = workspace.find(t => t.teamId === loggedInTeam);

    if (status.obj.isDoneState && currentWorkspace.config.isUserTasksEffortMandatory) {
      showSnackBar("Make sure you have added effort in the task", "info", {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
      });
    }
    let allItemsCompleted = this.props.taskTodoItems.every(
      todo => todo.isComplete
    ); /** check if todo items are all completed or not, if true then dont show modal */
    if (status.obj.isDoneState && !allItemsCompleted) {
      this.markAllConfirmDialogOpen();
      return;
    }
    this.handleStatusChange(status);
  };

  statusData = statusArr => {
    return statusArr.map(item => {
      return {
        label: item.statusTitle,
        value: item.statusId,
        icon: <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "20px" }} />,
        statusColor: item.statusColor,
        obj: item,
      };
    });
  };
  // handleTaskDescription = data => {
  //   const {currentTask} =  this.state;
  //   currentTask.description = data.html; /** function set the description i the state */
  //   this.setState({currentTask, EditMode: true});
  // };
  // handleClickAwayTaskDescription = () => {
  //   const {currentTask, EditMode} =  this.state;
  //   let taskData = {...currentTask};
  //   // taskData.description = window.btoa(taskData.description);
  //   if (EditMode) {
  //     this.props.UpdateTask(taskData, (response) => {
  //       this.setState({
  //         currentTask: currentTask,
  //         EditMode: false
  //       });
  //     })
  //   }
  // }

  getCreatedByName = () => {
    const { currentTask, members } = this.props;
    let createdByUser = members.find(e => e.userId == currentTask.createdBy) || false;
    return createdByUser ? createdByUser.fullName : "User not found!";
  };

  markAllConfirmDialogOpen = () => {
    this.setState({ markChecklistActionConf: true });
  };
  markAllConfirmDialogClose = () => {
    this.setState({ markChecklistActionConf: false });
  };

  handleClickHideOption = (event, field) => {
    let object = { ...field };
    object.settings.isHideCompletedTodos = !object.settings.isHideCompletedTodos;
    delete object.team;
    delete object.workspaces;
    delete object.level;
    this.props.editCustomField(
      object,
      object.fieldId,
      res => {},
      failure => {}
    );
  };

  customFieldChange = (option, obj, settings) => {
    const taskCopy = this.state.currentTask;

    if (obj.fieldType == "matrix") {
      const selectedField =
        taskCopy.customFieldData && taskCopy.customFieldData.find(x => x.fieldId == obj.fieldId);
      const isExist =
        selectedField &&
        selectedField.fieldData.data.findIndex(f => f.cellName == option.cellName) > -1;
      if (isExist === true) return;
    }
    const { fieldId } = obj;
    const newObj = {
      groupType: "task",
      groupId: taskCopy.taskId,
      fieldId,
      fieldData: { data: option },
    };

    //Updating Localstate

    const customFields =
      taskCopy.customFieldData &&
      taskCopy.customFieldData.map(c => {
        return c.fieldId === newObj.fieldId ? newObj : c;
      });

    let newTaskObj = { ...taskCopy, customFieldData: customFields ? customFields : [newObj] };
    this.setState({ currentTask: newTaskObj });

    UpdateTaskCustomField(newObj, res => {
      const taskObj = res.data.entity[0];
      let customFieldsArr = [];
      // Updating Global state
      const isExist = taskCopy.customFieldData
        ? taskCopy.customFieldData.findIndex(c => c.fieldId === taskObj.fieldId) > -1
        : false; /** if new task created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in task object and save it */
      if (isExist) {
        customFieldsArr = taskCopy.customFieldData.map(c => {
          return c.fieldId === taskObj.fieldId ? taskObj : c;
        });
      } else {
        customFieldsArr = taskCopy.customFieldData
          ? [...taskCopy.customFieldData, taskObj]
          : [
              taskObj,
            ]; /** add custom field in task object and save it, if newly created task because its custom field is already null */
      }
      let newTaskObj = { ...taskCopy, customFieldData: customFieldsArr };
      this.props.dispatchTask(newTaskObj);
    });
    this.handleAddAssessmentDialog(false);
  };
  handleAddAssessmentDialog = value => {
    this.setState({ assessmentDialogOpen: value });
  };
  addCustomFieldCallback = obj => {
    const { fieldId } = obj;
    const customFieldDataObj = {
      groupType: "task",
      groupId: this.state.currentTask.id,
      fieldId: fieldId,
      fieldData: { data: obj.settings.multiSelect ? [] : {} },
    };
    const taskCopy = cloneDeep(this.state.currentTask);
    taskCopy.customFieldData = [...taskCopy.customFieldData, customFieldDataObj];
    this.setState({ currentTask: taskCopy });
    this.props.dispatchTask(taskCopy);
  };

  editCustomFieldCallback = (obj, oldFieldObject) => {
    const { fieldId, fieldType } = obj;
    if (fieldType === "checklist") return;
    this.props.updateCustomFieldData(obj);
  };
  handleEditField = field => {
    this.props.updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "edit" });
  };
  handleCopyField = field => {
    this.props.updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "copy" });
  };
  handleSelectHideOption = (value, selectedFieldObj) => {
    let object = { ...selectedFieldObj };

    if (value == 1) {
      object.hideOption = "workspace";
      object.settings.isHidden = false;

      object.settings.hiddenInWorkspaces = [
        profileState.loggedInTeam,
        ...object.settings.hiddenInWorkspaces,
      ];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("task") < 0
          ? ["task", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    if (value == 2) {
      /** if user select the team level */
      object.hideOption = "allWorkspaces";
      object.settings.isHidden = true;
      object.settings.hiddenInWorkspaces = [];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("task") < 0
          ? ["task", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    delete object.team;
    delete object.workspaces;
    this.props.hideCustomField(
      object,
      res => {},
      failure => {}
    );
  };

  render() {
    const {
      classes,
      theme,
      childTasks,
      isArchivedSelected,
      permissions,
      taskPer,
      taskTodoItems,
      profileState,
    } = this.props;
    const intl = this.props.intl;
    const {
      open,
      currentTask,
      editTaskName,
      taskName,
      currentUser,
      priority,
      repeatDropdown,
      unArchiveFlag,
      unarchiveBtnQuery,
      comments,
      markChecklistActionConf,
      markChecklistActionBtnQuery,
    } = this.state;
    const { getPermission } = this;

    const taskStatusData = this.statusData(currentTask.taskStatus.statusList);
    let selectedStatus =
      currentTask.status == -1
        ? taskStatusData[0]
        : taskStatusData.find(item => item.value == currentTask.status) || {};

    const members =
      this.props.profileState && this.props.profileState.data && this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [];

    const permission = getTasksPermissions(currentTask, this.props);
    const permissionAction = getTasksPermissionsWithoutArchieve(currentTask, this.props);
    const canEditTaskTitle = currentTask.isDeleted
      ? false
      : currentTask.taskPermission
      ? currentTask.taskPermission.taskDetail.editTaskName.isAllowEdit
      : true;
    const canEditStartDate = currentTask.isDeleted
      ? false
      : currentTask.taskPermission
      ? currentTask.taskPermission.taskDetail.taskPlannedStartEndDate.isAllowEdit
      : true;
    const canEditDueDate = currentTask.isDeleted
      ? false
      : currentTask.taskPermission
      ? currentTask.taskPermission.taskDetail.taskActualStartEndDate.isAllowEdit
      : true;
    const canEditTaskProject = currentTask.isDeleted
      ? false
      : currentTask.taskPermission
      ? currentTask.taskPermission.taskDetail.taskProject.isAllowEdit
      : true;
    const canEditTaskDescription = currentTask.isDeleted
      ? false
      : currentTask.taskPermission
      ? currentTask.taskPermission.taskDetail.taskDescription.isAllowEdit
      : true;
    const customFieldPermission =
      (currentTask.taskPermission && currentTask.taskPermission.taskDetail.customsFields) || {};

    let notifications = this.notificationsData();

    const chatPermission = {
      addAttachment: currentTask.taskPermission.taskDetail.taskComments.taskattachment.isAllowAdd,
      deleteAttachment:
        currentTask.taskPermission.taskDetail.taskComments.taskattachment.isAllowDelete,
      downloadAttachment:
        currentTask.taskPermission.taskDetail.taskComments.downloadAttachment.cando,
      addComments: currentTask.taskPermission.taskDetail.taskComments.cando,
      editComment: currentTask.taskPermission.taskDetail.taskComments.comments.isAllowEdit,
      deleteComment: currentTask.taskPermission.taskDetail.taskComments.comments.isAllowDelete,
      convertToTask: currentTask.taskPermission.taskDetail.taskComments.convertToTask.cando,
      reply: currentTask.taskPermission.taskDetail.taskComments.reply.cando,
      replyLater: currentTask.taskPermission.taskDetail.taskComments.replyLater.cando,
      showReceipt: currentTask.taskPermission.taskDetail.taskComments.showReceipt.cando,
      clearAll: currentTask.taskPermission.taskDetail.taskComments.clearAllReplies.cando,
      clearReply: currentTask.taskPermission.taskDetail.taskComments.clearReply.cando,
    };
    const docPermission = {
      uploadDocument: currentTask.taskPermission.taskDetail.taskDocuments.uploadDocument.cando,
      addFolder: currentTask.taskPermission.taskDetail.taskDocuments.addFolder.cando,
      downloadDocument: currentTask.taskPermission.taskDetail.taskDocuments.downloadDocument.cando,
      viewDocument: currentTask.taskPermission.taskDetail.taskDocuments.cando,
      deleteDocument: currentTask.taskPermission.taskDetail.taskDocuments.deleteDocument.cando,
      openFolder: currentTask.taskPermission.taskDetail.taskDocuments.openFolder.cando,
      renameFolder: currentTask.taskPermission.taskDetail.taskDocuments.renameFolder.cando,
    };

    const chatConfig = {
      contextUrl: "communication",
      contextView: "Task",
      contextId: currentTask.taskId,
      contextKeyId: "taskId",
      contextChat: "single",
      assigneeLists: this.props.currentTask.assigneeLists,
    };

    const statusList = this.statusData(currentTask.taskStatus.statusList);
    const doneStatus = statusList.find(s => s.obj.isDoneState);

    // const isCustomFieldAccess = ENV === "dev" || ENV === "beta" || ENV === "test";
    // ||
    // [
    //   "fe3cd87bad3540d49571538016f0488c",
    //   "e26f65f31d2546b5bb9bf31f8e581d74",
    //   "630c29e8a2a84ff887f6d3ea1a2907f7",
    //   "6f190aff68584d7f83679db1a59fc89c",
    //   "6f190aff68584d7f83679db1a59fc89c",
    //   "630c29e8a2a84ff887f6d3ea1a2907f7",
    //   "d88054781f7d4123bac62a17c73dcc35",
    // ].includes(profileState.activeTeam);
    return (
      <Dialog
        classes={{
          root: classes.taskDetailsDialog,
          paper: classes.dialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        aria-labelledby="form-dialog-title"
        fullWidth
        scroll="body"
        open={open}
        onEscapeKeyDown={this.closeTaskDetailsHandler}>
        <ActionConfirmation
          open={markChecklistActionConf}
          closeAction={this.markAllConfirmDialogClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="task.detail-dialog.to-do-list.mark-all.action"
              defaultMessage="Yes, Mark All Complete"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="task.detail-dialog.to-do-list.mark-all.title"
              defaultMessage="Mark All Complete"
            />
          }
          iconType="markAll"
          msgText={
            <FormattedMessage
              id="task.detail-dialog.to-do-list.mark-all.message"
              defaultMessage="Are you sure you want to mark all to-do list items as completed?"
            />
          }
          successAction={() => this.handleStatusChange(doneStatus)}
          btnQuery={markChecklistActionBtnQuery}
        />
        {open && (
          <DropdownMenu
            open={open}
            anchorEl={this.anchorEl}
            placement="bottom-start"
            style={{ width: 600 }}
            id="taskRepeatDropdown"
            closeAction={this.closeRepeatDropdown}
            open={repeatDropdown}>
            <div>
              <RepeatTask
                closeAction={this.closeRepeatDropdown}
                task={currentTask}
                label
                taskPer={currentTask.taskPermission}
              />
            </div>
          </DropdownMenu>
        )}
        <DialogTitle id="form-dialog-title" classes={{ root: classes.defaultDialogTitle }}>
          {isArchivedSelected ? (
            <div className={classes.unArchiveTaskCnt}>
              <Typography variant="h6" className={classes.unArchiveTaskTxt}>
                This task is archived. You can't edit this task.
              </Typography>
              <CustomButton
                btnType="success"
                variant="contained"
                style={{ marginLeft: 10 }}
                onClick={event => {
                  this.openPopUP(event);
                }}>
                Unarchive
              </CustomButton>
            </div>
          ) : null}
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            style={{ padding: "10px 15px" }}>
            <Grid item sm={12} md={6} lg={6}>
              {editTaskName && canEditTaskTitle ? (
                <ClickAwayListener onClickAway={this.handleTaskNameClickAway}>
                  <OutlinedInput
                    labelWidth={150}
                    notched={false}
                    value={taskName}
                    onKeyDown={this.handleTaskNameSave}
                    autoFocus
                    error={this.state.taskNameError}
                    errorMessage={this.state.validateTaskName}
                    onChange={this.handleTaskNameInput}
                    placeholder="Add Task Title"
                    style={{ width: 500 }}
                    inputProps={{ maxLength: 250 }}
                    classes={{
                      root: classes.outlinedInputCnt,
                      input: classes.outlinedTaskInput,
                      notchedOutline: classes.notchedOutlineCnt,
                      focused: classes.outlineInputFocus,
                    }}
                  />
                </ClickAwayListener>
              ) : (
                <Typography
                  // style={{ lineHeight: "47px" }}
                  variant="h2"
                  onClick={this.handleTaskNameEdit}>
                  {taskName}
                </Typography>
              )}
            </Grid>
            <Grid item sm={12} md={6} lg={6}>
              <Grid container direction="row" justify="flex-end" alignItems="center">
                {currentTask ? (
                  <Timer
                    style={{ marginRight: 10 }}
                    startTime={this.startTime}
                    stopTime={this.stopTime}
                    allMembers={members}
                    taskId={currentTask.taskId}
                    currentTask={currentTask}
                    updateTask={this.updateTask}
                    handleRemoveUserTaskEffort={this.handleRemoveUserTaskEffort}
                    isArchivedSelected={currentTask.isDeleted}
                    showSnackBar={this.showSnackBar}
                    taskPer={currentTask.taskPermission}
                    miliSecToMinus={this.props.miliSecToMinus}
                  />
                ) : null}
                <div className={classes.statusDropdownCnt}>
                  <StatusDropdown
                    onSelect={status => {
                      this.handleUpdateStatus(status);
                    }}
                    iconButton={false}
                    option={selectedStatus}
                    style={{ marginRight: 8 }}
                    options={taskStatusData}
                    toolTipTxt={selectedStatus.label}
                    disabled={!currentTask.taskPermission.taskDetail.editTaskStatus.isAllowEdit}
                    legacyButton={true}
                    btnStyle={{
                      padding: "8px 0px",
                    }}
                    writeFirst={false}
                  />
                </div>
                <div className={classes.assigneeListCnt}>
                  <AssigneeDropdown
                    closeTaskDetailsPopUp={this.closeTaskDetailsHandler}
                    assignedTo={currentTask.assigneeLists}
                    updateAction={this.updateAssignee}
                    isArchivedSelected={currentTask.isDeleted}
                    obj={currentTask}
                    addPermission={
                      currentTask.taskPermission
                        ? currentTask.taskPermission.taskDetail.taskAssign.isAllowAdd
                        : true
                    }
                    deletePermission={
                      currentTask.taskPermission
                        ? currentTask.taskPermission.taskDetail.taskAssign.isAllowDelete
                        : true
                    }
                  />
                </div>
                <Grid item>
                  {!isArchivedSelected && teamCanView("repeatTaskAccess") && (
                    <CustomIconButton
                      onClick={this.repeatDropdownOpen}
                      btnType="filledWhite"
                      style={{ borderRadius: "50%", border: "none" }}
                      buttonRef={node => {
                        this.anchorEl = node;
                      }}
                      disabled={
                        currentTask.taskPermission
                          ? !currentTask.taskPermission.taskDetail.repeatTask.isAllowAdd
                          : false
                      }>
                      <SvgIcon
                        viewBox="0 0 14 12.438"
                        htmlColor={
                          currentTask.repeatTask
                            ? theme.palette.primary.light
                            : theme.palette.secondary.medDark
                        }
                        className={classes.recurrenceIcon}>
                        <RecurrenceIcon />
                      </SvgIcon>
                    </CustomIconButton>
                  )}
                  {this.props.profileState.data.isSlackLinked ? (
                    <SlackChannelIntegration
                      task={currentTask}
                      updateTask={this.updateTask}
                      isDisabled={isArchivedSelected}
                    />
                  ) : null}
                  <NotificationMenu
                    notificationsData={notifications}
                    MarkAllNotificationsAsRead={this.handleAllRead}
                    history={this.props.history}
                  />

                  <TaskDetailsActionDropDown
                    filterUnArchiveTask={this.props.filterUnArchiveTask}
                    deleteTaskFromArchiveList={this.props.deleteTaskFromArchiveList}
                    task={currentTask}
                    CopyTask={this.props.CopyTask}
                    DeleteTask={this.props.DeleteTask}
                    ArchiveTask={this.props.ArchiveTask}
                    onDeleteTask={this.props.onDeleteTask}
                    GanttCmp={this.props.GanttCmp}
                    CopyCalenderTask={this.props.CopyCalenderTask}
                    UnArchiveTask={this.props.UnArchiveTask}
                    UpdateTaskColorFromGridItem={this.props.UpdateTaskColorFromGridItem}
                    UpdateCalenderTask={this.props.UpdateCalenderTask}
                    selectedColor={currentTask.colorCode}
                    closeTaskDetailsPopUp={this.closeTaskDetailsHandler}
                    getGanttTasks={this.props.getGanttTasks}
                    activityLog={this.activityLog}
                    permission={permissionAction}
                    isGantt={this.props.isGantt} // The case is true if the task details is opened from Gantt
                    childTasks={childTasks} // for the case if task details is opened from gantt
                    showSnackBar={this.showSnackBar}
                    taskPer={currentTask.taskPermission}
                  />
                  <IconButton btnType="transparent" onClick={this.closeTaskDetailsHandler}>
                    <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </DialogTitle>

        <DialogContent classes={{ root: classes.defaultDialogContent }}>
          <Grid container direction="row" justify="flex-start" alignItems="stretch">
            <Grid item xs={7} style={{ zIndex: 1 }}>
              {/* <Scrollbars style={{ height: "100%" }}> */}
              <div className={classes.taskDetailsLeftCnt}>
                <div style={{ overflowY: "auto", height: calculateHeight() - 160 }}>
                  {/* <Scrollbars style={{ height: '100%' }}> */}
                  <div className={classes.taskDetailsLeftTopCnt}>
                    <Grid
                      container
                      direction="row"
                      justify="space-evenly"
                      alignItems="flex-end"
                      classes={{ container: classes.mainTaskDetailsCnt }}
                      spacing={2}>
                      {teamCanView("projectAccess") && (
                        <Grid item xs={3} classes={{ item: classes.taskDetailsFieldCnt }}>
                          <SelectProject
                            taskData={currentTask}
                            projectId={currentTask.projectId}
                            isGantt={this.props.isGantt}
                            getGanttTasks={this.props.getGanttTasks}
                            permission={canEditTaskProject}
                            isDisabled={currentTask.isDeleted || !canEditTaskProject}
                          />
                        </Grid>
                      )}
                      <Grid
                        item
                        xs={teamCanView("projectAccess") ? 3 : 4}
                        classes={{ item: classes.taskDateFieldPosition }}>
                        <CustomRangeDatePicker
                          label={intl.formatMessage({
                            id: "task.detail-dialog.plan-start-end.label",
                            defaultMessage: "Planned Start/End",
                          })}
                          placeholder={intl.formatMessage({
                            id: "task.detail-dialog.plan-start-end.label",
                            defaultMessage: "Select Date",
                          })}
                          pickerType="input"
                          startDate={currentTask.startDateString}
                          endDate={currentTask.dueDateString}
                          startTime={currentTask.startTime}
                          endTime={currentTask.dueTime}
                          permission={!canEditStartDate}
                          dateSaveAction={(startDate, endDate, startTime, endTime) =>
                            this.handleDateSave(startDate, endDate, "planned", startTime, endTime)
                          }
                          error={false}
                        />
                      </Grid>
                      <Grid
                        item
                        xs={teamCanView("projectAccess") ? 3 : 4}
                        classes={{ item: classes.taskDateFieldPosition }}>
                        <CustomRangeDatePicker
                          label={intl.formatMessage({
                            id: "task.detail-dialog.actual-start-end.label",
                            defaultMessage: "Actual Start/End",
                          })}
                          placeholder={intl.formatMessage({
                            id: "task.detail-dialog.actual-start-end.placeholder",
                            defaultMessage: "Select Date",
                          })}
                          pickerType="input"
                          error={false}
                          startDate={currentTask.actualStartDateString}
                          endDate={currentTask.actualDueDateString}
                          startTime={currentTask.actualStartTime}
                          endTime={currentTask.actualDueTime}
                          permission={!canEditDueDate}
                          dateSaveAction={(startDate, endDate, startTime, endTime) =>
                            this.handleDateSave(startDate, endDate, "actual", startTime, endTime)
                          }
                        />
                      </Grid>
                      <Grid
                        item
                        xs={teamCanView("projectAccess") ? 3 : 4}
                        classes={{ item: classes.taskDetailsFieldCnt }}>
                        <SelectSearchDropdown /* Priority select drop down */
                          data={() => {
                            return priorityData(theme, classes, intl);
                          }}
                          label={
                            <FormattedMessage
                              id="task.detail-dialog.priority.label"
                              defaultMessage="Priority"
                            />
                          }
                          selectChange={this.handleOptionsSelect}
                          type="priority"
                          selectedValue={priorityData(theme, classes, intl).find(s => {
                            return s.value == priorities[currentTask.priority];
                          })}
                          placeholder="Select Priority"
                          icon
                          isMulti={false}
                          isDisabled={
                            currentTask.isDeleted ||
                            (currentTask.taskPermission
                              ? !currentTask.taskPermission.taskDetail.editTaskPriority.isAllowEdit
                              : false)
                          }
                        />
                      </Grid>
                    </Grid>
                    <Grid item xs={12}>
                      <TaskDescriptionEditor
                        taskData={currentTask}
                        permission={canEditTaskDescription}
                      />
                    </Grid>
                    <div style={{ marginTop: 20 }}>
                      <TodoList
                        currentTask={currentTask}
                        closeTaskDetailsPopUp={this.closeTaskDetailsHandler}
                        todoItems={taskTodoItems}
                        isArchivedSelected={currentTask.isDeleted}
                        permission={permission}
                        intl={intl}
                        progressBar={true}
                        draggable={true}
                        style={{ paddingRight: 220 }}
                      />
                    </div>
                    <SectionsView
                      type={"task"}
                      disabled={currentTask.isDeleted}
                      customFieldData={currentTask.customFieldData}
                      permission={
                        currentTask &&
                        currentTask.taskPermission &&
                        currentTask.taskPermission.taskDetail.customsFields
                      }
                      customFieldChange={this.customFieldChange}
                      handleEditField={this.handleEditField}
                      handleCopyField={this.handleCopyField}
                      groupId={currentTask.id}
                      handleClickHideOption={this.handleClickHideOption}
                      handleSelectHideOption={this.handleSelectHideOption}
                    />
                    {/* if bussiness plan then show add custom field component */}
                    {teamCanView("customFieldAccess") && (
                      <div className={classes.addCustomFieldCnt}>
                        <CustomFieldView
                          feature="task"
                          onFieldAdd={this.addCustomFieldCallback}
                          onFieldEdit={this.editCustomFieldCallback}
                          onFieldDelete={() => {}}
                          permission={customFieldPermission}
                          disableAssessment={true}
                        />
                      </div>
                    )}
                  </div>
                </div>
                {this.props.currentTask ? (
                  <div className={classes.createdBy}>
                    <Typography variant="body2">
                      {intl.formatMessage({
                        id: "common.created-by.action",
                        defaultMessage: "Created By",
                      })}
                      :{" "}
                      {`${moment(currentTask.createdDate).format("LL")} at ${moment(
                        currentTask.createdDate
                      ).format("LT")} by ${this.getCreatedByName()}`}
                    </Typography>
                  </div>
                ) : null}
              </div>
            </Grid>

            <Grid item xs={5} display="flex">
              <SideTabs
                taskData={currentTask}
                showActivity={this.state.showActivity}
                members={members}
                hideActivityLog={this.hideActivityLog}
                isArchivedSelected={currentTask.isDeleted}
                type={this.props.type}
                permission={permission}
                taskActivities={
                  this.props.taskActivitiesState.data ? this.props.taskActivitiesState.data : []
                }
                taskPer={currentTask.taskPermission}
                onChatMount={this.onChatMount}
                onChatUnMount={this.onChatUnMount}
                chatConfig={chatConfig}
                chatPermission={chatPermission}
                docConfig={chatConfig}
                docPermission={docPermission}
                selectedTab={0}
                intl={intl}
              />
              {/* // )} */}
            </Grid>
          </Grid>
          {unArchiveFlag ? (
            <ActionConfirmation
              open={unArchiveFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText="Cancel"
              successBtnText="Unarchive"
              alignment="center"
              iconType="unarchive"
              headingText="Unarchive"
              msgText="Are you sure you want to unarchive this task?"
              successAction={this.handleUnArchived}
              btnQuery={unarchiveBtnQuery}
            />
          ) : null}
        </DialogContent>
      </Dialog>
    );
  }
}

// Specifies the default values for props:
TaskDetails.defaultProps = {
  currentTask: {},
  taskTodoItems: [],
  handleDeleteCustomField: () => {},
  updatingSelectedColumn: () => {},
};

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    taskActivitiesState: state.taskActivities,
    workspacePermissionsState: state.workspacePermissions,
    taskNotificationsState: state.taskNotifications,
    permissions: state.workspacePermissions.data.task,
    members: state.profile.data.member.allMembers || [],
    taskPer: state.workspacePermissions.data.task,
    taskTodoItems: state.todoItemsData.data.todoItems,

    projects: state.projects.data,
    workspaceLevelMeetingPer: state.workspacePermissions.data.meeting,
    workspaceLevelRiskPer: state.workspacePermissions.data.risk,
    workspaceLevelIssuePer: state.workspacePermissions.data.issue,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    tasks: state.tasks.data || [],
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {
    getGanttTasks,
    UpdateTask,
    FetchTasksInfo,
    CopyTask,
    DeleteTask,
    ArchiveTask,
    UnArchiveTask,
    UpdateTaskColorFromGridItem,
    UpdateCalenderTask,
    CopyCalenderTask,
    startTimer,
    stopTimer,
    removeUserEffort,
    UpdateGantTask,
    setGlobalTaskTime,
    removeGlobalTaskTime,
    MarkTaskNotificationsAsRead,
    getTaskComments,
    removeTaskComments,
    dispatchAddTodoItems,
    clearDocuments,
    CheckAllTodoList,
    editCustomField,
    dispatchTask,
    bulkUpdateTask,
    updateCustomFieldData,
    updateCustomFieldDialogState,
    hideCustomField
  })
)(TaskDetails);
