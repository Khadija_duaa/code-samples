import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import combineStyles from "../../../utils/mergeStyles";
import InputLabel from "@material-ui/core/InputLabel";
import { components } from "react-select";
import Grid from "@material-ui/core/Grid";
import taskDetailStyles from "./styles";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import DatePicker from "react-datepicker";
import moment from "moment";
import "../../../assets/css/react-datepicker.css";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import ClockIcon from "@material-ui/icons/AccessTime";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import CalendarIcon from "@material-ui/icons/DateRange";
import InputAdornment from "@material-ui/core/InputAdornment";
import helper from "../../../helper";
import {injectIntl}from "react-intl";
class TaskDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      actualDate: moment(),
      plannedDate: moment(),
      dueDate: moment(),
      open: false,
      value: 0,
      timeInput: true,
      hours: "",
      mins: "",
      am: "AM",
      addTime: false,
      time: "",
      plannedHours: "",
      plannedMins: "",
      plannedAm: "AM",
      plannedAddTime: false,
      plannedTime: ""
    };
    this.handleActualDate = this.handleActualDate.bind(this);
    this.handlePlannedDate = this.handlePlannedDate.bind(this);
    this.handleTabChange = this.handleTabChange.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleHoursInput = this.handleHoursInput.bind(this);
    this.handleAmChange = this.handleAmChange.bind(this);
    this.handleAddTimeClick = this.handleAddTimeClick.bind(this);
    this.handleTimeSave = this.handleTimeSave.bind(this);
    this.handleMinsInput = this.handleMinsInput.bind(this);
    this.handlePlanTimeSave = this.handlePlanTimeSave.bind(this);
    this.handlePlanAddTimeClick = this.handlePlanAddTimeClick.bind(this);
  }
  componentDidMount() {
    this.setState({
      actualDate: this.props.actualDateString
        ? moment(this.props.actualDateString)
        : null,
      time: this.props.actualDateString
        ? moment(this.props.actualDateString).format("hh:mm A")
        : null,
      plannedDate: this.props.dateString ? moment(this.props.dateString) : null,
      plannedTime: this.props.dateString
        ? moment(this.props.dateString).format("hh:mm A")
        : null
    });
  }

  handleActualDate(date) {
    this.setState({
      actualDate: date
    });
  }
  handlePlannedDate(date) {
    this.setState({
      plannedDate: date
    });
  }
  handleTabChange = (event, value) => {
    this.setState({ value });
  };
  handleToggle = () => {
    const { permission } = this.props;
    const condition = !permission;
    if (condition) {
      return;
    }
    this.setState(state => ({ open: !state.open }));
  };
  handleClose = event => {
    let completeDateFormat = this.state.actualDate
      ? helper.RETURN_CUSTOMDATEFORMAT(this.state.actualDate, this.state.time)
      : null;
    let completePlannedDate = this.state.plannedDate
      ? helper.RETURN_CUSTOMDATEFORMAT(
          this.state.plannedDate,
          this.state.plannedTime
        )
      : null;
    if (
      completeDateFormat !== this.props.actualDateString ||
      completePlannedDate !== this.props.dateString
    ) {
      this.props.selectedDate(
        completeDateFormat,
        this.props.startDate,
        completePlannedDate
      );
    }
    this.setState({ open: false });
  };
  handleHoursInput(event) {
    const value = event.currentTarget.value;
    if (
      (/^[0-9]$|^0[1-9]$|^1[0-2]$/.test(parseInt(value)) &&
        value.length <= 2 &&
        value !== "00" &&
        /^\d+$/.test(value)) ||
      !value
    ) {
      if (event.currentTarget.name == "plannedHours") {
        this.setState({ plannedHours: value });
      } else {
        this.setState({ hours: value });
      }
    }
  }
  handleMinsInput(event) {
    const value = event.currentTarget.value;
    if (
      (/^[0-9]$|^0[1-9]$|^[1-5][0-9]$|^60$/.test(parseInt(value)) &&
        value.length <= 2 &&
        /^\d+$/.test(value)) ||
      !value
    ) {
      if (event.currentTarget.name == "plannedMins") {
        this.setState({ plannedMins: value });
      } else {
        this.setState({ mins: value });
      }
    }
  }
  handleAmChange(event) {
    if (event.target.name == "plannedAm") {
      this.setState(prevState => ({
        plannedAm: prevState.plannedAm == "AM" ? "PM" : "AM"
      }));
    }
    this.setState(prevState => ({ am: prevState.am == "AM" ? "PM" : "AM" }));
  }
  handleAddTimeClick() {
    this.setState(prevState => ({ addTime: !prevState.addTime }));
  }
  handlePlanAddTimeClick() {
    this.setState(prevState => ({ plannedAddTime: !prevState.plannedAddTime }));
  }
  handleTimeSave() {
    const { hours, mins, am } = this.state;
    const time = `${hours.length < 2 ? `0${hours}` : hours}:${
      mins.length < 2 ? `0${mins}` : !mins ? "00" : mins
    } ${am}`;
    if (hours) {
      this.setState({ time, addTime: false });
    }
  }
  handlePlanTimeSave() {
    const { plannedHours, plannedMins, plannedAm } = this.state;
    const time = `${
      plannedHours.length < 2 ? `0${plannedHours}` : plannedHours
    }:${
      plannedMins.length < 2
        ? `0${plannedMins}`
        : !plannedMins
        ? "00"
        : plannedMins
    } ${plannedAm}`;
    if (plannedHours) {
      this.setState({ plannedTime: time, plannedAddTime: false });
    }
  }
  render() {
    const { classes, theme, taskData, permission } = this.props;
    const {
      actualDate,
      plannedDate,
      open,
      value,
      am,
      hours,
      addTime,
      mins,
      time,
      plannedTime,
      plannedAddTime,
      plannedAm,
      plannedMins,
      plannedHours,
      isEmpty
    } = this.state;

    const DateAndTimeCnt = props => {
      return (
        <Grid item classes={{ item: classes.menuTimeCnt }}>
          <p className={classes.dateTimeHeading}>Date & Time</p>
          <div className={classes.menuDateTimeInnerCnt}>
            <InputLabel classes={{ root: classes.dropdownsLabel }}>
              Start Date
            </InputLabel>
            <OutlinedInput
              labelWidth={150}
              notched={false}
              readOnly={true}
              value={
                props.type == "planned"
                  ? plannedDate
                    ? moment(plannedDate).format("MMM Do, YYYY")
                    : "-"
                  : actualDate
                  ? moment(actualDate).format("MMM Do, YYYY")
                  : "-"
              }
              //onClick={this.handleToggle}

              fullWidth
              classes={{
                root: classes.outlinedInputCnt,
                input: classes.outlinedDateInput,
                notchedOutline: classes.notchedOutlineCnt,
                focused: classes.outlineInputFocus
              }}
              placeholder="Add a start date"
            />
          </div>
          <div className={classes.timeFieldsCnt}>
            <InputLabel classes={{ root: classes.dropdownsLabel }}>
              Start Time
            </InputLabel>
            {(addTime && !props.type) ||
            (plannedAddTime && props.type == "planned") ? (
              <Fragment>
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                  <Grid item xs={4}>
                    <OutlinedInput
                      labelWidth={150}
                      notched={false}
                      value={props.type == "planned" ? plannedHours : hours}
                      onChange={this.handleHoursInput}
                      name={props.type == "planned" ? "plannedHours" : null}
                      classes={{
                        root: classes.outlinedHoursInputCnt,
                        input: classes.outlinedHoursInput,
                        notchedOutline: classes.notchedOutlineHoursCnt,
                        focused: classes.outlineInputFocus,
                        disabled: classes.outlinedInputDisabled
                      }}
                      placeholder="HH"
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <OutlinedInput
                      labelWidth={150}
                      notched={false}
                      value={props.type == "planned" ? plannedMins : mins}
                      onChange={this.handleMinsInput}
                      name={props.type == "planned" ? "plannedMins" : null}
                      classes={{
                        root: classes.outlinedMinInputCnt,
                        input: classes.outlinedMinInput,
                        notchedOutline: classes.notchedOutlineMinCnt,
                        focused: classes.outlineInputFocus,
                        disabled: classes.outlinedInputDisabled
                      }}
                      placeholder="MM"
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <OutlinedInput
                      labelWidth={150}
                      notched={false}
                      readOnly
                      name={props.type == "planned" ? "plannedAm" : null}
                      // disabled={true}
                      value={props.type == "planned" ? plannedAm : am}
                      onClick={this.handleAmChange}
                      classes={{
                        root: classes.outlinedAmInputCnt,
                        input: classes.outlinedAmInput,
                        notchedOutline: classes.notchedOutlineAMCnt,
                        focused: classes.outlineInputFocus,
                        disabled: classes.outlinedInputDisabled
                      }}
                      placeholder="AM"
                    />
                  </Grid>
                </Grid>
                <div className={classes.addTimeActionBtnCnt}>
                  <DefaultButton
                    onClick={
                      props.type == "planned"
                        ? this.handlePlanAddTimeClick
                        : this.handleAddTimeClick
                    }
                    text="Cancel"
                    buttonType="Transparent"
                    style={{ marginRight: 10 }}
                  />
                  <DefaultButton
                    onClick={
                      props.type == "planned"
                        ? this.handlePlanTimeSave
                        : this.handleTimeSave
                    }
                    text="Save"
                    disabled={
                      props.type == "planned"
                        ? plannedHours
                          ? false
                          : true
                        : hours
                        ? false
                        : true
                    }
                    buttonType="Plain"
                  />
                </div>
              </Fragment>
            ) : (
              <div
                className={classes.addTimeBtnCnt}
                onClick={
                  props.type == "planned"
                    ? this.handlePlanAddTimeClick
                    : this.handleAddTimeClick
                }
              >
                <ClockIcon
                  fontSize="small"
                  htmlColor={theme.palette.secondary.dark}
                />

                {props.type == "planned" ? (
                  <span className={classes.addTimeBtnText}>
                    {plannedTime ? plannedTime : "Add Time"}
                  </span>
                ) : (
                  <span className={classes.addTimeBtnText}>
                    {time ? time : "Add Time"}
                  </span>
                )}
              </div>
            )}
          </div>
        </Grid>
      );
    };
    return (
      <Fragment>
        <div>
          <InputLabel classes={{ root: classes.dropdownsLabel }}>
            {this.props.InputLabel}
          </InputLabel>
          <OutlinedInput
            labelWidth={150}
            notched={false}
            readOnly
            disabled={!permission}
            inputRef={node => {
              this.anchorEl = node;
            }}
            value={
              actualDate
                ? `${moment(actualDate).format("MMM Do")} ${time ? "-" : ""} ${
                    time ? time : ""
                  }`
                : this.props.intl.formatMessage({id:"common.date.placeholder",defaultMessage:"Select Date"})
            }
            endAdornment={
              <InputAdornment variant="filled" position="end">
                <CalendarIcon
                  htmlColor={theme.palette.secondary.light}
                  style={{ cursor: "auto" }}
                />
              </InputAdornment>
            }
            onClick={this.handleToggle}
            classes={{
              root: classes.outlinedInputCnt,
              input: classes.outlinedInput,
              notchedOutline: classes.notchedOutlineCnt,
              focused: classes.outlineInputFocus
            }}
            placeholder="Add a start date"
          />
          <Popper
            open={open}
            transition
            anchorEl={this.anchorEl}
            placement="bottom-start"
            style={{ zIndex: 99999 }}
            popperOptions={{
              positionFixed: true,
            }}
            modifiers={{
              flip: {
                enabled: false 
              },
              preventOverflow: {
                escapeWithReference: true
              }
            }}
            disablePortal={true}
            className={classes.datePickerPopper}
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                id="menu-list-grow"
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom"
                }}
              >
                <div>
                  <ClickAwayListener onClickAway={this.handleClose}>
                    <Grid container>
                      <Grid item>
                        <Tabs
                          value={value}
                          onChange={this.handleTabChange}
                          classes={{
                            root: classes.menuTabCnt,
                            indicator: classes.menuTabIndicator
                          }}
                        >
                          <Tab
                            label="Actual"
                            classes={{
                              root: classes.menuTab,
                              selected: classes.menuTabSelected
                            }}
                          />
                          <Tab
                            label="Planned"
                            classes={{
                              root: classes.menuTab,
                              selected: classes.menuTabSelected
                            }}
                          />
                        </Tabs>
                        {value === 1 && (
                          <div>
                            <DatePicker
                              inline
                              selected={plannedDate}
                              onChange={this.handlePlannedDate}
                              showDisabledMonthNavigation
                            />
                          </div>
                        )}
                        {value === 0 && (
                          <div>
                            <DatePicker
                              inline
                              selected={actualDate}
                              onChange={this.handleActualDate}
                              showDisabledMonthNavigation
                            />
                          </div>
                        )}
                      </Grid>
                      {value === 0 ? (
                        <DateAndTimeCnt type="planned" />
                      ) : (
                        <DateAndTimeCnt />
                      )}
                    </Grid>
                  </ClickAwayListener>
                </div>
              </Grow> 
            )}
          </Popper>
        </div>
      </Fragment>
    );
  }
}

export default withStyles(taskDetailStyles, {
  withTheme: true
})(TaskDatePicker);
