import React, { Component, Fragment } from "react";
import { compose } from "redux";
import findIndex from "lodash/findIndex";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import {
  getTaskComments,
  SaveTaskComments,
  saveUserComment,
  deleteTaskAttachmentComment,
  DeleteTaskAttachment
} from "../../../redux/actions/tasks";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import { withSnackbar } from "notistack";

import { withStyles } from "@material-ui/core/styles";
import CustomAvatar from "../../../components/Avatar/Avatar";
import taskDetailStyles from "./styles";
import ReactHtmlParser from 'react-html-parser';
import Truncate from "react-truncate";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import { Scrollbars } from "react-custom-scrollbars";
import helper from "../../../helper";
import moment from "moment";
import Icon from "../../../components/Icons/Icons";
import TextEditor from "../../../components/TextEditor/TextEditor";
import {
  UpdateTaskCommentData,
  DeleteTaskCommentData,
  AddNewTaskCommentData
} from "../../../redux/actions/taskComments";
import {
  uploadFileTextEditor,
  downloadDocs
} from "../../../redux/actions/constants";
import { getCompletePermissionsWithArchieve } from "../permissions";
import { saveTaskAttachment, UpdateTask, updateTaskData } from "../../../redux/actions/tasks";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { GetPermission } from "../../../components/permissions";
import {FormattedMessage} from 'react-intl';
class CommentsTab extends Component {
  constructor(props) {
    super(props);
    this.scrollbars = React.createRef();
    this.state = {
      comments: [],
      members: [],
      attachment: "",
      fileAttachment: null,
      fileAttachmentData: null,
      selectedDeleteComment: null,
      btnQuery: "",
      deleteDialogueStatus: false
    };
    this.handleNewComments = this.handleNewComments.bind(this);
    this.scrollComponent = React.createRef();
  }
  componentDidMount() {
    const { userId } = this.props.profileState.data;
    const { totalUnreadComment, unreadComments } = this.props.currentTask;

    if (totalUnreadComment) {
      let index = findIndex(unreadComments, function(o) {
        return o.userId == userId;
      });
      if (index > -1) {
        let unComments = unreadComments.concat();
        unComments[index].unreadComments = 0;
        const obj = {unreadComments: unComments }
        this.props.updateTaskData(
          { ...this.props.currentTask, obj },
          response => {
            if (response) {
            }
          }
        );
      }
    }

    let members =
      this.props.profileState &&
      this.props.profileState.data &&
      this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [];
    let memberList = members.map(x => {
      return {
        text: x.userName,
        value: x.userName,
        //  url: x.userName,
        userId: x.userId,
        isOnline: x.isOnline
      };
    });
    this.setState({ members: memberList }, () => {
      if (this.props.taskCommentsDataState.data) {
        this.setState(
          { comments: this.props.taskCommentsDataState.data },
          () => {
            if (this.scrollComponent.container) {
              this.scrollComponent.scrollToBottom();
            }
          }
        );
      } else {
        this.setState({ comments: [] });
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {

    if (
      JSON.stringify(this.props.taskCommentsDataState.data) !==
      JSON.stringify(prevProps.taskCommentsDataState.data)
    ) {
      //Filtering unrealted comments because all comments array in redux store can have comments of other tasks as well, in the case of update from websocket
      const filterUnrelatedComments = this.props.taskCommentsDataState.data.filter(c => {
        return c.comment.taskId == prevProps.currentTask.taskId
      })
      this.setState({ comments: filterUnrelatedComments }, () => {
        if (this.scrollComponent && this.scrollComponent.container) {
          this.scrollComponent.scrollToBottom();
        }
      });
    }
  }

  // static getderived
  handleNewComments(data) {
    this.props.SaveTaskComments(data, this.props.currentTask, response => {
      if (response && response.status === 200) {
        this.props.AddNewTaskCommentData(response.data);
      } else {
        this.setState({ comments: this.state.comments });
      }
    });
  }
  saveComment = (name, docsId, fileExtension, text) => {
    const fileType = {
      doc: 4,
      docx: 4,
      pdf: 5,
      png: 6,
      PNG: 6,
      jpg: 6,
      JPG: 6,
      jpeg: 6,
      JPEG: 6,
      bmp: 6,
      BMP: 6,
      tiff: 6,
      gif: 6,
      GIF: 6,
      xlsx: 7,
      ppt: 8
    };

    let commentType = fileType[fileExtension] ? fileType[fileExtension] : 1;

    var comment = {
      commentText: name,
      taskId: this.props.currentTask.taskId,
      commentType,
      fileCommentId: docsId,
      commentText: text.includes("<p></p>") ? "" : text
    };
    this.props.saveUserComment(
      comment,
      this.props.currentTask,
      data => {
        if (data && data.status === 200) {
          this.setState(
            {
              fileAttachment: null,
              fileAttachmentData: null
            },
            () => {
              this.props.AddNewTaskCommentData(data.data);
            }
          );
        }
      },
      error => {
        if (error.status === 500) {
          // self.showSnackBar('Server throws error','error');
        }
      }
    );
  };
  handleDeleteAttachment = () => {
    this.setState({
      attachment: "",
      fileAttachment: null,
      fileAttachmentData: null
    });
  };
  saveAttachment = (attachment, text) => {
    this.props.saveTaskAttachment(attachment, this.props.currentTask, data => {
      if (data && data.status === 200) {
        this.saveComment(
          data.data.docsName,
          data.data.docsId,
          attachment.docsType,
          text
        );
        
      }
    });
  };

  UploadFile = text => {
    const { fileAttachmentData, fileAttachment } = this.state;
    this.setState(
      {
        fileAttachment: null,
        fileAttachmentData: null
      })
    this.props.uploadFileTextEditor(
      fileAttachmentData,
      response => {
        if (response) {
          fileAttachment.docsPath = response.data;
          this.saveAttachment(fileAttachment, text);
        }
      },
      //Failure
      response => {
        if (response) {
          this.showSnackBar(response.data, "error");
        }
      }
    );
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right"
        },
        variant: type ? type : "info"
      }
    );
  };
  handleDeleteTaskAttachment = () => {
    const comment = this.state.selectedDeleteComment;
    this.setState({ btnQuery: "progress" }, () => {
      //DeleteTaskAttachment
      this.props.DeleteTaskAttachment(
        comment.commentId,
        this.props.currentTask,
        response => {
          this.setState({ btnQuery: "", deleteDialogueStatus: false });
          if (response.data.totalComments < this.state.comments.length)
            this.props.DeleteTaskCommentData(comment.commentId);
          else
            this.props.UpdateTaskCommentData({
              response: response.data,
              id: comment.commentId
            });
        },
        error => {
          this.setState({ btnQuery: "", deleteDialogueStatus: false });
        }
      );
    });
  };

  _handleImageChange = e => {
    e.preventDefault();
    var data = new FormData();
    let reader = new FileReader();
    let file = e.target;
    let self = this;
    if (file.files.length > 0) {
      let type = file.files[0].name.match(/[0-9a-zA-Z]{1,5}$/gm)[0];
      if (type != null && type !== "exe") {
        reader.onload = () => {
          data.append("taskid", this.props.currentTask.taskId);
          data.append("userid", this.props.profileState.data.userId);
          data.append("File", file.files[0]);

          let attachment = {
            docsType: type,
            docsName: file.files[0].name,
            taskId: this.props.currentTask.taskId,
            fileSize: file.files[0].size,
            docsPath: null
          };
          this.setState({
            fileAttachmentData: data,
            fileAttachment: attachment
          });
        };
        reader.readAsArrayBuffer(file.files[0]);
      } else {
        this.setState({
          imageUploadFlag: false
        });
      }
    }
    this.setState({
      imageUploadFlag: false
    });
  };

  openDeleteCommentAttachmentDialogue = comment => {
    this.setState({
      deleteDialogueStatus: true,
      selectedDeleteComment: comment
    });
  };

  handleDeleteCommentDialogClose = () => {
    this.setState({ deleteDialogueStatus: false, selectedDeleteComment: null });
  };

  render() {
    const {
      classes,
      theme,
      currentTask,
      permission,
      isArchivedSelected,
      taskPer
    } = this.props;
    const { fileAttachment, deleteDialogueStatus, btnQuery } = this.state;
    let permissionAttachment = getCompletePermissionsWithArchieve(
      currentTask,
      permission,
      "attachment"
    );
    let newComments = this.state.comments || [];
    const deleteAttachment =taskPer.taskDetail.taskComments.taskattachment.isAllowDelete;
    newComments = newComments.map(x => {
      if (x.userId === this.props.profileState.data.userId) {
        return {
          id: x.userId,
          user: "self",
          fullName: x.fullName,
          userName: x.userName,
          avatar: x.pictureUrl,
          email : this.props.profileState.data.email,
          timeStamp: moment(x.comment.createdDate).fromNow(),
          commentId: x.comment.id,
          fileSize: x.comment.attachment
            ? x.comment.attachment.fileSize + "kb"
            : "0kb",
          attachment: x.comment.attachment
            ? x.comment.attachment.docsName
            : null,
          text: x.comment.commentText,
          filteType: x.comment.attachment
            ? x.comment.attachment.docsType
            : null,
          filtePath: x.comment.attachment ? x.comment.attachment.docsPath : null
        };
      } else {
        let selectedMember = this.props.profileState.data.member.allMembers.find(
          member => {
            return member.userId == x.userId;
          }
        ) || {};

        return {
          id: x.userId,
          user: "other",
          fullName: x.fullName,
          userName: x.userName,
          avatar: `${x.pictureUrl}`,
          commentId: x.comment.id,
          isOnline: selectedMember.isOnline,
          isOwner: selectedMember.isOwner,
          email: selectedMember.email,
          timeStamp: x.comment
            ? x.comment.createdDate
              ? moment(x.comment.createdDate).fromNow()
              : null
            : null,
          fileSize: x.comment
            ? x.comment.createdDate
              ? x.comment.attachment
                ? x.comment.attachment.fileSize + "kb"
                : "0kb"
              : null
            : null,
          attachment: x.comment
            ? x.comment.createdDate
              ? x.comment.attachment
                ? x.comment.attachment.docsName
                : null
              : null
            : null,
          text: x.comment
            ? x.comment.createdDate
              ? x.comment.commentText
              : null
            : null,
          filteType: x.comment
            ? x.comment.createdDate
              ? x.comment.attachment
                ? x.comment.attachment.docsType
                : null
              : null
            : null,
          filtePath: x.comment
            ? x.comment.createdDate
              ? x.comment.attachment
                ? x.comment.attachment.docsPath
                : null
              : null
            : null
        };
      }
    });
    const comments = newComments;
    return (
      <Fragment>
        <div className={classes.commentsCnt}>
          {comments.length <= 0 ? (
            <div style={{ display: "flex", height: "100%" }}>
              <EmptyState
                screenType="comments"
                heading={<FormattedMessage id="common.comment.no-conversation.label" defaultMessage="No Conversations Yet" />}
                style={{ width: "90%" }}
                message={
                  currentTask.isDeleted
                    ? ""
                    : <FormattedMessage id="common.comment.no-conversation.placeholder" defaultMessage="Start conversation with your team members by typing your comment below." />
                }
                button={false}
              />
            </div>
          ) : (
            <Scrollbars
              style={{ height: "100%" }}
              ref={c => {
                this.scrollComponent = c;
              }}
            >
              <div style={{ padding: "20px 20px 0 20px" }}>
                {comments.map((comment, i) => (
                  <div
                    key={i}
                    style={{
                      display: "flex",
                      alignItems: "end",
                      marginBottom: 20,
                      flexDirection:
                        comment.user == "self" ? "row-reverse" : "row"
                    }}
                  >
                    {comment.user === "self" ? (
                      <CustomAvatar
                        styles={{ marginLeft: 10 }}
                        personal
                        size="small"
                      />
                    ) : (
                      <CustomAvatar
                        otherMember={{
                          imageUrl: comment.avatar,
                          fullName: comment.fullName,
                          lastName: "",
                          email: comment.email,
                          isOnline: comment.isOnline
                        }}
                        styles={{ marginRight: 10 }}
                        size="small"
                      />
                    )}
                    <div className={classes.commentContentOuterCnt}>
                      <div
                        className={
                          comment.attachment
                            ? classes.commentAttachContentCnt
                            : classes.commentContentCnt
                        }
                      >
                        {ReactHtmlParser(comment.text)}
                        {comment.attachment ? (
                          <div className={classes.attachmentCnt}>
                            <img
                              className={classes.commentsAttachmentIcon}
                              src={
                                comment.filteType == "pdf"
                                  ? Icon.PdfIcon
                                  : comment.filteType == "xlsx"
                                  ? Icon.XlsxIcon
                                  : ""
                              }
                            />
                            <div>
                              <p className={classes.commentsFileName}>
                                <Truncate
                                  trimWhitespace={true}
                                  width={250}
                                  ellipsis={<span>...</span>}
                                >
                                  {comment.attachment.split("/").pop()}
                                </Truncate>
                              </p>
                              <p className={classes.commentsFileSize}>
                                {comment.fileSize}
                              </p>
                            </div>
                            <a
                              onClick={() =>
                                helper.DOWNLOAD_TEMPLATE(
                                  comment.filtePath,
                                  comment.attachment,
                                  null,
                                  "attachment"
                                )
                              }
                            >
                              <ArrowDownward
                                classes={{
                                  root: classes.attachmentDownloadIcon
                                }}
                                htmlColor={theme.palette.common.white}
                              />
                            </a>
                            {deleteAttachment && !isArchivedSelected ? (
                              <a
                                onClick={() =>
                                  this.openDeleteCommentAttachmentDialogue(
                                    comment
                                  )
                                }
                              >
                                <DeleteOutline
                                  classes={{
                                    root: classes.attachmentDeleteIcon
                                  }}
                                  htmlColor={theme.palette.secondary.medDark}
                                  fontSize="small"
                                />
                              </a>
                            ) : null}
                          </div>
                        ) : null}
                      </div>
                      <p
                        className={
                          comment.user == "self"
                            ? classes.selfTimeStamp
                            : classes.otherTimeStamp
                        }
                      >
                        {comment.timeStamp}
                      </p>
                    </div>
                  </div>
                ))}
              </div>
            </Scrollbars>
          )}
        </div>

        <div className={classes.textEditorCnt}>
          {!currentTask.isDeleted && taskPer.taskDetail.taskComments.cando ? (
            <TextEditor
              currentTask={currentTask}
              handleNewComments={this.handleNewComments}
              fileUpload={this._handleImageChange}
              deleteAttachment={this.handleDeleteAttachment}
              saveUserComment={this.props.saveUserComment}
              UploadFileComment={this.UploadFile}
              fileAttachment={fileAttachment}
              view={"Task"}
              uploadPermission={taskPer.taskDetail.taskComments.taskattachment.isAllowAdd}
            />
          ) : null}
        </div>
        <DeleteConfirmDialog
          open={deleteDialogueStatus}
          closeAction={this.handleDeleteCommentDialogClose}
          cancelBtnText="Cancel"
          successBtnText="Delete"
          alignment="center"
          headingText="Delete"
          successAction={this.handleDeleteTaskAttachment}
          msgText={`Are you sure you want to delete this attachment?`}
          btnQuery={btnQuery}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    taskCommentsDataState: state.taskCommentsData
  };
};

export default compose(
  withSnackbar,
  withRouter,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateTask,
    getTaskComments,
    SaveTaskComments,
    UpdateTaskCommentData,
    uploadFileTextEditor,
    saveTaskAttachment,
    saveUserComment,
    downloadDocs,
    deleteTaskAttachmentComment,
    DeleteTaskAttachment,
    DeleteTaskCommentData,
    AddNewTaskCommentData,
    updateTaskData
  })
)(CommentsTab);
