import React, { Component } from "react";
import { Editor } from "react-draft-wysiwyg";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { UpdateTask, uploadDescriptionImage, saveDescriptionImage } from "../../../redux/actions/tasks";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import "../../../assets/css/richTextEditor.css";
import withStyles from "@material-ui/core/styles/withStyles";
import taskDetailStyles from "./styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import {
  EditorState,
  convertToRaw,
  ContentState,
} from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import InputLabel from "@material-ui/core/InputLabel";
import ReactHtmlParser from 'react-html-parser';
import { taskDetailsHelpText } from "../../../components/Tooltip/helptext";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import {injectIntl, FormattedMessage} from "react-intl";
import helper from "../../../helper";
import DescriptionEditor from "../../../components/TextEditor/CustomTextEditor/DescriptionEditor/DescriptionEditor";

const MAX_LENGTH = 10000;

class TaskDescriptionEditor extends Component {
  constructor(props) {
    super(props);
    // const html = props.taskData.description || "";
    // const contentBlock = htmlToDraft(html);
    // if (contentBlock) {
    //   const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
    //   const editorState = EditorState.createWithContent(contentState);
    //   this.state = {
    //     editorState,
    //     EditMode: false,
    //     contentState
    //   };
    // }
    // this.handleEditClick = this.handleEditClick.bind(this);
    // this.uploadImageCallBack = this.uploadImageCallBack.bind(this);
    // this.handleClickAway = this.handleClickAway.bind(this);
    // this.onEditorStateChange = this.onEditorStateChange.bind(this);
    this.state = {
      editorState: props.taskData.description || "",
    }
  }
  componentDidMount() {
    let members = (this.props.profileState && this.props.profileState.data && this.props.profileState.data.member) ? this.props.profileState.data.member.allMembers : [];
    let memberList = members.map(x => {
      return {
        text: x.userName,
        value: x.userName,
        //  url: x.userName,
        userId: x.userId
      }
    })
    this.setState({ members: memberList })
  }

//  uploadImageCallBack(file) {
//     let obj= null;
//     var data = new FormData();
//     data.append("File", file);
//     data.append("taskid", this.props.taskData.taskId);
//     data.append("userid", this.props.profileState.data.userId);
//     return  (new Promise(
//       (resolve, reject) => {
//         uploadDescriptionImage(data)
//           .then(response => {
//             let fileObj = response.data[0];
//             resolve({ data: { link: `${fileObj.publicURL}` } });
//           })
//           .catch(response => {
//             reject(response);
//           })
//       }));
//   }

  // handleEditClick() {
  //   this.setState({ EditMode: true });
  // }
  // onEditorStateChange(editorState) {
  //   this.setState({
  //     editorState
  //   });
  // }
  // handleClickAway() {
  //   this.setState({
  //     EditMode: false
  //   });
  //   if (draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())) !== this.props.taskData.description) {
  //     let data = this.props.taskData;
  //     data.description = window.btoa(draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())));
  //     this.props.UpdateTask(data, (response) => {
  //       if (response.status === 200) {
  //         response = response.data.CalenderDetails;
  //         ;
  //       }
  //     })
  //   }
  // }
  // getLengthOfSelectedText = () => {
  //   const currentSelection = this.state.editorState.getSelection();
  //   const isCollapsed = currentSelection.isCollapsed();

  //   let length = 0;

  //   if (!isCollapsed) {
  //     const currentContent = this.state.editorState.getCurrentContent();
  //     const startKey = currentSelection.getStartKey();
  //     const endKey = currentSelection.getEndKey();
  //     const startBlock = currentContent.getBlockForKey(startKey);
  //     const isStartAndEndBlockAreTheSame = startKey === endKey;
  //     const startBlockTextLength = startBlock.getLength();
  //     const startSelectedTextLength = startBlockTextLength - currentSelection.getStartOffset();
  //     const endSelectedTextLength = currentSelection.getEndOffset();
  //     const keyAfterEnd = currentContent.getKeyAfter(endKey);
  //     if (isStartAndEndBlockAreTheSame) {
  //       length += currentSelection.getEndOffset() - currentSelection.getStartOffset();
  //     } else {
  //       let currentKey = startKey;

  //       while (currentKey && currentKey !== keyAfterEnd) {
  //         if (currentKey === startKey) {
  //           length += startSelectedTextLength + 1;
  //         } else if (currentKey === endKey) {
  //           length += endSelectedTextLength;
  //         } else {
  //           length += currentContent.getBlockForKey(currentKey).getLength() + 1;
  //         }

  //         currentKey = currentContent.getKeyAfter(currentKey);
  //       };
  //     }
  //   }

  //   return length;
  // }
  // handleBeforeInput = () => {

  //   const currentContent = this.state.editorState.getCurrentContent();
  //   const currentContentLength = currentContent.getPlainText('').length;
  //   const selectedTextLength = this.getLengthOfSelectedText();

  //   if (currentContentLength - selectedTextLength > MAX_LENGTH - 1) {

  //     return 'handled';
  //   }
  // }
  // handlePastedText = (pastedText) => {
  //   const currentContent = this.state.editorState.getCurrentContent();
  //   const currentContentLength = currentContent.getPlainText('').length;
  //   const selectedTextLength = this.getLengthOfSelectedText();

  //   if (currentContentLength + pastedText.length - selectedTextLength > MAX_LENGTH) {

  //     return 'handled';
  //   }
  // }
  handleTaskDescription = editorState => {
    this.setState({
      editorState: editorState.html,
    });
    // const {currentTask} =  this.state;
    // currentTask.description = data.html; /** function set the description i the state */
    // this.setState({currentTask, EditMode: true});
  };
  handleClickAwayTaskDescription = () => {
    const {taskData} =  this.props;
    const {editorState} =  this.state;
    // let taskData = {...currentTask};
    if (editorState != taskData.description) {
      taskData.description = editorState;
      this.props.UpdateTask(taskData, (response) => {
        this.setState({
          // currentTask: currentTask,
        });
      })
    }
  }

  render() {
    const { classes, theme, permission, intl } = this.props;
    const { editorState } = this.state;
    // const html = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
    return (
      <div>
        <InputLabel classes={{ root: classes.dropdownsLabel }}>
         <FormattedMessage id="common.description.label" defaultMessage="Description"/> 
          <CustomTooltip helptext={<FormattedMessage id="common.description.hint" defaultMessage={taskDetailsHelpText.descriptionHelpText}/>} iconType="help" position="static" />
        </InputLabel>
        <div>
          <DescriptionEditor
            defaultValue={editorState}
            onChange={this.handleTaskDescription}
            handleClickAway={this.handleClickAwayTaskDescription}
            placeholder={intl.formatMessage({
              id: "common.type.label",
              defaultMessage: "Type something",
            })}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile
  }
}


export default compose(
  withRouter,
  injectIntl,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    { UpdateTask, UpdateCalenderTask }
  )
)(TaskDescriptionEditor);
