import React from "react";
import  {Calendar, momentLocalizer, Views} from "react-big-calendar";
import { withStyles, withTheme } from "@material-ui/core/styles";
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import moment from "moment";
import 'react-big-calendar/lib/css/react-big-calendar.css';
// import "react-big-calendar/lib/addons/dragAndDrop/styles.less";
import "../../../assets/css/react-big-calendar.css";
import LeftArrow from "@material-ui/icons/ArrowLeft";
import RightArrow from "@material-ui/icons/ArrowRight";
import CustomIconButton from "../../../components/Buttons/IconButton";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import calendarStyles from "./styles";
import Grid from "@material-ui/core/Grid";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import { UpdateTasksFromCalender } from "../../../redux/actions/workspace";
import COLORCODES from "../../../components/constants/colorCodes";
import { UpdateTask, createNextOccurrence , updateTaskData } from "../../../redux/actions/tasks";
import TaskDetails from "../TaskDetails/TaskDetails";
import DefaultDialog from "../../../components/Dialog/Dialog";
import AddNewTaskForm from "../../AddNewForms/addNewTaskFrom";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { withSnackbar } from "notistack";
import { canAdd } from "../permissions";
import { FormattedMessage } from "react-intl";
import { taskDetailDialogState } from "../../../redux/actions/allDialogs";

const DragAndDropCalendar = withDragAndDrop(Calendar);
const localizer = momentLocalizer(moment);

class TaskCalendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: this.caldenderEvents(this.props.tasksState.data),
      isChanged: false,
      currentTask: null,
      currentMonth: null,
      currentTaskId: null,
      currentDate: moment(),
      currentMonth: null,
      currentDate: moment(),
      selectableTask: false,
      startDate: new Date(),
      endDate: new Date(),
      openRecurrenceConfirm: false,
      createReccurrenceDate: "",
      activeTaskTitle: "",
      T: "",
      btnQuery: "",
      currentTaskOccuranceId: "",
    };
  }
  componentDidUpdate(prevProps, prevState) {
    const { tasksState } = this.props;
    if (
      JSON.stringify(prevState.events) !== JSON.stringify(this.caldenderEvents(tasksState.data))
    ) {
      this.setState({
        events: this.caldenderEvents(tasksState.data),
        currentTask: tasksState.data.find(t => {
          return t.taskId == this.state.currentTaskId;
        }),
      });
    }
  }
  addNewTask = ({ start, end }) => {
    if (start && end) {
      if (this.props.taskPer.createTask.cando) {
        this.setState({
          startDate: start,
          endDate: end,
          selectableTask: true,
          events: [
            ...this.state.events,
            {
              start,
              end: new Date(moment(end))
            },
          ],
        });
      }
    }
  };
  renderAddNewTaskForm = () => {
    const { events, currentDate, startDate, endDate } = this.state;
    const calenderCurrentDate = new Date(moment(currentDate));

    let components = {
      toolbar: this.Toolbar,
    };
    return (
      <div style={{ height: 700, width: "100%" }}>
        <DefaultDialog title="Add Task" open={this.state.selectableTask} onClose={this.handleClose}>
          <AddNewTaskForm
            closeAction={this.handleClose}
            preFilledData={{
              startDate,
              endDate,
              moreDetails: true,
            }}
          />
        </DefaultDialog>
        <DragAndDropCalendar
          popup
          selectable
          localizer={localizer}
          components={components}
          events={events}
          onEventDrop={this.onEventDrop}
          eventPropGetter={this.eventStyle}
          resizable
          startAccessor="start"
          endAccessor="end"
          onEventResize={this.onEventResize}
          onSelectSlot={this.addNewTask}
          defaultView={Views.MONTH}
          defaultDate={calenderCurrentDate}
          onDoubleClickEvent={this.onDoubleClickEvent}
        />
      </div>
    );
  };
  getTaskPermission = task => {
    /** pushing the task permission object in the task , if task has linked with any project i-e projectId !== null then find the project and fetch the task permission in project object */
    const {
      projects,
      taskPer,
      workspaceStatus,
      workspaceLevelMeetingPer,
      workspaceLevelRiskPer,
      workspaceLevelIssuePer,
    } = this.props;
    if (task.projectId) {
      let attachedProject = projects.find(p => p.projectId == task.projectId);
      if (attachedProject) {
        task.taskPermission = attachedProject.projectPermission.permission.task;
        task.meetingPermission = attachedProject.projectPermission.permission.meeting;
        task.riskPermission = attachedProject.projectPermission.permission.risk;
        task.issuePermission = attachedProject.projectPermission.permission.issue;
        // return task;
      } else {
        task.taskPermission = taskPer;
        task.meetingPermission = workspaceLevelMeetingPer;
        task.riskPermission = workspaceLevelRiskPer;
        task.issuePermission = workspaceLevelIssuePer;
        // return task;
      }
      if (attachedProject && attachedProject.projectTemplate) {
        task.taskStatus = attachedProject.projectTemplate;
      } else {
        task.taskStatus = workspaceStatus;
      }
      return task;
    } else {
      task.taskPermission = taskPer;
      task.taskStatus = workspaceStatus;
      task.meetingPermission = workspaceLevelMeetingPer;
      task.riskPermission = workspaceLevelRiskPer;
      task.issuePermission = workspaceLevelIssuePer;
      return task;
    }
  };
  caldenderEvents(tasks) {
    const filteredTasks = tasks
      ? tasks.map(t => {
          let task = this.getTaskPermission(t);
          return {
            id: task.taskId,
            title: task.taskTitle,
            taskOccurance: false,
            allDay: false,
            taskColor: task.statusColor, //COLORCODES.STATUSCOLORS[task.status],
            start: new Date(
              this.props.calenderDateType === "Actual Start/End"
                ? task.actualStartDate
                : this.props.calenderDateType === "Planned Start/End"
                ? task.startDate
                : this.props.calenderDateType === "Creation Date"
                ? task.createdDate
                : null
            ),
            end:
              this.props.calenderDateType === "Actual Start/End"
                ? new Date(
                    task.actualDueDate
                      ? moment(task.actualDueDate)
                          .format("MM/DD/YYYY")
                      : moment(task.actualStartDate)
                          .format("MM/DD/YYYY") || null
                  )
                : this.props.calenderDateType === "Planned Start/End"
                ? new Date(
                    task.dueDate
                      ? moment(task.dueDate)
                          .format("MM/DD/YYYY")
                      : moment(task.startDate).format("MM/DD/YYYY") || null
                  )
                : this.props.calenderDateType === "Creation Date"
                ? new Date(moment(task.createdDate).format("MM/DD/YYYY hh:mm A"))
                : null,

            taskObj: task,
            borderRadius: "0px",
          };
        })
      : [];
    return filteredTasks;
  }

  updateCalenderTask(start, end, id) {
    const { calenderDateType } = this.props;
    let index = this.props.tasksState.data
      .map(function(e) {
        return e.taskId;
      })
      .indexOf(id);
    let obj = Object.assign({}, this.props.tasksState.data[index]);
    delete obj.CalenderDetails;
    const object = {};
    if (calenderDateType === "Actual Start/End") {
      object.actualDueDate = moment(new Date(end))
        .format("MM/DD/YYYY hh:mm:ss A");
      object.actualStartDate = moment(new Date(start)).format("MM/DD/YYYY hh:mm:ss A");
    } else {
      object.dueDate = moment(new Date(end))
        .format("MM/DD/YYYY hh:mm:ss A");
      object.startDate = moment(new Date(start)).format("MM/DD/YYYY hh:mm:ss A");
    }
    this.props.updateTaskData({task : obj , object });
    // this.props.UpdateTask(obj, () => {});
  }

  onEventDrop = ({ event, start, end, isAllDay: droppedOnAllDaySlot }) => {
    if (!event.taskOccurance) {
      const { events } = this.state;
      const idx = events.indexOf(event);
      const updatedEvent = { ...event, start, end };
      const nextEvents = [...events];
      nextEvents.splice(idx, 1, updatedEvent);
      this.setState({
        events: nextEvents,
      });

      this.updateCalenderTask(start, end, event.id);
    }
  };

  onEventResize = ({ event, start, end }) => {
    if (!event.taskOccurance) {
      const { events } = this.state;
      const nextEvents = events.map(existingEvent => {
        return existingEvent.id === event.id ? { ...existingEvent, start, end } : existingEvent;
      });

      this.setState({
        events: nextEvents,
      });
      this.updateCalenderTask(start, end, event.id);
    }
  };
  onDoubleClickEvent = (event, e) => {
    if (event.taskOccurance) {
      let permission = event.taskObj.taskPermission.taskDetail.repeatTaskOccurances.cando;
      permission
        ? this.setState({
            currentTaskOccuranceId: event.taskObj.taskId,
            createReccurrenceDate: event.start,
            openRecurrenceConfirm: true,
            activeTaskTitle: event.title,
          })
        : null;
    } else {
      let taskDetailsPer = event.taskObj.taskPermission.taskDetail.cando;
      taskDetailsPer
        ? this.setState({ currentTask: event.taskObj, currentTaskId: event.id }, ()=>{
          this.setState({
            currentTask:null, currentTaskId: null
          })
        })
        : null;
    }
  };
  eventStyle(event) {
    if (event.taskColor) {
      if (event.borderColor) {
        return {
          style: {
            background: event.taskColor,
            border: event.borderColor,
            color: event.color,
            borderRadius: event.borderRadius,
          },
          className: event.className,
        };
      } else {
        return {
          style: {
            background: event.taskColor,
            borderRadius: event.borderRadius,
          },
        };
      }
    } else {
      return {};
    }
  }
  closeTaskDetailsPopUp = () => {
    this.setState({ currentTask: null, currentTaskId: null });
  };
  handleClose = () => {
    this.setState({ selectableTask: false });
  };
  Toolbar = toolbar => {
    const { theme, classes } = this.props;

    const goToBack = () => {
      toolbar.date.setMonth(toolbar.date.getMonth() - 1);
      toolbar.onNavigate("prev");
      this.setState({ currentDate: toolbar.date });
    };

    const goToNext = () => {
      toolbar.date.setMonth(toolbar.date.getMonth() + 1);
      toolbar.onNavigate("next");
      this.setState({ currentDate: toolbar.date });
    };

    const goToCurrent = () => {
      const now = new Date();
      toolbar.date.setMonth(now.getMonth());
      toolbar.date.setYear(now.getFullYear());
      DefaultButton;
      toolbar.onNavigate("current");
      this.setState({ currentDate: toolbar.date });
    };

    return (
      <div className={classes.calendarToolbar}>
        <Grid container>
          <Grid item>
            <DefaultButton
              text={<FormattedMessage id="common.action.today.label" defaultMessage="Today" />}
              buttonType="Plain"
              onClick={goToCurrent}
            />
          </Grid>
          <Grid item classes={{ item: classes.NextBackBtnCnt }}>
            <CustomIconButton btnType="condensed" onClick={goToBack}>
              <LeftArrow
                htmlColor={theme.palette.secondary.dark}
                classes={{ root: classes.navigationArrow }}
              />
            </CustomIconButton>
            <span className={classes.toolbarCurrentDate}>{toolbar.label}</span>
            <CustomIconButton btnType="condensed" onClick={goToNext}>
              <RightArrow
                htmlColor={theme.palette.secondary.dark}
                classes={{ root: classes.navigationArrow }}
              />
            </CustomIconButton>
          </Grid>
        </Grid>
      </div>
    );
  };
  getTaskOccurances = tasks => {
    const dates = [];
    const task = tasks.filter(t => {
      return t.repeatDates ? t : null;
    });

    task.map(t => {
      t.repeatDates.map(r => {
        const obj = {
          startDate: r,
          title: t.taskTitle,
          id: t.taskId,
          taskObj: t,
        };
        dates.push(obj);
      });
    });
    const repeatOccurance = this.getOccuranceobj(dates);
    return repeatOccurance;
  };
  getOccuranceobj = dates => {
    const { classes } = this.props;
    const className = classes.repeatOccuranceStyle;
    const occuranceObj = dates.map(d => {
      return {
        id: d.startDate,
        taskOccurance: true,
        title: d.title,
        allDay: false,
        start: new Date(moment(d.startDate).format("MM/DD/YYYY hh:mm:ss A")),
        end: new Date(moment(d.startDate).format("MM/DD/YYYY hh:mm:ss A")),
        taskColor: "#f1f1f1",
        borderColor: "1px solid #cecece",
        color: "#7e7e7e",
        taskObj: d.taskObj,
        borderRadius: "0px",
        className: className,
      };
    });
    return occuranceObj;
  };
  MonthEvent = ({ event }) => {
    const { theme, classes } = this.props;

    return (
      <>
        {event.title}
        {event.taskOccurance ? (
          <SvgIcon
            viewBox="0 0 14 12.438"
            htmlColor={"#0090ff"}
            className={classes.recurrenceIcon}>
            <RecurrenceIcon />
          </SvgIcon>
        ) : null}
      </>
    );
  };
  handleDialogClose = name => {
    this.setState({
      openRecurrenceConfirm: false,
      createReccurrenceDate: "",
      activeTaskTitle: "",
      currentTaskOccuranceId: "",
    });
  };
  handleCreateReccureTask = () => {
    const { createReccurrenceDate, currentTaskOccuranceId } = this.state;
    const data = {
      TaskId: currentTaskOccuranceId,
      date: moment(createReccurrenceDate).format("MM/DD/YYYY hh:mm:ss A"),
    };
    this.setState({ btnQuery: "progress" });
    //Create Next task occurence
    this.props.createNextOccurrence(
      data,
      //Success
      () => {
        this.setState({ btnQuery: "" });
        this.handleDialogClose();
      },
      //Failure
      error => {
        this.setState({ btnQuery: "" });
        this.showSnackBar(error.data.message, "error");
      }
    );
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  render() {
    const {
      currentTask,
      events,
      currentDate,
      openRecurrenceConfirm,
      activeTaskTitle,
      btnQuery,
    } = this.state;

    let components = {
      toolbar: this.Toolbar,
      event: this.MonthEvent,
    };
    const calenderCurrentDate = new Date(moment(currentDate));

    const TaskOccurances = this.getTaskOccurances(this.props.tasksState.data);
    const tasks = events.concat(TaskOccurances);

    return (
      <div style={{ height: 700, width: "100%" }}>
        {openRecurrenceConfirm && (
          <ActionConfirmation
            open={openRecurrenceConfirm}
            closeAction={this.handleDialogClose}
            cancelBtnText="Cancel"
            successBtnText="Yes, Create Next Occurrence"
            alignment="center"
            headingText="Create Next Occurrence"
            iconType="recurrence"
            msgText={
              <>
                This task is part of "{activeTaskTitle}" schedule and does not exist yet.
                <br />
                <br />
                Would you like to create next occurrence?
              </>
            }
            successAction={this.handleCreateReccureTask}
            btnQuery={btnQuery}
          />
        )}
        {currentTask &&
          this.props.taskDetailDialogState(null,{
            id: currentTask.taskId,
            afterCloseCallBack: () => {
              this.setState({
                currentTask: null,
                currentTaskId: null,
              });
            },
          })}
        {/* {currentTask && (
          <TaskDetails
          closeTaskDetailsPopUp={this.closeTaskDetailsPopUp}
          currentTask={currentTask}
          /> 
        )} */}
        {this.state.selectableTask ? (
          this.renderAddNewTaskForm()
        ) : (
          <DragAndDropCalendar
            popup
            selectable
            localizer={localizer}
            components={components}
            events={tasks}
            onEventDrop={this.onEventDrop}
            eventPropGetter={this.eventStyle}
            resizable
            startAccessor="start"
            endAccessor="end"
            onEventResize={this.onEventResize}
            onSelectSlot={this.addNewTask}
            defaultView={Views.MONTH}
            defaultDate={calenderCurrentDate}
            onDoubleClickEvent={this.onDoubleClickEvent}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tasksState: state.tasks || [],
    permissions: state.workspacePermissions.data.task,
    taskPer: state.workspacePermissions.data.task,
    projects: state.projects.data || [],
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    workspaceLevelMeetingPer: state.workspacePermissions.data.meeting,
    workspaceLevelRiskPer: state.workspacePermissions.data.risk,
    workspaceLevelIssuePer: state.workspacePermissions.data.issue,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(calendarStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateCalenderTask,
    UpdateTasksFromCalender,
    UpdateTask,
    createNextOccurrence,
    taskDetailDialogState,
    updateTaskData
  })
)(TaskCalendar);
