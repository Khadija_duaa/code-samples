import React from "react"
import getTaskByPriorityType from "../../../helper/getTaskByPriorityType";
import getTaskByStatusType from "../../../helper/getTaskByStatusType";
import getTaskByProjectType from "../../../helper/getTaskByProjectType";
import { getTaskProjectName } from "../../../helper/getTaskProjectName";
import {columns} from '../constants';
import Typography from "@material-ui/core/Typography";

let status = [
  "Not Started",
  "In Progress",
  "In Review",
  "Completed",
  "Cancelled"
];
let priority = [
  "", // Set first value empty to get right index because priority values are 1-4 in backend
  "Critical",
  "High",
  "Medium",
  "Low",
];
function translatedText(name,intl) {
 switch(name) {
        case "Not Started":
          name = intl.formatMessage({id:"task.common.status.dropdown.not-started",defaultMessage:"Not Started"});
          break;
          case "In Progress":
            name = intl.formatMessage({id:"task.common.status.dropdown.in-progress",defaultMessage:"In Progress"});
            break;
            case "In Review":
              name = intl.formatMessage({id:"task.common.status.dropdown.in-review",defaultMessage:"In Review"});
              break;
              case "Completed":
                name = intl.formatMessage({id:"task.common.status.dropdown.completed",defaultMessage:"Completed"});
                break;
                case "Cancelled":
                  name = intl.formatMessage({id:"task.common.status.dropdown.cancel",defaultMessage:"Cancelled"});
                  break;
                case "Critical":
                  name = intl.formatMessage({id:"task.common.priority.dropdown.critical", defaultMessage:"Critical"})
                  break;
                  case "High":
                    name = intl.formatMessage({id:"task.common.priority.dropdown.high", defaultMessage:"High"})
                    break;
                    case "Low":
                      name = intl.formatMessage({id:"task.common.priority.dropdown.low", defaultMessage:"Low"})
                      break;
                      case "Medium":
                        name = intl.formatMessage({id:"task.common.priority.dropdown.medium", defaultMessage:"Medium"})
                        break;
                        case "":
                        name = intl.formatMessage({id:"common.others.label" , defaultMessage:"Others"});
                        break;
 }
 return name;
}
export function GroupingRowRenderer(param, tasks, classes,intl=null, members) { // Grouping row renderer 

  const { name, columnGroupName } = param;
  let taskCount;
  let value;
  let templateName = false;
  switch (columnGroupName) { // Switch statement used to calculate countes of the tasks according to the grouping selected
    case ('priority'):
      taskCount = getTaskByPriorityType(tasks, name) //Function to return tasks based on priority
      value = intl != null ? translatedText(priority[name],intl) : priority[name];
      break; 
    case ('status'): {
      taskCount = getTaskByStatusType(tasks, name); //Function to return tasks based on task status
      // value = intl != null ? translatedText(status[name],intl) : status[name];
      if(taskCount.length > 0) {value = taskCount[0].statusTitle; templateName = taskCount[0].taskStatus.category == 3 ? `Custom (${taskCount[0].taskStatus.name})` : taskCount[0].taskStatus.name;  }
      else status[name];
    }
      break;
    case ('project'): {
      taskCount = getTaskByProjectType(tasks, name); //Function to return tasks based on task project
      value = intl != null ? translatedText(getTaskProjectName(name), intl) : getTaskProjectName(name);
    }
    break;
    case 'assigneeId':
        const assigneeUserObj = members.find(m => m.userId === name);
        taskCount = tasks.filter(task => {
          return task.assigneeId === name 
      })
      
        value = assigneeUserObj.fullName
      break;
    default:
      break;
  }
  return <div className={classes.groupingRow} {...param.renderer}>
    <div className={classes.groupingRowTextCnt}>
      <Typography variant="body1" className={classes.groupingRowText}>{value == "null" || !value ? "Others" : `${value} ${templateName ? `(${templateName})` : ""}`}</Typography>
      <span className={classes.groupingCount}>{taskCount && taskCount > 999 ? "999+" : taskCount ? taskCount.length : null}</span>
    </div>
  </div>
}
