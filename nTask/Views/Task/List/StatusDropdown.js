import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { BulkUpdateTask } from "../../../redux/actions/tasks";
import { withStyles, withTheme } from "@material-ui/core/styles";
import menuStyles from "../../../assets/jss/components/menu";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import DoneIcon from "@material-ui/icons/Done";
import RoundIcon from "@material-ui/icons/Brightness1";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import combineStyles from "../../../utils/mergeStyles";
import itemStyles from "../Grid/styles";
import Button from "@material-ui/core/Button";
import listStyles from "./styles";
import { FormattedMessage, injectIntl } from "react-intl";
import helper from "../../../helper";
class StatusDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      name: [],
      selectedStatus: false,
      checked: [],
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleClose(event) {
    this.setState({ open: false });
  }
  handleClick(event, placement) {
    const { currentTarget } = event;
    const { taskPer } = this.props;
    if (taskPer.taskDetail.editTaskStatus.isAllowEdit) {
      this.setState(state => ({
        open: state.placement !== placement || !state.open,
        placement,
      }));
    }
  }
  handleToggle = value => () => {
    let status = ["Not Started", "In Progress", "In Review", "Completed", "Cancelled"];
    if (this.state.open) {
      this.handleClose();
      this.setState(
        {
          checked: [value],
        },
        () => {
          const type = helper.RETURN_BULKACTIONTYPES("Status");
          let data = {
            type,
            taskIds: this.props.selectedIdsList,
            status: status.indexOf(value),
          };

          this.props.BulkUpdateTask(data, response => {
            this.handleClose();
            this.props.isBulkUpdated();
          });
        }
      );
    }
  };

  render() {
    const { classes, theme, intl } = this.props;
    const { open, placement } = this.state;
    const statusColor = theme.palette.taskStatus;
    const ddData = [
      {
        label: intl.formatMessage({
          id: "task.common.status.dropdown.not-started",
          defaultMessage: "Not Started",
        }),
        name: "Not Started",
        color: statusColor.NotStarted,
      },
      {
        label: intl.formatMessage({
          id: "task.common.status.dropdown.in-progress",
          defaultMessage: "In Progress",
        }),
        name: "In Progress",
        color: statusColor.InProgress,
      },
      {
        label: intl.formatMessage({
          id: "task.common.status.dropdown.in-review",
          defaultMessage: "In Review",
        }),
        name: "In Review",
        color: statusColor.InReview,
      },
      {
        label: intl.formatMessage({
          id: "task.common.status.dropdown.completed",
          defaultMessage: "Completed",
        }),
        name: "Completed",
        color: statusColor.Completed,
      },
      {
        label: intl.formatMessage({
          id: "task.common.status.dropdown.cancel",
          defaultMessage: "Cancelled",
        }),
        name: "Cancelled",
        color: statusColor.Cancelled,
      },
    ];
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <Button
            variant="outlined"
            style={{ borderRadius: 0, borderLeft: "none" }}
            classes={{ outlined: classes.BulkActionBtn }}
            onClick={event => {
              this.handleClick(event, "bottom-start");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}>
            <FormattedMessage id="common.bulk-action.status" defaultMessage="Select Status" />
          </Button>
          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            anchorRef={this.anchorEl}
            list={
              <List>
                <ListItem disableRipple={true} classes={{ root: classes.menuHeadingItem }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="common.bulk-action.status"
                        defaultMessage="Select Status"
                      />
                    }
                    classes={{ primary: classes.menuHeadingItemText }}
                  />
                </ListItem>
                {ddData.map(value => (
                  <ListItem
                    key={value.name}
                    button
                    disableRipple={true}
                    onClick={this.handleToggle(value.name)}
                    className={`${classes.menuItem} ${this.state.checked.indexOf(value.name) !== -1 ? classes.selectedValue : ""
                      }`}>
                    <RoundIcon htmlColor={value.color} classes={{ root: classes.statusIcon }} />
                    <ListItemText
                      primary={value.label}
                      classes={{
                        primary: classes.statusItemText,
                      }}
                    />
                  </ListItem>
                ))}
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(combineStyles(itemStyles, menuStyles, listStyles), {
    withTheme: true,
  }),
  connect(null, { BulkUpdateTask })
)(StatusDropdown);
