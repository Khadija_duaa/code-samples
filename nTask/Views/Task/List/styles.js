const listStyles = theme => ({
  BulkActionsCnt: {
    marginLeft: 62,
    marginBottom: 15,
    position: "absolute",
    top: 0,
    zIndex: 1,
  },
  BulkActionBtn: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    textTransform: "capitalize",
    fontWeight: theme.typography.fontWeightLight,
    color: theme.palette.text.primary,
    background: theme.palette.background.default,
  },
  shortcutText: {
    color: theme.palette.text.secondary,
    fontSize: "12px !important",
  },
  addNewTaskTextInput: {
    borderRadius: 0,
    marginTop: "-16px",
  },
  addNewTaskText: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
  },
  addTaskInputClearCnt: {
    position: "absolute",
    right: 10,
    zIndex: 1,
    top: 12,
  },
  addTaskInputClearText: {
    marginRight: 10,
    lineHeight: "26px",
    color: theme.palette.text.light,
    fontSize: "12px !important",
  },
  TableActionBtnDD: {
    position: "absolute",
    right: 26,
    zIndex: 5,
  },
  smallBtnIcon: {
    fontSize: "18px !important",
    color: theme.palette.secondary.light,
    marginRight: 5,
  },
  attachmentIcon: {
    fontSize: "22px !important",
    color: theme.palette.secondary.light,
    marginRight: 5,
  },
  commentsIcon: {
    fontSize: "22px !important",
    marginRight: 5,
  },
  commentsIconColorDim: {
    color: theme.palette.secondary.light,
  },
  commentsIconColorRed: {
    color: theme.palette.error.dark,
  },
  smallIconBtnCount: {
    fontSize: "16px !important",
    marginLeft: 3,
    lineHeight: "normal",
  },
  statusItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular,
  },
  statusMenuItemCnt: {
    padding: "2px 0 2px 16px",
    fontSize: "12px !important",
    height: 20,
  },
  statusMenuItemSelected: {
    background: `transparent !important`,
    "&:hover": {
      background: `transparent !important`,
    },
  },
  taskListTitleText: {
    display: "inline-block",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    lineHeight: "normal",
    fontSize: "12px !important",
  },

  taskListTitleTextCnt: {
    display: "flex",
    justifyContent: "space-between",
    height: 54,
    paddingLeft: 31,
    borderRadius: "4px 0 0 4px",
    alignItems: "center",
  },
  selectError: {
    fontSize: "12px !important",
    position: "absolute",
    top: -18,
    left: 10,
    color: theme.palette.text.danger,
  },
  recurrenceIcon: {
    fontSize: "18px !important",
  },
  unplannedMain: {
    fontFamily: theme.typography.fontFamilyLato,
    background: theme.palette.common.white,
    width: 356,
    height: "auto",
    top: 250,
    left: 200,
    padding: 20,
    position: "absolute",
    borderRadius: 4,
    boxShadow: `-1px 1px 5px -2px rgba(0,0,0,0.32)`,
  },
  unplannedRoot: {
    backgroundColor: "rgba(0, 0, 0, 0)",
  },
  groupingRow: {
    background: theme.palette.background.items,
  },
  groupingRowTextCnt: {
    display: "flex",
    alignItems: "center",
  },
  groupingCount: {
    padding: "4px 9px",
    marginLeft: 10,
    color: theme.palette.text.primary,
    fontSize: "14px !important",
    borderRadius: 12,
    background: theme.palette.background.dark,
  },
  groupingRowText: {
    color: theme.palette.text.primary,
  },  
});

export default listStyles;
