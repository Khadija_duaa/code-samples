import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles/";
import SearchDropdown from "../../../components/Menu/SearchMenu";
import itemStyles from "../Grid/styles";
import Button from "@material-ui/core/Button";
import listStyles from "./styles";
import combineStyles from "../../../utils/mergeStyles";
import { FormattedMessage } from "react-intl";

class ChangeProjectDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      selectedProject: "Select Project",
      ddData: [],
      selectedTaskIds: [],
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  componentDidMount() {
    let project = this.props.projectsState.data;
    project = project.length
      ? project.map((x) => {
        return { id: x.projectId, value: { name: x.projectName } };
      })
      : [];
    this.setState({ ddData: project });
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    return { selectedTaskIds: nextProps.selectedIdsList };
  }
  handleClose(event) {
    this.setState({ open: false });
  }
  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleSelect(value) {
    this.setState({ selectedProject: value });
  }
  render() {
    const { classes, theme, } = this.props;
    const { open, placement, ddData, selectedTaskIds } = this.state;

    return (
      <Fragment>
      <Button
          variant="outlined"
          style={{ borderRadius: 0, borderLeft: "none" }}
          onClick={(event) => {
            this.handleClick(event, "bottom-start");
          }}
          classes={{ outlined: classes.BulkActionBtn }}
          buttonRef={(node) => {
            this.anchorEl = node;
          }}
        >
          <FormattedMessage
            id="common.bulk-action.changeProject"
            defaultMessage="Change Project"
          />
        </Button>
        
        <SearchDropdown
          placement={placement}
          open={ open}
          title={
            <FormattedMessage id="project.label" defaultMessage="Projects" />
          }
          closeAction={this.handleClose}
          selectAction={this.handleSelect}
          searchQuery={["id", "value.name"]}
          data={ddData}
          isProjectBulk={true}
          isBulkUpdated={this.props.isBulkUpdated}
          taskIds={selectedTaskIds}
          anchorRef={ this.anchorEl}
        />
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    projectsState: state.projects,
  };
};

export default compose(
  withRouter,
  withStyles(combineStyles(itemStyles, listStyles), { withTheme: true }),
  connect(mapStateToProps, {})
)(ChangeProjectDropDown);
