import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { UpdateTask, BulkUpdateTask } from "../../../redux/actions/tasks";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "../Grid/styles";
import SearchDropdown from "../../../components/Menu/SearchMenu";
import Button from "@material-ui/core/Button";
import listStyles from "./styles";
import combineStyles from "../../../utils/mergeStyles";
import helper from "../../../helper";
import { generateUsername } from "../../../utils/common";
import { generateActiveMembers } from "../../../helper/getActiveMembers";
import { FormattedMessage } from "react-intl";

class AssigneeList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      allMembers: [],
      selectedTaskIds: [],
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  componentDidMount() {
    let allMembers = generateActiveMembers().map((x) => {
      const name = generateUsername(x.email, x.userName, x.fullName, null);
      return {
        id: x.userId,
        value: {
          name: name,
          avatar: x.imageUrl,
          email: x.email,
          fullName: x.fullName,
          isOnline: x.isOnline,
        },
        isActiveMember: x.isActive,
      };
    });
    this.setState({ allMembers });
  }
  componentDidUpdate(prevProps) {
    const { allMembers } = this.props.profileState.data.member;
    if (
      JSON.stringify(allMembers) !==
      JSON.stringify(prevProps.profileState.data.member.allMembers)
    ) {
      let members = allMembers
        ? allMembers.map((x) => {
            const name = generateUsername(
              x.email,
              x.userName,
              x.fullName,
              null
            );
            return {
              id: x.userId,
              value: {
                name: name,
                avatar: x.imageUrl,
                email: x.email,
                fullName: x.fullName,
                isOnline: x.isOnline,
              },
              isActiveMember: x.isActive,
            };
          })
        : [];
      this.setState({ allMembers: members });
    }
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    return { selectedTaskIds: nextProps.selectedIdsList };
  }
  handleClose(data) {
    if (this.state.open) {
      this.setState({ open: false }, () => {
        if (data && data.checkBulkAssignee) {
          const type = helper.RETURN_BULKACTIONTYPES("AddAssignee");
          let updatedTasks = {
            type,
            taskIds: data.taskIds,
            assigneIds: data.idList,
          };
          this.props.BulkUpdateTask(updatedTasks, () => {
            this.props.isBulkUpdated();
          });
        } else {
          this.setState({ open: false });
        }
      });
    }
  }
  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  render() {
    const { classes, theme } = this.props;
    const { open, placement, allMembers, selectedTaskIds } = this.state;
    const ddData = allMembers;
    return (
      <Fragment>
        <Button
          variant="outlined"
          style={{ borderRadius: 0, borderLeft: "none" }}
          classes={{ outlined: classes.BulkActionBtn }}
          onClick={(event) => {
            this.handleClick(event, "bottom-start");
          }}
          buttonRef={(node) => {
            this.anchorEl = node;
          }}
        >
          <FormattedMessage
            id="common.bulk-action.addAssignee"
            defaultMessage="Add Assignee"
          />
        </Button>
        <SearchDropdown
          placement={placement}
          open={open}
          title={<FormattedMessage
            id="common.bulk-action.addAssignee"
            defaultMessage="Add Assignee"
          />}
          closeAction={this.handleClose}
          searchQuery={["id", "value.name"]}
          anchorRef={this.anchorEl}
          data={ddData}
          avatar={true}
          checkedType="multi"
          taskIds={selectedTaskIds}
          isAssigneeBulk={true}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  withStyles(combineStyles(itemStyles, listStyles), { withTheme: true }),
  connect(mapStateToProps, { UpdateTask, BulkUpdateTask })
)(AssigneeList);
