import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Checkbox from "@material-ui/core/Checkbox";
import DoneIcon from "@material-ui/icons/Done";
import { Scrollbars } from "react-custom-scrollbars";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import { FormattedMessage, injectIntl } from "react-intl";

let Default_Column_Count = 6;
let total = Default_Column_Count;
const screenSize = 1380;
let checkedItems = [
  "ID",
  "Task",
  "Status",
  "Planned Start/End",
  "Actual Start/End",
  "Priority",
  "Project",
  "Progress",
];
class TableActionDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "top-start",
      pickerOpen: false,
      pickerPlacement: "",
      checked: ["Task"],
      isLoaded: false,
      loggedInTeam: "",
      isFirstLoad: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handleClick(event, placement) {
    event.stopPropagation();
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  componentDidMount() {
    this.setState({
      checked: checkedItems,
      loggedInTeam: this.props.profileState.data.loggedInTeam,
      isFirstLoad: true,
    });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      nextProps.itemOrderState.data.taskColumnOrder &&
      nextProps.itemOrderState.data.taskColumnOrder.length
    ) {
      let accessProject = teamCanView("projectAccess");
      checkedItems = nextProps.itemOrderState.data.taskColumnOrder;
      checkedItems = accessProject
        ? nextProps.itemOrderState.data.taskColumnOrder
        : nextProps.itemOrderState.data.taskColumnOrder.filter(
          (i) => i !== "Project"
        );
    }
    if (window.innerWidth <= screenSize) {
      checkedItems = checkedItems.slice(0, Default_Column_Count);
      total = 4;
    } else {
      total = Default_Column_Count;
    }
    if (prevState.loggedInTeam !== nextProps.profileState.data.loggedInTeam) {
      if (!prevState.checked.every((item) => checkedItems.includes(item))) {
        nextProps.selectedAction(checkedItems, true);
        return {
          isLoaded: true,
          checked: checkedItems,
          loggedInTeam: nextProps.profileState.data.loggedInTeam,
          isFirstLoad: false,
        };
      }
    }

    if (!prevState.isLoaded && prevState.isFirstLoad) {
      nextProps.selectedAction(checkedItems, prevState.isFirstLoad);
      return {
        isLoaded: true,
        checked: checkedItems,
        isFirstLoad: false,
      };
    }
    return { checked: prevState.checked };
  }

  handleToggle = (e, value) => {
    e.stopPropagation();
    const { checked } = this.state;
    const { selectedAction } = this.props;
    this.setState({
      isLoaded: true,
    });
    if (checked.length <= total + 2) {
      const currentIndex = checked.indexOf(value);
      const newChecked = [...checked];

      if (currentIndex === -1 && checked.length < total + 1) {
        newChecked.push(value);
      } else if (currentIndex !== -1) {
        // if (currentIndex === 1) {
        //   return;
        // }
        newChecked.splice(currentIndex, 1);
      }
      if (this.state.checked.length !== newChecked.length) {
        this.setState(
          {
            checked: newChecked,
          },
          () => {
            selectedAction(this.state.checked);
          }
        );
      }
    } else {
      return false;
    }
  };
  translate = (value) => {
    let id = "";
    switch (value) {
      case "ID":
        id = "common.ellipses-columns.id";
        break;
      case "Status":
        id = "common.ellipses-columns.status";
        break;
      case "Planned Start/End":
        id = "common.ellipses-columns.planned-start-end";
        break;
      case "Actual Start/End":
        id = "common.ellipses-columns.actual-start-end";
        break;
      case "Priority":
        id = "common.ellipses-columns.priority";
        break;
      case "Project":
        id = "common.ellipses-columns.project";
        break;
      case "Progress":
        id = "common.ellipses-columns.progress";
        break;
      case "Assignee":
        id = "common.ellipses-columns.assignee";
        break;
      case "Time Logged":
        id = "common.ellipses-columns.timelogged";
        break;
      case "Comments":
        id = "common.ellipses-columns.comment";
        break;
      case "Attachments":
        id = "common.ellipses-columns.attachment";
        break;
      case "Meetings":
        id = "common.ellipses-columns.meetings";
        break;
      case "Issues":
        id = "common.ellipses-columns.issues";
        break;
      case "Risks":
        id = "common.ellipses-columns.risks";
        break;
      case "Creation Date":
        id = "common.ellipses-columns.creationdate";
        break;
      case "Created By":
        id = "common.ellipses-columns.createdby";
        break;
    }
    return id == "" ? value : <FormattedMessage id={id} defaultMessage={value} />
  }
  render() {
    const { classes, theme, selectedColor, colorChange } = this.props;
    const { open, placement, checked } = this.state;
    const ddData = [
      "ID",
      "Status",
      "Planned Start/End",
      "Actual Start/End",
      "Priority",
      "Project",
      "Progress",
      "Assignee",
      "Time Logged",
      "Comments",
      "Attachments",
      "Meetings",
      "Issues",
      "Risks",
      "Creation Date",
      "Created By",
    ];
    // const ddData = [ "Task",  "Status", "Start Date", "Due Date", "Priority", "Project", "Assignee", "Progress",  "Time Logged", "Comments", "Meetings", "Issues", "Risks", "Creation Date"   ];
    const accessProject = teamCanView("projectAccess");
    const accessRisk = teamCanView("riskAccess");
    // const checkedLength = accessProject ? 2 : 3;
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomIconButton
            btnType="condensed"
            onClick={(event) => {
              this.handleClick(event, "bottom-end");
            }}
            id="taskListCustomizeBtn"
            buttonRef={(node) => {
              this.anchorEl = node;
            }}
          >
            <MoreVerticalIcon
              htmlColor={theme.palette.secondary.medDark}
              style={{ fontSize: "24px" }}
            />
          </CustomIconButton>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            checkedType="multi"
            anchorRef={this.anchorEl}
            list={
              <Scrollbars autoHide style={{ height: 250 }}>
                <List>
                  <ListItem
                    disableRipple={true}
                    classes={{ root: classes.menuHeadingItem }}
                  >
                    <ListItemText
                      primary={`${checked.length -
                        1}/${total} ${this.props.intl.formatMessage({
                          id: "common.selected.label",
                          defaultMessage: "Selected",
                        })}`}
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map((value) =>
                    (!accessProject && value == "Project") ||
                      (!accessRisk && value == "Risks") ? null : (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        className={`${classes.MenuItem} ${this.state.checked.indexOf(value) !== -1
                            ? classes.selectedValue
                            : ""
                          }`}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={(e) => this.handleToggle(e, value)}
                      >
                        <ListItemText
                          primary={this.translate(value)}
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                      </ListItem>
                    )
                  )}
                </List>
              </Scrollbars>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    itemOrderState: state.itemOrder,
    profileState: state.profile,
  };
};

export default compose(
  injectIntl,
  withRouter,
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {})
)(TableActionDropDown);
