import React, { Component, Fragment, useState } from "react";
import { findDOMNode } from "react-dom";
import ReactDataGrid from "react-data-grid";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import UpdateMatrixDialog from "../../../components/Matrix/updateMatrixDialog.cmp";
import CustomFieldsColumns from "../../CustomFieldsColumns/CustomFieldsColumns";
import isEqual from "lodash/isEqual";

// import ProjectDropDown from "../Grid/ProjectDropDown";
// import StatusDropdown from "../Grid/StatusDropdown";
import cloneDeep from "lodash/cloneDeep";
import { withStyles } from "@material-ui/core/styles";
import { Circle } from "rc-progress";
import Hotkeys from "react-hot-keys";
import Grid from "@material-ui/core/Grid";
import AddIcon from "@material-ui/icons/Add";
import CancelIcon from "@material-ui/icons/Cancel";
import SvgIcon from "@material-ui/core/SvgIcon";
import AttachmentIcon from "@material-ui/icons/Attachment";
import queryString from "query-string";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import FlagDropDown from "../Grid/FlagDropdown";
import BulkActions from "./BulkActions";
import AddTask from "./AddTask";
import listStyles from "./styles";
import IconButton from "../../../components/Buttons/IconButton";
// import TableActionDropDown from "./ActionDropDown";
import ColumnSelectionDropdown from "../../../components/Dropdown/SelectedItemsDropDown/index";
import helper from "../../../helper";
import TaskDetails from "../TaskDetails/TaskDetails";
import Starred from "../Grid/Starred";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import PublicLink from "../../../components/Dialog/PublicLink";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import SideDrawer from "../Drawer/Drawer";
import ConstantsPlan from "../../../components/constants/planConstant";
import { GetPermission } from "../../../components/permissions";
import { Data } from "react-data-grid-addons";
import { columns } from "../constants";
import { GroupingRowRenderer } from "./GroupingRowRenderer";
import {
  SaveTask,
  UpdateTask,
  FetchTasksInfo,
  CopyTask,
  DeleteTask,
  ArchiveTask,
  UnArchiveTask,
  BulkUpdateTask,
  getTaskWorkspace,
  UpdateTaskProgressBar,
  CheckAllTodoList,
  dispatchTask,
  UpdateTaskCustomField,
} from "../../../redux/actions/tasks";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import { UpdateCalenderTask, CopyCalenderTask } from "../../../redux/actions/calenderTasks";
import { FetchWorkspaceInfo, UpdateTaskColorFromGridItem } from "../../../redux/actions/workspace";
import { FetchUserInfo } from "../../../redux/actions/profile";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";

import { sortListData, getSortOrder } from "../../../helper/sortListData";

import TaskActionDropdown from "./TaskActionDropDown";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import CommentIcon from "@material-ui/icons/Comment";
import TimeIcon from "@material-ui/icons/AccessTime";
import BottomScrollListener from "react-bottom-scroll-listener";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import IssuesIcon from "../../../components/Icons/IssueIcon";
import RiskIcon from "../../../components/Icons/RiskIcon";
import Typography from "@material-ui/core/Typography";
import { calculateContentHeight } from "../../../utils/common";
import DefaultDialog from "../../../components/Dialog/Dialog";
import AddNewTaskForm from "../../AddNewForms/addNewTaskFrom";
import {
  getEditPermissionsWithArchieve,
  getTasksPermissions,
  getTasksPermissionsWithoutArchieve,
} from "../permissions";
import CustomRangeDatePicker from "../../../components/DatePicker/CustomRangeDatePicker";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import UnPlanned from "../../billing/UnPlanned/UnPlanned";
import Modal from "@material-ui/core/Modal";
import { nTaskSignalConnectionOpen } from "../../../utils/ntaskSignalR";
import { dispatchAddTodoItems } from "../../../redux/actions/todoItems";
import { FormattedMessage, injectIntl } from "react-intl";
import CircularIcon from "@material-ui/icons/Brightness1";
import { headerTranslations } from "../../../utils/headerTranslations";
import { getTemplate } from "../../../utils/getTemplate";
import TaskStatusChangeDialog from "../../../components/Templates/TaskStatusChangeDialog";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import getTableColumns from "../../../helper/getTableColumns";
import { getCustomFields } from "../../../helper/customFieldsData";
import { getIssuesPermissionsWithoutArchieve } from "../../Issue/permissions";
import IssueActionDropdown from "../../Issue/List/IssueActionDropDown";
import { UpdateIssueCustomField } from "../../../redux/actions/issues";
import { taskDetailDialogState } from "../../../redux/actions/allDialogs";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import isUndefined from "lodash/isUndefined";
import {TRIALPERIOD} from '../../constants/planConstant';

const {
  id,
  taskTitle,
  taskStatus,
  startDate,
  actualStartDate,
  taskPriority,
  projectName,
  progress,
  assignees,
  totalEffort,
  attachments,
  totalComment,
  meetings,
  issues,
  risks,
  createdDate,
  createdBy,
} = columns;

const {
  Draggable: { Container: DraggableContainer, RowActionsCell, DropTargetRowContainer },
  Data: { Selectors },
  Toolbar,
} = require("react-data-grid-addons");

const defaultColumnProperties = {
  resizable: true,
};
const RowRenderer = DropTargetRowContainer(ReactDataGrid.Row);
const Progress = (value, object) => {
  const overdue = helper.RETURN_OVER_DUE_DAYS_WITH_PROGRESS(
    value.row ? value.row.actualDueDateString : "",
    value.row ? value.row.progress : 0
  );
  let progress = 0;
  if (object.progress) {
    progress = object.progress;
  } else {
    progress = value.row ? value.row.progress : 0;
  }
  return (
    <div style={{ display: "flex", alignItems: "center" }}>
      <div style={{ width: 30, marginRight: 5, marginTop: 4 }}>
        <Circle
          percent={progress}
          strokeWidth="10"
          trailWidth=""
          trailColor="#dedede"
          strokeColor={overdue ? "#de133e" : "#30d56e"}
        />
      </div>
      <Typography variant="h6" align="center">
        {progress}%
      </Typography>
    </div>
  );
};

const increment = 20;
let Default_Column_Count = 6;
let total = Default_Column_Count;
const screenSize = 1380;

class TaskList extends React.Component {
  static defaultProps = { rowKey: "taskId" };

  constructor(props, context) {
    super(props, context);
    this.state = {
      repeatTaskDrawer: false,
      _columns: [],
      rows: [],
      publicLinkOpen: false,
      output: "",
      selectedIds: [],
      addTask: false,
      addNewForm: false,
      completeDetails: [],
      userId: "",
      value: "",
      progress: 0,
      taskId: "",
      isProgress: false,
      TaskError: false,
      TaskErrorMessage: "",
      isReorder: false,
      startDateOpen: false,
      dueDateOpen: false,
      newCol: [],
      showTaskDetails: false,
      detailsTaskId: "",
      loggedInTeam: "",
      isWorkspaceChanged: false,
      showRecords: increment,
      totalRecords: 0,
      newEntry: false,
      type: "comment",
      sortColumn: null,
      sortDirection: "NONE",
      showUnplanned: false,
      selectedTaskObj: [],
      openStatusDialog: false,
      newTemplateItem: null,
      oldStatusItem: null,
      columnddData: getTableColumns("task"),
      checkedItems: [
        "ID",
        "Task",
        "Status",
        "Planned Start/End",
        "Actual Start/End",
        "Priority",
        "Project",
        "Progress",
      ],
      markChecklistActionConf: false,
      markChecklistActionBtnQuery: "",
      selectedStatus: {},
      selectedTaskObjArr:[]
    };
    this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
    this.addTask = this.addTask.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addTaskInput = React.createRef();
    this.cancelAddTask = this.cancelAddTask.bind(this);
    this.handleCalculateProgress = this.handleCalculateProgress.bind(this);
    this.isUpdated = this.isUpdated.bind(this);
    this.showTaskDetailsPopUp = this.showTaskDetailsPopUp.bind(this);
    this.closeTaskDetailsPopUp = this.closeTaskDetailsPopUp.bind(this);
    this.handleScrollDown = this.handleScrollDown.bind(this);
    this.scrollListener = this.scrollListener.bind(this);
    this.BulkUpdate = this.BulkUpdate.bind(this);
  }

  getPermission = (task, key) => {
    /** function calling for getting permission */
    const { permissions } = this.props;
    return GetPermission.canEdit(
      task.isOwner,
      permissions,
      key
    ); /** passing current user owner , workspace permission and a key to check */
  };

  filterOnBasicsOfScreeenSize = param => {
    if (window.innerWidth <= screenSize) {
      total = 4;
      return param.slice(0, 4);
    } else {
      total = Default_Column_Count;
      return param;
    }
  };
  onColumnResize = (columnIndex, size) => {
    const { _columns } = this.state;
    if (size < 0) return;
    let columnWidth = JSON.parse(localStorage.getItem("columnWidthTask"));
    if (columnWidth) {
      columnWidth[_columns[columnIndex - 1].key] = {
        width: size,
      };
      localStorage.setItem("columnWidthTask", JSON.stringify(columnWidth));
    } else {
      let obj = {};
      obj[_columns[columnIndex - 1].key] = {
        width: size,
      };
      localStorage.setItem("columnWidthTask", JSON.stringify(obj));
    }
  };

  getColumns = () => {
    let columnWidth = JSON.parse(localStorage.getItem("columnWidthTask"));
    let _columns = [
      {
        key: id.key,
        name: id.name,
        cellClass: "firstColoumn",
        sortDescendingFirst: true,
        visible: true,
        resizable: true,
        width: columnWidth && columnWidth[id.key] ? columnWidth[id.key].width : 150,
        formatter: value => {
          return (
            <div
              className={this.props.classes.taskListTitleTextCnt}
              style={{
                borderLeft: `6px solid ${
                  value.row && value.row.colorCode == ""
                    ? "#fff"
                    : value.row
                    ? value.row.colorCode
                    : "#fff"
                }`,
              }}>
              <span className={this.props.classes.taskListTitleText}>
                {value.row ? value.row.uniqueId : ""}
              </span>
            </div>
          );
        },
        sortable: !teamCanView("advanceSortAccess"),
        columnOrder: 0,
      },
      {
        key: taskTitle.key,
        name: taskTitle.name,
        cellClass: "secondColoumn",
        visible: true,
        resizable: true,
        // width: 200,
        width: columnWidth && columnWidth[taskTitle.key] ? columnWidth[taskTitle.key].width : 400,
        sortDescendingFirst: true,
        formatter: value => {
          const isArchived = this.props.isArchivedSelected;
          return (
            <div style={{ display: "flex", alignItems: "center" }}>
              <Starred
                userId={this.props.profileState.data.userId}
                currentTask={value.row}
                disabled={isArchived}
                // style={{ position: "absolute", left: -25, top: -2 }}
              />

              <span
                title={value.row ? value.row.taskTitle : ""}
                className={this.props.classes.taskListTitleText}>
                {!isArchived &&
                  value.row &&
                  value.row.repeatTask &&
                  teamCanView("repeatTaskAccess") && (
                    <IconButton
                      btnType="transparent"
                      onClick={event => this.openRepeatTaskDrawer(event, value.row)}
                      style={{ padding: 5, marginRight: 2 }}>
                      <SvgIcon
                        viewBox="0 0 14 12.438"
                        htmlColor={this.props.theme.palette.primary.light}
                        className={this.props.classes.recurrenceIcon}>
                        <RecurrenceIcon />
                      </SvgIcon>
                    </IconButton>
                  )}
                {value.row ? value.row.taskTitle : ""}
              </span>
            </div>
          );
        },
        sortable: !teamCanView("advanceSortAccess"),
        columnOrder: 1,
      },
      {
        key: taskStatus.key,
        name: taskStatus.name,
        visible: true,
        resizable: true,
        width: columnWidth && columnWidth[taskStatus.key] ? columnWidth[taskStatus.key].width : 155,
        formatter: value => {
          const taskPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceTaskPer : this.props.permissionObject[value.row.ProjectPermissionSelector].task
              : this.props.workspaceTaskPer;
          let template = getTemplate(value.row);
          const taskStatusData = this.statusData(template ? template.statusList : false);
          // const taskStatusData = this.statusData(value.row ? value.row.taskStatus.statusList : null);
          let selectedStatus = value.row
            ? value.row.status == -1
              ? taskStatusData[0]
              : taskStatusData.find(item => item.value == value.row.status) || {}
            : {};
          return (
            <StatusDropdown
              onSelect={status => {
                this.handleStatusChange(status, value.row);
              }}
              iconButton={false}
              option={selectedStatus}
              style={{ marginRight: 8 }}
              options={taskStatusData}
              toolTipTxt={selectedStatus.label}
              disabled={
                this.props.isArchivedSelected ||
                !taskPermission.taskDetail.editTaskStatus.isAllowEdit
              }
              legacyButton
              writeFirst={false}
            />
          );
        },
        cellStyle: { background: "red" },
        cellClass: "dropDownCell",
        columnOrder: 2,
        sortable: !teamCanView("advanceSortAccess"),
      },

      {
        key: startDate.key,
        name: startDate.name,
        visible: true,
        resizable: true,
        width: columnWidth && columnWidth[startDate.key] ? columnWidth[startDate.key].width : 150,
        formatter: value => {
          const taskPermission =
            value && value.row && value.row.ProjectPermissionSelector
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceTaskPer : this.props.permissionObject[value.row.projectId].task
              : this.props.workspaceTaskPer;
          const canEdit = taskPermission.taskDetail.taskPlannedStartEndDate.isAllowEdit;

          return (
            <>
              <CustomRangeDatePicker
                label="Planned Start/End"
                placeholder={this.props.intl.formatMessage({
                  id: "common.date.placeholder",
                  defaultMessage: "Select Date",
                })}
                isArchivedSelected={this.props.isArchivedSelected}
                startDate={value.row ? value.row.startDateString : ""}
                endDate={value.row ? value.row.dueDateString : ""}
                startTime={value.row ? value.row.startTime : ""}
                endTime={value.row ? value.row.dueTime : ""}
                permission={this.props.isArchivedSelected || !canEdit}
                dateSaveAction={(startDate, endDate, startTime, endTime) =>
                  this.handleDateSave(startDate, endDate, value.row, "planned", startTime, endTime)
                }
                error={false}
              />
            </>
          );
        },
        columnOrder: 3,
        sortable: !teamCanView("advanceSortAccess"),
        cellClass: "dropDownCell",
      },

      {
        key: actualStartDate.key,
        name: actualStartDate.name,
        visible: true,
        resizable: true,
        width:
          columnWidth && columnWidth[actualStartDate.key]
            ? columnWidth[actualStartDate.key].width
            : 150,
        formatter: value => {
          const taskPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceTaskPer : this.props.permissionObject[value.row.projectId].task
              : this.props.workspaceTaskPer;
          const canEdit = taskPermission.taskDetail.taskActualStartEndDate.isAllowEdit;
          return (
            <>
              <CustomRangeDatePicker
                placeholder={this.props.intl.formatMessage({
                  id: "common.date.placeholder",
                  defaultMessage: "Select Date",
                })}
                startDate={value.row ? value.row.actualStartDateString : ""}
                endDate={value.row ? value.row.actualDueDateString : ""}
                startTime={value.row ? value.row.actualStartTime : ""}
                endTime={value.row ? value.row.actualDueTime : ""}
                permission={this.props.isArchivedSelected || !canEdit}
                dateSaveAction={(startDate, endDate, startTime, endTime) =>
                  this.handleDateSave(startDate, endDate, value.row, "actual", startTime, endTime)
                }
                error={false}
              />
            </>
          );
        },
        cellClass: "dropDownCell",
        sortable: !teamCanView("advanceSortAccess"),
        columnOrder: 4,
      },

      {
        key: taskPriority.key,
        name: taskPriority.name,
        visible: true,
        resizable: true,
        width:
          columnWidth && columnWidth[taskPriority.key] ? columnWidth[taskPriority.key].width : 95,
        formatter: value => {
          const taskPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceTaskPer : this.props.permissionObject[value.row.projectId].task
              : this.props.workspaceTaskPer;
          return (
            <FlagDropDown
              priority={value.row ? value.row.priority : 1}
              taskData={value.row ? value.row : ""}
              searchFilterApplied={this.props.searchFilterApplied}
              isArchivedSelected={this.props.isArchivedSelected}
              userId={this.state.userId}
              taskPer={taskPermission}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 5,

        sortable: !teamCanView("advanceSortAccess"),
      },
      {
        key: projectName.key,
        name: projectName.name,
        resizable: true,
        width:
          columnWidth && columnWidth[projectName.key] ? columnWidth[projectName.key].width : 240,
        visible: teamCanView("projectAccess"),
        formatter: value => {
          const taskPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceTaskPer : this.props.permissionObject[value.row.projectId].task
              : this.props.workspaceTaskPer;
          const permission = taskPermission.taskDetail.taskProject.isAllowEdit;
          const task = value.row ? value.row : [];
          // some times task effort comes in string and some times empty array so to check that below line of code is added
          const taskEffort = task.userTaskEffort ? task.userTaskEffort.length : task.userTaskEffort;
          return (
            <SearchDropdown
              initSelectedOption={task.projectId ? this.getSelectedProject(task) : []}
              obj={task}
              disableDropdown={!!(taskEffort || !permission || this.props.isArchivedSelected)}
              tooltip={!!taskEffort}
              key={task.projectId}
              tooltipText={
                <FormattedMessage
                  id="task.detail-dialog.project.hint1"
                  defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
                />
              }
              optionsList={this.generateProjectDropdownData(task)}
              singleSelect
              updateAction={this.handleSelectChange}
              selectedOptionHead={this.props.intl.formatMessage({
                id: "common.task-project.label",
                defaultMessage: "Task Project",
              })}
              allOptionsHead={this.props.intl.formatMessage({
                id: "project.label",
                defaultMessage: "Projects",
              })}
              buttonPlaceholder={
                <FormattedMessage
                  id="task.creation-dialog.form.project.placeholder"
                  defaultMessage="Select Project"
                />
              }
              buttonProps={{
                disabled: this.props.isArchivedSelected || !permission,
              }}
              TaskView={true}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 6,

        sortable: !teamCanView("advanceSortAccess"),
      },
      {
        key: progress.key,
        name: progress.name,
        resizable: true,
        formatter: value => {
          return Progress(value, this.state);
        },
        cellClass: "dropDownCell",
        columnOrder: 7,
        visible: true,
        width: columnWidth && columnWidth[progress.key] ? columnWidth[progress.key].width : 135,
        sortable: !teamCanView("advanceSortAccess"),
      },
      {
        key: assignees.key,
        name: assignees.name,
        align: "right",
        resizable: true,
        visible: true,
        width: columnWidth && columnWidth[assignees.key] ? columnWidth[assignees.key].width : 240,
        formatter: value => {
          const taskPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceTaskPer : this.props.permissionObject[value.row.projectId].task
              : this.props.workspaceTaskPer;
          const addPer = taskPermission.taskDetail.taskAssign.isAllowAdd;
          const delPer = taskPermission.taskDetail.taskAssign.isAllowDelete;
          return (
            <AssigneeDropdown
              assignedTo={value.row ? value.row.assigneeLists : []}
              updateAction={this.updateAssignee}
              isArchivedSelected={this.props.isArchivedSelected}
              obj={value.row}
              addPermission={addPer}
              deletePermission={delPer}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 8,
      },

      {
        key: totalEffort.key,
        name: totalEffort.name,
        visible: true,
        resizable: true,
        width:
          columnWidth && columnWidth[totalEffort.key] ? columnWidth[totalEffort.key].width : 165,
        formatter: value => {
          return (
            <DefaultButton buttonType="smallIconBtn">
              <TimeIcon classes={{ root: this.props.classes.smallBtnIcon }} />
              <Typography variant="body2" align="center">
                {value.row
                  ? helper.CHANGETIMEFORMAT(value.row.timeLogged)
                  : helper.CHANGETIMEFORMAT()}
              </Typography>
            </DefaultButton>
          );
        },

        cellClass: "dropDownCell",
        sortable: !teamCanView("advanceSortAccess"),
        columnOrder: 9,
      },
      {
        key: attachments.key,
        name: attachments.name,
        visible: true,
        resizable: true,
        width:
          columnWidth && columnWidth[attachments.key] ? columnWidth[attachments.key].width : 130,
        formatter: value => {
          return (
            <DefaultButton
              buttonType="smallIconBtn"
              onClick={() => this.setState({ type: "comment" })}>
              <AttachmentIcon classes={{ root: this.props.classes.attachmentIcon }} />
              <Typography variant="body2" align="center">
                {value.row && value.row.totalAttachment ? value.row.totalAttachment : 0}
              </Typography>
            </DefaultButton>
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 10,
      },
      {
        key: totalComment.key,
        name: totalComment.name,
        visible: true,
        resizable: true,
        width:
          columnWidth && columnWidth[totalComment.key] ? columnWidth[totalComment.key].width : 130,
        formatter: value => {
          return (
            <DefaultButton
              buttonType="smallIconBtn"
              onClick={() => this.setState({ type: "comment" })}>
              <CommentIcon
                className={`${this.props.classes.commentsIcon} ${
                  value.row && value.row.totalUnreadComment
                    ? this.props.classes.commentsIconColorRed
                    : this.props.classes.commentsIconColorDim
                }`}
              />
              <Typography
                variant="body2"
                align="center"
                className={`${
                  value.row && value.row.totalUnreadComment
                    ? this.props.classes.commentsIconColorRed
                    : this.props.classes.commentsIconColorDim
                }`}>
                {value.row && value.row.totalComment ? value.row.totalComment : 0}
              </Typography>
            </DefaultButton>
          );
        },

        columnOrder: 11,
        sortable: !teamCanView("advanceSortAccess"),
        cellClass: "dropDownCell",
      },
      {
        key: meetings.key,
        name: meetings.name,
        width: columnWidth && columnWidth[meetings.key] ? columnWidth[meetings.key].width : 130,
        resizable: true,
        visible: teamCanView("meetingAccess"),
        formatter: value => {
          return (
            <DefaultButton
              buttonType="smallIconBtn"
              onClick={() => this.setState({ type: "meeting" })}>
              <SvgIcon viewBox="0 0 17.031 17" className={this.props.classes.smallBtnIcon}>
                <MeetingsIcon />
              </SvgIcon>
              <Typography variant="body2" align="center">
                {value.row ? value.row.linkedMeetingsCount : ""}
              </Typography>
            </DefaultButton>
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 12,
        sortable: !teamCanView("advanceSortAccess"),
      },
      {
        key: issues.key,
        name: issues.name,
        visible: teamCanView("issueAccess"),
        width: columnWidth && columnWidth[issues.key] ? columnWidth[issues.key].width : 130,
        resizable: true,
        formatter: value => {
          return (
            <DefaultButton
              buttonType="smallIconBtn"
              onClick={() => this.setState({ type: "issue" })}>
              <SvgIcon viewBox="0 0 16 17.375" className={this.props.classes.smallBtnIcon}>
                <IssuesIcon />
              </SvgIcon>
              <Typography variant="body2" align="center">
                {value.row ? value.row.linkedIssuesCount : ""}
              </Typography>
            </DefaultButton>
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 13,
        sortable: !teamCanView("advanceSortAccess"),
      },

      {
        key: risks.key,
        name: risks.name,
        visible: teamCanView("riskAccess"),
        width: columnWidth && columnWidth[risks.key] ? columnWidth[risks.key].width : 130,
        resizable: true,
        formatter: value => {
          return (
            <DefaultButton
              buttonType="smallIconBtn"
              onClick={() => this.setState({ type: "risk" })}>
              <RiskIcon classes={{ root: this.props.classes.smallBtnIcon }} />
              <SvgIcon viewBox="0 0 18 15.75" className={this.props.classes.smallBtnIcon}>
                <RiskIcon />
              </SvgIcon>
              <Typography variant="body2" align="center">
                {value.row ? value.row.linkedRisksCount : ""}
              </Typography>
            </DefaultButton>
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 14,
        sortable: !teamCanView("advanceSortAccess"),
      },

      {
        key: createdDate.key,
        name: createdDate.name,
        visible: true,
        resizable: true,
        width:
          columnWidth && columnWidth[createdDate.key] ? columnWidth[createdDate.key].width : 150,
        formatter: value => {
          return (
            <Typography variant="body2" align="center">
              {value.row
                ? helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(value.row.createdDate)
                : helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(new Date())}
            </Typography>
          );
        },
        columnOrder: 15,
        sortable: !teamCanView("advanceSortAccess"),
        cellClass: "dropDownCell",
      },
      {
        key: createdBy.key,
        name: createdBy.name,
        width: columnWidth && columnWidth[createdBy.key] ? columnWidth[createdBy.key].width : 150,
        resizable: true,
        visible: true,
        formatter: value => {
          return (
            <Typography variant="body2" align="center">
              {value.row ? value.row.createdByName : ""}
            </Typography>
          );
        },
        columnOrder: 15,
        sortable: !teamCanView("advanceSortAccess"),
        cellClass: "dropDownCell",
      },
    ].map(c => ({ ...c, ...defaultColumnProperties }));
    let customColumns = getCustomFields(
      this.props.customFields,
      this.props.profileState.data,
      "task"
    ).filter(
      c => c.fieldType.toLowerCase() !== "checklist" && c.fieldType.toLowerCase() !== "matrix"
    );

    /** finding all custom fields realated to risk modules  */
    customColumns = customColumns.map((e, index) => {
      return {
        key: e.fieldName,
        name: e.fieldName,
        width: columnWidth && columnWidth[e.fieldName] ? columnWidth[e.fieldName].width : 250,
        resizable: true,
        formatter: value => {
          const taskPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceTaskPer : this.props.permissionObject[value.row.projectId].task
              : this.props.workspaceTaskPer;
          return (
            <CustomFieldsColumns
              column={e}
              rowValue={value}
              permission={taskPermission.taskDetail.customsFields}
              customFieldData={value.row.customFieldData}
              customFieldChange={this.customFieldChange}
              handleAddAssessmentDialog={this.handleAddAssessmentDialog}
              disabled={this.props.isArchivedSelected}
            />
          );
        },
        columnOrder: 10 + index,
        sortable: teamCanView("advanceSortAccess") ? false : true,
        cellClass: "dropDownCell",
      };
    });
    let arr = [..._columns, ...customColumns];

    let lastColumn = [
      {
        key: "optionList",
        visible: true,
        name: "",
        formatter: value => {
          const taskPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceTaskPer : this.props.permissionObject[value.row.projectId].task
              : this.props.workspaceTaskPer;
          return (
            <TaskActionDropdown
              task={value.row}
              deleteTaskFromArchiveList={this.props.deleteTaskFromArchiveList}
              filterUnArchiveTask={this.props.filterUnArchiveTask}
              CopyTask={this.props.CopyTask}
              DeleteTask={this.props.DeleteTask}
              ArchiveTask={this.props.ArchiveTask}
              CopyCalenderTask={this.props.CopyCalenderTask}
              openPublicLinkDialog={this.openPublicLinkDialog}
              UnArchiveTask={this.props.UnArchiveTask}
              UpdateTaskColorFromGridItem={this.props.UpdateTaskColorFromGridItem}
              UpdateCalenderTask={this.props.UpdateCalenderTask}
              selectedColor={value.row.colorCode}
              userId={this.state.userId}
              isArchivedSelected={this.props.isArchivedSelected}
              taskPermission={taskPermission}
            />
          );
        },
        cellClass: "dropDownCell lastColoumn",
        columnOrder: 16,
        width: columnWidth && columnWidth["optionList"] ? columnWidth["optionList"].width : 50,
      },
    ];
    return [...arr, ...lastColumn];
  };
  handleAddAssessmentDialog = (value, e = false, task, fieldId) => {
    const { customFields, profileState } = this.props;
    e && e.stopPropagation();
    e && e.preventDefault();
    const selectedMatrixObj =
      customFields.data.find(
        m =>
          m.settings.assessment &&
          m.settings.assessment.includes(profileState.loggedInTeam) &&
          !m.settings.isHidden &&
          m.settings.hiddenInWorkspaces.indexOf(profileState.loggedInTeam) == -1 &&
          m.settings.hiddenInGroup.indexOf("task") == -1
      ) || false;
    const assessmentMatrixValues =
      (task.customFieldData &&
        task.customFieldData.find(c => c.fieldId === selectedMatrixObj.fieldId)) ||
      false;
    this.setState({
      assessmentDialogOpen: value,
      selectedTask: task,
      selectedMatrix: fieldId,
      assessmentMatrixValues: assessmentMatrixValues,
    });
  };
  selectedColumnArr = () => {
    const { taskColumns = [] } = this.props;

    const taskColumnOrder = taskColumns.filter(c => c.isSelected).map(c => c.columnName);

    let newCols = this.getColumns();

    let columns = newCols.filter(
      (x, i) =>
        taskColumnOrder.indexOf(x.name) >= 0 || i === newCols.length - 1 || x.key == "taskTitle"
    );

    /** containing unique values */
    this.setState({
      _columns: columns,
    });
  };
  customFieldChange = (option, obj, currentTask) => {
    const taskCopy = currentTask;
    const { fieldId } = obj;
    const newObj = {
      groupType: "task",
      groupId: taskCopy.taskId,
      fieldId,
      fieldData: { data: option },
    };
    if (obj.fieldType == "matrix") {
      newObj.fieldData.data = newObj.fieldData.data.map(opt => {
        return { cellName: opt.cellName };
      });
    } else {
      if (obj.settings.multiSelect)
        newObj.fieldData.data = newObj.fieldData.data.map(opt => {
          return { id: opt.id };
        });
      else {
        newObj.fieldData.data = { id: `${option.length ? option[0].id : ""}` };
      }
    }

    this.props.UpdateTaskCustomField(newObj, res => {
      const resObj = res.data.entity[0];
      let customFieldsArr = [];
      // Updating Global state
      const isExist =
        taskCopy.customFieldData &&
        taskCopy.customFieldData.findIndex(c => c.fieldId === resObj.fieldId) > -1;
      if (isExist) {
        customFieldsArr = taskCopy.customFieldData.map(c => {
          return c.fieldId === resObj.fieldId ? resObj : c;
        });
      } else {
        customFieldsArr = taskCopy.customFieldData
          ? [...taskCopy.customFieldData, resObj]
          : [resObj];
      }
      let newTaskObj = { ...taskCopy, customFieldData: customFieldsArr };
      this.props.dispatchTask(newTaskObj);
      this.handleAddAssessmentDialog(false, false, {});
    });
  };
  filterNTaskField = itemArray => {
    /** function that checks if nTask fields exists and if it is not hide by the user/ other wise filter it from rows and also the column seletion drop down */
    const { customFields, profileState } = this.props;

    let items = itemArray.reduce((result, currentValue) => {
      let isExist = customFields.data.find(cf => cf.fieldName == currentValue);
      if (
        isExist &&
        !isExist.settings.isHidden &&
        isExist.settings.hiddenInWorkspaces.indexOf(profileState.data.loggedInTeam) == -1 &&
        isExist.settings.hiddenInGroup.indexOf("task") == -1
      )
        result.push(currentValue);
      if (!isExist) result.push(currentValue);
      return result;
    }, []);
    return items;
  };
  // handleUpdateColumns = () => {
  //   /** when adding new column or field then render again the columns so the added column will remaain added */
  //   this.setState(
  //     {
  //       _columns: this.getColumns(),
  //     },
  //     () => {
  //
  //     }
  //   );
  // };
  componentDidMount() {
    const { workspaces } = this.props;
    const { loggedInTeam } = this.props.profileState.data;
    const { userId } = this.props.profileState.data;

    if (loggedInTeam) {
      const { taskColumn, taskDirection } = getSortOrder(workspaces, loggedInTeam, 2);

      if (taskColumn && taskDirection) {
        const rows = this.getDetailedTasks();
        this.sortRows(rows, taskColumn, taskDirection);
      } else {
        this.setState({ rows: this.getDetailedTasks() });
      }
    }
    // this.selectedColumnArr();

    this.setState({
      // rows: this.getDetailedTasks(),
      userId,
      // newCol:
      //   this.state._columns.filter(c => {
      //     return c.visible == true;
      //   }) || [],
      showTaskDetails: false,
      loggedInTeam,
    });
  }

  // // Function that updates task project
  // updateProject = (selectedProject, task) => {
  //   const newTaskObj = {
  //     ...task,
  //     projectId: selectedProject.length ? selectedProject[0].id : "",
  //   };
  //   this.props.UpdateTask(newTaskObj, () => {});
  // };
  showMappingDialog = projectId => {
    const { workspaceStatus } = this.props;
    let attachedProject = this.props.projects.find(p => p.projectId == projectId);
    return attachedProject && attachedProject.projectTemplateId != workspaceStatus.templateId;
  };
  getDefaultTemplateProject = item => {
    const { workspaceStatus } = this.props;
    const projectId = item.length ? item[0].id : null;
    if (projectId) {
      let attachedProject = this.props.projects.find(p => p.projectId == projectId);
      if (attachedProject && attachedProject.projectTemplate) {
        return attachedProject.projectTemplate;
      } else {
        return workspaceStatus;
      }
    } else {
      return workspaceStatus;
    }
  };
  // Function that updates task project
  handleSelectChange = (newValue, task) => {
    const projectId = newValue.length ? newValue[0].id : task.projectId;
    if (!this.showMappingDialog(projectId)) {
      const newTaskObj = {
        ...task,
        projectId: newValue.length > 0 ? newValue[0].id : null,
        parentId: null,
      };
      this.props.UpdateTask(newTaskObj, () => {});
    } else {
      let taskStatus = task.taskStatus.statusList.find(el => el.statusId == task.status);
      let template = this.getDefaultTemplateProject(newValue);
      this.setState({
        tempProject: newValue || null,
        openStatusDialog: true,
        newTemplateItem: template,
        oldStatusItem: taskStatus,
        tempTask: task,
      });
    }
  };
  handleCloseDialog = () => {
    this.setState({ openStatusDialog: false, newTemplateItem: null, oldStatusItem: null });
  };
  handleSaveTemplate = newStatusItem => {
    const { tempProject, tempTask } = this.state;
    let updatedTask = { ...tempTask };
    updatedTask.projectId = tempProject.length ? tempProject[0].id : null;
    updatedTask.status = newStatusItem.statusId;
    updatedTask.statusTitle = newStatusItem.statusTitle;
    this.setState(
      {
        openStatusDialog: false,
        newTemplateItem: null,
        oldStatusItem: null,
      },
      () => {
        this.props.UpdateTask(updatedTask, res => {
          if (res && res.status !== 200) {
            /** if API fails */
            this.props.showSnackBar(res.data.message, "error");
          } else {
            this.setState({ openStatusDialog: false, newTemplateItem: null, oldStatusItem: null });
          }
        });
      }
    );
  };

  // Generate list of all projects for dropdown understandable form
  generateProjectDropdownData = task => {
    const { projects } = this.props;
    let filteredProjects = projects.filter(p => p.projectId !== task.projectId);
    const projectsArr = filteredProjects.map(project => {
      return { label: project.projectName, id: project.projectId, obj: task };
    });
    return projectsArr;
  };

  // Generate list of selected options for project dropdown understandable form
  getSelectedProject = task => {
    const { projects } = this.props;
    const selectedProject = projects.find(project => {
      return project.projectId == task.projectId;
    });

    return selectedProject
      ? [
          {
            label: selectedProject.projectName,
            id: selectedProject.projectId,
            obj: task,
          },
        ]
      : [];
  };

  componentDidUpdate(prevProps, prevState) {
    const { addTask, isReorder, sortColumn, sortDirection, detailsTaskId } = this.state;
    const { sortObj, filteredTasks } = this.props;
    const taskDetailsTask = filteredTasks.find(t => {
      // Checking if the task details task exist in the list or not
      return t.taskId == detailsTaskId;
    });
    if (detailsTaskId && !taskDetailsTask) {
      // if task does not exist in the filtered list remove the task details from state
      // This scenario can be produced if you apply filter on a task with assignee from sidebar and open task details and change assignee and than toggle filter from sidebar again
      this.setState({ showTaskDetails: false, detailsTaskId: "" });
    }
    if (
      JSON.stringify(prevProps.appliedFiltersState) !==
        JSON.stringify(this.props.appliedFiltersState) &&
      this.state.selectedIds.length > 0
    ) {
      this.setState({ selectedIds: [] });
    }
    if (addTask == true) {
      this.addTaskInput.current.focus();
    }

    if (JSON.stringify(prevProps.filteredTasks) !== JSON.stringify(this.props.filteredTasks)) {
      if (sortColumn) this.sortRows(this.getDetailedTasks(), sortColumn, sortDirection);
      else this.setState({ newEntry: true, rows: this.getDetailedTasks() });
    }

    if (isReorder) {
      this.setOrderedData();
    }

    if (
      sortObj &&
      sortObj.column &&
      (sortObj.column !== prevProps.sortObj.column ||
        sortObj.direction !== prevProps.sortObj.direction)
    ) {
      /** when user click on selected sort button for changing the sort order, this condtion will execute */
      this.sortRows(
        this.props.filteredTasks,
        sortObj.column,
        sortObj.direction
      ); /** function call for reseting the state task list into selected sort manner */
    } else if (
      sortObj &&
      sortObj.cancelSort == "NONE" &&
      sortObj.cancelSort !== prevProps.sortObj.cancelSort
    ) {
      /** when user click on cancel sort this condtion will execute */
      this.setState({
        newEntry: true,
        rows: this.getDetailedTasks(),
      }); /** updating state task list  */
    }
    if (
      !isEqual(prevProps.customFields, this.props.customFields) ||
      !isEqual(prevProps.taskColumns, this.props.taskColumns)
    ) {
      this.selectedColumnArr();
    }
    this.queryTaskUrl();
    this.renderHeaders();
  }

  columnChangeCallback = () => {
    this.selectedColumnArr();
  };
  renderHeaders = () => {
    const elements = document.getElementsByClassName("widget-HeaderCell__value");
    const sortableElements = document.getElementsByClassName("react-grid-HeaderCell-sortable");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerText = headerTranslations(elements[i].innerText, this.props.intl);
    }
    for (var i = 0; i < sortableElements.length; i++) {
      sortableElements[i].innerText = headerTranslations(
        sortableElements[i].innerText,
        this.props.intl
      );
    }
  };

  queryTaskUrl = () => {
    const searchQuery = this.props.history.location.search; // getting task id in the url
    const { filteredTasks, itemOrderState } = this.props;
    const { showTaskDetails } = this.state;
    if (searchQuery && !showTaskDetails && itemOrderState) {
      // checking if there is task Id in the url or not
      const { taskId } = queryString.parseUrl(searchQuery).query; // Parsing the url and extracting the task id using query string
      const validateTask = filteredTasks.findIndex(task => {
        // Checking if the task id is valid or not
        return taskId == task.taskId;
      });

      if (validateTask > -1) {
        // If the task id is valid
        this.setState({ showTaskDetails: true, detailsTaskId: taskId });
      } else {
        this.props.showSnackBar("Oops! Item not found", "info");
        // In case the task id in the url is wrong user is redirected to issues Page
        this.props.history.push("/tasks");
      }
      // else {
      //   //In case task id is from non active workspace
      //   this.props.showLoadingState();
      //   getTaskWorkspace(
      //     taskId,
      //     (response) => {
      //       $.connection.hub.stop();
      //       this.props.FetchWorkspaceInfo(response, () => {
      //         this.props.FetchUserInfo(() => {
      //           nTaskSignalConnectionOpen(this.props.profileState.data.userId);
      //           this.props.hideLoadingState();
      //         });
      //       });
      //     },
      //     () => {
      //       this.props.hideLoadingState();
      //       //In case the task id in the url is wrong user is redirected to tasks Page
      //       this.props.history.push("/tasks");
      //     }
      //   );
      // }
    }
  };

  setOrderedData = () => {
    const rows = cloneDeep(this.state.rows);
    let { showRecords } = this.state;

    const completeDetails = rows
      .map(task => {
        const position = helper.RETURN_ITEMORDER(this.props.itemOrderState.data.itemOrder, task.id);
        task.taskOrder = position || 0;
        return task;
      })
      .sort((a, b) => a.taskOrder - b.taskOrder);

    const allIds = completeDetails.map(x => x.taskId);
    const newArray = allIds.filter(obj => {
      return this.state.selectedIds.indexOf(obj) >= 0;
    });

    if (this.props.isChanged) {
      showRecords = increment;
      this.props.handleChangeState();
    }
    if (completeDetails.length && showRecords < increment) {
      showRecords = increment;
    }
    if (showRecords > completeDetails.length) showRecords = completeDetails.length;

    this.props.returnCount(completeDetails.length, completeDetails.length);

    this.setState({
      rows: completeDetails,
      isReorder: false,
      selectedIds: newArray,
      totalRecords: rows.length,
      showRecords,
    });
  };

  getDetailedTasks = () => {
    if (this.props.filteredTasks.length && this.canvas) {
      this.canvas = findDOMNode(this).querySelector(".react-grid-Canvas");
      this.canvas.addEventListener("scroll", this.scrollListener);
    }
    return this.props.filteredTasks;
  };

  handleDateSave = (startDate, endDate, task, dateType, startTime, endTime) => {
    const obj =
      dateType == "planned"
        ? {
            startDateString: helper.RETURN_CUSTOMDATEFORMAT(startDate),
            dueDateString: helper.RETURN_CUSTOMDATEFORMAT(endDate),
            startTime,
            dueTime: endTime,
          }
        : {
            actualStartDateString: helper.RETURN_CUSTOMDATEFORMAT(startDate),
            actualDueDateString: helper.RETURN_CUSTOMDATEFORMAT(endDate),
            actualStartTime: startTime,
            actualDueTime: endTime,
          };
    this.props.UpdateTask({ ...task, ...obj }, response => {
      if (response && response.status === 200) {
        response = response.data.CalenderDetails;
      }
    });
  };

  closePublicLinkDialog = () => {
    this.setState({ publicLinkOpen: false });
  };

  openPublicLinkDialog = (event, id, taskTitle) => {
    event.stopPropagation();
    this.setState({
      publicLinkOpen: true,
      publicLinkId: id,
      publicLinkTitle: taskTitle,
    });
  };

  handleColorChange = (data, color) => {
    const self = this;
    this.setState({ selectedColor: color.hex, taskId: data.taskId, isColored: true }, function() {
      const obj = { ...data };
      obj.colorCode = color.hex;
      delete obj.CalenderDetails;
      self.props.UpdateTaskColorFromGridItem(obj, data => {
        self.props.UpdateCalenderTask(data, () => {
          self.setState({
            selectedColor: "#fff",
            taskId: "",
            isColored: false,
          });
        });
      });
    });
  };

  forceUpdateHandler() {
    this.forceUpdate();
  }

  scrollListener() {
    if (this.canvas.scrollHeight - (this.canvas.scrollTop + this.canvas.clientHeight) < 1) {
      this.handleScrollDown();
    }
  }

  handleScrollDown = () => {
    this.setState({ showRecords: this.state.showRecords + increment }, () => {
      if (this.state.showRecords >= this.state.rows.length) {
        this.setState({ showRecords: this.state.rows.length });
      }
    });
  };

  showTaskDetailsPopUp(e, data = {}) {
    const { permissionObject, workspaceTaskPer } = this.props;
    const taskPermission = data && data.projectId ? isUndefined(permissionObject[data.projectId]) ? workspaceTaskPer : permissionObject[data.projectId].task : workspaceTaskPer;
    if (data && taskPermission.taskDetail.cando) {
      this.setState({ showTaskDetails: true, detailsTaskId: data.taskId });
    }
  }

  closeTaskDetailsPopUp(currentTask, taskTodoItems) {
    // if (currentTask && taskTodoItems) this.updateTaskProgress(currentTask, taskTodoItems);
    this.setState({ showTaskDetails: false, detailsTaskId: "" });
    this.props.dispatchAddTodoItems([], "ClearTodoItems");
    /** when task details modal closes then emtying the todoItems store array */
    this.props.history.push("/tasks");
  }

  updateTaskProgress = (currentTask, totalItems) => {
    // const totalItems = props.todoItems.length;
    const cmpltdItems = totalItems.filter(item => item.isComplete).length;
    const progressPercent = Math.round((cmpltdItems / totalItems.length) * 100) || 0;
    const newTaskObj = { ...currentTask, progress: progressPercent };

    this.props.UpdateTaskProgressBar(newTaskObj, () => {});
  };

  isUpdated(taskIds) {
    this.props.filterBulkTask(taskIds);
    this.setState({ selectedIds: [] });
  }

  BulkUpdate(data, callback) {
    this.props.BulkUpdateTask(data, (resp, err) => {
      if (callback) callback();
      if (resp) {
        if (this.props.taskState.indexOf("Archived") >= 0) {
          this.setState({ isArchived: true, selectedIds: [] });
          this.props.filterBulkTask(data.taskIds);
        } else this.setState({ isArchived: false });
      }
    });
  }

  handleCalculateProgress(value, data, isProgressUpdated) {
    if (isProgressUpdated) {
      this.setState({
        progress: 0,
        taskId: "",
        isProgress: false,
        showTaskDetails: false,
      });
    } else {
      const result = helper.RETURN_PROGRESS(data.checkList, data.status);
      this.setState(
        {
          progress: result,
          taskId: data.taskId,
          isProgress: true,
          showTaskDetails: false,
        },
        function() {
          return Progress(value, this.state);
        }
      );
    }
  }

  componentWillUnmount() {
    if (this.canvas) {
      this.canvas.removeEventListener("scroll", this.scrollListener);
    }
  }

  addTask(keyName) {
    if (keyName && keyName === "enter") {
      this.setState({ addNewForm: true });
    } else if (keyName && keyName === "inline") {
      this.setState({ addTask: true });
    } else {
      this.setState({ addNewForm: true });
    }
  }

  cancelAddTask() {
    this.setState({
      addTask: false,
      addNewForm: false,
      TaskError: false,
      TaskErrorMessage: "",
    });
  }

  handleChange(e) {
    if (e.target.value.length > 250) {
      this.setState({
        TaskError: true,
        TaskErrorMessage: this.props.intl.formatMessage({
          id: "task.creation-dialog.form.validation.title.length",
          defaultMessage: "Please enter less than 250 characters",
        }),
        value: e.target.value,
      });
    } else {
      this.setState({
        TaskError: false,
        TaskErrorMessage: "",
        value: e.target.value,
      });
    }
  }

  // Function that updates assignee of respective document (e.g. task/risk/issue/meeting)
  updateAssignee = (assignedTo, task) => {
    const assignedToArr = assignedTo.map(ass => {
      return ass.userId;
    });
    const newTaskObj = { ...task, assigneeList: assignedToArr };

    this.props.UpdateTask(newTaskObj, () => {});
  };

  onKeyDown = event => {
    //
    // Event.preventDefault();
    if (event.keyCode === 13) {
      const checkEmpty = helper.RETURN_CECKSPACES(this.state.value);
      if (!this.state.value || checkEmpty) {
        this.setState({
          TaskError: true,
          TaskErrorMessage:
            checkEmpty ||
            this.props.intl.formatMessage({
              id: "task.creation-dialog.form.validation.title.empty",
              defaultMessage: "Please enter task title",
            }),
        });

        return;
      }

      if (this.state.value && this.state.value.length > 250) {
        this.setState({
          TaskError: true,
          TaskErrorMessage: this.props.intl.formatMessage({
            id: "task.creation-dialog.form.validation.title.length",
            defaultMessage: "Please enter less than 250 characters",
          }),
        });
        return;
      }
      this.props.SaveTask(
        {
          taskTitle: this.state.value,
        },
        () => {},
        (err, x) => {
          if (x) {
            this.grid.selectAllCheckbox.checked = false;
          } else {
            this.setState({
              TaskError: false,
              TaskErrorMessage: err.data,
            });
          }
        }
      );
      this.setState({ value: "" });
    } else if (event.keyCode == 27) {
      this.setState({ addTask: false });
    } else {
      this.setState({ addTask: true });
    }
  };

  onFoucusOut = () => {
    this.setState({ addTask: false });
  };

  getRandomDate = (start, end) => {
    return new Date(
      start.getTime() + Math.random() * (end.getTime() - start.getTime())
    ).toLocaleDateString();
  };

  rowGetter = i => {
    if (i >= 0) return this.state.rows[i] ? this.state.rows[i] : {};
  };

  isDraggedRowSelected = (selectedRows, rowDragSource) => {
    if (selectedRows && selectedRows.length > 0) {
      const key = this.props.rowKey;
      return selectedRows.filter(r => r[key] === rowDragSource.data[key]).length > 0;
    }
    return false;
  };

  reorderRows = e => {
    const selectedRows = Selectors.getSelectedRowsByKey({
      rowKey: this.props.rowKey,
      selectedKeys: this.state.selectedIds,
      rows: this.state.rows,
    });
    const draggedRows = this.isDraggedRowSelected(selectedRows, e.rowSource)
      ? selectedRows
      : [e.rowSource.data];
    let undraggedRows = this.state.rows.filter(function(r) {
      return draggedRows.indexOf(r) === -1;
    });
    const args = [e.rowTarget.idx, 0].concat(draggedRows);
    Array.prototype.splice.apply(undraggedRows, args);

    undraggedRows = undraggedRows.map((x, i) => {
      x.taskOrder = i;
      return x;
    });
    const orderToBeSaved = undraggedRows.map(x => {
      return x.id;
    });
    const itemOrder = this.props.itemOrderState.data;
    itemOrder.itemOrder = itemOrder.itemOrder
      .map((x, i) => {
        if (orderToBeSaved.indexOf(x.itemId) >= 0) {
          return {
            itemId: x.itemId,
            position: orderToBeSaved.indexOf(x.itemId),
          };
        }
        return x;
      })
      .sort((a, b) => a.position - b.position);
    this.props.SaveItemOrder(
      itemOrder,
      data => {
        this.setState({
          rows: undraggedRows,
          isReorder: true,
          showTaskDetails: false,
        });
      },
      () => {},
      undraggedRows,
      "task"
    );
  };

  onRowsSelected = rows => {
    const filterRows = rows.filter(r => !r.row.__metaData); // Removing Grouping Rows from the selected list
    // Generating array of taskIds
    const taskIdsArr = filterRows.map(r => {
      return r.row.taskId;
    });

    // merging existing selected id's array with new selected tasks Ids
    const selectedObjArr = [...this.state.selectedIds, ...taskIdsArr];
    const selectedTaskObjArr = [...this.state.selectedTaskObjArr, ...rows];
    if (selectedObjArr.length > 1 && !teamCanView("bulkActionAccess")) {
      this.setState({ showUnplanned: true });
      return;
    }
    this.setState(
      {
        selectedIds: selectedObjArr,
        showTaskDetails: false,
        selectedTaskObjArr: selectedTaskObjArr,
      },
      () => {
        if (this.state.rows.length === this.state.selectedIds.length) {
          this.grid.selectAllCheckbox.checked = true;
        }
      }
    );
  };

  onRowsDeselected = rows => {
    const rowIds = rows.map(r => r.row[this.props.rowKey]);
    this.setState({
      selectedIds: this.state.selectedIds.filter(i => rowIds.indexOf(i) === -1),
      showTaskDetails: false,
      selectedTaskObjArr: this.state.selectedTaskObjArr.filter(s => rowIds.indexOf(s.row.taskId) === -1),
    });
  };

  sortRows = (initialRows, sortColumn, sortDirection) => {
    const rows =
      sortDirection === "NONE" ? initialRows : sortListData(initialRows, sortColumn, sortDirection);
    this.setState({
      rows,
      sortColumn: sortDirection === "NONE" ? null : sortColumn,
      sortDirection,
    });
  };

  openRepeatTaskDrawer = (event, task) => {
    event.stopPropagation();
    this.setState({ repeatTaskDrawer: true, repeatTaskId: task.taskId });
  };

  closeRepeatTaskDrawer = () => {
    this.setState({ repeatTaskDrawer: false, repeatTaskDrawer: "" });
  };

  handleClose = event => {
    /** function call when close drop down */
    this.setState({ showUnplanned: false });
  };

  markAllConfirmDialogClose = () => {
    this.setState({ markChecklistActionConf: false, selectedTaskObj: {}, selectedStatus: {}, selectedTaskObjArr:[] });
  };

  handleStatusChange = (status, task) => {
    if (status.obj.isDoneState) {
      this.setState(
        { selectedTaskObj: task, selectedStatus: status, markChecklistActionConf: true },
        () => {}
      );
      return;
    }
    this.handleUpdateStatus(status, task);
  };

  handleUpdateStatus = (status, task) => {
    const updatedTask = cloneDeep(task);
    updatedTask.status = status.value;
    updatedTask.statusTitle = status.label;

    if (status.obj.isDoneState) {
      this.setState({ markChecklistActionBtnQuery: "progress" });
      const checkData = {
        taskId: updatedTask.id,
        checkAll: true,
      };
      this.props.CheckAllTodoList(
        checkData,
        () => {},
        () => {},
        this.props.profileState.data
      );
    }

    this.props.UpdateTask(updatedTask, response => {
      if (response && response.status == 200) {
        this.setState({
          selectedTaskObj: {},
          markChecklistActionBtnQuery: "",
          markChecklistActionConf: false,
          selectedStatus: {},
        });
      } else {
        this.props.showSnackBar(response.data.message, "error");
        this.setState({ markChecklistActionBtnQuery: "" });
      }
    });
  };

  statusData = statusArr => {
    if (statusArr) {
      return statusArr.map(item => {
        return {
          label: item.statusTitle,
          value: item.statusId,
          icon: <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "20px" }} />,
          statusColor: item.statusColor,
          obj: item,
        };
      });
    }
    return [];
  };

  groupByAssignee = () => {
    return this.state.rows.reduce((r, cv) => {
      cv.assigneeLists.forEach(x => {
        r.push({ ...cv, assigneeId: x.userId });
      });
      return r;
    }, []);
  };

  render() {
    const {
      addTask,
      addNewForm,
      selectedIds,
      detailsTaskId,
      isWorkspaceChanged,
      publicLinkOpen,
      publicLinkId,
      publicLinkTitle,
      type,
      _columns,
      rows,
      repeatTaskDrawer,
      repeatTaskId,
      showUnplanned,
      openStatusDialog,
      newTemplateItem,
      oldStatusItem,
      markChecklistActionBtnQuery,
      markChecklistActionConf,
      selectedTaskObj,
      selectedStatus,
      checkedItems,
      columnddData,
    } = this.state;
    const {
      classes,
      theme,
      taskState,
      handleExportType,
      filteredTasks,
      appliedFiltersState,
      isArchivedSelected,
      selectedGroup = {},
      workspaceTaskPer,
      intl,
    } = this.props;
    if (rows.length > 0 && rows.length === this.state.selectedIds.length) {
      if (this.grid && this.grid.selectAllCheckbox) this.grid.selectAllCheckbox.checked = true;
    } else if (this.grid && this.grid.selectAllCheckbox)
      this.grid.selectAllCheckbox.checked = false;
    if (isWorkspaceChanged) {
      if (this.grid && this.grid.selectAllCheckbox) this.grid.selectAllCheckbox.checked = false;
    }
    const EmptyRowsView = () => {
      return isArchivedSelected ? (
        <EmptyState
          screenType="Archived"
          heading={
            <FormattedMessage id="common.archived.label" defaultMessage="No archived items found" />
          }
          message={
            <FormattedMessage
              id="common.archived.message"
              defaultMessage="You haven't archived any items yet."
            />
          }
          button={false}
        />
      ) : (taskState.length > 0 || appliedFiltersState.Task) &&
        (filteredTasks || filteredTasks.length <= 0) ? (
        <EmptyState
          screenType="search"
          heading={
            <FormattedMessage id="common.search-list.label" defaultMessage="No Results Found" />
          }
          message={
            <FormattedMessage
              id="common.search-list.message"
              defaultMessage="No matching results found against your filter criteria."
            />
          }
          button={false}
        />
      ) : workspaceTaskPer && workspaceTaskPer.createTask && workspaceTaskPer.createTask.cando ? (
        <EmptyState
          screenType="task"
          heading={
            <FormattedMessage
              id="common.create-first.task.label"
              defaultMessage="Create your first task"
            />
          }
          message={
            <FormattedMessage
              id="common.create-first.task.messageb"
              defaultMessage='You do not have any tasks yet. Press "Alt + T" or click on button below.'
            />
          }
          button
        />
      ) : (
        <EmptyState
          screenType="task"
          heading={
            <FormattedMessage
              id="common.create-first.task.messagec"
              defaultMessage="You do not have any tasks yet."
            />
          }
          message={
            <FormattedMessage
              id="common.create-first.task.messagea"
              defaultMessage="Request your team member(s) to assign you a task."
            />
          }
          button={false}
        />
      );
    };
    const assigneeGroupedRows = selectedGroup.key === assignees.key ? this.groupByAssignee() : rows;
    const groupBy = selectedGroup.key && [selectedGroup.key];
    const groupedRows = groupBy
      ? Data.Selectors.getRows({
          rows: assigneeGroupedRows,
          groupBy: selectedGroup.key === "assigneeLists" ? ["assigneeId"] : groupBy,
        })
      : rows;

    return (
      <>
        {openStatusDialog && (
          <TaskStatusChangeDialog
            open={openStatusDialog}
            handleClose={this.handleCloseDialog}
            oldStatus={oldStatusItem}
            newTemplateItem={newTemplateItem}
            handleSaveAsTemplate={this.handleSaveTemplate}
          />
        )}
        <ActionConfirmation
          open={markChecklistActionConf}
          closeAction={this.markAllConfirmDialogClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="task.detail-dialog.to-do-list.mark-all.action"
              defaultMessage="Yes, Mark All Complete"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="task.detail-dialog.to-do-list.mark-all.title"
              defaultMessage="Mark All Complete"
            />
          }
          iconType="markAll"
          msgText={
            <FormattedMessage
              id="task.detail-dialog.to-do-list.mark-all.message"
              defaultMessage="Are you sure you want to mark all to-do list items as completed?"
            />
          }
          successAction={() => this.handleUpdateStatus(selectedStatus, selectedTaskObj)}
          btnQuery={markChecklistActionBtnQuery}
        />
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={showUnplanned}
          onClose={this.handleClose}
          onBackdropClick={this.handleClose}
          BackdropProps={{
            classes: {
              root: classes.unplannedRoot,
            },
          }}>
          <div className={classes.unplannedMain} style={{ top: 325, left: 200 }}>
            <UnPlanned
              feature="premium"
              titleTxt={
                <FormattedMessage
                  id="common.discovered-dialog.premium-title"
                  defaultMessage="Wow! You've discovered a Premium feature!"
                />
              }
              boldText={this.props.intl.formatMessage({
                id: "common.discovered-dialog.list.bulk-action.title",
                defaultMessage: "Bulk Actions",
              })}
              descriptionTxt={
                <FormattedMessage
                  id="common.discovered-dialog.list.bulk-action.label"
                  defaultMessage={"is available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."}
                  values={{TRIALPERIOD: TRIALPERIOD}}
                />
              }
              showBodyImg={false}
              showDescription
              selectUpgrade={this.handleClose}
              titleStyle={{ fontSize: "18px"}}
            />
          </div>
        </Modal>
        {repeatTaskId && repeatTaskDrawer && (
          <SideDrawer
            closeAction={this.closeRepeatTaskDrawer}
            open={repeatTaskDrawer}
            repeatTaskId={repeatTaskId}
            allTasks={rows}
          />
        )}
        <DefaultDialog title="Add Task" open={addNewForm} onClose={this.cancelAddTask}>
          <AddNewTaskForm closeAction={this.cancelAddTask} />
        </DefaultDialog>
        {publicLinkOpen ? (
          <PublicLink
            open={publicLinkOpen}
            id={publicLinkId}
            taskTitle={publicLinkTitle}
            closeAction={this.closePublicLinkDialog}
            maxTitleWidth={500}
          />
        ) : null}

        {this.state.showTaskDetails && detailsTaskId && filteredTasks.length > 0
          ? // <TaskDetails
            //   filterUnArchiveTask={this.props.filterUnArchiveTask}
            //   isArchivedSelected={isArchivedSelected}
            //   deleteTaskFromArchiveList={this.props.deleteTaskFromArchiveList}
            //   closeTaskDetailsPopUp={this.closeTaskDetailsPopUp}
            //   currentTask={rows.find(task => {
            //     return task.taskId == detailsTaskId;
            //   })}
            //   type={type}
            //   handleDeleteCustomField={this.handleDeleteCustomField}
            //   updatingSelectedColumn={this.updatingSelectedColumn}
            // />
            this.props.taskDetailDialogState(null,{
              id: detailsTaskId,
              afterCloseCallBack: () => {
                this.closeTaskDetailsPopUp();
              },
              type: type,
              deleteTaskFromArchiveList: this.props.deleteTaskFromArchiveList,
              filterUnArchiveTask: this.props.filterUnArchiveTask,
              isArchived: isArchivedSelected,
            })
          : null}
        {isArchivedSelected ? null : <Hotkeys keyName="alt+t" onKeyDown={this.onKeyDown} />}
        <div className="taskList" style={{ position: "relative" }}>
          <div
            style={{
              width: "100%",
              position: "absolute",
              paddingLeft: 0,
            }}
            className={selectedIds.length > 1 ? "multiChecked" : null}>
            {selectedIds.length > 1 ? (
              <BulkActions
                selectedIdsList={this.state.selectedIds}
                isBulkUpdated={this.isUpdated}
                isBulkUpdate={this.BulkUpdate}
                isArchivedFilter={this.props.taskState.indexOf("Archived") >= 0}
                taskPer={this.props.workspaceTaskPer}
                selectedTaskObj={this.state.selectedTaskObjArr}
                permissionObject={this.props.permissionObject}
              />
            ) : null}
            <div className={classes.TableActionBtnDD}>
              <ColumnSelectionDropdown
                feature={"task"}
                columnChangeCallback={this.columnChangeCallback}
                hideColumns={["matrix"]}
              />
            </div>

            <BottomScrollListener onBottom={this.handleScrollDown}>
              <DraggableContainer>
                <ReactDataGrid
                  onRowClick={this.showTaskDetailsPopUp.bind(this)}
                  enableCellSelection={teamCanView("bulkActionAccess")}
                  ref={node => (this.grid = node)}
                  rowActionsCell={RowActionsCell}
                  columns={_columns}
                  emptyRowsView={EmptyRowsView}
                  onGridSort={(sortColumn, sortDirection) =>
                    this.sortRows(this.props.filteredTasks, sortColumn, sortDirection)
                  }
                  rowGetter={i => {
                    return groupedRows[i];
                  }}
                  rowsCount={groupedRows.length}
                  rowHeight={60}
                  headerRowHeight={
                    isArchivedSelected ||
                    (workspaceTaskPer && !workspaceTaskPer.createTask && !workspaceTaskPer.createTask.cando)
                      ? 50
                      : 105
                  }
                  minHeight={calculateContentHeight()}
                  selectAllCheckbox
                  rowRenderer={<RowRenderer onRowDrop={this.reorderRows} />}
                  rowGroupRenderer={param => {
                    return GroupingRowRenderer(
                      param,
                      assigneeGroupedRows,
                      classes,
                      intl,
                      this.props.profileState.data.member.allMembers
                    );
                  }}
                  rowSelection={{
                    showCheckbox: true,
                    enableShiftSelect: true,
                    onRowsSelected: this.onRowsSelected,
                    onRowsDeselected: this.onRowsDeselected,
                    selectBy: {
                      keys: {
                        rowKey: this.props.rowKey,
                        values: this.state.selectedIds,
                      },
                    },
                  }}
                  onColumnResize={this.onColumnResize}
                />
              </DraggableContainer>
            </BottomScrollListener>
            {isArchivedSelected ? null : workspaceTaskPer.createTask && workspaceTaskPer.createTask.cando ? (
              <AddTask
                className={addTask ? "addTaskInputCnt" : "addTaskCnt"}
                style={{ left: 36 }}
                onClick={!addTask ? () => this.addTask("inline") : undefined}>
                {addTask ? (
                  <>
                    {this.state.TaskError ? (
                      <div htmlFor="TaskError" className={classes.selectError}>
                        {this.state.TaskErrorMessage ? this.state.TaskErrorMessage : ""}
                      </div>
                    ) : null}
                    {/* <Hotkeys keyName="enter,esc" onKeyDown={this.onKeyDown}> */}
                    <input
                      ref={this.addTaskInput}
                      onKeyDown={event => this.onKeyDown(event)}
                      id="addTaskInput"
                      style={{
                        border: this.state.TaskError
                          ? `1px solid ${theme.palette.border.redBorder}`
                          : null,
                      }}
                      autoFocus={this.state.focus}
                      onBlur={this.cancelAddTask}
                      type="text"
                      onChange={this.handleChange}
                      value={this.state.value}
                    />
                    {/* </Hotkeys> */}
                    <Grid item classes={{ item: classes.addTaskInputClearCnt }}>
                      <span className={classes.addTaskInputClearText}>
                        <FormattedMessage
                          id="common.press.message"
                          defaultMessage="Press Enter to Add"
                        />
                      </span>
                      <IconButton btnType="condensed" onClick={this.cancelAddTask}>
                        <CancelIcon
                          fontSize="default"
                          htmlColor={theme.palette.secondary.light}
                        />
                      </IconButton>
                    </Grid>
                  </>
                ) : (
                  <Grid container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                      <Grid container direction="row" justify="flex-start" alignItems="center">
                        <AddIcon htmlColor={theme.palette.primary.light} />
                        <span className={classes.addNewTaskText}>
                          <FormattedMessage
                            id="task.common.add-new.label"
                            defaultMessage="Add New Task"
                          />
                        </span>
                      </Grid>
                    </Grid>
                    <Grid item>
                      <p className={classes.shortcutText}>
                        {`${this.props.intl.formatMessage({
                          id: "common.press.label",
                          defaultMessage: "Press",
                        })} Alt + T`}
                      </p>
                    </Grid>
                  </Grid>
                )}
              </AddTask>
            ) : null}
          </div>
        </div>
        <UpdateMatrixDialog
          saveOption={(param1, param2) =>
            this.customFieldChange(param1, param2, this.state.selectedTask)
          }
          feature={"task"}
          selectedMatrix={this.state.selectedMatrix}
          open={this.state.assessmentDialogOpen}
          closeAction={e => this.handleAddAssessmentDialog(false, e, {}, null)}
          selectedValues={
            this.state.assessmentMatrixValues
              ? this.state.assessmentMatrixValues.fieldData.data
              : []
          }
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    tasksState: state.tasks,
    itemOrderState: state.itemOrder,
    appliedFiltersState: state.appliedFilters,
    taskNotificationsState: state.taskNotifications,
    workspacePermissionsState: state.workspacePermissions,
    projects: state.projects.data,
    workspaces: state.profile.data.workspace,
    permissions: state.workspacePermissions.data.task,
    workspaceTaskPer: state.workspacePermissions.data.task,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    customFields: state.customFields,
    taskColumns: state.columns.task,
    permissionObject: ProjectPermissionSelector(state),

  };
};
export default compose(
  withRouter,
  injectIntl,
  withStyles(listStyles, { withTheme: true }),
  connect(mapStateToProps, {
    SaveTask,
    UpdateTask,
    UpdateCalenderTask,
    SaveItemOrder,
    FetchTasksInfo,
    CopyTask,
    DeleteTask,
    ArchiveTask,
    UnArchiveTask,
    BulkUpdateTask,
    CopyCalenderTask,
    UpdateTaskColorFromGridItem,
    FetchWorkspaceInfo,
    FetchUserInfo,
    dispatchAddTodoItems,
    UpdateTaskProgressBar,
    CheckAllTodoList,
    dispatchTask,
    taskDetailDialogState,
    UpdateTaskCustomField
  })
)(TaskList);
