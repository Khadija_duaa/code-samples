const taskFilterStyles = (theme) => ({
  datePickerRangeCnt: {
    display: 'flex',
    padding: '10px 0 15px 0'
  },
  priorityIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  headingCnt: {
    padding: 10,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    '& h3': {
      color: theme.palette.text.darkGray
    }
  },
  clearFilterText: {
    textDecoration: 'underline',
    color: theme.palette.text.danger
  },
  filterContentCnt: {
    padding: 10,
      // height: 'calc(100vh - 310px)',
    overflowY: 'auto'

},
  searchOuterCnt:{
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0
  },
  searchBtnCnt: {
    padding: '15px 10px',
    background: theme.palette.common.white,
    position: 'fixed',
    bottom: 0,
    width: 389,
    display: 'flex',
    borderTop: `1px solid ${theme.palette.border.lightBorder}`

  },

  switchCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: 12,
  },
  switchLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    margin: 0,
    marginRight: 30,
    fontWeight: theme.typography.fontWeightRegular,
    display: "flex",
  },
  pushPinIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  filtersHeaderHelpIcon: {
    fontSize: "14px !important",
    marginLeft: 5,
  },
  unplannedMain: {
    padding: 15,
    display: "flex",
    alignItems: "center",
    // height: "100%",
  },
  sectionOption:{
    display: "flex",
    justifyContent: "space-between",
    padding: "7px 13px 7px 13px",
    // marginTop: 10,
    borderBottom: "1px solid #dddddd",
    alignItems: "center",
    fontSize: "14px !important",
    color: "#646464"
  }
})

export default taskFilterStyles;