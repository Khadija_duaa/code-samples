export const dateFilterOptions = [
  {
    value: "today",
    label: "Today",
  },
  {
    value: "yesterday",
    label: "Yesterday",
  },
  {
    value: "currentWeek",
    label: "This Week",
  },
  {
    value: "nextWeek",
    label: "Next Week",
  },
  {
    value: "currentMonth",
    label: "This Month",
  },
  {
    value: "nextMonth",
    label: "Next Month",
  },
  {
    value: "custom",
    label: "Custom",
  },
];
export const textFilterOptions = [
  {
    value: "contains",
    label: "Contains",
  },
  {
    value: "notContains",
    label: "Not contains",
  },
  {
    value: "equals",
    label: "Equals",
  },
  {
    value: "notEquals",
    label: "Not equals",
  },
  {
    value: "startsWith",
    label: "Starts with",
  },
  {
    value: "endsWith",
    label: "Ends with",
  },
];
export const numberFilterOptions = [
  {
    value: "equals",
    label: "Equals",
  },
  {
    value: "notEquals",
    label: "Not equals",
  },
  {
    value: "lessThan",
    label: "Less than",
  },
  {
    value: "lessThanOrEqual",
    label: "Less than or equals",
  },
  {
    value: "greaterThan",
    label: "Greater than",
  },
  {
    value: "greaterThanOrEqual",
    label: "Greater than or equals",
  },
  {
    value: "inRange",
    label: "In range",
  },
];
const createdDateFilterTypes = [
  {
    value: "today",
    label: "Today",
  },
  {
    value: "yesterday",
    label: "Yesterday",
  },
  {
    value: "currentWeek",
    label: "This Week",
  },
  {
    value: "currentMonth",
    label: "This Month",
  },
  {
    value: "custom",
    label: "Custom",
  },
];
export const taskDateTypes = [
  { key: "startDate", value: "Planned Start", data: dateFilterOptions },
  { key: "dueDate", value: "Planned End", data: dateFilterOptions },
  { key: "actualStartDate", value: "Actual Start", data: dateFilterOptions },
  { key: "actualDueDate", value: "Actual End", data: dateFilterOptions },
  { key: "createdDate", value: "Created Date", data: createdDateFilterTypes },
  { key: "updatedDate", value: "Updated Date", data: createdDateFilterTypes },
];

export const initFilters = {
  project: { type: "", selectedValues: [] },
  assigneeList: { type: "", selectedValues: [] },
  statusTitle: { type: "", selectedValues: [] },
  startDate: { type: "", selectedValues: ['', ''] },
  dueDate: { type: "", selectedValues: ['', ''] },
  actualStartDate: { type: "", selectedValues: ['', ''] },
  actualDueDate: { type: "", selectedValues: ['', ''] },
  priority: { type: "", selectedValues: [] },
  createdDate: { type: "", selectedValues: ['', ''] },
  updatedDate: { type: "", selectedValues: ['', ''] },
  createdBy: { type: "", selectedValues: [] },
  updatedBy: { type: "", selectedValues: [] },
}