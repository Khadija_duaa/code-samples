import React, { Component, Fragment } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import { BlockPicker } from "react-color";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from '../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation';

class BulkActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      deleteBtnQuery: '',
      archiveBtnQuery: '',
      unarchiveBtnQuery: ''
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleColorClick = this.handleColorClick.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleCustomColor = this.handleCustomColor.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    // this.handleOperations = this.handleOperations.bind(this);
  }

  componentDidMount = () => {
    this.setState({
      selectedColor: this.props.selectedColor
    });
  };

  handleCustomColor(event) {
    let obj = Object.assign({}, this.props.task);
    obj.colorCode = event.hex;
    delete obj.CalenderDetails;

    this.props.UpdateTaskColor(obj, data => {
      this.props.UpdateCalender(data);
    });
  }
  handleOperations(value) {
    // 
    switch (value) {
      case "Copy":
        this.props.CopyTask(this.props.task, () => { }, () => { });
        break;
      case "Public Link":
        break;
      case "Archive":
        this.setState({ archiveFlag: true, popupFlag: true });
        break;
      case "Unarchive":
        this.setState({ unArchiveFlag: true, popupFlag: true });
        break;
      case "Delete":
        this.setState({ deleteFlag: true, popupFlag: true });
        break;
    }

    this.setState({ open: false, pickerOpen: false });
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  handleDialogClose() {
    this.setState({
      popupFlag: false,
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false
    });
  }

  deleteTask = () => {
    // this.props.DeleteTask
  }

  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }
  handleColorClick(event) {
    if (event.target.closest("#colorPickerCnt") == null) {
      this.setState(prevState => ({ pickerOpen: !prevState.pickerOpen }));
    }
  }

  colorChange = (color, task) => {
    let self = this;
    this.setState({ selectedColor: color.hex }, function () {
      let obj = Object.assign({}, task);
      this.setState({
        selectedColor: color.hex
      });
      obj.colorCode = color.hex;
      delete obj.CalenderDetails;
      self.props.UpdateTaskColorFromGridItem(obj, data => {
        //
        self.props.UpdateCalenderTask(data, () => { });
      });
    });
  };

  handleArchive = (e) => {
    if (e)
      e.stopPropagation()
    this.setState({ archiveBtnQuery: 'progress' }, () => {
      this.props.ArchiveTask(this.props.task, () => {
        this.setState({ archiveBtnQuery: '', archiveFlag: false, popupFlag: false })
      }, (error) => {
        this.setState({ archiveBtnQuery: '', archiveFlag: false, popupFlag: false })
      });
    })
  }

  handleUnArchive = (e) => {
    if (e)
      e.stopPropagation()
    this.setState({ unarchiveBtnQuery: 'progress' }, () => {
      this.props.UnArchiveTask(this.props.task, () => {
        this.setState({ unarchiveBtnQuery: '', unArchiveFlag: false, popupFlag: false })
      }, (error) => {
        this.setState({ unarchiveBtnQuery: '', unArchiveFlag: false, popupFlag: false })
      });
    })
  }

  render() {
    const { classes, theme } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      popupFlag,
      selectedColor,
      deleteBtnQuery,
      archiveBtnQuery,
      unarchiveBtnQuery
    } = this.state;
    const ddData = ["Copy", "Color", "Public Link", "Archive", "Delete"];

    return (
      <Fragment>
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={event => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}
            >
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px" }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List
                  onClick={event => { event.stopPropagation() }}>
                  <ListItem
                    disableRipple={true}
                    classes={{ root: classes.menuHeadingItem }}
                  >
                    <ListItemText
                      primary="Select Action"
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map(value =>
                    value == "Color" ? (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{
                          root: classes.selectColorMenuItem,
                          selected: classes.statusMenuItemSelected
                        }}
                        onClick={event => {
                          this.handleColorClick(event);
                        }}
                      >
                        <ListItemText
                          primary={value}
                          classes={{
                            primary: classes.statusItemText
                          }}
                        />
                        <RightArrow
                          htmlColor={theme.palette.secondary.dark}
                          classes={{ root: classes.submenuArrowBtn }}
                        />
                        <div
                          id="colorPickerCnt"
                          className={classes.colorPickerCntLeft}
                          style={
                            pickerOpen ? { display: "block" } : { display: "none" }
                          }
                        >
                          <BlockPicker
                            triangle="hide"
                            color={selectedColor}
                            onChangeComplete={event => {
                              this.colorChange(event, this.props.task);
                            }}
                            colors={[
                              "#D9E3F0",
                              "#F47373",
                              "#697689",
                              "#37D67A",
                              "#2CCCE4",
                              "#555555",
                              "#dce775",
                              "#ff8a65",
                              "#ba68c8",
                              "#ffffff"
                            ]}
                          />
                        </div>
                      </ListItem>
                    ) : (
                      <ListItem
                        key={
                          value === "Archive"
                            ? this.props.task && this.props.task.isDeleted === true
                              ? "Unarchive"
                              : "Archive"
                            : value
                        }
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={this.handleOperations.bind(
                          this,
                          value === "Archive"
                            ? this.props.task && this.props.task.isDeleted === true
                              ? "Unarchive"
                              : "Archive"
                            : value
                        )}
                      >
                        <ListItemText
                          primary={
                            value === "Archive"
                              ? this.props.task && this.props.task.isDeleted === true
                                ? "Unarchive"
                                : "Archive"
                              : value
                          }
                          classes={{
                            primary: classes.statusItemText
                          }}
                        />
                      </ListItem>
                    )
                  )}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
        <React.Fragment>
          {this.state.archiveFlag ? (
            <ActionConfirmation
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText="Cancel"
              successBtnText="Archive"
              alignment="center"
              headingText="Archive"
              iconType="archive"
              msgText={`Are you sure you want to archive this task?`}
              successAction={this.handleArchive}
              btnQuery={archiveBtnQuery}
            />
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {this.state.unArchiveFlag ? (
            <ActionConfirmation
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText="Cancel"
              successBtnText="Unarchive"
              alignment="center"
              iconType="unarchive"
              headingText="Unarchive"
              msgText={`Are you sure you want to unarchive this task?`}
              successAction={this.handleUnArchive}
              btnQuery={unarchiveBtnQuery}
            />
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {this.state.deleteFlag ? (
            <DeleteConfirmDialog
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText="Cancel"
              successBtnText="Delete"
              alignment="center"
              headingText="Delete"
              successAction={this.deleteTask}
              msgText={`Are you sure you want to delete this task?`}
              btnQuery={deleteBtnQuery}
            />
          ) : null}
        </React.Fragment>
      </Fragment>
    );
  }
}

export default withStyles(combineStyles(itemStyles, menuStyles), {
  withTheme: true
})(BulkActionDropdown);
