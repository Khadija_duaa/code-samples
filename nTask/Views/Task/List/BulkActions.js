import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import {
  BulkUpdateTask,
  BulkArchivedTask,
  BulkDeleteTask,
} from "../../../redux/actions/tasks";
import { withStyles } from "@material-ui/core/styles";
import ChangeProjectDropDown from "./ProjectDropdown";
import FlagDropdown from "./FlagDropdown";
import StatusDropdown from "./StatusDropdown";
import Button from "@material-ui/core/Button/";
import AssigneeList from "./AssigneeDropdown";
import listStyles from "./styles";
import { BlockPicker } from "react-color";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import helper from "../../../helper";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import PermissionAlert from "../../../components/Dialog/ConfirmationDialogs/PermissionAlert";
import Typography from "@material-ui/core/Typography";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import { FormattedMessage, injectIntl } from "react-intl";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";


class BulkActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDateOpen: false,
      dueDateOpen: false,
      selectedColor: "#D9E3F0",
      anchorEl: null,
      open: false,
      showConfirmation: false,
      buttonName: "",
      message: "",
      inputType: "",
      prevColor: "#D9E3F0",
      btnQuery: "",
    };

    this.handleStartDateClose = this.handleStartDateClose.bind(this);
    this.handleStartDateToggle = this.handleStartDateToggle.bind(this);
    this.handleDueDateClose = this.handleDueDateClose.bind(this);
    this.handleDueDateToggle = this.handleDueDateToggle.bind(this);
    this.handleColorPickerBtnClick = this.handleColorPickerBtnClick.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.closeConfirmationPopup = this.closeConfirmationPopup.bind(this);
  }
  handleColorPickerBtnClick(event) {
    const { currentTarget } = event;
    this.setState((state) => ({
      anchorEl: currentTarget,
      open: !state.open,
    }));
  }
  handleColorChange(color) {
    this.setState({ selectedColor: color }, () => {
      if (this.state.selectedColor !== this.state.prevColor) {
        const type = helper.RETURN_BULKACTIONTYPES("Color");
        let data = {
          type,
        };
        data["colorCode"] = this.state.selectedColor;
        data.taskIds = this.props.selectedIdsList;
        this.props.BulkUpdateTask(data, () => {});
        this.setState({ prevColor: this.state.selectedColor });
      }
    });
  }
  handleStartDateClose(
    completeDateFormat,
    completePlannedDate,
    selectedIdsList
  ) {
    const type = helper.RETURN_BULKACTIONTYPES("StartDate");
    let data = {
      type,
      taskIds: selectedIdsList,
      planDate: completePlannedDate,
      actualDate: completeDateFormat,
    };
    this.props.BulkUpdateTask(data, () => {
      this.setState({ startDateOpen: false });
    });
  }
  handleStartDateToggle() {
    this.setState((state) => ({ startDateOpen: !state.startDateOpen }));
  }
  showConfirmationPopup(inputType) {
    this.setState({
      showConfirmation: true,
      inputType: inputType || "",
    });
  }
  handleDueDateClose(completeDateFormat, completePlannedDate, selectedIdsList) {
    const type = helper.RETURN_BULKACTIONTYPES("DueDate");
    let data = {
      type,
      taskIds: selectedIdsList,
      planDate: completePlannedDate,
      actualDate: completeDateFormat,
    };

    this.props.BulkUpdateTask(data, () => {
      this.setState({ dueDateOpen: false });
    });
  }
  handleDueDateToggle() {
    this.setState((state) => ({ dueDateOpen: !state.dueDateOpen }));
  }
  handleArchive = () => {
    const type = helper.RETURN_BULKACTIONTYPES("Archive");
    let data = {
      type,
    };
    data.taskIds = this.props.selectedIdsList;
    this.setState({ btnQuery: "progress" }, () => {
      this.props.BulkArchivedTask(data, () => {
        this.props.isBulkUpdated();
        this.setState({ btnQuery: "", inputType: "", showConfirmation: false });
      });
    });
  };

  closeConfirmationPopup() {
    this.setState({ showConfirmation: false });
  }
  handleUnArchive = (e) => {
    if (e) e.stopPropagation();
    let inputType = this.state.inputType;
    const type = helper.RETURN_BULKACTIONTYPES(inputType);
    let data = {
      type: type,
      taskIds: this.props.selectedIdsList,
    };
    this.setState({ btnQuery: "progress" }, () => {
      this.props.isBulkUpdate(data, () => {
        this.setState({
          btnQuery: "",
          inputType: "",
          showConfirmation: false,
        });
      });
    });
  };
  handleDeleteClick = () => {
    const { selectedTaskObj, selectedIdsList, permissionObject, taskPer } = this.props;

    let filterTasksHaveDeletePer = selectedTaskObj.reduce((res, cv) => { /** filter those task which user have the permission to delete */
      if (cv.projectId) {
        if (permissionObject[cv.projectId].task.delete.cando) res.push(cv);
      } else if (taskPer.delete.cando) {
        res.push(cv);
      }
      return res;
    }, []);

    /* function for deleting tasks bulk option */
    const type = helper.RETURN_BULKACTIONTYPES("Delete");
    let data = {
      type,
    };
    //Generating array of taskIds
    let taskIdsArr = filterTasksHaveDeletePer.map((r) => {
      return r.row.taskId;
    });
    data.taskIds = taskIdsArr;
    this.setState({ btnQuery: "progress" }, () => {
      this.props.BulkDeleteTask(data, () => {
        this.props.isBulkUpdated(data.taskIds);
        this.setState({ btnQuery: "", inputType: "", showConfirmation: false });
      });
    });
  };
  handleClickAway = () => {
    this.setState({ open: false });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Color":
        value = "common.action.color.label";
        break;
      case "Archive":
        value = "common.action.archive.confirmation.archive-button.label";
        break;
      case "Delete":
        value = "common.action.delete.confirmation.delete-button.label";
        break;
      case "UnArchive":
        value = "common.action.un-archive.confirmation.title";
        break;
      case "Add Assignee":
        value = "common.bulk-action.addAssignee";
        break;
      case "Change Project":
        value = "common.bulk-action.changeProject";
        break;
      case "Start Date":
        value = "common.bulk-action.startDate";
        break;
      case "Due Date":
        value = "common.bulk-action.dueDate";
        break;
      case "Set Priority":
        value = "common.bulk-action.priority";
        break;
      case "Set Status":
        value = "common.bulk-action.status";
        break;
    }
    return value;
  }
  render() {
    const { btnQuery } = this.state;
    const {
      classes,
      theme,
      isArchivedFilter,
      taskPer,
      selectedTaskObj,
      selectedIdsList,
      intl,
      permissionObject
    } = this.props;
    const {
      dueDateOpen,
      startDateOpen,
      selectedColor,
      anchorEl,
      open,
      showConfirmation,
      inputType,
    } = this.state;
    const id = open ? "simple-popper" : null;
    const styles = (action, i, arr) => {
      return i == 0
        ? { borderRadius: "4px 0 0 4px" }
        : i == arr.length - 1
        ? { borderRadius: "0 4px 4px 0", borderLeft: "none" }
        : { borderLeft: "none", borderRadius: 0 };
    };

    let BulkActionsType = [];
    if (isArchivedFilter) {
      BulkActionsType = [
        this.props.selectedIdsList.length + " "+intl.formatMessage({id:"common.item-selected.label",defaultMessage:"items Selected"}),
        "UnArchive",
        "Delete",
      ];
    } else {
      BulkActionsType = [
        this.props.selectedIdsList.length + " "+intl.formatMessage({id:"common.item-selected.label",defaultMessage:"items Selected"}),
        "Color",
        "Add Assignee",

        "Start Date",
        "Due Date",
        "Set Priority",
      
        "Archive",
        "Delete",
      ];
      if (!teamCanView("customStatusAccess")) {
        BulkActionsType.splice(3, 0, "Change Project");
        BulkActionsType.splice(7, 0, "Set Status");
      }
    }
   
    let filterTasksHaveDeletePer = selectedTaskObj.reduce((res, cv) => { /** filter those task which user have the permission to delete */
      if (cv.projectId) {
        if (!permissionObject[cv.projectId].task.delete.cando) res.push(cv);
      } else if (!taskPer.delete.cando) {
        res.push(cv);
      }
      return res;
    }, []);

    return (
      <div className={classes.BulkActionsCnt}>
        {inputType === "Archive" ? (
          <ActionConfirmation
            open={showConfirmation && taskPer.archive.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.tasks.label2"
                defaultMessage="Archive Tasks"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.tasks.message"
                defaultMessage={`Are you sure you want to archive these ${this.props.selectedIdsList.length} tasks?`}
                values={{ t: this.props.selectedIdsList.length }}
              />
            }
            successAction={this.handleArchive}
            btnQuery={btnQuery}
          />
        ) : inputType === "UnArchive" ? (
          <ActionConfirmation
            open={showConfirmation && taskPer.unarchive.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.tasks.label"
                defaultMessage="Unarchive Tasks"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            msgText={
              <FormattedMessage
                id="common.un-archived.tasks.message"
                defaultMessage={`Are you sure you want to unarchive these ${this.props.selectedIdsList.length} tasks?`}
                values={{ t: this.props.selectedIdsList.length }}
              />
            }
            successAction={this.handleUnArchive}
            btnQuery={btnQuery}
            onClick={this.showConfirmationPopup.bind(this, "UnArchive")}
          /> 
        ) : inputType === "Delete" ? (
          // <PermissionAlert
          //   open={showConfirmation && taskPer.delete.cando}
          //   closeAction={this.closeConfirmationPopup}
          //   successAction={this.handleDeleteClick}
          //   btnQuery={btnQuery}
          //   disabled={selectedIdsList.length - filterTasksHaveDeletePer.length == 0}
          //   type="tasks"
          //   successBtnText={<FormattedMessage id="common.action.delete-task.label2"  defaultMessage="Delete Tasks" />
          //   }
          //   deleteView={true}
          // />
           <DeleteConfirmDialog
            open={showConfirmation && taskPer.delete.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            }
            successAction={this.handleDeleteClick}
            btnQuery={btnQuery}
            disabled={
              selectedIdsList.length - filterTasksHaveDeletePer.length == 0
            }
          >
            {" "}
            <>
              {filterTasksHaveDeletePer.length > 0 && (
                <Typography
                  variant="h5"
                  style={{
                    userSelect: "none",
                    marginBottom: 20,
                    // textAlign: "center",
                  }}
                >
                  {`Note : You cannot perform this action because some of the tasks which you are trying to delete are associated with a project on which you do not have deletion rights. If you continue, only those tasks will be deleted which are not associated with any project.`}
                </Typography>
              )}
               <Typography variant="h5" style={{ userSelect: "none" }}>
              <FormattedMessage
                id="common.action.delete.tasks.label"
                defaultMessage={`Are you sure you want to delete these ${this.props.selectedIdsList.length} tasks?`}
                values={{ t: this.props.selectedIdsList.length }}
              />
            </Typography>
            </>
          </DeleteConfirmDialog>
        ) : null}

        {BulkActionsType.map((action, i, arr) => {
          return i == 1 && !isArchivedFilter ? (
            <>
              <Button
                disabled={i == 0 ? true : false}
                style={styles(action, i, arr)}
                variant="outlined"
                onClick={this.handleColorPickerBtnClick}
                classes={{ outlined: classes.BulkActionBtn }}
                selectedIdsList={this.state.selectedIds}
              >
                <FormattedMessage
                  id={this.getTranslatedId(action)}
                  defaultMessage={action}
                />
              </Button>
              <Popper
                id={id}
                open={open}
                anchorEl={anchorEl}
                disablePortal
                transition
              >
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    id="menu-list-grow"
                    style={{
                      transformOrigin:
                        placement === "bottom" ? "center top" : "left bottom",
                    }}
                  >
                    <ClickAwayListener onClickAway={this.handleClickAway}>
                      <div id="colorPickerCnt">
                        <ColorPicker
                          triangle="hide"
                          onColorChange={(color) => {
                            this.handleColorChange(color);
                          }}
                          selectedColor={selectedColor}
                        />
                      </div>
                    </ClickAwayListener>
                  </Grow>
                )}
              </Popper>
            </>
          ) : i == 3 &&
            !isArchivedFilter &&
            taskPer.taskDetail.taskProject.isAllowEdit && !teamCanView("customStatusAccess") ? (
            <ChangeProjectDropDown
              selectedIdsList={this.props.selectedIdsList}
              isBulkUpdated={this.props.isBulkUpdated}
            />
          ) : i == 6 &&
            !isArchivedFilter &&
            taskPer.taskDetail.editTaskPriority.isAllowEdit ? (
            <FlagDropdown
              selectedIdsList={this.props.selectedIdsList}
              isBulkUpdated={this.props.isBulkUpdated}
            />
          ) : i == 7 &&
            !isArchivedFilter &&
            taskPer.taskDetail.editTaskStatus.isAllowEdit && !teamCanView("customStatusAccess") ? (
            <StatusDropdown
              selectedIdsList={this.props.selectedIdsList}
              isBulkUpdated={this.props.isBulkUpdated}
              taskPer={taskPer}
            />
          ) : i == 2 &&
            !isArchivedFilter &&
            taskPer.taskDetail.taskAssign.isAllowAdd ? (
            <AssigneeList
              selectedIdsList={this.props.selectedIdsList}
              isBulkUpdated={this.props.isBulkUpdated}
            />
          ) : i == 8 && !isArchivedFilter ? (
            <Button
              style={styles(action, i, arr)}
              variant="outlined"
              classes={{ outlined: classes.BulkActionBtn }}
              onClick={this.showConfirmationPopup.bind(this, "Archive")}
            >
              <FormattedMessage
                id={this.getTranslatedId(action)}
                defaultMessage={action}
              />
            </Button>
          ) : i == 9 && !isArchivedFilter /* delete button bulk  */ ? (
            <Button
              style={styles(action, i, arr)}
              variant="outlined"
              classes={{ outlined: classes.BulkActionBtn }}
              onClick={this.showConfirmationPopup.bind(this, "Delete")}
            >
              <FormattedMessage
                id={this.getTranslatedId(action)}
                defaultMessage={action}
              />
            </Button>
          ) : i == 4 && !isArchivedFilter ? (
            <Fragment></Fragment>
          ) : i == 5 && !isArchivedFilter ? (
            <></>
          ) : action !== "Start Date" && (
            <Button
              disabled={i == 0 ? true : false}
              style={styles(action, i, arr)}
              variant="outlined"
              classes={{ outlined: classes.BulkActionBtn }}
              onClick={this.showConfirmationPopup.bind(this, action)}
            >
              <FormattedMessage
                id={this.getTranslatedId(action)}
                defaultMessage={action}
              />
            </Button>
          );
        })}
      </div>
    );
  }
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(listStyles, { withTheme: true }),
  connect(null, { BulkUpdateTask, BulkArchivedTask, BulkDeleteTask })
)(BulkActions);
