import React, { Component, Fragment } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";

import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import { connect } from "react-redux";
import { compose } from "redux";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import RightArrow from "@material-ui/icons/ArrowRight";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { getCompletePermissionsWithArchieve, canView } from "../permissions";
import { DeleteArchiveTask } from "../../../redux/actions/tasks";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import { injectIntl, FormattedMessage } from "react-intl";

class TaskActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      deleteBtnQuery: "",
      archiveBtnQuery: "",
      unarchiveBtnQuery: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleColorClick = this.handleColorClick.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    // this.handleOperations = this.handleOperations.bind(this);
  }

  componentDidMount = () => {
    this.setState({
      selectedColor: this.props.selectedColor,
    });
  };

  handleOperations(value) {
    //
    switch (value) {
      case "Copy":
        this.props.CopyTask(this.props.task, () => { }, () => { });
        break;
      case "Public Link":
        break;
      case "Archive":
        this.setState({ archiveFlag: true, popupFlag: true });
        break;
      case "Unarchive":
        this.setState({ unArchiveFlag: true, popupFlag: true });
        break;
      case "Delete":
        this.setState({ deleteFlag: true, popupFlag: true });
        break;
    }

    this.setState({ open: false, pickerOpen: false });
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  handleDialogClose(e) {
    if (e) e.stopPropagation();
    this.setState({
      popupFlag: false,
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
    });
  }

  handleDelete = (e) => {
    e.stopPropagation();
    const { taskId, isDeleted } = this.props.task;
    if (isDeleted) {
      this.setState(
        { deleteBtnQuery: "progress" },
        //Success
        () => {
          DeleteArchiveTask(
            taskId,
            (response) => {
              this.props.deleteTaskFromArchiveList(taskId);
              this.setState({ deleteBtnQuery: "", deleteFlag: false });
            },
            //Failure
            () => {
              this.setState({ deleteBtnQuery: "", deleteFlag: false });
            }
          );
        }
      );
    } else {
      this.setState({ deleteBtnQuery: "progress" }, () => {
        this.props.DeleteTask(
          this.props.task,
          (response) => {
            this.setState({ deleteBtnQuery: "", deleteFlag: false });
          },
          (error) => {
            this.setState({ deleteBtnQuery: "", deleteFlag: false });
            if (error) {
              this.showSnackBar(error.data.message, "error");
            }
          }
        );
      });
    }
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };

  handleClick(event, placement) {
    event.stopPropagation();
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleColorClick(event) {
    event.stopPropagation();
    if (event.target.closest("#colorPickerCnt") == null) {
      this.setState((prevState) => ({ pickerOpen: !prevState.pickerOpen }));
    }
  }

  colorChange = (color, task) => {
    let self = this;
    this.setState({ selectedColor: color }, function () {
      let obj = Object.assign({}, task);
      this.setState({
        selectedColor: color,
      });
      obj.colorCode = color;
      delete obj.CalenderDetails;
      self.props.UpdateTaskColorFromGridItem(obj, (data) => {
        //
        self.props.UpdateCalenderTask(data, () => { });
      });
    });
  };

  handleArchive = (e) => {
    if (e) e.stopPropagation();
    this.setState({ archiveBtnQuery: "progress" }, () => {
      this.props.ArchiveTask(
        this.props.task,
        (response) => {
          this.setState({
            archiveBtnQuery: "",
            archiveFlag: false,
            popupFlag: false,
          });
        },
        (error) => {
          this.setState({
            archiveBtnQuery: "",
            archiveFlag: false,
            popupFlag: false,
          });
        }
      );
    });
  };

  handleUnArchive = (e) => {
    if (e) e.stopPropagation();
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.UnArchiveTask(
        this.props.task,
        () => {
          this.setState({
            unarchiveBtnQuery: "",
            unArchiveFlag: false,
            popupFlag: false,
          });
          this.props.filterUnArchiveTask(this.props.task.taskId);
        },
        (error) => {
          this.setState({
            unarchiveBtnQuery: "",
            unArchiveFlag: false,
            popupFlag: false,
          });
        }
      );
    });
  };

  getTranslatedId(value) {
    switch (value) {
      case "Color":
        value = "common.action.color.label";
        break;
      case "Archive":
        value = "common.action.archive.confirmation.archive-button.label";
        break;
      case "Delete":
        value = "common.action.delete.confirmation.delete-button.label";
        break;
      case "Unarchive":
        value = "common.action.un-archive.confirmation.title";
        break;
      case "Public Link":
        value = "common.action.public-link.label";
        break;
      case "Copy":
        value = "common.action.copy.label";
        break;
      case "Rename":
        value = "common.action.rename.label";
        break;
    }
    return value;
  }
  render() {
    const {
      classes,
      theme,
      openPublicLinkDialog,
      task,
      isArchivedSelected,
      taskPermission,
    } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      popupFlag,
      selectedColor,
      deleteBtnQuery,
      archiveBtnQuery,
      unarchiveBtnQuery,
    } = this.state;
    const ddData = [];

    let copyPer = taskPermission
      ? taskPermission.copyInWorkSpace.cando
      : true;


    let publicLinkPer = taskPermission
      ? taskPermission.publicLink.cando
      : true;

    let archivePer = taskPermission
      ? taskPermission.archive.cando
      : true;

    let unarchivePer = taskPermission
      ? taskPermission.unarchive.cando
      : true;

    let deletePer = taskPermission
      ? taskPermission.delete.cando
      : true;

    if (!isArchivedSelected && copyPer) {
      ddData.push("Copy");
    }
    if (!isArchivedSelected) {
      ddData.push("Color");
    }
    if (!isArchivedSelected && publicLinkPer) {
      ddData.push("Public Link");
    }
    if (!isArchivedSelected && archivePer) {
      ddData.push("Archive");
    }
    if (isArchivedSelected && unarchivePer) {
      ddData.push("Unarchive");
    }
    if (deletePer) {
      ddData.push("Delete");
    }

    return (
      <Fragment>
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={(event) => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
            >
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px" }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List
                  onClick={(e) => {
                    e.stopPropagation();
                  }}
                >
                  <ListItem
                    disableRipple={true}
                    classes={{ root: classes.menuHeadingItem }}
                  >
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="common.action.label"
                          defaultMessage="Select Action"
                        />
                      }
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>

                  {ddData.map((value) =>
                    value == "Color" ? (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{
                          root: classes.selectColorMenuItem,
                          selected: classes.statusMenuItemSelected,
                        }}
                        onClick={(event) => {
                          this.handleColorClick(event);
                        }}
                      >
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id={this.getTranslatedId(value)}
                              defaultMessage={value}
                            />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                        <RightArrow
                          htmlColor={theme.palette.secondary.dark}
                          classes={{ root: classes.submenuArrowBtn }}
                        />
                        <div
                          id="colorPickerCnt"
                          className={classes.colorPickerCntLeft}
                          style={
                            pickerOpen ? { display: "block" } : { display: "none" }
                          }
                        >
                          <ColorPicker
                            triangle="hide"
                            onColorChange={(color) => {
                              this.colorChange(color, this.props.task);
                            }}
                            selectedColor={selectedColor}
                          />
                        </div>
                      </ListItem>
                    ) : (
                      <Fragment>
                        {value === "Public Link" ? (
                          publicLinkPer ? (
                            <ListItem
                              button
                              key={value}
                              disableRipple={true}
                              classes={{ selected: classes.statusMenuItemSelected }}
                              onClick={(event) =>
                                openPublicLinkDialog(event, task.id, task.taskTitle)
                              }
                            >
                              <ListItemText
                                primary={
                                  <FormattedMessage
                                    id={this.getTranslatedId(value)}
                                    defaultMessage={value}
                                  />
                                }
                                classes={{
                                  primary: classes.statusItemText,
                                }}
                              />
                            </ListItem>
                          ) : null
                        ) : (
                          <ListItem
                            key={value}
                            button
                            disableRipple={true}
                            classes={{ selected: classes.statusMenuItemSelected }}
                            onClick={this.handleOperations.bind(this, value)}
                          >
                            <ListItemText
                              primary={
                                <FormattedMessage
                                  id={this.getTranslatedId(value)}
                                  defaultMessage={value}
                                />
                              }
                              classes={{
                                primary: classes.statusItemText,
                              }}
                            />
                          </ListItem>
                        )}
                      </Fragment>
                    )
                  )}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
        <React.Fragment>
          {this.state.archiveFlag ? (
            <ActionConfirmation
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.cancel.label"
                  defaultMessage="Cancel"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.archive.confirmation.archive-button.label"
                  defaultMessage="Archive"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="common.action.archive.confirmation.archive-button.label"
                  defaultMessage="Archive"
                />
              }
              iconType="archive"
              msgText={
                <FormattedMessage
                  id="common.archived.tasks.messageb"
                  defaultMessage="Are you sure you want to archive this task?"
                />
              }
              successAction={this.handleArchive}
              btnQuery={archiveBtnQuery}
            />
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {this.state.unArchiveFlag ? (
            <ActionConfirmation
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.cancel.label"
                  defaultMessage="Cancel"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.title"
                  defaultMessage="Unarchive"
                />
              }
              alignment="center"
              iconType="unarchive"
              headingText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.title"
                  defaultMessage="Unarchive"
                />
              }
              msgText={
                <FormattedMessage
                  id="common.un-archived.tasks.messageb"
                  defaultMessage="Are you sure you want to unarchive these tasks?"
                />
              }
              successAction={this.handleUnArchive}
              btnQuery={unarchiveBtnQuery}
            />
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {this.state.deleteFlag ? (
            <DeleteConfirmDialog
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.cancel.label"
                  defaultMessage="Cancel"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.delete.confirmation.delete-button.label"
                  defaultMessage="Delete"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="common.action.delete.confirmation.delete-button.label"
                  defaultMessage="Delete"
                />
              }
              successAction={this.handleDelete}
              msgText={
                <FormattedMessage
                  id="common.action.delete.tasks.message"
                  defaultMessage="Are you sure you want to delete this task?"
                />
              }
              btnQuery={deleteBtnQuery}
            />
          ) : null}
        </React.Fragment>
      </Fragment>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {

  };
};
export default compose(
  withSnackbar,
  injectIntl,
  connect(mapStateToProps),
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true,
  })
)(TaskActionDropdown);
