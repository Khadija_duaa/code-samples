import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { withSnackbar } from "notistack";

import loadable from '@loadable/component'
const CustomTable = loadable(() => import("../../../components/CustomTable2/listViewTable.cmp"));
import { useDispatch, useSelector } from "react-redux";
import isEqual from "lodash/isEqual";
import { AgGridColumn } from "@ag-grid-community/react";
import TaskActionDropdown from "../TaskActionDropdown/TaskActionDropdown";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import CircularIcon from "@material-ui/icons/Brightness1";
import { getTemplate } from "../../../utils/getTemplate";
import withStyles from "@material-ui/core/styles/withStyles";
import TaskStatusChangeDialog from "../../../components/Templates/TaskStatusChangeDialog";
import AddIcon from "@material-ui/icons/Add";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import {
  createTask,
  getSavedFilters,
  updateTaskData,
  CheckAllTodoList,
  startTimer,
  stopTimer,
  UpdateTaskCustomField,
  updateTaskObject,
  exportBulkTask,
} from "../../../redux/actions/tasks";
import { emptyTask } from "../../../utils/constants/emptyTask";
import taskListStyles from "./taskList.style";
import { FormattedMessage, injectIntl } from "react-intl";
import { priorityData } from "../../../helper/taskDropdownData";
import { compose } from "redux";
import helper from "../../../helper";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import searchQuery from "../../../components/CustomTable2/ColumnSettingDropdown/searchQuery";
import isEmpty from "lodash/isEmpty";
import moment from "moment";
import Typography from "@material-ui/core/Typography";
import { Circle } from "rc-progress";
import { taskColumnDefs, sortAlphabetically } from "./taskColumns";
import Taskcmp from "../../../components/BulkActions/Task.cmp.js";
import GroupByComponents from "../../../components/CustomTable2/GroupByComponents/GroupByComponents";
import CustomDatePicker from "../../../components/DatePicker/DatePicker/ListViewDatePicker";
import SvgIcon from "@material-ui/core/SvgIcon";
import ToggleUpdateIcon from "../../../components/Icons/ToggleUpdateIcon";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import IssueIcon from "../../../components/Icons/IssueIcon";
import RiskIcon from "../../../components/Icons/RiskIcon";
import Stared from "../../../components/Starred/Starred";
import { isDateEqual, inDateRange, excludeOffdays, getCalendar } from "../../../helper/dates/dates";
import { taskDetailDialogState } from "../../../redux/actions/allDialogs";
import ColumnSelector from "../../../redux/selectors/columnSelector";
import isNull from "lodash/isNull";
import CustomFieldRenderer from "../../../components/CustomTable2/CustomFieldsColumn/CustomFieldRenderer";
import { headerProps } from "./constants";
import SideDrawer from "../Drawer/Drawer";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import RecurrenceBtn from "../../../components/Buttons/RecurrenceBtn";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import AttachmentIcon from "@material-ui/icons/Attachment";
import TaskFilter from "./TaskFilter/taskFilter.view";
import StopIcon from "../../../components/Icons/StopIcon";
import PlayIcon from "../../../components/Icons/PlayIcon";
import { grid } from "../../../components/CustomTable2/gridInstance";
import isUndefined from "lodash/isUndefined";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import TemplatesSelector from "../../../redux/selectors/templatesSelector";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { setGlobalTaskTime } from "../../../redux/actions/globalTimerTask";
import { doesFilterPass } from "./TaskFilter/taskFilter.utils";
import { exportBulkIssue } from "../../../redux/actions/issues";
import fileDownload from "js-file-download";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../../mixpanel';
// import { MixPanelEvents } from '../';
import { GlobalTimeCustom, ProjectMandatory, TaskEffortMandatory } from "../../../helper/config.helper";
import { CanAccess } from "../../../components/AccessFeature/AccessFeature.cmp";
import { addPendingHandler } from "../../../redux/actions/backProcesses";

// const defaultColDef = { minWidth: 200, headerClass: "customHeader" };
const TaskList = React.memo(({ classes, theme, intl, enqueueSnackbar, style }) => {
  const state = useSelector(state => {
    return {
      taskColumns: ColumnSelector(state).task.columns,
      sections: ColumnSelector(state).task.sections,
      nonSectionFields: ColumnSelector(state).task.nonSectionFields,
      tasks: state.tasks.data,
      projects: state.projects.data,
      members: state.profile.data.member.allMembers,
      workspaceTemplates: state.workspaceTemplates.data,
      globalTimerTaskState: state.globalTimerTask,
      permissionObject: ProjectPermissionSelector(state),
      workspaceTaskPer: state.workspacePermissions.data.task,
      workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
      profileState: state.profile.data,
      quickFilters: state.tasks.quickFilters,
      taskFilters: state.tasks.taskFilter,
      templates: TemplatesSelector(state),
      companyInfo: state.whiteLabelInfo.data,
    };
  });
  const sectionGroup = localStorage.getItem("sectiongrouping");

  const dispatch = useDispatch();
  const [repeatDrawer, setRepeatDrawer] = useState("");
  const [selectedTasks, setSelectedTasks] = useState([]);
  const [mappingDialogState, setMappingDialogState] = useState({
    tempProject: null,
    openStatusDialog: false,
    newTemplateItem: {},
    oldStatusItem: {},
    tempTask: {},
  });
  const timerState = useRef(null);
  const statusTemplates = useRef(null);
  const [markChecklistActionConf, setMarkChecklistActionConf] = useState(false);
  const [doneStatus, setDoneStatus] = useState(null);
  const [otherStatus, setOtherStatus] = useState(false);
  const [selectedTask, setSelectedTask] = useState(null);
  const [sectionGrouping, setSectionGrouping] = useState(sectionGroup === "true" ? true : false);
  const {
    taskColumns = [],
    tasks = [],
    projects,
    members,
    sections = [],
    nonSectionFields = [],
    globalTimerTaskState,
    workspaceTaskPer,
    permissionObject,
    workspaceStatus,
    profileState,
    quickFilters,
    taskFilters,
    templates,
    companyInfo,
  } = state;

  useEffect(() => {
    getSavedFilters("task", dispatch);
    setGloabalTimer();
    return () => {
      grid.grid = null;
    };
  }, []);
  useEffect(() => {
    statusTemplates.current = templates;
  }, [templates]);
  useEffect(() => {
    grid.grid && grid.grid.onFilterChanged();
  }, [taskFilters, quickFilters]);
  const updateTask = (task, obj) => {
    updateTaskData(
      { task, obj },
      dispatch,
      //Success
      task => {
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(task.id);
          rowNode.setData(task);
        }
      },
      err => {
        let errMessage = err?.data?.message || "Oops! Server throws error.";
        showSnackBar(errMessage, "error");
      }
    );
  };
  //Handle planned/actual date save
  const handleDateSave = (type, date, task, time) => {
    const formatDate = helper.RETURN_CUSTOMDATEFORMAT(date);

    let obj;
    let key;
    switch (type) {
      case "actualStartDate":
        obj = { actualStartDate: formatDate, actualStartTime: time };
        key = "actualdate";
        break;
      case "actualDueDate":
        obj = { actualDueDate: formatDate, actualDueTime: time };
        key = "actualdate";
        break;
      case "startDate":
        obj = { startDate: formatDate, startTime: time };
        key = "planneddate";
        break;
      case "dueDate":
        obj = { dueDate: formatDate, dueTime: time };
        key = "planneddate";
        break;
      default:
    }

    updateTask(task, obj);
  };
  const handleTaskStared = (stared, obj) => {
    updateTask(obj, { isStared: stared });
  };
  const handlePriorityChange = (priority, obj) => {
    // Handle Priority change

    updateTask(obj, { priority: priority.value });
  };
  const handleUpdateAssignee = (assignees, obj) => {
    // Handle Assignee change
    const assigneeList = assignees.map(a => a.userId);
    updateTask(obj, { assigneeList: assigneeList });
  };

  const markAllConfirmDialogOpen = () => {
    setMarkChecklistActionConf(true);
  };
  const markAllConfirmDialogClose = () => {
    setMarkChecklistActionConf(false);
    setDoneStatus(null);
    setSelectedTask(null);
    setOtherStatus(false)
  };
  const handleStatusChange = (status, obj) => {
    const { loggedInTeam, workspace } = profileState;
    // const currentWorkspace = workspace.find(t => t.teamId === loggedInTeam);
    // complete status if tasks efforts mandatory
    // check if task efforts mandortry in workspace settings the allow user to confirm status changing
    if (TaskEffortMandatory(loggedInTeam)) {
      if (status.obj.isDoneState) {
        setDoneStatus(status);
      }
      setOtherStatus(status)
      setSelectedTask(obj);
      markAllConfirmDialogOpen();
      return;
    };

    if (status.obj.isDoneState) {
      setDoneStatus(status);
      setSelectedTask(obj);
      markAllConfirmDialogOpen();
      return;
    }
    updateTask(obj, { status: status.value });
  };
  const handleUpdateStatus = () => {
    if (doneStatus) {
      const checkData = {
        taskId: selectedTask.id,
        checkAll: true,
      };
      CheckAllTodoList(
        checkData,
        succ => { },
        fail => { },
        profileState,
        dispatch
      );
      updateTask(selectedTask, { status: doneStatus.value });
    } else {
      updateTask(selectedTask, { status: otherStatus.value });
    }
    markAllConfirmDialogClose();
  };
  const showMappingDialog = projectId => {
    let attachedProject = projects.find(p => p.projectId == projectId);
    return attachedProject && attachedProject.projectTemplateId != workspaceStatus.templateId;
  };
  const getDefaultTemplateProject = item => {
    const projectId = item.length ? item[0].id : null;
    if (projectId) {
      let attachedProject = projects.find(p => p.projectId == projectId);
      if (attachedProject && attachedProject.projectTemplate) {
        return attachedProject.projectTemplate;
      } else {
        return workspaceStatus;
      }
    } else {
      return workspaceStatus;
    }
  };
  // Handle Priority change
  const handleProjectChange = (newValue, obj) => {
    let defaultWsStatus = workspaceStatus.statusList.find(s => s.isDefault) || {};
    const projectId = newValue.length ? newValue[0].id : obj.projectId;

    if (newValue.length == 0 && !CanAccess({ group: 'task', feature: 'statusTitle' })) {
      /** if user is removing project and in task module the status feature is hide then do not show mapping dialogue, default workspace initial status will be added to the task  */
      updateTask(obj, {
        projectId: "",
        status: defaultWsStatus.statusId,
        statusTitle: defaultWsStatus.statusTitle
      });
      return;
    }

    if (newValue.length && !CanAccess({ group: 'task', feature: 'statusTitle' })) {
      /** if user selects a project or change project and in task module the status feacture is hide then do not show maping screen backend will automatically change its status and status title respective to selected project */
      updateTask(obj, { projectId: newValue[0].id });
      return;
    }

    if (!showMappingDialog(projectId)) {
      updateTask(obj, { projectId: newValue.length > 0 ? newValue[0].id : "" });
    }
    else {
      let taskTemplate = getTemplate(obj);
      const taskStatusData = statusData(taskTemplate ? taskTemplate.statusList : false);
      let taskStatus = taskStatusData.find(el => el.value == obj.status);
      let template = getDefaultTemplateProject(newValue);
      setMappingDialogState({
        tempProject: newValue || null,
        openStatusDialog: true,
        newTemplateItem: template,
        oldStatusItem: taskStatus,
        tempTask: obj,
      });
    }
  };
  // export issue api function
  const handleExportTask = props => {
    const selectedNodes = grid.grid.getSelectedNodes();
    const selectedNodesLength = selectedNodes?.length || 1;
    const postObj = selectedNodes.length
      ? { taskIds: selectedNodes.map(i => i.data.taskId) }
      : { taskIds: [props.node.data.taskId] };

    const obj = {
      type: 'tasks',
      apiType: 'post',
      data: postObj,
      fileName: 'tasks.xlsx',
      apiEndpoint: 'api/UserTask/ExportBulkTasks',
    }
    addPendingHandler(obj, dispatch);

    // exportBulkTask(postObj, dispatch, res => {
    //   fileDownload(res.data, "tasks.xlsx");
    //   showSnackBar(`${selectedNodesLength} tasks exported successfully`, "success");
    // },
    //   (err) => {
    //     if (err.response.status == 405) {
    //       showSnackBar("You don't have sufficient rights to export tasks", "error");
    //     } else {
    //       showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //     }
    //   });

  };
  // export all issue api function
  const handleExportAllTasks = props => {

    let allNodes = [];
    grid.grid.forEachNodeAfterFilter(node => allNodes.push(node.data));
    const postObj = allNodes.length && { taskIds: allNodes.map(i => i.taskId) };

    const obj = {
      type: 'tasks',
      apiType: 'post',
      data: postObj,
      fileName: 'tasks.xlsx',
      apiEndpoint: 'api/UserTask/ExportBulkTasks',
    }
    addPendingHandler(obj, dispatch);

    // exportBulkTask(postObj, dispatch, res => {
    //   fileDownload(res.data, "tasks.xlsx");
    //   showSnackBar(`${allNodes.length} tasks exported successfully`, "success");
    // },
    //   (err) => {
    //     if (err.response.status == 405) {
    //       showSnackBar("You don't have sufficient rights to export tasks", "error");
    //     } else {
    //       showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //     }
    //   });
  };
  const getContextMenuItems = useCallback(params => {
    const selectedNodes = grid.grid.getSelectedNodes();
    let result = [
      {
        // custom item
        name: selectedNodes.length ? "Export Selected Task(s)" : "Export Task",
        action: () => handleExportTask(params),
        // cssClasses: ['redFont', 'bold'],
      },
      {
        // custom item
        name: "Export all Tasks",
        action: () => handleExportAllTasks(params),
        // cssClasses: ['redFont', 'bold'],
      },
    ];
    return result;
  }, []);
  //Handle Add Task
  const handleAddTask = (data, callback) => {
    mixpanel.track(MixPanelEvents.TaskCreationEvent, data);
    //Post Obj is object to be posted to backend for task creation
    const postObj = { taskTitle: data.value, clientId: data.clientId };

    //Dispatch Obj is object that is dispatched before the api call for quick entry
    const dispatchObj = {
      ...emptyTask,
      taskTitle: data.value,
      clientId: data.clientId,
      id: data.clientId,
      taskId: data.clientId,
      isNew: true,
    };
    if (data.projectId) {
      postObj.projectId = data.projectId; // if project is mandatory than add projectId
      dispatchObj.projectId = data.projectId
    }
    //Calling grid add method to add task
    // callback(dispatchObj, "add");
    createTask(postObj, dispatch, dispatchObj, res => {
      //Calling callback to edit the added record with the actual data
      // callback(res, "edit");
    });
  };
  const startTime = task => {
    // if (GlobalTimeCustom(task.teamId)) {
    //   showSnackBar('Timer cannot be started', "error");
    //   return
    // }
    const startTime = "0:0:0";
    const data = {
      taskId: task.taskId,
      startTime: startTime,
      isStart: true,
    };
    startTimer(
      data,
      response => {
        const { createdDate, currentDate } = response.data;
        dispatch(setGlobalTaskTime(task, createdDate, currentDate));
      },
      error => {
        const err = error && error.data.message;
        const message = err || "Server throws error";
        showSnackBar(message, "error");
      },
      dispatch
    );
  };
  const stopTime = task => {
    const data = {
      taskId: task.taskId,
      isStart: false,
    };
    stopTimer(
      data,
      res => {
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(res.id);
          rowNode.setData(res);
        };
        showSnackBar('Task Effort added successfully!', "success");
        // if (!res.timeLogged || res.timeLogged === "00:00") {
        //   showSnackBar(`Oops! Effort not saved. Please add atleast 1 minute task effort!`, "error");
        // }
      },
      err => {
        if (err && err.data) showSnackBar(`Oops! Effort not saved .${err.data}`, "error");
      },
      dispatch
    );
  };
  const statusData = statusArr => {
    if (statusArr) {
      return statusArr.map(item => {
        return {
          label: item.statusTitle,
          value: item.statusId,
          icon: <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "20px" }} />,
          color: item.statusColor,
          obj: item,
          statusTitle: item.statusTitle,
          statusColor: item.statusColor,
        };
      });
    }
    return [];
  };

  // Generate list of all projects for dropdown understandable form
  const generateProjectDropdownData = task => {
    let filteredProjects = projects.filter(p => p.projectId !== task.projectId);
    const projectsArr = filteredProjects.map(project => {
      return { label: project.projectName, id: project.projectId, obj: task };
    });
    return projectsArr;
  };
  //Update Row if  Task Timer value is changed
  useEffect(() => {
    setGloabalTimer()
  }, [globalTimerTaskState, grid, grid.grid]);
  const setGloabalTimer = () => {
    if (!isEmpty(grid.grid)) {
      const task = timerState.current
        ? timerState.current.task
        : globalTimerTaskState
          ? globalTimerTaskState.task
          : null;
      const rowNode = task && grid.grid.getRowNode(task.id);
      rowNode && rowNode.setData(task);
      timerState.current = globalTimerTaskState;
    }
  }
  const priorityDData = useMemo(row => {
    return priorityData(theme, classes, intl);
  }, []);
  const StatusDropdownCmp = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    let template = rowData.projectId
      ? statusTemplates.current[rowData.projectId]
        ? statusTemplates.current[rowData.projectId]
        : statusTemplates.current["workspaceDefaultTemplate"]
      : statusTemplates.current["workspaceDefaultTemplate"];
    const taskStatusData = template && template.dropDownData;
    let selectedStatus = rowData
      ? taskStatusData.find(item => item.value == rowData.status) || taskStatusData[0]
      : taskStatusData[0];
    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    const isArcheive = rowData.isDeleted;
    return (
      <StatusDropdown
        onSelect={status => {
          handleStatusChange(status, rowData);
        }}
        buttonType={"listButton"}
        option={selectedStatus}
        options={taskStatusData}
        toolTipTxt={selectedStatus.label}
        // writeFirst={true}
        writeFirst={!TaskEffortMandatory(rowData.teamId)}
        disabled={isArcheive || !taskPermission.taskDetail.editTaskStatus.isAllowEdit}
        dropdownProps={{
          disablePortal: false,
        }}
      />
    );
  };
  const PriorityDropdown = row => {
    const rowData = row.data ? row.data : { data: {} };
    const taskPriorityData = priorityDData;
    const selectedPriority = taskPriorityData.find(p => p.value == rowData.priority);
    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    const isArcheive = rowData.isDeleted;
    return (
      <StatusDropdown
        onSelect={priority => {
          handlePriorityChange(priority, row.data);
        }}
        option={selectedPriority}
        options={taskPriorityData}
        buttonType={"listIconButton"}
        toolTipTxt={
          <FormattedMessage id="common.action.priority.label" defaultMessage="Task Priority" />
        }
        btnProps={{
          customClasses: {
            label: classes.priorityLabel,
          },
        }}
        dropdownProps={{
          disablePortal: false,
        }}
        disabled={isArcheive || !taskPermission.taskDetail.editTaskPriority.isAllowEdit}
      />
    );
  };
  const TaskTitleCellRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={classes.taskTitleCnt}>
        <div className={`${classes.taskTitleTextCnt} wrapText`} title={rowData.taskTitle}>
          <span className={classes.taskTitle}>{rowData.taskTitle} </span>
        </div>
        {rowData.isNew && <div style={{ minWidth: 54 }}>
          <span className={classes.newTag}>New</span>
        </div>}
        {RecurrenceRenderer(row)}
        <Stared
          isStared={rowData.isStared}
          handleCallback={isStared => handleTaskStared(isStared, rowData)}
          diabled={rowData.isDeleted}
        />
      </div>
    );
  };
  const TimeLoggedRenderer = row => {
    const rowData = row.data ? row.data : {};
    const timeLogged =
      rowData.timeLogged == "00:00" || !rowData.timeLogged ? "" : rowData.timeLogged;
    const isArchive = rowData.isDeleted;
    const timerStarted = timerState.current && timerState.current.task && timerState.current.task.taskId == rowData.taskId;
    return (
      <div className={classes.timeLogCellCnt}>
        <CustomIconButton
          variant={"transparent"}
          onClick={() =>
            timerStarted ? stopTime(rowData) : startTime(rowData)
          }
          style={{
            width: 12,
            height: 12,
            padding: 0,
            marginRight: 6,
          }}
          data-rowClick="cell"
          disabled={isArchive}>
          {timerState.current &&
            timerState.current.task &&
            timerState.current.task.taskId == rowData.taskId ? (
            <StopIcon className={classes.stopIcon} />
          ) : (
            <PlayIcon className={classes.playIcon} />
          )}
        </CustomIconButton>
        <Typography align="center" className={classes.timeLoggedText}>
          {helper.CHANGETIMEFORMAT(timeLogged)}
        </Typography>
      </div>
    );
  };
  const ActualStartDateRenderer = row => {
    const rowData = row.data ? row.data : { data: {} };
    const { actualStartDate, actualStartTime, actualDueDate } = rowData;
    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    const isArcheive = rowData.isDeleted;
    return (
      <CustomDatePicker
        date={actualStartDate && moment(actualStartDate)}
        dateFormat="MMM DD, YYYY"
        timeInput={true}
        onSelect={(date, time) => {
          handleDateSave("actualStartDate", date, rowData, time);
        }}
        selectedTime={actualStartTime}
        timeInputLabel={intl.formatMessage({
          id: "meeting.creation-dialog.form.start-time.label",
          defaultMessage: "Start Time",
        })}
        datePickerProps={{
          filterDate: date => {
            return actualDueDate
              ? moment(date).isBefore(actualDueDate, "day") || moment(date).isSame(actualDueDate, "day")
              : true;
          },
        }}
        // btnProps={{ className: classes.datePickerCustomStyle }}
        disabled={isArcheive || !taskPermission.taskDetail.taskActualStartEndDate.isAllowEdit}

        style={{ display: "flex", alignItems: "center", height: "100%" }}
        PopperProps={{ disablePortal: false }}
        btnType="transparent"
        placeholder={"Select"}
        icon={false}
      />
    );
  };
  const ActualDueDateRenderer = row => {
    const rowData = row.data ? row.data : { data: {} };
    const { actualDueDate, actualDueTime, actualStartDate } = rowData;

    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    // const obj = rowData.projectId ? { projectId: rowData.projectId } : { workspaceId: profileState.loggedInTeam };
    // const calendarWorkingdays = getCalendar(obj)?.workingdays || null;
    const isArcheive = rowData.isDeleted;
    return (
      <CustomDatePicker
        date={actualDueDate && moment(actualDueDate)}
        dateFormat="MMM DD, YYYY"
        timeInput={true}
        onSelect={(date, time) => {
          handleDateSave("actualDueDate", date, rowData, time);
        }}
        selectedTime={actualDueTime}
        timeInputLabel={intl.formatMessage({
          id: "meeting.creation-dialog.form.start-time.label",
          defaultMessage: "Start Time",
        })}
        datePickerProps={{
          filterDate: date => {
            return actualStartDate
              ? moment(date).isAfter(actualStartDate, "day") || moment(date).isSame(actualStartDate, "day")
              : true;
          },
        }}
        // btnProps={{ className: classes.datePickerCustomStyle }}
        disabled={isArcheive || !taskPermission.taskDetail.taskActualStartEndDate.isAllowEdit}
        style={{ display: "flex", alignItems: "center", height: "100%" }}
        PopperProps={{ disablePortal: false }}
        btnType="transparent"
        placeholder={"Select"}
        icon={false}
      />
    );
  };
  const PlannedStartDateRenderer = row => {
    const rowData = row.data ? row.data : { data: {} };
    const { startDate, startTime, dueDate } = rowData;
    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    const obj = rowData.projectId ? { projectId: rowData.projectId } : { workspaceId: profileState.loggedInTeam };
    const calendarWorkingdays = getCalendar(obj) || null;
    const isArcheive = rowData.isDeleted;
    return (
      <CustomDatePicker
        date={startDate && moment(startDate)}
        dateFormat="MMM DD, YYYY"
        timeInput={true}
        onSelect={(date, time) => {
          handleDateSave("startDate", date, rowData, time);
        }}
        selectedTime={startTime}
        timeInputLabel={intl.formatMessage({
          id: "meeting.creation-dialog.form.start-time.label",
          defaultMessage: "Start Time",
        })}
        datePickerProps={{
          filterDate: date => {
            // return dueDate ? date.isBefore(dueDate, "day") || date.isSame(dueDate, "day") : true;
            return excludeOffdays(moment(date), calendarWorkingdays) ?
              dueDate ? moment(date).isBefore(dueDate, "day") || moment(date).isSame(dueDate, "day") : true
              : false;
          },
        }}
        // btnProps={{ className: classes.datePickerCustomStyle }}
        disabled={isArcheive || !taskPermission.taskDetail.taskPlannedStartEndDate.isAllowEdit}
        style={{ display: "flex", alignItems: "center", height: "100%" }}
        PopperProps={{ disablePortal: false }}
        btnType="transparent"
        placeholder={"Select"}
        icon={false}
      />
    );
  };
  const PlannedDueDateRenderer = row => {
    const rowData = row.data ? row.data : { data: {} };
    const { dueDate, dueTime, startDate } = rowData;
    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    const obj = rowData.projectId ? { projectId: rowData.projectId } : { workspaceId: profileState.loggedInTeam };
    const calendarWorkingdays = getCalendar(obj) || null;
    const isArcheive = rowData.isDeleted;
    return (
      <CustomDatePicker
        date={dueDate && moment(dueDate)}
        dateFormat="MMM DD, YYYY"
        timeInput={true}
        onSelect={(date, time) => {
          handleDateSave("dueDate", date, rowData, time);
        }}
        selectedTime={dueTime}
        timeInputLabel={intl.formatMessage({
          id: "meeting.creation-dialog.form.start-time.label",
          defaultMessage: "Start Time",
        })}
        datePickerProps={{
          filterDate: date => {
            // return startDate ? date.isAfter(startDate, "day") || date.isSame(startDate, "day") : true;
            return excludeOffdays(moment(date), calendarWorkingdays) ?
              startDate ? moment(date).isAfter(startDate, "day") || moment(date).isSame(startDate, "day") : true
              : false;
          },
        }}
        // btnProps={{ className: classes.datePickerCustomStyle }}

        disabled={isArcheive || !taskPermission.taskDetail.taskPlannedStartEndDate.isAllowEdit}
        style={{ display: "flex", alignItems: "center", height: "100%" }}
        PopperProps={{ disablePortal: false }}
        btnType="transparent"
        placeholder={"Select"}
        icon={false}
      />
    );
  };
  const ProjectDropdownRenderer = row => {
    const rowData = row.data || {};
    const selectedProject = rowData.projectId
      ? [
        {
          label: rowData.projectInfor ? rowData.projectInfor.projectName : "",
          id: rowData.projectId,
          obj: rowData,
        },
      ]
      : [];
    const taskEffort = rowData.userTaskEffort;
    const isDisabled = taskEffort && taskEffort.length > 0;
    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    const isArcheive = rowData.isDeleted;
    return (
      <SearchDropdown
        initSelectedOption={selectedProject}
        obj={rowData}
        disabled={isArcheive || isDisabled || !taskPermission.taskDetail.taskProject.isAllowEdit}
        tooltip={isDisabled}
        key={rowData.projectId}
        popperProps={{ disablePortal: false }}
        tooltipText={
          <FormattedMessage
            id="task.detail-dialog.project.hint1"
            defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
          />
        }
        optionsList={generateProjectDropdownData(rowData)}
        singleSelect
        updateAction={handleProjectChange}
        selectedOptionHead={intl.formatMessage({
          id: "common.task-project.label",
          defaultMessage: "Task Project",
        })}
        allOptionsHead={intl.formatMessage({
          id: "project.label",
          defaultMessage: "Projects",
        })}
        buttonPlaceholder={
          <FormattedMessage
            id="task.creation-dialog.form.project.placeholder"
            defaultMessage="Select Project"
          />
        }
        buttonProps={{
          style: {
            width: "100%",
          },
        }}
        btnTextProps={{
          style: {
            width: "90%",
            overflow: "hidden",
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
          },
        }}
        isClearable={!ProjectMandatory()}
      />
    );
  };
  const AssigneeDropdownRenderer = row => {
    const rowData = row.data || {};
    const membersObjArr = members && members.filter(m => rowData.assigneeList.includes(m.userId));
    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    const isArcheive = rowData.isDeleted;
    return (
      <span data-rowClick="cell">
        <AssigneeDropdown
          popperProps={{ disablePortal: false }}
          assignedTo={membersObjArr || []}
          updateAction={handleUpdateAssignee}
          isArchivedSelected={isArcheive}
          obj={rowData}
          avatarSize={"xsmall"}
          buttonVariant={"small"}
          customIconButtonProps={{
            classes: {
              root: classes.iconBtnStyles,
            },
          }}
          totalAssigneeBtnProps={{
            style: {
              height: 29,
              width: 29,
              fontSize: "14px",
              fontWeight: theme.typography.fontWeightLarge,
            },
          }}
          customIconRenderer={<AddIcon htmlColor={theme.palette.text.dark} />}
          style={{ height: "100%" }}
          disabled={
            isArcheive ||
            (!taskPermission.taskDetail.taskAssign.isAllowDelete &&
              !taskPermission.taskDetail.taskAssign.isAllowAdd)
          }
          addPermission={taskPermission.taskDetail.taskAssign.isAllowAdd}
          deletePermission={taskPermission.taskDetail.taskAssign.isAllowDelete}
        />
      </span>
    );
  };
  const DateRenderer = row => {
    const rowData = row.data || {};
    const formatedDate = moment(rowData.createdDate).format("MMM DD, YY");
    return formatedDate;
  };
  const UpdatedDateRenderer = row => {
    const rowData = row.data || {};
    const formatedDate = moment(rowData.updatedDate).format("MMM DD, YY");
    return formatedDate;
  };
  const CreatedByRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} ${classes.textCenter} wrapText`} title={rowData.createdBy}>
        <span className={classes.taskTitle}>
          {!isEmpty(rowData.createdBy) ? rowData.createdBy : "-"}{" "}
        </span>
      </div>
    );
  };
  const UpdatedByRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} ${classes.textCenter} wrapText`} title={rowData.updatedBy}>
        <span className={classes.taskTitle}>
          {!isEmpty(rowData.updatedBy) ? rowData.updatedBy : "-"}{" "}
        </span>
      </div>
    );
  };
  const ProgressRenderer = row => {
    const rowData = row.data || {};

    const overdue = helper.RETURN_OVER_DUE_DAYS_WITH_PROGRESS(
      rowData.actualDueDateString || "",
      rowData.progress || 0
    );

    return (
      <div style={{ display: "flex", alignItems: "center" }}>
        <div style={{ width: 30, height: 30, position: "relative" }}>
          <Circle
            percent={rowData.progress}
            strokeWidth="10"
            trailWidth=""
            trailColor="#dedede"
            strokeColor={overdue ? "#de133e" : "#30d56e"}
          />
          <Typography
            variant="h6"
            align="center"
            style={{
              position: "absolute",
              top: "50%",
              left: "50%",
              fontSize: "10px",
              transform: "translate(-50%, -50%)",
            }}>
            {rowData.progress}
          </Typography>
        </div>
      </div>
    );
  };
  const CommentsRenderer = row => {
    const rowData = row.data || {};

    return (
      <div className={classes.commentsCell}>
        <SvgIcon
          viewBox="-1 2 24 21"
          className={`${classes.commentsIcon} ${rowData.totalUnreadComment ? classes.commentsIconColorRed : classes.commentsIconColorDim
            }`}>
          <ToggleUpdateIcon />
        </SvgIcon>
        <Typography
          variant="body2"
          align="center"
          className={`${rowData.totalUnreadComment ? classes.commentsIconColorRed : classes.commentsIconColorDim
            }`}>
          {rowData.comments ? rowData.comments : "-"}
        </Typography>
      </div>
    );
  };
  const AttachmentsRenderer = row => {
    const rowData = row.data || {};

    return (
      <div className={classes.commentsCell}>
        <AttachmentIcon classes={{ root: classes.attachmentIcon }} />
        {rowData.totalAttachment == 0 ? "-" : rowData.totalAttachment}
      </div>
    );
  };
  const ColorRenderer = row => {
    const rowData = row.data || {};

    return (
      <span
        style={{ background: rowData.colorCode || "transparent" }}
        className={classes.taskColor}></span>
    );
  };
  const isExternalFilterPresent = () => {
    return true;
  };
  const MeetingRenderer = row => {
    const rowData = row.data || {};

    return (
      <div className={classes.countsCnt}>
        <SvgIcon
          viewBox="0 0 15 16.667"
          classes={{ root: classes.meetingIcon }}
          htmlColor={theme.palette.icon.gray400}>
          <MeetingsIcon variant={"outlined"} />
        </SvgIcon>
        <Typography variant="body2" align="center" className={classes.countsText}>
          {rowData.meetings ? rowData.meetings : "-"}
        </Typography>
      </div>
    );
  };
  const IssuesRenderer = row => {
    const rowData = row.data || {};

    return (
      <div className={classes.countsCnt}>
        <SvgIcon
          viewBox="0 0 12.914 16.796"
          classes={{ root: classes.issueIcon }}
          htmlColor={theme.palette.icon.gray400}>
          <IssueIcon variant="outlined" />
        </SvgIcon>
        <Typography variant="body2" align="center" className={classes.countsText}>
          {rowData.issues ? rowData.issues : "-"}
        </Typography>
      </div>
    );
  };
  const RisksRenderer = row => {
    const rowData = row.data || {};

    return (
      <div className={classes.countsCnt}>
        <SvgIcon
          viewBox="0 0 17.355 15"
          classes={{ root: classes.riskIcon }}
          htmlColor={theme.palette.icon.gray400}>
          <RiskIcon variant={"outlined"} />
        </SvgIcon>
        <Typography variant="body2" align="center" className={classes.countsText}>
          {rowData.risks ? rowData.risks : "-"}
        </Typography>
      </div>
    );
  };
  const RecurrenceRenderer = row => {
    const rowData = row.data || {};

    return (
      rowData.repeatTask && (
        <RecurrenceBtn
          theme={theme}
          onClick={e => !rowData.isDeleted && openRepeatTaskDrawer(e, rowData)}
        />
      )
    );
  };
  //function handles drawer open
  const openRepeatTaskDrawer = (event, task) => {
    event.stopPropagation();
    setRepeatDrawer(task.taskId);
  };

  const closeRepeatTaskDrawer = () => {
    setRepeatDrawer(null);
  };

  //Clear selection if archived view is selected
  useEffect(() => {
    if (quickFilters && quickFilters.Archived) {
      grid.grid && grid.grid.deselectAll();
    }
  }, [quickFilters]);
  const handleTaskSelection = tasks => {
    setSelectedTasks(tasks);
  };

  const handleClearSelection = () => {
    setSelectedTasks([]);
    grid.grid && grid.grid.deselectAll();
  };
  //Close task detail
  const closeTaskDetailsPopUp = () => {
    taskDetailDialogState(dispatch, {
      id: "",
      afterCloseCallBack: null,
      type: "",
    });
  };
  //Open task details on tasks row click
  const handleTaskRowClick = row => {
    let rowData = row.data;
    const isRowClicked = row.event.target.closest("*[data-rowClick='cell']");
    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    if (isRowClicked) return;
    if (taskPermission.taskDetail.cando && row.data && row.data.taskId && row.data.uniqueId !== "-") {
      taskDetailDialogState(dispatch, {
        id: row.data.taskId,
        afterCloseCallBack: () => {
          closeTaskDetailsPopUp();
        },
        type: "comment",
      });
    }
  };
  const handleUpdateCustomField = (
    rowData,
    option,
    obj,
    settings,
    succ = () => { },
    fail = () => { }
  ) => {
    const taskCopy = { ...rowData };
    const { fieldId } = obj;
    const newObj = {
      groupType: "task",
      groupId: taskCopy.taskId,
      fieldId,
      fieldData: { data: option },
    };
    const customFields =
      taskCopy.customFieldData &&
      taskCopy.customFieldData.map(c => {
        return c.fieldId === newObj.fieldId ? newObj : c;
      });
    UpdateTaskCustomField(
      newObj,
      res => {
        const taskObj = res.data.entity[0];
        let customFieldsArr = [];
        // Updating Global state
        const isExist = taskCopy.customFieldData
          ? taskCopy.customFieldData.findIndex(c => c.fieldId === taskObj.fieldId) > -1
          : false; /** if new task created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in task object and save it */
        if (isExist) {
          customFieldsArr = taskCopy.customFieldData.map(c => {
            return c.fieldId === taskObj.fieldId ? taskObj : c;
          });
        } else {
          customFieldsArr = taskCopy.customFieldData
            ? [...taskCopy.customFieldData, taskObj]
            : [
              taskObj,
            ]; /** add custom field in task object and save it, if newly created task because its custom field is already null */
        }
        succ(customFieldsArr);
        let newTaskObj = { ...taskCopy, customFieldData: customFieldsArr };
        updateTaskObject(newTaskObj, dispatch);
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(newTaskObj.id);
          rowNode.setData(newTaskObj);
        }
      },
      () => {
        fail();
      },
      dispatch
    );
  };
  const CustomfieldRendere = row => {
    /** custom fields columns rendere */
    const obj = {
      fieldId: row.colDef.fieldId,
      fieldType: row.colDef.fieldType,
      fieldData: { data: null },
    };
    const rowData = row.data || {};
    const data = rowData.customFieldData
      ? rowData.customFieldData.find(cf => cf.fieldId === row.colDef.fieldId) || obj
      : obj;
    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    const canUpdateField =
      taskPermission && !rowData.isDeleted ? taskPermission.taskDetail.isUpdateField.cando : false;
    return (
      <CustomFieldRenderer
        field={data}
        fieldType={row.colDef.fieldType}
        rowData={rowData}
        handleUpdateCustomField={handleUpdateCustomField}
        permission={canUpdateField}
        groupType={"task"}
        placeholder={false}
      />
    );
  };
  const TaskActionDropdownRenderer = row => {
    const rowData = row.data || {};
    let taskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceTaskPer
          : permissionObject[rowData.projectId].task
        : workspaceTaskPer;
    return (
      <TaskActionDropdown
        btnProps={{ style: { width: 37 } }}
        data={rowData}
        handleActivityLog={() => { }}
        handleCloseCallBack={() => { }}
        taskPermission={taskPermission}
        isArchived={rowData.isDeleted}
      />
    );
  };
  const handleCloseDialog = () => {
    grid.grid.redrawRows([mappingDialogState.tempTask]);
    setMappingDialogState({
      tempProject: null,
      openStatusDialog: false,
      newTemplateItem: {},
      oldStatusItem: {},
      tempTask: {},
    });
  };
  const handleSaveTemplate = newStatusItem => {
    let obj = {
      projectId: mappingDialogState.tempProject.length ? mappingDialogState.tempProject[0].id : "",
      status: newStatusItem.statusId,
    };
    updateTaskData(
      { task: mappingDialogState.tempTask, obj },
      dispatch,
      //Success
      task => {
        grid.grid && grid.grid.redrawRows([task]);
        handleCloseDialog();
      },
      err => {
        if (err && err.data) showSnackBar(err.data.message, "error");
        handleCloseDialog();
      }
    );
  };
  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type, options) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
        ...options
      }
    );
  };
  const taskTitleColumn = (taskColumns && taskColumns.find(c => c.columnKey == "taskTitle")) || {};
  let headProps = headerProps;
  const nonSectionsFieldskeys = nonSectionFields
    .filter(
      item =>
        item.fieldType == "textarea" ||
        item.fieldType == "formula" ||
        item.fieldType == "filesAndMedia" ||
        item.fieldType == "location" ||
        (item.fieldType == "dropdown" && item.settings.multiSelect) ||
        (item.fieldType == "people" && item.settings.multiplePeople)
    )
    .map(nsf => nsf.columnKey);
  const sectionsFieldskeys = sections.reduce((res, cv) => {
    let keys = cv.fields
      .filter(
        item =>
          item.fieldType == "textarea" ||
          item.fieldType == "formula" ||
          item.fieldType == "filesAndMedia" ||
          item.fieldType == "location" ||
          (item.fieldType == "dropdown" && item.settings.multiSelect) ||
          (item.fieldType == "people" && item.settings.multiplePeople)
      )
      .map(cv => cv.columnKey);
    res = [...res, ...keys];
    return res;
  }, []);
  headProps.columnGroupingDisabled = [
    ...headProps.columnGroupingDisabled,
    ...nonSectionsFieldskeys,
    ...sectionsFieldskeys,
  ];

  const handleChangeGrouping = value => {
    let g = grid.grid;
    let sectionGroup = localStorage.getItem("sectiongrouping");
    if (sectionGroup == "true") localStorage.setItem("sectiongrouping", value);
    else {
      localStorage.setItem("sectiongrouping", value);
    }
    setSectionGrouping(value);
  };
  const RenderColumnsWithoutSectionGrouping = () => {
    const systemColumns = taskColumns
      .map((c, i) => {
        const defaultColDef = taskColumnDefs(classes)[c.columnKey];

        const {
          hide,
          pinned,
          rowGroup,
          rowGroupIndex,
          sort,
          sortIndex,
          width,
          wrapText,
          autoHeight,
          position,
        } = c;
        return (
          c.columnKey !== "taskTitle" &&
          c.isSystem && (
            <AgGridColumn
              key={c.id}
              headerName={c.columnName}
              field={c.columnKey}
              hide={hide}
              suppressKeyboardEvent={true}
              pinned={pinned}
              align="left"
              rowGroup={rowGroup}
              sort={sort}
              sortIndex={sortIndex == -1 ? null : sortIndex}
              wrapText={wrapText}
              rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
              autoHeight={autoHeight}
              width={width || (defaultColDef && defaultColDef.minWidth)}
              cellStyle={{ justifyContent: "center" }}
              position={position}
              {...defaultColDef}
            />
          )
        );
      })
      .filter(c => c !== false);
    const nonSectionColumns = nonSectionFields.map((c, i) => {
      const defaultColDef = taskColumnDefs(classes)[c.fieldType];
      const { hide, pinned, rowGroup, sort, sortIndex, width, wrapText, autoHeight, position } = c;
      return (
        <AgGridColumn
          key={c.id}
          headerName={c.columnName}
          field={c.columnKey}
          fieldType={c.fieldType}
          hide={hide}
          pinned={pinned}
          align="left"
          rowGroup={rowGroup}
          suppressKeyboardEvent={true}
          sort={sort}
          sortIndex={sortIndex == -1 ? null : sortIndex}
          wrapText={wrapText}
          autoHeight={autoHeight}
          fieldId={c.fieldId}
          position={position}
          width={width || (defaultColDef && defaultColDef.minWidth)}
          cellStyle={{
            justifyContent:
              c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                ? "flex-start"
                : "center",
          }}
          {...defaultColDef}
        />
      );
    });
    const sectionColumns = sections
      .map(section => {
        return (
          section.fields.length > 0 &&
          section.fields.map((c, i) => {
            const defaultColDef = taskColumnDefs(classes)[c.fieldType];
            const {
              hide,
              pinned,
              rowGroup,
              sort,
              sortIndex,
              width,
              wrapText,
              autoHeight,
              position,
            } = c;
            return (
              <AgGridColumn
                key={c.id}
                headerName={c.columnName}
                field={c.columnKey}
                hide={hide}
                fieldType={c.fieldType}
                pinned={pinned}
                align="left"
                suppressKeyboardEvent={true}
                position={position}
                rowGroup={rowGroup}
                sort={sort}
                sortIndex={sortIndex == -1 ? null : sortIndex}
                wrapText={wrapText}
                autoHeight={autoHeight}
                width={width || (defaultColDef && defaultColDef.minWidth)}
                fieldId={c.fieldId}
                cellStyle={{
                  justifyContent:
                    c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                      ? "flex-start"
                      : "center",
                }}
                {...defaultColDef}
              />
            );
          })
        );
      })
      .filter(c => c !== false);
    const spreadSectionCols = sectionColumns.reduce((r, cv) => {
      r = [...r, ...cv];
      return r;
    }, []);
    const allColumns = [...systemColumns, ...nonSectionColumns, ...spreadSectionCols];
    const sortedColumns = allColumns.sort((pv, cv) => {
      return pv.props.position - cv.props.position;
    });
    return sortedColumns;
  };
  const taskLabelSingle = companyInfo?.task?.sName;
  const taskLabelMulti = companyInfo?.task?.pName;
  return (
    <>
      {markChecklistActionConf && (
        <ActionConfirmation
          open={markChecklistActionConf}
          closeAction={markAllConfirmDialogClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            otherStatus && doneStatus ? <FormattedMessage
              id="task.detail-dialog.to-do-list.mark-all.action"
              defaultMessage="Yes, Mark All Complete"
            /> : otherStatus && !doneStatus ? <>Yes</> :
              <FormattedMessage
                id="task.detail-dialog.to-do-list.mark-all.action"
                defaultMessage="Yes, Mark All Complete"
              />
          }
          alignment="center"
          headingText={
            otherStatus && doneStatus ?
              <FormattedMessage
                id="task.detail-dialog.to-do-list.mark-all.title"
                defaultMessage="Mark All Complete"
              /> : otherStatus && !doneStatus ?
                <>Reminder - Task Effort</> :
                <FormattedMessage
                  id="task.detail-dialog.to-do-list.mark-all.title"
                  defaultMessage="Mark All Complete"
                />
          }
          iconType="markAll"
          msgText={
            otherStatus && doneStatus ? <>
              Are you sure you want to mark all to-do list items as completed?
              <br />
              Make sure you have added the time effort before updating task status
            </> : otherStatus && !doneStatus ?
              <> Make sure you have added the time effort before updating task status</> :
              <FormattedMessage
                id="task.detail-dialog.to-do-list.mark-all.message"
                defaultMessage="Are you sure you want to mark all to-do list items as completed?"
              />
          }
          successAction={() => handleUpdateStatus()}
          btnQuery={""}
        />
      )}
      {mappingDialogState.openStatusDialog && (
        <TaskStatusChangeDialog
          open={mappingDialogState.openStatusDialog}
          handleClose={handleCloseDialog}
          oldStatus={mappingDialogState.oldStatusItem}
          newTemplateItem={mappingDialogState.newTemplateItem}
          handleSaveAsTemplate={handleSaveTemplate}
        />
      )}
      {selectedTasks.length >= 1 ? (
        <Taskcmp selectedTasks={selectedTasks} clearSelection={handleClearSelection} />
      ) : null}
      {repeatDrawer && (
        <SideDrawer
          closeAction={closeRepeatTaskDrawer}
          open={!isEmpty(repeatDrawer)}
          repeatTaskId={repeatDrawer}
          allTasks={tasks}
        />
      )}
      {tasks.length == 0 ? (
        <div className={classes.emptyContainer} style={style}>
          <EmptyState
            screenType="task"
            heading={
              <FormattedMessage
                id="common.create-first.task.label"
                defaultMessage={`Create your first ${taskLabelSingle ? taskLabelSingle : "task"}`}
              />
            }
            message={
              <FormattedMessage
                id="common.create-first.task.messageb"
                defaultMessage={`You do not have any  ${taskLabelMulti ? taskLabelMulti : "tasks"
                  } yet. Press "Alt + T" or click on button below.`}
              />
            }
            button={workspaceTaskPer.createTask.cando}
          />
        </div>
      ) : (
        <div className={classes.taskListViewCnt} style={style}>
          <CustomTable
            columns={state.taskColumns}
            defaultColDef={{ lockPinned: true }}
            isExternalFilterPresent={isExternalFilterPresent}
            doesExternalFilterPass={doesFilterPass}
            onSelectionChanged={handleTaskSelection}
            idKey={"taskId"}
            frameworkComponents={{
              taskActionCellRenderer: TaskActionDropdownRenderer,
              statusDropdown: StatusDropdownCmp,
              taskTitleCmp: TaskTitleCellRenderer,
              priorityCmp: PriorityDropdown,
              actualStartDate: ActualStartDateRenderer,
              createdByRenderer: CreatedByRenderer,
              updatedByRenderer: UpdatedByRenderer,
              plannedStartDate: PlannedStartDateRenderer,
              actualDueDate: ActualDueDateRenderer,
              plannedDueDate: PlannedDueDateRenderer,
              taskProject: ProjectDropdownRenderer,
              assignee: AssigneeDropdownRenderer,
              groupRowInnerRenderer: GroupRowInnerRenderer,
              createdDateRenderer: DateRenderer,
              updatedDateCmp: UpdatedDateRenderer,
              progressRenderer: ProgressRenderer,
              timeLogRenderer: TimeLoggedRenderer,
              // recurrence: RecurrenceRenderer,
              comments: CommentsRenderer,
              totalAttachment: AttachmentsRenderer,
              meetings: MeetingRenderer,
              issues: IssuesRenderer,
              risks: RisksRenderer,
              color: ColorRenderer,
              textfield: CustomfieldRendere,
              // textarea: CustomfieldRendere,
              location: CustomfieldRendere,
              country: CustomfieldRendere,
              number: CustomfieldRendere,
              money: CustomfieldRendere,
              email: CustomfieldRendere,
              websiteurl: CustomfieldRendere,
              date: CustomfieldRendere,
              phone: CustomfieldRendere,
              rating: CustomfieldRendere,
              formula: CustomfieldRendere,
              people: CustomfieldRendere,
              dropdown: CustomfieldRendere,
              filesAndMedia: CustomfieldRendere,
            }}
            data={tasks}
            selectedTasks={selectedTasks}
            type="task"
            onRowGroupChange={(obj, key) => updateTask(obj, { [key]: obj[key] })}
            createableProps={{
              placeholder: `Enter title for new ${taskLabelSingle ? taskLabelSingle : "task"}`,
              id: "quickAddTask",
              btnText: `Add new ${taskLabelSingle ? taskLabelSingle : "task"}`,
              addAction: handleAddTask,
              addHelpTask: `Press enter to add new ${taskLabelSingle ? taskLabelSingle : "task"}`,
              emptyErrorMessage: 'Please Enter Task title',
              disabled: !workspaceTaskPer.createTask.cando,
            }}
            handleReadyFunction={setGloabalTimer}
            headerProps={headProps}
            gridProps={{
              getContextMenuItems: getContextMenuItems,
              groupDisplayType: "groupRows",
              onRowClicked: handleTaskRowClick,
              reactUi: true,
              // groupIncludeFooter: true,
              // groupIncludeTotalFooter: true,
              maintainColumnOrder: true,
              // autoGroupColumnDef: { minWidth: 300 },
            }}>
            {/*<AgGridColumn headerClass={classes.taskTitleField} >*/}
            <AgGridColumn
              headerName=""
              field="colorCode"
              cellRenderer="color"
              filter={false}
              cellClass={classes.taskColorCell}
              maxWidth={6}
              resizeable={false}
              pinned="left"
              lockPosition={true}
              suppressMovable={true}
            />
            {/*<AgGridColumn*/}
            {/*  headerName=""*/}
            {/*  field="drag"*/}
            {/*  suppressMovable={true}*/}
            {/*  filter={false}*/}
            {/*  */}
            {/*  cellClass={classes.taskDragCell}*/}
            {/*  maxWidth={20}*/}

            {/*  pinned={"left"}*/}
            {/*  // rowDragText={(params) => {*/}
            {/*  //   return params.rowNode.data.taskTitle*/}
            {/*  // }}*/}
            {/*/>*/}

            <AgGridColumn
              headerName={taskTitleColumn.columnName}
              field={taskTitleColumn.columnKey}
              resizable={true}
              suppressMovable={true}
              checkboxSelection={teamCanView("bulkActionAccess")}
              sortable={true}
              filter={true}
              lockPosition={true}
              wrapText={taskTitleColumn.wrapText}
              cellRenderer={"taskTitleCmp"}
              cellClass={`${classes.taskTitleCell} ag-textAlignLeft`}
              pinnedRowCellRenderer="customPinnedRowRenderer"
              pinned={"left"}
              comparator={sortAlphabetically}
              autoHeight={taskTitleColumn.autoHeight}
              width={taskTitleColumn.width || 300}
              minWidth={200}
              align="left"
              sort={taskTitleColumn.sort}
              sortIndex={taskTitleColumn.sortIndex == -1 ? null : taskTitleColumn.sortIndex}
              cellStyle={{ padding: 0 }}
              rowDrag={true}
            />
            <AgGridColumn
              headerName=""
              field="recurrence"
              suppressMovable={true}
              cellRenderer="recurrence"
              cellClass={classes.taskRecurrenceCell}
              filter={false}
              minWidth={22}
              maxWidth={22}
              pinned={"left"}
            />
            {/*</AgGridColumn>*/}
            {!sectionGroup || sectionGroup == "false"
              ? RenderColumnsWithoutSectionGrouping()
              : null}

            {sectionGroup == "true" && (
              <AgGridColumn
                visible={false}
                headerName="System Fields"
                headerClass={classes.systemFieldGroup}>
                {taskColumns.map((c, i) => {
                  const defaultColDef = taskColumnDefs(classes)[c.columnKey];
                  const {
                    hide,
                    pinned,
                    rowGroup,
                    rowGroupIndex,
                    sort,
                    sortIndex,
                    width,
                    wrapText,
                    autoHeight,
                  } = c;
                  return (
                    c.columnKey !== "taskTitle" &&
                    c.isSystem && (
                      <AgGridColumn
                        key={c.id}
                        headerName={c.columnName}
                        field={c.columnKey}
                        hide={hide}
                        pinned={pinned}
                        align="left"
                        rowGroup={rowGroup}
                        sort={sort}
                        sortIndex={sortIndex == -1 ? null : sortIndex}
                        wrapText={wrapText}
                        rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
                        autoHeight={autoHeight}
                        width={width || (defaultColDef && defaultColDef.minWidth)}
                        cellStyle={{ justifyContent: "center" }}
                        {...defaultColDef}
                      />
                    )
                  );
                })}
              </AgGridColumn>
            )}
            {sectionGroup == "true" &&
              sections.map(section => {
                return (
                  section.fields.length > 0 && (
                    <AgGridColumn
                      headerName={section.columnName}
                      resizeable={false}
                      headerGroupComponent="customHeaderGroupComponent"
                      color={section.color}
                      headerClass={classes.sectionFieldsGroup}>
                      {section.fields.map((c, i) => {
                        const defaultColDef = taskColumnDefs(classes)[c.fieldType];
                        const {
                          hide,
                          pinned,
                          rowGroup,
                          sort,
                          sortIndex,
                          width,
                          wrapText,
                          autoHeight,
                        } = c;
                        return (
                          <AgGridColumn
                            key={c.id}
                            headerName={c.columnName}
                            field={c.columnKey}
                            suppressKeyboardEvent={true}
                            hide={hide}
                            fieldType={c.fieldType}
                            pinned={pinned}
                            align="left"
                            rowGroup={rowGroup}
                            sort={sort}
                            sortIndex={sortIndex == -1 ? null : sortIndex}
                            wrapText={wrapText}
                            autoHeight={autoHeight}
                            width={width || (defaultColDef && defaultColDef.minWidth)}
                            fieldId={c.fieldId}
                            cellStyle={{
                              justifyContent:
                                c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                                  ? "flex-start"
                                  : "center",
                            }}
                            {...defaultColDef}
                          />
                        );
                      })}
                    </AgGridColumn>
                  )
                );
              })}
            {sectionGroup == "true" && nonSectionFields.length ? (
              <AgGridColumn resizeable={false} headerClass={classes.taskTitleField}>
                {nonSectionFields.map((c, i) => {
                  const defaultColDef = taskColumnDefs(classes)[c.fieldType];
                  const {
                    hide,
                    pinned,
                    rowGroup,
                    sort,
                    sortIndex,
                    width,
                    wrapText,
                    autoHeight,
                  } = c;
                  return (
                    <AgGridColumn
                      key={c.id}
                      headerName={c.columnName}
                      field={c.columnKey}
                      fieldType={c.fieldType}
                      hide={hide}
                      suppressKeyboardEvent={true}
                      pinned={pinned}
                      align="left"
                      rowGroup={rowGroup}
                      sort={sort}
                      sortIndex={sortIndex == -1 ? null : sortIndex}
                      wrapText={wrapText}
                      autoHeight={autoHeight}
                      fieldId={c.fieldId}
                      width={width || (defaultColDef && defaultColDef.minWidth)}
                      cellStyle={{
                        justifyContent:
                          c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                            ? "flex-start"
                            : "center",
                      }}
                      {...defaultColDef}
                    />
                  );
                })}
              </AgGridColumn>
            ) : null}

            <AgGridColumn
              headerName={""}
              field={"columnDropdown"}
              resizable={false}
              sortable={false}
              lockPinned={true}
              filter={false}
              width={40}
              headerClass={"columnSelectHeader"}
              pinned={"right"}
              suppressMovable={true}
              cellRenderer={"taskActionCellRenderer"}
              cellStyle={{ padding: 0, textAlign: "center" }}
            />
          </CustomTable>
          <TaskFilter
            sectionGrouping={sectionGrouping}
            handleChangeGrouping={handleChangeGrouping}
          />
        </div>
      )}

      {/*<Footer*/}
      {/*  total={tasks && tasks.length}*/}
      {/*  display={true}*/}
      {/*  type="Task"*/}
      {/*  // quote={this.props.quote}*/}
      {/*/>*/}
    </>
  );
}, areEqual);

function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}

const GroupRowInnerRenderer = props => {
  return <GroupByComponents data={props} feature="task" />;
};

export default compose(
  injectIntl,
  withSnackbar,
  withStyles(taskListStyles, { withTheme: true })
)(TaskList);
