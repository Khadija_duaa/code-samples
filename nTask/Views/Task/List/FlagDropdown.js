import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { BulkUpdateTask } from "../../../redux/actions/tasks";
import { withStyles, withTheme } from "@material-ui/core/styles";
import menuStyles from "../../../assets/jss/components/menu";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import DoneIcon from "@material-ui/icons/Done";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Checkbox from "@material-ui/core/Checkbox";
import combineStyles from "../../../utils/mergeStyles";
import itemStyles from "../Grid/styles";
import Button from "@material-ui/core/Button";
import listStyles from "./styles";
import helper from "../../../helper";
import FlagIcon from "../../../components/Icons/FlagIcon";
import { FormattedMessage, injectIntl } from "react-intl";

import SvgIcon from "@material-ui/core/SvgIcon";
class FlagDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      name: [],
      selectedStatus: false,
      checked: [],
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleClose(event) {
    this.setState({ open: false });
  }
  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleToggle = (value) => () => {
    let priority = ["Critical", "High", "Medium", "Low"];
    if (this.state.open) {
      this.handleClose();
      this.setState(
        {
          checked: [value],
        },
        () => {
          const type = helper.RETURN_BULKACTIONTYPES("Priority");
          let data = {
            type,
            taskIds: this.props.selectedIdsList,
            priority: priority.indexOf(value) + 1,
          };

          this.props.BulkUpdateTask(data, (response) => {
            this.handleClose();
            this.props.isBulkUpdated();
          });
        }
      );
    }
  };

  render() {
    const { classes, theme, intl } = this.props;
    const { open, placement } = this.state;
    const statusColor = theme.palette.taskPriority;
    const ddData = [
      { label: intl.formatMessage({ id: "task.common.priority.dropdown.critical", defaultMessage: "Critical" }), name: "Critical", color: statusColor.Critical },
      { label: intl.formatMessage({ id: "task.common.priority.dropdown.high", defaultMessage: "High" }), name: "High", color: statusColor.High },
      { label: intl.formatMessage({ id: "task.common.priority.dropdown.medium", defaultMessage: "Medium" }), name: "Medium", color: statusColor.Medium },
      { label: intl.formatMessage({ id: "task.common.priority.dropdown.low", defaultMessage: "Low" }), name: "Low", color: statusColor.Low }
    ];

    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <Button
            variant="outlined"
            style={{ borderRadius: 0, borderLeft: "none" }}
            classes={{ outlined: classes.BulkActionBtn }}
            onClick={(event) => {
              this.handleClick(event, "bottom-start");
            }}
            buttonRef={(node) => {
              this.anchorEl = node;
            }}
          >
            <FormattedMessage
              id="common.bulk-action.priority"
              defaultMessage="Select Priority"
            />
          </Button>
          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            anchorRef={this.anchorEl}
            list={
              <List>
                <ListItem classes={{ root: classes.menuHeadingItem }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="common.bulk-action.priority"
                        defaultMessage="Select Priority"
                      />
                    }
                    classes={{ primary: classes.menuHeadingItemText }}
                  />
                </ListItem>
                {ddData.map((value) => (
                  <ListItem
                    key={value.name}
                    button
                    disableRipple={true}
                    onClick={this.handleToggle(value.name)}
                    className={`${classes.menuItem} ${this.state.checked.indexOf(value.name) !== -1
                        ? classes.selectedValue
                        : ""
                      }`}
                  >
                    <SvgIcon
                      viewBox="0 0 24 32.75"
                      htmlColor={value.color}
                      classes={{ root: classes.priorityIcon }}
                    >
                      <FlagIcon />
                    </SvgIcon>
                    <ListItemText
                      primary={value.label ? value.label : value.name}
                      classes={{
                        primary: classes.statusItemText,
                      }}
                    />
                  </ListItem>
                ))}
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(combineStyles(itemStyles, menuStyles, listStyles), {
    withTheme: true,
  }),
  connect(null, { BulkUpdateTask })
)(FlagDropdown);
