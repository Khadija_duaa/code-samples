import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import find from "lodash/find";
import helper from "../../../helper";
import Starred from "./Starred";
import { withStyles } from "@material-ui/core/styles";
import ProjectDropDown from "./ProjectDropDown";
import Grid from "@material-ui/core/Grid";
import itemStyles from "./styles";
import Typography from "@material-ui/core/Typography";
// import StatusDropdown from "./StatusDropdown";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import FlagDropdown from "./FlagDropdown";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import LinearProgress from "@material-ui/core/LinearProgress";
import SvgIcon from "@material-ui/core/SvgIcon";
import CommentIcon from "@material-ui/icons/Comment";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import IssuesIcon from "../../../components/Icons/IssueIcon";
import RiskIcon from "../../../components/Icons/RiskIcon";
import AttachmentIcon from "@material-ui/icons/Attachment";
import ActionDropdown from "./ActionDropdown";
import PublicLink from "../../../components/Dialog/PublicLink";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import SideDrawer from "../Drawer/Drawer";
import IconButton from "../../../components/Buttons/IconButton";
import cloneDeep from "lodash/cloneDeep";
import { getTemplate } from "../../../utils/getTemplate";

import {
  UpdateTask,
  FetchTasksInfo,
  CopyTask,
  DeleteTask,
  ArchiveTask,
  UnArchiveTask,
  CheckAllTodoList,
  updateTaskData,
} from "../../../redux/actions/tasks";
import { UpdateCalenderTask, CopyCalenderTask } from "../../../redux/actions/calenderTasks";
import { UpdateTaskColorFromGridItem } from "../../../redux/actions/workspace";
import TaskDetails from "../TaskDetails/TaskDetails";
import { getTasksPermissions, getTasksPermissionsWithoutArchieve } from "../permissions";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import { FormattedMessage, injectIntl } from "react-intl";
import CircularIcon from "@material-ui/icons/Brightness1";
import TaskStatusChangeDialog from '../../../components/Templates/TaskStatusChangeDialog';
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { taskDetailDialogState } from "../../../redux/actions/allDialogs";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import isUndefined from "lodash/isUndefined";
import { CanAccessFeature } from "../../../components/AccessFeature/AccessFeature.cmp";
class TaskGridItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      starChecked: false,
      taskProgress: 30,
      selectedColor: "#fff",
      completeDetails: [],
      userId: "",
      taskId: "",
      progress: 0,
      isColored: false,
      isProgress: false,
      showTaskDetails: false,
      currentTask: null,
      type: "comment",
      publicLinkOpen: false,
      repeatTaskDrawer: false,
      openStatusDialog: false,
      newTemplateItem: null,
      oldStatusItem: null,
      markChecklistActionConf: false,
      markChecklistActionBtnQuery: "",
      selectedStatus: {},
      selectedTaskObj: {},
    };
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleCalculateProgress = this.handleCalculateProgress.bind(this);
    this.closeTaskDetailsPopUp = this.closeTaskDetailsPopUp.bind(this);
  }

  componentDidMount() {
    this.props.resetCount();
    let userId = this.props.profileState.data.userId;
    this.setState({ completeDetails: this.props.filteredTasks, userId });
  }

  componentDidUpdate(prevProps) {
    const { currentTask } = this.state;
    const { sortObj, filteredTasks } = this.props;
    const taskDetailsTask = filteredTasks.find(t => {
      // Checking if the task details task exist in the list or not
      return currentTask && t.taskId == currentTask.taskId;
    });
    if (currentTask && !taskDetailsTask) {
      // if task does not exist in the filtered list remove the task details from state
      //This scenario can be produced if you apply filter on a task with assignee from sidebar and open task details and change assignee and than toggle filter from sidebar again
      this.setState({ showTaskDetails: false, currentTask: null });
    }
    if (JSON.stringify(prevProps.filteredTasks) !== JSON.stringify(this.props.filteredTasks)) {
      let detailedTasks = this.props.filteredTasks;
      let task = currentTask
        ? detailedTasks.find(task => task.taskId === currentTask.taskId)
        : null;
      this.setState({ currentTask: task, completeDetails: detailedTasks });
    }
    if (JSON.stringify(prevProps.sortObj) !== JSON.stringify(this.props.sortObj)) {
      this.setState({ completeDetails: this.props.filteredTasks });
    }
  }

  handleCalculateProgress(data, isProgressUpdated) {
    if (isProgressUpdated) {
      this.setState({ progress: 0, taskId: "", isProgress: false });
    } else {
      let result = helper.RETURN_PROGRESS(data.checkList, data.status);
      this.setState({
        progress: result,
        taskId: data.taskId,
        isProgress: true,
      });
    }
  }
  handleColorChange = (data, color) => {
    this.setState({ selectedColor: color, taskId: data.taskId, isColored: true }, function () {
      let obj = { colorCode: color };
      this.props.updateTaskData({ task: data, obj })
    });
  };
  showTaskDetailsPopUp = (event, data, type) => {
    const { taskPer, permissionObject } = this.props;
    event.stopPropagation();
    const taskPermission =
      data && data.projectId
        ? isUndefined(permissionObject[data.projectId])
          ? taskPer
          : permissionObject[data.projectId].task
        : taskPer;
    if (taskPermission.taskDetail.cando) {
      this.setState({ showTaskDetails: true, currentTask: data, type }, () => {
        this.setState({
          showTaskDetails: false, currentTask: null,
        })
      });
    }
  };
  closeTaskDetailsPopUp() {
    this.setState({ showTaskDetails: false, type: "comment" });
  }
  openPublicLinkDialog = (event, id, taskTitle) => {
    event.stopPropagation();
    this.setState({
      publicLinkOpen: true,
      publicLinkId: id,
      publicLinkTitle: taskTitle,
    });
  };
  closePublicLinkDialog = () => {
    this.setState({ publicLinkOpen: false });
  };
  //Function that updates assignee of respective document (e.g. task/risk/issue/meeting)
  updateAssignee = (assignedTo, task) => {
    let assignedToArr = assignedTo.map(ass => {
      return ass.userId;
    });
    let obj = { assigneeList: assignedToArr };
    this.props.updateTaskData({ task: task, obj });
  };

  //Function that updates assignee of respective document (e.g. task/risk/issue/meeting)
  // updateProject = (selectedProject, task) => {
  //   let newTaskObj = {
  //     ...task,
  //     projectId: selectedProject.length ? selectedProject[0].id : "",
  //   };
  //   this.props.UpdateTask(newTaskObj, () => {});
  // };
  showMappingDialog = (projectId) => {
    const { workspaceStatus } = this.props;
    let attachedProject = this.props.projects.find(p => p.projectId == projectId);
    return attachedProject && attachedProject.projectTemplateId != workspaceStatus.templateId
  }
  getDefaultTemplateProject = (item) => {
    const { workspaceStatus } = this.props;
    const projectId = item.length ? item[0].id : null;
    if (projectId) {
      let attachedProject = this.props.projects.find(p => p.projectId == projectId);
      if (attachedProject && attachedProject.projectTemplate) {
        return attachedProject.projectTemplate;
      } else {
        return workspaceStatus;
      }
    } else {
      return workspaceStatus;
    }
  }
  handleSelectChange = (newValue, task) => {
    const projectId = newValue.length ? newValue[0].id : task.projectId;
    if (!this.showMappingDialog(projectId)) {
      let obj = { projectId: newValue.length ? newValue[0].id : "" };
      this.props.updateTaskData({ task: task, obj });
    } else {
      let taskTemplate = getTemplate(task);
      const taskStatusData = this.statusData(taskTemplate ? taskTemplate.statusList : false);

      let taskStatus = taskStatusData.find(el => el.value == task.status)
      let template = this.getDefaultTemplateProject(newValue);
      this.setState(
        {
          tempProject: newValue || null,
          openStatusDialog: true,
          newTemplateItem: template,
          oldStatusItem: taskStatus,
          tempTask: task
        }
      );
    }
  }
  handleCloseDialog = () => {
    this.setState({ openStatusDialog: false, newTemplateItem: null, oldStatusItem: null });
  }
  handleSaveTemplate = (newStatusItem) => {
    const { tempProject, tempTask } = this.state;
    let obj = {
      projectId: tempProject.length ? tempProject[0].id : "",
      status: newStatusItem.statusId,
    };

    this.setState(
      {
        openStatusDialog: false,
        newTemplateItem: null,
        oldStatusItem: null
      },
      () => {
        this.props.updateTaskData({ task: tempTask, obj }, null, succ => {
          this.setState({ openStatusDialog: false, newTemplateItem: null, oldStatusItem: null });
        });
      }
    );
  }
  // Generate list of all projects for dropdown understandable form
  generateProjectDropdownData = task => {
    const { projects } = this.props;
    let filteredProjects = projects.filter(p => p.projectId !== task.projectId);
    let projectsArr = filteredProjects.map(project => {
      return { label: project.projectName, id: project.projectId, obj: task };
    });
    return projectsArr;
  };
  //Generate list of selected options for project dropdown understandable form
  getSelectedProject = task => {
    const { projects } = this.props;
    let selectedProject = projects.find(project => {
      return project.projectId == task.projectId;
    });

    return selectedProject
      ? [
        {
          label: selectedProject.projectName,
          id: selectedProject.projectId,
          obj: task,
        },
      ]
      : [];
  };
  openRepeatTaskDrawer = (event, task) => {
    event.stopPropagation();
    this.setState({ repeatTaskDrawer: true, repeatTaskId: task.taskId });
  };
  closeRepeatTaskDrawer = () => {
    this.setState({ repeatTaskDrawer: false, repeatTaskDrawer: "" });
  };
  statusData = statusArr => {
    return statusArr.map(item => {
      return {
        label: item.statusTitle,
        value: item.statusId,
        icon: <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "20px" }} />,
        statusColor: item.statusColor,
        obj: item,
        statusTitle: item.statusTitle
      };
    });
  };
  markAllConfirmDialogClose = () => {
    this.setState({ markChecklistActionConf: false, selectedTaskObj: {}, selectedStatus: {} });
  };
  handleStatusChange = (status, task) => {
    const { loggedInTeam, workspace } = this.props.profileState.data;
    const currentWorkspace = workspace.find(t => t.teamId === loggedInTeam);

    if (status.obj.isDoneState) {
      if (currentWorkspace.config.isUserTasksEffortMandatory)
        this.props.showSnackBar("Make sure you have added effort in the task", "info", {
          anchorOrigin: {
            vertical: "bottom",
            horizontal: "right",
          },
        });

      this.setState(
        { selectedTaskObj: task, selectedStatus: status, markChecklistActionConf: true },
        () => { }
      );
      return;
    }
    this.handleUpdateStatus(status, task);
  };
  handleUpdateStatus = (status, task) => {
    const updatedTask = cloneDeep(task);
    let obj = {
      status: status.value,
      statusTitle: status.label
    };
    if (status.obj.isDoneState) {
      this.setState({ markChecklistActionBtnQuery: "progress" });
      const checkData = {
        taskId: updatedTask.id,
        checkAll: true,
      };
      this.props.CheckAllTodoList(checkData, () => { }, () => { }, this.props.profileState.data);
    }
    this.props.updateTaskData({ task: updatedTask, obj }, null, succ => {
      this.setState({
        selectedTaskObj: {},
        markChecklistActionBtnQuery: "",
        markChecklistActionConf: false,
        selectedStatus: {},
      });
    }, err => {
      this.props.showSnackBar(response.data.message, "error");
      this.setState({ markChecklistActionBtnQuery: "" });
    });
  };
  render() {
    const { classes, theme, isArchivedSelected, projects, taskPer, intl, permissionObject, members } = this.props;
    const {
      starChecked,
      selectedColor,
      taskId,
      isProgress,
      isColored,
      type,
      publicLinkOpen,
      publicLinkId,
      repeatTaskDrawer,
      publicLinkTitle,
      repeatTaskId,
      completeDetails,
      openStatusDialog,
      newTemplateItem,
      oldStatusItem,
      markChecklistActionBtnQuery,
      markChecklistActionConf,
      selectedTaskObj,
      selectedStatus,
    } = this.state;
    const profileUserId = this.props.profileState.data.userId;
    let selectedColorValue = "#fff",
      progress = 0;
    const gridData = completeDetails.slice(0, this.props.showRecords).map((x, i) => {
      if (taskId === x.taskId) {
        if (isColored) selectedColorValue = selectedColor;
        else selectedColorValue = x.colorCode;
        if (isProgress) progress = this.state.progress;
        else progress = x.progress;
      } else {
        selectedColorValue = x.colorCode;
        progress = x.progress;
      }
      const permission = getTasksPermissions(x, this.props);
      const permissionAction = getTasksPermissionsWithoutArchieve(x, this.props);
      const overdue = helper.RETURN_OVER_DUE_DAYS_WITH_PROGRESS(x.actualDueDateString, progress);
      const RemainingDaysStatus = helper.RETURN_REMAINING_DAYS_STATUS_NEW(
        x.actualDueDateString,
        progress,
        intl
      );
      //some times task effort comes in string and some times empty array so to check that below line of code is added
      const taskEffort = x.userTaskEffort ? x.userTaskEffort.length : x.userTaskEffort;
      let template = getTemplate(x);
      // const taskStatusData = this.statusData(x.taskStatus.statusList);
      const taskStatusData = this.statusData(template.statusList);
      let selectedStatus = taskStatusData.find(item => item.value == x.status) || {};
      const taskPermission =
        x && x.projectId
          ? isUndefined(permissionObject[x.projectId])
            ? taskPer
            : permissionObject[x.projectId].task
          : taskPer;
      const membersObjArr = members && members.filter(m => x.assigneeList && x.assigneeList.includes(m.userId));
      return (
        <Grid item xs={12} sm={6} md={6} lg={4} xl={3} key={x.taskId}>
          {repeatTaskId && repeatTaskDrawer && (
            <SideDrawer
              closeAction={this.closeRepeatTaskDrawer}
              open={repeatTaskDrawer}
              repeatTaskId={repeatTaskId}
              allTasks={completeDetails}
            />
          )}
          <div
            style={{
              borderLeft: `5px solid ${selectedColorValue !== "" ? selectedColorValue : "#fff"}`,
            }}
            className={classes.taskItemCnt}
            onClick={e => this.showTaskDetailsPopUp(e, x)}>
            <Typography classes={{ h6: classes.taskGridTaskList }} variant="h6">
              {x.uniqueId}
            </Typography>

            <div className={classes.gridItemHeadingCnt}>
              <Typography classes={{ h5: classes.taskGridTaskList }} variant="h5">
                {x.taskTitle}
                {!x.isDeleted && x.repeatTask && (
                  <IconButton
                    btnType="transparent"
                    onClick={event => this.openRepeatTaskDrawer(event, x)}
                    style={{ padding: 5 }}>
                    <SvgIcon
                      viewBox="0 0 14 12.438"
                      htmlColor={this.props.theme.palette.primary.light}
                      className={classes.recurrenceIcon}>
                      <RecurrenceIcon />
                    </SvgIcon>
                  </IconButton>
                )}
              </Typography>

              <div className="flex_center_start_row">
                <Starred userId={profileUserId} currentTask={x} />
                <ActionDropdown
                  selectedColor={selectedColor}
                  task={x}
                  filterUnArchiveTask={this.props.filterUnArchiveTask}
                  CopyTask={this.props.CopyTask}
                  DeleteTask={this.props.DeleteTask}
                  ArchiveTask={this.props.ArchiveTask}
                  openPublicLinkDialog={this.openPublicLinkDialog}
                  CopyCalenderTask={this.props.CopyCalenderTask}
                  UnArchiveTask={this.props.UnArchiveTask}
                  colorChange={this.handleColorChange}
                  permission={permissionAction}
                  isArchivedSelected={isArchivedSelected}
                  taskPer={taskPermission}
                />
              </div>
            </div>
            {teamCanView(
              "projectAccess"
            ) /** passing projectAccess param to function and getting permission */ && (
                <div className={classes.projectDropdown}>
                  <CanAccessFeature group='task' feature='project'>
                    <SearchDropdown
                      initSelectedOption={x.projectId ? this.getSelectedProject(x) : []}
                      buttonProps={{
                        disabled: this.props.isArchivedSelected,
                      }}
                      disableDropdown={
                        taskEffort || !taskPermission.taskDetail.taskProject.isAllowEdit
                          ? true
                          : false
                      }
                      tooltip={taskEffort ? true : false}
                      tooltipText={
                        <FormattedMessage
                          id="task.detail-dialog.project.hint1"
                          defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
                        />
                      }
                      obj={x}
                      labelAlign="left"
                      buttonPlaceholder={
                        <FormattedMessage
                          id="task.creation-dialog.form.project.placeholder"
                          defaultMessage="Select Project"
                        />
                      }
                      optionsList={this.generateProjectDropdownData(x)}
                      singleSelect={true}
                      updateAction={this.handleSelectChange}
                      selectedOptionHead={intl.formatMessage({
                        id: "common.task-project.label",
                        defaultMessage: "Task Project",
                      })}
                      allOptionsHead={intl.formatMessage({
                        id: "project.label",
                        defaultMessage: "Projects",
                      })}
                      TaskView={true}
                    />
                  </CanAccessFeature>
                </div>
              )}

            <div className={`${classes.gridItemDropdownCnt} flex_center_space_between_row`}>
              <AssigneeDropdown
                assignedTo={membersObjArr || []}
                updateAction={this.updateAssignee}
                isArchivedSelected={isArchivedSelected}
                obj={x}
                addPermission={taskPermission.taskDetail.taskAssign.isAllowAdd}
                deletePermission={taskPermission.taskDetail.taskAssign.isAllowDelete}
              />
              <div>
                <div className="flex_start_start_row">

                  <CanAccessFeature group='task' feature='statusTitle'>
                    <div style={{ marginRight: 10 }}>
                      {/* <StatusDropdown
                      status={x.status}
                      taskData={x}
                      btnType="iconBtn"
                      isArchivedSelected={isArchivedSelected}
                      permission={permission}
                      calculateProgress={this.handleCalculateProgress}
                      taskPer={taskPermission}
                    /> */}
                      <StatusDropdown
                        onSelect={status => {
                          this.handleStatusChange(status, x);
                        }}
                        option={selectedStatus}
                        style={{ marginRight: 8 }}
                        options={taskStatusData}
                        toolTipTxt={selectedStatus.label}
                        disabled={!taskPermission.taskDetail.editTaskStatus.isAllowEdit}
                        legacyIconButton={true}
                        writeFirst={false}
                      />
                    </div>
                  </CanAccessFeature>

                  <CanAccessFeature group='task' feature='priority'>
                    <div>
                      <FlagDropdown
                        priority={x.priority}
                        taskData={x}
                        searchFilterApplied={this.props.searchFilterApplied}
                        isArchivedSelected={isArchivedSelected}
                        permission={permission}
                        taskPer={taskPermission}
                      />
                    </div>
                    </CanAccessFeature>
                </div>
              </div>
            </div>

            <div className={`${classes.progressCountCnt} flex_center_space_between_row`}>
              <Typography variant="body1">{`${progress}%`}</Typography>
              <Typography variant="body1">{RemainingDaysStatus} </Typography>
            </div>
            <LinearProgress
              variant="determinate"
              value={progress}
              classes={{
                root: classes.progressBar,
                colorPrimary: overdue ? classes.progressBarBackColor : "",
                barColorPrimary: overdue ? classes.progressBarColor : "",
              }}
            />
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              classes={{ container: classes.itemBottomBtns }}>
              <DefaultButton
                buttonType="smallIconBtn"
                onClick={e => this.showTaskDetailsPopUp(e, x, "risk")}>
                <RiskIcon classes={{ root: classes.smallBtnIcon }} />
                <SvgIcon viewBox="0 0 18 15.75" className={classes.smallBtnIcon}>
                  <RiskIcon />
                </SvgIcon>
                <Typography variant="body2" align="center">
                  {x.risks > 0 ? 1 : '-'}
                </Typography>
              </DefaultButton>

              <DefaultButton
                buttonType="smallIconBtn"
                onClick={e => this.showTaskDetailsPopUp(e, x, "issue")}>
                <IssuesIcon classes={{ root: classes.smallBtnIcon }} />
                <SvgIcon viewBox="0 0 16 17.375" className={classes.smallBtnIcon}>
                  <IssuesIcon />
                </SvgIcon>
                <Typography variant="body2" align="center">
                  {x.issues > 0 ? x.issues : '-'}
                </Typography>
              </DefaultButton>

              <DefaultButton
                buttonType="smallIconBtn"
                onClick={e => this.showTaskDetailsPopUp(e, x, "meeting")}>
                <MeetingsIcon classes={{ root: classes.smallBtnIcon }} />
                <SvgIcon viewBox="0 0 17.031 17" className={classes.smallBtnIcon}>
                  <MeetingsIcon />
                </SvgIcon>
                <Typography variant="body2" align="center">
                  {x.meetings > 0 ? x.meetings : '-'}
                </Typography>
              </DefaultButton>

              <DefaultButton
                buttonType="smallIconBtn"
                onClick={e => this.showTaskDetailsPopUp(e, x, "comment")}>
                <CommentIcon
                  className={`${classes.commentsIcon} ${x.totalUnreadComments
                    ? classes.commentsIconColorRed
                    : classes.commentsIconColorDim
                    }`}
                />
                <Typography
                  variant="body2"
                  align="center"
                  className={`${x.totalUnreadComment
                    ? classes.commentsIconColorRed
                    : classes.commentsIconColorDim
                    }`}>
                  {x.comments > 0 ? x.comments : '-'}
                </Typography>
              </DefaultButton>
              <DefaultButton
                buttonType="smallIconBtn"
                onClick={e => this.showTaskDetailsPopUp(e, x, "comment")}>
                <AttachmentIcon classes={{ root: classes.attachmentIcon }} />
                <Typography variant="body2" align="center">
                  {x.totalAttachment ? x.totalAttachment : 0}
                </Typography>
              </DefaultButton>
            </Grid>
          </div>
        </Grid>
      );
    });

    return (
      <Fragment>
        {openStatusDialog && (
          <TaskStatusChangeDialog
            open={openStatusDialog}
            handleClose={this.handleCloseDialog}
            oldStatus={oldStatusItem}
            newTemplateItem={newTemplateItem}
            handleSaveAsTemplate={this.handleSaveTemplate}
          />
        )}
        <ActionConfirmation
          open={markChecklistActionConf}
          closeAction={this.markAllConfirmDialogClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="task.detail-dialog.to-do-list.mark-all.action"
              defaultMessage="Yes, Mark All Complete"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="task.detail-dialog.to-do-list.mark-all.title"
              defaultMessage="Mark All Complete"
            />
          }
          iconType="markAll"
          msgText={
            <FormattedMessage
              id="task.detail-dialog.to-do-list.mark-all.message"
              defaultMessage="Are you sure you want to mark all to-do list items as completed?"
            />
          }
          successAction={() => this.handleUpdateStatus(selectedStatus, selectedTaskObj)}
          btnQuery={markChecklistActionBtnQuery}
        />
        {this.state.showTaskDetails ? (
          // <TaskDetails
          //   closeTaskDetailsPopUp={this.closeTaskDetailsPopUp}
          //   currentTask={this.state.currentTask}
          //   type={type}
          //   filterUnArchiveTask={this.props.filterUnArchiveTask}
          //   isArchivedSelected={isArchivedSelected}
          //   deleteTaskFromArchiveList={this.props.deleteTaskFromArchiveList}
          // />
          this.props.taskDetailDialogState(null, {
            id: this.state.currentTask.taskId,
            afterCloseCallBack: () => {
              this.closeTaskDetailsPopUp()
            },
            type: type,
            filterUnArchiveTask: this.props.filterUnArchiveTask,
            deleteTaskFromArchiveList: this.props.deleteTaskFromArchiveList
          })
        ) : null}

        {publicLinkOpen ? (
          <PublicLink
            open={publicLinkOpen}
            id={publicLinkId}
            taskTitle={publicLinkTitle}
            closeAction={this.closePublicLinkDialog}
            maxTitleWidth={500}
          />
        ) : null}

        <Grid container spacing={2}>
          {gridData}
        </Grid>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    taskNotificationsState: state.taskNotifications,
    workspacePermissionsState: state.workspacePermissions,
    projects: state.projects.data,
    taskPer: state.workspacePermissions.data.task,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    permissionObject: ProjectPermissionSelector(state),
    members: state.profile.data.member.allMembers,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(itemStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateTask,
    FetchTasksInfo,
    UpdateTaskColorFromGridItem,
    UpdateCalenderTask,
    CopyTask,
    CopyCalenderTask,
    DeleteTask,
    ArchiveTask,
    UnArchiveTask,
    CheckAllTodoList,
    taskDetailDialogState,
    updateTaskData
  })
)(TaskGridItem);
