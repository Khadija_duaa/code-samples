const itemStyles = theme => ({
  AssigneeAvatar: {
    border: `2px solid ${theme.palette.common.white}`
  },
  disabledStatusBtn: {
    color: `${theme.palette.text.medLight} !important`
  },
  addAssigneeBtn:{
    background: theme.palette.common.white,
    border: `1px dashed ${theme.palette.border.lightBorder}`,
    padding: 7,
    marginLeft: 5,
    borderRadius: "100%"
  },
  addAssigneeBtnIcon: {
    fontSize: "20px !important"
  },
  taskGridTaskList: {
    width: 260,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  taskGridCnt: {
    padding: "0 30px 0 60px",
    flex: 1,
    overflowY: "auto"
  },
  TotalAssignee: {
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main
  },
  taskItemCnt: {
    background: theme.palette.common.white,
    padding: 15,
    borderRadius: "5px",
    boxShadow: "0px 0px 5px -1px rgba(0,0,0,0.07)"

  },
  statusIcon: {
    fontSize: "11px !important"
  },
  btnStatusIcon: {
    fontSize: "16px !important"
  },
  statusItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  statusMenuItemCnt: {
    padding: "2px 0 2px 16px",
    fontSize: "12px !important",
    height: 20
  },
  statusMenuItemSelected: {
    background: `transparent !important`,
    "&:hover": {
      background: `transparent !important`
    }
  },
  progressCountCnt: {
    marginBottom: 5
  },
  progressBar: {
    borderRadius:20,
    marginBottom:15
  },
  progressBarColor:{
    backgroundColor: theme.palette.error.dark
  },
  progressBarBackColor:{
    backgroundColor: theme.palette.error.xlight
  },
  smallBtnIcon:{
    fontSize: "18px !important",
    color: theme.palette.secondary.light,
    marginRight: 5
  },
  attachmentIcon:{
    fontSize: "22px !important",
    color: theme.palette.secondary.light,
    marginRight: 5
  },
  commentsIcon:{
    fontSize: "22px !important",
    marginRight: 5
  },
  commentsIconColorDim:{
    color: theme.palette.secondary.light,
  },
  commentsIconColorRed:{
    color: theme.palette.error.dark
  },
  smallIconBtnCount: {
    fontSize: "16px !important",
    marginLeft: 3,
    lineHeight: "normal"
  },
  itemBottomBtns: {
    marginBottom: 10
  },
  gridItemHeadingCnt:{
    display: "flex",
    justifyContent: "space-between"
  },
  gridItemDropdownCnt:{
    marginBottom: 20
  },
  menuItem:{
    padding: "8px 16px 8px 16px",
    marginBottom: 2
  },
  selectedValue:{ //Adds left border to selected list item
    "&:before":{
      content: "''",
      position: "absolute",
      left:0,
      right:0,
      top:0,
      bottom:0,
      width:5,
      background: theme.palette.secondary.main,
    }
  },
  priorityIcon:{
    fontSize: "16px !important"
  },
  projectDropdown:{
    marginBottom: 10
  },
  recurrenceIcon: {
    fontSize: "18px !important"
  }
});

export default itemStyles;
