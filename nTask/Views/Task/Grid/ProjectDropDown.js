import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles/";
import SearchDropdown from "../../../components/Menu/SearchMenu";
import itemStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import { getEditPermissionsWithArchieve } from "../permissions";
import CustomButton from "../../../components/Buttons/CustomButton";
import { UpdateTask, updateTaskData } from "../../../redux/actions/tasks";

class ProjectDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      selectedProject: ""
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }
  componentDidMount() {
    this.setSelectedProject();
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(prevProps.taskData.projectId) !== JSON.stringify(this.props.taskData.projectId)
    || JSON.stringify(prevProps.projectsState.data.length) !== JSON.stringify(this.props.projectsState.data.length)) {
      this.setSelectedProject();
    }
  }

  setSelectedProject = () => {
    const { projectId } = this.props.taskData;
    if (projectId) {
      let project = this.props.projectsState.data;
      project = project.length
        ? project
          .filter(x => x.projectId === projectId)
        : [];
      if (project.length) {
        this.setState({ selectedProject: project[0].projectName });
      }
      else
        this.setState({ selectedProject: "" });
    } else {
      this.setState({ selectedProject: "Select Project" });
    }
  }

  getProjectIdByName = () => {
    const { selectedProject } = this.state;
    if (selectedProject !== "Select Project") {
      let project = this.props.projectsState.data;
      project = project.length
        ? project
          .filter(x => x.projectName === selectedProject)
        : [];
      return project[0].projectId;
    }
    return '';
  }

  handleClose(event) {
    this.setState({ open: false }, () => {
      let task = { ...this.props.taskData };
      const { projectId } = task;
      const newProjectId = this.getProjectIdByName();
      if (projectId !== newProjectId) {
        let obj = { projectId: newProjectId };
        this.props.updateTaskData({task : task , obj});
      }
    });
  }

  handleSelect(value) {
    this.setState({ selectedProject: value });
  }

  handleClick(event, placement) {
    event.stopPropagation();
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }

  render() {
    const { classes, theme, permission, taskData, isArchivedSelected } = this.props;
    const { open, placement, selectedProject } = this.state;
    const ddData =
      this.props.projectsState &&
        this.props.projectsState.data &&
        this.props.projectsState.data.length
        ? this.props.projectsState.data
          .map(x => {
            return {
              id: x.projectId,
              value: { name: x.projectName }
            };
          })
        : [];

    const canEdit = getEditPermissionsWithArchieve(
      taskData,
      permission,
      "taskProject"
    );
    return (
      <Fragment>
        {!canEdit ? (
          <Typography variant="body2">{selectedProject}</Typography>
        ) : (
            <Fragment>
              <CustomButton
                buttonRef={node => {
                  this.anchorEl = node;
                }}
                disabled={isArchivedSelected || !canEdit}
                variant="text"
                btnType="plain"
                labelAlign="left"
                onClick={event => this.handleClick(event, "bottom-start")}
              >
                {selectedProject == "Select Project" ? (
                  <u>{selectedProject}</u>
                ) : (
                    selectedProject
                  )}
              </CustomButton>

              <SearchDropdown
                placement={placement}
                open={open}
                anchorRef={this.anchorEl}
                title="Projects"
                selectAction={this.handleSelect}
                closeAction={this.handleClose}
                searchQuery={["id", "value.name"]}
                data={ddData}
                isProject={true}
                selectedProject={selectedProject}
                autoUpdate={true}
              />
            </Fragment>
          )}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    projectsState: state.projects
  }
}


export default compose(
  withRouter,
  withStyles(itemStyles, { withTheme: true }),
  connect(
  mapStateToProps,
    { UpdateTask, updateTaskData }
  )
)(ProjectDropDown);
