import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { UpdateTask, FetchTasksInfo, UpdateGantTask, updateTaskData } from "../../../redux/actions/tasks";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import RoundIcon from "@material-ui/icons/Brightness1";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import { getEditPermissionsWithArchieve } from "../permissions";
import cloneDeep from "lodash/cloneDeep";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import CustomButton from "../../../components/Buttons/CustomButton";
import { GetPermission } from "../../../components/permissions";
import { injectIntl } from "react-intl";
class StatusDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      name: [],
      selectedStatus: false,
      isChanged: false,
      selectedColor: props.theme.palette.status.notStarted,
      checked: ["Not Started"],
      showPopup: false,
      btnQuery: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    // this.handleChecklist = this.handleChecklist.bind(this);
  }
  // handleChecklist() {
  //   let currentTask = cloneDeep(this.props.taskData),
  //     self = this;
  //   let checkList = currentTask.checkList.map(x => {
  //     x.isDone = true;
  //     x.checkedDate = new Date().toISOString();
  //     return x;
  //   });
  //   currentTask.checkList = checkList;
  //   currentTask.status = 3;
  //   this.setState({ btnQuery: "progress" }, () => {
  //     if (this.props.isGantt) {
  //       this.props.UpdateGantTask(currentTask, (err, response) => {
  //         if (response) {
  //           self.setState({ btnQuery: "", showPopup: false }, () => {
  //             if (this.props.statusChanged) this.props.statusChanged();
  //           });
  //         }
  //       });
  //     } else {
  //       self.props.UpdateTask(currentTask, x => {
  //         self.setState({ btnQuery: "", showPopup: false }, () => {
  //           if (this.props.statusChanged) this.props.statusChanged();
  //         });
  //       });
  //     }
  //   });
  // }
  handleDialogClose() {
    this.setState({ showPopup: false });
  }
  componentDidMount() {
    // let status = ["Not Started", "In Progress", "In Review", "Completed", "Cancelled"];
    let item = this.props.taskData.taskStatus.statusList.find(item => item.statusId == this.props.status);
    let value = ["Not Started"];
    if (item) {
      value = [item.statusTitle]
    }
    if (this.props.status >= 0)
      this.setState({ checked: value, isChanged: true });
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    // let status = ["Not Started", "In Progress", "In Review", "Completed", "Cancelled"];
    let item = nextProps.taskData.taskStatus.statusList.find(item => item.statusId == nextProps.status);
    let value = ["Not Started"];
    if (item) {
      value = [item.statusTitle]
    }
    if (nextProps.status >= 0 && prevState.isChanged)
      return { checked: value, isChanged: true };
    return { isChanged: false };
  }

  handleClose(event) {
    this.setState({ open: false });
  }
  handleClick(event, placement) {
    event.stopPropagation();

    // this.setState(state => ({
    //   open: !state.open
    // }));
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }

  handleToggle = (e, value, color) => {
    e.stopPropagation();
    let status = ["Not Started", "In Progress", "In Review", "Completed", "Cancelled"],
      ischeck = -1;

    let data = cloneDeep(this.props.taskData);
    // if (
    //   value === "Completed" &&
    //   (data.checkList &&
    //     data.checkList.length &&
    //     data.checkList.filter(x => x.isDone === false).length)
    // ) {
    //   this.setState({ showPopup: true });
    // } else {
    const obj = { status: status.indexOf(value) };

    ischeck = this.state.checked.indexOf(value);
    this.handleClose();
    if (ischeck < 0) {
      // if (this.props.calculateProgress)
      //   this.props.calculateProgress(data, true);
      this.props.updateTaskData({ task: data, obj }, res => {
        this.setState({
          isChanged: true,
        });
      });
    }

    this.setState({
      checked: [value],
      selectedColor: color,
      isChanged: false,
    });
    // }
    this.handleClose();
  };

  render() {
    const {
      classes,
      theme,
      renderText,
      status,
      btnStyle,
      btnType,
      isArchivedSelected,
      permission,
      taskData,
      taskPer,
      intl,
    } = this.props;
    const { open, placement, isChanged, btnQuery, checked, showPopup } = this.state;
    const statusColor = theme.palette.taskStatus;
    // const ddData = [
    //   { label: intl.formatMessage({id:"task.common.status.dropdown.not-started", defaultMessage:"Not Started"}),name: "Not Started", color: statusColor.NotStarted },
    //   { label: intl.formatMessage({id:"task.common.status.dropdown.in-progress", defaultMessage:"In Progress"}),name: "In Progress", color: statusColor.InProgress },
    //   { label: intl.formatMessage({id:"task.common.status.dropdown.in-review", defaultMessage:"In Review"}),name:"In Review", color: statusColor.InReview },
    //   { label: intl.formatMessage({id:"task.common.status.dropdown.completed", defaultMessage:"Completed"}),name:"Completed", color: statusColor.Completed },
    //   { label: intl.formatMessage({id:"task.common.status.dropdown.cancel", defaultMessage:"Cancelled"}),name:"Cancelled", color: statusColor.Cancelled }
    // ];
    const ddData = [];
    taskData.taskStatus.statusList.map(s => {
      ddData.push({
        label: s.statusTitle,
        name: s.statusTitle,
        color: s.statusColor,
      });
    });
    let taskStatusColor = this.state.selectedColor;
    if (this.props.status >= 0 && isChanged) {
      taskStatusColor = ddData[status].color;
    }
    const canEdit = taskPer ? taskPer.taskDetail.editTaskStatus.isAllowEdit : true;
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          {/* <ActionConfirmation
          open={showPopup}
          closeAction={this.handleDialogClose}
          cancelBtnText="Cancel"
          successBtnText="Yes"
          alignment="center"
          headingText="Task Status Update"
          iconType="archive"
          successAction={this.handleChecklist}
          btnQuery={btnQuery}
          msgText={<>Task checklist items are still incomplete.<div className="spacer"></div>Do you want to change checklist items status to complete?</>}
        /> */}

          {btnType == "iconBtn" ? (
            <CustomIconButton
              btnType="white"
              style={btnStyle}
              disabled={isArchivedSelected || !canEdit}
              buttonRef={node => {
                this.anchorEl = node;
              }}
              onClick={event => {
                this.handleClick(event, "bottom-start");
              }}>
              <RoundIcon htmlColor={taskStatusColor} classes={{ root: classes.btnStatusIcon }} />
            </CustomIconButton>
          ) : (
            <CustomButton
              style={{ background: taskStatusColor, ...btnStyle }}
              disabled={isArchivedSelected || !canEdit}
              buttonRef={node => {
                this.anchorEl = node;
              }}
              variant="contained"
              btnType="status"
              customClasses={{ disabled: classes.disabledStatusBtn }}
              onClick={event => {
                this.handleClick(event, "bottom-start");
              }}>
              {/* {ddData.find(f => f.name == checked[0]) || {}.label} */}
              {ddData.find(f => f.name == checked[0]).label}
            </CustomButton>
          )}
          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            anchorRef={this.anchorEl}
            list={
              <List
                onClick={event => {
                  event.stopPropagation();
                }}>
                <ListItem disableRipple={true} classes={{ root: classes.menuHeadingItem }}>
                  <ListItemText
                    primary={intl.formatMessage({
                      id: "task.common.status.label",
                      defaultMessage: "Set Status",
                    })}
                    classes={{ primary: classes.menuHeadingItemText }}
                  />
                </ListItem>
                {ddData.map(value => (
                  <ListItem
                    key={value.name}
                    button
                    disableRipple={true}
                    onClick={e => this.handleToggle(e, value.name, value.color)}
                    className={`${classes.menuItem} ${this.state.checked.indexOf(value.name) !== -1 ? classes.selectedValue : ""
                      }`}
                    classes={{ selected: classes.statusMenuItemSelected }}>
                    <RoundIcon htmlColor={value.color} classes={{ root: classes.statusIcon }} />
                    <ListItemText
                      primary={value.label}
                      classes={{
                        primary: classes.statusItemText,
                      }}
                    />
                  </ListItem>
                ))}
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(combineStyles(menuStyles, itemStyles), {
    withTheme: true,
  }),
  connect(null, { UpdateTask, FetchTasksInfo, UpdateCalenderTask, UpdateGantTask, updateTaskData })
)(StatusDropdown);
