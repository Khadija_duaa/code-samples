import React, { Component } from "react";
import findIndex from "lodash/findIndex";
import sortBy from "lodash/sortBy";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import {
  UpdateTask,
  FetchTasksInfo,
  UpdateGantTask,
  updateTaskData
} from "../../../redux/actions/tasks";
import Avatar from "@material-ui/core/Avatar";
import CustomAvatar from "../../../components/Avatar/Avatar";
import { withStyles } from "@material-ui/core/styles";
import itemStyles from "./styles";
import AddIcon from "@material-ui/icons/Add";
import SearchDropdown from "../../../components/Menu/SearchMenu";
import { getEditPermissionsWithArchieve } from "../permissions";
import { generateUsername } from "../../../utils/common";
import IconButton from "@material-ui/core/IconButton";
import { isArrayEqual } from "../../../utils/common";

class AssigneeList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      assigneeList: [],
      allMembers: []
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  componentDidMount() {
    let allMembers =
      this.props.profileState &&
      this.props.profileState.data &&
      this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers.map(x => {
            const name = generateUsername(
              x.email,
              x.userName,
              x.fullName,
              null
            );
            return {
              id: x.userId,
              value: {
                name: name,
                avatar: x.imageUrl,
                email: x.email,
                fullName: x.fullName,
                isOnline: x.isOnline
              },
              isActiveMember: x.isActive
            };
          })
        : [];
    this.setState({
      assigneeList: this.updateAssigneesFromProps(),
      allMembers
    });
  }

  componentDidUpdate(prevProps) {
    const { assigneeList, profileState } = this.props;
    const { allMembers } = profileState.data.member;

    if (
      JSON.stringify(prevProps.assigneeList) !== JSON.stringify(assigneeList)
    ) {
      this.setState({ assigneeList: this.updateAssigneesFromProps() });
    } else if (
      JSON.stringify(allMembers) !==
      JSON.stringify(prevProps.profileState.data.member.allMembers)
    ) {
    
      let members = allMembers
        ? allMembers.map(x => {
            const name = generateUsername(
              x.email,
              x.userName,
              x.fullName,
              null
            );
            return {
              id: x.userId,
              value: {
                name: name,
                avatar: x.imageUrl,
                email: x.email,
                fullName: x.fullName,
                isOnline: x.isOnline,
                isOwner: x.isOwner
              },
              isActiveMember: x.isActive
            };
          })
        : [];

      this.setState({
        allMembers: members,
        assigneeList: this.updateAssigneesFromProps()
      });
    }
  }

  updateAssigneesFromProps = () => {
    const assigneeList = this.props.assigneeList || [];
    let assigneeListNew = assigneeList.length
      ? assigneeList.map(x => {
          const name = generateUsername(x.email, x.userName, x.fullName, null);
          return {
            id: x.userId,
            value: {
              name: name,
              avatar: x.imageUrl,
              email: x.email,
              fullName: x.fullName,
              isOnline: x.isOnline,
              isOwner: x.isOwner
            }
          };
        })
      : [];
    return assigneeListNew;
  };

  handleClose(data) {
    if (this.state.open) {
      this.setState({ open: false }, () => {
        // formating state assignees list, as props assignees list contain different attributes
        // userId is missing in state assignee list
        let assignees =
          this.state.assigneeList.map(obj => {
            obj["userId"] = obj.id;
            return obj;
          }) || [];

        if (
          !isArrayEqual(assignees, this.props.assigneeList, "userId") &&
          data &&
          data.checkAssignee
        ) {
          let obj = this.props.tasksState.data.find(
            k => k.taskId == this.props.taskId
          );
          delete obj.assigneeLists;
          obj.assigneeList =
            data.idList && data.idList.length
              ? data.idList.filter((e, i, s) => s.indexOf(e) === i)
              : [];
          if (this.props.isGantt) {
            this.props.UpdateGantTask(obj, (err, response) => {
              this.setState({ open: false });
            });
          } else {
            const object = {
              assigneeLists : obj.assigneeList 
            };
            this.props.updateTaskData({task : obj , object }, succ=>{
              this.setState({ open: false });
            }, err => {});
          }
        }
      });
    }
  }
  handleClick(event, placement) {
    event.stopPropagation();
    const { permission, taskData } = this.props;
    const canEdit = getEditPermissionsWithArchieve(
      taskData,
      permission,
      "assignee"
    );
    this.setState(state => ({
      open: canEdit ? state.placement !== placement || !state.open : false,
      placement
    }));
  }
  updateAssignees = data => {
    let index = findIndex(this.state.assigneeList, function(o) {
      return o.id === data.id;
    });
    let items = this.state.assigneeList;
    if (index > -1) items.splice(index, 1);
    else items.push(data);
    items = sortBy(items, [
      function(o) {
        return o.value.name;
      }
    ]);
    this.setState({ assigneeList: items });
  };

  render() {
    const { classes, theme, permission, taskData, isArchivedSelected } = this.props;
    const { open, placement } = this.state;
    const ddData = this.state.assigneeList;
    const allData = this.state.allMembers; // _.differenceWith(this.state.allMembers, ddData, _.isEqual);
    const canEdit = getEditPermissionsWithArchieve(
      taskData,
      permission,
      "assignee"
    );
    const GenerateList = () => {
      return ddData.slice(0, 3).map((Assignee, i) => {
        return (
          <li key={i} style={{ marginLeft: !i == 0 ? -5 : null }}>
            <CustomAvatar
              otherMember={{
                imageUrl: Assignee.value.avatar,
                fullName: Assignee.value.fullName,
                lastName: "",
                email: Assignee.value.email,
                isOnline: Assignee.value.isOnline,
                isOwner: Assignee.value.isOwner
              }}
              size="small"
            />
          </li>
        );
      });
    };
    return (
      <ul className="AssigneeAvatarList">
        <GenerateList />
        {ddData.length > 3 ? (
          <li>
            <Avatar classes={{ root: classes.TotalAssignee }}>
              +{ddData.length - 3}
            </Avatar>
          </li>
        ) : (
          ""
        )}
        <li className="AssigneeListBtnCnt">
          {canEdit ? (
            <IconButton
              classes={{ root: classes.addAssigneeBtn }}
              disabled={isArchivedSelected}
              onClick={event => {
                this.handleClick(event, "bottom-start");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}
            >
              <AddIcon
                classes={{ root: classes.addAssigneeBtnIcon }}
                htmlColor={theme.palette.text.dark}
              />
            </IconButton>
          ) : null}

          <SearchDropdown
            open={open}
            title="Add Assignee"
            closeAction={this.handleClose}
            anchorRef={this.anchorEl}
            searchQuery={["id", "value.name"]}
            data={allData}
            avatar={true}
            removeOption={false}
            checkAssignee={true}
            taskId={this.props.taskId}
            checkedType="multi"
            autoUpdate={true}
            updateAssignees={this.updateAssignees}
            newAssigneeList={ddData}
            tasksLength={this.props.tasksLength}
          />
        </li>
      </ul>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    tasksState: state.tasks
  }
}

export default compose(
  withRouter,
  withStyles(itemStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    { UpdateTask, FetchTasksInfo, UpdateGantTask, updateTaskData }
  )
)(AssigneeList);
