import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { UpdateTask, FetchTasksInfo, UpdateGantTask, updateTaskData } from "../../../redux/actions/tasks";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import { getEditPermissionsWithArchieve } from "../permissions";
import FlagIcon from "../../../components/Icons/FlagIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import { GetPermission } from '../../../components/permissions';
import { injectIntl } from "react-intl";
class FlagDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      name: [],
      selectedStatus: false,
      isChanged: false,
      checked: ["Low"],
      selectedColor: props.theme.palette.status.notStarted
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  componentDidMount() {
    let priority = ["Critical", "High", "Medium", "Low"];
    this.setState({
      checked: [priority[this.props.priority - 1]],
      isChanged: true
    });
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (
      JSON.stringify(nextProps.priority) !=
      JSON.stringify(this.props.priority) ||
      JSON.stringify(nextState) != JSON.stringify(this.state) ||
      nextProps.anchorEl !=
      this.props.anchorEl
    ) {
      return true;
    } else {
      return false;
    }
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    let priority = ["Critical", "High", "Medium", "Low"];
    return { checked: [priority[nextProps.priority - 1]], isChanged: true };
  }

  handleClose(event) {
    this.setState({ open: false });
    // bulkAction FlagDropDown Close function
    this.props.handleClosePriority();
  }
  handleClick(event, placement) {
    event.stopPropagation();
    this.setState(state => ({
      open: !state.open
    }));
  }
  handleToggle = (e, value, color) => {
    e.stopPropagation();
    if (this.props.anchorEl && this.props.updateAction) {
      this.props.updateAction(value);
      return;
    }
    let priority = ["Critical", "High", "Medium", "Low"],
      ischeck = -1;
    let data = this.props.taskData;
    this.handleClose();
    data.priority = priority.indexOf(value.name) + 1;
    let obj = { priority: priority.indexOf(value.name) + 1 };
    ischeck = this.state.checked.indexOf(value.name);
    if (ischeck < 0) {
      this.props.updateTaskData({ task: this.props.taskData, obj }, null, succ => {
        this.setState({
          isChanged: true
        });
      }, fail => { })
      // this.props.UpdateTask(data, response => {
      //   if (response && response.status !== 200) {
      //   }

      //   // this.props.searchFilterApplied()
      // });
    }
    this.setState({
      checked: [value.name],
      selectedColor: value.color,
      isChanged: false
    });
  };

  render() {
    const { classes, theme, priority, permission, taskData, isArchivedSelected, taskPer, intl, customFlagBtnRender } = this.props;
    const { open, placement, isChanged, checked } = this.state;
    const statusColor = theme.palette.taskPriority;
    const ddData = [
      { label: intl.formatMessage({ id: "task.common.priority.dropdown.critical", defaultMessage: "Critical" }), name: "Critical", color: statusColor.Critical },
      { label: intl.formatMessage({ id: "task.common.priority.dropdown.high", defaultMessage: "High" }), name: "High", color: statusColor.High },
      { label: intl.formatMessage({ id: "task.common.priority.dropdown.medium", defaultMessage: "Medium" }), name: "Medium", color: statusColor.Medium },
      { label: intl.formatMessage({ id: "task.common.priority.dropdown.low", defaultMessage: "Low" }), name: "Low", color: statusColor.Low }
    ];
    let priorityColor = this.state.selectedColor;
    if (priority && isChanged) {
      priorityColor = ddData[priority - 1].color;
    }
    const canEdit = taskPer ? taskPer.taskDetail.editTaskPriority.isAllowEdit : true;
    return (
      <ClickAwayListener mouseEvent="onMouseDown"
        touchEvent="onTouchStart" onClickAway={this.handleClose}>
        <div>
          {customFlagBtnRender ? customFlagBtnRender : <CustomIconButton
            btnType="white"
            disabled={isArchivedSelected || !canEdit}
            buttonRef={node => {
              this.anchorEl = node;
            }}
            onClick={event => {
              this.handleClick(event, "bottom-start");
            }}
          >
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor}
              classes={{ root: classes.priorityIcon }}
            >
              <FlagIcon />
            </SvgIcon>
          </CustomIconButton>
          }
          <SelectionMenu
            open={this.props.anchorEl ? Boolean(this.props.anchorEl) : open}
            anchorRef={this.props.anchorEl ? this.props.anchorEl : this.anchorEl}
            closeAction={this.handleClose}
            placement={placement}

            list={
              <List
                onClick={event => {
                  event.stopPropagation();
                }}
              >
                <ListItem
                  disableRipple={true}
                  classes={{ root: classes.menuHeadingItem }}
                >
                  <ListItemText
                    primary={intl.formatMessage({ id: "task.common.priority.placeholder", defaultMessage: "Select Priority" })}
                    classes={{ primary: classes.menuHeadingItemText }}
                  />
                </ListItem>
                {ddData.map(value => (
                  <ListItem
                    key={value.name}
                    button
                    disableRipple={true}
                    onClick={e => this.handleToggle(e, value)}
                    className={`${classes.menuItem} ${this.state.checked.indexOf(value.name) !== -1
                      ? classes.selectedValue
                      : ""
                      }`}
                    classes={{ selected: classes.statusMenuItemSelected }}
                  >
                    <SvgIcon
                      viewBox="0 0 24 32.75"
                      htmlColor={value.color}
                      classes={{ root: classes.priorityIcon }}
                    >
                      <FlagIcon />
                    </SvgIcon>
                    <ListItemText
                      primary={value.label}
                      classes={{
                        primary: classes.statusItemText
                      }}
                    />
                  </ListItem>
                ))}
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}
FlagDropdown.defaultProps = {

  customBtnRender: false,
  handleClosePriority: () => { }
};
export default compose(
  withRouter,
  injectIntl,
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true
  }),
  connect(
    null,
    { UpdateTask, FetchTasksInfo, UpdateGantTask, updateTaskData }
  )
)(FlagDropdown);
