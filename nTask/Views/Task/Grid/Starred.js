import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import CustomIconButton from "../../../components/Buttons/IconButton";
import StarIcon from "@material-ui/icons/StarRate";
import itemStyles from "./styles";
import { MarkTaskAsStarted } from "../../../redux/actions/tasks";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
class Starred extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isStarred: false
    };
  }
  componentDidMount() {
    this.setState({
      isStarred: this.props.currentTask
        ? this.props.currentTask.isStared
        : false
    });
  }

  static getDerivedStateFromProps(nextProps) {
    return {
      isStarred: nextProps.currentTask ? nextProps.currentTask.isStared : false
    };
  }

  handleStarClick = (e, data) => {
    e.stopPropagation();
    if (!data.isDeleted) {
      let obj = {
        Id: data.id,
        MarkStar: !this.state.isStarred
      };
      this.setState({ isStarred: !this.state.isStarred });
      this.props.MarkTaskAsStarted(obj, () => {});
    }
  };
  render() {
    const { classes, theme, currentTask, userId, style , disabled=false} = this.props;
    const { isStarred } = this.state;
    return (
      <CustomIconButton
        btnType="condensed"
        id="starBtn"
        onClick={e => this.handleStarClick(e, currentTask)}
        disabled={disabled}
        style={{
          opacity: isStarred ? 1 : "",
          color: isStarred ? "#FFC700" : "",
          ...style
        }}
      >
        <StarIcon
          htmlColor={
            this.state.isStarred
              ? theme.palette.background.star
              : theme.palette.secondary.light
          }
        />
      </CustomIconButton>
    );
  }
}

export default compose(
  withRouter,
  withStyles(itemStyles, { withTheme: true }),
  connect(null, {
    MarkTaskAsStarted,
    UpdateCalenderTask
  })
)(Starred);
