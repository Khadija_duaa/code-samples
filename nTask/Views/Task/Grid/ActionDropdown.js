import React, { Component, Fragment } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { Scrollbars } from "react-custom-scrollbars";
import { getCompletePermissionsWithArchieve } from "../permissions";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import { FormattedMessage } from "react-intl";
import { v4 as uuidv4 } from "uuid";
import { emptyTask } from "../../../utils/constants/emptyTask";

class ActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      deleteBtnQuery: "",
      archiveBtnQuery: "",
      unarchiveBtnQuery: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleColorClick = this.handleColorClick.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleCustomColor = this.handleCustomColor.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }
  handleCustomColor(event) {
    let obj = Object.assign({}, this.props.task);
    obj.colorCode = event.hex;
    delete obj.CalenderDetails;

    this.props.UpdateTaskColor(obj, data => {
      this.props.UpdateCalender(data);
    });
  }
  handleOperations = (e, value) => {
    e.stopPropagation();
    switch (value) {
      case "Copy":
        const clientId = uuidv4();
        const postObj = {
          ...emptyTask,
          taskTitle: `Copy of ${this.props.task.taskTitle}`,
          clientId,
          id: clientId,
          isNew: true,

        };
        this.props.CopyTask(
          null,
          this.props.task.taskId,
          postObj,
        );
        break;
      case "Public Link":
        this.handleClose();
        break;
      case "Archive":
        this.setState({ archiveFlag: true, openFeedback: true });
        break;
      case "Unarchive":
        this.setState({ unArchiveFlag: true, openFeedback: true });
        break;
      case "Delete":
        this.setState({ deleteFlag: true, openFeedback: true });
        break;
    }
    this.setState({ open: false, pickerOpen: false });
  };
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  handleDialogClose(e) {
    if (e) e.stopPropagation();
    this.setState({
      openFeedback: false,
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
    });
  }

  handleDelete = e => {
    const { task, filterUnArchiveTask = () => { } } = this.props;
    e.stopPropagation();
    this.setState({ deleteBtnQuery: "progress" }, () => {
      this.props.DeleteTask(
        task,
        response => {
          filterUnArchiveTask(task.taskId);
          this.setState({ deleteBtnQuery: "", deleteFlag: false });
        },
        error => {
          this.setState({ deleteBtnQuery: "", deleteFlag: false });
          if (error) {
            // self.showSnackBar('Server throws error','error');
          }
        }, null
      );
    });
  };

  handleClick(event, placement) {
    event.stopPropagation();
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleColorClick(event) {
    if (event.target.closest("#colorPickerCnt") == null) {
      this.setState(prevState => ({ pickerOpen: !prevState.pickerOpen }));
    }
    event.stopPropagation();
  }

  handleArchive = e => {
    if (e) e.stopPropagation();
    this.setState({ archiveBtnQuery: "progress" }, () => {
      this.props.ArchiveTask(
        this.props.task.taskId,
        () => {
          this.setState({
            archiveBtnQuery: "",
            archiveFlag: false,
            openFeedback: false,
          });
        },
        error => {
          this.setState({
            archiveBtnQuery: "",
            archiveFlag: false,
            openFeedback: false,
          });
        }, null
      );
    });
  };

  handleUnArchive = e => {
    if (e) e.stopPropagation();
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.UnArchiveTask(
        this.props.task,
        () => {
          this.setState({
            unarchiveBtnQuery: "",
            unArchiveFlag: false,
            openFeedback: false,
          });
          this.props.filterUnArchiveTask(this.props.task.taskId);
        },
        error => {
          this.setState({
            unarchiveBtnQuery: "",
            unArchiveFlag: false,
            openFeedback: false,
          });
        }
      );
    });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Color":
        value = "common.action.color.label";
        break;
      case "Archive":
        value = "common.action.archive.confirmation.archive-button.label";
        break;
      case "Delete":
        value = "common.action.delete.confirmation.delete-button.label";
        break;
      case "Unarchive":
        value = "common.action.un-archive.confirmation.title";
        break;
      case "Public Link":
        value = "common.action.public-link.label";
        break;
      case "Copy":
        value = "common.action.copy.label";
        break;
    }
    return value;
  }
  render() {
    const {
      classes,
      theme,
      selectedColor,
      permission,
      task,
      openPublicLinkDialog,
      isArchivedSelected,
      taskPer,
    } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      openFeedback,
      deleteBtnQuery,
      archiveBtnQuery,
      unarchiveBtnQuery,
    } = this.state;
    const archivePermission = taskPer.archive.cando;
    const unArchivePermission = taskPer.unarchive.cando;
    const copyPermission = taskPer.copyInWorkSpace.cando;
    const deletePermission = taskPer.delete.cando;

    const ddData = [];

    if (copyPermission && !isArchivedSelected) {
      ddData.push("Copy");
    }
    if (!isArchivedSelected) {
      ddData.push("Color");
    }
    if (!isArchivedSelected && taskPer.publicLink.cando) {
      ddData.push("Public Link");
    }
    if (archivePermission && !isArchivedSelected) {
      ddData.push("Archive");
    }
    if (unArchivePermission && isArchivedSelected) {
      ddData.push("Unarchive");
    }
    if (deletePermission) {
      ddData.push("Delete");
    }
    return (
      <Fragment>
        <CustomIconButton
          btnType="condensed"
          onClick={event => {
            this.handleClick(event, "bottom-start");
          }}
          buttonRef={node => {
            this.anchorEl = node;
          }}>
          <MoreVerticalIcon htmlColor={theme.palette.secondary.light} />
        </CustomIconButton>
        <SelectionMenu
          isAddColSelectionClicked={true}
          open={open}
          closeAction={this.handleClose}
          placement={placement}
          anchorRef={this.anchorEl}
          list={
            <List>
              <ListItem disableRipple={true} classes={{ root: classes.menuHeadingItem }}>
                <ListItemText
                  primary={
                    <FormattedMessage id="common.action.label" defaultMessage="Select Action" />
                  }
                  classes={{ primary: classes.menuHeadingListItemText }}
                />
              </ListItem>
              {ddData.map(value =>
                value == "Color" ? (
                  <ListItem
                    key={value}
                    button
                    disableRipple={true}
                    classes={{
                      root: classes.selectColorMenuItem,
                      selected: classes.statusMenuItemSelected,
                    }}
                    onClick={event => {
                      this.handleColorClick(event);
                    }}>
                    <ListItemText
                      primary={
                        <FormattedMessage id={this.getTranslatedId(value)} defaultMessage={value} />
                      }
                      classes={{
                        primary: classes.statusItemText,
                      }}
                    />
                    <RightArrow
                      htmlColor={theme.palette.secondary.dark}
                      classes={{ root: classes.submenuArrowBtn }}
                    />
                    <div
                      id="colorPickerCnt"
                      className={classes.colorPickerCntLeft}
                      style={pickerOpen ? { display: "block" } : { display: "none" }}>
                      <ColorPicker
                        triangle="hide"
                        onColorChange={color => {
                          this.props.colorChange(this.props.task, color);
                        }}
                        selectedColor={selectedColor}
                      />
                    </div>
                  </ListItem>
                ) : value === "Public Link" ? (
                  <ListItem
                    button
                    disableRipple={true}
                    classes={{ selected: classes.statusMenuItemSelected }}
                    onClick={event => {
                      openPublicLinkDialog(event, task.id, task.taskTitle);
                      this.handleClose();
                    }}>
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id={this.getTranslatedId("Public Link")}
                          defaultMessage={"Public Link"}
                        />
                      }
                      classes={{
                        primary: classes.statusItemText,
                      }}
                    />
                  </ListItem>
                ) : (
                  <ListItem
                    key={value}
                    button
                    disableRipple={true}
                    classes={{ selected: classes.statusMenuItemSelected }}
                    onClick={e => this.handleOperations(e, value)}>
                    <ListItemText
                      primary={
                        <FormattedMessage id={this.getTranslatedId(value)} defaultMessage={value} />
                      }
                      classes={{
                        primary: classes.statusItemText,
                      }}
                    />
                  </ListItem>
                )
              )}
            </List>
          }
        />

        <React.Fragment>
          {this.state.archiveFlag ? (
            <ActionConfirmation
              open={openFeedback}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.archive.confirmation.archive-button.label"
                  defaultMessage="Archive"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="common.action.archive.confirmation.archive-button.label"
                  defaultMessage="Archive"
                />
              }
              iconType="archive"
              msgText={
                <FormattedMessage
                  id="common.archived.tasks.messageb"
                  defaultMessage="Are you sure you want to archive this {label}?"
                  values={{
                    label: "task",
                  }}
                />
              }
              successAction={this.handleArchive}
              btnQuery={archiveBtnQuery}
            />
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {this.state.unArchiveFlag ? (
            <ActionConfirmation
              open={openFeedback}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.title"
                  defaultMessage="Unarchive"
                />
              }
              alignment="center"
              iconType="unarchive"
              headingText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.title"
                  defaultMessage="Unarchive"
                />
              }
              msgText={
                <FormattedMessage
                  id="common.un-archived.tasks.messageb"
                  defaultMessage="Are you sure you want to unarchive these tasks?"
                />
              }
              successAction={this.handleUnArchive}
              btnQuery={unarchiveBtnQuery}
            />
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {this.state.deleteFlag ? (
            <DeleteConfirmDialog
              open={openFeedback}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.delete.confirmation.delete-button.label"
                  defaultMessage="Delete"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="common.action.delete.confirmation.title"
                  defaultMessage="Delete"
                />
              }
              successAction={this.handleDelete}
              msgText={
                <FormattedMessage
                  id="common.action.delete.confirmation.task.label"
                  defaultMessage="Are you sure you want to delete this task?"
                />
              }
              btnQuery={deleteBtnQuery}
            />
          ) : null}
        </React.Fragment>
      </Fragment >
    );
  }
}

export default withStyles(combineStyles(itemStyles, menuStyles), {
  withTheme: true,
})(ActionDropdown);
