export const columns = {
  id: {
    key: "uniqueId",
    name: "ID",
    valueGetter: 'uniqueId'
  },
  none: {
    key: "",
    name: "None",
  },
  taskTitle: {
    key: "taskTitle",
    name: "Task",
    valueGetter: 'taskTitle'
  },
  taskStatus: {
    key: "status",
    name: "Status",
    valueGetter: 'status'
  },
  startDate: {
    key: "startDate",
    name: "Planned Start/End",
    valueGetter: 'startDate'
  },
  actualStartDate: {
    key: "actualStartDate",
    name: "Actual Start/End",
    valueGetter: 'actualStartDate'
  },
  taskPriority: {
    key: "priority",
    name: "Priority",
    valueGetter: 'priority'
  },
  projectName: {
    key: "project",
    name: "Project",
    valueGetter: 'project'
  },
  progress: {
    key: "progress",
    name: "Progress",
    valueGetter: 'progress'
  },
  assignees: {
    key: "assigneeLists",
    name: "Assignee",
    valueGetter: 'assigneeLists'
  },
  totalEffort: {
    key: "totalEffort",
    name: "Time Logged",
    valueGetter: 'totalEffort'
  },
  attachments: {
    key: "totalAttachment",
    name: "Attachments",
    valueGetter: 'attachments'
  },
  totalComment: {
    key: "totalComment",
    name: "Comments",
    valueGetter: 'totalComment'
  },
  meetings: {
    key: "meetings",
    name: "Meetings",
    valueGetter: 'meetings'
  },
  issues: {
    key: "issues",
    name: "Issues",
    valueGetter: 'issues'
  },
  risks: {
    key: "risks",
    name: "Risks",
    valueGetter: 'risks'
  },
  createdDate: {
    key: "createdDate",
    name: "Creation Date",
    valueGetter: 'createdDate'
  },
  createdBy: {
    key: "createdBy",
    name: "Created By",
    valueGetter: 'createdBy'
  }
}

{

}