const listStyles = (theme) => ({
  smallBtnIcon: {
    fontSize: "16px !important",
    color: theme.palette.secondary.light,
    marginRight: 5,
  },
  checkedIcon: {
    fontSize: "20px !important",
    marginRight: 5,
  },
  statusIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  priorityIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  crownIcon: {
    fontSize: "13px !important",
    marginRight: 5,
  },
  timerIcon: {
    fontSize: "14px !important",
    marginRight: 5,
    color: theme.palette.secondary.light,
  },
  descriptionText: {
    wordBreak: "break-all",
  },
});

export default listStyles;
