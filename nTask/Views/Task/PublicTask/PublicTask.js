import React from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import instance from "../../../redux/instance";
import "./style.css";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import queryString from "query-string";
import RoundIcon from "@material-ui/icons/Brightness1";
import FlagIcon from "@material-ui/icons/Flag";
import SvgIcon from "@material-ui/core/SvgIcon";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import MailIcon from "../../../components/Icons/MailIcon";
import CheckBoxIcon from "../../../components/Icons/CheckBoxIcon";
import CrownIcon from "../../../components/Icons/CrownIcon";
import TimesheetIcon from "../../../components/Icons/TimesheetIcon";
import styles from "./styles";
import Logo from "./images/logo.svg";
import cloud1 from "./images/cloud1.png";
import cloud2 from "./images/cloud2.png";
import cloud3 from "./images/cloud3.png";
import ReactHtmlParser from 'react-html-parser';
import { FormattedMessage } from "react-intl";
import constants from "../../../redux/constants/types";
import axios from "axios";

class PublicTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      errorMessage: "",
    };
  }
  componentDidMount() {
    // let a = queryString(this.props.location.search)
    let id = queryString.parse(this.props.location.search).Id;
    instance()
      .get(`/api/usertask/GetTaskPublicById?ecryptedTaskId=${id}`)
      .then((response) => {
        this.setState({ data: response.data });
      })
      .catch((response) => {
        this.setState({ errorMessage: response.data.message });
      });
    // this.getCompanyDataWithUrl()
  }


  getCompanyDataWithUrl = () => {
    var data = JSON.stringify({
      "domain": "https://app.totalenvironment.com"
    });
    
    var config = {
      method: 'post',
      url: 'https://0yahalgupg.execute-api.us-east-1.amazonaws.com/dev/companydetail',
      headers: { 
        'Content-Type': 'application/json'
      },
      data : data
    };
    
    axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  naviagtor = (path) => {
    this.props.history.push(`/account/${path}`);
  };

  render() {
    const { data, errorMessage } = this.state;
    const { classes, theme } = this.props;
    const statusColor = theme.palette.taskStatus;
    const color = theme.palette;
    const statusColors = {
      "Not Started": statusColor.NotStarted,
      "In Progress": statusColor.InProgress,
      "In Review": statusColor.InReview,
      "Completed": statusColor.Completed,
      "Cancelled": statusColor.Cancelled,
    };
    const priorityColors = {
      Critical: color.taskPriority.Critical,
      High: color.taskPriority.High,
      Medium: color.taskPriority.Medium,
      Low: color.taskPriority.Low,
    };
    return (
      <React.Fragment>
        <Helmet>
          <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
            integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
            crossorigin="anonymous"
          />
        </Helmet>
        {
          <div className="taskPublicLink">
            <header className="header">
              <div className="c_container">
                <div className="header_inner">
                  <div className="logo">
                    <img src={Logo} alt="alt" />
                  </div>
                  <div className="header_btns">
                    <ul>
                      <li onClick={() => this.naviagtor("login")}>
                        <a className="headerButton font_regular">Sign In</a>
                      </li>
                      <li onClick={() => this.naviagtor("register")}>
                        <a className="headerButton font_regular">Sign Up</a>
                      </li>
                    </ul>
                  </div>
                  <div className="clear"></div>
                  <p className="font_regular">
                    The platform that works for you, us and everyone else!
                  </p>
                </div>
              </div>
            </header>
            {data ? (
              <main className="main">
                <div className="c_container">
                  <div className="cloud1">
                    <img src={cloud1} />
                  </div>
                  <div className="cloud2">
                    <img src={cloud2} />
                  </div>
                  <div className="cloud3">
                    <img src={cloud3} />
                  </div>
                  <div className="uid">
                    <div className="uid_head">
                      <div className="uid_heading">
                        <h1 className="font_regular">
                          {data.taskDetails.taskTitle}
                        </h1>
                        <ul>
                          <li className="font_regular">
                            <SvgIcon
                              viewBox="0 0 17.031 17"
                              className={classes.smallBtnIcon}
                            >
                              <MeetingsIcon />
                            </SvgIcon>
                            <span style={{ marginRight: 5, color: "#aeaeae" }}>
                              Start:
                            </span>{" "}
                            <span>
                              {data.taskDetails.startDate
                                ? moment
                                  .utc(data.taskDetails.startDate)
                                  .utcOffset(moment(new Date()).utcOffset())
                                  .format(" MMM DD, YYYY")
                                : "-"}
                            </span>
                          </li>
                          <li className="font_regular">
                            <SvgIcon
                              viewBox="0 0 17.031 17"
                              className={classes.smallBtnIcon}
                            >
                              <MeetingsIcon />
                            </SvgIcon>
                            <span style={{ marginRight: 5, color: "#aeaeae" }}>
                              End:
                            </span>{" "}
                            <span>
                              {data.taskDetails.endDate
                                ? moment
                                  .utc(data.taskDetails.endDate)
                                  .utcOffset(moment(new Date()).utcOffset())
                                  .format(" MMM DD, YYYY")
                                : "-"}
                            </span>
                          </li>
                        </ul>
                      </div>
                      <div className="head_contact">
                        <ul>
                          <li>
                            <SvgIcon
                              viewBox="0 0 17.031 17"
                              className={classes.smallBtnIcon}
                              onClick={() => {
                                window.print();
                              }}
                            >
                              <MailIcon />
                            </SvgIcon>
                          </li>
                          <li>
                            <SvgIcon
                              viewBox="0 0 17.031 17"
                              className={classes.smallBtnIcon}
                            >
                              <MailIcon />
                            </SvgIcon>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="uid_body">
                      <div className="progress_bar_before"></div>
                      <div
                        className="progress_bar_after"
                        style={{ width: `${data.taskDetails.progress}%` }}
                      ></div>
                      <div className="progress">
                        <input
                          id="progress_bar"
                          type="number"
                          value={`${100}`}
                        />
                        <span className="font_regular">
                          {data.taskDetails.progress}% Completed
                        </span>
                      </div>
                      <div className="owner">
                        <span className="font_regular">Task Owner: </span>

                        <SvgIcon
                          viewBox="0 0 20 20"
                          className={classes.crownIcon}
                        >
                          <CrownIcon />
                        </SvgIcon>
                        <span className="font_regular">
                          {data.taskOwner.fullName ||
                            data.taskOwner.userName ||
                            data.taskOwner.email}
                        </span>
                      </div>
                      <ul className="project">
                        <li>
                          <div className="status">
                            <span>Status:</span>

                            <RoundIcon
                              htmlColor={data.taskDetails.statusColor}
                              className={classes.statusIcon}
                            />
                            <span> {data.taskDetails.status}</span>
                          </div>
                          <div className="priority flex_center_start_row">
                            <span>Priority:</span>

                            <FlagIcon
                              htmlColor={
                                priorityColors[data.taskDetails.priority]
                              }
                              className={classes.priorityIcon}
                            />
                            <span>{data.taskDetails.priority}</span>
                          </div>
                        </li>
                        <li>
                          <div className="project_name">
                            <span>Project:</span>
                            <span>{data.taskDetails.projectName}</span>
                          </div>
                          <div className="time_logged">
                            <span>Time Logged:</span>
                            <span>
                              <SvgIcon
                                viewBox="0 0 17 19.625"
                                className={classes.timerIcon}
                              >
                                <TimesheetIcon />
                              </SvgIcon>
                              {data.taskDetails.timeLogged}
                            </span>
                          </div>
                        </li>
                      </ul>
                      <div className="desc">
                        <span className="descriptionText">Description:</span>
                        <span className={classes.descriptionText}>
                          {ReactHtmlParser(
                            data.taskDetails.description
                              ? data.taskDetails.description
                              : ""
                          )}
                        </span>
                      </div>
                      <div className="checklist">
                        <span>Checklist:</span>
                        <span>
                          <ul>
                            {data.taskDetails.checkList.map((item, index) => {
                              return (
                                <li
                                  className={item.isComplete ? "active" : ""}
                                  key={index}
                                >
                                  <SvgIcon
                                    viewBox="0 0 426.667 426.667"
                                    htmlColor={
                                      item.isComplete
                                        ? theme.palette.status.completed
                                        : theme.palette.background.items
                                    }
                                    classes={{ root: classes.checkedIcon }}
                                  >
                                    <CheckBoxIcon />
                                  </SvgIcon>
                                  <span style={{ wordBreak: "break-word" }}>{item.description}</span>
                                </li>
                              );
                            })}
                          </ul>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="overlay"></div>
              </main>
            ) : null}
            {errorMessage ? (
              <div className="c_container">
                <div className="cloud1">
                  <img src={cloud1} />
                </div>
                <div className="cloud2">
                  <img src={cloud2} />
                </div>
                <div className="cloud3">
                  <img src={cloud3} />
                </div>
                <div className="uid">
                  <div className="uid_body">{errorMessage}</div>
                </div>
              </div>
            ) : null}
          </div>
        }
      </React.Fragment>
    );
  }
}

export default compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(PublicTask);
