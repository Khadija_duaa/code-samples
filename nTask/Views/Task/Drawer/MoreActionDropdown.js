import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import workspaceSetting from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { FormattedMessage } from "react-intl";
class MoreActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false
    };
  }
  handleClose = (event) => {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose = (event) => {
    this.setState({ pickerOpen: false });
  }

  handleClick = (event, placement) => {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }
  render() {
    const {
      classes,
      theme,
      deleteSchedule,
      editSchedule,
      taskPer
    } = this.props;
    const {
      open,
      placement,
    } = this.state;
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomIconButton
            btnType="condensed"
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
          >
            <MoreVerticalIcon
              htmlColor={theme.palette.secondary.medDark}
              style={{ fontSize: "28px", color: 'white' }}
            />
          </CustomIconButton>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            anchorRef={this.anchorEl}
            list={
              <List>
                <ListItem disableRipple className={classes.headingItem}>
                  <ListItemText
                    primary={<FormattedMessage id="common.action.label" defaultMessage="Select Action" />}
                    classes={{ primary: classes.headingText }}
                  />
                </ListItem>
                {taskPer.taskDetail.repeatTask.isAllowEdit && <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusItemText }}
                  onClick={editSchedule}
                >
                  <ListItemText
                    primary={<FormattedMessage id="task.drawer.task-schedule.edit-schedule.label" defaultMessage="Edit Schedule" />}
                    classes={{
                      primary: classes.statusItemText
                    }}
                  />
                </ListItem>}
                {taskPer.taskDetail.repeatTask.isAllowDelete && <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusItemText }}
                  onClick={deleteSchedule}
                >
                  <ListItemText
                    primary={<FormattedMessage id="task.drawer.task-schedule.delete-schedule.label" defaultMessage="Delete Schedule" />}
                    classes={{
                      primary: classes.statusItemTextDanger
                    }}
                  />
                </ListItem>}
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

export default withStyles(combineStyles(workspaceSetting, menuStyles), {
  withTheme: true
})(MoreActionDropdown);
