import { columns } from "./constants";

export const sortingDropdownData = [
  { key: "", name: "None" },
  { key: "uniqueId", name: "ID" },
  { key: "taskTitle", name: "Title" },
  // { key: "risks", name: "Risks" },
  // { key: "issues", name: "Issues" },
  { key: "statusTitle", name: "Status" },
  { key: "priority", name: "Priority" },
  { key: "projectName", name: "Project" },
  { key: "progress", name: "Progress" },
  // { key: "meetings", name: "Meetings" },
  // { key: "totalComment", name: "Comments" },
  // { key: "attachments", name: "Attachments" },
  { key: "totalEffort", name: "Time Logged" },
  { key: "createdDate", name: "Creation Date" },
  { key: "actualStartDate", name: "Actual Start Date" },
  { key: "actualDueDate", name: "Actual End Date" },
  { key: "startDate", name: "Planned Start Date" },
  { key: "dueDate", name: "Planned End Date" }
]
export const groupingDropdownData = [columns.none, columns.taskStatus, columns.taskPriority, columns.projectName, columns.assignees]