import React, { useState, useRef, useEffect, memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import CustomListItem from "../../../components/ListItem/CustomListItem";
import withStyles from "@material-ui/core/styles/withStyles";
import taskActionDropdownStyles from "./taskActionDropdown.style";
import CustomMenuList from "../../../components/MenuList/CustomMenuList";
import {
  CopyTask,
  DeleteTask,
  ArchiveTask,
  updateTaskData,
  UnArchiveTask,
  MoveTask,
} from "../../../redux/actions/tasks";
import { emptyTask } from "../../../utils/constants/emptyTask";
import { v4 as uuidv4 } from "uuid";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { FormattedMessage } from "react-intl";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import {
  publickLink,
  setDeleteDialogueState,
  taskDetailDialogState,
} from "../../../redux/actions/allDialogs";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import isEqual from "lodash/isEqual";
import "react-toastify/dist/ReactToastify.css";
import IconActivity from "../../../components/Icons/TaskActionIcons/IconActivity";
import IconArchive from "../../../components/Icons/TaskActionIcons/IconArchive";
import IconColor from "../../../components/Icons/TaskActionIcons/IconColor";
import IconDelete from "../../../components/Icons/TaskActionIcons/IconDelete";
import SvgIcon from "@material-ui/core/SvgIcon";
import IconCopy from "../../../components/Icons/TaskActionIcons/IconCopy";
import IconLink from "../../../components/Icons/TaskActionIcons/IconLink";
import MoveIcon from "../../../components/Icons/MoveIcon";
import MoveTaskDialog from "../../../components/Dialog/MoveTaskDialog/MoveTaskDialog";
import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";

// import constants from "../constants/types";
function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}
const TaskActionDropdown = memo(props => {
  const [open, setOpen] = useState(null);
  const [movedTask, setMovedTask] = useState(null);
  const [moveBtnQuery, setMoveBtnQuery] = useState("");
  const [archiveConfirmation, setArchiveConfirmation] = useState(false);
  const [unArchiveConfirmation, setUnArchiveConfirmation] = useState(false);
  const [archiveBtnQuery, setArchiveBtnQuery] = useState("");
  const [unarchiveBtnQuery, setUnarchiveBtnQuery] = useState("");
  const { companyInfo } = useSelector(state => {
    return {
      companyInfo: state.whiteLabelInfo.data,
    };
  });
  const anchorEl = useRef(null);
  const dispatch = useDispatch();
  const {
    classes,
    data,
    btnProps,
    handleActivityLog,
    handleCloseCallBack,
    taskPermission,
    isArchived = false,
    activityLogOption = false,
  } = props;
  const handleClose = event => {
    // Function closes dropdown
    setOpen(null);
  };
  const handleArchiveConfirmClose = () => {
    setArchiveConfirmation(false);
    setUnArchiveConfirmation(false);
  };
  const handleArchiveConfirmation = () => {
    setArchiveConfirmation(true);
  };
  const handleUnArchiveConfirmation = () => {
    setUnArchiveConfirmation(true);
  };
  const handleDropdownOpen = event => {
    // Function Opens the dropdown
    setOpen(state => (state ? null : event.currentTarget));
  };
  const showSnackBar = (snackBarMessage = "", type = null) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  useEffect(() => {
    anchorEl.current &&
      anchorEl.current.addEventListener("click", e => {
        e.stopPropagation();
        handleDropdownOpen(e);
      });
  }, []);
  //Delete single task
  const deleteTask = () => {
    const { onDeleteTask, GanttCmp, childTasks, isGantt } = props;
    const childIds = childTasks && childTasks.map(t => t.id);
    const taskId = data.taskId;
    handleCloseCallBack();
    dispatch(setDeleteDialogueState({ btnQuery: "progress" }));
    if (isGantt && onDeleteTask) {
      onDeleteTask(taskId, GanttCmp, childIds, false);
    }
    DeleteTask(
      data,
      (data, res) => {
        closeDeleteConfirmation();
      },
      err => {
        closeDeleteConfirmation();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
  };
  //Copy of task
  const copyTask = () => {
    const clientId = uuidv4();
    const postObj = {
      ...emptyTask,
      taskTitle: `Copy of ${data.taskTitle}`,
      clientId,
      id: clientId,
      isNew: true,
    };
    CopyTask(dispatch, data.taskId, postObj);
    handleClose();
  };
  //archive a task
  const archiveTask = () => {
    const { onDeleteTask, GanttCmp, childTasks, isGantt } = props;
    const childIds = childTasks && childTasks.map(t => t.id);

    setArchiveBtnQuery("progress");
    handleCloseCallBack();
    const taskId = data.taskId;
    if (isGantt && onDeleteTask) {
      onDeleteTask(taskId, GanttCmp, childIds, false);
    }
    ArchiveTask(
      taskId,
      () => {
        // Success Callback
        handleArchiveConfirmClose();
        setArchiveBtnQuery("");
      },
      err => {
        // failure callback
        setArchiveBtnQuery("");
        handleArchiveConfirmClose();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
    handleClose();
  };

  const handleUnArchive = e => {
    if (e) e.stopPropagation();
    setUnarchiveBtnQuery('progress');
    handleCloseCallBack();
    UnArchiveTask(
      data,
      //success
      () => {
        handleArchiveConfirmClose();
        setUnarchiveBtnQuery('');
      },
      //failure
      error => {
        setUnarchiveBtnQuery('');
        showSnackBar("Task Cannot be UnArchived", "error");
      },
      dispatch
    );
  };
  //Handle Public Link dialog
  const handlePublicLink = () => {
    dispatch(publickLink({ taskId: data.id, taskTitle: data.taskTitle }));
    handleClose();
  };

  //Handle move task dialog
  const handleMoveTask = () => {
    if (data.projectId) {
      showSnackBar("You cannot move tasks those are linked to project", "error");
      return;
    }
    setMovedTask(data);
  };
  //Confirmation Dialog close
  const closeDeleteConfirmation = () => {
    dispatch(setDeleteDialogueState({ open: false, btnQuery: "" }));
  };
  //Open delete confirmation dialog
  const handleDeleteConfirmation = success => {
    const taskLabelSingle = companyInfo?.task?.sName;
    const dialogObj = {
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: `Are you sure you want to delete this ${taskLabelSingle || "task"}?`,
      successAction: deleteTask,
      closeAction: closeDeleteConfirmation,
      btnQuery: "",
    };
    dispatch(setDeleteDialogueState(dialogObj));
    handleClose();
  };
  const colorChange = color => {
    const obj = { colorCode: color };
    handleClose();
    updateTaskData(
      { task: data, obj },
      dispatch,
      //Success
      task => { }
    );
  };
  const handleSuccMoveTask = (e, team) => {
    if (e) e.stopPropagation();
    if (!data.projectId) {
      setMoveBtnQuery("progress");
      taskDetailDialogState(dispatch, { id: "", isArchived: false });
      let obj = {
        taskId: data.taskId,
        workspaceId: team.teamId,
      };
      MoveTask(
        obj,
        succ => {
          // FetchWorkspaceInfo(
          //   "",
          //   () => { },
          //   () => { },
          //   dispatch
          // );
          setMoveBtnQuery("");
          setMovedTask(null);
          showSnackBar("Task moved to workspce successfully", "success");
        },
        fail => {
          if (fail && fail?.data?.message) showSnackBar(fail.data.message, "error");
          setMoveBtnQuery("");
          setMovedTask(null);
        },
        dispatch
      );
    }
  };
  // permissions allowed
  let copyPer = taskPermission ? taskPermission.copyInWorkSpace.cando : true;

  let publicLinkPer = taskPermission ? taskPermission.publicLink.cando : true;
  let archivePer = taskPermission ? taskPermission.archive.cando : true;
  let unarchivePer = taskPermission ? taskPermission.unarchive.cando : true;
  let deletePer = taskPermission ? taskPermission.delete.cando : true;
  let movePer = taskPermission ? taskPermission.moveTask.cando : true;
  const taskAccess = companyInfo?.task?.isHidden ? false : true;
  const isOpen = Boolean(open);
  const taskLabelSingle = companyInfo?.task?.sName;
  return (
    <>
      <CustomIconButton
        btnType="transparent"
        buttonRef={node => (anchorEl.current = node)}
        style={{ padding: 0 }}
        {...btnProps}>
        <MoreVerticalIcon style={{fontSize: "24px"}} className={classes.ellipsesIcon} />
      </CustomIconButton>
      <DropdownMenu
        open={isOpen}
        closeAction={handleClose}
        anchorEl={anchorEl.current}
        size={"small"}
        placement="bottom-end"
        disablePortal={false}>
        <CustomMenuList>
          {copyPer && !isArchived && (
            <CustomListItem rootProps={{ onClick: copyTask }}>
              <SvgIcon viewBox="0 0 14 14" className={classes.icon}>
                <IconCopy />
              </SvgIcon>
              Copy
            </CustomListItem>
          )}
          {!isArchived && (
            <CustomListItem
              subNav={true}
              popperProps={{
                placement: "left-start",
              }}
              subNavRenderer={
                <>
                  <ColorPicker
                    triangle="hide"
                    onColorChange={colorChange}
                    selectedColor={data.colorCode || ""}
                    width={190}
                  />
                </>
              }>
              <SvgIcon viewBox="0 0 16.5 16.5" className={classes.icon}>
                <IconColor />
              </SvgIcon>
              Color
            </CustomListItem>
          )}
          {publicLinkPer && !isArchived && (
            <CustomListItem rootProps={{ onClick: handlePublicLink }}>
              <SvgIcon viewBox="0 0 15.01 15.001" className={classes.icon}>
                <IconLink />
              </SvgIcon>
              Public Link
            </CustomListItem>
          )}
          {activityLogOption && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleActivityLog }}>
              <SvgIcon viewBox="0 0 18 15.428" className={classes.icon}>
                <IconActivity />
              </SvgIcon>
              Activity Log
            </CustomListItem>
          )}
          {movePer && !isArchived && taskAccess && (
            <CustomListItem rootProps={{ onClick: handleMoveTask }}>
              <SvgIcon viewBox="0 0 40 40" className={classes.icon}>
                <MoveIcon />
              </SvgIcon>
              Move Task
            </CustomListItem>
          )}
          {archivePer && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.014" className={classes.icon}>
                <IconArchive />
              </SvgIcon>
              Archive
            </CustomListItem>
          )}
          {unarchivePer && isArchived && (
            <CustomListItem rootProps={{ onClick: handleUnArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.014" className={classes.icon}>
                <IconArchive />
              </SvgIcon>
              Un Archive
            </CustomListItem>
          )}
          {deletePer && (
            <CustomListItem rootProps={{ onClick: handleDeleteConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.002" className={classes.icon}>
                <IconDelete />
              </SvgIcon>
              Delete
            </CustomListItem>
          )}
        </CustomMenuList>
      </DropdownMenu>
      {archiveConfirmation ? (
        <ActionConfirmation
          open={true}
          closeAction={handleArchiveConfirmClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="common.archived.tasks.messageb"
              defaultMessage="Are you sure you want to archive this {label}?"
              values={{
                label: taskLabelSingle || "task",
              }}
            />
          }
          successAction={archiveTask}
          btnQuery={archiveBtnQuery}
        />
      ) : null}
      {unArchiveConfirmation ? (
        <ActionConfirmation
          open={unArchiveConfirmation}
          closeAction={handleArchiveConfirmClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.un-archive-button.label"
              defaultMessage="Unarchive"
            />
          }
          alignment="center"
          iconType="unarchive"
          headingText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.title"
              defaultMessage="Unarchive"
            />
          }
          msgText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.task.label"
              defaultMessage="Are you sure you want to unarchive this {label}?"
              values={{
                label: taskLabelSingle || "task",
              }}
            />
          }
          successAction={handleUnArchive}
          btnQuery={unarchiveBtnQuery}
        />
      ) : null}
      {movedTask && (
        <MoveTaskDialog
          open={movedTask}
          taskName={movedTask?.taskTitle}
          successAction={handleSuccMoveTask}
          closeAction={() => {
            setMovedTask(null);
            setMoveBtnQuery("");
          }}
          btnQuery={moveBtnQuery}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          exceptionText={""}
          headingText={"Move Task to Workspace"}
          isMulti={false}
        />
      )}
    </>
  );
}, areEqual);

export default compose(
  withSnackbar,
  withStyles(taskActionDropdownStyles, { withTheme: true })
)(TaskActionDropdown);
