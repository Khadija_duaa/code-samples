const getTasksPermissions = (taskData, props) => {
  const permission = taskData ? taskData.isOwner ? true : 
  props.workspacePermissionsState.data && props.workspacePermissionsState.data.task ? 
  props.workspacePermissionsState.data.task : false : false;
  return permission;
};

const getTasksPermissionsWithoutArchieve = (taskData, props) => {
  const permission = taskData
    ? taskData.isOwner
      ? true
      : props.workspacePermissionsState.data && props.workspacePermissionsState.data.task
      ? props.workspacePermissionsState.data.task
      : false
    : false;
  return permission;
};

const getEditPermissionsWithArchieve  = (taskData, permission, key) => {
  if (permission && Object.keys(permission).length && taskData && key) {
    return permission[key] && (permission[key].edit || permission[key].add) ? true : false;
  }
  return permission;
};

const getCompletePermissionsWithArchieve = (taskData, permission, key) => {
  if (permission && Object.keys(permission).length && taskData && key) {
    return permission[key];
  } 
  return permission;
};
export function canView(task, permissions, key){
  return task.isOwner ? true : permissions[key]
}
export function canDo(task, permissions, key){
return task.isOwner ? true : permissions[key]
}
export function canAdd(task, permissions, key){
return task.isOwner ? true : permissions[key].add

}
export function canEdit(task, permissions, key){
return task.isOwner ? true : permissions[key].edit

}
export function canDelete(task, permissions, key){
return task.isOwner ? true : permissions[key].delete
}
export {
  getEditPermissionsWithArchieve,
  getTasksPermissions,
  getTasksPermissionsWithoutArchieve,
  getCompletePermissionsWithArchieve
};
