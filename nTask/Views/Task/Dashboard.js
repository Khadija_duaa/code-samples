import React, { Component, Fragment, useEffect } from "react";
import { Helmet } from "react-helmet";
import queryString from "query-string";
import { compose } from "redux";
import { withRouter, Route } from "react-router-dom";
import findIndex from "lodash/findIndex";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import dashboardStyles from "./styles";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import selectStyles from "../../assets/jss/components/select";
import combineStyles from "../../utils/mergeStyles";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import ListItemText from "@material-ui/core/ListItemText";
import RoundIcon from "@material-ui/icons/Brightness1";
import ArchivedIcon from "@material-ui/icons/Archive";
import TaskGridItem from "./Grid/Item";
import TaskCalendar from "./Calendar/Calendar";
import CustomCalendar from "../../components/Calendar/Calendar.cmp";
import TaskList from "./List/TaskList.view";
import SvgIcon from "@material-ui/core/SvgIcon";
import AdvFilterIcon from "../../components/Icons/AdvFilterIcon";
import QuickFilterIcon from "../../components/Icons/QuickFilterIcon";
import IconRefresh from "../../components/Icons/IconRefresh";
import CustomButton from "../../components/Buttons/CustomButton";
import ImportExportDD from "../../components/Dropdown/ImportExportDD";
import TaskFilter from "./TaskFilter/TaskFilter";
import classNames from "classnames";
import Footer from "../../components/Footer/Footer";
import Divider from "@material-ui/core/Divider";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { getArchivedData } from "../../redux/actions/tasks";
import { saveSortingProjectList } from "../../redux/actions/projects";
import { getSortOrder, sortListData } from "../../helper/sortListData";
import { taskDetailDialogState } from "../../redux/actions/allDialogs";
import ColumnSelectionDropdown from "../../components/Dropdown/SelectedItemsDropDown";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import helper from "../../helper";

import RecurrenceIcon from "../../components/Icons/RecurrenceIcon";
import {
  assosiateRemoveTaskArchivedMethod,
  getTasks,
  removeTaskQuickFilter,
  updateQuickFilter,
  showAllTasks,
  updateTaskFilter,
  createNextOccurrence,
  updateTaskData,
} from "../../redux/actions/tasks";
import { setAppliedFilters } from "../../redux/actions/appliedFilters";
import { withSnackbar } from "notistack";
import { hideLoading } from "react-redux-loading-bar";
import EmptyState from "../../components/EmptyStates/EmptyState";
import PlannedVsActualDropDown from "./PlannedVsActualDropDown";
import { getFilteredTasks } from "./TaskFilter/FilterUtils";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import { sortingDropdownData, groupingDropdownData } from "./dropdownData";
import { calculateContentHeight, isScrollBottom } from "../../utils/common";
import PlainMenu from "../../components/Menu/menu";
import MenuList from "@material-ui/core/MenuList";
import { translate } from "../../i18n/translate";
import { injectIntl, FormattedMessage } from "react-intl";
import ListLoader from "../../components/ContentLoader/List";
import ColumnSelector from "../../redux/selectors/columnSelector";
import { getColumnsList } from "../../redux/actions/columns";
import isEmpty from "lodash/isEmpty";
import { grid } from "../../components/CustomTable2/gridInstance";
import DefaultTextField from "../../components/Form/TextField";
import debounce from "lodash/debounce";
import { doesFilterPass } from "./List/TaskFilter/taskFilter.utils";
import moment from "moment";
import isUndefined from "lodash/isUndefined";
import ProjectPermissionSelector from "../../redux/selectors/projectPermissionSelector";
import DefaultDialog from "../../components/Dialog/Dialog";
import AddNewTaskForm from "../AddNewForms/addNewTaskFrom";
export const SnackbarContext = React.createContext();
let increment = 20;

class TaskDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alignment: "left",
      filteredTasks: [],
      quickFilters: ["Show All"],
      open: false,
      placement: "bottom-start",
      anchorEl: "anchorEl",
      listView: true,
      gridView: false,
      calendarView: false,
      isArchived: false,
      openFilterSidebar: false,
      multipleFilters: "",
      clearlastState: false,
      total: this.props.tasks.length,
      loadMore: 0,
      isChanged: false,
      showRecords: increment,
      profileSettingsDialogueOpen: false,
      transform: 0,
      publicLinkOpen: false,
      addNewForm: false,
      calenderDateType: "Creation Date",
      sortObj: this.getSortValues(),
      sortingDropDownData: sortingDropdownData,
      groupingDropdownData,
      selectedGroup: {},
      searchQuery: "",
      isLoading: true,
      refreshBtnQuery: "",
      currentTaskOccuranceId: "",
      createReccurrenceDate: null,
      openRecurrenceConfirm: false,
      activeTaskTitle: "",
      currentTask: null,
      currentTaskId: "",
      btnQuery: "",
      selectableTask: false,
      startDate: new Date(),
      endDate: new Date(),
    };
    this.allFilterList = [
      { key: "Show All", value: "Show All" },
      { key: "Not Started", value: "Not Started" },
      { key: "In Progress", value: "In Progress" },
      { key: "In Review", value: "In Review" },
      { key: "Completed", value: "Completed" },
      { key: "Cancelled", value: "Cancelled" },
      { key: "Due Today", value: "Due Today" },
      { key: "Over Due", value: "Over Due" },
      { key: "Starred", value: "Starred" },
      { key: "Archived", value: "Archived" },
    ];
    this.child = React.createRef();
    this.renderListView = this.renderListView.bind(this);
    this.renderCalendarView = this.renderCalendarView.bind(this);
    this.renderGridView = this.renderGridView.bind(this);
    this.handleArchiveChange = this.handleArchiveChange.bind(this);
    this.handleExportType = this.handleExportType.bind(this);
    this.closeSnakBar = this.closeSnakBar.bind(this);
    this.searchFilterApplied = this.searchFilterApplied.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.returnCount = this.returnCount.bind(this);
    this.handleChangeState = this.handleChangeState.bind(this);
    this.resetCount = this.resetCount.bind(this);
    this.throttleHandleSearch = debounce(this.throttleHandleSearch, 1000);
  }
  getTranslatedId(value) {
    if (value.includes("Show All")) {
      return (value = "common.show.all");
    } else if (value.includes("Not Started")) {
      return (value = "task.common.status.dropdown.not-started");
    } else if (value.includes("In Progress")) {
      return (value = "task.common.status.dropdown.in-progress");
    } else if (value.includes("In Review")) {
      return (value = "task.common.status.dropdown.in-review");
    } else if (value.includes("Completed")) {
      return (value = "task.common.status.dropdown.completed");
    } else if (value.includes("Cancelled")) {
      return (value = "task.common.status.dropdown.cancel");
    } else if (value.includes("Due Today")) {
      return (value = "common.action.other.duetoday");
    } else if (value.includes("Over Due")) {
      return (value = "common.action.other.overdue");
    } else if (value.includes("Starred")) {
      return (value = "common.action.other.started");
    } else if (value.includes("Archived")) {
      return (value = "common.archived.archived");
    } else return value;
  }
  getSortValues = () => {
    const { workspaces } = this.props;
    let loggedInTeam = this.props.loggedInTeam;

    if (loggedInTeam) {
      let { taskColumn, taskDirection } = getSortOrder(workspaces, loggedInTeam, 2);

      if (taskColumn && taskDirection) {
        return { column: taskColumn, direction: taskDirection, cancelSort: "" };
      } else {
        return { column: "", direction: "", cancelSort: "NONE" };
      }
    }
  };

  componentDidMount() {
    this.props.getTasks(
      null,
      null,
      succ => {
        this.props.hideLoading();
      },
      () => { }
    );
    // this.props.assosiateRemoveTaskArchivedMethod(this.removeTaskFromArchivedList);
    const parsed = queryString.parse(location.search);
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 2000);
    // For handling default task filter
    this.setState({
      profileSettingsDialogueOpen: parsed.mode === "emailPreferences",
      loggedInTeam: this.props.loggedInTeam,
      // filteredTasks: getFilteredTasks(
      //   this.props.tasks,
      //   this.props.appliedFilters.Task,
      //   [],
      //   this.props.issues,
      //   this.props.risks,
      //   this.props.meetings,
      //   this.props.members,
      //   this.props.projects,
      //   this.props.taskPer,
      //   this.props.meetingPer,
      //   this.props.riskPer,
      //   this.props.issuePer,
      //   this.props.workspaceStatus
      // ),
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      dialogsState: { taskDetailDialog },
    } = this.props;
    let searchQuery = this.props.history.location.search; //getting task id in the url
    if (searchQuery) {
      let taskId = queryString.parseUrl(searchQuery).query.taskId; //Parsing the url and extracting the task id using query string
      let taskFound = this.props.tasks.find(item => item.taskId == taskId);
      if (taskId && taskFound) {
        !taskDetailDialog.id &&
          this.props.taskDetailDialogState(null, {
            id: taskId,
            afterCloseCallBack: () => { },
            type: "comment",
          });
      } else {
        this.handleExportType("Oops! Task not found.", "error");
        this.props.history.push("/tasks");
      }
    }
  }

  handleProfileSettingsDialogCloseAction = () => {
    this.setState({ profileSettingsDialogueOpen: false });
    this.props.history.push("/tasks");
  };
  resetCount() {
    this.setState({ showRecords: increment, loadMore: increment });
  }
  returnCount(total, loadMore) {
    this.setState({ total, loadMore });
  }

  handleScrollDown = e => {
    const bottom = isScrollBottom(e);
    if (bottom) {
      if (this.state.total > this.state.showRecords)
        this.setState({ showRecords: this.state.showRecords + increment }, () => {
          this.setState({ loadMore: this.state.showRecords }, () => {
            if (this.state.total < this.state.showRecords) {
              this.setState({
                showRecords: this.state.total,
                loadMore: this.state.total,
              });
            }
          });
        });
    }
  };
  handleChangeState = () => {
    this.setState({ isChanged: false });
  };
  clearFilter() {
    this.setState({ multipleFilters: "", clearlastState: false });
  }
  searchFilterApplied() {
    this.setState({ multipleFilters: "" });
  }
  closeSnakBar() {
    this.setState({ showSnakBar: false });
  }

  contextFunction = () => {
    document.querySelector("body").style.transform = "translateY(50px)";
    setTimeout(function () {
      document.querySelector("body").style.transform = "";
    }, 3000);
  };
  handleDrawerOpen = () => {
    this.setState({ openFilterSidebar: true });
  };

  handleDrawerClose = () => {
    this.setState({ openFilterSidebar: false });
  };

  handleExportType(snackBarMessage, type) {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  }

  handleArchiveChange() {
    this.setState({ isArchived: !this.state.isArchived });
  }

  renderListView() {
    // const {classes} = this.props;
    this.setState({
      listView: true,
      gridView: false,
      calendarView: false,
      alignment: "left",
    });
    //   this.props.enqueueSnackbar(<div className={classes.snackBarHeadingCnt}>
    //     <p className={classes.snackBarHeading}>Task Completed</p> // snackbar heading
    //     <p className={classes.snackBarContent}>User interface Design Completed</p> //snackbar message
    //  </div>,
    //     {
    //       anchorOrigin: { // defines position of snackbar
    //         vertical: "bottom",
    //         horizontal: "right"
    //       },
    //       variant: "info" // Type of snackbar
    //     }
    //   );
  }
  renderGridView() {
    this.setState({
      listView: false,
      gridView: true,
      calendarView: false,
      alignment: "center",
    });
  }
  renderCalendarView() {
    this.setState({
      listView: false,
      gridView: false,
      calendarView: true,
      alignment: "right",
    });
  }
  handleClick(event, placement) {
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleClose = () => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };
  handleRefresh = () => {
    this.props.getTasks(
      null,
      null,
      () => {        
      },
      () => {       
      }
    );
  };

  handleAlignment = (event, alignment) => this.setState({ alignment });

  handleSelectClick = value => {
    const { quickFilters } = this.props;
    const filterObj = { [value]: { type: value, selectedValue: [] } };
    if (quickFilters[value]) {
      this.props.removeTaskQuickFilter(value);
      setTimeout(() => {
        !isEmpty(grid.grid) && grid.grid.redrawRows();
      }, 0);
      return;
    }
    if (value == "Archived") {
      this.props.getArchivedData(2, () => {
        this.props.updateQuickFilter(filterObj);
        setTimeout(() => {
          !isEmpty(grid.grid) && grid.grid.redrawRows();
        }, 0);
      });
      return;
    }
    this.props.updateQuickFilter(filterObj);
  };
  handleShowAllTasks = () => {
    this.props.showAllTasks({});
    setTimeout(() => {
      !isEmpty(grid.grid) && grid.grid.redrawRows();
    }, 0);
  };
  componentWillUnmount() {
    this.handleShowAllTasks();
  }
  handleDialogClose = name => {
    this.setState({
      openRecurrenceConfirm: false,
      createReccurrenceDate: "",
      activeTaskTitle: "",
      currentTaskOccuranceId: "",
    });
  };
  handleCreateReccureTask = () => {
    const { createReccurrenceDate, currentTaskOccuranceId } = this.state;
    const data = {
      TaskId: currentTaskOccuranceId,
      date: moment(createReccurrenceDate).format("MM/DD/YYYY hh:mm:ss A"),
    };
    this.setState({ btnQuery: "progress" });
    //Create Next task occurence
    this.props.createNextOccurrence(
      data,
      //Success
      () => {
        this.setState({ btnQuery: "" });
        this.handleDialogClose();
      },
      //Failure
      error => {
        this.setState({ btnQuery: "" });
        this.handleExportType(error.data.message, "error");
      }
    );
  };
  handleClearArchived = () => {
    this.setState({ quickFilters: [], open: false });
  };
  handleTaskbarsType = (event, type) => {
    this.setState({ calenderDateType: type });
  };
  sortingTaskList = (column, direction, cancelSort = "") => {
    /** function passing to sorting drop down for getting the selected value  */
    this.setState({
      sortObj: { column: column, direction: direction, cancelSort: cancelSort },
    }); /** updating state object and passing sortObj to task List */
    let obj = {
      task: [column],
      type: 2,
      TaskOrder: direction,
    };
    this.props.saveSortingProjectList(
      obj,
      succ => { },
      err => { }
    );
  };
  throttleHandleSearch = data => {
    grid.grid && grid.grid.setQuickFilter(data);
  };
  //handle task search
  handleSearch = e => {
    this.throttleHandleSearch(e.target.value);
    // this.setState({searchQuery: e.target.value});
  };
  //Lifecycle method on group select
  onGroupSelect = option => {
    this.setState({ selectedGroup: option });
  };
  onColumnHide = obj => {
    if (grid.grid) {
      grid.grid.columnModel.applyColumnState({
        state: [
          {
            colId: obj.id,
            hide: obj.hide,
            rowGroup: obj.rowGroup,
          },
        ],
      });
    }
  };
  handleUpdateTasksDates = data => {
    const { actualStartTime, actualDueTime, startTime, dueTime } = data.obj;

    const startDate = helper.RETURN_CUSTOMDATEFORMAT(data.start);
    const endDate = helper.RETURN_CUSTOMDATEFORMAT(data.end);
    let obj;
    let key;
    switch (this.state.calenderDateType) {
      case "Planned Start/End":
        obj = {
          startDate: startDate,
          startTime: startTime || "12:00 AM",
          dueDate: endDate,
          dueTime: dueTime || "12:00 AM",
        };
        break;
      case "Actual Start/End":
        obj = {
          actualStartDate: startDate,
          actualStartTime: actualStartTime || "12:00 AM",
          actualDueDate: endDate,
          actualDueTime: actualDueTime || "12:00 AM",
        };
        break;
      default:
    }
    this.props.updateTaskData({ task: data.obj, obj }, null, () => { });
  };
  addNewTask = ({ start, end }) => {
    if (start && end) {
      this.setState({
        startDate: start,
        endDate: new Date(moment(end).add(-1 , 'days')),
        selectableTask: true,
      });
    }
  };
  handleCloseAddNewTask = () => {
    this.setState({
      startDate: new Date(),
      endDate: new Date(),
      selectableTask: false,
    });
  };
  onDoubleClickEvent = event => {
    const { taskPer, permissionObject } = this.props;
    let taskPermission = event?.obj?.projectId
      ? isUndefined(permissionObject[event.obj.projectId])
        ? taskPer
        : permissionObject[event.obj.projectId].task
      : taskPer;

    if (event.taskOccurance) {
      let permission = taskPermission.taskDetail.repeatTaskOccurances.cando;
      permission
        ? this.setState({
          currentTaskOccuranceId: event.taskObj.taskId,
          createReccurrenceDate: event.start,
          openRecurrenceConfirm: true,
          activeTaskTitle: event.title,
        })
        : null;
    } else {
      let taskDetailsPer = taskPermission.taskDetail.cando;
      taskDetailsPer
        ? this.setState({ currentTask: event.obj, currentTaskId: event.obj.taskId }, () => {
          this.setState({
            currentTask: null,
            currentTaskId: null,
          });
        })
        : null;
    }
  };
  closeTaskDetailsPopUp = () => {
    this.setState({ currentTask: null, currentTaskId: null });
  };
  getDate = task => {
    const { calenderDateType } = this.state;
    const datesObj = {
      "Actual Start/End": {
        start: task.actualStartDate || task.actualDueDate,
        end: task.actualDueDate || task.actualStartDate,
        // end: moment(task.actualDueDate).add(1 , 'days').format("MM/DD/YYYY") || moment(task.actualStartDate).add(1 , 'days').format("MM/DD/YYYY"),
      },
      "Planned Start/End": {
        start: task.startDate || task.dueDate,
        end: task.dueDate || task.startDate,
        // end: moment(task.dueDate).add(1 , 'days').format("MM/DD/YYYY") || moment(task.startDate).add(1 , 'days').format("MM/DD/YYYY"),
      },
      "Creation Date": { start: task.createdDate, end: task.createdDate },
    };
    return datesObj[calenderDateType];
  };
  getTaskOccurances = tasks => {
    const dates = [];
    const task = tasks.filter(t => {
      return t.repeatDates ? t : null;
    });

    task.map(t => {
      t.repeatDates.map(r => {
        const obj = {
          startDate: r,
          title: t.taskTitle,
          id: t.taskId,
          taskObj: t,
        };
        dates.push(obj);
      });
    });
    const repeatOccurance = this.getOccuranceobj(dates);
    return repeatOccurance;
  };
  getOccuranceobj = dates => {
    const { classes, theme } = this.props;
    const className = classes.repeatOccuranceStyle;
    const occuranceObj = dates.map(d => {
      return {
        id: d.startDate,
        taskOccurance: true,
        title: d.title,
        allDay: false,
        start: new Date(moment(d.startDate).format("MM/DD/YYYY hh:mm:ss A")),
        end: new Date(moment(d.startDate).format("MM/DD/YYYY hh:mm:ss A")),
        taskColor: theme.palette.border.extraLightBorder,
        borderColor: "1px solid #cecece",
        color: theme.palette.text.light,
        taskObj: d.taskObj,
        borderRadius: "0px",
        className: className,
      };
    });
    return occuranceObj;
  };
  render() {
    const {
      classes,
      theme,
      appliedFilters,
      quote,
      workspaces,
      loggedInTeam,
      taskPer,
      companyInfo,
      intl,
      tasks,
      quickFilters,
    } = this.props;
    const {
      alignment,
      calendarView,
      gridView,
      openFilterSidebar,
      multipleFilters,
      showRecords,
      loadMore,
      calenderDateType,
      selectedGroup = {},
      listView,
      isLoading,
      refreshBtnQuery,
    } = this.state;
    const statusColor = theme.palette.taskStatus;
    const taskLabelSingle = companyInfo?.task?.sName;
    const taskLabelMulti = companyInfo?.task?.pName;
    const createTaskText = taskLabelMulti
      ? "Create your first " + taskLabelSingle
      : "Create your first task";
    const statusArr = [
      {
        label: intl.formatMessage({
          id: "task.common.status.dropdown.not-started",
          defaultMessage: "Not Started",
        }),
        name: "Not Started",
        color: statusColor.NotStarted,
      },
      {
        label: intl.formatMessage({
          id: "task.common.status.dropdown.in-progress",
          defaultMessage: "In Progress",
        }),
        name: "In Progress",
        color: statusColor.InProgress,
      },
      {
        label: intl.formatMessage({
          id: "task.common.status.dropdown.in-review",
          defaultMessage: "In Review",
        }),
        name: "In Review",
        color: statusColor.InReview,
      },
      {
        label: intl.formatMessage({
          id: "task.common.status.dropdown.completed",
          defaultMessage: "Completed",
        }),
        name: "Completed",
        color: statusColor.Completed,
      },
      {
        label: intl.formatMessage({
          id: "task.common.status.dropdown.cancel",
          defaultMessage: "Cancelled",
        }),
        name: "Cancelled",
        color: statusColor.Cancelled,
      },
    ];
    const other = [
      {
        label: intl.formatMessage({
          id: "common.action.other.duetoday",
          defaultMessage: "Due Today",
        }),
        name: "Due Today",
        color: theme.palette.filter.dueToday,
      },
      {
        label: intl.formatMessage({
          id: "common.action.other.overdue",
          defaultMessage: "Over Due",
        }),
        name: "Over Due",
        color: theme.palette.filter.overDue,
      },
      {
        label: intl.formatMessage({
          id: "common.action.other.started",
          defaultMessage: "Starred",
        }),
        name: "Starred",
        color: theme.palette.filter.starred,
      },
      {
        label: 'Unassigned tasks',
        name: "unassignedTasks",
        color: theme.palette.filter.starred,
      },
    ];
    const isArchived = false;
    const companyName = companyInfo ? companyInfo.companyName : "nTask";
    const activeQuickFilterKeys = Object.keys(quickFilters).length
      ? Object.keys(quickFilters)
        .toString()
        .split(",")
        .join(", ")
      : "All";

    const eventStyle = event => {
      if (event.taskColor) {
        if (event.borderColor) {
          return {
            style: {
              background: event.taskColor,
              border: event.borderColor,
              color: event.color,
              borderRadius: event.borderRadius,
            },
            className: event.className,
          };
        } else {
          return {
            style: {
              background: event.taskColor,
              borderRadius: event.borderRadius,
            },
          };
        }
      } else {
        return {};
      }
    };
    const EventRenderer = ({ event }) => {
      return (
        <>
          {event.title}
          {event.taskOccurance ? (
            <SvgIcon
              viewBox="0 0 14 12.438"
              htmlColor={"#0090ff"}
              className={classes.recurrenceIcon}>
              <RecurrenceIcon />
            </SvgIcon>
          ) : null}
        </>
      );
    };
    let tasksData = this.props.tasks.map(x => {
      return {
        id: x.taskId,
        title: x.taskTitle,
        allDay: true,
        start: this.getDate(x).start,
        end: this.getDate(x).end,
        taskColor: x.statusColor,
        obj: x,
        taskOccurance: false,
      };
    });
    const TaskOccurances = this.getTaskOccurances(this.props.tasks);
    tasksData = tasksData.concat(TaskOccurances);
    return (
      <>
        <Helmet>
          <title>TaskBoard | {companyName}</title>
        </Helmet>
        <main
          className={
            // teamCanView("advanceFilterAccess") ?
            classNames(classes.content, {
              [classes.contentShift]: openFilterSidebar,
            })
            // : classes.noFiltercontent
          }>
          {!this.props.tasks.length && !this.props.taskColumns.length ? (
            <ListLoader style={{ paddingLeft: 32 }} />
          ) : (
            <>
              {/*<div className={classes.drawerHeader}>*/}
              <div className={classes.taskDashboardCnt}>
                {/* heading container starts here */}
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                  classes={{ container: classes.taskDashboardHeader }}>
                  <div className="flex_center_start_row">
                    {isArchived ? (
                      <>
                        <LeftArrow
                          onClick={this.handleClearArchived}
                          className={classes.backArrowIcon}
                        />
                        <Typography variant="h1">
                          <FormattedMessage
                            id="common.archived.tasks.label"
                            defaultMessage={
                              taskLabelMulti ? "Archived " + taskLabelMulti : "Archived Tasks"
                            }
                          />
                        </Typography>
                      </>
                    ) : (
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                          marginRight: "16px",
                        }}>
                        <Typography variant="h1" className={classes.listViewHeading}>
                          {/* {taskLabelMulti || "Tasks"} */}
                          Tasks
                        </Typography>
                        <span className={classes.count}>
                          {this.props.tasks.filter(t => doesFilterPass({ data: t })).length}
                        </span>
                      </div>
                    )}
                    {!isArchived ? (
                      <div className={classes.toggleContainer}>
                        <ToggleButtonGroup
                          value={alignment}
                          exclusive
                          onChange={this.handleAlignment}
                          classes={{ root: classes.toggleBtnGroup }}>
                          <ToggleButton
                            value="left"
                            onClick={this.renderListView}
                            classes={{
                              root: classes.toggleButton,
                              selected: classes.toggleButtonSelected,
                            }}>
                            <FormattedMessage id="common.list.label" defaultMessage="List" />
                          </ToggleButton>
                          <ToggleButton
                            value="center"
                            onClick={this.renderGridView}
                            classes={{
                              root: classes.toggleButton,
                              selected: classes.toggleButtonSelected,
                            }}>
                            <FormattedMessage id="common.grid.label" defaultMessage="Grid" />
                          </ToggleButton>

                          <ToggleButton
                            value="right"
                            onClick={this.renderCalendarView}
                            classes={{
                              root: classes.toggleButton,
                              selected: classes.toggleButtonSelected,
                            }}>
                            <FormattedMessage
                              id="common.calendar.label"
                              defaultMessage="Calendar"
                            />
                          </ToggleButton>
                        </ToggleButtonGroup>
                      </div>
                    ) : null}
                  </div>

                  {!isArchived ? (
                    <div className="flex_center_start_row">
                      {listView && (
                        <>
                          <DefaultTextField
                            fullWidth={false}
                            // errorState={forgotEmailError}
                            error={false}
                            // errorMessage={forgotEmailMessage}
                            formControlStyles={{ width: 250, marginBottom: 0, marginRight: 10 }}
                            defaultProps={{
                              id: "taskListSearch",
                              onChange: this.handleSearch,
                              placeholder: taskLabelMulti
                                ? "Search " + taskLabelMulti
                                : "Search Tasks",
                              autoFocus: true,
                              inputProps: { maxLength: 150, style: { padding: "9px 14px" } },
                            }}
                          />
                          {/*<GroupingDropdown*/}
                          {/*  id="taskGroupingDropdown"*/}
                          {/*  view="task"*/}
                          {/*  selectedGroup={selectedGroup}*/}
                          {/*  data={this.state.groupingDropdownData}*/}
                          {/*  onGroupSelect={this.onGroupSelect}*/}
                          {/*  height={160}*/}
                          {/*/>*/}

                          {/*  <SortingDropdown /** Sorting Drop Down Values */}
                          {/*    data={this.state.sortingDropDownData}*/}
                          {/*    sortingList={this.sortingTaskList}*/}
                          {/*    getSortOrder={getSortOrder(workspaces, loggedInTeam, 2)}*/}
                          {/*    height={!teamCanView("advanceSortAccess") ? 220 : null}*/}
                          {/*    width={!teamCanView("advanceSortAccess") ? 305 : null}*/}
                          {/*  />*/}
                        </>
                      )}
                      {/* Quick Filters*/}
                      {calendarView ? null : (
                        <FormControl className={classes.formControl}>
                          <CustomButton
                            onClick={event => {
                              this.setState(
                                { refreshBtnQuery: "progress", isLoading: true },
                                () => {
                                  this.props.getTasks(
                                    null,
                                    null,
                                    () => {
                                      this.setState({ refreshBtnQuery: "", isLoading: false });
                                      this.handleExportType(
                                        "Grid Refreshed Successfully!",
                                        "success"
                                      );
                                    },
                                    () => {
                                      this.setState({ refreshBtnQuery: "", isLoading: false });
                                      this.handleExportType("Oops! Server throws Error.", "error");
                                    }
                                  );
                                }
                              );
                            }}
                            query={refreshBtnQuery}
                            style={{
                              // padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
                              padding: "4px 8px 4px 8px",
                              borderRadius: "4px",
                              display: "flex",
                              justifyContent: "space-between",
                              minWidth: "auto",
                              whiteSpace: "nowrap",
                              marginRight: 10,
                            }}
                            btnType={"white"}
                            variant="contained">
                            <SvgIcon
                              viewBox="0 0 12 12.015"
                              className={classes.qckFfilterIconRefresh}>
                              <IconRefresh />
                            </SvgIcon>
                          </CustomButton>
                          <CustomButton
                            onClick={event => {
                              this.handleClick(event, "bottom-end");
                            }}
                            id="taskQuickFilterButton"
                            buttonRef={node => {
                              this.anchorEl = node;
                            }}
                            style={{
                              // padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
                              padding: "4px 8px 4px 4px",
                              borderRadius: "4px",
                              display: "flex",
                              justifyContent: "space-between",
                              minWidth: "auto",
                              whiteSpace: "nowrap",
                            }}
                            btnType={!isEmpty(quickFilters) ? "lightBlue" : "white"}
                            variant="contained">
                            <SvgIcon
                              classes={{root: classes.quickFilterIconSize}}
                              viewBox="0 0 24 24"
                              className={
                                !isEmpty(quickFilters)
                                  ? classes.qckFfilterIconSelected
                                  : classes.qckFilterIconSvg
                              }>
                              <QuickFilterIcon />
                            </SvgIcon>
                            <span
                              className={
                                !isEmpty(quickFilters)
                                  ? classes.qckFilterLblSelected
                                  : classes.qckFilterLbl
                              }>
                              <FormattedMessage id="common.show.label" defaultMessage="Show" /> :{" "}
                            </span>
                            <span className={classes.checkname}>{activeQuickFilterKeys}</span>
                          </CustomButton>
                          <PlainMenu
                            open={this.state.open}
                            closeAction={this.handleClose}
                            placement="bottom-start"
                            anchorRef={this.anchorEl}
                            style={{ width: 280 }}
                            offset="0 15px">
                            <MenuList disablePadding>
                              <MenuItem
                                className={`${classes.statusMenuItemCnt} ${isEmpty(quickFilters) ? classes.selectedValue : ""
                                  }`}
                                classes={{
                                  root: classes.customRootMenuItem,
                                  selected: classes.statusMenuItemSelected,
                                }}
                                value="Show All"
                                onClick={this.handleShowAllTasks}>
                                <ListItemText
                                  primary={
                                    <FormattedMessage
                                      id="common.show.all"
                                      defaultMessage="Show All"
                                    />
                                  }
                                  classes={{ primary: classes.plainItemText }}
                                />
                              </MenuItem>
                              <Divider />
                              {!quickFilters["Archived"] && (
                                <>
                                  <MenuItem classes={{ root: classes.menuHeadingItem }}>
                                    <FormattedMessage
                                      id="common.action.other.label"
                                      defaultMessage="Other"
                                    />
                                  </MenuItem>
                                  {other.map(item => (
                                    <MenuItem
                                      key={item.name}
                                      value={item.name}
                                      className={`${classes.statusMenuItemCnt} ${quickFilters[item.name] ? classes.selectedValue : ""
                                        }`}
                                      classes={{
                                        root: classes.customRootMenuItem, 
                                        selected: classes.statusMenuItemSelected,
                                      }}
                                      onClick={() => this.handleSelectClick(item.name)}>
                                      <RoundIcon
                                        htmlColor={item.color}
                                        classes={{ root: classes.statusIcon }}
                                      />
                                      <ListItemText
                                        primary={item.label}
                                        classes={{ primary: classes.statusItemText }}
                                      />
                                    </MenuItem>
                                  ))}
                                </>
                              )}
                              <MenuItem
                                value={"Archived"}
                                classes={{ root: classes.highlightItem }}
                                onClick={() => this.handleSelectClick("Archived")}>
                                <ArchivedIcon
                                  htmlColor={theme.palette.secondary.light}
                                  classes={{
                                    root: classes.selectHighlightItemIcon,
                                  }}
                                />
                                <FormattedMessage
                                  id="common.archived.archived"
                                  defaultMessage="Archived"
                                />
                              </MenuItem>
                            </MenuList>
                          </PlainMenu>
                          {/* </Select> */}
                        </FormControl>
                      )}
                      {!quickFilters["Archived"] && (
                        <ImportExportDD /** hide component when user is on archived mode */
                          handleExportType={this.handleExportType}
                          filterList={quickFilters}
                          multipleFilters={multipleFilters}
                          handleRefresh={this.handleRefresh}
                          id="taskImportExport"
                          ImportExportType={"task"}
                          data={tasks}
                        />
                      )}
                      {listView && (
                        <ColumnSelectionDropdown
                          feature={"task"}
                          onColumnHide={this.onColumnHide}
                          hideColumns={["matrix"]}
                          btnProps={{
                            style: {
                              border: "1px solid #dddddd",
                              padding: "5px 10px",
                              borderRadius: 4,
                              marginLeft: 10,
                            },
                          }}
                        />
                      )}

                      {calendarView ? (
                        <PlannedVsActualDropDown
                          handleTaskbarsType={this.handleTaskbarsType}
                          calenderDateType={calenderDateType}
                        />
                      ) : null}
                    </div>
                  ) : null}
                </Grid>
                {calendarView ? (
                  <Grid container classes={{ container: classes.dashboardContentCnt }}>
                    <Grid
                      item
                      classes={{ item: classes.taskCalendarCnt }}
                      style={{ height: "calc(100vh - 160px)" }}>
                      {/* <TaskCalendar taskState={quickFilters} calenderDateType={calenderDateType} /> */}
                      <CustomCalendar
                        onDoubleClickEvent={this.onDoubleClickEvent}
                        updateDates={this.handleUpdateTasksDates}
                        data={tasksData}
                        draggable={true}
                        EventRenderer={EventRenderer}
                        eventPropGetter={eventStyle}
                        onSelectSlot={this.addNewTask}
                        draggableAccessor={event => {
                          return calenderDateType.includes("Creation Date") ? false : !event.taskOccurance;
                        }}
                      />
                    </Grid>
                  </Grid>
                ) : gridView ? (
                  <Grid container classes={{ container: classes.dashboardContentCnt }}>
                    <div
                      onScroll={this.handleScrollDown}
                      className={classes.taskGridCnt}
                      style={{ height: "calc(100vh - 160px)" }}>
                      {this.props.tasks.length > 0 ? (
                        <TaskGridItem
                          taskState={quickFilters}
                          deleteTaskFromArchiveList={this.deleteTaskFromArchiveList}
                          filteredTasks={this.props.tasks.filter(t => doesFilterPass({ data: t }))}
                          filterUnArchiveTask={this.filterUnArchiveTask}
                          isArchivedSelected={isArchived}
                          searchFilterApplied={this.searchFilterApplied}
                          returnCount={this.returnCount}
                          showRecords={showRecords}
                          handleChangeState={this.handleChangeState}
                          isChanged={this.state.isChanged}
                          resetCount={this.resetCount}
                          sortObj={this.state.sortObj}
                          showSnackBar={this.handleExportType}
                        />
                      ) : isArchived ? (
                        <EmptyState
                          screenType="Archived"
                          heading={
                            <FormattedMessage
                              id="common.archived.label"
                              defaultMessage="No archived items found"
                            />
                          }
                          message={
                            <FormattedMessage
                              id="common.archived.message"
                              defaultMessage="You haven't archived any items yet."
                            />
                          }
                          button={false}
                        />
                      ) : taskPer.createTask.cando ? (
                        <EmptyState
                          screenType="task"
                          heading={
                            <FormattedMessage
                              id="common.create-first.task.label"
                              defaultMessage={createTaskText}
                            />
                          }
                          message={
                            <FormattedMessage
                              id="common.create-first.task.messageb"
                              defaultMessage='You do not have any tasks yet. Press "Alt + T" or click on button below.'
                            />
                          }
                          button={true}
                        />
                      ) : (
                        <EmptyState
                          screenType="task"
                          heading={
                            <FormattedMessage
                              id="common.create-first.task.messagec"
                              defaultMessage="You do not have any tasks yet."
                            />
                          }
                          message={
                            <FormattedMessage
                              id="common.create-first.task.messagea"
                              defaultMessage="Request your team member(s) to assign you a task."
                            />
                          }
                          button={false}
                        />
                      )}
                    </div>
                  </Grid>
                ) : null}

                {listView && (
                  <div className={classes.taskListCnt}>
                    {/* Here is the filteredTasks */}
                    {isLoading ? <ListLoader style={{ paddingLeft: 32 }} /> :  <TaskList />}

                  </div>
                )}
              </div>
              {/*</div>*/}
            </>
          )}
        </main>
        {this.state.openRecurrenceConfirm && (
          <ActionConfirmation
            open={this.state.openRecurrenceConfirm}
            closeAction={this.handleDialogClose}
            cancelBtnText="Cancel"
            successBtnText="Yes, Create Next Occurrence"
            alignment="center"
            headingText="Create Next Occurrence"
            iconType="recurrence"
            msgText={
              <>
                This task is part of "{this.state.activeTaskTitle}" schedule and does not exist yet.
                <br />
                <br />
                Would you like to create next occurrence?
              </>
            }
            successAction={this.handleCreateReccureTask}
            btnQuery={this.state.btnQuery}
          />
        )}
        {this.state.currentTask &&
          this.props.taskDetailDialogState(null, {
            id: this.state.currentTask.taskId,
            afterCloseCallBack: () => {
              this.setState({
                currentTask: null,
                currentTaskId: null,
              });
            },
          })}
        <DefaultDialog
          title="Add Task"
          open={this.state.selectableTask}
          onClose={this.handleCloseAddNewTask}>
          <AddNewTaskForm
            closeAction={this.handleCloseAddNewTask}
            preFilledData={{
              startDate: this.state.startDate,
              endDate: this.state.endDate,
              moreDetails: true,
              calenderDateType,
            }}
          />
        </DefaultDialog>
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loggedInTeam: state.profile.data.loggedInTeam,
    members:
      state.profile && state.profile.data && state.profile.data.member
        ? state.profile.data.member.allMembers
        : [],
    tasks: state.tasks.data || [],
    workspaces: state.profile.data.workspace,
    taskPer: state.workspacePermissions.data.task,
    companyInfo: state.whiteLabelInfo.data,
    taskColumns: ColumnSelector(state).task.columns || [],
    quickFilters: state.tasks.quickFilters || {},
    taskFilters: state.tasks.taskFilter,
    taskCount: state.tasks.taskCount,
    dialogsState: state.dialogStates,
    permissionObject: ProjectPermissionSelector(state),
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(dashboardStyles, selectStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    FetchWorkspaceInfo,
    updateQuickFilter,
    assosiateRemoveTaskArchivedMethod,
    hideLoading,
    setAppliedFilters,
    saveSortingProjectList,
    getTasks,
    getColumnsList,
    taskDetailDialogState,
    removeTaskQuickFilter,
    showAllTasks,
    getArchivedData,
    createNextOccurrence,
    updateTaskData,
  })
)(TaskDashboard);
