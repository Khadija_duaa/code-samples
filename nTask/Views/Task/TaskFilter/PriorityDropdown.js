import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import selectStyles from "../../../assets/jss/components/select";
import SelectMenu from "../../../components/Menu/SelectMenu";
import FlagIcon from "../../../components/Icons/FlagIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
class PriorityDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: [],
      clear: false
    };
  }
  componentDidMount() {
    const priority = this.props.priority || [];
    if (priority.length) {
      this.setState({ name: priority });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    const priority = this.props.priority || [];
    if (JSON.stringify(priority) !== JSON.stringify(prevProps.priority)) {
      this.setState({ name: priority });
    }
    if (this.state.clear) {
      this.props.handleClearState(true);
    }
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isClear) {
      return {
        name: [],
        clear: true
      };
    } else {
      return {
        clear: false
      };
    }
  }
  handleChange = event => {
    this.setState({ name: event.target.value }, () => {
      this.props.handleFilterChange("priority", this.state.name);
    });
  }
  render() {
    const { classes, theme } = this.props;
    const { name } = this.state;
    const statusColor = theme.palette.taskPriority;
    const names = [
      { name: "Critical", color: statusColor.Critical },
      { name: "High", color: statusColor.High },
      { name: "Medium", color: statusColor.Medium },
      { name: "Low", color: statusColor.Low }
    ]
    return (
      <SelectMenu
        label="Priority"
        multiple={true}
        value={name}
        onChange={this.handleChange}
      >
        {names.map(status => (
          <MenuItem
            key={status.name}
            value={status.name}
            className={`${classes.statusMenuItemCnt} ${this.state.name.indexOf(status.name) > -1 ? classes.selectedValue : ""}`}
            classes={{
              selected: classes.statusMenuItemSelected
            }}
          >
            <SvgIcon
                    viewBox="0 0 24 32.75"
                    htmlColor={status.color}
                    classes={{ root: classes.priorityIcon }}
                  >
                    <FlagIcon />
                  </SvgIcon>
            <ListItemText
              primary={status.name}
              classes={{ primary: classes.statusItemText }}
            />


          </MenuItem>
        ))}
      </SelectMenu>
    );
  }
}

export default withStyles(selectStyles, {
  withTheme: true
})(PriorityDropdown);
