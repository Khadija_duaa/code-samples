import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import SearchInput, { createFilter } from "react-search-input";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
import CustomAvatar from "../../../components/Avatar/Avatar";
import SelectMenu from "../../../components/Menu/SelectMenu";
import selectStyles from "../../../assets/jss/components/select";
import { generateUsername } from "../../../utils/common";
import find from "lodash/find";
import { Scrollbars } from "react-custom-scrollbars";
import {FormattedMessage} from "react-intl";
let object = {};
class AssignedToDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      searchTerm: "",
      checked: [],
      checkedIds: [],
      allMembers: [],
      clear: false,
    };
    this.searchUpdated = this.searchUpdated.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }

  componentDidMount() {
    const assignees = this.props.assignees || [];
    if (assignees.length) {
      const checkedIds = assignees.concat();
      const checked = this.getCheckedItems(assignees);
      this.setState({ checked, checkedIds });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const assignees = this.props.assignees || [];
    const { allMembers } = this.props.profileState.data.member;
    if (JSON.stringify(assignees) !== JSON.stringify(prevProps.assignees)) {
      const checkedIds = assignees.concat();
      const checked = this.getCheckedItems(assignees);
      this.setState({ checked, checkedIds });
    }
    else if (JSON.stringify(allMembers) !== JSON.stringify(prevProps.profileState.data.member.allMembers)) {
      let members = allMembers ? allMembers.map(x => {
        const name = generateUsername(x.email, x.userName, x.fullName, null);
        return {
          id: x.userId,
          value: { name: name, avatar: x.imageUrl, email: x.email, fullName: x.fullName, isOwner: x.isOwner, isOnline: x.isOnline }
        }
      }) : [];
      this.setState({ allMembers: members });
    }
  }

  getCheckedItems = (assignees) => {
    let checked = [];
    for (let i = 0; i < assignees.length; i++) {
      let member = find(this.state.allMembers, { 'id': assignees[i] });
      if (member) {
        const name = generateUsername(member.value.email, member.value.name, member.value.fullName, null);
        checked.push(name);
      }
    }
    return checked;
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isClear) {
      object = {
        checked: [],
        checkedIds: [],
        clear: true
      };
    } else {
      object = {
        clear: false
      };
    }
    let allMembers = (nextProps.profileState && nextProps.profileState.data && nextProps.profileState.data.member) ? nextProps.profileState.data.member.allMembers : [];
    if (prevState.allMembers.length !== allMembers.length) {
      allMembers = allMembers.map(x => {
        const name = generateUsername(x.email, x.userName, x.fullName, null);
        return {
          id: x.userId,
          value: { name: name, avatar: x.imageUrl, email: x.email, fullName: x.fullName, isOwner: x.isOwner, isOnline: x.isOnline  }
        }
      });
      object.allMembers = allMembers;
    } else
      object.allMembers = prevState.allMembers;

    return (object);
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }

  handleToggle = (value, id) => () => {
    const { checked, checkedIds } = this.state;
    const currentIndex = id ? checkedIds.indexOf(id) : checked.indexOf(value);
    let newChecked = [...checked];
    let newIds = [...checkedIds];
    if (currentIndex === -1) {
      newChecked.push(value);
      newIds.push(id);
    } else {
      newChecked = newChecked.filter(x => x !== value);
      newIds = newIds.filter(x => x !== id);
    }
    this.props.handleFilterChange("assignees", newIds);
    this.setState({
      checked: newChecked,
      checkedIds: newIds
    });
  };

  render() {
    const { theme, classes } = this.props;
    const { checked, allMembers, clear,checkedIds } = this.state;
    const data = allMembers;
    const filteredData = data.filter(
      createFilter(this.state.searchTerm, ["id", "value.name"])
    );
    if (clear) {
      this.props.handleClearState(true);
    }
    return (
      <SelectMenu
        label={<FormattedMessage
        id="common.assigned.label"
        defaultMessage="Assigned To"
      />}
        multiple={true}
        value={checked}
      >
      <Scrollbars autoHide style={{ height: 250 }}>
        <List classes={{ root: classes.searchMenuList }}>
          <ListItem disableRipple={true}>
            <SearchInput className="HtmlInput" onChange={this.searchUpdated} />
          </ListItem>
          {filteredData.map(data => {
            return (
              <ListItem
                key={data.id}
                value={data.value.name}
                onClick={this.handleToggle(data.value.name, data.id)}
               // className={`${classes[`menuItem${iconType}`]} ${(idList && idList.length?(idList.indexOf(value.id) !== -1):(checked.indexOf(value.value.name) !== -1)) ? classes.selectedValue : ""}`}
            
                className={`${classes.AvatarMenuItem} ${(checkedIds && checkedIds.length?(checkedIds.indexOf(data.id) !== -1):(checked.indexOf(data.value.name) !== -1)) ? classes.selectedValue : ""}`}
                button
                disableRipple={true}
              >
                <CustomAvatar
                  otherMember={{
                    imageUrl: data.value.avatar,
                    fullName: data.value.fullName,
                    lastName: '',
                    email: data.value.email,
                    isOnline: data.value.isOnline,
                    isOwner: data.value.isOwner
                  }}
                  size="small"
                />
                <ListItemText
                  primary={data.value.name}
                  classes={{ primary: classes.MenuItemText }}
                />

              </ListItem>
            );
          })}
        </List>
        </Scrollbars>
      </SelectMenu>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    profileState: state.profile
  }
}


export default compose(
  withRouter,
  withStyles(selectStyles, {
    withTheme: true
  }),
  connect(
    mapStateToProps,
    {}
  )
)(AssignedToDropdown);