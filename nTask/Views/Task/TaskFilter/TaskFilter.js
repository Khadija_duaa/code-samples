import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import isEqual from "lodash/isEqual";
import moment from "moment";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";
import RangeDatePicker from "../../../components/DatePicker/RangeDatePicker";
import Grid from "@material-ui/core/Grid";
import DefaultSwitch from "../../../components/Form/Switch";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import { Scrollbars } from "react-custom-scrollbars";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import HelpIcon from "@material-ui/icons/HelpOutline";
import BackArrow from "@material-ui/icons/KeyboardArrowLeft";
import IconButton from "../../../components/Buttons/IconButton";
import ReactResizeDetector from "react-resize-detector";
import DefaultTextField from "../../../components/Form/TextField";
import SvgIcon from "@material-ui/core/SvgIcon";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import PushPin from "../../../components/Icons/PushPin";
import CustomButton from "../../../components/Buttons/CustomButton";
import { setAppliedFilters } from "../../../redux/actions/appliedFilters";
import { validateGenericField } from "../../../utils/formValidations";
import CreateableSelectDropdown from "../../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateSelectData } from "../../../helper/generateSelectData";
import { generateAssigneeData } from "../../../helper/generateSelectData";

import cloneDeep from "lodash/cloneDeep";
import RoundIcon from "@material-ui/icons/Brightness1";
import FlagIcon from "../../../components/Icons/FlagIcon";
import { statusData, priorityData } from "../../../helper/taskDropdownData";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import {
  calculateFilterContentHeight,
  calculateHeaderHeight,
} from "../../../utils/common";
import UnPlanned from "../../billing/UnPlanned/UnPlanned";
import { FormattedMessage, injectIntl } from "react-intl";
import CircularIcon from "@material-ui/icons/Brightness1";
import { updateTaskFilter } from "../../../redux/actions/tasks";
import {TRIALPERIOD} from '../../../components/constants/planConstant';

const drawerWidth = 350;

const styles = (theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: !teamCanView("advanceFilterAccess") ? 390 : drawerWidth,
    padding: "43px 0 0 0",
    background: theme.palette.common.white,
    position: "fixed",
    display: "flex",
    height: "100%",
    zIndex: 100,
    flexDirection: "row",
    justifyContent: "stretch",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    overflowX: "hidden",
    borderTop: 0,
    borderBottom: 0,
  },
  drawerHeader: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: "100%",
  },
  filterFormCnt: {
    padding: "10px 20px 40px 20px",
  },
  switchCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: 12,
  },
  switchLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    margin: 0,
    marginRight: 30,
    fontWeight: theme.typography.fontWeightRegular,
    display: "flex",
  },
  filterBottomBtns: {
    padding: "10px 20px",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    marginTop: 40,
    position: "fixed",
    bottom: 183,
    background: theme.palette.common.white,
    zIndex: 1,
  },
  filterItem: {
    // marginTop: 20
  },
  filterCntHeader: {
    background: "#404040",
    padding: "12px 15px",
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  filterHeaderClearBtn: {
    textDecoration: "underline",
    color: theme.palette.common.white,
    fontSize: "12px !important",
    cursor: "pointer",
  },
  filterHeaderTextCnt: {
    display: "flex",
    alignItems: "center",
  },
  filtersHeaderText: {
    marginLeft: 10,
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "16px !important",
  },
  filtersHeaderCloseIcon: {
    fontSize: "11px !important",
    color: "#404040",
  },
  pushPinIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  filtersHeaderHelpIcon: {
    fontSize: "14px !important",
    marginLeft: 5,
  },
  statusIcon: {
    fontSize: "11px !important",
    marginRight: 5,
  },
  priorityIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  unplannedMain: {
    padding: 15,
    display: "flex",
    alignItems: "center",
    height: "100%",
  },
});
class TaskFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterCntHeight: `${calculateFilterContentHeight()}px`,
      saveFilter: false,
      loggedInTeam:
        props.profileState && props.profileState.data && props.profileState.data.loggedInTeam
          ? props.profileState.data.loggedInTeam
          : "",
      isDisable: true,
      isSaveDisable: true,
      filterId: ``,
      recurrence: false,
      filterName: "",
      defaultFilter: false,
      assignees: [],
      status: [],
      saveBtnQuery: false,
      priority: [],
      project: [],
      actualStart: { endDate: "", startDate: "" },
      plannedStart: { endDate: "", startDate: "" },
      plannedEnd: { endDate: "", startDate: "" },
      actualEnd: { endDate: "", startDate: "" },
      showSaveFilterPlan: false,
      statusDropDownData: this.generateStatusData(),
    };
    this.initialState = this.state;

    this.handleSwitch = this.handleSwitch.bind(this);
    this.onResize = this.onResize.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleSaveFilter = this.handleSaveFilter.bind(this);
    this.handleBackFilter = this.handleBackFilter.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.handleClearState = this.handleClearState.bind(this);
    this.handleSearchFilter = this.handleSearchFilter.bind(this);
  }
  componentDidMount() {
    // For handling default task filter
    const { Task } = this.props.appliedFiltersState;
    if (Task) {
      this.setState({
        ...this.initialState,
        ...this.transformFiltersBackToString(Task),
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { Task } = this.props.appliedFiltersState;
    const { loggedInTeam } = this.state;
    if (!isEqual(prevProps.appliedFiltersState.Task, Task)) {
      if (prevProps.appliedFiltersState.Task && Task === null)
        this.setState({
          ...this.initialState,
          ...this.transformFiltersBackToString(Task),
        });
      else
        this.setState({
          ...this.initialState,
          isSaveDisable: false,
          ...this.transformFiltersBackToString(Task),
        });
    }
    if (loggedInTeam !== this.props.profileState.data.loggedInTeam) {
      this.setState({
        loggedInTeam: this.props.profileState.data.loggedInTeam,
      });
    }
  }

  generateStatusData = () => {
    const { workspaceStatus, projects } = this.props;
    let statusArr = workspaceStatus.statusList;
    let filteredProjects = projects.filter(p => p.projectTemplateId !== workspaceStatus.templateId);
    filteredProjects.map(ele => {
      statusArr = statusArr.concat(ele.projectTemplate.statusList);
    });

    let data = this.generateStatus(statusArr);
    return this.removeDuplicateStatus(data, "label");
  };

  generateStatus = statusArr => {
    if (statusArr) {
      return statusArr.map(item => {
        return {
          label: item.statusTitle,
          value: item.statusTitle,
          icon: (
            <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "12px !important", marginRight: 5 }} />
          ),
          statusColor: item.statusColor,
          statusId: item.statusId
        };
      });
    } else return [];
  };

  handleSearchFilter() {
    const {
      filterCntHeight,
      saveFilter,
      isDisable,
      isSaveDisable,
      loggedInTeam,
      ...rest
    } = this.state;
    // let tasksStatus = ["Not Started", "In Progress", "In Review", "Completed", "Cancelled"];
    let tasksStatus = this.generateStatusData().map(ele => ele.label);
    let prioritiesStatus = ["Critical", "High", "Medium", "Low"];
    let taskObj = cloneDeep(rest);

    if (taskObj.project.length) {
      taskObj.project = taskObj.project.map(p => {
        return p.id;
      });
    }
    if (taskObj.status.length) {
      taskObj.status = taskObj.status.map(p => {
        return tasksStatus.indexOf(p.value) == -1 ? "" : p.label;
      });
    }
    if (taskObj.priority.length) {
      taskObj.priority = taskObj.priority.map(p => {
        return prioritiesStatus.indexOf(p.value);
      });
    }

    if (taskObj.assignees.length) {
      taskObj.assignees = taskObj.assignees.map(a => {
        return a.id;
      });
    }

    this.setState(
      {
        isDisable: true,
      },
      () => {
        this.props.setAppliedFilters(taskObj, "Task");
      }
    );
  }
  handleClearState(isCleared) {
    if (isCleared) {
    } else {
      this.setState(this.initialState, () => {
        this.props.setAppliedFilters(null, "Task");
      });
    }
  }
  handleFilterChange = (options) => {
    const obj = { project: { query: '', selectedValues: options } };
    this.props.updateTaskFilter(obj);
  };

  onProjectFilterSelect = (key, options) => {
    this.handleFilterChange(options.map(p => p.id));
  };
  // handleFilterChange(key, value) {
    // this.setState(
    //   {
    //     [key]: key === "recurrence" ? !value : value,
    //     isDisable: false,
    //     isSaveDisable: false,
    //   },
    //   () => {
    //     const {
    //       filterCntHeight,
    //       saveFilter,
    //       loggedInTeam,
    //       isDisable,
    //       isSaveDisable,
    //       filterName,
    //       recurrence,
    //       defaultFilter,
    //       ...rest
    //     } = this.state;
    //     const changedStateData = { ...this.initialState, ...rest };
    //     if (isEqual(changedStateData, this.initialState)) {
    //       this.handleClearState(false);
    //       this.setState({ isDisable: true, isSaveDisable: true });
    //     }
    //     if (key == "project") {
    //       if (value.length > 0) {
    //         let status = [];
    //         value.map(e => {
    //           status = status.concat(e.obj.projectTemplate.statusList);
    //         });
    //         let statuses = this.generateStatus(status);
    //         this.setState({ statusDropDownData: statuses, status: [] });
    //       } else if (value.length == 0) {
    //         this.setState({ Data: this.generateStatusData(), status: [] });
    //       }
    //     }
    //   }
    // );
  // }
  handleRemoveChange = obj => {
    this.setState(
      {
        status: this.state.status.filter(s => s.value !== obj.value),
        isDisable: false,
        isSaveDisable: false,
      },
      () => {
        const {
          filterCntHeight,
          saveFilter,
          loggedInTeam,
          isDisable,
          isSaveDisable,
          filterName,
          recurrence,
          defaultFilter,
          ...rest
        } = this.state;
        const changedStateData = { ...this.initialState, ...rest };
        if (isEqual(changedStateData, this.initialState)) {
          this.handleClearState(false);
          this.setState({ isDisable: true, isSaveDisable: true });
        }
      }
    );
  };
  handleSaveFilter() {
    if (this.state.saveFilter) {
      let filterName = this.state.filterName;
      let validationObj = validateGenericField("filter name", filterName, true, 40);
      if (validationObj["isError"]) {
        this.setState({
          isSaveDisable: true,
          filterNameError: validationObj["isError"],
          filterNameErrorMessage: validationObj["message"],
        });
        return;
      }
      const {
        filterCntHeight,
        saveFilter,
        isSaveDisable,
        isDisable,
        loggedInTeam,
        ...rest
      } = this.state;
      let itemOrder = this.props.itemOrderState.data;
      itemOrder.taskFilter = this.transformFilters(cloneDeep(rest));
      this.setState({ saveBtnQuery: "progress" });
      this.props.SaveItemOrder(
        itemOrder,
        resp => {
          this.setState({ saveBtnQuery: "" });

          this.props.closeAction();
          if (!this.props.appliedFiltersState.Task) {
            this.setState(this.initialState);
          } else {
            if (rest.filterId) this.setState({ saveFilter: false, isSaveDisable: true });
            else {
              const filter =
                resp.data.userTaskFilter[0].taskFilter[
                  resp.data.userTaskFilter[0].taskFilter.length - 1
                ];
              this.props.setAppliedFilters(filter, "Task");
            }
          }
        },
        error => {
          if (error.status === 409) {
            this.setState({
              saveBtnQuery: "",
              filterNameError: true,
              filterNameErrorMessage: "Same filter name already exists",
            });
          } else {
            this.setState({ saveFilter: false });
          }
        }
      );
    } else if (teamCanView("advanceFilterAccess") && !teamCanView("saveCustomFilter")) {
      this.setState({ showSaveFilterPlan: true });
    } else this.setState({ saveFilter: true });
  }
  transformFilters = filter => {
    // let status = {
    //   "Not Started": 0,
    //   "In Progress": 1,
    //   "In Review": 2,
    //   Completed: 3,
    //   Cancelled: 4,
    // };
    let priorities = { Critical: 0, High: 1, Medium: 2, Low: 3 };
    // [{value: "Completed", label: "Completed"}]
    // ["Completed", "Completed"]

    if (filter.status && filter.status.length)
      filter.status = filter.status.map((item, index) => {
        return item.statusId;
      });
    if (filter.priority && filter.priority.length)
      filter.priority = filter.priority.map((item, index) => {
        return priorities[item.value];
      });
    filter.assignees = this.state.assignees.map(o => o.id);
    filter.project = this.state.project.map(t => t.id);
    return filter;
  };
  removeDuplicateStatus = (arr, key) => {
    return [...new Map(arr.map(item => [item[key], item])).values()];
  };
  transformFiltersBackToString = taskFilter => {
    const { theme, classes, members } = this.props;
    const { statusDropDownData } = this.state;
    let filter = cloneDeep(taskFilter);
    if (filter) {
      let statusObj = {
        "Not Started": 0,
        "In Progress": 1,
        "In Review": 2,
        Completed: 3,
        Cancelled: 4,
      };
      //let priorities = { 0: "Critical", 1: "High", 2: "Medium", 3: "Low" };

      let priorities = {
        Critical: 0,
        High: 1,
        Medium: 2,
        Low: 3,
      };

      if (filter.status && filter.status.length && typeof filter.status[0] === "string") {
        filter.status = statusDropDownData.filter(s => filter.status.includes(s.label));
        filter.status = this.removeDuplicateStatus(filter.status, "label");
        // filter.status = statusData(theme, classes).filter(s => {
        //   return filter.status.indexOf(statusObj[s.value]) > -1;
        // });
      }
      if (filter.status && filter.status.length && typeof filter.status[0] === "number") {
        filter.status = statusDropDownData.filter(s => filter.status.includes(s.statusId));
        filter.status = this.removeDuplicateStatus(filter.status, "label");
      }
      if (filter.priority && filter.priority.length && typeof filter.priority[0] === "number")
        // filter.priority = filter.priority.map((item, index) => {
        //   return priorities[item];
        // });
        filter.priority = priorityData(theme, classes).filter(p => {
          return filter.priority.indexOf(priorities[p.value]) > -1;
        });

      if (
        filter.actualStart &&
        filter.actualStart.startDate &&
        typeof filter.actualStart.startDate === "string"
      )
        filter.actualStart.startDate = moment(filter.actualStart.startDate);
      if (
        filter.actualStart &&
        filter.actualStart.endDate &&
        typeof filter.actualStart.endDate === "string"
      )
        filter.actualStart.endDate = moment(filter.actualStart.endDate);

      if (
        filter.actualEnd &&
        filter.actualEnd.startDate &&
        typeof filter.actualEnd.startDate === "string"
      )
        filter.actualEnd.startDate = moment(filter.actualEnd.startDate);
      if (
        filter.actualEnd &&
        filter.actualEnd.endDate &&
        typeof filter.actualEnd.endDate === "string"
      )
        filter.actualEnd.endDate = moment(filter.actualEnd.endDate);

      if (
        filter.plannedStart &&
        filter.plannedStart.startDate &&
        typeof filter.plannedStart.startDate === "string"
      )
        filter.plannedStart.startDate = moment(filter.plannedStart.startDate);
      if (
        filter.plannedStart &&
        filter.plannedStart.endDate &&
        typeof filter.plannedStart.endDate === "string"
      )
        filter.plannedStart.endDate = moment(filter.plannedStart.endDate);

      if (
        filter.plannedEnd &&
        filter.plannedEnd.startDate &&
        typeof filter.plannedEnd.startDate === "string"
      )
        filter.plannedEnd.startDate = moment(filter.plannedEnd.startDate);
      if (
        filter.plannedEnd &&
        filter.plannedEnd.endDate &&
        typeof filter.plannedEnd.endDate === "string"
      )
        filter.plannedEnd.endDate = moment(filter.plannedEnd.endDate);

      if (filter.assignees && filter.assignees.length) {
        const assigneesData = generateAssigneeData(members);
        filter.assignees = assigneesData.filter(ass => {
          return filter.assignees.indexOf(ass.id) > -1;
        });
      }

      if (filter.project && filter.project.length) {
        const projectData = this.generateProjectData();
        filter.project = projectData.filter(ass => {
          return filter.project.indexOf(ass.id) > -1;
        });
      }

      return filter;
    }
    return {};
  };

  handleBackFilter() {
    this.setState({
      saveFilter: false,
      filterNameError: false,
      filterNameErrorMessage: "",
      filterName: "",
      showSaveFilterPlan: false,
    });
  }

  onResize() {
    this.setState({ filterCntHeight: `${calculateFilterContentHeight()}px` });
  }
  onSelectChange = (key, value) => {
    this.setState({ [key]: value });
  };
  handleInput(event, name) {
    this.setState({
      filterName: event.target.value,
      filterNameError: false,
      filterNameErrorMessage: ``,
      isSaveDisable: false,
    });
  }
  handleSwitch(name) {
    this.setState(prevState => ({
      [name]: !prevState[name],
    }));
  }
  handleClickAway = () => {
    const isDatePickerDialogueOpen = document.querySelectorAll('div[class^="RangeDatePicker"]')
      .length;
    const isDropdownMenuOpen = document.getElementById("menu-") !== null;

    if (!isDropdownMenuOpen && !isDatePickerDialogueOpen) this.props.closeAction();
  };
  //Generating react select dropdown readable object
  generateProjectData = () => {
    const { projects } = this.props;
    let ddData = projects.map((project, i) => {
      return generateSelectData(
        project.projectName,
        project.projectName,
        project.projectId,
        project.uniqueId,
        project
      );
    });
    return ddData;
  };

  render() {
    const { classes, theme, open, closeAction, promoBar, notificationBar, members } = this.props;
    const {
      recurrence,
      filterCntHeight,
      saveFilter,
      filterName,
      filterNameError,
      filterNameErrorMessage,
      isDisable,
      isSaveDisable,
      defaultFilter,
      assignees,
      status,
      priority,
      project,
      actualStart,
      plannedStart,
      actualEnd,
      plannedEnd,
      saveBtnQuery,
      showSaveFilterPlan,
      statusDropDownData,
    } = this.state;
    const statusColor = theme.palette.taskStatus;
    const priorityColor = theme.palette.taskPriority;

    const statusData = () => {
      return [
        {
          label: this.props.intl.formatMessage({
            id: "task.common.status.dropdown.not-started",
            defaultMessage: "Not Started",
          }),
          value: "Not Started",
          icon: <RoundIcon htmlColor={statusColor.NotStarted} className={classes.statusIcon} />,
        },
        {
          label: this.props.intl.formatMessage({
            id: "task.common.status.dropdown.in-progress",
            defaultMessage: "In Progress",
          }),
          value: "In Progress",
          icon: <RoundIcon htmlColor={statusColor.InProgress} className={classes.statusIcon} />,
        },
        {
          label: this.props.intl.formatMessage({
            id: "task.common.status.dropdown.in-review",
            defaultMessage: "In Review",
          }),
          value: "In Review",
          icon: <RoundIcon htmlColor={statusColor.InReview} className={classes.statusIcon} />,
        },
        {
          label: this.props.intl.formatMessage({
            id: "task.common.status.dropdown.completed",
            defaultMessage: "Completed",
          }),
          value: "Completed",
          icon: <RoundIcon htmlColor={statusColor.Completed} className={classes.statusIcon} />,
        },
        {
          label: this.props.intl.formatMessage({
            id: "task.common.status.dropdown.cancel",
            defaultMessage: "Cancelled",
          }),
          value: "Cancelled",
          icon: (
            <RoundIcon htmlColor={statusColor.Cancelled} classes={{ root: classes.statusIcon }} />
          ),
        },
      ];
    };
    const priorityData = () => {
      return [
        {
          label: this.props.intl.formatMessage({
            id: "task.common.priority.dropdown.critical",
            defaultMessage: "Critical",
          }),
          value: "Critical",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.Critical}
              className={classes.priorityIcon}>
              <FlagIcon />{" "}
            </SvgIcon>
          ),
        },
        {
          label: this.props.intl.formatMessage({
            id: "task.common.priority.dropdown.high",
            defaultMessage: "High",
          }),
          value: "High",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.High}
              className={classes.priorityIcon}>
              <FlagIcon />{" "}
            </SvgIcon>
          ),
        },
        {
          label: this.props.intl.formatMessage({
            id: "task.common.priority.dropdown.medium",
            defaultMessage: "Medium",
          }),
          value: "Medium",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.Medium}
              className={classes.priorityIcon}>
              <FlagIcon />{" "}
            </SvgIcon>
          ),
        },
        {
          label: this.props.intl.formatMessage({
            id: "task.common.priority.dropdown.low",
            defaultMessage: "Low",
          }),
          value: "Low",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.Low}
              className={classes.priorityIcon}>
              <FlagIcon />{" "}
            </SvgIcon>
          ),
        },
      ];
    };
    return (
      <ClickAwayListener onClickAway={this.handleClickAway}>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="right"
          open={open}
          PaperProps={{ style: { top: calculateHeaderHeight() } }}
          classes={{
            paper: classes.drawerPaper,
          }}>
          <ReactResizeDetector handleWidth handleHeight onResize={this.onResize} />
          <Scrollbars autoHide style={{ width: 415, height: filterCntHeight }}>
            <div className={classes.drawerHeader}>
              <div className={classes.filterCntHeader}>
                <div className={classes.filterHeaderTextCnt}>
                  <IconButton
                    btnType="grayBg"
                    onClick={saveFilter || showSaveFilterPlan ? this.handleBackFilter : closeAction}
                    style={{ padding: 4 }}>
                    {saveFilter || showSaveFilterPlan ? (
                      <BackArrow
                        classes={{ root: classes.filtersHeaderCloseIcon }}
                        htmlColor={theme.palette.secondary.medDark}
                      />
                    ) : (
                      <CloseIcon
                        classes={{ root: classes.filtersHeaderCloseIcon }}
                        htmlColor={theme.palette.secondary.medDark}
                      />
                    )}
                  </IconButton>
                  <Typography variant="h3" classes={{ h3: classes.filtersHeaderText }}>
                    {saveFilter ? (
                      <FormattedMessage id="filters.save.label" defaultMessage="Save Filter" />
                    ) : (
                      <FormattedMessage
                        id="filters.custom-filter.label"
                        defaultMessage="Custom Filter"
                      />
                    )}
                  </Typography>
                </div>
                {!saveFilter && teamCanView("advanceFilterAccess") && !showSaveFilterPlan ? (
                  <span
                    className={classes.filterHeaderClearBtn}
                    onClick={this.handleClearState.bind(this, false)}>
                    <FormattedMessage id="common.action.Clear.label" defaultMessage="Clear" />
                  </span>
                ) : null}
              </div>
              {saveFilter ? (
                <Grid container classes={{ container: classes.filterFormCnt }}>
                  <Grid item classes={{ item: classes.filterItem }} style={{ marginBottom: 0 }}>
                    <DefaultTextField
                      label={
                        <FormattedMessage id="filters.form.name" defaultMessage="Filter Name" />
                      }
                      formControlStyles={{ marginBottom: 0 }}
                      fullWidth={true}
                      errorState={filterNameError}
                      errorMessage={filterNameErrorMessage}
                      defaultProps={{
                        id: "filterName",
                        placeholder: this.props.intl.formatMessage({
                          id: "filters.form.placeholder",
                          defaultMessage: "Enter Filter Name",
                        }),
                        onChange: event => this.handleInput(event, "filterName"),
                        value: filterName,
                        inputProps: { maxLength: 40 },
                      }}
                    />
                  </Grid>
                  <Grid item classes={{ item: classes.filterItem }} style={{ marginTop: 0 }}>
                    <div className={classes.switchCnt} style={{ marginLeft: 0 }}>
                      <p className={classes.switchLabel}>
                        <SvgIcon
                          viewBox="0 0 8 11.562"
                          className={classes.pushPinIcon}
                          htmlColor={theme.palette.secondary.light}>
                          <PushPin />
                        </SvgIcon>
                        <FormattedMessage
                          id="filters.form.message"
                          defaultMessage="Make this filter my default view"
                        />

                        <HelpIcon
                          classes={{ root: classes.filtersHeaderHelpIcon }}
                          htmlColor={theme.palette.secondary.medDark}
                          fontSize="small"
                        />
                      </p>
                      <DefaultSwitch
                        checked={defaultFilter}
                        onChange={event => {
                          this.handleSwitch("defaultFilter", event);
                        }}
                      />
                    </div>
                  </Grid>
                </Grid>
              ) : !teamCanView("advanceFilterAccess") ? (
                <div className={classes.unplannedMain}>
                  <UnPlanned
                    feature="premium"
                    titleTxt={
                      <FormattedMessage
                        id="common.discovered-dialog.premium-title"
                        defaultMessage="Wow! You've discovered a Premium feature!"
                      />
                    }
                    boldText={this.props.intl.formatMessage({
                      id: "common.discovered-dialog.list.custom-filter.title",
                      defaultMessage: "Custom Filters",
                    })}
                    descriptionTxt={
                      <FormattedMessage
                        id="common.discovered-dialog.list.custom-filter.label"
                        defaultMessage={"is available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."}
                        values={{TRIALPERIOD: TRIALPERIOD}}
                      />
                    }
                    showBodyImg={false}
                    showDescription={true}
                  />
                </div>
              ) : showSaveFilterPlan ? (
                <div className={classes.unplannedMain}>
                  <UnPlanned
                    feature="business"
                    titleTxt={
                      <FormattedMessage
                        id="common.discovered-dialog.business-title"
                        defaultMessage="Wow! You've discovered a Business feature!"
                      />
                    }
                    boldText={this.props.intl.formatMessage({
                      id: "common.discovered-dialog.list.custom-filter.save",
                      defaultMessage: "Save Custom Filters",
                    })}
                    descriptionTxt={
                      <FormattedMessage
                        id="common.discovered-dialog.list.risk.label"
                        defaultMessage={"is available on our Business Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Business features."}
                        values={{TRIALPERIOD: TRIALPERIOD}}
                      />
                    }
                    showBodyImg={false}
                    showDescription={true}
                  />
                </div>
              ) : (
                <Grid container classes={{ container: classes.filterFormCnt }}>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={this.generateProjectData}
                      label={<FormattedMessage id="project.project" defaultMessage="Project" />}
                      selectChange={this.onProjectFilterSelect}
                      type="project"
                      selectedValue={project}
                      placeholder={
                        <FormattedMessage
                          id="task.creation-dialog.form.project.placeholder"
                          defaultMessage="Select Project"
                        />
                      }
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={() => generateAssigneeData(members)}
                      label={
                        <FormattedMessage id="common.assigned.label" defaultMessage="Assigned To" />
                      }
                      selectChange={this.handleFilterChange}
                      type="assignees"
                      selectedValue={assignees}
                      placeholder={
                        <FormattedMessage
                          id="risk.creation-dialog.form.assign-to.placeholder"
                          defaultMessage="Select Assignee"
                        />
                      }
                      avatar={true}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    {/* <SelectSearchDropdown
                      data={statusData}
                      label={
                        <FormattedMessage
                          id="common.bulk-action.status"
                          defaultMessage="Select Status"
                        />
                      }
                      selectChange={this.handleFilterChange}
                      type="status"
                      selectedValue={status}
                      placeholder={
                        <FormattedMessage
                          id="common.bulk-action.status"
                          defaultMessage="Select Status"
                        />
                      }
                      icon={true}
                    /> */}
                    <CreateableSelectDropdown
                      data={statusDropDownData}
                      // data={() => {
                      //   return statusData();
                      // }}
                      label={
                        <FormattedMessage
                          id="common.bulk-action.status"
                          defaultMessage="Select Status"
                        />
                      }
                      styles={{ marginRight: 20, flex: 1 }}
                      selectOptionAction={this.handleFilterChange}
                      removeOptionAction={this.handleRemoveChange}
                      type="status"
                      selectedValue={status}
                      placeholder={
                        <FormattedMessage
                          id="common.bulk-action.status"
                          defaultMessage="Select Status"
                        />
                      }
                      icon={true}
                      isMulti={true}
                      acceptDataInFormOfFun={false}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={priorityData}
                      label={
                        <FormattedMessage
                          id="task.detail-dialog.priority.label"
                          defaultMessage="Priority"
                        />
                      }
                      selectChange={this.handleFilterChange}
                      type="priority"
                      selectedValue={priority}
                      placeholder={
                        <FormattedMessage
                          id="issue.common.priority.placeholder"
                          defaultMessage="Select Priority"
                        />
                      }
                      icon={true}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <RangeDatePicker
                      label={
                        <FormattedMessage
                          id="common.date.startActual"
                          defaultMessage="Start Date (Actual)"
                        />
                      }
                      placeholder={this.props.intl.formatMessage({
                        id: "common.action.select.label",
                        defaultMessage: "Select",
                      })}
                      handleFilterChange={this.handleFilterChange}
                      handleClearState={this.handleClearState}
                      type="actualStart"
                      selectedDate={actualStart}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <RangeDatePicker
                      label={
                        <FormattedMessage
                          id="common.date.startPlanned"
                          defaultMessage="Start Date (Planned)"
                        />
                      }
                      placeholder={this.props.intl.formatMessage({
                        id: "common.action.select.label",
                        defaultMessage: "Select",
                      })}
                      handleFilterChange={this.handleFilterChange}
                      handleClearState={this.handleClearState}
                      type="plannedStart"
                      selectedDate={plannedStart}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <RangeDatePicker
                      label={
                        <FormattedMessage
                          id="common.date.dueActual"
                          defaultMessage="Due Date (Actual)"
                        />
                      }
                      placeholder={this.props.intl.formatMessage({
                        id: "common.action.select.label",
                        defaultMessage: "Select",
                      })}
                      handleFilterChange={this.handleFilterChange}
                      handleClearState={this.handleClearState}
                      type="actualEnd"
                      selectedDate={actualEnd}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <RangeDatePicker
                      label={
                        <FormattedMessage
                          id="common.date.duePlanned"
                          defaultMessage="Due Date (Planned)"
                        />
                      }
                      placeholder={this.props.intl.formatMessage({
                        id: "common.action.select.label",
                        defaultMessage: "Select",
                      })}
                      handleFilterChange={this.handleFilterChange}
                      handleClearState={this.handleClearState}
                      type="plannedEnd"
                      selectedDate={plannedEnd}
                    />
                  </Grid>
                  {/* <Grid item xs={12}>
                    <div className={classes.switchCnt}>
                      <p className={classes.switchLabel}>recurrence</p>
                      <DefaultSwitch
                        checked={recurrence}
                        onChange={event => {
                          this.handleFilterChange("recurrence", recurrence);
                        }}
                      />
                    </div>
                  </Grid> */}
                </Grid>
              )}
              {teamCanView("advanceFilterAccess") && !showSaveFilterPlan && (
                <Grid
                  container
                  direction="row"
                  justify="flex-end"
                  alignItems="center"
                  classes={{ container: classes.filterBottomBtns }}
                  style={{ bottom: promoBar || notificationBar ? 185 : 140 }}>
                  {saveFilter ? (
                    <CustomButton
                      style={{ flex: 1, marginRight: 20 }}
                      btnType="white"
                      variant="contained"
                      onClick={this.handleBackFilter}>
                      <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
                    </CustomButton>
                  ) : null}

                  <CustomButton
                    style={{ flex: 1 }}
                    btnType="success"
                    variant="contained"
                    onClick={this.handleSaveFilter}
                    disabled={saveFilter ? saveBtnQuery == "progress" : isSaveDisable}
                    query={saveFilter ? saveBtnQuery : null}>
                    {saveFilter ? (
                      <FormattedMessage id="filters.save.label" defaultMessage="Save Filter" />
                    ) : (
                      <FormattedMessage
                        id="filters.custom-filter.label"
                        defaultMessage="Custom Filter"
                      />
                    )}
                  </CustomButton>

                  {!saveFilter ? (
                    <DefaultButton
                      text={<FormattedMessage id="common.search.label" defaultMessage="Search" />}
                      buttonType="Plain"
                      style={{
                        flex: 1,
                        marginLeft: 20,
                      }}
                      onClick={this.handleSearchFilter}
                      disabled={isDisable}
                    />
                  ) : null}
                </Grid>
              )}
            </div>
          </Scrollbars>
        </Drawer>
      </ClickAwayListener>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    members: state.profile.data ? state.profile.data.member.allMembers : [],
    appliedFiltersState: state.appliedFilters,
    itemOrderState: state.itemOrder,
    tasks: state.tasks.data,
    projects: state.projects.data,
    promoBar: state.promoBar.promoBar,
    notificationBar: state.notificationBar.notificationBar,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(styles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    SaveItemOrder,
    setAppliedFilters,
    updateTaskFilter
  })
)(TaskFilter);
