import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import RoundIcon from "@material-ui/icons/Brightness1";
import selectStyles from "../../../assets/jss/components/select";
import SelectMenu from "../../../components/Menu/SelectMenu";
import {injectIntl} from "react-intl";
class StatusDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: [],
      clear: false
    };
  }

  componentDidMount() {
    const status = this.props.status || [];
    if (status.length) {
      this.setState({ name: status });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const status = this.props.status || [];
    if (JSON.stringify(status) !== JSON.stringify(prevProps.status)) {
      this.setState({ name: status });
    }
    if (this.state.clear) {
      this.props.handleClearState(true);
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isClear) {
      return {
        name: [],
        clear: true
      };
    } else {
      return {
        clear: false
      };
    }
  }

  handleChange = event => {
    this.setState({ name: event.target.value }, () => {
      this.props.handleFilterChange("status", this.state.name);
    });
  };
  render() {
    const { classes, theme } = this.props;
    const { name } = this.state;
    const statusColor = theme.palette.taskStatus;
    const names = [
      { label: intl.formatMessage({id:"task.common.status.dropdown.not-started", defaultMessage:"Not Started"}),name: "Not Started", color: statusColor.NotStarted },
      { label: intl.formatMessage({id:"task.common.status.dropdown.in-progress", defaultMessage:"In Progress"}),name: "In Progress", color: statusColor.InProgress },
      { label: intl.formatMessage({id:"task.common.status.dropdown.in-review", defaultMessage:"In Review"}),name:"In Review", color: statusColor.InReview },
      { label: intl.formatMessage({id:"task.common.status.dropdown.completed", defaultMessage:"Completed"}),name:"Completed", color: statusColor.Completed },
      { label: intl.formatMessage({id:"task.common.status.dropdown.cancel", defaultMessage:"Cancelled"}),name:"Cancelled", color: statusColor.Cancelled }
    ];

    return (
      <SelectMenu
        label="Status"
        multiple={true}
        value={name}
        onChange={this.handleChange}
      >
        {names.map(status => (
          <MenuItem
            key={status.name}
            value={status.name}
            className={`${classes.statusMenuItemCnt} ${this.state.name.indexOf(status.name) > -1 ? classes.selectedValue : ""}`}
            classes={{
              selected: classes.statusMenuItemSelected
            }}
          >
            <RoundIcon
              htmlColor={status.color}
              classes={{ root: classes.statusIcon }}
            />
            <ListItemText
              primary={status.label}
              classes={{ primary: classes.statusItemText }}
            />
          </MenuItem>
        ))}
      </SelectMenu>
    );
  }
}

export default injectIntl(withStyles(selectStyles, {
  withTheme: true
}))(StatusDropdown);
