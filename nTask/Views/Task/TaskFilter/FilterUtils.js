import helper from "../../../helper";
import { generateUsername } from "../../../utils/common";
import { getTaskProjectName } from "../../../helper/getTaskProjectName";
import intersectionWith from "lodash/intersectionWith";

export function getFilteredTasks(
  TaskArray,
  multipleFilters,
  taskState,
  issues,
  risks,
  meetings,
  members,
  projects,
  workspaceStatus
) {
  // Creating copy of TaskArray
  let tasks = JSON.parse(JSON.stringify(TaskArray)) || [];
  // let tasksState = ["Not Started", "In Progress", "In Review", "Completed", "Cancelled"];
  let tasksState = [];

  let statusArr = workspaceStatus.statusList;
  let filteredProjects = projects.filter(p => p.projectTemplateId !== workspaceStatus.templateId);
    filteredProjects.map(ele => {
      statusArr = statusArr.concat(ele.projectTemplate.statusList);
    });
    tasksState = statusArr.map(ele=>ele.statusTitle);

  if (multipleFilters) {
    // Advanced Filters

    multipleFilters = {
      assignees: [],
      status: [],
      project: [],
      priority: [],
      actualStart: {
        startDate: null,
        endDate: null,
      },
      actualEnd: {
        startDate: null,
        endDate: null,
      },
      plannedStart: {
        startDate: null,
        endDate: null,
      },
      plannedEnd: {
        startDate: null,
        endDate: null,
      },
      Recurrence: false,
      ...multipleFilters,
    };

    let priorities = ["Critical", "High", "Medium", "Low"];
    //      let priorities = { 0: "Critical", 1: "High", 2: "Medium", 3: "Low" };
    let start = "",
      end = "",
      compare = false;

    if (multipleFilters.assignees.length > 0) {
      tasks = tasks.filter(x => {
        if (
          intersectionWith(multipleFilters.assignees, x.assigneeList, (x, y) => x === y).length > 0
        ) {
          return x;
        }
      });
    }

    if (multipleFilters.status.length > 0) {
      tasks = tasks.filter(x => {
        // let taskStatusCheck = multipleFilters.status.findIndex(item => {
        //   return item == x.status;
        // });
        let taskStatusCheck = multipleFilters.status.indexOf(x.statusTitle);
        
        if (taskStatusCheck >= 0) {
          return x;
        }
        else if(multipleFilters.status.indexOf(x.status) >= 0){
          return x;
        }

        if (multipleFilters.status.indexOf("Starred") >= 0 && x.isStared) {
          return x;
        }
        if (multipleFilters.status.indexOf("Due Today") >= 0) {
          let days = helper.RETURN_REMAINING_DAYS_STATUS(x.actualDueDateString, x.progress);
          if (days === "Due today") return x;
        }
      });
    }

    if (multipleFilters.project && multipleFilters.project.length > 0) {
      tasks = tasks.filter(x => {
        if (multipleFilters.project.indexOf(x.projectId) >= 0) {
          return x;
        }
      });
    }
    // Here to Return the task again to Dashboard
    if (multipleFilters.priority.length > 0) {
      tasks = tasks.filter(x => {
        let taskPriorityCheck = multipleFilters.priority.findIndex(item => {
          // return item == x.priority;
          return item == x.priority - 1;
        });
        if (taskPriorityCheck >= 0) {
          return x;
        }
      });
    }

    if (multipleFilters.actualStart.startDate || multipleFilters.actualStart.endDate) {
      tasks = tasks.filter(x => {
        start = new Date(
          helper.RETURN_CUSTOMDATEFORMAT(multipleFilters.actualStart.startDate, "12:00:00 AM")
        );
        end = multipleFilters.actualStart.endDate
          ? new Date(
              helper.RETURN_CUSTOMDATEFORMAT(multipleFilters.actualStart.endDate, "12:00:00 AM")
            )
          : "";
        compare = helper.CHECKDATEIFPRESENTINRANGE(start, end, new Date(x.actualStartDateString));
        if (compare) return x;
      });
    }

    if (multipleFilters.plannedStart.startDate || multipleFilters.plannedStart.endDate) {
      tasks = tasks.filter(x => {
        start = new Date(
          helper.RETURN_CUSTOMDATEFORMAT(multipleFilters.plannedStart.startDate, "12:00:00 AM")
        );
        end = multipleFilters.plannedStart.endDate
          ? new Date(
              helper.RETURN_CUSTOMDATEFORMAT(multipleFilters.plannedStart.endDate, "12:00:00 AM")
            )
          : "";
        compare = helper.CHECKDATEIFPRESENTINRANGE(start, end, new Date(x.startDateString));
        if (compare) return x;
      });
    }

    if (multipleFilters.actualEnd.startDate || multipleFilters.actualEnd.endDate) {
      tasks = tasks.filter(x => {
        start = new Date(
          helper.RETURN_CUSTOMDATEFORMAT(multipleFilters.actualEnd.startDate, "12:00:00 AM")
        );
        end = multipleFilters.actualEnd.endDate
          ? new Date(
              helper.RETURN_CUSTOMDATEFORMAT(multipleFilters.actualEnd.endDate, "12:00:00 AM")
            )
          : "";
        compare = helper.CHECKDATEIFPRESENTINRANGE(start, end, new Date(x.actualDueDateString));
        if (compare) return x;
      });
    }

    if (multipleFilters.plannedEnd.startDate || multipleFilters.plannedEnd.endDate) {
      tasks = tasks.filter(x => {
        start = new Date(
          helper.RETURN_CUSTOMDATEFORMAT(multipleFilters.plannedEnd.startDate, "12:00:00 AM")
        );
        end = multipleFilters.plannedEnd.endDate
          ? new Date(
              helper.RETURN_CUSTOMDATEFORMAT(multipleFilters.plannedEnd.endDate, "12:00:00 AM")
            )
          : "";
        compare = helper.CHECKDATEIFPRESENTINRANGE(start, end, new Date(x.dueDateString));
        if (compare) return x;
      });
    }

    if (multipleFilters.Recurrence) {
      tasks = tasks.filter(x => {
        if (x.repeatedTask != "Never") {
          return x;
        }
      });
    }
  } else if (taskState && taskState.length) {
    // Qucik filters
    let filtered = [];
    let isStatusSelected = false;

    // Capturing flags
    let i = 0;
    for (i; i < taskState.length; i++) {
      if (tasksState.indexOf(taskState[i]) > -1) isStatusSelected = true;
    }

    // Filtering status values
    if (isStatusSelected) {
      tasks = tasks.filter(x => {
        if (taskState.indexOf(tasksState[x.status]) >= 0) {
          return x;
        }
      });
    }

    const isOtherOptionSelected =
      taskState.indexOf("Starred") > -1 ||
      taskState.indexOf("Due Today") > -1 ||
      taskState.indexOf("Over Due") > -1;
    if (isOtherOptionSelected) {
      // Filtering Other values [Due Today, Starred]
      tasks = tasks.filter(x => {
        if (taskState.indexOf("Starred") > -1 && x.isStared) {
          return x;
        }
        if (taskState.indexOf("Due Today") > -1) {
          let days = helper.RETURN_REMAINING_DAYS_STATUS(x.dueDateString, x.progress);
          if (days === "Due today") {
            return x;
          }
        }
        if (taskState.indexOf("Over Due") > -1) {
          let days = helper.RETURN_REMAINING_DAYS_STATUS(x.dueDateString, x.progress);
          if (days.includes("overdue")) {
            return x;
          }
        }
      });
    }
  }

  // inserting new fields {...task,  linkedIssuesCount, linkedRisksCount, linkedMeetingsCount, assigneeLists, createdByName, reOrderIdData, project, duedate, overdue}
  tasks = tasks.map((task, index) => generateTaskInfo(task, issues, risks, meetings, members));
  
  tasks = tasks.map(t => {
    if (t.projectId) {
      let attachedProject = projects.find(p => p.projectId == t.projectId);
      if (attachedProject && attachedProject.projectTemplate) {
        t.taskStatus = attachedProject.projectTemplate;
      } else {
        t.taskStatus = workspaceStatus;
      }
      return t;
    } else {
      t.taskStatus = workspaceStatus;
      return t;
    }
  });
  return tasks;
}

export const generateTaskInfo = (task, issues, risks, meetings, members) => {
  let obj = {};

  // Calculating linked Issues to this task count
  obj.linkedIssuesCount = issues.length
    ? issues.filter(issue => issue.linkedTasks && issue.linkedTasks.indexOf(task.taskId) >= 0)
        .length
    : 0;

  // Calculating linked Risks to this task count
  obj.linkedRisksCount = risks.length
    ? risks.filter(risk => risk.linkedTasks && risk.linkedTasks.indexOf(task.taskId) >= 0).length
    : 0;

  // Calculating linked Meetings to this task count
  obj.linkedMeetingsCount = meetings && meetings.length
    ? meetings.filter(meeting => meeting.taskId === task.taskId).length
    : 0;

  // reOrderIdData from createdBy field
  obj.reOrderIdData = task.id;
  // task from createdBy field
  obj.task = task.taskTitle;
  // project from createdBy field
  obj.project = task.projectId ? task.projectId : null;
  // projectName from projectId
  obj.projectName = task.projectId ? getTaskProjectName(task.projectId) : "";
  // duedate from createdBy field
  obj.duedate = helper.RETURN_REMAINING_DAYS_STATUS(task.dueDate, task.progress);

  obj.overdue = helper.RETURN_REMAINING_DAYS_STATUS(task.actualDueDateString, task.progress);

  obj.createdByName = " ";

  // assigneeLists from createdBy field
  obj.assigneeLists = members.filter(member => {
    // createdByName from createdBy field
    if (task.createdBy && member.userId === task.createdBy)
      obj.createdByName = generateUsername(member.email, member.userName, member.fullName);

    if (task.assigneeList && task.assigneeList.length >= 0)
      return task.assigneeList.indexOf(member.userId) >= 0;
  });

  return { ...task, ...obj };
};
