import React, { Component } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import SearchInput, { createFilter } from "react-search-input";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
import Avatar from "@material-ui/core/Avatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Checkbox from "@material-ui/core/Checkbox";
import DoneIcon from "@material-ui/icons/Done";
import SelectMenu from "../../../components/Menu/SelectMenu";
import selectStyles from "../../../assets/jss/components/select";

class ProjectDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      searchTerm: "",
      checked: [],
    };
    this.searchUpdated = this.searchUpdated.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }
  handleToggle = value => () => {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];
    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    this.setState({
      checked: newChecked
    });
  };
  render() {
    const { theme, classes } = this.props;
    const { checked } = this.state;
    const data = [
      { id: 1, value: { name: "Person 1",} },
      { id: 2, value: { name: "Person 2",} },
      { id: 3, value: { name: "Person 3",} },
      { id: 4, value: { name: "Person 4",} },
      { id: 5, value: { name: "Person 5"} }
    ];
    
    const filteredData = data.filter(
      createFilter(this.state.searchTerm, ["id", "value.name"])
    );

    return (
      <SelectMenu
        label="Project"
        multiple={true}
        value={checked}
      >
        <List classes={{root: classes.searchMenuList}}>
          <ListItem disableRipple={true}>
            <SearchInput className="HtmlInput" onChange={this.searchUpdated} />
          </ListItem>
          {filteredData.map(data => {
            return (
              <ListItem
                key={data.id}
                value={data.value.name}
                onClick={this.handleToggle(data.value.name)}
                classes={{
                  root: classes.PlainMenuItem
                }}
                button
                disableRipple={true}
              >
                <ListItemText
                  primary={data.value.name}
                  classes={{ primary: classes.MenuItemText }}
                />
                <ListItemSecondaryAction>
                  <Checkbox
                    checkedIcon={
                      <DoneIcon
                        htmlColor={theme.palette.primary.light}
                      
                      />
                    }
                    onChange={this.handleToggle(data.value.name)}
                    checked={this.state.checked.indexOf(data.value.name) !== -1}
                    icon={false}
                  />
                </ListItemSecondaryAction>
              </ListItem>
            );
          })}
        </List>
      </SelectMenu>
    );
  }
}

export default withStyles(selectStyles, {
  withTheme: true
})(ProjectDropdown);
