import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "../TaskDetails/styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { getCompletePermissionsWithArchieve } from "../permissions";
import { UpdateGantTask, DeleteArchiveTask } from "../../../redux/actions/tasks";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import { FormattedMessage } from "react-intl";
class TaskDetailsActionDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      deleteBtnQuery: "",
      archiveBtnQuery: "",
      unarchiveBtnQuery: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleColorClick = this.handleColorClick.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleCustomColor = this.handleCustomColor.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }
  componentDidMount = () => {
    this.setState({
      selectedColor: this.props.selectedColor,
    });
  };

  handleCustomColor(event) {
    let obj = Object.assign({}, this.props.task);
    obj.colorCode = event.hex;
    delete obj.CalenderDetails;
    if (this.props.isGantt) {
      this.props.UpdateGantTask(obj, (err, response) => {
        if (response) {
          self.props.UpdateCalenderTask(response, () => { });
        }
      });
    } else {
      this.props.UpdateTaskColor(obj, data => {
        this.props.UpdateCalender(data);
      });
    }
  }
  handleOperations(value) {
    //
    switch (value) {
      case "Copy":
        this.props.CopyTask(
          this.props.task,
          () => { },
          () => { }
        );
        break;
      case "Public Link":
        break;
      case "Archive":
        this.setState({ archiveFlag: true, popupFlag: true });
        break;
      case "Unarchive":
        this.setState({ unArchiveFlag: true, popupFlag: true });
        break;
      case "Delete":
        this.setState({ deleteFlag: true, popupFlag: true });
        break;
      case "Activity Log":
        this.props.activityLog();
        break;
    }

    this.setState({ open: false, pickerOpen: false });
  }
  getTranslatedId(value) {
    switch (value) {
      case "Copy":
        value = "common.action.copy.label";
        break;
      case "Public Link":
        break;
      case "Archive":
        value = "common.action.archive.label";
        break;
      case "Unarchive":
        value = "common.action.un-archive.label";
        break;
      case "Delete":
        value = "common.action.delete.label";
        break;
      case "Activity Log":
        value = "common.action.activity-log.label";
        break;
    }
    return value;
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  handleDialogClose() {
    this.setState(
      {
        popupFlag: false,
        archiveFlag: false,
        unArchiveFlag: false,
        deleteFlag: false,
      }
      // () => {
      //   if (this.props.closeTaskDetailsPopUp)
      //     this.props.closeTaskDetailsPopUp();
      // }
    );
  }

  handleDelete = (e, deleteOption) => {
    e.stopPropagation();
    const { childTasks, task, GanttCmp } = this.props;
    const { taskId, isDeleted } = task;
    const postObj = {
      taskId,
      isChildDelete: childTasks && deleteOption ? childTasks.length > 0 : false,
    };
    const childIds = childTasks && childTasks.map(t => t.id);
    // if task is archived then delete from local store
    if (isDeleted) {
      this.setState(
        { deleteBtnQuery: "progress" },
        //Success
        () => {
          this.props.closeTaskDetailsPopUp();
          DeleteArchiveTask(
            taskId,
            response => {
              this.props.deleteTaskFromArchiveList(taskId);
              this.setState({ deleteBtnQuery: "", deleteFlag: false });
            },
            //Failure
            () => {
              this.setState({ deleteBtnQuery: "", deleteFlag: false });
            }
          );
        }
      );
    }
    // if task is not archived then delete from global store
    else {
      this.setState({ deleteBtnQuery: "progress" }, () => {
        this.props.closeTaskDetailsPopUp();
        this.props.DeleteTask(
          postObj,
          //Success
          response => {
            // Only used for task deletion in gantt chart
            if (this.props.onDeleteTask) {
              this.props.onDeleteTask(
                this.props.task.taskId,
                this.props.GanttCmp,
                childIds,
                childTasks && deleteOption && childTasks.length > 0
              );
            }
            this.setState({ deleteBtnQuery: "", deleteFlag: false });
          },
          //failure
          error => {
            this.setState({ deleteBtnQuery: "", deleteFlag: false });
          }
        );
      });
    }
  };

  handleClick(event, placement) {
    event.stopPropagation();
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleColorClick(event) {
    event.stopPropagation();
    if (event.target.closest("#colorPickerCnt") == null) {
      this.setState(prevState => ({ pickerOpen: !prevState.pickerOpen }));
    }
  }

  colorChange = (color, task) => {
    let self = this;
    this.setState({ selectedColor: color }, function () {
      let obj = Object.assign({}, task);
      this.setState({
        selectedColor: color,
      });
      obj.colorCode = color;
      delete obj.CalenderDetails;

      if (this.props.isGantt) {
        this.props.UpdateGantTask(obj, (err, response) => {
          if (response) {
            self.props.UpdateCalenderTask(response, () => { });
          }
        });
      } else {
        self.props.UpdateTaskColorFromGridItem(obj, data => {
          self.props.UpdateCalenderTask(data, () => { });
        });
      }
    });
  };

  handleArchive = (e, deleteOption) => {
    if (e) e.stopPropagation();
    const { childTasks, task, GanttCmp } = this.props;
    const { taskId, isDeleted } = task;
    const childIds = childTasks && childTasks.map(t => t.id);
    this.setState({ archiveBtnQuery: "progress" }, () => {
      this.props.closeTaskDetailsPopUp();
      this.props.ArchiveTask(
        this.props.task,
        //success
        succ => {
          // onDeleteTask is action to delete gantt task if ordinary task is deleted from task details view
          if (this.props.onDeleteTask) {
            this.props.onDeleteTask(
              succ.data.taskId,
              this.props.GanttCmp,
              childIds,
              childTasks && deleteOption && childTasks.length > 0
            );
          }
          this.setState({
            archiveBtnQuery: "",
            archiveFlag: false,
            popupFlag: false,
          });
          // showTaskDetails false (in List of Table)
        },
        //failure
        error => {
          this.setState({
            archiveBtnQuery: "",
            archiveFlag: false,
            popupFlag: false,
          });
        }
      );
    });
  };

  handleUnArchive = e => {
    if (e) e.stopPropagation();
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.closeTaskDetailsPopUp();
      this.props.UnArchiveTask(
        this.props.task,
        //success
        () => {
          this.setState({
            unarchiveBtnQuery: "",
            unArchiveFlag: false,
            popupFlag: false,
          });
          //it will unarchive the task from local state
          this.props.filterUnArchiveTask(this.props.task.taskId);
        },
        //failure
        error => {
          this.setState({
            unarchiveBtnQuery: "",
            unArchiveFlag: false,
            popupFlag: false,
          });
          this.props.showSnackBar(error.data.message);
          // this.props.closeTaskDetailsPopUp();
        }
      );
    });
  };

  render() {
    const {
      classes,
      theme,
      permission,
      isGantt,
      childTasks,
      taskPer,
      btnStyles = {},
      iconStyles = {},
    } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      popupFlag,
      selectedColor,
      deleteBtnQuery,
      archiveBtnQuery,
      unarchiveBtnQuery,
    } = this.state;

    const archivePermission = taskPer.archive.cando;
    const unArchivePermission = taskPer.unarchive.cando;
    const copyPermission = taskPer.copyInWorkSpace.cando;
    const deletePermission = taskPer.delete.cando;

    const ddData = [];

    if (copyPermission && !this.props.task.isDeleted) {
      ddData.push("Copy");
    }
    if (!this.props.task.isDeleted) {
      ddData.push("Color");
    }
    ddData.push("Activity Log");
    if (archivePermission && !this.props.task.isDeleted) {
      ddData.push("Archive");
    }
    if (unArchivePermission && this.props.task.isDeleted) {
      ddData.push("Unarchive");
    }
    if (deletePermission) {
      ddData.push("Delete");
    }

    return (
      <Fragment>
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={event => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}
              style={btnStyles}>
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px", ...iconStyles }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              disablePortal={true}
              list={
                <List
                  onClick={e => {
                    e.stopPropagation();
                  }}>
                  <ListItem disableRipple={true} classes={{ root: classes.menuHeadingItem }}>
                    <ListItemText
                      primary={
                        <FormattedMessage id="common.action.label" defaultMessage="Select Action" />
                      }
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map(value =>
                    value == "Color" ? (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{
                          root: classes.selectColorMenuItem,
                          selected: classes.statusMenuItemSelected,
                        }}
                        onClick={event => {
                          this.handleColorClick(event);
                        }}>
                        <ListItemText
                          primary={
                            <FormattedMessage id="common.action.color.label" defaultMessage={value} />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                        <RightArrow
                          htmlColor={theme.palette.secondary.dark}
                          classes={{ root: classes.submenuArrowBtn }}
                        />
                        <div
                          id="colorPickerCnt"
                          className={classes.colorPickerCntLeft}
                          style={pickerOpen ? { display: "block" } : { display: "none" }}>
                          <ColorPicker
                            triangle="top"
                            onColorChange={color => {
                              this.colorChange(color, this.props.task);
                            }}
                            selectedColor={selectedColor}
                          />
                        </div>
                      </ListItem>
                    ) : (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={this.handleOperations.bind(this, value)}>
                        <ListItemText
                          primary={
                            <FormattedMessage id={this.getTranslatedId(value)} defaultMessage={value} />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                      </ListItem>
                    )
                  )}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
        <React.Fragment>
          {this.state.archiveFlag ? (
            <ActionConfirmation
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.archive.confirmation.cancel-button.label"
                  defaultMessage="Cancel"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.archive.confirmation.archive-button.label"
                  defaultMessage="Archive"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="common.action.archive.confirmation.title"
                  defaultMessage="Archive"
                />
              }
              iconType="archive"
              msgText={
                <FormattedMessage
                  id="common.action.archive.confirmation.task.label"
                  defaultMessage="Are you sure you want to archive this task?"
                />
              }
              successAction={this.handleArchive}
              btnQuery={archiveBtnQuery}
            />
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {this.state.unArchiveFlag ? (
            <ActionConfirmation
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.cancel-button.label"
                  defaultMessage="Cancel"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.un-archive-button.label"
                  defaultMessage="Unarchive"
                />
              }
              alignment="center"
              iconType="unarchive"
              headingText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.title"
                  defaultMessage="Unarchive"
                />
              }
              msgText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.task.label"
                  defaultMessage="Are you sure you want to unarchive this task?"
                />
              }
              successAction={this.handleUnArchive}
              btnQuery={unarchiveBtnQuery}
            />
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {this.state.deleteFlag && isGantt && childTasks.length ? (
            <DeleteConfirmDialog
              open={popupFlag}
              closeAction={event => this.handleDelete(event, true)} // True denotes that delete the parent task and its children as well
              cancelBtnText="Delete All Tasks"
              successBtnText="Delete only Parent Task"
              deleteBtnText="Cancel"
              deleteBtnProps={{
                btnType: "danger",
                variant: "outlined",
                onClick: this.handleDialogClose,
              }}
              alignment="center"
              headingText="Delete"
              successAction={event => this.handleDelete(event, false)} // True denotes that delete the parent task only
              msgText={
                <>
                  Looks like this task has subtask(s). <br />
                  <br />
                  Do you want to delete only this parent task or also all
                  <br /> subtask(s) associated with this task?
                </>
              }
              btnQuery={deleteBtnQuery}
            />
          ) : (
            this.state.deleteFlag && (
              <DeleteConfirmDialog
                open={popupFlag}
                closeAction={this.handleDialogClose}
                cancelBtnText={
                  <FormattedMessage
                    id="common.action.delete.confirmation.cancel-button.label"
                    defaultMessage="Cancel"
                  />
                }
                successBtnText={
                  <FormattedMessage
                    id="common.action.delete.confirmation.delete-button.label"
                    defaultMessage="Delete"
                  />
                }
                alignment="center"
                headingText={
                  <FormattedMessage
                    id="common.action.delete.confirmation.title"
                    defaultMessage="Delete"
                  />
                }
                successAction={this.handleDelete}
                msgText={
                  <FormattedMessage
                    id="common.action.delete.confirmation.task.label"
                    defaultMessage="Are you sure you want to delete this task?"
                  />
                }
                btnQuery={deleteBtnQuery}
              />
            )
          )}
        </React.Fragment>
      </Fragment>
    );
  }
}

export default compose(
  withRouter,
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true,
  }),
  connect(null, {
    UpdateGantTask,
  })
)(TaskDetailsActionDropDown);
