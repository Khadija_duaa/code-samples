import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import styles from "./style";
import CustomFieldLabelCmp from "../../../components/CustomFieldsViews/CustomFieldLabel.cmp";

import moment from "moment";
import CustomDatePicker from "../../../components/DatePicker/DatePicker/DatePicker";
import { injectIntl } from "react-intl";

function TaskDateDropdown({
  classes,
  theme,
  handleSelectDate,
  permission,
  label,
  timeInputLabel,
  intl,
  date,
  id,
  selectedTime,
  customProps,
  handleSelectHideOption,
  hideOperation= true
}) {
  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={label}
        iconType={"date"}
        handleDelete={() => {}}
        handleEdit={() => {}}
        handleCopy={() => {}}
        handleSelectHideOption={handleSelectHideOption}
        deleteOperation={false}
        editOperation={false}
        copyOperation={false}
        hideOperation={hideOperation}
        customIconProps={{
          color: "#999",
        }}
      />
      <CustomDatePicker
        date={date && moment(date)}
        dateFormat="MMM DD, YYYY"
        timeInput={true}
        onSelect={(date, time) => handleSelectDate(id, date, time)}
        selectedTime={selectedTime}
        style={{ borderRadius: 4, flex: 1, padding: "9px 0px 10px 8px", position: "relative" }}
        label={null}
        icon={false}
        PopperProps={{ size: null }}
        filterDate={moment()}
        timeInputLabel={timeInputLabel}
        btnProps={{ className: classes.datePickerCustomStyle }}
        deleteIcon={true}
        placeholder={"Select Date"}
        containerProps={{
          className: classes.datePickerContainer,
        }}
        disabled={!permission}
        deleteIconProps={{
          style: {
            marginRight: 10,
          },
        }}
        {...customProps}
      />
    </div>
  );
}

TaskDateDropdown.defaultProps = {
  classes: {},
  theme: {},
  handleSelectDate: () => {},
  handleSelectHideOption: () => {},
  permission: true,
  disabled: false,
  label: "",
  timeInputLabel: "",
  id: "",
  customProps: {},
  hideOperation:true
};

const mapStateToProps = state => {
  return {};
};

export default compose(
  withStyles(styles, { withTheme: true }),
  injectIntl,
  connect(mapStateToProps, {})
)(TaskDateDropdown);
