import React, { useMemo, useState, useEffect, useRef } from "react";
import { compose } from "redux";
import { connect, useSelector, useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";
import { withStyles } from "@material-ui/core/styles";
import moment from "moment";
import { injectIntl, FormattedMessage } from "react-intl";
import taskDetailStyles from "./style";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { priorityData } from "../../../helper/issueDropdownData";
import { getCustomFields } from "../../../helper/customFieldsData";

import { getFilteredTasks } from "../TaskFilter/FilterUtils";
import DetailsDialog from "../../../components/Dialog/DetailsDialog/DetailsDialog";
import { taskDetailDialogState } from "../../../redux/actions/allDialogs";
import SideTabs from "../TaskDetails/SideTabs";
import TaskDetailsActionDropDown from "./TaskDetailsActionDropDown";
import helper from "../../../helper";
import NotificationMenu from "../../../components/Header/NotificationsMenu";
import SlackChannelIntegration from "../TaskDetails/SlackChannelIntegration";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import CustomFieldLabelCmp from "../../../components/CustomFieldsViews/CustomFieldLabel.cmp";
import TodoList from "../../../components/TodoList/TodoList";
import { CanAccess, TeamCanAccessFeature } from "../../../components/AccessFeature/AccessFeature.cmp";
import {
  UpdateTask,
  FetchTasksInfo,
  CopyTask,
  DeleteTask,
  ArchiveTask,
  UnArchiveTask,
  startTimer,
  stopTimer,
  removeUserEffort,
  UpdateGantTask,
  MarkTaskNotificationsAsRead,
  getTaskComments,
  removeTaskComments,
  CheckAllTodoList,
  UpdateTaskCustomField,
  dispatchTask,
  bulkUpdateTask,
  updateTaskData,
} from "../../../redux/actions/tasks";
import { UpdateCalenderTask, CopyCalenderTask } from "../../../redux/actions/calenderTasks";
import { setGlobalTaskTime, removeGlobalTaskTime } from "../../../redux/actions/globalTimerTask";

import { UpdateTaskColorFromGridItem } from "../../../redux/actions/workspace";
import { dispatchAddTodoItems } from "../../../redux/actions/todoItems";
import { getGanttTasks } from "../../../redux/actions/gantt";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import CustomIconButton from "../../../components/Buttons/IconButton";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import RepeatTask from "../RepeatTask/RepeatTask";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import CircularIcon from "@material-ui/icons/Brightness1";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import Timer from "../../../components/Timer/Timer";
import EditableTextField from "../../../components/Form/EditableTextField";
import SectionsView from "../../../components/Sections/sections.view";
import { updateCustomFieldDialogState } from "../../../redux/actions/allDialogs";
import {
  editCustomField,
  updateCustomFieldData,
  hideCustomField,
} from "../../../redux/actions/customFields";
import CustomFieldView from "../../CustomFieldSettings/CustomFields.view";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import ProjectDropdown from "./ProjectDropdown";
import TaskDateDropDown from "./TaskDateDropdown";
import TaskDescriptionEditor from "./TaskDetailsDescription";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import isUndefined from "lodash/isUndefined";
import useUpdateTask from "../../../helper/customHooks/tasks/updateTask";
import TaskActionDropdown from "../TaskActionDropdown/TaskActionDropdown";
import { grid } from "../../../components/CustomTable2/gridInstance";
import { getTemplate } from "../../../utils/getTemplate";
import { clearDocuments } from "../../../redux/actions/documents";
import { toast } from "react-toastify";
import { toastMessages } from "../../../helper/customHooks/tasks/toastMessages";
import { updateFeatureAccessPermissions } from "../../../redux/actions/team";
import ProgressField from "./ProgressField";
import { excludeOffdays, getCalendar } from "../../../helper/dates/dates";
import { GlobalTimeCustom, TaskEffortMandatory } from "../../../helper/config.helper";
import { AST_Return } from "terser";

const priorities = ["", "Critical", "High", "Medium", "Low"];

function NewTaskDetailView(props) {
  const {
    theme,
    classes,
    intl,
    history,
    enqueueSnackbar,
    dialogsState,
    appliedFilters,
    members,
    tasks,
    taskPer,
    companyInfo,
    meetingPer,
    riskPer,
    issuePer,
    taskActivitiesState,
    startTimer,
    stopTimer,
    removeUserEffort,
    MarkTaskNotificationsAsRead,
    getTaskComments,
    setGlobalTaskTime,
    removeTaskComments,
    CheckAllTodoList,
    UpdateTaskCustomField,
    dispatchTask,
    taskTodoItems,
    dispatchAddTodoItems,
    getGanttTasks,
    taskNotificationsState,
    profileState,
    updateCustomFieldDialogState,
    editCustomField,
    updateCustomFieldData,
    hideCustomField,
    archivedTasks,
    workspaces,
    loggedInTeam,
    permissionObject,
    clearDocuments,
    customFields,
    accessFeaturesTask,
    updateFeatureAccessPermissions
  } = props;
  const { Task } = appliedFilters;
  const { taskDetailDialog } = dialogsState;
  let searchQuery = history.location.pathname; //getting url
  const dispatch = useDispatch();
  const { editTask, handleDateSave } = useUpdateTask();
  const [currentTask, setCurrentTask] = useState({});
  const [isDoneStatus, setIsDoneStatus] = useState(false);
  const [otherStatus, setOtherStatus] = useState(false);

  useEffect(() => {
    let taskList = taskDetailDialog.isArchived
      ? archivedTasks
      : tasks; /** if user screen is archived tasks than get archived tasks from store otherwise get all store tasks */
    const selectedTask = taskList.find(task => task.taskId === taskDetailDialog.id);
    if (!selectedTask) { // if task not found than close the task details
      closeTaskDetailsHandler();
      return;
    }
    setCurrentTask(selectedTask);
    taskDetails.current = selectedTask;
  }, [tasks]);
  const taskDetails = useRef(currentTask);

  useEffect(() => {
    if (taskDetails.current) {
      getTaskComments(
        { taskId: taskDetails.current.taskId, todoTaskId: taskDetails.current.id },
        response => { },
        error => { }
      );
    }
  }, [taskDetailDialog.id]);

  const clearData = () => {
    removeTaskComments();
    if (taskDetails.current.taskId)
      dispatchAddTodoItems(
        taskDetails.current.taskId,
        "ClearTodoItems"
      ); /** clearing the todo item from store when closing task details */
    clearDocuments(); //clear Documents
  };
  const handleCloseDialog = () => {
    clearData();
    taskDetailDialogState(dispatch, { id: "", isArchived: false });
    if (
      !searchQuery.includes("gantt") &&
      !searchQuery.includes("boards") &&
      !searchQuery.includes("/reports/taskOverview")
    ) {
      history.push("/tasks");
    } /** if modal is opend from gantt view */
    dialogsState.taskDetailDialog.afterCloseCallBack(taskDetails);
  };
  const [showActivity, setShowActivity] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [markChecklistActionConf, setMarkChecklistActionConf] = useState(false);
  const [sFSecOpen, setSFSecOpen] = useState(true);

  const handleShowSystemField = section => {
    setSFSecOpen(!sFSecOpen);
  };
  const activityLog = () => {
    setShowActivity(true);
  };

  const hideActivityLog = () => {
    setShowActivity(false);
  };
  const closeTaskDetailsHandler = () => {
    taskDetailDialogState(dispatch, { id: "", isArchived: false });
    dialogsState.taskDetailDialog.afterCloseCallBack(taskDetails);
    dispatchAddTodoItems([], "ClearTodoItems");
    /** when task details modal closes then emtying the todoItems store array */
    if (searchQuery !== "/reports/taskOverview") {
      history.push("/tasks");
    }
  };

  const showSnackBar = (snackBarMessage, type, options) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
        ...options
      }
    );
  };

  const notificationsData = () => {
    const taskNotificationsData = taskNotificationsState.data ? taskNotificationsState.data : [];
    const members =
      profileState && profileState.data && profileState.data.member
        ? profileState.data.member.allMembers
        : [];
    let notifications = taskNotificationsData
      ? taskNotificationsData.filter(x => {
        return x.users.filter(y => y.userId === profileState.data.userId && y.isViewed === false)
          .length;
      })
      : [];
    notifications = notifications
      .map(x => {
        x.createdName = members.find(m => m.userId === x.createdBy);
        x.users = x.users.map(u => ({
          userId: u.userId,
          isViewed: u.isViewed,
        }));
        return x;
      })
      .map(x => ({
        id: x.notificationId,
        // title: x.createdName
        //   ? `${x.createdName.fullName ||
        //       x.createdName.userName ||
        //       x.createdName.email} ${x.description}`
        //   : " ",
        title: x.description,
        date: helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(x.updatedDate || x.createdDate),
        time: helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate || x.createdDate),
        profilePic: x.createdName ? x.createdName.imageUrl : "",
        createdName: x.createdName,
        notificationUrl: x.notificationUrl,
        teamName: x.teamName || "N/A",
        teamId: x.teamId,
        workspaceName: x.workSpaceName || "N/A",
        workspaceId: x.workSpaceId,
      }));
    return notifications;
  };
  const handleAllRead = () => {
    const currentUser = profileState.data ? profileState.data.userId : [];
    MarkTaskNotificationsAsRead(currentTask.taskId, currentUser, (err, data) => { });
    taskDetailDialogState(dispatch, { id: "", isArchived: false });
    dialogsState.taskDetailDialog.afterCloseCallBack(taskDetails);
  };
  const updateTask = (type, value) => {
    editTask(
      currentTask,
      type,
      value,
      succ => { },
      err => {
        showSnackBar(err, "error");
      }
    );
  };
  const repeatDropdownOpen = e => {
    // e.stopPropagation();
    setAnchorEl(anchorEl ? null : e.currentTarget);
  };
  const closeRepeatDropdown = () => {
    setAnchorEl(null);
  };
  const updateAssignee = (assignedTo, task) => {
    editTask(
      currentTask,
      "assigneeList",
      assignedTo,
      succ => { },
      err => {
        showSnackBar(err, "error");
      }
    );
  };
  const handleStatusChange = status => {
    let task = { ...currentTask };
    task.status = status.value;
    task.statusTitle = status.label;
    if (status.obj.isDoneState) {
      const checkData = {
        taskId: currentTask.id,
        checkAll: true,
      };
      CheckAllTodoList(
        checkData,
        succ => { },
        fail => { },
        profileState.data
      );
    }
    updateTask("status", status.value);
    setMarkChecklistActionConf(false);
    // UpdateTask(task, response => {
    //   if (response && response.status == 200) {
    //     setMarkChecklistActionConf(false);
    //   } else {
    //     showSnackBar(response.data.message, "error");
    //     setMarkChecklistActionConf(false);
    //   }
    // });
  };
  const handleUpdateStatus = status => {
    // const { loggedInTeam, workspace } = profileState.data;
    // const currentWorkspace = workspace.find(t => t.teamId === loggedInTeam);
    if (TaskEffortMandatory(currentTask.teamId)) {
      // showSnackBar("Make sure you have added effort in the task", "info", {
      //   anchorOrigin: {
      //     vertical: "bottom",
      //     horizontal: "right",
      //   },
      // }); 
      if (status.obj.isDoneState) {
        setIsDoneStatus(true);
      }
      setOtherStatus(status);
      markAllConfirmDialogOpen();
      return;
    }

    let allItemsCompleted = taskTodoItems.every(
      todo => todo.isComplete
    ); /** check if todo items are all completed or not, if true then dont show modal */
    if (status.obj.isDoneState && !allItemsCompleted) {
      markAllConfirmDialogOpen();
      return;
    }
    handleStatusChange(status);
  };

  const handleUpdateStatusConformation = () => {
    if (isDoneStatus) {
      const checkData = {
        taskId: currentTask.id,
        checkAll: true,
      };
      CheckAllTodoList(
        checkData,
        succ => { },
        fail => { },
        profileState.data
      );
      updateTask("status", doneStatus.value);
    } else {
      updateTask("status", otherStatus.value);
    }
    setIsDoneStatus(null);
    setOtherStatus(null);
    markAllConfirmDialogClose();
  };
  const markAllConfirmDialogOpen = () => {
    setMarkChecklistActionConf(true);
  };
  const markAllConfirmDialogClose = () => {
    setMarkChecklistActionConf(false);
  };
  const statusData = statusArr => {
    return statusArr.map(item => {
      return {
        label: item.statusTitle,
        value: item.statusId,
        icon: <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "20px" }} />,
        statusColor: item.statusColor,
        obj: item,
        statusTitle: item.statusTitle,
      };
    });
  };
  const startTime = (time, start) => {
    // if (GlobalTimeCustom(currentTask.teamId)) {
    //   showSnackBar('Timer cannot be started', "error");
    //   return
    // }
    const data = {
      taskId: currentTask.taskId,
      startTime: time,
      isStart: true,
    };
    startTimer(
      data,
      response => {
        const { createdDate, currentDate } = response.data;
        setGlobalTaskTime(currentTask, createdDate, currentDate);
        start();
      },
      error => {
        const err = error.data.message;
        const message = err || "Server throws error";
        showSnackBar(message, "error");
      }
    );
  };

  const stopTime = time => {
    const data = {
      taskId: currentTask.taskId,
      endTime: time,
      isStart: false,
    };
    stopTimer(
      data,
      suss => {
        showSnackBar('Task Effort added successfully!', "success");
      },
      err => {
        if (err && err.data) showSnackBar(`Oops! Effort not saved .${err.data}`, "error");
      }
    );
  };
  const handleRemoveUserTaskEffort = taskEffortId => {
    removeUserEffort(
      currentTask.taskId,
      taskEffortId,
      response => {
        if (grid.grid) grid.grid.redrawRows([response.data]);
      },
      error => {
        const err = error.data.message;
        const message = err || "Server throws error";
        showSnackBar(message, "error");
      }
    );
  };
  const handleUpdateTitle = title => {
    /** Update task title when user changes */
    updateTask("taskTitle", title);
  };
  const customFieldChange = (option, obj, settings, success = () => { }, failure = () => { }) => {
    // const taskCopy = { ...currentTask };
    const taskCopy = { ...taskDetails.current };

    if (obj.fieldType == "matrix") {
      const selectedField =
        taskCopy.customFieldData && taskCopy.customFieldData.find(x => x.fieldId == obj.fieldId);
      const isExist =
        selectedField &&
        selectedField.fieldData.data.findIndex(f => f.cellName == option.cellName) > -1;
      if (isExist === true) return;
    }
    const { fieldId } = obj;
    const newObj = {
      groupType: "task",
      groupId: taskCopy.taskId,
      fieldId,
      fieldData: { data: option },
    };

    const message = toastMessages.customField;
    const defaultMessage = toastMessages.default;
    const loadingMessage = message ? `${obj.fieldName} ${message.loading}` : defaultMessage.loading;
    const successMessage = message ? `${obj.fieldName} ${message.success}` : defaultMessage.success;
    const failureMessage = message ? (
      <>
        {obj.fieldName} {message.failure}
      </>
    ) : (
      defaultMessage.failure
    );
    const fieldNameUpdatedToast = toast.loading(loadingMessage);
    UpdateTaskCustomField(
      newObj,
      res => {
        success();
        toast.update(fieldNameUpdatedToast, {
          render: successMessage,
          type: "success",
          isLoading: false,
          autoClose: 3000,
          hideProgressBar: false,
          pauseOnHover: true,
          closeOnClick: true,
          draggable: true,
        });
        const taskObj = res.data.entity[0];
        let customFieldsArr = [];
        // Updating Global state
        const isExist = taskCopy.customFieldData
          ? taskCopy.customFieldData.findIndex(c => c.fieldId === taskObj.fieldId) > -1
          : false; /** if new task created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in task object and save it */
        if (isExist) {
          customFieldsArr = taskCopy.customFieldData.map(c => {
            return c.fieldId === taskObj.fieldId ? taskObj : c;
          });
        } else {
          customFieldsArr = taskCopy.customFieldData
            ? [...taskCopy.customFieldData, taskObj]
            : [
              taskObj,
            ]; /** add custom field in task object and save it, if newly created task because its custom field is already null */
        }
        let newTaskObj = { ...taskCopy, customFieldData: customFieldsArr };
        dispatchTask(newTaskObj);
      },
      // failure
      () => {
        toast.update(fieldNameUpdatedToast, {
          render: failureMessage,
          type: "error",
          isLoading: false,
          autoClose: 2000,
          hideProgressBar: false,
          pauseOnHover: true,
          closeOnClick: true,
          draggable: true,
        });
      }
    );
    // this.handleAddAssessmentDialog(false);
  };
  const handleEditField = field => {
    updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "edit" });
  };
  const handleCopyField = field => {
    updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "copy" });
  };
  const handleSelectHideOption = (value, selectedFieldObj) => {
    let object = { ...selectedFieldObj };

    if (value == 1) {
      object.hideOption = "workspace";
      object.settings.isHidden = false;

      object.settings.hiddenInWorkspaces = [
        profileState.data.loggedInTeam,
        ...object.settings.hiddenInWorkspaces,
      ];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("task") < 0
          ? ["task", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    if (value == 2) {
      /** if user select the team level */
      object.hideOption = "allWorkspaces";
      object.settings.isHidden = true;
      object.settings.hiddenInWorkspaces = [];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("task") < 0
          ? ["task", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    delete object.team;
    delete object.workspaces;
    hideCustomField(
      object,
      res => { },
      failure => { }
    );
  };
  const handleSystemOptionSelect = (value, selectedFieldObj) => {
    let object = {
      fieldId: selectedFieldObj.fieldId
    };
    if (value == 0) {
      object.isHidden = false;
      object.IsHiddenInWorkspace = false;
      object.IsHiddenInTeam = false;
    }
    if (value == 1) {
      object.IsHiddenInWorkspace = true;
    }
    if (value == 2) {
      object.IsHiddenInTeam = true;
    }
    updateFeatureAccessPermissions(
      'task',
      object,
      succ => {
      },
      fail => {
      }
    );
  };
  const handleClickHideOption = (event, field) => {
    let object = { ...field };
    object.settings.isHideCompletedTodos = !object.settings.isHideCompletedTodos;
    delete object.team;
    delete object.workspaces;
    delete object.level;
    editCustomField(
      object,
      object.fieldId,
      res => { },
      failure => { }
    );
  };

  const editCustomFieldCallback = (obj, oldFieldObject) => {
    const { fieldId, fieldType } = obj;
    if (fieldType === "checklist") return;
    updateCustomFieldData(obj);
  };
  const handleDateUpdate = (key, date, time) => {
    handleDateSave(
      key,
      date,
      currentTask,
      time,
      succ => { },
      err => {
        showSnackBar(err, "error");
      }
    );
  };
  const handleProgressUpdate = (val) => {
    updateTask("progress", val ? val : '0');
  };
  const handleUpdateDuration = (val) => {
    updateTask("msProjectDuration", val ? Number(val) : 0);
  };
  const handlePrioritySelect = (key, value) => {
    updateTask("priority", priorities.indexOf(value.value));
  };
  const permissionsObj = taskDetailDialog.permissionObject
    ? taskDetailDialog.permissionObject
    : {
      task: taskPer,
      issue: issuePer,
      meeting: meetingPer,
      risk: riskPer,
    };
  const permissions =
    currentTask &&
      currentTask.projectId /** if task link with project then assign project permissions */
      ? isUndefined(permissionObject[currentTask.projectId])
        ? permissionsObj
        : permissionObject[currentTask.projectId]
      : permissionsObj; /** workspace level permissions */

  const chatConfig = {
    contextUrl: "communication",
    contextView: "Task",
    contextId: currentTask.taskId,
    contextKeyId: "taskId",
    contextChat: "single",
    assigneeLists: currentTask.assigneeLists,
  };
  const chatPermission = {
    addAttachment:
      !currentTask.isDeleted && permissions.task.taskDetail.taskComments.taskattachment.isAllowAdd,
    deleteAttachment:
      !currentTask.isDeleted &&
      permissions.task.taskDetail.taskComments.taskattachment.isAllowDelete,
    downloadAttachment:
      !currentTask.isDeleted && permissions.task.taskDetail.taskComments.downloadAttachment.cando,
    addComments: !currentTask.isDeleted && permissions.task.taskDetail.taskComments.cando,
    editComment:
      !currentTask.isDeleted && permissions.task.taskDetail.taskComments.comments.isAllowEdit,
    deleteComment:
      !currentTask.isDeleted && permissions.task.taskDetail.taskComments.comments.isAllowDelete,
    convertToTask:
      !currentTask.isDeleted && permissions.task.taskDetail.taskComments.convertToTask.cando,
    reply: !currentTask.isDeleted && permissions.task.taskDetail.taskComments.reply.cando,
    replyLater: !currentTask.isDeleted && permissions.task.taskDetail.taskComments.replyLater.cando,
    showReceipt:
      !currentTask.isDeleted && permissions.task.taskDetail.taskComments.showReceipt.cando,
    clearAll:
      !currentTask.isDeleted && permissions.task.taskDetail.taskComments.clearAllReplies.cando,
    clearReply: !currentTask.isDeleted && permissions.task.taskDetail.taskComments.clearReply.cando,
  };
  const docPermission = {
    uploadDocument:
      !currentTask.isDeleted && permissions.task.taskDetail.taskDocuments.uploadDocument.cando,
    addFolder: !currentTask.isDeleted && permissions.task.taskDetail.taskDocuments.addFolder.cando,
    downloadDocument:
      !currentTask.isDeleted && permissions.task.taskDetail.taskDocuments.downloadDocument.cando,
    viewDocument: !currentTask.isDeleted && permissions.task.taskDetail.taskDocuments.cando,
    deleteDocument:
      !currentTask.isDeleted && permissions.task.taskDetail.taskDocuments.deleteDocument.cando,
    openFolder:
      !currentTask.isDeleted && permissions.task.taskDetail.taskDocuments.openFolder.cando,
    renameFolder:
      !currentTask.isDeleted && permissions.task.taskDetail.taskDocuments.renameFolder.cando,
  };
  let notifications = notificationsData();
  const open = Boolean(anchorEl);
  let template = getTemplate(currentTask);
  let statusListArr = template.statusList
    ? template.statusList
    : taskDetailDialog.workspaceStatus
      ? taskDetailDialog.workspaceStatus.statusList
      : [];
  const taskStatusData = statusData(statusListArr || []);
  let selectedStatus =
    currentTask.status == -1
      ? taskStatusData[0]
      : taskStatusData.find(item => item.value == currentTask.status) || {};
  const doneStatus = taskStatusData.find(s => s.obj.isDoneState);
  const customFieldPermission =
    permissions.task && !currentTask.isDeleted
      ? permissions.task.taskDetail.customsFields
      : {
        isAllowEdit: false,
        isAllowDelete: false,
        isAllowAdd: false,
      };
  const canEditTaskProject = currentTask.isDeleted
    ? false
    : permissions.task
      ? permissions.task.taskDetail.taskProject.isAllowEdit
      : false;
  const canEditTaskDescription = currentTask.isDeleted
    ? false
    : permissions.task
      ? permissions.task.taskDetail.taskDescription.isAllowEdit
      : true;
  const activeWorkspace = workspaces.find(w => {
    return w.teamId == loggedInTeam
  })
  const canEditStartDate = currentTask.isDeleted
    ? false
    : permissions.task
      ? permissions.task.taskDetail.taskPlannedStartEndDate.isAllowEdit
      : false;
  const canEditDueDate = currentTask.isDeleted
    ? false
    : permissions.task
      ? permissions.task.taskDetail.taskActualStartEndDate.isAllowEdit
      : false;

  const canHideField =
    permissions.task && !currentTask.isDeleted
      ? permissions.task.taskDetail.isHideField.cando
      : false;
  const canUpdateField =
    permissions.task && !currentTask.isDeleted
      ? permissions.task.taskDetail.isUpdateField.cando
      : false;
  const canUseField =
    permissions.task && !currentTask.isDeleted
      ? permissions.task.taskDetail.isUseField.cando
      : false;
  const membersObjArr =
    currentTask && currentTask.assigneeList
      ? members.filter(m => currentTask.assigneeList.includes(m.userId))
      : [];
  const taskEffortAdded =
    currentTask && currentTask.userTaskEffort ? currentTask.userTaskEffort.length > 0 : false;
  const projectAccess = companyInfo?.project?.isHidden ? false : true;

  const canAccessField = ({ feature, group }) => {
    if (searchQuery.includes("/reports")) {
      return TeamCanAccessFeature({ group, feature, workspaceId: currentTask.teamId || "" });
    }
    else {
      return CanAccess({ group, feature })
    }
  }

  const currentWorkspace = workspaces.find(w => w.teamId === currentTask.teamId) || {}; // get worksapce id from task detail data
  const obj = currentTask?.projectId ? { projectId: currentTask.projectId } : { workspaceId: currentTask.teamId };
  const calendarWorkingdays = getCalendar(obj) || null;
  const globalTimeIncrement = GlobalTimeCustom(currentTask.teamId) || {};

  return Object.keys(currentTask).length ? (
    <DetailsDialog
      dialogProps={{
        open: dialogsState.taskDetailDialog.id !== "",
        onClose: () => {
          handleCloseDialog();
        },
        disableBackdropClick: true,
        disableEscapeKeyDown: true,
      }}
      headerRenderer={
        <>
          <Timer
            style={{ marginRight: 10 }}
            startTime={startTime}
            stopTime={stopTime}
            allMembers={members}
            taskId={currentTask.taskId}
            currentTask={currentTask}
            updateTask={obj => {
              updateTaskData(
                { task: currentTask, obj },
                dispatch,
                res => {
                  if (grid.grid) grid.grid.redrawRows([res]);
                },
                error => {
                  const err = error.data.message || "";
                  const message = err || "Server throws error";
                  showSnackBar(message, "error");
                }
              );
            }}
            handleRemoveUserTaskEffort={handleRemoveUserTaskEffort}
            isArchivedSelected={currentTask.isDeleted}
            showSnackBar={showSnackBar}
            taskPer={permissions.task}
            miliSecToMinus={0}
            customBtnProps={{
              style: {
                height: 32,
              },
            }}
            timeBtnProps={{
              style: {
                height: 32,
              },
            }}
            globalTimeIncrement={globalTimeIncrement}
          />
          <div>
            {canAccessField({ group: 'task', feature: 'statusTitle' }) &&
              <StatusDropdown
                onSelect={status => {
                  handleUpdateStatus(status);
                }}
                iconButton={false}
                option={selectedStatus}
                style={{ marginRight: 8 }}
                options={taskStatusData}
                toolTipTxt={selectedStatus.label}
                disabled={
                  currentTask.isDeleted
                    ? true
                    : !permissions.task.taskDetail.editTaskStatus.isAllowEdit
                }
                legacyButton={true}
                btnStyle={{
                  padding: "8px 0px",
                  marginRight: 15,
                  height: 32,
                }}
                dropdownProps={{
                  // placement: "top-start",
                  disablePortal: true,
                }}
                writeFirst={true}
              />}
          </div>
          {canAccessField({ group: 'task', feature: 'assigneeList' }) &&
            <AssigneeDropdown
              closeTaskDetailsPopUp={closeTaskDetailsHandler}
              assignedTo={membersObjArr || []}
              updateAction={updateAssignee}
              isArchivedSelected={currentTask.isDeleted}
              obj={currentTask}
              addPermission={
                permissions.task ? permissions.task.taskDetail.taskAssign.isAllowAdd : false
              }
              deletePermission={
                permissions.task ? permissions.task.taskDetail.taskAssign.isAllowDelete : false
              }
              customIconButtonProps={{
                classes: {
                  root: classes.iconBtnStyles,
                },
              }}
              totalAssigneeBtnProps={{
                style: {
                  height: 29,
                  width: 29,
                  fontSize: "14px",
                },
              }}
              avatarSize={"xsmall"}
            />}
          <div className={classes.vl}></div>

          <div style={{ position: "relative" }}>
            {teamCanView("repeatTaskAccess") && (
              <CustomIconButton
                onClick={repeatDropdownOpen}
                variant="text"
                btnType="plain"
                style={{
                  borderRadius: "50%",
                  border: "none",
                  marginRight: 8,
                  background: "transparent",
                }}
                buttonRef={anchorEl}
                disabled={
                  permissions.task && !currentTask.isDeleted
                    ? !permissions.task.taskDetail.repeatTask.isAllowAdd
                    : true
                }>
                <SvgIcon
                  viewBox="0 0 14 12.438"
                  htmlColor={
                    currentTask.repeatTask
                      ? theme.palette.primary.light
                      : theme.palette.secondary.medDark
                  }
                  className={classes.recurrenceIcon}>
                  <RecurrenceIcon />
                </SvgIcon>
              </CustomIconButton>
            )}
            {open && (
              <DropdownMenu
                open={open}
                anchorEl={anchorEl}
                placement="bottom-end"
                style={{ width: 600, position: "absolute", top: 40, right: 0 }}
                id="taskRepeatDropdown"
                closeAction={closeRepeatDropdown}>
                <div>
                  <RepeatTask
                    closeAction={closeRepeatDropdown}
                    task={currentTask}
                    label
                    taskPer={permissions.task}
                  />
                </div>
              </DropdownMenu>
            )}
          </div>
          {profileState.data.isSlackLinked ? (
            <SlackChannelIntegration
              task={currentTask}
              updateTask={updateTask}
              isDisabled={false}
              customButtonProps={{
                style: {
                  marginRight: 8,
                  minWidth: 0,
                  marginBottom: -6,
                },
              }}
            />
          ) : null}
          <NotificationMenu
            notificationsData={notifications}
            MarkAllNotificationsAsRead={handleAllRead}
            history={history}
            customButtonProps={{
              style: {
                marginRight: 5,
                minWidth: 0,
              },
            }}
          />
          <TaskActionDropdown
            data={currentTask}
            btnProps={{ style: { marginRight: 10, transform: "rotate(90deg)", padding: 0 } }}
            handleActivityLog={activityLog}
            handleCloseCallBack={() => handleCloseDialog()}
            onDeleteTask={taskDetailDialog.onDeleteTask}
            GanttCmp={taskDetailDialog.GanttCmp}
            childTasks={taskDetailDialog.childTasks}
            isGantt={taskDetailDialog.isGantt}
            activityLogOption={true}
            isArchived={currentTask.isDeleted}
            taskPermission={permissions?.task}
          />
        </>
      }
      leftSideContent={
        <>
          <div className={classes.createdBy}>
            <span>
              <b>{`#${currentTask.uniqueId}`}</b>
              {` by ${currentTask.createdBy} on ${moment(currentTask.createdDate).format(
                "ll"
              )} at ${moment(currentTask.createdDate).format("LT")}`}
            </span>
            <EditableTextField
              title={currentTask.taskTitle}
              module={"Task"}
              handleEditTitle={handleUpdateTitle}
              permission={
                currentTask.isDeleted
                  ? false
                  : permissions.task
                    ? permissions.task.taskDetail.editTaskName.isAllowEdit
                    : true
              }
              defaultProps={{
                inputProps: { maxLength: 250 },
              }}
            />
          </div>

          <div className={classes.systemFields}>
            <div>
              <div
                className={classes.systemFieldsHeading}
                style={{ background: "#89A3B2" }}
                onClick={() => {
                  handleShowSystemField();
                }}>
                <Typography
                  className={classes.systemFieldTitle}
                  onClick={() => {
                    handleShowSystemField();
                  }}>
                  System Fields
                </Typography>
                <div>
                  {sFSecOpen ? (
                    <RemoveIcon
                      className={classes.systemFieldRemoveIcon}
                      onClick={() => {
                        handleShowSystemField();
                      }}
                    />
                  ) : (
                    <AddIcon
                      className={classes.systemFieldAddIcon}
                      onClick={() => {
                        handleShowSystemField();
                      }}
                    />
                  )}
                </div>
              </div>
              {sFSecOpen && (
                <>
                  <Grid container spacing={1} classes={{ container: classes.sectionCnt }}>

                    {teamCanView("projectAccess") && canAccessField({ group: 'task', feature: 'project' }) &&
                      (
                        <Grid item sm={12} md={6}>
                          {projectAccess && (
                            <div className={classes.textFieldCnt}>
                              <ProjectDropdown
                                taskData={currentTask}
                                projectId={currentTask.projectId}
                                workspaceId={currentTask.teamId}
                                isGantt={false}
                                getGanttTasks={getGanttTasks}
                                isDisabled={
                                  currentTask.isDeleted ||
                                  !canEditTaskProject ||
                                  taskEffortAdded ||
                                  taskDetailDialog.isTaskOverview
                                }
                                handleSelectHideOption={option =>
                                  handleSystemOptionSelect(option, accessFeaturesTask['project'])
                                }
                                hideOperation={canHideField}
                              />
                            </div>
                          )}
                        </Grid>
                      )}
                    {canAccessField({ group: 'task', feature: 'priority' }) && (
                      <Grid item sm={12} md={projectAccess ? 6 : 12}>
                        <div className={classes.textFieldCnt}>
                          <CustomFieldLabelCmp
                            label={"Priority"}
                            iconType={"dropdown"}
                            handleDelete={() => { }}
                            handleEdit={() => { }}
                            handleCopy={() => { }}
                            handleSelectHideOption={option =>
                              handleSystemOptionSelect(option, accessFeaturesTask['priority'])
                            }
                            deleteOperation={false}
                            editOperation={false}
                            copyOperation={false}
                            hideOperation={canHideField}
                            customIconProps={{
                              color: "#999",
                            }}
                          />
                          <SelectSearchDropdown /* Priority select drop down */
                            data={() => {
                              return priorityData(theme, classes, intl);
                            }}
                            selectChange={handlePrioritySelect}
                            type="priority"
                            selectedValue={priorityData(theme, classes, intl).find(s => {
                              return s.value == priorities[currentTask.priority];
                            })}
                            placeholder="Select"
                            icon
                            isMulti={false}
                            isDisabled={
                              !currentTask.isDeleted && permissions.task
                                ? !permissions.task.taskDetail.editTaskPriority.isAllowEdit
                                : true
                            }
                            styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                            customStyles={{
                              control: {
                                border: "none",
                                // padding: "0px 5px",
                                fontFamily: theme.typography.fontFamilyLato,
                                fontWeight: theme.typography.fontWeightExtraLight,
                                "&:hover": {
                                  background: `${theme.palette.background.items} !important`,
                                },
                                minHeight: 32,
                              },
                            }}
                            writeFirst={true}
                          />
                        </div>
                      </Grid>
                    )}
                    {canAccessField({ group: 'task', feature: 'startDate' }) && (
                      <Grid item sm={12} md={6}>
                        <TaskDateDropDown
                          label={"Planned Start"}
                          timeInputLabel={"Start Time"}
                          date={currentTask.startDate}
                          selectedTime={currentTask.startTime}
                          id={"startDate"}
                          handleSelectDate={handleDateUpdate}
                          permission={canEditStartDate}
                          customProps={{
                            datePickerProps: {
                              // exception handling
                              // excludeDates: [new Date(2022, 11, 14), new Date(2022, 11, 13)],
                              // dayClassName: (date) => {
                              //   return !excludeOffdays(date, calendarWorkingdays) ? "customDisabled" : null
                              // },
                              filterDate: date => {
                                return excludeOffdays(moment(date), calendarWorkingdays) ?
                                  currentTask.dueDate
                                    ? moment(date).isBefore(currentTask.dueDate, "day") ||
                                    moment(date).isSame(currentTask.dueDate, "day")
                                    : true
                                  : false;
                                // return currentTask.dueDate
                                //   ? date.isBefore(currentTask.dueDate, "day") ||
                                //   date.isSame(currentTask.dueDate, "day")
                                //   : true
                              },
                            },
                          }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesTask['startDate'])
                          }
                          hideOperation={canHideField}

                        />
                      </Grid>
                    )}
                    {canAccessField({ group: 'task', feature: 'dueDate' }) && (
                      <Grid item sm={12} md={6}>
                        <TaskDateDropDown
                          label={"Planned End"}
                          timeInputLabel={"End Time"}
                          date={currentTask.dueDate}
                          selectedTime={currentTask.dueTime}
                          id={"dueDate"}
                          handleSelectDate={handleDateUpdate}
                          permission={canEditStartDate}
                          customProps={{
                            datePickerProps: {
                              // dayClassName: (date) => {
                              //   return !excludeOffdays(new Date(date), calendarWorkingdays) ? "customDisabled" : null
                              // },
                              filterDate: date => {
                                return excludeOffdays(moment(date), calendarWorkingdays) ?
                                  currentTask.startDate
                                    ? moment(date).isAfter(currentTask.startDate, "day") ||
                                    moment(date).isSame(currentTask.startDate, "day")
                                    : true
                                  : false;
                                // return currentTask.startDate
                                //   ? date.isAfter(currentTask.startDate, "day") ||
                                //   date.isSame(currentTask.startDate, "day")
                                //   : true;
                              },
                            },
                          }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesTask['dueDate'])
                          }
                          hideOperation={canHideField}

                        />
                      </Grid>
                    )}
                    {canAccessField({ group: 'task', feature: 'actualStartDate' }) && (
                      <Grid item sm={12} md={6}>
                        <TaskDateDropDown
                          label={"Actual Start"}
                          timeInputLabel={"Start Time"}
                          date={currentTask.actualStartDate}
                          selectedTime={currentTask.actualStartTime}
                          id={"actualStartDate"}
                          handleSelectDate={handleDateUpdate}
                          permission={canEditDueDate}
                          customProps={{
                            datePickerProps: {
                              // dayClassName: (date) => {
                              //   return !excludeOffdays(date, calendarWorkingdays) ? "customDisabled" : null
                              // },
                              filterDate: date => {
                                return currentTask.actualDueDate
                                  ? moment(date).isBefore(currentTask.actualDueDate, "day") ||
                                  moment(date).isSame(currentTask.actualDueDate, "day")
                                  : true;
                              },
                            },
                          }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesTask['actualStartDate'])

                          }
                          hideOperation={canHideField}

                        />
                      </Grid>
                    )}
                    {canAccessField({ group: 'task', feature: 'actualDueDate' }) && (
                      <Grid item sm={12} md={6}>
                        <TaskDateDropDown
                          label={"Actual End"}
                          timeInputLabel={"End Time"}
                          date={currentTask.actualDueDate}
                          selectedTime={currentTask.actualDueTime}
                          id={"actualDueDate"}
                          handleSelectDate={handleDateUpdate}
                          permission={canEditDueDate}
                          customProps={{
                            datePickerProps: {
                              // dayClassName: (date) => {
                              //   return !excludeOffdays(date, calendarWorkingdays) ? "customDisabled" : null
                              // },
                              filterDate: date => {
                                return currentTask.actualStartDate
                                  ? moment(date).isAfter(currentTask.actualStartDate, "day") ||
                                  moment(date).isSame(currentTask.actualStartDate, "day")
                                  : true;
                              },
                            },
                          }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesTask['actualDueDate'])

                          }
                          hideOperation={canHideField}
                        />
                      </Grid>
                    )}
                    <Grid item sm={12} md={6}>
                      <ProgressField
                        label={"Duration"}
                        timeInputLabel={"Set Duration"}
                        intVal={currentTask.msProjectDuration}
                        id={"duration"}
                        iconType={'duration'}
                        handleSetNumberVal={handleUpdateDuration}
                        minVal={0}
                        maxVal={9999}
                        permission={!currentTask.isDeleted}
                        hideOperation={false}
                      />
                    </Grid>
                    {canAccessField({ group: 'task', feature: 'description' }) && (
                      <Grid item sm={12} md={12}>
                        <TaskDescriptionEditor
                          taskData={currentTask}
                          permission={canEditTaskDescription}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesTask['description'])
                          }
                          hideOperation={canHideField}

                        />
                      </Grid>
                    )}
                    <Grid item sm={12} md={6}>
                      <ProgressField
                        label={"Progress"}
                        timeInputLabel={"Set Progress"}
                        intVal={currentTask.progress}
                        id={"progress"}
                        iconType={'progress'}
                        handleSetNumberVal={handleProgressUpdate}
                        permission={activeWorkspace.taskProgressCalculation && !currentTask.isDeleted}
                        minVal={0}
                        maxVal={100}
                        defaultProps={{ endAdornment: "%" }}
                        hideOperation={false}
                      />
                    </Grid>
                    {canAccessField({ group: 'task', feature: 'to-doItemsList' }) && (
                      <Grid item sm={12} md={12}>
                        <div className={classes.textFieldCnt}>
                          <CustomFieldLabelCmp
                            label={"To-do Items List"}
                            iconType={"checklist"}
                            handleDelete={() => { }}
                            handleEdit={() => { }}
                            handleCopy={() => { }}
                            handleSelectHideOption={option =>
                              handleSystemOptionSelect(option, accessFeaturesTask['to-doItemsList'])

                            }
                            deleteOperation={false}
                            editOperation={false}
                            copyOperation={false}
                            hideOperation={true}
                            customIconProps={{
                              color: "#999",
                            }}
                          />
                          <TodoList
                            currentTask={currentTask}
                            closeTaskDetailsPopUp={closeTaskDetailsHandler}
                            todoItems={taskTodoItems || []}
                            isArchivedSelected={currentTask.isDeleted}
                            permission={true}
                            intl={intl}
                            progressBar={true}
                            draggable={true}
                            style={{ paddingRight: 220 }}
                            taskPer={permissions.task}
                          />
                        </div>
                      </Grid>
                    )}
                  </Grid>
                </>
              )}
            </div>
            <SectionsView
              type={"task"}
              disabled={currentTask.isDeleted}
              customFieldData={currentTask.customFieldData}
              permission={{
                ...customFieldPermission,
                isAllowUpdate: canUpdateField,
                isAllowHide: canHideField,
                isUseField: canUseField,
              }}
              workspaceId={currentTask.teamId}
              customFieldChange={customFieldChange}
              handleEditField={handleEditField}
              handleCopyField={handleCopyField}
              groupId={currentTask.taskId}
              handleClickHideOption={handleClickHideOption}
              handleSelectHideOption={handleSelectHideOption}
            />
          </div>

          {/* if bussiness plan then show add custom field component */}
          {teamCanView("customFieldAccess") && !searchQuery.includes("/reports/taskOverview") && (
            <div className={classes.addCustomFieldCnt}>
              <CustomFieldView
                feature="task"
                onFieldAdd={() => { }}
                onFieldEdit={editCustomFieldCallback}
                onFieldDelete={() => { }}
                permission={{
                  ...customFieldPermission,
                  isAllowUpdate: canUpdateField,
                  isAllowHide: canHideField,
                  isUseField: canUseField,
                }}
                disableAssessment={true}
                disabled={currentTask.isDeleted}
              />
            </div>
          )}
          <ActionConfirmation
            open={markChecklistActionConf}
            closeAction={markAllConfirmDialogClose}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              otherStatus && isDoneStatus ? <FormattedMessage
                id="task.detail-dialog.to-do-list.mark-all.action"
                defaultMessage="Yes, Mark All Complete"
              /> : otherStatus && !isDoneStatus ? <>Yes</> :
                <FormattedMessage
                  id="task.detail-dialog.to-do-list.mark-all.action"
                  defaultMessage="Yes, Mark All Complete"
                />
            }
            alignment="center"
            headingText={
              otherStatus && isDoneStatus ?
              <FormattedMessage
                id="task.detail-dialog.to-do-list.mark-all.title"
                defaultMessage="Mark All Complete"
              /> : otherStatus && !isDoneStatus ?
                <>Reminder - Task Effort</> :
                <FormattedMessage
                  id="task.detail-dialog.to-do-list.mark-all.title"
                  defaultMessage="Mark All Complete"
                />
            }
            iconType="markAll"
            msgText={
              otherStatus && isDoneStatus ? <>
                Are you sure you want to mark all to-do list items as completed?
                <br />
                Make sure you have added the time effort before updating task status
              </> : otherStatus && !isDoneStatus ?
                <> Make sure you have added the time effort before updating task status</> :
                <FormattedMessage
                  id="task.detail-dialog.to-do-list.mark-all.message"
                  defaultMessage="Are you sure you want to mark all to-do list items as completed?"
                />
            }
            successAction={() => otherStatus ? handleUpdateStatusConformation() : handleStatusChange(doneStatus)}
            btnQuery={""}
          />
        </>
      }
      rightSideContent={
        <>
          <SideTabs
            taskData={currentTask}
            showActivity={showActivity}
            members={members}
            hideActivityLog={hideActivityLog}
            isArchivedSelected={currentTask.isDeleted}
            type={taskDetailDialog.type}
            permission={true}
            taskActivities={taskActivitiesState.data ? taskActivitiesState.data : []}
            taskPer={permissions.task}
            meetingPermission={permissions.meeting}
            issuePermission={permissions.issue}
            riskPermission={permissions.risk}
            onChatMount={() => { }}
            onChatUnMount={() => { }}
            chatConfig={chatConfig}
            chatPermission={chatPermission}
            docConfig={chatConfig}
            docPermission={docPermission}
            selectedTab={0}
            intl={intl}
          />
        </>
      }
    />
  ) : (
    <> </>
  );
}

NewTaskDetailView.defaultProps = {
  /** default props , calls when props are not coming from parent class undefined */
  theme: {},
  classes: {},
};

const mapStateToProps = state => {
  return {
    dialogsState: state.dialogStates,
    loggedInTeam: state.profile.data.loggedInTeam,
    members:
      state.profile && state.profile.data && state.profile.data.member
        ? state.profile.data.member.allMembers
        : [],
    appliedFilters: state.appliedFilters,
    tasks: state.tasks.data || [],
    itemOrderState: state.itemOrder.data,
    // This data is for calculating count of respective object linked with task
    issues: state.issues.data || [],
    risks: state.risks.data || [],
    meetings: state.meetings.data || [],
    workspaces: state.profile.data.workspace,
    taskPer: state.workspacePermissions.data.task,
    companyInfo: state.whiteLabelInfo.data,
    projects: state.projects.data || [],
    meetingPer: state.workspacePermissions.data.meeting,
    riskPer: state.workspacePermissions.data.risk,
    issuePer: state.workspacePermissions.data.issue,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    taskActivitiesState: state.taskActivities,
    taskTodoItems: state.todoItemsData.data.todoItems,
    taskNotificationsState: state.taskNotifications,
    profileState: state.profile,
    archivedTasks: state.archivedItems.data,
    permissionObject: ProjectPermissionSelector(state),
    customFields: state.customFields,
    accessFeaturesTask: state.featureAccessPermissions.task,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateTask,
    FetchTasksInfo,
    CopyTask,
    DeleteTask,
    ArchiveTask,
    UnArchiveTask,
    startTimer,
    stopTimer,
    removeUserEffort,
    UpdateGantTask,
    MarkTaskNotificationsAsRead,
    getTaskComments,
    removeTaskComments,
    CheckAllTodoList,
    UpdateTaskCustomField,
    dispatchTask,
    bulkUpdateTask,
    UpdateCalenderTask,
    CopyCalenderTask,
    UpdateTaskColorFromGridItem,
    dispatchAddTodoItems,
    getGanttTasks,
    setGlobalTaskTime,
    removeGlobalTaskTime,
    updateCustomFieldDialogState,
    editCustomField,
    updateCustomFieldData,
    hideCustomField,
    clearDocuments,
    updateFeatureAccessPermissions
  })
)(NewTaskDetailView);
