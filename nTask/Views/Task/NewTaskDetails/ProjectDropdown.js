import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import autoCompleteStyles from "../../../assets/jss/components/autoComplete";
import { UpdateTask, UpdateGantTask, updateTaskData } from "../../../redux/actions/tasks";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import { FormattedMessage } from "react-intl";
import TaskStatusChangeDialog from "../../../components/Templates/TaskStatusChangeDialog";
import CustomFieldLabelCmp from "../../../components/CustomFieldsViews/CustomFieldLabel.cmp";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { getTemplate } from "../../../utils/getTemplate";
import CircularIcon from "@material-ui/icons/Brightness1";
import { toast } from "react-toastify";
import { toastMessages } from "../../../helper/customHooks/tasks/toastMessages";
import { ProjectMandatory } from "../../../helper/config.helper";
import { TeamCanAccessFeature } from "../../../components/AccessFeature/AccessFeature.cmp";

class SelectProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AssignedToLabel: false,
      AddToProjectLabel: false,
      value: [],
      projectOptions: [],
      AssignEmailError: false,
      taskTitle: undefined,
      selectedProjectOption: null,
      openStatusDialog: false,
      newTemplateItem: null,
      oldStatusItem: null,
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  componentDidMount() {
    const { projectId, workspaceId } = this.props;
    let projectOptions =
      this.props.projectsState &&
      this.props.projectsState.data &&
      this.props.projectsState.data.length
        ? this.props.projectsState.data
        : [];

    let selectedProjectOption = null;
    if (projectId) {
      selectedProjectOption = projectOptions.find(x => x.projectId === this.props.projectId);
    }
    if (projectOptions.length){
      projectOptions.forEach(function(obj) {
        obj.Active = "false";
        obj.value = obj.projectName;
        obj.label = obj.projectName;
      });
    }
    if(workspaceId) {
      projectOptions = projectOptions.filter(p => p.teamId === workspaceId); /** filter projects on the basics of current task workspace */
    }
    projectOptions = projectOptions || [];
    this.setState({ projectOptions, selectedProjectOption });
  }

  componentDidUpdate(prevProps, prevState) {
    const { projectId, workspaceId, projectsState } = this.props;
    if (projectId !== prevProps.projectId) {
      let selectedProjectOption = this.state.projectOptions.find(
        x => x.projectId === projectId
      );
      this.setState({ selectedProjectOption });
    }
    if (
      JSON.stringify(projectsState.data) !== JSON.stringify(prevProps.projectsState.data)
    ) {
      let projectOptions =
        projectsState &&
        projectsState.data &&
        projectsState.data.length
          ? projectsState.data
          : [];
      if(workspaceId) {
        projectOptions = projectOptions.filter(p => p.teamId === workspaceId); /** filter projects on the basics of current task workspace */
      }
      this.setState({ projectOptions });
    }
  }
  showMappingDialog = projectId => {
    const { workspaceStatus } = this.props;
    let attachedProject = this.props.projectsState.data.find(p => p.projectId == projectId);
    return attachedProject && attachedProject.projectTemplateId != workspaceStatus.templateId;
  };
  getDefaultTemplateProject = item => {
    const { workspaceStatus } = this.props;
    if (item) {
      let attachedProject = this.props.projectsState.data.find(p => p.projectId == item.projectId);
      if (attachedProject && attachedProject.projectTemplate) {
        return attachedProject.projectTemplate;
      } else {
        return workspaceStatus;
      }
    } else {
      return workspaceStatus;
    }
  };

  statusData = statusArr => {
    return statusArr.map(item => {
      return {
        label: item.statusTitle,
        value: item.statusId,
        icon: <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "20px" }} />,
        statusColor: item.statusColor,
        obj: item,
        statusTitle: item.statusTitle,
      };
    });
  };
  
  handleUpdateTaskProject = (key, projectId) => {
    let updatedTask = { ...this.props.taskData };
    switch (key) {
      case 'removeProject': {
        if (this.props.isGantt) {
          updatedTask.projectId = "";
          this.props.UpdateGantTask(updatedTask, (err, response) => {
            if (response) {
              response = response.data.CalenderDetails;
            }
          });
        } else {
          let obj = {
            projectId: "",
          };
          this.props.updateTaskData({ task: updatedTask, obj });
        }
      }
        break;
      case 'linkProject': {
        if (this.props.isGantt) {
          updatedTask.projectId = projectId;
          this.props.UpdateGantTask(updatedTask, (err, response) => {
            if (response) {
              response = response.data.CalenderDetails;
            }
          });
        } else {
          let obj = {
            projectId: projectId,
          };
          this.props.updateTaskData({ task: updatedTask, obj });
        }
      }
        break;
    
      default:
        return;
        break;
    }
  }

  handleSelectChange(newValue, actionMeta) {
    const { workspaceId } = this.props;
    const message = toastMessages.project;
    const defaultMessage = toastMessages.default;
    const loadingMessage = message ? message.loading : defaultMessage.loading;
    const successMessage = message ? message.success : defaultMessage.success;
    const projectUpdateToast = toast.loading(loadingMessage);
    const projectId = newValue ? newValue.projectId : this.props.taskData.projectId;

    if(newValue == null && !TeamCanAccessFeature({group:'task' , feature:'statusTitle', workspaceId: workspaceId})){ 
      /** if user is removing project and in task module the status feature is hide then do not show mapping dialogue, default workspace initial status will be added to the task  */
      this.handleUpdateTaskProject('removeProject');
      return;
    }
    if(newValue && !TeamCanAccessFeature({group:'task' , feature:'statusTitle', workspaceId: workspaceId})){ 
      /** if user is removing project and in task module the status feature is hide then do not show mapping dialogue, default workspace initial status will be added to the task  */
      this.handleUpdateTaskProject('linkProject', projectId);
      return;
    }
    if (!this.showMappingDialog(projectId)) {
      let obj = { projectId: newValue ? newValue.projectId : "" };
      this.props.updateTaskData({ task: this.props.taskData, obj }, null, () => {
        toast.update(projectUpdateToast, {
          render: successMessage,
          type: "success",
          isLoading: false,
          autoClose: 3000,
          hideProgressBar: false,
          pauseOnHover: true,
          closeOnClick: true,
          draggable: true,
        });
      });
      // this.props.UpdateTask(newTaskObj, () => {});
    } 
    else {
      let taskTemplate = getTemplate(this.props.taskData);
      const taskStatusData = this.statusData(taskTemplate ? taskTemplate.statusList : false);
      let taskStatus = taskStatusData.find(el => el.value == this.props.taskData.status);
      let template = this.getDefaultTemplateProject(newValue);
      this.setState({
        tempProject: newValue || null,
        openStatusDialog: true,
        newTemplateItem: template,
        oldStatusItem: taskStatus,
      });
    }
  }
  handleCloseDialog = () => {
    let selectedProjectOption = null;
    if (this.props.projectId) {
      selectedProjectOption =
        this.state.projectOptions.find(x => x.projectId === this.props.projectId) || null;
    }
    this.setState({
      openStatusDialog: false,
      newTemplateItem: null,
      oldStatusItem: null,
      selectedProjectOption,
    });
  };
  handleSaveTemplate = newStatusItem => {
    const { tempProject } = this.state;
    let updatedTask = { ...this.props.taskData };
    updatedTask.projectId = tempProject ? tempProject.projectId : null;
    updatedTask.status = newStatusItem.statusId;
    updatedTask.statusTitle = newStatusItem.statusTitle;
    this.setState(
      {
        selectedProjectOption: tempProject || null,
        openStatusDialog: false,
        newTemplateItem: null,
        oldStatusItem: null,
      },
      () => {
        if (this.props.isGantt) {
          this.props.UpdateGantTask(updatedTask, (err, response) => {
            if (response) {
              response = response.data.CalenderDetails;
            }
          });
        } else {
          let obj = {
            projectId: tempProject ? tempProject.projectId : "",
            status: newStatusItem.statusId,
          };
          this.props.updateTaskData({ task: this.props.taskData, obj });
        }
      }
    ); //selectedProjectOption
  };
  searchDropDown = () => {
    const { classes, theme, taskData, isDisabled, customDropDownProps = {}, workspaceId } = this.props;
    return (
      <>
        <SelectSearchDropdown
          data={() => {
            return this.state.projectOptions;
          }}
          isClearable={!ProjectMandatory(workspaceId)} /** if project is mandatory then project clear option will be disable */
          placeholder={"Select"}
          optionBackground={false}
          isMulti={false}
          selectChange={(type, option) => {
            this.handleSelectChange(option, type);
          }}
          selectedValue={this.state.selectedProjectOption}
          selectClear={(type, option) => {
            this.handleSelectChange(option, type);
          }}
          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
          customStyles={{
            control: {
              border: "none",
              // padding: "0px 5px",
              fontFamily: theme.typography.fontFamilyLato,
              fontWeight: theme.typography.fontWeightExtraLight,
              "&:hover": {
                background: `${theme.palette.background.items} !important`,
              },
              minHeight: 32,
            },
          }}
          isDisabled={isDisabled}
          {...customDropDownProps}
          writeFirst={false}          
        />
      </>
    );
  };

  render() {
    const {
      classes,
      theme,
      taskData,
      isDisabled,
      customDropDownProps = {},
      handleSelectHideOption = () => {},
      hideOperation= true
    } = this.props;
    const { openStatusDialog, newTemplateItem, oldStatusItem } = this.state;
    const taskEffortAdded =
      taskData && taskData.userTaskEffort ? taskData.userTaskEffort.length > 0 : false;
    return (
      <Fragment>
        {openStatusDialog && (
          <TaskStatusChangeDialog
            open={openStatusDialog}
            handleClose={this.handleCloseDialog}
            oldStatus={oldStatusItem}
            newTemplateItem={newTemplateItem}
            handleSaveAsTemplate={this.handleSaveTemplate}
          />
        )}
        <CustomFieldLabelCmp
          label={"Project"}
          iconType={"dropdown"}
          handleDelete={() => {}}
          handleEdit={() => {}}
          handleCopy={() => {}}
          handleSelectHideOption={handleSelectHideOption}
          deleteOperation={false}
          editOperation={false}
          copyOperation={false}
          hideOperation={hideOperation}
          customIconProps={{
            color: "#999",
          }}
        />
        {taskEffortAdded ? (
          <CustomTooltip
            placement="right"
            helptext={
              <FormattedMessage
                id="task.detail-dialog.project.hint1"
                defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
              />
            }>
            <div style={{ flex: 1 }}>{this.searchDropDown()}</div>
          </CustomTooltip>
        ) : (
          this.searchDropDown()
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    projectsState: state.projects,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
  };
};

export default compose(
  withRouter,
  withStyles(autoCompleteStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, { UpdateTask, UpdateCalenderTask, UpdateGantTask, updateTaskData })
)(SelectProject);
