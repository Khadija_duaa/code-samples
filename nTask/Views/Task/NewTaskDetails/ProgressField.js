import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import styles from "./style";
import CustomFieldLabelCmp from "../../../components/CustomFieldsViews/CustomFieldLabel.cmp";

import DefaultTextField from "../../../components/Form/TextField";
import { injectIntl } from "react-intl";
import EditableTextField from "../../../components/Form/EditableTextField";

function ProgressField({
  classes,
  theme,
  handleSetNumberVal,
  permission,
  label,
  intVal,
  intl,
  date,
  id,
  selectedTime,
  customProps,
  defaultProps,
  iconType,
  minVal,
  maxVal,
  handleSelectHideOption,
  hideOperation = true
}) {
  const [inputValue, setInputValue] = useState("");
  const [editTitle, setEditTitle] = useState(false);
  useEffect(() => {
    setInputValue(intVal ? intVal : "");
  }, [intVal])

  const processValue = (val) => {
    if (val <= maxVal && 0 <= val)
      return val.replace(/[^.\d]/g, "").replace(/^(\d*\.?)|(\d*)\.?/g, "$1$2")
    else return inputValue;
  };
  const handleProgressChange = (e) => {
    e.stopPropagation();
    if (maxVal) {
      setInputValue(processValue(e.target.value));
    } else {
      setInputValue(e.target.value);
    }
  };
  const handleKeyDown = (e) => {
    e.stopPropagation();
    setEditTitle(true)
    if (e.keyCode === 13) {
      /** on Enter click */
      handleSetNumberVal(inputValue);
      setInputValue(inputValue ? inputValue : "");
      setEditTitle(false)
    } else if (e.keyCode === 27) {
      /** on ESC click */
      setInputValue(inputValue ? inputValue : "");
      setEditTitle(false)
    }
  };

  return (
    <div className={classes.textFieldCnt}>
      <CustomFieldLabelCmp
        label={label}
        iconType={iconType}
        handleDelete={() => { }}
        handleEdit={() => { }}
        handleCopy={() => { }}
        handleSelectHideOption={handleSelectHideOption}
        deleteOperation={false}
        editOperation={false}
        copyOperation={false}
        hideOperation={hideOperation}
        customIconProps={{
          color: "#999",
        }}
      />
      <EditableTextField
        title={inputValue ? inputValue : ""}
        module={""}
        placeholder={label}
        titleProps={{
          className: classes.textField,
        }}
        editTitle={editTitle}
        textFieldProps={{
          customInputClass: {
            input: classes.outlinedInputsmall,
            root: classes.outlinedInputCnt,
          },
        }}
        formStyles={{
          height: "32px",
          borderRadius: 4,
        }}
        handleEditTitle={() => {
          setInputValue(intVal ? intVal : "");
        }}
        permission={permission}
        defaultProps={{
          inputProps: { maxLength: 250 },
          value: inputValue,
          onChange: e => handleProgressChange(e),
          onKeyDown: e => handleKeyDown(e),
          ...defaultProps,
        }}
      />
    </div>
  );
}

ProgressField.defaultProps = {
  classes: {},
  theme: {},
  handleSetNumberVal: () => { },
  handleSelectHideOption: () => { },
  permission: true,
  disabled: false,
  label: "",
  intVal: "",
  id: "",
  customProps: {},
  defaultProps: {},
  hideOperation: true
};

const mapStateToProps = state => {
  return {};
};

export default compose(
  withStyles(styles, { withTheme: true }),
  injectIntl,
  connect(mapStateToProps, {})
)(ProgressField);
