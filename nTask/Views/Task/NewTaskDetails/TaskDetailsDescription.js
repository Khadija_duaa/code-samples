import React, { Component, useEffect, useRef, useState } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { UpdateTask } from "../../../redux/actions/tasks";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import "../../../assets/css/richTextEditor.css";
import withStyles from "@material-ui/core/styles/withStyles";
import taskDetailStyles from "./style";
import { injectIntl, FormattedMessage } from "react-intl";
import DescriptionEditor from "../../../components/TextEditor/CustomTextEditor/DescriptionEditor/DescriptionEditor";
import CustomFieldLabelCmp from "../../../components/CustomFieldsViews/CustomFieldLabel.cmp";
import useUpdateTask from "../../../helper/customHooks/tasks/updateTask";

function TaskDescriptionEditor(props) {
  const [editorState, setEditorState] = useState(props.taskData.description || "");
  const [members, setMembers] = useState([]);
  const {
    classes,
    theme,
    permission,
    intl,
    profileState,
    taskData,
    handleSelectHideOption = () => {},
    hideOperation=true
  } = props;
  const { editTask } = useUpdateTask();
  const editorStateRef = useRef(props.taskData.description || "");
  useEffect(() => {
    let allMembers =
      profileState && profileState.data && profileState.data.member
        ? profileState.data.member.allMembers
        : [];
    let memberList = allMembers.map(x => {
      return {
        text: x.userName,
        value: x.userName,
        //  url: x.userName,
        userId: x.userId,
      };
    });
    setMembers({ members: memberList });
  }, []);

  useEffect(() => {
    setEditorState(taskData.description || "");
    editorStateRef.current = taskData.description || "";
  }, [props.taskData]);

  const handleTaskDescription = editorState => {
    setEditorState(editorState.html);
    editorStateRef.current = editorState.html;
  };
  const handleClickAwayTaskDescription = () => {
    if (editorStateRef.current != taskData.description) {
      editTask(taskData, "description", editorStateRef.current);
      // this.props.UpdateTask(taskData, response => {
      //   this.setState({
      //     // currentTask: currentTask,
      //   });
      // });
    }
  };

  return (
    <div className={classes.editorContainer}>
      <CustomFieldLabelCmp
        label={"Description"}
        iconType={"textarea"}
        handleDelete={() => {}}
        handleEdit={() => {}}
        handleCopy={() => {}}
        handleSelectHideOption={handleSelectHideOption}
        deleteOperation={false}
        editOperation={false}
        copyOperation={false}
        hideOperation={hideOperation}
        customIconProps={{
          color: "#999",
        }}
      />
      <div style={{ flex: 1, padding: "0px 8px 0px 4px" }}>
        <DescriptionEditor
          id="taskEditorTask"
          defaultValue={editorState}
          onChange={handleTaskDescription}
          handleClickAway={handleClickAwayTaskDescription}
          placeholder={intl.formatMessage({
            id: "common.type.label",
            defaultMessage: "Type something",
          })}
          disableEdit={!permission}
        />
      </div>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, { UpdateTask, UpdateCalenderTask })
)(TaskDescriptionEditor);
