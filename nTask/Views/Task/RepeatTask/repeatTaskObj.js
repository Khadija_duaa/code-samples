import moment from "moment"
export const repeatTaskInitObj = {
  daily: {
    repeatType: "Daily",
    repeatDetails: {
      repeatAt: {
        hours: "12",
        minutes: "00",
        timeFormat: "AM"
      },
      repeatDaily: {
        interval: 1,
        weekendTask: "0"
      },
      stopBy: {
        type: "Date",
        date: moment().format("MM/DD/YYYY"),
        value: "5"
      },
    }
  },
  weekly: {
    repeatType: "Weekly",
    repeatDetails: {
      repeatWeekly: {
        dayInitial: ["Monday"],
        interval: 1,
        weekendTask: "0"
      },
      repeatAt: {
        hours: "12",
        minutes: "00",
        timeFormat: "AM"
      },
      stopBy: {
        type: "Date",
        value: "5",
        date: moment().format("MM/DD/YYYY")
      }
    }
  },
  monthly: {
    repeatType: "Monthly",
    repeatDetails: {
      repeatMonthly: {
        timePeriod: "Day",
        dayNo: 1,
        weekNo: 1,
        dayInitial: 'Monday',
        monthNo: 1,
        interval: 1,
        weekendTask: "0"
      },
      repeatAt: {
        hours: "12",
        minutes: "00",
        timeFormat: "AM"
      },
      stopBy: {
        type: "Date",
        date: moment().format("MM/DD/YYYY"),
        value: "5"
      },
    }
  }
}

export const repeatTaskOptions = [
  {
    value: '0',
    lable: "Skip",
  },
  {
    value: '3',
    lable: "Before",
  },
  {
    value: '2',
    lable: "On",
  },
  {
    value: '1',
    lable: "After",
  },
];