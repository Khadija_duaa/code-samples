import moment from 'moment';

// Function returns info msg to be shown on each repeat task tab
function getInfoMessage({ repeatTypeTab, daily, weekly, monthly }) {

  const selectedPeriod = // Saving reference to object depending on the selected Tab
    repeatTypeTab == "left"
      ? "daily"
      : repeatTypeTab == "center"
        ? "weekly"
        : "monthly";
  if (selectedPeriod == "daily") {
    if (daily) {
      const repeatDetails = daily.repeatDetails; // repeatDetails contains details of task repetition, it's common in daily/weekly/montly/yearly object
      const intervalMsg =
        repeatDetails.repeatDaily.interval == 1
          ? "daily"
          : `after every  ${getabbreviation(repeatDetails.repeatDaily.interval)} day`;
      const time = `${repeatDetails.repeatAt.hours}:${repeatDetails.repeatAt.minutes} ${repeatDetails.repeatAt.timeFormat}`
      const endsOnMsg =
        repeatDetails.stopBy.type == "Date"
          ? `on ${moment(repeatDetails.stopBy.date).format("DD MMMM, YYYY")}`
          : `after ${repeatDetails.stopBy.value} tasks`;
      return `Task occurs ${intervalMsg} at ${time} and ends ${endsOnMsg}.`;
    }
  } else if (selectedPeriod == "weekly") {
    if (weekly) {
      const repeatDetails = weekly.repeatDetails; // repeatDetails contains details of task repetition, it's common in daily/weekly/montly/yearly object
      const time = `${repeatDetails.repeatAt.hours}:${repeatDetails.repeatAt.minutes} ${repeatDetails.repeatAt.timeFormat}`
      const intervalMsg =
        repeatDetails.repeatWeekly.interval == 1
          ? "every week"
          : `after every  ${getabbreviation(repeatDetails.repeatWeekly.interval)} week`;
      const weekDayName = getWeeks(repeatDetails.repeatWeekly.dayInitial);
      const weekDays = weekDayName.length
        ? `${weekDayName.toString().replace(/,/g, ', ')} at ${time}`
        : " ";
      const endsOnMsg =
        repeatDetails.stopBy.type == "Date"
          ? ` on ${weekDays} and ends on ${moment(
            repeatDetails.stopBy.date
          ).format("DD MMMM, YYYY")}`
          : ` on ${weekDays} and ends after ${repeatDetails.stopBy.value} tasks`;
      return `Task occurs ${intervalMsg} ${endsOnMsg}.`;
    }
  } else if (selectedPeriod == "monthly") {
    if (monthly) {
      const repeatDetails = monthly.repeatDetails; // repeatDetails contains details of task repetition, it's common in daily/weekly/montly/yearly object
      const weekTitle = getWeekTitle(repeatDetails.repeatMonthly.weekNo);
      const day =
        repeatDetails.repeatMonthly.timePeriod == "Day"
          ? `on ${getabbreviation(repeatDetails.repeatMonthly.dayNo)}`
          : `on the ${weekTitle} ${repeatDetails.repeatMonthly.dayInitial}`;
      const month =
        repeatDetails.repeatMonthly.monthNo == 1
          ? `of every month`
          : `of ${getabbreviation(repeatDetails.repeatMonthly.monthNo)} month`;
      const repeatAt = `at ${repeatDetails.repeatAt.hours}:${repeatDetails.repeatAt.minutes} ${repeatDetails.repeatAt.timeFormat}`;
      const stopBy =
        repeatDetails.stopBy.type == "Date"
          ? ` on ${moment(repeatDetails.stopBy.date).format("DD MMMM, YYYY")}`
          : `after ${repeatDetails.stopBy.value} tasks`;
      return `Task repeats ${day} ${month} ${repeatAt} and ends ${stopBy}.`;
    }
  }
};

function getabbreviation(num) {
  if (num == 1) {
    return `${num}st`
  }
  else if (num == 2) {
    return `${num}nd`
  }
  else if (num == 3) {
    return `${num}rd`
  }
  else {
    return `${num}th`
  }
}

function getWeekTitle(weekNo) {
  switch (weekNo) {
    case 1:
      return 'First';
      break;
    case 2:
      return 'Second';
      break;
    case 3:
      return 'Third';
      break;
    case 4:
      return 'Fourth';
      break;
    case 5:
      return 'Last';
      break;
    default:
      break;
  }
}

function getWeeks(day) {
  const rawDays = {
    Monday: false,
    Tuesday: false,
    Wednesday: false,
    Thursday: false,
    Friday: false,
    Saturday: false,
    Sunday: false
  }
  day.map(item => rawDays[item] = true);
  const weekDays = Object.keys(rawDays).filter(item => rawDays[item] == true);
  return weekDays
}

export default getInfoMessage