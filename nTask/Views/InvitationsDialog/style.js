const invitationStyles = (theme) => ({
  contentCnt: {
    padding: "16px 25px 30px 25px",
  },
  headingBar: {
    background: theme.palette.background.items,
    padding: 12,
    borderRadius: 4,
  },
  invitationList: {
    listStyleType: "none",
    padding: 0,
    margin: "0 0 16px 0",
    display: "flex",
    flexDirection: "column",
    "& li": {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      padding: "12px 8px",
      borderBottom: `1px solid ${theme.palette.border.extraLightBorder}`,
    },
  },
  invitationsCount: {
    color: theme.palette.text.light,
  },
  invitationBtnCnt: {
    display: "flex",
  },
  invitationDetailsCnt: {
    display: "flex",
    alignItems: "center",
  },
  invitationTeamDetails: {
    display: "flex",
    flexDirection: "column",
    marginLeft: 8,
  },
  inviteTeamText: {
    color: theme.palette.text.primary,
    display: "flex",
  },
  invitedByUser: {},
  teamName: {
    color: theme.palette.text.azure,
    overflow: "hidden",
    textOverflow: "ellipsis",
    width: 130,
    whiteSpace: "nowrap",
    display: "inline-block",
  },
  historyTableHeader: {
    borderBottom: `1px solid ${theme.palette.border.extraLightBorder}`,
    marginTop: 8,
    padding: 8,
  },
  headerTitle: {
    fontSize: "10px !important",
  },
  bodyRow: {
    padding: 8,
    borderBottom: `1px solid ${theme.palette.border.extraLightBorder}`,
    "& h6": {
      lineHeight: "normal",
      fontWeight: "",
      fontWeight: 600,
    },
  },
  inviteByColumn: {
    display: "flex",
    flexDirection: "column",
  },
  inviteToColumn: {
    display: "flex",
    alignItems: "center",
  },
  statusColumn: {
    display: "flex",
    flexDirection: "column",
  },
  emptryStateCnt: {
    padding: "36px 0",
    display: "flex",
    justifyContent: "center",
  },
  noInvitationsText: {
    fontSize: "18px !important",
    fontWeight: 500,
  },
});

export default invitationStyles;
