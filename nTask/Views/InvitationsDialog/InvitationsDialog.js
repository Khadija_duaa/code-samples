// @flow

import React, { useState, useEffect } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import invitationStyles from "./style";
import { withStyles } from "@material-ui/core/styles";
import MobileStepper from "@material-ui/core/MobileStepper";
import Logo from "../../assets/images/nTask-White-logo.svg";
import CustomDialog from "../../components/Dialog/CustomDialog";
import CustomAvatar from "../../components/Avatar/Avatar";
import CustomButton from "../../components/Buttons/CustomButton";
import { compose } from "redux";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";
import {
  getTeamInvitations,
  acceptTeamInvitation,
  rejectTeamInvitation,
} from "../../redux/actions/teamInvitations";
import moment from "moment";
import { FormattedMessage } from "react-intl";

type InvitationsDialogProps = {
  closeAction: Function,
  classes: Object,
  theme: Object,
  open: boolean,
  acceptTeamInvitation: Function,
  rejectTeamInvitation: Function,
};

const dummyData = [
  {
    fullName: "Elon Musk",
    email: "Elon@musk.com",
    imageUrl: "",
    teamName: "Tesla",
  },
  {
    fullName: "Fischer Garland",
    email: "Fischer@Garland.com",
    imageUrl: "",
    teamName: "Android Developers",
  },
];
//Invitations Dialog Main Component
function InvitationsDialog(props: InvitationsDialogProps) {
  const [pendingInvitations, setPendingInvitations] = useState([]);
  const [acceptedInvitations, setAcceptedInvitations] = useState([]);

  const handleDialogClose = () => {
    const { closeAction } = props;
    // Close Dialog Function
    closeAction();
  };

  useEffect(() => {
    getTeamInvitations(
      //Success
      (response) => {
        const pendingInvitationsList = response.data.filter((i) => {
          // Extracting pending invitations list
          return i.status == "Pending";
        });
        const acceptedInvitationsList = response.data.filter((i) => {
          // Extracting pending invitations list
          return i.status == "Accepted" || i.status == "Rejected";
        });
        setPendingInvitations(pendingInvitationsList);
        setAcceptedInvitations(acceptedInvitationsList);
      },
      //Failure
      () => {}
    );
  }, []);
  const updateInvitations = (invitation: Object) => {
    // Function that handles local state update of pending invitations and history
    const newPendingInvitationsList = pendingInvitations.filter((i) => {
      return i.team.teamId !== invitation.team.teamId;
    });
    setPendingInvitations(newPendingInvitationsList);
    setAcceptedInvitations([invitation, ...acceptedInvitations]);
  };

  const handleAcceptTeamInvitation = (invitation: Object) => {
    setBtnQuery("progress"); /** showing spinner in accept button  */
    // Function that handles accept invitation of team
    props.acceptTeamInvitation(
      invitation.team.teamId,
      (response) => {
        updateInvitations({
          ...invitation,
          status: "Accepted",
          actionDate: new Date(),
        }); // Updating local state after invitation is accepted
        setBtnQuery(""); /** disable spinner in accept button  */
      },
      (err) => {
        setBtnQuery(""); /** disable spinner in accept button  */
        if (err) showSnackBar(err.data.message, "error");
      },
      true
    );
  };

  const handleRejectTeamInvitation = (invitation: Object) => {
    setRejectBtnQuery("progress"); /** showing spinner in accept button  */
    // Function that handles accept invitation of team
    props.rejectTeamInvitation(
      invitation.team.teamId,
      (response) => {
        updateInvitations({
          ...invitation,
          status: "Rejected",
          actionDate: new Date(),
        }); // Updating local state after invitation is accepted},
        setRejectBtnQuery(""); /** disable spinner in accept button  */
      },
      (err) => {
        setRejectBtnQuery(""); /** disable spinner in accept button  */
        if (err) showSnackBar(err.data.message, "error");
      },
      true
    );
  };

  const showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };

  const { classes, theme, open, closeAction } = props;

  const [rejectBtnQuery, setRejectBtnQuery] = useState("");
  const [btnQuery, setBtnQuery] = useState("");

  const getTranslatedId=(value)=> {
    switch (value) {
      case "Accepted":
        value = "common.accept.label2";
        break;
      case "Rejected":
        value = "common.reject.label";
        break;
    }
    return value;
  }

  return (
    <CustomDialog
      title={
        <FormattedMessage id="invitations.label" defaultMessage="Invitations" />
      }
      dialogProps={{
        open: open,
        onClose: handleDialogClose,
        PaperProps: {
          className: classes.dialogPaperCmp,
          style: { maxHeight: 597,overflow: 'auto' }
        },
      }}
    >
      <div className={classes.contentCnt}>
        <div className={classes.headingBar}>
          <Typography variant="h5">
            <FormattedMessage
              id="invitations.pending-invites.label"
              defaultMessage=" Pending Invites"
            />
            <span className={classes.invitationsCount}>
              {pendingInvitations.length ? (
                <>({pendingInvitations.length})</>
              ) : null}
            </span>
          </Typography>
        </div>
        {pendingInvitations.length ? (
          <ul className={classes.invitationList}>
            {pendingInvitations.map((m) => {
              return (
                <li>
                  <div className={classes.invitationDetailsCnt}>
                    <CustomAvatar
                      otherMember={{
                        imageUrl: m.invitedBy.imageUrl,
                        fullName: m.invitedBy.fullName,
                        lastName: "",
                        email: m.invitedBy.fullName,
                      }}
                      size="xmedium"
                      disableCard={true}
                    />
                    <div className={classes.invitationTeamDetails}>
                      <Typography
                        variant="h5"
                        className={classes.invitedByUser}
                      >
                        {m.invitedBy.fullName}
                      </Typography>
                      <Typography
                        variant="body2"
                        className={classes.inviteTeamText}
                      >
                        <span>
                          <FormattedMessage
                            id="invitations.invite.action"
                            defaultMessage="invited you to join team"
                          />
                          &nbsp;
                        </span>
                        <span className={classes.teamName}>
                          {m.team.teamName}
                        </span>
                      </Typography>
                    </div>
                  </div>
                  <div className={classes.invitationBtnCnt}>
                    <CustomButton
                      onClick={() => handleRejectTeamInvitation(m)}
                      style={{ marginRight: 16 }}
                      btnType="gray"
                      variant="outlined"
                      query={rejectBtnQuery}
                    >
                      <FormattedMessage
                        id="timesheet.reject.label"
                        defaultMessage="Reject"
                      />
                    </CustomButton>
                    <CustomButton
                      onClick={() => handleAcceptTeamInvitation(m)}
                      btnType="success"
                      variant="contained"
                      query={btnQuery}
                    >
                      <FormattedMessage
                        id="common.accept.label"
                        defaultMessage="Accept"
                      />
                    </CustomButton>
                  </div>
                </li>
              );
            })}
          </ul>
        ) : (
          <div className={classes.emptryStateCnt}>
            <Typography variant="body2" className={classes.noInvitationsText}>
              <FormattedMessage
                id="invitations.empty"
                defaultMessage="No pending invitations right now."
              />
            </Typography>
          </div>
        )}
        {/* Invitation History starts here */}
        <div className={classes.headingBar}>
          <Typography variant="h5">
            {" "}
            <FormattedMessage
              id="invitations.invite.label"
              defaultMessage="Invitation History"
            />
          </Typography>
        </div>
        <Grid container className={classes.historyTableHeader}>
          <Grid item xs={4}>
            <Typography variant="body2" className={classes.headerTitle}>
              <FormattedMessage
                id="invitations.invite.title"
                defaultMessage="Invited By"
              />
            </Typography>
          </Grid>
          <Grid item xs={4}>
            <Typography variant="body2" className={classes.headerTitle}>
              <FormattedMessage id="common.team.label" defaultMessage="Team" />
            </Typography>
          </Grid>
          <Grid item xs={4}>
            <Typography variant="body2" className={classes.headerTitle}>
              <FormattedMessage
                id="common.status.label"
                defaultMessage="Status"
              />
            </Typography>
          </Grid>
        </Grid>
        {acceptedInvitations.map((t) => {
          return (
            <Grid container className={classes.bodyRow}>
              {/* First Column */}
              <Grid item xs={4}>
                <div className={classes.inviteByColumn}>
                  <Typography variant="h6">{t.invitedBy.fullName}</Typography>{" "}
                  <Typography variant="body2">
                    on {moment(t.invitedOn).format("MMM D, YYYY")}
                  </Typography>
                </div>
              </Grid>
              {/* Second Column */}
              <Grid item xs={4}>
                <div className={classes.inviteToColumn}>
                  <CustomAvatar
                    otherTeam={{
                      // Avatar accepts team avatar in this format of json
                      companyImage: t.team.teamImage,
                      companyName: t.team.teamName,
                    }}
                    disableCard={true}
                    size="xsmall"
                  />
                  <div className={classes.invitationTeamDetails}>
                    <Typography
                      variant="body2"
                      className={classes.inviteTeamText}
                    >
                      <span className={classes.teamName}>
                        {t.team.teamName}
                      </span>
                    </Typography>
                  </div>
                </div>
              </Grid>
              <Grid item xs={4}>
                <div className={classes.statusColumn}>
                  <Typography
                    variant="h6"
                    style={{
                      color:
                        t.status == "Accepted"
                          ? theme.palette.text.green
                          : theme.palette.text.danger,
                    }}
                  >
                      <FormattedMessage
                      id={getTranslatedId(t.status)}
                      defaultMessage={t.status}
                      />
                  </Typography>{" "}
                  <Typography variant="body2">
                    {t.actionDate
                      ? `on ${moment(t.actionDate).format("MMM D, YYYY")}`
                      : ""}
                  </Typography>
                </div>
              </Grid>
            </Grid>
          );
        })}
      </div>
    </CustomDialog>
  );
}

export default compose(
  connect(null, { acceptTeamInvitation, rejectTeamInvitation }),
  withSnackbar,
  withStyles(invitationStyles, { withTheme: true })
)(InvitationsDialog);
